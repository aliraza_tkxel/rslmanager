﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Users
{
    public class RegisterUser
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public int TenancyId { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
