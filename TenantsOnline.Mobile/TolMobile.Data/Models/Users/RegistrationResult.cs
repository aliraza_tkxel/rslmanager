﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Users
{
    public class RegistrationResult
    {      
        public int CustomerId { get; set; }
        public string Error { get; set; }
    }
}
