﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Users
{
    public class User
    {
        public int Id { get; set; }
        public string AuthToken { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public int? Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public int? Gender { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string NINumber { get; set; }
        public DateTime DateOfBirth { get; set; }

        public string Occupation { get; set; }
        public int? MaritalStatus { get; set; }
        public int? Ethnicity { get; set; }
        public int? Religion { get; set; }
        public int? SexualOrientation { get; set; }
    }
}
