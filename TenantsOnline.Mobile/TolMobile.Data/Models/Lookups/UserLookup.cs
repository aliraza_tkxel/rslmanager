﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolMobile.Data.Models.Lookups
{
    public class UserLookup
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
    }

    public class UserTitle : UserLookup {}

    public class UserGender
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
    }

    public class UserOccupation : UserLookup {}
    public class UserStatus : UserLookup {}
}
