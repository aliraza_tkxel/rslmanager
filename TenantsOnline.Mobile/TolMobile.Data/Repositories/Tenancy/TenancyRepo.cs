﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface ITenancyRepo : IRepository<C_CUSTOMERTENANCY>
    {
        C_CUSTOMERTENANCY GetByUserId(int userId);
    }

    public class TenancyRepo : TolRepository<C_CUSTOMERTENANCY>, ITenancyRepo
    {
        public TenancyRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {

        }

        public C_CUSTOMERTENANCY GetByUserId(int userId)
        {
            return Get(x => x.CUSTOMERID == userId && x.ENDDATE == null);
        }

    }
}
