﻿using System;
using System.Linq;
using System.Linq.Expressions;
using TolMobile.Data.Context;

namespace TolMobile.Data
{
    public class XSimpleRepo<T> : ISimpleRepo<T> where T : class
    {
        protected readonly ITolDbContextProvider _ctxProvider;
        protected BaseModelContext _ctx
        {
            get
            {
                return _ctxProvider.DataContext;
            }
        }

        public XSimpleRepo(ITolDbContextProvider ctx)
        {
            _ctxProvider = ctx;
        }

        public void Add(T entity)
        {
            _ctx.Set<T>().Add(entity);
        }

        public void Remove(T entity)
        {
            _ctx.Set<T>().Remove(entity);
        }

        public IQueryable<T> GetAll()
        {
            return _ctx.Set<T>();
        }

        public T Get(int id)
        {
            return _ctx.Set<T>().Find(id);
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return _ctx.Set<T>().Where(predicate);
        }

        public void Save()
        {
            _ctx.SaveChanges();
        }
    }


}
