﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface IRentRepo 
    {
        IEnumerable<RentLine> GetRentStatement(int tenancyId);
        IEnumerable<RentLine> GetRentStatement(int tenancyId, DateTime startDate, DateTime endDate);
        HB GetHousingBenefit(int tenancyId);
        decimal? GetAccountBalance(int tenancyId);
    }

    public class RentRepo : IRentRepo
    {
        private RSLEntityModelContext _ctx;

        public RentRepo(ITolDbContextProvider ctx)
        {
            _ctx = ctx.DataContext;
        }

        public IEnumerable<RentLine> GetRentStatement(int tenancyId)
        {
            return _ctx.GetRentStatement(tenancyId);
        }

        public IEnumerable<RentLine> GetRentStatement(int tenancyId, DateTime startDate, DateTime endDate)
        {
            return _ctx.GetRentStatementForPeriod(tenancyId, startDate, endDate);
        }

        public HB GetHousingBenefit(int tenancyId)
        {
            return _ctx.GetHousingBenefit(tenancyId.ToString()).Single();
        }
        
        public decimal? GetAccountBalance(int tenancyId)
        {
            return _ctx.GetAccountBalance(tenancyId).Single();
        }
    }
}
