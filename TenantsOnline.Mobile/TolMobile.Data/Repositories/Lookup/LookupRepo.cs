﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Context;
using System.Linq.Expressions;
using System.Data.Objects;

namespace TolMobile.Data.Repositories.Lookup
{
    public interface ITitlesRepo : IRepository<G_TITLE> {}
    public class TitlesRepo : TolRepository<G_TITLE>, ITitlesRepo
    {
        public TitlesRepo(ITolDbContextProvider ctx) : base(ctx) {}        
    }

    public interface IMaritalStatusRepo : IRepository<G_MARITALSTATUS> { }
    public class MaritalStatusRepo : TolRepository<G_MARITALSTATUS>, IMaritalStatusRepo
    {
        public MaritalStatusRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface IEthnicityRepo : IRepository<G_ETHNICITY> { }
    public class EthnicityRepo : TolRepository<G_ETHNICITY>, IEthnicityRepo
    {
        public EthnicityRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface IReligionRepo : IRepository<G_RELIGION> { }
    public class ReligionRepo : TolRepository<G_RELIGION>, IReligionRepo
    {
        public ReligionRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface ISexualOrientationRepo : IRepository<G_SEXUALORIENTATION> { }
    public class SexualOrientationRepo : TolRepository<G_SEXUALORIENTATION>, ISexualOrientationRepo
    {
        public SexualOrientationRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface ICommunicationRepo : IRepository<G_COMMUNICATION> { }
    public class CommunicationRepo : TolRepository<G_COMMUNICATION>, ICommunicationRepo
    {
        public CommunicationRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface IPreferredContactRepo : IRepository<G_PREFEREDCONTACT> { }
    public class PreferredContactRepo : TolRepository<G_PREFEREDCONTACT>, IPreferredContactRepo
    {
        public PreferredContactRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface ILanguageRepo : IRepository<G_LANGUAGES> { }
    public class LanguageRepo : TolRepository<G_LANGUAGES>, ILanguageRepo
    {
        public LanguageRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface IDisabilityRepo : IRepository<G_DISABILITY> { }
    public class DisabilityRepo : TolRepository<G_DISABILITY>, IDisabilityRepo
    {
        public DisabilityRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface ITerminationReasonRepo : IRepository<C_TERMINATION_REASON> { }
    public class TerminationReasonRepo : TolRepository<C_TERMINATION_REASON>, ITerminationReasonRepo
    {
        public TerminationReasonRepo(ITolDbContextProvider ctx) : base(ctx) { }
    }

    public interface ILookupRepo<T> where T : class
    {
        void Add(T entity);
        void Remove(T entity);
        IQueryable<T> GetAll();
        T Get(Expression<Func<T, bool>> predicate);

        IQueryable<T> Where(Expression<Func<T, bool>> predicate);
        void Save();
    }
    
     public class LookupRepo<T> : ILookupRepo<T> where T : class
    {
        protected readonly ITolDbContextProvider _ctxProvider;
        private IObjectSet<T> _objectSet;
        protected RSLEntityModelContext _ctx
        {
            get
            {
                return _ctxProvider.DataContext;
            }
        }

        public LookupRepo(ITolDbContextProvider ctx)
        {
            _ctxProvider = ctx;
            _objectSet = ctx.DataContext.CreateObjectSet<T>();            
        }
      
        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.SingleOrDefault(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return _objectSet.AsQueryable<T>();
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.Where(predicate);
        }

        public void Add(T entity)
        {
            _objectSet.AddObject(entity);            
        }

        public void Remove(T entity)
        {
            _objectSet.DeleteObject(entity);         
        }

        public IQueryable<T> Query()
        {
            return _objectSet.AsQueryable();
        }
      
        public void Save()
        {
            _ctx.SaveChanges();
        }
    }

}
