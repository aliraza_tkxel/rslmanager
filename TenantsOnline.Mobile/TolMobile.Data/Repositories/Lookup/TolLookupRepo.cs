﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Context;
using System.Linq.Expressions;
using System.Data.Objects;

namespace TolMobile.Data.Repositories.Lookup
{
    public class TolLookupModel
    {
        public int id { get; set; }
        public string Name { get; set; }
    }

    public interface ITolLookupRepo  
    {
        IEnumerable<TolLookupValue> GetParkingTypes();
        IEnumerable<TolLookupValue> GetASBCategories();
        IEnumerable<TolLookupValue> GetComplaintTypes();
        IEnumerable<TolLookupValue> GetAbandonedTypes();
    }

    public class TolLookupRepo : ITolLookupRepo
    {
        private RSLEntityModelContext _ctx;
        public TolLookupRepo(ITolDbContextProvider ctx) 
        {
            _ctx = ctx.DataContext;
        }
            
        public IEnumerable<TolLookupValue> GetParkingTypes()
        {
            return _ctx.GetParkingTypeLookups();
        }

        public IEnumerable<TolLookupValue> GetASBCategories()
        {
            return _ctx.GetASBCategories();
        }

        public IEnumerable<TolLookupValue> GetComplaintTypes()
        {
            return _ctx.GetComplaintTypes();
        }

        public IEnumerable<TolLookupValue> GetAbandonedTypes()
        {
            return _ctx.GetAbandonedTypes();
        }
       
    } 
}