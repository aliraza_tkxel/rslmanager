﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface INewsRepo : IRepository<I_NEWS>
    {
        I_NEWS GetLatest();
        IEnumerable<I_NEWS> GetAll();
    }

    public class NewsRepo : TolRepository<I_NEWS>, INewsRepo
    {
        private RSLEntityModelContext _ctx;

        public NewsRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {
            _ctx = ctx.DataContext;
        }

        public I_NEWS GetLatest()
        {
            return _ctx.GetLatestNews().SingleOrDefault();          
        }

        public IEnumerable<I_NEWS> GetAll()
        {
            return _ctx.GetNews();
        }
      
    }
}
