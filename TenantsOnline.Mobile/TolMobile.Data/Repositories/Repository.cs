﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Context;
using System.Linq.Expressions;
using System.Data.Objects;

namespace TolMobile.Data.Repositories
{
    public class TolRepository<T> : IRepository<T> where T : class
    {
        private readonly ITolDbContextProvider _ctxProvider;
        private IObjectSet<T> _objectSet;

        protected RSLEntityModelContext _ctx
        {
            get
            {
                return _ctxProvider.DataContext;
            }
        }

        public TolRepository(ITolDbContextProvider ctx)
        {
            _ctxProvider = ctx;            
            _objectSet = ctx.DataContext.CreateObjectSet<T>();
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.SingleOrDefault(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return _objectSet.AsQueryable<T>();
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.Where(predicate);
        }

        public void Add(T entity)
        {
            _objectSet.AddObject(entity);            
        }

        public void Remove(T entity)
        {
            _objectSet.DeleteObject(entity);         
        }

        public IQueryable<T> Query()
        {
            return _objectSet.AsQueryable();
        }
      
        public void Save()
        {
            _ctx.SaveChanges();
        }
    }
}
