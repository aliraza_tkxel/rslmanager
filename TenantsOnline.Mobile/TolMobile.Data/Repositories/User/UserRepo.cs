﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;
using System.Data.Objects;

namespace TolMobile.Data
{
    public interface IUserRepo : IRepository<C__CUSTOMER>
    {
        C__CUSTOMER GetByUserId(int userId);
        RegistrationResult TenantRegistration(RegisterUser user);
    }

    public class UserRepo : TolRepository<C__CUSTOMER>, IUserRepo
    {
        public UserRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {

        }

        public C__CUSTOMER GetByUserId(int userId)
        {
            return Get(x => x.CUSTOMERID == userId);
        }

        public RegistrationResult TenantRegistration(RegisterUser user)
        {
            var customerId = new ObjectParameter("CUSTOMERID", typeof(int));
            var errorMsg = new ObjectParameter("ERRMESSAGE", typeof(string));

            _ctx.TenantRegistration(user.FirstName, user.LastName, user.TenancyId, user.Email, user.PostCode, user.Password, user.DateOfBirth,
                customerId, errorMsg);

            var registrationResult = new RegistrationResult
            {
                CustomerId = Convert.ToInt32(customerId.Value),
                Error = errorMsg.Value.ToString()
            };

            return registrationResult;
        }

    }
}
