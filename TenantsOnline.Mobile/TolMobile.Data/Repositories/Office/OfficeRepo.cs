﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface IOfficeRepo : IRepository<TO_OFFICE>
    {
        IEnumerable<TO_OFFICE> GetAll();
        TO_OFFICE GetById(int officeId);
    }

    public class OfficeRepo : TolRepository<TO_OFFICE>, IOfficeRepo
    {
        private RSLEntityModelContext _ctx;

        public OfficeRepo(ITolDbContextProvider ctx) : base(ctx)
        {
            _ctx = ctx.DataContext;
        }

        public IEnumerable<TO_OFFICE> GetAll()
        {
            return base.GetAll();            
        }

        public TO_OFFICE GetById(int officeId)
        {
            return base.Get(x => x.OFFICEID == officeId);
        }
      
    }
}
