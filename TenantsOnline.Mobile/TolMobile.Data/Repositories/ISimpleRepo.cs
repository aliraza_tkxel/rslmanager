﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace TolMobile.Data
{

    public interface ISimpleRepo<T> where T : class
    {
        void Add(T entity);
        void Remove(T entity);
        IQueryable<T> GetAll();
        T Get(int id);
        
        IQueryable<T> Where(Expression<Func<T, bool>> predicate);        
        void Save();        
    }
}