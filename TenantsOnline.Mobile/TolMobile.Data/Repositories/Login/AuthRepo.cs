﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface IAuthRepo : IRepository<TO_LOGIN>
    {
        TO_LOGIN GetByUsernameAndPwd(string userName, string password);
        TO_LOGIN GetByAuthToken(string authToken);
    }

    public class AuthRepo : TolRepository<TO_LOGIN>, IAuthRepo
    {
        public AuthRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {
            
        }

        public TO_LOGIN GetByUsernameAndPwd(string userName, string password)
        {
            var qry = from c in _ctx.C__CUSTOMER
                      join a in _ctx.C_ADDRESS on c.CUSTOMERID equals a.CUSTOMERID
                      join l in _ctx.TO_LOGIN on c.CUSTOMERID equals l.CustomerID
                      where (a.ISDEFAULT == 1 && l.Active == 1) && (a.EMAIL == userName && l.Password == password)
                      select l;

            return qry.SingleOrDefault();
        }

        public TO_LOGIN GetByAuthToken(string authToken)
        {
            return Where(x => x.AuthToken == authToken).SingleOrDefault();
        }
    }
}
