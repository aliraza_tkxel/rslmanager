﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface IContactRepo : IRepository<C_ADDRESS>
    {
        C_ADDRESS GetActive(int userId);
    }

    public class ContactRepo : TolRepository<C_ADDRESS>, IContactRepo
    {
        public ContactRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {

        }

        public C_ADDRESS GetActive(int userId)
        {
            return Get(x => x.CUSTOMERID == userId && x.ISDEFAULT == 1);
        }
    }
}
