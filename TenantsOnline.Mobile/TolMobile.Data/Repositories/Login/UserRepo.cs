﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface IUserRepo : IRepository<C__CUSTOMER>
    {
        C__CUSTOMER GetByUserId(int userId);
    }

    public class UserRepo : TolRepository<C__CUSTOMER>, IUserRepo
    {
        public UserRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {

        }

        public C__CUSTOMER GetByUserId(int userId)
        {
            return Get(x=>x.CUSTOMERID == userId);            
        }
      
    }
}
