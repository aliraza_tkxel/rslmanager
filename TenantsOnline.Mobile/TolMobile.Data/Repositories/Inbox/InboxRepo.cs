﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Context;
using TolMobile.Data.Repositories;
using System.Data.Entity;

namespace TolMobile.Data
{
    public interface IInboxRepo : IRepository<EnquiryLine>
    {
        IEnumerable<EnquiryLine> GetCustomerEnquiries(int? customerId, string responseStatus);
    }

    public class InboxRepo : TolRepository<EnquiryLine>, IInboxRepo
    {
        private RSLEntityModelContext _ctx;

        public InboxRepo(ITolDbContextProvider ctx)
            : base(ctx)
        {
            _ctx = ctx.DataContext;
        }

        public IEnumerable<EnquiryLine> GetCustomerEnquiries(int? customerId, string responseStatus)
        {
            return _ctx.GetCustomerEnquiries(customerId, responseStatus);
        }

    }
}
