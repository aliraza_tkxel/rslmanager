﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TolMobile.Data.Context.Initialisers;

namespace TolMobile.Data.Context
{
    public class TolTestDbContext : BaseModelContext
    {

        public static void InitialiseDatabase(IDatabaseInitializer<TolTestDbContext> strategy)
        {
            var sqlCeConnection = new SqlCeConnectionFactory("System.Data.SqlServerCe.4.0");
            Database.DefaultConnectionFactory = sqlCeConnection;

            Database.SetInitializer(strategy);
            
            new TolTestDbContext().Database.Initialize(true);    
        }
    }
}
