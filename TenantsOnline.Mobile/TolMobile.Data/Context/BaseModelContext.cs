﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Diagnostics;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Models.Lookups;
using TolMobile.Data.Models;

namespace TolMobile.Data.Context
{
    public class BaseModelContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserTitle> UserTitle { get; set; }
        public DbSet<UserOccupation> UserOccupations { get; set; }
        public DbSet<UserGender> UserGenders { get; set; }
        public DbSet<UserStatus> UserStatus { get; set; }
        public DbSet<Country> Country { get; set; }
        
        public BaseModelContext(string nameOrConnection)
            : base(nameOrConnection)
        {
        }

        public BaseModelContext()
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Debug.WriteLine("Re-creating db...");
            base.OnModelCreating(modelBuilder);
        }
    }
}
