﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;

namespace TolMobile.Data.Context.Initialisers
{

    public class DefaultSeeder<T> where T : BaseModelContext
    {
        private readonly T _ctx;

        public DefaultSeeder(T context)
        {
            _ctx = context;


            _ctx.Users.Add(new User { Username = "jon", Password = "123", Name = "Jon Swain", Gender = "Male" });

            _ctx.SaveChanges();
        }
    }

}
