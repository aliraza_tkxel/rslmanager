﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace TolMobile.Data.Context.Initialisers
{
    public class TolModelInitialiser : DropCreateDatabaseAlways<TolTestDbContext>
    {
        protected override void Seed(TolTestDbContext context)
        {
            new DefaultSeeder<TolTestDbContext>(context);
            base.Seed(context);
        }
    }
}
