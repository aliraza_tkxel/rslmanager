﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Models.Lookups;

namespace TolMobile.Data.Context.Initialisers
{

    public class DefaultSeeder<T> where T : BaseModelContext
    {
        private readonly T _ctx;

        public DefaultSeeder(T context)
        {
            _ctx = context;            
        }
    }

    public class TestDbSeeder<T> where T : BaseModelContext
    {
        private readonly T _ctx;

        public TestDbSeeder(T context)
        {
            _ctx = context;
            
            _ctx.Users.Add(new User
            {
                Username = "jon",
                Password = "123",
                AuthToken = "pa55w0rd",
                DateOfBirth = DateTime.Now,
                FirstName = "Jon",
                LastName = "Swain",
                MiddleName = "Lee",
                NINumber = "NI12345",
                Gender = 1
            });

            _ctx.Users.Add(new User
            {
                Username = "peter",
                Password = "5678",
                AuthToken = Guid.NewGuid().ToString(),
                DateOfBirth = DateTime.Now,
                FirstName = "Peter",
                LastName = "Thompson",
                MiddleName = "",
                NINumber = "NI14552345",
                Gender = 1
            });

            _ctx.UserGenders.Add(new UserGender { ItemName = "Male" });
            _ctx.UserGenders.Add(new UserGender { ItemName = "Female" });



            //_ctx.UserOccupations.Add(new UserOccupation { ItemName = "Housewife" });
            //_ctx.UserOccupations.Add(new UserOccupation { ItemName = "Skilled" });
            //_ctx.UserOccupations.Add(new UserOccupation { ItemName = "Non-Skilled" });

            //_ctx.UserStatus.Add(new UserStatus { ItemName = "Married" });
            //_ctx.UserStatus.Add(new UserStatus { ItemName = "Single" });
            //_ctx.UserStatus.Add(new UserStatus { ItemName = "Divorced" });

            //_ctx.UserTitle.Add(new UserTitle { ItemName = "Mr" });
            //_ctx.UserTitle.Add(new UserTitle { ItemName = "Mrs" });
            //_ctx.UserTitle.Add(new UserTitle { ItemName = "Ms" });
            //_ctx.UserTitle.Add(new UserTitle { ItemName = "Miss" });

            _ctx.SaveChanges();
        }
    }
}
