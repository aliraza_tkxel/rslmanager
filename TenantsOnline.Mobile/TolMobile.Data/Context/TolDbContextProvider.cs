﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolMobile.Data.Context
{    
    public interface ITolDbContextProvider
    {
        RSLEntityModelContext DataContext { get; }
    }

    public class TolDbContextProvider : ITolDbContextProvider
    {
        //public BaseModelContext DataContext { get; private set; }
        public RSLEntityModelContext DataContext { get; private set; }

        public TolDbContextProvider()
        {
            DataContext = new RSLEntityModelContext();
        }
    }

    //public class TestDbContextProvider : ITolDbContextProvider, IDisposable
    //{
    //    public BaseModelContext DataContext { get; private set; }

    //    public TestDbContextProvider()
    //    {
    //        DataContext = new TolTestDbContext();
    //    }

    //    public void Initialise(bool force)
    //    {
    //        DataContext.Database.Initialize(force);
    //    }

    //    public void Dispose()
    //    {
    //        DataContext.Dispose();
    //    }
    //}
}
