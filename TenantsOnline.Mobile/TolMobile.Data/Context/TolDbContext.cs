﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;

namespace TolMobile.Data.Context
{
    public class TolDbContext : RSLEntityModelContext
    {
        public TolDbContext()
            : base("TolMobileDb")
        {

        }

        public TolDbContext(string dbName)
            : base(dbName)
        {

        }

        public static void InitialiseDatabase()
        {            
            //Database.SetInitializer(strategy);            
            //new RSLEntityModelContext()..Database.Initialize(true);
        }
    }
}
