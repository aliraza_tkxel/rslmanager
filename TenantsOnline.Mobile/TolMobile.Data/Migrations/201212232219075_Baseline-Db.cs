namespace TolMobile.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BaselineDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuthToken = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        Title = c.Int(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        Gender = c.Int(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        NINumber = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Occupation = c.String(),
                        MaritalStatus = c.Int(),
                        Ethnicity = c.Int(),
                        Religion = c.Int(),
                        SexualOrientation = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserTitles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserOccupations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserGenders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        CountryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Priority = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CountryId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Countries");
            DropTable("dbo.UserStatus");
            DropTable("dbo.UserGenders");
            DropTable("dbo.UserOccupations");
            DropTable("dbo.UserTitles");
            DropTable("dbo.Users");
        }
    }
}
