Ext.define('Lookup', {
    extend: 'Ext.data.Model',
    config: {
        fields: [       
			{name: 'id',  type: 'int' },
			{name: 'itemname',  type: 'string' }
        ]
    }
});

Ext.define('Details', {
    extend: 'Ext.data.Model',
    config: {
        fields: [       
			{name: 'language',  type: 'int' }
        ]
    }
});


var myLookupStore = Ext.create('Ext.data.Store', {
    model: 'Lookup',
    data : [
        { id: 1, itemname: 'English' },
        { id: 2, itemname: 'Spanish' },
        { id: 3, itemname: 'French' },
		{ id: 3, itemname: 'Chinese' },
    ]
});	

var myDetailsStore = Ext.create('Ext.data.Store', {
    model: 'Details',
    data : [
        { language: "1,3" },
    ]
});

var p = Ext.create('Ext.form.Panel', {
    fullscreen: true,    
    items: [		
        {
            xtype: 'fieldset',
            title: 'Select',
            items: [               
				{
                          xtype: 'multiselectfield',                    
                          name: 'language',
                          store: myLookupStore,
                          valueField: 'id',
                          displayField: 'itemname'
                     }
            ]
        }
    ]
});
Ext.Viewport.setActiveItem(p);