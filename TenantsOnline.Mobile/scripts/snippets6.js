Ext.define("Overlay", {
	extend: "Ext.Container",
	
	config: {
		xtype: 'panel',
		id: "myoverlay",
		modal: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		centered: true,
		width: Ext.os.deviceType == 'Phone' ? 260 : 400,
		height: Ext.os.deviceType == 'Phone' ? 220 : 400,
		styleHtmlContent: true,
		items: [
			{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Overlay Title'
			},
			{
					xtype: "list",
					itemTpl: '{title},{author}',
					flex: 1,
					mode: 'MULTI',
					store: {
						autoLoad: true,
						fields : ['title', 'author'],
						proxy: {
							type: 'jsonp',
							url: 'https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=http://feeds.feedburner.com/SenchaBlog',
							reader: {
								type: 'json',
								rootProperty: 'responseData.feed.entries'
							}
						}
					}
				}
		],
		scrollable: true
	}
	
});

var x = Ext.create("Overlay");
Ext.Viewport.add(x);