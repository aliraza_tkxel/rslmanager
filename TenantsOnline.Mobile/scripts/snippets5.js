
Ext.define('Details', {
    extend: 'Ext.data.Model',
    config: {
        fields: [       
			{name: 'dob',  type: 'date', dateFormat: 'c' }
        ]
    }
});

Ext.define('UkDatePicker', {	
	extend: 'Ext.picker.Date',
	alias: 'widget.ukdatepicker',
	config: {
		slotOrder: ["day", "month", "year"],
		yearFrom: 1900	
	}
});

var p = Ext.create('Ext.form.Panel', {
    fullscreen: true,
    items: [
        {
            xtype: 'fieldset',
            title: 'Select',
            items: [               
				 {
                        xtype: 'datepickerfield',
                        label: 'Date of Birth *',
                        name: 'dob',                                                
                        picker: {
                            xtype: 'ukdatepicker'
                        }
					}
            ]
        }
    ]
});
Ext.Viewport.removeAt(0)
Ext.Viewport.add(p);

var m = Ext.create("Details");
m.set("dob", "1977-08-01T00:00:00")
m.get("dob")
p.setRecord(m);

Ext.Date.format(new Date(),"d/m/Y")