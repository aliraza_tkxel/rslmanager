Ext.define('TolMobile.store.EnquiryViews', {
    extend: 'Ext.data.Store',
    
    requires: [
        
    ],

    config: {
        data: [
				{ Id: 1, Xid: 'endtenancyview', DisplayName: "I'd like to end my tenancy" },
				{ Id: 2, Xid: 'requestgarageview', DisplayName: "I want a garage" },
				{ Id: 3, Xid: 'requestparkingview', DisplayName: "I want a parking space" },
				{ Id: 4, Xid: 'reportcomplaintview', DisplayName: "I want to make a complaint" },
				{ Id: 5, Xid: 'reportasb', DisplayName: "I'm a victim of ASB" },
				{ Id: 6, Xid: 'reportabandonedvehicle', DisplayName: "I can see an abandoned vehicle" },
				{ Id: 7, Xid: 'reportabandonedproperty', DisplayName: "I can see an abandoned property" }
		],
        autoLoad: true,
        storeId: 'theEnquiryViewStore'
	}
});