
Ext.define("Overlay", {
	extend: "Ext.Container",
	
	config: {
		id: 'myoverlay',
		xtype: 'panel',
		modal: true,
		hideOnMaskTap: true,		
		centered: true,
		width: Ext.os.deviceType == 'Phone' ? 260 : 400,
		height: Ext.os.deviceType == 'Phone' ? 220 : 400,
		styleHtmlContent: true,
		layout: "fit",
		items: [
			{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Select Disabilities',
				id: 'pickerTitle'
			},
			{
				docked: 'bottom',
				xtype: 'toolbar',				
				items: [
                    {
                        xtype: 'button',
                        ui: 'action',
                        text: 'Ok'
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel'
                    }
                ]
			},
			{				
					id: "mylist",
					xtype: "list",
					itemTpl: '{ItemName}',
					flex: 1,
					mode: 'MULTI',
					store: "theDisabilityStore"								
			}			
		]
	},
	
	setTitle: function(titleText) {		
		var title = this.getComponent("pickerTitle");		
		title.setTitle(titleText);
	},
	
	getSelectedValues: function() {		
		
		doGetSelectedValues(this);
	},
	
	setSelectedValues: function(values) {

		doSetSelectedValues(values, this);		
	}
	
});

var v = Ext.create("Overlay");
Ext.Viewport.add(v);

doGetSelectedValues = function(scope) { 
	var l = scope.getComponent("mylist");
	var values = l.getSelection();
	var csv = "";
	Ext.Array.each(values, function(v) {
		csv += v.get("Id") + ",";
		console.log(csv);
	});
}

doSetSelectedValues = function(values, this) {
	var l = scope.getComponent("mylist");
	var toSet = values.split(",");
	Ext.Array.each(toSet, function(v) {
		if ( toSet ) {
			
		}
	});	
}

var l = v.getComponent("mylist")