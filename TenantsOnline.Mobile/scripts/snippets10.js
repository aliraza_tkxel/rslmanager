Ext.define('EnquiryStore', {
    extend: 'Ext.data.Store',

    config: {
        data: [ 
				{ Id: 1, DisplayName: "I'd like to end my tenancy" },
				{ Id: 2, DisplayName: "I want a garage" },
				{ Id: 3, DisplayName: "I want a parking space" },
				{ Id: 4, DisplayName: "I want to make a complaint" },
				{ Id: 5, DisplayName: "I'm a victim of ASB" },
				{ Id: 6, DisplayName: "I can see an abandoned vehicle" },
				{ Id: 7, DisplayName: "I can see an abandoned property" }
		],
        storeId: 'theEnquiryViewStore',
        fields: [
            {
                name: 'Id'
            },
            {
                name: 'DisplayName'
            }
        ]
    }
});

//var s = Ext.create("MyArrayStore");
//var s = Ext.create("EnquiryStore");

Ext.define('MyList', {
    extend: 'Ext.Container',

    config: {
        layout: {
            type: 'fit'
        },
        items: [
			 {
                xtype: 'titlebar',
                docked: 'top'
            },
            {
                xtype: 'list',
                itemTpl: [
                    '<div>List Item {DisplayName}</div>'
                ],
				//store: s
				store: 'theEnquiryViewStore'
            }
        ]
    }

});

var l = Ext.create("MyList")