Ext.define('TolMobile.proxy.SecureProxy2', {
    alias: 'proxy.securerestproxy2',
    extend: 'Ext.data.proxy.Rest',

    // Set your proxy alias

    constructor: function () {       
        this.reader = {
            type: 'json'
        }
        this.callParent(arguments);
    },

    config: {
        type: 'rest',
        headers: {
            "AUTH-TOKEN": "pa55w0rd"
        }

    }
});

Ext.define('TolMobile.store.UserDetails', {
    extend: 'Ext.data.Store',
    
    // requires: [
        // 'TolMobile.model.UserDetails',
        // 'TolMobile.proxy.SecureProxy'
    // ],

    config: {        
        model: 'TolMobile.model.UserDetails',
        storeId: 'theUserDetailsStore',
        proxy: {
            type: 'securerestproxy2',
            url: '/api/user/',
            reader: {
                type: 'json'
            }
        }
    }    
});

var s = Ext.create("TolMobile.store.UserDetails")         

s.getData().getAt(0).get("FirstName")
