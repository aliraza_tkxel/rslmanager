
		Ext.define('Lookup', {
			extend: 'Ext.data.Model',
			config: {
				fields: [       
					{name: 'id',  type: 'int' },
					{name: 'itemname',  type: 'string' }
				]
			}
		});

		Ext.define('Details', {
			extend: 'Ext.data.Model',	
			config: {
				fields: [       
					{name: 'language',  type: 'auto' }
				]
			}
		});


		var myLookupStore = Ext.create('Ext.data.Store', {
			model: 'Lookup',
			data : [
				{ id: 1, itemname: 'English' },
				{ id: 2, itemname: 'Spanish' },
				{ id: 3, itemname: 'French' },
				{ id: 3, itemname: 'Chinese' },
			]
		});	

		var myDetailsStore = Ext.create('Ext.data.Store', {
			model: 'Details',
			data : [
				{ language: [3] },
			]
		});

		var p = Ext.create('Ext.form.Panel', {
			fullscreen: true,    
			items: [		
				{
					xtype: 'fieldset',
					title: 'Select',
					items: [               
						{
								  xtype: 'multiselectfield',                    
								  name: 'language',
								  store: myLookupStore,
								  valueField: 'id',
								  displayField: 'itemname'
							 }
					]
				}
			]
		});		
		var r = myDetailsStore.getAt(0);		
		Ext.Viewport.setActiveItem(p);		
		
		p.setRecord(r);		
		
		
	var g = {
		list: null,
		panel: null
	};
	
	function doHandler(scope) {
		console.log("ok tapped");
		console.log(scope);
			
		var list = Ext.ComponentQuery.query("#mylist")[0];
		g.panel = scope.listPanel;
		g.list = list;
		console.log(list);
		var values = list.getSelection();
		
		Ext.each(values, function(item) {
			console.log("selected value: "+item.data.id);
			//l.select(store.getById(id), true);
		});
		// loop through selected items
		console.log(values);
		scope.listPanel.hide();
	}	