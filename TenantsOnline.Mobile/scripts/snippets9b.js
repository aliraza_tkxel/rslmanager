var p = Ext.create('JonsPanel', {
    extend: 'Ext.Panel',

    config: {       
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                style: {
                    'text-align': 'left'
                },
                title: 'Home Panel',
                layout: {
                    pack: 'end',
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'MyButton16'
                    }
                ]
            },
            {
                xtype: 'button',
                id: 'mybutton',
                text: 'Home Panel'
            }
        ]
    }

});
Ext.Viewport.removeAll();
Ext.Viewport.add(p);	