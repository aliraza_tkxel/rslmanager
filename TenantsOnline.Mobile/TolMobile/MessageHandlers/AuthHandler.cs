﻿using System.Net.Http;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Linq;
using StructureMap;
using TolMobile.Services.Authentication;
using System.Web.Http.Dispatcher;
using System.Web.Http;

namespace TolMobile
{
    public class WebApiAuthenticationHandler : DelegatingHandler
    {
        private const string AuthToken = "AUTH-TOKEN";

        public WebApiAuthenticationHandler(HttpConfiguration httpConfiguration)
        {
            InnerHandler = new HttpControllerDispatcher(httpConfiguration);
        }

        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Method == HttpMethod.Options)
            {
                // cors pre-flight request, ignore auth
                return base.SendAsync(request, cancellationToken);
            }

            var requestAuthTokenList = GetRequestAuthTokens(request);
            if (ValidAuthorization(requestAuthTokenList))
            {
                return base.SendAsync(request, cancellationToken);
            }

            /*
            ** This will make the whole API protected by the API token.
            ** To only protect parts of the API then mark controllers/methods
            ** with the Authorize attribute and always return this:
            **
            ** return base.SendAsync(request, cancellationToken);
            */
            return Task<HttpResponseMessage>.Factory.StartNew(
                () =>
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("Authorization failed")
                    };

                    //var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized);                                                                                   
                    //resp.Headers.Add(SuppressFormsAuthenticationRedirectModule.SuppressFormsHeaderName,"true");
                    return resp;
                });
        }

        private static bool ValidAuthorization(IEnumerable<string> requestAuthTokens)
        {
            var authService = ObjectFactory.GetInstance<IWebApiAuthenticationService>();
            var authToken = requestAuthTokens.First();
            var okAuth = authService.AuthenticateForWebApi(authToken);

            return !string.IsNullOrEmpty(okAuth);
        }

        private static IEnumerable<string> GetRequestAuthTokens(HttpRequestMessage request)
        {
            IEnumerable<string> requestAuthTokens;
            if (!request.Headers.TryGetValues(AuthToken, out requestAuthTokens))
            {
                //Initialize list to contain a single not found token:
                requestAuthTokens = new[] { "No API token found" };
            }
            return requestAuthTokens;
        }
    }
}