﻿using System.Net.Http;
using System.IO;
using System;
using System.Net.Http.Headers;
using System.Linq;

public class TolMobileFormDataStreamProvider : MultipartFormDataStreamProvider
{
    public TolMobileFormDataStreamProvider(string path)
        : base(path)
    { }

    public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
    {
        // restrict what images can be selected
        var extensions = new[] { "png", "gif", "jpg" };
        var filename = headers.ContentDisposition.FileName.Replace("\"", string.Empty);

        if (filename.IndexOf('.') < 0)
            return Stream.Null;

        var extension = filename.Split('.').Last();
        
        var validExt = extensions.Any(i => i.Equals(extension, StringComparison.InvariantCultureIgnoreCase));
        if ( validExt ) 
        {
            return base.GetStream(parent, headers);
        }
        else
        {
            throw new Exception("Provider was supplied with an invalid file extension type for file:" + filename);
        }                   
    }

    public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
    {
        // override the filename which is stored by the provider (by default is bodypart_x)
        string oldfileName = headers.ContentDisposition.FileName.Replace("\"", string.Empty);
        string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(oldfileName);

        return newFileName;       
    }
}