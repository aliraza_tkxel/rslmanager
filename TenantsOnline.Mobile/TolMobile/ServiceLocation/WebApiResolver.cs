﻿using System.Web.Http.Dependencies;
using System;
using System.Collections.Generic;
using StructureMap;
using System.Linq;

public class WebApiDependencyResolver : IDependencyResolver
{
    private readonly IContainer _container;
    public WebApiDependencyResolver(IContainer container)
    {
        _container = container;
    }

    public IDependencyScope BeginScope()
    {
        // This example does not support child scopes, so we simply return 'this'.
        return this;
    }

    public object GetService(Type serviceType)
    {
        try
        {
            return _container.GetInstance(serviceType);
        }
        catch
        {
            return null;
        }
    }

    public IEnumerable<object> GetServices(Type serviceType)
    {
        return _container.GetAllInstances<object>().Where(s => s.GetType() == serviceType);
    }

    public void Dispose()
    {
        // When BeginScope returns 'this', the Dispose method must be a no-op.
    }
}