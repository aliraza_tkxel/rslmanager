﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using TolMobile.Services.Exceptions;

namespace TolMobile.Extensions
{
    public static class ApiExceptionExtensions
    {
        private static readonly IDictionary<Type, HttpStatusCode> ExceptionsLookup = new Dictionary<Type, HttpStatusCode>
                                                                                         {                                                                 
                                                                                             { typeof(NotFoundException), HttpStatusCode.NotFound },                                                                                             
                                                                                             { typeof(ModelValidationException), HttpStatusCode.BadRequest },                                                                                            
                                                                                             { typeof(ServiceException), HttpStatusCode.InternalServerError }
                                                                                         };

        public static HttpStatusCode ConvertToHttpStatus(this Exception exception)
        {
            if (ExceptionsLookup.ContainsKey(exception.GetType()))
            {
                return ExceptionsLookup[exception.GetType()];
            }

            // rethrow exception if not in lookup
            throw exception;
        }
    }
}
