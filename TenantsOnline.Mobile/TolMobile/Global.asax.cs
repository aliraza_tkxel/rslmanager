﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using TolMobile.Core.AutoMapper;
using TolMobile.Core.StructureMap;
using StructureMap;
using TolMobile.Filters;
using TolMobile.Data.Context;
using System.Web.Http.Validation.Providers;
using System.Net.Http.Formatting;
using TolMobile.WebApiNLog;
using System.Web.Http.Tracing;
//using TolMobile.Data.Context.Initialisers;

namespace TolMobile
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ValidationFilterAttribute());
        }

        static void ConfigureWebApi(HttpConfiguration config)
        {
            //config.MessageHandlers.Add(new CorsHandler());
            //config.MessageHandlers.Add(new WebApiAuthenticationHandler());

            config.Filters.Add(new ServiceLayerExceptionFilter());
            config.Filters.Add(new ElmahHandledErrorLoggerFilter());

            config.DependencyResolver = new WebApiDependencyResolver(ObjectFactory.Container);

            //config.Formatters.Clear();            
            //config.Formatters.Add(new JsonMediaTypeFormatter());
            //config.Formatters.Add(new FormUrlEncodedMediaTypeFormatter());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");

            routes.MapHttpRoute("NoAuthRequiredForLogin", "api/auth/", new { Controller = "Auth" });
            routes.MapHttpRoute("NoAuthRequiredForRegister", "api/register/", new { Controller = "Register" });
            routes.MapHttpRoute("LookupApi", "api/lookup/{action}", new { Controller = "Lookup" });

            // rpc style route...
            routes.MapHttpRoute("EnquiryApi", "api/enquiry/{action}", new { Controller = "Enquiry" }, null, new WebApiAuthenticationHandler(GlobalConfiguration.Configuration));
            routes.MapHttpRoute("TestApi", "api/test/{action}", new { Controller = "Test" });
            routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional }, null, new WebApiAuthenticationHandler(GlobalConfiguration.Configuration));

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // bootstrap automapper
            AutoMapperBootstrap.Initialize();
            StructureMapBootStrap.Initialize();

            ConfigureWebApi(GlobalConfiguration.Configuration);

            GlobalConfiguration.Configuration.Services.RemoveAll(
                typeof(System.Web.Http.Validation.ModelValidatorProvider),
                v => v is InvalidModelValidatorProvider);

            GlobalConfiguration.Configuration.Services.Replace(typeof(ITraceWriter), new NLogTraceWriter());

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);


        }
    }
}