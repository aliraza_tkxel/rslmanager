﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using TolMobile.Extensions;

namespace TolMobile.Filters
{
    public class Error
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }

    public class ValidationFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;
            if (!modelState.IsValid)
            {
                var errors = actionContext.ModelState
                     .Where(e => e.Value.Errors.Count > 0)
                     .Select(e => new Error
                     {
                         Name = e.Key,
                         Message = e.Value.Errors.First().ErrorMessage
                     }).ToArray();

                actionContext.Response = actionContext.Request.CreateResponse<Error[]>(HttpStatusCode.BadRequest, errors);                
            }
        }        
    }
}