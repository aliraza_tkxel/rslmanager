﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using TolMobile.Services.Exceptions;
using System.Web.Http.ModelBinding;
using System.Net.Http;
using System.Net;
using TolMobile.Extensions;

namespace TolMobile.Filters
{
    public class ServiceLayerExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Response == null)
            {
                var exception = context.Exception as ModelValidationException;

                if (exception != null)
                {
                    var modelState = new ModelStateDictionary();
                    modelState.AddModelError(exception.Key, exception.Description);

                    var errors = modelState.ErrorsToJson();
                    context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, errors);
                }
                else
                {
                    context.Response = new HttpResponseMessage(context.Exception.ConvertToHttpStatus());
                }
            }

            base.OnException(context);
        }

    }
}