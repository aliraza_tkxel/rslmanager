/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/
// Limitation of profiles within framework is such that all required views are loaded in 
// from profiles regardless of whether the profile is active or not which means that we cannot
// grab references to views via their xtype alias.

// A possible solution:
// http://www.sencha.com/forum/showthread.php?197910-Problem-with-Sencha-Profiles/page2
// Load only the profile that we need so that xtypes are not overwritten:
var activeProfile = Ext.os.is.Phone ? ['Phone'] : ['Tablet'];

console.log("Active profile is:" + activeProfile);
console.log("Is iOS: " + Ext.os.is.iOS);
console.log("Is browser standalone: " + Ext.browser.is.Standalone);
console.log("Browser version: " + Ext.browser.version);
console.log("Browser engine: " + Ext.browser.engineName);

Ext.application({
    name: 'TolMobile',

    requires: [
		'Ext.viewport.Viewport',
        'Ext.MessageBox',
		'Ext.field.MultiSelect',
        'Ext.picker.Picker',
        'Ext.data.Store',
        'Ext.data.StoreManager',
        'Ext.dataview.List',
        'Ext.form.Panel',

        'TolMobile.profile.Phone',
        'TolMobile.profile.Tablet',

        'TolMobile.proxy.SecureProxy',
        'TolMobile.utils.AddressBuilder'
    ],

    viewport: {
        // weird bug found on iPod touch with iOS 3 - not sure if fixed...
        // http://www.sencha.com/forum/showthread.php?207870-Way-to-hide-URL-bar-with-autoMaximize-false
        autoMaximize: !Ext.browser.is.Standalone && Ext.os.is.iOS && Ext.browser.version.isGreaterThan(3)
    },
    
    profiles: activeProfile,
    
    models: ['AuthToken'],
    views: ['Register', 'About', 'RegisterForm', 'Root'],

    controllers: [
    // main views        
        'Login', 'Root', 'Home', 'MyUserDetails', 'Welcome', 'Register',

    // enquiry
        'enquiry.EnquiryIndex', 'enquiry.EnquiryHandler', 'enquiry.ReportFaultIndex',

    // rent
        'rent.RentIndex', 'rent.MyRent',

    // offices
        'offices.Office',

    // news
        'news.News',

    // inbox
        'inbox.MyInbox',
		
	// debug
		'Debug'
		
    ],

    stores: [

    // lookups
        'ParkingTypes', 'ASBCategories', 'ComplaintTypes', 'AbandonedTypes',

        'Genders', 'Titles', 'MaritalStatus', 'Ethnicity', 'Religion', 'SexualOrientation', 'Language', 'Communication', 'PreferredContact', 'Disability',

        'TerminationReasons',

    // user
        'UserDetails', 'ContactDetails', 'RegisterDetails',

    // View stores
        'viewstores.EnquiryViews', 'viewstores.RentViews',

    // rent
        'rent.Rent', 'rent.Balances', 'rent.RentPeriod',
    //'rent.FakeRent',

    // offices
        'offices.Office',

    // news
        'news.News',

    // inbox
        'inbox.Enquiries', 'inbox.EnquiryCount', 'inbox.EnquiryResponse',
		
	// debug
		'Debug'
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function () {

		// if using trackJs log it.
		if ( typeof trackJs != "undefined" ) {
			trackJs.log("Tracking TolMobile app");
		}
		
		var qs = Ext.urlDecode(window.location.search.substring(1));
		TolMobile.app.isDebug = (qs.debug || qs.Debug);
		
		if ( TolMobile.app.isDebug ) {
			console.log("Activating Debug mode");
				
			var preLaunchLogs = window.bufferedConsole;			
			if ( preLaunchLogs ) {
				
				// write all outstanding buffered consoles to debug store
				var store = Ext.getStore("theDebugStore");
				
				for(var i=0;i<preLaunchLogs.length;i++) {
					var model = Ext.create("TolMobile.model.Debug");
					model.set("Type", "log");
					model.set(preLaunchLogs[i]);
					store.add(model);
				}
				
				/*preLaunchLogs = window.bufferedException;
				for(var i=0;i<preLaunchLogs.length;i++) {
					var model = Ext.create("TolMobile.model.Debug");
					model.set("Type", "exception");
					model.set(preLaunchLogs[i]);
					store.add(model);
				}*/
				
				delete window.bufferedConsole;
				//delete window.bufferedException;
				
				// now redirect log again
				window.console.log = function() {
 				
					window.preservedConsoleLog.apply(window.console, arguments);    
				
					var store = Ext.getStore("theDebugStore");
					var model = Ext.create("TolMobile.model.Debug");
					model.set("Type", "log");
					model.set(arguments);
					store.add(model);					
				}
				
				/*window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {	
				
					var store = Ext.getStore("theDebugStore");
					var model = Ext.create("TolMobile.model.Debug");
					model.set("Type", "exception");
					
					var record = { error: errorMsg, url: url, line: lineNumber}; 
					model.set(record);
					store.add(model);									
					return false;
				}*/
			}
		}
		
        // Destroy the #appLoadingIndicator element
        Ext.fly('tolMobileLoadingIndicator').destroy();

        this.addAjaxErrorHandler();

        console.log("Detected phone profile: " + Ext.os.is.Phone);		
	
        var self = this;

        TolMobile.model.AuthToken.load(101, {
            callback: function (rec) {

                if (rec) {
                    // check if cached auth token exists
                    self.authToken = rec.get("AuthToken");
                }
            }
        });
    },

    currentView: null,
    skipLogin: false,
    showWelcome: true,
    authToken: null,
	isDebug: null,

    switchRootView: function (newView, config, whichDirection) {
        Ext.Viewport.animateActiveItem(newView, { type: 'slide', direction: slideDirection })
    },

    switchMainView: function (newView, whichDirection) {

        var createView;
        if (typeof newView === "string") {
            createView = Ext.create(newView, {});
        } else {
            createView = newView;
        }

        whichDirection = whichDirection || "left";
        Ext.Viewport.animateActiveItem(createView, { type: 'slide', direction: whichDirection })
    },

    showLoad: function (showIt) {

        var maskCfg = false;
        if (showIt) {
            maskCfg = {
                xtype: 'loadmask',
                message: 'Please Wait...',
                indicator: true
            }
        }
        Ext.Viewport.setMasked(maskCfg);
    },

    addAjaxErrorHandler: function () {
        Ext.Ajax.on('beforerequest', function (conn, options, eOpts) {

            if (TolMobile.app.authToken) {
                console.log('set header: ' + TolMobile.app.authToken);

                var defaultHeaders = Ext.Ajax.getDefaultHeaders() || {};

                if (!defaultHeaders["AUTH-TOKEN"]) {
                    defaultHeaders["AUTH-TOKEN"] = TolMobile.app.authToken;
                    Ext.Ajax.setDefaultHeaders(defaultHeaders)
                }
            }
        });

        Ext.Ajax.on('requestcomplete', function () {

        });
        Ext.Ajax.on('requestexception', function (conn, response, options, e) {

            var statusCode = response.status;

            // 404 - file or method not found - special case
            if (statusCode == 404) {
                Ext.Msg.alert('Error 404', 'URL ' + response.request.options.url + ' not found');
                return;
            }

            // 401 - unauthorized
            if (statusCode == 401) {
				// reset auth token if auth failed otherwise an auto-login will be attempted on the user 
				// each time (there'll be no chance to enter the correct credentials)
				TolMobile.app.authToken = null;
				
				var authModel = Ext.create("TolMobile.model.AuthToken");
                authModel.setData({ id: 101, AuthToken: null });
				authModel.save();
						
				
                Ext.Msg.alert('Error', 'Authentication failed and you need to be logged in', function () {
                    window.location.reload();
                });
            }
        });
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
