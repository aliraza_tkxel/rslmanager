Ext.define('TolMobile.controller.rent.RentIndex', {
    extend: 'Ext.app.Controller',

    requires: [        
        'TolMobile.view.rent.RentIndex'
    ],
   
    config: {
        refs: {           
            rentIndexViewRef: 'rentindexview'
        },

        control: {
            'rentindexview list': {                
                itemtap: 'onItemTap',
                disclose: 'onDisclose'
            }                  
        }
    },

    onItemTap: function ( me, index, target, record ) {
        this.rentIndexSelect( me, index, target, record );
    },
    
    onDisclose: function ( me, record, target, index ) {
        this.rentIndexSelect( me, index, target, record );
    },

    rentIndexSelect: function ( me, index, target, record ) {
       console.log("onDisclose fired on rent index");

       me.select(index);

       var xtypeview = record.get("Xid");
       var navview = this.getRentIndexViewRef();
       var nextview = Ext.widget(xtypeview);

       var detailsview = nextview.down("tenantdetailview");
       if ( detailsview ) {
           // show tenants details if supported on next view
           var store = Ext.getStore("theUserDetailsStore");

           var tenant = store.getAt(0);
		   if ( tenant ) {
			   var name = tenant.get("FirstName") + " " + tenant.get("LastName");
			   var tenantRef = tenant.get("TenancyId");

			   detailsview.setTenantName(name);
			   detailsview.setTenantRef(tenantRef);
			}
       }

       navview.push(nextview);      
    }
});