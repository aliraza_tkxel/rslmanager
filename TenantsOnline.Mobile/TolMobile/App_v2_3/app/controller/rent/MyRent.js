Ext.define('TolMobile.controller.rent.MyRent', {
    extend: 'Ext.app.Controller',

    requires: [
		'Ext.ux.Deferred',		
        'TolMobile.view.rent.PayMyRent'   		
    ],

    config: {
        refs: {
            myRentViewRef: 'myrentview',
			myRentStatementViewRef: {
				xtype: 'myrentstatementview',
                selector: 'myrentstatementview',
                autoCreate: true
			},
			rentIndexViewRef: 'rentindexview',
            ddlRentPeriodRef: 'myrentview #ddlRentPeriod',
			viewStatementBtnRef: '[action=ViewStatement]'
        },

        control: {
            ddlRentPeriodRef: {
                change: 'onRentPeriodChange'
            },

            'myrentview': {
                initialize: 'onInitialize'
            }, 
			
			viewStatementBtnRef: {
				tap: 'onViewStatementBtnTap'
			}
        }
    },

    onInitialize: function () {
		
        console.log("myrentview onInitialize");

		var ddl = this.getDdlRentPeriodRef();
		if ( ddl ) {
			var rentPeriod = ddl.getValue();
			this.loadRentStatement(rentPeriod);
		}
    },

    onRentPeriodChange: function (scope, newValue, oldValue, eOpts) {
        console.log("onRentPeriodChange");

        this.loadRentStatement(newValue);
    },
	
	onViewStatementBtnTap: function() {
		console.log("onViewStatementBtnTap");
		
		var rentIndexView = this.getRentIndexViewRef();
		var statementView = this.getMyRentStatementViewRef();		
		rentIndexView.push(statementView);
	},  

	loadRentStatement: function(rentPeriod) {
		
		var view = this.getMyRentViewRef();

		var balanceStore = Ext.getStore("theBalanceStore");
		var accountStore = Ext.getStore("theRentAccountStore");
		
		var proxy = accountStore.getProxy();        
		
		if (proxy) {
			var params = {
				rentPeriod: rentPeriod
			}
			proxy.setExtraParams(params);
		}

		function aSyncGetBalances () {
			var dfd = Ext.create ('Ext.ux.Deferred');
			
			balanceStore.load(function (records) {
				// load balance in      
				if (records.length > 0) {
					var model = records[0];

					var lblBalance = view.down("#lblBalance");
					var lblHousingBenefit = view.down("#lblHousingBenefit");

					var bal = model.get("AccountBalance");
					var benefit = model.get("HousingBenefit");

					var arrears = model.get("InArrears");
					arrears ? lblBalance.setStyle({ "color": "red" }) : lblBalance.setStyle({ "color": "black" });
					lblBalance.setHtml(bal);
										
					lblHousingBenefit.setHtml(benefit);                    
				}
				
				dfd.resolve();
			});
										
			return dfd;
		}     

		function aSyncGetAccountStore() {
			var dfd = Ext.create ('Ext.ux.Deferred');
						
			// now load statement
			accountStore.load(function() {
				dfd.resolve();
			});
			
			return dfd;
		}

		// want the spinner to show and only hide once both balances and statement come back
		TolMobile.app.showLoad(true);	
		
		Ext.ux.Deferred
			.when (aSyncGetBalances, aSyncGetAccountStore)
			.then (function () {
				// wait for both async process to finish before removing spinner
				TolMobile.app.showLoad(false);				
			});
	}
});