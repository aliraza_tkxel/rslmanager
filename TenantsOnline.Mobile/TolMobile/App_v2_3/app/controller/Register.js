Ext.define('TolMobile.controller.Register', {
    extend: 'Ext.app.Controller',

    requires: [

    ],

    config: {
        refs: {

            loginViewRef: {
                selector: 'loginview',
                xtype: 'loginview',
                autoCreate: true
            },
            welcomeViewRef: 'welcomeview',
            btnRegisterRef: 'registerformview #btnRegister',
            btnRegisterBackRef: "registerformview [action=back]",

            registerFormViewRef: 'registerformview',

            // success view
            registerSuccessViewRef: {
                selector: 'registersuccessview',
                xtype: 'registersuccessview',
                autoCreate: true
            },
            btnBackToLoginRef: 'registersuccessview #btnBackToLogin'
        },

        control: {

            "btnRegisterRef": {
                tap: "onBtnRegisterTap"
            },
            btnRegisterBackRef: {
                tap: function () {
                    console.log("btnRegisterBack tap");

                    // 2 cards have loaded at this point so just remove 1 to get back                    
                    var welcome = this.getWelcomeViewRef();
                    TolMobile.app.switchMainView(welcome, "right");
                }
            },
            btnBackToLoginRef: {
                tap: 'onBtnBackToLoginTap'
            }
        }
    },

    onBtnRegisterTap: function (me) {
        console.log("onBtnRegisterTap");

        // do registration
        this.saveRegistrationDetails();
    },

    validateConfirmFields: function () {
		console.log("validateConfirmFields");
        // returns true if ok to proceed with registration
        var valid = true;
        var view = this.getRegisterFormViewRef();

        if (view.down("#email").getValue() !== view.down("#email2").getValue()) {
            Ext.Msg.alert('Register', 'Please enter the same email address twice', Ext.emptyFn);
            return false;
        }

        if (view.down("#password").getValue() !== view.down("#password2").getValue()) {
            Ext.Msg.alert('Register', 'Please enter the same password twice', Ext.emptyFn);
            return false;
        }

        return true;
    },

    saveRegistrationDetails: function () {
		console.log("saveRegistrationDetails");
        var form = this.getRegisterFormViewRef();

        // persist details on form        
        var record = form.getRecord();
        var newValues = form.getValues();

		console.log("setting record fields...");
        record.set("FirstName", newValues.FirstName);
        record.set("LastName", newValues.LastName);
        record.set("TenancyId", newValues.TenancyId);
        record.set("Email", newValues.Email);
        record.set("DateOfBirth", newValues.DateOfBirth);
        record.set("Postcode", newValues.Postcode);
        record.set("Password", newValues.Password);

		console.log("validating record...");
        var errors = record.validate();
        if (errors.isValid()) {
            // first errors pass ok, now check for identical confirm fields...
            if (this.validateConfirmFields()) {

                // send registration details to server
				console.log("getting values...");
                var paramsToSend = form.getValues();
                var self = this;

                Ext.Ajax.request({
                    scope: this,
                    url: '../api/register',
                    params: paramsToSend,
                    method: "POST",
                    success: function (resp) {
                        var response = Ext.decode(resp.responseText);
                        if (response.CustomerId > -1) {
                            // if customer id returned then treat this as success

                            // newly registered so clear-out any cached logged in other user
                            TolMobile.app.authToken = null;                            

                            // show success view
                            var successView = self.getRegisterSuccessViewRef();
                            TolMobile.app.switchMainView(successView);

                        } else {
                            self.registrationFailure(response);
                        }
                    },
                    failure: function () {
                        self.registrationFailure(resp);
                    }

                });

                // show next screen
            }
        } else {
            Ext.Msg.alert('Registration', errors.items[0].getMessage(), Ext.emptyFn);
        }
    },

    registrationFailure: function (response) {
        var reason = response ? response.Error + "<br/>" : "";

        var msg = [
            'Sorry, your registration was not successful.  ',
            reason
        ].join('');

        Ext.Msg.alert('Registration', msg, Ext.emptyFn);
    },

    onBtnBackToLoginTap: function () {
        var welcome = this.getWelcomeViewRef();
        TolMobile.app.switchMainView(welcome, "right");
    }
});