Ext.define('TolMobile.controller.MyUserDetails', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.field.MultiSelect'
    ],

    config: {
        refs: {
            myDetailsViewRef: "mydetailsview",
            myUserDetailsViewRef: "myuserdetailsview",
            myDetailsSaveButtonRef: "#myDetailsSaveButton",
            myDetailsEditButtonRef: "#myDetailsEditButton",

            otherDetailsFieldSetRef: "myuserdetailsview #fsetOtherDetails",
            contactDetailsFieldSetRef: "myuserdetailsview #fsetContact"
        },

        control: {
            myDetailsViewRef: {
                activeitemchange: function (e, oldcard, newcard) {
                    var btnSave = this.getMyDetailsSaveButtonRef();
                    var btnEdit = this.getMyDetailsEditButtonRef();
                    var form = this.getMyUserDetailsViewRef();
                   
                    this.CancelEdit(btnEdit, btnSave, form);
                }
            },
            myUserDetailsViewRef: {
                onCancelledEditCommand: "onCancelledEditCommandHandler"
            },

            myDetailsSaveButtonRef: {
                tap: "onMyDetailsSaveTapHandler"
            },

            myDetailsEditButtonRef: {
                tap: "onMyDetailsEditTapHandler"
            }
        }
    },

    EnableFieldSets: function(enable) {
        var otherDetailsFieldSetRef = this.getOtherDetailsFieldSetRef();
        var contactDetailsFieldSetRef = this.getContactDetailsFieldSetRef();
        
        var fieldsets = [
            otherDetailsFieldSetRef,
            contactDetailsFieldSetRef
        ]

        Ext.Array.forEach(fieldsets, function(fset) {
           enable ? fset.enable() : fset.disable();
        });
    },

    CancelEdit: function (btnEdit, btnSave, form, fieldsets) {
        btnEdit.editMode = false;
        btnEdit.setText('Edit');
        btnSave.setHidden(true);

        var record = form.getRecord();
        form.setRecord(record);
        this.EnableFieldSets(false);              
    },

    onMyDetailsEditTapHandler: function () {
        // button handling
        var btnSave = this.getMyDetailsSaveButtonRef();
        var btnEdit = this.getMyDetailsEditButtonRef();
        var form = this.getMyUserDetailsViewRef();
        
        if (!btnEdit.editMode) {
            // edit clicked
            btnEdit.editMode = true;
            btnEdit.setText('Cancel');
            btnSave.setHidden(false);

            this.EnableFieldSets(true);
        } else {
            // cancel clicked              
            this.CancelEdit(btnEdit, btnSave, form);
        }
    },

    onMyDetailsSaveTapHandler: function () {

        // button handling
        var btnSave = this.getMyDetailsSaveButtonRef();
        var btnEdit = this.getMyDetailsEditButtonRef();
        var form = this.getMyUserDetailsViewRef();

        // persist details on form        
        var record = form.getRecord();
        var newValues = form.getValues();

        record.set("Title", newValues.Title);
        record.set("FirstName", newValues.FirstName);
        record.set("LastName", newValues.LastName);
        record.set("MiddleName", newValues.MiddleName);
        record.set("Gender", newValues.Gender)
        record.set("NINumber", newValues.NINumber);
        record.set("DateOfBirth", newValues.DateOfBirth);

        record.set("Telephone", newValues.Telephone);
        record.set("Mobile", newValues.Mobile);
        record.set("Email", newValues.Email);

        record.set("Occupation", newValues.Occupation)
        record.set("MaritalStatus", newValues.MaritalStatus)
        record.set("Ethnicity", newValues.Ethnicity)
        record.set("Religion", newValues.Religion)
        record.set("SexualOrientation", newValues.SexualOrientation)

        record.set("Language", newValues.Language)
        record.set("Communication", newValues.Communication)
        record.set("PreferredContact", newValues.PreferredContact)
        record.set("Disability", newValues.Disability)

        var errors = record.validate();

        if (errors.isValid()) {

            // todo...fix date of birth

            btnSave.setHidden(true);
            btnEdit.setText('Edit');
            form.disable();

            var store = Ext.getStore("theUserDetailsStore");
            store.sync();

            Ext.getCmp('myDetailsSaveButton').setHidden(true);
            var editBtn = Ext.getCmp('myDetailsEditButton');
            editBtn.setText('Edit');
            editBtn.editMode = false;

            this.EnableFieldSets(false);            

            Ext.Msg.alert("Your Details", "Your details have been updated");
        } else {
            record.reject();
            Ext.Msg.alert('Sorry!', errors.items[0].getMessage(), Ext.emptyFn);
        }
        console.log("onMyDetailsSaveTapHandler");
    }

});