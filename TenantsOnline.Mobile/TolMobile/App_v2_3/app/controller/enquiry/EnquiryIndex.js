Ext.define('TolMobile.controller.enquiry.EnquiryIndex', {
    extend: 'Ext.app.Controller',

    requires: [
        'TolMobile.view.reporting.EnquiryIndex',        

        'TolMobile.view.reporting.EndTenancy',
        'TolMobile.view.reporting.RequestParkingType',
        'TolMobile.view.reporting.ReportComplaint',
        'TolMobile.view.reporting.ReportASB',
        'TolMobile.view.reporting.ReportAbandonedType'

    ],

    config: {
        refs: {
            enquiryIndexViewRef: 'enquiryindexview'
        },

        control: {
            '#enquirylist': {
                itemtap: 'onEnquiryIndexTap',
                disclose: 'onEnquiryIndexDisclose'
            }
        }    
    },

    launch: function () {
        console.log("EnquiryIndex controller launched");
    },

    onEnquiryIndexTap: function (me, index, target, record, e, eOpts) {
        this.enquiryIndexSelect(me, index, target, record);
    },

    onEnquiryIndexDisclose: function (me, record, target, index, e, eOpts) {
        this.enquiryIndexSelect(me, index, target, record);
    },

    enquiryIndexSelect: function (me, index, target, record) {

        console.log("enquiryIndexSelect fired on rent index");
        me.select(index);

        var xtypeview = record.get("Xid");

        // for now disable all other views   

		// Have a feeling that this line could be creating multiple views
		// and so causes the controller not to find the correct controls
		// thus assuming the values are empty?? Auto-destroy?
        nextview = Ext.ComponentQuery.query(xtypeview)[0]

		if ( !Ext.isDefined(nextview) ) {
			// create view if it doesn't already exist
			console.log("creating new view:" + xtypeview);
			nextview = Ext.widget(xtypeview);
		} else {
			console.log("reusing existing view:" + xtypeview);
		}
		
        var detailsview = nextview.down("tenantdetailview");
        var store = Ext.getStore("theUserDetailsStore");

        var tenant = store.getAt(0);
		if ( tenant ) {
			var name = tenant.get("FirstName") + " " + tenant.get("LastName");
			var tenantRef = tenant.get("TenancyId");

			if (detailsview) {
				detailsview.setTenantName(name);
				detailsview.setTenantRef(tenantRef);
			}
		}
		
		
        var title = record.get("Title")
        if (title && nextview.setTitle) {         
            nextview.setTitle(record.get("Title"));
        }

        if (record.get("SharedView")) {
            var usesSelectType = record.get("SelectType")
            if (usesSelectType) {
                var ddl = nextview.down("#ddlSelectType");
                ddl.setValue(usesSelectType);
            }
        }

        // if next view supports address...
        if (nextview.getSupportsAddress && nextview.getSupportsAddress()) {
            var contact = Ext.getStore("theContactDetailsStore").getAt(0);
			
			if ( contact ) {
				var addr = [];
				var houseno = contact.get("HouseNo") + " " + contact.get("Address1");

				addr.push(houseno.trim());
				addr.push(contact.get("Address2"));
				addr.push(contact.get("Address3"));
				addr.push(contact.get("TownCity"));
				addr.push(contact.get("County"));
				addr.push(contact.get("PostCode"));

				// build address         
				var addressLine = TolMobile.utils.AddressBuilder.buildSingleLineAddress(addr);
				detailsview.setTenantAddress(addressLine);
			}
        }		

        var navview = this.getEnquiryIndexViewRef();
        navview.push(nextview);

    }

});