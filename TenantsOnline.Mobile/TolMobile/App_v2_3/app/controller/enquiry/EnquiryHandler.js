Ext.define('TolMobile.controller.enquiry.EnquiryHandler', {
    extend: 'Ext.app.Controller',

    requires: [

    ],

    config: {
        refs: {
            enquiryIndexViewRef: 'enquiryindexview',

            // end tenancy view            
            endTenancyReasonRef: 'endtenancyview #ddlTerminationReason',
            endTenancyDateRef: 'endtenancyview #endTenancyDate',

            requestParkingTypeViewRef: 'requestparkingtypeview',
            ddlSelectTypeRef: 'requestparkingtypeview #ddlSelectType',

            reportAsbViewRef: 'reportasbview',
            reportComplaintViewRef: 'reportcomplaintview',

            ddlSelectAsbRef: 'reportasbview #ddlAsbType',
            ddlSelectComplaintTypeRef: 'reportcomplaintview #ddlComplaintType',

            abandonedTypeViewRef: 'reportabandonedtypeview',

            chkAddNeglectPhotoRef: 'reportabandonedtypeview #chkAddNeglectPhoto',
            btnSendAbandonedRef: 'reportabandonedtypeview button[action=send]',
            addNeglectPhotoViewRef: {
                xtype: 'addneglectphotoview',
                selector: 'addNeglectPhotoViewRef',
                autoCreate: true
            },

            fileUploaderRef: 'addneglectphotoview #fileBtn'
        },

        control: {
            'endtenancyview button[action=send]': {
                tap: 'onSendEndTenancy'
            },
            'requestparkingtypeview button[action=send]': {
                tap: function () {
                    this.onSendParkingTypeRequest(this.getRequestParkingTypeViewRef());
                }
            },
            'reportcomplaintview button[action=send]': {
                tap: function () {
                    this.onSendReportAsbOrComplaint(this.getReportComplaintViewRef(), 'ReportComplaint');
                }
            },
            'reportasbview button[action=send]': {
                tap: function () {
                    this.onSendReportAsbOrComplaint(this.getReportAsbViewRef(), 'ReportASB');
                }
            },
            btnSendAbandonedRef: {
                tap: 'onSendAbandonedTypeReport'
            },
            chkAddNeglectPhotoRef: {
                change: 'onChkAddNeglectPhotoChange'
            },
            fileUploaderRef: {
                success: 'onFileUploadSuccess',
                failure: 'onFileUploadFailure'
            }
        }
    },

    onSendEndTenancy: function () {
        var ddlReason = this.getEndTenancyReasonRef();
        var dtpEndDate = this.getEndTenancyDateRef();
        var reason = ddlReason.getValue();
        var endDate = dtpEndDate.getValue();


        if (!reason) {
            Ext.Msg.alert('End Tenancy', 'Please select a reason', Ext.emptyFn);
            return;
        }

        if (!(endDate > new Date())) {
            Ext.Msg.alert('End Tenancy Date', 'Please enter a date in the future', Ext.emptyFn);
            return;
        }

        Ext.Ajax.request({
            scope: this,
            url: '../api/enquiry/requesttermination',
            params: { Reason: reason, TerminationDate: endDate },
            method: "POST",
            success: function () {
                ddlReason.reset();
                dtpEndDate.reset();

                var indexView = this.getEnquiryIndexViewRef();
                Ext.Msg.alert('End Tenancy', 'Thank you, your request has been sent', function () {
                    indexView.pop();
                });
            },
            failure: function (resp) {
                Ext.Msg.alert('End Tenancy', 'Sorry, your request was not successful', Ext.emptyFn);
            }
        });
    },

    onSendParkingTypeRequest: function (view) {
        console.log("onSendParkingTypeRequest");
        
        var txtAdditionalInfo = view.down("#txtAdditionalInfo");
        var ddlParkingType = this.getDdlSelectTypeRef();

		var msgTitle = 'Request Parking Type';
		if (!ddlParkingType.getValue() || !txtAdditionalInfo.getValue()) {
			var msg =  'Please select a parking type and enter additional information';
            Ext.Msg.alert(msgTitle, msg, Ext.emptyFn);
            return;
        }
		
        Ext.Ajax.request({
            scope: this,
            url: '../api/enquiry/requestparkingtype',
            params: { ParkingType: ddlParkingType.getValue(), AdditionalInformation: txtAdditionalInfo.getValue() },
            method: "POST",
            success: function () {
                txtAdditionalInfo.reset();
                ddlParkingType.reset();

                var indexView = this.getEnquiryIndexViewRef();
                Ext.Msg.alert(msgTitle, 'Thank you, your request has been sent', function () {
                    indexView.pop();
                });
            },
            failure: function (resp) {
                Ext.Msg.alert(msgTitle, 'Sorry, your request was not successful', Ext.emptyFn);
            }
        });
    },

    onSendReportAsbOrComplaint: function (view, type) {
        console.log("onSendReportAsbOrComplaint");

        var title, msg, ddlSelectType, successMsg, failMsg, txtAdditionalInfo;
        txtAdditionalInfo = view.down("#txtAdditionalInfo");

		console.log("About to check Report ASB type");
        if (type === "ReportASB") {
			console.log("Report ASB type");
            title = 'Report ASB';
            msg = 'Please select a category and enter the problem';
            ddlSelectType = this.getDdlSelectAsbRef();

            successMsg = 'Thank you, your report has been sent';
            failMsg = 'Sorry, your report could not be sent';
        } else {
			console.log("Report Complaint type");
            title = "Report Complaint";
            msg = 'Please select a complaint and description';
            ddlSelectType = this.getDdlSelectComplaintTypeRef();

            successMsg = 'Thank you, your complaint has been sent'
            failMsg = 'Sorry, your complaint could not be sent';
        }
		
		console.log("ddlSelectType value:" + ddlSelectType.getValue());
		console.log("txtAdditionalInfo value:" + txtAdditionalInfo.getValue());
        if (!ddlSelectType.getValue() || !txtAdditionalInfo.getValue()) {
            Ext.Msg.alert(title, msg, Ext.emptyFn);
            return;
        }

        Ext.Ajax.request({
            scope: this,
            url: '../api/enquiry/' + type,
            params: { Type: ddlSelectType.getValue(), ProblemDescription: txtAdditionalInfo.getValue() },
            method: "POST",
            success: function () {
                txtAdditionalInfo.reset();
                ddlSelectType.reset();

                var indexView = this.getEnquiryIndexViewRef();
                Ext.Msg.alert(type, successMsg, function () {
                    indexView.pop();
                });
            },
            failure: function (resp) {
                Ext.Msg.alert(title, failMsg, Ext.emptyFn);
            }
        });
    },

    onSendAbandonedTypeReport: function () {
        console.log("onSendAbandonedTypeReport");

        var view = this.getAbandonedTypeViewRef();
        var txtLocation = view.down("#txtLocation");
        var txtAdditionalInfo = view.down("#txtAdditionalInfo");

        var ddlSelectType = view.down("#ddlSelectType");

        if (!ddlSelectType.getRecord()) {
            Ext.Msg.alert(title, "Please select what you wish to report", Ext.emptyFn);
            return;
        }
        if (!txtLocation.getValue()) {
            Ext.Msg.alert(title, "Please describe the location", Ext.emptyFn);
            return;
        }

        var reportTypeDesc = ddlSelectType.getRecord().data.ItemName;
        var reportType = ddlSelectType.getValue();

        var title = 'Report Abandoned ' + reportTypeDesc;

        var sendParams = {            
            ReportType: reportType,
            AdditionalInformation: txtAdditionalInfo.getValue(),
            LocationDescription: txtLocation.getValue()
        }

        var chk = this.getChkAddNeglectPhotoRef();
        var indexView = this.getEnquiryIndexViewRef();

        if (chk.getChecked()) {
            // if add photo is ticked then upload this first and then submit rest of data
            var addPhotoView = this.getAddNeglectPhotoViewRef();
            var fileUploader = addPhotoView.down("fileupload");
         
            fileUploader.setUrl("../api/enquiry/ReportAbandonedTypeWithPhoto?" + Ext.Object.toQueryString(sendParams));


            // add form params
            var authHeader = [{
                header: "AUTH-TOKEN",
                value: TolMobile.app.authToken
            }]

            if (TolMobile.app.authToken) {
                fileUploader.setHeaderParams(authHeader);
                indexView.push(addPhotoView);
            }

        } else {
            Ext.Ajax.request({
                scope: this,
                url: '../api/enquiry/ReportAbandonedType',
                params: sendParams,
                method: "POST",
                success: function () {
                    txtAdditionalInfo.reset();
                    txtLocation.reset();

                    Ext.Msg.alert(title, 'Thank you, your report has been sent', function () {
                        indexView.pop();
                    });
                },
                failure: function (resp) {
                    Ext.Msg.alert(title, 'Sorry, your report was not successful', Ext.emptyFn);
                }
            });
        }
    },

    onChkAddNeglectPhotoChange: function (scope, checked) {
        console.log("onChkAddNeglectPhotoChange");
        var btn = this.getBtnSendAbandonedRef();
        var btnText = !checked ? 'Send' : 'Next';
        btn.setText(btnText);
    },

    onFileUploadSuccess: function () {
        console.log('onFileUploadSuccess');

        var indexView = this.getEnquiryIndexViewRef();

        Ext.Msg.alert('Report Neglect', 'Thank you, your report and photo has been sent.', function () {
            indexView.reset();            
        });        
    },

    onFileUploadFailure: function () {
        console.log('onFileUploadFailure');

        var self = this;
        Ext.Msg.alert('Report Neglect', 'Sorry your report with photo could not be sent due to an error', function () {
            var indexView = self.getEnquiryIndexViewRef();
            indexView.pop();
        });
    }
});