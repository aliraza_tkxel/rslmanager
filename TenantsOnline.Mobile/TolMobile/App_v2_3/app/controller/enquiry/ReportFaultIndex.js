Ext.define('TolMobile.controller.enquiry.ReportFaultIndex', {
    extend: 'Ext.app.Controller',

    requires: [
        'TolMobile.view.reporting.ReportFaultIndex',
        'TolMobile.view.reporting.InternalFaultImages',
        'TolMobile.view.reporting.ExternalFaultImages'
    ],

    config: {
        refs: {
            faultIndexViewRef: 'reportfaultindexview',
            enquiryIndexViewRef: 'enquiryindexview'
        },

        control: {
            'reportfaultindexview #faultindexlist': {
                itemtap: 'onFaultIndexListTap',
                disclose: "faultindexlist disclose"
            }
        }
    },

    launch: function () {
        console.log("ReportFaultIndex controller launched");
    },

    onFaultIndexListTap: function (me, index, target, record, e, eOpts) {
        this.faultIndexSelect(me, index, target, record);
    },

    onFaultIndexListDisclose: function (me, record, target, index, e, eOpts) {
        this.faultIndexSelect(me, index, target, record);
    },

    faultIndexSelect: function (me, index, target, record) {

        console.log("faultIndexSelect fired");
        me.select(index);

        var xtypeview = record.get("Xid");

        var nextview = Ext.widget(xtypeview);

        //var navview = this.getFaultIndexViewRef();
        var navview = this.getEnquiryIndexViewRef();
        navview.push(nextview);
    }

});