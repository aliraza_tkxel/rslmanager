Ext.define('TolMobile.controller.Home', {
    extend: 'Ext.app.Controller',

    requires: [
        'TolMobile.view.reporting.EnquiryIndex',
        'TolMobile.view.rent.RentIndex',
        'TolMobile.view.MyUserDetails'

    // why can't this be added to the enquiry index view?        
    ],
	
    config: {
        refs: {
            rootViewRef: {
                selector: "rootview",
                xtype: "rootview",
                autoCreate: true
            },

            myUserDetailsViewRef: {
                selector: 'myuserdetailsview',
                xtype: 'myuserdetailsview',
                autoCreate: true
            },

            enquiryIndexViewRef: {
                selector: 'enquiryindexview',
                xtype: 'enquiryindexview',
                autoCreate: true
            },

            rentIndexViewRef: {
                selector: 'rentindexview',
                xtype: 'rentindexview',
                autoCreate: true
            },

            officesViewRef: {
                autoCreate: true,
                selector: 'officeindexview',
                xtype: 'officeindexview'
            },

            newsIndexViewRef: {
                autoCreate: true,
                selector: 'newsindexview',
                xtype: 'newsindexview'
            },

            myInboxViewRef: {
                autoCreate: true,
                selector: 'myinboxview',
                xtype: 'myinboxview'
            },			

			debugViewRef: {
                autoCreate: true,
                selector: 'debugview',
                xtype: 'debugview'
            },
			
			homeCarouselViewRef: {
                autoCreate: true,
                selector: 'homecarouselview',
                xtype: 'homecarouselview'
            },

			benefitCutsViewRef: {
                autoCreate: true,
                selector: 'benefitcutsview',
                xtype: 'benefitcutsview'
            },
			
            // home actions
            showMyDetailsButtonRef: '[action=ShowMyDetails]',
            reportItButtonRef: '[action=ReportIt]',
            myRentButtonRef: '[action=MyRent]',

            officesButtonRef: '[action=Offices]',
            newsButtonRef: '[action=News]',
			
			benefitCutsButtonRef: '[action=BenefitCuts]',

            myInboxButtonRef: '[action=MyInbox]',

			debugButtonRef: '[action=Debug]',
			
            logoutButton: '[action=Logout]',
			

            homeViewRef: 'homeview'			
        },

        control: {
            'showMyDetailsButtonRef': {
                tap: 'onShowMyDetailsButtonTap'
            },
            'reportItButtonRef': {
                tap: 'onReportItButtonTap'
            },
            'myRentButtonRef': {
                tap: 'onMyRentButtonTap'
            },
            'logoutButton': {
                tap: 'onLogoutButtonTap'
            },
            'officesButtonRef': {
                tap: 'onOfficesButtonTap'
            },
            'newsButtonRef': {
                tap: 'onNewsButtonTap'
            },
            'myInboxButtonRef': {
                tap: 'onMyInboxButtonRef'
            },
			'debugButtonRef' : {
				tap: 'onDebugButtonRef'
			},
			'benefitCutsButtonRef': {
                tap: 'onBenefitCutsButtonTap'
            },
			
			'homeViewRef' : {
				onPhoneOrientationChange: 'handlePhoneOrientationChange'
			},
									
			'homeCarouselViewRef' : {
				onPhoneOrientationChange: 'handlePhoneOrientationChange'
			}		
        },
		
		carouselMode: false
    },	
	
	init: function(app) {
		console.log("Home controller - init");
		app.application.on("returnHome", this.onReturnHome, this); 		
	},
	
    onShowMyDetailsButtonTap: function () {
        var rootView = this.getRootViewRef();
        var detailsView = this.getMyUserDetailsViewRef();

        rootView.animateActiveItem(detailsView, { type: 'slide', direction: 'left' });
    },

    onReportItButtonTap: function () {
        var rootView = this.getRootViewRef();
        var indexView = this.getEnquiryIndexViewRef();

        rootView.animateActiveItem(indexView, { type: 'slide', direction: 'left' });
    },

    onMyRentButtonTap: function () {
        var rootView = this.getRootViewRef();
        var indexView = this.getRentIndexViewRef();

        rootView.animateActiveItem(indexView, { type: 'slide', direction: 'left' });
    },

    onLogoutButtonTap: function () {
        var confirmBox = Ext.create("Ext.MessageBox");
        confirmBox.confirm("Logout", "Do you want to logout?", function (btn) {
            if (btn === 'yes') {

                var authModel = Ext.create("TolMobile.model.AuthToken");
                authModel.setData({ id: 101, AuthToken: null });
                authModel.save();

                window.location.reload();
            }
        });
    },

    onOfficesButtonTap: function () {
        var rootView = this.getRootViewRef();
        var indexView = this.getOfficesViewRef();

        rootView.animateActiveItem(indexView, { type: 'slide', direction: 'left' });
    },

    onNewsButtonTap: function () {
        var rootView = this.getRootViewRef();
        var indexView = this.getNewsIndexViewRef();

        rootView.animateActiveItem(indexView, { type: 'slide', direction: 'left' });
    },

    onMyInboxButtonRef: function () {
        var rootView = this.getRootViewRef();
        var myInboxView = this.getMyInboxViewRef();

        rootView.animateActiveItem(myInboxView, { type: 'slide', direction: 'left' });
    },
	
	onBenefitCutsButtonTap: function() {
	    var rootView = this.getRootViewRef();
        var benefitCutsViewRef = this.getBenefitCutsViewRef();

        rootView.animateActiveItem(benefitCutsViewRef, { type: "slide", direction: "left" });
	},
	
	handlePhoneOrientationChange: function(orientation) {
		console.log("handlePhoneOrientationChange");
				
		
		var rootView = this.getRootViewRef();	
		var switchNow = false;
		
		// only switch view if currently on the home screen (check the id)
		switchNow = rootView.getActiveItem().getId().indexOf("home") > -1;
			
		if ( orientation == "landscape") {
			// if landscape mode change to carousel view			
			this.setCarouselMode(true);						
			if ( switchNow ) {
				var landscapeView = this.getHomeCarouselViewRef();
				rootView.setActiveItem(landscapeView);
			}
		} else {			
			this.setCarouselMode(false);
			if ( switchNow ) {			
				var homeView = this.getHomeViewRef();
				rootView.setActiveItem(homeView);			
			}
		}	
	},
	
	onReturnHome: function () {
		console.log("onReturnHome");
				
		var rootView = this.getRootViewRef();
		
		// if in phone mode and received orientation event (via carouselMode)
		// check if carousel is shown and send the correct home view
        var homeView = this.getCarouselMode() ? this.getHomeCarouselViewRef() : this.getHomeViewRef();

        rootView.animateActiveItem(homeView, { type: "slide", direction: "right" });
	},
	
	onDebugButtonRef: function() {
		if ( TolMobile.app.isDebug ) {
			console.log("onDebugButtonRef");		
			var rootView = this.getRootViewRef();
			var debugView = this.getDebugViewRef();
			
			rootView.animateActiveItem(debugView, { type: "slide", direction: "right" });			
		}
	}
	
});