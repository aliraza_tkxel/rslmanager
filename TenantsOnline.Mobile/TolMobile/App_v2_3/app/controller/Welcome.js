Ext.define('TolMobile.controller.Welcome', {
    extend: 'Ext.app.Controller',

    requires: [

    ],

    config: {
        refs: {
            welcomeViewRef: 'welcomeview',
            loginView: {
                selector: 'loginview',
                xtype: 'loginview',
                autoCreate: true
            },

            registerView: {
                selector: 'registerformview',
                xtype: 'registerformview',
                autoCreate: true
            },

            // note: because of manually mapping views/classes in profiles, the selector
            // mechanism for xtypes now seems broken on the profile xtypes
            signInButtonRef: 'welcomeview button[action=signin]',
            registerButtonRef: 'welcomeview button[action=register]'
        },

        control: {
            "signInButtonRef": {
                tap: function () {
                    console.log("signInNowTap received");
                    var loginView = this.getLoginView();                    
                    TolMobile.app.switchMainView(loginView);                    
                }
            },

            "registerButtonRef":  {
                tap: function() {
                    var registerView = this.getRegisterView();
                    TolMobile.app.switchMainView(registerView);
                }                
            }
        }
    },

    launch: function () { console.log('welcome controller:launch'); },
    init: function () { console.log('welcome controller:init'); }
});