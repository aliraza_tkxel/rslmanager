Ext.define('TolMobile.controller.inbox.MyInbox', {
    extend: 'Ext.app.Controller',

    requires: [
        'TolMobile.view.inbox.MyInbox',
        'TolMobile.view.inbox.EnquiryDetails',
        'TolMobile.view.inbox.EnquiryReply',
        'TolMobile.view.inbox.EnquiryResponseDetails'
    ],

    config: {
        refs: {
            myInboxViewRef: 'myinboxview',
            enquiryDetailsViewRef: {
                xtype: 'enquirydetailsview',
                selector: 'enquirydetailsview',
                autoCreate: true
            },
            enquiryReplyViewRef: {
                xtype: 'enquiryreplyview',
                selector: 'enquiryreplyview',
                autoCreate: true
            },
            enquiryResponseDetailsViewRef: {
                xtype: 'enquiryresponsedetailsview',
                selector: 'enquiryresponsedetailsview',
                autoCreate: true
            },
            btnReplyToEnqRef: 'myinboxview #btnReplyToEnquiry',
            btnSendEnquiryReplyRef: 'myinboxview #btnSendEnquiryReply',
            btnHomeRef: 'myinboxview #btnHome',
            inBoxListViewRef: 'inboxlistview',

            enquiryResponseListItemRef: 'enquiryresponselistitem',
            enquiryResponseListViewRef: 'enquiryresponselistview',

            btnBackToEnquiryHistoryRef: 'enquirydetailsview #btnBackToEnquiryHistory'
        },

        control: {
            myInboxViewRef: {
                initialize: 'onMyInboxViewRefInitialized',                
                pop: 'onMyInboxViewPopped',
                push: 'onMyInboxViewPushed'
            },            
            btnReplyToEnqRef: {
                tap: 'onBtnReplyToEnqTapped'
            },
            btnSendEnquiryReplyRef: {
                tap: 'onBtnSendEnquiryReplyTapped'
            },
            inBoxListViewRef: {
                itemtap: 'onInBoxListViewItemTap'
            },
            enquiryResponseListViewRef: {
                itemtap: 'onEnquiryResponseListViewTap'
            },
            btnBackToEnquiryHistoryRef: {
                tap: 'onBtnBackToEnquiryHistoryTap'
            }
        }
    },

    onMyInboxViewRefInitialized: function () {
        console.log("onMyInboxViewRefInitialized");
        this.refreshInboxView();
    },

    onMyInboxViewRefPainted: function () {
        console.log("onMyInboxViewRefPainted");
        this.refreshInboxView();
    },

    onMyInboxViewPopped: function (view, nextview) {
        console.log("onMyInboxViewPopped");

        switch (nextview.$className) {
            case 'TolMobile.view.inbox.EnquiryDetails':
                var btn = this.getBtnReplyToEnqRef();
                btn.setHidden(true);                
                break;

            case 'TolMobile.view.inbox.EnquiryReply':
                var btn = this.getBtnSendEnquiryReplyRef();
                var btnReply = this.getBtnReplyToEnqRef();
                btn.setHidden(true);
                btnReply.setHidden(false);
                break;
        }
    },

    onMyInboxViewPushed: function (view, nextview) {
        console.log("onMyInboxViewPushed");

        switch (nextview.$className) {
            case 'TolMobile.view.inbox.EnquiryDetails':
                var btn = this.getBtnReplyToEnqRef();
                btn.setHidden(false);
                break;

            case 'TolMobile.view.inbox.EnquiryReply':
                var btn = this.getBtnSendEnquiryReplyRef();
                var btnReply = this.getBtnReplyToEnqRef();
                btn.setHidden(false);
                btnReply.setHidden(true);
                break;
        }
    },

    onEnquiryDetailsPainted: function () {
        console.log("onEnquiryDetailsPainted");
        this.getBtnReplyToEnqRef().setHidden(false);
    },

    onBtnReplyToEnqTapped: function () {
        console.log("onBtnReplyToEnqTapped");

        var navview = this.getMyInboxViewRef();
        var replyView = this.getEnquiryReplyViewRef();

        // pass record from previous view to next for later
        // ajax request on reply.
        var detailsView = this.getEnquiryDetailsViewRef()
        var rec = detailsView.getEnquiryRecord();
        replyView.setEnquiryId(rec.get("EnquiryLogID"));

        navview.push(replyView);
    },

    onBtnSendEnquiryReplyTapped: function () {
        console.log("onBtnSendEnquiryReplyTapped");

        var replyView = this.getEnquiryReplyViewRef();
        var detailsView = this.getEnquiryDetailsViewRef()
        var rec = detailsView.getEnquiryRecord();
        var enqId = rec.get("EnquiryLogID");

        var txt = replyView.down("#txtEnquiryResponse");
        var response = txt.getValue();
        if (response == "") {
            Ext.Msg.alert('Enquiry', 'Please enter a response.', Ext.emptyFn);
        } else {
            Ext.Ajax.request({
                scope: this,
                url: '../api/inbox',
                headers: {
                    "Accept": "application/json"
                },
                params: { EnquiryLogId: enqId, Response: response },
                method: "POST",
                success: function () {

                    Ext.Msg.alert('Enquiry', 'Thank you, your response has been sent', function () {
                        var navview = this.getMyInboxViewRef();
                        navview.pop();

                        this.refreshResponseView(enqId);
                        this.refreshInboxView();
                    }, this);
                },
                failure: function (resp) {
                    Ext.Msg.alert('Enquiry', 'Sorry, your response could not be sent', Ext.emptyFn);
                }
            });
        }
    },

    onInBoxListViewItemTap: function (scope, index, item, record) {
        console.log("onInBoxListViewItemTap");

        // set record on view
        var detailsView = this.getEnquiryDetailsViewRef();
        detailsView.setEnquiryRecord(record);

        this.refreshResponseView(record.get("EnquiryLogID"));

        // push view
        var navview = this.getMyInboxViewRef();        
        navview.push(detailsView);
    },

    refreshResponseView: function (enquiryLogId) {

        var respStore = Ext.getStore("theInboxEnquiryResponseStore");
        respStore.getProxy().setExtraParams({
            enquiryLogId: enquiryLogId
        });

        // shouldn't have to do this but for some reason the store
        // is not syncing the view on reload, seems to work if clearing it out            
        respStore.removeAll();

        respStore.load();
    },

    refreshInboxView: function () {
		TolMobile.app.showLoad(true);
		
        var enqStore = Ext.getStore("theInboxEnquiryStore");
        enqStore.load(function() {
			TolMobile.app.showLoad(false);
		});

        var self = this;
        var enqCountStore = Ext.getStore("theInboxEnquiryCountStore");
        if (!enqCountStore.isLoaded() || force) {
            enqCountStore.load(function () {
                var view = self.getMyInboxViewRef();
                var lblEnquiryCount = view.down("#lblEnquiryCount");
                var lblEnquiryResponseCount = view.down("#lblEnquiryResponseCount");
                var data = enqCountStore.getData().getAt(0);
				if ( data ) {
					lblEnquiryCount.setHtml(data.get("EnquiryCount") + " enquiries");
					lblEnquiryResponseCount.setHtml(data.get("EnquiryResponseCount") + " unread response");
				}
            });
        }
    },

    onEnquiryResponseListViewTap: function (scope, index, item) {
        console.log("onEnquiryResponseListViewTap");

        // hide the main list
        var list = Ext.ComponentQuery.query("#mainResponseListView")[0];
        list.setHidden(true);

        // add one record and select it
        var rec = item.getRecord();
        var store = Ext.create("TolMobile.store.inbox.EnquiryResponse");
        store.add(rec);

        var subListContainer = Ext.ComponentQuery.query("#subResponseListViewContainer")[0];
        var subList = subListContainer.down("enquiryresponselistview");
        var lblExtDescription = subListContainer.down("#lblExtendedDescription");

        subListContainer.setHidden(false);
        lblExtDescription.setHtml(rec.get("Notes"));

        // select the first item in the list
        subList.setStore(store);
        subList.select(0);
    },

    onBtnBackToEnquiryHistoryTap: function () {
        console.log("onBtnBackToEnquiryHistoryTap");

        // restore original view
        var list = Ext.ComponentQuery.query("#mainResponseListView")[0];
        list.setHidden(false);
        var subListContainer = Ext.ComponentQuery.query("#subResponseListViewContainer")[0];
        subListContainer.setHidden(true);
    }
});