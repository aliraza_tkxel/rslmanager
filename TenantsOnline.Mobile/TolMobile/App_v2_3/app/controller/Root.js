Ext.define('TolMobile.controller.Root', {
    extend: 'Ext.app.Controller',

    requires: [
        
    ],

    config: {
        refs: {
            homeButtonRef: '#homeButton',
            rootViewRef: 'rootview',

            homeViewRef: {
                autoCreate: true,
                selector: 'homeview',
                xtype: 'homeview'
            },
            officeIndexViewRef: {
                autoCreate: true,
                selector: 'officeindexview',
                xtype: 'officeindexview'
            },
            callViewRef: {
                autoCreate: true,
                selector: 'callview',
                xtype: 'callview'
            },
            emailViewRef: {
                autoCreate: true,
                selector: 'emailview',
                xtype: 'emailview'
            }
        },

        control: {
            "#homeButton": {
                tap: 'onHomeButtonTap'
            },

            "#officesButton": {
                tap: 'onOfficesButtonTap'
            },

            "#callButton": {
                tap: 'onCallButtonTap'
            },

            "#emailButton": {
                tap: 'onEmailButtonTap'
            }
        }
    },

    previousRootIdx: 0,    

    onHomeButtonTap: function (button, e, options) {
		console.log("onHomeButtonTap");
		this.getApplication().fireEvent('returnHome');	       
    },

    onOfficesButtonTap: function () {
        var rootView = this.getRootViewRef();
        var officeIndexView = this.getOfficeIndexViewRef();

        rootView.animateActiveItem(officeIndexView, { type: "slide", direction: "left" });
    },

    onCallButtonTap: function () {
        var rootView = this.getRootViewRef();
        var callView = this.getCallViewRef();

        rootView.animateActiveItem(callView, { type: "slide", direction: "left" });
    },

     onEmailButtonTap: function () {
        var rootView = this.getRootViewRef();
        var emailView = this.getEmailViewRef();

        rootView.animateActiveItem(emailView, { type: "slide", direction: "left" });
    }
});