Ext.define('TolMobile.controller.news.News', {
    extend: 'Ext.app.Controller',

    requires: [
        'TolMobile.view.news.NewsIndex',
        'TolMobile.view.news.NewsView'
    ],

    config: {
        refs: {
            newsIndexViewRef: 'newsindexview',
            newsListRef: 'newsindexview list',
            newsViewRef: {
                selector: 'newsview',
                xtype: 'newsview',
                autoCreate: true
            },
            btnReadMoreRef: 'newsindexview #btnReadMore'
        },

        control: {
            newsIndexViewRef: {
                initialize: 'onNewsIndexViewInitialized'
            },

            newsListRef: {
                itemtap: 'onNewsItemTap',
                disclose: 'onNewsItemDisclose'
            },

            officeViewRef: {
                initialize: 'onOfficeViewInitialized'
            },

            btnReadMoreRef: {
                tap: 'onBtnReadMoreTap'
            }
        },

        firstNewsItem: null
    },

    launch: function () {
        console.log("NewsIndex controller launched");
    },

    onNewsIndexViewInitialized: function () {
        console.log("onNewsIndexViewInitialized");

        var store = Ext.getStore("theNewsStore");

        var view = this.getNewsIndexViewRef();
        var self = this;
        if (!store.isLoaded()) {
            // store has not been loaded yet
            store.load(function (records) {
                if (records.length > 0) {
                    // at least one record so show this as main item on index                    
                    var headline = view.down("#lblHeadline");
                    var dateCreated = view.down("#lblDateCreated");
                    var newsContent = view.down("#lblNewsContent");
                    var img = view.down("#imgNews");
                    var rec = records[0];

                    headline.setHtml(rec.get("Headline"));
                    dateCreated.setHtml(rec.get("DateCreated"));

                    // content on index page shows only firts 30 words.
                    // On phones there isn't even enough room for some news content as the headline
                    // could be quite large so remove for now if on phone.
                    if (!Ext.os.is.Phone) {
                        var content = rec.get("NewsContent").split(' ').splice(0, 30).join(" ") + "..."
                        newsContent.setHtml(content);
                    } else {
                        newsContent.setHtml('');
                    }

                    img.setSrc(rec.get("ImagePath"));

                    // save for later
                    self.setFirstNewsItem(rec);
                }
                //TolMobile.app.showLoad(false);
            });
        }
    },

    onNewsItemTap: function (scope, index, target, record) {
        this.onNewsItemSelect(scope, index, target, record);
    },

    onNewsItemDisclose: function (scope, record, target, index) {
        this.onNewsItemSelect(scope, index, target, record);
    },

    onNewsItemSelect: function (scope, index, target, record) {
        console.log("onNewsItemSelect");

        scope.select(index);

        var navview = this.getNewsIndexViewRef();
        var newsview = this.getNewsViewRef();
        newsview.setNewsRecord(record);

        navview.push(newsview);
    },

    onOfficeViewInitialized: function () {
        console.log("onOfficeViewInitialized");
    },

    onBtnReadMoreTap: function () {
        var navview = this.getNewsIndexViewRef();
        var newsview = this.getNewsViewRef();
        newsview.setNewsRecord(this.getFirstNewsItem());

        navview.push(newsview);
    }
});