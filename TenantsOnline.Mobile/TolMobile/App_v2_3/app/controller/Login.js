Ext.define('TolMobile.controller.Login', {
    extend: 'Ext.app.Controller',

    requires: [

    ],

    config: {
        refs: {
            loginView: {
                autoCreate: true,
                selector: 'loginview',
                xtype: 'loginview'
            },
            loginForm: '#loginform',

            rootViewRef: {
                selector: "rootview",
                xtype: "rootview",
                autoCreate: true
            },

            registerFormView: {
                selector: 'registerformview',
                xtype: 'registerformview',
                autoCreate: true
            },

            homeViewRef: "homeview"
        },

        control: {
            "#btnLogin": {
                tap: 'onBtnLoginTap'
            },
            "#btnRegister": {
                tap: 'onBtnRegisterTap'
            },
            loginView: {
                doAutoLogin: "autoLoginHandler"
            }
        }
    },

    // only for debugging
    skipLoginForDebug: false,

    autoLoginHandler: function () {
        TolMobile.app.showLoad(true);
        Ext.Function.defer(function () {

            this.initStoresAndBootup(this);
            
        }, 3000, this);
    },

    onBtnRegisterTap: function () {
        var registerView = this.getRegisterFormView();
        TolMobile.app.switchMainView(registerView, "right");
    },

    onBtnLoginTap: function (button, e, options) {

        console.log("login");
        
        var form = this.getLoginForm();
        var credentials = form.getValues();

        if (!credentials.username || !credentials.password) {
            Ext.Msg.alert('Login', 'Please enter your username and password', Ext.emptyFn);
            return;
        }

        var success = false;
        if (!TolMobile.app.skipLogin) {

            TolMobile.app.showLoad(true);

            // scope weirdness...
            var self = this;

            var loginRequest = function () {
                Ext.Ajax.request({
                    url: '../api/auth',
                    params: credentials,
                    headers: {
                        "Accept":"application/json"
                    },
                    method: "POST",
                    success: function (response) {

                        var resp = Ext.JSON.decode(response.responseText.trim());

                        TolMobile.app.authToken = resp.AuthToken;

                        var authModel = Ext.create("TolMobile.model.AuthToken");

                        if (credentials.keepLoggedIn) {
                            // store auth-token to local storage                            
                            authModel.setData({ id: 101, AuthToken: TolMobile.app.authToken });
                        } else {
                            authModel.setData({ id: 101, AuthToken: null });
                        }
                        authModel.save();

                        success = true;
                        TolMobile.app.showLoad(false);

                        // load in stores
                        self.initStoresAndBootup(self);

                    },
                    failure: function (response, options) {
                        Ext.Msg.alert('Login', 'Sorry, your username or password is incorrect', Ext.emptyFn);                        

                        // instead of resetting both fields (can be annoying to user) just reset the pwd field
                        //form.reset();
                        var field = Ext.ComponentQuery.query("textfield[name=password]")[0];
                        field.reset();

                        TolMobile.app.showLoad(false);
                    }
                });
            }

            Ext.Function.defer(loginRequest, 3000);
        } else {
            TolMobile.app.switchMainView('TolMobile.view.tablet.Home');
        }

    },

    initStoresAndBootup: function (scope) {
        var self = scope;

        var contactStore = Ext.getStore("theContactDetailsStore");
        if (!contactStore.isLoaded()) {
            contactStore.load();
        }

        var userDetailsStore = Ext.getStore("theUserDetailsStore");
        if (!userDetailsStore.isLoaded()) {
            userDetailsStore.load({
                callback: function (record, operation, success) {
                    var rec = record[0];
                    if (success && rec) {

                        var welcomeText = "Welcome " + rec.get("FirstName") + ". What would you like to do today?";

                        var rootView = self.getRootViewRef();  // instantiate root & home view....
                        var homeView = self.getHomeViewRef();

                        homeView.setWelcomeHtml(welcomeText);
                        
                        TolMobile.app.switchMainView(rootView);
                        TolMobile.app.showLoad(false);
                    }
                }
            });
        }
    }

});