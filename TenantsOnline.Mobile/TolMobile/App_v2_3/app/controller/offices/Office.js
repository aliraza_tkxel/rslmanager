Ext.define('TolMobile.controller.offices.Office', {
    extend: 'Ext.app.Controller',

    requires: [
        'TolMobile.view.offices.OfficeIndex',
        'TolMobile.view.offices.OfficeView',
        'TolMobile.view.offices.OfficeMap'
    ],

    config: {
        refs: {
            officeIndexViewRef: 'officeindexview',
            officeListRef: 'officeindexview list',
            officeViewRef: {
                selector: 'officeview',
                xtype: 'officeview',
                autoCreate: true
            },
            officeMapViewRef: {
                selector: 'officemapview',
                xtype: 'officemapview',
                autoCreate: true
            },

            btnViewMapRef: 'officeview #btnViewMap',
            btnCallOfficeRef: 'officeview #btnCallOffice',

            mapRef: 'map'
        },

        control: {
            officeIndexViewRef: {
                initialize: 'onOfficeIndexViewInitialized',
                pop: 'onOfficeIndexViewPop'
            },

            officeListRef: {
                itemtap: 'onOfficeItemTap',
                disclose: 'onOfficeItemDisclose'
            },

            officeViewRef: {
                initialize: 'onOfficeViewInitialized'
            },

            btnViewMapRef: {
                tap: 'onBtnViewMap'
            },

            btnCallOfficeRef: {
                tap: 'onBtnCall'
            },

            mapRef: {
                googleMapRender: 'onGoogleMapRender'
            }
        }
    },

    onOfficeIndexViewPop: function () {
        if (Ext.os.is.Phone) {
            // restore font size of title
            var navview = this.getOfficeIndexViewRef();
            var bar = navview.getNavigationBar();
            bar.titleComponent.setStyle("font-size:1.2em");
        }
    },

    onOfficeIndexViewInitialized: function () {
        console.log("onOfficeIndexViewInitialized");

        TolMobile.app.showLoad(true);
        var s = Ext.getStore("theOfficesStore");
        if (!s.isLoaded()) {
            // store has not been loaded yet
            s.load(function () {
                TolMobile.app.showLoad(false);
            });
        }
    },

    onOfficeItemTap: function (scope, index, target, record) {
        this.onOfficeItemSelect(scope, index, target, record);
    },

    onOfficeItemDisclose: function (scope, record, target, index) {
        this.onOfficeItemSelect(scope, index, target, record);
    },

    onOfficeItemSelect: function (scope, index, target, record) {
        console.log("onOfficeItemTap");

        scope.select(index);

        var nextview = this.getOfficeViewRef();

        var addr = [];

        addr.push(record.get("AssociationName"));
        addr.push(record.get("Address1"));
        addr.push(record.get("Address2"));
        addr.push(record.get("Address3"));
        addr.push(record.get("Address4"));
        addr.push(record.get("TownCity"));
        addr.push(record.get("County"));
        addr.push(record.get("PostCode"));

        // build address and set on next view
        var multiLineAddr = TolMobile.utils.AddressBuilder.buildMultiLineAddress(addr);
        nextview.setOfficeAddress(multiLineAddr);
        nextview.setTelephone(record.get("Tel"));
        var navview = this.getOfficeIndexViewRef();

        // next view pushed onto stack
        navview.push(nextview);

        // save copy of record
        this.selectedOffice = record;

        // set title
        var bar = navview.getNavigationBar();
        bar.setTitle(record.get("Title"));

        // for phones the title is too long to fit so reduce font
        if (Ext.os.is.Phone) {
            bar.titleComponent.setStyle("font-size:0.8em");
        }

    },

    onBtnViewMap: function () {
		console.log("onBtnViewMap");
        var navview = this.getOfficeIndexViewRef();
		console.log("got nav view");
        var mapview = this.getOfficeMapViewRef();
		console.log("got map view");
        mapview.setOfficeRecord(this.selectedOffice);
		console.log("set office rec");

        navview.push(mapview);
		console.log("pushed navview");
    },

    onBtnCall: function () {
        var telNo = this.selectedOffice.get("Tel");
        Ext.Msg.confirm('External Link', 'Call ' + telNo + "?", function (res) {
            if (res == 'yes') {
                window.location = 'tel:' + telNo;
            }
        }, this);        
    },

    onOfficeViewInitialized: function () {
        console.log("onOfficeViewInitialized");
    },

    onGoogleMapRender: function (googleMap) {
				
        var record = this.selectedOffice;
        var xpos = record.get("Longitude");
        var ypos = record.get("Latitude");

		console.log("About to create google maps pos")        
		console.log("Created")
		console.log("About to create google maps marker")
		
		var pos = new google.maps.LatLng(ypos, xpos);
		
        var marker = new google.maps.Marker({
            position: pos
        });
		console.log("About to set maps map object")
        marker.setMap(googleMap);

        setTimeout(function () {
			console.log("map setTimeout")
            // weird timeout issue? - http://stackoverflow.com/questions/15041697/sencha-touch-google-map-and-centering-a-marker                
            googleMap.setZoom(17);
            googleMap.panTo(pos);
        }, 10);

    }
});