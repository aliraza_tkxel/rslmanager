﻿// builds an address from an array of address lines

Ext.define("TolMobile.utils.AddressBuilder", {

    singleton: true,

    buildAddress: function (addr, seperator) {
        var addressLine = "";
        for (var i = 0; i < addr.length; i++) {
            if (addr[i]) {
                if (addressLine && i < addr.length) {
                    addressLine += seperator;
                }
                addressLine += addr[i];
            }
        }
        return addressLine;
    },

    buildSingleLineAddress: function (addr) {
        var address = this.buildAddress(addr, ", ");
        return address;
    },

    buildMultiLineAddress: function (addr) {
        var address = this.buildAddress(addr, "<br/>");
        return address;
    }

});