﻿Ext.define('TolMobile.model.AuthToken', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.identifier.Uuid'
    ],

    config: {
        fields: [
            { name: 'id', type: 'int' },
            { name: 'AuthToken', type: 'string' }
        ],
        identifier: 'uuid',
        proxy: {
           type: 'localstorage',
           id: 'tol-auth-token-storage'
        }
    }
});
