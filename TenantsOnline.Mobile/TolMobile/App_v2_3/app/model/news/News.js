﻿Ext.define('TolMobile.model.news.News', {
    extend: 'Ext.data.Model',
    config: {

        fields: [
            { name: 'Headline', type: 'string' },
            { name: 'DateCreated', type: 'string' },
            { name: 'NewsContent', type: 'string' },
            { name: 'ImagePath', type: 'string' }
        ]
    }
});
