﻿Ext.define('TolMobile.model.Lookup', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'Id', type: 'int' },
            { name: 'ItemName', type: 'string' }
        ]
    }   
});
