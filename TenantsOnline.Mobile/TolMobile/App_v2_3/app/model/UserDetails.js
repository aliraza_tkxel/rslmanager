﻿
Ext.define('TolMobile.model.UserDetails', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: "Id",
        fields: [
            { name: 'Id', type: 'int' },
            { name: 'Title', type: 'int' },
            { name: 'FirstName', type: 'string' },
            { name: 'MiddleName', type: 'string' },
            { name: 'LastName', type: 'string' },
            { name: 'NINumber', type: 'string' },
            { name: 'Gender', type: 'string' },
            { name: 'DateOfBirth', type: 'date', dateFormat: 'c' },

            { name: 'Mobile', type: 'string' },
            { name: 'Telephone', type: 'string' },
            { name: 'Email', type: 'string' },     

            { name: 'Occupation', type: 'string' },
            { name: 'MaritalStatus', type: 'int' },
            { name: 'Ethnicity', type: 'int' },
            { name: 'Religion', type: 'int' },
            { name: 'SexualOrientation', type: 'int' },

            { name: 'Language', type: 'string' },
            { name: 'Communication' },
            { name: 'PreferredContact', type: 'string' },
            { name: 'Disability' },
            { name: 'TenancyId' }
        ],
        validations: [
            { type: 'presence', field: 'Title', message: 'Please select a title' },
            { type: 'presence', field: 'FirstName', message: 'Please enter a first name' },
            { type: 'presence', field: 'LastName', message: 'Please enter a last name' },
            { type: 'presence', field: 'Gender', message: 'Please select a gender' },
            { type: 'presence', field: 'NINumber', message: 'Please enter an NI number' },
            { type: 'presence', field: 'DateOfBirth', message: 'Please enter a Date Of Birth' },

            { type: 'presence', field: 'Mobile', message: 'Please enter a mobile number' },
            { type: 'presence', field: 'Telephone', message: 'Please enter a telephone number' },
            { type: 'presence', field: 'Email', message: 'Please enter an email address' },

            { type: 'presence', field: 'MaritalStatus', message: 'Please select marital status' },
            { type: 'presence', field: 'Ethnicity', message: 'Please select ethnicity type' }           
        ]
    }
});
