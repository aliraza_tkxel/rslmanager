﻿Ext.define('TolMobile.model.Debug', {
    extend: 'Ext.data.Model',
	
    config: {
        fields: [            
             { name: 'Type', type: 'string' }
        ]
    }   
});
