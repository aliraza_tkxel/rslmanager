﻿Ext.define('TolMobile.model.inbox.Enquiry', {
    extend: 'Ext.data.Model',
    config: {

        fields: [
            { name: 'EnquiryLogID', type: 'int' },
            { name: 'CreationDate', type: 'date', dateFormat: 'c' },
            { name: 'Nature', type: 'string' },
            { name: 'ImageKeyFlag', type: 'int' },
            { name: 'Description', type: 'string' }
        ]
    }
});
