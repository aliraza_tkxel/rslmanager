﻿Ext.define('TolMobile.model.inbox.EnquiryCount', {
    extend: 'Ext.data.Model',
    config: {

        fields: [
            { name: 'SentEnquiries', type: 'int' },
            { name: 'UnreadEnquiries', type: 'int' }            
        ]
    }
});
