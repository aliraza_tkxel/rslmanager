﻿Ext.define('TolMobile.model.inbox.EnquiryResponse', {
    extend: 'Ext.data.Model',
    config: {
        //idProperty:'EnquiryLogId',
        fields: [
            { name: 'EnquiryLogId', type: 'int' },
            { name: 'LastActionDate', type: 'date', dateFormat: 'c' },
            { name: 'Notes', type: 'string' },
            { name: 'JournalId', type: 'int' },
            { name: 'HistoryId', type: 'int' },
            { name: 'LastActionUser', type: 'string' }
        ]
    }
});
