﻿Ext.define('TolMobile.model.rent.Rent', {
    extend: 'Ext.data.Model',
    config: {

        fields: [
            { name: 'TransactionDate', type: 'string' },
            { name: 'Debit', type: 'string' },
            { name: 'Credit', type: 'string' },
            { name: 'Balance', type: 'string' }
        ]
    }
});
