﻿Ext.define('TolMobile.model.offices.Office', {
    extend: 'Ext.data.Model',
    config: {

        fields: [
            { name: 'Title', type: 'string' },
            { name: 'AssociationName', type: 'string' },

            { name: 'Address1', type: 'string' },
            { name: 'Address2', type: 'string' },
            { name: 'Address3', type: 'string' },
            
            { name: 'TownCity', type: 'string' },
            { name: 'County', type: 'string' },
            { name: 'PostCode', type: 'string' },
            { name: 'Tel', type: 'string' },
            
            { name: 'Latitude', type: 'string' },
            { name: 'Longitude', type: 'string' }           
        ]
    }
});
