﻿
Ext.define('TolMobile.model.RegisterDetails', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'FirstName', type: 'string' },
            { name: 'LastName', type: 'string' },
            { name: 'TenancyId', type: 'int' },
            { name: 'DateOfBirth', type: 'date', dateFormat: 'c' },

            { name: 'Email', type: 'string' },
            { name: 'Postcode', type: 'string' },
            { name: 'Password', type: 'string' }
        ],
        validations: [
            { type: 'presence', field: 'FirstName', message: 'Please enter a first name' },
            { type: 'presence', field: 'LastName', message: 'Please enter a last name' },
            { type: 'presence', field: 'TenancyId', message: 'Please enter your Tenancy Id' },
            { type: 'presence', field: 'DateOfBirth', message: 'Please enter a Date Of Birth' },

            { type: 'presence', field: 'Email', message: 'Please enter an email address' },
            { type: 'presence', field: 'Postcode', message: 'Please enter a postcode' },
            { type: 'presence', field: 'Password', message: 'Please enter a password' }
        ]
    }
});
