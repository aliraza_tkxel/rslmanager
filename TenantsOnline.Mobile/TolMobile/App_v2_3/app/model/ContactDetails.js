﻿Ext.define('TolMobile.model.ContactDetails', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: "Id",
        fields: [
            { name: 'Id', type: 'int' },

            { name: 'Mobile', type: 'string' },
            { name: 'Telephone', type: 'string' },
            { name: 'Email', type: 'string' },            

            { name: 'HouseNo', type: 'string' },
            { name: 'Address1', type: 'string' },
            { name: 'Address2', type: 'string' },
            { name: 'Address3', type: 'string' },
            { name: 'TownCity', type: 'string' },
            { name: 'County', type: 'string' },
            { name: 'PostCode', type: 'string' }
            
        ],
        validations: [
            { type: 'presence', field: 'Email', message: 'Please enter Email' },
            { type: 'presence', field: 'Address1', message: 'Please enter address 1' },
            { type: 'presence', field: 'PostCode', message: 'Please enter Post Code' }
            
        ]
    }
});
