﻿Ext.define('TolMobile.model.SimpleLookup', {
    extend: 'Ext.data.Model',
    config: {
        fields: [     
            { name: 'Id', type: 'string' },  
            { name: 'ItemName', type: 'string' }
        ]
    }   
});
