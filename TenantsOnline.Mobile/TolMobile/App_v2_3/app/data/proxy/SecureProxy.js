﻿Ext.define('TolMobile.proxy.SecureProxy', {
    extend: 'Ext.data.proxy.Rest',

    // Set your proxy alias
    alias: 'proxy.securerestproxy',

    config: {
        type: 'reset',
        url: '/api/user/',
        headers: {
            "AUTH-TOKEN": TolMobile.app.authToken
        },
        reader: {
            type: 'json'
        }
    }
});