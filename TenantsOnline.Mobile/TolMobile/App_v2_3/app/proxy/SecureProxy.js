﻿Ext.define('TolMobile.proxy.SecureProxy', {
    alias: 'proxy.securerestproxy',
    extend: 'Ext.data.proxy.Rest',

    constructor: function () {       
        this.reader = {
            type: 'json'
        }
        this.callParent(arguments);
    },

    config: {
    
    }
});