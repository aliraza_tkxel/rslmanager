﻿Ext.define('TolMobile.store.Ethnicity', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.Lookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.Lookup',
        storeId: 'theEthnicityStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/ethnicity',
            reader: {
                type: 'json'
            }
        }
    }    
});

                