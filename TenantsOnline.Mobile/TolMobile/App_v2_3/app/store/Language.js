﻿Ext.define('TolMobile.store.Language', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.SimpleLookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theLanguageStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/language/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                