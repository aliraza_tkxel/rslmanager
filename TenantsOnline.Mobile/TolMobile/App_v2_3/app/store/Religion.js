﻿Ext.define('TolMobile.store.Religion', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.Lookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.Lookup',
        storeId: 'theReligionStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/religion',
            reader: {
                type: 'json'
            }
        }
    }    
});

                