﻿Ext.define('TolMobile.store.ASBCategories', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.Lookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theASBCategoriesStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/asbcategory/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                