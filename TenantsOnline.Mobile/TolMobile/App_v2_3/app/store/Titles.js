﻿Ext.define('TolMobile.store.Titles', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.Lookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.Lookup',
        storeId: 'theTitleStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/titles',
            reader: {
                type: 'json'
            }
        }
    }    
});

                