﻿Ext.define('TolMobile.store.ParkingTypes', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.Lookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theParkingTypesStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/parkingtype/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                