﻿Ext.define('TolMobile.store.ComplaintTypes', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.Lookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theComplaintTypesStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/complainttype/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                