﻿Ext.define('TolMobile.store.SexualOrientation', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.Lookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.Lookup',
        storeId: 'theSexualOrientationStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/sexualorientation',
            reader: {
                type: 'json'
            }
        }
    }    
});

                