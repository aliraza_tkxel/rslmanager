﻿Ext.define('TolMobile.store.rent.Balances', {
    extend: 'Ext.data.Store',

    requires: [        
        'Ext.data.proxy.Rest'
    ],

    config: {
        // save creating a seperate model instance
        fields: [
            'AntHB', 'AdvHB', 'AccountBalance', 'InArrears', 'HousingBenefit'
        ],

        storeId: 'theBalanceStore',
        proxy: {
            type: 'rest',
            url: '../api/balance',
            reader: {
                type: 'json'             
            }
        }
    }
});