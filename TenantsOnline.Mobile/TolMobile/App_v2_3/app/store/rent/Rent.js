﻿Ext.define('TolMobile.store.rent.Rent', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.rent.Rent',
        'Ext.data.proxy.Rest'
    ],      

    config: {
        model: 'TolMobile.model.rent.Rent',
        storeId: 'theRentAccountStore',
        proxy: {
            type: 'rest',
            url: '../api/rent',
            reader: {
                type: 'json'             
            }
        }
    }
});