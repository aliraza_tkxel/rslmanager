﻿Ext.define('TolMobile.store.rent.RentPeriod', {
    extend: 'Ext.data.Store',

    requires: [
       'TolMobile.model.rent.RentPeriod'
    ],      

    config: {
        model: 'TolMobile.model.rent.RentPeriod',
        storeId: 'theRentPeriodStore',
        data: [
            { Id: 1, ItemName: "Recent Transactions" },
            { Id: 2, ItemName: "Previous 4 weeks" },
            { Id: 3, ItemName: "Previous 3 months" },
            { Id: 6, ItemName: "Previous 6 months" },
            { Id: 0, ItemName: "All" }
        ]
    }
});