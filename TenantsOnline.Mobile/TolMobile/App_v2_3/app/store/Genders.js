﻿Ext.define('TolMobile.store.Genders', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.SimpleLookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theGenderStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/genders',
            reader: {
                type: 'json'
            }
        }
    }    
});

                