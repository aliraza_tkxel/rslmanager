﻿Ext.define('TolMobile.store.AbandonedTypes', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.Lookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theAbandonedTypesStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/abandonedtype/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                