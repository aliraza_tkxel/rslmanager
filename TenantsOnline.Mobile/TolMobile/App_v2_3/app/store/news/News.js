﻿Ext.define('TolMobile.store.news.News', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.news.News',
        'Ext.data.proxy.Rest'
    ],

    config: {
        storeId: 'theNewsStore',
        model: 'TolMobile.model.news.News',
        proxy: {
            type: 'rest',
            url: '../api/news',
            reader: {
                type: 'json'                
            }
        }      
    }
});

                