﻿Ext.define('TolMobile.store.Debug', {
    extend: 'Ext.data.Store',

    requires: [
       'TolMobile.model.Debug'
    ],      

    config: {
        model: 'TolMobile.model.Debug',
        storeId: 'theDebugStore'        
    }
});