﻿Ext.define('TolMobile.store.TerminationReasons', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.SimpleLookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theTerminationReasonStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/terminationreason',
            reader: {
                type: 'json'
            }
        }
    }    
});

                