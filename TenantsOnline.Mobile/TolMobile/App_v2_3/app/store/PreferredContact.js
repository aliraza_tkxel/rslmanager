﻿Ext.define('TolMobile.store.PreferredContact', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.SimpleLookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'thePreferredContactStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/preferredcontact/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                