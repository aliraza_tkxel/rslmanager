﻿Ext.define('TolMobile.store.offices.OfficeIndex', {
    extend: 'Ext.data.Store',
    
    requires: [
        
    ],

    config: {        
        storeId: 'theOfficeIndexStore',
        fields: [
            {
                name: 'Id'
            },
            {
                name: 'Xid'
            },
            {
                name: 'DisplayName'
            }
        ]
    }
});

                