﻿Ext.define('TolMobile.store.offices.Office', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.offices.Office',
        'Ext.data.proxy.Rest'
    ],

    config: {
        storeId: 'theOfficesStore',
        model: 'TolMobile.model.offices.Office',
        proxy: {
            type: 'rest',
            url: '../api/office',
            reader: {
                type: 'json'                
            }
        }      
    }
});

                