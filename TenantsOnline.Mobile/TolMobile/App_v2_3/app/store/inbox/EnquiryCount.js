﻿Ext.define('TolMobile.store.inbox.EnquiryCount', {
    extend: 'Ext.data.Store',

    requires: [        
        'Ext.data.proxy.Rest'
    ],      

    config: {
        storeId: 'theInboxEnquiryCountStore',
        fields: [
            { name: 'EnquiryCount' },
            { name: 'EnquiryResponseCount' }            
        ],
        proxy: {
            type: 'rest',
            url: '../api/inboxcount',
            reader: {
                type: 'json'             
            }
        }
    }
});