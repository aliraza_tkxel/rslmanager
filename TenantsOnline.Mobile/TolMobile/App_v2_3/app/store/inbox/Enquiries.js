﻿Ext.define('TolMobile.store.inbox.Enquiries', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.inbox.Enquiry',
        'Ext.data.proxy.Rest'
    ],

    config: {        
        model: 'TolMobile.model.inbox.Enquiry',
        storeId: 'theInboxEnquiryStore',
        proxy: {
            type: 'rest',
            url: '../api/inbox',
            reader: {
                type: 'json'             
            }
        }
    }
});