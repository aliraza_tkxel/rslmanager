﻿Ext.define('TolMobile.store.inbox.EnquiryResponse', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.inbox.EnquiryResponse',
        'Ext.data.proxy.Rest'
    ],

    config: {        
        model: 'TolMobile.model.inbox.EnquiryResponse',
        storeId: 'theInboxEnquiryResponseStore',
        proxy: {
            type: 'rest',
            url: '../api/response',
            reader: {
                type: 'json'             
            }
        }
    }
});