﻿Ext.define('TolMobile.store.Disability', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.SimpleLookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theDisabilityStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/disability/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                