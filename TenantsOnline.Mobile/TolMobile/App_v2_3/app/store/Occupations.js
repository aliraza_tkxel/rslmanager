﻿Ext.define('TolMobile.store.Occupations', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.SimpleLookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theOccupationStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/occupations',
            reader: {
                type: 'json'
            }
        }
    }    
});

                