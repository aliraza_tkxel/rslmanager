﻿Ext.define('TolMobile.store.RegisterDetails', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.RegisterDetails',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: false,
        model: 'TolMobile.model.RegisterDetails',
        storeId: 'theRegisterDetailsStore',
        proxy: {
            type: 'rest',            
            url : '../api/register/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                