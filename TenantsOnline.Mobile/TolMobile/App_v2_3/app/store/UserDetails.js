﻿Ext.define('TolMobile.store.UserDetails', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.UserDetails',
        'TolMobile.proxy.SecureProxy'
    ],

    config: {        
        model: 'TolMobile.model.UserDetails',
        storeId: 'theUserDetailsStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/user/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                