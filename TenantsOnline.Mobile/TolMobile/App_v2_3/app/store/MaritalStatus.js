﻿Ext.define('TolMobile.store.MaritalStatus', {
    extend: 'Ext.data.Store',

    requires: [
        'TolMobile.model.Lookup',        
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.Lookup',
        storeId: 'theMaritalStatusStore',
        proxy: {
            type: 'rest',            
            url : '../api/lookup/maritalstatus',
            reader: {
                type: 'json'
            }
        }
    }    
});

                