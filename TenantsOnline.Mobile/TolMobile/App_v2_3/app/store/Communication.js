﻿Ext.define('TolMobile.store.Communication', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.SimpleLookup',
        'Ext.data.proxy.Rest'
    ],

    config: {
        autoLoad: true,
        model: 'TolMobile.model.SimpleLookup',
        storeId: 'theCommunicationStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/lookup/communication/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                