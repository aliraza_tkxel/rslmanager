﻿Ext.define('TolMobile.store.ContactDetails', {
    extend: 'Ext.data.Store',
    
    requires: [
        'TolMobile.model.ContactDetails',
        'TolMobile.proxy.SecureProxy'
    ],

    config: {
        model: 'TolMobile.model.ContactDetails',
        storeId: 'theContactDetailsStore',
        proxy: {
            type: 'securerestproxy',
            url: '../api/contact/',
            reader: {
                type: 'json'
            }
        }
    }    
});

                