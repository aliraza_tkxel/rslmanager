﻿Ext.define('TolMobile.store.viewstores.EnquiryViews', {
    extend: 'Ext.data.Store',
    
    requires: [
        
    ],

    config: {
        data: [
                // SelectTypes hard-coded to pre-populate
				{Id: 1, Xid: 'endtenancyview', DisplayName: "I'd like to end my tenancy" },

				{ Id: 2, Xid: 'requestparkingtypeview', DisplayName: "I'd like a garage", SelectType: 13, SharedView: true, Title: "I'd like a garage" },
				{ Id: 3, Xid: 'requestparkingtypeview', DisplayName: "I'd like a parking space", SelectType: 14, SharedView: true, Title: "I'd like a parking space" },

				{ Id: 4, Xid: 'reportcomplaintview', DisplayName: "I want to make a complaint", Title: "I want to make a complaint" },
				{ Id: 5, Xid: 'reportasbview', DisplayName: "I'm a victim of ASB", Title: "I'm a victim of ASB" },

				{ Id: 6, Xid: 'reportabandonedtypeview', DisplayName: "Report vandalism and neglect", SelectType: 16, SharedView: true, Title: 'Vandalism and Neglect' }
                //{ Id: 7, Xid: 'reportfaultindexview', DisplayName: "I want to report a fault", Title: 'Report Fault' }

		],
        autoLoad: true,
        storeId: 'theEnquiryViewStore',
        fields: [
            {
                name: 'Id'
            },
            {
                name: 'Xid'
            },
            {
                name: 'DisplayName'
            },
            {
                name: 'SelectType'
            },
            {
                name: 'SharedView'
            },
            {
                name: 'Title'
            }
        ]
    }
});

                