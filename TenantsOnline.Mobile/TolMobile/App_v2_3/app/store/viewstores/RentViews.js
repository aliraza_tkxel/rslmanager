﻿Ext.define('TolMobile.store.viewstores.RentViews', {
    extend: 'Ext.data.Store',
    
    requires: [
        
    ],

    config: {
        data: [
				{ Id: 1, Xid: 'paymyrentview', DisplayName: "Pay My Rent" },
				{ Id: 2, Xid: 'myrentview', DisplayName: "How much rent do I owe?" }				
		],
        autoLoad: true,
        storeId: 'theRentViewStore',
        fields: [
            {
                name: 'Id'
            },
            {
                name: 'Xid'
            },
            {
                name: 'DisplayName'
            }
        ]
    }
});

                