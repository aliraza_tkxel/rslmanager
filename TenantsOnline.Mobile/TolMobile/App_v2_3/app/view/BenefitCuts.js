Ext.define('TolMobile.view.BenefitCuts', {
    extend: 'Ext.Panel',
    alias: 'widget.benefitcutsview',

    config: {
        rootCardIdx: 3,
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Benefit Cuts'
            }
        ]
    }

});