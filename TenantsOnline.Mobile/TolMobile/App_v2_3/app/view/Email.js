Ext.define('TolMobile.view.Email', {
    extend: 'Ext.Panel',
    alias: 'widget.emailview',

    config: {
        rootCardIdx: 4,
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Email'
            }
        ]
    }

});