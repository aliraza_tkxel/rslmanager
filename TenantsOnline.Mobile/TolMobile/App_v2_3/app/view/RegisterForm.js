Ext.define('TolMobile.view.RegisterForm', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.registerformview',

    requires: [
        'TolMobile.model.RegisterDetails',
        'TolMobile.view.RegisterSuccess'
    ],

    config: {
        listeners: {
            initialize: function () {
                console.log("registerform view initialised");

                var record = Ext.create("TolMobile.model.RegisterDetails");
                if (record) {
                    this.setRecord(record);
                }
            }
        },

        items: [
			{
			    xtype: 'textfield',
			    label: 'First Name',
			    labelWidth: '50%',
			    name: 'FirstName'
			},
			{
			    xtype: 'textfield',
			    label: 'Last Name',
			    labelWidth: '50%',
			    name: 'LastName'
			},
			{
			    xtype: 'textfield',
			    label: 'My Tenancy ID',
			    labelWidth: '50%',
			    name: 'TenancyId'
			},
			{
			    xtype: 'emailfield',
			    label: 'My Email',
			    labelWidth: '50%',
			    placeHolder: 'email@example.com',
			    itemId: 'email',
			    name: 'Email'
			},
			{
			    xtype: 'emailfield',
			    label: 'Confirm Email',
			    labelWidth: '50%',
			    placeHolder: 'email@example.com',
			    itemId: 'email2'
			},
			{
			    xtype: 'datepickerfield',
			    label: 'Date of Birth',
			    labelWidth: '50%',
			    placeHolder: 'dd/mm/yyyy',
			    name: 'DateOfBirth',
			    picker: {
			        yearFrom: 1900,
			        slotOrder: ["day", "month", "year"]
			    }
			},
			{
			    xtype: 'textfield',
			    label: 'Postcode',
			    labelWidth: '50%',
			    itemId: 'postcode',
			    name: 'Postcode'
			},
			{
			    xtype: 'spacer'
			},
			{
			    xtype: 'label',
			    html: 'Protect your info with a password:',
			    margin: 5
			},
			{
			    xtype: 'passwordfield',
			    label: 'Password',
			    labelWidth: '50%',
			    itemId: 'password',
			    name: 'Password'
			},
			{
			    xtype: 'passwordfield',
			    label: 'Confirm',
			    labelWidth: '50%',
			    itemId: 'password2'
			},
			{
			    xtype: 'titlebar',
			    docked: 'top',
			    title: 'Register',
			    items: [
					{
					    xtype: 'button',
					    ui: 'back',
					    text: 'Back',
					    action: 'back'
					},
					{
					    xtype: 'button',
					    align: 'right',
					    text: 'Register',
					    itemId: 'btnRegister'
					}
			    ]
			}
        ]
    }
});