Ext.define('TolMobile.view.LoginTest', {
    extend: 'Ext.Panel',    

    requires: [
        'Ext.field.Password',
        'Ext.Img',
        'Ext.field.Email'
    ],    

    config: {
        // need id same as future xtype mapping in profile as component xtype is not registered 
        // with the component manager
        id: 'loginview',
        layout: {
            type: 'vbox'
        },
        scrollable: false,
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Tenants Online'
            },
            {
                xtype: 'formpanel',
                scrollable: false,
                flex: 1,
                id: 'loginform',
                margin: 7,
                defaults: {
                    labelWidth: "40%"
                },
                items: [
                    {
                        xtype: 'textfield',
                        label: 'Username',
                        name: 'username'
                    },
                    {
                        xtype: 'passwordfield',
                        label: 'Password',
                        name: 'password'
                    },
                   {
                       xtype: 'checkboxfield',
                       name: 'keepLoggedIn',
                       label: 'Keep Me Logged In',
                       labelWidth: "75%"
                   },
                    {
                        xtype: 'container',
                        layout: {
                            pack: 'center',
                            type: 'hbox'
                        },
                        items: [  
                             {
                                xtype: 'button',
                                id: 'btnRegister',
                                text: 'Register',
                                width: 100,
                                padding: 10,
                                margin: 5
                            },                         
                            {
                                xtype: 'button',
                                id: 'btnLogin',
                                text: 'Login',
                                width: 170,
                                padding: 10,
                                margin: 5,
                                ui: 'action'
                            }   
                        ]
                    }               
                ]
            }
        ],
        listeners: {
            painted: function () {
                // not sure if this logic should be here or view should
                // fire back to the controller??
                console.log("Login painted");

                if (TolMobile.app.authToken) {
                    console.log("cached auth token found:" + TolMobile.app.authToken);
                    this.fireEvent("doAutoLogin", this);
                }
			}
		}
    }

});