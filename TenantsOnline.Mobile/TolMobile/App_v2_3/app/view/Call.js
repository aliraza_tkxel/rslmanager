Ext.define('TolMobile.view.Call', {
    extend: 'Ext.Panel',
    alias: 'widget.callview',

    config: {
        rootCardIdx: 3,
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Call'
            }
        ]
    }

});