Ext.define('TolMobile.view.Root', {
    extend: 'Ext.Container',
    alias: 'widget.rootview',

    requires: [        
        'TolMobile.view.offices.OfficeIndex',
        'TolMobile.view.Call',
        'TolMobile.view.Email',
		'TolMobile.view.BenefitCuts'
    ],

    config: {
        layout: {
            type: 'card'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'center',
                    type: 'hbox'
                },
                defaults: {
                    padding: 10
                },
                items: [
                    {
                        xtype: 'button',
                        id: 'homeButton',                        
                        text: 'Home'                        
                    },
                    {
                        xtype: 'button',
                        id: 'officesButton',
                        text: 'Offices'                                      
                    },
                    {
                        xtype: 'button',
                        id: 'callButton',
                        text: 'Call'                        
                    },
                    {
                        xtype: 'button',
                        id: 'emailButton',
                        text: 'Email'                        
                    }
                ]
            },
            {
                xtype: 'homeview'
            }
        ]
    }

});