﻿Ext.define('TolMobile.view.tablet.RentGrid', {
    extend: 'Ext.ux.touch.grid.List',    

    requires: [                
        'TolMobile.store.rent.Rent'
    ],

    config: {
        title: 'Rent',
        fullscreen: false,
        store: 'theRentAccountStore',
        height: "100%",
        width: "100%",
        margin: 10,
        columns: [
            {
                header: 'Date',
                dataIndex: 'TransactionDate',
                width: '20%',
                style: 'padding-left: 1em;'               
            },
            {
                header: 'Debit',
                dataIndex: 'Debit',
                width: '20%',
                style: 'text-align: center;'
            },
            {
                header: 'Credit',
                dataIndex: 'Credit',
                width: '20%',
                style: 'text-align: center;'
            },
            {
                header: 'Balance',
                dataIndex: 'Balance',
                width: '20%',
                style: 'text-align: center;'
            }
        ],
        features: [
      
        ]
    }

});