﻿Ext.define('TolMobile.view.tablet.MyRent', {
    extend: 'Ext.Container',

    requires: [
        'TolMobile.store.rent.Rent'
    ],
	
	config: {    
        layout: 'vbox',
		title: 'My Rent',   		
        items: [			
            {
                xtype: 'container',
				// wp uses 2 rows for select field                
				items: [
					{
						xtype: 'container',
						margin: 10,
						items: [
							 {
								 xtype: 'tenantdetailview'
							 }
						]
					},
					{
						xtype: 'container',                 
						padding: "5 10",
						layout: 'hbox',
						cls: 'housingBenefit',
						items: [
							 {
								 xtype: 'label',                         
								 style: '',                                                 
								 html: 'Estimate Housing Benefit Due:'
							 },
							  {
								  xtype: 'label',                          
								  style: '',
								  itemId: 'lblHousingBenefit',
								  right: 0,  
								  padding: "5 0",                  
								  html: ''
							  }
						]
					 },
					{
						 xtype: 'container',
						 margin: 10,
						 layout: 'hbox',
						 items: [
							  {
								  xtype: 'label',                          
								  style: 'text-align:right;font-weight:bold',                          
								  html: 'Owed to BHA:'
							  },
							   {
								   xtype: 'label',
								   right: 0,
								   style: 'font-weight:bold',
								   itemId: 'lblBalance',
								   html: ''
							   }
						]
					},
					{
						xtype: 'selectfield',
						label: 'Period:',
						store: 'theRentPeriodStore',
						displayField: 'ItemName',
						valueField: 'Id',
						name: 'RentPeriod',
						itemId: 'ddlRentPeriod',
						margin: 10
					}
				]
            }, 
			{
				xtype: 'container',						
				flex: 1,                
				layout: 'fit',
				items: [
					{
						xtype: 'rentgridview'
					} 
				]
			}			
        ]
    }

});