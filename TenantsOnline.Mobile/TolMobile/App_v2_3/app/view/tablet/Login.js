Ext.define('TolMobile.view.tablet.Login', {
    extend: 'Ext.Panel',

    requires: [
        'Ext.field.Password', 
        'Ext.Img',
        'Ext.field.Email'
    ],             

    config: {
        // need id same as future xtype mapping in profile as component xtype is not registered 
        // with the component manager
        id: 'loginview',
        layout: {
            type: 'vbox'
        },        
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',               
                title: 'Tenants Online'
            },
            {
                xtype: 'formpanel',
                flex: 1,
                id: 'loginform',
                defaults: {                    
                    labelAlign: 'top'
                },
                items: [
                            {
                                xtype: 'fieldset',
                                title: 'Login',
                                items: [
                                            {
                                                xtype: 'emailfield',
                                                label: 'Username',
                                                name: 'username',
                                                labelAlign: 'top',
                                                placeHolder: 'john.smith@email.com'
                                            },
                                            {
                                                xtype: 'passwordfield',
                                                label: 'Password',
                                                name: 'password',
                                                labelAlign: 'top'
                                            },
                                           {
                                                xtype: 'checkboxfield',
                                                name : 'keepLoggedIn',
                                                label: 'Keep Me Logged In',
                                                labelAlign: 'left'
                                            }                                                                                          
                                        ]
                            },
                            {
                                xtype: 'container',
                                layout: {
                                    pack: 'center',
                                    type: 'hbox'
                                },
                                items: [                                   
                                    {
                                        xtype: 'button',
                                        id: 'btnRegister',
                                        text: 'Register',
                                        width: 200,
                                        padding: 10,
                                        margin: 10
                                    },
                                    {
                                        xtype: 'button',
                                        id: 'btnLogin',
                                        text: 'Login',
                                        width: 350,
                                        padding: 10,
                                        margin: 10,
                                        ui: 'action'
                                    }  
                                ]
                            }     
                        ]                
            }
        ],
        listeners: {
            painted: function() {
                // not sure if this logic should be here or view should
                // fire back to the controller??
                console.log("Login painted");

                if ( TolMobile.app.authToken ) {
                    console.log("cached auth token found:" + TolMobile.app.authToken);  
                    this.fireEvent("doAutoLogin", this);    
                }
            }
        }
    }
});