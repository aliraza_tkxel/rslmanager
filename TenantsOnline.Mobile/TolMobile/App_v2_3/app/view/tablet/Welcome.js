Ext.define("TolMobile.view.tablet.Welcome", {
    extend: 'Ext.Panel',
        
    requires: [
        'Ext.TitleBar'
    ],


    initialize: function () {
        this.callParent();       

        var childitems = [
            {
                xtype: 'image',
                height: 201,
                margin: 20,
                src: 'resources/css/images/logo200.png'
            },
            {
                xtype: 'label',
                html: '<div class="welcomeTitle">Welcome to <strong>Broadland Housing Tenants Online</strong>, where you can: </div><ul><li>View your rent statement</li><li>Report a fault</li><li>Pay your rent</li><li>Feedback on our service</li>',
                margin: 20,
                styleHtmlCls: 'WelcomeTol',
                styleHtmlContent: true
            },
            {
                xtype: 'container',
                defaults: {
                    flex: 1,
                    margin: 5
                },
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'Register',
                        action: 'register',
                        padding: 10                
                    },
                    {
                        xtype: 'button',
                        id: 'btnSignInNow',
                        text: 'Sign In Now',
                        action: 'signin',
                        ui: 'action',
                        padding:10                        
                    }
                ]
            }
        ];

        var root = [
            {
                xtype: 'container',               
                items: childitems,
                layout: {
                    type: 'vbox'                    
                },
                centered:true                               
            }
        ]

        this.add(root);

    },

    config: {
        layout: {
            type: 'vbox'
        }        
    }
});
