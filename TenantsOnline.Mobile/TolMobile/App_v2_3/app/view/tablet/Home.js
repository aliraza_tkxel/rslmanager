Ext.define('TolMobile.view.tablet.Home', {
    extend: 'Ext.Container',

    requires: ['Ext.Label', 'Ext.Img'],

    config: {
        id: 'homeview',
        rootCardIdx: 1,

        layout: {
            type: 'vbox'
        },
        defaults: {
            flex: 1,
            layout: 'hbox'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Home',
                layout: {
                    pack: 'end',
                    type: 'hbox'
                },
                items: [
                        {
                            xtype: 'button',
                            text: 'Logout',
                            action: 'Logout'
                        }
                ]
            },
            {
                xtype: 'label',
                flex: 0.15,
                margin: 10,
                id: "WelcomeText"
            },
            {
                xtype: 'container',
                flex: 1,
                defaults: {
                    flex: 1
                },
                layout: {
                    type: 'vbox'
                },
                items: [

                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: {
                            flex: 1,
                            height: 100
                        },
                        layout: {
                            align: 'end',
                            type: 'hbox'
                        },
                        items: [
                             {
                                 xtype: 'image',
                                 src: 'resources/css/images/cogs.png',
                                 action: 'ShowMyDetails'
                             },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/speech.png',
                                action: 'ReportIt'
                            },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/house.png',
                                action: 'MyRent'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        defaults: {
                            flex: 0.5
                        },
                        layout: {
                            align: 'start',
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'My Details',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'Report It',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'Pay Rent',
                                style: 'text-align:center'
                            }
                        ]
                    }
                ]
            },
            {
                 xtype: 'container',
                 flex: 1,
                 defaults: {
                     flex: 1
                 },
                 layout: {
                     type: 'vbox'
                 },
                 items: [

                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: {
                            flex: 1,
                            height: 100
                        },
                        layout: {
                            align: 'end',
                            type: 'hbox'
                        },
                        items: [
                             {
                                 xtype: 'image',
                                 src: 'resources/css/images/offices.png',
                                 action: 'Offices'
                             },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/news.png',
                                action: 'News'
                            },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/benefitCuts.png',
                                action: 'BenefitCuts'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        defaults: {
                            flex: 0.5
                        },
                        layout: {
                            align: 'start',
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Offices',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'News',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'Benefit Cuts',
                                style: 'text-align:center'
                            }
                        ]
                    }
                ]
             },
            {
                xtype: 'container',
                flex: 1,
                defaults: {
                    flex: 1
                },
                layout: {
                    type: 'vbox'
                },
                items: [

                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: {
                            flex: 1,
                            height: 100
                        },
                        layout: {
                            align: 'end',
                            type: 'hbox'
                        },
                        items: [
                             {
                                 xtype: 'image',
                                 src: 'resources/css/images/inbox.png',
                                 action: 'MyInbox'
                             },
                            {
                                xtype: 'image',
                                src: '',
                                action: ''
                            },
                            {
								id: 'imgDebug',
                                xtype: 'image',
                                src: '',
                                action: 'Debug'								
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        defaults: {
                            flex: 0.5
                        },
                        layout: {
                            align: 'start',
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'My Inbox',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                style: 'text-align:center',
								id: 'lblDebug'
                            }
                        ]
                    }
                ]
            }
        ],
		
		listeners: {
			initialize: function(view) {
				console.log("init home view");
				
				if ( TolMobile.app.isDebug ) {
					var lbl = view.down("#lblDebug");
					lbl.setHtml("Debug");
					var img = view.down("#imgDebug");
					img.setSrc("resources/css/images/appIcon.png");				
				}
			}
        }
    },
	
    // methods
    setWelcomeHtml: function (html) {
        var lbl = Ext.getCmp("WelcomeText");
        lbl.setHtml(html);
    }

});