Ext.define('TolMobile.view.inbox.EnquiryResponseList', {
    extend: 'Ext.dataview.DataView',
    alias: 'widget.enquiryresponselistview',
  
    requires: [
        'TolMobile.view.inbox.EnquiryResponseListItem'
    ],

    config: {
        layout: 'fit',             
        useComponents: true,
        singleSelect: true,
        cls: '',
        store: 'theInboxEnquiryResponseStore',      
        defaultType: 'enquiryresponselistitem'
    }
});