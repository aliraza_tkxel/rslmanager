Ext.define('TolMobile.view.inbox.MyInbox', {
    extend: 'Ext.navigation.View',
    alias: 'widget.myinboxview',

    requires: [
        'Ext.navigation.View',
        'TolMobile.view.inbox.InboxList'
    ],

    config: {
        title: 'My Inbox',
        fullscreen: true,
        navigationBar: {
            items: [
                    {
                        xtype: 'button',
                        text: 'Reply',
                        align: 'right',
                        itemId: 'btnReplyToEnquiry',
                        hidden: true                       
                    },
                    {
                        xtype: 'button',
                        text: 'Send',
                        align: 'right',
                        itemId: 'btnSendEnquiryReply',
                        ui: 'action',
                        hidden: true                          
                    }
            ]
        },        
     
        items: [
            {
                layout: 'vbox',
                title: 'My Inbox',
                items: [
                    {
                        xtype: 'container',
                        items: [
                            {
                                xtype: 'container',
                                margin: "10 20 0",
                                //padding: "10 20 0",
                                cls: 'lineBelow',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'You have sent'
                                    }, 
                                    {
                                        xtype: 'label',
                                        html: '0 enquiry',
                                        //docked: 'right',
                                        right: 0,
                                        itemId: 'lblEnquiryCount'
                                    }
                               ]
                           }, 
                           {
                                xtype: 'container',
                                margin: "10 20 0",
                                //padding: "10 20 0",
                                cls: 'lineBelow',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'You have'
                                    }, 
                                    {
                                        xtype: 'label',
                                        //docked: 'right',
                                        right: 0,
                                        itemId: 'lblEnquiryResponseCount'
                                    }
                                ]
                         }
                    ]
                }, 
                {
                    xtype: 'inboxlistview',                    
                    flex: 1                    
                }
            ]
            }
        ]
    }
});