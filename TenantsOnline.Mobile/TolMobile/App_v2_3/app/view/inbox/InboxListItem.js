Ext.define('TolMobile.view.inbox.InboxListItem', {
    extend: 'Ext.dataview.component.DataItem',
    alias: 'widget.inboxlistitem',

    requires: [

    ],

    config: {

        //cls: 'x-list-item-body',
        padding: "0.65em 0.8em",
		
        image: {        
            width: 29,
            height: 20,
            padding: "0 25" // why is this needed?
        },

        description: {
            cls: 'inboxItemText',
            flex: 1,
            padding: "0 5"
        },

        layout: {
            type: 'hbox',            
            align: 'left'
        }
    },

    applyImage: function (config) {
        var image = Ext.factory(config, Ext.Img, this.getImage());
        return image;
    },

    updateImage: function (newImage, oldImage) {
        if (newImage) {
            this.add(newImage);
        }

        if (oldImage) {
            this.remove(oldImage);
        }
    },

    applyDescription: function (config) {
        console.log("applyDesc");
        var desc = Ext.factory(config, Ext.Component, this.getDescription());
        return desc;
    },

    updateDescription: function (newDesc, oldDesc) {
        console.log("updateDesc");
        if (newDesc) {
            this.add(newDesc);
        }

        if (oldDesc) {
            this.remove(oldDesc);
        }        
    },

    applyRecord: function (newRecord) {
        console.log("InboxListItem::applyRecord");

        // set image
        var imageUrl;
        switch (newRecord.get("ImageKeyFlag")) {
            case 0:
                imageUrl = 'resources/css/images/inbox/im_sent.gif'
                break;

            case 1:
                imageUrl = 'resources/css/images/inbox/im_read.gif'
                break;

            case 2:
                imageUrl = 'resources/css/images/inbox/im_responded.gif'
                break;
        }

        this.getImage().setSrc(imageUrl);

        var dt = Ext.Date.format(newRecord.get("CreationDate"), 'd M Y');
        
        var desc = dt + ": " + (newRecord.get("Description") || newRecord.get("Nature"));
        this.getDescription().setHtml(desc);

        return newRecord;
    },

    updateRecord: function (newRecord) {
        console.log("InboxListItem::updateRecord");
    }
});