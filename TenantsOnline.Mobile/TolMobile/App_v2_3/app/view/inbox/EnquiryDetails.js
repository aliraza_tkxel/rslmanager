Ext.define('TolMobile.view.inbox.EnquiryDetails', {
    extend: 'Ext.Container',
    alias: 'widget.enquirydetailsview',

    requires: [
        'TolMobile.view.inbox.EnquiryResponseList'
    ],

    config: {
        title: 'Enquiry Details',
        layout: {
            type: 'vbox'
        },
		scrollable: true,
        items: [
            {
                xtype: 'container',                
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'container',
                        margin: "10 20 0",
                        cls: 'lineBelow',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Enq No.'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                docked: 'right',
                                itemId: 'lblEnquiryNo'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        margin: "10 20 0",
                        cls: 'lineBelow',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Date:'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                docked: 'right',
                                itemId: 'lblEnquiryDate'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        margin: "10 20 0",
                        cls: 'lineBelow',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Description:'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                itemId: 'lblEnquiryDesc'
                            }
                        ]
                    }
                // grid header was here...
                ]
            },
            {
                xtype: 'container',                
                margin: "10 0 0",
                layout: {
                    type: 'vbox'
                },
                items: [
                     {
                         xtype: 'container',
                         margin: "10 20 0",
                         cls: 'lineBelow',
                         layout: {
                             type: 'vbox'
                         },
                         items: [
                            {
                                xtype: 'label',
                                html: 'Enquiry History'
                            },
                            {
                                xtype: 'container',
                                //margin: 10,                                
                                layout: {
                                    type: 'hbox'
                                },
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Date:',
                                        flex: 1
                                    },
                                    {
                                        xtype: 'label',
                                        html: 'From:',
                                        flex: 1
                                    }
                                ]
                            }
                           ]
                     },
                    {
                        flex: 1,
                        xtype: 'enquiryresponselistview',
                        itemId: 'mainResponseListView'
                    },
                   {
                       itemId: 'subResponseListViewContainer',
                       xtype: 'container',
                       flex: 1,
                       hidden: true,
                       layout: {
                           type: 'vbox'
                       },
                       items: [                          
                            {
                                xtype: 'enquiryresponselistview',
                                height: "60px",
                                store: null
                            },
                            {
                                xtype: 'label',
                                html: '',
                                margin: "10 20 0",
                                minHeight:"100px",
                                itemId: 'lblExtendedDescription'
                            },
                            {
                                xtype: 'container',
                                Margin: 5,
                                layout: {
                                    type: 'hbox'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        flex: 2,
                                        pack: 'center',                                        
                                        html: 'Back to enquiry history',
                                        ui: 'back',
                                        itemId: 'btnBackToEnquiryHistory'                                      
                                    },
                                    {
                                        xtype: 'spacer'
                                    }
                                ]
                            }
                        ]
                   }
                ]
            }
        ],

        enquiryRecord: null
    },


    applyEnquiryRecord: function (record) {
        var lblEnquiryNo = this.down("#lblEnquiryNo");
        var lblEnquiryDesc = this.down("#lblEnquiryDesc");
        var lblEnquiryDate = this.down("#lblEnquiryDate");

        // set min data for enquiry
        var enqNo = record.get("EnquiryLogID")
        lblEnquiryNo.setHtml(enqNo);
        lblEnquiryDesc.setHtml(record.get("Description") || "none");
        var dt = Ext.Date.format(record.get("CreationDate"), 'd/n/Y');
        lblEnquiryDate.setHtml(dt);
        
        return record;
    }

});