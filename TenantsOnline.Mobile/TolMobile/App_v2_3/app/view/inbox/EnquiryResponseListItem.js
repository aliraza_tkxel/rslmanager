Ext.define('TolMobile.view.inbox.EnquiryResponseListItem', {
    extend: 'Ext.dataview.component.DataItem',
    alias: 'widget.enquiryresponselistitem',

    requires: [

    ],

    config: {

        cls: 'x-list-item-body',

        enquiryDate: {
            cls: 'inboxItemText',
            flex: 1,
            padding: "0 5"
        },

        enquiryFrom: {
            cls: 'inboxItemText',
            flex: 1,
            padding: "0 5"
        },

        layout: {
            type: 'hbox',
            align: 'left'
        }
    },

    applyEnquiryDate: function (config) {
        console.log("applyEnquiryDate");
        var enqDate = Ext.factory(config, Ext.Component, this.getEnquiryDate());
        return enqDate;
    },

    updateEnquiryDate: function (newDate, oldDate) {
        console.log("updateEnquiryDate");
        if (newDate) {
            this.add(newDate);
        }

        if (oldDate) {
            this.remove(oldDate);
        }
    },

    applyEnquiryFrom: function (config) {
        console.log("applyEnquiryFrom");
        var enqFrom = Ext.factory(config, Ext.Component, this.getEnquiryFrom());
        return enqFrom;
    },

    updateEnquiryFrom: function (newItem, oldItem) {
        console.log("updateEnquiryFrom");
        if (newItem) {
            this.add(newItem);
        }

        if (oldItem) {
            this.remove(oldItem);
        }
    },

    applyRecord: function (newRecord) {
        console.log("EnquiryResponseListItem::applyRecord");
        
        if (newRecord) {            
            var dt = Ext.Date.format(newRecord.get("LastActionDate"), 'd/n/Y');
            this.getEnquiryDate().setHtml(dt);
            this.getEnquiryFrom().setHtml(newRecord.get("LastActionUser") !== -1 ? "Me" : "Broadlands");                        

            return newRecord;
        }
    },

    updateRecord: function (newRecord) {
        console.log("EnquiryResponseListItem::updateRecord");
    }
});