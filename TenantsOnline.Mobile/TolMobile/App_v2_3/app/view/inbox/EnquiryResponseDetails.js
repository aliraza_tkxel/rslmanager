Ext.define('TolMobile.view.inbox.EnquiryResponseDetails', {
    extend: 'Ext.Container',
    alias: 'widget.enquiryresponsedetailsview',

    config: {
        title: 'Details',
        navigationBar: {
            ui: 'sencha',
            items: [

            ]
        },
        items: [
            {
                xtype: 'container',
                items: [
                    {
                        xtype: 'container',
                        margin: 10,
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Enq No.'
                            },
                            {
                                xtype: 'label',
                                html: '0 enquiry',
                                right: 0
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        margin: 10,
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Date'
                            },
                            {
                                xtype: 'label',
                                html: '0 enquiry',
                                right: 0
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        margin: 10,
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Description:'
                            },
                            {
                                xtype: 'label',
                                html: 'Description --'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        html: '',
                        margin: 10,
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Enquiry History'
                            }
                        ]
                    }
                ]
            }
        ]
    }

});