Ext.define('TolMobile.view.inbox.EnquiryReply', {
    extend: 'Ext.Container',
    alias: 'widget.enquiryreplyview',

    config: {
        layout: {
            type: 'fit'
        },
        navigationBar: {
            items: [
                {
                    xtype: 'button',
                    itemId: 'btnSendEnquiryReply',
                    text: 'Send',
                    align: 'right'
                }
            ]
        },
        items: [
            {
                xtype: 'formpanel',
                items: [
                     {
                         xtype: 'textareafield',
                         label: '',
                         labelAlign: 'top',
                         placeHolder: 'Enter response here',
                         maxRows: Ext.os.is.Phone ? 12 : 20,
                         itemId: 'txtEnquiryResponse'
                     }                   
                ]
            }
        ],

        enquiryId: null
    }

});