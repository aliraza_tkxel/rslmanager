Ext.define('TolMobile.view.inbox.InboxList', {
    extend: 'Ext.dataview.DataView',
    alias: 'widget.inboxlistview',
  
    requires: [
        'TolMobile.view.inbox.InboxListItem'
    ],

    config: {
               
        useComponents: true,
        singleSelect: true,
        cls: '',
        store: 'theInboxEnquiryStore',      
        defaultType: 'inboxlistitem'
    }
});