Ext.define('TolMobile.view.About', {
    extend: 'Ext.Panel',
    alias: 'widget.aboutview',

    config: {
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'image',
                flex: 0.5,
                height: 201,
                margin: '20px',
                width: '',
                src: 'http://www.broadlandhousing.org/images/site_images/broadland_housing_logo.jpg'
            }
        ]
    }

});