Ext.define('TolMobile.view.offices.OfficeView', {
    extend: 'Ext.Panel',
    alias: 'widget.officeview',

    config: {
        margin: 10,
        scrollable: true,
        items: [           
            {                
                xtype: 'label',
                itemId: 'lblAddress',
                cls: 'wideLineSpacing'
            },
            {
                xtype: 'container',
                margin: '10 0',
                layout: {
                    align: 'end',
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'label',
                        flex: 3
                    },
                    {
                        xtype: 'button',
                        flex: 2,
                        text: 'View on Map',
                        itemId: 'btnViewMap'
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '10 0',
                cls: 'linesAboveBelow',
                padding: '5 0',
                layout: {
                    pack: 'end',
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'label',
                        flex: 3,
                        itemId: 'lblTelephone'
                    },
                    {
                        xtype: 'button',
                        flex: 2,
                        text: 'Call',
                        hidden: !Ext.os.is.Phone,
                        itemId: 'btnCallOffice'
                    }
                ]
            },
           
            {
                xtype: 'label',
                cls: 'wideLineSpacing smallFont',
                html: 'Email Customer Services at  enq@broadlandgroup.org.<br/> We will respond to emails within 5 working days',
                margin: '10 0'
            }
        ]
    },

    setOfficeAddress: function (value) {
        var lbl = this.down("#lblAddress");
        lbl.setHtml(value);
    },

    setTelephone: function (value) {
        var lbl = this.down("#lblTelephone");
        lbl.setHtml("Tel: <a href='Tel: " + value +"'>"+value+"</a>");
    }


});