Ext.define('TolMobile.view.offices.OfficeIndex', {
    extend: 'Ext.navigation.View',
    alias: 'widget.officeindexview',

    requires: [
        'Ext.navigation.View'
    ],

    config: {
        rootCardIdx: 2,        
        items: [
            {
                id: 'officelist',
                xtype: 'list',
                title: 'Offices',
                itemTpl: [
                    '<div>{Title}</div>'
                ],
                onItemDisclosure: true,
                store: 'theOfficesStore'               
            }
        ]                
    }
});