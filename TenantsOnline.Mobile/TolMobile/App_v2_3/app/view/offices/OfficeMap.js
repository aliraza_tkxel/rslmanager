Ext.define('TolMobile.view.offices.OfficeMap', {
    extend: 'Ext.Panel',
    alias: 'widget.officemapview',

    requires: [
        'Ext.Map'
    ],

    config: {
        layout: 'fit',
        items: [
            {
                xtype: 'map',
                listeners: {
					activate: function(me, newActiveItem, oldActiveItem, eOpts){
						console.log("activate fired");
												
					},
					
                    maprender: function () {
						console.log("maprender fired");						
						var gMap = this.getMap();
                        this.fireEvent('googleMapRender', gMap);						
                    }
                }
            }
        ],

        officeRecord: null
    }

});