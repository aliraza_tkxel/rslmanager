Ext.define('TolMobile.view.news.NewsView', {
    extend: 'Ext.Container',
    alias: 'widget.newsview',

    config: {
        margin: 10,
        scrollable: true,
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'image',                                
                                url: 'app/images/',
                                height: 100,
                                width: Ext.os.is.Phone ? 75 : 100,
                                itemId: 'imgNews'                                
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        flex: 2,
                        items: [
                            {
                                xtype: 'label',
                                itemId: 'lblHeadline'                                
                            },
                            {
                                xtype: 'label',
                                cls: [
                                    'wideLineSpacing'
                                ],
                                html: '10 December 2012',
                                itemId: 'lblDateCreated',
                                style: 'font-size:.9em;'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                margin:10,
                items: [
                    {
                        xtype: 'label',                        
                        style: 'font-size: 0.8em',
                        itemId: 'lblNewsContent'
                    }
                ]
            }
        ],

        // data
        newsRecord: null
    },

    // funcs
    updateNewsRecord: function (rec) {
        var headline = this.down("#lblHeadline");
        var datecreated = this.down("#lblDateCreated");
        var newsContent = this.down("#lblNewsContent");
        var img = this.down("#imgNews");

        headline.setHtml(rec.get("Headline"));
        datecreated.setHtml(rec.get("DateCreated"));
        newsContent.setHtml(rec.get("NewsContent"));
        img.setSrc(rec.get("ImagePath"));
    }
    

});
