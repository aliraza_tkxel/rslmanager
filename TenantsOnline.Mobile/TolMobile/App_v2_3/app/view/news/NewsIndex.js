Ext.define('TolMobile.view.news.NewsIndex', {
    extend: 'Ext.navigation.View',
    alias: 'widget.newsindexview',

    requires: [
        'Ext.navigation.View'
    ],

    config: {
        title: 'News',
        
        items: [
                    {
                        title: 'News & Events',
                        xtype: 'container',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                             {
                                 xtype: 'label',
                                 margin: '10 10',
                                 html: 'Latest News:'
                             },
                             {
                                xtype: 'container',
                                flex: 1,
                                margin: '10 0',
                                layout: {
                                    type: 'hbox'
                                },
                                items: [                                    
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: {
                                            type: 'hbox',
                                            pack: 'center'
                                        },
                                        items: [
                                            {
                                                xtype: 'image',
                                                url: 'app/images/',
                                                height: 100,
                                                width: Ext.os.is.Phone ? 75 : 100,
                                                itemId: 'imgNews'                                                
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 2,
                                        items: [
                                            {
                                                xtype: 'label',                                                
                                                itemId: 'lblHeadline'
                                            },
                                            {
                                                xtype: 'label',
                                                cls: [
                                                    'wideLineSpacing'
                                                ],
                                                itemId: 'lblDateCreated',
                                                style: 'font-size:.9em;'
                                            },
                                            {
                                                xtype: 'label',                                                
                                                style: 'font-size: 0.8em',
                                                itemId: 'lblNewsContent',
                                                margin: '0 10'
                                            },
                                            {
                                                xtype: 'button',
                                                margin: '10 10',                                                
                                                text: 'Read More',
                                                itemId: 'btnReadMore'
                                            }
                                        ]
                                    }
                        ]
                    },
                    {
                        xtype: 'container',
                        margin: '10 10',
                        flex: 1,
                        layout: {
                            type: 'vbox'
                        },                        
                        items: [
                            {
                                xtype: 'label',
                                html: 'Other News:'
                            },
                            {
                                flex: 1,                               
                                xtype: 'list',
                                onItemDisclosure: true, 
                                margin: "0 10",
                                itemTpl: [
                                    '<div>{Headline}</div>'
                                ],
                                store: 'theNewsStore'                                
                            }       
                        ]
                    }                   
                ]
            }         
        ]      
    }
});