Ext.define('TolMobile.view.ReportIt', {
    extend: 'Ext.navigation.View',
    alias: 'widget.reportitview',

    config: {        
        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                title: 'Report It'
            }
        ]
    }

});