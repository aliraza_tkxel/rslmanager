﻿Ext.define('TolMobile.view.reporting.ExternalFaultImages', {
    extend: 'Ext.DataView',
    alias: 'widget.externalfaultimagesview',

    requires: [
        'TolMobile.view.reporting.FaultListItem'
    ],

    config: {
        fullscreen: true,
        useComponents: true,
        defaultType: 'faultlistitem',
        store: {
            fields: [
                "name",
                "image"
            ],
            data: [
               { name: 'Path/Fence', image: 'images/faults/path_fence.jpg' },
               { name: 'Windows', image: 'images/faults/windows.jpg' },
               { name: 'Walls', image: 'images/faults/walls.jpg' },
               { name: 'Roof/Chimney', image: 'images/faults/roof_chimney.jpg' }
            ]
        }
    }

});