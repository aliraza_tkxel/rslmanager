﻿Ext.define('TolMobile.view.reporting.TenantDetail', {
    extend: 'Ext.Container',
    alias: 'widget.tenantdetailview',

    requires: [
        'Ext.Label'
    ],

    config: {        
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'label',
                        flex: 2,
                        html: 'Tenant:'
                    },
                    {
                        xtype: 'label',
                        flex: 3,
                        itemId: 'lblTenantName'
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'label',
                        flex: 2,
                        html: 'Tenant Ref:'
                    },
                    {
                        xtype: 'label',
                        flex: 3,
                        itemId: 'lblTenantRef'
                    }
                ]
            },
            {
                 hidden: true,
                 xtype: 'container',
                 margin: '10 0',
                 layout: {
                     type: 'hbox'
                 },
                 itemId: "cntTenantAddress",
                 items: [
                            {
                                xtype: 'label',
                                flex: 2,
                                html: 'Address:'
                            },
                            {
                                xtype: 'label',
                                flex: 3,
                                html: '5 Chatfield',
                                itemId: 'lblTenantAddress'
                            }
                        ]
             }
        ]
    },    

    setTenantName: function (value) {
        this.down("#lblTenantName").setHtml(value);
    },

    setTenantRef: function (value) {
        this.down("#lblTenantRef").setHtml(value);
    },

    setTenantAddress: function (value) {
        var cnt = this.down("#cntTenantAddress");
        var lbl = this.down("#lblTenantAddress");
        cnt.setHidden(false);
        lbl.setHtml(value);
    }

});