﻿Ext.define('TolMobile.view.reporting.FaultConfirmation', {
    extend: 'Ext.Container',

    config: {
        items: [
            {
                xtype: 'label',
                html: 'Thank you, your fault has been reported.  A member of our support services will be in touch to arrange an appointment',
                margin: 30
            },
            {
                xtype: 'label',
                html: 'Check your fault log for updates on the progress of your account.',
                margin: 30
            }
        ]
    }

});