﻿Ext.define('TolMobile.view.reporting.EndTenancy', {
    extend: 'Ext.Container',
    alias: 'widget.endtenancyview',

    requires: [
        'Ext.field.DatePicker',
        'TolMobile.view.reporting.TenantDetail'
    ],

    config: {
        scrollable: true,
        title: 'End My Tenancy',
        items: [          
            {
                xtype: 'container',
                margin: 10,
                layout: {
                    type: 'vbox'
                },                
                items: [
                    {
                        xtype: 'tenantdetailview'
                    },
                   
                    {
                        xtype: 'selectfield',                        
                        html: '',
                        label: 'Reason',
                        labelAlign: 'top',
                        store: 'theTerminationReasonStore',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        name: 'terminationReason',
                        itemId: 'ddlTerminationReason',
                        autoSelect: false
                    },
                    {
                        xtype: 'datepickerfield',
                        label: 'I\'d like to end my tenancy on:',
                        labelAlign: 'top',
                        placeHolder: 'dd/mm/yyyy',
                        value: new Date(),
                        id: 'endTenancyDate',
                        dateFormat: 'd/m/Y',
                        picker: {                        
                            slotOrder: ["day", "month", "year"]                            
                        }
                    },                
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'spacer'
                            },
                            {
                                xtype: 'button',
                                margin: 10,
                                padding: 10,
                                flex: 0.5,
                                text: 'Send',
                                action:'send'
                            }
                        ]
                    }
                ]
            }
        ]
    }

});