﻿Ext.define('TolMobile.view.reporting.InternalFaultImages', {
    extend: 'Ext.dataview.DataView',
    alias: 'widget.internalfaultimagesview',

    requires: [
        'TolMobile.view.reporting.FaultListItem'
    ],

    config: {
        fullscreen: true,
        layout: {
            flex: 1
        },
        useComponents: true,
        defaultType: 'faultlistitem',
        store: {
            fields: [
                "name",
                "image"
            ],
            data: [
               { name: 'Basement', image: 'images/faults/Basement.jpg' },
               { name: 'Bathroom/WC', image: 'images/faults/Bathroom_WC.jpg' },
               { name: 'Bedroom', image: 'images/faults/Bedroom.jpg' },
               { name: 'Dining Room', image: 'images/faults/dining_room.jpg' },
               { name: 'Hall/Stairs/Landing', image: 'images/faults/hall_stairs_landing.jpg' },
               { name: 'Kitchen', image: 'images/faults/Kitchen.jpg' },
               { name: 'Living Room', image: 'images/faults/Living_Room.jpg' },
               { name: 'Loft', image: 'images/faults/Loft.jpg' }
            ]
        }
    }

});