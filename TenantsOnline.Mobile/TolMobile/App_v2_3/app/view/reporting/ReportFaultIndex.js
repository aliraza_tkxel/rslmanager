﻿Ext.define('TolMobile.view.reporting.ReportFaultIndex', {
    //extend: 'Ext.navigation.View',
    extend: 'Ext.Container',
    alias: 'widget.reportfaultindexview',

    requires: [
        'TolMobile.view.reporting.ReportFault'
    ],

    config: {
        title: 'Report a Fault',
        layout: 'fit',
        items: [
            {             
                id: 'faultindexlist',
                xtype: 'list',
                title: '',
                itemTpl: [
                    '<div>{Title}</div>'
                ],                
                onItemDisclosure: true,
                store: {
                    autoLoad: true,
                    fields: [
                        "Title",
                        "Xid"
                    ],
                    data: [                    
                        { Title: 'Report a Fault', Xid: 'internalfaultimagesview' },
                        { Title: 'My Fault Log', Xid: 'faultlogview' }                    
                    ]
                }             
            }
        ]
    }
});