﻿Ext.define('TolMobile.view.reporting.ReportASB', {
    extend: 'Ext.Container',
    alias: 'widget.reportasbview',

    requires: [
        'Ext.field.Select',
        'Ext.field.TextArea',
        'TolMobile.view.reporting.TenantDetail'
    ],

    config: {
        scrollable: true,
        title: 'Report ASB',
        items: [            
            {                
                xtype: 'container',
                margin: 10,
                items: [
                    {
                        xtype: 'tenantdetailview'
                    },
                    {
                        xtype: 'container',
                        margin: '20 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'selectfield',
                                flex: 1,
                                html: '',
                                label: 'Select category:',
                                labelAlign: 'top',
                                store: 'theASBCategoriesStore',
                                displayField: 'ItemName',
                                valueField: 'Id',
                                name: 'asbCategory',
                                itemId: 'ddlAsbType',
                                autoSelect: false
                            }
                        ]
                    },
                    {
                        xtype: 'textareafield',
                        label: 'Enter your problem',
                        labelAlign: 'top',
                        itemId: 'txtAdditionalInfo'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'spacer'
                            },
                            {
                                xtype: 'button',
                                flex: 0.5,
                                margin: 10,
                                padding: 10,
                                text: 'Send',
                                action: 'send'
                            }
                        ]
                    }
                ]
            }
        ]
    }

});