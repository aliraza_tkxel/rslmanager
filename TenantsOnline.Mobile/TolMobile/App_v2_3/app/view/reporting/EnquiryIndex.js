Ext.define('TolMobile.view.reporting.EnquiryIndex', {
    extend: 'Ext.navigation.View',

    alias: 'widget.enquiryindexview',

    requires: [
        
    ],
   

    config: {                        
        items: [            
            {
                id: 'enquirylist',
                xtype: 'list',                
                title: 'Report It',
                itemTpl: [
                    '<div>{DisplayName}</div>'
                ],                
                onItemDisclosure: true,
                store: 'theEnquiryViewStore'             
            }
        ]
    }
});