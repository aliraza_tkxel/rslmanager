﻿Ext.define('TolMobile.view.reporting.AddNeglectPhoto', {
    extend: 'Ext.Container',
    alias: 'widget.addneglectphotoview',

    requires: [
        'Ext.ux.Fileup'
    ],

    config: {
        title: 'Add Fault Photo',
        
        items: [
            {
                margin: 10,
                itemId: 'fileBtn',
                xtype: 'fileupload',
                autoUpload: false,
                
                states: {
                    browse: {
                        text: 'Select photo...'
                    },
                    ready: {
                        text: 'Send Report with Photo'
                    },
                    uploading: {
                        text: 'Loading',
                        loading: true// Enable loading spinner on button
                    }
                }
            }
        ]
    }

});