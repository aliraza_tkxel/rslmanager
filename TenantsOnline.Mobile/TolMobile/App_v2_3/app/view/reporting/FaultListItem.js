﻿Ext.define('TolMobile.view.reporting.FaultListItem', {
    extend: 'Ext.dataview.component.DataItem',
    xtype: 'faultlistitem',

    requires: [
        'Ext.Img'
    ],

    config: {
        image: true,

        name: {
            cls: 'x-name',
            flex: 1
        },      

        layout: {
            type: 'hbox',
            align: 'center'
        },

        dataMap: {
            getImage: {
                setSrc: 'image'
            },
            getName: {
                setHtml: 'name'
            }            
        }
    },

    applyImage: function (config) {
        return Ext.factory(config, Ext.Img, this.getImage());
    },

    updateImage: function (newImage, oldImage) {
        if (newImage) {
            this.add(newImage);
        }

        if (oldImage) {
            this.remove(oldImage);
        }
    }

});