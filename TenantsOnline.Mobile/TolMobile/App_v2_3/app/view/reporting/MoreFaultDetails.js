﻿Ext.define('TolMobile.view.reporting.MoreFaultDetails', {
    extend: 'Ext.Container',

    config: {
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'formpanel',
                items: [
                    {
                        xtype: 'spinnerfield',
                        label: 'Quantity'
                    },
                    {
                        xtype: 'selectfield',
                        label: 'How long have you had this problem?',
                        labelAlign: 'top'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: 'Fault Recurring Problem?',
                        labelWidth: '80%'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: 'Fault in communal area?',
                        labelWidth: '80%'
                    },
                    {
                        xtype: 'textareafield',
                        label: 'Notes you feel are relevant to the job',
                        labelAlign: 'top'
                    }
                ]
            }
        ]
    }

});