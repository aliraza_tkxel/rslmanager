﻿Ext.define('TolMobile.view.reporting.ReportAbandonedType', {
    extend: 'Ext.Container',
    alias: 'widget.reportabandonedtypeview',

    requires: [
        'Ext.field.Select',
        'Ext.field.TextArea',
        'TolMobile.view.reporting.TenantDetail',
        'TolMobile.view.reporting.AddNeglectPhoto'
    ],

    config: {
        scrollable: true,
        title: 'Vandalism and Neglect',
        items: [
            {
                xtype: 'container',
                margin: 10,
                items: [
                    {
                        xtype: 'tenantdetailview'
                    },
                    {
                        xtype: 'container',
                        margin: '20 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'selectfield',
                                flex: 1,
                                html: '',
                                label: 'I wish to report:',
                                labelAlign: 'top',
                                store: 'theAbandonedTypesStore',
                                displayField: 'ItemName',
                                valueField: 'Id',
                                name: 'abandonedType',
                                itemId: 'ddlSelectType',
                                autoSelect: false
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        items: [
                            {
                                xtype: 'textareafield',
                                label: 'Describe the location',
                                labelAlign: 'top',
                                itemId: 'txtLocation'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        items: [
                            {
                                xtype: 'textareafield',
                                label: 'Other information (optional)',
                                labelAlign: 'top',
                                itemId: 'txtAdditionalInfo'
                            }
                        ]
                    },
                    {
                        xtype: 'checkboxfield',
                        itemId: 'chkAddNeglectPhoto',
                        label: 'Add photo',
                        labelWidth: "50%" 
                    },   
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'spacer'
                            },                                                                                                      
                            {
                                xtype: 'button',
                                flex: 0.5,
                                margin: 10,
                                padding: 10,
                                text: 'Send',
                                action: 'send'                                
                            }
                        ]
                    }
                ]
            }
        ]
    }
});