﻿Ext.define('TolMobile.view.reporting.RequestParkingType', {
    extend: 'Ext.Container',
    alias: 'widget.requestparkingtypeview',

    requires: [
        'Ext.field.Select',
        'Ext.field.TextArea',
        'TolMobile.view.reporting.TenantDetail'
    ],

    config: {
        supportsAddress: true, 
        title: 'Request Garage',
        scrollable: true,
        items: [            
            {
                xtype: 'container',
                margin: 10,
                items: [
                    {
                        xtype: 'tenantdetailview'
                    },
                   {
                       xtype: 'selectfield',
                       label: 'I\'d like a',
                       labelAlign: 'top',
                       store: 'theParkingTypesStore',
                       displayField: 'ItemName',
                       valueField: 'Id',
                       name: 'parkingType',
                       itemId: 'ddlSelectType',
                       autoSelect: false
                   },
                    {
                        xtype: 'textareafield',
                        label: 'Additional information',
                        labelAlign: 'top',
                        itemId: 'txtAdditionalInfo'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'spacer'
                            },
                            {
                                xtype: 'button',
                                flex: 0.5,
                                margin: 10,
                                padding: 10,
                                text: 'Send',
                                action: 'send'
                            }
                        ]
                    }
                ]
            }
        ]
    }
});