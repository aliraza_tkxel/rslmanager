﻿Ext.define('TolMobile.view.reporting.ReportComplaint', {
    extend: 'Ext.Container',
    alias: 'widget.reportcomplaintview',

    requires: [
        'Ext.field.Select',
        'Ext.field.TextArea',
        'TolMobile.view.reporting.TenantDetail'
    ],

    config: {
        scrollable: true,
        title: 'Report Complaint',
        items: [            
            {
                xtype: 'container',
                margin: 10,
                items: [
                    {
                        xtype: 'tenantdetailview'
                    },
                    {
                        xtype: 'container',
                        margin: '20 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'selectfield',
                                flex: 1,
                                html: '',
                                label: 'I wish to complain about',
                                labelAlign: 'top',
                                store: 'theComplaintTypesStore',
                                displayField: 'ItemName',
                                valueField: 'Id',
                                name: 'complaintType',
                                itemId: 'ddlComplaintType',
                                autoSelect: false
                            }
                        ]
                    },                   
                    {
                        xtype: 'textareafield',
                        label: 'My complaint is',
                        labelAlign: 'top',
                        itemId: 'txtAdditionalInfo'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'spacer'
                            },
                            {
                                xtype: 'button',
                                flex: 0.5,
                                margin: 10,
                                padding: 10,
                                text: 'Send',
                                action: 'send'
                            }
                        ]
                    }
                ]
            }
        ]
    }

});