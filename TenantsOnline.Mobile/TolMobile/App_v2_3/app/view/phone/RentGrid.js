﻿Ext.define('TolMobile.view.phone.RentGrid', {
    extend: 'Ext.ux.touch.grid.List',    

    requires: [               
        'TolMobile.store.rent.Rent'
    ],

    config: {
        title: 'Rent',
        store: 'theRentAccountStore',        
        margin: "10",        
        columns: [
            {
                header: 'Date',
                dataIndex: 'TransactionDate',
                width: '30%',
                style: 'padding:0px; text-align: center;font-size:0.7em;'               
				
            },
            {
                header: 'Debit',
                dataIndex: 'Debit',
                width: '20%',
                style: 'padding:0px; text-align: center; font-size:0.7em;'
            },
            {
                header: 'Credit',
                dataIndex: 'Credit',
                width: '20%',
                style: 'padding:0px; text-align: center; font-size:0.7em;'
            },
            {
                header: 'Balance',
                dataIndex: 'Balance',
                width: '30%',
                style: 'padding:0px; text-align: center; font-size:0.7em;'
            }
        ]
    }

});