Ext.define('TolMobile.view.phone.MyRentStatement', {
    extend: 'Ext.Container',
    alias: 'widget.myrentstatementview',

    requires: [
        'Ext.navigation.View'
    ],

    config: {
		title: 'Statement',		
		flex: 1,
		layout: 'fit',
		
		// weird overlapping problem on phones
		fullscreen: true,
        items: [
            {
				xtype: 'rentgridview'
			}	
        ]                
    }	
});