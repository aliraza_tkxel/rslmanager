Ext.define("TolMobile.view.phone.Welcome", {
    extend: 'Ext.Panel',
    
    requires: [
        'Ext.TitleBar'
    ],

  
    config: {
        layout: {
            type: 'vbox'
        },
		items:  {                     
			id: 'welcomeview',       
			xtype: 'container',                                
			centered:true,
			items: [
				{
					xtype: 'image',
					height: 90,
					margin: 10,
					src: 'resources/css/images/logo200.png',
					mode: 'image'
				},                                 
				{
					 xtype: 'label',					                  
					 html: 'Welcome to',
					 styleHtmlCls: 'textcentered',
					 styleHtmlContent: true					 
				},
				{
					 xtype: 'label',					                  
					 html: '<strong>Broadland Housing Tenants Online</strong>',
					 styleHtmlCls: 'textcentered',
					 styleHtmlContent: true
				},				
				{
					xtype: 'container',
					margin: "10 5",
					defaults: {
						flex: 1,
						margin: 3
					},
					layout: {
						type: 'hbox'
					},
					items: [
						{
							xtype: 'button',
							text: 'Register',
							action:'register',
							padding: 10							
						},
						{
							xtype: 'button',
							id: 'btnSignInNow',
							text: 'Sign In Now',
							action:'signin',
							ui: 'action',
							padding: 10							
						}
					]
				}                  
			]
        } 		
    }
});
