Ext.define('TolMobile.view.phone.HomeCarousel', {
    extend: 'Ext.carousel.Carousel',
	alias: 'widget.homecarouselview',
		
    requires: ['Ext.Label', 'Ext.Img'],

	defaults: {
        styleHtmlContent: true
    },

	config : {	
		id: 'homecarouselview',
		items: [
			{
                xtype: 'toolbar',
                docked: 'top',
                title: 'Home',
                layout: {
                    pack: 'end',
                    type: 'hbox'
                },
                items: [
                        {
                            xtype: 'button',
                            text: 'Logout',
                            action: 'Logout'
                        }
                ]
            },
			{
                xtype: 'container',
                flex: 1,
                margin: '',
                defaults: {
                    flex: 1
                },
                layout: {
                    type: 'vbox'
                },
                items: [
                   {
                       xtype: 'container',
                       defaults: {
                           flex: 1,
                           height: 65
                       },
                       layout: {
                           align: 'end',
                           type: 'hbox'
                       },
                       items: [
                            {
                                xtype: 'image',
                                src: 'resources/css/images/cogs.png',
                                action: 'ShowMyDetails'
                            },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/speech.png',
                                action: 'ReportIt'
                            },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/house.png',
                                action: 'MyRent'
                            }
                        ]
                   },
                    {
                        xtype: 'container',
                        defaults: {
                            flex: 0.5
                        },
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'My Details',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'Report It',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'Pay Rent',
                                style: 'text-align:center'
                            }
                        ]
                    }
                ]
            },
             {
                 xtype: 'container',
                 flex: 1,
                 margin: '',
                 defaults: {
                     flex: 1
                 },
                 layout: {
                     type: 'vbox'
                 },
                 items: [
                   {
                       xtype: 'container',
                       defaults: {
                           flex: 1,
                           height: 65
                       },
                       layout: {
                           align: 'end',
                           type: 'hbox'
                       },
                       items: [
                            {
                                xtype: 'image',
                                src: 'resources/css/images/offices.png',
                                action: 'Offices'
                            },
                            {
                                xtype: 'image',
                                src: 'resources/css/images/news.png',
                                action: 'News'
                            },
                           {
                               xtype: 'image',
                               src: 'resources/css/images/benefitCuts.png',
                               action: 'BenefitCuts'
                           }
                        ]
                   },
                    {
                        xtype: 'container',
                        defaults: {
                            flex: 0.5
                        },
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Offices',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'News',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: 'Benefit Cuts',
                                style: 'text-align:center'
                            }
                        ]
                    }
                ]
             },
             {
                 xtype: 'container',
                 flex: 1,
                 margin: '',
                 defaults: {
                     flex: 1
                 },
                 layout: {
                     type: 'vbox'
                 },
                 items: [
                   {
                       xtype: 'container',
                       defaults: {
                           flex: 1,
                           height: 65
                       },
                       layout: {
                           align: 'end',
                           type: 'hbox'
                       },
                       items: [
                            {
                                xtype: 'image',
                                src: 'resources/css/images/inbox.png',
                                action: 'MyInbox'
                            },
                            {
                                xtype: 'image',
                                src: '',
                                action: ''
                            },
                           {
                               xtype: 'image',
                               src: '',
                               action: ''
                           }
                        ]
                   },
                    {
                        xtype: 'container',
                        defaults: {
                            flex: 0.5
                        },
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'My Inbox',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                style: 'text-align:center'
                            },
                            {
                                xtype: 'label',
                                html: '',
                                style: 'text-align:center'
                            }
                        ]
                    }
                ]
            }			
		]
	},

    // methods
    setWelcomeHtml: function (html) {
        //var lbl = Ext.getCmp("WelcomeText");
        //lbl.setHtml(html);
    },
	
	// Events
	initialize: function (arguments) {
        console.log('HomeCarousel - initialize');        
        Ext.Viewport.on('orientationchange', 'handleOrientationChange', this, {buffer: 50 });
        this.callParent(arguments);
    },
	
    handleOrientationChange: function(){
        console.log('HomeCarousel - handleOrientationChange');
        		
    }

});