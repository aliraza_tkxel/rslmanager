Ext.define('TolMobile.view.Debug', {
    extend: 'Ext.navigation.View',
    alias: 'widget.debugview',

    requires: [
        'Ext.navigation.View'
    ],

    config: {           
        items: [
            {
                id: 'debuglist',
                xtype: 'list',
                title: 'Debug',
                itemTpl: [
                    '<div>{Type}:{0}</div>'
                ],                
                store: 'theDebugStore'               
            }
        ]                
    }
});