﻿Ext.define('TolMobile.view.rent.PayMyRent', {
    extend: 'Ext.Container',
    alias: 'widget.paymyrentview',

    requires: [        
       
    ],

    config: {
        fullscreen: true,  
        title: 'Pay Rent',     
        items: [        
            {
                xtype: 'panel',
                margin: 10,
                html: '<iframe width="100%" height="100%" src="https://www.allpayments.net/allpayments/Signin.aspx?ReturnUrl=%2fallpayments"></iframe>'                 
            }                  
        ]
    }

});