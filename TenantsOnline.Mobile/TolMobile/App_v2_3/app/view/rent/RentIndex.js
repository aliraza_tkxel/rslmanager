Ext.define('TolMobile.view.rent.RentIndex', {
    extend: 'Ext.navigation.View',

    alias: 'widget.rentindexview',

    requires: [
        'Ext.navigation.View'
    ],

    config: {

        title: 'My Rent',
        items: [
            {
                id: 'rentlist',
                xtype: 'list',
                title: 'Pay Rent',
                itemTpl: [
                    '<div>{DisplayName}</div>'
                ],                
                onItemDisclosure: true,
                store: 'theRentViewStore'               
            }
        ]
    }
});