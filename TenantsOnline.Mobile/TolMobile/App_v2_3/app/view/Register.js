Ext.define('TolMobile.view.Register', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.registerview',

   
    config: {
        
    },

    initialize: function () {
        this.callParent();

        var childitems = [
            {
                xtype: 'image',
                height: 150,
                margin: 20,
                src: '../app/images/phone.png'
            },
            {
                xtype: 'label',
                html: 'To register with Tenants Online, please call our services team on: 0303 303 0003',
                margin: 20,
                styleHtmlCls: 'WelcomeTol',
                styleHtmlContent: true
            },
            {
                xtype: 'button',
                id: 'btnLoginFromRegister',
                text: 'Login',
                margin:5,
                padding:10,
                ui:'action'
            }
        ];


        var root = {
                xtype: 'container',                
                items: childitems,
                layout: {
                    type: 'vbox'
                },
                centered: true
            }        

        this.add(root);        
    }

});