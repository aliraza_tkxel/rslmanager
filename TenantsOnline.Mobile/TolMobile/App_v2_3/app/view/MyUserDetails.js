Ext.define('TolMobile.view.MyUserDetails', {
    extend: 'Ext.form.FormPanel',

    alias: 'widget.myuserdetailsview',

    requires: [
        'TolMobile.store.Titles',
        'TolMobile.store.Genders',
        'TolMobile.store.Religion',
        'TolMobile.store.MaritalStatus',
        'TolMobile.store.Ethnicity',
        'TolMobile.store.SexualOrientation',
        'TolMobile.store.ContactDetails',

        'TolMobile.store.Language', 
        'TolMobile.store.Communication',
        'TolMobile.store.PreferredContact',
        'TolMobile.store.Disability',
        'TolMobile.store.UserDetails',

        'Ext.MessageBox',
        'Ext.form.FieldSet',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Text'
    ],

    config: {
        title: 'My Details',
        id: 'editUserDetailsForm',
        
        listeners: {
            activate: function () {
                console.log("userdetails view activated");

            },
            painted: function () {
                console.log("userdetails view painted");

                var form = Ext.getCmp('editUserDetailsForm');
                form.disable();
            },

            initialize: function () {
                console.log("userdetails view initialised");

                var record = Ext.getStore("theUserDetailsStore").getAt(0);
                if (record) {
                    this.setRecord(record);
                }
            }
        }
    },

    initialize: function () {
        this.callParent();

        var items = [
            {
                xtype: 'toolbar',
                store: 'theUserDetailsStore',
                docked: 'top',
                title: { 
                    title: 'My Details',                    
                    docked: 'left'
                },               
                layout: {
                    align: 'center',
                    pack: 'end'                   
                },
                items: [
                     {
                         id: 'myDetailsSaveButton',
                         xtype: 'button',
                         text: 'Save',
                         action: 'MyDetailsSave',
                         scope: this,
                         hidden: true,
                         ui: 'action',
                         margin: 5
                     },
                    {
                        id: 'myDetailsEditButton',
                        xtype: 'button',
                        text: 'Edit',
                        action: 'MyDetailsEdit',
                        scope: this,
                        editMode: false,
                        margin: 5                    
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'My details',
                itemId: 'fsetMyDetails',
                defaults: {
                    labelWidth: Ext.os.is.Phone? "50%" : "30%"
                },
                items: [
                     {
                         xtype: 'selectfield',
                         label: 'Title',
                         displayField: 'ItemName',
                         valueField: 'Id',
                         store: 'theTitleStore',
                         name: 'Title',
                         readOnly: true
                     },
                    {
                        xtype: 'textfield',
                        label: 'First Name',
                        name: 'FirstName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        label: 'Middle Name',
                        name: 'MiddleName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        label: 'Last Name',
                        name: 'LastName',
                        readOnly: true
                    },
                    {
                        xtype: 'selectfield',
                        label: 'Gender',
                        autoSelect: false,
                        displayField: 'ItemName',
                        valueField: 'Id',
                        store: 'theGenderStore',
                        name: 'Gender',
                        placeHolder: 'Select Gender'                        
                    },
                     {
                         xtype: 'textfield',
                         label: 'NI Number',
                         name: 'NINumber'
                     },
                    {
                        xtype: 'datepickerfield',
                        label: 'Date of Birth',
                        name: 'DateOfBirth',                        
                        dateFormat: 'd/m/Y',

                        picker: {
                            yearFrom: 1900,
                            slotOrder: ["day", "month", "year"]
                        }
                    }
                ]
             },
             {
                 // following can be edited
                 xtype: 'fieldset',
                 itemId: 'fsetContact',
                 title: 'Contact',
                 defaults: {
                     labelWidth: Ext.os.is.Phone ? "50%" : "30%"
                 },
                 items: [
                     {
                         xtype: 'textfield',
                         label: 'Telephone',
                         name: 'Telephone',
                         itemId: 'txtTelephone'
                     },
                    {
                        xtype: 'textfield',
                        label: 'Mobile',
                        name: 'Mobile',
                        itemId: 'txtMobile'
                    },
                    {
                        xtype: 'emailfield',
                        label: 'Email *',
                        name: 'Email',
                        itemId: 'txtEmail',
                        placeHolder: 'john.smith@email.com'
                    }
                ]
             },
            {
                xtype: 'fieldset',
                title: 'Other details',
                itemId: 'fsetOtherDetails',
                defaults: {
                    labelWidth: Ext.os.is.Phone? "50%" : "30%"
                },
                items: [
                     {
                         xtype: 'textfield',
                         label: 'Occupation',
                         name: 'Occupation'
                     },
                    {
                        xtype: 'selectfield',
                        label: 'Marital Status *',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        autoSelect: false,
                        store: 'theMaritalStatusStore',
                        name: 'MaritalStatus'
                    },
                    {
                        xtype: 'selectfield',
                        label: 'Ethnicity *',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        store: 'theEthnicityStore',
                        name: 'Ethnicity'
                    },
                    {
                        xtype: 'selectfield',
                        label: 'Religion',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        store: 'theReligionStore',
                        name: 'Religion'
                    },
                    {
                        xtype: 'selectfield',
                        label: 'Sexual Orientation',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        store: 'theSexualOrientationStore',
                        name: 'SexualOrientation'
                    },
                     {
                         xtype: 'selectfield',
                         label: 'Language',
                         displayField: 'ItemName',
                         valueField: 'Id',
                         store: 'theLanguageStore',
                         name: 'Language'
                     },
                     {
                         xtype: 'multiselectfield',
                         label: 'Communication',
                         displayField: 'ItemName',
                         valueField: 'Id',
                         store: 'theCommunicationStore',
                         name: 'Communication',
                         resetItemName: 'None'
                     },
                     {
                        xtype: 'selectfield',
                        label: 'Preferred Contact',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        store: 'thePreferredContactStore',
                        name: 'PreferredContact'
                     },
                     {
                        xtype: 'multiselectfield',
                        label: 'Disability',
                        displayField: 'ItemName',
                        valueField: 'Id',
                        store: 'theDisabilityStore',
                        name: 'Disability',
                        resetItemName: 'None'  
                     }
                ]
            }
        ]

        this.add(items);
    }
});