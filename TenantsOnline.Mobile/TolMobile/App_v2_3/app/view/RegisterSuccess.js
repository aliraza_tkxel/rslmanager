Ext.define('TolMobile.view.RegisterSuccess', {
    extend: 'Ext.Container',
    xtype: 'registersuccessview',

    config: {
        items: [
            {
                xtype: 'label',
                html: 'You have successfully registered for Broadland Housing Tenants Online.',
                margin: 30
            },
            {
                xtype: 'titlebar',
                docked: 'top',
                title: 'Confirmation',
                items: [
                    {
                        xtype:'button',
                        text:'Back',
                        ui:'back',
                        itemId: 'btnBackToLogin'
                    }
                ]
            }
        ]
    }
});