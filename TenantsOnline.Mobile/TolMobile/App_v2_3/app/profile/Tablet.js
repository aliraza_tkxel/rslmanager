﻿Ext.define('TolMobile.profile.Tablet', {
    extend: 'TolMobile.profile.Base',

    config: {
        name: 'Tablet',
        views: ['Login', 'Home', 'Welcome', 'RentGrid', 'MyRent'],

        viewsToAliasMap: [
			{ 'TolMobile.view.tablet.Login': 'widget.loginview' },
            { 'TolMobile.view.tablet.Home': 'widget.homeview' },
            { 'TolMobile.view.tablet.Welcome': 'widget.welcomeview' },
            { 'TolMobile.view.tablet.RentGrid': 'widget.rentgridview' },
			{ 'TolMobile.view.tablet.MyRent': 'widget.myrentview' }
		]
    },

    isActive: function () {
        var active = !Ext.os.is.Phone;
        if (active) {
            //this.mapViewAliases();
        }
        return active;        
    },

    launch: function () {
        console.log("Tablet profile launched");
        
        this.mapViewAliases();

        if (!TolMobile.app.showWelcome) {
            var view = Ext.widget('loginview')
            Ext.Viewport.add(view);
        } else {
            console.log("showing tablet welcome...");
            var view = Ext.widget('welcomeview')
            Ext.Viewport.add(view);
        }
    }
});