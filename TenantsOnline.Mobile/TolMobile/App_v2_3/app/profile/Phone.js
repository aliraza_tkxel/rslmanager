﻿Ext.define('TolMobile.profile.Phone', {
    extend: 'TolMobile.profile.Base',

    config: {
        name: 'Phone',
        views: ['Login', 'Home', 'Welcome', 'RentGrid', 'MyRent', 'MyRentStatement'],

        viewsToAliasMap: [
			{ 'TolMobile.view.phone.Login': 'widget.loginview' },
            { 'TolMobile.view.phone.Home': 'widget.homeview' },
            { 'TolMobile.view.phone.Welcome': 'widget.welcomeview' },
            { 'TolMobile.view.phone.RentGrid': 'widget.rentgridview' },
			{ 'TolMobile.view.phone.MyRent': 'widget.myrentview' },
			{ 'TolMobile.view.phone.MyRentStatement': 'widget.myrentstatementview' }
		]
    },

    isActive: function () {
        var active = Ext.os.is.Phone;
        if (active) {
            //this.mapViewAliases();
        }
        return active;
    },

    launch: function () {
        console.log("Phone profile launched");

        this.mapViewAliases();

        if (!TolMobile.app.showWelcome) {
            var view = Ext.widget('loginview')
            Ext.Viewport.add(view);
        } else {
            console.log("showing phone welcome...");
            var view = Ext.widget('welcomeview')
            Ext.Viewport.add(view);
        }
    }
});