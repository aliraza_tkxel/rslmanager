﻿Ext.define('TolMobile.profile.Base', {
    extend: 'Ext.app.Profile',

    config: {

    },

    mapViewAliases: function () {
        var self = this;

        var views = this.getDependencies().view;
        var newAliasMap = null;

        Ext.each(views, function (view) {

            Ext.Array.some(self.getViewsToAliasMap(), function (map) {

                // create map of classes to views
                if (map[view]) {
                    // set alias on the view
                    console.log("setting alias: " + map[view] + " on " + view);

                    //Ext.ClassManager.setAlias(view, map[view]);

                    var xtype = map[view].substring(7);
                    var cls = Ext.ClassManager.get(view);
                    cls.addXtype(xtype);

                    //TolMobile.view.phone.Login.addXtype("loginview")


                    if (!newAliasMap) {
                        newAliasMap = {};
                    }

                    newAliasMap[view] = [map[view]];
                    return true;
                }
            });
        });

        if (newAliasMap) {
            console.log('view aliases being mapped for: ' + this.$className);
            //Ext.ClassManager.addNameAliasMappings(newAliasMap)
        }
    }
});