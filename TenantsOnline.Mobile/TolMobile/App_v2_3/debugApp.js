(function(scope){

	if ( scope.location.search.substring(1).indexOf("debug") > -1 ) {
		scope.bufferedConsole = [];
		scope.bufferedException = [];
		
		scope.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
		
			scope.bufferedException.push({ error: errorMsg, url: url, line: lineNumber});		
			return false;
		}

		//saving the original console.log function
		scope.preservedConsoleLog = console.log;
	 
		//overriding console.log function
		console.log = function() {
	 
			//we can't just call to `preservedConsoleLog` function,
			//that will throw an error (TypeError: Illegal invocation)
			//because we need the function to be inside the
			//scope of the `console` object so we going to use the
			//`apply` function
			scope.preservedConsoleLog.apply(console, arguments);    

			scope.bufferedConsole.push(arguments);
		}
	}
})(window);