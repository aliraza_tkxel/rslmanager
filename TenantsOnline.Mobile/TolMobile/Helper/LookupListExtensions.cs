﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TolMobile.Data.Models.Lookups;

namespace TolMobile.Helper
{
    public static class LookupListExtensions
    {        
        public static IEnumerable<KeyPairLookupModel> WithDefaultOption(this IEnumerable<KeyPairLookupModel> model)
        {
            return model.ToList().WithDefaultOption();
        }

        public static IEnumerable<KeyPairLookupModel> WithDefaultOption(this List<KeyPairLookupModel> model)
        {
            model.Add(new KeyPairLookupModel { Id = null, ItemName = "Please select" });
            return model.OrderBy(x => x.Id);
        }

        public static IEnumerable<SimpleLookupModel> WithDefaultOption(this IEnumerable<SimpleLookupModel> model)
        {
            var lookup = model.ToList();
            lookup.Add(new SimpleLookupModel { Id = string.Empty, ItemName = "Please select" });
            return lookup.OrderBy(x => x.Id);
        }

        //public static IEnumerable<SimpleLookupModel> ConvertToSimpleLookup(this IEnumerable<KeyPairLookupModel> model)
        //{
        //    return model.Select(x => new SimpleLookupModel
        //    {
        //        Id = x.Id == null ? null : x.ItemName.ToString(),
        //        ItemName = x.ItemName
        //    });
        //}
    }
}