﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Data.Models.Users;
using TolMobile.Filters;
using TolMobile.Services.News;


namespace TolMobile.Controllers
{
    public class NewsController : ApiController
    {
        private INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        public IEnumerable<NewsModel> GetAll()
        {
           var news = _newsService.GetAll();
            return news;
        } 
     
    }
}
