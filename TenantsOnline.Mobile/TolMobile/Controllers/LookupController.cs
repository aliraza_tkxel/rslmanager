﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Repositories.Lookup;
using TolMobile.Data.Models.Lookups;
using TolMobile.Services.Users;
using System.Web.Http;
using TolMobile.Data.Context;
using TolMobile.Helper;

namespace TolMobile.Controllers
{
    public class LookupController : ApiController
    {
        private ILookupService _lookupService;

        public LookupController(ILookupService lookupService)
        {
            _lookupService = lookupService;
        }


        [HttpGet]
        public IEnumerable<SimpleLookupModel> Genders()
        {            
            return _lookupService.GetGenders();
        }


        [HttpGet]
        public IEnumerable<KeyPairLookupModel> Titles()
        {
            return _lookupService.GetTitles().WithDefaultOption();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> MaritalStatus()
        {
            return _lookupService.GetMaritalStatusTypes();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> Ethnicity()
        {
            return _lookupService.GetEthnicityTypes().WithDefaultOption();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> Religion()
        {
            return _lookupService.GetReligionTypes().WithDefaultOption();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> SexualOrientation()
        {
            return _lookupService.GetSexualOrientationTypes().WithDefaultOption();
        }

        [HttpGet]
        public IEnumerable<SimpleLookupModel> Language()
        {
            return _lookupService.GetLanguageTypes().WithDefaultOption();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> Communication()
        {
            return _lookupService.GetCommunicationTypes();
        }

        [HttpGet]
        public IEnumerable<SimpleLookupModel> PreferredContact()
        {
            return _lookupService.GetPreferredContactTypes().WithDefaultOption();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> Disability()
        {
            return _lookupService.GetDisabilityTypes();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> ParkingType()
        {
            return _lookupService.GetParkingTypes();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> ASBCategory()
        {
            return _lookupService.GetASBCategories();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> ComplaintType()
        {
            return _lookupService.GetComplaintTypes();
        }

        [HttpGet]
        public IEnumerable<KeyPairLookupModel> AbandonedType()
        {
            return _lookupService.GetAbandonedTypes();
        }

        [HttpGet]
        public IEnumerable<SimpleLookupModel> TerminationReason()
        {
            return _lookupService.GetTerminationReasons();
        }
    }
}
