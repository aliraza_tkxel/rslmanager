﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TolMobile.Services;
using System.Web.Http;
using TolMobile.Services.Users;
using TolMobile.Services.Offices;
using TolMobile.Services.Rent;

namespace TolMobile.Controllers
{
    public class OfficeController : ApiController
    {
        private IOfficeService _officeService;

        public OfficeController(IOfficeService officeService)                             
        {
            _officeService = officeService;         
        }

        public IEnumerable<OfficeModel> GetAll()
        {
            var offices = _officeService.GetAll();
            return offices;
        }

    }
}
