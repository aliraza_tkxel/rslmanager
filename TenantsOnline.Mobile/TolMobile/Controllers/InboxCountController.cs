﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;
using TolMobile.Data.Models.Lookups;
using System.Web.Http;
using AttributeRouting.Web.Http;
using System.Net.Http;

namespace TolMobile.Controllers
{
    public class InboxCountController : ApiController
    {
        private IInboxService _inboxService;

        public InboxCountController(IInboxService inboxService)
        {
            _inboxService = inboxService;            
        }

        public EnquiryCountModel GetAll()
        {            
            var model = new EnquiryCountModel
            {
                EnquiryCount = _inboxService.GetCustomerEnquiriesCount(),
                EnquiryResponseCount = _inboxService.GetCustomerEnquiryResponseCount()
            };
            return model;
        }
       
    }
}
