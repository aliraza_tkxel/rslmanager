﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Data.Models.Users;
using TolMobile.Filters;

namespace TolMobile.Controllers
{
    public class AuthController : ApiController
    {
        private IAuthenticationService _authService;

        public AuthController(IAuthenticationService authService)
        {
            _authService = authService;
        }

        [ValidationFilter]
        public HttpResponseMessage Post(LoginModel model)        
        {
            var authResponse = _authService.AuthenticateUser(model);
            if (authResponse!=null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, authResponse);
            }

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
     
    }
}
