﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TolMobile.Services;
using System.Web.Http;
using TolMobile.Services.Rent;

namespace TolMobile.Controllers
{
    public class RentController : ApiController
    {
        private IRentService _rentService;
        private IAuthenticationService _authService;

        public RentController(IRentService rentService,
                              IAuthenticationService authService)
        {
            _rentService = rentService;
            _authService = authService;
        }

        public IEnumerable<RentLineModel> GetAll(RentPeriods rentPeriod)
        {
            var transactions = _rentService.GetRentStatement(rentPeriod);            
            return transactions;
        }

#region "test method"
        //public IEnumerable<RentLineModel> GetAll(RentPeriods rentPeriod)
        //{        
        //    var transactions = new List<RentLineModel>();
        //    transactions.Add(new RentLineModel { Balance = "123", Credit = "123", Debit = "123" });
        //    transactions.Add(new RentLineModel { Balance = "123", Credit = "123", Debit = "123" });
        //    transactions.Add(new RentLineModel { Balance = "123", Credit = "123", Debit = "123" });
        //    transactions.Add(new RentLineModel { Balance = "123", Credit = "123", Debit = "123" });
        //    return transactions;
        //}
#endregion

    }
}
