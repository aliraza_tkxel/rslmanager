﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;

namespace TolMobile.Controllers
{
    public class ContactController : ApiController
    {
        private IUserService _userService;

        public ContactController(IUserService userService)
        {
            _userService = userService;
        }

        [ValidationFilter]
        public ContactDetailsModel Put(ContactDetailsModel model)
        {
            return _userService.UpdateUserContact(model);
        }

        public ContactDetailsModel Get()
        {
            return _userService.GetCurrentContactDetails();            
        }
    }
}
