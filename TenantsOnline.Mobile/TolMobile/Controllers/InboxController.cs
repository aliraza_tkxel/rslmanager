﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;
using TolMobile.Data.Models.Lookups;
using System.Web.Http;
using AttributeRouting.Web.Http;
using System.Net.Http;

namespace TolMobile.Controllers
{
    public class InboxController : ApiController
    {
        private IInboxService _inboxService;
       
        public InboxController(IInboxService inboxService)
        {
            _inboxService = inboxService;            
        }

        public IEnumerable<EnquiryLineModel> GetAll()
        {            
            return _inboxService.GetInboxEnquiries(null);
        }

        //[ActionName("EnquiryCount")]
        //public int GetEnquiryCount()
        //{
        //    return _inboxService.GetCustomerEnquiriesCount();
        //}

        //[ActionName("EnquiryResponseCount")]
        //public int GetEnquiryResponseCount()
        //{
        //    return _inboxService.GetCustomerEnquiryResponseCount();
        //}

        [ValidationFilter]
        public HttpResponseMessage Post(EnquiryReplyModel model)
        {            
            _inboxService.AddCustomerResponse(model.EnquiryLogId, model.Response, DateTime.Now);

            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }
      
        #region "test"

        //[GET("/Api/Inbox/EnquiryCount")]
        //public EnquiryCountModel GetEnquiryCounts()
        //{
        //    var model = new EnquiryCountModel();
        //    model.EnquiryCount = _inboxService.GetCustomerEnquiriesCount();
        //    model.EnquiryResponseCount = _inboxService.GetCustomerEnquiryResponseCount();
        //    return model;
        //}

        //[HttpGet("Inbox/EnquiryResponseCount")]
        //public int GetEnquiryResponseCount()
        //{
        //    return _inboxService.GetCustomerEnquiryResponseCount();
        //}

        //public IEnumerable<EnquiryLineModel> GetAll()
        //{
        //    var list = new List<EnquiryLineModel>();
        //    list.Add(new EnquiryLineModel
        //        {
        //            CreationDate = DateTime.Now,
        //            EnquiryLogID = 1,
        //            ImageKeyFlag = 1,
        //            Item = "Item",
        //            Nature = "Nature",
        //            ResponseStatus = "Reponse"
        //        });

        //    list.Add(new EnquiryLineModel
        //    {
        //        CreationDate = DateTime.Now,
        //        EnquiryLogID = 1,
        //        ImageKeyFlag = 1,
        //        Item = "Item",
        //        Nature = "Nature",
        //        ResponseStatus = "Reponse"
        //    });            
        //    return list;
        //}

        #endregion
    }
}
