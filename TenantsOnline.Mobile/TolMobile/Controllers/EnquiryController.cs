﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;
using TolMobile.Data.Models.Lookups;
using System.IO;
using System.Threading.Tasks;
using System.Diagnostics;
using TolMobile.Services.Properties;
using System.Configuration;

namespace TolMobile.Controllers
{
    public class EnquiryController : ApiController
    {
        private IEnquiryService _enquiryService;

        public EnquiryController(IEnquiryService enquiryService)
        {
            _enquiryService = enquiryService;
        }

        [ValidationFilter]
        public void RequestTermination(EndTenancyModel model)
        {
            _enquiryService.RequestTermination(model);
        }

        [ValidationFilter]
        public void RequestParkingType(RequestParkingModel model)
        {
            _enquiryService.RequestParkingType(model);
        }

        [ValidationFilter]
        public void ReportComplaint(ReportComplaintModel model)
        {
            _enquiryService.ReportComplaint(model);
        }

        [ValidationFilter]
        public void ReportASB(ReportASBModel model)
        {
            _enquiryService.ReportASB(model);
        }

        [ValidationFilter]
        public void ReportAbandonedType(ReportAbandonedModel model)
        {
            _enquiryService.ReportAbandonedType(model);
        }
        
        public Task<FileUpModel> ReportAbandonedTypeWithPhoto([FromUri]int ReportType, [FromUri]string LocationDescription, [FromUri]string AdditionalInformation)
        {
            if (Request.Content.IsMimeMultipartContent())
            {                

                var imagePath = TolMobile.Services.Properties.Settings.Default.NeglectPhotoLocation;

                var provider = new TolMobileFormDataStreamProvider(imagePath);

                var currentHttpContext = HttpContext.Current;

                var task = Request.Content.ReadAsMultipartAsync(provider).ContinueWith(
                t =>
                {
                    // Enquiry service requires access to authentication data which is retrieved via http context.
                    // Not ideal but set the http context here so it available on the new thread.                    
                    HttpContext.Current = currentHttpContext;

                    if (t.IsFaulted || t.IsCanceled)
                    {
                        if (t.Exception != null)
                        {                            
                            var flattenedException = t.Exception.Flatten();                            
                            Elmah.ErrorSignal.FromCurrentContext().Raise(flattenedException);
                            var httpResponseException = Request.CreateResponse(HttpStatusCode.InternalServerError, flattenedException.Message);
                            throw new HttpResponseException(httpResponseException);
                        }
                    }

                    try
                    {
                        var fileInfo = provider.FileData.FirstOrDefault();
                        if (fileInfo == null)
                        {
                            throw new Exception("No file data was found");                            
                        }

                        _enquiryService.ReportAbandonedType(new ReportAbandonedModel
                        {
                            AdditionalInformation = AdditionalInformation,
                            LocationDescription = LocationDescription,
                            ReportType = ReportType
                        }, Path.GetFileName(fileInfo.LocalFileName));

                    }
                    catch (Exception e)
                    {
                        // convert exception into http friendly and log it
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        var httpResponseException = Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                        throw new HttpResponseException(httpResponseException);                        
                    }
                   
                    var uploadModel = new FileUpModel { success = true };
                    return uploadModel;
                });

                return task;
            }
            else
            {
                var httpResponseException = new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted"));
                Elmah.ErrorSignal.FromCurrentContext().Raise(httpResponseException);
                throw httpResponseException;
            }
        }
    }
}
