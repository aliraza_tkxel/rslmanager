﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;

namespace TolMobile.Controllers
{
    public class UserController : ApiController
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [ValidationFilter]
        public UserDetailsModel Put(UserDetailsModel model)
        {
            return _userService.UpdateUser(model);
        }

        public UserDetailsModel Get()
        {
            return _userService.GetCurrentUserDetails();            
        }
    }
}
