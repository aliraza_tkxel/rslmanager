﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;
using TolMobile.Services;

namespace TolMobile.Controllers
{
    public class RegisterController : ApiController
    {
        private IUserService _userService;

        public RegisterController(IUserService userService)
        {
            _userService = userService;
        }

        [ValidationFilter]
        public RegistrationResultModel Post(RegisterModel model)
        {
            return _userService.RegisterUser(model);
        }       
    }
}
