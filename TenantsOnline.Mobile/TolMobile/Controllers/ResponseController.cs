﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Filters;
using TolMobile.Data.Models.Lookups;
using System.Web.Http;
using AttributeRouting.Web.Http;
using System.Net.Http;

namespace TolMobile.Controllers
{
    public class ResponseController : ApiController
    {
        private IInboxService _inboxService;

        public ResponseController(IInboxService inboxService)
        {
            _inboxService = inboxService;            
        }

        public IEnumerable<EnquiryResponseLineModel> Get()
        {
            var responses = _inboxService.GetEnquiryResponses(1);
            return responses;
        }

        public IEnumerable<EnquiryResponseLineModel> Get(int enquiryLogId)
        {
            var responses = _inboxService.GetEnquiryResponses(enquiryLogId);
            return responses;
        }
      
    }
}
