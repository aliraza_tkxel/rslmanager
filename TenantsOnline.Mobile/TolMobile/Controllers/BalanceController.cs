﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TolMobile.Services;
using System.Web.Http;
using TolMobile.Services.Rent;

namespace TolMobile.Controllers
{
    public class BalanceController : ApiController
    {
        private IRentService _rentService;
        private IAuthenticationService _authService;

        public BalanceController(IRentService rentService,
                              IAuthenticationService authService)
        {
            _rentService = rentService;
            _authService = authService;
        }

        public BalanceModel GetAll()
        {
            return _rentService.GetBalance();            
        }
        #region "test method"
        //public BalanceModel GetAll()
        //{            
        //    var model = new BalanceModel
        //    {
        //        AccountBalance = "CR 12345",
        //        HousingBenefit = "567"
        //    };
        //    return model;
        //}
        #endregion
    }
}
