﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Filters;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Controllers
{
    public class TestModel
    {
        [Required]
        public string Field1 { get; set; }
        [Required]
        public string Field2 { get; set; }
    }
    public class TestController : ApiController
    {
        public TestController()
        {

        }

        public string Get()
        {
            throw new Exception("An error");            
        }

        public HttpResponseMessage Get(int id)
        {
            var obj = new[]
            {
                 new { Field1 = "Field1", Field2 = "Field2"},
                 new { Field1 = "Field1", Field2 = "Field2"}
            };

            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        public HttpResponseMessage Post([FromUri]string filename)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.Created;
            return response;
        }

        [ValidationFilter]
        public TestModel Put(TestModel model)
        {

            return model;
        }
    }
}
