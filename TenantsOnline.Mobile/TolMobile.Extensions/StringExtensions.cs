﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Security.Cryptography;

namespace TolMobile.Extensions
{
    public static class StringExtensions
    {
        public static string ToSHA256(this string value)
        {
            var hasher = new SHA256Managed();
            var hashedBytes = hasher.ComputeHash(Encoding.Default.GetBytes(value));

            return BitConverter.ToString(hashedBytes).Replace("-", "");
        }

        public static string ToMD5(this string value)
        {
            var hasher = new MD5CryptoServiceProvider();
            var hashedBytes = hasher.ComputeHash(Encoding.ASCII.GetBytes(value));

            var result = string.Empty;

            for (int i = 0; i < hashedBytes.Length; i++)
            {
                result += hashedBytes[i].ToString("x2").ToLower();
            }

            return result;
        }

        public static Int32 ToInt32(this string value)
        {
            return Convert.ToInt32(value);
        }

        public static string With(this string value, params object[] args)
        {
            return string.Format(value, args);
        }

        public static string Pluralize(this string value, int count)
        {
            return (value + ((count != 1) ? "s" : string.Empty));
        }

        public static bool IsNumeric(this string text)
        {
            return text.All(Char.IsNumber);
        }
    }
}
