﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Http.ModelBinding;

namespace TolMobile.Extensions
{
    public static class ModelValidationExtension
    {
        public static JObject ErrorsToJson(this ModelStateDictionary modelState)
        {
            var errors = new JObject();
            foreach (var key in modelState.Keys)
            {
                var state = modelState[key];
                if (state.Errors.Any())
                {
                    errors[key] = state.Errors.First().ErrorMessage;
                }
            }
            return errors;
        }
    }
}
