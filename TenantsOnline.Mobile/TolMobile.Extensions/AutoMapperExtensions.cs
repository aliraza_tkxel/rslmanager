﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using AutoMapper;

namespace TolMobile.Extensions
{
    public static class AutoMapperExtensions
    {
        public static TDest MapTo<TDest>(this object source) where TDest : class
        {
            var dest = (TDest)Mapper.Map(source, source.GetType(), typeof(TDest));
            return dest;
        }

        public static IEnumerable<TDest> MapToEnumerable<TDest>(this IEnumerable source) where TDest : class
        {
            var dest = (IEnumerable<TDest>)Mapper.Map(source, source.GetType(), typeof(IEnumerable<TDest>));
            return dest;
        }

        public static IList<TDest> MapToList<TDest>(this IEnumerable source) where TDest : class
        {
            var dest = (IList<TDest>)Mapper.Map(source, source.GetType(), typeof(IList<TDest>));
            return dest;
        }
       
    }
}
