﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolMobile.Services.Exceptions
{
    public class ServiceException : Exception
    {
        public string Description { get; set; }

        public ServiceException(string message)
        {
            Description = message;
        }
    }
}
