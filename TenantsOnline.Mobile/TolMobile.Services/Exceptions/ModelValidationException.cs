﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolMobile.Services.Exceptions
{
    public class ModelValidationException : Exception
    {
        public string Key { get; set; }
        public string Description { get; set; }

        public ModelValidationException(string key, string message)
        {
            Key = key;
            Description = message;
        }
    }
}
