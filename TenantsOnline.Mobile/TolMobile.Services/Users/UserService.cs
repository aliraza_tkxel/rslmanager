﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;

namespace TolMobile.Services.Users
{
    public interface IUserService
    {
        UserDetailsModel UpdateUser(UserDetailsModel model);
        ContactDetailsModel UpdateUserContact(ContactDetailsModel model);
        UserDetailsModel GetCurrentUserDetails();
        ContactDetailsModel GetCurrentContactDetails();
        RegistrationResultModel RegisterUser(RegisterModel model);
    }

    public class UserService : IUserService
    {
        private IAuthRepo _authRepo;
        private IUserRepo _userRepo;
        private IContactRepo _contactRepo;
        private IAuthenticationService _authService;
        private ITenancyRepo _tenancyRepo;

        private int CurrentUserId
        {
            get
            {
                return _authService.UserId;
            }
        }

        public UserService(IAuthRepo authRepo,
                           IUserRepo userRepo,
                           IAuthenticationService authService,
                           IContactRepo contactRepo,
                           ITenancyRepo tenancyRepo)
        {
            _authRepo = authRepo;
            _authService = authService;
            _contactRepo = contactRepo;
            _userRepo = userRepo;
            _tenancyRepo = tenancyRepo;
        }

        public UserDetailsModel UpdateUser(UserDetailsModel model)
        {
            // first save user details
            var user = _userRepo.GetByUserId(CurrentUserId);
            if (user == null)
            {
                throw new NotFoundException();
            }

            Mapper.Map(model, user);

            _userRepo.Save();

            //  manually map across contact details
            var contactDetails = _contactRepo.GetActive(CurrentUserId);
            contactDetails.MOBILE = model.Mobile;
            contactDetails.TEL = model.Telephone;
            contactDetails.EMAIL = model.Email;
            _contactRepo.Save();

            return model;
        }

        public ContactDetailsModel UpdateUserContact(ContactDetailsModel model)
        {
            // save address details
            var contact = _contactRepo.GetActive(CurrentUserId);
            if (contact == null)
            {
                throw new NotFoundException();
            }
            Mapper.Map(model, contact);
            _contactRepo.Save();

            return model;
        }

        public UserDetailsModel GetCurrentUserDetails()
        {
            // get user details
            var userModel = _userRepo.GetByUserId(CurrentUserId).MapTo<UserDetailsModel>();
            var tenancyModel = _tenancyRepo.GetByUserId(CurrentUserId);

            userModel.TenancyId = tenancyModel.TENANCYID;

            // manually map across contact details
            var contactDetails = _contactRepo.GetActive(CurrentUserId);
            userModel.Mobile = contactDetails.MOBILE;
            userModel.Telephone = contactDetails.TEL;
            userModel.Email = contactDetails.EMAIL;
            return userModel;
        }

        public ContactDetailsModel GetCurrentContactDetails()
        {
            // get contact details
            var userModel = _contactRepo.GetActive(CurrentUserId).MapTo<ContactDetailsModel>();
            return userModel;
        }

        public RegistrationResultModel RegisterUser(RegisterModel model)
        {
            var result = _userRepo.TenantRegistration(model.MapTo<RegisterUser>());
            return result.MapTo<RegistrationResultModel>();
        }
    }
}
