﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class UserDetailsModel
    {
        public int Id { get; set; }

        // user details
        [Required]
        public int? Title { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Gender { get; set; }

        [Required]
        public string Telephone { get; set; }

        [Required]
        public string Mobile { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string NINumber { get; set; }
        [Required]
        public DateTime? DateOfBirth { get; set; }      

        // other details
        public string Occupation { get; set; }
        [Required]
        public int? MaritalStatus { get; set; }
        [Required]
        public int? Ethnicity { get; set; }
        public int? Religion { get; set; }
        public int? SexualOrientation { get; set; }

        public string Language { get; set; }
        public IEnumerable<int> Communication { get; set; }
        public string PreferredContact { get; set; }
        public IEnumerable<int> Disability { get; set; }

        public int TenancyId { get; set; }
    }
}
