﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class RegisterModel
    {             
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }

        [Required]
        public string TenancyId { get; set; }

        [Required]
        public string Email { get; set; }
      
        [Required]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
