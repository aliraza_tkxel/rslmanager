﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class ContactDetailsModel
    {
        public int Id { get; set; }

        public string Telephone { get; set; }
        public string Mobile { get; set; }

        [Required]
        public string Email { get; set; }

        public string HouseNo { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        [Required]
        public string PostCode { get; set; }
    }
}
