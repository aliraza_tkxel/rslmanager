﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class RegistrationResultModel
    {      
        public int CustomerId { get; set; }
        public string Error { get; set; }
    }
}
