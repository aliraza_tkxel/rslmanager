﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using TolMobile.Data;

namespace TolMobile.Services.Authentication
{
    public interface IWebApiAuthenticationService  
    {
        string AuthenticateForWebApi(string authToken);
    }

    public class WebApiAuthenticationService : IWebApiAuthenticationService
    {
        private IAuthRepo _authRepo;
        private ITenancyRepo _tenancyRepo;

        private static AuthenticationModel AuthUserData
        {
            get
            {
               // return ((WebApiIdentity)HttpContext.Current.User.Identity).UserData;
                return null;
            }
        }

        public WebApiAuthenticationService(IAuthRepo authRepo, ITenancyRepo tenancyRepo)
        {
            _authRepo = authRepo;
            _tenancyRepo = tenancyRepo;
        }

   
        public string AuthenticateForWebApi(string authToken)
        {
            var authenticatedUser = _authRepo.GetByAuthToken(authToken);

            if (authenticatedUser != null)
            {                
                var userId = authenticatedUser.CustomerID.Value;
                var tenancy = _tenancyRepo.GetByUserId(userId);
                
                var userData = new AuthenticationModel { UserId = userId, AuthToken = authToken, TenancyId = tenancy.TENANCYID  };

                var principal = new WebApiPrincipal(new WebApiIdentity(authenticatedUser.LoginID.ToString()) { UserData = userData });
                HttpContext.Current.User = principal;

                return userData.AuthToken;

                //HttpContext.Current.User = new WebApiPrincipal(new WebApiIdentity(authToken));
                //return true;
            }
            return string.Empty;            
        
        }
      
    }
}
