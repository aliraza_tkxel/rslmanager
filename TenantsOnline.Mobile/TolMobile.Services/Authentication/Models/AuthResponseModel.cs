﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolMobile.Data.Models.Authentication
{
    public class AuthResponseModel
    {        
        public string AuthToken { get; set; }
    }    
}
