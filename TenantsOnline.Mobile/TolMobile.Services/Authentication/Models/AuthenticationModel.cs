﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{    
    public class AuthenticationModel
    {
        public int UserId { get; set; }        
        public string AuthToken { get; set; }
        public int TenancyId { get; set; }
    }

    public class LoginModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }   
    }
}
