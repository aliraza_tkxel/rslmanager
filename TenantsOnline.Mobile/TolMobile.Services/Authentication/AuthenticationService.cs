﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Services.Authentication;
using System.Web;
using TolMobile.Data.Models.Authentication;

namespace TolMobile.Services
{
    public interface IAuthenticationService
    {
        AuthResponseModel AuthenticateUser(LoginModel model);
        int UserId { get; }
        int TenancyId { get; }
    }

    public class AuthenticationService : IAuthenticationService
    {

        private IAuthRepo _authRepo;

        #region "properties"
        private static AuthenticationModel AuthUserData
        {
            get
            {
                return ((WebApiIdentity)HttpContext.Current.User.Identity).UserData;
            }
        }

        public int UserId
        {
            get 
            {
                return AuthUserData.UserId;
            }
        }

        public int TenancyId
        {
            get
            {
                return AuthUserData.TenancyId;
            }
        }
#endregion

        public AuthenticationService(IAuthRepo authRepo)
        {
            _authRepo = authRepo;
        }

        public AuthResponseModel AuthenticateUser(LoginModel model)
        {
            if (model != null)
            {
                var authUser = _authRepo.GetByUsernameAndPwd(model.Username, model.Password);

                if (authUser != null)
                {
                    if (string.IsNullOrEmpty(authUser.AuthToken))
                    {
                        // create new token if one doesn't already exist
                        authUser.AuthToken = Guid.NewGuid().ToString();
                        _authRepo.Save();
                    }

                    // create authenticated principal
                    var userData = new AuthenticationModel { UserId = authUser.CustomerID.Value, AuthToken = authUser.AuthToken };
                    var principal = new WebApiPrincipal(new WebApiIdentity(authUser.CustomerID.ToString()) { UserData = userData });
                    HttpContext.Current.User = principal;
                    return new AuthResponseModel { AuthToken = userData.AuthToken };
                }
            }
            return null;
        }

       
    }
}
