﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

namespace TolMobile.Services.Authentication
{
    public class WebApiIdentity : IIdentity
    {
        private readonly string _identityName;

        public AuthenticationModel UserData { get; set; }

        public WebApiIdentity(string name)
        {
            _identityName = name;
        }

        public string Name
        {
            get { return _identityName; }
        }

        public string AuthenticationType
        {
            get { return "WebApi Token Auth"; }
        }

        public bool IsAuthenticated
        {
            get
            {
                return true;
            }
        }
    }
}
