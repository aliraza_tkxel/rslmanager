﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

namespace TolMobile.Services.Authentication
{
    public class WebApiPrincipal : IPrincipal
    {
        private readonly IIdentity _customIdentity;

        public WebApiPrincipal(IIdentity identity)
        {
            _customIdentity = identity;
        }

        public IIdentity Identity
        {
            get { return _customIdentity; }
        }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}
