﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;
using TolMobile.Data.Context;

namespace TolMobile.Services.Offices
{   
    public interface IOfficeService
    {
        IEnumerable<OfficeModel> GetAll();
    }

    public class OfficeService : IOfficeService
    {
               
        private IOfficeRepo _officeRepo;
       
        public OfficeService(IOfficeRepo officeRepo)
        {
            _officeRepo = officeRepo;
        }

        public IEnumerable<OfficeModel> GetAll()
        {
            return _officeRepo.GetAll().MapToEnumerable<OfficeModel>();
        }
       
    }
}
