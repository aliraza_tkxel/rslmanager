﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{

    public class OfficeModel
    {
        public string Title { get; set; }
        public string AssociationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string Tel { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
