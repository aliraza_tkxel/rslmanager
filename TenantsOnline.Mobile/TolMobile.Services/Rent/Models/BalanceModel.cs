﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services.Rent
{    
    public class BalanceModel
    {        
        public string AccountBalance { get; set; }
        public string HousingBenefit { get; set; }
        public bool InArrears { get; set; }
    }
}
