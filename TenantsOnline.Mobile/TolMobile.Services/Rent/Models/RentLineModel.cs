﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class RentStatementModel
    {
        public IEnumerable<RentLineModel> RentLines { get; set; }
        public string Balance { get; set; }
    }

    public class RentLineModel
    {
        public string Credit { get; set; }
        public string Debit { get; set; }
        public string TransactionDate { get; set; }
        public string PaymentStartDate { get; set; }
        public string PaymentEndDate { get; set; }
        public string Balance { get; set; }        
    }
}
