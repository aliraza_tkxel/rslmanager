﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;
using TolMobile.Data.Context;

namespace TolMobile.Services.Rent
{
    public enum RentPeriods
    {
        All = 0,
        RecentTransactions = 1,
        Previous4Weeks = 2,
        Previous3Months = 3,
        Previous6Months = 6,
    }

    public interface IRentService
    {
        IEnumerable<RentLineModel> GetRentStatement(RentPeriods rentPeriod);
        BalanceModel GetBalance();
    }

    public class RentService : IRentService
    {
        
        private IAuthenticationService _authService;
        private IRentRepo _rentRepo;

        private int TenancyId 
        {
            get
            {
                return _authService.TenancyId;
            }
        }

        public RentService(IAuthenticationService authService, IRentRepo rentRepo)
        {
            _authService = authService;
            _rentRepo = rentRepo;
        }

        public BalanceModel GetBalance()
        {
            var hb = _rentRepo.GetHousingBenefit(TenancyId);
            var balance = _rentRepo.GetAccountBalance(TenancyId);

            if (!balance.HasValue)
                return null;


            // logic copied from tol....
            decimal newBalance;
            decimal housingBenefit = 0;
            if ( hb.AntHB > 0 )
            {
                housingBenefit = hb.AntHB;
                newBalance = balance.Value - hb.AntHB;
            }
            else
            {                
                newBalance = balance.Value + hb.AdvHB;
            }

            bool inArrears = balance > 0;

            var dr = inArrears ? "DR " : "";
            var model = new BalanceModel
            {
                AccountBalance = string.Format("{0}£{1}", dr, newBalance.ToString("F")),
                HousingBenefit = string.Format("£{0}", housingBenefit.ToString("F")),
                InArrears = inArrears
            };

            return model;
        }

        public IEnumerable<RentLineModel> GetRentStatement(RentPeriods rentPeriod)
        {
            DateTime startDate; 
            DateTime endDate = DateTime.Now;

            switch (rentPeriod)
            {
                case RentPeriods.All:
                    return _rentRepo.GetRentStatement(TenancyId).MapToEnumerable<RentLineModel>();                    
                    break;

                case RentPeriods.Previous4Weeks:
                    startDate = DateTime.Now.AddMonths(-1);
                    break;

                case RentPeriods.Previous3Months:
                    startDate = DateTime.Now.AddMonths(-3);
                    break;

                case RentPeriods.Previous6Months:
                    startDate = DateTime.Now.AddMonths(-6);
                    break;

                case RentPeriods.RecentTransactions:
                    startDate = DateTime.Now;
                    break;

                default:
                    throw new Exception("Incorrect rent period selected");
            }


            var rentStatement = _rentRepo.GetRentStatement(TenancyId, startDate, endDate).MapToEnumerable<RentLineModel>();         
            return rentStatement;
        }

    }
}
