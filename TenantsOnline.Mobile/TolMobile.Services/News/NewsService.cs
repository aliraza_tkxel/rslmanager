﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;
using TolMobile.Data.Context;
using System.IO;
using TolMobile.Services.Properties;

namespace TolMobile.Services.News
{   
    public interface INewsService
    {
        NewsModel GetLatest();
        IEnumerable<NewsModel> GetAll();
    }

    public class NewsService : INewsService
    {
               
        private INewsRepo _newsRepo;
        private const string ImagesFolder = "/app/images/";

        public NewsService(INewsRepo newsRepo)
        {            
            _newsRepo = newsRepo;
        }

        private string Combine(string start, string finish)
        {                     
            return Path.Combine(start, finish).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }

        private string GetImagePath(NewsModel model)
        {
            if (string.IsNullOrEmpty(model.ImagePath))
            {
                // return local from sencha app
                return Combine(ImagesFolder, Settings.Default.NoImageFile);
            }
            else
            {
                // return news path from RSL (setup using virtual folder)
                return Combine(Settings.Default.NewsPath, model.ImagePath);
            }            
        }

        public IEnumerable<NewsModel> GetAll()
        {
            var news = _newsRepo.GetAll().MapToEnumerable<NewsModel>();
            foreach ( var item in news)
            {
                item.ImagePath = GetImagePath(item);
            }
            return news;
        }

        public NewsModel GetLatest()
        {
            var model =_newsRepo.GetLatest().MapTo<NewsModel>();
            model.ImagePath = GetImagePath(model);
            return model;
        }
    }
}
