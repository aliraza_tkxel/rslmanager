﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{

    public class NewsModel
    {
        public string Headline { get; set; }
        public string DateCreated { get; set; }
        public string NewsContent { get; set; }
        public string ImagePath { get; set; }      
    }
}
