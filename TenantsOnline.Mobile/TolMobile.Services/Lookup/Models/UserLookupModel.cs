﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolMobile.Data.Models.Lookups
{
    public class KeyPairLookupModel
    {
        public int? Id { get; set; }
        public string ItemName { get; set; }
    }

    public class SimpleLookupModel
    {
        public string Id { get; set; }
        public string ItemName { get; set; }
    }

    public class SerialisedLookupModel
    {
        public string Values { get; set; }
    }
}
