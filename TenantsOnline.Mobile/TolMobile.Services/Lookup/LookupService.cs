﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;
using TolMobile.Data.Repositories.Lookup;
using TolMobile.Data.Models.Lookups;
using TolMobile.Data.Context;

namespace TolMobile.Services.Users
{
    public interface ILookupService
    {
        // rsl lookups
        IEnumerable<SimpleLookupModel> GetGenders();
        IEnumerable<KeyPairLookupModel> GetTitles();
        IEnumerable<KeyPairLookupModel> GetMaritalStatusTypes();
        IEnumerable<KeyPairLookupModel> GetEthnicityTypes();
        IEnumerable<KeyPairLookupModel> GetReligionTypes();
        IEnumerable<KeyPairLookupModel> GetSexualOrientationTypes();
        IEnumerable<SimpleLookupModel> GetLanguageTypes();
        IEnumerable<KeyPairLookupModel> GetCommunicationTypes();
        IEnumerable<SimpleLookupModel> GetPreferredContactTypes();
        IEnumerable<KeyPairLookupModel> GetDisabilityTypes();

        IEnumerable<SimpleLookupModel> GetTerminationReasons();

        // tol lookups
        IEnumerable<KeyPairLookupModel> GetParkingTypes();
        IEnumerable<KeyPairLookupModel> GetASBCategories();
        IEnumerable<KeyPairLookupModel> GetComplaintTypes();
        IEnumerable<KeyPairLookupModel> GetAbandonedTypes();

        
    }


    public class LookupService : ILookupService
    {
        private ITitlesRepo _titlesRepo;
        private IMaritalStatusRepo _maritalStatusRepo;
        private IEthnicityRepo _ethnicityRepo;
        private IReligionRepo _religionRepo;
        private ISexualOrientationRepo _sexualOrientationRepo;
        private ICommunicationRepo _communicationRepo;
        private IPreferredContactRepo _preferredContactRepo;
        private ILanguageRepo _languageRepo;
        private IDisabilityRepo _disabilityRepo;
        private ITerminationReasonRepo _terminationReasonRepo;

        // generic tol lookup repo
        private ITolLookupRepo _tolLookupRepo;


        public LookupService(ITitlesRepo titlesRepo,
                             IMaritalStatusRepo maritalStatusRepo,
                             IEthnicityRepo ethnicityRepo,
                             IReligionRepo religionRepo,
                             ISexualOrientationRepo sexualOrientationRepo,
                             ICommunicationRepo communicationRepo,
                             IPreferredContactRepo preferredContactRepo,
                             ILanguageRepo languageRepo,
                             IDisabilityRepo disabilityRepo,
                             ITolLookupRepo tolLookupRepo,
                             ITerminationReasonRepo terminationReasonRepo
                             )
        {

            _titlesRepo = titlesRepo;
            _maritalStatusRepo = maritalStatusRepo;
            _ethnicityRepo = ethnicityRepo;
            _religionRepo = religionRepo;
            _sexualOrientationRepo = sexualOrientationRepo;
            _communicationRepo = communicationRepo;
            _preferredContactRepo = preferredContactRepo;
            _languageRepo = languageRepo;
            _disabilityRepo = disabilityRepo;
            _tolLookupRepo = tolLookupRepo;
            _terminationReasonRepo = terminationReasonRepo;
        }


        // rsl lookup types
        #region "Rsl lookup types"
        public IEnumerable<SimpleLookupModel> GetGenders()
        {
            var lookup = new List<SimpleLookupModel> { new SimpleLookupModel{ Id = "Male",  ItemName = "Male" },
                                                  new SimpleLookupModel{ Id = "Female", ItemName = "Female" } };

            return lookup.MapToEnumerable<SimpleLookupModel>();

        }
        public IEnumerable<KeyPairLookupModel> GetTitles()
        {
            return _titlesRepo.GetAll().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetMaritalStatusTypes()
        {
            return _maritalStatusRepo.GetAll().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetEthnicityTypes()
        {
            return _ethnicityRepo.GetAll().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetReligionTypes()
        {
            return _religionRepo.GetAll().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetSexualOrientationTypes()
        {
            return _sexualOrientationRepo.GetAll().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetCommunicationTypes()
        {
            var comms = _communicationRepo.GetAll();
            var ret = comms.MapToEnumerable<KeyPairLookupModel>();
            return ret;
        }

        public IEnumerable<SimpleLookupModel> GetPreferredContactTypes()
        {
            return _preferredContactRepo.GetAll().MapToEnumerable<SimpleLookupModel>();
        }

        public IEnumerable<SimpleLookupModel> GetLanguageTypes()
        {
            return _languageRepo.GetAll().MapToEnumerable<SimpleLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetDisabilityTypes()
        {
            return _disabilityRepo.GetAll().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<SimpleLookupModel> GetTerminationReasons()
        {
            return _terminationReasonRepo.GetAll().MapToEnumerable<SimpleLookupModel>();
        }
        #endregion

        // tol generic lookup types
        #region "Tol Lookup Types"
        public IEnumerable<KeyPairLookupModel> GetParkingTypes()
        {
            return _tolLookupRepo.GetParkingTypes().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetASBCategories()
        {
            return _tolLookupRepo.GetASBCategories().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetComplaintTypes()
        {
            return _tolLookupRepo.GetComplaintTypes().MapToEnumerable<KeyPairLookupModel>();
        }

        public IEnumerable<KeyPairLookupModel> GetAbandonedTypes()
        {
            return _tolLookupRepo.GetAbandonedTypes().MapToEnumerable<KeyPairLookupModel>();
        }
        #endregion
    }
}
