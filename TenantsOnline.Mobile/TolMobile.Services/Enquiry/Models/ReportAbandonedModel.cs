﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Lookups
{
    public class ReportAbandonedModel
    {        
        public int ReportType { get; set; }
        [Required]
        public string LocationDescription { get; set; }
        public string AdditionalInformation { get; set; }
    }
}
