﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Lookups
{
    public class RequestParkingModel
    {        
        public int ParkingType { get; set; }        
        public string AdditionalInformation { get; set; }
    }
}
