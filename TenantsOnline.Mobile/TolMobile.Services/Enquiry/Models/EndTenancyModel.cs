﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Lookups
{
    public class EndTenancyModel
    {
        [Required]
        public string Reason { get; set; }
        public DateTime TerminationDate { get; set; }
    }
}
