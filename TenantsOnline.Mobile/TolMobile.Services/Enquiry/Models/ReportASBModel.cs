﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Data.Models.Lookups
{
    public class ReportASBModel
    {        
        public int Type { get; set; }
        [Required]
        public string ProblemDescription { get; set; }
    }
    
}
