﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;
using TolMobile.Data.Context;
using System.Data.Objects;
using TolMobile.Data.Models.Lookups;

namespace TolMobile.Services.Users
{
    public interface IEnquiryService
    {
        void RequestTermination(EndTenancyModel model);
        
        void RequestParkingType(RequestParkingModel model);

        void ReportComplaint(ReportComplaintModel complaint);
        void ReportASB(ReportASBModel model);

        void ReportAbandonedType(ReportAbandonedModel model);
        void ReportAbandonedType(ReportAbandonedModel model, string guidFileName);        
    }

    public class EnquiryService : IEnquiryService
    {
        private enum NatureIds
        {
            // keep in sync with C_NATURE table - TODO: improve this so they don't need to be synced
            Termination = 50,
            ServiceComplaint = 51,
            RequestParkingType = 52,
            ReportASB = 53,
            ReportAbandoned = 54
        }

        private RSLEntityModelContext _ctx;
        private IAuthenticationService _authService;
        private int OutputResultId;

        private int CurrentUserId
        {
            get
            {
                return _authService.UserId;
            }
        }

        public EnquiryService(ITolDbContextProvider provider, IAuthenticationService authService)
        {
            _ctx = provider.DataContext;
            _authService = authService;
        }

        public void RequestTermination(EndTenancyModel model)
        {
            if (model.TerminationDate > DateTime.Now)
            {
                _ctx.RequestTermination(model.TerminationDate, DateTime.Now, model.Reason, int.Parse(ApplicationConstants.CustomerEnquiryStatusNew),
                    _authService.TenancyId, CurrentUserId, (int)NatureIds.Termination, new ObjectParameter("terminationid", this.OutputResultId));
            }
            else
            {
                throw new ModelValidationException("Termination Date", "The requested termination date must be in the future");
            }
        }

        public void RequestParkingType(RequestParkingModel model)
        {
            _ctx.RequestParkingType(DateTime.Now, model.AdditionalInformation, int.Parse(ApplicationConstants.CustomerEnquiryStatusNew), _authService.TenancyId,
               _authService.UserId, (int)NatureIds.RequestParkingType, model.ParkingType, new ObjectParameter("parkingSpaceId", this.OutputResultId));
        }       

        public void ReportComplaint(ReportComplaintModel model)
        {
            _ctx.ReportComplaint(model.Type, DateTime.Now, model.ProblemDescription, int.Parse(ApplicationConstants.CustomerEnquiryStatusNew), _authService.TenancyId,
                _authService.UserId, (int)NatureIds.ServiceComplaint, new ObjectParameter("complaintid", this.OutputResultId));
        }

        public void ReportASB(ReportASBModel model)
        {
            _ctx.ReportASB(DateTime.Now, model.ProblemDescription, int.Parse(ApplicationConstants.CustomerEnquiryStatusNew), _authService.TenancyId,
                _authService.UserId, (int)NatureIds.ReportASB, model.Type, new ObjectParameter("asbID", this.OutputResultId));
        }

        public void ReportAbandonedType(ReportAbandonedModel model)
        {
            _ctx.ReportAbandoned(DateTime.Now, model.AdditionalInformation, int.Parse(ApplicationConstants.CustomerEnquiryStatusNew), _authService.TenancyId,
                _authService.UserId, (int)NatureIds.ReportAbandoned, model.LocationDescription, model.ReportType, new ObjectParameter("abandonedId", this.OutputResultId));
        }
  
        public void ReportAbandonedType(ReportAbandonedModel model, string guidFileName)
        {
            _ctx.ReportAbandonedWithImage(DateTime.Now, model.AdditionalInformation, int.Parse(ApplicationConstants.CustomerEnquiryStatusNew), _authService.TenancyId,
                _authService.UserId, (int)NatureIds.ReportAbandoned, model.LocationDescription, model.ReportType, guidFileName, new ObjectParameter("abandonedId", this.OutputResultId));
        }            
    }
}
