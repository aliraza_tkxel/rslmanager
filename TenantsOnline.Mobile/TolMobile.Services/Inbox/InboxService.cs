﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TolMobile.Data;
using TolMobile.Extensions;
using TolMobile.Data.Models.Users;
using AutoMapper;
using TolMobile.Services.Exceptions;
using TolMobile.Data.Context;
using System.Data.Objects;
using TolMobile.Data.Models.Lookups;

namespace TolMobile.Services.Users
{
    public interface IInboxService
    {
        IEnumerable<EnquiryLineModel> GetInboxEnquiries(string responseStatus);
        int GetCustomerEnquiriesCount();
        int GetCustomerEnquiryResponseCount();
        int AddCustomerResponse(int? enquiryId, string notes, DateTime responseDate);
        IEnumerable<EnquiryResponseLineModel> GetEnquiryResponses(int? enquiryLogId);
    }

    public class InboxService : IInboxService
    {
        private RSLEntityModelContext _ctx;
        private IAuthenticationService _authService;

        private int CurrentUserId
        {
            get
            {
                return _authService.UserId;
            }
        }

        public InboxService(ITolDbContextProvider provider, IAuthenticationService authService)
        {
            _ctx = provider.DataContext;
            _authService = authService;
        }

        public IEnumerable<EnquiryLineModel> GetInboxEnquiries(string responseStatus)
        {
            var enquiries = _ctx.GetCustomerEnquiries(CurrentUserId, responseStatus);
            return enquiries.MapToEnumerable<EnquiryLineModel>();
        }

        public IEnumerable<EnquiryResponseLineModel> GetEnquiryResponses(int? enquiryLogId)
        {
            var responses = _ctx.GetCustomerEnquiryResponses(enquiryLogId);
            return responses.MapToEnumerable<EnquiryResponseLineModel>();
        }

        public int GetCustomerEnquiriesCount()
        {
            var result = _ctx.GetCustomerEnquiriesCount(CurrentUserId).FirstOrDefault();
            return result.Value;
        }

        public int GetCustomerEnquiryResponseCount()
        {
            var result = _ctx.GetCustomerEnquiryResponseCount(CurrentUserId).FirstOrDefault();
            return result.Value;
        }

        public int AddCustomerResponse(int? enquiryId, string notes, DateTime responseDate)
        {
            var customerResponseId = new ObjectParameter("customerResponseId",typeof(int));
            _ctx.AddCustomerEnquiryResponse(enquiryId, notes, responseDate, customerResponseId);
            return Convert.ToInt32(customerResponseId.Value);
        }

        public int GetCustomerEnquiryHistory(int enquiryId)
        {
            var result = _ctx.GetCustomerEnquiryResponseCount(CurrentUserId).FirstOrDefault();
            return result.Value;
        }

    }
}
