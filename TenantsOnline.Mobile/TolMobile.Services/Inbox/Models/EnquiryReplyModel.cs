﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class EnquiryReplyModel
    {        
        public int EnquiryLogId { get; set; }

        [Required]
        public string Response { get; set; }
    }
}
