﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class EnquiryResponseLineModel
    {
        public int EnquiryLogId;
        public DateTime LastActionDate;
        public string Notes;
        public int JournalId;
        public int HistoryId;
        public int LastActionUser;
    }
}
