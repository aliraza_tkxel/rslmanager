﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TolMobile.Services
{
    public class EnquiryLineModel
    {
        public DateTime CreationDate;
        public int EnquiryLogID;
        public int ImageKeyFlag;
        public string Item;
        public int ItemNatureID;
        public int? JournalID;
        public string Nature;
        public string ResponseStatus;
        public string Description;
    }
}
