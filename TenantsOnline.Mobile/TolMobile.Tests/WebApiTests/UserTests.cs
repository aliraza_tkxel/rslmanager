﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Reflection;
using TolMobile.Core.AutoMapper;
using StructureMap;
using TolMobile.Services.Authentication;
using Moq;
using TolMobile.Tests.WebApiTests;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;

namespace TolMobile.Tests
{
    [TestFixture]
    public class UserTests : BaseWebApiTest
    {
        private const string BaseEndPoint = "http://tol/webapi/user";
        [TestFixtureSetUp]
        public void Init()
        {
            
        }

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        [Test]
        [Ignore]
        public void Test_AuthenticationApiController()
        {
            var newModel = new UserDetailsModel { Id = 1,  Title = 1, FirstName = "Bill", LastName = "Smith", Gender = "Male", NINumber = "123", DateOfBirth = new DateTime(1990,2,1), MaritalStatus = 1, Ethnicity =  2 };

            var authMock = new Mock<IWebApiAuthenticationService>();
            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.UpdateUser(It.IsAny<UserDetailsModel>())).Returns(newModel);
            
            
            ObjectFactory.Initialize(x => x.For<IWebApiAuthenticationService>().Use(authMock.Object));
            ObjectFactory.Initialize(x => x.For<IUserService>().Use(userServiceMock.Object));        

            var client = new TestClient(BaseEndPoint);

            var result = client.Put<UserDetailsModel>(newModel);

            Assert.That(result.FirstName, Is.EqualTo(newModel.FirstName));
            Assert.That(result.LastName, Is.EqualTo(newModel.LastName));
            Assert.That(result.Id, Is.EqualTo(newModel.Id));
        }
       
    }
}
