﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Reflection;
using TolMobile.Core.AutoMapper;
using StructureMap;
using TolMobile.Services.Authentication;
using Moq;
using TolMobile.Tests.WebApiTests;
using TolMobile.Services;
using System.Net.Http;
using TolMobile.Data.Models.Authentication;

namespace TolMobile.Tests
{
    [TestFixture]
    public class AuthTests : BaseWebApiTest
    {

        private const string BaseEndPoint = "http://tol/webapi/auth";

        [TestFixtureSetUp]
        public void Init()
        {
         
        }

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        [Test]
        public void Test_AuthenticationApiController_WithIncorrectCredentials_ThrowsUnauathorisedException()
        {
            var authMock = new Mock<IAuthenticationService>();
            
            ObjectFactory.Initialize(x => x.For<IAuthenticationService>().Use(authMock.Object));
            
            var client = new TestClient(BaseEndPoint);          
            Assert.That(() =>  client.Post<LoginModel>(new LoginModel { Username = "test", Password = "pwd" }),
                       Throws.Exception.TypeOf<HttpRequestException>());         
        }

        [Test]
        public void Test_AuthenticationApiController_WithCorrectCredentials_ReturnsAuthToken()
        {
            var authMock = new Mock<IAuthenticationService>();
            var authToken = Guid.NewGuid().ToString();
            var authModel = new AuthResponseModel { AuthToken = authToken };
            authMock.Setup(x => x.AuthenticateUser(It.IsAny<LoginModel>())).Returns(authModel);

            ObjectFactory.Initialize(x => x.For<IAuthenticationService>().Use(authMock.Object));

            var client = new TestClient(BaseEndPoint);
            var result = client.Post<LoginModel, AuthResponseModel>(new LoginModel { Username = "test", Password = "pwd" });
            
            Assert.That(result.AuthToken, Is.EqualTo(authToken));
        }
    }
}
