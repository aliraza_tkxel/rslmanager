﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using StructureMap;
using System.Web.Script.Serialization;

namespace TolMobile.Tests.WebApiTests
{
    public class TestClient
    {
        protected readonly HttpClient _httpClient;
        protected readonly string _endpoint;

        public HttpStatusCode LastStatusCode { get; set; }

        public TestClient(string endpoint, bool enableAuthHandler = false)
        {
            _endpoint = endpoint;

            var config = new HttpConfiguration
            {
                DependencyResolver = new WebApiDependencyResolver(ObjectFactory.Container)
            };

            config.Routes.MapHttpRoute("LookupApi", "webapi/lookup/{action}", new { Controller = "Lookup" } );
            config.Routes.MapHttpRoute("default", "webapi/{controller}/{id}", new { id = RouteParameter.Optional });

            if (enableAuthHandler)
            {
                config.MessageHandlers.Add(new WebApiAuthenticationHandler(config));
            }

            _httpClient = new HttpClient(new HttpServer(config));
        }

        public T Post<T>(T model)
        {
            var serializer = new JavaScriptSerializer();
            var content = serializer.Serialize(model);

            var response = _httpClient.PostAsync(_endpoint, new StringContent(content, Encoding.UTF8, "application/json")).Result;
            response.EnsureSuccessStatusCode();
            return response.Content.ReadAsAsync<T>().Result;
        }

        public TOut Post<T, TOut>(T model)
        {
            var serializer = new JavaScriptSerializer();
            var content = serializer.Serialize(model);

            var response = _httpClient.PostAsync(_endpoint, new StringContent(content, Encoding.UTF8, "application/json")).Result;
            response.EnsureSuccessStatusCode();
            return response.Content.ReadAsAsync<TOut>().Result;
        }

        public T Put<T>(T model)
        {
            return Put<T, T>(model);            
        }

        public TOut Put<T, TOut>(T model)
        {
            var serializer = new JavaScriptSerializer();
            var content = serializer.Serialize(model);

            var response = _httpClient.PutAsync(_endpoint, new StringContent(content, Encoding.UTF8, "application/json")).Result;
            response.EnsureSuccessStatusCode();
            return response.Content.ReadAsAsync<TOut>().Result;
        }

        public T Get<T>() where T : class
        {
            var response = _httpClient.GetAsync(_endpoint).Result;
            response.EnsureSuccessStatusCode(); // need this to throw exception to unit test

            return response.Content.ReadAsAsync<T>().Result;
        }
    }
}
