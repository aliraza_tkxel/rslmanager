﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using StructureMap;
using System.Web.Script.Serialization;

namespace TolMobile.Tests.WebApiTests
{
    public class TestClient
    {
        protected readonly HttpClient _httpClient;
        protected readonly string _endpoint;

        public HttpStatusCode LastStatusCode { get; set; }

        public TestClient(string endpoint, bool enableAuthHandler = false)
        {
            _endpoint = endpoint;

            var config = new HttpConfiguration
            {
                DependencyResolver = new WebApiDependencyResolver(ObjectFactory.Container)
            };

            config.Routes.MapHttpRoute("default", "webapi/{controller}/{id}", new { id = RouteParameter.Optional });

            if (enableAuthHandler)
            {
                config.MessageHandlers.Add(new WebApiAuthenticationHandler());
            }

            _httpClient = new HttpClient(new HttpServer(config));
        }

        public JObject Post<T>(T model)
        {
            var serializer = new JavaScriptSerializer();
            var content = serializer.Serialize(model);

            var response = _httpClient.PostAsync(_endpoint, new StringContent(content, Encoding.UTF8, "application/json")).Result;
            response.EnsureSuccessStatusCode();
            return response.Content.ReadAsAsync<JObject>().Result;
        }

        public T Get<T>() where T : class
        {
            var response = _httpClient.GetAsync(_endpoint).Result;
            response.EnsureSuccessStatusCode(); // need this to throw exception to unit test

            return response.Content.ReadAsAsync<T>().Result;
        }
    }
}
