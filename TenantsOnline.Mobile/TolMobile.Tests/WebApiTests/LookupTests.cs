﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Reflection;
using TolMobile.Core.AutoMapper;
using StructureMap;
using TolMobile.Services.Authentication;
using Moq;
using TolMobile.Tests.WebApiTests;
using TolMobile.Services;
using TolMobile.Data;
using TolMobile.Services.Users;
using TolMobile.Data.Models.Lookups;
using TolMobile.Core.StructureMap;
using TolMobile.Data.Repositories.Lookup;
using TolMobile.Data.Context;
using TolMobile.Data.Models.Users;
using TolMobile.Data.Models;
using TolMobile.Controllers;

namespace TolMobile.Tests
{
    [Ignore]
    [TestFixture]
    public class LookupTests : BaseWebApiTest
    {
        private const string BaseEndPoint = "http://tol/webapi/lookup/";

        [TestFixtureSetUp]
        public void Init()
        {
            Assembly.Load("TolMobile");
            Assembly.Load("TolMobile.Data");
            Assembly.Load("TolMobile.Services");
            StructureMapBootStrap.Initialize();
        }

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        [Test]        
        public void test1()
        {
            //var x = ObjectFactory.GetInstance<LookupController>();
            //var a = ObjectFactory.GetInstance<ITolDbContextProvider>();
            //var b = ObjectFactory.GetInstance<IAnotherLookupRepo<UserDetailsModel>>();

            //var s = ObjectFactory.GetInstance<ILookupRepo<G_TITLE>>();
            //var z = ObjectFactory.GetInstance<ILookupService2>();

        }

        //[TestCase("genders")]
        //public void Test_GetGenders(string resource)
        //{
        //    var serviceMock = new Mock<ILookupService>();
        //    var items = new List<SimpleLookupModel>  
        //    { 
        //           new SimpleLookupModel { ItemName = "Male" },
        //           new SimpleLookupModel { ItemName = "Female" } 
        //    };

        //    serviceMock.Setup(x => x.GetGenders()).Returns(items);
        //    ObjectFactory.Initialize(x => x.For<ILookupService>().Use(serviceMock.Object));

        //    var client = new TestClient(BaseEndPoint + resource);

        //    var result = client.Get<IEnumerable<SimpleLookupModel>>();
        //    CollectionAssert.AreEqual(result.ToList(), items);
            
        //}

        //[TestCase("titles")]
        //public void Test_GetTitles(string resource)
        //{
        //    var serviceMock = new Mock<ILookupService>();
        //    var items = new List<KeyPairLookupModel>  
        //    { 
        //           new KeyPairLookupModel { ItemName = "Item 1" },
        //           new KeyPairLookupModel { ItemName = "Item 2" } 
        //    };

        //    serviceMock.Setup(x => x.GetTitles()).Returns(items);
        //    ObjectFactory.Initialize(x => x.For<ILookupService>().Use(serviceMock.Object));

        //    var client = new TestClient(BaseEndPoint + resource);

        //    var result = client.Get<IEnumerable<SimpleLookupModel>>();
        //    CollectionAssert.AreEqual(result, items);
        //}

        //[TestCase("occupations")]
        //public void Test_GetOccupations(string resource)
        //{
        //    var serviceMock = new Mock<ILookupService>();
        //    var items = new List<SimpleLookupModel>  
        //    { 
        //           new SimpleLookupModel { ItemName = "Item 1" },
        //           new SimpleLookupModel { ItemName = "Item 2" } 
        //    };

        //    serviceMock.Setup(x => x.Get()).Returns(items);
        //    ObjectFactory.Initialize(x => x.For<ILookupService>().Use(serviceMock.Object));

        //    var client = new TestClient(BaseEndPoint + resource);

        //    var result = client.Get<IEnumerable<SimpleLookupModel>>();
        //    CollectionAssert.AreEqual(result, items);
        //}

        //[TestCase("status")]
        //public void Test_GetStatus(string resource)
        //{
        //    var serviceMock = new Mock<ILookupService>();
        //    var items = new List<SimpleLookupModel>  
        //    { 
        //           new SimpleLookupModel { ItemName = "Item 1" },
        //           new SimpleLookupModel { ItemName = "Item 2" } 
        //    };

        //    serviceMock.Setup(x => x.GetMaritalStatusTypes()).Returns(items);
        //    ObjectFactory.Initialize(x => x.For<ILookupService>().Use(serviceMock.Object));

        //    var client = new TestClient(BaseEndPoint + resource);

        //    var result = client.Get<IEnumerable<SimpleLookupModel>>();
        //    CollectionAssert.AreEqual(result, items);
        //}
    }
}
