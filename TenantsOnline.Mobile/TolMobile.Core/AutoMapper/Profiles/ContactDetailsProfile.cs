﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TolMobile.Data.Models.Users;
using TolMobile.Services;
using TolMobile.Data.Context;

namespace TolMobile.Core.AutoMapper.Profiles
{
    public class ContactDetailsProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ContactDetails"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<C_ADDRESS, ContactDetailsModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(src => src.CUSTOMERID))
                .ForMember(x => x.Telephone, opt => opt.MapFrom(src => src.TEL))
                .ForMember(x => x.Mobile, opt => opt.MapFrom(src => src.MOBILE))
                .ForMember(x => x.Email, opt => opt.MapFrom(src => src.EMAIL))

                .ForMember(x => x.HouseNo, opt => opt.MapFrom(src => src.HOUSENUMBER))
                .ForMember(x => x.Address1, opt => opt.MapFrom(src => src.ADDRESS1))
                .ForMember(x => x.Address2, opt => opt.MapFrom(src => src.ADDRESS2))
                .ForMember(x => x.Address3, opt => opt.MapFrom(src => src.ADDRESS3))
                .ForMember(x => x.County, opt => opt.MapFrom(src => src.COUNTY))
                .ForMember(x => x.TownCity, opt => opt.MapFrom(src => src.TOWNCITY))
                .ForMember(x => x.PostCode, opt => opt.MapFrom(src => src.POSTCODE));

            Mapper.CreateMap<ContactDetailsModel, C_ADDRESS>()
              .ForMember(x => x.CUSTOMERID, opt => opt.MapFrom(src => src.Id))
              .ForMember(x => x.TEL, opt => opt.MapFrom(src => src.Telephone))
              .ForMember(x => x.MOBILE, opt => opt.MapFrom(src => src.Mobile))
              .ForMember(x => x.EMAIL, opt => opt.MapFrom(src => src.Email))

              .ForMember(x => x.HOUSENUMBER, opt => opt.MapFrom(src => src.HouseNo))
              .ForMember(x => x.ADDRESS1, opt => opt.MapFrom(src => src.Address1))
              .ForMember(x => x.ADDRESS2, opt => opt.MapFrom(src => src.Address2))
              .ForMember(x => x.ADDRESS3, opt => opt.MapFrom(src => src.Address3))
              .ForMember(x => x.COUNTY, opt => opt.MapFrom(src => src.County))
              .ForMember(x => x.TOWNCITY, opt => opt.MapFrom(src => src.TownCity))
              .ForMember(x => x.POSTCODE, opt => opt.MapFrom(src => src.PostCode));

        }
    }
}
