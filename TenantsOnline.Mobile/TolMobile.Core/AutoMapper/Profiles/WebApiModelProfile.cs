﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TolMobile.Data.Models.Users;
using TolMobile.Services;
using TolMobile.Data.Context;
using TolMobile.Extensions;

namespace TolMobile.Core.AutoMapper.Profiles
{
    public class WebApiModelProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModel"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserDetailsModel, TO_LOGIN>();
            Mapper.CreateMap<TO_LOGIN, UserDetailsModel>()
                .ForMember(x => x.DateOfBirth, opt => opt.Ignore());

            Mapper.CreateMap<C_ADDRESS, UserDetailsModel>();
            Mapper.CreateMap<UserDetailsModel, C_ADDRESS>();

            Mapper.CreateMap<TO_OFFICE, OfficeModel>()
                  .ForMember(x => x.Latitude, opt => opt.MapFrom(src => src.LATITUDE.ToString()))
                  .ForMember(x => x.Longitude, opt => opt.MapFrom(src => src.LONGITUDE.ToString()));

            Mapper.CreateMap<I_NEWS, NewsModel>()
                  .ForMember(x => x.DateCreated, opt => opt.MapFrom(src => ((DateTime)src.DateCreated).ToShortDateString()));

            Mapper.CreateMap<RentLine, RentLineModel>()
                  .ForMember(x => x.Balance, opt => opt.MapFrom(src => src.Breakdown));

            Mapper.CreateMap<RegisterModel, RegisterUser>();
            Mapper.CreateMap<RegistrationResult, RegistrationResultModel>();

            Mapper.CreateMap<EnquiryLine, EnquiryLineModel>()
                .ForMember(x => x.Nature, opt => opt.MapFrom(src => src.NATURE))
                .ForMember(x => x.ImageKeyFlag, opt => opt.MapFrom(src => src.ImageKeyFlag.ToInt32()))
                .ForMember(x => x.Description, opt => opt.MapFrom(src => src.EnqDescription));

            Mapper.CreateMap<EnquiryResponseLine, EnquiryResponseLineModel>()
                .ForMember(x => x.EnquiryLogId, opt => opt.MapFrom(src => src.ENQID));
        }
    }
}
