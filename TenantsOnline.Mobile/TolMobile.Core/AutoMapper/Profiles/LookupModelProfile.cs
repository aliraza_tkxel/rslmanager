﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TolMobile.Data.Models.Lookups;
using TolMobile.Data.Context;

namespace TolMobile.Core.AutoMapper.Profiles
{
    public class LookupModelProfile : Profile
    {
        public override string ProfileName
        {
            get { return "LookupModelProfile"; }
        }

        protected override void Configure()
        {                
            // generic tol lookup values
            CreateMap<TolLookupValue, KeyPairLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.id))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.val));

            CreateMap<G_TITLE, KeyPairLookupModel>()
                .ForMember(x=>x.Id, opt=>opt.MapFrom(x=>x.TITLEID))
                .ForMember(x=>x.ItemName, opt=>opt.MapFrom(x=>x.DESCRIPTION));

            CreateMap<G_ETHNICITY, KeyPairLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.ETHID))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<G_MARITALSTATUS, KeyPairLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.MARITALSTATUSID))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<G_RELIGION, KeyPairLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.RELIGIONID))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<G_SEXUALORIENTATION, KeyPairLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.SEXUALORIENTATIONID))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<G_COMMUNICATION, KeyPairLookupModel>()
              .ForMember(x => x.Id, opt => opt.MapFrom(x => x.COMMUNICATIONID))
              .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<G_PREFEREDCONTACT, SimpleLookupModel>()                
              .ForMember(x => x.Id, opt => opt.MapFrom(x => x.DESCRIPTION))
              .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<G_LANGUAGES, SimpleLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.LANGUAGE))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.LANGUAGE));

            CreateMap<G_DISABILITY, KeyPairLookupModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.DISABILITYID))
               .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));

            CreateMap<C_TERMINATION_REASON, SimpleLookupModel>()
              .ForMember(x => x.Id, opt => opt.MapFrom(x => x.DESCRIPTION))
              .ForMember(x => x.ItemName, opt => opt.MapFrom(x => x.DESCRIPTION));
        }
    }
}
