﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TolMobile.Data.Models.Users;
using TolMobile.Services;
using TolMobile.Data.Context;
using TolMobile.Core.AutoMapper.Converters;

namespace TolMobile.Core.AutoMapper.Profiles
{
    public class UserDetailsProfile : Profile
    {
        public override string ProfileName
        {
            get { return "UserDetails"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<string, IEnumerable<int>>()
                .ConvertUsing<CsvToListConverter>();

            Mapper.CreateMap<IEnumerable<int>, string>()
                .ConvertUsing<ListToCsvConverter>();

            Mapper.CreateMap<C__CUSTOMER, UserDetailsModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(src => src.CUSTOMERID))
                .ForMember(x => x.Title, opt => opt.MapFrom(src => src.TITLE))
                .ForMember(x => x.FirstName, opt => opt.MapFrom(src => src.FIRSTNAME))
                .ForMember(x => x.LastName, opt => opt.MapFrom(src => src.LASTNAME))
                .ForMember(x => x.MiddleName, opt => opt.MapFrom(src => src.MIDDLENAME))
                .ForMember(x => x.Gender, opt => opt.MapFrom(src => src.GENDER))
                .ForMember(x => x.NINumber, opt => opt.MapFrom(src => src.NINUMBER))
                .ForMember(x => x.DateOfBirth, opt => opt.MapFrom(src => src.DOB))

                .ForMember(x => x.MaritalStatus, opt => opt.MapFrom(src => src.MARITALSTATUS))
                .ForMember(x => x.Ethnicity, opt => opt.MapFrom(src => src.ETHNICORIGIN))
                .ForMember(x => x.Religion, opt => opt.MapFrom(src => src.RELIGION))
                .ForMember(x => x.SexualOrientation, opt => opt.MapFrom(src => src.SEXUALORIENTATION))

                .ForMember(x => x.Language, opt => opt.MapFrom(src => src.FIRSTLANGUAGE))
                .ForMember(x => x.Communication, opt => opt.MapFrom(src => src.COMMUNICATION))
                .ForMember(x => x.PreferredContact, opt => opt.MapFrom(src => src.PREFEREDCONTACT))                
                .ForMember(x => x.Disability, opt => opt.MapFrom(src => src.DISABILITY));                                                                     

            Mapper.CreateMap<UserDetailsModel, C__CUSTOMER>()
                .ForMember(x => x.CUSTOMERID, opt => opt.MapFrom(src => src.Id))
                .ForMember(x => x.TITLE, opt => opt.MapFrom(src => src.Title))
                .ForMember(x => x.FIRSTNAME, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(x => x.LASTNAME, opt => opt.MapFrom(src => src.LastName))
                .ForMember(x => x.MIDDLENAME, opt => opt.MapFrom(src => src.MiddleName))
                .ForMember(x => x.GENDER, opt => opt.MapFrom(src => src.Gender))
                .ForMember(x => x.NINUMBER, opt => opt.MapFrom(src => src.NINumber))
                .ForMember(x => x.DOB, opt => opt.MapFrom(src => src.DateOfBirth))

                .ForMember(x => x.MARITALSTATUS, opt => opt.MapFrom(src => src.MaritalStatus))
                .ForMember(x => x.ETHNICORIGIN, opt => opt.MapFrom(src => src.Ethnicity))
                .ForMember(x => x.RELIGION, opt => opt.MapFrom(src => src.Religion))
                .ForMember(x => x.SEXUALORIENTATION, opt => opt.MapFrom(src => src.SexualOrientation))

                .ForMember(x => x.FIRSTLANGUAGE, opt => opt.MapFrom(src => src.Language))
                .ForMember(x => x.COMMUNICATION, opt => opt.MapFrom(src => src.Communication))
                .ForMember(x => x.PREFEREDCONTACT, opt => opt.MapFrom(src => src.PreferredContact))
                .ForMember(x => x.DISABILITY, opt => opt.MapFrom(src => src.Disability));

        }
    }
}
