﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TolMobile.Core.AutoMapper.Profiles;

namespace TolMobile.Core.AutoMapper
{
    public class AutoMapperBootstrap
    {
        public static void Initialize()
        {
            Mapper.Initialize(m =>
            {
                m.AddProfile<WebApiModelProfile>();
                m.AddProfile<UserDetailsProfile>();
                m.AddProfile<ContactDetailsProfile>();

                m.AddProfile<LookupModelProfile>();   
            });
        }
    }
}
