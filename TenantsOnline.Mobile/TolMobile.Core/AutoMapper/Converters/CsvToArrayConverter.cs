﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace TolMobile.Core.AutoMapper.Converters
{
        // Automapper int to string
    public class CsvToListConverter : TypeConverter<string, IEnumerable<int>>
    {
        protected override IEnumerable<int> ConvertCore(string source)
        {
            var list = new List<int>();
            if (!string.IsNullOrEmpty(source))
            {
                list.AddRange(source.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => System.Convert.ToInt32(x)));
            }

            return list;
        }
    }

    public class ListToCsvConverter : TypeConverter<IEnumerable<int>, string>
    {
        protected override string ConvertCore(IEnumerable<int> values)
        {
            string serialise = string.Empty;
            foreach (var item in values)
            {
                serialise += item.ToString() + ",";
            }
            return serialise;
        }
    }
}
