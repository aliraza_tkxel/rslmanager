﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap.Configuration.DSL;
using TolMobile.Data.Context;
using StructureMap;
using TolMobile.Data.Repositories.Lookup;
using System.Diagnostics;
using TolMobile.Data;

namespace TolMobile.Core.StructureMap.Registries
{
    public class RepositoryRegistry : Registry
    {
        public RepositoryRegistry()
        {
            Scan(x =>
            {
                x.Assembly("TolMobile.Data");
                x.TheCallingAssembly();
                x.WithDefaultConventions();                
                //x.AddAllTypesOf(typeof(ILookupRepo<>));                      
            });
            
            For<ITolDbContextProvider>().Use(x => ObjectFactory.GetInstance<TolDbContextProvider>());            

            Debug.WriteLine(ObjectFactory.WhatDoIHave());
        }
    }
}
