﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap.Configuration.DSL;
using TolMobile.Data.Repositories.Lookup;

namespace TolMobile.Core.StructureMap.Registries
{
    public class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            Scan(x =>
            {
                x.Assembly("TolMobile.Services");
                x.TheCallingAssembly();
                x.WithDefaultConventions();            
            });

        }
    }
}
