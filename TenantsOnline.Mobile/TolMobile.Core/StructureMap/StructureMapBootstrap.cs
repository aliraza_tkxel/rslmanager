﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap;
using System.Diagnostics;
using TolMobile.Core.StructureMap.Registries;

namespace TolMobile.Core.StructureMap
{
    public static class StructureMapBootStrap
    {
        public static void Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<RepositoryRegistry>();
                x.AddRegistry<ServiceRegistry>();                        
                       
            });

            Debug.WriteLine(ObjectFactory.WhatDoIHave());

            
        }       
    }    
}
