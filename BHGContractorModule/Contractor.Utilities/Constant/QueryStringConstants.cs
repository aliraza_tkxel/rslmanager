﻿namespace Contractor.Utilities.Constant
{
    public class QueryStringConstants
    {

        #region "Common"
        public const string Page = "page";
        public const string SortBy = "sort";
        public const string SortOrder = "sortdir";
        #endregion

        #region "Health And Safety"
        public const string SearchType = "type";
        #endregion

    }
}
