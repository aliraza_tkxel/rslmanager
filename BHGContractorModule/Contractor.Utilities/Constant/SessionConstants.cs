﻿namespace Contractor.Utilities.Constant
{
    public class SessionConstants
    {
        public const string SessionUserId = "SESSION_USER_ID";
        public const string UserFullName = "UserFullName";
        public const string EmployeeId = "UserEmployeeId";
        public const string HealthAndSafetyInfo = "HealthAndSafetyInfo";
        public const string SearchType = "type";
        public const string SearchValue = "searchValue";
    }
}
