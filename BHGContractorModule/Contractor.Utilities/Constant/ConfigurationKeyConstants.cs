﻿namespace Contractor.Utilities.Constant
{
    public class ConfigurationKeyConstants
    {           
        public const string PropertyDocumentUploadPath = "PropertyDocumentUploadPath";
        public const string SchemeDocumentUploadPath = "SchemeDocumentUploadPath";
        public const string BlockDocumentUploadPath = "BlockDocumentUploadPath";
       
    }
}
