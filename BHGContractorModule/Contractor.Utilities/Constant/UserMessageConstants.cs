﻿namespace Contractor.Utilities.Constant
{
    public class UserMessageConstants
    {
        public const string AsbestosSuccessMessage = "Asbestos is saved successfully.";
        public const string DocumentSuccessMessage = "Document is saved successfully.";
        public const string AccessLockdownMsg = "Your account is now locked, please contact the Systems Team for assistance.";
        public const string AccessLockdownWarningMsg = "You have one more login attempt before your access is locked down!";
        public const string LoginFailureMsg = "Your username and password do not appear to be correct!";
        public const string LoginSuccessMsg = "Authenticated";
        public const string ContractorOnlyErrorMsg = "Only contractors can login to contractor module, please contact the Systems Team for assistance.";
        public const string ErrorSendingEmailMsg = "An error occurred while sending email, please try again.";
    }
}
