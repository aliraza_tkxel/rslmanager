﻿namespace Contractor.Utilities.Constant
{
    public class ApplicationConstants
    {
        #region "Health And Safety"
        public const string TypeScheme = "scheme";
        public const string TypeBlock = "block";
        public const string TypeProperty = "property";
        public const string DocumentTypeAsbestos = "Asbestos";
        public const string DocumentCategoryCompliance = "Compliance";
        public const string UrgentAsbestosEmailSubject = "Urgent Asbestos Issue Raised";
        #endregion

        #region >>> Common <<<
        public const string LoginPath = "~/Home/Login";
        public const string UsersAccountDeactivated = "Your access to Contractor Module has been de-activiated. Please contact administrator.";
        public const string KeyDateFormat = "DateFormat";
        public const string KeyDateFormatModel = "DateFormatForModel";
        public const string VirtualDirectory = "VirtualDirectory";
        public const string ApplicationDateForamt = "dd-MM-yyyy";
        public const string ContractorUserType = "Contractors";
        #endregion
    }
}
