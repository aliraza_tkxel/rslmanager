﻿using Contractor.Utilities.Constant;
using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace Contractor.Utilities.Helper
{
    public class FileHelper
    {
        public static void WriteFileFromStream(Stream stream, string filePath)
        {
            using (FileStream fileToSave = new FileStream(filePath, FileMode.Create))
            {
                stream.CopyTo(fileToSave);
            }
        }

        public static void createDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(@path))
                {
                    Directory.CreateDirectory(@path);
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        public static string GetRefDocUploadPath()
        {
            string documentUploadPath = ConfigurationManager.AppSettings["DocumentUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return documentUploadPath;
        }

        public static string getUniqueFileName(string pfileName)
        {
            string fileName = Path.GetFileNameWithoutExtension(pfileName);
            string ext = Path.GetExtension(pfileName);
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileName + uniqueString;

            return fileName + ext;
        }

        public static string getFileSize(HttpPostedFileBase file)
        {
            string fileSize = null;
            if (file != null)
            {             
                fileSize = string.Format("{0}KB", Math.Ceiling(Convert.ToDecimal(file.ContentLength / 1024)).ToString());
            }
            return fileSize;
        }

        public static string getFileDirectoryPath(string filetype, string id)
        {            

            var fileDirectoryPath = string.Empty;
            if (filetype == ApplicationConstants.TypeScheme)
            {
                fileDirectoryPath = string.Format("{0}\\{1}\\Documents", ConfigHelper.GetSchemeDocUploadPath(), id.ToString());
            }
            else if (filetype == ApplicationConstants.TypeBlock)
            {
                fileDirectoryPath = string.Format("{0}\\{1}\\Documents", ConfigHelper.GetBlockDocUploadPath(), id.ToString());
            }
            else if (filetype == ApplicationConstants.TypeProperty)
            {
                fileDirectoryPath = string.Format("{0}\\{1}\\Documents", ConfigHelper.GetPropertyDocUploadPath(), id.ToString());
            }
            return fileDirectoryPath;
        }

    }
}