﻿using Contractor.Utilities.Constant;
using System;
using System.Web;

namespace Contractor.Utilities.Helper
{
    public class SessionManager
    {
        #region >>> Set / Get / Remove Session User Id <<<

        public static void SetSessionUserId(int userId)
        {
            HttpContext.Current.Session[SessionConstants.SessionUserId] = userId;
        }

        public static int GetSessionUserId()
        {
            if (HttpContext.Current.Session[SessionConstants.SessionUserId] != null)
            {
                return Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SessionUserId]);
            }

            return 0;
        }

        public static void RemoveSessionUserId()
        {
            if (HttpContext.Current.Session[SessionConstants.SessionUserId] != null)
            {
                HttpContext.Current.Session[SessionConstants.SessionUserId] = null;
            }
        }

        #endregion

        #region get set remove Request type

        public static void SetRequestType(string type)
        {
            HttpContext.Current.Session[SessionConstants.SearchType] = type;
        }

        public static string GetRequestType()
        {
            if (HttpContext.Current.Session[SessionConstants.SearchType] != null)
            {
                return HttpContext.Current.Session[SessionConstants.SearchType].ToString();
            }

            return "Property";
        }

        public static void RemoveRequestType()
        {
            if (HttpContext.Current.Session[SessionConstants.SearchType] != null)
            {
                HttpContext.Current.Session[SessionConstants.SearchType] = null;
            }
        }

        #endregion

        #region get set remove Search Value

        public static void SetSearchValue(string type)
        {
            HttpContext.Current.Session[SessionConstants.SearchValue] = type;
        }

        public static string GetSearchValue()
        {
            if (HttpContext.Current.Session[SessionConstants.SearchValue] != null)
            {
                return HttpContext.Current.Session[SessionConstants.SearchValue].ToString();
            }

            return string.Empty;
        }

        public static void RemoveSearchValue()
        {
            if (HttpContext.Current.Session[SessionConstants.SearchValue] != null)
            {
                HttpContext.Current.Session[SessionConstants.SearchValue] = null;
            }
        }

        #endregion
    }
}
