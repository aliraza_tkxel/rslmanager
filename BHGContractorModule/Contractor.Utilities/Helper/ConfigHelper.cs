﻿using Contractor.Utilities.Constant;
using System.Configuration;

namespace Contractor.Utilities.Helper
{
    public class ConfigHelper
    {

        #region "Get Property/Scheme/Block Doc Upload Path"
        public static string GetSchemeDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.SchemeDocumentUploadPath];
            if (configKey != null)
                return configKey + "\\Schemes";

            return returnVal;
        }

        public static string GetPropertyDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.PropertyDocumentUploadPath];
            if (configKey != null)
                return configKey + "";

            return returnVal;
        }

        public static string GetBlockDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.BlockDocumentUploadPath];
            if (configKey != null)
                return configKey + "\\Blocks";

            return returnVal;
        }

        #endregion

    }
}
