#@del /F /Q *.user
@PUSHD Contractor.Web
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD


@PUSHD Contractor.Utilities
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD Contractor.DataAccess
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD Contractor.BusinessObject
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD Contractor.BusinessLogic
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD


@PAUSE