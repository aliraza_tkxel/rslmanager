﻿namespace Contractor.BusinessLogic
{
    public class SchemeManager : BaseManager
    {
        public SchemeManager()
        {

        }

        #region "Get Scheme Address"
        public string GetSchemeAddress(int? schemeId)
        {
            return this.RepositoryUnit.schemeRepository.GetSchemeAddress(schemeId);
        }
        #endregion
    }

}
