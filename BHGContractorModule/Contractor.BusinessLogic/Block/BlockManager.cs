﻿using System.Linq;

namespace Contractor.BusinessLogic
{
    public class BlockManager : BaseManager
    {
        public BlockManager()
        {

        }

        #region "Get Block Address"
        public string GetBlockAddress(int? blockId)
        {
            string address = string.Empty;
            var block = this.RepositoryUnit.blockRepository.Find(b => b.BLOCKID == blockId).FirstOrDefault();
            if (block != null)
            {
                var adrsArray = new[] { block.BlockReference, block.BLOCKNAME, block.ADDRESS1, block.ADDRESS2, block.COUNTY, block.TOWNCITY, block.POSTCODE };
                address = string.Format("Block > {0}", string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s))));
            }

            return address;

        }
        #endregion
    }
}
