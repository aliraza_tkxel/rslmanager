﻿using Contractor.BusinessLogic.Login;

namespace Contractor.BusinessLogic
{
    public class ManagerProxy : IManagerProxy
    {
        private HealthAndSafetyManager _healthAndSafetyManager;
        public HealthAndSafetyManager HealthAndSafetyManager
        {
            get
            {
                if (_healthAndSafetyManager == null)
                {
                    _healthAndSafetyManager = new HealthAndSafetyManager();
                }
                return _healthAndSafetyManager;
            }
        }

        private LoginManager _LoginManager;
        public LoginManager LoginManager
        {
            get
            {
                if (_LoginManager == null)
                {
                    _LoginManager = new LoginManager();
                }
                return _LoginManager;
            }
        }

        private PropertiesManager _propertiesManager;
        public PropertiesManager PropertiesManager
        {
            get
            {
                if (_propertiesManager == null)
                {
                    _propertiesManager = new PropertiesManager();
                }
                return _propertiesManager;
            }
        }

        private SchemeManager _schemeManager;
        public SchemeManager SchemeManager
        {
            get
            {
                if (_schemeManager == null)
                {
                    _schemeManager = new SchemeManager();
                }
                return _schemeManager;
            }
        }


        private BlockManager _blockManager;
        public BlockManager BlockManager
        {
            get
            {
                if (_blockManager == null)
                {
                    _blockManager = new BlockManager();
                }
                return _blockManager;
            }
        }

        public void Dispose()
        {
            _healthAndSafetyManager = null;
            _LoginManager = null;
            _propertiesManager = null;
            _blockManager = null;
            _schemeManager = null;
        }
    }
}
