﻿using Contractor.DataAccess.UnitOfWork;

namespace Contractor.BusinessLogic
{
    public class BaseManager
    {
        protected readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IUnitOfWork _uow;
        public BaseManager()
        {

        }

        public BaseManager(IUnitOfWork repositryUnit)
        {
            this.RepositoryUnit = repositryUnit;
        }

        public IUnitOfWork RepositoryUnit
        {
            get
            {

                if (_uow == null)
                    _uow = new UnitOfWork();
                return _uow;
            }
            private set { _uow = value; }
        }

        public void Dispose()
        {
            this.RepositoryUnit = null;
        }
    }
}
