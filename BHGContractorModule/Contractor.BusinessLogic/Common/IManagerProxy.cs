﻿using Contractor.BusinessLogic.Login;
using System;

namespace Contractor.BusinessLogic
{
    public interface IManagerProxy: IDisposable
    {
        HealthAndSafetyManager HealthAndSafetyManager { get; }
        PropertiesManager PropertiesManager { get; }
        SchemeManager SchemeManager { get; }
        BlockManager BlockManager { get; }
        LoginManager LoginManager { get; }

    }
}
