﻿using Contractor.BusinessObject;
using Contractor.Utilities.Constant;
using System.Linq;

namespace Contractor.BusinessLogic
{
    public class HealthAndSafetyManager : BaseManager
    {

        public HealthAndSafetyManager()
        {

        }

        #region "Get Asbestos Listing"

        public AsbestosListingResponse GetAsbestosListing(AsbestosListingRequest model)
        {
            AsbestosListingResponse response = new AsbestosListingResponse();
            response = this.RepositoryUnit.asbestosRepository.GetAsbestosListing(model);

            var elements = this.RepositoryUnit.asbestosElementRepository.Find(x => x.other == false).ToList();
            response.lookUps.asbestosElements = elements.Select(x => new Lookup() { lookUpDescription = x.RISKDESCRIPTION, lookUpId = x.ASBESTOSID, order = x.sorder }).ToList().OrderBy(x => x.order).ToList();

            var riskLevels = this.RepositoryUnit.asbestosRiskLevelRepository.GetAll().ToList();
            response.lookUps.riskLevels = riskLevels.Select(x => new Lookup() { lookUpDescription = x.Description, lookUpId = x.AsbestosLevelId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var asbestosTypes = this.RepositoryUnit.asbestosTypeRepository.GetAll().ToList();
            response.lookUps.asbestosTypes = asbestosTypes.Select(x => new Lookup() { lookUpDescription = x.ASBRISKLEVELDESCRIPTION, lookUpId = x.ASBRISKLEVELID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var asbriskTypes = this.RepositoryUnit.asbRiskTypeRepository.GetAll().ToList();
            response.lookUps.types = asbriskTypes.Select(x => new Lookup() { lookUpDescription = x.ASBRISKID, lookUpIdString = x.ASBRISKID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var documentTypes = this.RepositoryUnit.documentTypeRepository.Find(x => x.Title == ApplicationConstants.DocumentTypeAsbestos).ToList();
            response.lookUps.documentTypes = documentTypes.Select(x => new Lookup() { lookUpDescription = x.Title, lookUpId = x.DocumentTypeId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var documentSubTypes = this.RepositoryUnit.documentSubTypeRepository.getAsbestosDocumentSubTypes();
            response.lookUps.documentSubTypes = documentSubTypes.Select(x => new Lookup() { lookUpDescription = x.Title, lookUpId = x.DocumentSubtypeId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            return response;
        }
        #endregion

        #region "Add Amend Asbestos"
        public void AddAmendAsbestos(AsbestosDetailRequest request)
        {
            this.RepositoryUnit.asbestosRepository.AddAmendAsbestos(request);
        }
        #endregion

        #region "Add Asbestos Document"
        public void AddAsbestosDocument(AsbestosDocumentRequest request)
        {
            this.RepositoryUnit.documentRepository.AddAsbestosDocument(request);
        }

        #endregion

        #region "Employee Email For Asbestos Email Notifications"

        public AsbestosEmailNotificationsResponse GetEmployeeEmailForAsbestosEmailNotifications(AsbestosEmailNotificationsRequest request)
        {
            return this.RepositoryUnit.asbestosRepository.GetEmployeeEmailForAsbestosEmailNotifications(request);
        }

        #endregion

    }
}
