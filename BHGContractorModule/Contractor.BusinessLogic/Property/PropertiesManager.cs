﻿using Contractor.BusinessObject.Property;
using System.Linq;

namespace Contractor.BusinessLogic
{
    public class PropertiesManager : BaseManager
    {
        public PropertiesManager()
        {

        }

        #region "Get Property Address"
        public string GetPropertyAddress(string propertyId)
        {
            string address = string.Empty;
            var property = this.RepositoryUnit.propertyRepository.Find(b => b.PROPERTYID == propertyId).FirstOrDefault();
            if (property != null)
            {
                var adrsArray = new[] { property.HOUSENUMBER, property.ADDRESS1, property.ADDRESS2, property.COUNTY, property.TOWNCITY, property.POSTCODE};
                address = string.Format("Property > {0}", string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s))));
            }

            return address;

        }

        #endregion

        public PropertySearchResponse SearchProperty(PropertySearchRequest request)
        {
            return this.RepositoryUnit.propertyRepository.SearchProperty(request);
        }

    }
}
