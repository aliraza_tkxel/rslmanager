﻿using Contractor.BusinessObject.Login;

namespace Contractor.BusinessLogic.Login
{
    public class LoginManager : BaseManager
    {

        public LoginManager()
        {

        }
        public LoginResponse LoginUser(LoginRequest loginRequest)
        {
            return this.RepositoryUnit.loginRepository.LoginUser(loginRequest);
        }
    }
}
