﻿using Contractor.BusinessObject.Login;

namespace Contractor.BusinessLogic.Login
{
    public interface ILoginManager
    {
        LoginResponse LoginUser(LoginRequest loginRequest);
    }
}
