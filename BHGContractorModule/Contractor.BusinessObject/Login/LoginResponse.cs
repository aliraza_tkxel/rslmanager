﻿using System;

namespace Contractor.BusinessObject.Login
{
    public class LoginResponse
    {
        public UserInfo UserInfo { get; set; } = new UserInfo();

        public byte? Threshold { get; set; }
        public bool? Locked { get; set; }
        public string ResponseMessage { get; set; }

    }

    public class UserInfo
    {
        public string EmployeeFullName { get; set; }
        public int? IsActive { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public int? TeamId { get; set; }
        public string TeamCode { get; set; }
        public string Gender { get; set; }
        public string JobRole { get; set; }
        public DateTime? Dob { get; set; }

    }
}
