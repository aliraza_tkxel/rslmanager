﻿using System.Collections.Generic;

namespace Contractor.BusinessObject
{
    public class AsbestosLookups
    {
        public List<Lookup> asbestosTypes { get; set; }
        public List<Lookup> asbestosElements { get; set; }
        public List<Lookup> types { get; set; }
        public List<Lookup> riskLevels { get; set; }
        public List<Lookup> documentTypes { get; set; }
        public List<Lookup> documentSubTypes { get; set; }

    }
}
