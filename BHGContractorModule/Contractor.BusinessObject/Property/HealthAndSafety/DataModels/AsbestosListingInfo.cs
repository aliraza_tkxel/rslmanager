﻿using System.Collections.Generic;

namespace Contractor.BusinessObject
{
   public  class AsbestosListingInfo
    {
        public List<AsbestosDetail> asbestosList { get; set; }
        public Pagination pagination { get; set; }
    }
}
