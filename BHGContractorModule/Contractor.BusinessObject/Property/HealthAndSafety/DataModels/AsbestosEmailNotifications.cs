﻿

namespace Contractor.BusinessObject
{
    public class AsbestosEmailNotifications
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string jobeRoleDescription { get; set; }
        public string workEmail { get; set; }
        public string PropertyAddress { get; set; }
    }
}
