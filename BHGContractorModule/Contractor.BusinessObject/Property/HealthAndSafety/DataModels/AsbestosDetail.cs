﻿using System;

namespace Contractor.BusinessObject
{
    public class AsbestosDetail
    {

        public int asbestosId { get; set; }
        public int? employeeId { get; set; }
        public int? asbestosTypeId { get; set; }
        public string asbestosType { get; set; }
        public int? asbestosElementId { get; set; }
        public string asbestosElement { get; set; }
        public string typeId { get; set; }
        public string type { get; set; }
        public int? riskLevelId { get; set; }
        public string riskLevel { get; set; }
        public string addedDate { get; set; }
        public DateTime? addedDateTime { get; set; }
        public string removedDate { get; set; }
        public DateTime? removedDateTime { get; set; }
        public string notes { get; set; }
        public bool? isUrgentActionRequired { get; set; }
        public int? createdBy { get; set; }
        public string createdByEmployeeName { get; set; }
        public string createdByFullName { get; set; }

    }
}
