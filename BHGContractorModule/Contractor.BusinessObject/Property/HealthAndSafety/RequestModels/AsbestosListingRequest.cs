﻿namespace Contractor.BusinessObject
{
    public class AsbestosListingRequest
    {
        public int? blockId { get; set; }
        public int? schemeId { get; set; }
        public string propertyId { get; set; }
        public string type { get; set; }
        public string sortBy { get; set; } = "asbestosId";
        public string sortOrder { get; set; } = "DESC";
        public Pagination pagination { get; set; } = new Pagination
        {
            pageNo = 1,
            pageSize = 10,
            totalPages = 4,
            totalRows = 3,
        };
    }
}
