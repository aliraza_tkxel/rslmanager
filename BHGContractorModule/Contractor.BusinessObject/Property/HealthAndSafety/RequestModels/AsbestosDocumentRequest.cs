﻿
namespace Contractor.BusinessObject
{
    public class AsbestosDocumentRequest
    {
        public string documentCategory { get; set; } 
        public int documentTypeId { get; set; } 
        public int documentSubTypeId { get; set; } 
        public string documentDate { get; set; } 
        public string expiryDate { get; set; } 
        public string keywords { get; set; } 

        public string propertyId { get; set; } 
        public int? blockId { get; set; } 
        public int? schemeId { get; set; } 

        public string documentPath { get; set; } 
        public string documentSize { get; set; } 
        public string documentFormat { get; set; } 
        public string documentName { get; set; } 
        public int uploadedBy { get; set; } 

    }
}
