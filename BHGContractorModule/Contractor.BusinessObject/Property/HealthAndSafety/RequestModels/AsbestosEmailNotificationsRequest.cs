﻿namespace Contractor.BusinessObject
{
    public class AsbestosEmailNotificationsRequest
    {
        public string propertyId { get; set; }
        public string requestType { get; set; }
    }
}
