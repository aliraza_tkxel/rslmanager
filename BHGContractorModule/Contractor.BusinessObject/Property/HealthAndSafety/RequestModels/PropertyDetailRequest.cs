﻿namespace Contractor.BusinessObject
{
    public class PropertyDetailRequest
    {
        public int? schemeId { get; set; }
        public int? blockId { get; set; }
        public string propertyId { get; set; }
        public string type { get; set; }
    }
}
