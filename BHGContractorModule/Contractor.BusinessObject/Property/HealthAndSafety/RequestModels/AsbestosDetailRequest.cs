﻿
namespace Contractor.BusinessObject
{
    public class AsbestosDetailRequest
    {
        public int asbestosId { get; set; }

        public int? employeeId { get; set; }

        public int? asbestosTypeId { get; set; }

        public int? asbestosElementId { get; set; }

        public string typeId { get; set; }

        public int? riskLevelId { get; set; }

        public string addedDate { get; set; }

        public string removedDate { get; set; }

        public string notes { get; set; }

        public bool? isUrgentActionRequired { get; set; }

        public int? createdBy { get; set; }

        public int? schemeId { get; set; }

        public int? blockId { get; set; }

        public string propertyId { get; set; }

    }
}
