﻿namespace Contractor.BusinessObject
{
    public class AsbestosListingResponse
    {
        public AsbestosListingInfo asbestosList { get; set; } = new AsbestosListingInfo();
        public AsbestosLookups lookUps { get; set; } = new AsbestosLookups();        
    }
}
