﻿
using System.Collections.Generic;

namespace Contractor.BusinessObject
{
    public class AsbestosEmailNotificationsResponse
    {
        public List<AsbestosEmailNotifications> Employee { get; set; } = new List<AsbestosEmailNotifications>();
    }
}
