﻿using System.Collections.Generic;

namespace Contractor.BusinessObject.Property
{
    public class PropertySearchResponse
    {
        public List<PropertySearch> Properties { get; set; } = new List<PropertySearch>();
    }

    public class PropertySearch
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public string TypeDescription { get; set; }
        public string PropertyType { get; set; }
    }
}
