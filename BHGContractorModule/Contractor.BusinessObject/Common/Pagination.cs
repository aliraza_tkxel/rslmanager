﻿

namespace Contractor.BusinessObject
{
    public class Pagination
    {       
        public int pageSize { get; set; }
        public int pageNo { get; set; }
        public int totalRows { get; set; }
        public int totalPages { get; set; }
    }
}


