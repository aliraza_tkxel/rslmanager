﻿
namespace Contractor.BusinessObject
{
    public class Lookup
    {
        public int lookUpId { get; set; }
        public string lookUpDescription { get; set; }
        public string lookUpIdString { get; set; }
        public int? order { get; set; }
    }
}
