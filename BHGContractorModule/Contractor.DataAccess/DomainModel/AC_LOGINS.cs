//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Contractor.DataAccess.DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class AC_LOGINS
    {
        public int LOGINID { get; set; }
        public Nullable<int> EMPLOYEEID { get; set; }
        public string LOGIN { get; set; }
        public string PASSWORD { get; set; }
        public Nullable<int> ACTIVE { get; set; }
        public Nullable<System.DateTime> DATECREATED { get; set; }
        public Nullable<System.DateTime> DATEMODIFIED { get; set; }
        public Nullable<int> CREATEDBY { get; set; }
        public Nullable<int> MODIFIEDBY { get; set; }
        public Nullable<System.DateTime> EXPIRES { get; set; }
        public string DEVICETOKEN { get; set; }
        public Nullable<bool> ISLOCKED { get; set; }
        public Nullable<byte> THRESHOLD { get; set; }
        public Nullable<System.DateTime> DATEUPDATED { get; set; }
        public Nullable<System.DateTime> LASTLOGGEDIN { get; set; }
    }
}
