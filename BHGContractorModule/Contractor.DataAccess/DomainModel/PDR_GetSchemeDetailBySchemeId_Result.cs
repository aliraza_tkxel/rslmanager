//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Contractor.DataAccess.DomainModel
{
    using System;
    
    public partial class PDR_GetSchemeDetailBySchemeId_Result
    {
        public string SCHEMENAME { get; set; }
        public string Address1 { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
    }
}
