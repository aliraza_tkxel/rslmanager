﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contractor.DataAccess.DomainModel;
using Contractor.BusinessObject;
using Contractor.Utilities.Constant;
using System.Globalization;

namespace Contractor.DataAccess.DataRepository
{

    public class AsbestosRepository : BaseRepository<P_PROPERTY_ASBESTOS_RISKLEVEL>
    {
        public AsbestosRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region "Get Asbestos Listing"

        public AsbestosListingResponse GetAsbestosListing(AsbestosListingRequest model)
        {
            AsbestosListingResponse response = new AsbestosListingResponse();

            List<AsbestosDetail> asbestosList = new List<AsbestosDetail>();
            if (model.type.Equals(ApplicationConstants.TypeProperty))
            {
                asbestosList = getPropertyAsbestosRisk(model.propertyId);
            } else if (model.type.Equals(ApplicationConstants.TypeScheme))
            {
                asbestosList = getSchemeAsbestosRisk(model.schemeId);
            } else if (model.type.Equals(ApplicationConstants.TypeBlock))
            {
                asbestosList = getBlockAsbestosRisk(model.blockId);
            }

            Pagination page = new Pagination();
            page.totalRows = asbestosList.Count();
            page.totalPages = (int)Math.Ceiling((double)asbestosList.Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.asbestosList.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = asbestosList;

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"asbestosId";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            foreach (AsbestosDetail item in finalData)
            {
                if (item.addedDateTime.HasValue)
                {
                    DateTime startDate = (DateTime)item.addedDateTime;
                    item.addedDate = startDate.ToString("dd/MM/yyyy");
                }

                if (item.removedDateTime.HasValue)
                {
                    DateTime removedDate = (DateTime)item.removedDateTime;
                    item.removedDate = removedDate.ToString("dd/MM/yyyy");
                }

            }
            response.asbestosList.asbestosList = finalData;

            return response;
        }

        #endregion

        #region "Add Amend Asbestos"

        public void AddAmendAsbestos(AsbestosDetailRequest request)
        {
            using (var transaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    #region "Insertion in P_PROPERTY_ASBESTOS_RISKLEVEL"

                    P_PROPERTY_ASBESTOS_RISKLEVEL asbestos = new P_PROPERTY_ASBESTOS_RISKLEVEL();
                    if (request.asbestosId > 0)
                    {                        
                        asbestos = DbContext.P_PROPERTY_ASBESTOS_RISKLEVEL
                                            .Where(a => a.PROPASBLEVELID == request.asbestosId)
                                            .FirstOrDefault();
                    }
                    
                    asbestos.ASBRISKLEVELID = request.asbestosTypeId;
                    asbestos.ASBESTOSID = request.asbestosElementId;
                    asbestos.RiskLevelId = request.riskLevelId;

                    DateTimeFormatInfo ukDtfi = new CultureInfo("en-GB", false).DateTimeFormat;

                    if (request.addedDate != null && request.addedDate != string.Empty)
                        asbestos.DateAdded = Convert.ToDateTime(request.addedDate, ukDtfi);

                    if (request.removedDate != null && request.removedDate != string.Empty)
                        asbestos.DateRemoved = Convert.ToDateTime(request.removedDate, ukDtfi);

                    asbestos.Notes = request.notes;
                    asbestos.IsUrgentActionRequired = request.isUrgentActionRequired;
                    asbestos.UserID = request.createdBy;

                    if (request.asbestosId == 0)
                    {
                        asbestos.PROPERTYID = request.propertyId;
                        asbestos.SchemeId = request.schemeId;
                        asbestos.BlockId = request.blockId;

                        DbContext.P_PROPERTY_ASBESTOS_RISKLEVEL.Add(asbestos);
                    }

                    DbContext.SaveChanges();

                    #endregion

                    #region "Insertion in P_PROPERTY_ASBESTOS_RISK"

                    P_PROPERTY_ASBESTOS_RISK asbestosRisk = new P_PROPERTY_ASBESTOS_RISK();
                    if (request.asbestosId > 0)
                    {
                        asbestosRisk = DbContext.P_PROPERTY_ASBESTOS_RISK
                                                .Where(asb => asb.PROPASBLEVELID == asbestos.PROPASBLEVELID)
                                                .FirstOrDefault();
                    }

                    asbestosRisk.ASBRISKID = request.typeId.ToString();
                    asbestosRisk.PROPASBLEVELID = asbestos.PROPASBLEVELID;
                    if (request.asbestosId == 0)
                    {
                        DbContext.P_PROPERTY_ASBESTOS_RISK.Add(asbestosRisk);
                    }
                    DbContext.SaveChanges();
                    #endregion

                    transaction.Commit();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }

            }
        }
        #endregion

        #region Get Property Asbestos Risk
        public List<AsbestosDetail> getPropertyAsbestosRisk(string propertyId)
        {
            var asbestosQuery = (from pparl in DbContext.P_PROPERTY_ASBESTOS_RISKLEVEL
                                    join parl in DbContext.P_ASBRISKLEVEL on pparl.ASBRISKLEVELID equals parl.ASBRISKLEVELID
                                    join pa in DbContext.P_ASBESTOS on pparl.ASBESTOSID equals pa.ASBESTOSID
                                    join ppar in DbContext.P_PROPERTY_ASBESTOS_RISK on pparl.PROPASBLEVELID equals ppar.PROPASBLEVELID
                                    join pasbrl in DbContext.P_AsbestosRiskLevel on pparl.RiskLevelId equals pasbrl.AsbestosLevelId into ps
                                    from pasbrl in ps.DefaultIfEmpty()

                                    join emp in DbContext.E__EMPLOYEE on pparl.UserID equals emp.EMPLOYEEID into ep
                                    from emp in ep.DefaultIfEmpty()

                                 where pparl.PROPERTYID.ToLower() == propertyId.ToLower() && (pparl.DateRemoved == null || pparl.DateRemoved >= DateTime.Now)

                                    select new AsbestosDetail
                                    {
                                        asbestosId = pparl.PROPASBLEVELID,
                                        employeeId = pparl.UserID,
                                        asbestosTypeId = pparl.ASBRISKLEVELID,
                                        asbestosType = parl.ASBRISKLEVELDESCRIPTION,
                                        asbestosElementId = pparl.ASBESTOSID,
                                        asbestosElement = pa.RISKDESCRIPTION,
                                        typeId = ppar.ASBRISKID,
                                        type = ppar.ASBRISKID,
                                        riskLevelId = pparl.RiskLevelId,
                                        riskLevel = pasbrl == null ? string.Empty : pasbrl.Description.Trim(),
                                        addedDateTime = pparl.DateAdded,
                                        removedDateTime = pparl.DateRemoved,
                                        notes = pparl.Notes,
                                        isUrgentActionRequired = pparl.IsUrgentActionRequired,
                                        createdBy = pparl.UserID,
                                        createdByFullName = (emp == null)
                                                                ? "-"
                                                                : (emp.FIRSTNAME == null ? "" : emp.FIRSTNAME) + " " + (emp.LASTNAME == null ? "" : emp.LASTNAME),
                                        createdByEmployeeName = (emp == null) 
                                                                ? "-" 
                                                                : (emp.FIRSTNAME ==null ? "": emp.FIRSTNAME.Substring(0, 1)) + " "+ (emp.LASTNAME == null ? "" : emp.LASTNAME.Substring(0, 1))

                                    });
            List<AsbestosDetail> asbList = new List<AsbestosDetail>();
            if (asbestosQuery.Count() > 0)
            {
                asbList = asbestosQuery.Distinct().ToList();
            }

            return asbList;
        }
        #endregion

        #region Get Scheme Asbestos Risk
        public List<AsbestosDetail> getSchemeAsbestosRisk(int? schemeId)
        {
            var asbestosQuery = (from pparl in DbContext.P_PROPERTY_ASBESTOS_RISKLEVEL
                                 join parl in DbContext.P_ASBRISKLEVEL on pparl.ASBRISKLEVELID equals parl.ASBRISKLEVELID
                                 join pa in DbContext.P_ASBESTOS on pparl.ASBESTOSID equals pa.ASBESTOSID
                                 join ppar in DbContext.P_PROPERTY_ASBESTOS_RISK on pparl.PROPASBLEVELID equals ppar.PROPASBLEVELID
                                 join pasbrl in DbContext.P_AsbestosRiskLevel on pparl.RiskLevelId equals pasbrl.AsbestosLevelId into ps
                                 from pasbrl in ps.DefaultIfEmpty()

                                 join emp in DbContext.E__EMPLOYEE on pparl.UserID equals emp.EMPLOYEEID into ep
                                 from emp in ep.DefaultIfEmpty()

                                 where pparl.SchemeId == schemeId && (pparl.DateRemoved == null || pparl.DateRemoved >= DateTime.Now)

                                 select new AsbestosDetail
                                 {
                                     asbestosId = pparl.PROPASBLEVELID,
                                     employeeId = pparl.UserID,
                                     asbestosTypeId = pparl.ASBRISKLEVELID,
                                     asbestosType = parl.ASBRISKLEVELDESCRIPTION,
                                     asbestosElementId = pparl.ASBESTOSID,
                                     asbestosElement = pa.RISKDESCRIPTION,
                                     typeId = ppar.ASBRISKID,
                                     type = ppar.ASBRISKID,
                                     riskLevelId = pparl.RiskLevelId,
                                     riskLevel = pasbrl == null ? string.Empty : pasbrl.Description.Trim(),
                                     addedDateTime = pparl.DateAdded,
                                     removedDateTime = pparl.DateRemoved,
                                     notes = pparl.Notes,
                                     isUrgentActionRequired = pparl.IsUrgentActionRequired,
                                     createdBy = pparl.UserID,
                                     createdByFullName = (emp == null)
                                                                ? "-"
                                                                : (emp.FIRSTNAME == null ? "" : emp.FIRSTNAME) + " " + (emp.LASTNAME == null ? "" : emp.LASTNAME),
                                     createdByEmployeeName = (emp == null)
                                                             ? "-"
                                                             : (emp.FIRSTNAME == null ? "" : emp.FIRSTNAME.Substring(0, 1)) + " " + (emp.LASTNAME == null ? "" : emp.LASTNAME.Substring(0, 1))

                                 });
            List<AsbestosDetail> asbList = new List<AsbestosDetail>();
            if (asbestosQuery.Count() > 0)
            {
                asbList = asbestosQuery.Distinct().ToList();
            }

            return asbList;
        }
        #endregion

        #region Get Block Asbestos Risk
        public List<AsbestosDetail> getBlockAsbestosRisk(int? blockId)
        {
            var asbestosQuery = (from pparl in DbContext.P_PROPERTY_ASBESTOS_RISKLEVEL
                                 join parl in DbContext.P_ASBRISKLEVEL on pparl.ASBRISKLEVELID equals parl.ASBRISKLEVELID
                                 join pa in DbContext.P_ASBESTOS on pparl.ASBESTOSID equals pa.ASBESTOSID
                                 join ppar in DbContext.P_PROPERTY_ASBESTOS_RISK on pparl.PROPASBLEVELID equals ppar.PROPASBLEVELID
                                 join pasbrl in DbContext.P_AsbestosRiskLevel on pparl.RiskLevelId equals pasbrl.AsbestosLevelId into ps
                                 from pasbrl in ps.DefaultIfEmpty()

                                 join emp in DbContext.E__EMPLOYEE on pparl.UserID equals emp.EMPLOYEEID into ep
                                 from emp in ep.DefaultIfEmpty()

                                 where pparl.BlockId == blockId && (pparl.DateRemoved == null || pparl.DateRemoved >= DateTime.Now)

                                 select new AsbestosDetail
                                 {
                                     asbestosId = pparl.PROPASBLEVELID,
                                     employeeId = pparl.UserID,
                                     asbestosTypeId = pparl.ASBRISKLEVELID,
                                     asbestosType = parl.ASBRISKLEVELDESCRIPTION,
                                     asbestosElementId = pparl.ASBESTOSID,
                                     asbestosElement = pa.RISKDESCRIPTION,
                                     typeId = ppar.ASBRISKID,
                                     type = ppar.ASBRISKID,
                                     riskLevelId = pparl.RiskLevelId,
                                     riskLevel = pasbrl == null ? string.Empty : pasbrl.Description.Trim(),
                                     addedDateTime = pparl.DateAdded,
                                     removedDateTime = pparl.DateRemoved,
                                     notes = pparl.Notes,
                                     isUrgentActionRequired = pparl.IsUrgentActionRequired,
                                     createdBy = pparl.UserID,
                                     createdByFullName = (emp == null)
                                                                ? "-"
                                                                : (emp.FIRSTNAME == null ? "" : emp.FIRSTNAME) + " " + (emp.LASTNAME == null ? "" : emp.LASTNAME),
                                     createdByEmployeeName = (emp == null)
                                                             ? "-"
                                                             : (emp.FIRSTNAME == null ? "" : emp.FIRSTNAME.Substring(0, 1)) + " " + (emp.LASTNAME == null ? "" : emp.LASTNAME.Substring(0, 1))

                                 });
            List<AsbestosDetail> asbList = new List<AsbestosDetail>();
            if (asbestosQuery.Count() > 0)
            {
                asbList = asbestosQuery.Distinct().ToList();
            }

            return asbList;
        }
        #endregion

        #region "Employee Email For Asbestos Email Notifications"

        public AsbestosEmailNotificationsResponse GetEmployeeEmailForAsbestosEmailNotifications(AsbestosEmailNotificationsRequest request)
        {
            var response = new AsbestosEmailNotificationsResponse();
            response.Employee = DbContext.AS_GetEmployeeEmailForAsbestosEmailNotifications(request.propertyId, request.requestType).Select(e => new AsbestosEmailNotifications()
            {
                employeeId = e.EMPLOYEEID,
                fullName = e.FULLNAME,
                jobeRoleDescription = e.JobeRoleDescription,
                PropertyAddress = e.PROPERTYADDRESS,
                workEmail = e.WORKEMAIL
            }).ToList();

            return response;
        }

        #endregion
    }

}

