﻿using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.DataRepository
{
    public class AsbestosRiskLevelRepository : BaseRepository<P_AsbestosRiskLevel>
    {
        public AsbestosRiskLevelRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
