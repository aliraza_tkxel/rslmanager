﻿using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.DataRepository
{
    public class DocumentTypeRepository : BaseRepository<P_Documents_Type>
    {
        public DocumentTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
