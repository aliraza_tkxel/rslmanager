﻿using System;
using Contractor.DataAccess.DomainModel;
using Contractor.BusinessObject;
using System.Globalization;

namespace Contractor.DataAccess.DataRepository
{

    public class DocumentRepository : BaseRepository<P_Documents>
    {
        public DocumentRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        public void AddAsbestosDocument(AsbestosDocumentRequest request)
        {
            using (var transaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    P_Documents document = new P_Documents();
                    document.Category = request.documentCategory;
                    document.DocumentTypeId = request.documentTypeId;
                    document.DocumentSubtypeId = request.documentSubTypeId;

                    DateTimeFormatInfo ukDtfi = new CultureInfo("en-GB", false).DateTimeFormat;

                    if (request.documentDate != null && request.documentDate != string.Empty)
                        document.DocumentDate = Convert.ToDateTime(request.documentDate, ukDtfi);

                    if (request.expiryDate != null && request.expiryDate != string.Empty)
                        document.ExpiryDate = Convert.ToDateTime(request.expiryDate, ukDtfi);

                    document.Keywords = request.keywords;                    
                    document.PropertyId = request.propertyId;
                    document.SchemeId = request.schemeId;
                    document.BlockId = request.blockId;
                    document.DocumentPath = request.documentPath;
                    document.DocumentSize = request.documentSize;
                    document.DocumentFormat = request.documentFormat;
                    document.DocumentName = request.documentName;
                    document.UploadedBy = request.uploadedBy;
                    document.CreatedDate = DateTime.Now;

                    DbContext.P_Documents.Add(document);
                    DbContext.SaveChanges();
                    transaction.Commit();
                    transaction.Dispose();
                                      
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }

            }
        }

    }
}
