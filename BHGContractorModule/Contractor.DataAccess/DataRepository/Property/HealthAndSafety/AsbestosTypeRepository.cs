﻿using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.DataRepository
{
    public class AsbestosTypeRepository : BaseRepository<P_ASBRISKLEVEL>
    {
        public AsbestosTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
