﻿using Contractor.DataAccess.DomainModel;
using Contractor.Utilities.Constant;
using System.Collections.Generic;
using System.Linq;

namespace Contractor.DataAccess.DataRepository
{
    public class DocumentSubTypeRepository : BaseRepository<P_Documents_SubType>
    {
        public DocumentSubTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        public List<P_Documents_SubType> getAsbestosDocumentSubTypes()
        {
            var query = (from dst in DbContext.P_Documents_SubType
                         join dt in DbContext.P_Documents_Type on dst.DocumentTypeId equals dt.DocumentTypeId
                         where dt.Title == ApplicationConstants.DocumentTypeAsbestos
                         select dst
                         );
            List<P_Documents_SubType> docSubTypes = new List<P_Documents_SubType>();
            if (query.Count() > 0)
            {
                docSubTypes = query.Distinct().ToList();
            }

            return docSubTypes;
        }

    }
}
