﻿using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.DataRepository
{
    public class AsbRiskTypeRepository : BaseRepository<P_ASBRISK>
    {
        public AsbRiskTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
