﻿using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.DataRepository
{
    public class AsbestosElementRepository : BaseRepository<P_ASBESTOS>
    {
        public AsbestosElementRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
