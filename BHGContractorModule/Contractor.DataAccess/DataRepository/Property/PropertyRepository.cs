﻿using Contractor.BusinessObject.Property;
using Contractor.DataAccess.DomainModel;
using System.Linq;

namespace Contractor.DataAccess.DataRepository
{
    public class PropertyRepository : BaseRepository<P__PROPERTY>
    {
        public PropertyRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        public PropertySearchResponse SearchProperty(PropertySearchRequest request)
        {

            var response = DbContext.CONTRACTOR_GetSearchProperty(request.Search).Select(e => new PropertySearch()
            {
                Address = e.Address,
                Id = e.Id,
                PropertyType = e.PropertyType,
                TypeDescription = e.TypeDescription
            }).ToList();


            return new PropertySearchResponse() { Properties = response };
        }
    }
}
