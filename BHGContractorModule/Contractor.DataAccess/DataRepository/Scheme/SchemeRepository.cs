﻿using Contractor.DataAccess.DomainModel;
using System.Linq;

namespace Contractor.DataAccess.DataRepository
{
    public class SchemeRepository : BaseRepository<P_SCHEME>
    {
        public SchemeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        public string GetSchemeAddress(int? schemeId)
        {
            var address = string.Empty;
            var scheme =  DbContext.PDR_GetSchemeDetailBySchemeId(schemeId).FirstOrDefault();
            if (scheme != null)
            {
                var adrsArray = new[] { scheme.SCHEMENAME, scheme.Address1, scheme.Town, scheme.PostCode };
                address = string.Format("Scheme > {0}", string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s))));
            }
            return address;
        }

    }
}
