﻿using Contractor.DataAccess.DomainModel;
using System;
using System.Linq;
using Contractor.BusinessObject.Login;
using Contractor.DataAccess.DataRepository;
using Contractor.Utilities.Constant;

namespace Contractor.DataAccess
{
    public class LoginRepository : BaseRepository<AC_LOGINS>
    {
        public LoginRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region Reset login threshold           
        public void resetLoginThreshold(String username)
        {
            var userRecord = DbContext.AC_LOGINS.Where(usr => usr.LOGIN == username);

            if (userRecord.Count() > 0)
            {
                AC_LOGINS user = userRecord.First();
                user.THRESHOLD = 0;
                user.ISLOCKED = false;
                DbContext.SaveChanges();
            }
        }
        #endregion

        #region Set Login Threshold      
        public SP_NET_SETLOGIN_THRESHOLD_Result setLoginThreshold(string userName)
        {

            var data = (from lst in DbContext.SP_NET_SETLOGIN_THRESHOLD(userName)
                        select lst).ToList();


            SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = null;
            if (data.Count() > 0)
            {
                loginThreshold = data.FirstOrDefault();
            }

            return loginThreshold;
        }

        #endregion

        #region Check whether the user have frontline team and Neighbourhood Officer role
        private bool CheckUserTeamAndRole(string username)
        {
            var data = (from emp in DbContext.E__EMPLOYEE
                        join login in DbContext.AC_LOGINS on emp.EMPLOYEEID equals login.EMPLOYEEID
                        join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                        join et in DbContext.E_TEAM on jd.TEAM equals et.TEAMID
                        where (jd.ACTIVE == 1)
                         && et.TEAMNAME == ApplicationConstants.ContractorUserType
                        && (et.ACTIVE == 1)
                        && login.LOGIN == username
                        orderby emp.FIRSTNAME, emp.LASTNAME
                        select emp
                        ).FirstOrDefault();
            if (data != null && data.EMPLOYEEID > 0)
            {
                return true;
            }
            return false;

        }

        #endregion

        public LoginResponse LoginUser(LoginRequest loginRequest)
        {
            var response = new LoginResponse();

            
            int userId = 0;
            if (CheckUserTeamAndRole(loginRequest.UserName) == true)
            {
                var data = DbContext.SP_NET_CHECKLOGIN(loginRequest.UserName, loginRequest.Password).ToList();
                if (data.Count() > 0)
                {
                    userId = data.Select(x => x.LOGINID).FirstOrDefault();

                    response.UserInfo.EmployeeFullName = data.Select(x => x.FIRSTNAME).FirstOrDefault() + " " + data.Select(x => x.LASTNAME).FirstOrDefault();
                    response.UserInfo.IsActive = data.Select(x => x.ACTIVE).FirstOrDefault();
                    response.UserInfo.UserId = data.Select(x => x.EMPLOYEEID).FirstOrDefault();
                    response.UserInfo.UserName = data.Select(x => x.LOGIN).FirstOrDefault();
                    response.UserInfo.TeamId = data.Select(x => x.TEAMID).FirstOrDefault();
                    response.UserInfo.TeamCode = data.Select(x => x.TEAMCODE).FirstOrDefault();

                    //get gender of logged in user
                    response.UserInfo.Gender = DbContext.E__EMPLOYEE.Where(x => x.EMPLOYEEID == response.UserInfo.UserId).Select(x => x.GENDER).FirstOrDefault();
                    response.ResponseMessage = UserMessageConstants.LoginSuccessMsg;
                }
                else {

                    SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = setLoginThreshold(loginRequest.UserName);//set threshold limits

                    if (loginThreshold != null)
                    {

                        if (loginThreshold.Locked == true)//access lockdown msg
                        {
                            response.Locked = loginThreshold.Locked;
                            response.ResponseMessage = UserMessageConstants.AccessLockdownMsg;                            
                        }
                        else if (loginThreshold.Threshold == 4)//access lockdown warning msg
                        {
                            response.Threshold = loginThreshold.Threshold;
                            response.ResponseMessage = UserMessageConstants.AccessLockdownWarningMsg;                            
                        }
                        else//wrong UN and password msg
                        {
                            response.ResponseMessage = UserMessageConstants.LoginFailureMsg;                            
                        }

                    }
                    else //sussess msg
                    {
                        response.ResponseMessage = UserMessageConstants.LoginSuccessMsg;
                        resetLoginThreshold(loginRequest.UserName); //reset threshold limist in case of correct UN & password                          
                    }

                }
            }
            else {
                response.ResponseMessage = UserMessageConstants.LoginFailureMsg;
            }
            

            return response;
        }
    }
}
