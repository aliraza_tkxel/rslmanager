﻿using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.DataRepository
{
    public class BlockRepository : BaseRepository<P_BLOCK>
    {
        public BlockRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
