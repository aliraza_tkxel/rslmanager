﻿using Contractor.DataAccess.DataRepository;

namespace Contractor.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        #region "Health And Safety"       
        AsbestosRepository asbestosRepository { get; }
        DocumentRepository documentRepository { get; }
        AsbestosElementRepository asbestosElementRepository { get; }
        AsbestosRiskLevelRepository asbestosRiskLevelRepository { get; }
        AsbestosTypeRepository asbestosTypeRepository { get; }
        AsbRiskTypeRepository asbRiskTypeRepository { get; }
        DocumentSubTypeRepository documentSubTypeRepository { get; }
        DocumentTypeRepository documentTypeRepository { get; }
        #endregion

        #region "Scheme"   
        SchemeRepository schemeRepository { get; }
        #endregion

        #region "Block"   
        BlockRepository blockRepository { get; }
        #endregion

        #region "Property"   
        PropertyRepository propertyRepository { get; }
        #endregion

        #region login

        LoginRepository loginRepository { get; }

        #endregion

        int Commit();
    }
}
