﻿

using Contractor.DataAccess.DataRepository;
using Contractor.DataAccess.DomainModel;

namespace Contractor.DataAccess.UnitOfWork
{
    public class UnitOfWork : Disposable, IUnitOfWork
    {

        #region "Unit Of Work"

        private RSLBHALiveEntities dataContext = null;

        public UnitOfWork()
        {
            if (dataContext == null)
            {
                dataContext = new RSLBHALiveEntities();
                dataContext.Configuration.ProxyCreationEnabled = false;
            }
        }

        int IUnitOfWork.Commit()
        {
            return dataContext.SaveChanges();
        }
        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }

        #endregion

        #region "Repositories"

        #region "Health And Safety"

        private AsbestosRepository _asbestosRepository;
        public AsbestosRepository asbestosRepository
        {
            get { return (_asbestosRepository == null) ? _asbestosRepository = new AsbestosRepository(dataContext) : _asbestosRepository; }
        }

        private DocumentRepository _documentRepository;
        public DocumentRepository documentRepository
        {
            get { return (_documentRepository == null) ? _documentRepository = new DocumentRepository(dataContext) : _documentRepository; }
        }

        private AsbestosElementRepository _asbestosElementRepository;
        public AsbestosElementRepository asbestosElementRepository
        {
            get { return (_asbestosElementRepository == null) ? _asbestosElementRepository = new AsbestosElementRepository(dataContext) : _asbestosElementRepository; }
        }

        private AsbestosRiskLevelRepository _asbestosRiskLevelRepository;
        public AsbestosRiskLevelRepository asbestosRiskLevelRepository
        {
            get { return (_asbestosRiskLevelRepository == null) ? _asbestosRiskLevelRepository = new AsbestosRiskLevelRepository(dataContext) : _asbestosRiskLevelRepository; }
        }

        private AsbestosTypeRepository _asbestosTypeRepository;
        public AsbestosTypeRepository asbestosTypeRepository
        {
            get { return (_asbestosTypeRepository == null) ? _asbestosTypeRepository = new AsbestosTypeRepository(dataContext) : _asbestosTypeRepository; }
        }

        private AsbRiskTypeRepository _asbRiskTypeRepository;
        public AsbRiskTypeRepository asbRiskTypeRepository
        {
            get { return (_asbRiskTypeRepository == null) ? _asbRiskTypeRepository = new AsbRiskTypeRepository(dataContext) : _asbRiskTypeRepository; }
        }

        private DocumentSubTypeRepository _documentSubTypeRepository;
        public DocumentSubTypeRepository documentSubTypeRepository
        {
            get { return (_documentSubTypeRepository == null) ? _documentSubTypeRepository = new DocumentSubTypeRepository(dataContext) : _documentSubTypeRepository; }
        }

        private DocumentTypeRepository _documentTypeRepository;
        public DocumentTypeRepository documentTypeRepository
        {
            get { return (_documentTypeRepository == null) ? _documentTypeRepository = new DocumentTypeRepository(dataContext) : _documentTypeRepository; }
        }

        private SchemeRepository _schemeRepository;
        public SchemeRepository schemeRepository
        {
            get { return (_schemeRepository == null) ? _schemeRepository = new SchemeRepository(dataContext) : _schemeRepository; }
        }

        private BlockRepository _blockRepository;
        public BlockRepository blockRepository
        {
            get { return (_blockRepository == null) ? _blockRepository = new BlockRepository(dataContext) : _blockRepository; }
        }


        private PropertyRepository _propertyRepository;
        public PropertyRepository propertyRepository
        {
            get { return (_propertyRepository == null) ? _propertyRepository = new PropertyRepository(dataContext) : _propertyRepository; }
        }

        #endregion

        #region Login

        private LoginRepository _loginRepository;
        public LoginRepository loginRepository
        {
            get { return (_loginRepository == null) ? _loginRepository = new LoginRepository(dataContext) : _loginRepository; }
        }

        #endregion

        #endregion

    }
}
