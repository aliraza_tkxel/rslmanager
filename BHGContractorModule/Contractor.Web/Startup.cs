﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Contractor.Web.Startup))]
namespace Contractor.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
