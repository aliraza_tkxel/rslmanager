﻿using System.Web.Mvc;

namespace Contractor.Web.Controllers
{
    public class MenuController : BaseController
    {
        #region >>> Action Methods <<<

        // GET: Menu
        public ActionResult GetTopNavigationMenu()
        {
            return PartialView("~/Views/Shared/_TopNavigation.cshtml");
        }

        #endregion
    }
}