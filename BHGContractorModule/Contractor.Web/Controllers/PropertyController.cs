﻿using Contractor.BusinessLogic;
using Contractor.BusinessObject;
using Contractor.BusinessObject.Property;
using Contractor.Utilities.Constant;
using Contractor.Utilities.Helper;
using Contractor.Web.Models;
using Contractor.Web.Models.Property;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Contractor.Web.Controllers
{
    public class PropertyController : BaseController
    {

        public PropertyController(IManagerProxy manager) : base(manager)
        {

        }

        // GET: Property
        public ActionResult Search()
        {
            var model = new PropertySearchViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(PropertySearchViewModel model)
        {
            PropertySearchRequest resquest = new PropertySearchRequest();
            resquest.Search = model.Search;
            var response = this.Managers.PropertiesManager.SearchProperty(resquest);
            model.Properties = response.Properties.Where(e => e.PropertyType == "Property").Select(e => new PropertySearchListViewModel()
            {
                SearchText = model.Search,
                Address = e.Address,
                Id = e.Id,
                PropertyType = e.PropertyType,
                TypeDescription = e.TypeDescription
            }).ToList();
            model.Schemes = response.Properties.Where(e => e.PropertyType == "Scheme").Select(e => new PropertySearchListViewModel()
            {
                SearchText = model.Search,
                Address = e.Address,
                Id = e.Id,
                PropertyType = e.PropertyType,
                TypeDescription = e.TypeDescription
            }).ToList();
            model.Blocks = response.Properties.Where(e => e.PropertyType == "Block").Select(e => new PropertySearchListViewModel()
            {
                SearchText = model.Search,
                Address = e.Address,
                Id = e.Id,
                PropertyType = e.PropertyType,
                TypeDescription = e.TypeDescription
            }).ToList();
            return PartialView("~/Views/Property/PropertyList.cshtml", model);
        }

        public ActionResult PropertyDetail(string id, string type)
        {
            var model = new PropertyDetailViewModel();
            model.id = id;
            model.type = type;
            SessionManager.SetRequestType(type.ToLower());
            SessionManager.SetSearchValue(id);

            var request = GetPropertyDetailRequestParams();
            var address = string.Empty;
            if (request.type.Equals(ApplicationConstants.TypeProperty))
            {
                address = this.Managers.PropertiesManager.GetPropertyAddress(request.propertyId);
            }
            else if (request.type.Equals(ApplicationConstants.TypeScheme))
            {
                address = this.Managers.SchemeManager.GetSchemeAddress(request.schemeId);
            }
            else if (request.type.Equals(ApplicationConstants.TypeBlock))
            {
                address = this.Managers.BlockManager.GetBlockAddress(request.blockId);
            }

            model.fullHeading = address;

            return View(model);
        }


        #region "Get Property Detail Request Params"
        public PropertyDetailRequest GetPropertyDetailRequestParams()
        {
            var request = new PropertyDetailRequest();
            string searchType = SessionManager.GetRequestType();
            string searchValue = SessionManager.GetSearchValue();
            request.type = searchType;

            if (searchValue != string.Empty)
            {
                string id = searchValue;
                if (searchType.Equals(ApplicationConstants.TypeScheme))
                {
                    request.schemeId = Convert.ToInt32(id);
                }
                else if (searchType.Equals(ApplicationConstants.TypeBlock))
                {
                    request.blockId = Convert.ToInt32(id);
                }
                else
                {
                    request.propertyId = id;
                }
            }

            return request;
        }
        #endregion
    }
}