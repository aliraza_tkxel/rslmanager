﻿using Contractor.BusinessObject.Login;
using VM = Contractor.Web.Models.Login;
using System;
using System.Web.Mvc;
using Contractor.Utilities.Helper;
using Contractor.BusinessLogic;
using Contractor.Utilities.Constant;

namespace Contractor.Web.Controllers
{
    public class HomeController : Controller
    {
        public IManagerProxy Managers { get; set; }
        public HomeController(IManagerProxy managers)
        {
            this.Managers = managers;
        }

        private VM.LoginViewModel LoginUser(VM.LoginViewModel model)
        {
            var request = new LoginRequest();
            request.UserName = model.UserName;
            var byt = System.Text.Encoding.UTF8.GetBytes(model.Password);
            request.Password = Convert.ToBase64String(byt);
            var response = this.Managers.LoginManager.LoginUser(request);

            if (response.ResponseMessage == UserMessageConstants.LoginSuccessMsg)
            {
                SessionManager.SetSessionUserId(response.UserInfo.UserId.Value);
            }

            model.ResponseMessage = response.ResponseMessage;
            model.Locked = response.Locked;
            model.Threshold = response.Threshold;
            return model;
        }

        public ActionResult Index()
        {
            SessionManager.RemoveSessionUserId();
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            SessionManager.RemoveSessionUserId();
            var model = new VM.LoginViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(VM.LoginViewModel model)
        {
            model = LoginUser(model);
            if (model.ResponseMessage == UserMessageConstants.LoginSuccessMsg)
            {
                return RedirectToAction("Search", "Property");
            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}