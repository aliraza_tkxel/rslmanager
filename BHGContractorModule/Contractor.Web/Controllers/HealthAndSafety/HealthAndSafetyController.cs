﻿using AutoMapper;
using Contractor.BusinessLogic;
using Contractor.BusinessObject;
using Contractor.Utilities.Constant;
using Contractor.Utilities.Helper;
using Contractor.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Contractor.Web.Controllers.HealthAndSafety
{
    public class HealthAndSafetyController : BaseController
    {

        public HealthAndSafetyController(IManagerProxy manager) : base(manager)
        {

        }

        #region "Controller Method"

        #region "Index"
        // GET: HealthAndSafety
        public ActionResult Index(string id, string type)
        {
            if (IsValidRequest(id))
            {
                HealthAndSafetyViewModel asbestosListing = GetAsbestosList(id);
                return PartialView("~/Views/Property/HealthAndSafety/_Asbestos.cshtml", asbestosListing);
            }
            else
            {
                return new HttpNotFoundResult();
            }

        }
        #endregion

        #region "Get Asbestos By Id"
        public JsonResult GetAsbestosById(int Id)
        {
            HealthAndSafetyViewModel model = new HealthAndSafetyViewModel();
            AsbestosDetailViewModel detail = new AsbestosDetailViewModel();
            model = (HealthAndSafetyViewModel)Session[SessionConstants.HealthAndSafetyInfo];
            detail = model.asbestosList.asbestosList.SingleOrDefault(c => c.asbestosId == Id);
            return Json(detail, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Search Asbestos"
        public ActionResult SearchAsbestos(string id)
        {
            HealthAndSafetyViewModel model = GetAsbestosList(id);
            return PartialView("~/Views/Property/HealthAndSafety/_AsbestosListing.cshtml", model.asbestosList);
        }
        #endregion

        #region "Add/Amend Asbestos"

        [HttpPost]
        public ActionResult AddAmendAsbestos(AsbestosDetailViewModel asbestosDetail)
        {
            var response = "";            
            asbestosDetail.createdBy = GetUserIdFromSession();

            var request = Mapper.Map<AsbestosDetailViewModel, AsbestosDetailRequest>(asbestosDetail);

            PropertyDetailRequest propertyRequest = GetPropertyDetailRequestParams();
            request.schemeId = propertyRequest.schemeId;
            request.blockId = propertyRequest.blockId;
            request.propertyId = propertyRequest.propertyId;

            this.Managers.HealthAndSafetyManager.AddAmendAsbestos(request);
            response = UserMessageConstants.AsbestosSuccessMessage;

            if (asbestosDetail.isUrgentActionRequired.HasValue && asbestosDetail.isUrgentActionRequired.Value == true)
            {
                var emailResponse = sendEmailNotificationOnUrgentAction(asbestosDetail);
                if (emailResponse == "Ok" || emailResponse == "NaN")
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(emailResponse, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Add Document"

        public ActionResult AddDocument(HealthAndSafetyViewModel docDetail)
        {
            var response = "";
            var documentInfo = docDetail.asbestosDocument;

            if (documentInfo.file != null && documentInfo.file.ContentLength > 0)
                try
                {
                    #region "Directory setup & saving document functionality"

                    PropertyDetailRequest propertyRequest = GetPropertyDetailRequestParams();
                    string directorypath = SetupDirectoryPath();
                    string fileName = FileHelper.getUniqueFileName(documentInfo.file.FileName);
                    string path = Path.Combine(string.Format("{0}\\", directorypath), fileName);
                    documentInfo.file.SaveAs(path);

                    ViewBag.Message = "File uploaded successfully";

                    #endregion

                    #region "Saving info in database"
                    var request = new AsbestosDocumentRequest();
                    request = Mapper.Map<AsbestosDocumentViewModel, AsbestosDocumentRequest>(documentInfo);
                    request.schemeId = propertyRequest.schemeId;
                    request.blockId = propertyRequest.blockId;
                    request.propertyId = propertyRequest.propertyId;

                    request.documentPath = fileName;
                    request.documentSize = FileHelper.getFileSize(documentInfo.file);
                    request.documentFormat = Path.GetExtension(fileName).Replace(".", "");
                    request.documentName = fileName;
                    request.uploadedBy = GetUserIdFromSession();

                    this.Managers.HealthAndSafetyManager.AddAsbestosDocument(request);
                    response = UserMessageConstants.DocumentSuccessMessage;
                    #endregion

                }
                catch (Exception ex)
                {
                    response = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                response = "You have not specified a file.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #endregion

        #region "Helper Method"

        #region "Send Email notification on Urgent Action Required"
        private string sendEmailNotificationOnUrgentAction(AsbestosDetailViewModel asbestosDetail)
        {
            try
            {
                // property address
                // specific job role persons email address
                var request = new AsbestosEmailNotificationsRequest();
                request.propertyId = SessionManager.GetSearchValue();
                request.requestType = SessionManager.GetRequestType();
                //var employeeEmailForAsbestosDS = new DataSet();
                var employeeEmailForAsbestos = this.Managers.HealthAndSafetyManager.GetEmployeeEmailForAsbestosEmailNotifications(request);
                if (employeeEmailForAsbestos.Employee != null && employeeEmailForAsbestos.Employee.Count > 0)
                {
                    foreach (var item in employeeEmailForAsbestos.Employee)
                    {
                        if (!string.IsNullOrEmpty(item.workEmail))
                        {
                            // send email
                            var mailMessage = new MailMessage();
                            mailMessage.Subject = ApplicationConstants.UrgentAsbestosEmailSubject;

                            // load email body

                            var body = new StringBuilder();
                            StreamReader reader = new StreamReader(Server.MapPath("~/Email/AsbestosUrgentAction.htm"));
                            body.Append(reader.ReadToEnd());

                            //==========================================='
                            // Employee Name
                            body.Replace("{FULLNAME}", item.fullName);
                            // Property Address
                            body.Replace("{PROPERTYADDRESS}", item.PropertyAddress);
                            //==========================================='
                            // There is an asbestos present at the below property. Please contact Broad land Housing for details.
                            // Attach logo images with email
                            //==========================================='

                            var logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Content/Images/broadland.png"));
                            logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                            body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                            var mimeType = new ContentType("text/html");
                            var alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                            alternatevw.LinkedResources.Add(logoBroadLandRepairs);

                            //'==========================================='

                            // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                            mailMessage.To.Add(new MailAddress(item.workEmail, item.fullName));
                            mailMessage.Body = body.ToString();
                            mailMessage.AlternateViews.Add(alternatevw);
                            mailMessage.IsBodyHtml = true;
                            EmailHelper.sendEmail(ref mailMessage);
                        }
                    }

                    return "Ok";
                }

                return "NaN";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region "Setup Directory"
        private string SetupDirectoryPath()
        {
            PropertyDetailRequest propertyRequest = GetPropertyDetailRequestParams();
            string directorypath = string.Empty;
            if (propertyRequest.type.Equals(ApplicationConstants.TypeProperty))
            {
                directorypath = FileHelper.getFileDirectoryPath(propertyRequest.type, propertyRequest.propertyId);
                FileHelper.createDirectory(directorypath);
            }
            else if (propertyRequest.type.Equals(ApplicationConstants.TypeBlock))
            {
                directorypath = FileHelper.getFileDirectoryPath(propertyRequest.type, propertyRequest.blockId.ToString());
                FileHelper.createDirectory(directorypath);
            }
            else if (propertyRequest.type.Equals(ApplicationConstants.TypeScheme))
            {
                directorypath = FileHelper.getFileDirectoryPath(propertyRequest.type, propertyRequest.schemeId.ToString());
                FileHelper.createDirectory(directorypath);
            }
            return directorypath;
        }
        #endregion

        #region "Get Asbestos List"
        public HealthAndSafetyViewModel GetAsbestosList(string id)
        {

            string page = HttpContext.Request.QueryString[QueryStringConstants.Page];
            string sortBy = HttpContext.Request.QueryString[QueryStringConstants.SortBy];
            string sortOrder = HttpContext.Request.QueryString[QueryStringConstants.SortOrder];

            var request = new AsbestosListingRequest();
            PropertyDetailRequest propertyRequest = GetPropertyDetailRequestParams();
            request.schemeId = propertyRequest.schemeId;
            request.blockId = propertyRequest.blockId;
            request.propertyId = propertyRequest.propertyId;
            request.type = propertyRequest.type;

            request.sortBy = "asbestosId";
            request.sortOrder = "DESC";
            int pageNo = 1;
            if (page != null && page != "0")
            {
                pageNo = Convert.ToInt32(page);
            }
            if (sortBy != null && sortOrder != null)
            {
                request.sortBy = sortBy.ToString();
                request.sortOrder = sortOrder.ToString();
            }
            request.pagination.pageSize = _PageSize;
            request.pagination.pageNo = pageNo;
            var response = this.Managers.HealthAndSafetyManager.GetAsbestosListing(request);
            HealthAndSafetyViewModel model = Mapper.Map<AsbestosListingResponse, HealthAndSafetyViewModel>(response);
            if (model != null)
            {
                ViewBag.TotalRows = model.asbestosList.pagination.totalRows;
                ViewBag.PageSize = model.asbestosList.pagination.pageSize;
                Session[SessionConstants.HealthAndSafetyInfo] = model;
            }

            return model;
        }
        #endregion

        #region "Check Valid Request"

        public bool IsValidRequest(string id)
        {
            bool isValid = true;
            string searchtype = SessionManager.GetRequestType();
            if (string.IsNullOrEmpty(searchtype)
                || string.IsNullOrEmpty(id))
            {
                isValid = false;
            }
            else
            {
                List<string> validSearchTypes = new List<string> { ApplicationConstants.TypeProperty
                                                                   , ApplicationConstants.TypeScheme
                                                                   , ApplicationConstants.TypeBlock };
                isValid = validSearchTypes.Where(x => x == searchtype).Count() > 0 ? true : false;
            }

            return isValid;
        }

        #endregion

        #region "Get Property Detail Request Params"
        public PropertyDetailRequest GetPropertyDetailRequestParams()
        {
            var request = new PropertyDetailRequest();
            string searchType = SessionManager.GetRequestType();
            string searchValue = SessionManager.GetSearchValue();
            request.type = searchType;

            if (searchValue != string.Empty)
            {
                string id = searchValue;
                if (searchType.Equals(ApplicationConstants.TypeScheme))
                {
                    request.schemeId = Convert.ToInt32(id);
                }
                else if (searchType.Equals(ApplicationConstants.TypeBlock))
                {
                    request.blockId = Convert.ToInt32(id);
                }
                else
                {
                    request.propertyId = id;
                }
            }

            return request;
        }
        #endregion

        #endregion

    }
}