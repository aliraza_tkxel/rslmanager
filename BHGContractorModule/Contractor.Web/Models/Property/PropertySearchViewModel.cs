﻿using System.Collections.Generic;

namespace Contractor.Web.Models.Property
{
    public class PropertySearchViewModel
    {
        public string Search { get; set; }
        public List<PropertySearchListViewModel> Properties { get; set; } = new List<PropertySearchListViewModel>();
        public List<PropertySearchListViewModel> Schemes { get; set; } = new List<PropertySearchListViewModel>();
        public List<PropertySearchListViewModel> Blocks { get; set; } = new List<PropertySearchListViewModel>();
    }
}