﻿namespace Contractor.Web.Models
{
    public class PropertyDetailViewModel
    {
        public string id { get; set; }
        public string propertyId { get; set; }
        public int? blockId { get; set; }
        public int? schemeId { get; set; }
        public string type { get; set; }
        public string fullHeading { get; set; }
    }
}