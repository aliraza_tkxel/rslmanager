﻿using System.Text.RegularExpressions;

namespace Contractor.Web.Models.Property
{
    public class PropertySearchListViewModel
    {
        public string SearchText { get; set; }
        public string Id { get; set; }
        public string Address { get; set; }
        public string TypeDescription { get; set; }
        public string PropertyType { get; set; }
        public string AddressDisplay
        {
            get
            {
                Regex expression = new Regex(SearchText.Replace(" ", "|"), RegexOptions.IgnoreCase);
                return expression.Replace(Address, new MatchEvaluator(ReplaceKeywords));
            }
        }

        public string ReplaceKeywords(Match m)
        {
            return "<b>" + m.Value + "</b>";
        }
    }
}