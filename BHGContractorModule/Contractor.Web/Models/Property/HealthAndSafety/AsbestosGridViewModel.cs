﻿using System.Collections.Generic;

namespace Contractor.Web.Models
{
    public class AsbestosGridViewModel
    {
        public List<AsbestosDetailViewModel> asbestosList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}