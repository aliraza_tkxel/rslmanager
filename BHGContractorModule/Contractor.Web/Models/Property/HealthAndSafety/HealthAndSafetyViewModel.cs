﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Contractor.Utilities.Constant;

namespace Contractor.Web.Models
{
    public class HealthAndSafetyViewModel
    {
        public AsbestosDetailViewModel asbestosDetail { get; set; } = new AsbestosDetailViewModel();
        public AsbestosDocumentViewModel asbestosDocument { get; set; } = new AsbestosDocumentViewModel();
        public AsbestosGridViewModel asbestosList { get; set; } = new AsbestosGridViewModel();
        public LookupsViewModel lookUps { get; set; }

        public List<SelectListItem> asbestosTypes
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.asbestosTypes
                              .Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() })
                              .ToList();
                return list;
            }
            set { }
        }
        public List<SelectListItem> asbestosElements
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.asbestosElements
                              .Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() })
                              .ToList();
                return list;
            }
            set { }
        }
        public List<SelectListItem> types
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.types
                              .Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpIdString })
                              .ToList();
                return list;
            }
            set { }
        }

        public List<SelectListItem> riskLevels
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.riskLevels
                              .Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() })
                              .ToList();
                return list;
            }
            set { }
        }

        public List<SelectListItem> documentCategories
        {
            get
            {
                // Only Compliance is selected due to business rule in Applinaces project
                var list = new List<SelectListItem>() { new SelectListItem() {
                                                        Text = ApplicationConstants.DocumentCategoryCompliance
                                                        , Value = ApplicationConstants.DocumentCategoryCompliance } };
                return list;
            }
            set { }
        }

        public List<SelectListItem> documentTypes
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.documentTypes
                              .Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() })
                              .ToList();
                return list;
            }
            set { }
        }

        public List<SelectListItem> documentSubTypes
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.documentSubTypes
                              .Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() })
                              .ToList();
                return list;
            }
            set { }
        }

    }
}