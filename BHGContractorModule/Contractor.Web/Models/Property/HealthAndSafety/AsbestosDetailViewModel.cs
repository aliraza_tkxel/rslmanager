﻿using System.ComponentModel.DataAnnotations;

namespace Contractor.Web.Models
{
    public class AsbestosDetailViewModel
    {
        [Display(Name = "asbestosId")]
        public int asbestosId { get; set; }

        [Display(Name = "employeeId")]
        public int? employeeId { get; set; }

        [Display(Name = "Asbestos Type")]
        [Required(ErrorMessage = "Please select asbestos type")]
        public int? asbestosTypeId { get; set; }

        [Display(Name = "Asbestos Type")]
        public string asbestosType { get; set; }

        [Display(Name = "Asbestos Elements")]
        [Required(ErrorMessage = "Please select asbestos element")]
        public int? asbestosElementId { get; set; }

        [Display(Name = "Asbestos Element")]
        public string asbestosElement { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please select type")]
        public string typeId { get; set; }

        [Display(Name = "Type")]
        public string type { get; set; }

        [Display(Name = "Risk Level")]
        [Required(ErrorMessage = "Please select risk level")]
        public int? riskLevelId { get; set; }

        [Display(Name = "Risk Level")]
        public string riskLevel { get; set; }

        [Display(Name = "Added")]
        [Required(ErrorMessage = "Please select added date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string addedDate { get; set; }

        [Display(Name = "Removed")]        
        public string removedDate { get; set; }

        [Display(Name = "Notes")]
        public string notes { get; set; }

        [Display(Name = "Urgent Action Required?")]
        public bool? isUrgentActionRequired { get; set; }

        [Display(Name = "createdBy")]
        public int? createdBy { get; set; }

        [Display(Name = "Created By Employee Name")]
        public string createdByEmployeeName { get; set; }

        public string createdByFullName { get; set; }
        

        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";

    }
}