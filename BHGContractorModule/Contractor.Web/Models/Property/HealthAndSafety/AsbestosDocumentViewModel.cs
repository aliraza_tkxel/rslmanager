﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Contractor.Web.Models
{
    public class AsbestosDocumentViewModel
    {
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please select category")]
        public string documentCategory { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please select document type")]
        public int documentTypeId { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please select title")]
        public int? documentSubTypeId { get; set; }

        [Display(Name = "Document")]
        [Required(ErrorMessage = "Please select document date")]
        public string documentDate { get; set; }

        [Display(Name = "Expiry")]
        [Required(ErrorMessage = "Please select expiry date")]
        public string expiryDate { get; set; }

        [Display(Name = "Keywords")]
        public string keywords { get; set; }

        [Required(ErrorMessage = "Document is required")]
        public HttpPostedFileBase file { get; set; }

    }
}