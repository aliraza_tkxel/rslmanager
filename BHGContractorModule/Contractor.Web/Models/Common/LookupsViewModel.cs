﻿using System.Collections.Generic;

namespace Contractor.Web.Models
{
    public class LookupsViewModel
    {
        #region "Property Detail"

        #region "Health & Safety"
        public List<LookUpFields> asbestosTypes { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> asbestosElements { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> types { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> riskLevels { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> documentTypes { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> documentSubTypes { get; set; } = new List<LookUpFields>();
        #endregion

        #endregion

    }

    public class LookUpFields
    {
        public int lookUpId { get; set; }
        public string lookUpDescription { get; set; }
        public string lookUpIdString { get; set; }
    }
}