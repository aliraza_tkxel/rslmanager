﻿using System.ComponentModel.DataAnnotations;

namespace Contractor.Web.Models.Login
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="You must enter a username") ]
        [StringLength(50, ErrorMessage = "You cannot enter a username of more than 50 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "You must enter a password")]
        [StringLength(50, ErrorMessage = "You cannot enter a password of more than 50 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int LoginCount { get; set; }

        public byte? Threshold { get; set; }
        public bool? Locked { get; set; }
        public string ResponseMessage { get; set; }
    }
}