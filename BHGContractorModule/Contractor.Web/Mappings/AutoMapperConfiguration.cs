﻿using AutoMapper;
using Contractor.BusinessObject;
using Contractor.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contractor.Web.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<AsbestosListingResponse, HealthAndSafetyViewModel>();
                cfg.CreateMap<HealthAndSafetyViewModel , AsbestosListingResponse>();

                cfg.CreateMap<AsbestosDetailViewModel, AsbestosDetailRequest>();
                cfg.CreateMap<AsbestosDetailRequest , AsbestosDetailViewModel>();

                cfg.CreateMap<AsbestosDocumentViewModel, AsbestosDocumentRequest>();
                cfg.CreateMap<AsbestosDocumentRequest , AsbestosDocumentViewModel>();

                cfg.CreateMap<AsbestosListingInfo, AsbestosGridViewModel>();
                cfg.CreateMap<AsbestosGridViewModel, AsbestosListingInfo>();

                cfg.CreateMap<AsbestosLookups, LookupsViewModel>();
                cfg.CreateMap<LookupsViewModel, AsbestosLookups>();

                cfg.CreateMap<AsbestosDetail, AsbestosDetailViewModel>();
                cfg.CreateMap<AsbestosDetailViewModel, AsbestosDetail>();

                cfg.CreateMap<Pagination, PaginationViewModel>();
                cfg.CreateMap<PaginationViewModel, Pagination>();

                cfg.CreateMap<Lookup, LookUpFields>();
                cfg.CreateMap<LookUpFields, Lookup>();

            });

        }
    }
}