USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_BlockTemplates]    Script Date: 12/05/2014 10:45:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[P_BlockTemplates](
	[BlockTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[BlockId] [int] NOT NULL,
 CONSTRAINT [PK_P_BlockTemplates] PRIMARY KEY CLUSTERED 
(
	[BlockTemplateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


