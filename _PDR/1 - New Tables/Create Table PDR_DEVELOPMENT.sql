USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_DEVELOPMENT]    Script Date: 01/08/2015 10:55:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PDR_DEVELOPMENT](
	[DEVELOPMENTID] [int] IDENTITY(1,1) NOT NULL,
	[PATCHID] [int] NULL,
	[DEVELOPMENTNAME] [nvarchar](100) NULL,
	[DEVELOPMENTTYPE] [int] NULL,
	[STARTDATE] [smalldatetime] NULL,
	[TARGETDATE] [smalldatetime] NULL,
	[ACTUALDATE] [smalldatetime] NULL,
	[DEFECTSPERIOD] [smalldatetime] NULL,
	[NUMBEROFUNITS] [float] NULL,
	[USETYPE] [int] NULL,
	[SUITABLEFOR] [int] NULL,
	[LOCALAUTHORITY] [int] NULL,
	[LEADDEVOFFICER] [int] NULL,
	[SUPPORTDEVOFFICER] [int] NULL,
	[PROJECTMANAGER] [nvarchar](100) NULL,
	[CONTRACTOR] [int] NULL,
	[INSURANCEVALUE] [money] NULL,
	[AQUISITIONCOST] [money] NULL,
	[CONTRACTORCOST] [money] NULL,
	[OTHERCOST] [money] NULL,
	[SCHEMECOST] [money] NULL,
	[DEVELOPMENTADMIN] [money] NULL,
	[INTERESTCOST] [money] NULL,
	[BALANCE] [money] NULL,
	[HAG] [money] NULL,
	[MORTGAGE] [money] NULL,
	[BORROWINGRATE] [money] NULL,
	[NETTCOST] [money] NULL,
	[SURVEYOR] [int] NULL,
	[SUPPORTEDCLIENTS] [int] NULL,
	[ADDRESS1] [varchar](500) NULL,
	[ADDRESS2] [varchar](500) NULL,
	[TOWN] [varchar](50) NULL,
	[COUNTY] [varchar](50) NULL,
	[Architect] [varchar](50) NULL,
	[DefectsPeriodEnd] [smalldatetime] NULL,
	[LandValue] [float] NULL,
	[FundingSource] [int] NULL,
	[GrantAmount] [float] NULL,
	[BorrowedAmount] [float] NULL,
	[OutlinePlanningApplication] [smalldatetime] NULL,
	[DetailedPlanningApplication] [smalldatetime] NULL,
	[OutlinePlanningApproval] [smalldatetime] NULL,
	[DetailedPlanningApproval] [smalldatetime] NULL,
	[PostCode] [varchar](50) NULL,
	[PurchaseDate] [smalldatetime] NULL,
 CONSTRAINT [PK_PDR_DEVELOPMENT] PRIMARY KEY CLUSTERED 
(
	[DEVELOPMENTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 60) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



