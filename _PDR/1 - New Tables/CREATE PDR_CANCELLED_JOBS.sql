USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PDR_CANCELLED_JOBS]    Script Date: 03/19/2015 16:41:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PDR_CANCELLED_JOBS](
	[CancelId] [INT] IDENTITY(1,1) NOT NULL,
	[AppointmentId] [INT] NULL,
	[RecordedOn] [SMALLDATETIME] NULL,
	[Notes] [NVARCHAR](1000) NULL,
 CONSTRAINT [PK_PDR_CANCELLED_JOBS] PRIMARY KEY CLUSTERED 
(
	[CancelId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PDR_CANCELLED_JOBS]  WITH CHECK ADD  CONSTRAINT [FK_PDR_CANCELLED_JOBS_PDR_APPOINTMENTS] FOREIGN KEY([AppointmentId])
REFERENCES [dbo].[PDR_APPOINTMENTS] ([APPOINTMENTID])
GO

ALTER TABLE [dbo].[PDR_CANCELLED_JOBS] CHECK CONSTRAINT [FK_PDR_CANCELLED_JOBS_PDR_APPOINTMENTS]
GO


