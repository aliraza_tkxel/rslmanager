/*
   Monday, February 02, 201510:35:08 AM
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.PDR_DocumentExpiryNotificationLog
	(
	DocumentId int NOT NULL,
	DocumentTitle varchar(200) NULL,
	UserId int NOT NULL,
	NotificationDate datetime NOT NULL,
	Status varchar(50) NOT NULL,
	Message varchar(500) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.PDR_DocumentExpiryNotificationLog SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_DocumentExpiryNotificationLog', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_DocumentExpiryNotificationLog', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_DocumentExpiryNotificationLog', 'Object', 'CONTROL') as Contr_Per 