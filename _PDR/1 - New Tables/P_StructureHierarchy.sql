USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_StructureHierarchy]    Script Date: 12/08/2014 12:25:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[P_StructureHierarchy](
	[StructureHeirarchyID] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[PSTypeId] [int] NOT NULL,
 CONSTRAINT [PK_P_StructureHierarchy] PRIMARY KEY CLUSTERED 
(
	[StructureHeirarchyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


