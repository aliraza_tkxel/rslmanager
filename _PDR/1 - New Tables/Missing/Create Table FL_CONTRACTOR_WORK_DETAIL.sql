USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[FL_CONTRACTOR_WORK_DETAIL]    Script Date: 02/03/2015 18:04:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FL_CONTRACTOR_WORK_DETAIL](
	[FaultWorkDetailId] [int] IDENTITY(1,1) NOT NULL,
	[FaultContractorId] [int] NOT NULL,
	[WorkRequired] [nvarchar](max) NOT NULL,
	[NetCost] [smallmoney] NOT NULL,
	[VatId] [int] NOT NULL,
	[Vat] [smallmoney] NOT NULL,
	[Gross] [smallmoney] NOT NULL,
	[ExpenditureId] [int] NOT NULL,
	[PURCHASEORDERITEMID] [int] NOT NULL,
 CONSTRAINT [PK_FL_CONTRACTOR_WORK_DETAIL] PRIMARY KEY CLUSTERED 
(
	[FaultWorkDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


