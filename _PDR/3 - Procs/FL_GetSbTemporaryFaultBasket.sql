USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetTemporaryFaultBasket]    Script Date: 01/22/2015 17:09:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Nor Muhammad>
-- Create date: <Create Date,, 22 Jan,2015>
-- Description:	<Description,,This procedure returns the fault in temporary fault basket>
-- WebPage: AvailableAppointments.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetSbTemporaryFaultBasket] 
	(
		@tempFaultIds as varchar(max),
		@tempFaultTradeIds as varchar(max),
		@SchemeId as Int,
		@blockId as int = null,
		@isRecall as BIT
	)
AS
BEGIN

	--=================================================================================================================
	--------------------------------------------- START Fetch Temporary Faults-----------------------------------------
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 Declare @selectTempClause varchar(3000)  
	Declare @fromTempClause varchar(1000)
	Declare @whereTempClause varchar(max)
	Declare @mainTempQuery varchar(max)
	
	--=================================================================================================================
	-- SELECT CLAUSE--
 SET @selectTempClause = ' SELECT
	FL_TEMP_FAULT.TempFaultId								AS TempFaultId
	,FL_Fault.FaultId										AS FaultId
	,AreaName												AS AreaName
	,FL_Fault.Description									AS Description
	,FL_Fault.Description									AS Description
	,COALESCE(FL_TEMP_FAULT.Duration, FL_Fault.Duration)	AS Duration
	,ISNULL(CONVERT(NVARCHAR, COALESCE(FL_TEMP_FAULT.Duration, FL_Fault.Duration))
	+ CASE
			WHEN COALESCE(FL_TEMP_FAULT.Duration,
				FL_Fault.Duration) <= 1
				THEN '' hr''
			ELSE '' hrs''
		END, ''N/A'')											AS DurationString
	,G_Trade.Description									AS TradeName
	,G_Trade.TradeId										AS TradeId
	,CASE
		days
		WHEN 1
			THEN CONVERT(CHAR(6), DATEADD(DAY, FL_Fault_Priority.ResponseTime, GETDATE()), 0)
		ELSE CONVERT(CHAR(6), DATEADD(HOUR, FL_Fault_Priority.ResponseTime, GETDATE()), 0)
	END														AS DueDate
	,FL_TEMP_FAULT.Quantity									AS Quantity
	,FL_TEMP_FAULT.ProblemDays								AS ProblemDays
	,FL_TEMP_FAULT.RecuringProblem							AS RecuringProblem
	,FL_TEMP_FAULT.CommunalProblem							AS CommunalProblem
	,FL_TEMP_FAULT.Notes									AS Notes
	,FL_TEMP_FAULT.OrgId									AS OrgId
	,FL_TEMP_FAULT.Recharge									AS Recharge
	,FL_TEMP_FAULT.FaultLogId								AS FaultLogId
	,FL_TEMP_FAULT.FaultTradeId								AS FaultTradeId
	,FL_TEMP_FAULT.IsAppointmentConfirmed					AS IsAppointmentConfirmed
	,ISNULL(FL_FAULT.IsGasSafe, 0)							AS IsGasSafe
	,ISNULL(FL_FAULT.IsOftec, 0)							AS IsOftec
	,(CASE
		WHEN FL_Fault_Priority.Days = 1
			THEN CONVERT(NVARCHAR(50), FL_Fault_Priority.ResponseTime) + '' days''
		ELSE CONVERT(NVARCHAR(50), FL_Fault_Priority.ResponseTime) + '' hours''
	END)													AS Response
	,CASE
		days
		WHEN 1
			THEN CONVERT(CHAR(12), DATEADD(DAY, FL_Fault_Priority.ResponseTime, GETDATE()), 103)
		ELSE CONVERT(CHAR(12), DATEADD(HOUR, FL_Fault_Priority.ResponseTime, GETDATE()), 103)
	END														AS CompleteDueDate
	,FL_Fault_Priority.PriorityId							AS PriorityId
	,FL_Fault_Priority.PriorityName							AS PriorityName '  

	--=================================================================================================================
	-- FROM CLAUSE--
	SET @fromTempClause = 'FROM FL_TEMP_FAULT 
	INNER JOIN FL_Fault ON FL_TEMP_FAULT.FaultId = FL_Fault.FaultId
	INNER JOIN FL_Area ON FL_Fault.AreaId = FL_Area.AreaId
	INNER JOIN FL_Fault_Trade ON FL_TEMP_FAULT.FaultTradeId = FL_Fault_Trade.FaultTradeId	
	INNER JOIN G_Trade ON FL_Fault_Trade.TradeId = G_Trade.TradeId
	INNER JOIN FL_Fault_Priority ON FL_Fault.PriorityId  = FL_Fault_Priority.PriorityId'
	
	--=================================================================================================================
	-- WHERE CLAUSE--
	SET @whereTempClause  = ' WHERE FL_TEMP_FAULT.TempFaultId IN ('+@tempFaultIds+')
	AND G_Trade.TradeId IN ('+@tempFaultTradeIds+')
	AND isAppointmentconfirmed is null	
	AND FL_TEMP_FAULT.ISRECALL = '+ CONVERT(varchar(2),@isRecall) +'
	ORDER BY FL_Fault_Trade.TradeId ASC'
	
	SET @mainTempQuery  = @selectTempClause  + CHAR(10) + @fromTempClause  + CHAR(10) + @whereTempClause 
	
	print @mainTempQuery
	EXEC (@mainTempQuery)
	
	-- --------------------------------------END - Fetch Temporary Faults----------------------------------------------
	--=================================================================================================================
	
	--=================================================================================================================
	---------------------------------------- START Fetch Confirmed Appointments----------------------------------------
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @selectAppointClause varchar(2000)
	Declare @fromAppointClause varchar(1000)
	Declare @whereAppointClause varchar(max)
	Declare @mainAppointQuery varchar(max)		
	DECLARE @searchCriteria NVARCHAR(4000)
	--=================================================================================================================
	-- SELECT CLAUSE--
	SET @selectAppointClause = '
	SELECT
	FL_TEMP_FAULT.TempFaultId								AS TempFaultId
	,FL_Fault.FaultId										AS FaultId
	,AreaName												AS AreaName
	,FL_Fault.Description									AS Description
	,COALESCE(FL_TEMP_FAULT.Duration, FL_Fault.Duration)	AS Duration
	,ISNULL(CONVERT(NVARCHAR, COALESCE(FL_TEMP_FAULT.Duration, FL_Fault.Duration))
	+ CASE
			WHEN COALESCE(FL_TEMP_FAULT.Duration,
				FL_Fault.Duration) <= 1
				THEN '' hr''
			ELSE '' hrs''
		END, ''N/A'')											AS DurationString
	,G_Trade.Description									AS TradeName
	,G_Trade.TradeId										AS TradeId
	,CASE
		days
		WHEN 1
			THEN CONVERT(CHAR(6), DATEADD(DAY, FL_Fault_Priority.ResponseTime, GETDATE()), 0)
		ELSE CONVERT(CHAR(6), DATEADD(HOUR, FL_Fault_Priority.ResponseTime, GETDATE()), 0)
	END														AS DueDate
	,FL_CO_APPOINTMENT.AppointmentId						AS AppointmentId
	,FL_CO_APPOINTMENT.AppointmentDate						AS AppointmentDate
	,FL_CO_APPOINTMENT.OperativeID							AS OperativeId
	,FL_CO_APPOINTMENT.Time									AS StartTime
	,FL_CO_APPOINTMENT.EndTime								AS EndTime
	,E__EMPLOYEE.FirstName + '' '' + E__EMPLOYEE.LastName	AS OperativeName
	,FL_TEMP_FAULT.FaultLogId								AS FaultLogId 
	,COALESCE(AppointmentEndDate,AppointmentDate)			AS AppointmentEndDate ' 
	
	--=================================================================================================================
	-- FROM CLAUSE--
	SET @fromAppointClause  = 'FROM FL_Temp_fault 
	INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId = FL_Temp_fault.FaultLogId
	INNER JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentId =FL_FAULT_APPOINTMENT.AppointmentId
	INNER JOIN FL_Fault ON FL_TEMP_FAULT.FaultId = FL_Fault.FaultId
	INNER JOIN FL_Area ON FL_Fault.AreaId = FL_Area.AreaId
	INNER JOIN FL_Fault_Trade ON FL_TEMP_FAULT.FaultTradeId = FL_Fault_Trade.FaultTradeId	
	INNER JOIN G_Trade ON FL_Fault_Trade.TradeId = G_Trade.TradeId
	INNER JOIN FL_Fault_Priority ON FL_Fault.PriorityId  = FL_Fault_Priority.PriorityId
	INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EmployeeId = FL_CO_APPOINTMENT.OperativeId'

	-- These conditions will be added into where clause based on search criteria provided      
      
    
    SET @searchCriteria = 'And 1=1 '
       
	IF @schemeId IS NOT NULL AND @schemeId > 0    
	   BEGIN    
		SET @searchCriteria = +'  AND SchemeId = ' + CONVERT(varchar(10), @schemeId) + ' '      
	   END       
  IF @blockId IS NOT NULL AND  @blockId >0    
	   BEGIN    
		SET @searchCriteria =@searchCriteria + CHAR(10) +'  AND BlockId = ' +  CONVERT(varchar(10), @blockId) + ' '      
	   END    
  -- End building SearchCriteria clause  
	--=================================================================================================================
	-- WHERE CLAUSE--
	SET @whereAppointClause  = 'WHERE 
	FL_Temp_fault.faultlogid > 0 
	AND FL_TEMP_FAULT.ISRECALL = '+ CONVERT(varchar(2),@isRecall) + CHAR(10) +@searchCriteria
	
	 
	
	SET @mainAppointQuery  = @selectAppointClause  + CHAR(10) + @fromAppointClause  + CHAR(10) + @whereAppointClause 
	
	print @mainAppointQuery
	EXEC (@mainAppointQuery)
	-- --------------------------------------END - Fetch Confirmed Appointments----------------------------------------
	--=================================================================================================================
END
