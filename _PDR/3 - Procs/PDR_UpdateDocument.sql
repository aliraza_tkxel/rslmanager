USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_UpdateDocument]    Script Date: 02/18/2015 16:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,09/02/2015>
-- Description:	<Description,,Update document after uploading on directory>
-- =============================================
CREATE PROCEDURE [dbo].[PDR_UpdateDocument]
@title varchar(50),
@DocId int,
@expires smalldatetime,
@DevelopmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update PDR_DOCUMENTS Set Title = @title , Expires = @expires Where DocumentID = @DocId
	 
	 If (@DevelopmentId > 0)
	 BEGIN
		Exec PDR_SaveDevelopmentDocument @DevelopmentId,@DocId
	 END
END
