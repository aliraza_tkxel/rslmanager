USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyStatus]    Script Date: 12/10/2014 18:27:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description: Get Property Status for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Status for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyStatus
  =================================================================================*/

CREATE PROCEDURE [dbo].[PDR_GetPropertyStatus]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select S.STATUSID,S.DESCRIPTION from P_STATUS S
	
END
