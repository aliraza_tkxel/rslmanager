-- Stored Procedure

/* =================================================================================    
    Page Description:  Delete Warranty Data

    Author: Salman Nazir
    Creation Date: Dec-25-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-25-2014      Salman Nazir           Delete Warranty Data

    Execution Command:

    Exec PDR_DeleteWarranty 1
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_DeleteWarranty] 
@warrantyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update PDR_WARRANTY Set PROPERTYID = null, BLOCKID = null,SCHEMEID = null Where WARRANTYID = @warrantyId
END
GO