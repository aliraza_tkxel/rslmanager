-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,08/12/2014>
-- Description:	<Description,,Update Phase's parent id after saving Development>
-- =============================================
CREATE PROCEDURE PDR_UpdatePhaseHeirarchyParent
@developmentId int,
@phaseId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @developmentHierarchyId int, @phaseHierarchyId int
	Select @developmentHierarchyId = StructureHeirarchyID from P_DEVELOPMENT Where DEVELOPMENTID = @developmentId
	
	Select @phaseHierarchyId = StructureHeirarchyID From P_PHASE Where PHASEID = @phaseId
	
	Update P_StructureHierarchy Set ParentId = @developmentHierarchyId Where StructureHeirarchyID = @phaseHierarchyId
	
END
GO
