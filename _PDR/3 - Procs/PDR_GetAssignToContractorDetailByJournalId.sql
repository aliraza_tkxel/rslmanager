SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 29/1/2015
-- Description:	Get Assign To Contractor by JournalId
-- =============================================
CREATE PROCEDURE PDR_GetAssignToContractorDetailByJournalId 
	-- Add the parameters for the stored procedure here
	@JournalId INT	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


DECLARE @pdrContractorId int

SELECT	@pdrContractorId = PDRContractorId
FROM	PDR_CONTRACTOR_WORK
WHERE	JournalId = @JournalId

SELECT	PDR_CONTRACTOR_WORK.PDRContractorId 
		,PDR_CONTRACTOR_WORK.ContractorId
		,PDR_CONTRACTOR_WORK.ContactId
		,S_ORGANISATION.name ContractorName
		,PDR_CONTRACTOR_WORK.Estimate
		,PDR_CONTRACTOR_WORK.EstimateRef
		--,CONVERT(VARCHAR(10),PDR_CONTRACTOR_WORK.ContractStartDate,103) AS ContractStartDate
		--,CONVERT(VARCHAR(10),PDR_CONTRACTOR_WORK.ContractEndDate,103) AS ContractEndDate
		,PDR_CONTRACTOR_WORK.ContractStartDate
		,PDR_CONTRACTOR_WORK.ContractEndDate
FROM	PDR_CONTRACTOR_WORK
		INNER JOIN S_ORGANISATION ON PDR_CONTRACTOR_WORK.ContractorId = S_ORGANISATION.ORGID
WHERE	PDRContractorId = @pdrContractorId


SELECT	PCWD.WorkDetailId
		,PCWD.ServiceRequired
		,PCWD.NetCost
		,PCWD.VatId AS VatType
		,PCWD.Vat
		,PCWD.Gross
		,FP.PISTATUS AS PIStatus
		,PCWD.ExpenditureId
		,PCWD.BudgetHeadId
		,PCWD.CostCenterId		
FROM	PDR_CONTRACTOR_WORK_DETAIL PCWD
		INNER JOIN F_PURCHASEITEM FP ON PCWD.PURCHASEORDERITEMID = FP.ORDERITEMID
WHERE	PCWD.PDRContractorId = @pdrContractorId


END
GO