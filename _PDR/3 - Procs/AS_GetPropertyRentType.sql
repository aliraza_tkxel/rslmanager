USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyRentType]    Script Date: 03/25/2015 11:27:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
--EXEC [dbo].[AS_GetPropertyRentType]  
-- @propertyId='BHA0000018'  
-- Author:  <Aqib Javed>  
-- Create date: <18-Apr-2014>  
-- Description: <This procedure 'll get the Rent Type and Associate Financial information >  
-- Web Page: FinancialTab.ascx  
ALTER PROCEDURE [dbo].[AS_GetPropertyRentType]   
 @propertyId varchar(100)    
AS  
BEGIN  
 Select TENANCYTYPEID, [DESCRIPTION] from C_TENANCYTYPE order by TENANCYTYPEID  
 Select * from P_FINANCIAL where propertyId=@propertyId  
	Select FID, [DESCRIPTION] from P_RENTFREQUENCY order by [DESCRIPTION]
END