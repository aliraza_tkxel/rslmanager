-- =============================================
-- EXEC AS_AppointmentsCalendarDetail @startDate='2012-12-31',@endDate='2014-01-04'
-- Author:  Aqib Javed
-- Create date: <9/21/2012>
-- Last Modified: <04/01/2013>
-- Description:
-- Webpage : WeeklyCalendar.aspx, MonthlyCalendar.aspx
-- =============================================

ALTER PROCEDURE [dbo].[As_appointmentscalendardetail] @startDate DATE, @endDate DATE AS BEGIN ; WITH cte_appointment (appointmentshift, appointmentdate, NAME, mobile, custaddress, assignedto, postcode, title, appointmentstarttime, appointmentendtime, [description], [type], appointmentenddate) AS
  (SELECT DISTINCT CASE
                       WHEN Datepart(hour, planned_appointments.appointmentstarttime) < 12 THEN 'AM'
                       ELSE 'PM'
                   END AS APPOINTMENTSHIFT,
                   CONVERT(VARCHAR, appointmentdate, 103) AS APPOINTMENTDATE,
                   Isnull(CG.list, 'N/A') AS NAME,
                   '' AS MOBILE,
                   Isnull(p__property.housenumber, '') + ' ' + Isnull(p__property.address1, '') + ' ' + Isnull(p__property.address2, '') + ' ' + Isnull(p__property.address3, '') AS CUSTADDRESS,
                   planned_appointments.assignedto,
                   p__property.postcode,
                   planned_status.title,
                   planned_appointments.appointmentstarttime,
                   planned_appointments.appointmentendtime,
                   p_status.[description],
                   'Planned' AS TYPE,
                   CONVERT(VARCHAR, appointmentenddate, 103) AS APPOINTMENTENDDATE
   FROM planned_appointments
   INNER JOIN planned_journal ON planned_appointments.journalid = planned_journal.journalid
   INNER JOIN planned_status ON planned_journal.statusid = planned_status.statusid
   INNER JOIN p__property ON planned_journal.propertyid = p__property.propertyid
   INNER JOIN p_status ON p__property.[status] = p_status.statusid
   INNER JOIN planned_component_trade pcd ON planned_appointments.comptradeid = pcd.comptradeid
   LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid
   AND (dbo.c_tenancy.enddate IS NULL
        OR dbo.c_tenancy.enddate > Getdate())
   LEFT JOIN c_customer_names_grouped CG ON CG.i = dbo.c_tenancy.tenancyid
   AND CG.id IN
     (SELECT Max(id) ID
      FROM c_customer_names_grouped
      GROUP BY i)
   WHERE planned_appointments.appointmentdate BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR planned_appointments.appointmentenddate BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR (planned_appointments.appointmentdate <= CONVERT(DATE, @startDate, 103)
         AND planned_appointments.appointmentenddate >= CONVERT(DATE, @endDate, 103))
   UNION ALL SELECT DISTINCT as_appointments.appointmentshift,
                             CONVERT(VARCHAR, appointmentdate, 103) AS APPOINTMENTDATE,
                             Isnull(CG.list, 'N/A') AS NAME --,C__CUSTOMER.FIRSTNAME+'  '+C__CUSTOMER.LASTNAME AS Name

                                                          ,
                                                          '' AS MOBILE --C_ADDRESS.MOBILE

                                                                     ,
                                                                     Isnull(p__property.housenumber, '') + ' ' + Isnull(p__property.address1, '') + ' ' + Isnull(p__property.address2, '') + ' ' + Isnull(p__property.address3, '') AS CUSTADDRESS,
                                                                     as_appointments.assignedto,
                                                                     p__property.postcode,
                                                                     as_status.title,
                                                                     as_appointments.appointmentstarttime,
                                                                     as_appointments.appointmentendtime,
                                                                     p_status.[description],
                                                                     'GAS' AS TYPE,
                                                                     CONVERT(VARCHAR, appointmentdate, 103) AS APPOINTMENTENDDATE
   FROM as_appointments
   INNER JOIN as_journal ON as_appointments.journalid = as_journal.journalid
   INNER JOIN as_status ON as_journal.statusid = as_status.statusid
   INNER JOIN p__property ON as_journal.propertyid = p__property.propertyid
   INNER JOIN p_status ON p__property.[status] = p_status.statusid
   LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid
   AND (dbo.c_tenancy.enddate IS NULL
        OR dbo.c_tenancy.enddate > Getdate())
   LEFT JOIN c_customer_names_grouped CG ON CG.i = dbo.c_tenancy.tenancyid
   AND CG.id IN
     (SELECT Max(id) ID
      FROM c_customer_names_grouped
      GROUP BY i)
   WHERE as_journal.iscurrent = 1
     AND as_appointments.appointmentdate BETWEEN @startDate AND @endDate
   UNION ALL SELECT DISTINCT CASE
                                 WHEN Datepart(hour, pdr_appointments.appointmentstarttime) < 12 THEN 'AM'
                                 ELSE 'PM'
                             END AS APPOINTMENTSHIFT,
                             CONVERT(VARCHAR, appointmentstartdate, 103) AS APPOINTMENTDATE,
                             Isnull(CG.list, 'N/A') AS NAME,
                             '' AS MOBILE,
                             Isnull(p__property.housenumber, '') + ' ' + Isnull(p__property.address1, '') + ' ' + Isnull(p__property.address2, '') + ' ' + Isnull(p__property.address3, '') AS CUSTADDRESS,
                             pdr_appointments.assignedto,
                             p__property.postcode,
                             pdr_status.title,
                             pdr_appointments.appointmentstarttime,
                             pdr_appointments.appointmentendtime,
                             p_status.[description],
                             PDR_MSATType.MSATTypeName AS TYPE,
                             CONVERT(VARCHAR, appointmentenddate, 103) AS APPOINTMENTENDDATE
   FROM PDR_APPOINTMENTS
   INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
   INNER JOIN PDR_STATUS ON PDR_APPOINTMENTS.APPOINTMENTSTATUS = PDR_STATUS.statusid
  INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
   LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
   LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
   LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
   INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
   LEFT JOIN p_status ON p__property.[status] = p_status.statusid
   LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid
   AND (dbo.C_TENANCY.ENDDATE IS NULL
        OR dbo.C_TENANCY.ENDDATE > Getdate())
   LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.i = dbo.C_TENANCY.tenancyid
   AND CG.id IN
     (SELECT Max(id) ID
      FROM C_CUSTOMER_NAMES_GROUPED
      GROUP BY i)
   WHERE PDR_APPOINTMENTS.APPOINTMENTSTARTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR PDR_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR (PDR_APPOINTMENTS.APPOINTMENTSTARTDATE <= CONVERT(DATE, @startDate, 103)
         AND PDR_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103)))
SELECT appointmentshift,
       appointmentdate,
       NAME,
       mobile,
       custaddress,
       assignedto,
       postcode,
       title,
       appointmentstarttime,
       appointmentendtime,
       [description],
       [type],
       appointmentenddate
FROM cte_appointment
ORDER BY appointmentdate ASC,
         appointmentstarttime ASC END