-- =============================================    
-- Exec AS_SavePropertyTargetRentInformation   
  
 --@rentType =2,    
 --@rent =145.00,    
 --@councilTax=11.00,    
 --@water=10.00,    
 --@inetlig=10.00,    
 --@water=10.00,    
 --@inetlig=10.00,    
 --@supp=10.00,    
 --@garage=10.00,    
 --@totalRent=10.00,    
 --@propertyId='BHA0000020',    
 --@userId=605    
-- Author:  <Aqib Javed>    
-- Create date: <25-4-2014>    
-- Description: <This store procedure will save the information of Current Rent>    
-- Web Page: TargetRentTab.ascx    
-- =============================================    
Create PROCEDURE [dbo].[AS_SavePropertyTargetRentInformation]    
   
 @rentType int,   
 @rent decimal,    
 @services decimal,    
 @councilTax decimal,    
 @water decimal,    
 @inetlig decimal,    
 @supp decimal,    
 @garage decimal,    
 @totalRent decimal,    
 @propertyId varchar(100),    
 @userId int    
AS    
BEGIN
    
Declare @isRecordExist int
SELECT
	@isRecordExist = COUNT(PROPERTYID)
FROM P_FINANCIAL_TARGET
WHERE PROPERTYID = @propertyId
IF (@isRecordExist <= 0) BEGIN
INSERT INTO P_FINANCIAL_TARGET (RENTTYPE, RENT, [SERVICES], COUNCILTAX, WATERRATES, INELIGSERV, SUPPORTEDSERVICES, GARAGE, TOTALRENT, PROPERTYID, PFUSERID)
	VALUES (@rentType, @rent, @services, @councilTax, @water, @inetlig, @supp, @garage, @totalRent, @propertyId, @userId)
END ELSE BEGIN
UPDATE P_FINANCIAL_TARGET
SET	RENTTYPE = @rentType,
	RENT = @rent,
	[SERVICES] = @services,
	COUNCILTAX = @councilTax,
	WATERRATES = @water,
	INELIGSERV = @inetlig,
	SUPPORTEDSERVICES = @supp,
	GARAGE = @garage,
	TOTALRENT = @totalRent,
	PFUSERID = @userId
WHERE PROPERTYID = @propertyId
END

END