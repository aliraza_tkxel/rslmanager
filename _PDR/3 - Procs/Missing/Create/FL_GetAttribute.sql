USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetArea]    Script Date: 02/01/2015 13:54:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: Get Attribute for Dop down list over scheme/Block Assign to contractor PopUp 
 
    Author: Ali Raza
    Creation Date: Feb-1-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Feb-1-2015     Ali Raza           Get Attribute for Dop down list over scheme/Block Assign to contractor PopUp 
    
    Execution Command:
    
    Exec FL_GetAttribute 6
  =================================================================================*/
CREATE PROCEDURE [dbo].[FL_GetAttribute]
	-- Add the parameters for the stored procedure here
	@areaId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select DISTINCT  PA_ITEM.ItemID ,PA_AREA.AreaName+' > '+ ItemName as  ItemName from PA_ITEM
INNER JOIN PA_AREA ON PA_AREA.AreaID = PA_ITEM.AreaID
INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM.ItemID= PA_ITEM_PARAMETER.ItemId
INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
Where PA_ITEM.AreaID = @areaId AND PA_PARAMETER.ParameterName like '%Replacement Due%'
GROUP By PA_ITEM.ItemID,ItemName,PA_AREA.AreaName

END
