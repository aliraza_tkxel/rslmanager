/****** Object:  StoredProcedure [dbo].[P_GetPropertyEPCImages]    Script Date: 02/25/2015 15:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC P_GetPropertyImages
--@propertyId = 'BHA0000119'
-- Author:		<Author,,Muhammad Awais>
-- Create date: <Create Date,, 17 Sep,2013>
-- Description:	<Description,,This stored procedure returns all the epc images against the property>
-- =============================================
CREATE PROCEDURE [dbo].[P_GetPropertyEPCImages] 
	@propertyId as varchar(20)
	,@count int out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 SELECT 
		PA_PROPERTY_EPC_IMAGES.ImagePath 
		,ImageName
		,'Stock' as Type
		,COnvert(varchar(10),CreatedOn,103) as CreatedOn
		,(ISNULL(E__EMPLOYEE.FirstNAME,'')+' '+ISNULL(E__EMPLOYEE.LastName,'')) as CreatedBy
		,PA_PROPERTY_EPC_IMAGES.Title
	FROM
		PA_PROPERTY_EPC_IMAGES
		INNER JOIN E__EMPLOYEE on PA_PROPERTY_EPC_IMAGES.CreatedBy = E__EMPLOYEE.EmployeeId
		INNER JOIN P__PROPERTY ON P__PROPERTY.PropertyPicId = PA_PROPERTY_EPC_IMAGES.SID
		
	WHERE 
		PA_PROPERTY_EPC_IMAGES.PROPERTYID=@propertyId
			
    SET @count = (SELECT COUNT(*) from PA_PROPERTY_EPC_IMAGES Where 	PROPERTYID=@propertyId	)

END
