/****** Object:  StoredProcedure [dbo].[AS_SaveEPCPhotograph]    Script Date: 02/25/2015 11:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Muhammad Awais>  
-- Create date: <25-Feb-2015>  
-- Description: <Save the Photograph for property>  
-- WebPage: PropertyRecord.aspx => Summary Tab  
-- =============================================  
CREATE PROCEDURE [dbo].[AS_SaveEPCPhotograph] ( 
@propertyId varchar(500),  
@itemId int,  
@title varchar(500),  
@uploadDate smalldatetime,  
@imagePath varchar(1000),  
@imageName varchar(500),  
@createdBy int,
@isDefaultImage bit
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  

	DECLARE @propertyPicId INT 
	 
	INSERT INTO PA_PROPERTY_EPC_IMAGES  
		([PROPERTYID], [ItemId], [ImagePath], [ImageName], [CreatedOn], [CreatedBy], [Title])  
	VALUES  
		(@propertyId, NULL, @imagePath, @imageName, @uploadDate, @createdBy, @title)  
	
	--SET @propertyPicId=SCOPE_IDENTITY()
	--IF @isDefaultImage = 1 And @propertyPicId > 0
	--	 Begin 
	--		 Update P__PROPERTY SET PropertyPicId = @propertyPicId WHERE PROPERTYID = @propertyId  
	--	 END
END  
