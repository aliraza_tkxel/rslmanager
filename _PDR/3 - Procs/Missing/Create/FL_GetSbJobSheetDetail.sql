-- Stored Procedure

-- =============================================
--EXEC FL_GetJobSheetDetail
--@jobSheetNumber ='JS5'
-- Author:		<Ahmed Mehmood>
-- Create date: <6/2/2013>
-- Description:	<Returns Job Sheet Summary>
-- Webpage:JobSheetDetail.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetSbJobSheetDetail] 
	-- Add the parameters for the stored procedure here
	(
	@jobSheetNumber varchar(50)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Fault Log Info and Appointment Info

	Select	FL_FAULT_LOG.JobSheetNumber JSN,
			ISNULL(S_ORGANISATION.NAME,'N/A') ContractorName,
			ISNULL(E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME,'N/A') as OperativeName,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as Priority,
			convert(VARCHAR, FL_FAULT_LOG.DueDate,106)+' '+ convert(VARCHAR(5), FL_FAULT_LOG.DueDate,108)as Completion,
			convert(VARCHAR, FL_FAULT_LOG.SubmitDate,106)+' '+convert(VARCHAR(5), FL_FAULT_LOG.SubmitDate,108)as OrderDate,
			FL_AREA.AreaName Location,
			FL_FAULT.Description Description,
			FL_FAULT_LOG.Notes Notes,
			convert(VARCHAR(20), FL_CO_APPOINTMENT.AppointmentDate,106)as AppointmentTimeDate,
			FL_CO_APPOINTMENT.Notes Notes,
			ISNULL(LEFT(CONVERT(TIME,FL_CO_APPOINTMENT.Time),5) + ' - ' +LEFT(CONVERT(TIME,FL_CO_APPOINTMENT.EndTime),5) + ' '+datename(weekday,FL_CO_APPOINTMENT.AppointmentDate),'N/A') Time ,
			G_TRADE.[Description] as Trade,
			FL_FAULT_STATUS.Description FaultStatus,
		    ISNULL(FL_CO_APPOINTMENT.RepairNotes ,'N/A') RepairNotes,
			FL_FAULT_LOG.PROPERTYID AS [PROPERTYID]


	From    FL_FAULT_LOG
			left outer join FL_FAULT_APPOINTMENT on  FL_FAULT_LOG.FaultLogID=FL_FAULT_APPOINTMENT.FaultLogId
			left outer join FL_CO_APPOINTMENT on FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
			inner join FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
			inner join FL_FAULT_PRIORITY  on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
			inner join FL_AREA on FL_AREA.AreaID = FL_FAULT.AREAID 
			left outer join S_ORGANISATION on S_ORGANISATION.ORGID=FL_FAULT_LOG.ORGID 
			left outer join E__EMPLOYEE on FL_CO_APPOINTMENT.OperativeID =E__EMPLOYEE.EMPLOYEEID
			inner join FL_FAULT_TRADE on FL_FAULT_LOG.FaultTradeId =FL_FAULT_TRADE.FaultTradeId 
			inner join G_TRADE on G_TRADE.TradeId = FL_FAULT_TRADE.TradeId
			inner join FL_FAULT_STATUS on FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID 
		   LEFT OUTER JOIN (Select * from FL_FAULT_LOG_HISTORY Where FaultStatusID = 17 ) as TblRepairNotes on TblRepairNotes.FaultLogID = FL_FAULT_LOG.FaultLogID

	Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
	Order by TblRepairNotes.LastActionDate DESC



-- Customer Info

Select ISNULL(P_SCHEME.SCHEMENAME,' ') as SchemeName, 
Case 
	When FL_FAULT_LOG.BlockId IS NOT NULL THEN P_BLOCK.BLOCKNAME
	ELSE 'Non Selected' END as [BlockName],

Case 
	When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.ADDRESS1,' ')+' '+ISNULL(P_BLOCK.ADDRESS2,' ')+' '+ISNULL(P_BLOCK.ADDRESS3,' ') 
	ELSE ISNULL(PDR_DEVELOPMENT.ADDRESS1,' ')+' '+ISNULL(PDR_DEVELOPMENT.ADDRESS2,' ') END as [Address],
Case	
	When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.TOWNCITY,'N/A')
	ELSE ISNULL(PDR_DEVELOPMENT.TOWN,'N/A ') END as Town,

Case	
	When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.COUNTY,'N/A')
	ELSE ISNULL(PDR_DEVELOPMENT.COUNTY,'N/A ') END as County,
Case	
	When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.POSTCODE,'N/A')
	ELSE ISNULL(PDR_DEVELOPMENT.PostCode,'N/A ') END as PostCode		


 from FL_FAULT_LOG

 INNER JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
 Left JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID  
 LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID	
Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber


END
GO