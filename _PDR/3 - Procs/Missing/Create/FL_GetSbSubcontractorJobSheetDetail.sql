USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetSubcontractorJobSheetDetail]    Script Date: 01/29/2015 16:10:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- EXEC FL_GetSubcontractorJobSheetDetail
--  @tempfaultId = 1487,
--  @tradeId = 1
-- Author:		<Ahmed Mehmood>
-- Create date: <08/03/2013>
-- Description:	<Get Fault Basket Info FL_TEMP_FAULT table>
-- Web Page: JobSheetSummarySubcontractor.aspx
-- Modification: <Aamir Waheed, 08/05/2013>
-- =============================================

CREATE PROCEDURE [dbo].[FL_GetSbSubcontractorJobSheetDetail] 

 (
	@tempfaultId INT,
	@tradeId INT
  )
AS

	-- Fault and Subcontractor Information
	
	Select  S_ORGANISATION.ORGID contractorId ,
		    S_ORGANISATION.NAME contractorName,
		    FL_FAULT_PRIORITY.PriorityID priorityId,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as priority,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(VARCHAR, DATEADD(DAY ,FL_FAULT_PRIORITY.ResponseTime,GETDATE()),106)+' '+ convert(VARCHAR(5), DATEADD(DAY ,FL_FAULT_PRIORITY.ResponseTime,GETDATE()),108)
			ELSE
			convert(VARCHAR, DATEADD(Hour,FL_FAULT_PRIORITY.ResponseTime,GETDATE()),106)+' '+ convert(VARCHAR(5), DATEADD(Hour,FL_FAULT_PRIORITY.ResponseTime,GETDATE()),108)
			END as completionDue,
			convert(VARCHAR, GETDATE(),106)+' '+ convert(VARCHAR(5), GETDATE(),108) as orderDate,
			FL_AREA.AreaName location,
			FL_FAULT.[Description] description,
			G_TRADE.[Description] trade,
			G_TRADE.TradeId tradeId,
			FL_TEMP_FAULT.TempFaultID tempFaultId,
			FL_TEMP_FAULT.Notes notes,
			FL_TEMP_FAULT.FaultID faultId,
			FL_TEMP_FAULT.ORGID orgid,
			FL_TEMP_FAULT.Quantity,
			FL_TEMP_FAULT.ProblemDays,
			FL_TEMP_FAULT.RecuringProblem,
			FL_TEMP_FAULT.CommunalProblem,
			FL_TEMP_FAULT.ItemActionId ,
			FL_TEMP_FAULT.Recharge ,
			FL_FAULT_TRADE.FaultTradeId ,
			FL_TEMP_FAULT.OriginalFaultLogId,
			ISNULL(S_ORGANISATION.Email, '') Email 
			
	from	FL_TEMP_FAULT 
			Inner Join S_ORGANISATION on FL_TEMP_FAULT.ORGID = S_ORGANISATION.ORGID
			Inner Join FL_FAULT On FL_TEMP_FAULT.FaultID = FL_FAULT.FaultID
			Inner Join FL_FAULT_TRADE On FL_FAULT.FaultId = FL_FAULT_TRADE.FaultId 
			Inner Join G_TRADE  On FL_FAULT_TRADE.TradeId = G_TRADE.TradeId 
			Inner Join FL_AREA  On FL_FAULT.AREAID = FL_AREA.AreaID 
			Inner Join FL_FAULT_PRIORITY On FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID  
	Where	FL_TEMP_FAULT.TempFaultID = @tempfaultId and G_TRADE.TradeId = @tradeId
	
	
	-- Asbestos Information
	
	Select	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID ,
			P_Asbestos.RISKDESCRIPTION Description
			
	From  P_PROPERTY_ASBESTOS_RISKLEVEL
	INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
	INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID
	
	Where P_PROPERTY_ASBESTOS_RISKLEVEL.SchemeId = (Select SchemeId From FL_TEMP_FAULT Where	FL_TEMP_FAULT.TempFaultID = @tempfaultId)
	And (P_PROPERTY_ASBESTOS_RISKLEVEL.BlockId IS NULL OR P_PROPERTY_ASBESTOS_RISKLEVEL.BlockId =(Select BlockId From FL_TEMP_FAULT 
	Where	FL_TEMP_FAULT.TempFaultID = @tempfaultId) )
