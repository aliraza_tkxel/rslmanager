USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GETCOMPLETEDWORKSLIST]    Script Date: 03/19/2015 12:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,18/03/2015>
-- Description:	<Description,,Get the completed planned works list for completed works report in reports area of Planned>
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GETCOMPLETEDWORKSLIST] 
@schemeId int =-1,
@plannedType int =-1,
@searchText VARCHAR(200),
--Parameters which would help in sorting and paging
@pageSize int = 30,
@pageNumber int = 1,
@sortColumn varchar(500) = 'Address', 
@sortOrder varchar (5) = 'DESC',
@totalCount int = 0 output	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --Completed Status
        @completedStatus varchar(100),
        
        --variables for paging
        @offset int,
		@limit int
		
				--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SET @completedStatus = '''COMPLETED'''
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		SET @searchCriteria = '1 = 1 AND PLANNED_STATUS.TITLE= '+ @completedStatus
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PLANNED_JOURNAL.JOURNALID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PA.APPOINTMENTID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E.FIRSTNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E.LASTNAME LIKE ''%' + @searchText + '%'') '
		END	
		
		
		IF @schemeId > -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P__PROPERTY.SCHEMEID = '+ CONVERT(VARCHAR(10),@schemeId)
		END
		
		IF @plannedType > -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PLANNED_JOURNAL.APPOINTMENTTYPEID = '+ CONVERT(VARCHAR(10),@plannedType)
		END

		-- End building SearchCriteria clause   
		--========================================================================================
		
				
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here		
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') 
		PLANNED_JOURNAL.JOURNALID as PMO
		,APPOINTMENT_AID.JSN as JSN 
		,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address	
        ,ISNULL(PAT.Planned_Appointment_Type,''N/A'') as [Type]
        ,ISNULL(PLOCATION.LocationName,''N/A'') AS LOCATION
        ,ISNULL(PV.ValueDetail,''N/A'') AS [DESCRIPTION]    
		,coalesce(pmt.TRADES,ComponentStat.TRADES,''N/A'') as Trades
		,PA.APPOINTMENTDATE AS APPOINTMENT
		,CONVERT(VARCHAR(10),PA.APPOINTMENTENDDATE,103)+''  ''+SUBSTRING(ISNULL(E.FIRSTNAME,''''),1,1)+'' ''+ ISNULL(E.LASTNAME,'''') AS COMPLETEDBY
		,P__PROPERTY.PROPERTYID,ISNULL(PLANNED_JOURNAL.COMPONENTID,''0'') AS COMPONENTID,PA.APPOINTMENTID
		'
				
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) + '	FROM 
		PLANNED_APPOINTMENTS PA
		INNER JOIN PLANNED_JOURNAL	 ON PA.JournalId = PLANNED_JOURNAL.JOURNALID
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PA.ASSIGNEDTO 
		INNER JOIN P_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
		LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
        LEFT JOIN PLANNED_APPOINTMENT_TYPE PAT ON PLANNED_JOURNAL.APPOINTMENTTYPEID = PAT.Planned_Appointment_TypeId     
		LEFT JOIN PLANNED_COMPONENT_ITEM PCI ON PLANNED_COMPONENT.COMPONENTID = PCI.COMPONENTID
		INNER JOIN (SELECT PLANNED_APPOINTMENTS.APPOINTMENTID as AID , ''JSN'' +RIGHT(''0000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),4) as JSN    
		FROM PLANNED_APPOINTMENTS ) as APPOINTMENT_AID ON PA.APPOINTMENTID = APPOINTMENT_AID.AID 
		LEFT JOIN PA_ITEM PITEM ON PCI.ITEMID = PITEM.ItemID
		LEFT JOIN PA_AREA PAREA ON PITEM.AreaID = PAREA.AreaID
		LEFT JOIN PA_LOCATION PLOCATION ON PAREA.LocationId = PLOCATION.LocationID 
		LEFT JOIN PLANNED_MISC_TRADE PM ON PLANNED_JOURNAL.JOURNALID = PM.JournalId
		LEFT JOIN PA_PARAMETER_VALUE PV ON PM.ParameterValueId = PV.ValueID
		LEFT JOIN 
		(SELECT	PLANNED_COMPONENT.COMPONENTID AS COMPONENTID , TRADES  = 
		STUFF((SELECT '', '' + G_TRADE.Description 
           FROM PLANNED_COMPONENT_TRADE COMP_TRADE_B 
           INNER JOIN G_TRADE ON COMP_TRADE_B.TRADEID = G_TRADE.TradeId 
           WHERE COMP_TRADE_B.COMPONENTID  = COMP_TRADE_A.COMPONENTID
           ORDER BY COMP_TRADE_B.SORDER ASC
          FOR XML PATH('''')), 1, 2, '''')
          ,SUM (DURATION ) AS DURATION
			FROM	PLANNED_COMPONENT 
		LEFT JOIN PLANNED_COMPONENT_TRADE AS COMP_TRADE_A ON PLANNED_COMPONENT.COMPONENTID = COMP_TRADE_A.COMPONENTID 
		GROUP BY PLANNED_COMPONENT.COMPONENTID,COMP_TRADE_A.COMPONENTID)
		 as ComponentStat ON PLANNED_JOURNAL.COMPONENTID = ComponentStat.COMPONENTID		
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
		Outer Apply (Select SUM(Duration) as Duration, TRADES  = 
		STUFF((SELECT '', '' + G_TRADE.Description 
           FROM PLANNED_MISC_TRADE INNER JOIN G_TRADE ON G_TRADE.TradeId  = PLANNED_MISC_TRADE.TradeId
           WHERE PLANNED_MISC_TRADE.JOURNALID = pmt.JOURNALID
          FOR XML PATH('''')), 1, 2, '''') from PLANNED_MISC_TRADE AS pmt  Where pmt.JOURNALID = PLANNED_JOURNAL.JOURNALID group by pmt.JOURNALID) pmt '
      		-- End building From clause
		--======================================================================================== 														  
		
				
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address ' 	
			
		END
		
		IF(@sortColumn = 'PMO')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'PMO' 		
		END
		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'JSN' 		
		END
		IF(@sortColumn = 'TYPE')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TYPE' 		
		END		
		IF(@sortColumn = 'LOCATION')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'LOCATION' 		
		END
		IF(@sortColumn = 'DESCRIPTION')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'DESCRIPTION' 		
		END
		IF(@sortColumn = 'TRADE')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TRADE' 		
		END
		IF(@sortColumn = 'APPOINTMENT')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'APPOINTMENT' 		
		END
		IF(@sortColumn = 'COMPLETEDBY')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'COMPLETEDBY' 		
		END
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	    
				
END
