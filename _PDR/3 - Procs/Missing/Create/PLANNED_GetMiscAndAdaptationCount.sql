/****** Object:  StoredProcedure [dbo].[PLANNED_GetMiscAndAdaptationCount]    Script Date: 02/27/2015 11:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetMiscAndAdaptationCount] 
/* ===========================================================================
--NAME:				PLANNED_GetMiscAndAdaptationCount
--EXEC				[dbo].[PLANNED_GetMiscAndAdaptationCount]	
-- Author:			<Muhammad Awais>
-- Create date:		<10/Mar/2015>
-- Description:		<Get Adaptation Not Scheduled Count>
-- Web Page:		PLANNED -> Dashboard.aspx
'==============================================================================*/
(
	@componentId int = -1,
	@adaptationStatus varchar(100) = 'To Be Arranged',
	@appointmentType varchar(100) = 'Adaptation'
)
AS
	DECLARE @SearchCriteria varchar(500) = ''

	IF @componentId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' WHERE J.ComponentId = ' + CONVERT(nvarchar(10), @componentId) 	 	
		END
	 
	SELECT COUNT(J.JOURNALID) AS TOTALCOUNT
	  FROM PLANNED_JOURNAL J  
			INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID AND S.TITLE = @adaptationStatus 
			INNER JOIN PLANNED_APPOINTMENT_TYPE PAT ON PAT.Planned_Appointment_TypeId = J.APPOINTMENTTYPEID AND PAT.Planned_Appointment_Type = @appointmentType
	+ @searchCriteria



