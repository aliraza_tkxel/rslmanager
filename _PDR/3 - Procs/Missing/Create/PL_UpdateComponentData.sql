USE [RSLBHALive1]
GO
/****** Object:  StoredProcedure [dbo].[PL_UpdateComponentData]    Script Date: 03/11/2014 17:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PL_UpdateComponentData 
--@componentId = 1,
--@frequency = 1,
--@cycle = 10,
--@labourCost = 10.5,
--@materialCost = 100.5
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/03/2014>
-- Description:	<Description,,Update the parameters value>
-- WebPage: AnnualSpendReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PL_UpdateComponentData]
@componentId int,
@frequency varchar(50),
@cycle int,
@labourCost decimal,
@materialCost decimal,
@editedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update PLANNED_COMPONENT Set FREQUENCY = @frequency,CYCLE=@cycle,LABOURCOST=@labourCost
	,MATERIALCOST=@materialCost, EDITEDON = GETDATE(), EDITEDBY = @editedBy
	Where COMPONENTID = @componentId
END
