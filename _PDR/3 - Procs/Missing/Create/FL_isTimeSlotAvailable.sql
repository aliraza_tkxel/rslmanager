USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.FL_isTimeSlotAvailable
(
	@operativeId INT
	,@startDateTime DATETIME2
	,@endDateTime DATETIME2
	,@isTimeSlotAvailable BIT OUTPUT
)  
/* =================================================================================      
    Page Description: Scheduling Calender => To check on scheduling calendar for time slot availability.
	
	DECLARE @isTimeSlotAvailable BIT
	
	EXEC FL_isTimeSlotAvailable 
		@operativeId = 760
		, @startDateTime = '20150326 09:00'
		,@endDateTime = '20150326 10:00'
		,@isTimeSlotAvailable = @isTimeSlotAvailable OUTPUT
	PRINT '@isTimeSlotAvailable: ' + CONVERT(NVARCHAR,@isTimeSlotAvailable)
	   
    Author: Aamir Waheed  
    Creation Date: Mar-25-2015  
  
    Change History:  
  
    Version      Date           By                  Description  
    =======     ============    ========		===========================  
    v1.0         Mar-25-2015    Aamir Waheed	Created new  sp to check time slot existance.
  =================================================================================*/  

AS 
BEGIN
SET NOCOUNT ON;

SET @isTimeSlotAvailable = 1

--======================================================================================

-----------------------------------------------------------------------------------------------------------------------    
-- Check for Fault  appointment(s)
IF @isTimeSlotAvailable = 1 AND EXISTS
(
	SELECT
		'Fault Appointment' AS AppointmentType
	FROM
		FL_CO_Appointment A
			INNER JOIN FL_FAULT_APPOINTMENT FA ON A.AppointmentID = FA.AppointmentId
			INNER JOIN FL_FAULT_LOG FL ON FA.FaultLogId = FL.FaultLogId
			INNER JOIN FL_FAULT_STATUS FS ON FL.StatusID = FS.FaultStatusID
					AND
					(FS.Description <> 'Cancelled')
	WHERE
		((@startDateTime >= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + Time, 103)
		AND @startDateTime < CONVERT(DATETIME2, CONVERT(VARCHAR(10), COALESCE(A.AppointmentEndDate, A.AppointmentDate), 103) + ' ' + A.EndTime, 103))
		OR (@endDateTime > CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + Time, 103)
		AND @endDateTime <= CONVERT(DATETIME2, CONVERT(VARCHAR(10), COALESCE(A.AppointmentEndDate, A.AppointmentDate), 103) + ' ' + A.EndTime, 103))
		OR (@startDateTime <= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + Time, 103)
		AND @endDateTime >= CONVERT(DATETIME2, CONVERT(VARCHAR(10), COALESCE(A.AppointmentEndDate, A.AppointmentDate), 103) + ' ' + A.EndTime, 103))
		)
		AND A.OperativeID = @operativeId
)
BEGIN
	SET @isTimeSlotAvailable = 0
	PRINT 'Fault Appointment Exists'
END
-----------------------------------------------------------------------------------------------------------------------    
-- Check for Gas appointment(s)
IF @isTimeSlotAvailable = 1 AND EXISTS
(
	SELECT
		'Gas Appointment' AS AppointmentType
	FROM
		AS_APPOINTMENTS A
			INNER JOIN AS_JOURNAL J ON A.JournalId = J.JOURNALID
			INNER JOIN AS_Status S ON J.StatusId = S.StatusId
	WHERE
		((@startDateTime >= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTSTARTTIME, 103)
		AND @startDateTime < CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTENDTIME, 103))
		OR (@endDateTime > CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTSTARTTIME, 103)
		AND @endDateTime <= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTENDTIME, 103))
		OR (@startDateTime <= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTSTARTTIME, 103)
		AND @endDateTime >= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTENDTIME, 103))
		)
		AND (S.Title <> 'Cancelled')
		AND A.ASSIGNEDTO = @operativeId
)
BEGIN
	SET @isTimeSlotAvailable = 0
	PRINT 'Gas Appointment Exists'
END
-----------------------------------------------------------------------------------------------------------------------    
-- Check for Planned appointment(s)
IF @isTimeSlotAvailable = 1 AND EXISTS
(
	SELECT
		'Planned Appointment' AS AppointmentType
	FROM
		PLANNED_APPOINTMENTS A
			INNER JOIN PLANNED_JOURNAL J ON J.JOURNALID = A.JournalId
			INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID
	WHERE
		(
		(@startDateTime >= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.APPOINTMENTDATE, 103) + ' ' + A.APPOINTMENTSTARTTIME, 103)
		AND @startDateTime < CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.APPOINTMENTENDDATE, 103) + ' ' + A.APPOINTMENTENDTIME, 103))
		OR (@endDateTime > CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTSTARTTIME, 103)
		AND @endDateTime <= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.APPOINTMENTENDDATE, 103) + ' ' + A.APPOINTMENTENDTIME, 103))
		OR (@startDateTime <= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.AppointmentDate, 103) + ' ' + A.APPOINTMENTSTARTTIME, 103)
		AND @endDateTime >= CONVERT(DATETIME2, CONVERT(VARCHAR(10), A.APPOINTMENTENDDATE, 103) + ' ' + A.APPOINTMENTENDTIME, 103))
		)
		AND (S.TITLE <> 'Cancelled'
		AND APPOINTMENTSTATUS <> 'Cancelled')
		AND A.ASSIGNEDTO = @operativeId

) 
BEGIN
	SET @isTimeSlotAvailable = 0
	PRINT 'Planned Appointment Exists'
END

IF @isTimeSlotAvailable = 1 AND 
-- Check for Employee Leave Record
EXISTS
(
	SELECT 'Operative Leave' AS LeaveType
	FROM (
	SELECT
		CASE
			WHEN
				(A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.STARTDATE)))
				THEN A.STARTDATE
			WHEN HolType = ''
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103)
			WHEN HolType = 'M'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103)
			WHEN HolType = 'A'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103)
			WHEN HolType = 'F'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103), 103)
			WHEN HolType = 'F-F'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103), 103)
			WHEN HolType = 'F-M'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103), 103)
			WHEN HolType = 'A-F'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103)
		END	AS StartDateTime
		,CASE
			WHEN
				(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE)))
				THEN A.RETURNDATE
			WHEN HolType = ''
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00', 103)
			WHEN HolType = 'M'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + '12:00 PM', 103)
			WHEN HolType = 'A'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + '05:00 PM', 103)
			WHEN HolType = 'F'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + '11:59 PM', 103)
			WHEN HolType = 'F-F'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + '11:59 PM', 103)
			WHEN HolType = 'F-M'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + '12:00 PM', 103)
			WHEN HolType = 'A-F'
				THEN CONVERT(DATETIME2, CONVERT(VARCHAR(10), ISNULL(RETURNDATE,StartDate), 103) + ' ' + '11:59 PM', 103)
		END	AS EndDateTime
	FROM
		E_JOURNAL J
			INNER JOIN E_ABSENCE A ON J.JOURNALID = A.JOURNALID
			CROSS APPLY
				(
					SELECT
						MAX(ABSENCEHISTORYID) MAXABSENCEHISTORYID
					FROM
						E_ABSENCE
					WHERE
						JOURNALID = J.JOURNALID
				) MaxAbsenceHistory
	WHERE
		A.ABSENCEHISTORYID = MaxAbsenceHistory.MAXABSENCEHISTORYID
		AND (
		-- To filter for planned i.e annual leaves etc. where approval is needed  
				(A.ITEMSTATUSID = 5
					AND itemnatureid IN (2, 3, 4, 5, 6, 8, 9, 10, 11, 13, 14, 15, 16, 32, 43, 47, 48, 51) )
			
		-- To filter for sickness leaves. where the operative is now returned to work.  
			OR
				(ITEMNATUREID = 1
				AND A.ITEMSTATUSID = 2
				AND A.RETURNDATE IS NOT NULL)
		)
		AND (
			CONVERT(DATE,@startDateTime) = CONVERT(DATE, StartDate)
			OR CONVERT(DATE,@endDateTime) = CONVERT(DATE, StartDate)
			OR CONVERT(DATE,@startDateTime) = CONVERT(DATE, ISNULL(RETURNDATE,StartDate))
			OR CONVERT(DATE,@endDateTime) = CONVERT(DATE, ISNULL(RETURNDATE,StartDate))
		)		
		AND J.employeeid = @operativeId
		) OperativeLeaves 
	WHERE
		(@startDateTime >=StartDateTime AND @startDateTime < EndDateTime)
		 OR (@endDateTime > StartDateTime AND @endDateTime <= EndDateTime)
		OR (@startDateTime <= StartDateTime AND @endDateTime >= EndDateTime)
)
BEGIN
	SET @isTimeSlotAvailable = 0
END

IF @isTimeSlotAvailable = 1 AND 
-- Check for Bank holidays
EXISTS
(
	SELECT
		'Bank Holiday' AS LeaveType
	FROM
		G_BANKHOLIDAYS bh
	WHERE
		bh.BHDATE BETWEEN @startDateTime AND @endDateTime
		AND bh.BHA = 1
)
BEGIN
	SET @isTimeSlotAvailable = 0
END

PRINT 'IsAppointmentSlot Available:' + CONVERT(NVARCHAR,@isTimeSlotAvailable)

SELECT @isTimeSlotAvailable AS isTimeSlotAvailable

RETURN @isTimeSlotAvailable
END