/* =================================================================================    
    Page Description:    Get the Replacement due date for assign to contractor
 
    Author: Ali Raza
    Creation Date: Feb-18-2015  

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        Feb-18-2015     Ali Raza           Get the Replacement due date for assign to contractor
    
    Execution Command:
    
    Exec FL_GetReplacementDueForAssignToContractor
    @itemId=28,
    @schemeId = 1
    @blockId = null
  =================================================================================*/
CREATE PROCEDURE [dbo].[FL_GetReplacementDueForAssignToContractor]          
 @itemId int,  
 @schemeId int=null, 
 @blockId int=null 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF @blockId <= 0 
BEGIN
SET @blockId = NULL
END
    -- Insert statements for procedure here
SELECT [SID],ItemId,convert(varchar(20),DueDate,103)AS DueDate  from PA_PROPERTY_ITEM_DATES
WHERE (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL) AND ItemId =@itemId  
END
GO
