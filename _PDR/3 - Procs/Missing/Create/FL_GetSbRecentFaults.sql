-- Stored Procedure

-- =============================================  
--EXEC [dbo].[FL_GetSbRecentFaults]  
--@schemeId = 1,  
--@blockId = 0,  
--@searchText = N'The',  
--@jsn = N''  

-- Author:  <Noor Muhammad>  
-- Create date: <19 Jan,2015>  
-- Description: <This Procedure 'll be used to fetch the recent faults>  
-- Web Page: SearchFault.aspx  
-- =============================================  
CREATE PROCEDURE [dbo].[FL_GetSbRecentFaults](  
 @schemeId int,  
 @blockId int,  
 @searchText varchar(500),   
 @jsn varchar(50)  

)  

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  


  IF (@schemeId != 0) and (@searchText != '' OR @searchText != NULL)  
	  BEGIN    
		set @searchText = REPLACE(@searchText,'''','''''')
		 EXEC	[dbo].[FL_GetRecentFaultForSearch] @searchText                               
	  END     
  ELSE IF (@schemeId != 0 AND @blockId = 0) AND (@Jsn != '' OR @Jsn != NULL)  
	  BEGIN  

		SELECT   
		 FL_FAULT_LOG.FaultLogId  
		,FL_FAULT_LOG.SchemeId  
		,FL_FAULT_LOG.FaultId  
		,FL_AREA.AreaName  
		,FL_AREA.AreaName + ' : '+ FL_FAULT.Description as Description,  
		FL_FAULT.Description as DescriptionOnly  

		FROM FL_FAULT_LOG   
		INNER JOIN FL_FAULT ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID  
		INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  

		Where FL_FAULT_LOG.JobSheetNumber = @Jsn AND  
		FL_FAULT_LOG.schemeId = @schemeId
		AND ISNULL(FL_FAULT.FAULTACTIVE,0) = 1  
		Order by FL_FAULT_LOG.FaultID DESC  
	  END    
  ELSE IF (@schemeId != 0 AND @blockId != 0) AND (@Jsn != '' OR @Jsn != NULL)  
	  BEGIN  

		SELECT   
		 FL_FAULT_LOG.FaultLogId  
		,FL_FAULT_LOG.SchemeId  
		,FL_FAULT_LOG.FaultId  
		,FL_AREA.AreaName  
		,FL_AREA.AreaName + ' : '+ FL_FAULT.Description as Description,  
		FL_FAULT.Description as DescriptionOnly  

		FROM FL_FAULT_LOG   
		INNER JOIN FL_FAULT ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID  
		INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  

		Where FL_FAULT_LOG.JobSheetNumber = @Jsn AND  
		FL_FAULT_LOG.schemeId = @schemeId  AND  
		FL_FAULT_LOG.BlockId = @blockId  
		AND ISNULL(FL_FAULT.FAULTACTIVE,0) = 1  
		Order by FL_FAULT_LOG.FaultID DESC  
	  END
  ELSE   
	BEGIN               
		--Select top 10 recent unique faults  

		CREATE TABLE #RecentFaults(  
		FaultLogId int,  
		FaultId int    
		)  

		INSERT INTO #RecentFaults(FaultLogId,FaultId) SELECT top 10 MAX(FaultLogId) as FaultLogId, FaultId FROM FL_FAULT_LOG GROUP BY FaultId ORDER BY FaultLogId DESC      

		SELECT FL_FAULT_LOG.FaultLogId, FL_FAULT.FaultId  
		,'' As CustomerId  
		, FL_FAULT.FaultId As FaultId  
		, FL_AREA.AreaName  
		, FL_AREA.AreaName + ' : '+FL_FAULT.Description as Description  
		FROM FL_FAULT_LOG   
		INNER JOIN FL_FAULT ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID  
		INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  
		WHERE FL_Fault_Log.FaultLogId IN (Select FaultLogId FROM #RecentFaults)      
		Order by FL_FAULT_LOG.FaultLogId desc                 
	END  

	IF (@schemeId != 0 AND @blockId =0)  
		BEGIN  
			-------------------------------------  
			---Get the Scheme Name 
			SELECT  P_SCHEME.SCHEMENAME as SchemeName
			,P_SCHEME.SCHEMEID AS SchemeId
			,'Non Selected' as BlockName
			,0 as BlockId						
			,Case 
				When @blockId > 0 Then			
					ISNULL(P_BLOCK.ADDRESS1,'-') +' '+ ISNULL(P_BLOCK.ADDRESS2,'') 
				ELSE
					ISNULL(P_SCHEME.SCHEMENAME,'-')  
			 END			 
			  AS Address     
		    ,ISNULL(PDR_DEVELOPMENT.TOWN,'-') as TownCity      
		    ,ISNULL(PDR_DEVELOPMENT.COUNTY,'-') as County      
		    ,ISNULL(PDR_DEVELOPMENT.PostCode,'-') as PostCode      
		    ,E_PATCH.PatchId as PatchId  
		    ,E_PATCH.Location as PatchName  
			FROM P_SCHEME LEFT JOIN P_BLOCK ON P_SCHEME.SCHEMEID = P_BLOCK.SCHEMEID   
			INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID			
			LEFT JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
			WHERE P_SCHEME.SCHEMEID= @schemeId          

		END

	IF (@schemeId != 0 AND @blockId !=0)  
		BEGIN  
			-------------------------------------  
			---Get the Scheme Name & Block Name
			SELECT  P_SCHEME.SCHEMENAME as SchemeName
			,P_SCHEME.SCHEMEID AS SchemeId
			,P_BLOCK.BLOCKNAME as BlockName
			,P_BLOCK.BLOCKID as BlockId
			,Case 
				When @blockId > 0 Then			
					ISNULL(P_BLOCK.ADDRESS1,'-') +' '+ ISNULL(P_BLOCK.ADDRESS2,'') 
				ELSE
					ISNULL(P_SCHEME.SCHEMENAME,'-')  
			 END			 
			  AS Address      
		    ,ISNULL(P_BLOCK.TOWNCITY,'-') as TownCity      
		    ,ISNULL(P_BLOCK.COUNTY,'-') as County      
		    ,ISNULL(P_BLOCK.POSTCODE,'-') as PostCode      
		    ,E_PATCH.PatchId as PatchId  
		    ,E_PATCH.Location as PatchName  
			FROM P_SCHEME LEFT JOIN P_BLOCK ON P_SCHEME.SCHEMEID = P_BLOCK.SCHEMEID   
			INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID			
			LEFT JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
			WHERE P_SCHEME.SCHEMEID= @schemeId          
			AND P_BLOCK.BLOCKID =@blockId 
		END          

END  
GO