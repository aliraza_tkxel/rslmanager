USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetRecentFaults]    Script Date: 08/30/2013 16:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
--EXEC [dbo].[FL_GetRecentFaults]  
--@propertyId = N'A570010000',  
--@customerId = 10074,  
--@searchText = N'The',  
--@jsn = N''  
  
-- Author:  <Noor Muhammad>  
-- Create date: <10 Jan,2013>  
-- Description: <This Procedure 'll be used to fetch the recent faults>  
-- Web Page: SearchFault.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[FL_GetRecentFaults](  
 @propertyId varchar(20),  
 @customerId int,  
 @searchText varchar(500),  
 --Added by Behroz - 20/02/2013 - Start  
 @jsn varchar(50)  
 --Added by Behroz - 20/02/2013 - End  
)  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     
   
 IF (@propertyId != '' and @customerId != 0) and (@searchText != '' OR @searchText != NULL)  
  BEGIN    
     EXEC	[dbo].[FL_GetRecentFaultForSearch] @searchText
   --Select top 10 recent unique faults  
    --SELECT        
    -- FL_FAULT.FaultId as FaultId  
    -- ,FL_AREA.AreaName as AreaName  
    -- ,FL_AREA.AreaName + ' : '+FL_FAULT.Description as Description    
    -- FROM FL_FAULT INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  
    -- Where FL_FAULT.FaultId in (      
    --        SELECT top(10)   
    --        FL_FAULT.FaultId as FaultId              
    --        FROM FL_FAULT   
    --        INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  
                 
    --        WHERE   
    --       -- FL_FAULT.Description like '%'+@searchText+'%'  
    --       FREETEXT(FL_FAULT.Description, @searchText)
    --        AND ISNULL(FL_FAULT.FAULTACTIVE,0) = 1  
    --        Group By FaultId              
    --         )       
    -- Order by Description ASC                               
  END   
  --Added by Behroz - 20/02/2013 - Start  
  ELSE IF (@propertyId != '' AND @customerId != 0) AND (@Jsn != '' OR @Jsn != NULL)  
  BEGIN  
     
   SELECT   
     FL_FAULT_LOG.FaultLogId  
    ,FL_FAULT_LOG.CustomerId  
    ,FL_FAULT_LOG.FaultId  
    ,FL_AREA.AreaName  
    ,FL_AREA.AreaName + ' : '+ FL_FAULT.Description as Description,  
    FL_FAULT.Description as DescriptionOnly  
        
    FROM FL_FAULT_LOG   
    INNER JOIN FL_FAULT ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID  
    INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  
      
    Where FL_FAULT_LOG.JobSheetNumber = @Jsn AND  
    FL_FAULT_LOG.PROPERTYID = @propertyId  AND  
    FL_FAULT_LOG.CustomerID = @customerId  
    AND ISNULL(FL_FAULT.FAULTACTIVE,0) = 1  
    Order by FL_FAULT_LOG.FaultID DESC  
  END  
  --End by Behroz - 20/02/2013 - End   
  ELSE   
   BEGIN               
    --Select top 10 recent unique faults  
          
    CREATE TABLE #RecentFaults(  
     FaultLogId int,  
     FaultId int    
    )  
   
    INSERT INTO #RecentFaults(FaultLogId,FaultId) SELECT top 10 MAX(FaultLogId) as FaultLogId, FaultId FROM FL_FAULT_LOG GROUP BY FaultId ORDER BY FaultLogId DESC      
  
    SELECT FL_FAULT_LOG.FaultLogId, FL_FAULT.FaultId  
    ,'' As CustomerId  
    , FL_FAULT.FaultId As FaultId  
    , FL_AREA.AreaName  
    , FL_AREA.AreaName + ' : '+FL_FAULT.Description as Description  
    FROM FL_FAULT_LOG   
    INNER JOIN FL_FAULT ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID  
    INNER JOIN FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID  
    WHERE FL_Fault_Log.FaultLogId IN (Select FaultLogId FROM #RecentFaults)      
    Order by FL_FAULT_LOG.FaultLogId desc  
               
   END  
      
 IF (@propertyId != '' and @customerId != 0)  
  BEGIN  
   -------------------------------------  
   ---Get the Customer Telephone Number  
   SELECT  C_ADDRESS.Tel as Telephone  
   ,C_ADDRESS.MOBILE as Mobile     
   ,C_ADDRESS.Email as Email   
   ,ISNULL(C__CUSTOMER.FirstName,'') +' '+ ISNULL(C__CUSTOMER.MiddleName,'') +' '+ ISNULL(C__CUSTOMER.LastName,'') as Name  
     
   FROM C_Address   
   INNER JOIN C__CUSTOMER ON C__CUSTOMER.CustomerId = C_Address.CustomerId  
   WHERE C_ADDRESS.IsDefault = 1  
   AND C__CUSTOMER.CustomerID= @customerId  
     
   --------------------------  
   --Get the Property Address  
   SELECT ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address     
   ,P__PROPERTY.TownCity as TownCity      
   ,P__PROPERTY.County as County      
   ,P__PROPERTY.PostCode as PostCode      
   ,E_PATCH.PatchId as PatchId  
   ,E_PATCH.Location as PatchName  
   FROM P__PROPERTY      
   LEFT JOIN P_SCHEME ON  P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
   INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
   INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID 
   WHERE PROPERTYID = @propertyId        
  END     
   
END  