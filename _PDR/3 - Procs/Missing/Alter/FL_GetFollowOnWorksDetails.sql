-- Stored Procedure

-- =============================================  
-- EXEC FL_GetFollowOnWorksDetails  
-- @jobSheetNumber ='JS5'  
-- Author:  <Ahmed Mehmood>  
-- Create date: <15/2/2013>  
-- Description: <Returns Follow On Works Details>  
-- Webpage:ReportsArea.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[FL_GetFollowOnWorksDetails]   
 -- Add the parameters for the stored procedure here  
 (  
 @jobSheetNumber varchar(50)  
 )  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  

 SET NOCOUNT ON;  
  Declare @schemeId INT = 0,@blockId INT=0 
	SET NOCOUNT ON;
Select @schemeId = SchemeId,@blockId= BlockId from FL_FAULT_LOG WHERE	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber   
	if @schemeId > 0 OR @blockId > 0 
	BEGIN 
	-- Job Sheet Detail Customer Information
	EXEC FL_GetSbJobSheetDetail @jobSheetNumber

	END
	ELSE
	BEGIN 
 -- Job Sheet Detail Customer Information  
 EXEC FL_GetJobSheetAndCustomerDetail @jobSheetNumber  

	END



 -- Repair Detail     
 Select convert(date, FL_CO_FAULTLOG_TO_REPAIR.InspectionDate,103) as CompletionDate,  
   Ltrim(Rtrim(FL_FAULT_REPAIR_LIST.Description)) as Description         
 FROM FL_CO_FAULTLOG_TO_REPAIR   
   INNER JOIN FL_FAULT_REPAIR_LIST on FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID=FL_FAULT_REPAIR_LIST.FaultRepairListID  
   INNER JOIN FL_FAULT_LOG ON FL_FAULT_LOG.FaultLogId = FL_CO_FAULTLOG_TO_REPAIR.FaultLogId  
   --INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FAULTLOGID = FL_FAULT_APPOINTMENT.FAULTLOGID   
   --INNER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.APPOINTMENTID = FL_CO_APPOINTMENT.APPOINTMENTID     
 Where FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber  

 -- Follow on Work List  
 SELECT FL_FAULT_FOLLOWON.FollowOnNotes  
 FROM FL_FAULT_LOG   
  INNER JOIN FL_FAULT_FOLLOWON on FL_FAULT_LOG.FaultLogID = FL_FAULT_FOLLOWON.FaultLogId  
 WHERE FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber  

END  
GO