-- Stored Procedure

-- =============================================
--EXEC	[PLANNED_GetSchemesAgainstReplacementYearAndComponent]
--		@replacementYear = -1,
--		@componentId = -1
-- Author:		<Noor Muhammad>
-- Create date: <13/1/2014>
-- Description:	<Returns Schemes Against Selected Year And Component>
-- Webpage: ReplacementList.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetSchemesAgainstReplacementYearAndComponent] 

	@replacementYear int = -1,
	@componentId int = -1
AS
BEGIN

	DECLARE
	@selectClause varchar(8000),
	@fromClause   varchar(8000),
	@whereClause  varchar(8000),	        
	@mainSelectQuery varchar(8000),        
	@searchCriteria varchar(8000)= ''	        

    IF NOT (@componentId = -1 OR @componentId = 0)

		BEGIN

	       SET @searchCriteria = @searchCriteria + CHAR(10) +'AND PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId)  
		END
	IF NOT (@replacementYear = -1 OR @replacementYear = 0) 			       
		BEGIN
		 SET @searchCriteria = @searchCriteria + CHAR(10) +'AND YEAR(DueDate) = '+ CONVERT(VARCHAR(10),@replacementYear)  					
		END


	SET @selectClause = 'SELECT 
	Distinct P_SCHEME.SCHEMEID as DevelopmentId
	,P_SCHEME.SCHEMENAME as SchemeName'                                            		

	SET @fromClause = CHAR(10) + 'FROM 
	PA_PROPERTY_ITEM_DATES
	Inner JOIN P__PROPERTY ON PA_PROPERTY_ITEM_DATES.PROPERTYID = P__PROPERTY.PROPERTYID 
	INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
	INNER JOIN PLANNED_COMPONENT ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PLANNED_COMPONENT.COMPONENTID'

    SET @whereClause =  CHAR(10)+ 'WHERE 1=1 ' + CHAR(10) + @searchCriteria 

	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + CHAR(10)+ 'ORDER BY SchemeName ASC'
	print @selectClause
	print @fromClause
	print @whereClause	
	EXEC (@mainSelectQuery)

--SELECT Distinct P_DEVELOPMENT.DEVELOPMENTID as DevelopmentId, P_DEVELOPMENT.SCHEMENAME as SchemeName, COMPONENTID				
--FROM PA_PROPERTY_ITEM_DATES
--Inner JOIN P__PROPERTY ON PA_PROPERTY_ITEM_DATES.PROPERTYID = P__PROPERTY.PROPERTYID 
--INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID 
--INNER JOIN PLANNED_COMPONENT ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PLANNED_COMPONENT.COMPONENTID 

--WHERE  
--YEAR(DueDate)=2025 
--AND COMPONENTID = 1
--AND 1=1 
--ORDER BY SchemeName ASC

END

GO