
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_getFirstDetectionList]
			@postCode varchar(15),
			@caseOwnedById int=0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@surname		varchar(100),
			@skipIndex	int = 0,
			@pageSize	int = 10,
            @sortBy     varchar(100),
            @sortDirection varchar(10),
            @address1 varchar(200),
            @tenancyid VARCHAR(15)
			
AS
BEGIN
--
declare @orderbyClause varchar(50)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @RegionSuburbClause varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)+ ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
END



SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
SET @query = 
'SELECT TOP ('+convert(varchar(10),@pageSize)+' )
				  AM_FirstDetecionList.TENANCYID, 
				  Max(AM_FirstDetecionList.CUSTOMERID) AS CUSTOMERID,
				  Max(ISNULL(Convert(varchar(100),(customer.LastPaymentDate), 103), '''')) AS TRANSACTIONDATE, 
				  Max(ISNULL((customer.LastPayment), 0.0)) AS LastCPAY, 
				  Max(customer.CustomerAddress) AS CustomerAddress, 
				  --Max(ISNULL(Convert(varchar(100),AM_FirstDetecionList.FirstDetectionDate, 103), '''')) AS FirstDetectionDate, 
				  Max(AM_FirstDetecionList.FirstDetectionDate) AS FirstDetectionDate, 
                  Max(customer.RentBalance) AS RentBalance,
 				  Max(customer.EstimatedHBDue) AS EstimatedHBDue,
				  Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,

				  (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
						WHERE AM_Customer_Rent_Parameters.TenancyId = AM_FirstDetecionList.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

				  (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
						WHERE AM_Customer_Rent_Parameters.TenancyId = AM_FirstDetecionList.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

				  (SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
						WHERE	AM_Customer_Rent_Parameters.TenancyId = AM_FirstDetecionList.TENANCYID 
						AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())) as JointTenancyCount,
				   
				  (SELECT ISNULL(P_FINANCIAL.Totalrent , 0)
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID 
													 INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
													 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P_SCHEME.DEVELOPMENTID	
													 INNER JOIN P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
													 WHERE C_TENANCY.TENANCYID = AM_FirstDetecionList.TENANCYID) AS TotalRent
					
					--ISNULL((ISNULL(RentBalance, 0) - ISNULL(null, 0)), 0) AS OwedToBHA
	
	FROM         AM_FirstDetecionList 
				  --INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
                  INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.TENANCYID = customer.TENANCYID
                  INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
				  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID	
				  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID		  
				 -- INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
                  INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
	
	WHERE '+ @RegionSuburbClause +' AND dbo.AM_FN_CHECK_OWED_TO_BHA(ISNULL(customer.RentBalance, 0.0),ISNULL(customer.EstimatedHBDue, 0.0))=''true''
             AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId 
														FROM AM_Case 
														WHERE AM_Case.IsActive= ''true'' ) 		   
		   AND AM_FirstDetecionList.IsDefaulter = ''true''
		   AND customer.LASTNAME LIKE '''' + CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN customer.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END + ''%''
           --AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
           AND P__PROPERTY.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN P__PROPERTY.POSTCODE ELSE '''+@postCode+''' END 
           AND (
				  (P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')  
				  --OR 
				  --(C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.HOUSENUMBER + '' '' + C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')
				)
		   AND customer.TenancyId='''' + CASE WHEN '''' = '''+ REPLACE(@tenancyid,'''','''''') +''' THEN customer.TenancyId ELSE '''+ REPLACE(@tenancyid,'''','''''') +''' END + ''''
		   AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE>GETDATE())
			and AM_FirstDetecionList.TENANCYID NOT IN ('
		SET @subQuery ='SELECT TENANCYID FROM (
						SELECT TOP ('+convert(varchar(10),@skipIndex)+')								
								  AM_FirstDetecionList.TENANCYID, 
								  Max(AM_FirstDetecionList.CUSTOMERID) AS CUSTOMERID,
								  Max(ISNULL(Convert(varchar(100),(customer.LastPaymentDate), 103), '''')) AS TRANSACTIONDATE, 
								  Max(ISNULL((customer.LastPayment), 0.0)) AS LastCPAY, 
								  Max(customer.CustomerAddress) AS CustomerAddress, 
								  --Max(ISNULL(Convert(varchar(100),AM_FirstDetecionList.FirstDetectionDate, 103), ''''))  AS FirstDetectionDate, 
								  Max(AM_FirstDetecionList.FirstDetectionDate) AS FirstDetectionDate, 
								  Max(customer.RentBalance) AS RentBalance,
 								  Max(customer.EstimatedHBDue) AS EstimatedHBDue,
								  --Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
                                  Max(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0)) as OwedToBHA,

								  (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
										FROM AM_Customer_Rent_Parameters
										WHERE TenancyId = AM_FirstDetecionList.TENANCYID
										ORDER BY CustomerId ASC) as CustomerName,

								  (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
										FROM AM_Customer_Rent_Parameters
										WHERE TenancyId = AM_FirstDetecionList.TENANCYID
										ORDER BY CustomerId DESC) as CustomerName2,

								  (SELECT Count(DISTINCT CustomerId)
										FROM AM_Customer_Rent_Parameters
										WHERE	TenancyId = AM_FirstDetecionList.TENANCYID) as JointTenancyCount,
								   
									(SELECT ISNULL(P_FINANCIAL.Totalrent , 0)
																	 FROM C_TENANCY INNER JOIN
																	 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID 
																	 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
																	 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
																	 INNER JOIN P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
																	 WHERE C_TENANCY.TENANCYID = AM_FirstDetecionList.TENANCYID) AS TotalRent
									
									--ISNULL((ISNULL(RentBalance, 0) - ISNULL(null, 0)), 0) AS OwedToBHA
					
					FROM         AM_FirstDetecionList 
				--INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
                INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.TENANCYID = customer.TENANCYID
                  INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
				  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
				  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID			  
				--  INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
                  INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID

						WHERE '+@RegionSuburbClause+' AND dbo.AM_FN_CHECK_OWED_TO_BHA(ISNULL(customer.RentBalance, 0.0),ISNULL(customer.EstimatedHBDue, 0.0))=''true''
							  AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId 
																		FROM AM_Case 
																		WHERE AM_Case.IsActive= ''true'' ) 		   
						   AND AM_FirstDetecionList.IsDefaulter = ''true''
						  AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE>GETDATE())
						   AND customer.LASTNAME LIKE '''' + CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN customer.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END  + ''%''
                         --AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
						   AND P__PROPERTY.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN P__PROPERTY.POSTCODE ELSE '''+@postCode+''' END 
                           AND (
								  (P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')  
								  --OR 
								  --(C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.HOUSENUMBER + '' '' + C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')
								) 
						   AND customer.TenancyId='''' + CASE WHEN '''' = '''+ REPLACE(@tenancyid,'''','''''') +''' THEN customer.TenancyId ELSE '''+ REPLACE(@tenancyid,'''','''''') +''' END + ''''
							GROUP BY	AM_FirstDetecionList.TENANCYID '
							+ ' '  + @orderbyClause + ') as Temp)
	GROUP By	AM_FirstDetecionList.TENANCYID ' 




--SET @query = @query + ' '  + @orderbyClause

print(@query + @subQuery + @orderbyClause);

exec(@query + @subQuery + @orderbyClause);		   


END
GO
