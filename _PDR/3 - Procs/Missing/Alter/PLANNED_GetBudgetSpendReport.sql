SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--EXEC	@return_value = [dbo].[PLANNED_GetBudgetSpendReport]
--		@replacementYear = N'1934',
--		@schemeId = -1,
--		@searchText = '',
--		@isFullList = 0,
--		@pageSize = 20,
--		@pageNumber = 1,
--		@sortColumn = N'PC.SOrder',
--		@sortOrder = N'asc'		
-- Author:		<Ahmed Mehmood>
-- Create date: <28/11/2014>
-- Description:	<Get Budget Spend Report>
-- Web Page: BudgetSpendReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetBudgetSpendReport]
( 
	-- Add the parameters for the stored procedure here

		@replacementYear varchar(10),
		@schemeId int,
		@searchText varchar(100),
		@isFullList bit,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 20,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'SOrder',
		@sortOrder varchar (5) = 'ASC'
)
AS
BEGIN
	DECLARE
	 
		@SelectClause varchar(max),
        @fromClause   varchar(max),
        @whereClause  varchar(max),	        
        @orderClause  varchar(max),	
        @mainSelectQuery varchar(Max),        
        @rowNumberQuery varchar(max),
        @finalQuery varchar(Max)
        
        ,@forecastSelectClause varchar(max)
		,@forecastFromClause varchar(max)
		,@forecastMainQuery varchar(max)
		,@statusSelectClause varchar(max)
		,@statusFromClause varchar(max)
		,@statusMainQuery varchar(max)

		,@approvedMainQuery varchar(max)
		,@arrangedMainQuery varchar(max)
		,@completedMainQuery varchar(max)
        
        -- used to add in conditions in WhereClause based on search criteria provided
        ,@searchCriteria varchar(max)
        ,@forecastSearchCriteria varchar(max)
        ,@statusSearchCriteria varchar(max)
        
        --variables for paging
        ,@offset int
		,@limit int
		,@dataLimitation varchar(10)

		--Variable to filter report by fiscal year
		,@YearStartDate DateTime
		,@YearEndDate DateTime

		Select @YearStartDate = CONVERT(DATETIME, @replacementYear+'0401') ,
		       @YearEndDate = CONVERT(DATETIME, CONVERT(NVARCHAR,CONVERT(INT,@replacementYear)+1)+'0331 23:59:59')
		       
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PC.COMPONENTNAME LIKE ''%' + @searchText + '%'''
		END	

		--SET		@forecastSearchCriteria = 'WHERE YEAR(PID.DueDate) = ' + CONVERT(nvarchar(10), @replacementYear) + 
		SET @forecastSearchCriteria = ' WHERE Convert( DateTime, Convert( Char, PID.DueDate, 103 ), 103 ) BETWEEN  
						  Convert( DateTime, ''' + Ltrim( RTrim( CONVERT(Char, @YearStartDate, 103) ) ) + ''', 103 )  AND 
						  Convert( DateTime, ''' + Ltrim( RTrim( CONVERT(Char, @YearEndDate, 103) ) ) + ''', 103 ) 
                          AND PID.PLANNED_COMPONENTID = PC.COMPONENTID '
		
		IF @schemeId != -1 
		BEGIN
			SET @forecastSearchCriteria = @forecastSearchCriteria + ' AND P.SCHEMEID = ' + CONVERT(nvarchar(10), @schemeId) 		
		END
				
		--SET @statusSearchCriteria = ' WHERE YEAR(PJH.CREATIONDATE) = ' + CONVERT(nvarchar(10), @replacementYear) + 
		SET @statusSearchCriteria = ' WHERE Convert( DateTime, Convert( Char, PJH.CREATIONDATE, 103 ), 103 ) BETWEEN  
						  Convert( DateTime, ''' + Ltrim( RTrim( CONVERT(Char, @YearStartDate, 103) ) ) + ''', 103 )  AND 
						  Convert( DateTime, ''' + Ltrim( RTrim( CONVERT(Char, @YearEndDate, 103) ) ) + ''', 103 ) ' + 
		                  ' AND PJ.COMPONENTID = PC.COMPONENTID '
		
		
		IF @schemeId != -1 
		BEGIN
			SET @statusSearchCriteria = @statusSearchCriteria + ' AND P.DEVELOPMENTID = ' + CONVERT(nvarchar(10), @schemeId) 		
		END 	
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET		@forecastSelectClause = ' SELECT	COUNT(DISTINCT PID.PROPERTYID) '
		SET		@forecastFromClause = '	FROM		PA_PROPERTY_ITEM_DATES AS PID
											INNER JOIN P__PROPERTY AS P ON PID.PROPERTYID = P.PROPERTYID
											INNER JOIN PLANNED_COMPONENT PLC ON PID.PLANNED_COMPONENTID = PLC.COMPONENTID '
		SET		@forecastMainQuery = @forecastSelectClause + CHAR(10) + @forecastFromClause + CHAR(10) + @forecastSearchCriteria

		SET		@statusSelectClause = ' SELECT	COUNT(PJ.JOURNALID) '
		SET		@statusFromClause = '	FROM	PLANNED_JOURNAL AS PJ
												INNER JOIN PLANNED_STATUS AS PS ON PJ.STATUSID = PS.STATUSID
												INNER JOIN P__PROPERTY AS P ON PJ.PROPERTYID = P.PROPERTYID
												INNER JOIN (SELECT PLANNED_JOURNAL_HISTORY.JOURNALID,MAX(PLANNED_JOURNAL_HISTORY.CREATIONDATE ) CREATIONDATE
															FROM PLANNED_JOURNAL_HISTORY  
															GROUP BY PLANNED_JOURNAL_HISTORY.JOURNALID ) AS PJH  ON PJ.JOURNALID = PJH.JOURNALID   '
		SET		@statusMainQuery = @statusSelectClause + CHAR(10) + @statusFromClause + CHAR(10) + @statusSearchCriteria	

		SET		@approvedMainQuery = @statusMainQuery + ' AND (PS.TITLE = ''To be Arranged'' OR PS.TITLE = ''Arranged'' OR PS.TITLE = ''Completed'' )'
		SET		@arrangedMainQuery = @statusMainQuery + ' AND PS.TITLE = ''Arranged'' '
		SET		@completedMainQuery = @statusMainQuery + ' AND PS.TITLE = ''Completed'' '
				
		
		IF (@isFullList = 1) 
			SET @dataLimitation = ''
		ELSE
			SET @dataLimitation = 'top ('+convert(varchar(10),@limit)+')'
		
		SET @selectClause = 'SELECT '+@dataLimitation+'
		PC.COMPONENTID 
		,PC.COMPONENTNAME
		,PC.SOrder AS SOrder
		,('+@forecastMainQuery+') AS FORECAST_COUNT						
		,('+@forecastMainQuery+') * (PC.COST) AS FORECAST_COST
		,('+@approvedMainQuery+') APPROVED_COUNT
		,('+@approvedMainQuery+') * (PC.COST) AS APPROVED_COST
		,('+@arrangedMainQuery+') ARRANGED_COUNT
		,('+@arrangedMainQuery+') * (PC.COST) ARRANGED_COST
		,('+@completedMainQuery+') COMPLETED_COUNT
		,('+@completedMainQuery+') * (PC.COST) COMPLETED_COST
		,PC.ANNUALBUDGETCOST AS ANNUAL_BUDGET
		,PC.ANNUALBUDGETCOST - (('+@approvedMainQuery+')  * (PC.COST)) AS BUDGET_AVAILABLE	
		'
		
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'FROM	(SELECT	PLC.COMPONENTID,PLC.COMPONENTNAME ,ISNULL(PLC.ANNUALBUDGETCOST,0) ANNUALBUDGETCOST,PLC.LABOURCOST + PLC.MATERIALCOST AS COST,PLC.SOrder    
												FROM	PLANNED_COMPONENT PLC) AS PC '
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
				
	
		SET @sortColumn = CHAR(10)+ ' SOrder ' 		
	
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--PRINT @finalQuery
		
		--========================================================================================
		-- Begin - Execute the Query 
		
		if (@isFullList = 1) 
			BEGIN
				PRINT(@mainSelectQuery)
				EXEC (@mainSelectQuery)
			END		
		else
			BEGIN	
				PRINT(@finalQuery)
				EXEC (@finalQuery)
			END
		
																										
		-- End - Execute the Query 
		--========================================================================================	
		
		-- End building the main select Query
		--========================================================================================																																			

		Declare @selectTotalcount nvarchar(2000)
		SET @selectTotalcount = 'SELECT count(*) as TotalCount' + @fromClause + @whereClause
		--PRINT(@selectTotalcount)
		EXEC (@selectTotalcount)
		
		--========================================================================================							
END
GO
