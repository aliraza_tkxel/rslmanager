
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetPreviousTenantsCount] 
			@caseOwnedById int=0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@surname		varchar(100),
			@skipIndex	int = 0,
			@pageSize	int = 10,
			@address1 varchar(200),
            @tenancyid VARCHAR(15)
AS
BEGIN

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'')'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END

SET @query =
					'SELECT COUNT(*) as recordCount
				
					FROM         AM_Customer_Rent_Parameters 
								  INNER JOIN C_TENANCY ON AM_Customer_Rent_Parameters.TENANCYID = C_TENANCY.TENANCYID
								  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID	
								  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
								  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 			  
								  --INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
								  LEFT JOIN AM_CASE ON  AM_CASE.TENANCYID=AM_CUSTOMER_RENT_PARAMETERS.TENANCYID		
		
					WHERE '+@RegionSuburbClause+' 
	--					   AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId 
	--																	FROM AM_Case 
	--																	WHERE AM_Case.IsActive= ''true'' ) 		   
	--					   AND AM_FirstDetecionList.IsDefaulter = ''false''
						   AND C_TENANCY.ENDDATE IS NOT NULL
						   AND AM_Customer_Rent_Parameters.LASTNAME LIKE '''' + CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN AM_Customer_Rent_Parameters.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END + ''%'' 
						   AND (
									(P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')  
									--OR 
									--(C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.HOUSENUMBER + '' '' + C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')
									)
						  AND AM_Customer_Rent_Parameters.TenancyId='''' + CASE WHEN '''' = '''+ REPLACE(@tenancyid,'''','''''') +''' THEN AM_Customer_Rent_Parameters.TenancyId ELSE '''+ REPLACE(@tenancyid,'''','''''') +''' END + ''''  '
						   
PRINT(@query);
EXEC(@query);

END









GO
