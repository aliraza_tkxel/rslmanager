
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_GetCustomerInformation]
			@tenantId	int,
			@customerId	int
AS
BEGIN

SELECT TOP 1 customer.TENANCYID, 
			   customer.RentBalance,
			   customer.EstimatedHBDue,
			   (ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0)) as OwedToBHA,
			   (ISNULL(customerhistory.RentBalance, 0.0) - ISNULL(customerhistory.EstimatedHBDue, 0.0)) as LastMonthNetArrears,
			   ISNULL(Convert(varchar(100),(customer.LastPaymentDate), 103), '') AS LastPaymentDate, 
			   ISNULL((customer.LastPayment), 0.0) AS LastPayment,
			   P_FINANCIAL.Totalrent as rentAmount,
			   customer.CustomerId,
			   customer.CustomerAddress,

				(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE AM_Customer_Rent_Parameters.TenancyId = @tenantId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

				(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE AM_Customer_Rent_Parameters.TenancyId = @tenantId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

				(SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE	AM_Customer_Rent_Parameters.TenancyId =@tenantId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						) as JointTenancyCount,
				P_SCHEME.SCHEMENAME as suburb,P__PROPERTY.HOUSENUMBER,P__PROPERTY.ADDRESS1,P__PROPERTY.ADDRESS2,
				P__PROPERTY.TOWNCITY,P__PROPERTY.POSTCODE,P__PROPERTY.COUNTY
FROM                   AM_Customer_Rent_Parameters customer INNER JOIN
                      C_TENANCY ON C_TENANCY.TENANCYID = customer.TENANCYID INNER JOIN                      
                      P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID 
					  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					  INNER JOIN P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID LEFT JOIN 
					  AM_Customer_Rent_Parameters_History customerhistory ON customerhistory.customerid=customer.customerid
WHERE customer.TenancyId = @tenantId

--order by  F_RENTJOURNAL.TENANCYID DESC

END

--exec [AM_SP_GetCustomerInformation] 972014,8924






GO
