-- Stored Procedure

-- =============================================
-- EXEC [dbo].[RD_GetCountInProgress]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'InProgress and Paused Status'>
-- Webpage: dashboard.aspx

-- =============================================
ALTER PROCEDURE [dbo].[RD_GetCountInProgress]
	-- Add the parameters for the stored procedure here


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @InProgress INT
DECLARE @Paused INT
DECLARE @totalCount INT
	-- Declaring the variables to be used in this query
SET  @InProgress=(Select COUNT(FL_FAULT_LOG.FaultLogID) FROM FL_FAULT_LOG
		JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		JOIN FL_FAULT_TRADE on FL_FAULT.FaultID = FL_FAULT_TRADE.FaultId
		JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
		JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
		JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId
		LEFT JOIN P_SCHEME on FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID	
		LEFT JOIN P_BLOCK on FL_FAULT_LOG.BLOCKID = P_BLOCK.BLOCKID	

WHERE
StatusID =15)
SET @Paused=(select COUNT(FL_FAULT_LOG.FaultLogID)
FROM FL_FAULT_PAUSED		
		INNER JOIN FL_FAULT_LOG on FL_FAULT_PAUSED.FaultLogId = FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		INNER JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		INNER JOIN (SELECT max(FL_FAULT_JOBTIMESHEET.TimeSheetID) TimeSheetID,FL_FAULT_JOBTIMESHEET.FaultLogId, MAX(FL_FAULT_JOBTIMESHEET.StartTime) StartTime
					FROM FL_FAULT_JOBTIMESHEET 
					group by FL_FAULT_JOBTIMESHEET.FaultLogId ) TimeSheetTable on FL_FAULT_PAUSED.FaultLogId = TimeSheetTable.FaultLogId		
		INNER JOIN E__EMPLOYEE on FL_FAULT_PAUSED.PausedBy = E__EMPLOYEE.EMPLOYEEID 
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID	
		LEFT JOIN P_SCHEME on FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID	
		LEFT JOIN P_BLOCK on FL_FAULT_LOG.BLOCKID = P_BLOCK.BLOCKID	

		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
		JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
		JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId

WHERE
 StatusID =16 )
SELECT  @totalCount = @InProgress+@Paused 

	Select @totalCount as totalCount

	return @totalCount	

END





GO