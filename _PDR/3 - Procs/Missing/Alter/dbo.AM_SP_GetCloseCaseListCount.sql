
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_GetCloseCaseListCount] 
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle  varchar(100),
        @surname varchar(50)
AS
BEGIN
	declare @RegionSuburbClause varchar(8000)
	declare @query varchar(8000)


	IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

SET @query =
	'SELECT COUNT(*) as recordCount FROM(SELECT COUNT(*) as recordCount

		  FROM   AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
					  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID

			Where (AM_Case.CaseOfficer = (case when 0 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when 0 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  '+ @RegionSuburbClause +'
						  AND (customer.LASTNAME = case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end) 
						  AND AM_Case.IsActive = 0
						  AND (AM_Status.Title LIKE '''' + case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end + ''%'')
                          GROUP BY AM_Case.TenancyId) as TEMP'

PRINT(@query);
EXEC(@query);


END







GO
