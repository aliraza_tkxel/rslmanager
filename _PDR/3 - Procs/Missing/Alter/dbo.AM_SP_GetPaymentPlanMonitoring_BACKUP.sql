SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetPaymentPlanMonitoring_BACKUP]
			@postCode varchar(50)='',
			@assignedToId	int = 0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@allAssignedFlag	bit,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@skipIndex	int = 0,
			@pageSize	int = 10,
            @missedCheck varchar(15)='',
			@sortBy     varchar(100),
			@sortDirection varchar(10)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @orderbyClause varchar(50)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @RegionSuburbClause varchar(8000)
declare @MissedClause varchar(8000)

IF(@assignedToId = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
	
	IF(@missedCheck='')
		BEGIN
			SET @MissedClause='-1=-1'
        END
    ELSE
        BEGIN
			SET @MissedClause='AM_PaymentPlan.PaymentPlanId IN (select distinct AM_MissedPayments.PaymentPlanId 
															 FROM AM_MissedPayments INNER JOIN
																	AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
															  WHERE (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																	OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId)+ ' END) ) 
																	)'
       END
       

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		--SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
		--												FROM AM_ResourcePatchDevelopment 
		--												WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ 'AND IsActive=''true'')'
		SET @RegionSuburbClause='-1=-1'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

IF(@missedCheck='')
		BEGIN
			SET @MissedClause='-1=-1'
        END
    ELSE
        BEGIN
			SET @MissedClause='AM_PaymentPlan.PaymentPlanId IN (select distinct AM_MissedPayments.PaymentPlanId 
															 FROM AM_MissedPayments INNER JOIN
																	AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
															  WHERE (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																	OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId)+ ' END) ) 
																	)'
       END

END


SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
SET @query = 
'SELECT DISTINCT TOP('+convert(varchar(10),@pageSize)+')    Max(customer.CUSTOMERID) AS CUSTOMERID,
								  Max(AM_PaymentPlan.TennantId) AS TennantId, 
								  Max(AM_PaymentPlan.PaymentPlanId) AS PaymentPlanId, 
								  Max(AM_PaymentPlan.StartDate) AS StartDate, 
								  Max(AM_PaymentPlan.EndDate) AS EndDate, 
								  Max(AM_PaymentPlan.FrequencyLookupCodeId) AS FrequencyLookupCodeId,
								  Max(AM_PaymentPlan.AmountToBeCollected) AS AmountToBeCollected, 
								  Max(AM_PaymentPlan.ReviewDate) AS ReviewDate, 
								  Max(AM_PaymentPlan.FirstCollectionDate) AS FirstCollectionDate, 
								  
							(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
								FROM AM_Customer_Rent_Parameters
								WHERE TenancyId = AM_PaymentPlan.TennantId
								ORDER BY CustomerId ASC) as CustomerName,

							(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
								FROM AM_Customer_Rent_Parameters
								WHERE TenancyId = AM_PaymentPlan.TennantId
								ORDER BY CustomerId DESC) as CustomerName2,

							 (SELECT Count(DISTINCT CustomerId)
								FROM AM_Customer_Rent_Parameters
								WHERE	TenancyId = AM_PaymentPlan.TennantId) as JointTenancyCount,
								  
								Max(customer.RentBalance) AS RentBalance,
								  
								  ISNULL((SELECT P_FINANCIAL.Totalrent 
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
													 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID
													 WHERE C_TENANCY.TENANCYID = AM_PaymentPlan.TennantId), 0.00) as WeeklyRentAmount,
                                 (SELECT TOP 1 CaseId 
										FROM AM_CaseHistory 
										WHERE TennantId = AM_PaymentPlan.TennantId and IsActive = ''true'' )as CaseId
									


				FROM         AM_PaymentPlan INNER JOIN
							 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
							 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId INNER JOIN
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
							 INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID

Where AM_PaymentPlan.IsCreated = ''true'' 
	  AND '+ @RegionSuburbClause +'
	  AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
	  AND AM_PaymentPlan.CreatedBy = (CASE WHEN -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_PaymentPlan.CreatedBy ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
      AND '+@MissedClause+ '
	  --AND AM_PaymentPlan.PaymentPlanId IN (CASE WHEN '''' = ''' +  convert(varchar(10),@missedCheck )+ ''' THEN  AM_PaymentPlan.PaymentPlanId ELSE
														--	(select distinct AM_MissedPayments.PaymentPlanId 
														--	 FROM AM_MissedPayments INNER JOIN
																--	AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
															--  WHERE (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																--	OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId)+ ' END) ) 
																	--) 
																--END)
	AND AM_PaymentPlan.TennantId NOT IN('
	SET @subQuery ='SELECT TennantId FROM(											
										SELECT DISTINCT TOP('+convert(varchar(10),@skipIndex)+')    Max(customer.CUSTOMERID) AS CUSTOMERID,
								  Max(AM_PaymentPlan.TennantId) AS TennantId, 
								  Max(AM_PaymentPlan.PaymentPlanId) AS PaymentPlanId, 
								  Max(AM_PaymentPlan.StartDate) AS StartDate, 
								  Max(AM_PaymentPlan.EndDate) AS EndDate, 
								  Max(AM_PaymentPlan.FrequencyLookupCodeId) AS FrequencyLookupCodeId,
								  Max(AM_PaymentPlan.AmountToBeCollected) AS AmountToBeCollected, 
								  Max(AM_PaymentPlan.ReviewDate) AS ReviewDate, 
								  Max(AM_PaymentPlan.FirstCollectionDate) AS FirstCollectionDate, 
								  
							(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
								FROM AM_Customer_Rent_Parameters
								WHERE TenancyId = AM_PaymentPlan.TennantId
								ORDER BY CustomerId ASC) as CustomerName,

							(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
								FROM AM_Customer_Rent_Parameters
								WHERE TenancyId = AM_PaymentPlan.TennantId
								ORDER BY CustomerId DESC) as CustomerName2,

							 (SELECT Count(DISTINCT CustomerId)
								FROM AM_Customer_Rent_Parameters
								WHERE	TenancyId = AM_PaymentPlan.TennantId) as JointTenancyCount,
								  
								Max(customer.RentBalance) AS RentBalance,
								  
								  ISNULL((SELECT P_FINANCIAL.Totalrent 
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
													 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID
													 WHERE C_TENANCY.TENANCYID = AM_PaymentPlan.TennantId), 0.00) as WeeklyRentAmount,
                                 (SELECT TOP 1 CaseId 
										FROM AM_CaseHistory 
										WHERE TennantId = AM_PaymentPlan.TennantId and IsActive = ''true'' )as CaseId
											


										FROM         AM_PaymentPlan INNER JOIN
													 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
													 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
													 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId
													 INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID

									Where AM_PaymentPlan.IsCreated = ''true'' 
										  AND '+ @RegionSuburbClause +'
										  AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
										  AND AM_PaymentPlan.CreatedBy = (CASE WHEN -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_PaymentPlan.CreatedBy ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
										  AND '+@MissedClause+ ' GROUP BY AM_PaymentPlan.TennantId  '+ @orderbyClause + ') as Temp
												) GROUP BY AM_PaymentPlan.TennantId ' 

print(@query + @subQuery + @orderbyClause);
exec(@query + @subQuery + @orderbyClause);	
																

END













GO
