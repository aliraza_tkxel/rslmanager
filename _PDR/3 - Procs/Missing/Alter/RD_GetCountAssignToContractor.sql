USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GetCountAssignToContractor]    Script Date: 12/31/2014 15:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- EXEC [dbo].[RD_GetCountAssignToContractor]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx
-- Updated On: 31/12/2014
-- Updated By: Salman Nazir
-- Reason:  Change Dashboard counts for Block and Schemes
-- =============================================
ALTER PROCEDURE [dbo].[RD_GetCountAssignToContractor]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	
	SET NOCOUNT ON;
	SELECT   count(*)  
	FROM FL_FAULT_LOG
		INNER JOIN (SELECT FL_FAULT_LOG.FaultLogID faultLogID
					FROM FL_FAULT_LOG
					WHERE FL_FAULT_LOG.StatusID=2) SubcontractorsFaults on FL_FAULT_LOG.FaultLogID=SubcontractorsFaults.faultLogID
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		INNER JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		INNER JOIN S_ORGANISATION on FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID
		LEFT OUTER JOIN E__EMPLOYEE on FL_FAULT_LOG.UserID = E__EMPLOYEE.EMPLOYEEID
		INNER JOIN FL_FAULT_STATUS on  FL_FAULT_LOG.StatusID=FL_FAULT_STATUS.FaultStatusID
		LEFT JOIN P_SCHEME on FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID
		LEFT JOIN P_BLOCK on FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID
END



