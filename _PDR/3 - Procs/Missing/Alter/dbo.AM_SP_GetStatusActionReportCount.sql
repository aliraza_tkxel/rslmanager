
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetStatusActionReportCount]
@assignedToId	int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,
		@allAssignedFlag	bit,
		@allRegionFlag	bit,
		@allSuburbFlag	bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  if(@allAssignedFlag = 0)
		BEGIN
			if(@allRegionFlag = 0)
			BEGIN
				if(@allSuburbFlag = 0)
				BEGIN
					SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID INNER JOIN
										  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
										   LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
					WHERE		(PDR_DEVELOPMENT.PATCHID = @regionId and P__PROPERTY.SCHEMEID = @suburbId
								and (AM_CaseHistory.CaseOfficer = @assignedToId or AM_CaseHistory.CaseManager = @assignedToId ))
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

				END
				else
				BEGIN
					SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID INNER JOIN
										  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
										    LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
					WHERE		(PDR_DEVELOPMENT.PATCHID = @regionId 
								and (AM_CaseHistory.CaseOfficer = @assignedToId or AM_CaseHistory.CaseManager = @assignedToId ))
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

				END
			END
			else
			BEGIN
					SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID INNER JOIN
										  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
										  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
					WHERE		(PDR_DEVELOPMENT.PATCHID IN (select PatchId From AM_ResourcePatchDevelopment where ResourceId = @assignedToId) 
								and (AM_CaseHistory.CaseOfficer = @assignedToId or AM_CaseHistory.CaseManager = @assignedToId ))
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

			END
		END
		else
		BEGIN
			if(@allRegionFlag = 0)
			BEGIN
				if(@allSuburbFlag = 0)
				BEGIN
					SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID INNER JOIN
										  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
										    LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
					WHERE		(PDR_DEVELOPMENT.PATCHID = @regionId and P__PROPERTY.SCHEMEID = @suburbId)
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

				END
				else
				BEGIN
					SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID INNER JOIN
										  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
										  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
					WHERE		(PDR_DEVELOPMENT.PATCHID = @regionId)
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

				END
			END
			else
			BEGIN
					SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID INNER JOIN
										  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID	
										  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 			
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

			END
		END
END

-- exec AM_SP_GetStatusActionReport null, null, null, 1,1,1





GO
