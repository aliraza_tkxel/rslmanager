
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Adeel Ahmed>
-- Create date: <03/05/2010>
-- Description:	<Counting the My Cases on the basis of CaseOfficer/Region/Suburb>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetMyCasesCount]
	@postCode varchar(25),
	@caseOfficerId INT,
	@regionId INT,
	@suburbId INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
if(@caseOfficerId > 0  and @regionId <= 0 and @suburbId <= 0)
BEGIN
	SELECT DISTINCT AM_Case.TenancyId  from AM_Case
    INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TenancyId
    INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
    INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
    INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
    INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P_SCHEME.DEVELOPMENTID 
	WHERE 1=1 --AM_Case.CaseOfficer = @caseOfficerId 
	and AM_Case.IsActive = 'true' 
	AND (C_TENANCY.ENDDATE > GETDATE() OR C_TENANCY.ENDDATE IS NULL)
    AND C_ADDRESS.POSTCODE=CASE WHEN '' = @postCode THEN C_ADDRESS.POSTCODE ELSE @postCode END
    AND (P_SCHEME.SCHEMEID IN (	SELECT DISTINCT SCHEMEID 
								FROM AM_ResourcePatchDevelopment 
								WHERE ResourceId = @caseOfficerId  AND IsActive='true') 
		OR PDR_DEVELOPMENT.PATCHID IN (	SELECT PatchId 
										FROM AM_ResourcePatchDevelopment 
										WHERE ResourceId =@caseOfficerId AND IsActive='true')) 
    
END
else if(@caseOfficerId > 0  and @regionId > 0 and @suburbId <= 0)
BEGIN
	SELECT DISTINCT AM_Case.TenancyId  from AM_Case 

	INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
	LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
    INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TenancyId
    INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID

	WHERE 1=1 --AM_Case.CaseOfficer = @caseOfficerId 
	and PDR_DEVELOPMENT.PATCHID = @regionId 
	and AM_Case.IsActive = 'true'
	AND (C_TENANCY.ENDDATE > GETDATE() OR C_TENANCY.ENDDATE IS NULL)
	AND PDR_DEVELOPMENT.PATCHID IN (
								  SELECT PatchId FROM AM_ResourcePatchDevelopment 
								  WHERE ResourceId =@caseOfficerId AND IsActive='true'
								  )
    AND C_ADDRESS.POSTCODE=CASE WHEN ''=@postCode THEN C_ADDRESS.POSTCODE ELSE @postCode END
END
else if(@caseOfficerId > 0  and @regionId > 0 and @suburbId > 0)
BEGIN
	
	SELECT DISTINCT AM_Case.TenancyId  from AM_Case 
	INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
	LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
    INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TenancyId
    INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
	WHERE 1=1 --AM_Case.CaseOfficer = @caseOfficerId 
	and PDR_DEVELOPMENT.PATCHID = @regionId 
	AND PDR_DEVELOPMENT.PATCHID IN (
								  SELECT PatchId FROM AM_ResourcePatchDevelopment 
								  WHERE ResourceId =@caseOfficerId AND IsActive='true'
								  )
	and P__PROPERTY.SCHEMEID = @suburbId 
	AND P__PROPERTY.SCHEMEID IN (
										SELECT SCHEMEID 
										FROM AM_ResourcePatchDevelopment 
										WHERE ResourceId = @caseOfficerId  AND IsActive='true'
										)
	and AM_Case.IsActive = 'true'
	AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
    AND C_ADDRESS.POSTCODE=CASE WHEN ''=@postCode THEN C_ADDRESS.POSTCODE ELSE @postCode END

END

else if(@caseOfficerId > 0  and @regionId <= 0 and @suburbId > 0)
BEGIN
	
	SELECT DISTINCT AM_Case.TenancyId  from AM_Case 
	INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
	LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
    INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TenancyId
    INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
	WHERE 1=1 --AM_Case.CaseOfficer = @caseOfficerId 
	and P__PROPERTY.SCHEMEID = @suburbId 
	AND P__PROPERTY.SCHEMEID IN (
										SELECT SCHEMEID 
										FROM AM_ResourcePatchDevelopment 
										WHERE ResourceId = @caseOfficerId  AND IsActive='true'
										)
	and AM_Case.IsActive = 'true'
	AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
    AND C_ADDRESS.POSTCODE=CASE WHEN ''=@postCode THEN C_ADDRESS.POSTCODE ELSE @postCode END

END

else
BEGIN
	SELECT DISTINCT AM_Case.TenancyId  from AM_Case 
	INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
	LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
    INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TenancyId
    INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID 
	Where AM_Case.IsActive = 'true' AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL) AND C_ADDRESS.POSTCODE=CASE WHEN ''=@postCode THEN C_ADDRESS.POSTCODE ELSE @postCode END
END


	END











GO
