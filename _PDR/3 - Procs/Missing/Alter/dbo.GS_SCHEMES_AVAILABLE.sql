-- Stored Procedure

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GS_SCHEMES_AVAILABLE]

-- EXEC GS_SCHEMES_AVAILABLE

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  SCHEMEID AS DEVELOPMENTID,SCHEMENAME AS  DEVELOPMENTNAME
FROM P_SCHEME
WHERE SCHEMEID NOT IN( 
	SELECT ISNULL(SS.SCHEMEID,-1)
	FROM S_SCOPETOPATCHANDSCHEME SS 
	INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID 
	WHERE S.RENEWALDATE>=DATEADD(WW,10,GETDATE()) 
	)     

END

GO