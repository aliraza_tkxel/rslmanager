-- Stored Procedure

-- =============================================
--DECLARE	@totalCount int
--EXEC	[dbo].[RD_GetFaultsOverdueList]
--		@searchedText = NULL,	
--		@totalCount = @totalCount OUTPUT		
-- Author:		<Ali Raza>
-- Create date: <8/16/2013>
-- Description:	<Get list of Completed Faults>
-- Web Page: ReportsArea.aspx
-- =============================================
ALTER PROCEDURE [dbo].[RD_GetFaultsOverdueList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@searchedText varchar(8000),
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Recorded',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(3000),
        @whereClause  varchar(3000),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(MAX),        
        @rowNumberQuery varchar(MAX),
        @finalQuery varchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),

        --variables for paging
        @offset int,
		@limit int		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided

		SET @searchCriteria = ' 1=1 AND GETDATE() > IncompleteFaults.Deadline '

		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
		END				
		IF(@schemeId != -1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.SCHEMEID = '+convert(varchar(10),@schemeId)
			END
		IF(@blockId != -1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.BLOCKID = '+convert(varchar(10),@blockId)
			END
		-- End building SearchCriteria clause   
		--========================================================================================

		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here

		SET @selectClause = 'SELECT DISTINCT TOP ('+convert(varchar(10),@limit)+') FL_FAULT_LOG.JobSheetNumber as JSN,
		CONVERT(nvarchar(50),FL_FAULT_LOG.SubmitDate, 103) as Recorded,
		ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
		ISNULL(P__PROPERTY.HOUSENUMBER,'''') + ISNULL('' '' + P__PROPERTY.ADDRESS1,'''') as Address,
		FL_AREA.AreaName as Location, FL_FAULT.Description as Description,FL_FAULT_STATUS.Description AS Status,
		FL_FAULT_PRIORITY.PriorityName AS Priority,convert(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103)+'' ''+ convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentDate,  P__PROPERTY.HouseNumber as HouseNumber,
		P__PROPERTY.ADDRESS1 as Address1, FL_FAULT_LOG.SubmitDate as RecordedOn,
		FL_FAULT.duration as Interval, FL_FAULT_LOG.FaultLogID as FaultLogID
		,FL_CO_APPOINTMENT.AppointmentDate +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentSort
		,Case 
			When FL_FAULT_LOG.PROPERTYID IS NULL THEN	''SbFault''	
			Else ''Fault''	End	AS AppointmentType
		'

		-- End building SELECT clause
		--======================================================================================== 							


		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'		
		FROM (SELECT distinct FL_FAULT_LOG.FaultLogID ,FL_FAULT_LOG.SubmitDate,CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+'' days'' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+'' hours'' END as Priority ,CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			DATEADD(day,FL_FAULT_PRIORITY.ResponseTime,FL_FAULT_LOG.SubmitDate) ELSE
			DATEADD(hour,FL_FAULT_PRIORITY.ResponseTime,FL_FAULT_LOG.SubmitDate) END as Deadline
				FROM FL_FAULT_LOG 
				INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID 
				INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
				--17	Complete
				--11	Cancelled Fault
				--13	Cancelled
				where StatusID NOT IN (11,13,17)	) IncompleteFaults
		INNER JOIN FL_FAULT_LOG on IncompleteFaults.FaultLogID = FL_FAULT_LOG.FaultLogId
		INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		INNER JOIN FL_FAULT_TRADE on FL_FAULT.FaultID = FL_FAULT_TRADE.FaultId
		INNER JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
		INNER JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId
		LEFT JOIN P_SCHEME on FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID	
		LEFT JOIN P_BLOCK on FL_FAULT_LOG.BLOCKID = P_BLOCK.BLOCKID	'
		-- End building From clause
		--======================================================================================== 														  


		--========================================================================================    
		-- Begin building OrderBy clause		

		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ADDRESS1' 		
		END

		IF(@sortColumn = 'Recorded')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Recorded' 		
		END

		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
		END

		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Scheme' 		
		END	

		IF(@sortColumn = 'Block')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Block' 		
		END	

		IF(@sortColumn = 'AppointmentDate')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'AppointmentSort' 		
		END	



		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		-- End building OrderBy clause
		--========================================================================================								

		--========================================================================================
		-- Begin building WHERE clause

		-- This Where clause contains subquery to exclude already displayed records			  

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		-- End building WHERE clause
		--========================================================================================

		--========================================================================================
		-- Begin building the main select Query

		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query

		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		-- End building the row number query
		--========================================================================================

		--========================================================================================
		-- Begin building the final query 

		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		-- End building the final query
		--========================================================================================									

		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT  @totalCount = COUNT( DISTINCT FL_FAULT_LOG.FaultLogID) ' + @fromClause + @whereClause

		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================							
END



GO