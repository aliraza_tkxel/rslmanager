USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetJobSheetSummaryDetails]    Script Date: 10/24/2014 10:21:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetJobSheetSummaryDetails]
	-- Add the parameters for the stored procedure here
	@JOURNALHISTORYID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @propertyId varchar(50)
    -- Insert statements for procedure here
    --=============================================================
    -- Appointment Info Details
    --============================================================
Select  PA.APPOINTMENTID as JSN ,ISNULL(PAH.APPOINTMENTDATE, ' ') as StartDate,ISNULL(PAH.APPOINTMENTENDDATE,' ') as EndDate,ISNULL(PAH.APPOINTMENTSTATUS,' ') as InterimStatus,PA.JournalId as PMO,PJH.PROPERTYID,PJH.JOURNALHISTORYID
,PAH.COMPTRADEID , ISNULL(PC.COMPONENTNAME,' ') as Component, ISNULL(T.Description,' ') as Trade, PAt.Planned_Appointment_Type,ISNULL(PMT.DURATION,' ') as Duration, ISNULL(PAH.ASSIGNEDTO, ' ') as Operative,PAH.isMiscAppointment
,ISNULL(PAH.CUSTOMERNOTES,' ') as CustomerNotes,ISNULL(PAH.APPOINTMENTNOTES,' ') as JobSheetNotes,ISNULL(PAH.APPOINTMENTSTARTTIME,' ') as StartTime,ISNULL(PAH.APPOINTMENTENDTIME,' ') as EndTime,ROW_NUMBER() over (order by PA.APPOINTMENTID) as Row
from PLANNED_JOURNAL_HISTORY PJH
INNER Join PLANNED_APPOINTMENTS PA on PJH.JOURNALID = PA.JournalId
INNER JOIN PLANNED_APPOINTMENTS_HISTORY PAH on PAH.APPOINTMENTID = PA.APPOINTMENTID
INNER JOIN (SELECT MAX(APPOINTMENTHISTORYID) AS [MAXAPPOINTMENTHISTORYID], APPOINTMENTID
	 FROM PLANNED_APPOINTMENTS_HISTORY GROUP BY APPOINTMENTID) MPAH ON MPAH.MAXAPPOINTMENTHISTORYID = PAH.APPOINTMENTHISTORYID
Left JOIN PLANNED_COMPONENT_TRADE PCT on  PCT.COMPTRADEID = PAH.COMPTRADEID
Left Join PLANNED_COMPONENT PC on PC.COMPONENTID = PCT.COMPONENTID
Left Join G_TRADE T on T.TradeId = PCT.TRADEID
Left Join PLANNED_MISC_TRADE PMT on PMT.TradeId = T.TradeId AND PMT.AppointmentId = PA.APPOINTMENTID
Left Join Planned_Appointment_Type PAT on PAT.Planned_Appointment_TypeId = PA.Planned_Appointment_TypeId
Where PJH.JOURNALHISTORYID = @JOURNALHISTORYID ORDER BY PAH.LOGGEDDATE DESC

Select @propertyId = PROPERTYID From PLANNED_JOURNAL_HISTORY Where JOURNALHISTORYID = @JOURNALHISTORYID
   --==================================================================
   -- Property & Customer Info
   --==================================================================
   EXEC PLANNED_GetPropertyDetail  @propertyId

END
