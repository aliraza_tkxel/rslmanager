USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GetCountFollowOn]    Script Date: 12/31/2014 14:49:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- EXEC [dbo].[RD_GetCountFOLLOWON]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx
-- Updated On: 31/12/2014
-- Updated By: Salman Nazir
-- Reason:  Change Dashboard counts for Block and Schemes
-- =============================================
ALTER PROCEDURE [dbo].[RD_GetCountFollowOn]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT   count(FollowOnID)  FROM FL_FAULT_FOLLOWON    
	JOIN FL_FAULT_LOG on FL_FAULT_FOLLOWON.FaultLogId = FL_FAULT_LOG.FaultLogID    
	LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_SCHEME on FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID
	LEFT JOIN P_BLOCK on FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID
	WHERE isFollowonScheduled = 0
		
END

