-- Stored Procedure

-- =============================================
--DECLARE	@totalCount int
--EXEC	[dbo].[RD_GetReportedFaultList]
--		@searchedText = NULL	,	
--		@totalCount = @totalCount OUTPUT		
-- Author:		Adnan Mirza
-- Create date: <21/10/2013>
-- Modified: Aamir Waheed, 06/12/2013
-- Description:	<Get list of Reported Faults>
-- Web Page: ReportsArea.aspx
-- =============================================
ALTER PROCEDURE [dbo].[RD_GetReportedFaultList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@searchedText varchar(8000),
		--@operativeId int,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'FaultLogID',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),
        @total INT,

        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		--Fiscal Year Formula
		DECLARE @StartDate DATETIME
		DECLARE @EndDate DATETIME
		DECLARE @StartDateVar nvarchar(20)
		DECLARE @EndDateVar nvarchar(20)
		SET @StartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ) )
		SET @EndDate = DATEADD(SS,-1,DATEADD(mm,12,@StartDate))
		SET @StartDateVar =CONVERT(VARCHAR(10),@StartDate, 120)
		SET @EndDateVar =CONVERT(VARCHAR(10),@EndDate, 120)
		--========================================================================================
		-- Begin building SearchCriteria clause for InProgress
		-- These conditions will be added into where clause based on search criteria provided

		SET @searchCriteria = ' 1=1 AND CONVERT(VARCHAR(10),dbo.FL_FAULT_LOG.SubmitDate, 120) BETWEEN  CONVERT(VARCHAR(10), '''+@StartDateVar+''', 120)  AND CONVERT(VARCHAR(10), '''+@EndDateVar+''', 120) '

		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
		END
				-- Add checks for Scheme and Block 
		IF(@schemeId > 0)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.SCHEMEID = '+convert(varchar(10),@schemeId)
			END
		IF(@blockId > 0)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.BLOCKID = '+convert(varchar(10),@blockId)
			END	
		-- End building SearchCriteria clause   
		--========================================================================================

		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- SELECT statements for procedure here

		SET @SelectClause = 'SELECT  DISTINCT TOP ('+convert(varchar(10),@limit)+') FL_FAULT_LOG.JobSheetNumber as JSN,
		CONVERT(nvarchar(50),FL_FAULT_LOG.SubmitDate, 103) as Recorded,
		ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,case 
		WHEN LEN(P__PROPERTY.PROPERTYID) > 0 THEN
		ISNULL(P__PROPERTY.HOUSENUMBER,'''') + ISNULL(''''  + P__PROPERTY.ADDRESS1,'''') 
		ELSE 
		ISNULL(P_BLOCK.ADDRESS1,'''')+ISNULL('' ''+P_BLOCK.ADDRESS2,'''') 
		END as Address,
		FL_AREA.AreaName as Location, FL_FAULT.Description as Description,FL_FAULT_STATUS.Description AS Status,
		FL_FAULT_PRIORITY.PriorityName AS Priority,convert(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103)+'' ''+ convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentDate,  P__PROPERTY.HouseNumber as HouseNumber,
		P__PROPERTY.ADDRESS1 as Address1, FL_FAULT_LOG.SubmitDate as RecordedOn,
		FL_FAULT.duration as Interval, FL_FAULT_LOG.FaultLogID as FaultLogID
		,FL_CO_APPOINTMENT.AppointmentDate +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentSort
		,Case 
			When FL_FAULT_LOG.PROPERTYID IS NULL THEN	''SbFault''	
			Else ''Fault''	End	AS AppointmentType
		'

		-- End building SELECT clause
		--======================================================================================== 							


		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'FROM FL_FAULT_LOG
		JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		LEFT JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
		LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
		LEFT JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId
		LEFT JOIN P_SCHEME on FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID	
		LEFT JOIN P_BLOCK on FL_FAULT_LOG.BLOCKID = P_BLOCK.BLOCKID	'
		-- End building From clause
		--========================================================================================

		--========================================================================================
		-- Begin building OrderBy clause

		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ADDRESS1' 		
		END

		IF(@sortColumn = 'Recorded')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
		END

		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
		END

		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Scheme' 		
		END	

		IF(@sortColumn = 'Block')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Block' 		
		END	

		IF(@sortColumn = 'AppointmentDate')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'AppointmentSort' 		
		END	



		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		-- End building OrderBy clause
		--========================================================================================								

		--========================================================================================
		-- Begin building WHERE clause

		-- This Where clause contains subquery to exclude already displayed records			  

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		-- End building WHERE clause
		--========================================================================================


		--========================================================================================
		-- Begin building the main select Query

		Set @mainSelectQuery = @SelectClause +@fromClause+@whereClause + @orderClause 

		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query

		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		-- End building the row number query
		--========================================================================================

		--========================================================================================
		-- Begin building the final query 

		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		-- End building the final query
		--========================================================================================									

		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									



--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT  @totalCount = COUNT( DISTINCT FL_FAULT_LOG.FaultLogID) ' + @fromClause + @whereClause

		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

END
GO