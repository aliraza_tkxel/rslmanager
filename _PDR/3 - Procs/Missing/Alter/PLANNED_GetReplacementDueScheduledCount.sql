-- Stored Procedure

ALTER PROCEDURE [dbo].[PLANNED_GetReplacementDueScheduledCount] 

/* ===========================================================================
 '   NAME:           PLANNED_GetReplacementDueScheduledCount
--EXEC	[dbo].[PLANNED_GetReplacementDueScheduledCount]	
-- Author:		<Ahmed Mehmood>
-- Create date: <10/08/2013>
-- Description:	<Get Replacement Due Scheduled Count>
-- Web Page: Dashboard.aspx
 '==============================================================================*/

	(
	    -- These Parameters are passed as Search Criteria

		@componentId int = -1

	)

AS	
	DECLARE @SelectClause varchar(8000),
			@FromClause   varchar(8000),
			@WhereClause  varchar(8000),	        
			@mainSelectQuery varchar(5500),        
			@SearchCriteria varchar(8000)


	SET @SearchCriteria = ' '

    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND '


	SET @SelectClause = 'SELECT	COUNT(JOURNAL_PMO.JID) as TotalCount '
	SET @FromClause = CHAR(10) +'		
		FROM 
		PLANNED_JOURNAL 
		INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
		LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
		LEFT JOIN E__EMPLOYEE ON PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
		LEFT JOIN PLANNED_COMPONENT_TRADE ON  PLANNED_APPOINTMENTS.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID 
		--INNER JOIN PLANNED_MISC_TRADE ON PLANNED_APPOINTMENTS.APPOINTMENTID = PLANNED_MISC_TRADE.AppointmentId  

		INNER JOIN (SELECT	PLANNED_JOURNAL.JOURNALID as JID , ''PMO''+CONVERT(VARCHAR,PLANNED_JOURNAL.JOURNALID) as PMO
					FROM	PLANNED_JOURNAL ) JOURNAL_PMO ON PLANNED_JOURNAL.JOURNALID = JOURNAL_PMO.JID 
		INNER JOIN (SELECT	PLANNED_APPOINTMENTS.APPOINTMENTID as AID , ''JSN'' +RIGHT(''00000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),5) as JSN
					FROM	PLANNED_APPOINTMENTS ) as APPOINTMENT_AID ON PLANNED_APPOINTMENTS.APPOINTMENTID = APPOINTMENT_AID.AID 
		INNER JOIN (SELECT	P__PROPERTY.PROPERTYID as PID,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
					FROM	P__PROPERTY) AS PROPERTYADDRESS ON P__PROPERTY.PROPERTYID = PROPERTYADDRESS.PID	'
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE PLANNED_STATUS.TITLE=''Arranged'' AND ' + 
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 '
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
	print (@mainSelectQuery)
	EXEC (@mainSelectQuery)


















GO