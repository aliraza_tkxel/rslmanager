USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[AS_SchemeNames]    Script Date: 03/04/2015 11:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC AS_SchemeNames 
   --@patchid int = -1
-- Author:		<Salman Nazir>
-- Create date: <09/06/2012>
-- Description:	<This Stored Proceedure fetch the Scheme Names and shows in the drop down on Add User Page>
-- WebPage: Resources.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_SchemeNames](
@patchid int = -1
)	
AS
BEGIN
		SET NOCOUNT ON;

    If (@patchid <= 0)
    BEGIN
		SELECT	SCHEMENAME,P_SCHEME.SCHEMEID AS DEVELOPMENTID
		From	P_SCHEME
		ORDER BY CAST(ISNULL(NULLIF(LEFT(LTRIM(P_SCHEME.SCHEMENAME),PATINDEX('%[^0-9]%',LTRIM(P_SCHEME.SCHEMENAME))-1),''), '100000') AS INT)
			,LTRIM(P_SCHEME.SCHEMENAME) ASC
    END
    else
    BEGIN
		SELECT	P_SCHEME.SCHEMENAME ,P_SCHEME.SCHEMEID AS  DEVELOPMENTID
		From	P_SCHEME
				INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
		WHERE	PDR_DEVELOPMENT.PATCHID = @patchid
		ORDER BY CAST(ISNULL(NULLIF(LEFT(LTRIM(P_SCHEME.SCHEMENAME),PATINDEX('%[^0-9]%',LTRIM(P_SCHEME.SCHEMENAME))-1),''), '100000') AS INT)
			,LTRIM(P_SCHEME.SCHEMENAME) ASC
	End

END


GO


