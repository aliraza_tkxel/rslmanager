-- Stored Procedure

-- =============================================
-- Author:		jimmy M
-- Create date: 17 Aug 07
-- Update Date: 19 May 2008
-- Updated By: Umair Naeem
-- Description:	Gas Servicing 
-- UPDATED BY ROBERT EGAN (30/04/2008
-- UPDATE INCLUSED CASE STATEMENT ON DEVELOPMENTID AND SCOPE JOIN AND INNER JOINS AS APPOSED TO LEFT
-- =============================================
--
-- NOTE: REMEMBER TO ADD IN EXTRA INDEXES AND REBUILD!
-- EXEC [GS_CONTRACT_SUMMARY] @ORGID=1270,@PATCHID=18,@DEVELOPMENTID=239
--
ALTER PROCEDURE [dbo].[GS_CONTRACT_SUMMARY]
	-- Add the parameters for the stored procedure here
	@ORGID INT,
	@PATCHID INT=NULL,
	@DEVELOPMENTID INT=NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	PD.PATCHID,PP.DEVELOPMENTID AS DEVELOPMENTID,E.LOCATION,PD.DEVELOPMENTNAME,COUNT(PP.PROPERTYID) AS PROPERTIES,
	   ISNULL(S.PROPERTY,0)*ISNULL(PCOUNT.PROPERTYCOUNT,0) AS COSTPERPROPERTY,
	   ISNULL(NTB.NOOFTRADITIONALBOLIER,0) AS NOOFTRADITIONALBOLIER,
	   ISNULL(NTB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NTB.NOOFTRADITIONALBOLIER),0) AS COSTPERTRADITIONALBOILER,
	   ISNULL(NF.NOOFFIRE,0) AS NOOFFIRE,
	   ISNULL(NF.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NF.NOOFFIRE),0) AS COSTPERFIRE,	
	   ISNULL(NWM.NOOFWATERHEATER_MULTIPOINT,0) AS NOOFWATERHEATER_MULTIPOINT,
	   ISNULL(NWM.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWM.NOOFWATERHEATER_MULTIPOINT),0) AS COSTPERWATERHEATER_MULTIPOINT,	
	   ISNULL(NCH.NOOFCOOKER_HOB,0) AS NOOFCOOKER_HOB,
	   ISNULL(NCH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCH.NOOFCOOKER_HOB),0) AS COSTPERCOOKER_HOB,
	   ISNULL(NCB.NOOFCOMBIBOILER,0) AS NOOFCOMBIBOILER,
	   ISNULL(NCB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCB.NOOFCOMBIBOILER),0) AS COSTPERCOMBIBOILER,	
	   ISNULL(NOB.NOOFOILBOILERS,0) AS NOOFOILBOILERS,
	   ISNULL(NOB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NOB.NOOFOILBOILERS),0) AS COSTPEROILBOILERS,	
	   ISNULL(NWH.NOOFWALLHEATER,0) AS NOOFWALLHEATER,
	   ISNULL(NWH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWH.NOOFWALLHEATER),0) AS COSTPERWALLHEATER,
	   (ISNULL(S.PROPERTY,0)*ISNULL(PCOUNT.PROPERTYCOUNT,0))+		
	   (ISNULL(NTB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NTB.NOOFTRADITIONALBOLIER),0))+	
	   (ISNULL(NF.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NF.NOOFFIRE),0)) +
	   (ISNULL(NWM.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWM.NOOFWATERHEATER_MULTIPOINT),0))+
	   (ISNULL(NCH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCH.NOOFCOOKER_HOB),0))+  
	   (ISNULL(NCB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCB.NOOFCOMBIBOILER),0))+   	
	   (ISNULL(NOB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NOB.NOOFOILBOILERS),0))+
	   (ISNULL(NWH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWH.NOOFWALLHEATER),0)) AS CONTRACTVALUE, 	
	   ISNULL(OVERDUE.OVERDUEPROPERTIES,0)  AS OVERDUEPROPERTIES, S.SCOPEID,S.CONTRACTNAME	
FROM S_SCOPE S
	INNER JOIN C_REPAIR CR ON CR.SCOPEID=S.SCOPEID
	INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=CJ.PROPERTYID
	LEFT JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID
	LEFT JOIN E_PATCH E ON E.PATCHID=PD.PATCHID
	INNER JOIN S_COSTTYPE SC ON SC.COSTTYPEID=S.COSTTYPEID
	LEFT JOIN (
				SELECT COUNT(GP.PROPERTYID) AS NOOFTRADITIONALBOLIER,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
					INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
					INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
					INNER JOIN (
							   SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=1
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
			  ) NTB ON NTB.DEVELOPMENTID=PD.DEVELOPMENTID AND NTB.SCOPEID = S.SCOPEID	
	LEFT JOIN(
				SELECT COUNT(GP.PROPERTYID) AS NOOFFIRE,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
				INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
				INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
				INNER JOIN (
						   SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=2
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				) NF ON NF.DEVELOPMENTID=PD.DEVELOPMENTID AND NF.SCOPEID = S.SCOPEID	
	LEFT JOIN (
				SELECT COUNT(GP.PROPERTYID) AS NOOFWATERHEATER_MULTIPOINT,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
				INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
				INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
				INNER JOIN (
						   SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=3
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				) NWM ON NWM.DEVELOPMENTID=PD.DEVELOPMENTID AND NWM.SCOPEID = S.SCOPEID
	LEFT JOIN (
				SELECT COUNT(GP.PROPERTYID) AS NOOFCOOKER_HOB,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
				INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
				INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
				INNER JOIN (
						   SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=4
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				) NCH ON NCH.DEVELOPMENTID=PD.DEVELOPMENTID AND NCH.SCOPEID = S.SCOPEID
	LEFT JOIN (
				SELECT COUNT(GP.PROPERTYID) AS NOOFCOMBIBOILER,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
				INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
				INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
				INNER JOIN (
						   SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=5
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				) NCB ON NCB.DEVELOPMENTID=PD.DEVELOPMENTID AND NCB.SCOPEID = S.SCOPEID
	LEFT JOIN (
				SELECT COUNT(GP.PROPERTYID) AS NOOFOILBOILERS,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
				INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
				INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
				INNER JOIN (
						   SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=6
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				) NOB ON NOB.DEVELOPMENTID=PD.DEVELOPMENTID AND NOB.SCOPEID = S.SCOPEID
	LEFT JOIN (
				SELECT COUNT(GP.PROPERTYID) AS NOOFWALLHEATER,PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				FROM GS_PROPERTY_APPLIANCE  GP 
				INNER JOIN GS_APPLIANCE_TYPE GA ON GA.APPLIANCETYPEID=GP.APPLIANCETYPEID	
				INNER JOIN P__PROPERTY PP ON GP.PROPERTYID=PP.PROPERTYID
				INNER JOIN (
						  SELECT SA.COSTPERAPPLIANCE,ISNULL(SS.DEVELOPMENTID,PS.DEVELOPMENTID) AS DEVELOPMENTID ,SA.APPLIANCETYPEID,SA.SCOPEID
							   FROM S_SCOPETOAPPLIANCE SA 
								    LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=SA.SCOPEID
								    LEFT JOIN PDR_DEVELOPMENT  PD ON PD.DEVELOPMENTID = SS.DEVELOPMENTID AND PD.PATCHID=SS.PATCHID
									LEFT JOIN P_SCHEME PS ON (PS.SCHEMEID=SS.SchemeId OR SS.SchemeId IS NULL) 
	 						  ) S ON S.APPLIANCETYPEID=GA.APPLIANCETYPEID AND S.DEVELOPMENTID=PP.DEVELOPMENTID
				WHERE GP.APPLIANCETYPEID=7
				GROUP BY PP.DEVELOPMENTID,S.COSTPERAPPLIANCE,S.SCOPEID
				) NWH ON NWH.DEVELOPMENTID = PD.DEVELOPMENTID AND NWH.SCOPEID = S.SCOPEID
	LEFT JOIN (
				SELECT COUNT(PROPERTYID) PROPERTYCOUNT,DEVELOPMENTID 
				FROM P__PROPERTY 
				GROUP BY DEVELOPMENTID
				) PCOUNT  ON  PCOUNT.DEVELOPMENTID = PD.DEVELOPMENTID 		
	LEFT JOIN (
				SELECT COUNT(PP.PROPERTYID) OVERDUEPROPERTIES,PP.DEVELOPMENTID 
				FROM P__PROPERTY PP
					INNER JOIN GS_PROPERTY_APPLIANCE GS ON GS.PROPERTYID=PP.PROPERTYID
				WHERE ((WARRANTYEXPIRYDATE<=GETDATE() AND ISSUEDATE IS NULL) OR ISSUEDATE <=GETDATE())
				GROUP BY PP.DEVELOPMENTID
				)OVERDUE  ON  OVERDUE.DEVELOPMENTID = PD.DEVELOPMENTID 
	LEFT JOIN (	
					SELECT CR.ITEMACTIONID AS ACTION,PD.DEVELOPMENTID,PWP.PATCHID
				    FROM C_REPAIR CR 
	       	        INNER JOIN P_WOTOREPAIR PWR ON PWR.JOURNALID=CR.JOURNALID 
	         		INNER JOIN P_WORKORDER WO ON WO.WOID=PWR.WOID 
					INNER JOIN P_WORKORDERTOPATCH PWP ON PWP.WOID=PWR.WOID
					LEFT JOIN PDR_DEVELOPMENT PD ON PD.PATCHID=PWP.PATCHID AND (PD.DEVELOPMENTID=WO.DEVELOPMENTID OR WO.DEVELOPMENTID IS NULL)
				    WHERE WO.GASSERVICINGYESNO = 1 
					AND CR.REPAIRHISTORYID=(SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID=CR.JOURNALID)	
					GROUP BY CR.ITEMACTIONID,PWP.PATCHID,PD.DEVELOPMENTID
				  )  PROPERTYSTATUS ON PROPERTYSTATUS.DEVELOPMENTID = PD.DEVELOPMENTID  
WHERE CR.REPAIRHISTORYID =(SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID=CJ.JOURNALID)
	AND S.ORGID=COALESCE(@ORGID, S.ORGID)
	AND ISNULL(PD.DEVELOPMENTID,-1)=COALESCE(@DEVELOPMENTID,ISNULL(PD.DEVELOPMENTID,-1))
	AND ISNULL(E.PATCHID,-1)=COALESCE(@PATCHID,ISNULL(E.PATCHID,-1))
	AND PROPERTYSTATUS.ACTION<>2	 	
GROUP BY	PD.PATCHID,PP.DEVELOPMENTID,E.LOCATION,PD.DEVELOPMENTNAME,
			ISNULL(S.PROPERTY,0)*ISNULL(PCOUNT.PROPERTYCOUNT,0),ISNULL(NTB.NOOFTRADITIONALBOLIER,0),
			ISNULL(NTB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NTB.NOOFTRADITIONALBOLIER),0),
			ISNULL(NF.NOOFFIRE,0),ISNULL(NF.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NF.NOOFFIRE),0),
			ISNULL(NWM.NOOFWATERHEATER_MULTIPOINT,0), 
			ISNULL(NWM.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWM.NOOFWATERHEATER_MULTIPOINT),0),
			ISNULL(NCH.NOOFCOOKER_HOB,0),ISNULL(NCH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCH.NOOFCOOKER_HOB),0),
			ISNULL(NCB.NOOFCOMBIBOILER,0),ISNULL(NCB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCB.NOOFCOMBIBOILER),0),
			ISNULL(NOB.NOOFOILBOILERS,0),ISNULL(NOB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NOB.NOOFOILBOILERS),0),
			ISNULL(NWH.NOOFWALLHEATER,0),ISNULL(NWH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWH.NOOFWALLHEATER),0), 
			(ISNULL(S.PROPERTY,0)*ISNULL(PCOUNT.PROPERTYCOUNT,0))+
			(ISNULL(NTB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NTB.NOOFTRADITIONALBOLIER),0))+	
			(ISNULL(NF.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NF.NOOFFIRE),0)) +
			(ISNULL(NWM.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWM.NOOFWATERHEATER_MULTIPOINT),0))+
			(ISNULL(NCH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCH.NOOFCOOKER_HOB),0))+  
	        (ISNULL(NCB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NCB.NOOFCOMBIBOILER),0))+   	
	        (ISNULL(NOB.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NOB.NOOFOILBOILERS),0))+
	        (ISNULL(NWH.COSTPERAPPLIANCE,0)*ISNULL(CONVERT(MONEY,NWH.NOOFWALLHEATER),0)),
			 ISNULL(OVERDUE.OVERDUEPROPERTIES,0), S.SCOPEID,S.CONTRACTNAME	
--ORDER BY S.SCOPEID,pd.patchid	
SET NOCOUNT OFF;
END














GO