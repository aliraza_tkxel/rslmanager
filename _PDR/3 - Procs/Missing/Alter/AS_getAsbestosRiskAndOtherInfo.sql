-- Stored Procedure

-- =============================================  
-- Author:  Abdullah Saeed  
-- Create date: July 4,2014  
-- Description: Return Asbestos Level, Element, Risk and Other Information  
-- =============================================  
ALTER  PROCEDURE [dbo].[AS_getAsbestosRiskAndOtherInfo]  
 -- Add the parameters for the stored procedure here  
 @asbestosID int  
 AS  
 BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

    -- Insert statements for procedure here  
 SELECT  P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID, ASBRISKLEVELID, ASBESTOSID, ASBRISKID, convert(varchar(10), DateAdded, 103) as  DateAdded,convert(varchar(10), DateRemoved, 103) as DateRemoved, Notes  
  from P_PROPERTY_ASBESTOS_RISKLEVEL   
 INNER JOIN P_PROPERTY_ASBESTOS_RISK ON P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID =P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID  

 where P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID =  @asbestosID  
 END  
GO