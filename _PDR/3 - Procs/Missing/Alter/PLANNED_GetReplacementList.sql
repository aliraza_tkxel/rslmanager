USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetReplacementList]    Script Date: 03/16/2015 17:47:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
--EXEC	@return_value = [dbo].[PLANNED_GetReplacementList]
--		@componentId = -1,
--		@replacementYear = N'1934',
--		@schemeId = -1,
--		@aptStatusId = -1,
--		@yearOperator = 0,
--		@isFullList = 0,
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Scheme',
--		@sortOrder = N'asc'		
-- Author:		<Ahmed Mehmood>
-- Create date: <10/22/2013>
-- Description:	<Get replacement list>
-- Web Page: ReplacementList.aspx
-- Updated By: Salman Nazir, Ticket#6469 , 06/06/2014
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetReplacementList]
( 
	-- Add the parameters for the stored procedure here
		@componentId int,
		@replacementYear varchar(10),
		@schemeId int,
		@aptStatusId int,
		@yearOperator int,
		@isFullList bit,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Scheme',
		@sortOrder varchar (5) = 'DESC'
)
AS
BEGIN
	DECLARE 
		@SelectClause varchar(2500),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(500),	
        @mainSelectQuery varchar(6000),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(8000),
        @InspectionArrangeId int ,
	@ArrangeId int ,
        @InProgressId int ,  
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int,
		@dataLimitation varchar(10)
		
				--Variable to filter report by fiscal year
		,@YearStartDate DateTime
		,@YearEndDate DateTime

		--Paging Formula
		SET @offset = 1 + (@pageNumber - 1) * @pageSize
		SET @limit = (@offset + @pageSize) - 1
	
	IF @replacementYear != '' AND @replacementYear != '-1' AND @replacementYear IS NOT NULL AND LEN(@replacementYear) = 4
	BEGIN
		SELECT
				@YearStartDate = CONVERT(DATETIME, @replacementYear + '0401')
				,@YearEndDate = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(INT, @replacementYear) + 1) + '0331 23:59:59')	
	END

--========================================================================================
-- Begin building SearchCriteria clause
-- These conditions will be added into where clause based on search criteria provided

SELECT
	@InProgressId = PLANNED_STATUS.statusid
FROM
	PLANNED_STATUS
WHERE
	PLANNED_STATUS.title = 'InProgress'

SET @searchCriteria = ' 1=1 '

SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P__PROPERTY.STATUS not in (Select STATUSID from P_STATUS Where P_STATUS.DESCRIPTION = ''Sold'' or P_STATUS.DESCRIPTION = ''Demolished'' or P_STATUS.DESCRIPTION = ''Transfer'' or P_STATUS.DESCRIPTION = ''Other Losses'')'
IF @componentId != -1 SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = ' + CONVERT(NVARCHAR(10), @componentId)


-- YEAR OPERATOR IS USED TO GET RECORDS HAVING REPLACEMENT DUE DATES IN "REPLACEMENT YEAR" , "BEFORE REPLACEMENT YEAR" OR "REPLACEMENT AND PREVIOUS YEAR"
-- -1 => "CURRENT AND PREVIOUS YEAR"
-- -2 => "PREVIOUS YEAR"
-- ELSE => EQUAL TO REPLACEMENT YEAR

	IF @yearOperator = '-1' 
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND CONVERT( DateTime, CONVERT( CHAR, PA_PROPERTY_ITEM_DATES.DueDate, 103 ), 103 ) <= ' + ' CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @YearEndDate, 103) ) ) + ''', 103 ) '
	ELSE IF @yearOperator = '-2' 
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND CONVERT( DateTime, CONVERT( CHAR, PA_PROPERTY_ITEM_DATES.DueDate, 103 ), 103 ) < ' + ' CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @YearStartDate, 103) ) ) + ''', 103 ) '
	ELSE 
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND CONVERT( DateTime, CONVERT( CHAR, PA_PROPERTY_ITEM_DATES.DueDate, 103 ), 103 ) BETWEEN  
								CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @YearStartDate, 103) ) ) + ''', 103 )  AND 
								CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @YearEndDate, 103) ) ) + ''', 103 ) '

IF @schemeId != -1 SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P__PROPERTY.SCHEMEID = ' + CONVERT(NVARCHAR(10), @schemeId)

IF @aptStatusId != -1 AND @aptStatusId != 0 SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PLANNED_STATUS.STATUSID = ' + CONVERT(NVARCHAR(10), @aptStatusId)
--Salman Nazir
--Add Filter on Status "To Be Scheduled" 
--Ticket# 6469
--Date 06/06/2014

IF @aptStatusId = 0 SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PLANNED_JOURNAL.JOURNALID is null'


-- End building SearchCriteria clause   
--========================================================================================

SET NOCOUNT ON;
--========================================================================================	        
-- Begin building SELECT clause
-- Insert statements for procedure here

SELECT
	@InspectionArrangeId = PLANNED_STATUS.statusid
FROM
	PLANNED_STATUS
WHERE
	PLANNED_STATUS.title = 'Inspection Arranged'


SELECT	@ArrangeId = PLANNED_STATUS.statusid 
FROM PLANNED_STATUS 
WHERE PLANNED_STATUS.title ='Arranged'


IF (@isFullList = 1) SET @dataLimitation = '' ELSE SET @dataLimitation = 'top (' + CONVERT(VARCHAR(10), @limit) + ')'

SET @selectClause = 'SELECT ' + @dataLimitation + '
		P_SCHEME.SCHEMENAME as Scheme
		,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address	
		,PLANNED_COMPONENT.COMPONENTNAME as ComponentName
		,YEAR(PA_PROPERTY_ITEM_DATES.LastDone) as ReplacedDate
		,PA_PROPERTY_ITEM_DATES.DueDate
		,YEAR(PA_PROPERTY_ITEM_DATES.DueDate) as ReplacementDueDate
		,YEAR(RecentSurvey.LastSurveyDate) as LastSurveyDate
		,CASE WHEN PLANNED_JOURNAL.JOURNALID IS NULL THEN
		''To be Scheduled'' ELSE
		 PLANNED_STATUS.TITLE END as AppointmentStatus	
		,ISNULL(CONVERT(VARCHAR(10)
		,CASE  
			WHEN PLANNED_STATUS.TITLE = ''Inspection Arranged'' THEN
				PLANNED_INSPECTION_APPOINTMENTS.APPOINTMENTDATE  
			WHEN PLANNED_STATUS.TITLE = ''Arranged'' THEN
				PLANNED_APPOINTMENTS.APPOINTMENTDATE
			ELSE
				NULL
		END, 103),''-'') as AppointmentDate
		,dbo.PLANNED_fnDecimalToCurrency(PLANNED_COMPONENT.LABOURCOST+PLANNED_COMPONENT.MATERIALCOST) as TotalCost
		
		-- For sorting purpose
		,P_SCHEME.SCHEMEID as SchemeId
		,PA_PROPERTY_ITEM_DATES.SID as SID
		,P__PROPERTY.HouseNumber as HouseNumber
		,P__PROPERTY.ADDRESS1 as PrimaryAddress
		,P__PROPERTY.POSTCODE as POSTCODE 
		
		,PA_PROPERTY_ITEM_DATES.LastDone as ReplacedDateSort
		,PA_PROPERTY_ITEM_DATES.DueDate as ReplacementDueDateSort
		,RecentSurvey.LastSurveyDate as LastSurveyDateSort
		,CONVERT(VARCHAR(10)
		,CASE  
			WHEN PLANNED_STATUS.TITLE = ''Inspection Arranged'' THEN
				PLANNED_INSPECTION_APPOINTMENTS.APPOINTMENTDATE  
			WHEN PLANNED_STATUS.TITLE = ''Arranged'' THEN
				PLANNED_APPOINTMENTS.APPOINTMENTDATE
			ELSE
				NULL
		END, 120) as AppointmentDateSort
		
		,P__PROPERTY.PROPERTYID as PropertyId
		,PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID as ComponentId
		,ISNULL(PLANNED_STATUS.STATUSID,-1) as StatusId
		,CASE  
			WHEN PLANNED_STATUS.TITLE = ''Inspection Arranged'' THEN
				ISNULL(PLANNED_INSPECTION_APPOINTMENTS.InspectionID,-1) 			
			ELSE
				-1
		 END as InspectionId		
		,PLANNED_COMPONENT.LABOURCOST+PLANNED_COMPONENT.MATERIALCOST as TotalCostSort		
		'

-- End building SELECT clause
--======================================================================================== 							


--========================================================================================    
-- Begin building FROM clause
SET @fromClause = CHAR(10) + 'FROM (	SELECT	PID.SID AS SID
					,PID.PROPERTYID AS PROPERTYID
					,PID.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
					,PID.DueDate as DueDate
					,PID.LastDone AS LastDone
			FROM	PA_PROPERTY_ITEM_DATES AS PID
					INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID.PLANNED_COMPONENTID = PCI.COMPONENTID
									) AS PA_PROPERTY_ITEM_DATES 
		INNER JOIN P__PROPERTY ON PA_PROPERTY_ITEM_DATES.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN P_SCHEME ON  P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
		INNER JOIN PLANNED_COMPONENT ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
		LEFT OUTER JOIN (	SELECT PROPERTYID as PropertyId,MAX(SurveyDate ) as LastSurveyDate
							FROM PS_SURVEY
							GROUP BY PROPERTYID ) RecentSurvey on PA_PROPERTY_ITEM_DATES.PROPERTYID = RecentSurvey.PropertyId 					
  LEFT OUTER JOIN PLANNED_JOURNAL ON PA_PROPERTY_ITEM_DATES.PROPERTYID = PLANNED_JOURNAL.PROPERTYID AND PLANNED_COMPONENT.COMPONENTID = dbo.PLANNED_JOURNAL.COMPONENTID  
		LEFT OUTER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId AND dbo.PLANNED_APPOINTMENTS.APPOINTMENTSTATUS NOT IN (''Cancelled'') 
		LEFT OUTER JOIN PLANNED_INSPECTION_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_INSPECTION_APPOINTMENTS.JournalId
		LEFT OUTER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID'
-- End building From clause
--======================================================================================== 														  



--========================================================================================    
-- Begin building OrderBy clause		

-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
IF (@sortColumn = 'Address') BEGIN
SET @sortColumn = CHAR(10) + ' cast(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)'

END

IF (@sortColumn = 'ReplacedDate') BEGIN
SET @sortColumn = CHAR(10) + 'ReplacedDateSort'
END

IF (@sortColumn = 'ReplacementDueDate') BEGIN
SET @sortColumn = CHAR(10) + 'ReplacementDueDateSort'
END

IF (@sortColumn = 'LastSurveyDate') BEGIN
SET @sortColumn = CHAR(10) + 'LastSurveyDateSort'
END

IF (@sortColumn = 'AppointmentDate') BEGIN
SET @sortColumn = CHAR(10) + 'AppointmentDateSort'
END

IF (@sortColumn = 'TotalCost') BEGIN
SET @sortColumn = CHAR(10) + 'TotalCostSort'
END

SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

-- End building OrderBy clause
--========================================================================================								

--========================================================================================
-- Begin building WHERE clause

-- This Where clause contains subquery to exclude already displayed records			  

SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria

-- End building WHERE clause
--========================================================================================

--========================================================================================
-- Begin building the main select Query

SET @mainSelectQuery = @selectClause + @fromClause + @whereClause + @orderClause

--========================================================================================
-- Begin building the row number query

SET @rowNumberQuery = '  SELECT *, row_number() over (order by ' + CHAR(10) + @sortColumn + CHAR(10) + @sortOrder + CHAR(10) + ') as row	
								FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'

-- End building the row number query
--========================================================================================

--========================================================================================
-- Begin building the final query 

SET @finalQuery = ' SELECT *
							FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result 
							WHERE
							Result.row between' + CHAR(10) + CONVERT(VARCHAR(10), @offset) + CHAR(10) + 'and' + CHAR(10) + CONVERT(VARCHAR(10), @limit)

-- End building the final query
--========================================================================================									

PRINT @finalQuery

--========================================================================================
-- Begin - Execute the Query 

IF (@isFullList = 1) BEGIN
--print(@mainSelectQuery)
EXEC (@mainSelectQuery)
END ELSE BEGIN
--print(@finalQuery)
EXEC (@finalQuery)
END


-- End - Execute the Query 
--========================================================================================	

-- End building the main select Query
--========================================================================================																																			


DECLARE @selectTotalAmount NVARCHAR(max)
SET @selectTotalAmount = 'SELECT dbo.PLANNED_fnDecimalToCurrency(SUM(PLANNED_COMPONENT.LABOURCOST+PLANNED_COMPONENT.MATERIALCOST)) as TotalCost ' + @fromClause + @whereClause

--PRINT(@mainSelectQuery)
EXEC (@selectTotalAmount)


DECLARE @selectTotalcount NVARCHAR(max)
SET @selectTotalcount = 'SELECT count(PLANNED_COMPONENT.COMPONENTID) as TotalCount' + @fromClause + @whereClause
--PRINT(@selectTotalcount)
EXEC (@selectTotalcount)

--========================================================================================							
END

