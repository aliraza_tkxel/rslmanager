SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


ALTER PROCEDURE [dbo].[FL_CO_GET_MONITORING_SEARCH_PANEL_DATA] 

/* ===========================================================================
 '   NAME:           FL_CO_GET_MONITORING_SEARCH_PANEL_DATA
 '   DATE CREATED:   30TH DECEMBER 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist Faults of monitoring using search criteria provided
 '   IN:             @locationId, @areaId, @elementId, @priorityId, @status,@userId,@teamId,
					 @patch,@scheme,@postcode,@due,@ORGID
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	    @locationId	int	= NULL,
		@areaId	    int	= NULL,		
		@elementId	int = NULL,
		@priorityId	int = NULL,
		@status		int = NULL,   
		@userId		int = NULL,
		@teamId		int = NULL,
	   	 @patch      int = NULL,
		@scheme		int = NULL,
	  	@postcode	nvarchar(50) = NULL,
		@due		nvarchar(100) = NULL,
		@orgId		int = NULL,
		@stageId	int = NULL,
		
	
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(200) = 'FaultLogID ',
		@sortOrder varchar (100) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000),
	        @UptoDateDiff int,
	        @StartDate varchar(100),
	        @ConvertedDueDate varchar(100)
	        
	SET @StartDate ='1900-01-01'			
	
	SET @UptoDateDiff = DATEDIFF(day, @StartDate, Convert(varchar,@due,120))
	
	SET @ConvertedDueDate = Convert(varchar(100),@due,105)
	
	
    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @status IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@status)) + ' AND'  
    IF @patch IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E_PATCH.PATCHID = '+ LTRIM(STR(@patch)) + ' AND'  
    IF @scheme IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P_SCHEME.SCHEMEID  = '+ LTRIM(STR(@scheme)) + ' AND'  
    IF @postcode IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY.POSTCODE LIKE ''%'+ CONVERT(varchar,@postCode)+ '%'' AND'  
    IF @due IS NOT NULL
	Begin
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							 'DATEDIFF(d,  '''+@StartDate+''', FL_FAULT_LOG.DueDate ) <= '+Convert(varchar,@UptoDateDiff)+' AND'
                             --'Convert(varchar(10),FL_FAULT_LOG.DueDate,103) <='''+ Convert(varchar,@due,103) +''' AND' 
       
	End
    IF @orgId IS NOT NULL
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'FL_FAULT_LOG.ORGID = '+ LTRIM(STR(@orgId))+ ' AND' 
    
    IF @userId IS NOT NULL       	   
		Begin 
			SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
								 'FL_CO_APPOINTMENT.OperativeID = '+ LTRIM(STR(@userId))+' AND'
			SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E__EMPLOYEE.EMPLOYEEID = '+ LTRIM(STR(@userId))+' AND'                                                                                        
	    END                                                   
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT Convert(varchar,FL_FAULT_LOG.DueDate,103),' +                      
						CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID AS FaultLogID, FL_FAULT_LOG.JobSheetNumber AS JSNumber,' +
						CHAR(10) + CHAR(9) + 'Convert(varchar, FL_FAULT_LOG.SubmitDate, 103) AS Logged,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY.PriorityName AS Priority, Convert(varchar, FL_FAULT_PRIORITY.ResponseTime) + '' '' + Case  FL_FAULT_PRIORITY.Days When 0 then ''Hour(s)'' when 1 then ''Day(s)'' end AS PriorityTime,' +
						CHAR(10) + CHAR(9) + 'Convert(varchar, FL_FAULT_LOG.DueDate, 103) AS Due, P__PROPERTY.ADDRESS1 AS Address,' +
						CHAR(10) + CHAR(9) + 'P__PROPERTY.HOUSENUMBER + '','' +  P__PROPERTY.ADDRESS1 + '','' +  ISNULL(P__PROPERTY.ADDRESS2,'''') + '','' + P__PROPERTY.TOWNCITY + '','' +    P__PROPERTY.POSTCODE + '', '' + P__PROPERTY.COUNTY AS COMPLETEADDRESS,' +
						CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE.StageName AS Stage,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT.Description,' +
						CHAR(10) + CHAR(9) + 'E__EMPLOYEE.FIRSTNAME +''  ''+ E__EMPLOYEE.LASTNAME As Operative,'+
						CHAR(10) + CHAR(9) + 'Case When DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()) < 0 then  ''''  When DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()) > 0 then Convert(varchar, ABS(DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()))) + ''  day(s)''  When DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()) = 0 then '''' end as OverDue '
											
                       
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM FL_LOCATION INNER JOIN '+
					  CHAR(10) + CHAR(10)+ 'FL_AREA ON FL_AREA.LocationID = FL_LOCATION.LocationID INNER JOIN ' +                      
					  CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_AREA.AreaID = FL_ELEMENT.AreaID INNER JOIN' +                      
                      CHAR(10) + CHAR(9) + 'FL_FAULT ON FL_ELEMENT.ElementID = FL_FAULT.ElementID INNER JOIN' +                      
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN'+                     
                      CHAR(10) + CHAR(9) + 'C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN'+                     
                         CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN'+ 
		      CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID LEFT JOIN'+
                      CHAR(10) + CHAR(9) + 'P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID INNER JOIN'+   
                      CHAR(10) + CHAR(9) + 'PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID INNER JOIN'+	
                      CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID INNER JOIN'+                      
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN'+                        
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID'
		
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FaultLOGID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FaultLOGID '
	
	SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	--SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    

    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ' +
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                                                   
                        
                        CHAR(10) + CHAR(10) + 'FL_FAULT_STATUS.Description IN (SELECT Description From FL_FAULT_STATUS WHERE Description IN (''Appointment To Be Arranged'',''Appointment Arranged'', ''Appointment Cancelled'', ''Works Completed'', ''Post Inspection'', ''Post Inspected'', ''Invoiced''))' + ' AND' +
                        
                        CHAR(10) + CHAR(9) + ' 1=1 '
                        
    -- End building WHERE clause
    --========================================================================================
        
	
Print (@SelectClause + @FromClause + @WhereClause+ @OrderClause)
EXEC (@SelectClause + @FromClause + @WhereClause +@OrderClause )

GO
