USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC PLANNED_GetAllSchemes
-- Author:		<Ahmed Mehmood>
-- Create date: <21/10/2013>
-- Description:	<Returns All Schemes>
-- Webpage: ReplacementList.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetAllSchemes] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN


SELECT	SCHEMEID as DevelopmentId, SCHEMENAME as SchemeName				
FROM	P_SCHEME 
ORDER BY SchemeName ASC

END