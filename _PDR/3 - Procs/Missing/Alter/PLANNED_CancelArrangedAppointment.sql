USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_CancelArrangedAppointment @pmo,@reason,@isCancelled
-- Author:		Ahmed Mehmood
-- Create date: <12/7/2013>
-- Last Modified: <12/7/2013>
-- Description:	<Description,cancel appointments associated with PMO >
-- Webpage : ViewArrangedAppointments.aspx

-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_CancelArrangedAppointment]
	@pmo int
	,@reason varchar(1000)
	,@appointmentStatus varchar(1000)
	,@isCancelled int = 0 out
AS
BEGIN

DECLARE @JournalTypeId INT = NULL
DECLARE @ComponentId INT = NULL


BEGIN TRANSACTION;
BEGIN TRY

BEGIN -- Region for insertion in PLANNED_CANCELLED_JOBS
	-- ========================================================
	--	INSERT INTO PLANNED_CANCELLED_JOBS
	-- ========================================================
	INSERT INTO PLANNED_CANCELLED_JOBS (AppointmentId,RecordedOn ,Notes)
	SELECT PLANNED_APPOINTMENTS.APPOINTMENTID as AppointmentId, GETDATE() as RecordedOn,@reason as Notes 
	FROM PLANNED_APPOINTMENTS
	WHERE PLANNED_APPOINTMENTS.JournalId = @pmo
	AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS <> 'Complete'

END -- Region for insertion in PLANNED_CANCELLED_JOBS
		
BEGIN -- Region for Update Jounal with 'Cancelled' Status and insert an entry in history.
	-- ========================================================
	--	UPDATE PLANNED_JOURNAL
	-- ========================================================
	
	DECLARE @StatusId INT
	SELECT @StatusId = STATUSID
	FROM PLANNED_STATUS WHERE PLANNED_STATUS.TITLE= 'Cancelled'
	
	UPDATE	PLANNED_JOURNAL
	SET		PLANNED_JOURNAL.STATUSID = @StatusId			
	WHERE	PLANNED_JOURNAL.JOURNALID = @pmo
	
	/* Get Journal Type Id and Component Id for condition and Adaptation type appointments/journals
	 * to make a design either to set appointment status back to 'To be Arranged' or not.
	 */
	SELECT @JournalTypeId = APPOINTMENTTYPEID
			, @ComponentId = COMPONENTID
	FROM PLANNED_JOURNAL J
	LEFT JOIN PLANNED_APPOINTMENT_TYPE JT ON J.APPOINTMENTTYPEID = JT.Planned_Appointment_TypeId
		AND JT.Planned_Appointment_Type IN ('Misc', 'Adaptation')	
	WHERE	J.JOURNALID = @pmo

	-- ========================================================
	--	INSERT INTO PLANNED_JOURNAL_HISTORY
	-- ========================================================
	
	INSERT INTO PLANNED_JOURNAL_HISTORY
           ([JOURNALID]
           ,[PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]
           ,[StatusHistoryId]
           ,[ActionHistoryId]
           ,[IsDocumentAttached])
           
	SELECT	PLANNED_JOURNAL.JOURNALID  AS JOURNALID
			,PLANNED_JOURNAL.PROPERTYID AS PROPERTYID
			,PLANNED_JOURNAL.COMPONENTID AS COMPONENTID
			,PLANNED_JOURNAL.STATUSID AS STATUSID
			,PLANNED_JOURNAL.ACTIONID AS ACTIONID
			,GETDATE() AS CREATIONDATE
			,PLANNED_JOURNAL.CREATEDBY AS CREATEDBY
			,@reason AS NOTES
			,0 AS ISLETTERATTACHED
			,NULL AS StatusHistoryId
			,NULL AS ActionHistoryId
			,0 AS IsDocumentAttached
	FROM PLANNED_JOURNAL
	WHERE PLANNED_JOURNAL.JournalId = @pmo
	
	-- Get Journal history Id to change in in planned appointment
	DECLARE @JournalHistoryId BIGINT = SCOPE_IDENTITY()
	
END -- Region for Update Jounal with 'Cancelled' Status and insert an entry in history.	

BEGIN -- Region for Update planned appopintment with 'Cancelled' Status and updated journal history id.
	
	-- ========================================================
	--	UPDATE INTO PLANNED_APPOINTMENTS
	-- ========================================================
	-- Update planned appointment status an entry will be inserted in histroy with defined triger.
	UPDATE PLANNED_APPOINTMENTS SET
		JOURNALHISTORYID = @JournalHistoryId,
		APPOINTMENTSTATUS = 'Cancelled'		
	WHERE JournalId = @pmo AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
	
End -- Region for Update planned appopintment with 'Cancelled' Status and updated journal history id.

IF (@ComponentId IS NOT NULL AND @JournalTypeId IS NULL)
BEGIN -- Region for Update Jounal with 'To be Arranged' Status and insert an entry in history.	
	
	-- ========================================================
	--	UPDATE PLANNED_JOURNAL
	-- ========================================================
		
	SELECT @StatusId = STATUSID
	FROM PLANNED_STATUS WHERE PLANNED_STATUS.TITLE = @appointmentStatus

	UPDATE	PLANNED_JOURNAL
	SET		PLANNED_JOURNAL.STATUSID = @StatusId
	WHERE	PLANNED_JOURNAL.JOURNALID = @pmo

	-- ========================================================
	--	INSERT INTO PLANNED_JOURNAL_HISTORY
	-- ========================================================
	
	DECLARE @StatusHistoryId INT
	SELECT @StatusHistoryId = MAX(StatusHistoryId) FROM PLANNED_StatusHistory WHERE STATUSID = @StatusId

	INSERT INTO PLANNED_JOURNAL_HISTORY
           ([JOURNALID]
           ,[PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY]           
           ,[ISLETTERATTACHED]
           ,[StatusHistoryId]
           ,[ActionHistoryId]
           ,[IsDocumentAttached])
           
	SELECT	PLANNED_JOURNAL.JOURNALID  AS JOURNALID
			,PLANNED_JOURNAL.PROPERTYID AS PROPERTYID
			,PLANNED_JOURNAL.COMPONENTID AS COMPONENTID
			,PLANNED_JOURNAL.STATUSID AS STATUSID
			,PLANNED_JOURNAL.ACTIONID AS ACTIONID
			,GETDATE() AS CREATIONDATE
			,PLANNED_JOURNAL.CREATEDBY AS CREATEDBY			
			,0 AS ISLETTERATTACHED
			,NULL AS StatusHistoryId
			,NULL AS ActionHistoryId
			,0 AS IsDocumentAttached
	FROM PLANNED_JOURNAL
	WHERE PLANNED_JOURNAL.JournalId = @pmo
	
END -- Region for Update Jounal with 'To be Arranged' Status and insert an entry in history.		
		
	END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isCancelled = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isCancelled = 1
 END

END