USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetNoEntryList]    Script Date: 02/03/2015 11:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
--EXEC	[dbo].[FL_GetNoEntryList]
--		@searchedText = N'fox',
--		@pageSize = 2,
--		@pageNumber = 1,
--		@sortColumn = N'Recorded',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <15/2/2013>
-- Description:	<Get list of No Entries>
-- Web Page: ReportsArea.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetNoEntryList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@searchedText varchar(8000),
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Recorded',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN			
			--SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+'''))'
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
		END 	
		IF(@schemeId != -1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.SCHEMEID = '+convert(varchar(10),@schemeId)
			END
		IF(@blockId != -1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.BLOCKID = '+convert(varchar(10),@blockId)
			END
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') 
		FL_FAULT_LOG.JobSheetNumber as JSN,
		CONVERT(nvarchar(50),FL_FAULT_NOENTRY.RecordedOn, 103) as Recorded,
		ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
		Case 
		When FL_FAULT_LOG.PROPERTYID <> '''' THEN
		ISNULL(P__PROPERTY.HouseNumber, '''') + '' ''
		+ ISNULL(P__PROPERTY.ADDRESS1, '''') + '', ''
		+ ISNULL(P__PROPERTY.ADDRESS2, '''') 
		WHEN FL_FAULT_LOG.BLOCKID > 0 THEN 
		ISNULL(P_BLOCK.ADDRESS1,'''')
		When FL_FAULT_LOG.SCHEMEID > 0 THEN
		ISNULL(P_SCHEME.SCHEMENAME,'''')
		END																			AS Address,
		FL_AREA.AreaName as Location, FL_FAULT.Description as Description,
		G_TRADE.Description as Trade, CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			CONVERT(nvarchar(50), FL_FAULT_PRIORITY.ResponseTime) + '' Day(s)'' ELSE
			CONVERT(nvarchar(50), FL_FAULT_PRIORITY.ResponseTime) + '' Hour(s)'' END as Response,
		CONVERT(NVARCHAR, COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.duration)) + '' hr'' 
			+ CASE WHEN COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.duration) > 1 THEN ''s'' ELSE '''' END AS Duration,
		P__PROPERTY.HouseNumber as HouseNumber,
		P__PROPERTY.ADDRESS1 as Address1,
		 FL_FAULT_NOENTRY.RecordedOn as RecordedOn,
		FL_FAULT.duration as Interval,
		FL_FAULT_LOG.FaultLogID as FaultLogID,
		FL_FAULT_NOENTRY.isNoEntryScheduled
		,Case 
	When FL_FAULT_LOG.PROPERTYID IS NULL THEN	''SbFault''	
	Else ''Fault''	End	AS AppointmentType	'
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +' FROM	FL_FAULT_NOENTRY
		JOIN FL_FAULT_LOG on FL_FAULT_NOENTRY.FaultLogId = FL_FAULT_LOG.FaultLogID
		JOIN FL_FAULT_TRADE on FL_FAULT_LOG.FaultTradeID = FL_FAULT_TRADE.FaultTradeId
		JOIN FL_FAULT on FL_FAULT_TRADE.FaultID = FL_FAULT.FaultID	
		JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID 
		LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID	'
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address1'		
		END
		
		IF(@sortColumn = 'Recorded')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'RecordedOn'		
		END
		
		IF(@sortColumn = 'Duration')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Interval'		
		END
		
		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
		END				
		
		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Scheme' 		
		END	
		
		IF(@sortColumn = 'Block')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Block' 		
		END	
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							

END



