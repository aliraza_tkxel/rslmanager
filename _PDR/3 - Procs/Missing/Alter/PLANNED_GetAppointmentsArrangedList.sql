-- Stored Procedure

-- =============================================    
-- Author:  <Author,Ahmed Mehmood>    
-- Create date: <Create Date,11/8/2013>    
-- Description: <Description,Update/Insert PLANNED_JOURNAL,PLANNED_APPOINTMENTS    
--Last modified Date:11/7/2013    
-- =============================================    

ALTER  PROCEDURE [dbo].[PLANNED_GetAppointmentsArrangedList]    
 -- Add the parameters for the stored procedure here    

  @schemeId INT = -1,    
  @searchText VARCHAR(200) = '',
  @componentId INT = -1,    
  @appointmentDate VARCHAR(200) = '',

 --Parameters which would help in sorting and paging    
  @pageSize int = 30,    
  @pageNumber int = 1,    
  @sortColumn varchar(500) = 'Address',    
  @sortOrder varchar (5) = 'DESC',    
  @totalCount int = 0 output     
AS    
BEGIN    
 DECLARE     

  @SelectClause varchar(3000),    
        @fromClause   varchar(2000),    
        @whereClause  varchar(1500),             
        @orderClause  varchar(1000),     
        @mainSelectQuery varchar(6000),            
        @rowNumberQuery varchar(7000),    
        @finalQuery varchar(8000),    
        -- used to add in conditions in WhereClause based on search criteria provided    
        @searchCriteria varchar(1500),    

        --variables for paging    
        @offset int,    
  @limit int    

  ,@appointmentArranged varchar(50)    

  SET @appointmentArranged = '''Arranged'''    

  --Paging Formula    
  SET @offset = 1+(@pageNumber-1) * @pageSize    
  SET @limit = (@offset + @pageSize)-1    

  --========================================================================================    
  -- Begin building SearchCriteria clause    
  -- These conditions will be added into where clause based on search criteria provided    

  SET @searchCriteria = ' 1=1 AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS <> ''Cancelled'' AND PLANNED_STATUS.TITLE='+ @appointmentArranged    

  IF(@searchText != '' OR @searchText != NULL)    
  BEGIN          
   SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( JOURNAL_PMO.PMO LIKE ''%' + @searchText + '%'''    
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PROPERTYADDRESS.Address LIKE ''%' + @searchText + '%'''    
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PLANNED_COMPONENT.COMPONENTNAME LIKE ''%' + @searchText + '%'''
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E__EMPLOYEE.FIRSTNAME LIKE ''%' + @searchText + '%'''
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E__EMPLOYEE.LASTNAME LIKE ''%' + @searchText + '%'''
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR APPOINTMENT_AID.JSN LIKE ''%' + @searchText + '%'') '    
  END     

  IF @schemeId != -1    
   SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P__PROPERTY.SCHEMEID = '+CONVERT(nvarchar(10), @schemeId)    

  IF NOT @componentId = -1    
   SET @SearchCriteria = @SearchCriteria + CHAR(10) +' AND PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' '    

  IF ( @appointmentDate != '' OR @appointmentDate != NULL ) 
	BEGIN
		SET @SearchCriteria = @SearchCriteria + CHAR(10) +' AND CONVERT( DATETIME, CONVERT( CHAR, PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103 ), 103 ) >= CONVERT(DATETIME, ''' + @appointmentDate + ''',103 ) '    
	END


  -- End building SearchCriteria clause       
  --========================================================================================    

  SET NOCOUNT ON;    
  --========================================================================================             
  -- Begin building SELECT clause    
  -- Insert statements for procedure here    

  SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')    

  JOURNAL_PMO.PMO as Ref    
  ,APPOINTMENT_AID.JSN as JSN    
  ,ISNULL(P_SCHEME.SCHEMENAME,'''') as Scheme    
  ,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address    
  ,P__PROPERTY.POSTCODE as Postcode    
  , ISNULL(PLANNED_APPOINTMENT_TYPE.Planned_Appointment_Type,''Planned'')  as AppointmentType    
  ,CASE isMiscAppointment WHEN 1    
   THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + ''(misc)'', ''Miscellaneous'')    
   ELSE ISNULL(PLANNED_COMPONENT.COMPONENTNAME, ''N/A'')    
  END  AS Component      
  ,E__EMPLOYEE.FIRSTNAME+'' ''+ E__EMPLOYEE.LASTNAME as Operative    
  ,ISNULL((CASE isMiscAppointment WHEN 1    
   THEN CONVERT(NVARCHAR,ISNULL(    
     NULLIF(PLANNED_MISC_TRADE.DURATION,0),         
      DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME),     
       CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME))))    
   ELSE     
    CONVERT(VARCHAR ,ISNULL(PLANNED_APPOINTMENTS.DURATION,''''))        
   END    
   +    
   CASE WHEN (COALESCE(PLANNED_APPOINTMENTS.DURATION,NULLIF(PLANNED_MISC_TRADE.DURATION,0),DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME),     
       CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME)))) > 1 THEN '' hours''    
       ELSE '' hour'' END),''N/A'') as Duration    
  , ISNULL(CONVERT(VARCHAR(3),datename(weekday,PLANNED_APPOINTMENTS.APPOINTMENTDATE)) +'' ''+    
  CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 106),''N/A'') as Appointment    
  ,PLANNED_STATUS.TITLE    
  ,PLANNED_STATUS.TITLE AS PLANNEDSTATUS

  -- Sorting purpose column    
  ,PLANNED_JOURNAL.JOURNALID as RefSort    
  ,PLANNED_APPOINTMENTS.APPOINTMENTID as JSNSort    
  ,P__PROPERTY.HouseNumber as HouseNumber    
  ,P__PROPERTY.ADDRESS1 as PrimaryAddress    
  ,PLANNED_APPOINTMENTS.APPOINTMENTDATE as AppointmentDateSort      
  ,PLANNED_APPOINTMENTS.AppointmentId as AppointmentId    
  ,P__PROPERTY.PropertyId    
  ,ISNULL(PLANNED_COMPONENT.ComponentId, -1) as ComponentId
  ,CASE isMiscAppointment WHEN 1    
   THEN ISNULL(    
     NULLIF(PLANNED_MISC_TRADE.DURATION,0),         
      DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME),     
       CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME)))    
   ELSE     
    ISNULL(PLANNED_APPOINTMENTS.DURATION,0)       
   END as DurationSort
   , ( ISNULL( COMPONENTTRADE.Description, IsNULL( MISCTRADE.Description, ''N/A'') ) ) as TradeTitle
  '    


  -- End building SELECT clause    
  --========================================================================================            


  --========================================================================================        
  -- Begin building FROM clause    
  SET @fromClause =   CHAR(10) +'      
  FROM PLANNED_JOURNAL     
  INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId     
  INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID     
  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID  = P_SCHEME.SCHEMEID       
  LEFT JOIN PLANNED_APPOINTMENT_TYPE ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = PLANNED_APPOINTMENT_TYPE.Planned_Appointment_TypeId     
  LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID     
  LEFT JOIN E__EMPLOYEE ON PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID     
  INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID    
  LEFT JOIN PLANNED_COMPONENT_TRADE ON  PLANNED_APPOINTMENTS.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID     
  LEFT JOIN PLANNED_MISC_TRADE ON PLANNED_APPOINTMENTS.APPOINTMENTID = PLANNED_MISC_TRADE.AppointmentId

  LEFT JOIN G_TRADE MISCTRADE ON PLANNED_MISC_TRADE.TradeId = MISCTRADE.TradeId
  LEFT JOIN G_TRADE COMPONENTTRADE ON PLANNED_COMPONENT_TRADE.TradeId = COMPONENTTRADE.TradeId

  INNER JOIN (SELECT PLANNED_JOURNAL.JOURNALID as JID , ''PMO''+CONVERT(VARCHAR,PLANNED_JOURNAL.JOURNALID) as PMO    
     FROM PLANNED_JOURNAL ) JOURNAL_PMO ON PLANNED_JOURNAL.JOURNALID = JOURNAL_PMO.JID     
  INNER JOIN (SELECT PLANNED_APPOINTMENTS.APPOINTMENTID as AID , ''JSN'' +RIGHT(''0000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),4) as JSN    
     FROM PLANNED_APPOINTMENTS ) as APPOINTMENT_AID ON PLANNED_APPOINTMENTS.APPOINTMENTID = APPOINTMENT_AID.AID     
  INNER JOIN (SELECT P__PROPERTY.PROPERTYID as PID,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address    
     FROM P__PROPERTY) AS PROPERTYADDRESS ON P__PROPERTY.PROPERTYID = PROPERTYADDRESS.PID    

  '    
  -- End building From clause    
  --========================================================================================                     


  --========================================================================================        
  -- Begin building OrderBy clause      

  -- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias    
  IF(@sortColumn = 'Address')    
  BEGIN    
   SET @sortColumn = CHAR(10)+ ' CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)'         
  END    

  IF(@sortColumn = 'Ref')    
  BEGIN    
   SET @sortColumn = CHAR(10)+ 'RefSort'         
  END    

  IF(@sortColumn = 'JSN')    
  BEGIN    
   SET @sortColumn = CHAR(10)+ 'JSNSort'         
  END    

  IF(@sortColumn = 'Appointment')    
  BEGIN    
   SET @sortColumn = CHAR(10)+ 'AppointmentDateSort'         
  END    

  IF(@sortColumn = 'Duration')    
  BEGIN    
   SET @sortColumn = CHAR(10)+ 'DurationSort'         
  END

  SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder    

  -- End building OrderBy clause    
  --========================================================================================            

  --========================================================================================    
  -- Begin building WHERE clause    

  -- This Where clause contains subquery to exclude already displayed records         

  SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria     

  -- End building WHERE clause    
  --========================================================================================    

  --========================================================================================    
  -- Begin building the main select Query    

  Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause     

  -- End building the main select Query    
  --========================================================================================                                       

  --========================================================================================    
  -- Begin building the row number query    

  Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row     
        FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'    

  -- End building the row number query    
  --========================================================================================    

  --========================================================================================    
  -- Begin building the final query     

  Set @finalQuery  =' SELECT *    
       FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result     
       WHERE    
       Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)        

  -- End building the final query    
  --========================================================================================             

  --========================================================================================    
  -- Begin - Execute the Query     
  print(@finalQuery)    
  EXEC (@finalQuery)                             
  -- End - Execute the Query     
  --========================================================================================             

  --========================================================================================    
  -- Begin building Count Query     

  Declare @selectCount nvarchar(4000),     
  @parameterDef NVARCHAR(500)    

  SET @parameterDef = '@totalCount int OUTPUT';    
  SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause    

  --print @selectCount    
  EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;    

  -- End building the Count Query    
  --========================================================================================     


END 
GO