SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








ALTER PROCEDURE [dbo].[FL_CO_GET_JOB_SHEET_DATA] 

/* ===========================================================================
 '   NAME:           FL_CO_GET_JOB_SHEET_DATA
 '   DATE CREATED:   30TH DECEMBER 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist job sheet faults based on search criteria provided
 '   IN:             @locationId, @areaId, @elementId, @priorityId, @status,@userId,@teamId,
					 @patch,@scheme,@postcode,@due,@ORGID
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	    @locationId	int	= NULL,
		@areaId	    int	= NULL,		
		@elementId	int = NULL,
		@priorityId	int = NULL,
		@status		int = NULL,   
		@userId		int = NULL,
		@teamId		int = NULL,
	    @patch      int = NULL,
		@scheme		int = NULL,
	  	@postcode	nvarchar(20) = NULL,
		@due		nvarchar(20) = NULL,
		@orgId		int = NULL,
		@jsNumber		int = NULL,
			
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int=50 ,
		@offSet   int=0 ,
		
		-- column name on which sorting is performed
		--@sortColumn varchar(50)='FL_FAULT_LOG.FaultLogID',
		--@sortColumn varchar(100) = 'FL_FAULT_LOG.DueDate',
		@sortColumn nvarchar(150) = 'FL_FAULT_LOG.FaultLogID',
		@sortOrder varchar (5)='DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @status IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@status)) + ' AND'  
    IF @patch IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E_PATCH.PATCHID = '+ LTRIM(STR(@patch)) + ' AND'  
    IF @scheme IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P_SCHEME.SCHEMEID  = '+ LTRIM(STR(@scheme)) + ' AND'  
    IF @postcode IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY.POSTCODE like ''%'+ LTRIM(RTRIM(@postcode)) + '%'' AND'  
    IF @due IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'Convert(varchar(10),FL_FAULT_LOG.DueDate,103) ='''+ Convert(varchar,@due,120) +''' AND' 
    IF @orgId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'FL_FAULT_LOG.ORGID = '+ LTRIM(STR(@orgId))+ ' AND' 

	 IF @jsNumber IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'FL_FAULT_LOG.FAULTLOGID = '+ LTRIM(STR(@jsNumber))+ ' AND' 							
    
    IF @userId IS NOT NULL       	   
	   SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E__EMPLOYEE.EMPLOYEEID = '+ LTRIM(STR(@userId))+' AND'                                                           
	                                                       
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
						CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, (@noOfRows)) +
						CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID AS FaultLogID,Convert(varchar, FL_FAULT_LOG.SubmitDate, 103) as SubmitDate,  Case FL_FAULT_LOG.IsSelected WHEN 0 THEN 0 WHEN -1 THEN 0 ELSE 1 END AS IsSelected,  FL_FAULT_LOG.JobSheetNumber AS JSNumber,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.DueDate AS Due, P_SCHEME.SCHEMENAME AS Shceme, P__PROPERTY.POSTCODE AS PostCode, P__PROPERTY.HOUSENUMBER + '','' +  P__PROPERTY.ADDRESS1 + '','' +  ISNULL(P__PROPERTY.ADDRESS2,'''') + '','' + P__PROPERTY.TOWNCITY + '','' +    P__PROPERTY.POSTCODE + '', '' + P__PROPERTY.COUNTY AS COMPLETEADDRESS, ' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.Description AS Status, E_PATCH.LOCATION as Patch,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY.PriorityName AS Priority,Convert(varchar, FL_FAULT_PRIORITY.ResponseTime) + '' '' + Case  FL_FAULT_PRIORITY.Days When 0 then ''Hour(s)'' when 1 then ''Day(s)'' end AS PriorityTime ,' +
						CHAR(10) + CHAR(9) + 'FL_AREA.AreaName AS Area, FL_ELEMENT.ElementName AS Element, FL_FAULT.Description AS Description,' +
						CHAR(10) + CHAR(9) + 'Case  FL_FAULT_PRIORITY.Days'+
						CHAR(10) +CHAR(9) +' When 0 then ''Hour(s)'' when 1 then ''Day(s)''  end as Type ' 
                       
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM FL_LOCATION INNER JOIN '+
	              CHAR(10) + CHAR(10)+ 'FL_AREA ON FL_AREA.LocationID = FL_LOCATION.LocationID INNER JOIN ' +                      
                      CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_AREA.AreaID = FL_ELEMENT.AreaID INNER JOIN' +                      
                      CHAR(10) + CHAR(9) + 'FL_FAULT ON FL_ELEMENT.ElementID = FL_FAULT.ElementID INNER JOIN' +                      
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN'+                     
                      CHAR(10) + CHAR(9) + 'C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN'+                     
                      CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN'+ 
	        CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL  INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID LEFT JOIN'+
                      CHAR(10) + CHAR(9) + 'P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID INNER JOIN'+   
                      CHAR(10) + CHAR(9) + 'PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID INNER JOIN'+	
                      CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID'
                      
   IF @userId IS NOT NULL
       SET @FromClause = @FromClause + CHAR(10) + CHAR(9) +                       
						'INNER JOIN E__EMPLOYEE ON FL_FAULT_LOG.ORGID = E__EMPLOYEE.ORGID'
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.FaultLogID'       
	SET @sortColumn = @sortColumn
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE FL_FAULT_LOG.FaultLogID NOT IN' +
                        
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) + ' ' + 'FL_FAULT_LOG.FaultLogID' + 
                        CHAR(10) + CHAR(9) + @FromClause +                         
                        CHAR(10) + CHAR(9) + 'WHERE 1=1 AND'+ @SearchCriteria +                         
                        CHAR(10) + CHAR(9) + '1 = 1 AND FL_FAULT_STATUS.DESCRIPTION IN (''Assigned To Contractor'', ''Appointment To Be Arranged'',''Appointment Arranged'',''Appointment Cancelled'', ''Works Completed'', ''Post Inspection'', ''Post Inspected'',''Invoiced'') ' + CHAR(10) + CHAR(9) + @OrderClause+' ) AND' + 
                                                
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                                                                               
                        CHAR(10) + CHAR(10) + 'FL_FAULT_STATUS.DESCRIPTION IN (''Assigned To Contractor'',''Appointment To Be Arranged'', ''Appointment Arranged'',''Appointment Cancelled'', ''Works Completed'', ''Post Inspection'', ''Post Inspected'',''Invoiced'')  AND' +                        
                        CHAR(10) + CHAR(9) + ' 1=1 '
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)

EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)

--PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)

GO
