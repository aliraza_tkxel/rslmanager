
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




ALTER PROCEDURE [dbo].[AM_SP_GetCaseListCount] 
		@postCode varchar(50)='',
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle  varchar(100),
        @surname varchar(50), 
		@IsNoticeExpiryCheck	bit,
        @overdue varchar(50)=''
AS
BEGIN
	

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)
Declare @missedPaymentQuery varchar(6000)

--	IF(@caseOwnedBy = -1 )
--BEGIN

--	IF(@regionId = -1 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = P_DEVELOPMENT.PATCHID'
--	END
--	ELSE IF(@regionId > 0 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
--	END
--	ELSE IF(@regionId > 0 and @suburbId > 0)
--	BEGIN
--		SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId )+ ') ' 
--	END
--    ELSE IF(@regionId = -1 and @suburbId > 0)
--    BEGIN
--         	SET @RegionSuburbClause = 'P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) 
--    END

--END
--ELSE 
--BEGIN

--		IF(@regionId = -1 and @suburbId = -1)
--		BEGIN
--			SET @RegionSuburbClause = '(P_DEVELOPMENT.DEVELOPMENTID IN (SELECT DevelopmentId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true'') 
--											OR P_DEVELOPMENT.PATCHID IN (SELECT PatchId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
--		END
--		ELSE IF(@regionId > 0 and @suburbId = -1)
--		BEGIN
--			SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +'
--									   AND (P_DEVELOPMENT.PATCHID IN (SELECT PatchId FROM AM_ResourcePatchDevelopment 
--																	 WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ '
--																	 AND IsActive=''true''))' 
--		END
--		ELSE IF(@regionId > 0 and @suburbId > 0)
--		BEGIN
--			SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' 
--									  AND (P_DEVELOPMENT.PATCHID IN (SELECT PatchId FROM AM_ResourcePatchDevelopment 
--																	 WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ '
--																	 AND IsActive=''true'')) 	
--									  AND (P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') 
--									  AND (P_DEVELOPMENT.DEVELOPMENTID IN (SELECT DevelopmentId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true''))' 	 
									  
--	  END
--	  ELSE IF(@regionId = -1 and @suburbId > 0)
--      BEGIN
--         	SET @RegionSuburbClause = 'P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) +'
--         							  AND (P_DEVELOPMENT.DEVELOPMENTID IN (SELECT DevelopmentId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true''))' 	
--      END
--END
IF(@caseOwnedBy = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy)+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)+ ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy)+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
END

if(@IsNoticeExpiryCheck = 1)
BEGIN
		
SET @query=
		  'SELECT COUNT(*) as recordCount FROM( SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
					  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
			Where 1=1
						  AND  ' + @RegionSuburbClause + ' AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END
						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end + ''%'' ) 
						  AND AM_Case.IsActive = 1 
						  AND AM_STATUS.Title=''NISP''
						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN(select CaseId
												FROM AM_CASE
												WHERE NoticeExpiryDate IS NOT NULL 
														AND IsActive = ''True'' 
														AND dbo.AM_FN_Check_Case_Notice_Expiry_Date(AM_Case.NoticeExpiryDate) = ''True'')
			GROUP BY AM_Case.TenancyId) as TEMP'

	END
    ELSE IF(@overdue='Actions')
    BEGIN
    
 --   if @caseOwnedBy = -1
	--	BEGIN
	--		SET @missedPaymentQuery = 'select distinct AM_MissedPayments.TenantId
	--									FROM AM_MissedPayments 
	--									INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
	--									WHERE IsActive=''true'''
	--	END
	--ELSE
	--	BEGIN
	--		SET @missedPaymentQuery = 'select distinct AM_MissedPayments.TenantId
	--							FROM AM_MissedPayments 
	--							INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
	--							WHERE AM_Case.CaseOfficer= ' + convert(varchar(10), @caseOwnedBy)+ ' AND IsActive=''true'''
	--	END															
 
    	SET @query=	' SELECT distinct  COUNT(*)tenancyid
			  		  FROM  AM_FindOverDueActionCase  
			  
			  				INNER JOIN C_TENANCY on  AM_FindOverDueActionCase .TenancyId = C_TENANCY.TENANCYID 
			  				INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
			  				LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
							WHERE 1=1 AND  ' + @RegionSuburbClause + ' '
		  --'SELECT COUNT(*) as recordCount FROM( SELECT COUNT(*) as recordCount

		  --FROM  AM_Action INNER JOIN
				--	  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
				--	  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
				--	  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
				--	  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
				--	  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID INNER JOIN
				--	  P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID
    --                  INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
                      
				--	Where ( AM_Case.TenancyId IN(' + @missedPaymentQuery + ') OR
				--		  (' + @RegionSuburbClause + ' 
				--		  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end + ''%'' ) 
				--		  AND AM_Case.IsActive = 1 
				--		  --AND AM_CASE.IsPaymentPlan<>1
				--		  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
				--		  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
				--		  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
				--									FROM AM_CaseHistory INNER JOIN
				--										 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
				--										 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
				--										 AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
				--				                         E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
				--				                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
				--				                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
				--				                         AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

			 --                                                    WHERE AM_CaseHistory.IsActive = 1 
				--                                                    	--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = ''true'' 
				--                                                    	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, ''Days'', AM_CaseHistory.ActionReviewDate ) = ''true'' 
		  --                                                  	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId))
				--           ) --end of query brace that enclose all AND conditions
							 
				--	) --end of query brace that enclose all AND conditions and missed payment query
				--				GROUP BY AM_Case.TenancyId) as TEMP'

    
    
    END
    
	ELSE IF(@overdue='Stages')
    BEGIN
    SET @query=
		  'SELECT COUNT(*) as recordCount FROM (SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
					  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
                      
					Where 1=1
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' END + ''%'' ) 
						  AND AM_Case.IsActive = 1 
						  AND AM_CASE.IsPaymentPlan<>1
						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
													FROM AM_CaseHistory INNER JOIN
														 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
														 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
														 AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								                         E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								                         AM_LookupCode ON AM_Status.NextStatusAlertFrequencyLookupCodeId = AM_LookupCode.LookupCodeId

			                                                     WHERE AM_CaseHistory.IsActive = 1 
				                                                    	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Status.NextStatusAlert, AM_LookupCode.CodeName, AM_CaseHistory.StatusReview ) = ''true'' 
				                                                    	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId))
                              GROUP BY AM_Case.TenancyId) as TEMP'
    END
	
	ELSE
	BEGIN
SET @query=
		  'SELECT COUNT(*) as recordCount FROM(
			SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
					  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
			Where 1=1
						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)	
						  AND  ' + @RegionSuburbClause + ' AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END
						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' END + ''%'' ) 
						  AND AM_Case.IsActive = 1 
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
			GROUP BY AM_Case.TenancyId) as TEMP'

	END	  
PRINT(@query);
EXEC(@query);

END




















GO
