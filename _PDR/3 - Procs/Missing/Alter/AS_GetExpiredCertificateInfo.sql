USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetExpiredCertificateInfo]    Script Date: 12/14/2012 12:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------
-- FILTERS -------------
------------------------
-- 0	      EXPIRED 
-- 1	<=	  1 WEEK
-- 2	1-4   WEEKS
-- 3	4-8   WEEKS								     
-- 4	8-12  WEEKS
-- 5	12-16 WEEKS
-- 6	16-52 WEEKS
-- 7	>  52 WEEKS
-- 8		  NO ISSUE DATE
-- =============================================
  --EXEC  AS_GetExpiredCertificateInfo
		--@fuelType = 1,
		--@propertyType = -1,
		--@patch = -1,
		--@scheme = -1,
		--@stage = -1,
		--@UPRN = '',
		--@pageSize = 30,
		--@pageNumber = 1,
		--@sortColumn  = 'AS_JOURNAL.JOURNALID',
		--@sortOrder  = 'DESC',
		--@totalCount =0 output
		
-- Author:		<Aqib Javed>
-- Create date: <30/11/2012>
-- Description:	<The stored procedure shall provide the detail information for Certificate Expiry Report >
-- WebPage : CertificateExpiry.aspx
-- Parameters : 
--@fuelType	INT,
--@propertyType INT,
--@patch INT,
--@scheme INT,
--@stage INT,
--@UPRN NVARCHAR(50),
--@pageSize int = 30,
--@pageNumber int = 1,
--@sortColumn varchar(50) = 'AS_JOURNAL.JOURNALID',
--@sortOrder varchar (5) = 'DESC',
--@totalCount int=0 output
			
-- =============================================
ALTER PROCEDURE  [dbo].[AS_GetExpiredCertificateInfo]
@fuelType	INT,
@propertyType INT,
@patch INT,
@scheme INT ,
@stage INT,
@UPRN NVARCHAR(50),
		--Parameters which would help in sorting and paging
@pageSize int = 30,
@pageNumber int = 1,
@sortColumn varchar(50) = 'EXPIRYDATE',
@sortOrder varchar (5) = 'ASC',
@totalCount int=0 output
AS
BEGIN
DECLARE 
        @SelectClause varchar(2400),
        @fromClause   varchar(2000),
        @searchCriteria varchar(2000),
        @orderClause  varchar(100),
        @mainSelectQuery varchar(5000),        
        @rowNumberQuery varchar(5500),
        @finalQuery varchar(6000),	        
	    --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
	-- Begin building SELECT clause
    SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
	LGSR.CP12NUMBER,P.PROPERTYID,
	DATEADD(YEAR,1,LGSR.ISSUEDATE)AS EXPIRYDATE,
	PT.DESCRIPTION AS PTYPE,AT.DESCRIPTION AS ATYPE,
	ISNULL(P.FLATNUMBER,'''') +'' ''+ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'''') +'' ''+ ISNULL(P.ADDRESS3,'''')  AS ADDRESS,
	Case 
	 WHEN ST.StatusId=1 then ''Appointment to be arranged (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.PropertyId = P.PROPERTYID AND AS_JournalHistory.StatusId=1))+'')''
	 WHEN ST.StatusId=2 then ''Appointment (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.PropertyId = P.PROPERTYID AND AS_JournalHistory.StatusId=2))+'')''
	 WHEN ST.StatusId=3 then ''No Entry (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.PropertyId = P.PROPERTYID AND AS_JournalHistory.StatusId=3))+'')''
	 WHEN ST.StatusId=4 then ''Legal Proceedings (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.PropertyId = P.PROPERTYID AND AS_JournalHistory.StatusId=4))+'')''
	else ST.Title end AS Stage'
	


  -- End building SELECT clause
--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause= ' FROM dbo.P__PROPERTY P
	INNER JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) LGSR ON LGSR.PROPERTYID=P.PROPERTYID
	LEFT JOIN P_SCHEME PD ON PD.SCHEMEID=P.SCHEMEID  
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID=P.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID=P.ASSETTYPE
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=P.STATUS '
    -- End building From clause
	--======================================================================================== 														  
	-- Begin building Search clause
	SET @searchCriteria = 'Where P.FUELTYPE ='+CONVERT(varchar, (@fuelType))	
   
    If (@propertyType<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.PROPERTYTYPE ='+CONVERT(varchar, (@propertyType)) 
    End 
      If (@patch<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.PATCH ='+CONVERT(varchar, (@patch)) 
    End
      If (@scheme<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND PD.SCHEMEID ='+CONVERT(varchar, (@scheme)) 
    End
    If (@stage<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.STATUS ='+CONVERT(varchar, (@stage)) 
    End
    If (@UPRN<>'') 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.PROPERTYID='''+@UPRN+''''
    End
--	P.FUELTYPE = COALESCE(@FUELID, P.FUELTYPE)
--		AND PD.DEVELOPMENTID = COALESCE(@DEVELOPMENTID, PD.DEVELOPMENTID)
--		AND PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID)
--		AND P.PROPERTYID=COALESCE(@UPRN, P.PROPERTYID)
--ORDER BY EXPIRYDATE	
  -- End building Search clause`
	--======================================================================================== 														  
	-- Begin building Order clause
	SET @orderClause= ' ORDER BY EXPIRYDATE ASC'
	-- End building Order clause
	--======================================================================================== 	
--print @selectClause 
--print @fromClause
--print @searchCriteria 
--print @orderClause
	-- Begin building the main select Query
		
	Set @mainSelectQuery = @selectClause + @fromClause + @searchCriteria + @orderClause 
	
	--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		 
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		--print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(3000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @searchCriteria
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		--print @selectCount		
		-- End building the Count Query
		--========================================================================================							

END
