USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetItemDetail]    Script Date: 02/16/2015 20:57:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Author,Ali Raza>          
-- Create date: <Create Date,10/22/2013>          
-- Description: <Description,Get the detail of tree leaf node>          
--EXEC [dbo].[AS_GetItemDetail]          
--  @itemId = 1,          
--  @PropertyId = N'BHA0000987'          
-- =============================================          
ALTER PROCEDURE [dbo].[AS_GetItemDetail]          
 -- Add the parameters for the stored procedure here          
 --@areaId int,            
 @itemId int,          
 @propertyId nvarchar(20)            
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
--=================================================================          
  --get Parameters          
 select ItemParamID,PA_ITEM.ItemID,PA_PARAMETER.ParameterID,ParameterName,DataType,ControlType,IsDate,ParameterSorder,      
 (Select Top 1 convert(varchar(10),UPDATEDON,103 )as UPDATEDON  FROM PA_PROPERTY_ATTRIBUTES      
INNER JOIN PA_ITEM_PARAMETER on PA_ITEM_PARAMETER.ItemParamID = PA_PROPERTY_ATTRIBUTES.ItemParamId       
WHERE PROPERTYID =@propertyId AND ItemId = @itemId order by UPDATEDON DESC ) as LastInspected ,    
PA_PARAMETER.ShowInApp as  ShowInApp    
  from PA_ITEM_PARAMETER           
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID           
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId           
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID           
 where PA_ITEM.ItemID=@itemId and PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1         
 ORDER BY ParameterSorder  --AND PA_AREA.AreaID =@areaId          
           
 --=================================================================          
  ---get parameter values          
  --=================================================================          
 SELECT ValueID, ParameterID, ValueDetail, Sorder         
 FROM PA_PARAMETER_VALUE where ParameterID IN (         
 select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER           
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID           
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId           
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID           
 where PA_ITEM.ItemID=@itemId and PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 ) and PA_PARAMETER_VALUE.IsActive = 1          
 Order BY SOrder --AND PA_AREA.AreaID =@areaId)           
           
 --=================================================================          
 ---get pre inserted values           
 --=================================================================          
SELECT PROPERTYID,ATTRIBUTEID,ITEMPARAMID,PARAMETERVALUE,VALUEID,UPDATEDON,UPDATEDBY ,IsCheckBoxSelected FROM PA_PROPERTY_ATTRIBUTES          
  WHERE PROPERTYID=  @propertyId AND ITEMPARAMID IN  ( select ItemParamID from PA_ITEM_PARAMETER           
 INNER JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID           
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId           
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID           
where PA_ITEM.ItemID=@itemId and PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 ) --AND PA_AREA.AreaID =@areaId)            
           
 --=================================================================          
  ---get Last Replaced Dates          
  --=================================================================          
  SELECT [SID],PROPERTYID,ItemId,LastDone,DueDate,PA_PARAMETER.ParameterId,ParameterName,PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID as ComponentId   from PA_PROPERTY_ITEM_DATES          
   left JOIN PA_PARAMETER On PA_PARAMETER.ParameterID = PA_PROPERTY_ITEM_DATES.ParameterId          
   where PROPERTYID=@propertyId AND ItemId =@itemId       
       
  --=================================================================          
  ---get Maintenance,Servicing and testing         
SELECT 
MSATId,PDR_MSAT.ItemId,PDR_MSAT.CycleTypeId, IsRequired, convert(varchar(20),LastDate,103)AS LastDate,Cycle,convert(varchar(20),NextDate,103)AS NextDate,AnnualApportionment,CycleType, PDR_MSAT.MSATTypeId,  MSATTypeName

from PDR_MSAT
left JOIN PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId And PDR_CycleType.IsActive = 1
INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId and PDR_MSATType.IsActive = 1  
INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId
INNER JOIN PA_ITEM ON PA_ITEM.ItemID= PDR_MSAT.ItemId
where PDR_MSAT.PROPERTYID=@propertyId AND PDR_MSAT.ItemId =@itemId   
           
 --=================================================================          
 ---Get planned_component mapping      
 SELECT PLANNED_COMPONENT_ITEM.COMPONENTID, PLANNED_COMPONENT_ITEM.ITEMID,PLANNED_COMPONENT_ITEM.PARAMETERID,PLANNED_COMPONENT_ITEM.VALUEID,      
CASE WHEN PLANNED_COMPONENT.FREQUENCY  = 'yrs' THEN      
   convert(INT,PLANNED_COMPONENT.CYCLE )* 12 ELSE      
   convert(INT,PLANNED_COMPONENT.CYCLE ) END AS CYCLE,      
   CASE WHEN PLANNED_COMPONENT.FREQUENCY  = 'yrs' THEN      
   convert(varchar(10),PLANNED_COMPONENT.CYCLE )+' Years'  ELSE      
   convert(varchar(10),PLANNED_COMPONENT.CYCLE )+' Months' END AS LIFECYCLE,      
   PLANNED_COMPONENT.ISACCOUNTING,       
PLANNED_COMPONENT_ITEM.SubParameter,PLANNED_COMPONENT_ITEM.SubValue      
   FROM PLANNED_COMPONENT_ITEM       
INNER JOIN PLANNED_COMPONENT ON PLANNED_COMPONENT.COMPONENTID = PLANNED_COMPONENT_ITEM.COMPONENTID       
INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PLANNED_COMPONENT_ITEM.ITEMID       
WHERE PA_ITEM.ItemID =@itemId AND PLANNED_COMPONENT_ITEM.isActive=1      
       
 --====================================================================
-- Get Property CP12 certificate info in case of heating item.
--====================================================================
-- Get Item Name by PropertyId
DECLARE @ItemName NVARCHAR(50)
SELECT @ItemName = I.ItemName FROM PA_ITEM I
WHERE I.ItemID = @itemId

-- Get Property CP12 Information
SELECT ISNULL(CP12NUMBER,'N/A') [CP12 Number], ISNULL(CONVERT(NVARCHAR,ISSUEDATE,103),'N/A') [CP12 Issued], ISNULL(CONVERT(NVARCHAR, DATEADD(YEAR, 1, ISSUEDATE),103),'N/A') [CP12 Renewal]
FROM P_LGSR
WHERE PROPERTYID = @propertyId
AND @ItemName = 'Heating' -- Get CP12 Info only in case of Heating Item
             
END 