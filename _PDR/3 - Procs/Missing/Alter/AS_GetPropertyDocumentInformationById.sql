-- Stored Procedure

--=============================================
-- EXEC AS_GetPropertyDocumentInformationById
-- @documentId = 4
-- Author:		<Aamir Waheed>
-- Create date: <14-Dec-2013>
-- Description:	<Delete Property Document By Document Id>
-- WebPage: PropertyRecord.aspx > Document Tab
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetPropertyDocumentInformationById]
	@documentId int	
AS
BEGIN

	-- Get Document Complete Information to show document on client side.
	SELECT	DocumentId, PropertyId, DocumentName, DocumentPath, DocumentType, Keywords, CreatedDate, DocumentSize, DocumentFormat 
	FROM	P_Documents
	WHERE DocumentId = @documentId

END

GO