SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Zunair Minhas
-- Create date: <Create Date,31 March 2010,>
-- Description:	<Description,,>
-- =============================================

 ALTER PROCEDURE [dbo].[AM_SP_GetOverDueList]
		
		@postCode varchar(50)='',
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle	varchar(100),
		@skipIndex	int = 0,
		@pageSize	int = 10,
        @surname varchar(50),
		@sortBy     varchar(100),
        @sortDirection varchar(10)
		
        
        AS
BEGIN

declare @orderbyClause varchar(200)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @WhereClause	varchar(5000)
declare @RegionSuburbClause varchar(8000)
IF(@caseOwnedBy = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy  )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)+ ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy  )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy  )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
END


	SET @orderbyClause = ' ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
							

      SET @query = 'SELECT TOP('+convert(varchar(10),@pageSize)+') AM_FindOverDueActionCase.tenancyid,CustomerId,CustomerName,CustomerName2,CustomerAddress,RentBalance,OwedToBha as OwedToBHA,
					ActionTitle,IsSuppressed,PaymentPlan,SuppressedDate,TotalRent,ActionReviewDate,JointTenancyCount,CaseId, StatusTitle,EstimatedHBDue
	  				FROM AM_FindOverDueActionCase  
	  
	  				INNER JOIN C_TENANCY on  AM_FindOverDueActionCase .TenancyId = C_TENANCY.TENANCYID 
	  				INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
	  				LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
	  				WHERE 1=1 AND  ' + @RegionSuburbClause + ' 
	  				AND AM_FindOverDueActionCase.LASTNAME LIKE '''' + CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN AM_FindOverDueActionCase.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END  + ''%''
	  				AND AM_FindOverDueActionCase.TenancyId NOT IN ('	
	  				
       SET @subquery = 'SELECT TOP('+convert(varchar(10),@skipIndex)+')AM_FindOverDueActionCase.tenancyid 
							FROM AM_FindOverDueActionCase			  
			  				INNER JOIN C_TENANCY on  AM_FindOverDueActionCase.TenancyId = C_TENANCY.TENANCYID 
			  				INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
			  				LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID
			  				WHERE 1=1 AND  ' + @RegionSuburbClause + @orderbyClause+ ')'		  				
			  				

				
				
print @query 
print @subQuery 
print @orderbyClause
exec(@query + @subQuery + @orderbyClause);	
							
							




end


GO
