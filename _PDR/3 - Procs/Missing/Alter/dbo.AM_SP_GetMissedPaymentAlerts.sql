
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[AM_SP_GetMissedPaymentAlerts]
		    @postCode varchar(15),
			@caseOwnedById int=0,
            @regionId	int = 0,
			@suburbId	int = 0
			
AS
BEGIN

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID=PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId )+ ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) 
	END

END
ELSE 
BEGIN

		IF(@regionId = -1 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') 
										OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'
			
		END
		ELSE IF(@regionId > 0 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) + ' 
									   AND (PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
																	  FROM AM_ResourcePatchDevelopment 
																	  WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ '
																	    AND IsActive=''true''))'
		END
		ELSE IF(@regionId > 0 and @suburbId > 0)
		BEGIN
			SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' 
								   AND (PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId 
																	FROM AM_ResourcePatchDevelopment 
																	WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ '
																	AND IsActive=''true''))	
								   AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') 
								   AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
																	FROM AM_ResourcePatchDevelopment 
																	WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' 
																	AND IsActive=''true'')) ' 
		END
        ELSE IF(@regionId = -1 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause ='P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) +'
         							  AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
																	FROM AM_ResourcePatchDevelopment 
																	WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' 
																	AND IsActive=''true'')) '
	END
END

SET @query = 
			'SELECT COUNT(Distinct TenantId) as recordCount		
			FROM         AM_MissedPayments 
			
                  INNER JOIN AM_PaymentPlan ON AM_MissedPayments.TenantId = AM_PaymentPlan.TennantId
                  INNER JOIN AM_Case ON AM_PaymentPlan.PaymentPlanId=AM_Case.PaymentPlanId
                  INNER JOIN C_TENANCY ON AM_MissedPayments.TenantId = C_TENANCY.TENANCYID
				  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
				  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 				  
                  INNER JOIN C_ADDRESS ON AM_MissedPayments.customerId=C_ADDRESS.CUSTOMERID
			WHERE '+ @RegionSuburbClause+' 
					AND AM_Case.IsActive=''true''
					AND ISNULL(AM_PaymentPlan.IsActive,1)=1
					AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END '
							   
			--GROUP By	AM_MissedPayments.TenantId) as Temp

--'
PRINT(@query);
EXEC(@query);






END

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--				@postCode varchar(50)='',
--				@assignedToId	int = 0,
--				@regionId	int = 0,
--				@suburbId	int = 0,
--				@allAssignedFlag	bit,
--				@allRegionFlag	bit,
--				@allSuburbFlag	bit,
				
--				@missedCheck varchar(15)=''
--AS
--BEGIN
	
--declare @RegionSuburbClause varchar(8000)
--declare @query	varchar(8000)

--IF(@assignedToId = -1 )
--BEGIN

--	IF(@regionId = -1 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
--	END
--	ELSE IF(@regionId > 0 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
--	END
--	ELSE IF(@regionId > 0 and @suburbId > 0)
--	BEGIN
--		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
--	END
--     ELSE IF(@regionId = -1 and @suburbId > 0)
--    BEGIN
--         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
--    END

--END
--ELSE 
--BEGIN

--IF(@regionId = -1 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT DevelopmentId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ ' 
--																	AND IsActive=''true'') 
--									OR PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '
--																	AND IsActive=''true''))'
		
--	END
--	ELSE IF(@regionId > 0 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) + ' 
--								   AND (PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '
--																	AND IsActive=''true''))'	
--	END
--	ELSE IF(@regionId > 0 and @suburbId > 0)
--	BEGIN
--		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' 
--								   AND (PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '
--																	AND IsActive=''true''))	
--								   AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') 
--								   AND (P_SCHEME.SCHEMEID IN (SELECT DevelopmentId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ ' 
--																	AND IsActive=''true'')) ' 
--	END
--ELSE IF(@regionId = -1 and @suburbId > 0)
--    BEGIN
--         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) +'
--         							  AND (P_SCHEME.SCHEMEID IN (SELECT DevelopmentId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ ' 
--																	AND IsActive=''true'')) '	
--    END
--END

--		if(@missedCheck = '')
--		BEGIN

--		SET @query =
--			   'SELECT COUNT(*) as recordCount FROM(SELECT COunt(Distinct customer.CustomerId) as TotalRecords
--				FROM         AM_PaymentPlan INNER JOIN
--							 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
--							 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
--							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId INNER JOIN
--							 P_DEVELOPMENT ON P_SCHEME.SCHEMEID=P__PROPERTY.DEVELOPMENTID
--                             INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
--							 INNER JOIN AM_Case ON AM_PaymentPlan.PaymentPlanId=AM_Case.PaymentPlanId LEFT JOIN
--							 AM_MissedPayments ON dbo.AM_Case.PaymentPlanId = dbo.AM_MissedPayments.PaymentPlanId
--				Where AM_PaymentPlan.IsCreated = ''true''
--					  AND '+ @RegionSuburbClause +'
--					  AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
--					  AND (AM_Case.IsActive=''true'') 
--					  AND AM_CASE.IsPaymentPlan=1
--                      GROUP BY AM_PaymentPlan.TennantId) as TEMP'

--		END
--		ELSE
--		BEGIN
--		SET @query =
--				'SELECT COUNT(*) as recordCount FROM(SELECT COunt(Distinct customer.CustomerId) as TotalRecords
--				FROM         AM_PaymentPlan INNER JOIN
--							 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
--							 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
--							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId INNER JOIN
--							 P_DEVELOPMENT ON P_SCHEME.SCHEMEID=P__PROPERTY.DEVELOPMENTID
--                             INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
--							 INNER JOIN AM_Case ON AM_PaymentPlan.PaymentPlanId=AM_Case.PaymentPlanId LEFT JOIN
--							  AM_MissedPayments ON dbo.AM_Case.PaymentPlanId = dbo.AM_MissedPayments.PaymentPlanId
--				Where AM_PaymentPlan.IsCreated = ''true''
--					  AND '+ @RegionSuburbClause +'
--					  AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
--					  AND AM_Case.IsActive=''true''
--					  AND AM_CASE.IsPaymentPlan=1
--					  AND AM_PaymentPlan.PaymentPlanId IN (select distinct AM_MissedPayments.PaymentPlanId 
--															 FROM AM_MissedPayments INNER JOIN
--																AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId) 
--					GROUP BY AM_PaymentPlan.TennantId) as TEMP'
														


--		END

--PRINT(@query);
--EXEC(@query);


--END
--SELECT COUNT(*) as recordCount FROM(SELECT COunt(Distinct customer.CustomerId) as TotalRecords
--				FROM         AM_PaymentPlan INNER JOIN
--							 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
--							 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
--							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId INNER JOIN
--							 P_DEVELOPMENT ON P_SCHEME.SCHEMEID=P__PROPERTY.DEVELOPMENTID
--                             INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
--							 INNER JOIN AM_Case ON AM_PaymentPlan.PaymentPlanId=AM_Case.PaymentPlanId LEFT JOIN
--							 AM_MissedPayments ON dbo.AM_Case.PaymentPlanId = dbo.AM_MissedPayments.PaymentPlanId
--				Where AM_PaymentPlan.IsCreated = 'true'
--					  AND (P_SCHEME.SCHEMEID IN (SELECT DevelopmentId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =175 
--																	AND IsActive='true') 
--									OR PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId 
--																	FROM AM_ResourcePatchDevelopment 
--																	WHERE ResourceId =175
--																	AND IsActive='true'))
--					  AND C_ADDRESS.POSTCODE=CASE WHEN ''='' THEN C_ADDRESS.POSTCODE ELSE '' END 
--					  AND (AM_Case.IsActive='true') 
--					  AND AM_CASE.IsPaymentPlan=1
--                      GROUP BY AM_PaymentPlan.TennantId) as TEMP


--END










GO
