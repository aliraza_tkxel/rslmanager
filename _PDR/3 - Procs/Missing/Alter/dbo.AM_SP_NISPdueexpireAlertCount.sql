
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_NISPdueexpireAlertCount]
	@caseOwnedBy int=0
AS
	BEGIN
		declare @RegionSuburbClause varchar(8000)
		declare @query varchar(8000)
	
		IF(@caseOwnedBy <= 0 )
		BEGIN
			SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
		END 
		ELSE
		BEGIN
			SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true'') 
											OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
		END
		
		SET @query=
					' SELECT COUNT(*) as recordCount 
					  FROM(
						   SELECT Count(*) as TotalRecords
						   FROM AM_Case 
								INNER JOIN AM_Action ON AM_Case.ActionId = AM_Action.ActionId 
								INNER JOIN AM_Status ON AM_Case.StatusId = AM_Status.StatusId 
								--INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID 
								INNER JOIN C_TENANCY ON AM_Case.TenancyId  = C_TENANCY.TENANCYID 
								INNER JOIN AM_LookupCode ON AM_Status.NextStatusAlertFrequencyLookupCodeId = AM_LookupCode.LookupCodeId
								INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
								LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
								INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
								--INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
							WHERE AM_Case.IsActive = 1 
							AND  ' + @RegionSuburbClause + '
							AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
							AND AM_STATUS.Title=''NISP''
							AND AM_Case.CaseId IN ( SELECT CASEID
													FROM AM_CASE
													WHERE NoticeExpiryDate IS NOT NULL 
														AND IsActive = ''True'' 
														AND dbo.AM_FN_Check_Case_Notice_Expiry_Date(AM_Case.NoticeExpiryDate) = ''True''
												  )
							GROUP BY AM_Case.TenancyId
						) as TEMP '
	

	PRINT @query;
	EXEC(@query);
END


--EXEC [AM_SP_OverdueStagesAlertCount] @caseOwnedBy=160

GO
