
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Zunair Minhas
-- Create date: <Create Date,31 March 2010,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[AM_SP_GetCaseList]
		@postCode varchar(50)='',
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle	varchar(100),
		@skipIndex	int = 0,
		@pageSize	int = 10,
        @surname varchar(50),
		@sortBy     varchar(100),
        @sortDirection varchar(10),
		@IsNoticeExpiryCheck	bit,
        @overdue varchar(8000)

AS
BEGIN

declare @orderbyClause varchar(50)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @WhereClause	varchar(5000)
declare @RegionSuburbClause varchar(8000)
declare @missedPaymentQuery varchar(8000)

--IF(@caseOwnedBy = -1 )
--BEGIN

--	IF(@regionId = -1 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = P_DEVELOPMENT.PATCHID'
--	END
--	ELSE IF(@regionId > 0 and @suburbId = -1)
--	BEGIN
--		SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
--	END
--	ELSE IF(@regionId > 0 and @suburbId > 0)
--	BEGIN
--		SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
--	END
--    ELSE IF(@regionId = -1 and @suburbId > 0)
--    BEGIN
--         	SET @RegionSuburbClause = 'P_DEVELOPMENT.DEVELOPMENTID= ' + convert(varchar(10), @suburbId) 
--    END

--END
--ELSE 
--BEGIN

--		IF(@regionId = -1 and @suburbId = -1)
--		BEGIN
--			SET @RegionSuburbClause = '(P_DEVELOPMENT.DEVELOPMENTID IN (SELECT DevelopmentId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true'') 
--											OR P_DEVELOPMENT.PATCHID IN (SELECT PatchId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
--		END
--		ELSE IF(@regionId > 0 and @suburbId = -1)
--		BEGIN
--			SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +'
--									   AND (P_DEVELOPMENT.PATCHID IN (SELECT PatchId FROM AM_ResourcePatchDevelopment 
--																	 WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ '
--																	 AND IsActive=''true''))' 
--		END
--		ELSE IF(@regionId > 0 and @suburbId > 0)
--		BEGIN
--			SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' 
--									  AND (P_DEVELOPMENT.PATCHID IN (SELECT PatchId FROM AM_ResourcePatchDevelopment 
--																	 WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ '
--																	 AND IsActive=''true'')) 	
--									  AND (P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') 
--									  AND (P_DEVELOPMENT.DEVELOPMENTID IN (SELECT DevelopmentId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true''))' 	 
									  
--	  END
--	  ELSE IF(@regionId = -1 and @suburbId > 0)
--      BEGIN
--         	SET @RegionSuburbClause = 'P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) +'
--         							  AND (P_DEVELOPMENT.DEVELOPMENTID IN (SELECT DevelopmentId 
--															FROM AM_ResourcePatchDevelopment 
--															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true''))' 	
--      END
--END
IF(@caseOwnedBy = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy)+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)+ ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy)+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
END
IF(@IsNoticeExpiryCheck = 1)
	BEGIN

	SET	@WhereClause = 'Where  1=1
							  AND  ' + @RegionSuburbClause + ' 
							  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end + ''%'' ) 
							  AND AM_Case.IsActive = 1 
							  AND AM_STATUS.Title=''NISP''
							  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
							  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
							  AND AM_Case.CaseId IN(select CaseId
													FROM AM_CASE
													WHERE NoticeExpiryDate IS NOT NULL 
															AND IsActive = ''True'' 
															AND dbo.AM_FN_Check_Case_Notice_Expiry_Date(AM_Case.NoticeExpiryDate) = ''True'')'

	END
	ELSE IF(@overdue='Actions')
	BEGIN
	
		if @caseOwnedBy = -1
		BEGIN
			SET @missedPaymentQuery = 'select distinct AM_MissedPayments.TenantId
										FROM AM_MissedPayments 
										INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
										WHERE IsActive=''true'''
		END
	ELSE
		BEGIN
			SET @missedPaymentQuery = 'select distinct AM_MissedPayments.TenantId
								FROM AM_MissedPayments 
								INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
								WHERE AM_CASE.CASEOFFICER= ' + convert(varchar(10), @caseOwnedBy)+ 'AND ISActive=''true'''
				
								
		END		
SET	@WhereClause = 'Where 1=1
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end + ''%'' ) 
						  AND AM_Case.IsActive = 1 
						  --AND AM_CASE.IsPaymentPlan<>1
						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
													FROM AM_CaseHistory INNER JOIN
														 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
														 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
														 AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								                         E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								                         AM_LookupCode ON AM_ACTION.NextActionAlertFrequencyLookup = AM_LookupCode.LookupCodeId

			                                                     WHERE AM_CaseHistory.IsActive = 1 
				                                                    	--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_ACTION.NextActionAlert, AM_LookupCode.CodeName, AM_CaseHistory.StatusReview ) = ''true'' 
				                                                    	--and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId)
				                                                       and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, ''Days'', AM_CaseHistory.ActionReviewDate ) = ''true'' 
		                                                    	       and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId))
				                                                      OR AM_Case.TenancyId IN(' + @missedPaymentQuery + ') '
END



--SET	@WhereClause = 'Where 1=1
----						  AND  ' + @RegionSuburbClause + ' 
----						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end + ''%'' ) 
----						  AND AM_Case.IsActive = 1 
----						  --AND AM_CASE.IsPaymentPlan<>1
----						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
----						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
----						  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
----													FROM AM_CaseHistory INNER JOIN
----														 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
----														 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
----														-- AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
----								                       --  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
----								                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
----								                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
----								                         AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

----			                                                     WHERE AM_CaseHistory.IsActive = 1 
----				                                                    	--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = ''true'' 
----				                                                    	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionReviewDate ) = ''true'' 
----				                                                    	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId))'
--END
ELSE IF(@overdue='Stages')
BEGIN
SET	@WhereClause = 'Where 1=1
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' end + ''%'' ) 
						  AND AM_Case.IsActive = 1 
						  AND AM_CASE.IsPaymentPlan<>1
						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
													FROM AM_CaseHistory INNER JOIN
														 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
														 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
														 --AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								                         --E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								                         AM_LookupCode ON AM_Status.NextStatusAlertFrequencyLookupCodeId = AM_LookupCode.LookupCodeId

			                                                     WHERE AM_CaseHistory.IsActive = 1 
				                                                    	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Status.NextStatusAlert, AM_LookupCode.CodeName, AM_CaseHistory.StatusReview ) = ''true'' 
				                                                    	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId)
				                                                    	)'
END


ELSE
BEGIN

SET	@WhereClause = 'Where 1=1
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME LIKE '''' + case when '''' = '''+ REPLACE(@surname,'''','''''') +''' then customer.LASTNAME else '''+ REPLACE(@surname,'''','''''') +''' END + ''%'' ) 
						  AND AM_Case.IsActive = 1 
						  AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)'

END

SET @orderbyClause = ' ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
SET @query = 
'SELECT TOP('+convert(varchar(10),@pageSize)+')  
						Max(AM_Action.Title) AS ActionTitle, 
						Max(AM_Case.CaseId) as CaseId, 
						Max(AM_Status.Title) AS StatusTitle, 
						
						(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
								INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
							WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
							ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

					    (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
								INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
							WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
							ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

					    (SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
							FROM AM_Customer_Rent_Parameters
								INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
							WHERE	AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID  
							AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())) as JointTenancyCount,						

                  		Max(customer.CustomerAddress) AS CustomerAddress, 
						Max(convert(varchar(10), AM_Case.IsPaymentPlan)) AS PaymentPlan, 
						AM_Case.TenancyId as TenancyId, 
						Max(convert(varchar(10), AM_Case.IsSuppressed)) as IsSuppressed,
						Max(ISNULL(convert(varchar(100),AM_Case.SuppressedDate, 103), '''')) as SuppressedDate, 
						Max(customer.CUSTOMERID) as CustomerId,
						--isnull((SELECT F_HBINFORMATION.CUSTOMERID 
						--FROM F_HBACTUALSCHEDULE
						--	INNER JOIN dbo.F_HBINFORMATION ON dbo.F_HBACTUALSCHEDULE.HBID = dbo.F_HBINFORMATION.HBID AND STATUS=1
						--	INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C_CUSTOMERTENANCY.CUSTOMERID=F_HBINFORMATION.CUSTOMERID  
						--	WHERE F_HBINFORMATION.ACTUALENDDATE IS NULL
						--AND  F_HBACTUALSCHEDULE.HBROW = (SELECT MIN(HBROW) FROM dbo.F_HBACTUALSCHEDULE WHERE HBID=F_HBINFORMATION.HBID AND VALIDATED IS NULL)
						--AND F_HBINFORMATION.TENANCYID =AM_Case.TENANCYID  
						--AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						--),(SELECT  MAX(ACRP.CustomerId)
						--	FROM AM_Customer_Rent_Parameters ACRP
						--		INNER JOIN C_CUSTOMERTENANCY CCT ON  CCT.CustomerId=ACRP.CustomerId
						--	WHERE	ACRP.TenancyId = AM_Case.TENANCYID  
						--	AND (CCT.ENDDATE IS NULL OR CCT.ENDDATE>GETDATE()))) AS CustomerId,
						Max(customer.RentBalance) AS RentBalance,
 				  		Max(customer.EstimatedHBDue) AS EstimatedHBDue,
						--Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
						Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,
						MAX(AM_Case.ModifiedDate) as ModifiedDate,
						MAX(AM_Case.ActionReviewDate) as ActionReviewDate,
						convert(float, ISNULL((SELECT P_FINANCIAL.Totalrent 
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
													 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
													 WHERE C_TENANCY.TENANCYID = AM_Case.TenancyId), 0.00)) as totalRent

						FROM         AM_Action INNER JOIN
											  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
											  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
											  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
											  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
											  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
											  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
											  INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
                                              INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID '
					+ @WhereClause + ' AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+'''   
					END
				
					AND AM_Case.TenancyId NOT IN ('
SET @subQuery ='SELECT TenancyId FROM(SELECT TOP('+convert(varchar(10),@skipIndex)+')  
						Max(AM_Action.Title) AS ActionTitle, 
						Max(AM_Case.CaseId) as CaseId, 
						Max(AM_Status.Title) AS StatusTitle, 
						
						(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
								INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
							WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
							ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

					    (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
								INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
							WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
							ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

					    (SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
							FROM AM_Customer_Rent_Parameters
								INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
							WHERE	AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID  
							AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())) as JointTenancyCount,						

                  		Max(customer.CustomerAddress) AS CustomerAddress, 
						Max(convert(varchar(10), AM_Case.IsPaymentPlan)) AS PaymentPlan, 
						AM_Case.TenancyId as TenancyId, 
						Max(convert(varchar(10), AM_Case.IsSuppressed)) as IsSuppressed,
						Max(ISNULL(convert(varchar(100),AM_Case.SuppressedDate, 103), '''')) as SuppressedDate, 
						Max(customer.CUSTOMERID) as CustomerId,
						--isnull((SELECT F_HBINFORMATION.CUSTOMERID 
						--FROM F_HBACTUALSCHEDULE
						--	INNER JOIN dbo.F_HBINFORMATION ON dbo.F_HBACTUALSCHEDULE.HBID = dbo.F_HBINFORMATION.HBID AND STATUS=1
						--	INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C_CUSTOMERTENANCY.CUSTOMERID=F_HBINFORMATION.CUSTOMERID  
						--	WHERE F_HBINFORMATION.ACTUALENDDATE IS NULL
						--AND  F_HBACTUALSCHEDULE.HBROW = (SELECT MIN(HBROW) FROM dbo.F_HBACTUALSCHEDULE WHERE HBID=F_HBINFORMATION.HBID AND VALIDATED IS NULL)
						--AND F_HBINFORMATION.TENANCYID =AM_Case.TENANCYID  
						--AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						--),(SELECT  MAX(ACRP.CustomerId)
						--	FROM AM_Customer_Rent_Parameters ACRP
						--		INNER JOIN C_CUSTOMERTENANCY CCT ON  CCT.CustomerId=ACRP.CustomerId
						--	WHERE	ACRP.TenancyId = AM_Case.TENANCYID  
						--	AND (CCT.ENDDATE IS NULL OR CCT.ENDDATE>GETDATE()))) AS CustomerId,
						Max(customer.RentBalance) AS RentBalance,
 				  		Max(customer.EstimatedHBDue) AS EstimatedHBDue,
						--Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
						Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,
						MAX(AM_Case.ModifiedDate) as ModifiedDate,
						MAX(AM_Case.ActionReviewDate) as ActionReviewDate,
						convert(float, ISNULL((SELECT P_FINANCIAL.Totalrent 
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
													 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
													 WHERE C_TENANCY.TENANCYID = AM_Case.TenancyId), 0.00)) as totalRent

						FROM         AM_Action INNER JOIN
											  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
											  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
											  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
											  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
											  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
											  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
											  INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID 
                                              INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID '
					+ @WhereClause + ' AND	  C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END  
												GROUP BY AM_Case.TenancyId 
												'  + @orderbyClause + ') as Temp
									) GROUP BY AM_Case.TenancyId 
								'
					--order by AM_Case.ModifiedDate ASC
--print @orderbyClause 
print(@query )
print( @subQuery) 
print( @orderbyClause)
exec(@query + @subQuery + @orderbyClause);	


END






















GO
