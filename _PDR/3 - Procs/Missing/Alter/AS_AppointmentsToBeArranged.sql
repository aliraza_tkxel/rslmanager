USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AppointmentsToBeArranged]    Script Date: 07/31/2013 12:48:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
-- EXEC AS_AppointmentsToBeArranged
			@check56Days = 0,		
			@searchedText = NULL,
			@pageSize = 10,
			@pageNumber = 1,
			@sortColumn = 'Address',
			@sortOrder = 'ASC',
			@totalCount = 0
		
-- Author:		<Salman Nazir>
-- Modified By: <Noor Muhammad>, <Aamir Waheed, March 25, 2013>, <Aamir Waheed, May 30, 2013>
-- Create date: <15 Sep 2012>
-- Description:	<This stored procedure returns the appointments that have been arranged against the property>
-- Parameters:	
		--@check56Days bit,
		--@searchedText varchar(8000),
		--@pageSize int = 30,
		--@pageNumber int = 1,
		--@sortColumn varchar(50) = 'AS_JOURNAL.JOURNALID',
		--@sortOrder varchar (5) = 'DESC',
		--@totalCount int=0 output
-- ============================================= */
ALTER PROCEDURE [dbo].[AS_AppointmentsToBeArranged]
( 
		--These parameters will be used for search
		@check56Days bit,				
		@searchedText varchar(5000),		
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'AS_JOURNAL.JOURNALID',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int=0 output
		
)
AS
BEGIN
		DECLARE @SelectClause varchar(5000),
        @fromClause   varchar(1000),
        @whereClause  varchar(1000),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(8000),        
        @rowNumberQuery varchar(8000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
        		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (P__PROPERTY.ADDRESS1 LIKE ''%'+@searchedText+'%''  OR P__PROPERTY.ADDRESS2 LIKE ''%'+@searchedText+'%''  OR P__PROPERTY.ADDRESS3 LIKE ''%@'+@searchedText+'%''  OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+@searchedText+'%'')'
		END 
				
		IF(@check56Days = 1)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <=56 
																AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0'
		END
		
		--These conditions wíll be used in every case
		
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND AS_JOURNAL.STATUSID = 1 
															AND AS_JOURNAL.IsCurrent = 1
															AND dbo.P__PROPERTY.FUELTYPE = 1
															AND dbo.P__PROPERTY.STATUS NOT IN (9,5,6) 
															AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)'
	
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @SelectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
							ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''')+ ISNULL('' , ''+P__PROPERTY.TOWNCITY,'''')  AS ADDRESS ,  
							P__PROPERTY.HouseNumber as HouseNumber,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							P__PROPERTY.ADDRESS3 as ADDRESS3,	
							P__PROPERTY.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,							
							P_FUELTYPE.FUELTYPE AS FUEL,
							P__PROPERTY.PROPERTYID,
							C_TENANCY.TENANCYID,
							AS_JOURNAL.JOURNALID,
							AS_Status.Title AS StatusTitle,
							ISNULL(P_STATUS.DESCRIPTION, ''N/A'') AS PropertyStatus,
							E_PATCH.PatchId as PatchId,
							E_PATCH.Location as PatchName,
							'' '' as Telephone'
		
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		
		SET @fromClause = CHAR(10) + 'FROM P__PROPERTY 
										INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID							
										INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
										INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
										LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
										LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
										LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
										INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID'
		
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause						
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
		END		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building WHERE clause
	    			  				
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================	
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================		
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================									
END


