USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GetCountNoEntry]    Script Date: 12/31/2014 12:46:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- EXEC	[dbo].[RD_GetCountNoEntry]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'No Entries'>
-- Webpage: dashboard.aspx
-- Updated On: 31/12/2014
-- Updated By: Salman Nazir
-- Reason:  Change Dashboard counts for Block and Schemes
-- =============================================
ALTER PROCEDURE [dbo].[RD_GetCountNoEntry]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declaring the variables to be used in this query
SELECT  count(*) 
FROM	FL_FAULT_NOENTRY
		JOIN FL_FAULT_LOG on FL_FAULT_NOENTRY.FaultLogId = FL_FAULT_LOG.FaultLogID
		JOIN FL_FAULT_TRADE on FL_FAULT_LOG.FaultTradeID = FL_FAULT_TRADE.FaultTradeId
		JOIN FL_FAULT on FL_FAULT_TRADE.FaultID = FL_FAULT.FaultID	
		JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID 
		LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID	
		
END



