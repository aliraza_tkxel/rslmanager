SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- AUTHOR:		<ADNAN MIRZA>
-- CREATE DATE: <5 NOV 2010>
-- DESCRIPTION:	<FOR RISK MONITORING REPORT>
-- =============================================
ALTER PROCEDURE [dbo].[RISK_LIST_ad] 

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	

	SELECT CASE WHEN CR.ITEMSTATUSID=14 THEN DTIMESTAMP END AS CLOSEDATE,CR.RISKHISTORYID,J.JOURNALID,J.CUSTOMERID,CUST.FIRSTNAME + ' ' + CUST.LASTNAME AS [NAME],CUSTADD.HOUSENUMBER + ' ' + CUSTADD.ADDRESS1 AS [ADDRESS] ,CR.STARTDATE,CR.REVIEWDATE, EMP.FIRSTNAME + ' ' + EMP.LASTNAME AS ASSIGNEDTO
	FROM C_JOURNAL J
	INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID
	INNER JOIN C_ADDRESS CUSTADD ON CUSTADD.CUSTOMERID = J.CUSTOMERID AND CUSTADD.ISDEFAULT=1
	INNER JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID=CUSTADD.CUSTOMERID
	INNER JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = CR.ASSIGNTO
	LEFT JOIN C_CUSTOMERTENANCY T  WITH (NOLOCK) ON T.CUSTOMERID= CUST.CUSTOMERID
	LEFT JOIN C_TENANCY CT  WITH (NOLOCK) ON CT.TENANCYID= T.TENANCYID
	LEFT JOIN P__PROPERTY P  WITH (NOLOCK) ON P.PROPERTYID = CT.PROPERTYID
	LEFT JOIN PDR_DEVELOPMENT DEV ON P.DEVELOPMENTID = DEV.DEVELOPMENTID  
	LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 	

	WHERE J.ITEMNATUREID=63
	ORDER BY J.CUSTOMERID 

END

GO
