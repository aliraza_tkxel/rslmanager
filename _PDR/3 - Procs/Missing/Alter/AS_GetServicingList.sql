USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetServicingList]    Script Date: 03/30/2015 16:46:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,16-01-2015>
-- Description:	<Description,,Get list of Servicing status type data in Appliacing DashBoard>
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetServicingList]
@schemeId int =-1,
@blockId int =-1,
@servicingType int =-1,

	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Address',
	@sortOrder varchar (5) = 'ASC',
	@totalCount int=0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @offset int
	declare @limit int

	set @offset = 1+(@pageNumber-1) * @pageSize
	set @limit = (@offset + @pageSize)-1	

	-- Declaring the variables to be used in this query
	DECLARE @statusId int,
			@selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@orderClause  varchar(100),	
			@whereClause	varchar(8000),			
			@mainSelectQuery varchar(5000),        
			@rowNumberQuery varchar(5500),
			@finalQuery varchar(6000),
			@searchCriteria varchar(3000) 
			
	Set @searchCriteria = '1=1 '+ CHAR(10)	
	IF (@schemeId > 0 AND @blockId > 0)
		BEGIN
					Set @searchCriteria = @searchCriteria + ' AND( P_SCHEME.SCHEMEID = '+ CONVERT(varchar,@schemeId)+ CHAR(10)
					Set @searchCriteria = @searchCriteria + ' OR P_BLOCK.BLOCKID = '+ CONVERT(varchar,@blockId)+ CHAR(10)+')'
		
		END
	ELSE
		BEGIN	
			If @schemeId > 0
				BEGIN 
					Set @searchCriteria = @searchCriteria + ' AND P_SCHEME.SCHEMEID = '+ CONVERT(varchar,@schemeId)+ CHAR(10)
				END		
			If @blockId > 0
				BEGIN 
					Set @searchCriteria = @searchCriteria + ' AND P_BLOCK.BLOCKID = '+ CONVERT(varchar,@blockId)+ CHAR(10)
				END	
		END	
	IF @servicingType = 1
	Begin	
		Select @statusId = StatusId from PDR_Status Where TITLE = 'No Entry'
	END
	Else IF @servicingType = 2
	Begin
		Select @statusId = StatusId from PDR_Status Where TITLE = 'To be Arranged'				
	END
	
	Set @searchCriteria = @searchCriteria + ' AND PDR_STATUS.STATUSID = '+ CONVERT(varchar,@statusId)+ CHAR(10)
	
	
	Set @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
	CASE
	WHEN PDR_MSAT.PROPERTYID IS not null THEN
		ISNULL(P__PROPERTY.ADDRESS1,'''')+'' ''+ISNULL(P__PROPERTY.ADDRESS2,'''')+'' ''+ISNULL(P__PROPERTY.ADDRESS3,'''') 
	ELSE 
		ISNULL(P_BLOCK.ADDRESS1,'''')+'' ''+ISNULL(P_BLOCK.ADDRESS2,'''')+'' ''+ISNULL(P_BLOCK.ADDRESS3,'''') 
	END as [Address]
	,ISNULL(PDR_STATUS.TITLE,'' '') as [StatusTitle],PDR_MSAT.NextDate' + CHAR(10)
	
	
	Set @fromClause = 'FROM PDR_JOURNAL
						INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
						INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
						INNER JOIN	PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId
						LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
						LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
						LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
						LEFT JOIN P_BLOCK AS P_PROPERTY_BLOCK ON P__PROPERTY.BLOCKID = P_PROPERTY_BLOCK.BLOCKID
						LEFT JOIN P_SCHEME AS P_PROPERTY_SCHEME ON P__PROPERTY.SCHEMEID = P_PROPERTY_SCHEME.SCHEMEID
						INNER JOIN	PA_ITEM ON PDR_MSAT.ItemId = PA_ITEM.ItemID 
						INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID'+ CHAR(10)
						
	Set @whereClause = 'Where '	+ CHAR(10) + @searchCriteria
	
	
	
	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder	
	
	
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause	
	
		--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================	
		--========================================================================================
	-- Begin building Count Query 

	Declare @selectCount nvarchar(2000), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		
	-- End building the Count Query
	--========================================================================================				
END
