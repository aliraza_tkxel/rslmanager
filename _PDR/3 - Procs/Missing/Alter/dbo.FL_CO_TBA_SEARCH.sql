SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[FL_CO_TBA_SEARCH] 

/* ===========================================================================
 '   NAME:           FL_CO_TBA_SEARCH
 '   DATE CREATED:   27 DECEMBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist reported fault based on search criteria provided
 '   IN:             @locationId, @areaId, @elementId, @priorityId, @status,@user,
					 @patch,@scheme,@postcode,@due,@ORGID
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	    @locationId	int	= NULL,
		@areaId	    int	= NULL,		
		@elementId	int = NULL,
		@priorityId	int = NULL,
		@status		int = NULL,   
	 	@user		int = NULL,
	             @patch      int = NULL,
		@scheme		int = NULL,
	  	@postcode	nvarchar(20) = NULL,
		@due		nvarchar(20) = NULL,
		@ORGID		int = NULL,
		@JsNumber INT =NULL,
		
	
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FaultLOGID',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @status IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@status)) + ' AND'  
    IF @patch IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E_PATCH.PATCHID = '+ LTRIM(STR(@patch)) + ' AND'  
    IF @scheme IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P_SCHEME.SCHEMEID  = '+ LTRIM(STR(@scheme)) + ' AND'  
    IF @postcode IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY.POSTCODE = '''+ @postcode + ''' AND'  
    IF @due IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'Convert(varchar(10),FL_FAULT_LOG.DueDate,103) ='''+ Convert(varchar,@due,120) +''' AND'
	
    IF @JsNumber IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_LOG.FAULTLOGID = '+ LTRIM(STR(@JsNumber)) + ' AND'                              
    IF @ORGID IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_LOG.ORGID = '+ LTRIM(STR(@ORGID)) + ' AND' 
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.CustomerId AS CUSTOMERID, FL_FAULT_LOG.FaultLogID AS FaultLogID, FL_FAULT_LOG.JobSheetNumber AS JSNumber,' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.SubmitDate AS Due,C__CUSTOMER.FIRSTNAME AS Name, C__CUSTOMER.MIDDLENAME, C__CUSTOMER.LASTNAME, G_TITLE.DESCRIPTION AS TITLEDESCRIPTION, ' +
                        CHAR(10) + CHAR(9) + 'P__PROPERTY.ADDRESS1 AS Address,  P__PROPERTY.HOUSENUMBER + '','' +  P__PROPERTY.ADDRESS1 + '','' +  ISNULL(P__PROPERTY.ADDRESS2,'''') + '','' + P__PROPERTY.TOWNCITY + '','' +    P__PROPERTY.POSTCODE + '', '' + P__PROPERTY.COUNTY AS COMPLETEADDRESS,FL_FAULT_PRIORITY.ResponseTime AS Priority,' +
                        CHAR(10) + CHAR(9) + 'FL_AREA.AreaName AS Area, FL_ELEMENT.ElementName AS Element, FL_FAULT.Description AS Description,' +
	           CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.DueDate as DueDate,Case  FL_FAULT_PRIORITY.Days'+
	           CHAR(10) +CHAR(9) +' When 0 then ''Hour(s)'' when 1 then ''Day(s)''  end as Type ,dbo.FL_CO_TBA_IMG_POPUP(FL_FAULT_LOG.CustomerId) as status' 
                       
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG INNER JOIN ' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID =FL_FAULT_STATUS.FaultStatusID AND FL_FAULT_STATUS.FaultStatusID NOT IN (11) INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT ON FL_FAULT.FAULTID =FL_FAULT_LOG.FAULTID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_FAULT.ELEMENTID=FL_ELEMENT.ELEMENTID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_AREA ON FL_ELEMENT.AREAID=FL_AREA.AREAID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_LOCATION ON FL_AREA.LOCATIONID=FL_LOCATION.LOCATIONID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PRIORITYID=FL_FAULT_PRIORITY.PRIORITYID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = FL_FAULT_LOG.CustomerId LEFT JOIN'+
					  CHAR(10) + CHAR(9) + 'G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID INNER JOIN' + 
                      CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN'+ 
                      CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL INNER JOIN'+		
                      --CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN'+ 
                      --CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID LEFT JOIN'+
                      CHAR(10) + CHAR(9) + 'P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID INNER JOIN'+   
					  CHAR(10) + CHAR(9) + 'PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID INNER JOIN'+ 
                      CHAR(10) + CHAR(9) + 'E_PATCH ON PDR_DEVELOPMENT.PATCHID=E_PATCH.PATCHID'
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FaultLOGID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) +  @sortOrder+
					' , FaultLOGID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FaultLOGID NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FaultLOGID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'WHERE 1=1 AND'+ @SearchCriteria + 
                        CHAR(10)+CHAR(9)+' FL_FAULT_LOG.JOBSHEETNUMBER NOT IN(SELECT FL_FAULT_LOG.JOBSHEETNUMBER '+
	                    CHAR(10)+CHAR(9)+'FROM FL_FAULT_LOG INNER JOIN FL_CO_APPOINTMENT'+
	                    CHAR(10)+CHAR(9)+' ON FL_FAULT_LOG.JOBSHEETNUMBER=FL_CO_APPOINTMENT.JOBSHEETNUMBER) AND'+
	                    CHAR(10)+CHAR(9)+' FL_FAULT_LOG.ISSELECTED=1 AND'+
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        CHAR(10)+CHAR(9)+' FL_FAULT_LOG.JOBSHEETNUMBER NOT IN(SELECT FL_FAULT_LOG.JOBSHEETNUMBER '+
	                    CHAR(10)+CHAR(9)+'FROM FL_FAULT_LOG INNER JOIN FL_CO_APPOINTMENT'+
	                    CHAR(10)+CHAR(9)+' ON FL_FAULT_LOG.JOBSHEETNUMBER=FL_CO_APPOINTMENT.JOBSHEETNUMBER) AND'+
                         CHAR(10)+CHAR(9)+' FL_FAULT_LOG.ISSELECTED=1 AND'+
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)



GO
