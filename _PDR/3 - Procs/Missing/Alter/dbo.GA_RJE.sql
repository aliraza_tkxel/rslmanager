SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE  [dbo].[GA_RJE]
@DEVELOPMENTID	INT = NULL,
@PATCHID INT = NULL

AS
SET NOCOUNT ON
--EXEC [GA_RJE] NULL, NULL

--DECLARE @DEVELOPMENTID int
--DECLARE @PATCHID int
--SET @DEVELOPMENTID = NULL
--SET @PATCHID = NULL

CREATE TABLE #GA_CP12 (
	ID INT IDENTITY(1,1),
	CP12NUMBER NVARCHAR(20),
	PROPERTYID NVARCHAR(20),
	SCHEMEID INT, 
	PATCHID INT,
	STATUS INT
)

INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 1
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
	((GPA.WARRANTYEXPIRYDATE<GETDATE() AND ISSUEDATE IS NULL) OR  DATEADD(MM,12,GPA.ISSUEDATE)<GETDATE()) 
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID

INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 2
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
      (  
		(DATEDIFF(DD,GETDATE(),GPA.WARRANTYEXPIRYDATE)<7 AND DATEDIFF(DD,GETDATE(),GPA.WARRANTYEXPIRYDATE)>0 AND ISSUEDATE IS NULL) 
        OR  
        (DATEDIFF(DD,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))<7 AND DATEDIFF(DD,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))>0)	
      ) 
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID

INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 3
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
     ( 
        (DATEDIFF(D,GETDATE(),GPA.WARRANTYEXPIRYDATE)>=7 AND DATEDIFF(WK,GETDATE(),GPA.WARRANTYEXPIRYDATE+1)<3 AND ISSUEDATE IS NULL) 
        OR  
		(DATEDIFF(D,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))>=7 AND DATEDIFF(Wk,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE)+1)<3) 
     ) 
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID

INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 4
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
	( 
		(DATEDIFF(WK,GETDATE(),GPA.WARRANTYEXPIRYDATE+1)>=3 AND DATEDIFF(WK,GETDATE(),GPA.WARRANTYEXPIRYDATE+1)<=4 AND ISSUEDATE IS NULL) 
		OR  
		(DATEDIFF(WK,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE)+1)>=3 AND DATEDIFF(WK,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE)+1)<=4) 
	)  
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID

INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 5
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
	( 
		(DATEDIFF(WK,GETDATE(),GPA.WARRANTYEXPIRYDATE+1)>4 AND DATEDIFF(MM,GETDATE(),GPA.WARRANTYEXPIRYDATE)<=6 AND ISSUEDATE IS NULL) 
		OR  
		(DATEDIFF(WK,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE)+1)>4 AND DATEDIFF(MM,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))<=6) 
    ) 
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID


INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 6
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
   	( 
		(DATEDIFF(MM,GETDATE(),GPA.WARRANTYEXPIRYDATE)>6 AND DATEDIFF(MM,GETDATE(),GPA.WARRANTYEXPIRYDATE)<=12 AND ISSUEDATE IS NULL) 
		OR  
		(DATEDIFF(MM,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))>6 AND DATEDIFF(MM,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))<=12) 
	) 
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID

INSERT INTO #GA_CP12 (CP12NUMBER,PROPERTYID,SCHEMEID,PATCHID, STATUS)
SELECT GPA.CP12NUMBER, PP.PROPERTYID, PS.SCHEMEID, PD.PATCHID, 7
FROM GS_PROPERTY_APPLIANCE GPA 
	INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=GPA.PROPERTYID    
	LEFT JOIN P_SCHEME PS ON PP.SCHEMEID = PS.SCHEMEID    
	LEFT JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID = PD.DEVELOPMENTID 
WHERE PS.SCHEMEID = COALESCE(@DEVELOPMENTID, PS.SCHEMEID) AND
	  PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID) AND
	( 
        (DATEDIFF(MM,GETDATE(),GPA.WARRANTYEXPIRYDATE)>12 AND ISSUEDATE IS NULL ) 
		OR  
		(DATEDIFF(MM,GETDATE(),DATEADD(MM,12,GPA.ISSUEDATE))>12) 
     ) 
GROUP BY ISSUEDATE, PP.PROPERTYID, GPA.CP12NUMBER, PD.PATCHID, PS.SCHEMEID

SELECT  (SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 1) AS EXPIRED,
	    (SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 2) AS LESSTHANWEEK_CP12,
		(SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 3) AS BETWEEN1AND2WEEKS_CP12,
		(SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 4) AS BETWEEN2AND4WEEKS_CP12,
		(SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 5) AS BETWEEN3AND6MONTHS_CP12,
		(SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 6) AS BETWEEN6AND12MONTHS_CP12,
		(SELECT COUNT(1) FROM #GA_CP12 WHERE STATUS = 7) AS GREATERTHAN12MONTHS_CP12

DROP TABLE #GA_CP12

SET NOCOUNT OFF
GO
