-- Stored Procedure

ALTER PROCEDURE [dbo].[FL_GetFaultManagementSearchResults]
/* ===========================================================================
--	EXEC FL_GetFaultManagementSearchResults
		@searchTerm = '',
		@noOfRows = 1000,
		@offSet = 0,
		@sortColumn = 'FaultID ',
		@sortOrder = 'DESC'
--  Author:			Aamir Waheed
--  DATE CREATED:	1 March 2013
--  Description:	To shortlist faults based on search criteria provided
--  Webpage:		View/Reports/ReportArea.aspx 
 '==============================================================================*/

	(	
		-- Quick find parameters
		@schemeId int = -1,
		@blockId int = -1,
	    @searchTerm varchar(max) = NULL,

		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 10,
		@offSet   int = 0,

		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FaultID ',
		@sortOrder varchar (20) = 'DESC',

		-- Paging parameters if not passed will be calculated from @noOfRows and @offSet

		@startRow int = 0,
		@endRow int = 10			
	)


AS

	DECLARE --@selectClause varchar(8000),
	        --@fromClause   varchar(8000),
	        --@whereClause  varchar(8000),	        
	        @orderClause  varchar(500),
	        @query varchar(max),
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @searchCriteria varchar(8000)

	--========================================================================================    
    -- Begin Calculating Start Row and End Row
    SET @startRow = (@noOfRows * @offSet) + 1
	SET	@endRow = @startRow + @noOfRows - 1    		
	 --End Calculating Start Row and End Row
    --========================================================================================*/	


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided

    SET @searchCriteria = ''      


    IF @searchTerm IS NOT NULL AND @searchTerm != ''
       SET @searchCriteria = @searchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT.Description LIKE ''%'+ LTRIM(@searchTerm) + '%'' AND'                            

      -- Filter on the basis of BlockId and SchemeId
		IF(@schemeId != -1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.SCHEMEID = '+convert(varchar(10),@schemeId)
			END
		IF(@blockId != -1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND FL_FAULT_LOG.BLOCKID = '+convert(varchar(10),@blockId)
			END


    -- End building SearchCriteria clause   
    --========================================================================================*/

/*    --========================================================================================    
    -- Begin building FROM clause

    SET @fromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT INNER JOIN' +
                      --CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_LOCATION ON FL_AREA.LocationID = FL_LOCATION.LocationID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID'


    -- End building FROM clause
    --========================================================================================*/

	-- Begin building OrderBy clause

	  IF @sortColumn != 'FaultID'       
		SET @sortColumn = @sortColumn + ' ' + @sortOrder + 
						' , FaultID'

		--SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 

		--SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'

		SET @orderClause =  @sortColumn + ' DESC'    


		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Scheme' 		
		END	

		IF(@sortColumn = 'Block')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Block' 		
		END		

    -- End building OrderBy clause
    --========================================================================================*/


/*    --========================================================================================
    -- Begin building WHERE clause

    -- This Where clause contains subquery to exclude already displayed records
    SET @whereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FaultID NOT IN' + 


                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FaultID ' +  
                        CHAR(10) + CHAR(9) + @fromClause + 
                        CHAR(10) + CHAR(9) + 'AND'+ @searchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @orderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 

                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @searchCriteria +


                        CHAR(10) + CHAR(9) + ' 1=1 )'

    -- End building WHERE clause
    --========================================================================================*/        

SET @query = '
SELECT
		x2.*		
	FROM (	
		SELECT
					x1.*
					,ROW_NUMBER() OVER(ORDER BY '+ @orderClause + ') AS rowNumber
		FROM (

				SELECT	
					FL_FAULT.FaultID,
					FL_AREA.AreaName AS AreaName,
					FL_FAULT.Description AS Description,
					ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
					ISNULL(STUFF(
					(
					SELECT
					'', '' + [G_TRADE].[Description] 
					FROM
					FL_FAULT_TRADE
					JOIN G_TRADE
					ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
					WHERE
					FL_FAULT_TRADE.FaultId = FL_FAULT.FaultID
					FOR XML PATH('''')					
					)
					,1,1,''''), ''N/A'') AS Trade ,
					FL_FAULT_PRIORITY.PriorityName AS PriorityName,
					CASE WHEN FL_FAULT.duration IS NULL THEN ''N/A'' ELSE CONVERT(nvarchar, FL_FAULT.duration) +'' hr'' END AS duration,
					CASE WHEN FL_FAULT.NetCost IS NULL THEN ''N/A'' ELSE CONVERT(nvarchar, FL_FAULT.NetCost) END AS NetCost,
					Case FL_FAULT.FaultActive when 0 then ''Inactive'' when 1 then ''Active'' end as FaultActive,
					CASE WHEN FL_FAULT.isContractor IS NULL THEN ''N/A'' WHEN FL_FAULT.isContractor = 1 THEN ''Yes'' ELSE ''No'' END as isContractor

				FROM 
					FL_FAULT INNER JOIN
					--FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID INNER JOIN
					FL_AREA ON FL_Fault.AreaID = FL_AREA.AreaID INNER JOIN
					FL_LOCATION ON FL_AREA.LocationID = FL_LOCATION.LocationID INNER JOIN
					FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
					INNER JOIN FL_FAULT_LOG on FL_FAULT_LOG.FAULTID = FL_FAULT.FAULTID
					LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = FL_FAULT_LOG.SCHEMEID
					LEFT JOIN P_BLOCK on P_BLOCK.BLOCKID = FL_FAULT_LOG.BLOCKID

				WHERE ( '+
					@searchCriteria +
					' 1=1 )


		) AS x1

) AS x2
	WHERE 
		rowNumber BETWEEN ' +  CONVERT(varchar, @startRow) + ' AND ' + CONVERT(varchar, @endRow)
PRINT @query

EXEC (@query)
GO