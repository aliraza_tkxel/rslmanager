USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AppointmentsArranged]    Script Date: 08/15/2013 13:05:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC  AS_AppointmentsArranged
		--@check56Days = 0,		
		--@searchedText = NULL,
		--@pageSize = 10,
		--@pageNumber = 1,
		--@sortColumn = 'Address',
		--@sortOrder = 'ASC',
		--@totalCount = 0
-- Author:		<Salman Nazir>
-- Create date: <09/20/2012 11:24:54>
-- Description:	<This Stored Proceedure shows the data on Appointment Arranged Tab, on Scheduling Page>
-- WebPage :	Scheduling.aspx...Appointment Arranged Tab
-- Parameters : 
		--@check56Days bit,			
		--@searchedText varchar(8000),
		--@pageSize int = 30,
		--@pageNumber int = 1,
		--@sortColumn varchar(50) = 'AS_JOURNAL.JOURNALID',
		--@sortOrder varchar (5) = 'DESC',
		--@totalCount int=0 output
			
-- =============================================
ALTER PROCEDURE [dbo].[AS_AppointmentsArranged] 
	(
		-- Add the parameters for the stored procedure here
		@check56Days bit,			
		@searchedText varchar(8000),
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'AS_JOURNAL.JOURNALID',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int=0 output
	)
AS
BEGIN
	DECLARE @SelectClause varchar(5000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(8000),        
        @rowNumberQuery varchar(8000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS2, '''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS3, '''+@searchedText+'''))'
		END 
				
		IF(@check56Days = 1)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <=56 
																AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0'
		END				
		
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (AS_JOURNAL.STATUSID = 2 OR AS_JOURNAL.STATUSID = 3 OR AS_JOURNAL.STATUSID = 4)
															AND dbo.P__PROPERTY.FUELTYPE = 1
															AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9) -- Garage, Car port, car space'																	
				
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause			
		
		SET @SelectClause = 'SELECT TOP (' + CONVERT(NVARCHAR(10),@limit) + ') 
							AS_APPOINTMENTS.JSGNUMBER AS JSGNUMBER,CONVERT(varchar,AS_APPOINTMENTS.APPOINTMENTDATE,103)+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME+''-''+ AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENT,AS_APPOINTMENTS.ASSIGNEDTO,AS_APPOINTMENTS.LOGGEDDATE as LOGGEDDATE,
							LEFT(E__EMPLOYEE.FIRSTNAME, 1)+LEFT(E__EMPLOYEE.LASTNAME,1) AS ENGINEER,
							ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''')+ ISNULL('' , ''+P__PROPERTY.TOWNCITY,'''')  AS ADDRESS ,  
							P__PROPERTY.HouseNumber as HouseNumber,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							P__PROPERTY.ADDRESS3 as ADDRESS3,							
							P__PROPERTY.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,
							P_FUELTYPE.FUELTYPE AS FUEL,
							P__PROPERTY.PROPERTYID,
							C_TENANCY.TENANCYID,
							AS_APPOINTMENTS.APPOINTMENTID,
							AS_APPOINTMENTS.JournalId,							
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
							
							ISNULL(P_STATUS.DESCRIPTION, ''N/A'') AS PropertyStatus,
							E_PATCH.PatchId as PatchId,
							E_PATCH.Location as PatchName,
							'' '' as Telephone'
				
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause				
		
		SET @fromClause = CHAR(10) + 'FROM AS_APPOINTMENTS 
										INNER JOIN	AS_JOURNAL on AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID 
										INNER JOIN	AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId 
										INNER JOIN E__EMPLOYEE on AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
										INNER JOIN P__PROPERTY on AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
										INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
										LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
										LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
										LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID 
										INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID'
		
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
		END		
		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							
END
