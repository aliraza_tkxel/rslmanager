USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDevelopmentByPatchIdAndLAId]    Script Date: 02/03/2015 19:38:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get Patch For DropDown in reports
 
    Author: Ali Raza
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Ali Raza           Get Patch For DropDown in reports
    
    Execution Command:
    
    Exec PDR_GetDevelopmentByPatchIdAndLAId 18, 1
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetDevelopmentByPatchIdAndLAId]
	(@patchId INT = NULL,
	 @LAId INT = NULL
	)
AS
BEGIN
	
	SET NOCOUNT ON;   
	 IF @LAId >0 AND @patchId >0 
		BEGIN
		SELECT DEVELOPMENTID,DEVELOPMENTNAME from PDR_DEVELOPMENT  WHERE PATCHID=@patchId AND  LOCALAUTHORITY = @LAId 
		END 
	ELSE IF @patchId >0
		BEGIN
		SELECT DEVELOPMENTID,DEVELOPMENTNAME from PDR_DEVELOPMENT  WHERE PATCHID=@patchId 
		END	
	ELSE IF @LAId >0
		BEGIN
		SELECT DEVELOPMENTID,DEVELOPMENTNAME from PDR_DEVELOPMENT  WHERE LOCALAUTHORITY=@LAId 
		END	
	
	ELSE	
		BEGIN
		SELECT DEVELOPMENTID,DEVELOPMENTNAME from PDR_DEVELOPMENT 
		END	
END
