USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveAsbestosInformation]    Script Date: 03/02/2015 15:43:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PDR_SaveAsbestosInformation]
@asbestosLevelId int,  
 @asbestosElementId int,  
 @riskId varchar(50),  
 @addedDate date,  
 @removedDate varchar(50),  
 @notes varchar(250),  
 @Id int,  
 @userId int,
 @RequestType varchar(100)  
AS  
BEGIN 

if @removedDate IS NULL OR @removedDate = ''
	Begin
	IF (@RequestType = 'Block')
			BEGIN
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,BlockId,DateAdded,UserID,Notes)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@userId,@Notes)  
			END
	ELSE
			BEGIN
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,SchemeId,DateAdded,UserID,Notes)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@userId,@Notes)  
			END            
	END
Else
	BEGIN
		IF (@RequestType = 'Block')
			BEGIN	
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,BlockId,DateAdded,DateRemoved,UserID,Notes)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@RemovedDate,@userId,@Notes)  
			END
		ELSE
			BEGIN
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,SchemeId,DateAdded,DateRemoved,UserID,Notes)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@RemovedDate,@userId,@Notes)  	
			END	            
	END
	INSERT INTO P_PROPERTY_ASBESTOS_RISK(PROPASBLEVELID,ASBRISKID)  
	VALUES(@@IDENTITY,@riskId)
END	

