  
-- =============================================  
-- EXEC FL_SaveFaultTemporary  
--  @faultId = 531,  
--  @customerId = 5,  
-- @propertyId ="A010000018",  
--  @schemeId =1  
-- @blockId = 0  
-- @quantity =1,  
-- @problemDays =1,  
-- @recuringProblem =1,  
-- @recharge =1,  
-- @notes ="This is demo values",  
-- @tempFaultId =0,  
--  @isRecall = 0,  
--  @originalFaultLogId = 0  
-- Author:  <Aqib Javed>  
-- Create date: <11/01/2013>  
-- Description: <Save fault information into FL_TEMP_FAULT table>  
-- Web Page: MoreDetails.aspx  
-- =============================================  
  
ALTER PROCEDURE [dbo].[FL_SaveFaultTemporary]  
 @faultId    INT  
 ,@customerId   INT  
 ,@propertyId   VARCHAR(100)  
 ,@schemeId    INT  
 ,@blockId    INT  
 ,@quantity    INT  
 ,@problemDays   INT  
 ,@recuringProblem  INT  
 ,  
 --@communalProblem INT,  
 @recharge    INT  
 ,@notes     NVARCHAR(4000)  
 ,@tempFaultId   INT OUTPUT  
 ,@isRecall    BIT  
 ,@originalFaultLogId INT  
 ,@isFollowOn   BIT  
 ,@followOnFaultLogId INT  
AS  
BEGIN  
 BEGIN TRY  
  BEGIN TRANSACTION  
  
   DECLARE @faultTradeId AS INT  
     ,@duration AS DECIMAL(9, 2)  
  
   --Declare a cursor  
   DECLARE TempFaultCursor CURSOR FOR  
   SELECT  
    FT.FaultTradeID  
    ,F.duration  
   FROM  
    FL_FAULT F  
     INNER JOIN FL_FAULT_TRADE FT ON F.FAULTID = FT.FAULTID  
   WHERE  
    F.FaultId = @faultId  
    AND ISNULL(F.FAULTACTIVE, 0) = 1  
  
   --To check that trade exist against the fault or not  
   SET @tempFaultId = 0  
  
   --Check isFollowOn if it is false then empty @followOnFaultLogId  
   IF @isFollowOn = 0  
   BEGIN  
    SET @followOnFaultLogId = NULL  
   END  
  
  
   OPEN TempFaultCursor  
  
   FETCH NEXT FROM TempFaultCursor INTO @FaultTradeId, @duration  
  
   WHILE @@FETCH_STATUS = 0  
   BEGIN  
  
    INSERT INTO FL_TEMP_FAULT (  
        FaultID  
        ,CustomerId  
        ,SchemeId  
        ,BlockId  
        ,Quantity  
        ,ProblemDays  
        ,RecuringProblem  
        ,Recharge  
        ,Notes  
        ,ItemStatusID  
        ,PROPERTYID  
        ,ISRECALL  
        ,FaultTradeId  
        ,OriginalFaultLogId  
        ,isFollowon  
        ,FollowOnFaultLogID  
        ,Duration  
       )  
     VALUES (  
       @faultId  
       ,@customerId  
       ,@schemeId         
       ,@blockId  
       ,@quantity  
       ,@problemDays  
       ,@recuringProblem  
       ,@recharge  
       ,@notes  
       ,1  
       ,@propertyId  
       ,@isRecall  
       ,@FaultTradeId  
       ,@originalFaultLogId  
       ,@isFollowOn  
       ,@followOnFaultLogId  
       ,@duration  
      )  
  
    IF SCOPE_IDENTITY() < 0  
    BEGIN  
     SET @tempFaultId = -1  
     BREAK  
    END  
    ELSE  
    BEGIN  
     SET @tempFaultId = SCOPE_IDENTITY()  
     PRINT (@tempFaultId)  
    END  
    --SELECT @tempFaultId as tempFaultId  
  
    FETCH NEXT FROM TempFaultCursor INTO @FaultTradeId, @duration  
   END  
  
   IF (@tempFaultId >= 0)  
   BEGIN  
    COMMIT TRANSACTION  
   END  
   ELSE  
   BEGIN  
    ROLLBACK TRANSACTION  
   END  
  
  CLOSE TempFaultCursor;  
  DEALLOCATE TempFaultCursor;  
  
 END TRY  
 BEGIN CATCH  
  IF @@TRANCOUNT > 0  
  BEGIN  
   ROLLBACK TRANSACTION;  
  END  
  DECLARE @ErrorMessage NVARCHAR(4000);  
  DECLARE @ErrorSeverity INT;  
  DECLARE @ErrorState INT;  
  
  SELECT  
   @ErrorMessage = ERROR_MESSAGE()  
   ,@ErrorSeverity = ERROR_SEVERITY()  
   ,@ErrorState = ERROR_STATE();  
  
  -- Use RAISERROR inside the CATCH block to return   
  -- error information about the original error that   
  -- caused execution to jump to the CATCH block.  
  RAISERROR (@ErrorMessage, -- Message text.  
  @ErrorSeverity, -- Severity.  
  @ErrorState -- State.  
  );  
 END CATCH  
END