USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetTerriorReportData]    Script Date: 02/05/2015 22:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Terrior Report data on Terrior Report Page
 
    Author: Ali Raza
    Creation Date:  03/12/2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         03/12/2014     Ali Raza           Get Terrior Report data on Terrior Report Page
    
    Execution Command:

--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_GetTerriorReportData]
--		@SCHEMEID = NULL,
--		@SEARCHTEXT = N'26 Herringswell Road',
--		@pageSize = 50,
--		@pageNumber = 1,
--		@sortColumn = N'ADDRESS',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'*/
CREATE PROCEDURE [dbo].[PDR_GetTerriorReportData]
	-- Add the parameters for the stored procedure here
		@SCHEMEID INT = null,
		@SEARCHTEXT VARCHAR(200)= null,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ADDRESS', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		IF @SCHEMEID != -1
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( P.SCHEMEID  = ' +convert(varchar(10),@SCHEMEID)  + ')'	
		END
		IF(@SEARCHTEXT != '' OR @SEARCHTEXT != NULL)
		BEGIN	
            SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ISNULL(P.HouseNumber, '''') + '' '' + ISNULL(P.ADDRESS1, '''') + '' '' + ISNULL(P.ADDRESS2, '''') + '' '' + ISNULL(P.ADDRESS3, '''')  LIKE ''%' + @searchText + '%'''
					
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR  HOUSENUMBER  LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR ADDRESS1 LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR POSTCODE LIKE ''%' + @searchText + '%'') '
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		(ISNULL(HOUSENUMBER,'' '')+'' ''+ISNULL(ADDRESS1,'' '')) as ADDRESS,
		 ISNULL(TOWNCITY,'' '') as TownCITY,
		ISNULL(POSTCODE,'' '') as POSTCODE, F.OMV, f.EUV,f.RENT,f.YIELD,
		(f.RENT*12) as AnnualRent, ISNULL(S.DESCRIPTION,'' '') as [STATUS],ISNULL(AT.DESCRIPTION, '' '') as [Type]'
		
		
		--============================From Clause============================================
		SET @fromClause = 'from P__PROPERTY P 
							INNER JOIN P_FINANCIAL F on F.PROPERTYID = P.PROPERTYID 
							INNER JOIN P_STATUS S on S.STATUSID = P.STATUS
							INNER JOIN P_ASSETTYPE AT on AT.ASSETTYPEID = P.ASSETTYPE'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'ADDRESS')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ADDRESS' 	
			
		END
		
		IF(@sortColumn = 'TownCITY')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TOWNCITY' 	
			
		END
		IF(@sortColumn = 'POSTCODE')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'POSTCODE' 	
			
		END
		IF(@sortColumn = 'OMV')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'OMV' 	
			
		END
		IF(@sortColumn = 'EUV')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'EUV' 	
			
		END
		IF(@sortColumn = 'RENT')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'RENT' 	
			
		END
		IF(@sortColumn = 'YIELD')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'YIELD' 	
			
		END
		IF(@sortColumn = '[STATUS]')
		BEGIN
			SET @sortColumn = CHAR(10)+ '[STATUS]' 	
			
		END	
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
											
END
