USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocks]    Script Date: 12/16/2014 21:09:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:   Get Blocks By developmentId 
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Blocks By developmentId 
    
    Execution Command:
    
    Exec PDR_GetBlocksByDevelopmentID
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetBlocksByDevelopmentID]
( @developmentId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select B.BLOCKID,B.BLOCKNAME from P_BLOCK B
	WHERE B.DEVELOPMENTID =@developmentId
END
