-- Stored Procedure

/* =================================================================================    
    Page Description:  Get Blocks By SchemeID

    Author: Ali Raza
    Creation Date: Dec-24-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-24-2014      Ali Raza           Get Blocks By SchemeID

    Execution Command:

    Exec PDR_GetBlocksBySchemeID 33
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetBlocksBySchemeID]
(		@SchemeId INT,	
		--Parameters which would help in sorting and paging
		@pageSize int = 10,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'BlockID', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	)
AS
BEGIN
	DECLARE 

		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),
        @GroupByCluase varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1


		--=====================Search Criteria==================================
		SET @searchCriteria = ' 1=1 '

		IF(@SchemeId > 0 OR @SchemeId != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (  B.SCHEMEID =' + convert(varchar(10),@SchemeId) +') '
		END	

		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
									 B.BLOCKID, B.BLOCKNAME AS BlockName,P.PROPERTYID,PT.DESCRIPTION AS PropertyType '


		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM P_BLOCK B
	Inner join P__PROPERTY P ON B.BLOCKID = P.BLOCKID
	INNER JOIN P_SCHEME S ON B.SCHEMEID = S.SCHEMEID
	INNER JOIN P_PROPERTYTYPE PT ON P.PROPERTYTYPE = PT.PROPERTYTYPEID'

		--============================Order Clause==========================================


		SET @orderClause =  CHAR(10) + ' Order By B.' + @sortColumn + CHAR(10) + @sortOrder

		--=================================	Where Clause ================================

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause  + @orderClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================	

END
GO