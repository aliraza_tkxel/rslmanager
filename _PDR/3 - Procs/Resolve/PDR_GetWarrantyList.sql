-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/12/2014>
-- Description:	<Description,,Get WARRANTIES LIST ON warranty page>
-- 
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetWarrantyList]
	-- Add the parameters for the stored procedure here
		@ID varchar(100),
		@requestType varchar(100),
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'WARRANTYTYPE', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 

		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1


		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		if (@requestType = 'Block')
			BEGIN							
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  W.BLOCKID  = '''+@ID+''''
			END
		ELSE IF(@requestType = 'Scheme')
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  W.SCHEMEID  = '''+@ID+''''
			END
		ELSE
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  W.PROPERTYID ='''+@ID+''''

			END	
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		WARRANTYID,convert(varchar(10), EXPIRYDATE, 103) EXPIRYDATE,
		ISNULL(AI.DESCRIPTION,'' '') AS [For],ISNULL(O.NAME,'' '') AS CONTRACTOR
		,ISNULL(WT.DESCRIPTION,'' '') as WARRANTYTYPE '


		--============================From Clause============================================
		SET @fromClause = 'from PDR_WARRANTY W
						INNER JOIN P_WARRANTYTYPE WT on WT.WARRANTYTYPEID = W.WARRANTYTYPE 
						INNER JOIN S_ORGANISATION O on O.ORGID = W.CONTRACTOR
						INNER JOIN R_AREAITEM AI ON AI.AREAITEMID = W.AREAITEM'

		--============================Order Clause==========================================
		IF(@sortColumn = 'WARRANTYTYPE')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' WARRANTYTYPE' 	

		END

		IF(@sortColumn = 'EXPIRY')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'EXPIRYDATE' 	

		END
		IF(@sortColumn = 'CONTRACTOR')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'CONTRACTOR' 	

		END
		IF(@sortColumn = 'For')
		BEGIN
			SET @sortColumn = CHAR(10)+ '[For]' 	

		END	

		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		--=================================	Where Clause ================================

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		print(@whereClause)
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================	

END
GO