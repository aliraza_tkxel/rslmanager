-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,02/12/2014>
-- Description:	<Description,,Save Documents on SaveDevelopment.aspx>
-- DECLARE	@return_value int

--EXEC	@return_value = [dbo].[PDR_SAVEDOCUMENTS]
--		@title = N'test',
--		@documentPath = N'/documents/',
--		@expires = '02/12/2014',
--		@fileType = N'pdf',
--		@appointmentType = 3

--SELECT	'Return Value' = @return_value

--GO
-- =============================================
CREATE PROCEDURE PDR_SAVEDOCUMENTS
--@title varchar(50),
@documentPath varchar(500),
--@expires smalldatetime,
@fileType varchar(50),
@createdBy int,
@DocumentId int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO PDR_DOCUMENTS (DocumentPath,AppointmentTypeId,FileType,CreatedBy)
	VALUES (@documentPath,0,@fileType,@createdBy)
	
	SELECT @DocumentId = SCOPE_IDENTITY()
	Return @DocumentId 
END
GO
