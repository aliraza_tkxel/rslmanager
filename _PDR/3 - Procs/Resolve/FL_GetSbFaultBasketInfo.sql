USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetFaultBasketInfo]    Script Date: 01/19/2015 16:30:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:    Get Fault Basket Info FL_TEMP_FAULT table for scheme/Block
 
    Author: Ali Raza
    Creation Date: jan-19-2015  

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         jan-19-2015      Ali Raza          Get Fault Basket Info FL_TEMP_FAULT table for scheme/Block
    
    Execution Command:
	--EXEC FL_GetSbFaultBasketInfo
	-- @schemeId = 5,
	--@blockId =NULL,
	--@isRecall = 01
  =================================================================================*/

CREATE PROCEDURE [dbo].[FL_GetSbFaultBasketInfo]
	(
		@schemeId		INT = null
		,@blockId		INT = null
		,@isRecall		BIT
	)
AS
	SELECT
		FL_FAULT.[Description]									AS [Description]
		,G_TRADE.[Description]									AS Trade
		,FL_AREA.AreaName										AS AreaName
		,CASE
			WHEN FL_FAULT_PRIORITY.Days = 1
				THEN CONVERT(VARCHAR(5), FL_FAULT_PRIORITY.ResponseTime) + ' days'
			ELSE CONVERT(VARCHAR(5), FL_FAULT_PRIORITY.ResponseTime) + ' hours'
		END														AS ResponseTime
		,FL_TEMP_FAULT.TempFaultID
		,COALESCE(FL_TEMP_FAULT.Duration, FL_FAULT.duration)	AS duration
		,FL_TEMP_FAULT.FaultLogId								AS faultLogId
		,FL_TEMP_FAULT.IsAppointmentConfirmed					AS isAppointmentConfirmed
		,G_TRADE.TradeId										AS TradeId
	FROM
		FL_TEMP_FAULT
			INNER JOIN FL_FAULT_TRADE ON FL_TEMP_FAULT.FaultTradeId = FL_FAULT_TRADE.FaultTradeId
			INNER JOIN FL_FAULT ON FL_FAULT_TRADE.FaultId = FL_FAULT.FaultID
			INNER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
			INNER JOIN FL_AREA ON FL_FAULT.AREAID = FL_AREA.AreaID
			INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
	WHERE
		
		(FL_TEMP_FAULT.SchemeId = @schemeId OR @schemeId IS NULL)
		AND (FL_TEMP_FAULT.BlockId = @blockId OR @blockId IS NULL)		
		AND FL_TEMP_FAULT.ISRECALL = @isRecall