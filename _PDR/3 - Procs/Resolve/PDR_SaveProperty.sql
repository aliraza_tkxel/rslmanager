-- Stored Procedure

/* =================================================================================    
    Page Description:  Save property in database

    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date                   By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza         Save property in database
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_SaveProperty]

@DevelopmentId int,
@BlockId int,
@PhaseId int,
@HouseNumber nvarchar(50),
@Address1 varchar(500),
@Address2 varchar(500),
@Address3 varchar(50),
@TownCity varchar(50),
@County varchar(50),
@PostCode nvarchar(10),
@Status int,
@SubStatus int,
@PropertyType int,
@AssetType int,
@StockType int,
@PropertyLevel int,
@DateBuilt smalldatetime= null,
@RightToBuy int,
@DefectsPeriodEnd smalldatetime= null,
@OwnerShip int,
@MinPurchase float,
@PurchaseLevel float,
@DwellingType int,
@NROSHAssetTypeMain int,
@NROSHAssetTypeSub int,
@PropertyValue float,
@IsTemplate bit,
@TemplateName nvarchar(200),
@LeaseStart smalldatetime= null,
@LeaseEnd smalldatetime= null,
@FloodingRisk bit,
@DateAcquired smalldatetime= null,
@viewingcommencement smalldatetime= null,
@Valuationdate smalldatetime= null,
@GroundRent nvarchar(200) ,
@LandLord nvarchar(200) ,
@RentReview smalldatetime= null,
@ExistingPropertyID nvarchar(200)= null,
@PropertyId nvarchar(200)= null output 	
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY 

	if (@PhaseId = -1)
	begin
		set @PhaseId =null
	end

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	IF @ExistingPropertyID = '' OR @ExistingPropertyID IS NULL 
	BEGIN
			IF Not Exists(select top 1 * from P__Property Where(HOUSENUMBER = @HouseNumber OR FLATNUMBER=@HouseNumber)
		And ADDRESS1= @Address1 And TOWNCITY = @TownCity) 
			BEGIN

				Declare @PID nvarchar(20)

				SELECT @PID = convert(varchar(20),'BHA'+Convert(varchar(20),RIGHT('00000' + RTRIM(P_IDENTITY+1), 7))) FROM P_PROPERTY_IDENTITY 
				SELECT @PID
				SET NOCOUNT ON;

				INSERT INTO P__PROPERTY(PROPERTYID,DEVELOPMENTID ,BLOCKID ,HOUSENUMBER  ,ADDRESS1 ,ADDRESS2 ,ADDRESS3 ,TOWNCITY ,COUNTY ,POSTCODE ,STATUS ,SUBSTATUS ,
				PROPERTYTYPE ,ASSETTYPE ,STOCKTYPE ,PROPERTYLEVEL ,DATEBUILT   ,RIGHTTOBUY ,DEFECTSPERIODEND ,OWNERSHIP ,MINPURCHASE ,PURCHASELEVEL ,
				DWELLINGTYPE ,NROSHASSETTYPEMAIN ,NROSHASSETTYPESUB ,PROPERTYVALUE ,IsTemplate ,TemplateName,PhaseId,LeaseStart,LeaseEnd,FloodingRisk,DateAcquired,
				viewingcommencement,Valuationdate,GroundRent,LandLord,RentReview)

				VALUES(@PID,@DevelopmentId ,@BlockId ,@HouseNumber  ,@Address1 ,@Address2 ,@Address3 ,@TownCity ,@County ,@PostCode ,@Status ,@SubStatus ,@PropertyType ,
				@AssetType ,@StockType ,@PropertyLevel ,@DateBuilt  ,@RightToBuy ,@DefectsPeriodEnd ,@OwnerShip ,@MinPurchase ,@PurchaseLevel ,
				@DwellingType ,@NROSHAssetTypeMain ,@NROSHAssetTypeSub ,@PropertyValue ,@IsTemplate ,@TemplateName,@PhaseId,@LeaseStart,@LeaseEnd,@FloodingRisk,@DateAcquired,
				@viewingcommencement,@Valuationdate,@GroundRent,@LandLord,@RentReview)
				SET @PropertyId = @PID

				--- Update P_PROPERTY_IDENTITY table
				UPDATE P_PROPERTY_IDENTITY SET P_IDENTITY = (SELECT P_IDENTITY + 1 FROM P_PROPERTY_IDENTITY)
		   END
		   ELSE
			   BEGIN
			   SET @PropertyId='Exist'
			   END
	END
ELSE
	BEGIN
	UPDATE P__PROPERTY SET 
	DEVELOPMENTID=@DevelopmentId ,BLOCKID=@BlockId ,HOUSENUMBER=@HouseNumber  ,ADDRESS1=@Address1 ,ADDRESS2 =@Address2,ADDRESS3=@Address3 ,TOWNCITY=@TownCity ,COUNTY=@County ,POSTCODE=@PostCode ,STATUS=@Status ,SUBSTATUS=@SubStatus ,
	PROPERTYTYPE=@PropertyType ,ASSETTYPE=@AssetType ,STOCKTYPE=@StockType ,PROPERTYLEVEL=@PropertyLevel ,DATEBUILT=@DateBuilt,RIGHTTOBUY=@RightToBuy ,DEFECTSPERIODEND=@DefectsPeriodEnd ,OWNERSHIP=@OwnerShip ,MINPURCHASE=@MinPurchase ,PURCHASELEVEL=@PurchaseLevel ,
	DWELLINGTYPE=@DwellingType ,NROSHASSETTYPEMAIN=@NROSHAssetTypeMain ,NROSHASSETTYPESUB=@NROSHAssetTypeSub ,PROPERTYVALUE=@PropertyValue ,IsTemplate=@IsTemplate ,TemplateName=@TemplateName,PhaseId=@PhaseId,LeaseStart=@LeaseStart,LeaseEnd=@LeaseEnd,FloodingRisk=@FloodingRisk,DateAcquired=@DateAcquired,
	viewingcommencement=@viewingcommencement,Valuationdate=@Valuationdate,GroundRent=@GroundRent,LandLord=@LandLord,RentReview=@RentReview 
	Where PROPERTYID = @ExistingPropertyID

	SET @PropertyId=@ExistingPropertyID
	END	   
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @PropertyId='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  

IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;

	END 
END
GO