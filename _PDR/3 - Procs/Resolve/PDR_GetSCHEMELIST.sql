USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetSCHEMELIST]    Script Date: 12/17/2014 10:31:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Scheme listing page
 
    Author: Salman
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-09-2014      Salman         Get Scheme list on Scheme listing page 
  =================================================================================*/
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetSCHEMELIST]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'SCHEMENAME', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( S.SCHEMENAME  LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR D.DEVELOPMENTNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR D.ADDRESS1 LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR D.ADDRESS2 LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 SCHEMEID,ISNULL(S.SCHEMENAME,'' '') as SCHEMENAME
							,ISNULL(D.DEVELOPMENTNAME,'' '') as DEVELOPMENT
							, (ISNULL(ADDRESS1,'' '')+'' ''+ISNULL(ADDRESS2,'' ''))AS ADDRESS'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' FROM P_SCHEME S
						   INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = S.DevelopmentID  '
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'DEVELOPMENT')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' DEVELOPMENTNAME' 	
			
		END
		
		IF(@sortColumn = 'ADDRESS')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ADDRESS' 	
			
		END
		IF(@sortColumn = 'SCHEMENAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'SCHEMENAME' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
