USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AmendPropertyItemDetail]    Script Date: 01/19/2015 16:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
CREATE PROCEDURE [dbo].[AS_AmendPropertyItemDetail]   
  -- Add the parameters for the stored procedure here          
  @PropertyId    VARCHAR(100),   
  @ItemId        INT,   
  @UpdatedBy     INT,   
  @ItemDetail    AS AS_ITEMDETAILS readonly,   
  @ItemDates     AS AS_ITEMDATES readonly,   
  @ConditionWork AS AS_CONDITIONRATINGLIST readonly ,
  @MSATDetail AS AS_MSATDETAIL readonly   
AS   
  BEGIN   
      -- SET NOCOUNT ON added to prevent extra result sets from          
      -- interfering with SELECT statements.          
      SET nocount ON;   
  
      ---Begin Iterate  @ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES        
      --=================================================================================      
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES          
      DECLARE @minItemParamId     INT,   
              @attributeId        INT,   
              @itemParamId        INT,   
              @parameterValue     NVARCHAR(1000),   
              @valueId            INT,   
              @isCheckBoxSelected BIT,   
              @Count              INT,   
              @ControlType        NVARCHAR(50),   
              @ParameterName      NVARCHAR(50)   
      ---Declare Cursor variable          
      DECLARE @ItemToInsertCursor CURSOR   
  
      --Initialize cursor          
      SET @ItemToInsertCursor = CURSOR fast_forward   
      FOR SELECT itemparamid,   
                 parametervalue,   
                 valueid,   
                 ischeckboxselected   
          FROM   @ItemDetail;   
  
      --Open cursor          
      OPEN @ItemToInsertCursor   
  
      ---fetch row from cursor          
      FETCH next FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue,   
      @valueId, @isCheckBoxSelected   
  
      ---Iterate cursor to get record row by row          
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
            --- Select Control type  of parameter            
            SELECT @ControlType = controltype,   
                   @ParameterName = parametername   
            FROM   pa_parameter   
                   INNER JOIN pa_item_parameter   
                           ON pa_item_parameter.parameterid =   
                              pa_parameter.parameterid   
            WHERE  itemparamid = @itemParamId   
  
            IF @ControlType = 'TextBox'   
                OR @ControlType = 'TextArea'   
              BEGIN   
                  SET @valueId=NULL   
  
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId IS NULL    
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  propertyid = @PropertyId   
                         AND itemparamid = @itemParamId   
                         AND valueid IS NULL   
              END   
            ELSE IF @ControlType = 'CheckBoxes'   
              BEGIN   
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId =@valueId    
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  propertyid = @PropertyId   
                         AND itemparamid = @itemParamId   
                         AND valueid = @valueId   
              END   
            ELSE   
              BEGIN   
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId     
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  propertyid = @PropertyId   
                         AND itemparamid = @itemParamId   
              END   
  
            -- if record exist update that row otherwise insert a new record             
            IF @attributeId > 0   
              BEGIN   
                  IF EXISTS(SELECT worksrequired   
                            FROM   @ConditionWork)   
                     AND EXISTS(SELECT *   
                                FROM   planned_conditionworks   
                                WHERE  attributeid = @attributeId)   
                     AND Upper(@ParameterName) = Upper('Condition Rating')   
                    BEGIN   
                        DECLARE @ConditionWorksId INT,   
                                @WorkCompnentId   INT   
                        DECLARE db_cursor_conditionworks CURSOR FOR   
                          SELECT conditionworksid,   
                                 componentid   
                          FROM   planned_conditionworks   
                          WHERE  attributeid = @attributeId   
  
                        OPEN db_cursor_conditionworks   
  
                        FETCH next FROM db_cursor_conditionworks INTO   
                        @ConditionWorksId,   
                        @WorkCompnentId   
  
                        WHILE @@FETCH_STATUS = 0   
                          BEGIN   
                              DECLARE @ConditionWorksCount INT   
  
                              SELECT @ConditionWorksCount = Count(*)   
                              FROM   @ConditionWork   
  
                              IF @ConditionWorksCount = 1   
                                BEGIN   
                                    SET @WorkCompnentId =   
                                    (SELECT TOP 1 componentid   
                                     FROM   @ConditionWork)   
  
                                    IF @WorkCompnentId = 0   
                                      BEGIN   
                                          SET @WorkCompnentId =NULL   
                                      END   
                                END   
  
                              UPDATE planned_conditionworks   
                              SET    worksrequired = (SELECT TOP 1 worksrequired   
                                                      FROM   @ConditionWork),   
                                     conditionaction =   
                                     (SELECT actionid   
                                      FROM   planned_action   
                                      WHERE  title = 'Recommended'),   
                                     componentid = @WorkCompnentId,   
                                     modifiedby = @UpdatedBy,   
                                     modifieddate = Getdate()   
                              WHERE  conditionworksid = @ConditionWorksId   
  
                              INSERT INTO planned_conditionworks_history   
                                          (conditionworksid,   
                                           worksrequired,   
                                           attributeid,   
                                           valueid,   
                                           conditionaction,   
                                           componentid,   
                                           createdby,   
                                           createddate)   
                              VALUES      (@ConditionWorksId,   
                                           (SELECT TOP 1 worksrequired   
                                            FROM   @ConditionWork),   
                                           @attributeId,   
                                           @valueId,   
                                           (SELECT actionid   
                                            FROM   planned_action   
                                            WHERE  title = 'Recommended'),   
                                           @WorkCompnentId,   
                                           @UpdatedBy,   
                                           Getdate())   
  
                              FETCH next FROM db_cursor_conditionworks INTO   
                              @ConditionWorksId,   
                              @WorkCompnentId   
                          END   
  
                        CLOSE db_cursor_conditionworks   
  
                        DEALLOCATE db_cursor_conditionworks   
                    END   
                  ELSE IF Upper(@ParameterName) = Upper('Condition Rating')   
                    BEGIN   
                        DECLARE @conditionRatingWorksRequired NVARCHAR(4000),   
                                @conditionRatingComponentId   INT,   
                                @conditionRatingValueId       INT   
                        DECLARE db_cursor_insertconditionworks CURSOR FOR   
                          SELECT componentid,   
                                 worksrequired,   
                                 valueid   
                          FROM   @ConditionWork   
  
                        OPEN db_cursor_insertconditionworks   
  
                        FETCH next FROM db_cursor_insertconditionworks INTO   
                        @conditionRatingComponentId,   
                        @conditionRatingWorksRequired   
                        ,   
                        @conditionRatingValueId   
  
                        WHILE @@FETCH_STATUS = 0   
                          BEGIN   
                              IF @conditionRatingComponentId = 0   
                                BEGIN   
                                    SET @conditionRatingComponentId =NULL   
                                END   
  
                              INSERT INTO planned_conditionworks   
                                          (worksrequired,   
                                           attributeid,   
                                           conditionaction,   
                                           componentid,   
                                           createdby,   
                                           createddate)   
                              VALUES      (@conditionRatingWorksRequired,   
                                           @attributeId,   
                                           (SELECT actionid   
                                            FROM   planned_action   
                                            WHERE  title = 'Recommended'),   
                                           @conditionRatingComponentId,   
                                           @UpdatedBy,   
                                           Getdate())   
  
                              INSERT INTO planned_conditionworks_history   
                                          (conditionworksid,   
                                           worksrequired,   
                                           attributeid,   
                                           valueid,   
                                           conditionaction,   
                                           componentid,   
                                           createdby,   
                                           createddate)   
                              VALUES      (Scope_identity(),   
                                           @conditionRatingWorksRequired,   
                                           @attributeId,   
                                           @conditionRatingValueId,   
                                           (SELECT actionid   
                                            FROM   planned_action   
                                            WHERE  title = 'Recommended'),   
                                           @conditionRatingComponentId,   
                                           @UpdatedBy,   
                                           Getdate())   
  
                              FETCH next FROM db_cursor_insertconditionworks   
                              INTO   
                              @conditionRatingComponentId,   
                              @conditionRatingWorksRequired   
                              ,   
                 @conditionRatingValueId   
                          END   
  
                        CLOSE db_cursor_insertconditionworks   
  
                        DEALLOCATE db_cursor_insertconditionworks   
                    END   
  
                  UPDATE pa_property_attributes   
                  SET    itemparamid = @itemParamId,   
                         parametervalue = @parameterValue,   
                         valueid = @valueId,   
                         updatedon = Getdate(),   
                         updatedby = @UpdatedBy,   
                         ischeckboxselected = @isCheckBoxSelected   
                  WHERE  attributeid = @attributeId   
              END   
            ELSE   
              BEGIN   
                  DECLARE @latestAttributeId INT   
  
                  INSERT INTO pa_property_attributes   
                              (propertyid,   
                               itemparamid,   
                               parametervalue,   
                               valueid,   
                               updatedon,   
                               updatedby,   
                               ischeckboxselected)   
                  VALUES      (@PropertyId,   
                               @itemParamId,   
                               @parameterValue,   
                               @valueId,   
                               Getdate(),   
                               @UpdatedBy,   
                               @isCheckBoxSelected)   
  
                  SET @latestAttributeId = Scope_identity()   
  
                  IF EXISTS(SELECT worksrequired   
                            FROM   @ConditionWork)   
                     AND Upper(@ParameterName) = Upper('Condition Rating')   
                    BEGIN   
                        DECLARE @conditionWorksRequired NVARCHAR(4000),   
                                @conditionComponentId   INT,   
                                @conditionValueId       INT   
                        DECLARE db_cursor_insertconditionworks CURSOR FOR   
                          SELECT componentid,   
                                 worksrequired,   
                                 valueid   
                          FROM   @ConditionWork   
  
                        OPEN db_cursor_insertconditionworks   
  
                        FETCH next FROM db_cursor_insertconditionworks INTO   
                        @conditionComponentId,   
                        @conditionWorksRequired, @conditionValueId   
  
                        WHILE @@FETCH_STATUS = 0   
                          BEGIN   
                              IF @conditionComponentId = 0   
                                BEGIN   
                                    SET @conditionComponentId =NULL   
                                END   
  
                              INSERT INTO planned_conditionworks   
                                          (worksrequired,   
                                           attributeid,   
                                           conditionaction,   
                                           componentid,   
                                           createdby,   
                                           createddate)   
                              VALUES      (@conditionWorksRequired,   
                                           @latestAttributeId,   
                                           (SELECT actionid   
                                            FROM   planned_action   
                                            WHERE  title = 'Recommended'),   
                                           @conditionComponentId,   
                                           @UpdatedBy,   
                                           Getdate())   
  
                              INSERT INTO planned_conditionworks_history   
                                          (conditionworksid,   
                                           worksrequired,   
                                           attributeid,   
                                       valueid,   
                                           conditionaction,   
                                           componentid,   
                                           createdby,   
                                           createddate)   
                              VALUES      (Scope_identity(),   
                                           @conditionWorksRequired,   
                                           @latestAttributeId,   
                                           @conditionValueId,   
                                           (SELECT actionid   
                                            FROM   planned_action   
                                            WHERE  title = 'Recommended'),   
                                           @conditionComponentId,   
                                           @UpdatedBy,   
                                           Getdate())   
  
                              FETCH next FROM db_cursor_insertconditionworks   
                              INTO   
                              @conditionComponentId,   
                              @conditionWorksRequired, @conditionValueId   
                          END   
  
                        CLOSE db_cursor_insertconditionworks   
  
                        DEALLOCATE db_cursor_insertconditionworks   
                    END   
              END   
  
            FETCH next FROM @ItemToInsertCursor INTO @itemParamId,   
            @parameterValue   
            ,   
            @valueId, @isCheckBoxSelected   
        END   
  
      --close & deallocate cursor            
      CLOSE @ItemToInsertCursor   
  
      DEALLOCATE @ItemToInsertCursor   
  
      --Strart Iterate ItemDates to insert or update data in           
      --=====================================================================================          
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES          
      DECLARE @sid         INT,   
              @parameterId INT,   
              @lastDone    DATETIME,   
              @dueDate     DATETIME,   
              @componentId INT,   
              @psid        INT   
  
     
      ---Declare Cursor variable          
      DECLARE @ItemToInsertDateCursor CURSOR   
   
      --Initialize cursor          
      SET @ItemToInsertDateCursor = CURSOR fast_forward   
      FOR SELECT ParameterId,   
                  convert( Datetime, LastDone, 103 )as LastDone,   
                 convert( Datetime,DueDate, 103 )as DueDate,   
                 ComponentId   
          FROM   @ItemDates;   
  
      --Open cursor          
      OPEN @ItemToInsertDateCursor   
  
      ---fetch row from cursor          
      FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone,   
      @dueDate, @componentId   
  
      ---Iterate cursor to get record row by row          
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
           IF @lastDone = '1900-01-01 00:00:00.000'   
               OR @lastDone IS NULL   
              SET @lastDone=NULL   
  
            IF @dueDate = '1900-01-01 00:00:00.000'   
               OR @dueDate IS NULL   
              SET @dueDate=NULL   
  
            SELECT @sid = [sid]   
            FROM   pa_property_item_dates   
            WHERE  propertyid = @PropertyId   
                   AND itemid = @ItemId   
                   AND  parameterid = @parameterId    
  
            SELECT @psid = [sid]   
            FROM   pa_property_item_dates   
            WHERE  propertyid = @PropertyId   
                   AND itemid = @ItemId   
                   AND ( parameterid = 0   
                          OR parameterid IS NULL )   
                   AND ( lastdone IS NOT NULL   
                   AND duedate IS NOT NULL )  
  
            IF @psid > 0 And ( @parameterId = 0 OR @parameterId IS NULL )   
              BEGIN   
                  
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,   
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy,   
                               planned_componentid = @componentId   
                        WHERE  [sid] = @psid   
                    
              END   
            ELSE IF @sid > 0   
              BEGIN   
                  IF @lastDone IS NOT NULL   
                     AND @dueDate IS NOT NULL   
                    BEGIN   
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,   
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy,   
                               planned_componentid = @componentId   
                        WHERE  [sid] = @sid   
                    END   
                  ELSE IF @dueDate IS NOT NULL   
                    BEGIN   
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,    
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy,   
                               planned_componentid = @componentId   
                        WHERE  [sid] = @sid   
                    END   
              END   
            ELSE   
              BEGIN   
                  IF @lastDone IS NOT NULL   
                     AND @dueDate IS NOT NULL   
                    BEGIN   
                        INSERT INTO pa_property_item_dates   
                                    (propertyid,   
                                     itemid,   
                                     lastdone,   
                                     duedate,   
                                     updatedon,   
                                     updatedby,   
                                     parameterid,   
                                     planned_componentid)   
                        VALUES      ( @PropertyId,   
                                      @ItemId,   
                                      @lastDone,   
                                      @dueDate,   
                                      Getdate(),   
                                      @UpdatedBy,   
                                      @parameterId,   
                                      @componentId)   
                    END   
                  ELSE IF @dueDate IS NOT NULL   
                    BEGIN   
                        INSERT INTO pa_property_item_dates   
                                    (propertyid,   
                                     itemid,   
                                     duedate,   
                                     updatedon,   
                                     updatedby,   
                                     parameterid,   
                                     planned_componentid)   
                        VALUES      ( @PropertyId,   
                                      @ItemId,   
                                       @dueDate,    
                                      Getdate(),   
                                      @UpdatedBy,   
                                      @parameterId,   
                                      @componentId)   
                    END   
              END   
     
            FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone,   
            @dueDate, @componentId   
        END   
  
      --close & deallocate cursor            
      CLOSE @ItemToInsertDateCursor   
  
      DEALLOCATE @ItemToInsertDateCursor 
      
      --Change : Added @UpdatedBy 
      --By : Ahmed
      Exec PDR_AmendpMSTAItemDetail @PropertyId,null,null,@ItemId,@UpdatedBy,@MSATDetail 
  END   