USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_AmendpMSTAItemDetail]    Script Date: 01/14/2015 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Page Description:     Get AmendpMSTA Item Detail For DropDown

  Author: Ali Raza
  Creation Date: Dec-30-2014

  Change History:

  Version      Date             By                      Description
  =======     ============    ========           ===========================
  v1.0         Dec-30-2014      Ali Raza           Get AmendpMSTA Item Detail For DropDown
  v1.1  	 Jan-1-2015		Ahmed Mehmood		Added INSERT AND UPDATE query of PDR_JOURNAL table.
  Execution Command:
  
  Exec PDR_AmendpMSTAItemDetail
=================================================================================*/
CREATE PROCEDURE [dbo].[PDR_AmendpMSTAItemDetail]
-- Add the parameters for the stored procedure here            
@PropertyId varchar(100) = NULL,
@schemeId int = NULL,
@blockId int = NULL,
@ItemId int,
@UpdatedBy int,
@MSATDetail AS AS_MSATDETAIL READONLY

AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from            
  -- interfering with SELECT statements.            
  SET NOCOUNT ON;
  --SELECT * FROM PDR_MSAT
  ---Begin Iterate  @ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES          
  --=================================================================================        
  ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES            
  DECLARE @IsRequired bit,
          @LastDate datetime,
          @Cycle int,
          @CycleTypeId int,
          @NextDate datetime,
          @AnnualApportionment float,
          @MSATTypeId int,
          @MSATId int,
          @ToBeArrangedStatusId int,
          @CompletedStatus int                                                                                           
  ---Declare Cursor variable            
  DECLARE @ItemToInsertCursor CURSOR

  SELECT
    @ToBeArrangedStatusId = PDR_STATUS.STATUSID
  FROM PDR_STATUS
  WHERE TITLE = 'To be Arranged'
  
SELECT
    @CompletedStatus = PDR_STATUS.STATUSID
  FROM PDR_STATUS
  WHERE TITLE = 'Completed'
  
  --Initialize cursor            
  SET @ItemToInsertCursor = CURSOR FAST_FORWARD
  FOR
  SELECT
    [isrequired],
    [lastdate],
    [cycle],
    [cycletypeid],
    [nextdate],
    [annualapportionment],
    [msattypeid]
  FROM @MSATDetail;

  --Open cursor            
  OPEN @ItemToInsertCursor

  ---fetch row from cursor            
  FETCH NEXT FROM @ItemToInsertCursor INTO @IsRequired, @LastDate, @Cycle,
  @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId

  ---Iterate cursor to get record row by row            
  WHILE @@FETCH_STATUS = 0
  BEGIN
    SET @MSATId = 0
    SELECT
      @MSATId = MSATId
    FROM PDR_MSAT
    WHERE (PropertyId = @PropertyId
    OR @PropertyId IS NULL)
    AND (SchemeId = @schemeId
    OR @schemeId IS NULL)
    AND (BlockId = @blockId
    OR @blockId IS NULL)
    AND ItemId = @ItemId
    AND MSATTypeId = @MSATTypeId

    IF @LastDate = '1900-01-01 00:00:00.000'
      OR @LastDate IS NULL
      SET @LastDate = NULL

    IF @NextDate = '1900-01-01 00:00:00.000'
      OR @NextDate IS NULL
      SET @NextDate = NULL
    IF @MSATId > 0
    BEGIN

      UPDATE PDR_MSAT
      SET CycleTypeId = @CycleTypeId,
          IsRequired = @IsRequired,
          Cycle = @Cycle,
          LastDate = @LastDate,
          NextDate = @NextDate,
          AnnualApportionment = @AnnualApportionment,
          IsActive = 1
      WHERE MSATId = @MSATId
 IF @IsRequired = 1
      BEGIN
		  UPDATE [PDR_JOURNAL]
		  SET [STATUSID] = @ToBeArrangedStatusId,
			  [CREATIONDATE] = GETDATE(),
			  [CREATEDBY] = @UpdatedBy
		  WHERE MSATId = @MSATId And STATUSID IN (@ToBeArrangedStatusId,@CompletedStatus)
	  END		
    END
    ELSE
    BEGIN

      INSERT INTO PDR_MSAT (IsRequired, PropertyId, ItemId, LastDate, Cycle, CycleTypeId, NextDate, AnnualApportionment, MSATTypeId, IsActive, SchemeId, BlockId)
        VALUES (@IsRequired, @PropertyId, @ItemId, @LastDate, @Cycle, @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId, 1, @schemeId, @blockId)

      SELECT
        @MSATId = SCOPE_IDENTITY()
      IF @IsRequired = 1
      BEGIN
        INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
          VALUES (@MSATId, @ToBeArrangedStatusId, GETDATE(), @UpdatedBy)
      END
    END

    FETCH NEXT FROM @ItemToInsertCursor INTO @IsRequired, @LastDate,
    @Cycle, @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId

  END

  --close & deallocate cursor              
  CLOSE @ItemToInsertCursor


  DEALLOCATE @ItemToInsertCursor

END