-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,16-Jan-2015>
-- Description:	<Description,,NoEntry Counts on ME Dashboard >
-- EXEC PDR_GetCountNoEntry -1,-1
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetCountNoEntry]
@schemeId int =-1,
@blockId int =-1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @statusId int

	Select @statusId = STATUSID from PDR_STATUS Where TITLE = 'No Entry'

Select COUNT(A.APPOINTMENTID) from 
			PDR_APPOINTMENT_HISTORY A
			INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
			INNER JOIN PDR_MSAT MS on MS.MSATId = J.MSATID
			INNER JOIN PDR_STATUS S on S.STATUSID = J.STATUSID

			Where S.STATUSID = @statusId
			AND (MS.SchemeId = @schemeId
			OR MS.BlockId = @blockId)

END
GO