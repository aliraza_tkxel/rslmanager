SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 29/01/2015
-- Description:	To get detail to send email to contractor, details are: contractor details, property details, tenant details, tenant risk details
-- Web Page:	AssignToContractor.ascx(User Control)(Details for email)
-- =============================================
CREATE PROCEDURE PDR_GetDetailforEmailToContractor

	@journalId INT
	,@empolyeeId INT
		
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @empolyeeId

--=================================================
--Get Property Detail(s)
--=================================================
SELECT
	P__PROPERTY.PROPERTYID,
	HOUSENUMBER,
	FLATNUMBER,
	P__PROPERTY.ADDRESS1,
	P__PROPERTY.ADDRESS2,
	P__PROPERTY.ADDRESS3,
	P__PROPERTY.TOWNCITY,
	P__PROPERTY.COUNTY,
	P__PROPERTY.POSTCODE,
	ISNULL(NULLIF(ISNULL('Flat No:' + P__PROPERTY.FLATNUMBER + ', ', '') + ISNULL(P__PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + P__PROPERTY.ADDRESS1, '')
	+ ISNULL(' ' + P__PROPERTY.ADDRESS2, '') + ISNULL(' ' + P__PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress]
	,P_BLOCK.BLOCKNAME AS Block
	,P_SCHEME.SCHEMENAME as Scheme
FROM	PDR_JOURNAL
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PropertyId
		LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID
		LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
WHERE JOURNALID = @journalId

--=================================================
-- Get CustomerId to get Tenant Detail(s) and Risk
--=================================================

DECLARE @customerId int = -1

SELECT
	@customerId = ISNULL(CW.CustomerId, -1)
FROM PLANNED_CONTRACTOR_WORK CW
WHERE CW.JournalId = @journalId


SELECT	@customerId = ISNULL(C__CUSTOMER.CustomerId, -1)		
FROM	PDR_JOURNAL
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN P__PROPERTY ON  PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID 
		INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID  AND C_TENANCY.ENDDATE IS NULL
		INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
										AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					)
		INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
WHERE	PDR_JOURNAL.JOURNALID = @journalId

--=================================================
--Get Customer/Tenant Detail(s)
--=================================================

SELECT
	C.CUSTOMERID AS [CUSTOMERID],
	FIRSTNAME AS [FIRSTNAME],
	MIDDLENAME AS [MIDDLENAME],
	LASTNAME AS [LASTNAME],
	TEL AS [TEL],
	T.DESCRIPTION [TITLE],
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '') + ISNULL(' ' + C.MIDDLENAME, '')
	+ ISNULL(' ' + C.LASTNAME, '') + ISNULL(', Tel:' + A.TEL, ''), ''), 'N/A') AS ContactDetail,
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '')
	+ ISNULL(' ' + C.LASTNAME, ''), ''), 'N/A') [FullName]
FROM	C__CUSTOMER C
		INNER JOIN G_TITLE T ON C.TITLE = T.TITLEID
		INNER JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1
WHERE	C.CUSTOMERID = @customerId

--=================================================
--Get Customer/Tenant Risk Detail(s)
--=================================================
SELECT
	RISKHISTORYID,
	CATDESC,
	SUBCATDESC
FROM dbo.RISK_CATS_SUBCATS(@customerId)

--=================================================
--Get Customer/Tenant Vulnerability Detail(s)
--=================================================
DECLARE @VULNERABILITYHISTORYID int = 0

SELECT
	@VULNERABILITYHISTORYID = VULNERABILITYHISTORYID
FROM C_JOURNAL J
INNER JOIN C_VULNERABILITY CV
	ON CV.JOURNALID = J.JOURNALID
WHERE CUSTOMERID = @customerId
AND ITEMNATUREID = 61
AND CV.ITEMSTATUSID <> 14
AND CV.VULNERABILITYHISTORYID = (SELECT
	MAX(VULNERABILITYHISTORYID)
FROM C_VULNERABILITY IN_CV
WHERE IN_CV.JOURNALID = J.JOURNALID)

EXECUTE VULNERABILITY_CAT_SUBCAT @VULNERABILITYHISTORYID

END
GO