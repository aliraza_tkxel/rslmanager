-- Stored Procedure

   /* =================================================================================    
    Page Description:    save the detail of tree leaf node controls

    Author: Ali Raza
    Creation Date: jan-14-2015  

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         jan-14-2015      Ali Raza            save the detail of tree leaf node controls


  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_AmendAttributeItemDetail]   
  -- Add the parameters for the stored procedure here          
  @schemeId		 INT=null, 
  @blockId		 INT=null,  
  @ItemId        INT,   
  @UpdatedBy     INT,   
  @ItemDetail    AS AS_ITEMDETAILS readonly,   
  @ItemDates     AS AS_ITEMDATES readonly,   
  @ConditionWork AS AS_CONDITIONRATINGLIST readonly ,
  @MSATDetail AS AS_MSATDETAIL readonly   
AS   
  BEGIN   
      -- SET NOCOUNT ON added to prevent extra result sets from          
      -- interfering with SELECT statements.          
      SET nocount ON;   

      ---Begin Iterate  @ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES        
      --=================================================================================      
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES          
      DECLARE @minItemParamId     INT,   
              @attributeId        INT,   
              @itemParamId        INT,   
              @parameterValue     NVARCHAR(1000),   
              @valueId            INT,   
              @isCheckBoxSelected BIT,   
              @Count              INT,   
              @ControlType        NVARCHAR(50),   
              @ParameterName      NVARCHAR(50)   
      ---Declare Cursor variable          
      DECLARE @ItemToInsertCursor CURSOR   

      --Initialize cursor          
      SET @ItemToInsertCursor = CURSOR fast_forward   
      FOR SELECT itemparamid,   
                 parametervalue,   
                 valueid,   
                 ischeckboxselected   
          FROM   @ItemDetail;   

      --Open cursor          
      OPEN @ItemToInsertCursor   

      ---fetch row from cursor          
      FETCH next FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue,   
      @valueId, @isCheckBoxSelected   

      ---Iterate cursor to get record row by row          
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
            --- Select Control type  of parameter            
            SELECT @ControlType = controltype,   
                   @ParameterName = parametername   
            FROM   pa_parameter   
                   INNER JOIN pa_item_parameter   
                           ON pa_item_parameter.parameterid =   
                              pa_parameter.parameterid   
            WHERE  itemparamid = @itemParamId   

            IF @ControlType = 'TextBox'   
                OR @ControlType = 'TextArea'   
              BEGIN   
                  SET @valueId=NULL   

                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId IS NULL    
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)  
                         AND itemparamid = @itemParamId   
                         AND VALUEID IS NULL   
              END   
            ELSE IF @ControlType = 'CheckBoxes'   
              BEGIN   
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId =@valueId    
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)   
                         AND itemparamid = @itemParamId   
                         AND valueid = @valueId   
              END   
            ELSE   
              BEGIN   
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId     
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)   
                         AND itemparamid = @itemParamId   
              END   

            -- if record exist update that row otherwise insert a new record             
            IF @attributeId > 0   
              BEGIN   

                  UPDATE pa_property_attributes   
                  SET    itemparamid = @itemParamId,   
                         parametervalue = @parameterValue,   
                         valueid = @valueId,   
                         updatedon = Getdate(),   
                         updatedby = @UpdatedBy,   
                         ischeckboxselected = @isCheckBoxSelected   
                  WHERE  attributeid = @attributeId   
              END   
            ELSE   
              BEGIN   
                  DECLARE @latestAttributeId INT   

                  INSERT INTO pa_property_attributes   
                              (propertyid,   
                               itemparamid,   
                               parametervalue,   
                               valueid,   
                               updatedon,   
                               updatedby,   
                               ischeckboxselected,   
                               SchemeId,
                               BlockId )   
                  VALUES      (NULL,   
                               @itemParamId,   
                               @parameterValue,   
                               @valueId,   
                               Getdate(),   
                               @UpdatedBy,   
                               @isCheckBoxSelected,
                               @schemeId,
                               @blockId  )   

                  SET @latestAttributeId = Scope_identity()   


              END   

            FETCH next FROM @ItemToInsertCursor INTO @itemParamId,   
            @parameterValue   
            ,   
            @valueId, @isCheckBoxSelected   
        END   

      --close & deallocate cursor            
      CLOSE @ItemToInsertCursor   

      DEALLOCATE @ItemToInsertCursor   

      --Strart Iterate ItemDates to insert or update data in           
      --=====================================================================================          
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES          
      DECLARE @sid         INT,   
              @parameterId INT,   
              @lastDone    DATETIME,   
              @dueDate     DATETIME,   
              @componentId INT,   
              @psid        INT   


      ---Declare Cursor variable          
      DECLARE @ItemToInsertDateCursor CURSOR   

      --Initialize cursor          
      SET @ItemToInsertDateCursor = CURSOR fast_forward   
      FOR SELECT ParameterId,   
                  convert( Datetime, LastDone, 103 )as LastDone,   
                 convert( Datetime,DueDate, 103 )as DueDate,   
                 ComponentId   
          FROM   @ItemDates;   

      --Open cursor          
      OPEN @ItemToInsertDateCursor   

      ---fetch row from cursor          
      FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone,   
      @dueDate, @componentId   

      ---Iterate cursor to get record row by row          
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
           IF @lastDone = '1900-01-01 00:00:00.000'   
               OR @lastDone IS NULL   
              SET @lastDone=NULL   

            IF @dueDate = '1900-01-01 00:00:00.000'   
               OR @dueDate IS NULL   
              SET @dueDate=NULL   

            SELECT @sid = [sid]   
            FROM   pa_property_item_dates   
            WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)     
                   AND itemid = @ItemId   
                   AND  parameterid = @parameterId    

            SELECT @psid = [sid]   
            FROM   pa_property_item_dates   
            WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)     
                   AND itemid = @ItemId   
                   AND ( parameterid = 0   
                          OR parameterid IS NULL )   
                   AND ( lastdone IS NOT NULL   
                   AND duedate IS NOT NULL )  

            IF @psid > 0 And ( @parameterId = 0 OR @parameterId IS NULL )   
              BEGIN   

                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,   
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy   
                        WHERE  [sid] = @psid   

              END   
            ELSE IF @sid > 0   
              BEGIN   
                  IF @lastDone IS NOT NULL   
                     AND @dueDate IS NOT NULL   
                    BEGIN   
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,   
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy                                  
                        WHERE  [sid] = @sid   
                    END   
                  ELSE IF @dueDate IS NOT NULL   
                    BEGIN   
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,    
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy  

                        WHERE  [sid] = @sid   
                    END   
              END   
            ELSE   
              BEGIN   
                  IF @lastDone IS NOT NULL   
                     AND @dueDate IS NOT NULL   
                    BEGIN   
                        INSERT INTO pa_property_item_dates   
                                    (propertyid,   
                                     itemid,   
                                     lastdone,   
                                     duedate,   
                                     updatedon,   
                                     updatedby,   
                                     parameterid,   
                                     SchemeId,
                                     BlockId )   
                        VALUES      ( NULL,   
                                      @ItemId,   
                                      @lastDone,   
                                      @dueDate,   
                                      Getdate(),   
                                      @UpdatedBy,   
                                      @parameterId,   
                                      @schemeId,
                                      @blockId)   
                    END   
                  ELSE IF @dueDate IS NOT NULL   
                    BEGIN   
                        INSERT INTO pa_property_item_dates   
                                    (propertyid,   
                                     itemid,   
                                     duedate,   
                                     updatedon,   
                                     updatedby,   
                                     parameterid,
                                     SchemeId,
                                     BlockId   
                                     )   
                        VALUES      ( NULL,   
                                      @ItemId,   
                                       @dueDate,    
                                      Getdate(),   
                                      @UpdatedBy,   
                                      @parameterId,
                                      @schemeId,
                                      @blockId   
                                      )   
                    END   
              END   

            FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone,   
            @dueDate, @componentId   
        END   

      --close & deallocate cursor            
      CLOSE @ItemToInsertDateCursor   

      DEALLOCATE @ItemToInsertDateCursor 


      Exec PDR_AmendpMSTAItemDetail NULL,@schemeId,@blockId,@ItemId,@UpdatedBy,@MSATDetail 
  END   
GO