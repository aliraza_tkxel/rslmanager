-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,15-Jan-2015>
-- Description:	<Description,,Get Arranged Appointments counts>
-- EXEC PDR_GetCountArrangedAppointments -1,-1
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetCountArrangedAppointments]
@schemeId int =-1,
@blockId int =-1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DECLARE @SelectClause varchar(5000),
        @fromClause   varchar(1000),
        @whereClause  varchar(1000),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(8000),        
        @rowNumberQuery varchar(8000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1000)


		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided

		SET @searchCriteria = ' 1=1 '

		IF (@schemeId > 0 AND @blockId > 0)
			BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (( PDR_MSAT.SchemeId = ' + CONVERT(VARCHAR,@schemeId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.SCHEMEID = ' + CONVERT(VARCHAR,@schemeId) + ')'
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR ( PDR_MSAT.blockId = ' + CONVERT(VARCHAR,@blockId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.BLOCKID = ' + CONVERT(VARCHAR,@blockId) + '))'

			END 
		ELSE
			BEGIN


				IF(@schemeId > 0)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PDR_MSAT.SchemeId = ' + CONVERT(VARCHAR,@schemeId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.SCHEMEID = ' + CONVERT(VARCHAR,@schemeId) + ')'
				END 

				IF(@blockId > 0)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PDR_MSAT.blockId = ' + CONVERT(VARCHAR,@blockId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.BLOCKID = ' + CONVERT(VARCHAR,@blockId) + ')'
				END 
			END		


		--These conditions wíll be used in every case

		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND PDR_STATUS.TITLE IN (''Arranged'',''Assigned To Contractor'')
															AND PDR_MSATType.MSATTypeName = ''M&E Servicing'' AND PDR_MSAT.IsRequired = 1 '

		-- End building SearchCriteria clause   
		--========================================================================================

   		SET @SelectClause = 'SELECT count(*) as Count '


		-- Begin building FROM clause

		SET @fromClause = CHAR(10) + ' FROM	PDR_JOURNAL
		LEFT JOIN	PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID 
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		LEFT JOIN	G_TRADE ON PDR_APPOINTMENTS.TRADEID = G_TRADE.TradeId
		INNER JOIN	PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
		LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
		LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
		LEFT JOIN P_BLOCK AS P_PROPERTY_BLOCK ON P__PROPERTY.BLOCKID = P_PROPERTY_BLOCK.BLOCKID
		LEFT JOIN P_SCHEME AS P_PROPERTY_SCHEME ON P__PROPERTY.SCHEMEID = P_PROPERTY_SCHEME.SCHEMEID
		INNER JOIN	PA_ITEM ON PDR_MSAT.ItemId = PA_ITEM.ItemID 
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		LEFT JOIN	E__EMPLOYEE ON  PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID  '

		-- End building From clause
		--======================================================================================== 														  
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause

		PRINT @mainSelectQuery
		EXEC (@mainSelectQuery)	
		-- End building the Count Query
		--========================================================================================

END
GO