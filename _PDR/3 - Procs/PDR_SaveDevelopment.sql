USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveDevelopment]    Script Date: 02/04/2015 17:26:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,02/12/2014>
-- Description:	<Description,,save development on SaveDevelopment.aspx>
-- =============================================
CREATE PROCEDURE [dbo].[PDR_SaveDevelopment]
	@DevelopmentId int
	,@DEVELOPMENTNAME varchar(50)
	,@PATCH int
	,@ADDRESS1 varchar(500)
	,@ADDRESS2 varchar(500)
	,@PostCode varchar(50)
	,@TownCity varchar(50)
	,@COUNTY varchar(50)
	,@Architect varchar(50),@PROJECTMANAGER varchar(50)
	,@CompanyId int
	,@DevelopmentType int,@DevelopmentStatus int
	,@LandValue float,@PurchaseDate smalldatetime
    ,@GrantAmount float,@BorrowedAmount float,@OutlinePlanningApplication smalldatetime
    ,@DetailedPlanningApplication smalldatetime,@OutlinePlanningApproval smalldatetime
    ,@DetailedPlanningApproval smalldatetime,@NoofUnits int
    ,@developmentFundingDetail AS PDR_DevelopmentFundingInfo READONLY
    ,@UpdatedDevelopmentId varchar(50) Output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	--===================================
	-- Setting null to default values
	--===================================
	IF(@LandValue = -1)
	BEGIN
		SET @LandValue = null;
	END
	
	IF(@GrantAmount= -1 )
	BEGIN
		SET @GrantAmount = null;
	END
	
	IF(@BorrowedAmount = -1)
	BEGIN
		SET @BorrowedAmount = null;
	END 
	
	IF( @NoofUnits = -1)
	BEGIN
		SET @NoofUnits = null;
	END
	
	
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY 	
	
	--=======================================================
	-- DEVELOPMENT INFO
	--=======================================================
	
	IF (@DevelopmentId > 0)
	BEGIN
	
	UPDATE [RSLBHALive].[dbo].[PDR_DEVELOPMENT]
   SET [PATCHID] = @PATCH
      ,[DEVELOPMENTNAME] = @DEVELOPMENTNAME
      ,[DEVELOPMENTTYPE] = @DevelopmentType
      ,[NUMBEROFUNITS] = @NoofUnits
      ,[PROJECTMANAGER] = @PROJECTMANAGER
      ,[ADDRESS1] = @ADDRESS1
      ,[ADDRESS2] = @ADDRESS2
      ,[TOWN] = @TownCity
      ,[COUNTY] = @COUNTY
      ,[Architect] = @Architect    
      ,[LandValue] = @LandValue
      ,[GrantAmount] = @GrantAmount
      ,[BorrowedAmount] = @BorrowedAmount
      ,[OutlinePlanningApplication] = @OutlinePlanningApplication
      ,[DetailedPlanningApplication] = @DetailedPlanningApplication
      ,[OutlinePlanningApproval] = @OutlinePlanningApproval
      ,[DetailedPlanningApproval] = @DetailedPlanningApproval
      ,[PostCode] = @PostCode
      ,[PurchaseDate] = @PurchaseDate
      ,[DEVELOPMENTSTATUS] = @DevelopmentStatus
	  ,[CompanyId] = @CompanyId
	WHERE DEVELOPMENTID = @DevelopmentId
		SET @UpdatedDevelopmentId = @DevelopmentId
	END
	ELSE
		BEGIN
	
			IF Not Exists(select top 1 * from PDR_DEVELOPMENT Where DEVELOPMENTNAME = @DEVELOPMENTNAME AND ADDRESS1= @Address1 And TOWN = @TownCity)
			BEGIN
    
				-- Insert statements for procedure here
				INSERT INTO PDR_DEVELOPMENT (DEVELOPMENTNAME,PATCHID,POSTCODE,ADDRESS1,ADDRESS2,TOWN,COUNTY,Architect,PROJECTMANAGER,PurchaseDate,LandValue
				,GrantAmount,BorrowedAmount,OutlinePlanningApplication,DetailedPlanningApplication,OutlinePlanningApproval,DetailedPlanningApproval, CompanyId)
				VALUES(@DEVELOPMENTNAME,@PATCH,@PostCode,@ADDRESS1,@ADDRESS2,@TownCity,@COUNTY,@PROJECTMANAGER,@Architect,@PurchaseDate,@LandValue
				,@GrantAmount,@BorrowedAmount,@OutlinePlanningApplication,@DetailedPlanningApplication,@OutlinePlanningApproval,@DetailedPlanningApproval, @CompanyId)
      
				SELECT	@UpdatedDevelopmentId = SCOPE_IDENTITY()
				SET		@DevelopmentId = @UpdatedDevelopmentId
				--Return @Developmentid
			END
			ELSE
			BEGIN
				SET @UpdatedDevelopmentId = 'Exist'
			END
		
		END
	
	--=======================================================
	-- DEVELOPMENT FUNDING SOURCE
	--=======================================================

DECLARE fundingSourceCursor CURSOR FOR SELECT
	*
FROM @developmentFundingDetail
OPEN fundingSourceCursor

-- Declare Variable to use with cursor
DECLARE
@DEVELOPMENTFUNDINGID int,
@FUNDINGAUTHORITYID int ,
@FUNDINGAUTHORITY VARCHAR(100) ,
@FUNDGRANTAMOUNT float
	
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM fundingSourceCursor INTO @DEVELOPMENTFUNDINGID,@FUNDINGAUTHORITYID,@FUNDINGAUTHORITY,@FUNDGRANTAMOUNT

		WHILE @@FETCH_STATUS = 0 BEGIN
	
		IF (@DEVELOPMENTFUNDINGID = -1 AND @UpdatedDevelopmentId <> 'Exist')
		BEGIN
		
			INSERT INTO [PDR_DEVELOPMENT_FUNDING]
           ([FUNDINGAUTHORITYID]
           ,[GRANTAMOUNT]
           ,[DEVELOPMENTID])
			VALUES
           (@FUNDINGAUTHORITYID
           ,@FUNDGRANTAMOUNT
           ,@DevelopmentId)	
           
		END

-- Fetch record for next loop iteration.
FETCH NEXT FROM fundingSourceCursor INTO @DEVELOPMENTFUNDINGID,@FUNDINGAUTHORITYID,@FUNDINGAUTHORITY,@FUNDGRANTAMOUNT
END

CLOSE fundingSourceCursor
DEALLOCATE fundingSourceCursor

	
	
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @UpdatedDevelopmentId='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;
		
	END 
       
      
END
