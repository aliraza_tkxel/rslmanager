USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetFaultsAndDefects]    Script Date: 12/25/2014 18:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Scheme reported fault Count
 
    Author: Ali Raza
    Creation Date: Dec-25-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-25-2014      Ali raza         Get Scheme reported fault Count
    EXEC PDR_GetSchemeRepairsCount 1
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetSchemeRepairsCount]   
 @schemeId as INT  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
SELECT COUNT(FL_FAULT_LOG.FaultLogID)As RepairCount
FROM  
FL_FAULT_LOG  
 
LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID
LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId =P_BLOCK.BLOCKID
INNER JOIN FL_FAULT ON FL_FAULT_LOG.FAULTID=FL_FAULT.FAULTID   
INNER JOIN FL_AREA ON FL_FAULT.AREAID = FL_AREA.AreaID   
INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FAULTSTATUSID=FL_FAULT_LOG.STATUSID  
WHERE  
P_SCHEME.SCHEMEID=@schemeId 

END  