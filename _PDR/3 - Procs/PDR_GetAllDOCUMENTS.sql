USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetAllDOCUMENTS]    Script Date: 02/09/2015 22:13:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get Documents for Documents Grid on New Development Page
 
    Author: Salman nazir
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014    Salman Nazir           Get Documents Data
    
    Execution Command:
    
    Exec PDR_GetAllDOCUMENTS
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetAllDOCUMENTS]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select DocumentID, ISNULL(Title,'-') as Title, convert(varchar(10), Expires, 103) as Expire, ISNULL(FileType,'-') as FileType
	From PDR_Documents 
END
