USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePropertyFinancialInformation]    Script Date: 07/07/2014 15:07:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_SavePropertyFinancialInformation
 --   @rentFrequency = 1,
	--@nominatingBody='Aqib Javed',
	--@fundingAuthority =2,
	--@charge =1,
	--@chargeValue=12.00,
	--@OMVSt=11.00,
	--@EUV=10.00,
	--@insuranceValue=9.00,
	--@OMV=8.00,
	--@propertyId='BHA0000020',
	--@userId=605
-- Author:		<Aqib Javed>
-- Create date: <25-4-2014>
-- Description:	<This store procedure will save the information of Asbestos and Risk>
-- Web Page: FinancialTab.ascx
-- =============================================
ALTER PROCEDURE [dbo].[AS_SavePropertyFinancialInformation]
	@rentFrequency int,
	@nominatingBody varchar(150),
	@fundingAuthority int,
	@charge int,
	@chargeValue decimal,
	@OMVSt decimal,
	@EUV decimal,
	@insuranceValue decimal,
	@OMV decimal,
	@propertyId varchar(100),
	@userId int,
	@Yield decimal
AS
BEGIN
Declare @isRecordExist int
Select @isRecordExist =count(PROPERTYID)  from P_Financial where PROPERTYID =@propertyId
if (@isRecordExist<=0)
Begin
     Insert into P_FINANCIAL(FUNDINGAUTHORITY,NOMINATINGBODY,INSURANCEVALUE,CHARGEVALUE,OMVST,EUV,OMV,CHARGE,PROPERTYID,RENTFREQUENCY,PFUSERID,YIELD)
     Values(@fundingAuthority,@nominatingBody,@insuranceValue,@chargeValue,@OMVSt,@EUV,@OMV,@charge,@propertyId,@rentFrequency,@userId,@Yield)     
End            
Else
 Begin
     Update P_FINANCIAL set 
            RENTFREQUENCY=@rentFrequency,
            FUNDINGAUTHORITY=@fundingAuthority,
            NOMINATINGBODY=@nominatingBody,
            INSURANCEVALUE=@insuranceValue,
			CHARGEVALUE=@chargeValue,
			OMVST=@OMVSt,
			EUV=@EUV,
			OMV=@OMV,
			CHARGE=@charge,
		    PFUSERID=@userId,
		    Yield = @Yield
			where PROPERTYID=@propertyId
End  

Insert into P_FINANCIAL_HISTORY(FUNDINGAUTHORITY,NOMINATINGBODY,INSURANCEVALUE,CHARGEVALUE,OMVST,EUV,OMV,CHARGE,PROPERTYID,PFUSERID,YIELD,IsValuation)
     Values(@fundingAuthority,@nominatingBody,@insuranceValue,@chargeValue,@OMVSt,@EUV,@OMV,@charge,@propertyId,@userId,@Yield,1)       
                
END	