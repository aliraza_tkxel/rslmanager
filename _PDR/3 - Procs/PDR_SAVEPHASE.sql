USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SAVEPHASE]    Script Date: 02/18/2015 16:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/12/2014>
-- Description:	<Description,,Save Phase on SaveDevelopment.aspx>
-- EXEC PDR_SAVEPHASE 
-- DECLARE	@return_value int

--EXEC	@return_value = [dbo].[PDR_SAVEPHASE]
--		@PhaseName = N'Test',
--		@ConstructionStart = '01/12/2014',
--		@AnticipatedCompletion = '01/12/2014',
--		@ActualCompletion = '01/12/2014',
--		@HandoverintoManagement = '01/12/2014',
--		@SchemeId = 1

--SELECT	'Return Value' = @return_value

--GO
-- =============================================
CREATE PROCEDURE [dbo].[PDR_SAVEPHASE]
@PhaseName varchar(100),
@ConstructionStart smallDatetime,
@AnticipatedCompletion smallDatetime,
@ActualCompletion smallDatetime,
@HandoverintoManagement smallDatetime,
@schemeId int,
@PId int,
@DevelopmentId int,
@PhaseId int output
AS
BEGIN

if (@PId != 0)
BEGIN
	Update P_PHASE Set PhaseName = @PhaseName,ConstructionStart = @ConstructionStart, AnticipatedCompletion = @AnticipatedCompletion, ActualCompletion = @ActualCompletion,HandoverintoManagement = @HandoverintoManagement
	Where PHASEID = @PId
	SET @PhaseId = @PId
	Update P_SCHEME Set PhaseId = @PhaseId Where SchemeID = @schemeId
END
ELSE
BEGIN
	INSERT INTO P_PHASE (PHASENAME,ConstructionStart,AnticipatedCompletion,ActualCompletion,HandoverintoManagement,DEVELOPMENTID)
 VALUES (@PhaseName,@ConstructionStart,@AnticipatedCompletion,@ActualCompletion,@HandoverintoManagement,@DevelopmentId)
	SELECT @PhaseId = SCOPE_IDENTITY()
	
	Update P_SCHEME Set PhaseId = @PhaseId Where SchemeID = @schemeId
	
	Return @PhaseId
END


 
 
	 
END
