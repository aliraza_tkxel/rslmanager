-- Stored Procedure

-- =============================================  
-- EXEC AS_SavePropertyDocumentInformation   
--@propertyId = 'A530010002'  
--@documentTitle='My Document',  
--@documentName ='demo_file.doc',  
--@documentType = 'Agenda',  
--@keyword ='This is a demo document',  
--@documentSize ='100 KB',  
--@documentFormat ='.DOC'  

-- Author:  <Aqib Javed>  
-- Create date: <19-Dec-2013>  
-- Description: <Save Property Document information>  
-- WebPage: PropertyRecord.aspx > Document Tab  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_SavePropertyDocumentInformation]  
@propertyId varchar(50),  
@documentName  varchar(500),  
@keyword varchar(255),  
@documentSize varchar(50),  
@documentFormat varchar(50),  
@documentTypeId int,  
@documentSubTypeId int , 
 @schemeId int=null,
@blockId int = null 

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

    -- Insert statements for procedure here  
 INSERT INTO P_Documents  
 (PropertyId,DocumentName,Keywords,DocumentSize,DocumentFormat,DocumentTypeId,DocumentSubtypeId,SchemeId,BlockId)  
 VALUES  
 (@propertyId,@documentName ,@keyword,@documentSize,@documentFormat,@documentTypeId,@documentSubTypeId,@schemeId,@blockId)  
END
GO