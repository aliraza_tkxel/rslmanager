-- Stored Procedure

-- =============================================    
-- Author:  <Salman Nazir>    
-- Create date: <15/11/2012>    
-- Description: <Save the Photograph for property and Item>    
-- WebPage: PropertyRecord.aspx => Attributes Tab    
-- =============================================    
ALTER PROCEDURE [dbo].[AS_SavePhotograph] (    
@propertyId varchar(500)=null,    
@itemId int,    
@title varchar(500),    
@uploadDate smalldatetime,    
@imagePath varchar(1000),    
@imageName varchar(500),    
@createdBy int,  
@schemeId int=null, 
@blockId int=null, 
@isDefaultImage bit = null 
)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 If @itemId = 0    
 BEGIN    
  set @itemid =  (Select ItemId from PA_ITEM Where ItemName like '%Appliance%')    
 END    
  DECLARE @propertyPicId INT   
 INSERT INTO PA_PROPERTY_ITEM_IMAGES    
 ([PROPERTYID],[ItemId],[ImagePath],[ImageName],[CreatedOn],[CreatedBy],[Title],SchemeId,BlockId )    
 VALUES    
 (@propertyId,@itemId,@imagePath ,@imageName,@uploadDate,@createdBy,@title,@schemeId,@blockId)    
   SET @propertyPicId=SCOPE_IDENTITY()  
   if @isDefaultImage = 1 And @propertyPicId > 0  And @propertyId <> ''
  Begin   
   Update P__PROPERTY SET PropertyPicId = @propertyPicId WHERE PROPERTYID = @propertyId    
  END  
END 
GO