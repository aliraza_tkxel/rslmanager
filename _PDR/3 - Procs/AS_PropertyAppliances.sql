USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_PropertyAppliances]    Script Date: 02/13/2015 18:07:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
--EXEC [AS_PropertyAppliances]      
--  @propertyId = N'A010060001',      
--  @pageSize = 5,      
--  @pageNumber = 1,      
--  @sortColumn = N'Type',      
--  @sortOrder = N'ASC'      
      
-- Author:  <Noor Muhammad>      
-- Create date: <31/10/2012>      
-- Description: <This stored procedure returns the all the appliances against the property>      
-- Parameters:       
  --@propertyId varchar(50),        
         
  ---- column name on which sorting is performed      
  --@sortColumn varchar(50) = 'Type',      
  --@sortOrder varchar (5) = 'DESC'      
-- Webpage :PropertyRecord.aspx      
-- Conrol: Appliances.ascx      
-- =============================================      
ALTER PROCEDURE [dbo].[AS_PropertyAppliances]      
(       
  @propertyId varchar(50)=null,  
  @itemId int,          
  @schemeId int=null, 
  @blockId int=null,          
  @pageSize int = 30,        
  @pageNumber int = 1,      
  @sortColumn varchar(50) = 'Type',      
  @sortOrder varchar (5) = 'ASC',      
  @totalCount int=0 output      
        
)      
AS      
BEGIN      
        
     -------------------------------With paging--------------------------------      
declare @offset int      
declare @limit int      
      
set @offset = 1+(@pageNumber-1) * @pageSize      
set @limit = (@offset + @pageSize)-1      
      
Declare @finalQuery varchar(8000)      
      
 SELECT Type, Location,Make, Model, DatePurchased, PurchaseCost, ApplianceId,LifeSpan,Dimensions,Quantity,Item,LocationID,TypeId,MakeId  
 ,ModelId,SerialNumber,Notes      
 FROM(      
   SELECT *, row_number() over (order by ApplianceId DESC) as row       
   FROM (      
     SELECT       
     top (@limit)       
      isnull(GS_Appliance_Type.APPLIANCETYPE,'-') as [Type]      
     ,isnull(GS_Location.Location,'-') as Location      
     ,isnull(GS_MANUFACTURER.MANUFACTURER,'-') as Make      
     ,isnull(GS_APPLIANCEModel.Model,'-') as Model      
     ,Convert(varchar(10),GS_PROPERTY_APPLIANCE.Datepurchased,103) as DatePurchased      
     ,GS_PROPERTY_APPLIANCE.PurchaseCost as PurchaseCost      
     ,GS_PROPERTY_APPLIANCE.PropertyApplianceId as ApplianceId    
     ,GS_PROPERTY_APPLIANCE.LifeSpan    
     ,GS_PROPERTY_APPLIANCE.Dimensions               
     ,GS_PROPERTY_APPLIANCE.Quantity  
     ,GS_PROPERTY_APPLIANCE.Item  
     ,GS_PROPERTY_APPLIANCE.LocationID  
     ,GS_PROPERTY_APPLIANCE.APPLIANCETYPEID as TypeId  
     ,GS_PROPERTY_APPLIANCE.MANUFACTURERID as MakeId  
     ,GS_PROPERTY_APPLIANCE.ModelID      as ModelId  
     ,GS_PROPERTY_APPLIANCE.SerialNumber    
     ,GS_PROPERTY_APPLIANCE.Notes          
     FROM GS_PROPERTY_APPLIANCE            
     Left JOIN GS_Appliance_Type ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_Appliance_Type.APPLIANCETYPEID      
     Left JOIN GS_MANUFACTURER ON GS_PROPERTY_APPLIANCE.MANUFACTURERID = GS_MANUFACTURER.MANUFACTURERID      
     left join GS_APPLIANCEModel on  GS_PROPERTY_APPLIANCE.ModelID =   GS_APPLIANCEModel.ModelID  
     Left Join GS_Location On GS_PROPERTY_APPLIANCE.LocationID = GS_Location.LocationID  
    WHERE
    ItemId=@itemId
    And (GS_PROPERTY_APPLIANCE.PropertyID = @propertyId OR  @propertyId IS NULL)AND
     (GS_PROPERTY_APPLIANCE.SchemeId = @schemeId OR @schemeId IS NULL)
     AND (GS_PROPERTY_APPLIANCE.BlockId = @blockId OR @blockId IS NULL)
       
       Order BY ApplianceId 
    )AS Records      
  ) AS Result       
 WHERE      
 Result.row between  @offset AND @limit         
   
        
      
     -----------------------------------Count Query------------------------------      
     SELECT       
     @totalCount =count(*)      
     FROM GS_PROPERTY_APPLIANCE            
     Left JOIN GS_Appliance_Type ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_Appliance_Type.APPLIANCETYPEID      
     Left JOIN GS_MANUFACTURER ON GS_PROPERTY_APPLIANCE.MANUFACTURERID = GS_MANUFACTURER.MANUFACTURERID      
     left join GS_APPLIANCEModel on  GS_PROPERTY_APPLIANCE.ModelID =   GS_APPLIANCEModel.ModelID  
     Left Join GS_Location On GS_PROPERTY_APPLIANCE.LocationID = GS_Location.LocationID  
     WHERE       
     (GS_PROPERTY_APPLIANCE.PropertyID = @propertyId OR  @propertyId IS NULL)AND
     (GS_PROPERTY_APPLIANCE.SchemeId = @schemeId OR @schemeId IS NULL)AND (GS_PROPERTY_APPLIANCE.BlockId = @blockId OR @blockId IS NULL)
     And ItemId=@itemId     
        
     -------------------------------With paging End--------------------------------      
       
     
END