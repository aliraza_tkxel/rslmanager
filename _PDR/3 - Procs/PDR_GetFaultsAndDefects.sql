/* =================================================================================    
    Page Description:  Get Scheme reported fault 
 
    Author: Ali Raza
    Creation Date: Dec-24-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-24-2014      Ali raza         Get Scheme reported fault 
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetFaultsAndDefects]   
 @schemeId as INT  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
SELECT top (5)  
CONVERT(VARCHAR(10),FL_FAULT_LOG.SUBMITDATE, 103) as ReportedDate  
,FAULTLOGID as Id  
,P_BLOCK.BLOCKNAME   
,FL_AREA.AreaName +' > '+ FL_FAULT.DESCRIPTION as Description  
,FL_FAULT_STATUS.Description as Status  
,'Reactive' as Type  
FROM  
FL_FAULT_LOG  
 
LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID
LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId =P_BLOCK.BLOCKID
INNER JOIN FL_FAULT ON FL_FAULT_LOG.FAULTID=FL_FAULT.FAULTID   
INNER JOIN FL_AREA ON FL_FAULT.AREAID = FL_AREA.AreaID   
INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FAULTSTATUSID=FL_FAULT_LOG.STATUSID  
WHERE  
P_SCHEME.SCHEMEID=@schemeId  
ORDER BY FL_FAULT_LOG.SUBMITDATE DESC  
END  