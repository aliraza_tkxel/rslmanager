USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetFaultsAndDefects]    Script Date: 12/25/2014 16:24:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Total ASB for Scheme
 
    Author: Ali Raza
    Creation Date: Dec-24-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-24-2014      Ali raza         Get Total ASB for Scheme
    EXEC [PDR_GetSchemeASB]  1
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetSchemeASB]   
 @schemeId as INT  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
select Count(P.PROPERTYID) AS ASB				 
							 FROM 	C_JOURNAL J  
									INNER JOIN C_ASB G ON J.JOURNALID = G.JOURNALID 
                             LEFT JOIN dbo.C_ASB_CATEGORY_BY_GRADE CAT ON CAT.CATEGORYID=G.CATEGORY 
									LEFT JOIN  C_ASB_CATEGORY AC ON AC.CATEGORYID = G.CATEGORY 
									INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID  
									LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO 
									LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE 
									INNER JOIN (  
											SELECT MAX(TENANCYID) AS MAXTEN, CUSTOMERID  
											FROM   C_CUSTOMERTENANCY  
											GROUP BY CUSTOMERID 
										   ) MT ON MT.CUSTOMERID = C.CUSTOMERID 
									INNER JOIN (  
											SELECT MAX(ASBHISTORYID) AS MAXHIST, JOURNALID 
											FROM   C_ASB 
											GROUP BY JOURNALID 
										   ) MH ON MH.JOURNALID = J.JOURNALID 
									INNER JOIN C_TENANCY T ON T.TENANCYID = MT.MAXTEN  
									INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID 
									 LEFT JOIN P_SCHEME sch ON sch.SCHEMEID = P.SCHEMEID
							       LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   
							       LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 
									INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID 
									INNER JOIN G_TITLE TIT2 ON TIT2.TITLEID = E.TITLE 
								WHERE J.ITEMNATUREID IN (8,9,24,25) AND sch.SCHEMEID = @schemeId 
							   AND G.ASBHISTORYID=(SELECT MAX(ASBHISTORYID) FROM C_ASB WHERE JOURNALID=G.JOURNALID) 
END  