USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocks]    Script Date: 12/31/2014 16:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get Blocks for dropdown
 
    Author: Salman Nazir
    Creation Date: Dec-09-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-09-2014      Salman           get blocks for dropdown
    
    Execution Command:
    
    Exec PDR_GetBlocks
  =================================================================================*/
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetBlocks]
@SchemeId int = -1 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @SchemeId = -1
		BEGIN
			Select B.BLOCKID,B.BLOCKNAME from P_BLOCK B
			WHERE B.SCHEMEID IS NULL
		END
	ELSE
		BEGIN
			Select B.BLOCKID,B.BLOCKNAME from P_BLOCK B
			WHERE B.SCHEMEID = @SchemeId
		END
END
