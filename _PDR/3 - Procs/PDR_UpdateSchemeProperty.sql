-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: on Save Scheme Page
 
    Author: Salman Nazir
    Creation Date: Dec-09-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-09-2014      Salman         Update Property After saving SCHEME
    
    
    =================================================================================
    EXEC PDR_UpdateSchemeProperty  1, 'A0120354'
  =================================================================================*/
CREATE PROCEDURE PDR_UpdateSchemeProperty
@SCHEMEID int,
@PROPERTYID nvarchar
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE P__PROPERTY SET SCHEMEID =@SCHEMEID WHERE PROPERTYID =@PROPERTYID
END
GO
