USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDevelopmentBlocks]    Script Date: 02/05/2015 16:44:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/12/2014>
-- Description:	<Description,,SaveDevelopment.aspx>
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetDevelopmentBlocks] 

@developmentId Int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

		Select BLOCKID,BLOCKNAME,ISNULL(ADDRESS1,' ')+' '+ISNULL(ADDRESS2,' ')+'  '+ISNULL(ADDRESS3,' ') as BlockAddress 
		from P_BLOCK Where DEVELOPMENTID  = @developmentId AND @developmentId != 0

END
