USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetSchemeDetailBySchemeId]    Script Date: 02/03/2015 18:05:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Scheme Detail By schemeId
 
    Author: Ali Raza
    Creation Date: Dec-25-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-25-2014      Ali raza         Get Scheme Detail By schemeId
    EXEC PDR_GetSchemeDetailBySchemeId 1
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetSchemeDetailBySchemeId]   
 @schemeId as INT  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  Select s.SCHEMENAME,ISNULL(d.ADDRESS1,'-') AS Address1,ISNULL(d.TOWN,'-') AS Town,ISNULL(d.PostCode,'-')as PostCode  
  from P_SCHEME s 
  INNER JOIN PDR_DEVELOPMENT d ON s.DEVELOPMENTID = d.DEVELOPMENTID 
  where s.SCHEMEID = @schemeId

END  