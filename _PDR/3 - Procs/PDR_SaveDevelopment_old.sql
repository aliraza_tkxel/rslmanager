USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveDevelopment]    Script Date: 02/04/2015 17:26:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,02/12/2014>
-- Description:	<Description,,save development on SaveDevelopment.aspx>
-- =============================================
ALTER PROCEDURE [dbo].[PDR_SaveDevelopment]
@DEVELOPMENTNAME varchar(50),@PATCH int,@ADDRESS1 varchar(500),@ADDRESS2 varchar(500),@PostCode varchar(50),@TownCity varchar(50),@COUNTY varchar(50),@Architect varchar(50),@PROJECTMANAGER varchar(50),@DevelopmentType int,@DevelopmentStatus int,@LandValue float,@PurchaseDate smalldatetime
      ,@GrantAmount float,@BorrowedAmount float,@OutlinePlanningApplication smalldatetime,@DetailedPlanningApplication smalldatetime,@OutlinePlanningApproval smalldatetime,@DetailedPlanningApproval smalldatetime,@NoofUnits int,@FundingSource int,
      @DevelopmentId varchar(50) Output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY 	
	
	IF Not Exists(select top 1 * from PDR_DEVELOPMENT Where DEVELOPMENTNAME = @DEVELOPMENTNAME AND ADDRESS1= @Address1 And TOWN = @TownCity)
	BEGIN


    -- Insert statements for procedure here
	INSERT INTO PDR_DEVELOPMENT (DEVELOPMENTNAME,PATCHID,POSTCODE,ADDRESS1,ADDRESS2,TOWN,COUNTY,Architect,PROJECTMANAGER,PurchaseDate,LandValue,FundingSource
      ,GrantAmount,BorrowedAmount,OutlinePlanningApplication,DetailedPlanningApplication,OutlinePlanningApproval,DetailedPlanningApproval)
VALUES(@DEVELOPMENTNAME,@PATCH,@PostCode,@ADDRESS1,@ADDRESS2,@TownCity,@COUNTY,@PROJECTMANAGER,@Architect,@PurchaseDate,@LandValue,@FundingSource
      ,@GrantAmount,@BorrowedAmount,@OutlinePlanningApplication,@DetailedPlanningApplication,@OutlinePlanningApproval,@DetailedPlanningApproval)
       
       
       SELECT @Developmentid = SCOPE_IDENTITY()
       --Return @Developmentid
       END
   ELSE
	   BEGIN
	   SET @Developmentid='Exist'
	   END
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @Developmentid='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;
		
	END 
       
      
END
