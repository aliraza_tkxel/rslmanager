-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/12/2014>
-- Description:	<Description,,Get Block List on BlockList.aspx>
-- 
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetBlockList]
	-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'BlockName', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 

		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1


		--=====================Search Criteria==================================
		SET @searchCriteria = ' 1=1 '

		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( BLOCKNAME LIKE  ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR D.DEVELOPMENTNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PhaseName LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR S.SchemeName LIKE ''%' + @searchText + '%'') '
		END	

		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
								B.BLOCKID,BLOCKNAME,(ISNULL(B.ADDRESS1,'''')+''  ''+ISNULL(B.ADDRESS2,'''')+''  ''+ISNULL(B.ADDRESS3,'''')) as Address,
								ISNULL(D.DEVELOPMENTNAME,'''') AS DEVELOPMENT,ISNULL(B.NoOfProperties,'''') AS PROPERTIES
								,ISNULL(S.SchemeName,'''') AS SCHEME,ISNULL(PH.PhaseName,'''') AS PHASE'


		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'from P_BLOCK B
							LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID
							LEFT JOIN P_SCHEME S ON S.SCHEMEID = B.SchemeId
							LEFT JOIN P_PHASE PH ON PH.PHASEID = B.PhaseId'

		--============================Order Clause==========================================
		IF(@sortColumn = 'BLOCKNAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' BLOCKID' 	

		END

		IF(@sortColumn = 'DEVELOPMENT')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'DEVELOPMENT' 	

		END
		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'SCHEME' 	

		END
		IF(@sortColumn = 'Phase')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'PHASE' 	

		END

		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		--=================================	Where Clause ================================

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause  + @orderClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =   count( B.BLOCKID ) ' +@fromClause + @whereClause

		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================	

END
GO