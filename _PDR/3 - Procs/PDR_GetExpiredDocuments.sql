-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,30-Jan-2015>
-- Description:	<Description,,Get all expired documents to send push notifications>
-- EXEC PDR_GetExpiredDocuments
-- =============================================
CREATE PROCEDURE PDR_GetExpiredDocuments 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select DocumentID,Title,CreatedBy,Expires 
	from PDR_DOCUMENTS
	WHere (DATEADD(day,14,expires)) = Convert(datetime, Convert(char,GETDATE(),103),103)
END
GO
