USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocksPropertiesByPhaseId]    Script Date: 02/03/2015 20:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Create Scheme Page 
 
    Author: Salman Nazir
    Creation Date: Jan-07-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Jan-07-2015      Salman         Get Blocks and Properties by PhaseId 
  =================================================================================*/
  -- Exec PDR_GetBlocksPropertiesByPhaseId -1
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetBlocksPropertiesByPhaseId]
@phaseId int = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select BLOCKID,ISNULL(BLOCKNAME,' ') as BlockName from P_BLOCk Where PhaseId = @phaseId
Select PROPERTYID,(ISNULL(HOUSENUMBER,' ')+' '+ISNULL(ADDRESS1,' ')) PropertyAddress from P__PROPERTY Where PhaseId = @phaseId

END
