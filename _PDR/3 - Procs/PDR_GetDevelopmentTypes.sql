-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get DevelopmentType For DropDown in New Development
 
    Author: Salman Nazir
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Salman Nazir           Get DevelopmentType For DropDown in New Development
    
    Execution Command:
    
    Exec PDR_GetDevelopmentTypes
  =================================================================================*/
CREATE PROCEDURE PDR_GetDevelopmentTypes

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DEVELOPMENTTYPEID,[DESCRIPTION] from P_DEVELOPMENTTYPE
END
GO
