-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,04/12/2014>
-- Description:	<Description,,Save Block data>
--
-- =============================================
ALTER PROCEDURE [dbo].[PDR_SaveBlock]
@BlockName varchar(100),
@BlockReference varchar (200),
@developmentid int,
@phaseId int,
@address1 varchar(500),
@address2 varchar(500),
@address3 varchar(500),
@town varchar(50),
@county varchar(50),
@postCode varchar(50),
@buildDate varchar(50),
@noOfProperties int,
@Ownership int,
@existingBlockId int= null,
@BlockId nvarchar(50) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	BEGIN TRY 	
	IF @existingBlockId <= 0 OR @existingBlockId IS NULL 
	BEGIN
		IF Not Exists(select top 1 * from P_BLOCK Where BLOCKNAME = @BlockName AND ADDRESS1= @Address1 And TOWNCITY = @TOWN)
		BEGIN

		-- Insert statements for procedure herer
		INSERT INTO P_BLOCK (BLOCKNAME,DEVELOPMENTID,PHASEID,ADDRESS1,ADDRESS2,ADDRESS3,TOWNCITY,COUNTY,POSTCODE,COMPLETIONDATE,NoOfProperties,OWNERSHIP,IsTemplate,BlockReference)
		VALUES (@BlockName,@developmentid,@phaseId,@address1,@address2,@address3,@town,@county,@postCode,@buildDate,@noOfProperties,@Ownership,1,@BlockReference)



		   SELECT @BlockId = SCOPE_IDENTITY()

		   END
	   ELSE
		   BEGIN
		   SET @BlockId='Exist'
		   END
	END
	ELSE
		BEGIN
		Update P_BLOCK Set BLOCKNAME=@BlockName,DEVELOPMENTID=@developmentid,PHASEID=@phaseId,ADDRESS1=@address1,ADDRESS2=@address2,
		ADDRESS3=@address3,TOWNCITY=@town,COUNTY=@county,POSTCODE=@postCode,COMPLETIONDATE=@buildDate,NoOfProperties=@noOfProperties,OWNERSHIP=@Ownership,BlockReference=@BlockReference
		WHERE BLOCKID = @existingBlockId
		 SET @BlockId=@existingBlockId
		END	   
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @BlockId='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  

IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;

	END 


END
GO