-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,16/02/2015>
-- Description:	<Description,,Get Scheme Block Refurbishment Info>
-- =============================================
CREATE PROCEDURE PDR__GetRefurbishmentInformaiton
@Id int,
@requestType varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
   IF(@requestType = 'Block')
	   BEGIN
			 SELECT BlockId,CONVERT(nvarchar(20), REFURBISHMENT_DATE, 103) as REFURBISHMENT_DATE,e.FIRSTNAME+', '+e.LASTNAME as USERNAME,NOTES FROM P_REFURBISHMENT_HISTORY  
		 INNER JOIN E__EMPLOYEE e on e.EMPLOYEEID = USERID   
		  WHERE BlockId=@Id  ORDER BY REFURBISHMENT_DATE desc 
	  END
  ELSE
	  BEGIN
		   SELECT SchemeId,CONVERT(nvarchar(20), REFURBISHMENT_DATE, 103) as REFURBISHMENT_DATE,e.FIRSTNAME+', '+e.LASTNAME as USERNAME,NOTES FROM P_REFURBISHMENT_HISTORY  
		 INNER JOIN E__EMPLOYEE e on e.EMPLOYEEID = USERID   
		  WHERE SchemeId=@Id  ORDER BY REFURBISHMENT_DATE desc 
	  END
END
GO
