USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,25/02/2015>
-- Description:	<Description,,SaveDevelopment.aspx>
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetDevelopmentDetail] 

@developmentId Int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--============================================================
	-- DEVELOPMENT DETAIL
	--============================================================

	SELECT	ISNULL(DEV.DEVELOPMENTNAME,'') AS DEVELOPMENTNAME
			,ISNULL(DEV.PATCHID,-1) AS PATCHID
			,ISNULL(DEV.ADDRESS1,'') AS ADDRESS1
			,ISNULL(DEV.ADDRESS2,'') AS  ADDRESS2
			,ISNULL(DEV.TOWN,'') AS  TOWN
			,ISNULL(DEV.COUNTY,'') AS  COUNTY
			,ISNULL(DEV.PostCode,'') AS PostCode
			,ISNULL(DEV.Architect,'') AS Architect
			,ISNULL(DEV.PROJECTMANAGER,'') AS PROJECTMANAGER
			,ISNULL(DEV.Companyid,-1) AS COMPANYID
			,ISNULL(DEV.DEVELOPMENTTYPE,-1) AS DEVELOPMENTTYPE
			,ISNULL(DEV.DEVELOPMENTSTATUS,-1) AS STATUS
			,ISNULL(CONVERT(VARCHAR,DEV.LandValue),'') AS LandValue
			,CONVERT(VARCHAR(10),DEV.PurchaseDate,103) AS PurchaseDate
			,ISNULL(DEV.GrantAmount,'') AS  GrantAmount
			,ISNULL(DEV.BorrowedAmount,'') AS BorrowedAmount
			,CONVERT(VARCHAR(10),DEV.OutlinePlanningApplication,103) AS OutlinePlanningApplication
			,CONVERT(VARCHAR(10),DEV.DetailedPlanningApplication,103) AS DetailedPlanningApplication
			,CONVERT(VARCHAR(10),DEV.OutlinePlanningApproval,103) AS OutlinePlanningApproval
			,CONVERT(VARCHAR(10),DEV.DetailedPlanningApproval,103) AS DetailedPlanningApproval
			,ISNULL(DEV.NUMBEROFUNITS,'') AS NUMBEROFUNITS
			,isnull(DEV.Companyid,1) as CompanyId
	FROM	PDR_DEVELOPMENT AS DEV
			
	WHERE	DEV.DEVELOPMENTID = @developmentId
	
	--============================================================
	-- DEVELOPMENT FUNDING
	--============================================================
	
	SELECT	DEVELOPMENTFUNDINGID,PDF.FUNDINGAUTHORITYID,PF.DESCRIPTION FUNDINGAUTHORITY,GRANTAMOUNT AS FUNDGRANTAMOUNT
	FROM	PDR_DEVELOPMENT_FUNDING PDF
			INNER JOIN P_FUNDINGAUTHORITY PF ON PDF.FUNDINGAUTHORITYID = PF.FUNDINGAUTHORITYID
	WHERE	DEVELOPMENTID = @developmentId

END
