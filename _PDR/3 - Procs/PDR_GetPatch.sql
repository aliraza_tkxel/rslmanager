/* =================================================================================    
    Page Description:     Get Patch Fro DropDown
 
    Author: Ali Raza
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Ali Raza           Get Patch Fro DropDown
    
    Execution Command:
    
    Exec PDR_GetPatch
  =================================================================================*/
CREATE PROCEDURE PDR_GetPatch
	
AS
BEGIN
	
	SET NOCOUNT ON;    
	SELECT PATCHID,LOCATION From E_PATCH
END
GO
