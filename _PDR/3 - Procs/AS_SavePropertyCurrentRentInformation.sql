-- =============================================    
-- Exec AS_SavePropertyCurrentRentInformation    
    --@rentDateSet = 4/30/2014,    
    --@rentEffectiveSet = 5/30/2014,    
    --@rentType =2,    
 --@rent =145.00,    
 --@services=12.00,    
 --@councilTax=11.00,    
 --@water=10.00,    
 --@inetlig=10.00,    
 --@water=10.00,    
 --@inetlig=10.00,    
 --@supp=10.00,    
 --@garage=10.00,    
 --@totalRent=10.00,    
 --@propertyId='BHA0000020',    
 --@userId=605    
-- Author:  <Aqib Javed>    
-- Create date: <25-4-2014>    
-- Description: <This store procedure will save the information of Current Rent>    
-- Web Page: CurrentRentTab.ascx    
-- =============================================    
ALTER PROCEDURE [dbo].[AS_SavePropertyCurrentRentInformation]    
 @rentDateSet date,    
 @rentEffectiveSet date,    
 @rentType int,
 @rentFrequency int,   
 @rent decimal,    
 @services decimal,    
 @councilTax decimal,    
 @water decimal,    
 @inetlig decimal,    
 @supp decimal,    
 @garage decimal,    
 @totalRent decimal,    
 @propertyId varchar(100),    
 @userId int    
AS    
BEGIN    
Declare @isRecordExist int    
Select @isRecordExist =count(PROPERTYID)  from P_Financial where PROPERTYID =@propertyId    
if (@isRecordExist<=0)    
Begin    
     Insert into P_FINANCIAL(DATERENTSET,RENTEFFECTIVE,RENTTYPE,RENTFREQUENCY,RENT,[SERVICES],COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,PROPERTYID,PFUSERID)    
     Values(@rentDateSet,@rentEffectiveSet,@rentType,@rentFrequency,@rent,@services,@councilTax,@water,@inetlig,@supp,@garage,@totalRent,@propertyId,@userId)         
End                
Else    
 Begin    
     Update P_FINANCIAL set     
            DATERENTSET=@rentDateSet,    
            RENTEFFECTIVE=@rentEffectiveSet,    
            RENTTYPE=@rentType, 
            RENTFREQUENCY=@rentFrequency ,  
            RENT=@rent,    
   [SERVICES]=@services,    
   COUNCILTAX=@councilTax,    
   WATERRATES=@water,    
   INELIGSERV=@inetlig,    
   SUPPORTEDSERVICES=@supp,    
   GARAGE=@garage,    
   TOTALRENT=@totalRent,    
      PFUSERID=@userId    
   where PROPERTYID=@propertyId    
End      

 Insert into P_FINANCIAL_HISTORY(DATERENTSET,RENTEFFECTIVE,RENTTYPE,RENT,[SERVICES],COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,PROPERTYID,PFUSERID,PFTIMESTAMP,RentFrequency)    
     Values(@rentDateSet,@rentEffectiveSet,@rentType,@rent,@services,@councilTax,@water,@inetlig,@supp,@garage,@totalRent,@propertyId,@userId,GETDATE(),@rentFrequency)    
          
END 