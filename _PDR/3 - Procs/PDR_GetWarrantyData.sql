-- Stored Procedure

/* =================================================================================    
    Page Description:  Get Warranty Data

    Author: Salman Nazir
    Creation Date: Dec-25-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-25-2014      Salman Nazir           Get Warranty Data

    Execution Command:

    Exec PDR_GetWarrantyData 1
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_GetWarrantyData] 
@warrantyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select WARRANTYID,convert(varchar(10), EXPIRYDATE, 103) EXPIRYDATE,CONTRACTOR,NOTES,AREAITEM,WARRANTYTYPE from PDR_WARRANTY Where WarrantyId = @warrantyId
END
GO