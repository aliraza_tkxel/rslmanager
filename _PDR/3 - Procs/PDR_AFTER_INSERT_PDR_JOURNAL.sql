USE [RSLBHALive]
GO
/****** Object:  Trigger [dbo].[PDR_AFTER_INSERT_PDR_JOURNAL]    Script Date: 01/08/2015 12:08:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--====================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,6th January,2015>
-- Description:	<Description,,This trigger 'll insert the record in PDR_JOURNAL_HISTORY after insertion in PDR_JOURNAL
--====================================
CREATE TRIGGER [dbo].[PDR_AFTER_INSERT_PDR_JOURNAL]
 ON [dbo].[PDR_JOURNAL]   
AFTER INSERT  
AS  
BEGIN  	
	INSERT INTO PDR_JOURNAL_HISTORY
            ([JOURNALID]
           ,[MSATID]
           ,[STATUSID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[WORKSREQUIRED]
           ,[NOTES] )
           SELECT 
            i.JOURNALID
           ,i.MSATID
           ,i.STATUSID 
           ,i.CREATIONDATE
           ,I.CREATEDBY
           ,i.WORKSREQUIRED 
           ,i.NOTES        
           FROM INSERTED i                         
 END    
