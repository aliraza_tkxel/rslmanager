USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlockTemplateData]    Script Date: 02/03/2015 20:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,04/12/2014>
-- Description:	<Description,,Get Selected Block Template Data>
-- EXEC PDR_GetBlockTemplateData 16 
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetBlockTemplateData]
@blockId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select BLOCKNAME,DEVELOPMENTNAME,PhaseName,B.ADDRESS1,B.ADDRESS2,ADDRESS3
	,TOWNCITY,B.COUNTY,B.POSTCODE,COMPLETIONDATE as BuildDate,NoOfProperties,O.Description as ConfirmOwnership,BlockReference,D.DEVELOPMENTID,P.PHASEID
	from P_BLOCK B
	LEFT JOIN PDR_DEVELOPMENT D on D.DEVELOPMENTID = B.DEVELOPMENTID
	LEFT JOIN P_PHASE P on P.PHASEID = B.PhaseId
	Left JOIN P_OWNERSHIP O on O.OwnershipId = B.Ownership
	Where B.BLOCKID = @blockId
END
