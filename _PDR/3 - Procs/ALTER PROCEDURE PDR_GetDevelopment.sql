USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDevelopment]    Script Date: 02/09/2015 17:46:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: populate Development dropdown
 
    Author: Salman Nazir
    Creation Date: Dec-04-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    1.0          09 Feb 2015    Noor Muhammad      added order by field
    
    Execution Command:
    
    Exec PDR_GetDevelopment
  =================================================================================*/
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetDevelopment]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

			Select DevelopmentId,DevelopmentName from PDR_DEVELOPMENT ORDER BY DevelopmentName ASC
		
END
