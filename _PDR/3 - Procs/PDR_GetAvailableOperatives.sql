USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetAvailableOperatives]    Script Date: 12/10/2013 12:58:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--EXEC	[dbo].[PDR_GetAvailableOperatives]
--		@tradeIds = N'1,2,3',
--		@msattype= 'M&E Servicing',
--		@startDate = N'' 
-- Author:		<Author,,Ahmed Muhammad>
-- Create date: <Create Date,,16 Jan,2015>
-- Last Updated By: Ahmed Mehmood
-- Description:	<Description,,This stored procedure fetch  the employees >
-- WebPage: ScheduleMEServicing.aspx
-- ============================================= */
CREATE PROCEDURE [dbo].[PDR_GetAvailableOperatives] 
	-- Add the parameters for the stored procedure here
	@tradeIds as varchar(max),
	@msattype as varchar(max),
	@startDate as datetime	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @operativeIdStr NVARCHAR(MAX)
	
	--=================================================================================================================
	------------------------------------------------------ Step 0------------------------------------------------------
	--Create Temporary Table
	
	
    CREATE TABLE #AvailableOperatives(
		EmployeeId int
		,FirstName nvarchar(50)
		,LastName nvarchar(50)
		,FullName nvarchar(100)		
		,PatchId int
		,PatchName nvarchar(20)		
		,Distance nvarchar(10)
		,TradeId INT				
	)
	
	DECLARE @InsertClause varchar(200)
	DECLARE @SelectClause varchar(800)
	DECLARE @FromClause varchar(800)
	DECLARE @WhereClause varchar(max)		
	DECLARE @MainQuery varchar(max)
	--=================================================================================================================
	------------------------------------------------------ Step 1------------------------------------------------------
	--This query fetches the employees which matches with the following criteria
	--Trade of employee is same as trade of component 
	--User type is M&E Servicing or Cyclic Maintenance		
	--Exclude the sick leaves
	
	SET @InsertClause = 'INSERT INTO #AvailableOperatives(EmployeeId,FirstName,LastName,FullName, PatchId, PatchName, TradeId)'
	
	SET @SelectClause = 'SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + '' ''+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	,E_TRADE.TradeId AS TradeId ' 
	
	SET @FromClause  = 'FROM  E__EMPLOYEE 
	INNER JOIN E_TRADE ON E_TRADE.EmpId = E__EMPLOYEE.EmployeeId
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID '
	
	SET @WhereClause  = 'WHERE P_INSPECTIONTYPE.Description  ='''+@msattype+''' 	
	AND E_JOBDETAILS.Active=1		
	AND E_TRADE.tradeid in ('+@tradeIds+') '
	
	---------------------------------------------------------
	-- Filter to skip out operative(s) of sick leave.
	SET @WhereClause  =  @WhereClause +CHAR(10)+ ' AND E__EMPLOYEE.EmployeeId NOT IN (
																						SELECT 
																							EMPLOYEEID 
																							FROM E_JOURNAL
																							INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
																							AND E_ABSENCE.ABSENCEHISTORYID IN (
																																SELECT 
																																	MAX(ABSENCEHISTORYID) 
																																	FROM E_ABSENCE 
																																	GROUP BY JOURNALID
																																)
																							WHERE ITEMNATUREID = 1
																							AND E_ABSENCE.RETURNDATE IS NULL
																							AND E_ABSENCE.ITEMSTATUSID = 1
																						)	'
	---------------------------------------------------------				
	--Combine the query clauses to make the one / main query
	SET @MainQuery = @InsertClause+Char(10)+@SelectClause+Char(10)+@FromClause+Char(10)+@WhereClause 										
	
	
	--print @InsertClause+Char(10)
	print @SelectClause+Char(10)
	print @FromClause+Char(10)
	print @WhereClause 						
	
	EXEC (@MainQuery)		
	
	SELECT Distinct EmployeeId, FirstName, LastName, FullName, PatchId, PatchName, Distance, TradeID  FROM  #AvailableOperatives	
	--=================================================================================================================
	------------------------------------------------------ Step 3------------------------------------------------------						
	--This query selects the leaves of employees i.e the employess which we get in step1
	--M : morning - 08:00 AM - 12:00 PM
	--A : mean after noon - 01:00 PM - 05:00 PM
	--F : Single full day
	--F-F : Multiple full days
	--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
	--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days
	
	
	SELECT E_ABSENCE.STARTDATE as StartDate
	, E_ABSENCE.RETURNDATE	as EndDate
	, E_JOURNAL.employeeid as OperativeId
	,E_ABSENCE.HolType as HolType
	,E_ABSENCE.duration as Duration
	,CASE 
		WHEN HolType = 'M' THEN '09:00 AM'
		WHEN HolType = 'A' THEN '01:00 PM'
		WHEN HolType = 'F' THEN '00:00 AM'
		WHEN HolType = 'F-F' THEN '00:00 AM'
		WHEN HolType = 'F-M' THEN '00:00 AM'
		WHEN HolType = 'A-F' THEN '01:00 PM'
	END as StartTime
	,CASE 
		WHEN HolType = 'M' THEN '01:00 PM'
		WHEN HolType = 'A' THEN '05:00 PM'
		WHEN HolType = 'F' THEN '11:59 PM'
		WHEN HolType = 'F-F' THEN '11:59 PM'
		WHEN HolType = 'F-M' THEN '01:00 PM'
		WHEN HolType = 'A-F' THEN '11:59 PM'
	END as EndTime
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '09:00 AM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
	END as StartTimeInMin
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '05:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
	END as EndTimeInMin
	FROM E_JOURNAL 
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID AND E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID)
	WHERE
	(
		-- To filter for M&E Servicing or Cyclic Maintenance i.e annual leaves etc. where approval is needed
		( 
			E_ABSENCE.ITEMSTATUSID = 5
			AND itemnatureid in (2,3,4,5,6,8,9,10,11,13,14,15,16,32,43,47)   
		)
		OR
		-- To filter for sickness leaves. where the operative is now returned to work.
		( 
			ITEMNATUREID = 1
			AND E_ABSENCE.ITEMSTATUSID = 2
			AND E_ABSENCE.RETURNDATE IS NOT NULL 
		)
	)
	AND E_ABSENCE.RETURNDATE >= CONVERT(DATE,@startDate)
	AND E_JOURNAL.employeeid in(SELECT employeeid FROM  #AvailableOperatives)								

	--=================================================================================================================
	------------------------------------------------------ Step 4------------------------------------------------------						
	--This query selects the appointments of employees i.e the employess which we get in step1
	--Fault Appointments
	--Gas Appointments
	--Planned Appointments
	--M&E Servicing or Cyclic Maintenance Appointments
	-----------------------------------------------------------------------------------------------------------------------
	-- Fault Appointments
	SELECT AppointmentDate as AppointmentStartDate
	,AppointmentDate as AppointmentEndDate
	,OperativeId as OperativeId
	,Time as StartTime
	,EndTime as EndTime
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + Time,103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,p__property.postcode as PostCode
	,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
	,P__PROPERTY.TownCity as TownCity    
	,P__PROPERTY.County as County
	,'Fault Appointment'  as AppointmentType
	FROM FL_CO_Appointment 
	inner join fl_fault_appointment on FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
	inner join fl_fault_log on fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
	inner join p__property on fl_fault_log.propertyid = p__property.propertyid				
	WHERE 
	1=1
	AND appointmentdate >= CONVERT(DATE,@startDate)
	AND (FL_FAULT_STATUS.Description <> 'Cancelled' AND FL_FAULT_STATUS.Description <> 'Complete')
	AND operativeid in (SELECT employeeid FROM  #AvailableOperatives)	
	-----------------------------------------------------------------------------------------------------------------------
	--Appliance Appointments	
	UNION ALL 	
	SELECT 
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address
		,P__PROPERTY.TownCity as TownCity
		,P__PROPERTY.County as County
		,'Gas Appointment' as AppointmentType  
	FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	WHERE AS_JOURNAL.IsCurrent = 1		
		AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= Convert(date,@startDate,103)		
		AND AS_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  #AvailableOperatives)		
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,P__PROPERTY.TownCity as TownCity    
		,P__PROPERTY.County as County
		,'Planned Appointment' as AppointmentType
	FROM 
		PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
	WHERE 
		Convert(date,AppointmentDate,103) >= Convert(date,@startDate,103)		
		AND PLANNED_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  #AvailableOperatives)
		
	-----------------------------------------------------------------------------------------------------------------------
	--M&E Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTSTARTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTSTARTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,P__PROPERTY.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,ISNULL(P__PROPERTY.TownCity,'') as TownCity    
		,ISNULL(P__PROPERTY.County,'') as County
		,@msattype+' Appointment' as AppointmentType
	FROM 
		PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId 
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PROPERTYID = P__PROPERTY.PROPERTYID 
	WHERE 
		Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)		
		AND PDR_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  #AvailableOperatives)		
				
	DROP TABLE #AvailableOperatives		

END
