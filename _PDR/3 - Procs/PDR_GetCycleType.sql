USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPatch]    Script Date: 12/30/2014 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get Cycle Type For DropDown
 
    Author: Ali Raza
    Creation Date: Dec-30-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-30-2014      Ali Raza           Get Cycle Type For DropDown
    
    Execution Command:
    
    Exec PDR_GetCycleType
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetCycleType]
	
AS
BEGIN
	
	SET NOCOUNT ON;    
	SELECT CycleTypeId, CycleType from PDR_CycleType
END
