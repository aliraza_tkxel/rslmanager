/* =================================================================================    
    Page Description:     Get LocalAuthority For DropDown
 
    Author: Ali Raza
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Ali Raza           Get LocalAuthority For DropDown
    
    Execution Command:
    
    Exec PDR_GetLocalAuthority
  =================================================================================*/
CREATE PROCEDURE PDR_GetLocalAuthority
	
AS
BEGIN
	
	SET NOCOUNT ON;    
	SELECT LOCALAUTHORITYID,DESCRIPTION From G_LOCALAUTHORITY
END
GO
