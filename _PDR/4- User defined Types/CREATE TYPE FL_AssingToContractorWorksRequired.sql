USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PLANNED_AssingToContractorWorksRequired]    Script Date: 02/02/2015 12:31:42 ******/
CREATE TYPE [dbo].[FL_AssingToContractorWorksRequired] AS TABLE(
	[WorksRequired] [nvarchar](4000) NOT NULL,
	[NetCost] [smallmoney] NOT NULL,
	[VatType] [int] NOT NULL,
	[VAT] [smallmoney] NOT NULL,
	[GROSS] [smallmoney] NOT NULL,
	[PIStatus] [int] NOT NULL,
	[ExpenditureId] [int] NOT NULL
)
GO


