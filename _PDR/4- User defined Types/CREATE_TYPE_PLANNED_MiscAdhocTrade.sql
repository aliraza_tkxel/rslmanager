USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PLANNEDCOMPONENTTRADE]    Script Date: 03/10/2015 13:45:00 ******/
CREATE TYPE PLANNED_MiscAdhocTrade AS TABLE(
	[TRADEID] [int] NULL,
	durationString varchar (100) NULL,
	[DURATION] [float] NULL,
	tradeName varchar(100) NULL,
	[SortORDER] [int] NULL	
)
GO


