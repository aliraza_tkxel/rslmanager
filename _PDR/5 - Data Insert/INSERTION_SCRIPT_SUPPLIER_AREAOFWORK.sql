/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	7 January 2015
--  Description:	Insertion script Area of work related to Supplier.
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY     

	INSERT INTO [S_AREAOFWORK]([DESCRIPTION])
	VALUES ('M&E Servicing'),('PAT Testing'),('Fuel Servicing'),('Cyclic Maintenance')

END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 





