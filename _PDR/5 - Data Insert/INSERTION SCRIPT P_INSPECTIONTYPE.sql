/* =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <2 Jan 2015>
-- Description:	<Script to insert inspection type in P_INSPECTIONTYPE>
-- ============================================= */

INSERT [dbo].[P_INSPECTIONTYPE] ( [Description]) VALUES ( N'M&E Servicing')
INSERT [dbo].[P_INSPECTIONTYPE] ( [Description]) VALUES ( N'Cyclic Maintenance')
INSERT [dbo].[P_INSPECTIONTYPE] ( [Description]) VALUES ( N'PAT Testing')


