--//********************** Add Columns in Phase Table***************** //

ALTER TABLE P_Block ADD PhaseId int Default(0)
ALTER TABLE P_Block ADD NoOfProperties int Default(0)
ALTER TABLE P_Block ADD Ownership int Default(0)
ALTER TABLE P_Block ADD BlockReference varchar(200) Default(0)
ALTER TABLE P_Block ADD IsTemplate bit Default(1)