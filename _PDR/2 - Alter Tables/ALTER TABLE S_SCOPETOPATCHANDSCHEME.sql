/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	30 January 2015
--  Description:	Script to add new columns(SchemeId, BlockId) in S_SCOPETOPATCHANDSCHEME
--					
 '==============================================================================*/


ALTER TABLE S_SCOPETOPATCHANDSCHEME
ADD SchemeId int

ALTER TABLE S_SCOPETOPATCHANDSCHEME
ADD BlockId int
