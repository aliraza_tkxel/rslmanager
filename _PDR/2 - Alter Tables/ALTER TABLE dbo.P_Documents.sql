/*
   Friday, February 13, 20157:33:44 PM
   User: TKxel
   Server: 10.0.1.175
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_Documents
	DROP CONSTRAINT FK_P_Documents_P_Documents_Type
GO
ALTER TABLE dbo.P_Documents_Type SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_Documents_Type', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_Documents_Type', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_Documents_Type', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_Documents
	DROP CONSTRAINT FK_P_Documents_PropId
GO
ALTER TABLE dbo.P__PROPERTY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_Documents
	DROP CONSTRAINT DF_P_Documents_createdDate
GO
CREATE TABLE dbo.Tmp_P_Documents
	(
	DocumentId int NOT NULL IDENTITY (1, 1),
	PropertyId nvarchar(20) NULL,
	DocumentName varchar(500) NULL,
	DocumentType varchar(500) NULL,
	DocumentTypeId int NOT NULL,
	DocumentSubtypeId int NOT NULL,
	DocumentPath varchar(50) NULL,
	Keywords varchar(255) NULL,
	CreatedDate datetime NULL,
	DocumentSize varchar(50) NULL,
	DocumentFormat varchar(50) NULL,
	SchemeId int NULL,
	BlockId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_P_Documents SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_P_Documents ADD CONSTRAINT
	DF_P_Documents_createdDate DEFAULT (getdate()) FOR CreatedDate
GO
SET IDENTITY_INSERT dbo.Tmp_P_Documents ON
GO
IF EXISTS(SELECT * FROM dbo.P_Documents)
	 EXEC('INSERT INTO dbo.Tmp_P_Documents (DocumentId, PropertyId, DocumentTypeId, DocumentSubtypeId, DocumentPath, Keywords, CreatedDate, DocumentSize, DocumentFormat, SchemeId, BlockId)
		SELECT DocumentId, PropertyId, DocumentTypeId, DocumentSubtypeId, DocumentPath, Keywords, CreatedDate, DocumentSize, DocumentFormat, SchemeId, BlockId FROM dbo.P_Documents WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_P_Documents OFF
GO
DROP TABLE dbo.P_Documents
GO
EXECUTE sp_rename N'dbo.Tmp_P_Documents', N'P_Documents', 'OBJECT' 
GO
ALTER TABLE dbo.P_Documents ADD CONSTRAINT
	PK_P_Documents PRIMARY KEY CLUSTERED 
	(
	DocumentId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.P_Documents ADD CONSTRAINT
	FK_P_Documents_PropId FOREIGN KEY
	(
	PropertyId
	) REFERENCES dbo.P__PROPERTY
	(
	PROPERTYID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_Documents ADD CONSTRAINT
	FK_P_Documents_P_Documents_Type FOREIGN KEY
	(
	DocumentTypeId
	) REFERENCES dbo.P_Documents_Type
	(
	DocumentTypeId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_Documents', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_Documents', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_Documents', 'Object', 'CONTROL') as Contr_Per 