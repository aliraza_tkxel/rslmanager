if exists (select * from sysobjects where id = object_id(N'[dbo].[SessionState]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SessionState]
GO

CREATE TABLE [dbo].[SessionState] (
	[ID] uniqueidentifier  NOT NULL ,
	[Data] [image] NOT NULL ,
	[Last_Accessed] [datetime] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[SessionState] WITH NOCHECK ADD 
	CONSTRAINT [PK_SessionState] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO

