VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "Session2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const REQUEST_OBJECT As String = "Request"
Private Const RESPONSE_OBJECT As String = "Response"
Private Const APPLICATION_OBJECT As String = "Application"
Private Const CONNECTION_STRING As String = "SessionDSN"
Private Const TIME_OUT As String = "SessionTimeOut"
Private Const DEFAULT_TIMEOUT As Long = 20

Private myObjectContext As ObjectContext
Private myContentsEntity As mySession
Private mySessionID As String
Private myIsNewSession As Boolean
Private myDSNString As String
Private myTimeOut As Integer
Private mySessionPersistence As ISessionPersistence


Public Property Get Contents() As mySession
Attribute Contents.VB_UserMemId = 0
   
On Error GoTo ErrHandler:
   
    Const METHOD_NAME As String = "Contents"
   
    If myContentsEntity Is Nothing Then Call InitContents
    
    Set Contents = myContentsEntity
   
    Exit Property
   
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Property


Private Sub InitContents()

On Error GoTo ErrHandler:
   
    Const METHOD_NAME As String = "InitContents"

    
    If mySessionID = "" Then
        Set myContentsEntity = New mySession
        mySessionID = mySessionPersistence.GenerateKey
        myIsNewSession = True
    Else
        Set myContentsEntity = mySessionPersistence.LoadSession(mySessionID, myDSNString, myTimeOut)
    End If
        
    Exit Sub
   
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Sub



Private Sub SetDataForSessionID()

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "SetDataForSessionID"
    
    Call mySessionPersistence.SaveSession(mySessionID, myDSNString, myContentsEntity, myIsNewSession)
    
    If myIsNewSession Then Call WriteSessionID(mySessionID)
    
    Set myContentsEntity = Nothing
    Set myObjectContext = Nothing
    Set mySessionPersistence = Nothing
       
    Exit Sub
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Sub

Public Property Get SessionID() As String

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "SessionID"
    
    SessionID = mySessionID
    
    Exit Property
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Property


Private Function ReadSessionID()

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "ReadSessionID"
    
    ReadSessionID = myObjectContext.Item(REQUEST_OBJECT).Cookies(mySessionPersistence.SessionID)
    
    Exit Function
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Function

Private Sub WriteSessionID(ByRef aSessionID As String)

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "WriteSessionID"
    
    myObjectContext.Item(RESPONSE_OBJECT).Cookies(mySessionPersistence.SessionID) = aSessionID
    
    Exit Sub
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Sub

Private Function GetConnectionDSN()

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "GetConnectionDSN"
    
    GetConnectionDSN = myObjectContext.Item(APPLICATION_OBJECT).Contents(CONNECTION_STRING)
    
    Exit Function
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description
        
End Function

Private Function GetSessionTimeOut()

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "GetConnectionDSN"
    
    Dim str As String
    
    str = myObjectContext.Item(APPLICATION_OBJECT).Contents(TIME_OUT)
    If IsNumeric(str) Then
        GetSessionTimeOut = CLng(str)
    Else
        GetSessionTimeOut = DEFAULT_TIMEOUT
    End If
    
    Exit Function
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description
        
End Function



Private Sub Class_Initialize()

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "Class_Initialize"
     
    Set mySessionPersistence = New SessionPersistence
   
    Set myObjectContext = GetObjectContext()
    mySessionID = ReadSessionID()
    myDSNString = GetConnectionDSN()
    myTimeOut = GetSessionTimeOut()
    myIsNewSession = False
    Call InitContents
    
    Exit Sub
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description

End Sub

Private Sub Class_Terminate()

On Error GoTo ErrHandler:

    Const METHOD_NAME As String = "Class_Terminate"

    Call SetDataForSessionID
    
    Exit Sub
    
ErrHandler:

    Err.Raise Err.Number, METHOD_NAME & ":" & Err.Source, Err.Description
       
End Sub

