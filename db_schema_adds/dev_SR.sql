﻿/*
Run this script on a database with the schema represented by:

        C:\builds\BHA.rslmanager\dev\source\db_schema    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with:

        10.0.1.75.RSLBHALive

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 23/08/2012 17:31:11

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
USE RSLBHALive
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'BHG\simon.rogers')
CREATE LOGIN [BHG\simon.rogers] FROM WINDOWS
GO
CREATE USER [BHG\simon.rogers] FOR LOGIN [BHG\simon.rogers]
GO
PRINT N'Creating role portfolio_datareader'
GO
CREATE ROLE [portfolio_datareader]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'portfolio_datareader', N'BHG\simon.rogers'
GO
BEGIN TRANSACTION
GO
PRINT N'Refreshing [dbo].[STATEMENT_GRANT]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_GRANT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwTCProgrammeCustomerCount]'
GO
EXEC sp_refreshview N'[dbo].[vwTCProgrammeCustomerCount]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwTCPProgrammeCustomerCount]'
GO
EXEC sp_refreshview N'[dbo].[vwTCPProgrammeCustomerCount]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_GRANT]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_GRANT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[C_LASTCOMPLETEDACTION]'
GO
EXEC sp_refreshview N'[dbo].[C_LASTCOMPLETEDACTION]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_HOUSINGBENEFIT]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_HOUSINGBENEFIT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_INVOICE]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_INVOICE]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_LAREFUND]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_LAREFUND]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_PAYMENTCARD]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_PAYMENTCARD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_SINGLEITEMS]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_SINGLEITEMS]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwTCProgrammeResponse]'
GO
EXEC sp_refreshview N'[dbo].[vwTCProgrammeResponse]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_SUPPORTPAYMENT]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_SUPPORTPAYMENT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_TENANTREFUND]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_TENANTREFUND]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vw_Test]'
GO
EXEC sp_refreshview N'[dbo].[vw_Test]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwTCProgrammeOptedOut]'
GO
EXEC sp_refreshview N'[dbo].[vwTCProgrammeOptedOut]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwPDAProperty]'
GO
EXEC sp_refreshview N'[dbo].[vwPDAProperty]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[Test]'
GO
EXEC sp_refreshview N'[dbo].[Test]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwPDAProperty_SM]'
GO
EXEC sp_refreshview N'[dbo].[vwPDAProperty_SM]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwPDAPropertyCustomerContact]'
GO
EXEC sp_refreshview N'[dbo].[vwPDAPropertyCustomerContact]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwPDAPropertyCustomer]'
GO
EXEC sp_refreshview N'[dbo].[vwPDAPropertyCustomer]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwPDASurveyProperty]'
GO
EXEC sp_refreshview N'[dbo].[vwPDASurveyProperty]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[vwPropertyApplianceCP12]'
GO
EXEC sp_refreshview N'[dbo].[vwPropertyApplianceCP12]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_DIRECTDEBIT]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_DIRECTDEBIT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_HOUSINGBENEFIT]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_HOUSINGBENEFIT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_INVOICE]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_INVOICE]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_LAREFUND]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_LAREFUND]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_PAYMENTCARD]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_PAYMENTCARD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_SINGLEITEMS]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_SINGLEITEMS]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_SUPPORTPAYMENT]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_SUPPORTPAYMENT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_TENANTREFUND]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_TENANTREFUND]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_DIRECTDEBIT]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_DIRECTDEBIT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[STATEMENT_ALL]'
GO
EXEC sp_refreshview N'[dbo].[STATEMENT_ALL]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[CASHBOOK_ALL]'
GO
EXEC sp_refreshview N'[dbo].[CASHBOOK_ALL]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering permissions on [dbo].[P__PROPERTY]'
GO
GRANT SELECT ON  [dbo].[P__PROPERTY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P__PROPERTY_20090323]'
GO
GRANT SELECT ON  [dbo].[P__PROPERTY_20090323] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P__PROPERTY_NEW]'
GO
GRANT SELECT ON  [dbo].[P__PROPERTY_NEW] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ACCESSIBILITYSTANDARDS]'
GO
GRANT SELECT ON  [dbo].[P_ACCESSIBILITYSTANDARDS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ADAPTATIONCAT]'
GO
GRANT SELECT ON  [dbo].[P_ADAPTATIONCAT] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ADAPTATIONS]'
GO
GRANT SELECT ON  [dbo].[P_ADAPTATIONS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ANNUALSERVICINGS]'
GO
GRANT SELECT ON  [dbo].[P_ANNUALSERVICINGS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ASBESTOS]'
GO
GRANT SELECT ON  [dbo].[P_ASBESTOS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ASBRISK]'
GO
GRANT SELECT ON  [dbo].[P_ASBRISK] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ASBRISKLEVEL]'
GO
GRANT SELECT ON  [dbo].[P_ASBRISKLEVEL] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ASSETTYPE]'
GO
GRANT SELECT ON  [dbo].[P_ASSETTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ASSETTYPE_NROSH_MAIN]'
GO
GRANT SELECT ON  [dbo].[P_ASSETTYPE_NROSH_MAIN] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ASSETTYPE_NROSH_SUB]'
GO
GRANT SELECT ON  [dbo].[P_ASSETTYPE_NROSH_SUB] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ATTRIBUTES]'
GO
GRANT SELECT ON  [dbo].[P_ATTRIBUTES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ATTTOKITCHEN]'
GO
GRANT SELECT ON  [dbo].[P_ATTTOKITCHEN] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ATTTOSCHEME]'
GO
GRANT SELECT ON  [dbo].[P_ATTTOSCHEME] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_ATTTOSERVICES]'
GO
GRANT SELECT ON  [dbo].[P_ATTTOSERVICES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_BLOCK]'
GO
GRANT SELECT ON  [dbo].[P_BLOCK] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_CHARGE]'
GO
GRANT SELECT ON  [dbo].[P_CHARGE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_COUNCILTAXBAND]'
GO
GRANT SELECT ON  [dbo].[P_COUNCILTAXBAND] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DAILYSTATUS]'
GO
GRANT SELECT ON  [dbo].[P_DAILYSTATUS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEFECTS]'
GO
GRANT SELECT ON  [dbo].[P_DEFECTS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEVELOPMENT]'
GO
GRANT SELECT ON  [dbo].[P_DEVELOPMENT] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEVELOPMENT_FUNDING_NROSH]'
GO
GRANT SELECT ON  [dbo].[P_DEVELOPMENT_FUNDING_NROSH] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEVELOPMENT_SUPPORTEDCLIENTS]'
GO
GRANT SELECT ON  [dbo].[P_DEVELOPMENT_SUPPORTEDCLIENTS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEVELOPMENTFUNDINGLIST]'
GO
GRANT SELECT ON  [dbo].[P_DEVELOPMENTFUNDINGLIST] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEVELOPMENTTYPE]'
GO
GRANT SELECT ON  [dbo].[P_DEVELOPMENTTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DEVLOPMENTGRANT]'
GO
GRANT SELECT ON  [dbo].[P_DEVLOPMENTGRANT] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_DWELLINGTYPE]'
GO
GRANT SELECT ON  [dbo].[P_DWELLINGTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_31_MAR_05]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_31_MAR_05] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_31MARCH2008]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_31MARCH2008] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_COPY]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_COPY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_HISTORY]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_HISTORY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_HISTORY_20080414]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_HISTORY_20080414] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_HISTORY_COPY]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_HISTORY_COPY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FINANCIAL_TARGET]'
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_TARGET] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FUELTYPE]'
GO
GRANT SELECT ON  [dbo].[P_FUELTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FUNDINGAUTHORITY]'
GO
GRANT SELECT ON  [dbo].[P_FUNDINGAUTHORITY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_FURNISHING]'
GO
GRANT SELECT ON  [dbo].[P_FURNISHING] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_GARDEN]'
GO
GRANT SELECT ON  [dbo].[P_GARDEN] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_GASJOURNAL]'
GO
GRANT SELECT ON  [dbo].[P_GASJOURNAL] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_GASJOURNALHISTORY]'
GO
GRANT SELECT ON  [dbo].[P_GASJOURNALHISTORY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_HEATING]'
GO
GRANT SELECT ON  [dbo].[P_HEATING] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_KITCHENAPPLIANCES]'
GO
GRANT SELECT ON  [dbo].[P_KITCHENAPPLIANCES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_KPI]'
GO
GRANT SELECT ON  [dbo].[P_KPI] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_LETTINGJOBHISTORY]'
GO
GRANT SELECT ON  [dbo].[P_LETTINGJOBHISTORY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_LETTINGVOIDENTRIES]'
GO
GRANT SELECT ON  [dbo].[P_LETTINGVOIDENTRIES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_LEVEL]'
GO
GRANT SELECT ON  [dbo].[P_LEVEL] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_LGSR]'
GO
GRANT SELECT ON  [dbo].[P_LGSR] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_LGSR_HISTORY]'
GO
GRANT SELECT ON  [dbo].[P_LGSR_HISTORY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_LOCALAUTHORITYISTHISUSED]'
GO
GRANT SELECT ON  [dbo].[P_LOCALAUTHORITYISTHISUSED] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_MAINTENANCE]'
GO
GRANT SELECT ON  [dbo].[P_MAINTENANCE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_MANAGINGAGENT]'
GO
GRANT SELECT ON  [dbo].[P_MANAGINGAGENT] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_MARKETING]'
GO
GRANT SELECT ON  [dbo].[P_MARKETING] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_MOSTSUITABLEFOR]'
GO
GRANT SELECT ON  [dbo].[P_MOSTSUITABLEFOR] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_NROSH_TENANCYTYPE]'
GO
GRANT SELECT ON  [dbo].[P_NROSH_TENANCYTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_OWNERSHIP]'
GO
GRANT SELECT ON  [dbo].[P_OWNERSHIP] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PLANNEDINCLUDES]'
GO
GRANT SELECT ON  [dbo].[P_PLANNEDINCLUDES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PLANNEDINCLUDES2]'
GO
GRANT SELECT ON  [dbo].[P_PLANNEDINCLUDES2] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PLANNEDMAINTENANCE]'
GO
GRANT SELECT ON  [dbo].[P_PLANNEDMAINTENANCE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PORTFOLIOADAPTATIONS]'
GO
GRANT SELECT ON  [dbo].[P_PORTFOLIOADAPTATIONS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PRE_TARGETRENT_TBL]'
GO
GRANT SELECT ON  [dbo].[P_PRE_TARGETRENT_TBL] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PROPERTY_ASBESTOS_RISK]'
GO
GRANT SELECT ON  [dbo].[P_PROPERTY_ASBESTOS_RISK] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PROPERTY_ASBESTOS_RISKLEVEL]'
GO
GRANT SELECT ON  [dbo].[P_PROPERTY_ASBESTOS_RISKLEVEL] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PROPERTY_ASBESTOSK_RISK]'
GO
GRANT SELECT ON  [dbo].[P_PROPERTY_ASBESTOSK_RISK] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PROPERTY_IDENTITY]'
GO
GRANT SELECT ON  [dbo].[P_PROPERTY_IDENTITY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[p_property_sap]'
GO
GRANT SELECT ON  [dbo].[p_property_sap] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[p_property_sap2]'
GO
GRANT SELECT ON  [dbo].[p_property_sap2] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_PROPERTYTYPE]'
GO
GRANT SELECT ON  [dbo].[P_PROPERTYTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_REFURBISHMENT_HISTORY]'
GO
GRANT SELECT ON  [dbo].[P_REFURBISHMENT_HISTORY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_RENTFREQUENCY]'
GO
GRANT SELECT ON  [dbo].[P_RENTFREQUENCY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_RENTINCREASE_FILES]'
GO
GRANT SELECT ON  [dbo].[P_RENTINCREASE_FILES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_RENTINCREASE_LETTERTEXT]'
GO
GRANT SELECT ON  [dbo].[P_RENTINCREASE_LETTERTEXT] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_RMTOREPAIR]'
GO
GRANT SELECT ON  [dbo].[P_RMTOREPAIR] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_RUNNINGMAINTENANCE]'
GO
GRANT SELECT ON  [dbo].[P_RUNNINGMAINTENANCE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_RUNNINGTOPURCHASE]'
GO
GRANT SELECT ON  [dbo].[P_RUNNINGTOPURCHASE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_SCHEMEFACILITIES]'
GO
GRANT SELECT ON  [dbo].[P_SCHEMEFACILITIES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_SECURITY]'
GO
GRANT SELECT ON  [dbo].[P_SECURITY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_STATUS]'
GO
GRANT SELECT ON  [dbo].[P_STATUS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_STATUSCHANGEHISTORY]'
GO
GRANT SELECT ON  [dbo].[P_STATUSCHANGEHISTORY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_STOCKTYPE]'
GO
GRANT SELECT ON  [dbo].[P_STOCKTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_SUBSTATUS]'
GO
GRANT SELECT ON  [dbo].[P_SUBSTATUS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_SUPPLIEDSERVICES]'
GO
GRANT SELECT ON  [dbo].[P_SUPPLIEDSERVICES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_SUPPORTEDCLIENTGROUPS]'
GO
GRANT SELECT ON  [dbo].[P_SUPPORTEDCLIENTGROUPS] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_TARGETRENT]'
GO
GRANT SELECT ON  [dbo].[P_TARGETRENT] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_TARGETRENT_ADDITIONALASSET]'
GO
GRANT SELECT ON  [dbo].[P_TARGETRENT_ADDITIONALASSET] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_TARGETRENT_COPY]'
GO
GRANT SELECT ON  [dbo].[P_TARGETRENT_COPY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_USETYPE]'
GO
GRANT SELECT ON  [dbo].[P_USETYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_UTILITIES]'
GO
GRANT SELECT ON  [dbo].[P_UTILITIES] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WARRANTY]'
GO
GRANT SELECT ON  [dbo].[P_WARRANTY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WARRANTYTYPE]'
GO
GRANT SELECT ON  [dbo].[P_WARRANTYTYPE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WATERAUTHORITY]'
GO
GRANT SELECT ON  [dbo].[P_WATERAUTHORITY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WORKORDER]'
GO
GRANT SELECT ON  [dbo].[P_WORKORDER] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WORKORDER_OVERTIME]'
GO
GRANT SELECT ON  [dbo].[P_WORKORDER_OVERTIME] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WORKORDERENTITY]'
GO
GRANT SELECT ON  [dbo].[P_WORKORDERENTITY] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WORKORDERMODULE]'
GO
GRANT SELECT ON  [dbo].[P_WORKORDERMODULE] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WORKORDERTOPATCH]'
GO
GRANT SELECT ON  [dbo].[P_WORKORDERTOPATCH] TO [portfolio_datareader]
GO
PRINT N'Altering permissions on [dbo].[P_WOTOREPAIR]'
GO
GRANT SELECT ON  [dbo].[P_WOTOREPAIR] TO [portfolio_datareader]
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
