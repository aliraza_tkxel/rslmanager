CREATE TABLE [dbo].[AS_Pages]
(
[PageId] [int] NOT NULL IDENTITY(1, 1),
[PageName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NULL,
[ParentPageId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_Pages] ADD 
CONSTRAINT [PK_AS_Pages] PRIMARY KEY CLUSTERED  ([PageId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
