USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_IncidentType')
BEGIN
CREATE TABLE [dbo].[ASB_IncidentType]
(
	[IncidentTypeId] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[Description] [nvarchar](100) 
) 
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Alcohol')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Communal areas/loitering')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Domestic violence')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Drugs')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Garden')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Harassment/Threats')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Hate related')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Noise')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Other criminal behaviour')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Other violence')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Pets/animals')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Prostitution/Sex')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Rubbish')
INSERT [dbo].[ASB_IncidentType] ( [Description]) VALUES ( N'Vandalism')
INSERT [dbo].[ASB_IncidentType] ([Description]) VALUES ( N'Vehicles')
PRINT 'Table Created'
END

ELSE
BEGIN
PRINT 'Table already exist..'
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH