CREATE TABLE [dbo].[TBL_PDA_SURVEY_PARAMETER]
(
[SIDID] [bigint] NOT NULL,
[ParameterID] [int] NOT NULL,
[ValueID] [bigint] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_SURVEY_PARAMETER] ADD 
CONSTRAINT [PK_TBL_PDA_SURVEY_PARAMETER] PRIMARY KEY CLUSTERED  ([SIDID], [ParameterID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_SURVEY_PARAMETER] ADD
CONSTRAINT [FK_TBL_PDA_SURVEY_PARAMETER_TBL_PDA_SURVEY_ITEM] FOREIGN KEY ([SIDID]) REFERENCES [dbo].[TBL_PDA_SURVEY_ITEM] ([SIDID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
