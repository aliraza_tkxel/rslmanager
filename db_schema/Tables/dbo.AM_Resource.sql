CREATE TABLE [dbo].[AM_Resource]
(
[ResourceId] [int] NOT NULL IDENTITY(1, 1),
[LookupCodeId] [int] NOT NULL,
[EmployeeId] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Resource] ADD 
CONSTRAINT [PK_AM_Resource] PRIMARY KEY CLUSTERED  ([ResourceId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_Resource] ADD CONSTRAINT [FK_AM_Resource_AM_LookupCode] FOREIGN KEY ([LookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
