CREATE TABLE [dbo].[GS_FUEL_TYPE]
(
[FUELTYPEID] [int] NOT NULL IDENTITY(1, 1),
[FUELTYPE] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_FUEL_TYPE] ADD 
CONSTRAINT [PK__GS_FUEL___309557DC3D2AAC56] PRIMARY KEY CLUSTERED  ([FUELTYPEID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
