CREATE TABLE [dbo].[FL_FAULT_PAUSED]
(
[PauseID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FaultLogId] [int] NULL,
[PausedOn] [smalldatetime] NULL,
[Notes] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reason] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PausedBy] [int] NULL,
[FaultLogHistoryID] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_PAUSED] ADD
CONSTRAINT [FK_FL_FAULT_PAUSED_FL_FAULT_LOG_HISTORY] FOREIGN KEY ([FaultLogHistoryID]) REFERENCES [dbo].[FL_FAULT_LOG_HISTORY] ([FaultLogHistoryID])
ALTER TABLE [dbo].[FL_FAULT_PAUSED] ADD 
CONSTRAINT [PK_FL_FAULT_PAUSED] PRIMARY KEY CLUSTERED  ([PauseID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FL_FAULT_PAUSED] ADD CONSTRAINT [FL_FAULT_PAUSED_FL_FAULT_LOG] FOREIGN KEY ([FaultLogId]) REFERENCES [dbo].[FL_FAULT_LOG] ([FaultLogID])
GO
