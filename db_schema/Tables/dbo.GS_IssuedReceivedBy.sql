CREATE TABLE [dbo].[GS_IssuedReceivedBy]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[AppointmentID] [int] NOT NULL,
[CP12No] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssuedBy] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssuedDate] [smalldatetime] NOT NULL,
[ReceivedOnBehalf] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceivedDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_IssuedReceivedBy] ADD 
CONSTRAINT [PK_GS_IssuedReceivedBy] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[GS_IssuedReceivedBy] ADD
CONSTRAINT [FK_GS_IssuedReceivedBy_PS_Appointment] FOREIGN KEY ([AppointmentID]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO
