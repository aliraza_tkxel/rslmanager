IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CategoryId'
          AND Object_ID = Object_ID(N'E_alerts'))
BEGIN

alter table E_alerts add CategoryId [int] NULL

END



USE RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_alerts')
BEGIN
  PRINT 'Table Exists';
IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Rejected Faults')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(27,'Rejected Faults',27,
		'/BRSFaultLocator/Bridge.aspx?report=yes&rd=atc&rbc=true')
	PRINT 'Rejected Faults added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END	

IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Declaration of Interest: Pending Approval')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(30,'Declaration of Interest: Pending Approval',30,
		'/RSLHRModuleWeb/Bridge/?UserId={UserId}&Reports=DeclarationOfInterests')
	PRINT 'Declaration of Interest: Pending Approval added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT ' Row already exits Declaration of Interest: Pending Approval'
	END

IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Defects Requiring Approval')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(29,'Defects Requiring Approval',29,
		'/RSLApplianceServicing/Bridge.aspx?pg=dapr')
	PRINT 'Defects Requiring Approval added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Row already exits'
	END

IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='ASB Open Cases')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(28,'ASB Open Cases',250,
		'/RSLASBModuleWeb/ASB?reports=true&User={UserId}')
	PRINT 'ASB Open Cases added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END	

    UPDATE [dbo].[E_ALERTS]
  SET [AlertUrl]='/RSLASBModuleWeb/ASB?reports=true&User={UserId}'
  where [AlertName]='ASB Open Cases'

  UPDATE [dbo].[E_ALERTS]
  SET [AlertUrl]='/RSLHRModuleWeb/Bridge/?Module=MyJob&redirectTab=mnuMyStaff&UserId={UserId}'
  where [AlertName]= 'Leave Request' OR [AlertName]= 'Absentees'

  IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Training Approval (My Staff)')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(31,'Training Approval (My Staff)',260,
		'/RSLHRModuleWeb/Bridge/?Module=MyJob&redirectTab=mnuMyStaff&UserId={UserId}')
	PRINT 'Training Approval (My Staff) added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Row already exits'
	END	

	IF NOT EXISTS (	SELECT	1 
					FROM	E_ALERTS ea 
					WHERE	ea.AlertName='Pay Point Review')
	BEGIN
		
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		VALUES(34,'Pay Point Review',34,'/RSLHRModuleWeb/Bridge/?Module=MyJob&redirectTab=mnuMyStaff&isDirector=true&UserId={UserId}')

		PRINT 'Pay Point Review added successfully!'
	END
	ELSE
	BEGIN 
		PRINT 'Row already exits Pay Point Review'
	END
--========================================================================
-- US591 - Addition of new Alert 'Declaration of Interest: Pending Review'
--========================================================================

	IF NOT EXISTS (	SELECT	1 
					FROM	E_ALERTS ea 
					WHERE	ea.AlertName='Declaration of Interest: Pending Review')
	BEGIN
		
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		VALUES(33,'Declaration of Interest: Pending Review',33,'/RSLHRModuleWeb/Bridge/?Module=MyJob&redirectTab=mnuMyStaff&UserId={UserId}')

		PRINT 'Declaration of Interest: Pending Review added successfully!'
	END
	ELSE
	BEGIN 
		PRINT 'Row already exits Declaration of Interest: Pending Review'
	END


	IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Training Approval (HR)')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(32,'Training Approval (HR)',270,
		'/RSLHRModuleWeb/Bridge/?UserId={UserId}&Reports=GroupTraining&Director=true')
	PRINT 'Training Approval (HR) added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT 'Row already exits'
	END	

	IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Gift & Hospitality')
	BEGIN
		INSERT INTO E_alerts(AlertID,AlertName,SOrder,AlertUrl)
		 VALUES(35,'Gift & Hospitality',280,
		'/RSLHRModuleWeb/Bridge/?UserId={UserId}&Reports=GiftHospitality')
	PRINT 'Gift & Hospitality added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT 'Row already exits'
	END	

	IF NOT EXISTS (SELECT 1 FROM E_ALERTS ea WHERE ea.AlertName='Awaiting Invoice')
	BEGIN
		INSERT INTO E_alerts(AlertID, AlertName, SOrder, AlertUrl, CategoryId)
		 VALUES(37, 'Awaiting Invoice', 300,
		'/Finance/UploadInvoice.asp?BUDGETFILLTER=checked', 1)
	PRINT 'Awaiting Invoice added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT 'Awaiting Invoice already exits'
	END	

END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 
 
 