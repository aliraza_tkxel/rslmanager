USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'CM_Status')
BEGIN
		CREATE TABLE [dbo].[CM_Status](
			[StatusId] [int] IDENTITY(1,1) NOT NULL,
			[Title] [nvarchar](200) NULL,
			[Active] [bit] NULL,
		 CONSTRAINT [PK_CM_Status] PRIMARY KEY CLUSTERED 
		(
			[StatusId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

END

IF NOT EXISTS ( Select * from CM_Status where CM_Status.Title ='To Be Allocated' )
BEGIN
	SET IDENTITY_INSERT [dbo].[CM_Status] ON 


	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (1, N'To Be Allocated', 1)

	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (2, N'Assigned', 1)

	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (3, N'Accepted', 1)

	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (4, N'Works Completed', 1)

	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (5, N'Invoice Uploaded', 1)

	SET IDENTITY_INSERT [dbo].[CM_Status] OFF

END
IF NOT EXISTS ( Select * from CM_Status where CM_Status.Title ='Rejected' )
BEGIN
SET IDENTITY_INSERT [dbo].[CM_Status] ON 


	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (6, N'Rejected', 1)
	SET IDENTITY_INSERT [dbo].[CM_Status] OFF
	Print('Rejected added successfully!')
END


IF NOT EXISTS ( Select * from CM_Status where CM_Status.Title ='Cancelled' )
BEGIN
SET IDENTITY_INSERT [dbo].[CM_Status] ON 


	INSERT [dbo].[CM_Status] ([StatusId], [Title], [Active]) VALUES (7, N'Cancelled', 1)
	SET IDENTITY_INSERT [dbo].[CM_Status] OFF
	Print('Cancelled added successfully!')
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




