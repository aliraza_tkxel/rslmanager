CREATE TABLE [dbo].[G_NEWS]
(
[NewsID] [int] NOT NULL IDENTITY(1, 1),
[Content] [varchar] (7500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_G_NEWS_DateCreated] DEFAULT (getdate()),
[DateExpires] [smalldatetime] NULL,
[NewsFor] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewsImage] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[DateModified] [smalldatetime] NULL CONSTRAINT [DF_G_NEWS_DateModified] DEFAULT (getdate())
) ON [PRIMARY]
GO
