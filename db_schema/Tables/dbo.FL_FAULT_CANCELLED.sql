CREATE TABLE [dbo].[FL_FAULT_CANCELLED]
(
[CancelID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FaultLogId] [int] NULL,
[RecordedOn] [smalldatetime] NULL,
[Notes] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_CANCELLED] ADD 
CONSTRAINT [PK_FL_FAULT_CANCELLED] PRIMARY KEY CLUSTERED  ([CancelID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FL_FAULT_CANCELLED] ADD CONSTRAINT [FL_FAULT_CANCELLED_FL_FAULT_LOG] FOREIGN KEY ([FaultLogId]) REFERENCES [dbo].[FL_FAULT_LOG] ([FaultLogID])
GO
