Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'HR_Documents')
BEGIN

CREATE TABLE [dbo].[HR_Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeId] [int] NOT NULL,
	[EmployeeID] [int] NULL,
	[Title] [nvarchar](50) NULL,
	[DocumentPath] [nvarchar](500) NULL,
	[DocumentDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[Keywords] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [int] NULL,
 CONSTRAINT [PK_HR_Documents] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END --if

---=====================================================================================================================
---==================================Add new Columns-===================================================================
---=====================================================================================================================

IF COL_LENGTH('HR_Documents', 'EmployeeVisibility') IS NULL
	BEGIN
			ALTER TABLE HR_Documents
			ADD EmployeeVisibility bit NULL
			PRINT('COLUMN EmployeeVisibility CREATED')
	END	

IF COL_LENGTH('HR_Documents', 'DocumentVisibility') IS NULL
	BEGIN
			ALTER TABLE HR_Documents
			ADD DocumentVisibility int NULL
			CONSTRAINT DV_HR_Documents_E_DOCVISIBILITY DEFAULT 1
			CONSTRAINT FK_HR_Documents_E_DOCVISIBILITY FOREIGN KEY(DocumentVisibility) REFERENCES E_DOCVISIBILITY(DOCVISIBILITYID);
			PRINT('COLUMN DocumentVisibility CREATED')
	END	

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 
