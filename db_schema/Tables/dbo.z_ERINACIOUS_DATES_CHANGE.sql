CREATE TABLE [dbo].[z_ERINACIOUS_DATES_CHANGE]
(
[ORDERID] [float] NULL,
[ITEM_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_DESCRIPTION] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REPAIR_DATE] [smalldatetime] NULL,
[to_complete_by] [smalldatetime] NULL,
[DAYS TO COMPLETE] [float] NULL,
[ACTUAL_COMPLETION] [smalldatetime] NULL,
[OVER DUE DAYS] [float] NULL,
[act compl] [smalldatetime] NULL,
[F10] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OLD_COMPLETION_DATE_IN_CASE_OF_REVERSE] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
