/****** Script for SelectTopNRows command from SSMS  ******/

USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

	-------------------------------- CREATING TABLE IF NOT EXISTS ------------------------------------------

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'PA_ITEM')
	BEGIN

		CREATE TABLE [dbo].[PA_ITEM](
			[ItemID] [int] IDENTITY(1,1) NOT NULL,
			[AreaID] [int] NOT NULL,
			[ItemName] [varchar](50) NOT NULL,
			[ItemSorder] [int] NULL,
			[IsActive] [bit] NULL CONSTRAINT [DF__PA_ITEM__IsActiv__500941F7]  DEFAULT ((1)),
			[ParentItemId] [int] NULL,
			[ShowInApp] [bit] NULL CONSTRAINT [DF__PA_ITEM__ShowInA__28DB3194]  DEFAULT ((1)),
			[ShowListView] [bit] NULL CONSTRAINT [DF__PA_ITEM__ShowLis__4A3C255F]  DEFAULT ((0)),
			[IsForSchemeBlock] [bit] NULL,
		 CONSTRAINT [PK_PA_ITEM] PRIMARY KEY CLUSTERED 
		(
			[ItemID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
		) ON [PRIMARY]


		ALTER TABLE [dbo].[PA_ITEM]  WITH NOCHECK ADD  CONSTRAINT [FK_PA_ITEM_PA_AREA] FOREIGN KEY([AreaID])
		REFERENCES [dbo].[PA_AREA] ([AreaID])
		ON UPDATE CASCADE
		ON DELETE CASCADE

		ALTER TABLE [dbo].[PA_ITEM] CHECK CONSTRAINT [FK_PA_ITEM_PA_AREA]
		END

	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
GO


BEGIN TRANSACTION
BEGIN TRY
	------------------------------------  ADD NEW VALUES FOR TICKET# 10840 ----------------------------------------

	IF NOT EXISTS (	SELECT	1 FROM	PA_ITEM WHERE ItemName IN ('Sundry'))
		BEGIN 	
			INSERT INTO [dbo].[PA_ITEM]([AreaId],[ItemName],[ItemSorder],[isActive],[ParentItemId],
			[ShowInApp],[ShowListView],[isForSchemeBlock]) VALUES (8,'Sundry',6,0,NULL,0,0,1)
		END
	Update PA_ITEM set IsActive = 0 where ItemName = 'Sundry'
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

	------------------------------------  Upadte values FOR TICKET# 10832 ----------------------------------------

	IF  EXISTS ( Select 1 from pa_item WHERE  itemName = 'Electrics' )
	BEGIN 	
		update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Electrics'), isActive = 1 
		where itemid in (Select itemid from pa_item WHERE  itemName = 'Electrics') and IsForSchemeBlock = 1
	END

	IF  EXISTS ( Select 1 from pa_item WHERE  itemName = 'Meters' )
	BEGIN 
		update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Meters'), isActive = 1 
		where itemid in (Select itemid from pa_item WHERE  itemName = 'Meters') and IsForSchemeBlock = 1
	END
	
	IF  EXISTS ( Select 1 from pa_area PA inner join pa_item PI on PA.areaid = PI.areaid
		where PA.AreaName = 'Services' AND PI.ItemName IN ('Electric','Gas') )
	BEGIN 
		update pa_item set areaid = (Select AreaID from pa_area where AreaName = 'Meters')
		where itemid in (Select itemid from pa_area PA inner join pa_item PI on PA.areaid = PI.areaid
		where PA.AreaName = 'Services' AND PI.ItemName IN ('Electric','Gas') ) and IsForSchemeBlock = 1
	END

	IF  EXISTS ( SELECT	 1  FROM	PA_ITEM WHERE ItemName IN ('Smoke'))
	BEGIN 
		update pa_item 
		set areaId = (Select AreaID from pa_area where AreaName = 'Building Systems'), ItemSorder = 7, ParentItemId = null
		where itemname = 'Smoke' and IsForSchemeBlock = 1
	END
	------------------------------------ END TICKET# 10832 ----------------------------------------


	--==============================================================================================
	-- #12410 - Enable Smoke and Co detector for Property Survey app
	--==============================================================================================

		IF  EXISTS ( Select 1
			     FROM		PA_ITEM
							INNER JOIN PA_AREA ON PA_ITEM.AREAID = PA_AREA.AREAID
							INNER JOIN PA_LOCATION ON PA_AREA.LOCATIONID = PA_LOCATION.LOCATIONID
				 WHERE		itemname in ('CO','Smoke','Systems')
							AND LOCATIONNAME = 'Internals'
							AND AREANAME = 'Services' 
							AND PA_ITEM.ShowInApp = 0
			)
	BEGIN 	
		  UPDATE	PA_ITEM
		  SET		ShowInApp = 1
		  FROM		PA_ITEM
					INNER JOIN PA_AREA ON PA_ITEM.AREAID = PA_AREA.AREAID
					INNER JOIN PA_LOCATION ON PA_AREA.LOCATIONID = PA_LOCATION.LOCATIONID
		  WHERE		itemname in ('CO','Smoke','Systems')
					AND LOCATIONNAME = 'Internals'
					AND AREANAME = 'Services'
				
	END


	--==============================================================================================
	-- #12956 - Fixed 'Electrics' issue
	--==============================================================================================

	IF NOT EXISTS ( SELECT  1 
					 FROM	PA_ITEM INNER JOIN PA_AREA ON PA_ITEM.AREAID = PA_AREA.AREAID
					 WHERE  AreaName = 'Services' 
							AND PA_ITEM.IsForSchemeBlock = 0
							AND itemname in ('Electrics','Meters','Electric','Gas')
				  )
	BEGIN 	
			UPDATE	PA_ITEM
			SET		PA_ITEM.AREAID = (SELECT AREAID FROM PA_AREA WHERE AreaName = 'Services')
			WHERE	itemname in ('Electrics','Meters','Electric','Gas')  and IsForSchemeBlock = 0
			
			print 'Area name Services set for Electrics, Meters, Electric, Gas itemname '
	END



DECLARE @SAreaId INT

Select @SAreaId=PA_AREA.AreaID from PA_AREA where PA_AREA.AreaName ='Supplied Services'

IF NOT EXISTS (Select * from PA_ITEM where PA_ITEM.AreaID= @SAreaId and PA_ITEM.ItemName = 'Electricity')
BEGIN
	INSERT INTO PA_ITEM (PA_ITEM.AreaID,PA_ITEM.ItemName,PA_ITEM.ItemSorder,PA_ITEM.IsActive,PA_ITEM.ShowInApp,PA_ITEM.ShowListView,PA_ITEM.IsForSchemeBlock)
	VALUES(@SAreaId,'Electricity',1,1,0,0,1)
END

IF NOT EXISTS (Select * from PA_ITEM where PA_ITEM.AreaID= @SAreaId and PA_ITEM.ItemName = 'Gas')
BEGIN
	INSERT INTO PA_ITEM (PA_ITEM.AreaID,PA_ITEM.ItemName,PA_ITEM.ItemSorder,PA_ITEM.IsActive,PA_ITEM.ShowInApp,PA_ITEM.ShowListView,PA_ITEM.IsForSchemeBlock)
	VALUES(@SAreaId,'Gas',3,1,0,0,1)
END

--- us 552---
declare @ItemId as int
declare @ParentItemId as int

select @ItemId=ItemID from PA_ITEM where ItemName='Window cleaning'
select @ParentItemId=ItemId from PA_ITEM where ItemName='Cleaning'

IF EXISTS (	SELECT	1 FROM	PA_ITEM WHERE ItemID=@ItemId)
	BEGIN 	
	
		UPDATE	PA_ITEM SET		ParentItemId= @ParentItemId FROM	PA_ITEM WHERE ItemID=@ItemId
		print('window cleaning updated')	

	END

END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
