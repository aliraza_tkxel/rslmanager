CREATE TABLE [dbo].[P_LEVEL]
(
[LEVELID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_LEVEL] TO [rackspace_datareader]
GO
