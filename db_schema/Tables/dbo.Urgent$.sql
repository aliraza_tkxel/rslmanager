CREATE TABLE [dbo].[Urgent$]
(
[JS No#] [float] NULL,
[ORDERID] [float] NULL,
[SCHEME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROPERTYID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CDATE] [datetime] NULL,
[Completion Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Actual COMPLETIONDATE] [datetime] NULL,
[Days] [float] NULL,
[NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
