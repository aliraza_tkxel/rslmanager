Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF Not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_GradePointSalary')
BEGIN

CREATE TABLE [dbo].[E_GradePointSalary](
	[GradePointSalaryId] [int] IDENTITY(1,1) NOT NULL,
	[Grade] [int] NULL,
	[GradePoint] [int] NULL,
	[Salary] [float] NULL,
	[fiscalYearId] [int] NULL,
 CONSTRAINT [PK_E_GradePointSalary] PRIMARY KEY CLUSTERED 
(
	[GradePointSalaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

IF NOT EXISTS (SELECT * FROM E_GradePointSalary
           WHERE Grade = (select GRADEID from e_grade where Description = 'Exec1') and 
		   GradePoint = (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'))
BEGIN

insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
	((select GRADEID from e_grade where Description = 'Exec1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'), 0, 20)

END

IF NOT EXISTS (SELECT * FROM E_GradePointSalary
           WHERE Grade = (select GRADEID from e_grade where Description = 'Exec2') and 
		   GradePoint = (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'))
BEGIN

insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
	((select GRADEID from e_grade where Description = 'Exec2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'), 0, 20)

END

IF NOT EXISTS (SELECT * FROM E_GradePointSalary
           WHERE Grade = (select GRADEID from e_grade where Description = 'Agency') and 
		   GradePoint = (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'))
BEGIN

insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
	((select GRADEID from e_grade where Description = 'Agency'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'), 0, 20)

END

IF NOT EXISTS (SELECT * FROM E_GradePointSalary
           WHERE Grade = (select GRADEID from e_grade where Description = 'Apprentice') and 
		   GradePoint = (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'))
BEGIN

insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
	((select GRADEID from e_grade where Description = 'Apprentice'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'), 0, 20)

END

IF NOT EXISTS (SELECT * FROM E_GradePointSalary
           WHERE Grade = (select GRADEID from e_grade where Description = 'Work Placement/Volunteer') and 
		   GradePoint = (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'))
BEGIN

insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
	((select GRADEID from e_grade where Description = 'Work Placement/Volunteer'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'no point'), 0, 20)

END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 


