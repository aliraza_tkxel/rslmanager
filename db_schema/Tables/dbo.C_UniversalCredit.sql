USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT  * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'C_UniversalCredit')
    BEGIN

      CREATE TABLE [dbo].[C_UniversalCredit] (
        [UniversalCreditId] [int] IDENTITY (1, 1) NOT NULL,
        [JournalId] [int] NULL,
		[PaidDirectlyTo] [nvarchar] (100) NULL,
		[StartRentBalance] [float] NULL,
		[EndRentBalance] [float] NULL,
        [IsCurrent] [bit] NULL,
        [CreatedBy] [int] NULL,
		[Notes] [nvarchar] (1000) NULL,
		[StartDate] [date] NULL,
		[EndDate] [date] NULL,
		[CreationDate] [datetime] NULL,
        CONSTRAINT [PK_C_UniversalCredit] PRIMARY KEY CLUSTERED
        (
        [UniversalCreditId] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
      ) ON [PRIMARY]


      ALTER TABLE [dbo].[C_UniversalCredit] WITH CHECK ADD CONSTRAINT [FK_C_UniversalCredit] FOREIGN KEY ([JournalId])
      REFERENCES [dbo].[C_Journal] ([JournalId])


      ALTER TABLE [dbo].[C_UniversalCredit] CHECK CONSTRAINT [FK_C_UniversalCredit]

    END
    ELSE

    BEGIN
      PRINT 'Table Already Exist';
    END

    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_C_UniversalCredit')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_C_UniversalCredit
		  ON C_UniversalCredit (UniversalCreditId);  
	  PRINT 'IX_C_UniversalCredit created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_C_UniversalCredit Index Already Exist';
	  END

   


    --========================================================================================================
    --========================================================================================================

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH