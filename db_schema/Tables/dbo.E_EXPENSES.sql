CREATE TABLE [dbo].[E_EXPENSES]
(
[EXPENSESHISTORYID] [int] NOT NULL IDENTITY(1, 1),
[JOURNALID] [int] NULL,
[MONTHCLAIMED] [int] NULL,
[AMOUNT] [float] NULL,
[DESCRIPTION] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TOTALAMOUNT] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[E_EXPENSES] ADD CONSTRAINT [PK_EXPENSES] PRIMARY KEY NONCLUSTERED  ([EXPENSESHISTORYID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[E_EXPENSES] WITH NOCHECK ADD CONSTRAINT [FK_E_EXPENSES_E_JOURNAL] FOREIGN KEY ([JOURNALID]) REFERENCES [dbo].[E_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[E_EXPENSES] NOCHECK CONSTRAINT [FK_E_EXPENSES_E_JOURNAL]
GO
