CREATE TABLE [dbo].[PS_Appointment2SurveyForm]
(
[Appoint2SurveyFromId] [int] NOT NULL IDENTITY(1, 1),
[AppointId] [int] NOT NULL,
[SurveyFormId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Appointment2SurveyForm] ADD 
CONSTRAINT [PK_PS_Appointment2SurveyForm] PRIMARY KEY CLUSTERED  ([Appoint2SurveyFromId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Appointment2SurveyForm] ADD
CONSTRAINT [FK_PS_Appointment2SurveyForm_PS_Appointment] FOREIGN KEY ([AppointId]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
ALTER TABLE [dbo].[PS_Appointment2SurveyForm] ADD
CONSTRAINT [FK_PS_Appointment2SurveyForm_PS_SurveyForm] FOREIGN KEY ([SurveyFormId]) REFERENCES [dbo].[PS_SurveyForm] ([SurveyFormId])
GO
