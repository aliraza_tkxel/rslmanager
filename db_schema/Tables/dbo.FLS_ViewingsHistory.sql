USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_ViewingsHistory')
	BEGIN
	CREATE TABLE [dbo].[FLS_ViewingsHistory]
	(
		[ViewingHistoryId] [int] IDENTITY(1,1) NOT NULL,
		[ViewingId] [int] NOT NULL,
		[CustomerId] [int] NOT NULL,
		[PropertyId] [nvarchar](20) NOT NULL,
		[HousingOfficerId] [int] NOT NULL,
		[viewingStatusId] [int] NOT NULL,
		[ViewingDate] [smalldatetime] NOT NULL,
		[ViewingTime] [nvarchar](10) NOT NULL,
		[EndTime] [nvarchar](10)  NULL,
		[Duration] [float]  NULL,
		[ActualEndTime] [nvarchar](10)  NULL,
		[CreatedById] [int] NOT NULL,
		[IsActive] [bit] NOT NULL,
		[RecordingSource] [nvarchar](50)  NULL,
		[AppVersion] [nvarchar](100)  NULL,
		[CreatedOnApp] [smalldatetime]  NULL,
		[CreatedOnServer] [smalldatetime]  NULL,
		[LastModifiedOnApp] [smalldatetime]  NULL,
		[LastModifiedOnServer] [smalldatetime]  NULL,
		[Notes] [nvarchar](1000) NULL
		CONSTRAINT [PK_FLS_ViewingsHistory] PRIMARY KEY CLUSTERED 
		(
			[ViewingHistoryId] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) 		ON [PRIMARY]
	PRINT 'Table FLS_ViewingsHistory created'
	END
ELSE
BEGIN
PRINT 'Table FLS_ViewingsHistory already exist'
END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
