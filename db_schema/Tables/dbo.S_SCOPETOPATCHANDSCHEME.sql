Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'S_SCOPETOPATCHANDSCHEME')
BEGIN

	CREATE TABLE [dbo].[S_SCOPETOPATCHANDSCHEME]
	(
	[SCOPEID] [int] NOT NULL,
	[PATCHID] [int] NOT NULL,
	[DEVELOPMENTID] [int] NULL
	) ON [PRIMARY]
	ALTER TABLE [dbo].[S_SCOPETOPATCHANDSCHEME] ADD 
	CONSTRAINT [PK_SCOPEPATCHSCHEME] PRIMARY KEY CLUSTERED  ([SCOPEID], [PATCHID]) WITH (FILLFACTOR=100) ON [PRIMARY]
	ALTER TABLE [dbo].[S_SCOPETOPATCHANDSCHEME] ADD
	CONSTRAINT [FK__S_SCOPETO__SCOPE__058F9352] FOREIGN KEY ([SCOPEID]) REFERENCES [dbo].[S_SCOPE] ([SCOPEID])
	ALTER TABLE [dbo].[S_SCOPETOPATCHANDSCHEME] ADD
	CONSTRAINT [FK__S_SCOPETO__PATCH__0683B78B] FOREIGN KEY ([PATCHID]) REFERENCES [dbo].[E_PATCH] ([PATCHID])
	 

	ALTER TABLE [dbo].[S_SCOPETOPATCHANDSCHEME] ADD CONSTRAINT [FK__S_SCOPETO__DEVEL__0777DBC4] FOREIGN KEY ([DEVELOPMENTID]) REFERENCES [dbo].[P_DEVELOPMENT] ([DEVELOPMENTID])
	 
END
ELSE
	BEGIN
	Print 'S_SCOPETOPATCHANDSCHEME already exists!'
		IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME='PK_SCOPEPATCHSCHEME')
			BEGIN
				ALTER TABLE [S_SCOPETOPATCHANDSCHEME]
				DROP CONSTRAINT [PK_SCOPEPATCHSCHEME]
			END

		ALTER TABLE [S_SCOPETOPATCHANDSCHEME]
		ALTER COLUMN PATCHID int NULL
	END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

