Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AC_MODULES')
BEGIN

	CREATE TABLE [dbo].[AC_MODULES]
	(
	[MODULEID] [int] NOT NULL IDENTITY(1, 1),
	[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ACTIVE] [int] NULL,
	[DIRECTORY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PAGE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ALLACCESS] [int] NULL,
	[DISPLAY] [int] NULL,
	[IMAGE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IMAGEOPEN] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IMAGEMOUSEOVER] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IMAGETAG] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IMAGETITLE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ORDERTEXT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	) ON [PRIMARY]
END



--IF NOT EXISTS (Select 1 from AC_MODULES where DESCRIPTION='HR')
--	BEGIN  
--		INSERT [dbo].AC_MODULES ([DESCRIPTION], ACTIVE, DIRECTORY,PAGE, ALLACCESS,DISPLAY)
--		Values('HR',1,'RSLHRModuleWeb','Bridge/?UserId=',0,1)
--		PRINT 'HR Module added successfully'	
--	END
	
--	IF EXISTS(Select 1 from AC_MODULES where DESCRIPTION='My Job')
--		BEGIN
--		  UPDATE AC_MODULES Set DIRECTORY='RSLHRModuleWeb',PAGE='Bridge/?Module=MyJob&UserId=' where DESCRIPTION='My Job'
		  
--		  PRINT 'My Job Module updated successfully'	
--		END
--	IF NOT EXISTS(Select 1 from AC_MODULES where DESCRIPTION='Old My Job')
--		BEGIN
--			INSERT INTO [dbo].[AC_MODULES]
--           ([DESCRIPTION]
--           ,[ACTIVE]
--           ,[DIRECTORY]
--           ,[PAGE]
--           ,[ALLACCESS]
--           ,[DISPLAY]
--           ,[IMAGE]
--           ,[IMAGEOPEN]
--           ,[IMAGEMOUSEOVER]
--           ,[IMAGETAG]
--           ,[IMAGETITLE]
--           ,[ORDERTEXT])
--     VALUES
--           ('Old My Job'
--           ,1
--           ,'My Job'
--           ,'erm.asp'
--           ,1
--           ,0
--           ,NULL
--           ,NULL
--           ,NULL
--           ,NULL
--           ,NULL
--           ,NULL)
--		END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH