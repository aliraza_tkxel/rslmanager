CREATE TABLE [dbo].[AM_SupplierModule]
(
[SupplierModuleId] [int] NOT NULL IDENTITY(1, 1),
[AreaOfWorkId] [int] NOT NULL,
[NameOfContract] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PatchId] [int] NOT NULL,
[DevelopmentId] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_SupplierModule] ADD 
CONSTRAINT [PK_AM_SupplierModule] PRIMARY KEY CLUSTERED  ([SupplierModuleId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
