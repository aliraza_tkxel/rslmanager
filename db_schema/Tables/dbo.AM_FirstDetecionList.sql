CREATE TABLE [dbo].[AM_FirstDetecionList]
(
[FirstDetecionListId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NULL,
[TenancyId] [int] NULL,
[FirstDetectionDate] [datetime] NULL,
[IsDefaulter] [bit] NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_FirstDetecionList] ADD 
CONSTRAINT [PK__AM_First__AA3CB0946C259261] PRIMARY KEY CLUSTERED  ([FirstDetecionListId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
