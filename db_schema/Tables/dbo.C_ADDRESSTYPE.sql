USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'C_ADDRESSTYPE')
BEGIN
CREATE TABLE [dbo].[C_ADDRESSTYPE](
	[ADDRESSTYPEID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [nvarchar](100) NULL,
	[CAREOF] [bit] NOT NULL CONSTRAINT [DF_C_ADDRESSTYPE_CAREOF]  DEFAULT (0)
) ON [PRIMARY]

INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'Previous Address', 0)
INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'Next of Kin', 1)
INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'Social Worker', 1)
INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'Forwarding Address', 0)
INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'Current', 0)
INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'POA', 1)
PRINT 'Table C_ADDRESSTYPE created'
END
ELSE
BEGIN
PRINT 'Table C_ADDRESSTYPE already exist'
IF NOT EXISTS (Select 1 from C_ADDRESSTYPE where DESCRIPTION='ICE')
	BEGIN  
	INSERT [dbo].[C_ADDRESSTYPE] ([DESCRIPTION], [CAREOF]) VALUES (N'ICE', 1)
	PRINT 'DESCRIPTION ICE added successfully'	
	END
ELSE
BEGIN
PRINT 'DESCRIPTION ICE already exist'
END
END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH

