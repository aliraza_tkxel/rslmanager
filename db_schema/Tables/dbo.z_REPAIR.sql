CREATE TABLE [dbo].[z_REPAIR]
(
[REPAIRHISTORYID] [int] NULL,
[JOURNALID] [int] NULL,
[ITEMSTATUSID] [int] NULL,
[ITEMACTIONID] [int] NULL,
[LASTACTIONDATE] [smalldatetime] NULL CONSTRAINT [DF_z_REPAIR_LASTACTIONDATE] DEFAULT (getdate()),
[LASTACTIONUSER] [int] NULL,
[ITEMDETAILID] [int] NULL,
[CONTRACTORID] [int] NULL,
[TITLE] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOTES] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
