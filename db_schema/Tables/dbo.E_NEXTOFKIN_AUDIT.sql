CREATE TABLE [dbo].[E_NEXTOFKIN_AUDIT]
(
[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PK] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextofkinId] [int] NULL,
[EmployeeId] [int] NULL,
[FieldName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValue] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDate] [datetime] NULL,
[UserName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastActionUser] [int] NULL,
[BatchId] [uniqueidentifier] NULL,
[AppName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HostName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_E_NEXTOFKIN_EMPLOYEEID] ON [dbo].[E_NEXTOFKIN_AUDIT] ([EmployeeId]) WITH (FILLFACTOR=100) ON [PRIMARY]

GO
