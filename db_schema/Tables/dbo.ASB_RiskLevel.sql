USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_RiskLevel')
BEGIN
CREATE TABLE [dbo].[ASB_RiskLevel]
(
	[RiskLevelId] [int] IDENTITY(1,1)PRIMARY KEY  NOT NULL,
	[Description] [nvarchar](100)
) 
INSERT [dbo].[ASB_RiskLevel] ([Description]) VALUES (N'Low')
INSERT [dbo].[ASB_RiskLevel] ([Description]) VALUES (N'Medium')
INSERT [dbo].[ASB_RiskLevel] ([Description]) VALUES (N'High')
PRINT 'Table Created'
END
ELSE
BEGIN
PRINT 'Table already exist..'
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH