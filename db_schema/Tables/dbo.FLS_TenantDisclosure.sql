USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_TenantDisclosure')
BEGIN
	CREATE TABLE [dbo].[FLS_TenantDisclosure](
		[DisclosureId] [int] IDENTITY(1,1) NOT NULL,
		[EvictedFromTenancy] [BIT] NULL,
		[Reason] [nvarchar](200) NULL,
		[DebtReliefOrder] [bit] NULL,
		[UnsatisfiedCCJ] [bit] NULL,
		[CriminalOffence] [bit] NULL,
		[TenancyId] [int] NULL,
		[CustomerId] [int] NULL,
	 CONSTRAINT [PK_FLS_TenantDisclosure] PRIMARY KEY CLUSTERED 
	(
		[DisclosureId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
PRINT 'Table FLS_TenantDisclosure created'
END
ELSE
BEGIN

PRINT 'Table FLS_TenantDisclosure already exist'
END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH


