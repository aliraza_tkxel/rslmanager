CREATE TABLE [dbo].[AM_Documents]
(
[DocumentId] [int] NOT NULL IDENTITY(1, 1),
[CaseId] [int] NULL,
[DocumentName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseHistoryId] [int] NULL,
[ActivityId] [int] NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Documents] ADD 
CONSTRAINT [PK_AM_Documents] PRIMARY KEY CLUSTERED  ([DocumentId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Documents] ADD
CONSTRAINT [FK_AM_Documents_AM_Activity] FOREIGN KEY ([ActivityId]) REFERENCES [dbo].[AM_Activity] ([ActivityId])
ALTER TABLE [dbo].[AM_Documents] ADD
CONSTRAINT [FK_AM_Documents_AM_CaseHistory] FOREIGN KEY ([CaseHistoryId]) REFERENCES [dbo].[AM_CaseHistory] ([CaseHistoryId])
GO

ALTER TABLE [dbo].[AM_Documents] ADD CONSTRAINT [FK_AM_Documents_AM_Case] FOREIGN KEY ([CaseId]) REFERENCES [dbo].[AM_Case] ([CaseId])
GO
