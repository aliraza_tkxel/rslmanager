USE RSLBHALive
GO
BEGIN TRANSACTION
BEGIN TRY
--Check if table exists or not
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_Documents_SubType')
BEGIN
	

	CREATE TABLE [dbo].[P_Documents_SubType](
	[DocumentSubtypeId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeId] [int] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	 CONSTRAINT [PK__P_Docume__DD7012646023A6E2] PRIMARY KEY CLUSTERED 
	(
		[DocumentSubtypeId] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[P_Documents_SubType]  WITH NOCHECK ADD  CONSTRAINT [p_documents_subtype_parent_FK] FOREIGN KEY([DocumentTypeId])
	REFERENCES [dbo].[P_Documents_Type] ([DocumentTypeId])
	ON UPDATE CASCADE
	ON DELETE CASCADE

	ALTER TABLE [dbo].[P_Documents_SubType] CHECK CONSTRAINT [p_documents_subtype_parent_FK]

	ALTER TABLE [dbo].[P_Documents_SubType] ADD  CONSTRAINT [DF_P_Documents_Subtype_active]  DEFAULT ((1)) FOR [Active]

	ALTER TABLE [dbo].[P_Documents_SubType] ADD  CONSTRAINT [DF_P_Documents_Subtype_order]  DEFAULT ((1000)) FOR [Order]

	  PRINT 'Table Created';
 END
 
 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_Documents_SubType')
BEGIN
  PRINT 'Table Exists';
 
 
--IF EXISTS (Select 1 from P_Documents_Type where Title='CP12')
--	BEGIN  
--		PRINT 'CP12 already exist'
--		IF NOT EXISTS(Select 1 from P_Documents_SubType where Title='Certificate' and DocumentTypeId = (Select DocumentTypeId from P_Documents_Type where Title='CP12')  )
--		BEGIN
--		INSERT INTO P_Documents_SubType(DocumentTypeId, Title, Active, [Order])
--	    VALUES((Select DocumentTypeId from P_Documents_Type where Title='CP12'),'Certificate',1,1) 
--	    PRINT 'Certificate added'
--		END
--	END
IF NOT EXISTS (Select 1 from P_Documents_Type where Title='CP12')
	BEGIN	

	INSERT INTO P_Documents_Type (Title, Active, [Order]) VALUES  ('CP12',1,1000)
	PRINT 'New CP12 added'
	
	INSERT INTO P_Documents_SubType(DocumentTypeId, Title, Active, [Order])
	VALUES(SCOPE_IDENTITY(),'Certificate',1,1) 
END

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title='kitchen inspection')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order]) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = 'Kitchens'), 'kitchen inspection', 1, 1000)

	PRINT 'kitchen inspection added'

END


IF COL_LENGTH('P_Documents_SubType','IsDevelopment') IS NULL
 BEGIN
	ALTER TABLE P_Documents_SubType
	ADD IsDevelopment int NULL;
	print('IsDevelopment is added')
 END

IF COL_LENGTH('P_Documents_SubType','IsScheme') IS NULL
 BEGIN
	ALTER TABLE P_Documents_SubType
	ADD IsScheme int NULL;
	print('IsScheme is added')
 END

IF COL_LENGTH('P_Documents_SubType','IsProperty') IS NULL
 BEGIN
	ALTER TABLE P_Documents_SubType
	ADD IsProperty int NULL;
	print('IsProperty is added')
 END
Declare @mainQuery nvarchar (max)
set @mainQuery = 'IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Employers Requirements'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Building Contract Documents''), ''Employers Requirements'', 1, 1000,1,0,0)

	PRINT ''Employers Requirements added''

END

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Form of Tender'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Building Contract Documents''), ''Form of Tender'', 1, 1000,1,0,0)

	PRINT ''Form of Tender added''

END

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Conveyance'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Conveyance'', 1, 1000,1,0,0)

	PRINT ''Conveyance added''

END
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Financial Account Summary'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Building Contract Documents''), ''Financial Account Summary'', 1, 1000,0,0,0)
	PRINT ''Financial Account Summary added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =0
  where title =''Financial Account Summary''
  print(''Financial Account Summary updated'')
end
---------------------------------------------------------------------------------------------------------
IF exists (select 1 from P_Documents_SubType where title = ''Agreement'' and isDevelopment is null and isScheme is null and isProperty is null )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Agreement'' and isDevelopment is null and isScheme is null and isProperty is null
		print(''Agreement is updated'')
	end
IF not exists (select * from P_Documents_SubType where title = ''Chancel search indemnity'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Chancel search indemnity'', 1, 1000,0,1,1)
		print(''Chancel search indemnity added'')	
	END
IF exists (select 1 from P_Documents_SubType where title = ''Chancel search indemnity'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty = 1 
		where title = ''Chancel search indemnity''
		print(''Chancel search indemnity'')
	end	
IF not exists (select 1 from P_Documents_SubType where title = ''Contaminated land indemnity'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Contaminated land indemnity'', 1, 1000,0,1,1)
		print(''Contaminated land indemnity added'')	
	END
IF exists (select 1 from P_Documents_SubType where title = ''Contaminated land indemnity'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty = 1 
		where title = ''Contaminated land indemnity''
		print(''Contaminated land indemnity'')
	end	
IF exists (select 1 from P_Documents_SubType where title = ''Conveyance'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Conveyance''
		print(''Conveyance is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Contract of Sale'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Contract of Sale''
		print(''Contract of Sale is updated'')
	end
else 
	BEGIn
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Contract of Sale'', 1, 1000,1,1,1)
		print(''Contract of Sale'')
	end
-----------------------------------------------------------------------------------------------------
IF not exists (select 1 from P_Documents_SubType where title = ''Land Registry Title'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Land Registry Title'', 1, 1000,0,0,1)
		print(''Land Registry Title'')	
	END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Land Registry Title''
  print(''Land Registry Title updated'')
end
-----------------------------------------------------------------------------------------------------
IF not exists (select 1 from P_Documents_SubType where title = ''Party Wall Documentation'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Party Wall Documentation'', 1, 1000,0,0,1)
		print(''Land Registry Title'')	
	END
IF not exists (select 1 from P_Documents_SubType where title = ''Party Wall Award'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Party Wall Award'', 1, 1000,0,0,1)
		print(''Party Wall Award'')	
	END
IF exists (select 1 from P_Documents_SubType where title = ''Title deed'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Title deed''
		print(''Title deed is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Transfer'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Transfer''
		print(''Transfer is updated'')
	end

IF exists (select 1 from P_Documents_SubType where title = ''Building Regulations Completion Certificate'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Building Regulations Completion Certificate''
		print(''Building Regulations Completion Certificate is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''NHBC Certificate'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''NHBC Certificate''
		print(''NHBC Certificate is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''NHBC Covernote'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''NHBC Covernote''
		print(''NHBC Covernote is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Making Good Defects Certificate'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Making Good Defects Certificate''
		print(''Making Good Defects Certificate is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''HCA Section 9 Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''HCA Section 9 Agreement''
		print(''HCA Section 9 Agreement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Legal Charge Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Legal Charge Agreement''
		print(''Legal Charge Agreement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Chancel Repair Obligation '' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Chancel Repair Obligation ''
		print(''Chancel Repair Obligation  is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Chancel search indemnity'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
		where title = ''Chancel search indemnity''
		print(''Chancel search indemnity  is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Contaminated land indemnity'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
		where title = ''Contaminated land indemnity''
		print(''Contaminated land indemnity  is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Contract of Sale'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Contract of Sale''
		print(''Contract of Sale  is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Deeds and Covenants'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =0
		where title = ''Deeds and Covenants''
		print(''Deeds and Covenants is updated'')
	end
IF not exists (select 1 from P_Documents_SubType where title = ''Defective Lease Indeminity'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Defective Lease Indeminity'', 1, 1000,0,1,0)
		print(''Defective Lease Indeminity'')	
	END
IF not exists (select 1 from P_Documents_SubType where title = ''Defective title (lack of consent) Indemnity'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''Defective title (lack of consent) Indemnity'', 1, 1000,0,1,0)
		print(''Defective title (lack of consent) Indemnity'')	
	END
IF exists (select 1 from P_Documents_SubType where title = ''Lease'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Lease''
		print(''Lease is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Management Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Management Agreement''
		print(''Management Agreement is updated'')
	end
IF not exists (select 1 from P_Documents_SubType where title = ''No search indemnity'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Legal Documents''), ''No search indemnity'', 1, 1000,0,1,0)
		print(''No search indemnity'')	
	END
IF exists (select 1 from P_Documents_SubType where title = ''Management Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Nomination Agreement''
		print(''Nomination Agreement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Section 104 Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Section 104 Agreement''
		print(''Section 104 Agreement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Section 106 Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Section 106 Agreement''
		print(''Section 106 Agreement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Section 38 Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Section 38 Agreement''
		print(''Section 38 Agreement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Title Deed'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Title Deed''
		print(''Title Deed is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Title Restrictions'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Title Restrictions''
		print(''Title Restrictions is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Transfer'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Transfer''
		print(''Title Restrictions is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Wayleave / Easement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Wayleave / Easement''
		print(''Wayleave / Easement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Wayleave / Easement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Wayleave / Easement''
		print(''Wayleave / Easement is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Building Regulations Completion Certificate'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 1, isProperty =1
		where title = ''Building Regulations Completion Certificate''
		print(''Building Regulations Completion Certificate is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Tree Preservation Order'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Tree Preservation Order''
		print(''Tree Preservation Order is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Whole Life Costing'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Whole Life Costing''
		print(''Whole Life Costing is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Development Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =1
		where title = ''Development Agreement''
		print(''Development Agreement is updated'')
	end
IF not exists (select 1 from P_Documents_SubType where title = ''Final Account Summary'')
	begin
		INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
		VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Building Contract Documents''), ''Final Account Summary'', 1, 1000,1,0,0)
		print(''Final Account Summary'')	
	END
IF exists (select 1 from P_Documents_SubType where title = ''JCT Contract'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''JCT Contract''
		print(''JCT Contract is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''PC Certificate'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''PC Certificate''
		print(''PC Certificate is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Application for HCA Grant'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Application for HCA Grant''
		print(''Application for HCA Grant is updated'')
	end
IF exists (select 1 from P_Documents_SubType where title = ''Confirmation of HCA Grant'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Confirmation of HCA Grant''
		print(''Confirmation of HCA Grant is updated'')
	end

IF exists (select 1 from P_Documents_SubType where title = ''Report on Title'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Report on Title''
		print(''Confirmation of HCA Grant is updated'')
	end	
IF exists (select 1 from P_Documents_SubType where title = ''Building Regulations Full Plans Approval'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Building Regulations Full Plans Approval''
		print(''Building Regulations Full Plans Approval is updated'')
	end	

IF exists (select 1 from P_Documents_SubType where title = ''Listed Building Consent'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Listed Building Consent''
		print(''Listed Building Consent is updated'')
	end	

IF exists (select 1 from P_Documents_SubType where title = ''Outline Planning Approval'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Outline Planning Approval''
		print(''Outline Planning Approval is updated'')
	end	
IF exists (select 1 from P_Documents_SubType where title = ''Planning - Release of Conditions'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Planning - Release of Conditions''
		print(''Planning - Release of Conditions is updated'')
	end	
IF exists (select 1 from P_Documents_SubType where title = ''Planning Approval'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Planning Approval''
		print(''Planning Approval'')
	end	
IF exists (select 1 from P_Documents_SubType where title = ''Planning Approval of Reserved Matters'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 1, isScheme = 0, isProperty =0
		where title = ''Planning Approval of Reserved Matters''
		print(''Planning Approval of Reserved Matters'')
	end	

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Bathroom Inspection'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Bathrooms''), ''Bathroom Inspection'', 1, 1000,0,1,1)

	PRINT ''Bathroom Inspection added''

END
IF EXISTS (select 1 from P_Documents_SubType where title = ''Disclaimer Form - Bathroom'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
		where title = ''Disclaimer Form - Bathroom''
		print(''Disclaimer Form - Bathroom'')
	end	
IF EXISTS (select 1 from P_Documents_SubType where title = ''Disclaimer Form - Kitchen'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
		where title = ''Disclaimer Form - Kitchen''
		print(''Disclaimer Form - Kitchen'')
	end	

IF EXISTS (select 1 from P_Documents_SubType where title = ''Disclaimer Form - Other'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
		where title = ''Disclaimer Form - Other''
		print(''Disclaimer Form - Other'')
	end	

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Drainage Survey'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Drainage''), ''Drainage Survey'', 1, 1000,0,0,1)

	PRINT ''Drainage Survey added''

END
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Service/Maintenance Check List'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryId = 3), ''Service/Maintenance Check List'', 1, 1000,0,1,1)

	PRINT ''Service/Maintenance Check List''

END
IF EXISTS (Select 1 from P_Documents_SubType where Title=''Service/Maintenance Check List'')
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, DocumentTypeId=(SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryId = 3), isProperty =1
		where title = ''Service/Maintenance Check List''
		print(''Service/Maintenance Check List'')	

END
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Warning/Advice Notice Report'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryId = 3), ''Warning/Advice Notice Report'', 1, 1000,0,1,1)

	PRINT ''Warning/Advice Notice Report''

END

IF EXISTS (Select 1 from P_Documents_SubType where Title=''Warning/Advice Notice Report'')
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, DocumentTypeId=(SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryId = 3), isProperty =1
		where title = ''Warning/Advice Notice Report''
		print(''Warning/Advice Notice Report'')	

	
END

IF EXISTS (select 1 from P_Documents_SubType where title = ''kitchen inspection'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
		where title = ''kitchen inspection''
		print(''kitchen inspection'')
	end	
IF EXISTS (select 1 from P_Documents_SubType where title = ''Layout Plan'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
		where title = ''Layout Plan''
		print(''Layout Plan'')
	end	
IF EXISTS (select 1 from P_Documents_SubType where title = ''Maintenance Report'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1, DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts'' and categoryid = 3)
		where title = ''Maintenance Report''
		print(''Maintenance Report'')
	end	
--IF EXISTS (select 1 from P_Documents_SubType where title = ''Other Documents (quotes/reports/notifications)'' )
--	BEGIN
--		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1, DocumentTypeId = 48
--		where title = ''Other Documents (quotes/reports/notifications)''
--		print(''Other Documents (quotes/reports/notifications)'')
--	end	

IF EXISTS (select 1 from P_Documents_SubType where title = ''Garage License'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
		where title = ''Garage License''
		print(''Garage License'')
	end	
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Survery'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Property Information''), ''Survery'', 1, 1000,0,0,1)

	PRINT ''Survery''

END
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Air Permeability Test'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Ventilation ''), ''Air Permeability Test'', 1, 1000,0,0,1)

	PRINT ''Air Permeability Test''

END
IF EXISTS (select 1 from P_Documents_SubType where title = ''Air Permeability Test'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
		where title = ''Air Permeability Test''
		print(''Air Permeability Test'')
	end	

IF EXISTS (select 1 from P_Documents_SubType where title = ''Ventilation Unit Guarantee'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
		where title = ''Ventilation Unit Guarantee''
		print(''Ventilation Unit Guarantee'')
	end	
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Routine Preventative Maintenance Visit'')
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''CCTV'' and categoryid = 3), ''Routine Preventative Maintenance Visit'', 1, 1000,0,1,0)

	PRINT ''Routine Preventative Maintenance Visit''

END
IF EXISTS (select 1 from P_Documents_SubType where title = ''Service Level Agreement'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Service Level Agreement''
		print(''Service Level Agreement'')
	end	
IF EXISTS (select 1 from P_Documents_SubType where title = ''Supporting People'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Supporting People''
		print(''Supporting People'')
	end
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Other Documents (Quotes/Reports/Notifications)'' and DocumentTypeId = (SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Assisted Baths and Overhead Hoists'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Assisted Baths and Overhead Hoists'' and categoryid = 3), ''Other Documents (Quotes/Reports/Notifications)'', 1, 1000,0,1,0)

	PRINT ''Other Documents (Quotes/Reports/Notifications)''

END	

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Other Documents (Quotes/Reports/Notifications)'' and DocumentTypeId = (SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Door Entry Systems'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Door Entry Systems'' and categoryid = 3), ''Other Documents (Quotes/Reports/Notifications)'', 1, 1000,0,1,0)

	PRINT ''Other Documents (Quotes/Reports/Notifications)''

END	
-------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Other Documents (Quotes/Reports/Notifications)'' and DocumentTypeId = (SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts'' and categoryid = 3), ''Other Documents (Quotes/Reports/Notifications)'', 1, 1000,0,1,1)

	PRINT ''Other Documents (Quotes/Reports/Notifications)''
	 
END	
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where Title=''Other Documents (Quotes/Reports/Notifications)'' and DocumentTypeId = (SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts'' and categoryid = 3)
  print(''Other Documents (Quotes/Reports/Notifications) updated for GENERAL '')
end
-------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Reactive Repair'' and DocumentTypeId = (SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 3), ''Reactive Repair'', 1, 1000,0,1,0)

	PRINT ''Reactive Repair''

END	

IF EXISTS (select 1 from P_Documents_SubType where title = ''Fire Investigation Report'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Fire Investigation Report''
		print(''Fire Investigation Report'')
	end
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Fire Officer Letters'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Management'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Management'' and categoryid = 3), ''Fire Officer Letters'', 1, 1000,0,1,0)

	PRINT ''Fire Officer Letters''

END	
IF EXISTS (select 1 from P_Documents_SubType where title = ''Fire Risk Assessment Action Points'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Fire Risk Assessment Action Points''
		print(''Fire Risk Assessment Action Points'')
	end

IF EXISTS (select 1 from P_Documents_SubType where title = ''Site Testing Instructions'' )
	BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Site Testing Instructions''
		print(''Site Testing Instructions'')
	end

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Home User Guide'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Scheme Information'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Scheme Information'' and categoryid = 3), ''Home User Guide'', 1, 1000,0,1,0)

	PRINT ''Home User Guide''

END

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Scheme Brochure'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Scheme Information'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Scheme Information'' and categoryid = 3), ''Scheme Brochure'', 1, 1000,0,1,0)

	PRINT ''Scheme Brochure''

END

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Site Plan'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Scheme Information'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Scheme Information'' and categoryid = 3), ''Site Plan'', 1, 1000,0,1,0)

	PRINT ''Site Plan''

END
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Site Plan'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Scheme Information'' and categoryid = 3))
	BEGIN	

	INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
	VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Scheme Information'' and categoryid = 3), ''Site Plan'', 1, 1000,0,1,0)

	PRINT ''Site Plan''

END
else BEGIN
		UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
		where title = ''Site Testing Instructions''
		print(''Site Testing Instructions'')
end
-------------------------- Compliance Asbestos -------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Airtest/Reoccupation Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Asbestos '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Asbestos'' and categoryid = 2), ''Airtest/Reoccupation Certificate'', 1, 1000,0,1,1)

 PRINT ''Airtest/Reoccupation Certificate inserted''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty = 1
  where title = ''Airtest/Reoccupation Certificate''
  print(''Airtest/Reoccupation Certificate updated'')
end
--------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Management Survey'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Asbestos'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Asbestos'' and categoryid = 2), ''Management Survey'', 1, 1000,0,1,1)

 PRINT ''Management Survey ADDED''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Management Survey''
  print(''Management Survey Updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Notification Form'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Asbestos'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Asbestos'' and categoryid = 2), ''Notification Form'', 1, 1000,0,1,1)

 PRINT ''Notification Form added ''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Notification Form''
  print(''Notification Form updated '')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Refurbishment Survey'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Asbestos '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Asbestos '' and categoryid = 2), ''Refurbishment Survey'', 1, 1000,0,1,1)

 PRINT ''Refurbishment Survey added''
END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Refurbishment Survey''
  print(''Refurbishment Survey updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Routine Preventative Maintenance Visit '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''CCTV'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''CCTV'' and categoryid = 2), ''Routine Preventative Maintenance Visit '', 1, 1000,0,0,1)

 PRINT ''Routine Preventative Maintenance Visit added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Routine Preventative Maintenance Visit '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''CCTV'' and categoryid = 2)
  print(''Routine Preventative Maintenance Visit updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Routine Preventative Maintenance Visit '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''CCTV'' and categoryid = 3))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''CCTV'' and categoryid = 3), ''Routine Preventative Maintenance Visit '', 1, 1000,0,1,0)

 PRINT ''Routine Preventative Maintenance Visit added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Routine Preventative Maintenance Visit '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''CCTV'' and categoryid = 3)
  print(''Routine Preventative Maintenance Visit updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Damp Proof Course Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Damp Proof Course Certificate'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Damp Proof Course Certificate'' and categoryid = 2), ''Damp Proof Course Certificate'', 1, 1000,0,1,1)

 PRINT ''Damp Proof Course Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Damp Proof Course Certificate''
  print(''Damp Proof Course Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Dry Lining Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Dry Lining Certificate'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Dry Lining Certificate'' and categoryid = 2), ''Dry Lining Certificate'', 1, 1000,0,0,1)

 PRINT ''Dry Lining Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Dry Lining Certificate''
  print(''Dry Lining Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''5 Year Peridotic Electrical Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''5 Year Peridotic Electrical Certificate'', 1, 1000,0,1,1)

 PRINT ''5 Year Peridotic Electrical Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''5 Year Peridotic Electrical Certificate''
  print(''5 Year Peridotic Electrical Certificate updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Domestic CO Alarm Certificate '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Domestic CO Alarm Certificate '', 1, 1000,0,0,1)

 PRINT ''Domestic CO Alarm Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Domestic CO Alarm Certificate ''
  print(''Domestic CO Alarm Certificate updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Domestic Smoke Alarm Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Domestic Smoke Alarm Certificate'', 1, 1000,0,0,1)

 PRINT ''Domestic Smoke Alarm Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Domestic Smoke Alarm Certificate''
  print(''Domestic Smoke Alarm Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Electrical Installation Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Electrical Installation Certificate'', 1, 1000,0,1,1)

 PRINT ''Electrical Installation Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Electrical Installation Certificate''
  print(''Electrical Installation Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Removal Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Asbestos'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Asbestos'' and categoryid = 2), ''Removal Certificate'', 1, 1000,0,1,1)

 PRINT ''Removal Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Removal Certificate''
  print(''Removal Certificate'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Electrical Installation Condition Report'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Electrical Installation Condition Report'', 1, 1000,0,1,1)

 PRINT ''Electrical Installation Condition Report added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Electrical Installation Condition Report''
  print(''Electrical Installation Condition Report updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Minor Works Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Minor Works Certificate'', 1, 1000,0,1,1)

 PRINT ''Minor Works Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Minor Works Certificate''
  print(''Minor Works Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''New Consumer Unit - NIC Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''New Consumer Unit - NIC Certificate'', 1, 1000,0,1,1)

 PRINT ''New Consumer Unit - NIC Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1, Active = 1
  where title =''New Consumer Unit - NIC Certificate''
  print(''New Consumer Unit - NIC Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Part P Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Part P Certificate'', 1, 1000,0,1,1)

 PRINT ''Part P Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Part P Certificate''
  print(''Part P Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''PAT Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''PAT Certificate'', 1, 1000,0,1,1)

 PRINT ''PAT Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''PAT Certificate''
  print(''PAT Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Visual Electrical Inspection Report'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''Visual Electrical Inspection Report'', 1, 1000,0,1,1)

 PRINT ''Visual Electrical Inspection Report added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Visual Electrical Inspection Report''
  print(''Visual Electrical Inspection Report updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Installation Report '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Installation Report '', 1, 1000,0,1,1)

 PRINT ''Installation Report added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Installation Report ''
  print(''Installation Report updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''EPC'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''EPC'' and categoryid = 2), ''Certificate'', 1, 1000,0,0,1)

 PRINT ''Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Certificate''
  print(''Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''AirSource Heat Pump Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''AirSource Heat Pump Installation'', 1, 1000,0,1,1)

 PRINT ''AirSource Heat Pump Installation added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''AirSource Heat Pump Installation''
  print(''AirSource Heat Pump Installation updated'')
end
-------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Gas Service'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Gas Service'', 1, 1000,0,1,1)

 PRINT ''Annual Gas Service added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Gas Service''
  print(''Annual Gas Service updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Domestic Air Source Heat Pump'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Service of Domestic Air Source Heat Pump'', 1, 1000,0,1,1)

 PRINT ''Annual Service of Domestic Air Source Heat Pump added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Service of Domestic Air Source Heat Pump''
  print(''Annual Service of Domestic Air Source Heat Pump updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Domestic Unvented Cylinder (CP8)'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Service of Domestic Unvented Cylinder (CP8)'', 1, 1000,0,1,1)

 PRINT ''Annual Service of Domestic Unvented Cylinder (CP8) added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Service of Domestic Unvented Cylinder (CP8)''
  print(''Annual Service of Domestic Unvented Cylinder (CP8) updated'')
end

-------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Domestic Unvented Electric Only Cylinder'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Service of Domestic Unvented Electric Only Cylinder'', 1, 1000,0,1,1)

 PRINT ''Annual Service of Domestic Unvented Electric Only Cylinder added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Service of Domestic Unvented Electric Only Cylinder''
  print(''Annual Service of Domestic Unvented Electric Only Cylinder updated'')
end
-------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service Of Ground Source Heat Pump (C13)'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Service Of Ground Source Heat Pump (C13)'', 1, 1000,0,1,1)

 PRINT ''Annual Service Of Ground Source Heat Pump (C13)''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Service Of Ground Source Heat Pump (C13)''
  print(''Annual Service Of Ground Source Heat Pump (C13)'')
end
 -------------------------------------------------------------------------------------------------------------

 IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annueal Service of Solar Thermal System (CP11)'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annueal Service of Solar Thermal System (CP11)'', 1, 1000,0,1,1)

 PRINT ''Annueal Service of Solar Thermal System (CP11) added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annueal Service of Solar Thermal System (CP11)''
  print(''Annueal Service of Solar Thermal System (CP11) updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Thermostatic Mixer Valves'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Service of Thermostatic Mixer Valves'', 1, 1000,0,1,1)

 PRINT ''Annual Service of Thermostatic Mixer Valves''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Service of Thermostatic Mixer Valves''
  print(''Annual Service of Thermostatic Mixer Valves'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Servicing of Electric Cylinder'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Servicing of Electric Cylinder'', 1, 1000,0,1,1)

 PRINT ''Annual Servicing of Electric Cylinder''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Servicing of Electric Cylinder''
  print(''Annual Servicing of Electric Cylinder'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Servicing of Heat Recovering Sytem'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Servicing of Heat Recovering Sytem'', 1, 1000,0,1,1)

 PRINT ''Annual Servicing of Heat Recovering Sytem''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Servicing of Heat Recovering Sytem''
  print(''Annual Servicing of Heat Recovering Sytem'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Servicing of NIBE System'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Servicing of NIBE System'', 1, 1000,0,1,1)

 PRINT ''Annual Servicing of NIBE System added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Servicing of NIBE System''
  print(''Annual Servicing of NIBE System updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Water Supply Sample Testing'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Annual Water Supply Sample Testing'', 1, 1000,0,1,1)

 PRINT ''Annual Water Supply Sample Testing ADDED''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Water Supply Sample Testing''
  print(''Annual Water Supply Sample Testing UPDATED'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Cylinder Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Cylinder Installation'', 1, 1000,0,1,1)

 PRINT ''Cylinder Installation ADDED''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Cylinder Installation''
  print(''Cylinder Installation UPDATED'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Gas Boiler Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Gas Boiler Installation'', 1, 1000,0,1,1)

 PRINT ''Gas Boiler Installation ADDED''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Gas Boiler Installation''
  print(''Gas Boiler Installation UPDATED'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Gas Safe Certificate for New Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Gas Safe Certificate for New Installation'', 1, 1000,0,1,1)

 PRINT ''Gas Safe Certificate for New Installation ADDED''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Gas Safe Certificate for New Installation''
  print(''Gas Safe Certificate for New Installation UPDATED'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Gas Safety Inspection Report'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Gas Safety Inspection Report'', 1, 1000,0,1,1)

 PRINT ''Gas Safety Inspection Report''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Gas Safety Inspection Report''
  print(''Gas Safety Inspection Report'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''New MVHR Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''New MVHR Installation'', 1, 1000,0,1,1)

 PRINT ''New MVHR Installation''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''New MVHR Installation''
  print(''New MVHR Installation'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Oil Boiler - OFTEC CD10/CD11 Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating '' and categoryid = 2), ''Oil Boiler - OFTEC CD10/CD11 Certificate'', 1, 1000,0,1,1)

 PRINT ''Oil Boiler - OFTEC CD10/CD11 Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Oil Boiler - OFTEC CD10/CD11 Certificate''
  print(''Oil Boiler - OFTEC CD10/CD11 Certificate'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Cavity Insulation Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Insulation '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Insulation '' and categoryid = 2), ''Cavity Insulation Certificate'', 1, 1000,0,1,1)

 PRINT ''Cavity Insulation Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Cavity Insulation Certificate''
  print(''Cavity Insulation Certificate'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Insurers Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts '' and categoryid = 2), ''Annual Insurers Inspection'', 1, 1000,0,1,1)

 PRINT ''Annual Insurers Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Insurers Inspection''
  print(''Annual Insurers Inspection'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Insurers Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts '' and categoryid = 2), ''Annual Insurers Inspection'', 1, 1000,0,1,1)

 PRINT ''Annual Insurers Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Insurers Inspection''
  print(''Annual Insurers Inspection'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Installation Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts '' and categoryid = 2), ''Installation Certificate'', 1, 1000,0,1,1)

 PRINT ''Installation Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Installation Certificate''
  print(''Installation Certificate'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Other Documents (quotes/ reports/ notifications/ reactive repairs)'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Warden Call'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Warden Call'' and categoryid = 2), ''Other Documents (quotes/ reports/ notifications/ reactive repairs)'', 1, 1000,0,1,1)

 PRINT ''Other Documents (quotes/ reports/ notifications/ reactive repairs) added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Other Documents (quotes/ reports/ notifications/ reactive repairs)''
  print(''Other Documents (quotes/ reports/ notifications/ reactive repairs) updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Fensa Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Windows and Doors '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Windows and Doors '' and categoryid = 2), ''Fensa Certificate'', 1, 1000,0,1,1)

 PRINT ''Fensa Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Fensa Certificate''
  print(''Fensa Certificate'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Servicing Certificate '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Air conditioning  '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Air conditioning  '' and categoryid = 2), ''Annual Servicing Certificate '', 1, 1000,0,1,0)

 PRINT ''Annual Servicing Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Servicing Certificate ''
  print(''Annual Servicing Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Inspection Reports '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Assisted Baths and Overhead Hoists'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Assisted Baths and Overhead Hoists'' and categoryid = 2), ''Inspection Reports '', 1, 1000,0,1,0)

 PRINT ''Inspection Reports added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Inspection Reports ''
  print(''Inspection Reports updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Risk Assesmnets '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''CCTV'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''CCTV'' and categoryid = 2), ''Annual Risk Assesmnets '', 1, 1000,0,1,0)

 PRINT ''Annual Risk Assemnets added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Risk Assemnets ''
  print(''Annual Risk Assemnets updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Commercial EPC '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Commercial EPC '' and categoryid = 2), ''Certificate'', 1, 1000,0,1,0)

 PRINT ''Certificate Certificate for commercial added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Commercial EPC '' and categoryid = 2)
  print(''Certificate for commercial updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Communal Kitchen Equipment Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Communal Kitchen Equipment Inspection'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Communal Kitchen Equipment Inspection'' and categoryid = 2), ''Communal Kitchen Equipment Inspection'', 1, 1000,0,1,0)

 PRINT ''Communal Kitchen Equipment Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Communal Kitchen Equipment Inspection''
  print(''Communal Kitchen Equipment Inspection updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Legionella Sampling Certificate '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Communal Water Supply'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Communal Water Supply'' and categoryid = 2), ''Legionella Sampling Certificate '', 1, 1000,0,1,0)

 PRINT ''Legionella Sampling Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Legionella Sampling Certificate ''
  print(''Legionella Sampling Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Risk Assesment'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Communal Water Supply'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Communal Water Supply'' and categoryid = 2), ''Risk Assesment'', 1, 1000,0,1,0)

 PRINT ''Risk Assesment added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Risk Assesment''
  print(''Risk Assesment updared'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Damp Proof Course Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Damp Proof Course Certificate'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Damp Proof Course Certificate'' and categoryid = 2), ''Damp Proof Course Certificate'', 1, 1000,0,1,1)

 PRINT ''Damp Proof Course Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Damp Proof Course Certificate''
  print(''Damp Proof Course Certificate'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Reactive Repair'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Door Entry Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Door Entry Systems'' and categoryid = 2), ''Reactive Repair'', 1, 1000,0,1,0)

 PRINT ''Reactive Repair added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Reactive Repair''
  print(''Reactive Repair updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Other Documents (Quotes/Reports/Notifications)'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Door Entry Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Door Entry Systems'' and categoryid = 2), ''Other Documents (Quotes/Reports/Notifications)'', 1, 1000,0,1,0)

 PRINT ''Other Documents (Quotes/Reports/Notifications)''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Other Documents (Quotes/Reports/Notifications)'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Door Entry Systems'' and categoryid = 2)
  print(''Other Documents (Quotes/Reports/Notifications)'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''AOV Inspection '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems  '' and categoryid = 2), ''AOV Inspection '', 1, 1000,0,1,0)

 PRINT ''AOV Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''AOV Inspection ''
  print(''AOV Inspection updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Bi-annual Fire Alarm Servicing Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems  '' and categoryid = 2), ''Bi-annual Fire Alarm Servicing Certificate'', 1, 1000,0,1,0)

 PRINT ''Bi-annual Fire Alarm Servicing Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Bi-annual Fire Alarm Servicing Certificate''
  print(''Bi-annual Fire Alarm Servicing Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Communal Smoke Detector Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Communal Smoke Detector Inspection'', 1, 1000,0,1,0)

 PRINT ''Communal Smoke Detector Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Communal Smoke Detector Inspection''
  print(''Communal Smoke Detector Inspection updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Dry Riser Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Dry Riser Inspection'', 1, 1000,0,1,0)

 PRINT ''Dry Riser Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Dry Riser Inspection''
  print(''Dry Riser Inspection updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Emergency Lighting Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems  '' and categoryid = 2), ''Emergency Lighting Inspection'', 1, 1000,0,1,0)

 PRINT ''Emergency Lighting Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Emergency Lighting Inspection''
  print(''Emergency Lighting Inspection updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Fire Alarm Servicing Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Fire Alarm Servicing Certificate'', 1, 1000,0,1,0)

 PRINT ''Fire Alarm Servicing Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Fire Alarm Servicing Certificate''
  print(''Fire Alarm Servicing Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Fire Appliance Inspections/Certificates'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Fire Appliance Inspections/Certificates'', 1, 1000,0,1,0)

 PRINT ''Fire Appliance Inspections/Certificates added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Fire Appliance Inspections/Certificates''
  print(''Fire Appliance Inspections/Certificates updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Fire Shutter Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Fire Shutter Inspection'', 1, 1000,0,1,0)

 PRINT ''Fire Shutter Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Fire Shutter Inspection''
  print(''Fire Shutter Inspection updated '')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Lightening Conductors Annual Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Alarm Systems'' and categoryid = 2), ''Lightening Conductors Annual Inspection'', 1, 1000,0,1,0)

 PRINT ''Lightening Conductors Annual Inspection added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Lightening Conductors Annual Inspection''
  print(''Lightening Conductors Annual Inspection updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Fire Risk Assesment Review'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Management'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Management'' and categoryid = 2), ''Annual Fire Risk Assesment Review'', 1, 1000,0,1,0)

 PRINT ''Annual Fire Risk Assesment Review aded''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Fire Risk Assesment Review''
  print(''Annual Fire Risk Assesment Review updated'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Fire Risk Assessment'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Fire Management'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Fire Management'' and categoryid = 2), ''Fire Risk Assessment'', 1, 1000,0,1,0)

 PRINT ''Fire Risk Assessment added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Fire Risk Assessment''
  print(''Fire Risk Assessment updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Anchor Points for Working at Heights Annual Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Health & Safety '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Health & Safety '' and categoryid = 2), ''Anchor Points for Working at Heights Annual Inspection'', 1, 1000,0,1,0)

 PRINT ''Anchor Points for Working at Heights Annual Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Anchor Points for Working at Heights Annual Inspection''
  print(''Anchor Points for Working at Heights Annual Inspection'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Man Safe Annual Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Health & Safety '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Health & Safety '' and categoryid = 2), ''Man Safe Annual Inspection'', 1, 1000,0,1,0)

 PRINT ''Man Safe Annual Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Man Safe Annual Inspection''
  print(''Man Safe Annual Inspection'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Monthly Health and Safety Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Health & Safety '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Health & Safety '' and categoryid = 2), ''Monthly Health and Safety Inspection'', 1, 1000,0,1,0)

 PRINT ''Monthly Health and Safety Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Monthly Health and Safety Inspection''
  print(''Monthly Health and Safety Inspection'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Play Area Inspection '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Health & Safety '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Health & Safety '' and categoryid = 2), ''Play Area Inspection '', 1, 1000,0,1,0)

 PRINT ''Play Area Inspection ''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Play Area Inspection ''
  print(''Play Area Inspection '')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Slips & Trips Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Health & Safety'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Health & Safety '' and categoryid = 2), ''Slips & Trips Inspection'', 1, 1000,0,1,0)

 PRINT ''Slips & Trips Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Slips & Trips Inspection''
  print(''Slips & Trips Inspection'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Tree Inspection '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Health & Safety '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Health & Safety '' and categoryid = 2), ''Tree Inspection '', 1, 1000,0,1,0)

 PRINT ''Tree Inspection ''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Tree Inspection ''
  print(''Tree Inspection'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Gas Service of Communal Heating System'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryid = 2), ''Annual Gas Service of Communal Heating System'', 1, 1000,0,1,0)

 PRINT ''Annual Gas Service of Communal Heating System''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Gas Service of Communal Heating System''
  print(''Annual Gas Service of Communal Heating System'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Biomass Heating System'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryid = 2), ''Annual Service of Biomass Heating System'', 1, 1000,0,1,0)

 PRINT ''Annual Service of Biomass Heating System''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Service of Biomass Heating System''
  print(''Annual Service of Biomass Heating System'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Thermostatic Mixer Valves'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryid = 2), ''Annual Service of Thermostatic Mixer Valves'', 1, 1000,0,1,0)

 PRINT ''Annual Service of Thermostatic Mixer Valves''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Service of Thermostatic Mixer Valves''
  print(''Annual Service of Thermostatic Mixer Valves'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Communal Heating Plant and Pump Annual Inspection'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryid = 2), ''Communal Heating Plant and Pump Annual Inspection'', 1, 1000,0,1,0)

 PRINT ''Communal Heating Plant and Pump Annual Inspection''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Communal Heating Plant and Pump Annual Inspection''
  print(''Communal Heating Plant and Pump Annual Inspection'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Gas Catering Appliance Certificate '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryid = 2), ''Gas Catering Appliance Certificate '', 1, 1000,0,1,0)

 PRINT ''Gas Catering Appliance Certificate ''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Gas Catering Appliance Certificate ''
  print(''Gas Catering Appliance Certificate '')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Passenger Lift Inspection Reports'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts'' and categoryid = 2), ''Annual Passenger Lift Inspection Reports'', 1, 1000,0,1,0)

 PRINT ''Annual Passenger Lift Inspection Reports''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Passenger Lift Inspection Reports''
  print(''Annual Passenger Lift Inspection Reports'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Servicing Certificate '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Sewage Treatment Plant / Pumps'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Sewage Treatment Plant / Pumps'' and categoryid = 2), ''Annual Servicing Certificate '', 1, 1000,0,1,0)

 PRINT ''Annual Servicing Certificate ''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Annual Servicing Certificate ''
  print(''Annual Servicing Certificate '')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Confidential Waste Certificate '' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Waste Management'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Waste Management '' and categoryid = 2), ''Confidential Waste Certificate '', 1, 1000,0,1,0)

 PRINT ''Confidential Waste Certificate ''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Confidential Waste Certificate ''
  print(''Confidential Waste Certificate '')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Waste Disposal Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Waste Management'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Waste Management '' and categoryid = 2), ''Waste Disposal Certificate'', 1, 1000,0,1,0)

 PRINT ''Waste Disposal Certificate''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Waste Disposal Certificate''
  print(''Waste Disposal Certificate'')
end
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Annual Service of Thermostatic Mixer Valves'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Heating'' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Heating'' and categoryid = 2), ''Annual Service of Thermostatic Mixer Valves'', 1, 1000,0,1,0)

 PRINT ''Annual Service of Thermostatic Mixer Valves added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =1
  where title =''Annual Service of Thermostatic Mixer Valves''
  print(''Annual Service of Thermostatic Mixer Valves updated'')
end

-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Owned Stair Lifts - Warranty Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Lifts'' and categoryid = 2), ''Owned Stair Lifts - Warranty Certificate'', 1, 1000,0,1,0)

 PRINT ''Owned Stair Lifts - Warranty Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 1, isProperty =0
  where title =''Owned Stair Lifts - Warranty Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Lifts '' and categoryid = 2)
  print(''Owned Stair Lifts - Warranty Certificate updated'')
end

---------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''PV Installation Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical '' and categoryid = 2))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Electrical'' and categoryid = 2), ''PV Installation Certificate'', 1, 1000,0,0,1)

 PRINT ''PV Installation Certificate added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''PV Installation Certificate'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Electrical'' and categoryid = 2)
  print(''PV Installation Certificate updated'')
end
-------------------------------------------------------------------------------------------------------------
--Annueal Service of Solar Thermal System (CP11)
UPDATE P_Documents_SubType set title = ''Annual Service of Solar Thermal System (CP11)'' where title = ''Annueal Service of Solar Thermal System (CP11)''
----------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Gas Cooker Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Kitchens '' and categoryid = 3))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Kitchens'' and categoryid = 3), ''Gas Cooker Installation'', 1, 1000,0,0,1)

 PRINT ''Gas Cooker Installation added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Gas Cooker Installation'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Kitchens '' and categoryid = 3)
  print(''Gas Cooker Installation updated'')
end
---------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''Property Condition'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Housing '' and categoryid = 3))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Housing'' and categoryid = 3), ''Property Condition'', 1, 1000,0,0,1)

 PRINT ''Property Condition added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''Property Condition'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Housing '' and categoryid = 3)
  print(''Property Condition updated'')
end

---------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (Select 1 from P_Documents_SubType where Title=''SLA'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Housing '' and categoryid = 3))
 BEGIN 

 INSERT INTO P_Documents_SubType (DocumentTypeId, Title, Active, [Order], isDevelopment, IsScheme, IsProperty) 
 VALUES ((SELECT top 1 DocumentTypeId FROM P_Documents_Type WHERE Title = ''Housing'' and categoryid = 3), ''SLA'', 1, 1000,0,0,1)

 PRINT ''SLA added''

END
else BEGIN
  UPDATE P_Documents_SubType set isDevelopment = 0, isScheme = 0, isProperty =1
  where title =''SLA'' and DocumentTypeId = (select documenttypeid from P_Documents_Type where title = ''Housing '' and categoryid = 3)
  print(''updated SLA'')
end
---------------------------------------------------------------------------------------------------------------

--Insert NHBC Document Sub Types
Declare @countOfSubTypes int =0, @NHBCDocumentTypeId int = -1

Select TOP(1) @NHBCDocumentTypeId = DocumentTypeId from dbo.P_Documents_Type Where Title like ''%NHBC%''; 

IF (@NHBCDocumentTypeId = -1)
	BEGIN
		Insert Into P_Documents_Type  (Title,Active,[Order])
		Values (''NHBC'',1,1000) 	
		SELECT @NHBCDocumentTypeId  = SCOPE_IDENTITY()
	END
ELSE	--IF TYPEID ALREADY DEFINED, THEN LOOK FOR SUBTYPES
	BEGIN
		Select @countOfSubTypes=COUNT(*) from P_Documents_SubType where DocumentTypeId = @NHBCDocumentTypeId
		IF(@countOfSubTypes = 0)
			BEGIN
				INSERT INTO P_Documents_SubType(DocumentTypeId,Title,Active,[order], IsDevelopment, IsScheme,IsProperty)
				Values(@NHBCDocumentTypeId,''NHBC Certificate'',1,1000,0,1,0)

				INSERT INTO P_Documents_SubType(DocumentTypeId,Title,Active,[order], IsDevelopment, IsScheme,IsProperty)
				Values(@NHBCDocumentTypeId,''NHBC '',1,1000,0,1,0)
				print (''NHBC updated'')
			END
			
	END  
	'
--print @mainQuery
Exec (@mainQuery)
END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH



