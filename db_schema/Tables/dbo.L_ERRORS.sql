CREATE TABLE [dbo].[L_ERRORS]
(
[ErrorID] [int] NOT NULL IDENTITY(1, 1),
[ASPCode] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASPDescription] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASPCategory] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASPColumn] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filename] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cookies] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Form] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Querystring] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Session] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorDate] [datetime] NULL CONSTRAINT [DF_L_ERRORS_ErrorDate] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[L_ERRORS] ADD CONSTRAINT [PK_L_ERRORS] PRIMARY KEY CLUSTERED  ([ErrorID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
