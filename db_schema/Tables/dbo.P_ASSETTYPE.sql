USE RSLBHALive
GO
BEGIN TRANSACTION
BEGIN TRY
--Check if table exists or not
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_ASSETTYPE')
BEGIN
	


CREATE TABLE [dbo].[P_ASSETTYPE]
(
[ASSETTYPEID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

END


IF COL_LENGTH('P_ASSETTYPE', 'IsActive') IS NULL
BEGIN
	ALTER TABLE P_ASSETTYPE 
	ADD IsActive int NULL
	Print('IsActive added successfully  ') 
	
END

IF NOT EXISTS (Select 1 from P_ASSETTYPE where IsActive=1)
	BEGIN 
		Update P_ASSETTYPE set IsActive=1
	END
IF EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Communal')
	BEGIN  
		UPDATE P_ASSETTYPE set isactive=0 where DESCRIPTION='Communal'
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Low cost home ownership')
	BEGIN  
		INSERT INTO P_ASSETTYPE (DESCRIPTION,IsActive)Values ('Low cost home ownership',1)
		Print('Low cost home ownership added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Leased')
	BEGIN  
		INSERT INTO P_ASSETTYPE (DESCRIPTION,IsActive)Values ('Leased',1)
		Print('Leased added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Lease Holder')
	BEGIN  
		INSERT INTO P_ASSETTYPE (DESCRIPTION,IsActive)Values ('Lease Holder',1)
		Print('Lease Holder added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Low Cost Rental')
	BEGIN  
		INSERT INTO P_ASSETTYPE (DESCRIPTION,IsActive)Values ('Low Cost Rental',1)
		Print('Low Cost Rental added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Older Person Housing')
	BEGIN  
		INSERT INTO P_ASSETTYPE (DESCRIPTION,IsActive)Values ('Older Person Housing',1)
		Print('Older Person Housing added successfully  ') 
	END



IF  EXISTS (Select 1 from P_ASSETTYPE where DESCRIPTION='Leased Housing')
	BEGIN  
		Update P_ASSETTYPE set IsActive=0 where DESCRIPTION IN ('Leased Housing','Key Worker','Intermediate Rented','HAMA','Asylum Seeker','Shared Ownership','Sheltered','Leasehold Managed','Not applicable',
		'Housing with Care','Staff','Student','Disabled','Communal')		 
	END


IF EXISTS(Select PROPERTYID from P__PROPERTY where Assettype in (Select [ASSETTYPEID] from P_ASSETTYPE where DESCRIPTION like'%Lease%'))
  BEGIN
    Update P__PROPERTY 
	SET ASSETTYPE = (Select [ASSETTYPEID] from P_ASSETTYPE where DESCRIPTION='Leased')
	Where PROPERTYID in (Select PROPERTYID from P__PROPERTY where Assettype in (Select [ASSETTYPEID] from P_ASSETTYPE where DESCRIPTION like'%Lease%'))

  END

IF EXISTS(Select * from P__PROPERTY where Assettype in (Select [ASSETTYPEID] from P_ASSETTYPE where DESCRIPTION IN ('Key Worker','Intermediate Rented','HAMA','Asylum Seeker','Shared Ownership','Sheltered','Not applicable',
		'Housing with Care','Staff','Student','Disabled','Communal')))
  BEGIN
    Update P__PROPERTY 
	SET ASSETTYPE = (Select [ASSETTYPEID] from P_ASSETTYPE where DESCRIPTION='Low cost home ownership')
	Where PROPERTYID in (Select PROPERTYID from P__PROPERTY where Assettype in (Select [ASSETTYPEID] from P_ASSETTYPE where DESCRIPTION IN ('Key Worker','Intermediate Rented','HAMA','Asylum Seeker','Shared Ownership','Sheltered','Not applicable',
		'Housing with Care','Staff','Student','Disabled','Communal')))

  END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
