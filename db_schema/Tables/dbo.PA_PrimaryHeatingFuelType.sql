/*
   Wednesday, March 16, 201611:56:50 AM
   User: TkXel
   Server: 10.0.2.19
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'PA_PrimaryHeatingFuelType')
		BEGIN
		CREATE TABLE dbo.PA_PrimaryHeatingFuelType
			(
			PrimaryFuelTypeId int NOT NULL IDENTITY (1, 1),
			FuelTypeId int NOT NULL,
			ParameterValueId int NOT NULL,			
			IsActive bit NULL
			)  ON [PRIMARY]

		ALTER TABLE dbo.PA_PrimaryHeatingFuelType ADD CONSTRAINT
			PK_PA_PrimaryHeatingFuelType PRIMARY KEY CLUSTERED 
			(
			PrimaryFuelTypeId 
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


		ALTER TABLE dbo.PA_PrimaryHeatingFuelType SET (LOCK_ESCALATION = TABLE)

		END --if
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState );
	Print (@ErrorMessage)
END CATCH 
  
Go
--=============================================================
-- Insertion Script
--=============================================================
BEGIN TRANSACTION
BEGIN TRY

	IF  EXISTS (SELECT 1 FROM PA_PARAMETER WHERE ParameterName = N'Heating Fuel')
		BEGIN
		DECLARE @ParameterId int,@ParameterValueId int,@FuelTypeId int
		--get ParameterId for Heating Fuel
		SELECT @ParameterId=ParameterId FROM PA_PARAMETER WHERE ParameterName = N'Heating Fuel'
--=============================================================
-- Script for Air Source Mapping
--=============================================================
		--Get ParameterValueID for Air Source from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Air Source' AND ParameterID=@ParameterId)
			BEGIN
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Air Source' AND ParameterID=@ParameterId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Air Source',0,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Air Source from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Air Source')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Air Source'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Air Source')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END
--=============================================================
-- Script for Biomass Mapping
--=============================================================
		--Get ParameterValueID for Biomass from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Biomass' AND ParameterID=@ParameterId)
			BEGIN
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Biomass' AND ParameterID=@ParameterId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Biomass',1,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Biomass from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Biomass')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Biomass'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Biomass')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END
--=============================================================
-- Script for E7
--=============================================================
		--Get ParameterValueID for E7 from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'E7' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'E7' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'E7',2,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for E7 from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'E7')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'E7'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('E7')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END
--=============================================================
-- Script for Electric
--=============================================================
		--Get ParameterValueID for Electric from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Electric' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Electric' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Electric',3,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Electric from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Electric')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Electric'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Electric')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END			
--=============================================================
-- Script for Ground Source
--=============================================================
		--Get ParameterValueID for Ground Source from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Ground Source' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Ground Source' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Ground Source',4,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Ground Source from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Ground Source')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Ground Source'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Ground Source')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END	
--=============================================================
-- Script for LPG
--=============================================================
		--Get ParameterValueID for LPG from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'LPG' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'LPG' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'LPG',5,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for LPG from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'LPG')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'LPG'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('LPG')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END				
--=============================================================
-- Script for Mains Electric
--=============================================================
		--Get ParameterValueID for Mains Electric from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Mains Electric' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Mains Electric' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Mains Electric',6,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Mains Electric from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Mains Electric')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Mains Electric'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Mains Electric')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END			
--=============================================================
-- Script for Mains Gas
--=============================================================
		--Get ParameterValueID for Mains Gas from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Mains Gas' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Mains Gas' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Mains Gas',7,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Mains Gas from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Mains Gas')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Mains Gas'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Mains Gas')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END		
--=============================================================
-- Script for Not Applicable
--=============================================================
		--Get ParameterValueID for Not Applicable from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Not Applicable' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Not Applicable' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Not Applicable',81,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Not Applicable from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Not Applicable')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Not Applicable'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Not Applicable')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END		
--=============================================================
-- Script for Oil
--=============================================================
		--Get ParameterValueID for Oil from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Oil' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Oil' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Oil',9,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Oil from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Oil')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Oil'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Oil')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END							
--=============================================================
-- Script for Solar
--=============================================================
		--Get ParameterValueID for Solar from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Solar' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Solar' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Solar',10,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Solar from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Solar')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Solar'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Solar')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END		
--=============================================================
-- Script for Wind
--=============================================================
		--Get ParameterValueID for Wind from PA_PARAMETER_VALUE
		IF  EXISTS (SELECT 1 FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Wind' AND ParameterID=@ParameterId)
			BEGIN			
			SELECT @ParameterValueId=ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail  = N'Wind' AND ParameterID=@ParameterId
			UPDATE PA_PARAMETER_VALUE SET IsActive = 1 WHERE ValueID =  @ParameterValueId
			END
		ELSE
			BEGIN
			insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Wind',11,1)
			set @ParameterValueId = SCOPE_IDENTITY()
			END
		--Get FuelTypeId for Wind from P_FUELTYPE
		IF  EXISTS (SELECT 1 FROM P_FUELTYPE WHERE FUELTYPE  = N'Wind')
			BEGIN
			SELECT @FuelTypeId=FUELTYPEID FROM P_FUELTYPE WHERE FUELTYPE  = N'Wind'
			END
		ELSE
			BEGIN
			insert into P_FUELTYPE (FUELTYPE) values('Wind')
			set @FuelTypeId = SCOPE_IDENTITY()
			END
		--Inset into PA_PrimaryHeatingFuelType Mapping table
		IF NOT EXISTS (SELECT 1 FROM PA_PrimaryHeatingFuelType WHERE FuelTypeId  = @FuelTypeId AND ParameterValueId = @ParameterValueId)
			BEGIN
			INSERT INTO PA_PrimaryHeatingFuelType ( FuelTypeId, ParameterValueId, IsActive) VALUES( @FuelTypeId, @ParameterValueId,1)
			END										
		END --if
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
	DECLARE @ErrorInsertMessage NVARCHAR(4000);
	DECLARE @ErrorInsertSeverity INT;
	DECLARE @ErrorInsertState INT;

	SELECT @ErrorInsertMessage = ERROR_MESSAGE(),
	@ErrorInsertSeverity = ERROR_SEVERITY(),
	@ErrorInsertState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorInsertMessage, @ErrorInsertSeverity, @ErrorInsertState );
	Print (@ErrorInsertMessage)
END CATCH 


Go
BEGIN TRANSACTION
BEGIN TRY
	DECLARE @propertyid varchar(100),@fueltitle varchar(200)
	DECLARE @fueltype INT
	DECLARE @parametervalueid INT,@itemparamid int
	Select @itemparamid=ITP.ItemParamID from PA_ITEM_PARAMETER ITP
		INNER JOIN PA_PARAMETER P ON ITP.ParameterId = P.ParameterID
		INNER JOIN PA_ITEM I ON ITP.ItemId = I.ItemID
		Where  I.ItemName = 'Heating' And P.ParameterName='Heating Fuel'
		
	DECLARE fuelCursor CURSOR FOR SELECT    P__PROPERTY.PROPERTYID,P__PROPERTY.FUELTYPE,P_FuelType.FUELTYPE as FuelTitle,PA_PrimaryHeatingFuelType.ParameterValueId from P__PROPERTY
	INNER JOIN P_FuelType ON P__PROPERTY.FUELTYPE= FUELTYPEID
	INNER JOIN PA_PrimaryHeatingFuelType ON P_FuelType.FUELTYPEID = PA_PrimaryHeatingFuelType.FuelTypeID 
	--Where P__PROPERTY.FUELTYPE IS NULL
	OPEN fuelCursor

	FETCH NEXT FROM fuelCursor INTO @propertyid, @fueltype,@fueltitle,@parametervalueid

	WHILE @@FETCH_STATUS = 0 BEGIN

		IF  EXISTS (SELECT 1 FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @propertyid AND ITEMPARAMID = @itemparamid)
			BEGIN
			 Update PA_PROPERTY_ATTRIBUTES Set VALUEID = @parametervalueid, PARAMETERVALUE=@fueltitle WHERE PROPERTYID = @propertyid AND ITEMPARAMID = @itemparamid
			Print ('Update Query Executed For '+ @propertyid)
			END
		Else 
			BEGIN
			INSERT INTO PA_PROPERTY_ATTRIBUTES(PROPERTYID, ITEMPARAMID,PARAMETERVALUE,VALUEID,UPDATEDON, UPDATEDBY, IsCheckBoxSelected)
			VALUES( @propertyid,@itemparamid,@fueltitle,@parametervalueid,GETDATE(),760,0) 
			Print ('Insert Query Executed for '+@propertyid)
			END	
	 FETCH NEXT FROM fuelCursor INTO @propertyid, @fueltype,@fueltitle,@parametervalueid
	END

	CLOSE fuelCursor    
	DEALLOCATE fuelCursor
	IF @@TRANCOUNT > 0
			BEGIN     
				COMMIT TRANSACTION;   	
			END

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
	DECLARE @ErrorMappingMessage NVARCHAR(4000);	
	SELECT @ErrorMappingMessage = ERROR_MESSAGE();
	Print (@ErrorMappingMessage)
END CATCH 

 