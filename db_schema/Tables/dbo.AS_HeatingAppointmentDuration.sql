USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[AS_HeatingAppointmentDuration]    Script Date: 17-Jul-18 3:20:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT *
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'AS_HeatingAppointmentDuration') 
BEGIN
CREATE TABLE [dbo].[AS_HeatingAppointmentDuration](
	[HeatingAppointmentDurationID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentId] INT NOT NULL,
	[Duration] INT NOT NULL,
	[HeatingTypeId] INT NOT NULL,
	[TradeId] INT NOT NULL
 CONSTRAINT [PK_AS_HeatingAppointmentDuration] PRIMARY KEY CLUSTERED 
(
	[HeatingAppointmentDurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO


