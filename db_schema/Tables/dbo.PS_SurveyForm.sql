CREATE TABLE [dbo].[PS_SurveyForm]
(
[SurveyFormId] [int] NOT NULL IDENTITY(1, 1),
[SurveyFormName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyPortionName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SavedBy] [int] NULL,
[SavedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[PS_SurveyForm] ADD 
CONSTRAINT [PK_PS_SurveyForm] PRIMARY KEY CLUSTERED  ([SurveyFormId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
