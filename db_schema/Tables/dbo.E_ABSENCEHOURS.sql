CREATE TABLE [dbo].[E_ABSENCEHOURS]
(
[AbsenceHourID] [int] NOT NULL IDENTITY(1, 1),
[EmployeeID] [int] NOT NULL,
[AbsenceDate] [date] NOT NULL,
[AbsenceHoursSick] [float] NOT NULL,
[AbsenceHoursOthers] [float] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[E_ABSENCEHOURS] ADD 
CONSTRAINT [PK_E_AbsentHours] PRIMARY KEY CLUSTERED  ([AbsenceHourID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
