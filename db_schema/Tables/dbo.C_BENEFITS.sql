CREATE TABLE [dbo].[C_BENEFITS]
(
[BENEFITID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[C_BENEFITS] ADD CONSTRAINT [PK_C_BENEFITS] PRIMARY KEY NONCLUSTERED  ([BENEFITID]) WITH FILLFACTOR=30 ON [PRIMARY]
GO
