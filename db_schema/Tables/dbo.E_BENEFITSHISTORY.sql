USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_BENEFITSHISTORY]    Script Date: 8/4/2017 12:39:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_BENEFITSHISTORY')
BEGIN

CREATE TABLE [dbo].[E_BENEFITSHISTORY](
	[BENEFITHISTORYID] [int] IDENTITY(1,1) NOT NULL,
	[BENEFITID] [int] NOT NULL,
	[EMPLOYEEID] [int] NOT NULL,
	[SALARYPERCENT] [float] NULL,
	[CONTRIBUTION] [money] NULL,
	[EMPLOYEECONTRIBUTION] [money] NULL,
	[EMPLOYERCONTRIBUTION] [money] NULL,
	[AVC] [money] NULL,
	[CONTRACTEDOUT] [int] NULL,
	[LISTPRICE] [money] NULL,
	[EXCESSCONTRIBUTION] [money] NULL,
	[CARALLOWANCE] [money] NULL,
	[CARLOANSTARTDATE] [smalldatetime] NULL,
	[TERM] [nvarchar](20) NULL,
	[VALUE] [money] NULL,
	[PROFESSIONALFEES] [money] NULL,
	[TELEPHONEALLOWANCE] [money] NULL,
	[FIRSTAIDALLOWANCE] [money] NULL,
	[CALLOUTALLOWANCE] [money] NULL,
	[MEMNUMBER] [nvarchar](50) NULL,
	[SCHEME] [int] NULL,
	[SCHEMENUMBER] [nvarchar](50) NULL,
	[CARMAKE] [nvarchar](50) NULL,
	[MODEL] [nvarchar](50) NULL,
	[CONTHIRECHARGE] [money] NULL,
	[EMPCONTRIBUTION] [money] NULL,
	[ENGINESIZE] [nvarchar](10) NULL,
	[CARSTARTDATE] [smalldatetime] NULL,
	[CO2EMISSIONS] [float] NULL,
	[FUEL] [nvarchar](50) NULL,
	[COMPEMPCONTRIBUTION] [money] NULL,
	[ALLOWANCE] [money] NULL,
	[MONTHLYREPAY] [money] NULL,
	[ACCOMODATIONRENT] [money] NULL,
	[COUNCILTAX] [money] NULL,
	[HEATING] [money] NULL,
	[LINERENTAL] [money] NULL,
	[ANNUALPREMIUM] [money] NULL,
	[TAXABLEBENEFIT] [money] NULL,
	[MEDICALORGID] [int] NULL,
	[ADDITIONALMEMBERS] [nvarchar](50) NULL,
	[ADDITIONALMEMBERS2] [nvarchar](50) NULL,
	[ADDITIONALMEMBERS3] [nvarchar](50) NULL,
	[ADDITIONALMEMBERS4] [nvarchar](50) NULL,
	[ADDITIONALMEMBERS5] [nvarchar](50) NULL,
	[LOANINFORMATION] [int] NULL,
	[DRIVINGLICENCENO] [nvarchar](100) NULL,
	[MOTCERTNO] [nvarchar](100) NULL,
	[INSURANCENO] [nvarchar](50) NULL,
	[INSURANCERENEWALDATE] [smalldatetime] NULL,
	[GROUPSCHEMEREF] [nchar](50) NULL,
	[MEMBERSHIPNO] [nchar](50) NULL,
	[LASTACTIONTIME] [datetime] NULL,
	[LASTACTIONUSER] [int] NULL,
	[DrivingLicenceImage] [nvarchar](max) NULL,
	[ECUTopUp] [money] NULL,
	[MOTRenewalDate] [smalldatetime] NULL,
	[ToolAllowance] [money] NULL,
	[FirstAid] [money] NULL,
	[UnionSubscription] [money] NULL,
	[LifeAssurance] [money] NULL,
	[EnhancedHolidayEntitlement] [int] NULL,
	[CarParkingFacilities] [money] NULL,
	[EyeCareAssistance] [money] NULL,
	[EmployeeAssistanceProgramme] [nvarchar](500) NULL,
	[EnhancedSickPay] [money] NULL,
	[LearningAndDevelopment] [money] NULL,
	[ProfessionalSubscriptions] [money] NULL,
	[ChildcareVouchers] [money] NULL,
	[FluAndHepBJabs] [money] NULL,
 CONSTRAINT [PK_BENEFITS_History] PRIMARY KEY NONCLUSTERED 
(
	[BENEFITHISTORYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



END

IF COL_LENGTH('E_BENEFITSHISTORY', 'AdditionalDriver') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITSHISTORY 
			ADD AdditionalDriver NVARCHAR(500)
			Print('Added Additional Driver') 
		END


