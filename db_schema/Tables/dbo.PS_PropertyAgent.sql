CREATE TABLE [dbo].[PS_PropertyAgent]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LandlordName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNo] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_PropertyAgent] ADD 
CONSTRAINT [PK_PS_PropertyAgent] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
