CREATE TABLE [dbo].[H_ASSIGNHISTORY]
(
[AssignID] [int] NOT NULL IDENTITY(1, 1),
[AssignDate] [datetime] NULL CONSTRAINT [DF_H_ASSIGNHISTORY_AssignDate] DEFAULT (getdate()),
[AssignedFrom] [int] NULL,
[ActualStartDate] [datetime] NULL,
[Quoted] [money] NULL,
[Days] [int] NULL,
[RequestType] [int] NULL,
[AuthorisedBy] [int] NULL,
[AssignedTo] [int] NULL,
[FontColor] [int] NULL,
[AssignmentMadeBy] [int] NULL,
[Job_ID] [int] NULL
) ON [PRIMARY]
GO
