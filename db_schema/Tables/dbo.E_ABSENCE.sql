USE RSLBHALive


BEGIN TRANSACTION
  BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_ABSENCE')
BEGIN
		CREATE TABLE [dbo].[E_ABSENCE](
			[ABSENCEHISTORYID] [int] IDENTITY(1,1) NOT NULL,
			[JOURNALID] [int] NULL,
			[ITEMSTATUSID] [int] NULL,
			[ITEMACTIONID] [int] NULL,
			[LASTACTIONDATE] [smalldatetime] NULL,
			[LASTACTIONUSER] [int] NULL,
			[STARTDATE] [smalldatetime] NULL,
			[RETURNDATE] [smalldatetime] NULL,
			[DURATION] [float] NULL,
			[CERTNO] [nvarchar](20) NULL,
			[NOTES] [nvarchar](1000) NULL,
			[DRNAME] [nvarchar](300) NULL,
			[REASON] [nvarchar](1000) NULL,
			[HOLTYPE] [nvarchar](10) NULL,
			[DURATION_HRS] [float] NULL,
			[REASONID] [int] NULL,
			[DURATION_TYPE] [NVARCHAR](200) NULL,
		 CONSTRAINT [PK_ABSENCE] PRIMARY KEY NONCLUSTERED 
		(
			[ABSENCEHISTORYID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY]

	

		ALTER TABLE [dbo].[E_ABSENCE]  WITH NOCHECK ADD  CONSTRAINT [FK_ITEMABSENCE_ITEMACTION] FOREIGN KEY([ITEMACTIONID])
		REFERENCES [dbo].[E_ACTION] ([ITEMACTIONID])
	

		ALTER TABLE [dbo].[E_ABSENCE] NOCHECK CONSTRAINT [FK_ITEMABSENCE_ITEMACTION]
	

		ALTER TABLE [dbo].[E_ABSENCE]  WITH NOCHECK ADD  CONSTRAINT [FK_ITEMABSENCE_ITEMJOURNAL] FOREIGN KEY([JOURNALID])
		REFERENCES [dbo].[E_JOURNAL] ([JOURNALID])
	

		ALTER TABLE [dbo].[E_ABSENCE] NOCHECK CONSTRAINT [FK_ITEMABSENCE_ITEMJOURNAL]
	

END

	--------------------------------------------------------------------------------------
	 IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'X_E_ABSENCE_JOURNAL_DURATION')
	  BEGIN
		 Create NONCLUSTERED INDEX [X_E_ABSENCE_JOURNAL_DURATION]
			ON [dbo].[E_ABSENCE] (JOURNALID)
			INCLUDE([ABSENCEHISTORYID],[DURATION_HRS])
	  PRINT 'X_E_ABSENCE_JOURNAL_DURATION created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'X_E_ABSENCE_JOURNAL_DURATION Index Already Exist';
	  END
	--------------------------------------------------------------------------------------

IF NOT EXISTS(
SELECT *
FROM sys.columns 
WHERE Name      = N'AnticipatedReturnDate'
    AND Object_ID = Object_ID(N'E_ABSENCE'))
BEGIN
    ALTER TABLE [dbo].[E_ABSENCE]
	ADD [AnticipatedReturnDate] [smalldatetime] NULL
	
	PRINT 'Added Column AnticipatedReturnDate'
END
IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_ABSENCE]' ) AND name = 'DURATION_TYPE')
	BEGIN
		ALTER TABLE E_ABSENCE ADD DURATION_TYPE NVARCHAR(200) NULL
	PRINT 'DURATION_TYPE added successfully!'
	END
    
    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    
