CREATE TABLE [dbo].[C_JOURNALTOPLANNEDMAINTENANCE]
(
[JOURNALID] [int] NOT NULL,
[PROPERTYLISTID] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[C_JOURNALTOPLANNEDMAINTENANCE] ADD 
CONSTRAINT [PK__C_JOURNALTOPLANN__0080C572] PRIMARY KEY CLUSTERED  ([JOURNALID], [PROPERTYLISTID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[C_JOURNALTOPLANNEDMAINTENANCE] WITH NOCHECK ADD
CONSTRAINT [FK_C_JOURNALTOPLANNEDMAINTENANCE_C_JOURNAL] FOREIGN KEY ([JOURNALID]) REFERENCES [dbo].[C_JOURNAL] ([JOURNALID])
ALTER TABLE [dbo].[C_JOURNALTOPLANNEDMAINTENANCE] WITH NOCHECK ADD
CONSTRAINT [FK_C_JOURNALTOPLANNEDMAINTENANCE_PM_PROGRAMME_ASSIGNMENT] FOREIGN KEY ([PROPERTYLISTID]) REFERENCES [dbo].[PM_PROGRAMME_ASSIGNMENT] ([PROPERTYLISTID])
GO
