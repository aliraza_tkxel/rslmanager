USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PDR_WARRANTY]    Script Date: 16-Oct-17 10:53:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PDR_WARRANTY')
BEGIN

CREATE TABLE [dbo].[PDR_WARRANTY](
	[WARRANTYID] [int] IDENTITY(1,1) NOT NULL,
	[BLOCKID] [int] NULL,
	[WARRANTYTYPE] [int] NULL,
	[AREAITEM] [int] NULL,
	[CONTRACTOR] [int] NULL,
	[EXPIRYDATE] [smalldatetime] NULL,
	[NOTES] [nvarchar](1000) NULL,
	[SERIALNUMBER] [nvarchar](50) NULL,
	[MODEL] [nvarchar](50) NULL,
	[SCHEMEID] [int] NULL,
	[PROPERTYID] [nvarchar](1000) NULL,
 CONSTRAINT [PK_PDR_WARRANTY] PRIMARY KEY NONCLUSTERED 
(
	[WARRANTYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 30) ON [PRIMARY]
) ON [PRIMARY]

END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Category'
      AND Object_ID = Object_ID(N'PDR_WARRANTY'))
BEGIN
    ALTER TABLE [dbo].[PDR_WARRANTY]
	ADD [Category] int NULL
	
	PRINT 'Added Column Category'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Area'
      AND Object_ID = Object_ID(N'PDR_WARRANTY'))
BEGIN
    ALTER TABLE [dbo].[PDR_WARRANTY]
	ADD [Area] int NULL
	
	PRINT 'Added Column Area'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Item'
      AND Object_ID = Object_ID(N'PDR_WARRANTY'))
BEGIN
    ALTER TABLE [dbo].[PDR_WARRANTY]
	ADD [Item] int NULL
	
	PRINT 'Added Column Item'
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
