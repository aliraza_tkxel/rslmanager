---=========================  Flex Leave  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator is NULL or 
					jd.HolidayIndicator = 1 
				)
			GROUP BY j.EMPLOYEEID
		)
		and ntr.DESCRIPTION = 'Flex Leave'
)

--12129

---=========================  Annual Leave  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID -- and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator is NULL or 
					jd.HolidayIndicator = 1 
				)
			GROUP BY j.EMPLOYEEID
		)
		and ntr.DESCRIPTION = 'Annual Leave' 
)

--60680

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in (43614, 43644, 46014)

-- 3

---=========================  Sickness  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID -- and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator is NULL or 
					jd.HolidayIndicator = 1 
				)
			GROUP BY j.EMPLOYEEID
		)
		and ntr.DESCRIPTION = 'Sickness' 
)
--9433

---=========================  ALL  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator is NULL or 
					jd.HolidayIndicator = 1 
					--or 
					--jd.HolidayIndicator = 2
				)
			GROUP BY j.EMPLOYEEID
		)
		and ntr.DESCRIPTION <> 'Sickness' and ntr.DESCRIPTION <> 'Annual Leave' and ntr.DESCRIPTION <> 'Internal Meeting' and ntr.DESCRIPTION <> 'Training' and ntr.DESCRIPTION <> 'Flex Leave' 
		and ntr.DESCRIPTION <> 'BRS TOIL Recorded' and ntr.DESCRIPTION <> 'BRS Time Off in Lieu' and ntr.DESCRIPTION <> 'BHG TOIL'
		--and abs.DURATION_HRS is not	NULL and abs.DURATION_HRS > 0
		--and (abs.DURATION_HRS is NULL or abs.DURATION_HRS = 0)
)

--4578

---=========================  TOIL  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = DURATION_HRS
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
	left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
	left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
	AND jrl.EMPLOYEEID in 
	(
		select j.EMPLOYEEID
		from E_absence a 
			left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
			left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
		where a.DURATION_TYPE is NULL 
			and 
			(
				jd.HolidayIndicator = 1 
			)
		GROUP BY j.EMPLOYEEID
	)
	and (ntr.DESCRIPTION = 'BRS TOIL Recorded' OR ntr.DESCRIPTION = 'BRS Time Off in Lieu')
	and abs.DURATION_HRS is not	NULL and abs.DURATION_HRS > 0
)
--721

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
	left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
	left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
	AND jrl.EMPLOYEEID in 
	(
		select j.EMPLOYEEID
		from E_absence a 
			left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
			left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
		where a.DURATION_TYPE is NULL 
			and 
			(
			--	jd.HolidayIndicator is NULL or 
				jd.HolidayIndicator = 1 
				--or 
				--jd.HolidayIndicator = 2
			)
		GROUP BY j.EMPLOYEEID
	)
	and (ntr.DESCRIPTION = 'BRS TOIL Recorded' OR ntr.DESCRIPTION = 'BRS Time Off in Lieu')
	and (abs.DURATION_HRS is NULL or abs.DURATION_HRS = 0)
)
--5

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = DURATION_HRS
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
	left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
	left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
	AND jrl.EMPLOYEEID in 
	(
		select j.EMPLOYEEID
		from E_absence a 
			left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
			left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
		where a.DURATION_TYPE is NULL 
			and 
			(
				jd.HolidayIndicator = 1 
			)
		GROUP BY j.EMPLOYEEID
	)
	and ntr.DESCRIPTION = 'BHG TOIL'
	and abs.DURATION_HRS is not	NULL and abs.DURATION_HRS > 0
)
--40

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
	left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
	left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
	AND jrl.EMPLOYEEID in 
	(
		select j.EMPLOYEEID
		from E_absence a 
			left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
			left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
		where a.DURATION_TYPE is NULL 
			and 
			(
				jd.HolidayIndicator = 1 
			)
		GROUP BY j.EMPLOYEEID
	)
	and ntr.DESCRIPTION = 'BHG TOIL'
	and (abs.DURATION_HRS is NULL or abs.DURATION_HRS = 0)
)

--817

---=========================  Training  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID  --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator is NULL or 
					jd.HolidayIndicator = 1 
				)
			GROUP BY j.EMPLOYEEID
		)

	 and ntr.DESCRIPTION = 'Training'
	 and (abs.DURATION_HRS = 0 or abs.DURATION_HRS is NULL)
)
-- 3227

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = DURATION_HRS
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator = 1 
				)
			GROUP BY j.EMPLOYEEID
		)

	 and ntr.DESCRIPTION = 'Training'
	  and abs.DURATION_HRS <> 0 and abs.DURATION_HRS is not NULL
)
-- 136

---=========================  Hourly Base employee  ==============================

UPDATE E_ABSENCE 
set DURATION_TYPE = 'Hours'
where ABSENCEHISTORYID in (
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(select j.EMPLOYEEID
		 from E_absence a 
			left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
			left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
		 where a.DURATION_TYPE is NULL 
			and 
			(
			--	jd.HolidayIndicator is NULL or 
			--	jd.HolidayIndicator = 1 
				--or 
				jd.HolidayIndicator = 2
			)
		 GROUP BY j.EMPLOYEEID)
		and abs.DURATION_HRS is not	NULL and abs.DURATION_HRS > 0
)
--5225

UPDATE E_ABSENCE 
set DURATION_TYPE = 'Days'
where ABSENCEHISTORYID in (
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator = 2
				)
			GROUP BY j.EMPLOYEEID
		)
		and (abs.DURATION_HRS is NULL or abs.DURATION_HRS = 0)
)
--10280

---=========================  Internal Meeting  ==============================

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = DURATION_HRS
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
	from E_ABSENCE abs
		left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID -- and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
		left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
	where abs.DURATION_TYPE is NULL 
		AND jrl.EMPLOYEEID in 
		(
			select j.EMPLOYEEID
			from E_absence a 
				left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
				left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
			where a.DURATION_TYPE is NULL 
				and 
				(
					jd.HolidayIndicator is NULL or 
					jd.HolidayIndicator = 1 
				)
			GROUP BY j.EMPLOYEEID
		)

	 and ntr.DESCRIPTION = 'Internal Meeting'
	 and abs.DURATION_HRS <> 0 and abs.DURATION_HRS is not NULL
)
-- 12588

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = (DURATION * 7.5)
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
		from E_ABSENCE abs
			left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID  and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
			left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
		where abs.DURATION_TYPE is NULL 
			AND jrl.EMPLOYEEID in 
			(
				select j.EMPLOYEEID
				from E_absence a 
					left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
					left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
				where a.DURATION_TYPE is NULL 
					and 
					(
					--	jd.HolidayIndicator is NULL or 
						jd.HolidayIndicator = 1 
						--or 
						--jd.HolidayIndicator = 2
					)
				GROUP BY j.EMPLOYEEID
			)

		 and ntr.DESCRIPTION = 'Internal Meeting'
		 and jrl.EMPLOYEEID != 817
		 and (abs.DURATION_HRS = 0 or abs.DURATION_HRS is NULL)
)

--452

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = 8.5
where ABSENCEHISTORYID in 
(
	select abs.ABSENCEHISTORYID
		from E_ABSENCE abs
			left join E_JOURNAL jrl on jrl.JOURNALID = abs.JOURNALID  --and abs.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = jrl.journalid)
			left JOIN E_NATURE ntr on jrl.ITEMNATUREID = ntr.ITEMNATUREID
		where abs.DURATION_TYPE is NULL 
			AND jrl.EMPLOYEEID in 
			(
				select j.EMPLOYEEID
				from E_absence a 
					left join E_JOURNAL j on a.JOURNALID = j.JOURNALID --AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM E_absence WHERE journalid = A.journalid)
					left join E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
				where a.DURATION_TYPE is NULL 
					and 
					(
					--	jd.HolidayIndicator is NULL or 
						jd.HolidayIndicator = 1 
						--or 
						--jd.HolidayIndicator = 2
					)
				GROUP BY j.EMPLOYEEID
			)

		 and ntr.DESCRIPTION = 'Internal Meeting'
		 and jrl.EMPLOYEEID = 817
		 and abs.STARTDATE > GETDATE()
		 and (abs.DURATION_HRS = 0 or abs.DURATION_HRS is NULL)
)
--97

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = 8.5
where ABSENCEHISTORYID in (129598, 129599, 129600)

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = (DURATION * 7.5)
where ABSENCEHISTORYID in (70577, 70578, 72870, 72871)

--7

UPDATE E_ABSENCE 
	set DURATION_TYPE = 'Hours', DURATION = (DURATION * 7.5)
where ABSENCEHISTORYID in 
(
	
	select abs.ABSENCEHISTORYID
	from E_absence abs 
		left join E_JOURNAL j on abs.JOURNALID = j.JOURNALID
		LEFT JOIN E_NATURE ntr on j.ITEMNATUREID = ntr.ITEMNATUREID
		LEFT JOIN E_JOBDETAILS jd on j.EMPLOYEEID = jd.EMPLOYEEID
	where abs.DURATION_TYPE is NULL
	and ntr.DESCRIPTION = 'Internal Meeting'
)

--678