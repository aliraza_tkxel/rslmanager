CREATE TABLE [dbo].[I_SUGGESTIONS]
(
[SuggestionID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[DateSubmitted] [smalldatetime] NULL CONSTRAINT [DF_I_SUGGESTIONS_DateSubmitted] DEFAULT (getdate()),
[Topic] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suggestion] [varchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Action] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[I_SUGGESTIONS] ADD CONSTRAINT [PK_I_SUGGESTIONS] PRIMARY KEY CLUSTERED  ([SuggestionID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
