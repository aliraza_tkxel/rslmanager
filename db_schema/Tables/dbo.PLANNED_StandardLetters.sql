CREATE TABLE [dbo].[PLANNED_StandardLetters]
(
[StandardLetterId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [smallint] NOT NULL,
[ActionId] [int] NOT NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedBy] [int] NULL,
[CreatedDate] [smalldatetime] NOT NULL,
[ModifiedDate] [smalldatetime] NULL,
[SignOffLookupCode] [int] NULL,
[TeamId] [int] NULL,
[FromResourceId] [int] NULL,
[IsPrinted] [bit] NULL,
[IsActive] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[PLANNED_StandardLetters] ADD 
CONSTRAINT [PK_PLANNED_StandardLetters] PRIMARY KEY CLUSTERED  ([StandardLetterId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PLANNED_StandardLetters] ADD CONSTRAINT [FK_PLANNED_StandardLetters_PLANNED_ACTION_ActionId] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[PLANNED_Action] ([ActionId])
GO
