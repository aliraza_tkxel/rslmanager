CREATE TABLE [dbo].[z_tempjim1]
(
[CID] [int] NOT NULL IDENTITY(1, 1),
[ORDERID] [int] NULL,
[INVOICENUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICEID] [int] NULL,
[PROCESSDATE] [smalldatetime] NULL,
[GROSSCOST] [money] NULL,
[DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BREAKDOWN] [money] NULL,
[DEBIT] [money] NULL,
[CREDIT] [money] NULL
) ON [PRIMARY]
GO
