BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'G_Company')
BEGIN

CREATE TABLE [dbo].[G_Company](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreatedDate] [smalldatetime] NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[LastModifiedDate] [smalldatetime] NULL,
	[DeletedBy] [uniqueidentifier] NULL,
	[DeletedDate] [smalldatetime] NULL,
	[DeletedFlag] [bit] NULL CONSTRAINT [DF_G_Company_DeletedFlag]  DEFAULT ((0)),
 CONSTRAINT [PK_AI_BodyPart] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS ( SELECT * FROM  dbo.G_Company)
 BEGIN
 
insert into [G_Company] (Description, CreatedDate)
select 'Broadland Housing Association',getdate()

insert into [G_Company] (Description, CreatedDate)
select 'St Benedicts',getdate()

insert into [G_Company] (Description, CreatedDate)
select 'Broadland Developments',getdate()

end