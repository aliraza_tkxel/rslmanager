CREATE TABLE [dbo].[F_DIRECTPOSTITEMS]
(
[ITEMID] [int] NOT NULL,
[ITEMNAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F_ITEMTYPE] [int] NULL,
[F_PAYMENTTYPE] [int] NULL,
[F_CORRESPONDINGTYPE] [int] NULL,
[F_STATUS] [int] NULL,
[DIRECTION] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[F_DIRECTPOSTITEMS] ADD CONSTRAINT [PK_F_DIRECTPOSTITEMS] PRIMARY KEY CLUSTERED  ([ITEMID]) WITH FILLFACTOR=30 ON [PRIMARY]
GO
