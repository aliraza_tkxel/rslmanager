CREATE TABLE [dbo].[PS_Survey_Item_Images]
(
[ImagesId] [int] NOT NULL IDENTITY(1, 1),
[SurveyId] [int] NULL,
[ItemId] [int] NULL,
[ImagePath] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageDate] [smalldatetime] NULL,
[ImageIdentifier] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Item_Images] ADD 
CONSTRAINT [PK_PS_Survey_Item_Images] PRIMARY KEY CLUSTERED  ([ImagesId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Item_Images] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Item_Images_PA_ITEM] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[PA_ITEM] ([ItemID])
ALTER TABLE [dbo].[PS_Survey_Item_Images] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Item_Images_PS_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[PS_Survey] ([SurveyId])
GO
