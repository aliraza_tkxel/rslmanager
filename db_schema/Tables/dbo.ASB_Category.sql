USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_Category')
BEGIN
CREATE TABLE [dbo].[ASB_Category]
(
	[CategoryId] [int] IDENTITY(1,1)PRIMARY KEY NOT NULL,
	[Description] [nvarchar](100)
)
INSERT [dbo].[ASB_Category] ([Description]) VALUES ( N'Yes')
INSERT [dbo].[ASB_Category] ([Description]) VALUES ( N'No')
PRINT 'Table Created'
END
ELSE
BEGIN
if exists(select * from [dbo].[ASB_Category]
              where [CategoryId] = 3 and [Description] = 'Personal')
BEGIN
	DELETE FROM [RSLBHALive].[dbo].[ASB_Category]
	WHERE [CategoryId]=3
	UPDATE [RSLBHALive].[dbo].[ASB_Category]
	SET [Description]='Yes'
	 where [CategoryId]=1

    UPDATE [RSLBHALive].[dbo].[ASB_Category]
	SET [Description]='No'
	where [CategoryId]=2
	PRINT 'Categories changed to Yes and No'
END
PRINT 'Table already exist..'
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH