USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

/****** Object:  Table [dbo].[C_ADDRESS]    Script Date: 12/15/2016 17:33:18 ******/


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'C_ADDRESS')
	BEGIN

CREATE TABLE [dbo].[C_ADDRESS](
	[ADDRESSID] [int] IDENTITY(1,1) NOT NULL,
	[CUSTOMERID] [int] NULL,
	[HOUSENUMBER] [nvarchar](50) NULL,
	[ADDRESS1] [nvarchar](100) NULL,
	[ADDRESS2] [nvarchar](100) NULL,
	[ADDRESS3] [nvarchar](100) NULL,
	[TOWNCITY] [nvarchar](100) NULL,
	[POSTCODE] [nvarchar](20) NULL,
	[COUNTY] [nvarchar](100) NULL,
	[TEL] [nvarchar](20) NULL,
	[MOBILE] [nvarchar](20) NULL,
	[FAX] [nvarchar](20) NULL,
	[EMAIL] [nvarchar](150) NULL,
	[ISDEFAULT] [int] NULL,
	[ADDRESSTYPE] [int] NULL,
	[ISPOWEROFATTORNEY] [int] NULL,
	[UPDATEDBYZ] [int] NULL,
	[CONTACTFIRSTNAME] [nvarchar](200) NULL,
	[CONTACTSURNAME] [nvarchar](200) NULL,
	[RELATIONSHIP] [nvarchar](1000) NULL,
	[CONTACTDOB] [smalldatetime] NULL,
	[TELWORK] [nvarchar](20) NULL,
	[TELRELATIVE] [nvarchar](20) NULL,
	[TELRELATIONSHIP] [nvarchar](1000) NULL,
	[MOBILE2] [nvarchar](20) NULL,
 CONSTRAINT [PK_C_ADDRESS] PRIMARY KEY NONCLUSTERED 
(
	[ADDRESSID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 60) ON [PRIMARY]
) ON [PRIMARY]



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This stores the home telephone number could not rename the column because it is being used in other procedures (TELHOME)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ADDRESS', @level2type=N'COLUMN',@level2name=N'TEL'


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This stores the first mobile number MOBILE1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ADDRESS', @level2type=N'COLUMN',@level2name=N'MOBILE'

End
ELSE
	BEGIN
	print 'C_ADDRESS Table already Exist'
	END
	
---=====================================================================================================================
---==================================Add new Columns-===================================================================
---=====================================================================================================================

IF COL_LENGTH('C_ADDRESS', 'AppVersion') IS NULL
	BEGIN
			ALTER TABLE C_ADDRESS
			ADD AppVersion NVARCHAR(100) NULL
			PRINT('COLUMN AppVersion Added')
	END
	
IF COL_LENGTH('C_ADDRESS', 'CreatedOnApp') IS NULL
	BEGIN
			ALTER TABLE C_ADDRESS
			ADD CreatedOnApp SMALLDATETIME NULL
			PRINT('COLUMN CreatedOnApp Added')
	END
								
IF COL_LENGTH('C_ADDRESS', 'CreatedOnServer') IS NULL
	BEGIN
			ALTER TABLE C_ADDRESS
			ADD CreatedOnServer SMALLDATETIME NULL
			PRINT('COLUMN CreatedOnServer Added')
	END
		
IF COL_LENGTH('C_ADDRESS', 'LastModifiedOnApp') IS NULL
	BEGIN
			ALTER TABLE C_ADDRESS
			ADD LastModifiedOnApp SMALLDATETIME NULL
			PRINT('COLUMN LastModifiedOnApp Added')
	END		
IF COL_LENGTH('C_ADDRESS', 'LastModifiedOnServer') IS NULL
	BEGIN
			ALTER TABLE C_ADDRESS
			ADD LastModifiedOnServer SMALLDATETIME NULL
			PRINT('COLUMN LastModifiedOnServer Added')
	END			










IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH