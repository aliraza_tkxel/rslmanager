Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E__EMPLOYEE')
BEGIN
  PRINT 'Table Exists';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E__EMPLOYEE]' ) AND name = 'EmploymentContracts')
	BEGIN
		ALTER TABLE E__EMPLOYEE ADD EmploymentContracts NVARCHAR(500) DEFAULT NULL
	PRINT 'EmploymentContracts added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END	

-- Adding columns for HR Module


IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E__EMPLOYEE]' ) AND name = 'AKA')
	BEGIN
		ALTER TABLE E__EMPLOYEE ADD AKA NVARCHAR(100) DEFAULT NULL
	PRINT 'AKA added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END


IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E__EMPLOYEE]' ) AND name = 'NiNumber')
	BEGIN
		ALTER TABLE E__EMPLOYEE ADD NiNumber NVARCHAR(100) DEFAULT NULL
	PRINT 'NiNumber added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E__EMPLOYEE]' ) AND name = 'DBS_Date')
	BEGIN
		ALTER TABLE E__EMPLOYEE ADD DBS_Date DATETIME DEFAULT NULL
	PRINT 'DBS_Date added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E__EMPLOYEE]' ) AND name = 'Reference')
	BEGIN
		ALTER TABLE E__EMPLOYEE ADD Reference NVARCHAR(100) DEFAULT NULL
	PRINT 'Reference added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E__EMPLOYEE]' ) AND name = 'NextOfKin')
	BEGIN
		ALTER TABLE E__EMPLOYEE ADD NextOfKin NVARCHAR(100) DEFAULT NULL
	PRINT 'NextOfKin added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END



END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 