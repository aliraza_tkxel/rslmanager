USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_CaseStatus')
BEGIN
CREATE TABLE [dbo].[ASB_CaseStatus]
(
	[StatusId] [int] IDENTITY(1,1) PRIMARY KEY  NOT NULL,
	[Description] [nvarchar](100) NULL
) 
INSERT [dbo].[ASB_CaseStatus] ([Description]) VALUES ( N'Open')
INSERT [dbo].[ASB_CaseStatus] ([Description]) VALUES ( N'Closed Resolved')
INSERT [dbo].[ASB_CaseStatus] ([Description]) VALUES ( N'Closed Unresolved')
INSERT [dbo].[ASB_CaseStatus] ([Description]) VALUES ( N'Closed None ASB')
PRINT 'Table Created'
END

ELSE
BEGIN
IF EXISTS(select * from [dbo].[ASB_CaseStatus] where [Description]='Closed')
begin
Update [dbo].[ASB_CaseStatus] set [Description]='Closed Resolved'
where [Description]='Closed'
PRINT 'Row added for Colsed Resolved'
end
IF NOT EXISTS(select * from [dbo].[ASB_CaseStatus] where [Description]='Closed Unresolved')
BEGIN
INSERT [dbo].[ASB_CaseStatus] ([Description]) VALUES ( N'Closed Unresolved')
PRINT 'Row added for Colsed Unresolved'
END
IF NOT EXISTS(select * from [dbo].[ASB_CaseStatus] where [Description]='Closed None ASB')
BEGIN
INSERT [dbo].[ASB_CaseStatus] ([Description]) VALUES ( N'Closed None ASB')
PRINT 'Row added for Colsed none asb'
END
PRINT 'Table already exist..'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH