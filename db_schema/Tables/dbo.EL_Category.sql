CREATE TABLE [dbo].[EL_Category]
(
[CategoryID] [int] NOT NULL IDENTITY(1, 1),
[CategoryName] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[EL_Category] ADD 
CONSTRAINT [PK_EL_Categories] PRIMARY KEY CLUSTERED  ([CategoryID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
