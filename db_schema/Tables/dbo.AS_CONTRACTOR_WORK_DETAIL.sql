
--check table existance
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AS_CONTRACTOR_WORK_DETAIL')
BEGIN
CREATE TABLE AS_CONTRACTOR_WORK_DETAIL
	(
		ApplianceServicingWorkDetailId int primary key  not null identity(1,1),
		ContractorId int not null,
		WorkRequired NVARCHAR(MAX)NOT NULL,
		NetCost SMALLMONEY NOT NULL,
		VatId INT NOT NULL,
		Vat SMALLMONEY NOT NULL,
		Gross SMALLMONEY NOT NULL,
		ExpenditureId INT NOT NULL,
		PurchaseOrderItemId int not null
	)
  PRINT 'Table Created';
END
--if table not exist
ELSE IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AS_CONTRACTOR_WORK_DETAIL')
BEGIN

PRINT 'Table already exist.'

END




