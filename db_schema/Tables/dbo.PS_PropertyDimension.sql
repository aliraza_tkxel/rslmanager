CREATE TABLE [dbo].[PS_PropertyDimension]
(
[SurveyDimId] [int] NOT NULL IDENTITY(1, 1),
[PropertyId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SurveyId] [int] NULL,
[HallwayWidth] [float] NULL,
[HallwayLength] [float] NULL,
[KitchenWidth] [float] NULL,
[KitchenLength] [float] NULL,
[LivingRoomWidth] [float] NULL,
[LivingRoomLength] [float] NULL,
[Bedroom1Width] [float] NULL,
[Bedroom1Length] [float] NULL,
[Bedroom2Width] [float] NULL,
[Bedroom2Hegiht] [float] NULL,
[BathroomWidth] [float] NULL,
[BathroomLength] [float] NULL,
[WcWidth] [float] NULL,
[WcLength] [float] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_PropertyDimension] ADD 
CONSTRAINT [PK_Ps_AccomodationDetail] PRIMARY KEY CLUSTERED  ([SurveyDimId]) WITH (FILLFACTOR=100) ON [PRIMARY]

GO
