CREATE TABLE [dbo].[AM_HistoricalBalanceList]
(
[HistoricalBalanceListId] [int] NOT NULL IDENTITY(1, 1),
[TenancyId] [int] NOT NULL,
[CustomerId] [int] NOT NULL,
[RentBalance] [float] NOT NULL,
[HB] [float] NOT NULL,
[SLB] [float] NOT NULL,
[AccountTimeStamp] [datetime] NULL,
[CreatedDate] [datetime] NOT NULL,
[month] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[year] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_HistoricalBalanceList] ADD 
CONSTRAINT [PK_AM_HistoricalBalanceList] PRIMARY KEY CLUSTERED  ([HistoricalBalanceListId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
