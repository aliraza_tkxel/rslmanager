CREATE TABLE [dbo].[AM_ActionHistory]
(
[ActionHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ActionId] [int] NOT NULL,
[StatusId] [int] NOT NULL,
[StatusHistoryId] [int] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [int] NOT NULL,
[RecommendedFollowupPeriod] [int] NOT NULL,
[RecommendedFollowupPeriodFrequencyLookup] [int] NOT NULL,
[NextActionAlert] [int] NOT NULL,
[NextActionAlertFrequencyLookup] [int] NOT NULL,
[NextActionDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsProceduralAction] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NOT NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[IsPaymentPlanMandotry] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_ActionHistory] ADD 
CONSTRAINT [PK_AM_ActionHistory] PRIMARY KEY CLUSTERED  ([ActionHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_ActionHistory] ADD CONSTRAINT [Action] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AM_ActionHistory] ADD CONSTRAINT [ActionHistoryNextActionAlertLookupCode] FOREIGN KEY ([NextActionAlertFrequencyLookup]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_ActionHistory] ADD CONSTRAINT [ActionHistoryRecommendedFollowupPeriodLookupCode] FOREIGN KEY ([RecommendedFollowupPeriodFrequencyLookup]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_ActionHistory] ADD CONSTRAINT [ActionHistoryStatusHistory] FOREIGN KEY ([StatusHistoryId]) REFERENCES [dbo].[AM_StatusHistory] ([StatusHistoryId])
GO
ALTER TABLE [dbo].[AM_ActionHistory] ADD CONSTRAINT [StatusActionHistory] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
GO
