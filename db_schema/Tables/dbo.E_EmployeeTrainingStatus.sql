Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_EmployeeTrainingStatus')
BEGIN
	 CREATE TABLE [dbo].[E_EmployeeTrainingStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
 CONSTRAINT [PK_E_EmployeeTrainingStatus] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END 

IF NOT EXISTS (SELECT * FROM E_EmployeeTrainingStatus)
BEGIN

SET IDENTITY_INSERT [dbo].[E_EmployeeTrainingStatus] ON 
INSERT [dbo].[E_EmployeeTrainingStatus] ([StatusId], [Title]) VALUES (1, N'Submitted')

INSERT [dbo].[E_EmployeeTrainingStatus] ([StatusId], [Title]) VALUES (2, N'Approved')

INSERT [dbo].[E_EmployeeTrainingStatus] ([StatusId], [Title]) VALUES (3, N'Declined')

SET IDENTITY_INSERT [dbo].[E_EmployeeTrainingStatus] OFF
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Requested')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'Requested')
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Supported')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'Supported')
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Exec Approved')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'Exec Approved')
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Exec Declined')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'Exec Declined')
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='HR Approved')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'HR Approved')
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='HR Declined')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'HR Declined')
END

IF EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Submitted')
BEGIN
	DELETE from E_EmployeeTrainingStatus where Title='Submitted'
END

IF EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Approved')
BEGIN
	DELETE from E_EmployeeTrainingStatus where Title='Approved'
END

IF EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Pending')
BEGIN
	DELETE from E_EmployeeTrainingStatus where Title='Pending'
END

IF EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Supported')
BEGIN
	update E_EmployeeTrainingStatus set Title='Manager Supported' where Title='Supported'
END

IF NOT EXISTS (Select 1 from E_EmployeeTrainingStatus where Title='Exec Supported')
BEGIN
	INSERT [dbo].[E_EmployeeTrainingStatus] ([Title]) VALUES (N'Exec Supported')
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 



