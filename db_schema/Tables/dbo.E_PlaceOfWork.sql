USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[G_OFFICE]    Script Date: 04/07/2017 07:52:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_PlaceOfWork')
BEGIN
CREATE TABLE [dbo].[E_PlaceOfWork](
	[PLACEID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
 CONSTRAINT [PK_E_PlaceOfWork] PRIMARY KEY NONCLUSTERED 
(
	[PLACEID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Norwich')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Norwich - Agile')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Kings Lynn')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Kings Lynn - Agile')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Great Yarmouth')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Great Yarmouth - Agile')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Dereham')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Dereham - Agile')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � York Place')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � St Kaths')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � Woodcote')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � Cedars')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � Samford/Oulton')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � Robert Kett Court')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme - Harriett/Dellrose')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � The Lawns')
INSERT INTO [dbo].[E_PlaceOfWork] VALUES ('Scheme � Lloyd/Benjamin')

Print 'Table Place Of Work Created'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH