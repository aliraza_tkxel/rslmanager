CREATE TABLE [dbo].[P_TARGETRENT]
(
[TARGETRENTID] [int] NOT NULL IDENTITY(1, 1),
[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REASSESMENTDATE] [smalldatetime] NULL,
[CUTOFFDATE] [smalldatetime] NULL,
[OLD_RENTTOTAL] [money] NULL,
[OLD_DDTOTAL] [money] NULL,
[OLD_ADLASSETCNT] [int] NULL,
[OLD_ADLASSETTOTAL] [money] NULL,
[NEW_ADLASSETTOTAL] [money] NULL,
[OLD_RENT] [money] NULL,
[OLD_SERVICES] [money] NULL,
[OLD_COUNCILTAX] [money] NULL,
[OLD_WATERRATES] [money] NULL,
[OLD_INELIGSERV] [money] NULL,
[OLD_SUPPORTEDSERVICES] [money] NULL,
[RENT] [money] NULL,
[SERVICES] [money] NULL,
[COUNCILTAX] [money] NULL,
[WATERRATES] [money] NULL,
[INELIGSERV] [money] NULL,
[SUPPORTEDSERVICES] [money] NULL,
[GARAGECOUNT] [int] NULL,
[OLDGARAGECHARGE] [money] NULL,
[NEWGARAGECHARGE] [money] NULL,
[DATERENTSET] [datetime] NULL,
[RENTEFFECTIVE] [datetime] NULL,
[YEARLY_DD_INCREASE] [int] NULL,
[DDSCHEDULEID] [int] NULL,
[DDREGULARPAYMENT] [money] NULL,
[DDPAYMENTDATE] [smalldatetime] NULL,
[FILEID] [int] NULL,
[TARGET_RENT] [money] NULL,
[ACTUAL_INCREASE_AT_APRIL1ST] [money] NULL,
[ACTIVE] [int] NULL,
[NOTINFILE_ADDEDFROM_ASSET] [int] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_P_TARGETRENT_PROPERTYID] ON [dbo].[P_TARGETRENT] ([PROPERTYID]) WITH (FILLFACTOR=100) ON [PRIMARY]

GO
ALTER TABLE [dbo].[P_TARGETRENT] ADD CONSTRAINT [PK_P_TAGETRENT] PRIMARY KEY CLUSTERED  ([TARGETRENTID]) WITH (FILLFACTOR=30) ON [PRIMARY]
GO

GRANT SELECT ON  [dbo].[P_TARGETRENT] TO [rackspace_datareader]
GO
