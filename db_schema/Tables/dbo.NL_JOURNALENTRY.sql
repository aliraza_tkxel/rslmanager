Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'NL_JOURNALENTRY')
BEGIN
CREATE TABLE [dbo].[NL_JOURNALENTRY]
(
[TXNID] [int] PRIMARY KEY NOT NULL IDENTITY(1, 1),
[TRANSACTIONTYPE] [int] NULL,

[TRANSACTIONID] [int] NULL,
[SUBTRANSACTIONID] [int] NULL,
[TIMECREATED] [datetime] NOT NULL CONSTRAINT [DF_NL_JOURNALENTRY_TIMECREATED] DEFAULT (getdate()),
[TIMEMODIFIED] [datetime] NULL,
[DESCRIPTION] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXNDATE] [smalldatetime] NULL,
[REFNUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EXCHANGERATE] [float] NULL
--FOREIGN KEY [TRANSACTIONTYPE] REFERENCES NL_TRANSACTIONTYPE(TRANSACTIONTYPEID)

) 
ALTER TABLE [dbo].[NL_JOURNALENTRY]  WITH CHECK ADD  
CONSTRAINT [FK_NL_JOURNALENTRY_NL_TRANSACTIONTYPE] FOREIGN KEY([TRANSACTIONTYPE])
REFERENCES [dbo].[NL_TRANSACTIONTYPE] ([TRANSACTIONTYPEID])
ALTER TABLE [dbo].[NL_JOURNALENTRY] CHECK CONSTRAINT [FK_NL_JOURNALENTRY_NL_TRANSACTIONTYPE]


PRINT 'Table Created..'
END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'NL_JOURNALENTRY')
BEGIN
  PRINT 'Table Exists';
IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[NL_JOURNALENTRY]' ) AND name = 'SchemeId')--adding schemeId
	BEGIN
		ALTER TABLE NL_JOURNALENTRY ADD SchemeId INT DEFAULT NULL
	PRINT 'SchemeId added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT 'SchemeId already exits'
	END	

	
	
	
	IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[NL_JOURNALENTRY]' ) AND name = 'BlockId')--adding blockId
	BEGIN
		ALTER TABLE NL_JOURNALENTRY ADD BlockId INT DEFAULT NULL
	PRINT 'BlockId added successfully!'
	END--if
	
	ELSE
	BEGIN 
		PRINT 'BlockId already exits'
	END	
	
	
	
	IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[NL_JOURNALENTRY]' ) AND name = 'PropertyId')--adding propertyId
	BEGIN
		ALTER TABLE NL_JOURNALENTRY ADD PropertyId nvarchar(20) DEFAULT NULL
	PRINT 'PropertyId added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT 'PropertyId already exits'
	END	

	IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[NL_JOURNALENTRY]' ) AND name = 'CompanyId')--adding propertyId
	BEGIN
		ALTER TABLE NL_JOURNALENTRY ADD CompanyId int DEFAULT NULL
	PRINT 'CompanyId added successfully!'
	END--if
	ELSE
	BEGIN 
		PRINT 'CompanyId already exits'
	END	

END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 

