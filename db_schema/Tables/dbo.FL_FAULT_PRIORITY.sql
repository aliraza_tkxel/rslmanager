CREATE TABLE [dbo].[FL_FAULT_PRIORITY]
(
[PriorityID] [int] NOT NULL IDENTITY(1, 1),
[PriorityName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseTime] [int] NULL,
[Status] [bit] NULL,
[Days] [bit] NULL,
[Hrs] [bit] NULL,
[sortorder] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_PRIORITY] ADD 
CONSTRAINT [PK_FL_PIRORITY] PRIMARY KEY CLUSTERED  ([PriorityID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
