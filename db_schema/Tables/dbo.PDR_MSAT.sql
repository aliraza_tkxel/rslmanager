USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PDR_MSAT')
BEGIN
CREATE TABLE [dbo].[PDR_MSAT](
	[MSATId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](50) NULL,
	[ItemId] [int] NULL,
	[MSATTypeId] [int] NULL,
	[CycleTypeId] [int] NULL,
	[IsRequired] [bit] NULL,
	[LastDate] [datetime] NULL,
	[Cycle] [int] NULL,
	[NextDate] [datetime] NULL,
	[AnnualApportionment] [float] NULL,
	[IsActive] [bit] NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[CustomerId] [int] NULL,
	[TenancyId] [int] NULL,
	[TerminationDate] [datetime] NULL,
	[ReletDate] [datetime] NULL,
	[isPending] [bit] NULL,
	[attributeTypeId] [int] NULL,
	[ProvisionId] [int] NULL,
 CONSTRAINT [PK_PDR_MSAT] PRIMARY KEY CLUSTERED 
(
	[MSATId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

END

IF COL_LENGTH('PDR_MSAT', 'CycleCost') IS NULL
	BEGIN
		ALTER TABLE PDR_MSAT
		ADD [CycleCost] [float] NULL
		PRINT('COLUMN CycleCost CREATED')
	END
IF COL_LENGTH('PDR_MSAT', 'SchemeProperty') IS NULL
	BEGIN
		ALTER TABLE PDR_MSAT
		ADD [SchemeProperty] [nvarchar](50) NULL
		PRINT('COLUMN SchemeProperty CREATED')
	END

IF COL_LENGTH('PDR_MSAT', 'INCSC') IS NULL
	BEGIN
		ALTER TABLE PDR_MSAT
		ADD [INCSC] [bit] NULL
		PRINT('COLUMN INCSC CREATED')
	END

IF COL_LENGTH('PDR_MSAT', 'PropertyApportionment') IS NULL
	BEGIN
		ALTER TABLE PDR_MSAT
		ADD [PropertyApportionment] [money] NULL
		PRINT('COLUMN PropertyApportionment CREATED')
	END

IF COL_LENGTH('PDR_MSAT', 'ProvisionCategory') IS NULL
	BEGIN
		ALTER TABLE PDR_MSAT
		ADD [ProvisionCategory] [nvarchar](50) NULL
		PRINT('COLUMN ProvisionCategory CREATED')
	END

IF COL_LENGTH('PDR_MSAT', 'ProvisionDescription') IS NULL
	BEGIN
		ALTER TABLE PDR_MSAT
		ADD [ProvisionDescription] [nvarchar](50) NULL
		PRINT('COLUMN ProvisionDescription CREATED')
	END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH