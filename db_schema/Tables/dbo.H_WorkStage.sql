CREATE TABLE [dbo].[H_WorkStage]
(
[WorkStageID] [int] NOT NULL IDENTITY(1, 1),
[WorkStageName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
