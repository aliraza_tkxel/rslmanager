USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PS_Survey')
BEGIN

CREATE TABLE [dbo].[PS_Survey](
	[SurveyId] [int] IDENTITY(1,1) NOT NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[CompletedBy] [int] NULL,
	[SurveyDate] [smalldatetime] NULL,
 CONSTRAINT [PK_PS_Survey] PRIMARY KEY CLUSTERED 
(
	[SurveyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

END


	--===============================================================================================================
	--  10828 - Add schemeId and blockId columns
	--===============================================================================================================
	IF COL_LENGTH('PS_Survey', 'schemeId') IS NULL
	BEGIN
			ALTER TABLE PS_Survey
			ADD schemeId int NULL
			PRINT('COLUMN schemeId CREATED')
	END

	IF COL_LENGTH('PS_Survey', 'blockId') IS NULL
	BEGIN
			ALTER TABLE PS_Survey
			ADD blockId int NULL
			PRINT('COLUMN blockId CREATED')
	END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH



