CREATE TABLE [dbo].[C_REPAIR]
(
[REPAIRHISTORYID] [int] NOT NULL IDENTITY(1, 1),
[JOURNALID] [int] NULL,
[ITEMSTATUSID] [int] NULL,
[ITEMACTIONID] [int] NULL,
[LASTACTIONDATE] [smalldatetime] NULL CONSTRAINT [DF_C_REPAIR_LASTACTIONDATE] DEFAULT (getdate()),
[LASTACTIONUSER] [int] NULL,
[ITEMDETAILID] [int] NULL,
[CONTRACTORID] [int] NULL,
[TITLE] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOTES] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCOPEID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- 	THIS TRIGGER WILL RUN IF A MAJOR REPAIR HAS BEEN EITHER ACCEPTED BY THE CONTRACTOR
--	OR APPROVED BY A MEMBER OF BROADLAND. IF ACCEPTED THE PROPERTY STATUS WILL BE SET TO 
--	'UNAVAILABLE' AND THE SUBSTATUS TO 'UNDER REPAIR'. IF APPROVED THE PROPERTY STATUS
--	WILL BE SET TO 'AVAILABLE' AND THE SUBSTATUS TO NULL

CREATE          TRIGGER [dbo].[UPDATE_PROPERTYSTATUS] ON [dbo].[C_REPAIR]
FOR 	INSERT
AS

BEGIN
	DECLARE @DATE SMALLDATETIME
	DECLARE @PROPERTYID VARCHAR(30)
	DECLARE @ACTION INT
	DECLARE @ACCEPTED INT
	DECLARE @APPROVED INT
	DECLARE @DECLINED INT
	DECLARE @UNAVAILABLE INT
	DECLARE @UNDERREPAIR INT
	DECLARE @AVAILABLE INT
	DECLARE @LET INT
	DECLARE @ASSETTYPEID INT
	DECLARE @REPAIRCOUNTER INT
	DECLARE @KPI11_ACTIVE INT
	DECLARE @PENDINGTENANCY INT
	DECLARE @PENDINGTERMINATION INT

	SET 	@ACCEPTED = 5
	SET	@APPROVED = 9
	SET 	@DECLINED = 7
	SET	@UNAVAILABLE = 4 
	SET @LET = 2
	SET	@UNDERREPAIR = 3
	SET	@AVAILABLE = 1
	SET @PENDINGTENANCY = 21
	SET @PENDINGTERMINATION = 22
	SET 	@DATE = CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103)  

	/*
		Following statement was driven by mager repairs only....Vinu/Jimmy changed to include KPI11_ACTIVE
		Which was requested by Bordland
	*/

/*
by Adnan on 3rd Oct 2008
comment this bit AND ISNULL(DE.KPI11_ACTIVE,0) = 1 to make the query work.

*/
	SELECT 	@KPI11_ACTIVE=ISNULL(KPI11_ACTIVE,0),@PROPERTYID = P.PROPERTYID, @ASSETTYPEID = P.ASSETTYPE, @ACTION = REP.ITEMACTIONID
	FROM 	INSERTED REP
		INNER JOIN C_JOURNAL J ON J.JOURNALID = REP.JOURNALID
		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID
		INNER JOIN R_ITEMDETAIL DE ON DE.ITEMDETAILID = REP.ITEMDETAILID
		LEFT JOIN C_TENANCY T ON T.PROPERTYID = J.PROPERTYID
	WHERE	REP.ITEMACTIONID IN (@ACCEPTED, @APPROVED, @DECLINED) AND ISNULL(DE.KPI11_ACTIVE,0) = 1
		AND (T.ENDDATE IS NOT NULL OR T.TENANCYID IS NULL)
		--NOT EXISTS (SELECT PROPERTYID FROM C_TENANCY 
			--	   WHERE ENDDATE IS NULL AND PROPERTYID = J.PROPERTYID)
	

	-- IF ACTION IS ACCEPTED
	IF @ACTION = @ACCEPTED
		BEGIN
		
		--PRINT 'ACTION IS ''ACCEPTED'', UPDATING PROPERTY STATUS....'
	
			/*Vinu ammended on 01 march 2005 to fix the propery staus change during the mager repair approvel.*/

			-- UPDATE PROPERTY RECORD
			IF ((SELECT STATUS FROM  P__PROPERTY WHERE PROPERTYID = @PROPERTYID)= 1) AND @KPI11_ACTIVE=1
			BEGIN
				UPDATE P__PROPERTY SET STATUS = @UNAVAILABLE, SUBSTATUS = @UNDERREPAIR
				WHERE PROPERTYID = @PROPERTYID
			END		
		END


	IF (@ACTION = @APPROVED) OR (@ACTION = @DECLINED)
		BEGIN
		
		--PRINT 'ACTION IS ''APPROVED'', UPDATING PROPERTY STATUS....'

		-- CHANGE PROPERTY STATUS BACK TO AVAILABLE BUT ONLY IF THIS IS THE ONLY
		-- CURRENT REPAIR

		IF (SELECT COUNT(*) FROM C_JOURNAL J 
			INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS MAXREP FROM C_REPAIR 
					GROUP BY JOURNALID) R ON R.JOURNALID = J.JOURNALID
			INNER JOIN C_REPAIR REP ON REP.REPAIRHISTORYID = R.MAXREP
			INNER JOIN R_ITEMDETAIL DE ON DE.ITEMDETAILID = REP.ITEMDETAILID
		  WHERE	DE.KPI11_ACTIVE=1 AND PROPERTYID = @PROPERTYID AND REP.ITEMACTIONID IN (5,6,8)) = 0
				BEGIN
					-- UPDATE PROPERTY RECORD

					-- AMENDED BY MARTIN (22-SEP-06) TO TAKE INTO ACCOUNT WHETHER THE PROPERTY IS ACTUALLY AVAILABLE OR NOT!
					IF ( SELECT COUNT(*) FROM C_TENANCY	WHERE PROPERTYID = @PROPERTYID  AND ENDDATE < GETDATE() AND ENDDATE IS NOT NULL ) = 0 		-- I THINK THE INEQUALITIES HERE ARE CORRECT!?!
							BEGIN
								-- NO CURRENT TENANCIES ON THE PROPERTY ... MARK IT AVAILABLE
								UPDATE P__PROPERTY SET STATUS = @AVAILABLE, SUBSTATUS = NULL
								WHERE PROPERTYID = @PROPERTYID
							
								-- ANY PENDING TENANCIES?
								IF ( SELECT COUNT(*) FROM C_TENANCY WHERE PROPERTYID = @PROPERTYID AND STARTDATE > GETDATE() AND ENDDATE IS NULL ) > 0
								BEGIN -- THEN THERE'S A PENDING TENANCY
									UPDATE P__PROPERTY SET SUBSTATUS = @PENDINGTENANCY
									WHERE PROPERTYID = @PROPERTYID
								END		
							  --ELSE JUST LEAVE IT AS NULL				
							END
					ELSE
						-- THERE IS A TENANCY ACTIVE IN THIS PROPERTY CURRENTLY!
							BEGIN
								-- CURRENT TENANCY ON THE PROPERTY ... MARK PROPERTY AS LET
								UPDATE P__PROPERTY SET STATUS = @LET, SUBSTATUS = NULL
								WHERE PROPERTYID = @PROPERTYID

								-- ANY PENDING TERMINATIONS? (THE LOGIC IS THAT IF THERE IS AN ENDDATE AFTER TODAY THERE IS A PENDING TERMINATION
								IF ( SELECT COUNT(*) FROM C_TENANCY WHERE PROPERTYID = @PROPERTYID AND ENDDATE IS NOT NULL AND ENDDATE > GETDATE() ) > 0
								BEGIN -- THEN THERE'S A PENDING TENANCY
									UPDATE P__PROPERTY SET SUBSTATUS = @PENDINGTERMINATION
									WHERE PROPERTYID = @PROPERTYID
								END		
							  --ELSE ... NO PENDING TERMINATION JUST LEAVE IT AS NULL
							END
				
				END
		END



END



GO
ALTER TABLE [dbo].[C_REPAIR] ADD CONSTRAINT [PK_C_REPAIRS] PRIMARY KEY NONCLUSTERED  ([REPAIRHISTORYID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_C_REPAIR] ON [dbo].[C_REPAIR] ([JOURNALID] DESC, [REPAIRHISTORYID] DESC) WITH FILLFACTOR=90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[C_REPAIR] ADD CONSTRAINT [FK__C_REPAIR__SCOPEI__13DDB2A9] FOREIGN KEY ([SCOPEID]) REFERENCES [dbo].[S_SCOPE] ([SCOPEID])
GO
