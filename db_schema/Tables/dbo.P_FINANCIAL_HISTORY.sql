CREATE TABLE [dbo].[P_FINANCIAL_HISTORY]
(
[SID] [bigint] NOT NULL IDENTITY(1, 1),
[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOMINATINGBODY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FUNDINGAUTHORITY] [int] NULL,
[RENT] [money] NULL,
[SERVICES] [money] NULL,
[COUNCILTAX] [money] NULL,
[WATERRATES] [money] NULL,
[INELIGSERV] [money] NULL,
[SUPPORTEDSERVICES] [money] NULL,
[GARAGE] [money] NULL,
[TOTALRENT] [money] NULL,
[RENTTYPE] [int] NULL,
[OLDTOTAL] [money] NULL,
[RENTTYPEOLD] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DATERENTSET] [smalldatetime] NULL,
[RENTEFFECTIVE] [smalldatetime] NULL,
[TARGETRENT] [money] NULL,
[YIELD] [money] NULL,
[CAPITALVALUE] [money] NULL,
[INSURANCEVALUE] [money] NULL,
[OMVST] [money] NULL,
[EUV] [money] NULL,
[OMV] [money] NULL,
[CHARGE] [int] NULL,
[CHARGEVALUE] [money] NULL,
[NORENTCHANGE] [int] NULL,
[TARGETRENTSET] [int] NULL,
[PFUSERID] [int] NULL,
[FRApplicationDate] [smalldatetime] NULL,
[FRRegistrationDate] [smalldatetime] NULL,
[FREffectDate] [smalldatetime] NULL,
[FRStartDate] [smalldatetime] NULL,
[FRRegNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FRRegNextDue] [smalldatetime] NULL,
[PFTIMESTAMP] [smalldatetime] NOT NULL CONSTRAINT [DF_P_FINANCIAL_HISTORY_PFTIMESTAMP] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[P_FINANCIAL_HISTORY] ADD CONSTRAINT [PK_P_FINANCIAL_HISTORY] PRIMARY KEY NONCLUSTERED  ([SID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_P_FINANCIAL_HISTORY_PFTIMESTAMP] ON [dbo].[P_FINANCIAL_HISTORY] ([PFTIMESTAMP]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [P_FINANCIAL_HISTORY_PROPERTYID] ON [dbo].[P_FINANCIAL_HISTORY] ([PROPERTYID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[P_FINANCIAL_HISTORY] WITH NOCHECK ADD CONSTRAINT [FK_P_FINANCIAL_HISTORY_P__PROPERTY] FOREIGN KEY ([PROPERTYID]) REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])
GO
GRANT SELECT ON  [dbo].[P_FINANCIAL_HISTORY] TO [rackspace_datareader]
GO
