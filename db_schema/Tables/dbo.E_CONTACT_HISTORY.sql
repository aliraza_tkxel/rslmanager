USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_CONTACT_HISTORY')
BEGIN

	CREATE TABLE [dbo].[E_CONTACT_HISTORY](
		[CONTACTHISTORYID] [int] IDENTITY(1,1) NOT NULL,
		[CONTACTID] [int] NOT NULL,
		[EMPLOYEEID] [int] NOT NULL,
		[ADDRESS1] [nvarchar](50) NULL,
		[ADDRESS2] [nvarchar](40) NULL,
		[ADDRESS3] [nvarchar](50) NULL,
		[POSTALTOWN] [nvarchar](40) NULL,
		[COUNTY] [nvarchar](30) NULL,
		[POSTCODE] [nvarchar](10) NULL,
		[HOMETEL] [nvarchar](50) NULL,
		[MOBILE] [nvarchar](100) NULL,
		[WORKDD] [nvarchar](20) NULL,
		[WORKEXT] [nvarchar](20) NULL,
		[HOMEEMAIL] [nvarchar](120) NULL,
		[WORKEMAIL] [nvarchar](120) NULL,
		[EMERGENCYCONTACTNAME] [nvarchar](30) NULL,
		[EMERGENCYCONTACTTEL] [nvarchar](30) NULL,
		[EMERGENCYINFO] [nvarchar](150) NULL,
		[WORKMOBILE] [nchar](100) NULL,
		[LASTACTIONTIME] [datetime] NULL,
		[LASTACTIONUSER] [int] NULL,
		[NextOfKin] [nvarchar](100) NULL,
	 CONSTRAINT [PK_CONTACT_HISTORY] PRIMARY KEY NONCLUSTERED 
	(
		[CONTACTHISTORYID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 60) ON [PRIMARY]
	) ON [PRIMARY]

END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_CONTACT_HISTORY]' ) AND name = 'MobilePersonal')
	BEGIN
		ALTER TABLE E_CONTACT_HISTORY ADD MobilePersonal NVARCHAR(100) DEFAULT NULL
	PRINT 'MobilePersonal added successfully!'
	END--if

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_CONTACT_HISTORY]' ) AND name = 'EmergencyContactRelationship')
BEGIN
	ALTER TABLE E_CONTACT_HISTORY ADD EmergencyContactRelationship NVARCHAR(max) DEFAULT NULL
PRINT 'EmergencyContactRelationship added successfully!'
END--if


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 