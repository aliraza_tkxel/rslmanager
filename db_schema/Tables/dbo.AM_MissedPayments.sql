USE RSLBHALive


BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AM_MissedPayments')
    BEGIN


		CREATE TABLE [dbo].[AM_MissedPayments]
		(
		[MissedTenantId] [int] NOT NULL IDENTITY(1, 1),
		[CustomerId] [int] NOT NULL,
		[TenantId] [int] NOT NULL,
		[PaymentPlanId] [int] NOT NULL,
		[PaymentId] [int] NOT NULL,
		[MissedPaymentDetectionDate] [datetime] NOT NULL CONSTRAINT [DF_AM_MissedPayments_MissedPaymentDetectionDate] DEFAULT (((1)/(1))/(1))
		) ON [PRIMARY]
		ALTER TABLE [dbo].[AM_MissedPayments] ADD 
		CONSTRAINT [PK_AM_MissedTenants] PRIMARY KEY CLUSTERED  ([MissedTenantId]) WITH (FILLFACTOR=100) ON [PRIMARY]

END
ELSE

BEGIN
PRINT 'Table Already Exist';
END
    --========================================================================================================
    ------Adding Index
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_MissedPayments_PaymentPlanId')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_MissedPayments_PaymentPlanId
		  ON AM_MissedPayments (PaymentPlanId);  
	  PRINT 'IX_AM_MissedPayments_PaymentPlanId created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_MissedPayments_PaymentPlanId Index Already Exist';
	  END
    --========================================================================================================
    --======================================================================================================== 
    
    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    