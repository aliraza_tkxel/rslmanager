USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PLANNED_JOURNAL_HISTORY]    Script Date: 26-Dec-17 3:12:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PLANNED_JOURNAL_HISTORY')
BEGIN

CREATE TABLE [dbo].[PLANNED_JOURNAL_HISTORY](
	[JOURNALHISTORYID] [bigint] IDENTITY(1,1) NOT NULL,
	[JOURNALID] [int] NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[COMPONENTID] [smallint] NULL,
	[STATUSID] [smallint] NULL,
	[ACTIONID] [int] NULL,
	[CREATIONDATE] [smalldatetime] NULL,
	[CREATEDBY] [int] NULL,
	[NOTES] [nvarchar](1000) NULL,
	[ISLETTERATTACHED] [bit] NULL,
	[StatusHistoryId] [int] NULL,
	[ActionHistoryId] [int] NULL,
	[IsDocumentAttached] [bit] NULL,
 CONSTRAINT [PK_PLANNED_JOURNAL_HISTORYid] PRIMARY KEY CLUSTERED 
(
	[JOURNALHISTORYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK__PLANNED_J__COMPO__3085A9A6] FOREIGN KEY([COMPONENTID])
REFERENCES [dbo].[PLANNED_COMPONENT] ([COMPONENTID])


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY] CHECK CONSTRAINT [FK__PLANNED_J__COMPO__3085A9A6]


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_ACTION_ActionId] FOREIGN KEY([ACTIONID])
REFERENCES [dbo].[PLANNED_Action] ([ActionId])


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY] CHECK CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_ACTION_ActionId]


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_ACTIONHISTORY_ActionHistoryId] FOREIGN KEY([ActionHistoryId])
REFERENCES [dbo].[PLANNED_ActionHistory] ([ActionHistoryId])


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY] CHECK CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_ACTIONHISTORY_ActionHistoryId]


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_JOURNAL] FOREIGN KEY([JOURNALID])
REFERENCES [dbo].[PLANNED_JOURNAL] ([JOURNALID])


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY] CHECK CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_JOURNAL]


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_STATUS_StatusId] FOREIGN KEY([STATUSID])
REFERENCES [dbo].[PLANNED_STATUS] ([STATUSID])


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY] CHECK CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_STATUS_StatusId]


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_STATUSHISTORY_StatusHistoryId] FOREIGN KEY([StatusHistoryId])
REFERENCES [dbo].[PLANNED_StatusHistory] ([StatusHistoryId])


ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY] CHECK CONSTRAINT [FK_PLANNED_JOURNAL_HISTORY_PLANNED_STATUSHISTORY_StatusHistoryId]


END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SchemeId'
      AND Object_ID = Object_ID(N'PLANNED_JOURNAL_HISTORY'))
BEGIN
    ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]
	ADD [SchemeId] int NULL
	
	PRINT 'Added Column SchemeId'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'BlockId'
      AND Object_ID = Object_ID(N'PLANNED_JOURNAL_HISTORY'))
BEGIN
    ALTER TABLE [dbo].[PLANNED_JOURNAL_HISTORY]
	ADD [BlockId] int NULL
	
	PRINT 'Added Column BlockId'
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH



