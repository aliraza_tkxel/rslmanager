CREATE TABLE [dbo].[AM_ActivityAndStandardLetters]
(
[ActivityAndStandardLettersId] [int] NOT NULL IDENTITY(1, 1),
[ActivityId] [int] NOT NULL,
[StandardLettersId] [int] NOT NULL,
[StandardLetterHistoryId] [int] NULL,
[MarketRent] [float] NULL,
[RentBalance] [float] NULL,
[CreatedDate] [datetime] NULL,
[RentAmount] [float] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_ActivityAndStandardLetters] ADD 
CONSTRAINT [PK_AM_ActivityAndStandardLetters] PRIMARY KEY CLUSTERED  ([ActivityAndStandardLettersId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_ActivityAndStandardLetters] ADD
CONSTRAINT [Relation_ActivityId] FOREIGN KEY ([ActivityId]) REFERENCES [dbo].[AM_Activity] ([ActivityId])
ALTER TABLE [dbo].[AM_ActivityAndStandardLetters] ADD
CONSTRAINT [Relation_Activity_StadardLetterHistoryId] FOREIGN KEY ([StandardLetterHistoryId]) REFERENCES [dbo].[AM_StandardLetterHistory] ([StandardLetterHistoryId])
ALTER TABLE [dbo].[AM_ActivityAndStandardLetters] ADD
CONSTRAINT [Relation_StandardLetterId] FOREIGN KEY ([StandardLettersId]) REFERENCES [dbo].[AM_StandardLetters] ([StandardLetterId])
GO
