--Script to Add a new Column : Category of Type Varchar for Property Documents in Prop > Search > Documents Tab .
USE [RSLBHALive]
	GO
	
	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO

	SET ANSI_PADDING ON
	GO


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PDR_DOCUMENTS')
BEGIN
	

	CREATE TABLE [dbo].[PDR_DOCUMENTS]
	(
		[DocumentID] [int] IDENTITY(1,1) NOT NULL,
		[Title] [varchar](50) NOT NULL,
		[Expires] [smalldatetime] NOT NULL,
		[DocumentPath] [varchar](500) NOT NULL,
		[AppointmentTypeId] [int] NOT NULL,
		[FileType] [varchar](50) NOT NULL,
		[CreatedBy] [int] NOT NULL,
		[DocumentTypeId] [int] NULL,
		[DocumentDate] [datetime] NULL,
		[UploadedDate] [datetime] NULL,
		CONSTRAINT [PK_PDR_DOCUMENTS] PRIMARY KEY CLUSTERED 
		(
			[DocumentID] ASC
		)	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) 	ON [PRIMARY]

	  PRINT 'Table Created';
 END
 
 

IF COL_LENGTH('PDR_DOCUMENTS', 'DocumentName') IS NULL
	BEGIN
			ALTER TABLE PDR_DOCUMENTS
			ADD DocumentName Varchar(100) NULL
	END

GO

IF COL_LENGTH('PDR_DOCUMENTS', 'Category') IS NULL
	BEGIN
			ALTER TABLE PDR_DOCUMENTS
			ADD Category Varchar(15) NULL
	END

GO	
--Update Existing document's category to Compliance.

UPDATE	dbo.PDR_DOCUMENTS 
SET Category = 'Compliance'
Where	Category IS NULL

GO	
