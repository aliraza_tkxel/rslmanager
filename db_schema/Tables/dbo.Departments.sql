CREATE TABLE [dbo].[Departments]
(
[deptid] [int] NOT NULL,
[deptname] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[deptmgrid] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Departments] ADD CONSTRAINT [PK__Departme__BE2C1AEE69DE2293] PRIMARY KEY CLUSTERED  ([deptid]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Departments] ADD CONSTRAINT [FK__Departmen__deptm__6BC66B05] FOREIGN KEY ([deptmgrid]) REFERENCES [dbo].[Employees] ([empid])
GO
