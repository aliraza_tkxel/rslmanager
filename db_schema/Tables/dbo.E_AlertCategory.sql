CREATE TABLE [dbo].[E_AlertCategory]
(
[AlertCategoryID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[E_AlertCategory] ADD CONSTRAINT [PK_E_AlertCategory] PRIMARY KEY NONCLUSTERED  ([AlertCategoryID]) WITH FILLFACTOR=30 ON [PRIMARY]
GO

--SELECT * FROM  dbo.E_ALERTS ORDER BY AlertName

INSERT INTO [E_AlertCategory] (DESCRIPTION) SELECT 'Purchasing'
INSERT INTO [E_AlertCategory] (DESCRIPTION) SELECT 'My Team'
INSERT INTO [E_AlertCategory] (DESCRIPTION) SELECT 'Customers'
INSERT INTO [E_AlertCategory] (DESCRIPTION) SELECT 'Property'

GO

UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 1
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 22
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 4
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 23
--Purchasing:
--xx Goods for Approval
--� Invoices for Approval
--� Queued Purchases
--� Queued Repair POs

UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 3
--UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 2
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 2
--My Team:
--� Absentees
--� Declaration of Interest: Pending Approval (******* DOESNT EXIST ********)
--� Leave Request

UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 8
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 17
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 28
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 5
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 6
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 16
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 9
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 18
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 11
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 13
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 7
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 12
--Customers:
--� Adaptation Enquiries
--� Asb
--� ASB Open Cases
--� General Enquiries
--� Rent Enquiries
--� Risk Reviews
--� Service Complaints
--� Tenant Support Referrals
--� Tenants Online Enquiries
--� Tenants Responses
--� Transfer /Exchange
--� Vulnerability Reviews

UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 20
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 24
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 19
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 26
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 15
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 14
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 27
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 6
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 10
UPDATE dbo.E_ALERTS SET categoryid = 1 WHERE AlertID = 25
--Property:
--� Approved Condition Works
--� Available Properties
--� Condition Works Approval Required
--� Defects Requiring Approval
--� Gas Void Notifications
--� Post Inspection
--� Pre Inspection
--� Rejected Faults
--� Repairs Awaiting Approval
--� Void Inspections To Be Arranged


UPDATE dbo.E_ALERTS SET categoryid = 2 WHERE AlertID = 30
UPDATE dbo.E_ALERTS SET categoryid = 2 WHERE AlertID = 33
UPDATE dbo.E_ALERTS SET categoryid = 4 WHERE AlertID = 29

UPDATE dbo.E_ALERTS SET categoryid = 2 WHERE AlertID = 34
UPDATE dbo.E_ALERTS SET categoryid = 2 WHERE AlertID = 32
UPDATE dbo.E_ALERTS SET categoryid = 2 WHERE AlertID = 31