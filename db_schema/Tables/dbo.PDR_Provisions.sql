USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PDR_Provisions]    Script Date: 08/29/2016 14:23:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_NAME = N'PDR_Provisions'
		)
BEGIN
	CREATE TABLE [dbo].[PDR_Provisions] (
		[ProvisionId] [int] IDENTITY(1, 1) NOT NULL
		,[ProvisionName] [nvarchar](100) NOT NULL
		,[ProvisionParentId] [int] NULL
		,[Manufacturer] [nvarchar](100) NOT NULL
		,[Model] [nvarchar](50) NOT NULL
		,[SerialNumber] [nvarchar](100) NOT NULL
		,[InstalledDate] [smalldatetime] NOT NULL
		,[InstallationCost] [decimal](18, 0) NOT NULL
		,[LifeSpan] [int] NOT NULL
		,[CycleId] [int] NULL
		,[ReplacementDue] [smalldatetime] NOT NULL
		,[ConditionRating] [int] NOT NULL
		,[IsActive] [bit] NOT NULL
		,[SchemeId] [int] NULL
		,[BlockId] [int] NULL
		,[LastReplaced] [smalldatetime] NOT NULL
		,[UpdatedBy] [int] NULL
		,CONSTRAINT [PK_PDR_Provisions] PRIMARY KEY CLUSTERED ([ProvisionId] ASC) WITH (
			PAD_INDEX = OFF
			,STATISTICS_NORECOMPUTE = OFF
			,IGNORE_DUP_KEY = OFF
			,ALLOW_ROW_LOCKS = ON
			,ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].[PDR_Provisions]
		WITH CHECK ADD CONSTRAINT [FK_PDR_Provisions_PDR_Provisions] FOREIGN KEY ([CycleId]) REFERENCES [dbo].[PDR_CycleType]([CycleTypeId])

	ALTER TABLE [dbo].[PDR_Provisions] CHECK CONSTRAINT [FK_PDR_Provisions_PDR_Provisions]
END

BEGIN TRANSACTION

BEGIN TRY
	--- Added new columns
	IF COL_LENGTH('PDR_Provisions', 'ProvisionDescription') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ProvisionDescription] [nvarchar] (100) NULL

		PRINT ('COLUMN ProvisionDescription Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ProvisionType') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ProvisionType] [int] NULL

		PRINT ('COLUMN ProvisionType Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ProvisionCategoryId') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ProvisionCategoryId] [int] NULL

		PRINT ('COLUMN ProvisionCategoryId Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'Quantity') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [Quantity] [int] NULL

		PRINT ('COLUMN Quantity Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ProvisionLocation') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ProvisionLocation] [nvarchar] (100) NULL

		PRINT ('COLUMN ProvisionLocation Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'SupplierId') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [SupplierId] [int] NULL

		PRINT ('COLUMN SupplierId Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'BHG') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [BHG] [nvarchar] (100) NULL

		PRINT ('COLUMN BHG Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'GasAppliance') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [GasAppliance] [bit] NULL

		PRINT ('COLUMN GasAppliance Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'Sluice') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [Sluice] [bit] NULL

		PRINT ('COLUMN Sluice Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'PurchasedDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [PurchasedDate] [smalldatetime] NULL

		PRINT ('COLUMN PurchasedDate Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'PurchasedCost') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [PurchasedCost] [decimal] (
			18
			,0
			) NULL

		PRINT ('COLUMN PurchasedCost Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'Warranty') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [Warranty] BIT NULL

		PRINT ('COLUMN Warranty Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'Owned') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [Owned] BIT NULL

		PRINT ('COLUMN Owned Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'LeaseExpiryDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [LeaseExpiryDate] [smalldatetime] NULL

		PRINT ('COLUMN LeaseExpiryDate Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'LeaseCost') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [LeaseCost] [decimal] (
			18
			,0
			) NULL

		PRINT ('COLUMN LeaseCost Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ProvisionCharge') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ProvisionCharge] BIT NULL

		PRINT ('COLUMN ProvisionCharge Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'AnnualApportionmentPC') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [AnnualApportionmentPC] [nvarchar] (100) NULL

		PRINT ('COLUMN AnnualApportionmentPC Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'BlockPC') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [BlockPC] [int] NULL

		PRINT ('COLUMN BlockPC Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'PATTesting') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [PATTesting] BIT NULL

		PRINT ('COLUMN PATTesting Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ServicingRequired') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ServicingRequired] BIT NULL

		PRINT ('COLUMN ServicingRequired Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ServicingProviderId') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ServicingProviderId] [int] NULL

		PRINT ('COLUMN ServicingProviderId Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'DuctServicing') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [DuctServicing] [nvarchar] (100) NULL

		PRINT ('COLUMN DuctServicing Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'lastDuctServicingDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [lastDuctServicingDate] [smalldatetime] NULL

		PRINT ('COLUMN lastDuctServicingDate Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'nextDuctServicingDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [nextDuctServicingDate] [smalldatetime] NULL

		PRINT ('COLUMN nextDuctServicingDate Added.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ductServiceCycle') IS NULL
	BEGIN
		ALTER TABLE PDR_Provisions ADD [ductServiceCycle] [nvarchar] (100) NULL

		PRINT ('COLUMN ductServiceCycle Added.')
	END

	--- Updated Column Types
	IF COL_LENGTH('PDR_Provisions', 'ReplacementDue') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provisions

		ALTER COLUMN [ReplacementDue] [smalldatetime] NULL

		PRINT ('Update ReplacementDue to nullable.')
	END

	IF COL_LENGTH('PDR_Provisions', 'LastReplaced') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provisions

		ALTER COLUMN [LastReplaced] [smalldatetime] NULL

		PRINT ('Update LastReplaced to nullable.')
	END

	IF COL_LENGTH('PDR_Provisions', 'ConditionRating') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provisions

		ALTER COLUMN [ConditionRating] [int] NULL

		PRINT ('Update ConditionRating to nullable.')
	END

	IF COL_LENGTH('PDR_Provisions', 'GasAppliance') IS NOT NULL
	BEGIN
		Alter Table PDR_Provisions 
		Alter COLUMN  GasAppliance bit null
		PRINT ('Update GasAppliance removed.')
	END

	--- Drop Columns
	IF COL_LENGTH('PDR_Provisions', 'ProvisionName') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provisions

		DROP COLUMN [ProvisionName]

		PRINT ('Drop Coulmn ProvisionName.')
	END
	
	IF COL_LENGTH('PDR_Provisions', 'ProvisionParentId') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provisions

		DROP COLUMN [ProvisionParentId]

		PRINT ('Drop Coulmn ProvisionParentId.')
	END

	IF COL_LENGTH('PDR_Provisions', 'Manufacturer') IS NOT NULL
	BEGIN
		Alter Table PDR_Provisions Drop COLUMN  Manufacturer
		PRINT ('Update Manufacturer removed.')
	END

	IF COL_LENGTH('PDR_Provisions', 'InstalledDate') IS NOT NULL
	BEGIN
		Alter Table PDR_Provisions Drop COLUMN  InstalledDate
		PRINT ('Update InstalledDate removed.')
	END

	IF COL_LENGTH('PDR_Provisions', 'InstallationCost') IS NOT NULL
	BEGIN
		Alter Table PDR_Provisions Drop COLUMN  InstallationCost
		PRINT ('Update InstallationCost removed.')
	END  

	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
	END

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE()
		,@ErrorSeverity = ERROR_SEVERITY()
		,@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (
			@ErrorMessage
			,@ErrorSeverity
			,@ErrorState
			);

	PRINT (@ErrorMessage)
END CATCH