CREATE TABLE [dbo].[F_SALESLEDGERBALANCE_DAILY]
(
[SALESLEDGERBALANCEDAILYID] [int] NOT NULL IDENTITY(1, 1),
[TENANCYID] [int] NULL,
[EMPLOYEEID] [int] NULL,
[SUPPLIERID] [int] NULL,
[SALESCUSTOMERID] [int] NULL,
[BALANCE] [money] NOT NULL,
[CREATIONDATE] [smalldatetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[F_SALESLEDGERBALANCE_DAILY] ADD 
CONSTRAINT [PK__F_SALESLEDGERBAL__770C69D1] PRIMARY KEY CLUSTERED  ([SALESLEDGERBALANCEDAILYID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
