USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_WorkingHours_History] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_WorkingHours_History')
BEGIN
CREATE TABLE [dbo].[E_WorkingHours_History]
(
[Wid] [int] NOT NULL IDENTITY(1, 1),
[EmployeeId] [int] NOT NULL,
[StartDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[Mon] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Mon] DEFAULT ((0.0)),
[Tue] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Tue] DEFAULT ((0.0)),
[Wed] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Wed] DEFAULT ((0.0)),
[Thu] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Thu] DEFAULT ((0.0)),
[Fri] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Fri] DEFAULT ((0.0)),
[Sat] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Sat] DEFAULT ((0.0)),
[Sun] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Sun] DEFAULT ((0.0)),
[Total] [float] NULL CONSTRAINT [DF_E_WorkingHours_History_Total] DEFAULT ((0.0)),
[CreatedBy] [int] NULL,
[ctimestamp] [smalldatetime] NULL CONSTRAINT [DF__E_Working__ctime__73FBBE53] DEFAULT (getdate())
) ON [PRIMARY]
ALTER TABLE [dbo].[E_WorkingHours_History] ADD 
CONSTRAINT [PK_E_WorkingHours_History] PRIMARY KEY CLUSTERED  ([Wid]) WITH (FILLFACTOR=100) ON [PRIMARY]
END
ELSE
BEGIN
PRINT 'Table already exist..'
END

IF COL_LENGTH('E_WorkingHours_History', 'GroupNumber') IS NULL
BEGIN
ALTER TABLE E_WorkingHours_History 
ADD GroupNumber int NULL DEFAULT ((0))
Print('GroupNumber added successfully  ') 
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH