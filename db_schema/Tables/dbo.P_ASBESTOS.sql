USE RSLBHALive
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (	SELECT	*  
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'P_ASBESTOS')
BEGIN

	CREATE TABLE [dbo].[P_ASBESTOS](
		[ASBESTOSID] [int] IDENTITY(3386,1) NOT NULL,
		[RISKDESCRIPTION] [varchar](200) NULL,
		[other] [bit] NULL,
		[sorder] [int] NULL,
	 CONSTRAINT [PK_P_ASBESTOS] PRIMARY KEY CLUSTERED 
	(
		[ASBESTOSID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]


END



	UPDATE P_ASBESTOS SET SORDER = 1 WHERE RISKDESCRIPTION = 'Pitched roof felt'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated pitched roof felt')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated pitched roof felt',0,2)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 2 WHERE RISKDESCRIPTION = 'Encapsulated pitched roof felt'
	END

	UPDATE P_ASBESTOS SET SORDER = 3 WHERE RISKDESCRIPTION = 'Flat roof felt'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated flat roof felt')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated flat roof felt',0,4)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 4 WHERE RISKDESCRIPTION = 'Encapsulated flat roof felt'
	END

	UPDATE P_ASBESTOS SET SORDER = 5 WHERE RISKDESCRIPTION = 'Asbestos cement roof sheets'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated asbestos cement roof sheets')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated asbestos cement roof sheets',0,6)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 6 WHERE RISKDESCRIPTION = 'Encapsulated asbestos cement roof sheets'
	END


	UPDATE P_ASBESTOS SET SORDER = 7 WHERE RISKDESCRIPTION = 'Cladding'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated cladding')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated cladding',0,8)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 8 WHERE RISKDESCRIPTION = 'Encapsulated cladding'
	END

	UPDATE P_ASBESTOS SET SORDER = 9 WHERE RISKDESCRIPTION = 'Eaves Soffits'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated eaves soffits')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated eaves soffits',0,10)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 10 WHERE RISKDESCRIPTION = 'Encapsulated eaves soffits'
	END

	UPDATE P_ASBESTOS SET SORDER = 11 WHERE RISKDESCRIPTION = 'Floor tiles'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated floor tiles')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated floor tiles',0,12)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 12 WHERE RISKDESCRIPTION = 'Encapsulated floor tiles'
	END

	UPDATE P_ASBESTOS SET SORDER = 13 WHERE RISKDESCRIPTION = 'Boiler flues'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated boiler flues')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated boiler flues',0,14)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 14 WHERE RISKDESCRIPTION = 'Encapsulated boiler flues'
	END


	UPDATE P_ASBESTOS SET SORDER = 15 WHERE RISKDESCRIPTION = 'Linings to elec n/s gas heating appliances'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated linings to elec n/s gas heating appliances')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated linings to elec n/s gas heating appliances',0,16)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 16 WHERE RISKDESCRIPTION = 'Encapsulated linings to elec n/s gas heating appliances'
	END


	UPDATE P_ASBESTOS SET SORDER = 17 WHERE RISKDESCRIPTION = 'Lining to heating appliance cupboards'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated lining to heating appliance cupboards')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated lining to heating appliance cupboards',0,18)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 18 WHERE RISKDESCRIPTION = 'Encapsulated lining to heating appliance cupboards'
	END


	UPDATE P_ASBESTOS SET SORDER = 19 WHERE RISKDESCRIPTION = 'Internal ductwork boxing in'
		IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated internal ductwork boxing in')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated internal ductwork boxing in',0,20)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 20 WHERE RISKDESCRIPTION = 'Encapsulated internal ductwork boxing in'
	END


	UPDATE P_ASBESTOS SET SORDER = 21 WHERE RISKDESCRIPTION = 'Stair cladding'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated stair cladding')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated stair cladding',0,22)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 22 WHERE RISKDESCRIPTION = 'Encapsulated stair cladding'
	END

	UPDATE P_ASBESTOS SET SORDER = 23 WHERE RISKDESCRIPTION = 'Fire doors'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated fire doors')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated fire doors',0,24)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 24 WHERE RISKDESCRIPTION = 'Encapsulated fire doors'
	END

	UPDATE P_ASBESTOS SET SORDER = 25 WHERE RISKDESCRIPTION = 'Comm boiler room'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated comm boiler room')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated comm boiler room',0,26)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 26 WHERE RISKDESCRIPTION = 'Encapsulated comm boiler room'
	END


	UPDATE P_ASBESTOS SET SORDER = 27 WHERE RISKDESCRIPTION = 'Wall inspection panels'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated wall inspection panels')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated wall inspection panels',0,28)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 28 WHERE RISKDESCRIPTION = 'Encapsulated wall inspection panels'
	END


	UPDATE P_ASBESTOS SET SORDER = 29 WHERE RISKDESCRIPTION = 'Textured Coating'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated textured coating')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated textured coating',0,30)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 30 WHERE RISKDESCRIPTION = 'Encapsulated textured coating'
	END


	UPDATE P_ASBESTOS SET SORDER = 31 WHERE RISKDESCRIPTION = 'Toilet Cistern'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated toilet cistern')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated toilet cistern',0,32)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 32 WHERE RISKDESCRIPTION = 'Encapsulated toilet cistern'
	END


	UPDATE P_ASBESTOS SET SORDER = 33 WHERE RISKDESCRIPTION = 'Bitumen Pad'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated bitumen pad')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated bitumen pad',0,34)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 34 WHERE RISKDESCRIPTION = 'Encapsulated bitumen pad'
	END

	UPDATE P_ASBESTOS SET SORDER = 35 WHERE RISKDESCRIPTION = 'Water Tank'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated water tank')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated water tank',0,36)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 36 WHERE RISKDESCRIPTION = 'Encapsulated water tank'
	END

	UPDATE P_ASBESTOS SET SORDER = 37 WHERE RISKDESCRIPTION = 'None'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated none')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated none',0,38)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 38 WHERE RISKDESCRIPTION = 'Encapsulated none'
	END

	UPDATE P_ASBESTOS SET SORDER = 39 WHERE RISKDESCRIPTION = 'Other'
	IF NOT EXISTS(SELECT 1 FROM P_ASBESTOS WHERE RISKDESCRIPTION = 'Encapsulated other')
	BEGIN		
		INSERT INTO [dbo].[P_ASBESTOS]([RISKDESCRIPTION],[other],[sorder])
		VALUES	('Encapsulated other',0,40)
	END
	ELSE
	BEGIN
		UPDATE P_ASBESTOS SET SORDER = 40 WHERE RISKDESCRIPTION = 'Encapsulated other'
	END


IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH


