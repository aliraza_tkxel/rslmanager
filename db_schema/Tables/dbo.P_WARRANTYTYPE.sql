USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'P_WARRANTYTYPE')
BEGIN


	CREATE TABLE [dbo].[P_WARRANTYTYPE](
		[WARRANTYTYPEID] [int] IDENTITY(1,1) NOT NULL,
		[DESCRIPTION] [nvarchar](100) NULL,
	 CONSTRAINT [PK_P_WARRANTYTYPE] PRIMARY KEY NONCLUSTERED 
	(
		[WARRANTYTYPEID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 30) ON [PRIMARY]
	) ON [PRIMARY]


END


	--===============================================================================
	-- US578 - INSERT warranty type 'Restriction'
	--===============================================================================

	IF NOT EXISTS (	SELECT	1 
					FROM	P_WARRANTYTYPE 
					WHERE	[DESCRIPTION] ='NHBC Cover')
		BEGIN  

			INSERT INTO [dbo].[P_WARRANTYTYPE] ([DESCRIPTION])
			VALUES ('NHBC Cover')

		END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH