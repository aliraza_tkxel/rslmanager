USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

/****** Object:  Table [dbo].[C_ADDRESS]    Script Date: 12/15/2016 17:33:18 ******/


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_CustomerCurrentHome')
	BEGIN


CREATE TABLE dbo.FLS_CustomerCurrentHome
	(
	CurrentHomeId int NOT NULL IDENTITY (1, 1),
	CustomerId int NOT NULL,
	LandlordTypeId int NULL,
	LandlordName nvarchar(500) NULL,
	HouseTypeId int NULL,
	floor INT NULL,
	LivingWithFamilyId int NULL,
	NoOfBedrooms int NULL,
	TenancyStartDate date NULL,
	TenancyEndDate date NULL,
	PreviousLandLordName nvarchar(500) NULL
	)  ON [PRIMARY]

ALTER TABLE dbo.FLS_CustomerCurrentHome ADD CONSTRAINT
	PK_FLS_CustomerCurrentHome PRIMARY KEY CLUSTERED 
	(
	CurrentHomeId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


ALTER TABLE dbo.FLS_CustomerCurrentHome SET (LOCK_ESCALATION = TABLE)
END

IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH