CREATE TABLE [dbo].[TBL_PDA_DECENTHOMES]
(
[ID] [bigint] NOT NULL,
[SDID] [int] NOT NULL,
[ItemID] [int] NOT NULL,
[CriterionValueID] [int] NOT NULL,
[Remarks] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_DECENTHOMES] ADD 
CONSTRAINT [PK_TBL_PDA_DECENTHOMES] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_DECENTHOMES] WITH NOCHECK ADD
CONSTRAINT [FK_TBL_PDA_DECENTHOMES_TBL_PDA_ITEM] FOREIGN KEY ([ItemID]) REFERENCES [dbo].[TBL_PDA_ITEM] ([ItemID]) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE [dbo].[TBL_PDA_DECENTHOMES] ADD
CONSTRAINT [FK_TBL_PDA_DECENTHOMES_TBL_PDA_CRITERIONVALUE] FOREIGN KEY ([CriterionValueID]) REFERENCES [dbo].[TBL_PDA_CRITERIONVALUE] ([ValueID]) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE [dbo].[TBL_PDA_DECENTHOMES] ADD
CONSTRAINT [FK_TBL_PDA_DECENTHOMES_TBL_PDA_SURVEY_DETAIL] FOREIGN KEY ([SDID]) REFERENCES [dbo].[TBL_PDA_SURVEY_DETAIL] ([SDID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
