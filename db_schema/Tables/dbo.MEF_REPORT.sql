CREATE TABLE [dbo].[MEF_REPORT]
(
[MEFREPORTID] [int] NOT NULL IDENTITY(1, 1),
[EMPLOYEEID] [int] NULL,
[YRANGE] [int] NULL,
[MRANGE] [int] NULL,
[MEFREPORT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MEREPORT_XLS] [varbinary] (max) NULL,
[UPLOADDATE] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[MEF_REPORT] ADD 
CONSTRAINT [PK_MEF_REPORT] PRIMARY KEY CLUSTERED  ([MEFREPORTID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[MEF_REPORT] ADD
CONSTRAINT [F_MEF_REPORT_E__EMPLOYEE] FOREIGN KEY ([EMPLOYEEID]) REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])
GO


ALTER TABLE [dbo].[MEF_REPORT] ADD CONSTRAINT [F_MEF_REPORT_F_FISCALYEARS] FOREIGN KEY ([YRANGE]) REFERENCES [dbo].[F_FISCALYEARS] ([YRange])
GO
