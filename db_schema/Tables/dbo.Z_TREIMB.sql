CREATE TABLE [dbo].[Z_TREIMB]
(
[CHEQUEDATE] [smalldatetime] NULL,
[CHEQUENUM] [float] NULL,
[Amount] [float] NULL,
[tenancyid] [float] NULL,
[customerid] [float] NULL,
[Expenditure] [float] NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
