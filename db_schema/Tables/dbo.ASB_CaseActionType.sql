USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_CaseActionType')
BEGIN
CREATE TABLE [dbo].[ASB_CaseActionType]
(
	[ActionTypeId] [int] IDENTITY(1,1)PRIMARY KEY NOT NULL,
	[Description] [nvarchar](100)
) 
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Close Case')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Conversation')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Email')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Follow Up')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Letter')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Meeting')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Telephone')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'TXT')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Visit')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Complainant Added')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Complainant Deleted')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Perpetrator Added')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Perpetrator Deleted')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Document Added')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Document Deleted')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Photograph Added')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Photograph Deleted')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Associated Contact Added')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Associated Contact Deleted')
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Stages')

PRINT 'Table Created'
END

ELSE
BEGIN
IF NOT EXISTS(select * from [ASB_CaseActionType] WHERE [Description]='Stages')
BEGIN
INSERT [dbo].[ASB_CaseActionType] ([Description]) VALUES ( N'Stages')
PRINT 'Stages Action Type added'
END
PRINT 'Table already exist..'
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH