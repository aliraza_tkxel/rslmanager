CREATE TABLE [dbo].[KPI_TOPIC]
(
[TOPICID] [int] NOT NULL IDENTITY(1, 1),
[CATEGORYID] [int] NULL,
[TOPIC] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIVE] [int] NULL,
[FRAMENAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISCONTINUED] [int] NULL
) ON [PRIMARY]
GO
