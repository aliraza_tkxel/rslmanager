CREATE TABLE [dbo].[C_ADDRESS_HISTORICAL]
(
[HISTORICALADDRESSID] [int] NOT NULL IDENTITY(1, 1),
[ADDRESSID] [int] NOT NULL,
[CUSTOMERID] [int] NULL,
[HOUSENUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS3] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TOWNCITY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTCODE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TEL] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MOBILE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMAIL] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISDEFAULT] [int] NULL,
[ADDRESSTYPE] [int] NULL,
[ISPOWEROFATTORNEY] [int] NULL,
[UPDATEDBYZ] [int] NULL,
[CONTACTFIRSTNAME] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTACTSURNAME] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELATIONSHIP] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTACTDOB] [smalldatetime] NULL,
[TELWORK] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELRELATIVE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELRELATIONSHIP] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MOBILE2] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESSUPDATED] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[C_ADDRESS_HISTORICAL] ADD 
CONSTRAINT [PK_C_ADDRESS_HISTORICAL_1] PRIMARY KEY CLUSTERED  ([HISTORICALADDRESSID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

EXEC sp_addextendedproperty N'MS_Description', N'This stores the first mobile number MOBILE1', 'SCHEMA', N'dbo', 'TABLE', N'C_ADDRESS_HISTORICAL', 'COLUMN', N'MOBILE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This stores the home telephone number could not rename the column because it is being used in other procedures (TELHOME)', 'SCHEMA', N'dbo', 'TABLE', N'C_ADDRESS_HISTORICAL', 'COLUMN', N'TEL'
GO
