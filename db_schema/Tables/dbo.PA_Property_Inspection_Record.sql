USE RSLBHALive
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PA_Property_Inspection_Record')
BEGIN

		CREATE TABLE [dbo].[PA_Property_Inspection_Record]
		(
		[InspectionId] [int] NOT NULL IDENTITY(1, 1),
		[PropertyId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SurveyId] [int] NULL,
		[InspectionDocument] [image] NULL,
		[inspectionDate] [datetime] NULL,
		[createdBy] [int] NULL,
		[CreatedDate] [datetime] NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
ELSE
BEGIN
	Update PA_Property_Inspection_Record set inspectionDate = CreatedDate where inspectionDate ='1970-01-01 00:00:00.000'
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState );
	Print (@ErrorMessage)
END CATCH
