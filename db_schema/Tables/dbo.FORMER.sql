CREATE TABLE [dbo].[FORMER]
(
[NAME] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CF] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TENANCYID] [int] NULL,
[MAIN] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECOND] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FORMER] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BALANCE] [money] NULL
) ON [PRIMARY]
GO
