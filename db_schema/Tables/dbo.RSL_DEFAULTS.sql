USE RSLBHALive

BEGIN TRANSACTION
	BEGIN TRY

		IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'RSL_DEFAULTS')
			BEGIN

				CREATE TABLE [dbo].[RSL_DEFAULTS]
				(
				[DEFAULTID] [int] NOT NULL IDENTITY(1, 1),
				[DEFAULTNAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
				[DEFAULTVALUE] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
				[DESCRIPTION] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
				) ON [PRIMARY]
		
				ALTER TABLE [dbo].[RSL_DEFAULTS] ADD CONSTRAINT [PK_RSL_DEFAULTS] PRIMARY KEY NONCLUSTERED  ([DEFAULTID]) WITH FILLFACTOR=30 ON [PRIMARY]

			END
		ELSE

			BEGIN
			PRINT 'Table Already Exist';
			END

		--========================================================================================================
		--======================================================================================================== 
    
		IF (Exists(SELECT 1 FROM RSL_DEFAULTS WHERE DEFAULTID = 55) )
			BEGIN
				UPDATE RSL_DEFAULTS
				SET DEFAULTVALUE = 'E:\Inetpub\sysdata\bha.rslmanager'
				WHERE  DEFAULTID = 55
			END

		--========================================================================================================
		--======================================================================================================== 
    
		IF @@TRANCOUNT > 0
			BEGIN
				COMMIT TRANSACTION;
			END
	END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
	DECLARE @ErrorMessage nvarchar(4000);
	DECLARE @ErrorSeverity int;
	DECLARE @ErrorState int;

	SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,
	@ErrorSeverity,
	@ErrorState
	);
	PRINT (@ErrorMessage)
END CATCH    
