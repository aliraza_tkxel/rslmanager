CREATE TABLE [dbo].[PS_Appointment2Survey]
(
[Appoint2SurveyId] [int] NOT NULL IDENTITY(1, 1),
[AppointId] [int] NOT NULL,
[SurveyId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Appointment2Survey] ADD 
CONSTRAINT [PK_PS_Appointment2Survey] PRIMARY KEY CLUSTERED  ([Appoint2SurveyId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Appointment2Survey] ADD
CONSTRAINT [FK_PS_Appointment2Survey_PS_Appointment] FOREIGN KEY ([AppointId]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
ALTER TABLE [dbo].[PS_Appointment2Survey] ADD
CONSTRAINT [FK_PS_Appointment2Survey_PS_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[PS_Survey] ([SurveyId])
GO
