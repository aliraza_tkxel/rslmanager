CREATE TABLE [dbo].[C_ASB_CATEGORY_BY_GRADE]
(
[CategoryId] [smallint] NOT NULL,
[GradeId] [smallint] NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
