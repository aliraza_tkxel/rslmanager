USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_WorkingHours]    Script Date: 24-Jul-17 4:02:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_WorkingHours')
BEGIN
CREATE TABLE [dbo].[E_WorkingHours](
	[Wid] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Mon] [float] NULL CONSTRAINT [DF_E_WorkingHours_Mon]  DEFAULT (0.0),
	[Tue] [float] NULL CONSTRAINT [DF_E_WorkingHours_Tue]  DEFAULT (0.0),
	[Wed] [float] NULL CONSTRAINT [DF_E_WorkingHours_Wed]  DEFAULT (0.0),
	[Thu] [float] NULL CONSTRAINT [DF_E_WorkingHours_Thu]  DEFAULT (0.0),
	[Fri] [float] NULL CONSTRAINT [DF_E_WorkingHours_Fri]  DEFAULT (0.0),
	[Sat] [float] NULL CONSTRAINT [DF_E_WorkingHours_Sat]  DEFAULT (0.0),
	[Sun] [float] NULL CONSTRAINT [DF_E_WorkingHours_Sun]  DEFAULT (0.0),
	[Total] [float] NULL CONSTRAINT [DF_E_WorkingHours_Total]  DEFAULT (0.0),
	[STARTDATE] [smalldatetime] NULL,
	[CREATEDBY] [int] NULL,
	[CREATEDON] [smalldatetime] NULL DEFAULT (getdate()),
	[LASTACTIONTIME] [datetime] NULL CONSTRAINT [DF_E_WorkingHours_LASTACTIONTIME]  DEFAULT (getdate()),
	[LASTACTIONUSER] [int] NULL,
 CONSTRAINT [PK_E_WorkingHours] PRIMARY KEY CLUSTERED 
(
	[Wid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

END
ELSE
BEGIN
PRINT 'Table already exist..'
END

IF COL_LENGTH('E_WorkingHours', 'GroupNumber') IS NULL
BEGIN
ALTER TABLE E_WorkingHours 
ADD GroupNumber int NULL DEFAULT (0)
Print('GroupNumber added successfully  ') 
END

IF COL_LENGTH('E_WorkingHours', 'DaysTimings') IS NULL
BEGIN
ALTER TABLE E_WorkingHours 
ADD DaysTimings varchar(MAX) NULL DEFAULT ''
Print('DaysTimings added successfully  ') 
END

if exists(SELECT  * 
		  FROM DBO.E_WORKINGHOURS
		  group BY EmployeeId
		  having count(*) > 1)
BEGIN
	;WITH cte AS
	(
	   SELECT *,
			 ROW_NUMBER() OVER (PARTITION BY employeeid ORDER BY wid) AS rn
	   FROM E_WorkingHours
	   where EmployeeId in (SELECT  EmployeeId 
							FROM DBO.E_WORKINGHOURS
							group BY EmployeeId
							having count(*) > 1)
	)

	Delete from E_WorkingHours
	WHERE Wid IN (SELECT wid
				  FROM E_WorkingHours where EmployeeId in (SELECT  EmployeeId 
														  FROM DBO.E_WORKINGHOURS
														  group BY EmployeeId
														  having count(*) > 1) and wid not in (SELECT wid
																							  FROM cte
																							  WHERE rn = 1)
				 )
	Print 'Deleted Corrupt data for Employee Working Hours'
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


