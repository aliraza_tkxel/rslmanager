USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_DrivingLicence')
BEGIN

CREATE TABLE dbo.E_DrivingLicence
	(
	DrivingLicenceId int NOT NULL IDENTITY (1, 1),
	EmployeeId int NULL,
	LicenceImage nvarchar(500) NULL,
	DrivingLicenceType nvarchar(50) NULL,
	DrivingLicenceNumber nvarchar(100) NULL,
	DrivingLicenceIssueNumber nvarchar(50) NULL,
	ExpiryDate datetime NULL,
	MedicalHistory nvarchar(500) NULL,
	CreatedBy int NULL,
	CreatedDate datetime NULL
	)  ON [PRIMARY]

ALTER TABLE dbo.E_DrivingLicence ADD CONSTRAINT
	PK_E_DrivingLicence PRIMARY KEY CLUSTERED 
	(
	DrivingLicenceId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = N'E_DrivingLicence' and COLUMN_NAME = N'DrivingLicenceIssueNumber' and DATA_TYPE = N'int')
BEGIN
	ALTER TABLE E_DrivingLicence
	ALTER COLUMN DrivingLicenceIssueNumber nvarchar(50);
	print ('column type updated')
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




