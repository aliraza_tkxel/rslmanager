CREATE TABLE [dbo].[h_support_units_backup2]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[JOB_ID] [int] NULL,
[DEBIT] [money] NULL,
[CREDIT] [money] NULL,
[MONETARY_VALUE] [money] NULL,
[COMMENTS] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SENDEMAIL] [int] NULL,
[USER_ID] [int] NULL,
[TIMESTAMP] [datetime] NULL,
[ORG_ID] [int] NULL,
[PURCHASE_NO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USER_AUTH_ID] [int] NULL,
[AUTH_DATE] [datetime] NULL,
[PENDING] [int] NULL
) ON [PRIMARY]
GO
