CREATE TABLE [dbo].[AM_Schedule_Job_Log]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ScheduleJobType] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartTime] [datetime] NULL,
[EndTime] [datetime] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_Schedule_Job_Log] ADD 
CONSTRAINT [PK_AM_Schedule_Job_Log] PRIMARY KEY CLUSTERED  ([Id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
