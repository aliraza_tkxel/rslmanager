CREATE TABLE [dbo].[F_HEAD]
(
[HEADID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HEADDATE] [smalldatetime] NULL,
[USERID] [int] NOT NULL,
[ACTIVE_Z] [bit] NULL,
[LASTMODIFIED] [smalldatetime] NOT NULL,
[HEADALLOCATION_Z] [money] NULL,
[COSTCENTREID] [int] NULL
) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_476580786_1_2] ON [dbo].[F_HEAD] ([HEADID], [DESCRIPTION])

CREATE UNIQUE CLUSTERED INDEX [IX_HEAD_HEAD_ID] ON [dbo].[F_HEAD] ([HEADID]) WITH (FILLFACTOR=90) ON [PRIMARY]

GO
CREATE STATISTICS [_dta_stat_476580786_1_8] ON [dbo].[F_HEAD] ([HEADID], [COSTCENTREID])

GO
