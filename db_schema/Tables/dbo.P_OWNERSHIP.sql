CREATE TABLE [dbo].[P_OWNERSHIP]
(
[OwnershipId] [smallint] NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nrosh_code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nrosh_Description] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_OWNERSHIP] TO [rackspace_datareader]
GO
