CREATE TABLE [dbo].[P_RMTOREPAIR]
(
[RMTOREPAIR] [int] NOT NULL IDENTITY(1, 1),
[RUNID] [int] NOT NULL,
[JOURNALID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[P_RMTOREPAIR] ADD CONSTRAINT [PK_P_RMTOREPAIR] PRIMARY KEY NONCLUSTERED  ([RMTOREPAIR]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[P_RMTOREPAIR] WITH NOCHECK ADD CONSTRAINT [FK_P_RMTOREPAIR_C_JOURNAL] FOREIGN KEY ([JOURNALID]) REFERENCES [dbo].[C_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[P_RMTOREPAIR] ADD CONSTRAINT [FK_P_RMTOREPAIR_P_RUNNINGMAINTENANCE] FOREIGN KEY ([RUNID]) REFERENCES [dbo].[P_RUNNINGMAINTENANCE] ([RUNID])
GO
GRANT SELECT ON  [dbo].[P_RMTOREPAIR] TO [rackspace_datareader]
GO
