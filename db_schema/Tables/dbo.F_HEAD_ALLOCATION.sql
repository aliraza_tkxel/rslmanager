USE RSLBHALive
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'F_HEAD_ALLOCATION')      
BEGIN
	PRINT 'Table Exists';
	DECLARE @CostCentreId INT
	DECLARE @MaxFiscalYearId INT
	DECLARE @MaxFiscalYearIdInAllocationTable INT
	SELECT @CostCentreId=COSTCENTREID FROM F_COSTCENTRE WHERE DESCRIPTION ='Services'
	SELECT @MaxFiscalYearId= MAX(YRange)From F_FISCALYEARS 
	
	Select @MaxFiscalYearIdInAllocationTable= Max(FISCALYEAR) 
	FROM F_HEAD_ALLOCATION
	INNER JOIN F_HEAD ON F_HEAD.HEADID = F_HEAD_ALLOCATION.HEADID
	WHERE COSTCENTREID=@CostCentreId 
	
	DECLARE @HeadId INT
	DECLARE @HeadAllocation INT
	DECLARE cur CURSOR FOR  
	
	Select F_HEAD_ALLOCATION. HEADID,
	F_HEAD_ALLOCATION.HEADALLOCATION
	FROM F_HEAD_ALLOCATION
	INNER JOIN F_HEAD ON F_HEAD.HEADID = F_HEAD_ALLOCATION.HEADID
	Where COSTCENTREID=@CostCentreId AND FISCALYEAR=@MaxFiscalYearIdInAllocationTable
	
	OPEN cur
	FETCH NEXT FROM cur 
	INTO @HeadId,@HeadAllocation	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
	IF NOT EXISTS (SELECT 1 FROM F_HEAD_ALLOCATION WHERE FISCALYEAR=@MaxFiscalYearId AND HEADID =@HeadId)
	BEGIN
		INSERT INTO F_HEAD_ALLOCATION( HEADID, FISCALYEAR, MODIFIED, 
		MODIFIEDBY , HEADALLOCATION , ACTIVE)
		VALUES
		(@HeadId,@MaxFiscalYearId,GETDATE(),113,@HeadAllocation,1)
		PRINT 'Record added'
	END
	
	
	
	FETCH NEXT FROM cur 
	INTO @HeadId,@HeadAllocation
	END
	CLOSE cur;
	DEALLOCATE cur;	
		
END --if
ELSE
PRINT 'Table not Exists ';


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_HEAD_ALLOCATION'))
BEGIN
alter table F_HEAD_ALLOCATION add CompanyId INT
END
GO










