USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'C_JOURNAL')
BEGIN
	CREATE TABLE [dbo].[C_JOURNAL](
		[JOURNALID] [int] IDENTITY(1,1) NOT NULL,
		[CUSTOMERID] [int] NULL,
		[TENANCYID] [int] NULL,
		[PROPERTYID] [nvarchar](20) NULL,
		[ITEMID] [int] NULL,
		[ITEMNATUREID] [int] NULL,
		[CURRENTITEMSTATUSID] [int] NULL,
		[LETTERACTION] [int] NULL,
		[CREATIONDATE] [smalldatetime] NULL CONSTRAINT [DF_C_JOURNAL_CREATIONDATE]  DEFAULT (getdate()),
		[TITLE] [nvarchar](1000) NULL,
		[LASTACTIONDATE] [smalldatetime] NULL,
		[NEXTACTIONDATE] [smalldatetime] NULL,
		[NATURESUBTYPEID] [int] NULL,
		[NATUREACTIVITYID] [int] NULL,
	 CONSTRAINT [PK_C_JOURNAL] PRIMARY KEY NONCLUSTERED 
	(
		[JOURNALID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

END

IF Not EXISTS (SELECT name FROM sys.indexes
WHERE name = N'IX_C_Journal_TenancyId')
BEGIN
	CREATE NONCLUSTERED INDEX IX_C_Journal_TenancyId
	ON C_JOURNAL (TENANCYID)
	INCLUDE (JOURNALID, PROPERTYID, CUSTOMERID);  
PRINT 'IX_C_Journal_TenancyId created successfully';
END
ELSE
BEGIN
	PRINT 'IX_C_Journal_TenancyId Index Already Exist';
END

IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH

