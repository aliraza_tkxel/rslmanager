Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'HR_DocumentType')
BEGIN

CREATE TABLE [dbo].[HR_DocumentType](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_HR_DocumentType] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END --if


IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Contract')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Contract',1)
	PRINT 'Contract added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Terms & Conditions')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Terms & Conditions',1)
	PRINT 'Terms & Conditions added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Appraisal')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Appraisal',1)
	PRINT 'Appraisal added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'References')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('References',1)
	PRINT 'References added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Correspondence')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Correspondence',1)
	PRINT 'Correspondence added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Medical')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Medical',1)
	PRINT 'Medical added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Qualifications')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Qualifications',1)
	PRINT 'Qualifications added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Motoring')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Motoring',1)
	PRINT 'Motoring added successfully'
END

IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Misc')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Misc',1)
	PRINT 'Misc added successfully'
END
IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'DOI')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('DOI',1)
	PRINT 'DOI added successfully'
END
IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Gifts & Hospitality')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Gifts & Hospitality',1)
	PRINT 'Gifts & Hospitality added successfully'
END
IF NOT EXISTS (SELECT * FROM HR_DocumentType WHERE Description = 'Corporate Compliance')
BEGIN
	insert into HR_DocumentType (Description,IsActive) VALUES('Corporate Compliance',1)
	PRINT 'Corporate Compliance added successfully'
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 


