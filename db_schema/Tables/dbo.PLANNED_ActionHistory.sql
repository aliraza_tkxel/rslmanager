CREATE TABLE [dbo].[PLANNED_ActionHistory]
(
[ActionHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ActionId] [int] NOT NULL,
[StatusId] [smallint] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[IsEditable] [bit] NULL,
[Ctimestamp] [smalldatetime] NULL CONSTRAINT [DF__PLANNED_A__Ctime__26081B33] DEFAULT (getdate())
) ON [PRIMARY]
ALTER TABLE [dbo].[PLANNED_ActionHistory] ADD 
CONSTRAINT [PK_PLANNED_ActionHistory] PRIMARY KEY CLUSTERED  ([ActionHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PLANNED_ActionHistory] ADD CONSTRAINT [FK_PLANNED_ACTIONHistory_PLANNED_STATUS] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[PLANNED_Action] ([ActionId])
GO
