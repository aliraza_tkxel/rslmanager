BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AC_FINANCIALLOCKDOWN')
BEGIN

	CREATE TABLE [dbo].[AC_FINANCIALLOCKDOWN]
	(
	[LOCKDOWNID] [int] NOT NULL,
	[Module] [int] NULL,
	[SubModuleDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PageID] [int] NULL,
	[ExtendedDays] [int] NULL,
	[Active] [int] NULL
	) ON [PRIMARY]

END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MonthYear' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN
	ALTER TABLE AC_FINANCIALLOCKDOWN ADD MonthYear NVARCHAR(100)
END
IF not EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ExtendedMonth' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN
	ALTER TABLE AC_FINANCIALLOCKDOWN ADD ExtendedMonth INT
END
IF not EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ExtendedYear' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN
	ALTER TABLE AC_FINANCIALLOCKDOWN ADD ExtendedYear INT
END
 IF not EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DeletedFlag' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN
	ALTER TABLE AC_FINANCIALLOCKDOWN ADD DeletedFlag BIT DEFAULT(0)
END 

 IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DeletedFlag' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN
	UPDATE AC_FINANCIALLOCKDOWN SET DeletedFlag = 0
	UPDATE AC_FINANCIALLOCKDOWN SET DeletedFlag = 1 WHERE Module = 7
	UPDATE AC_FINANCIALLOCKDOWN SET DeletedFlag = 1 WHERE Module = 9
	UPDATE AC_FINANCIALLOCKDOWN SET DeletedFlag = 1 WHERE LOCKDOWNID IN (3,5, 12)
	UPDATE AC_FINANCIALLOCKDOWN SET Module = 14 WHERE LOCKDOWNID IN (11,17) 

	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'Payment Upload' WHERE LOCKDOWNID = 6
	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'H/B Validation' WHERE LOCKDOWNID = 7
	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'Create Purchase' WHERE LOCKDOWNID = 8
	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'Credit Note' WHERE LOCKDOWNID = 9
	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'Reconcile Purchase' WHERE LOCKDOWNID = 10
	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'Miscellanous Income' WHERE LOCKDOWNID = 11
	UPDATE AC_FINANCIALLOCKDOWN SET Description = 'Misclellanous Payments' WHERE LOCKDOWNID = 17
END 

GO

IF (NOT EXISTS (SELECT * FROM dbo.AC_FINANCIALLOCKDOWN WHERE LOCKDOWNID = 18))
BEGIN
INSERT INTO AC_FINANCIALLOCKDOWN (LOCKDOWNID, Module, SubModuleDescription, Description, PageID, ExtendedDays,
Active,MonthYear,ExtendedMonth, ExtendedYear,DeletedFlag)
SELECT  18, 11, 'Sales', 'New Sales Invoice',1,5,1,'11-2017',11,2017,0

END
go
 IF not EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CompanyId' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN
	ALTER TABLE AC_FINANCIALLOCKDOWN ADD CompanyId INT
END 
go
 IF  EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CompanyId' AND Object_ID = Object_ID(N'AC_FINANCIALLOCKDOWN'))
BEGIN

	IF not EXISTS(SELECT 1 FROM AC_FINANCIALLOCKDOWN where companyid = 3)
	BEGIN

		UPDATE AC_FINANCIALLOCKDOWN SET Companyid = 1

 
		INSERT into AC_FINANCIALLOCKDOWN 
		(
		[LOCKDOWNID],
		[Module],[SubModuleDescription] ,[Description],[PageID] ,
		[ExtendedDays] ,[Active] ,[MonthYear],[ExtendedMonth] ,[ExtendedYear] ,[DeletedFlag] ,
		CompanyId)
		SELECT 
		[LOCKDOWNID],[Module],
		[SubModuleDescription] ,[Description],[PageID] ,[ExtendedDays] ,[Active] ,[MonthYear],[ExtendedMonth] ,[ExtendedYear] ,
		[DeletedFlag] ,
		2
		FROM 
		AC_FINANCIALLOCKDOWN WHERE companyid = 1

		INSERT into AC_FINANCIALLOCKDOWN 
		(
		[LOCKDOWNID],
		[Module],[SubModuleDescription] ,[Description],[PageID] ,
		[ExtendedDays] ,[Active] ,[MonthYear],[ExtendedMonth] ,[ExtendedYear] ,[DeletedFlag] ,
		CompanyId)
		SELECT 
		[LOCKDOWNID],[Module],
		[SubModuleDescription] ,[Description],[PageID] ,[ExtendedDays] ,[Active] ,[MonthYear],[ExtendedMonth] ,[ExtendedYear] ,
		[DeletedFlag] ,
		3
		FROM 
		AC_FINANCIALLOCKDOWN WHERE companyid = 1

	end
END


--=======================================================================
-- US596 - Added 'HB Upload' in validation module as it was the requirement 
-- to replicate values just like 'Payment Upload' and 'H/B Validation'
-- that is why hard coded values have been added
--=======================================================================


IF (NOT EXISTS (SELECT	* 
				FROM	dbo.AC_FINANCIALLOCKDOWN 
				WHERE	Description = 'HB Upload' AND CompanyId = 1))
BEGIN

	INSERT [dbo].[AC_FINANCIALLOCKDOWN] ([LOCKDOWNID], [Module], [SubModuleDescription]
	, [Description], [PageID], [ExtendedDays]
	, [Active], [MonthYear], [ExtendedMonth]
	, [ExtendedYear], [DeletedFlag], [CompanyId]) 
	VALUES (19, 11, N'Validation', N'HB Upload', 0, 7, 1, NULL, 2, 2019, 0, 1)

END

IF (NOT EXISTS (SELECT	* 
				FROM	dbo.AC_FINANCIALLOCKDOWN 
				WHERE	Description = 'HB Upload' AND CompanyId = 2))
BEGIN

	INSERT [dbo].[AC_FINANCIALLOCKDOWN] ([LOCKDOWNID], [Module], [SubModuleDescription]
	, [Description], [PageID], [ExtendedDays]
	, [Active], [MonthYear], [ExtendedMonth]
	, [ExtendedYear], [DeletedFlag], [CompanyId]) 
	VALUES (19, 11, N'Validation', N'HB Upload', 0, 7, 1, NULL, 2, 2019, 0, 2)

END

IF (NOT EXISTS (SELECT	* 
				FROM	dbo.AC_FINANCIALLOCKDOWN 
				WHERE	Description = 'HB Upload' AND CompanyId = 3))
BEGIN

	INSERT [dbo].[AC_FINANCIALLOCKDOWN] ([LOCKDOWNID], [Module], [SubModuleDescription]
	, [Description], [PageID], [ExtendedDays]
	, [Active], [MonthYear], [ExtendedMonth]
	, [ExtendedYear], [DeletedFlag], [CompanyId]) 
	VALUES (19, 11, N'Validation', N'HB Upload', 0, 7, 1, NULL, 2, 2019, 0, 3)

END


