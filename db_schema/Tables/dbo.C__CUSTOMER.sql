USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'C__CUSTOMER')
	BEGIN
	CREATE TABLE [dbo].[C__CUSTOMER](
	[CUSTOMERID] [int] IDENTITY(1,1) NOT NULL,
	[TITLE] [int] NULL,
	[TITLEOTHER] [nvarchar](50) NULL,
	[FIRSTNAME] [nvarchar](50) NULL,
	[MIDDLENAME] [nvarchar](70) NULL,
	[LASTNAME] [nvarchar](50) NULL,
	[CUSTOMERTYPE] [int] NULL,
	[GENDER] [nvarchar](20) NULL,
	[DOB] [smalldatetime] NULL,
	[MARITALSTATUS] [int] NULL,
	[ETHNICORIGIN] [int] NULL,
	[ETHNICORIGINOTHER] [nvarchar](300) NULL,
	[EMPLOYMENTSTATUS] [int] NULL,
	[NINUMBER] [nvarchar](20) NULL,
	[MOVINGTIMEFRAME] [int] NULL,
	[OCCUPATION] [nvarchar](100) NULL,
	[TENANCY] [nvarchar](50) NULL,
	[PROPERTY] [nvarchar](50) NULL,
	[HBREF] [nvarchar](50) NULL,
	[TTYPE] [int] NULL,
	[ACCOUNTTYPE] [nvarchar](50) NULL,
	[PAYMENTMETHOD] [int] NULL,
	[LA] [nvarchar](50) NULL,
	[ORGFLAG] [int] NULL,
	[temp_name1] [nvarchar](50) NULL,
	[temp_name2] [nvarchar](50) NULL,
	[TAKEHOMEPAY] [int] NULL,
	[RELIGION] [int] NULL,
	[RELIGIONOTHER] [nvarchar](50) NULL,
	[SEXUALORIENTATION] [int] NULL,
	[SEXUALORIENTATIONOTHER] [char](10) NULL,
	[FIRSTLANGUAGE] [nvarchar](80) NULL,
	[COMMUNICATION] [nvarchar](70) NULL,
	[COMMUNICATIONOTHER] [nvarchar](100) NULL,
	[PREFEREDCONTACT] [nvarchar](100) NULL,
	[DISABILITYOTHER] [nvarchar](100) NULL,
	[OCCUPANTSCHILDREN] [int] NULL,
	[OCCUPANTSADULTS] [int] NULL,
	[OTHERCOMMUNICATION] [int] NULL,
	[DISABILITY] [nvarchar](70) NULL,
	[CONSENT] [int] NULL,
	[LOCALAUTHORITY] [int] NULL,
	[INTERNETACCESS] [nvarchar](70) NULL,
	[RENTSTATEMENTTYPE] [int] NULL,
	[NATIONALITY] [int] NULL,
	[NATIONALITYOTHER] [nvarchar](100) NULL,
	[OWNTRANSPORT] [smallint] NULL,
	[WHEELCHAIR] [smallint] NULL,
	[CARER] [smallint] NULL,
	[INTERNETACCESSOTHER] [nvarchar](100) NULL,
	[SUPPORTAGENCIES] [nvarchar](70) NULL,
	[SUPPORTAGENCIESOTHER] [nvarchar](100) NULL,
	[BENEFIT] [nvarchar](70) NULL,
	[BANKFACILITY] [nvarchar](50) NULL,
	[BANKFACILITYOTHER] [nvarchar](100) NULL,
	[HOUSEINCOME] [smallint] NULL,
	[EMPLOYERNAME] [nvarchar](70) NULL,
	[EMPADDRESS1] [nvarchar](70) NULL,
	[EMPADDRESS2] [nvarchar](70) NULL,
	[EMPTOWNCITY] [nvarchar](30) NULL,
	[EMPPOSTCODE] [nvarchar](10) NULL,
	[TEXTYESNO] [smallint] NULL,
	[EMAILYESNO] [smallint] NULL,
	[LASTACTIONTYPE] [int] NULL,
	[LASTACTIONUSER] [int] NULL,
	[LASTACTIONTIME] [datetime] NULL,
	[BENEFITOTHER] [varchar](100) NULL,
 CONSTRAINT [PK_C__CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[CUSTOMERID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 60) ON [PRIMARY]
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [dbo].[C__CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_C__CUSTOMER_C_RENTSTATEMENTTYPE] FOREIGN KEY([RENTSTATEMENTTYPE])
REFERENCES [dbo].[C_RENTSTATEMENTTYPE] ([RENTSTATEMENTTYPEID])
ALTER TABLE [dbo].[C__CUSTOMER] CHECK CONSTRAINT [FK_C__CUSTOMER_C_RENTSTATEMENTTYPE]
ALTER TABLE [dbo].[C__CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_C__CUSTOMER_HISTORICAL_C_RENTSTATEMENTTYPE] FOREIGN KEY([RENTSTATEMENTTYPE])
REFERENCES [dbo].[C_RENTSTATEMENTTYPE] ([RENTSTATEMENTTYPEID])
ALTER TABLE [dbo].[C__CUSTOMER] CHECK CONSTRAINT [FK_C__CUSTOMER_HISTORICAL_C_RENTSTATEMENTTYPE]
ALTER TABLE [dbo].[C__CUSTOMER] ADD  CONSTRAINT [DF_C__CUSTOMER_ORGFLAG]  DEFAULT (0) FOR [ORGFLAG]
ALTER TABLE [dbo].[C__CUSTOMER] ADD  CONSTRAINT [DF_C__CUSTOMER_LASTACTIONTIME]  DEFAULT (getdate()) FOR [LASTACTIONTIME]

	End
ELSE
	BEGIN
	print 'C__Customer Table already Exist'
	END		
---=====================================================================================================================
---==================================Add new Columns-===================================================================
---=====================================================================================================================

IF COL_LENGTH('C__Customer', 'ProfilePicture') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD ProfilePicture NVARCHAR(1000) NULL
			PRINT('COLUMN ProfilePicture CREATED')
	END		


IF COL_LENGTH('C__Customer', 'SubjectToImmigration') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD SubjectToImmigration int NULL
			PRINT('COLUMN SubjectToImmigration CREATED')
	END	
IF COL_LENGTH('C__Customer', 'PermanentUKResidency') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD PermanentUKResidency int NULL
			PRINT('COLUMN PermanentUKResidency CREATED')
	END	

IF COL_LENGTH('C__Customer', 'AppVersion') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD AppVersion NVARCHAR(100) NULL
			PRINT('COLUMN AppVersion Added')
	END
	
IF COL_LENGTH('C__Customer', 'CreatedOnApp') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD CreatedOnApp SMALLDATETIME NULL
			PRINT('COLUMN CreatedOnApp Added')
	END
								
IF COL_LENGTH('C__Customer', 'CreatedOnServer') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD CreatedOnServer SMALLDATETIME NULL
			PRINT('COLUMN CreatedOnServer Added')
	END
		
IF COL_LENGTH('C__Customer', 'LastModifiedOnApp') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD LastModifiedOnApp SMALLDATETIME NULL
			PRINT('COLUMN LastModifiedOnApp Added')
	END		
IF COL_LENGTH('C__Customer', 'LastModifiedOnServer') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD LastModifiedOnServer SMALLDATETIME NULL
			PRINT('COLUMN LastModifiedOnServer Added')
	END	

IF COL_LENGTH('C__Customer', 'AidAdaptations') IS NULL
	BEGIN
			ALTER TABLE C__Customer
			ADD AidAdaptations NVARCHAR(100) NULL
			PRINT('COLUMN AidAdaptations Added')
	END	

if exists(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME = 'C__CUSTOMER' AND COLUMN_NAME = 'BENEFIT'
AND DATA_TYPE = 'nvarchar' AND CHARACTER_MAXIMUM_LENGTH = 70)
BEGIN
	ALTER TABLE [C__CUSTOMER]
	ALTER COLUMN [BENEFIT] NVARCHAR(500)
	pRINT 'Changed benefit datatype from nvarchar(70) to nvarchar(500)'
END


			
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH