CREATE TABLE [dbo].[AM_LookupCode]
(
[LookupCodeId] [int] NOT NULL IDENTITY(1, 1),
[LookupTypeId] [int] NOT NULL,
[CodeName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentId] [int] NULL,
[CreatedBy] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedBy] [int] NULL,
[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_LookupCode] ADD 
CONSTRAINT [PK_AM_LookupCode] PRIMARY KEY CLUSTERED  ([LookupCodeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_LookupCode] ADD CONSTRAINT [FK_AM_LookupCode_AM_LookupType] FOREIGN KEY ([LookupTypeId]) REFERENCES [dbo].[AM_LookupType] ([LookupTypeId])
GO
