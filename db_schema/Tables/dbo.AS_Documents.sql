CREATE TABLE [dbo].[AS_Documents]
(
[DocumentId] [int] NOT NULL IDENTITY(1, 1),
[DocumentName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JournalHistoryId] [int] NULL,
[CreatedDate] [datetime] NULL,
[ModifiedDate] [datetime] NULL,
[DocumentPath] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_Documents] ADD 
CONSTRAINT [PK_AS_Documents] PRIMARY KEY CLUSTERED  ([DocumentId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
