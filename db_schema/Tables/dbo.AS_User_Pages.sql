CREATE TABLE [dbo].[AS_User_Pages]
(
[UserPagesId] [int] NOT NULL IDENTITY(1, 1),
[PageId] [int] NOT NULL,
[EmployeeId] [int] NOT NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_User_Pages] ADD 
CONSTRAINT [PK_AS_User_Pages] PRIMARY KEY CLUSTERED  ([UserPagesId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AS_User_Pages] ADD
CONSTRAINT [FK_AS_User_Pages_AS_Pages] FOREIGN KEY ([PageId]) REFERENCES [dbo].[AS_Pages] ([PageId])
ALTER TABLE [dbo].[AS_User_Pages] ADD
CONSTRAINT [FK_AS_User_Pages_AS_User] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
