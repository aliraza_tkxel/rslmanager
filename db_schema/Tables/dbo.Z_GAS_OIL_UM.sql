CREATE TABLE [dbo].[Z_GAS_OIL_UM]
(
[F1] [float] NULL,
[Property Ref] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[House Number] [float] NULL,
[Address 1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address 2 ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Town] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F7] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F8] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[post code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Appliance Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F11] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F12] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F13] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Manufacturer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CP12 Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date of Issue] [datetime] NULL,
[Contractor for Warranty] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Warranty Expiry Parts] [float] NULL,
[Warranty Expiry Parts & Labour] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F21] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F22] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F23] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
