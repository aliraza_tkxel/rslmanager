CREATE TABLE [dbo].[z_careline]
(
[ORDERID] [int] NULL,
[WOID] [int] NULL,
[LOGDATE] [smalldatetime] NULL,
[LOGTIME] [money] NULL,
[REPAIRDATE] [smalldatetime] NULL,
[REPAIRTIME] [money] NULL
) ON [PRIMARY]
GO
