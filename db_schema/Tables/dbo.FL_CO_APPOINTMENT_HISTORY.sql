CREATE TABLE [dbo].[FL_CO_APPOINTMENT_HISTORY]
(
[AppointmentHistoryID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[AppointmentId] [int] NULL,
[JobSheetNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppointmentDate] [datetime] NULL,
[OperativeID] [int] NULL,
[LastActionDate] [datetime] NULL,
[Notes] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FaultLogId] [int] NULL,
[FaultLogHistoryId] [int] NULL,
[EndTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isCalendarAppointment] [bit] NULL,
[AppointmentEndDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_CO_APPOINTMENT_HISTORY] ADD 
CONSTRAINT [PK_FL_CO_APPOINTMENT_HISTORY] PRIMARY KEY CLUSTERED  ([AppointmentHistoryID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[FL_CO_APPOINTMENT_HISTORY] ADD
CONSTRAINT [FK_FL_CO_APPOINTMENT_HISTORY_FL_FAULT_LOG_HISTORY] FOREIGN KEY ([FaultLogHistoryId]) REFERENCES [dbo].[FL_FAULT_LOG_HISTORY] ([FaultLogHistoryID])
GO
