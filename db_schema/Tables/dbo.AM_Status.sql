CREATE TABLE [dbo].[AM_Status]
(
[StatusId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RentParameter] [int] NULL,
[RentParameterFrequencyLookupCodeId] [int] NULL,
[IsRentParameter] [bit] NULL,
[RentAmount] [float] NULL,
[IsRentAmount] [bit] NULL,
[Ranking] [int] NOT NULL,
[ReviewPeriod] [int] NOT NULL,
[ReviewPeriodFrequencyLookupCodeId] [int] NOT NULL,
[NextStatusAlert] [int] NOT NULL,
[NextStatusAlertFrequencyLookupCodeId] [int] NOT NULL,
[NextStatusDetail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNoticeParameterRequired] [bit] NULL,
[IsRecoveryAmount] [bit] NULL,
[IsNoticeIssueDate] [bit] NULL,
[IsNoticeExpiryDate] [bit] NULL,
[IsDocumentUpload] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedBy] [int] NOT NULL,
[ModifiedDate] [datetime] NULL,
[IsHearingDate] [bit] NULL,
[IsWarrantExpiryDate] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_Status] ADD 
CONSTRAINT [PK_AM_Status] PRIMARY KEY CLUSTERED  ([StatusId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_Status] ADD CONSTRAINT [FK_AM_Status_AM_Resource] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_Status] ADD CONSTRAINT [FK_AM_Status_AM_Resource1] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_Status] ADD CONSTRAINT [FK_AM_Status_AM_LookupCode2] FOREIGN KEY ([NextStatusAlertFrequencyLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_Status] ADD CONSTRAINT [FK_AM_Status_AM_LookupCode1] FOREIGN KEY ([ReviewPeriodFrequencyLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
