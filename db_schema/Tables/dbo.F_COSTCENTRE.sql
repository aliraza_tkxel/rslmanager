BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_COSTCENTRE')
BEGIN


CREATE TABLE [dbo].[F_COSTCENTRE]
(
[COSTCENTREID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DEPARTMENT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACCBODY] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DATESTART] [smalldatetime] NOT NULL,
[DATEEND] [smalldatetime] NOT NULL,
[COSTCENTREALLOCATION_Z] [money] NULL,
[ACTIVE_Z] [bit] NULL,
[USERID] [int] NULL,
[LASTMODIFIED] [smalldatetime] NULL,
[APPROVEDBY] [int] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[F_COSTCENTRE] ADD CONSTRAINT [PK_F_FUND] PRIMARY KEY NONCLUSTERED  ([COSTCENTREID]) WITH FILLFACTOR=90 ON [PRIMARY]



END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


 
IF not EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_COSTCENTRE'))
BEGIN

alter table F_COSTCENTRE add CompanyId INT

END

go

IF not EXISTS (SELECT 1 FROM dbo.F_COSTCENTRE WHERE CompanyId IS NOT NULL)
BEGIN

	update  dbo.F_COSTCENTRE set CompanyId = 1 where CompanyId is null

end