Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


DECLARE db_cursor CURSOR FOR 
SELECT DISTINCT P.PROPERTYID,PROP_ATTRIB.VALUEID
	FROM P__PROPERTY P
	INNER JOIN PA_PROPERTY_ATTRIBUTES PROP_ATTRIB ON PROP_ATTRIB.PROPERTYID = P.PROPERTYID	AND
								PROP_ATTRIB.ITEMPARAMID =
								(
									SELECT
										ItemParamID
									FROM
										PA_ITEM_PARAMETER
											INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
											INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
									WHERE
										ParameterName = 'Heating Fuel'
										AND ItemName = 'Heating'
										AND PARAMETERVALUE = 'Mains Gas'
								)

 
DECLARE @propertyId VARCHAR(20);
DECLARE @valueId INT;
DECLARE @heatingMappingId int;
declare @itemid int;
SELECT @itemid=PA_ITEM.ItemID from PA_ITEM where ItemName = 'Heating'

OPEN db_cursor;
FETCH NEXT FROM db_cursor INTO @propertyId, @valueId;
WHILE @@FETCH_STATUS = 0  
BEGIN  
	
--==========Insert into PA_HeatingMapping 
	
	  INSERT INTO PA_HeatingMapping(HeatingType,PropertyId,IsActive)VALUES(@valueId,@propertyId,1)

      SET @heatingMappingId  = SCOPE_IDENTITY()
--==========Update heating mapping id in PA_Property_Attributes
	UPDATE PA_PROPERTY_ATTRIBUTES   SET HeatingMappingId =@heatingMappingId
	Where PA_PROPERTY_ATTRIBUTES.PROPERTYID = @propertyId
	AND PA_PROPERTY_ATTRIBUTES.ITEMPARAMID IN (Select PA_ITEM_PARAMETER.ItemParamID from PA_PARAMETER
		INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
		where PA_ITEM_PARAMETER.ItemId=@itemid AND
		PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 
		AND PA_ITEM_PARAMETER.ParameterValueId = (Select PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where ValueDetail = 'Mains Gas' and PA_PARAMETER_VALUE.ParameterId =
		(Select ParameterId from PA_PARAMETER where PA_PARAMETER.ParameterName IN ('Heating Fuel'))
		 )
		 OR  PA_ITEM_PARAMETER. ParameterId=( Select ParameterId from PA_PARAMETER where PA_PARAMETER.ParameterName IN ('Heating Fuel'))
		 
		)
--==========Update heating mapping id in PA_PROPERTY_ITEM_DATES
	Update PA_PROPERTY_ITEM_DATES SET HeatingMappingId=@heatingMappingId where PA_PROPERTY_ITEM_DATES.PROPERTYID=@propertyId
	AND PA_PROPERTY_ITEM_DATES.ItemId=@itemid
	
--==========Update heating mapping id in AS_Journal
Update AS_JOURNAL SET HeatingMappingId = @heatingMappingId where AS_JOURNAL.PROPERTYID=@propertyId and AS_JOURNAL.ISCURRENT=1
Update AS_JOURNALHISTORY set HeatingMappingId=@heatingMappingId where AS_JOURNALHISTORY.PROPERTYID =@propertyId
	
--==========Update heating mapping id in P_LGSR
	Update P_LGSR SET HeatingMappingId= @heatingMappingId where P_LGSR.PROPERTYID=@propertyId 


Update PS_Survey_Parameters set HeatingMappingId=@heatingMappingId
where SID IN (Select SID from PS_Survey_Parameters
INNER JOIN PS_Survey on PS_Survey.SurveyId = PS_Survey_Parameters.SurveyId
Where PROPERTYID = @propertyId AND
	 PS_Survey_Parameters.ITEMPARAMID IN (Select PA_ITEM_PARAMETER.ItemParamID from PA_PARAMETER
		INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
		where PA_ITEM_PARAMETER.ItemId=@itemid AND
		PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 
		AND PA_ITEM_PARAMETER.ParameterValueId = (Select PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where ValueDetail = 'Mains Gas' and PA_PARAMETER_VALUE.ParameterId =
		(Select ParameterId from PA_PARAMETER where PA_PARAMETER.ParameterName IN ('Heating Fuel'))
		 )
		 OR  PA_ITEM_PARAMETER. ParameterId=( Select ParameterId from PA_PARAMETER where PA_PARAMETER.ParameterName IN ('Heating Fuel'))
		
		))

Update PS_Survey_Item_Dates set HeatingMappingId=@heatingMappingId
Where PS_Survey_Item_Dates.ItemId=@itemid and PS_Survey_Item_Dates.SurveyId IN (Select top 1 PS_Survey.SurveyId from PS_Survey where PS_Survey.PROPERTYID=@propertyId ORDER By PS_Survey.SurveyId DESC)

Update PA_PROPERTY_ITEM_NOTES Set HeatingMappingId=@heatingMappingId
Where PA_PROPERTY_ITEM_NOTES.PROPERTYID=@propertyId and PA_PROPERTY_ITEM_NOTES.ItemId=@itemid


Update PA_PROPERTY_ITEM_IMAGES set HeatingMappingId=@heatingMappingId
Where PA_PROPERTY_ITEM_IMAGES.PROPERTYID=@propertyId and PA_PROPERTY_ITEM_IMAGES.ItemId=@itemid


Update PS_Survey_Item_Notes SET HeatingMappingId=@heatingMappingId
Where PS_Survey_Item_Notes.ItemId=@itemid And PS_Survey_Item_Notes.SurveyId IN (Select top 1 PS_Survey.SurveyId from PS_Survey where PS_Survey.PROPERTYID=@propertyId ORDER By PS_Survey.SurveyId DESC)

      FETCH NEXT FROM db_cursor INTO @propertyId, @valueId;
END;
CLOSE db_cursor;
DEALLOCATE db_cursor;


--IF NOT EXISTS(SELECT * from AS_JournalHeatingMapping)
--BEGIN
--INSERT INTO AS_JournalHeatingMapping(JournalId,HeatingMappingId,JournalHistoryId,Createdon)
--	SELECT AS_JOURNAL.JOURNALID, MIN(AS_JOURNAL.HeatingMappingId) AS HeatingMappingId, MAX(AS_JOURNALHISTORY.JOURNALHISTORYID) AS JOURNALHISTORYID,GETDATE()
--	FROM AS_JOURNAL
--	INNER JOIN AS_JOURNALHISTORY ON AS_JOURNAL.JOURNALID = AS_JOURNALHISTORY.JOURNALID
--	Where AS_JOURNAL.HeatingMappingId IS NOT NULL
--	GROUP BY AS_JOURNAL.JOURNALID
--END


IF @@TRANCOUNT > 0
	BEGIN  
	print'commit'   
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
