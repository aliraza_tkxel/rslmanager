CREATE TABLE [dbo].[PA_PropertyDimension]
(
[PropertyDimId] [int] NOT NULL IDENTITY(1, 1),
[PropertyId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RoomName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomWidth] [float] NULL,
[RoomLength] [float] NULL,
[RoomHeight] [float] NULL,
[UpdatedBy] [int] NULL,
[UpdatedOn] [smalldatetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PA_PropertyDimension] ADD 
CONSTRAINT [PK_PA_AccomodationDetail] PRIMARY KEY CLUSTERED  ([PropertyDimId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
