Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_LGSR_Certificates')
BEGIN

CREATE TABLE [dbo].[P_LGSR_Certificates](
	[CertificateId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[HeatingType] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_P_LGSR_Certificates] PRIMARY KEY CLUSTERED 
(
	[CertificateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

IF NOT EXISTS(SELECT * FROM [P_LGSR_Certificates])
BEGIN
SET IDENTITY_INSERT [dbo].[P_LGSR_Certificates] ON 

INSERT [dbo].[P_LGSR_Certificates] ([CertificateId], [Title], [HeatingType], [IsActive]) VALUES (1, N'CP12', 172, 1)

INSERT [dbo].[P_LGSR_Certificates] ([CertificateId], [Title], [HeatingType], [IsActive]) VALUES (2, N'NA', 853, 1)

INSERT [dbo].[P_LGSR_Certificates] ([CertificateId], [Title], [HeatingType], [IsActive]) VALUES (3, N'NA', (Select top 1 ValueID from PA_PARAMETER_VALUE where ParameterID=65 and ValueDetail='Oil'), 1)

INSERT [dbo].[P_LGSR_Certificates] ([CertificateId], [Title], [HeatingType], [IsActive]) VALUES (4, N'NA', (Select top 1 ValueID from PA_PARAMETER_VALUE where ParameterID=65 and ValueDetail='MVHR & Heat Recovery'), 1)

INSERT [dbo].[P_LGSR_Certificates] ([CertificateId], [Title], [HeatingType], [IsActive]) VALUES (5, N'NA', (Select top 1 ValueID from PA_PARAMETER_VALUE where ParameterID=65 and ValueDetail='Unvented cylinder'), 1)

INSERT [dbo].[P_LGSR_Certificates] ([CertificateId], [Title], [HeatingType], [IsActive]) VALUES (6, N'NA', (Select top 1 ValueID from PA_PARAMETER_VALUE where ParameterID=65 and ValueDetail='PV'), 1)

SET IDENTITY_INSERT [dbo].[P_LGSR_Certificates] OFF

END

IF EXISTS(SELECT 1 FROM P_LGSR_Certificates WHERE HeatingType = ( SELECT ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail = 'MVHR & Heat Recovery') AND IsActive = 1)
 BEGIN
	UPDATE P_LGSR_Certificates
	SET IsActive = 0
	WHERE HeatingType = ( SELECT ValueID FROM PA_PARAMETER_VALUE WHERE ValueDetail = 'MVHR & Heat Recovery')
 PRINT 'Successfully set certificate "MVHR & Heat Recovery" active state to false.'
End

IF EXISTS (SELECT 1
		FROM P_LGSR_Certificates plgsr
		INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
		WHERE pv.ValueDetail = 'Ground Source'
			AND plgsr.Title = 'NA')
BEGIN
	UPDATE P_LGSR_Certificates
	SET Title = 'CP13'
	WHERE CertificateId = (
			SELECT plgsr.CertificateId
			FROM P_LGSR_Certificates plgsr
			INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
			WHERE pv.ValueDetail = 'Ground Source'
				AND plgsr.Title = 'NA')

	PRINT 'Added Title for Ground Source".'
END

IF EXISTS (SELECT 1
		FROM P_LGSR_Certificates plgsr
		INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
		WHERE pv.ValueDetail = 'Solar'
			AND plgsr.Title = 'NA')
BEGIN
	UPDATE P_LGSR_Certificates
	SET Title = 'CP11'
	WHERE CertificateId = (
			SELECT plgsr.CertificateId
			FROM P_LGSR_Certificates plgsr
			INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
			WHERE pv.ValueDetail = 'Solar'
				AND plgsr.Title = 'NA')

	PRINT 'Added Title for Solar".'
END

IF EXISTS (SELECT 1
		FROM P_LGSR_Certificates plgsr
		INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
		WHERE pv.ValueDetail = 'Unvented Cylinder'
			AND plgsr.Title = 'NA')
BEGIN
	UPDATE P_LGSR_Certificates
	SET Title = 'CP8'
	WHERE CertificateId = (
			SELECT plgsr.CertificateId
			FROM P_LGSR_Certificates plgsr
			INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
			WHERE pv.ValueDetail = 'Unvented Cylinder'
				AND plgsr.Title = 'NA')

	PRINT 'Added Title for Unvented Cylinder".'
END

IF EXISTS (SELECT 1
		FROM P_LGSR_Certificates plgsr
		INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
		WHERE pv.ValueDetail = 'PV'
			AND plgsr.Title = 'NA')
BEGIN
	UPDATE P_LGSR_Certificates
	SET Title = 'PVIR'
	WHERE CertificateId = (
			SELECT plgsr.CertificateId
			FROM P_LGSR_Certificates plgsr
			INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
			WHERE pv.ValueDetail = 'PV'
				AND plgsr.Title = 'NA')

	PRINT 'Added Title for PV".'
END

IF EXISTS (SELECT 1
		FROM P_LGSR_Certificates plgsr
		INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
		WHERE pv.ValueDetail = 'Oil'
			AND plgsr.Title = 'NA')
BEGIN
	UPDATE P_LGSR_Certificates
	SET Title = 'CD/11'
	WHERE CertificateId = (
			SELECT plgsr.CertificateId
			FROM P_LGSR_Certificates plgsr
			INNER JOIN PA_PARAMETER_VALUE pv ON pv.ValueID = plgsr.HeatingType
			WHERE pv.ValueDetail = 'Oil'
				AND plgsr.Title = 'NA')

	PRINT 'Added Title for Oil".'
END

IF NOT EXISTS( SELECT 1 FROM P_LGSR_Certificates WHERE HeatingType = -1)
 BEGIN
  INSERT [dbo].[P_LGSR_Certificates] ([Title], [HeatingType], [IsActive]) VALUES ('N/A', -1 , 1)
 PRINT 'Added N/A for "MVHR & Heat Recovery", "Electric", "Mains Electric", "Air Source", "Wet Electric" and "Biomass".'
End

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 