CREATE TABLE [dbo].[MAJOR]
(
[MajorID] [int] NOT NULL IDENTITY(1, 1),
[Location] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Element] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Repair Details] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Priority] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Net Cost] [float] NULL
) ON [PRIMARY]
GO
