
USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					   WHERE TABLE_NAME = N'PA_ITEM_PARAMETER')
	BEGIN
		CREATE TABLE [dbo].[PA_ITEM_PARAMETER](
			[ItemParamID] [int] IDENTITY(1,1) NOT NULL,
			[ItemId] [int] NULL,
			[ParameterId] [int] NULL,
			[IsActive] [bit] NULL CONSTRAINT [DF__PA_ITEM_P__IsAct__4F151DBE]  DEFAULT ((1)),
			[SchemeId] [int] NULL,
			[BlockId] [int] NULL,
		 CONSTRAINT [PK_PA_ITEM_PARAMETER] PRIMARY KEY CLUSTERED 
		(
			[ItemParamID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
		) ON [PRIMARY]

		 

		ALTER TABLE [dbo].[PA_ITEM_PARAMETER]  WITH NOCHECK ADD  CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_ITEM] FOREIGN KEY([ItemId])
		REFERENCES [dbo].[PA_ITEM] ([ItemID])
		 

		ALTER TABLE [dbo].[PA_ITEM_PARAMETER] CHECK CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_ITEM]
		 

		ALTER TABLE [dbo].[PA_ITEM_PARAMETER]  WITH NOCHECK ADD  CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_PARAMETER] FOREIGN KEY([ParameterId])
		REFERENCES [dbo].[PA_PARAMETER] ([ParameterID])
		 

		ALTER TABLE [dbo].[PA_ITEM_PARAMETER] CHECK CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_PARAMETER]
		 

	END
	IF EXISTS (	SELECT	1 FROM	PA_ITEM_PARAMETER WHERE ParameterId = 59)
		BEGIN 	
			update PA_ITEM_PARAMETER 
			set isActive = 0
			where ParameterId =59
			Print('DISABLING SMOKE DETECTOR TYPE ') 
		END

	IF EXISTS (	SELECT	1 FROM	PA_ITEM_PARAMETER WHERE ParameterId = 60)
		BEGIN 	
			update PA_ITEM_PARAMETER 
			set isActive = 0
			where ParameterId =60
			Print('DISABLING NUMBER OF SMOKE DETECTOR') 
		END


	--==============================================================================
	-- Ticket # ID79 (Added 'CUR Last Replaced' parameter in 'Electrics' item)
	--==============================================================================	

	IF EXISTS (	SELECT	1 
			FROM	PA_PARAMETER 
			WHERE	ParameterName='CUR Last Replaced')
		BEGIN 	
	
			DECLARE @ElectricsItemId INT
			DECLARE @CurLastReplacedParameterId INT

			SELECT	@ElectricsItemId = itemid 
			FROM	PA_ITEM 
			WHERE	ITEMNAME = 'Electrics' AND ISFORSCHEMEBLOCK = 0

			SELECT	@CurLastReplacedParameterId = parameterid 
			FROM	PA_PARAMETER 
			WHERE	ParameterName='CUR Last Replaced'
	

			IF NOT EXISTS (	SELECT	1
							FROM	PA_ITEM_PARAMETER
							WHERE	itemid = @ElectricsItemId
									AND parameterid = @CurLastReplacedParameterId)
				BEGIN 		
					INSERT INTO [dbo].[PA_ITEM_PARAMETER]
					   ([ItemId], [ParameterId], [IsActive], [SchemeId], [BlockId])
					VALUES
					   (@ElectricsItemId,@CurLastReplacedParameterId,1,null,null)

					PRINT 'CUR Last Replaced parameter successfully added in PA_ITEM_PARAMETER.'
				END
			ELSE
				BEGIN
					PRINT 'CUR Last Replaced parameter already exist in PA_ITEM_PARAMETER.'
				END

		END
	ELSE
		BEGIN
			PRINT 'CUR Last Replaced does not exist in PA_PATAMETER table, please execute dbo.PA_PATAMETER.sql first.'
		END


	--==============================================================================
	-- Fixed bug of US437
	--==============================================================================
	IF EXISTS(	SELECT * 
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE 
						TABLE_NAME = 'PA_ITEM_PARAMETER' AND 
						COLUMN_NAME = 'ParameterValueId' AND  DATA_TYPE = 'INT')
	BEGIN
			ALTER TABLE PA_ITEM_PARAMETER
			ALTER COLUMN ParameterValueId BIGINT NULL

			PRINT 'ParameterValueId COLUMN UPDATED OF PA_ITEM_PARAMETER '
	END




IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
GO

