USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_EmployeeTrainings_History')
BEGIN

	CREATE TABLE [dbo].[E_EmployeeTrainings_History](
		[TrainingHistoryId] [int] IDENTITY(1,1) NOT NULL,
		[TrainingId] [int]   NOT NULL,
		[EmployeeId] [int] NULL,
		[Justification] [nvarchar](500) NULL,
		[Course] [nvarchar](200) NULL,
		[StartDate] [smalldatetime] NULL,
		[EndDate] [smalldatetime] NULL,
		[TotalNumberOfDays] [int] NULL,
		[ProviderName] [nvarchar](200) NULL,
		[ProviderWebsite] [nvarchar](500) NULL,
		[Location] [nvarchar](200) NULL,
		[Venue] [nvarchar](200) NULL,
		[TotalCost] [money] NULL,
		[IsSubmittedBy] [int] NULL,
		[IsMandatoryTraining] [bit] NULL,
		[Expiry] [smalldatetime] NULL,
		[AdditionalNotes] [nvarchar](1000) NULL,
		[Status] [int] NULL,
		[Createdby] [int] NULL,
		[CreatedDate] [datetime] NULL,
		[Updatedby] [int] NULL,
		[UpdateDate] [datetime] NULL,
		[Active] [bit] NULL,
	 CONSTRAINT [PK_E_EmployeeTrainings_History] PRIMARY KEY CLUSTERED 
	(
		[TrainingHistoryId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END

IF COL_LENGTH('E_EmployeeTrainings_History', 'GroupId') IS NULL
		BEGIN
			ALTER TABLE E_EmployeeTrainings_History 
			ADD GroupId INT NULL
			Print('GroupId added successfully  ') 
		END
IF COL_LENGTH('E_EmployeeTrainings_History', 'ProfessionalQualification') IS NULL
		BEGIN
			ALTER TABLE E_EmployeeTrainings_History 
			ADD ProfessionalQualification INT NULL
			Print('ProfessionalQualification added successfully  ') 
		END
IF COL_LENGTH('E_EmployeeTrainings_History', 'Postcode') IS NULL
		BEGIN
			ALTER TABLE E_EmployeeTrainings_History 
			ADD Postcode NVARCHAR(50) NULL
			Print('Postcode added successfully  ') 
		END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 