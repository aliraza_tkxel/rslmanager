Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'DF_RejectionReasons')
BEGIN


CREATE TABLE [dbo].[DF_RejectionReasons](
	[ReasonId] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](255) NULL,
)


INSERT INTO DF_RejectionReasons ([Description]) VALUES ('Work not required')
INSERT INTO DF_RejectionReasons ([Description]) VALUES ('Work completed as a reactive repair')
INSERT INTO DF_RejectionReasons ([Description]) VALUES ('Raised in error')
INSERT INTO DF_RejectionReasons ([Description]) VALUES ('Work on hold')
INSERT INTO DF_RejectionReasons ([Description]) VALUES ('Other')

END
	
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


