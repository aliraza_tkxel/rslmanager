Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_EmployeeTrainings')
BEGIN
	CREATE TABLE [dbo].[E_EmployeeTrainings](
	[TrainingId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[Justification] [nvarchar](500) NULL,
	[Course] [nvarchar](200) NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[TotalNumberOfDays] [int] NULL,
	[ProviderName] [nvarchar](200) NULL,
	[ProviderWebsite] [nvarchar](500) NULL,
	[Location] [nvarchar](200) NULL,
	[Venue] [nvarchar](200) NULL,
	[TotalCost] [money] NULL,
	[IsSubmittedBy] [int] NULL,
	[IsMandatoryTraining] [bit] NULL,
	[Expiry] [smalldatetime] NULL,
	[AdditionalNotes] [nvarchar](1000) NULL,
	[Status] [int] NULL,
	[Createdby] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Updatedby] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_E_EmployeeTrainings] PRIMARY KEY CLUSTERED 
(
	[TrainingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


END 

IF COL_LENGTH('E_EmployeeTrainings', 'GroupId') IS NULL
		BEGIN
			ALTER TABLE E_EmployeeTrainings 
			ADD GroupId INT NULL
			Print('GroupId added successfully  ') 
		END
IF COL_LENGTH('E_EmployeeTrainings', 'ProfessionalQualification') IS NULL
		BEGIN
			ALTER TABLE E_EmployeeTrainings 
			ADD ProfessionalQualification INT NULL
			Print('ProfessionalQualification added successfully  ') 
		END
IF COL_LENGTH('E_EmployeeTrainings', 'Postcode') IS NULL
		BEGIN
			ALTER TABLE E_EmployeeTrainings 
			ADD Postcode NVARCHAR(50) NULL
			Print('Postcode added successfully  ') 
		END

IF Exists(select * from E_EmployeeTrainings t
			inner join E_EmployeeTrainingStatus s on t.Status = s.StatusId
			where  s.Title = 'Exec Approved' and (t.TotalCost > 1000 OR t.ProfessionalQualification >=1)
		)
	BEGIN
		update E_EmployeeTrainings set Status = (select StatusId from E_EmployeeTrainingStatus where Title = 'Exec Supported')
		where  Status = (select StatusId from E_EmployeeTrainingStatus where Title = 'Exec Approved') 
			and (TotalCost > 1000 OR ProfessionalQualification >=1)
	END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 


