USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_PROPERTY_APPLIANCE_DEFECTS')
BEGIN

CREATE TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS](
	[PropertyDefectId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](20) NOT NULL,
	[CategoryId] [int] NULL,
	[JournalId] [int] NULL,
	[IsDefectIdentified] [bit] NULL,
	[DefectNotes] [nvarchar](200) NULL,
	[IsActionTaken] [bit] NULL,
	[ActionNotes] [nvarchar](200) NULL,
	[IsWarningIssued] [bit] NULL,
	[ApplianceId] [int] NULL,
	[SerialNumber] [nvarchar](10) NULL,
	[IsWarningFixed] [bit] NULL,
	[DefectDate] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
	[DetectorTypeId] [int] NULL,
	[DateCreated] [datetime] NULL,
	[PhotoNotes] [nvarchar](1000) NULL,
	[DateCreated1] [datetime] NULL,
	[GasCouncilNumber] [nvarchar](20) NULL,
	[IsDisconnected] [bit] NULL,
	[IsPartsrequired] [bit] NULL,
	[IsPartsOrdered] [bit] NULL,
	[PartsOrderedBy] [int] NULL,
	[PartsDue] [date] NULL,
	[PartsDescription] [nvarchar](1000) NULL,
	[PartsLocation] [nvarchar](200) NULL,
	[IsTwoPersonsJob] [bit] NULL,
	[ReasonFor2ndPerson] [nvarchar](1000) NULL,
	[Duration] [decimal](9, 2) NULL,
	[Priority] [int] NULL,
	[TradeId] [int] NULL,
	[IsCustomerHaveHeating] [bit] NULL,
	[IsHeatersLeft] [bit] NULL,
	[NumberOfHeatersLeft] [tinyint] NULL,
	[IsCustomerHaveHotWater] [bit] NULL,
	[DefectJobSheetStatus] [int] NULL,
	[ApplianceDefectAppointmentJournalId] [int] NULL,
	[NoEntryNotes] [nvarchar](1000) NULL,
	[CancelNotes] [nvarchar](1000) NULL,
	[CreatedDate] [datetime2](3) NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](3) NULL,
 CONSTRAINT [PK_P_PROPERTY_APPLIANCE_DEFECTS] PRIMARY KEY CLUSTERED 
(
	[PropertyDefectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_G_TRADE] FOREIGN KEY([TradeId])
REFERENCES [dbo].[G_TRADE] ([TradeId])


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_G_TRADE]


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_P__PROPERTY_PROPERTYID] FOREIGN KEY([PropertyId])
REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_P__PROPERTY_PROPERTYID]


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_P_DEFECTS_CATEGORY] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[P_DEFECTS_CATEGORY] ([CategoryId])


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_P_DEFECTS_CATEGORY]


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_P_DEFECTS_PRIORITY] FOREIGN KEY([Priority])
REFERENCES [dbo].[P_DEFECTS_PRIORITY] ([PriorityID])


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_P_DEFECTS_PRIORITY]
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PDR_JOURNAL] FOREIGN KEY([ApplianceDefectAppointmentJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PDR_JOURNAL]
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PDR_STATUS] FOREIGN KEY([DefectJobSheetStatus])
REFERENCES [dbo].[PDR_STATUS] ([STATUSID])
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PDR_STATUS]
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] ADD  CONSTRAINT [DF__P_PROPERT__Defec__1EE61C58]  DEFAULT (GETDATE()) FOR [DefectDate]
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] ADD  DEFAULT (GETDATE()) FOR [DateCreated]
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] ADD  CONSTRAINT [DF__P_PROPERT__Photo__4C246DD1]  DEFAULT (NULL) FOR [PhotoNotes]
ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] ADD  CONSTRAINT [DF_P_PROPERTY_APPLIANCE_DEFECTS_DateCreated1]  DEFAULT (GETDATE()) FOR [DateCreated1]


END


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_PROPERTY_APPLIANCE_DEFECTS')
BEGIN

	--==============================================================================
	-- Ticket # 8134 : Added 'BoilerTypeId' 
	--==============================================================================	
	IF NOT EXISTS (	SELECT * 
					FROM sys.columns 
					WHERE  object_id = OBJECT_ID(N'[dbo].[P_PROPERTY_APPLIANCE_DEFECTS]' ) 
						AND name = 'BoilerTypeId')
	BEGIN
		
		ALTER TABLE [P_PROPERTY_APPLIANCE_DEFECTS]
		ADD [BoilerTypeId] BIGINT  NULL
		
		ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  
		WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PA_PARAMETER_VALUE] 
		FOREIGN KEY([BoilerTypeId])	REFERENCES [dbo].[PA_PARAMETER_VALUE] ([ValueID])
		
		ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS] 
		CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PA_PARAMETER_VALUE]
		
		PRINT 'BoilerTypeId column added successfully'
	END	
	
--==============================================================================
-- Ticket # 10706 : Added 'warningNoteSerialNo' 
--==============================================================================	
	IF NOT EXISTS (	SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[P_PROPERTY_APPLIANCE_DEFECTS]' ) 
						AND name = 'WarningNoteSerialNo')
	BEGIN
		
		ALTER TABLE [P_PROPERTY_APPLIANCE_DEFECTS]
		ADD WarningNoteSerialNo [nvarchar](100)  NULL
		PRINT 'WarningIssuedNotes column added successfully'
	END	
	
	
	--==============================================================================
	-- Ticket # 10692 : Increase the length of 'SerialNumber' and 'GasCouncilNumber'
	--==============================================================================	
	IF EXISTS (	SELECT	*
					FROM	INFORMATION_SCHEMA.COLUMNS 
					WHERE	TABLE_NAME = 'P_PROPERTY_APPLIANCE_DEFECTS'
							AND COLUMN_NAME = 'SerialNumber'
							AND CHARACTER_MAXIMUM_LENGTH < 50 )
	BEGIN		
		
		ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS 
		ALTER COLUMN SerialNumber nvarchar(50)	
		
	END	
	
	IF EXISTS (	SELECT	*
					FROM	INFORMATION_SCHEMA.COLUMNS 
					WHERE	TABLE_NAME = 'P_PROPERTY_APPLIANCE_DEFECTS'
							AND COLUMN_NAME = 'GasCouncilNumber'
							AND CHARACTER_MAXIMUM_LENGTH < 50 )
	BEGIN		
		
		ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS 
		ALTER COLUMN GasCouncilNumber nvarchar(50)	
		
	END

	--===============================================================================================================
	--  RSUP-1204 (App version) : Added JobsheetCompletionDate, JobsheetCompletedAppVersion, JobsheetCurrentAppVersion	
	--===============================================================================================================
	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'JobsheetCompletionDate') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD JobsheetCompletionDate smalldatetime NULL
			PRINT('COLUMN JobsheetCompletionDate CREATED')
	END

	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'JobsheetCompletedAppVersion') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD JobsheetCompletedAppVersion nvarchar(20) NULL
			PRINT('COLUMN JobsheetCompletedAppVersion CREATED')
	END

	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'JobsheetCurrentAppVersion') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD JobsheetCurrentAppVersion nvarchar(20) NULL
			PRINT('COLUMN JobsheetCurrentAppVersion CREATED')
	END

	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'RejectionReasonId') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD RejectionReasonId int NULL

			ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]  
			WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_DF_RejectionReasons] 
			FOREIGN KEY([RejectionReasonId])	REFERENCES [dbo].[DF_RejectionReasons] ([ReasonId])
			PRINT('COLUMN RejectionReasonId CREATED')
	END

	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'RejectionReasonNotes') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD RejectionReasonNotes nvarchar(max)

			PRINT('COLUMN RejectionReasonNotes CREATED')
	END

	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'SchemeId') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD SchemeId int null

			PRINT('COLUMN SchemeId CREATED')
	END

	IF COL_LENGTH('P_PROPERTY_APPLIANCE_DEFECTS', 'BlockId') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
			ADD BlockId int null

			PRINT('COLUMN blockId CREATED')
	END
	
	--==============================================================================
-- Ticket # US457 : Added 'DefectCompletionNotes' 
--==============================================================================	
	IF NOT EXISTS (	SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[P_PROPERTY_APPLIANCE_DEFECTS]' ) 
						AND name = 'DefectCompletionNotes')
	BEGIN
		
		ALTER TABLE [P_PROPERTY_APPLIANCE_DEFECTS]
		ADD DefectCompletionNotes [nvarchar](MAX)  NULL
		PRINT 'DefectCompletionNotes column added successfully'
	END	

	IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'HeatingMappingId'
      AND Object_ID = Object_ID(N'P_PROPERTY_APPLIANCE_DEFECTS'))
	BEGIN
		ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]
		ADD [HeatingMappingId] int null

		EXEC('UPDATE P_PROPERTY_APPLIANCE_DEFECTS
		SET [HeatingMappingId] = (SELECT MIN(PA_HeatingMapping.HeatingMappingId) 
									FROM AS_JOURNAL
									INNER JOIN PA_HeatingMapping on PA_HeatingMapping.PropertyId = AS_JOURNAL.PROPERTYID
									WHERE JOURNALID = P_PROPERTY_APPLIANCE_DEFECTS.JournalId AND PA_HeatingMapping.HeatingType=172
									GROUP BY JOURNALID)')

		PRINT 'Added Column [HeatingMappingId]'
	END

	if ((select IS_NULLABLE 
		from INFORMATION_SCHEMA.COLUMNS 
		where TABLE_SCHEMA='dbo' 
				AND TABLE_NAME='P_PROPERTY_APPLIANCE_DEFECTS' 
				AND COLUMN_NAME='PropertyId') = 'NO')
	BEGIN
		ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS
		ALTER COLUMN PropertyId [nvarchar](20) NULL

		Print 'Changed Data type of Propertyid from not null to null'
	END
	
END


	




IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH