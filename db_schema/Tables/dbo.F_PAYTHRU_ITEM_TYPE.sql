CREATE TABLE [dbo].[F_PAYTHRU_ITEM_TYPE]
(
[ItemTypeId] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[F_PAYTHRU_ITEM_TYPE] ADD 
CONSTRAINT [PK_F_PAYTHRU_ITEMTYPE] PRIMARY KEY CLUSTERED  ([ItemTypeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
