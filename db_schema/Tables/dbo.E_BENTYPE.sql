Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'E_BENTYPE')
BEGIN


CREATE TABLE [dbo].[E_BENTYPE](
	[BenefitTypeID] [int] IDENTITY(1,1) NOT NULL,
	[BenefitTypeDesc] [nvarchar](50) NULL,
 CONSTRAINT [PK_E_BENTYPE] PRIMARY KEY CLUSTERED 
(
	[BenefitTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 30) ON [PRIMARY]
) ON [PRIMARY]

END
	--===========================================================================================================
	-- US379 - Rename the benefit type "Medical Insurance" with "Health Cover/Plan"
	--===========================================================================================================
	IF EXISTS (	SELECT	1
	FROM	E_BENTYPE 
	WHERE	BenefitTypeDesc in ('Medical Insurance'))
	BEGIN

		UPDATE	E_BENTYPE 
		SET		BenefitTypeDesc = 'Health Cover/Plan' 
		WHERE	BenefitTypeDesc = 'Medical Insurance'

    END
	IF EXISTS (	SELECT	1
	FROM	E_BENTYPE 
	WHERE	BenefitTypeDesc in ('Subscription'))
	BEGIN

		UPDATE	E_BENTYPE 
		SET		BenefitTypeDesc = 'Professional Subscriptions' 
		WHERE	BenefitTypeDesc = 'Subscription'

    END
	if not exists (select * from E_BENTYPE with (updlock, rowlock, holdlock) where BenefitTypeDesc='Professional Subscriptions')
	BEGIN
	INSERT INTO E_BENTYPE(BenefitTypeDesc) VALUES ('Professional Subscriptions')
	END

	if not exists (select * from E_BENTYPE with (updlock, rowlock, holdlock) where BenefitTypeDesc='PRP')
	BEGIN
	INSERT INTO E_BENTYPE(BenefitTypeDesc) VALUES ('PRP')
	END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


