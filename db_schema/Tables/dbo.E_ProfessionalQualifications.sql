/****** Script for SelectTopNRows command from SSMS  ******/

USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

-------------------------------- CREATING TABLE IF NOT EXISTS ------------------------------------------

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			WHERE TABLE_NAME = N'E_ProfessionalQualifications')
BEGIN


	CREATE TABLE [dbo].[E_ProfessionalQualifications](
		[ProfQualId] [int] IDENTITY(1,1) NOT NULL,
		[QualificationId] [int] NOT NULL,
		[Description] [nvarchar](500) NULL,
		[Active] [bit] NULL,
	 CONSTRAINT [PK_E_ProfessionalQualifications] PRIMARY KEY CLUSTERED 
	(
		[ProfQualId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END
IF NOT EXISTS(Select 1 from E_ProfessionalQualifications)
	BEGIN
	
	INSERT INTO E_ProfessionalQualifications (QualificationId,Description,Active)
	Select QUALIFICATIONSID,PROFESSIONALQUALIFICATION,1  from E_QUALIFICATIONSANDSKILLS
	END


IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH

