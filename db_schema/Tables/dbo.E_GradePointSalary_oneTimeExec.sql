
-- >>> BHA grades <<<
	-- ### BHA - A ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 15919, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 16443, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 16968, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 17493, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 18018, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 18543, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - A'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 19067, 19)

	-- ### BHA - B ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 17477, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 18053, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 18629, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 19205, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 19781, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 20357, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - B'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 20934, 19)

	-- ### BHA - C ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 18774, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 19392, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 20011, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 20630, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 21249, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 21868, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - C'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 22487, 19)

		
	-- ### BHA - D ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 20757, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 21441, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 22125, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 22810, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 23494, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 24178, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - D'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 24862, 19)		

	-- ### BHA - E ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 23478, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 24252, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 25026, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 25800, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 26574, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 27348, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - E'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 28122, 19)		

	-- ### BHA - F ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 26143, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 27006, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 27867, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 28729, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 29591, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 30453, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - F'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 31314, 19)		
	
	-- ### BHA - G ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 29022, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 29979, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 30936, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 31893, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 32849, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 33806, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - G'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 34762, 19)		

	-- ### BHA - H ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 32993, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 34081, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 35169, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 36256, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 37344, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 38431, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - H'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 39520, 19)		

	-- ### BHA - I ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 38281, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 39543, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 40806, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 42067, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 43329, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 44591, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - I'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 45853, 19)		

	-- ### BHA - J ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 43886, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 45332, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 46780, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 48226, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 49673, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 51120, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - J'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 52566, 19)		

	-- ### BHA - K ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 49090, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 50709, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 52327, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 53946, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 55564, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 57183, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - K'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 58800, 19)		

	-- ### BHA - L ###
	-- point 1
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 1'), 57732, 19)
	-- point 2
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 2'), 59635, 19)
	-- point 3
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 3'), 61538, 19)
	-- point 4
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 4'), 63441, 19)
	-- point 5
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 5'), 65344, 19)
	-- point 6
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 6'), 67248, 19)
	-- point 7
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'BHA - L'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point 7'), 69151, 19)		


-- >>> BRS Pay Grades <<<
	-- ### General Operative ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'General Operative'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'),  17834, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'General Operative'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 18422, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'General Operative'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 19010, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'General Operative'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 19598, 19)

	-- ### Plumber ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Plumber'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'),  23255, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Plumber'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 23975, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Plumber'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 24716, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Plumber'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 25480, 19)

	-- ### Carpenter ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Carpenter'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'),  24465, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Carpenter'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 25271, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Carpenter'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 26078, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Carpenter'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 26884, 19)

	-- ### Multi-skilled 1 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 24539, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 25348, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 26157, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 26966, 19)

	-- ### Multi-skilled 2 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 25695, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 26543, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 27390, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 28236, 19)

	-- ### Multi-skilled 2 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 25695, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 26543, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 27390, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Multi-skilled 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 28236, 19)

	-- ### Electrician 1 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 26294, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 27161, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 28028, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 28894, 19)

	-- ### Electrician 2 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 28476, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 29357, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 30264, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrician 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 31200, 19)

	-- ### Heating & Renewables Electrical Engineer ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Heating & Renewables Electrical Engineer'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 29723, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Heating & Renewables Electrical Engineer'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 30703, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Heating & Renewables Electrical Engineer'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 31683, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Heating & Renewables Electrical Engineer'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 32663, 19)

	-- ### Gas Engineer 1 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 29723, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 30703, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 31683, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 1'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 32663, 19)

	-- ### Gas Engineer 2 ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 31210, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 32239, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 33268, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Engineer 2'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 34296, 19)

	-- ### Maintenance Team Leader ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Maintenance Team Leader'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 30410, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Maintenance Team Leader'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 31412, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Maintenance Team Leader'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 32415, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Maintenance Team Leader'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 33417, 19)

	-- ### Gas Supervisor ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Supervisor'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 32467, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Supervisor'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 33537, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Supervisor'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 34608, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Supervisor'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 35678, 19)

	-- ### Area Maintenance Manager ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Area Maintenance Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 33090, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Area Maintenance Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 34114, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Area Maintenance Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 35169, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Area Maintenance Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 36256, 19)

	-- ### Electrical Manager ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrical Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 34083, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrical Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 35137, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrical Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 36224, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Electrical Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 37344, 19)

	-- ### Gas Manager  ###
	-- point A
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point A'), 37497, 19)
	-- point B
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point B'), 38733, 19)
	-- point C
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point C'), 39969, 19)
	-- point D
	insert into E_GradePointSalary (Grade, GradePoint, Salary, fiscalYearId) values 
		((select GRADEID from E_GRADE where GRADE = 'Gas Manager'), (select POINTID from E_GRADE_POINT where POINTDESCRIPTION = 'Point D'), 41205, 19)
