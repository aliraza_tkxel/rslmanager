USE [RSLBHALive]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'FL_CO_APPOINTMENT')
BEGIN

CREATE TABLE [dbo].[FL_CO_APPOINTMENT](
	[AppointmentID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[JobSheetNumber] [varchar](50) NULL,
	[AppointmentDate] [datetime] NULL,
	[OperativeID] [int] NULL,
	[LetterID] [int] NULL,
	[AppointmentStageID] [int] NULL,
	[LastActionDate] [datetime] NULL,
	[Notes] [varchar](500) NULL,
	[Time] [varchar](50) NOT NULL,
	[AppointmentStatus] [nvarchar](20) NULL,
	[EndTime] [varchar](50) NULL,
	[RepairNotes] [varchar](500) NULL,
	[RepairCompletionDateTime] [smalldatetime] NULL,
	[isCalendarAppointment] [bit] NULL,
	[AppointmentEndDate] [datetime] NULL,
 CONSTRAINT [PK_FL_CO_APPOINTMENT] PRIMARY KEY CLUSTERED 
(
	[AppointmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

	PRINT('TABLE FL_CO_APPOINTMENT CREATED')
END

IF COL_LENGTH('FL_CO_APPOINTMENT', 'AppointmentCompletionDate') IS NULL
	BEGIN
			ALTER TABLE FL_CO_APPOINTMENT
			ADD AppointmentCompletionDate smalldatetime NULL
			PRINT('COLUMN AppointmentCompletionDate CREATED')
	END

IF COL_LENGTH('FL_CO_APPOINTMENT', 'AppointmentCompletedAppVersion') IS NULL
	BEGIN
			ALTER TABLE FL_CO_APPOINTMENT
			ADD AppointmentCompletedAppVersion nvarchar(20) NULL
			PRINT('COLUMN AppointmentCompletedAppVersion CREATED')
	END

IF COL_LENGTH('FL_CO_APPOINTMENT', 'AppointmentCurrentAppVersion') IS NULL
	BEGIN
			ALTER TABLE FL_CO_APPOINTMENT
			ADD AppointmentCurrentAppVersion nvarchar(20) NULL
			PRINT('COLUMN AppointmentCurrentAppVersion CREATED')
	END

GO
