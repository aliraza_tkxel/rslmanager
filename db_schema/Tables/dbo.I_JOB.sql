CREATE TABLE [dbo].[I_JOB]
(
[JobId] [bigint] NOT NULL IDENTITY(1, 1),
[TeamId] [bigint] NOT NULL,
[Description] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeId] [bigint] NOT NULL,
[Title] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ref] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationId] [bigint] NOT NULL,
[Duration] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DatePosted] [datetime] NOT NULL,
[ClosingDate] [datetime] NULL,
[Salary] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationId] [int] NULL,
[Active] [smallint] NOT NULL,
[OtherType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[I_JOB] ADD 
CONSTRAINT [PK_I_JOB] PRIMARY KEY CLUSTERED  ([JobId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[I_JOB] WITH NOCHECK ADD
CONSTRAINT [FK_I_JOB_I_JOB_TYPE] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[I_JOB_TYPE] ([JobTypeId])
GO
