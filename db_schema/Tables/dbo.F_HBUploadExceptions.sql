USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_HBUploadExceptions')
BEGIN

CREATE TABLE [dbo].[F_HBUploadExceptions](
	[HBUploadExceptionId] [int] IDENTITY(1,1) NOT NULL,
	[HBUploadFileDataId] [int] NULL,
	[ReasonId] [int] NULL,
	[IsResolved] [bit] NULL,
	[Display] [bit] NULL,
 CONSTRAINT [PK_F_HBUploadExceptions] PRIMARY KEY CLUSTERED 
(
	[HBUploadExceptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[F_HBUploadExceptions]  WITH CHECK ADD  CONSTRAINT [FK_F_HBUploadExceptions_F_HBExceptionReason] FOREIGN KEY([ReasonId])
REFERENCES [dbo].[F_HBExceptionReason] ([ReasonId])


ALTER TABLE [dbo].[F_HBUploadExceptions] CHECK CONSTRAINT [FK_F_HBUploadExceptions_F_HBExceptionReason]


ALTER TABLE [dbo].[F_HBUploadExceptions]  WITH CHECK ADD  CONSTRAINT [FK_F_HBUploadExceptions_F_HBUploadFileData] FOREIGN KEY([HBUploadFileDataId])
REFERENCES [dbo].[F_HBUploadFileData] ([HBUploadFileDataId])


ALTER TABLE [dbo].[F_HBUploadExceptions] CHECK CONSTRAINT [FK_F_HBUploadExceptions_F_HBUploadFileData]

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
