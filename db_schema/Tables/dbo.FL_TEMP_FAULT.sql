CREATE TABLE [dbo].[FL_TEMP_FAULT]
(
[TempFaultID] [int] NOT NULL IDENTITY(1, 1),
[FaultID] [int] NULL,
[CustomerID] [int] NULL,
[Quantity] [int] NULL,
[ProblemDays] [int] NULL,
[RecuringProblem] [bit] NULL,
[CommunalProblem] [bit] NULL,
[Notes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORGID] [int] NULL,
[ItemStatusId] [int] NULL,
[ItemActionId] [int] NULL,
[Recharge] [bit] NULL,
[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaultLogId] [int] NULL,
[IsAppointmentConfirmed] [bit] NULL,
[ISRECALL] [bit] NULL,
[FaultTradeId] [int] NULL,
[ISREARRANGE] [bit] NULL,
[OriginalFaultLogId] [int] NULL,
[isFollowon] [bit] NOT NULL CONSTRAINT [DF__FL_TEMP_F__isFol__7B3DB8BF] DEFAULT ((0)),
[FollowOnFaultLogID] [int] NULL,
[Duration] [decimal] (9, 2) NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_TEMP_FAULT] ADD 
CONSTRAINT [PK_FL_TEMP_FAULT] PRIMARY KEY CLUSTERED  ([TempFaultID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[FL_TEMP_FAULT] ADD
CONSTRAINT [FK_FL_TEMP_FAULT_FL_FAULT] FOREIGN KEY ([FaultID]) REFERENCES [dbo].[FL_FAULT] ([FaultID])
GO

ALTER TABLE [dbo].[FL_TEMP_FAULT] ADD CONSTRAINT [FK_FL_TEMP_FAULT_S_ORGANISATION] FOREIGN KEY ([ORGID]) REFERENCES [dbo].[S_ORGANISATION] ([ORGID])
GO
