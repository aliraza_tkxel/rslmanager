CREATE TABLE [dbo].[C_OCCUPATION]
(
[OCCUPATIONID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[C_OCCUPATION] ADD 
CONSTRAINT [PK_C_OCCUPATION] PRIMARY KEY CLUSTERED  ([OCCUPATIONID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
