USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_Viewings')
	BEGIN
	CREATE TABLE [dbo].[FLS_Viewings]
	(
		[ViewingId] [int] IDENTITY(1,1) NOT NULL,
		[CustomerId] [int] NOT NULL,
		[PropertyId] [nvarchar](20) NOT NULL,
		[HousingOfficerId] [int] NOT NULL,
		[viewingStatusId] [int] NOT NULL,
		[ViewingDate] [smalldatetime] NOT NULL,
		[ViewingTime] [nvarchar](10) NOT NULL,
		[EndTime] [nvarchar](10)  NULL,
		[Duration] [float]  NULL,
		[ActualEndTime] [nvarchar](10)  NULL,
		[CreatedById] [int] NOT NULL,
		[IsActive] [bit] NOT NULL,
		[RecordingSource] [nvarchar](50)  NULL,
		[AppVersion] [nvarchar](100)  NULL,
		[CreatedOnApp] [smalldatetime]  NULL,
		[CreatedOnServer] [smalldatetime]  NULL,
		[LastModifiedOnApp] [smalldatetime]  NULL,
		[LastModifiedOnServer] [smalldatetime]  NULL,
		[Notes] [nvarchar](1000) NULL
		CONSTRAINT [PK_FLS_Viewings] PRIMARY KEY CLUSTERED 
		(
			[ViewingId] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) 		ON [PRIMARY]
	PRINT 'Table FLS_Viewings created'
	END
ELSE
BEGIN
PRINT 'Table FLS_Viewings already exist'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'outlookIdentifier'
      AND Object_ID = Object_ID(N'FLS_Viewings'))
BEGIN
    ALTER TABLE [dbo].[FLS_Viewings]
	ADD outlookIdentifier NVARCHAR(MAX) NULL
	PRINT 'Added Column outlookIdentifier'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'AssignedById'
      AND Object_ID = Object_ID(N'FLS_Viewings'))
BEGIN
    ALTER TABLE [dbo].[FLS_Viewings]
	ADD AssignedById INT NULL
	PRINT 'Added Column AssignedById'

	EXEC('UPDATE FLS_Viewings SET AssignedById=CreatedById')

END


IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
