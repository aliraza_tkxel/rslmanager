CREATE TABLE [dbo].[AM_Payment]
(
[PaymentId] [int] NOT NULL IDENTITY(1, 1),
[Payment] [float] NOT NULL,
[Date] [datetime] NOT NULL,
[PaymentPlanId] [int] NOT NULL,
[PaymentPlanHistoryId] [int] NOT NULL,
[IsFlexible] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Payment] ADD 
CONSTRAINT [PK_AM_Payment] PRIMARY KEY CLUSTERED  ([PaymentId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_Payment] ADD CONSTRAINT [PaymentPlanHistory] FOREIGN KEY ([PaymentPlanHistoryId]) REFERENCES [dbo].[AM_PaymentPlanHistory] ([PaymentPlanHistoryId])
GO
ALTER TABLE [dbo].[AM_Payment] ADD CONSTRAINT [FK_AM_Payment_AM_PaymentPlan] FOREIGN KEY ([PaymentPlanId]) REFERENCES [dbo].[AM_PaymentPlan] ([PaymentPlanId])
GO
