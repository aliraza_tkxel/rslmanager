CREATE TABLE [dbo].[TMPHANDOVERORIGINAL]
(
[PropertyID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewHandoverDate] [datetime] NULL,
[Rent] [money] NULL,
[Services] [money] NULL,
[Ineligible] [money] NULL,
[Supported] [money] NULL,
[Water] [money] NULL,
[CouncilTax] [money] NULL,
[Total] [money] NULL,
[Garage] [money] NULL
) ON [PRIMARY]
GO
