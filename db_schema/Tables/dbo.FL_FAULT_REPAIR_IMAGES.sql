CREATE TABLE [dbo].[FL_FAULT_REPAIR_IMAGES]
(
[FaultRepairImageId] [int] NOT NULL IDENTITY(1, 1),
[PropertyId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JobSheetNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImageName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsBeforeImage] [bit] NULL,
[CreatedOn] [smalldatetime] NULL,
[CreatedBy] [int] NULL,
[ImageIdentifier] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FL_FAULT_REPAIR_IMAGES] ADD CONSTRAINT [PK_FL_FAULT_REPAIR_IMAGES] PRIMARY KEY CLUSTERED  ([FaultRepairImageId]) ON [PRIMARY]
GO
