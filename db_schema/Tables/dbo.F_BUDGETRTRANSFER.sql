CREATE TABLE [dbo].[F_BUDGETRTRANSFER]
(
[CCTranId] [int] NOT NULL IDENTITY(1, 1),
[FrmCosetCentre] [int] NULL,
[Amount] [int] NULL,
[ToCostCentre] [int] NULL,
[Note] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BDDate] [datetime] NULL,
[Approved] [bit] NULL,
[userid] [int] NULL,
[PHYYEAR] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[F_BUDGETRTRANSFER] ADD CONSTRAINT [PK_F_BUDGETRTRANSFER] PRIMARY KEY CLUSTERED  ([CCTranId]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
