USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_Adaptations')
BEGIN
CREATE TABLE [dbo].[FLS_Adaptations](
	[AdaptationId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_FLS_Adaptations] PRIMARY KEY CLUSTERED 
(
	[AdaptationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Level access shower', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Over bath shower', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Wet room (fully wheel chair accessible)', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Stair lift', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Through floor lift', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Adapted kitchen', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Widened doorways (only if all are suitable for wheelchair users)', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Specialist Wash dry WC', 1)
INSERT [dbo].[FLS_Adaptations] ([Description], [isActive]) VALUES ( N'Wheelchair access to property', 1)
SET IDENTITY_INSERT [dbo].[FLS_Adaptations] OFF
PRINT 'Table FLS_Adaptations created'
END
ELSE
BEGIN
PRINT 'Table FLS_Adaptations already exist'

END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
