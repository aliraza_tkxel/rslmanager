USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_GENERAL_HEATING_CHECKS')
BEGIN

CREATE TABLE [dbo].[P_GENERAL_HEATING_CHECKS](
	[HeatingCheckId] [int] IDENTITY(1,1) NOT NULL,
	[RadiatorCondition] [nvarchar](15) NULL,
	[HeatingControl] [nvarchar](15) NULL,
	[SystemUsage] [nvarchar](15) NULL,
	[GeneralHeating] [nvarchar](15) NULL,
	[AccessIssues] [nvarchar](15) NULL,
	[AccessIssueNotes] [nvarchar](300) NULL,
	[Confirmation] [nvarchar](15) NULL,
	[CreationDate] [datetime] NULL,
	[CheckedBy] [int] NULL,
	[JournalId] [int] NULL,
 CONSTRAINT [PK_P_GENERAL_HEATING_CHECKS] PRIMARY KEY CLUSTERED 
(
	[HeatingCheckId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 