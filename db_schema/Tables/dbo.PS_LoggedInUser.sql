CREATE TABLE [dbo].[PS_LoggedInUser]
(
[LoggedInUsername] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoggedInUserDateTime] [datetime] NULL,
[LoggedInUserSalt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_LoggedInUser] ADD 
CONSTRAINT [PK_PS_LoggedInUser] PRIMARY KEY CLUSTERED  ([LoggedInUsername]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
