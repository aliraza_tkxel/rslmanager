USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_CORE_WORKING_HOURS_HISTORY')
BEGIN
	
CREATE TABLE [dbo].[E_CORE_WORKING_HOURS_HISTORY](
	[COREWORKINGHOURID] [int] NOT NULL,
	[EMPLOYEEID] [int] NOT NULL,
	[DAYID] [int] NOT NULL,
	[STARTTIME] [varchar](10) NULL,
	[ENDTIME] [varchar](10) NULL,
	[CREATEDDATE] [datetime] NULL,
	[CREATEDBY] [int] NULL
) ON [PRIMARY]

END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


