CREATE TABLE [dbo].[startdates]
(
[StartDate] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CandF] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tenantid] [int] NULL,
[NEWDATE] [smalldatetime] NULL,
[ZDATE] [smalldatetime] NULL
) ON [PRIMARY]
GO
