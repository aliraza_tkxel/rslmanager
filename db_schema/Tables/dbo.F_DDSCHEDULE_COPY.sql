CREATE TABLE [dbo].[F_DDSCHEDULE_COPY]
(
[DDSCHEDULEID] [int] NOT NULL,
[USERID] [int] NULL,
[CREATIONDATE] [smalldatetime] NULL,
[CUSTOMERID] [int] NULL,
[TENANCYID] [int] NULL,
[ITEMTYPE] [int] NULL,
[ACCOUNTNAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SORTCODE] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACCOUNTNUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAYMENTDATE] [smalldatetime] NULL,
[INITIALAMOUNT] [money] NULL,
[REGULARPAYMENT] [money] NULL,
[DDPERIOD] [int] NULL,
[PERIODBALANCE] [int] NULL,
[LASTPOSTDATE] [smalldatetime] NOT NULL,
[SUSPEND] [int] NULL
) ON [PRIMARY]
GO
