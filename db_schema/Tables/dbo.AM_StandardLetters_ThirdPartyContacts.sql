USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AM_StandardLetters_ThirdPartyContacts')
		BEGIN
			CREATE TABLE [dbo].[AM_StandardLetters_ThirdPartyContacts](
				[ThirdPartyContactId] [int] IDENTITY(1,1) NOT NULL,
				[ContactName] [varchar](100) NULL,
				[Address1] [varchar](max) NULL,
				[Address2] [varchar](max) NULL,
				[Address3] [varchar](max) NULL,
				[TownCity] [varchar](100) NULL,
				[PostCode] [varchar](100) NULL
				) 	ON [PRIMARY]

				ALTER TABLE [dbo].[AM_StandardLetters_ThirdPartyContacts]
				ADD CONSTRAINT [PK_AM_StandardLetters_ThirdPartyContacts] 
				PRIMARY KEY NONCLUSTERED  ([ThirdPartyContactId] )
				PRINT 'Table Created';
		END
	ELSE 
		BEGIN
			
			PRINT 'Table Exists';
		END


	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)
END CATCH