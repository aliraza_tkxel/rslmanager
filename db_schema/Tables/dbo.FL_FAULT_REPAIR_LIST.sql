CREATE TABLE [dbo].[FL_FAULT_REPAIR_LIST]
(
[FaultRepairListID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetCost] [float] NULL,
[VatRateID] [int] NULL,
[Vat] [float] NULL,
[Gross] [float] NULL,
[PostInspection] [bit] NULL,
[StockConditionItem] [bit] NULL,
[RepairActive] [bit] NULL,
[PMParameterId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_REPAIR_LIST] ADD 
CONSTRAINT [PK_FL_FAULT_REPAIR_LIST] PRIMARY KEY CLUSTERED  ([FaultRepairListID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FL_FAULT_REPAIR_LIST] ADD CONSTRAINT [FK_FL_FAULT_REPAIR_LIST_F_VAT] FOREIGN KEY ([VatRateID]) REFERENCES [dbo].[F_VAT] ([VATID])
GO
