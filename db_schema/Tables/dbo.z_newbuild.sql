CREATE TABLE [dbo].[z_newbuild]
(
[ITEM_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCATION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELEMENT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REPAIR DETAILS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F6] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F7] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F8] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost Centre] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Head] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expenditure] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F12] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost Centre1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Head1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expenditure1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
