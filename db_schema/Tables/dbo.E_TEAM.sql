Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'E_TEAM')
BEGIN


CREATE TABLE [dbo].[E_TEAM](
	[TEAMID] [int] IDENTITY(1,1) NOT NULL,
	[CREATIONDATE] [smalldatetime] NOT NULL,
	[TEAMNAME] [nvarchar](100) NULL,
	[MANAGER] [int] NULL,
	[DIRECTOR] [int] NULL,
	[MAINFUNCTION] [nvarchar](200) NULL,
	[DESCRIPTION] [nvarchar](500) NULL,
	[ACTIVE] [int] NULL,
	[DIRECTORATEID] [int] NULL,
 CONSTRAINT [PK_TEAM] PRIMARY KEY NONCLUSTERED 
(
	[TEAMID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[E_TEAM] ADD  CONSTRAINT [DF_TEAM_CREATIONDATE]  DEFAULT (convert(datetime,convert(varchar,getdate(),103),103)) FOR [CREATIONDATE]

ALTER TABLE [dbo].[E_TEAM] ADD  CONSTRAINT [DF_E_TEAM_MANAGER]  DEFAULT (null) FOR [MANAGER]


END
	--===========================================================================================================
	-- ID136 - Rename 'Frontline' with 'Housing Team'
	--===========================================================================================================
	IF EXISTS (	SELECT	1
				FROM	E_TEAM 
				WHERE	TEAMNAME = 'Frontline')
	BEGIN

		UPDATE	E_TEAM 
		SET		TEAMNAME = 'Housing Team' 
		WHERE	TEAMNAME = 'Frontline'

    END

	IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_TEAM]' ) AND name = 'DIRECTORATEID')
	BEGIN
		ALTER TABLE E_TEAM ADD DIRECTORATEID int DEFAULT NULL
	PRINT 'DIRECTORATEID added successfully!'
	END--if


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


