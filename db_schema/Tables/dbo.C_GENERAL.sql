CREATE TABLE [dbo].[C_GENERAL]
(
[GENERALHISTORYID] [int] NOT NULL IDENTITY(1, 1),
[JOURNALID] [int] NULL,
[ITEMSTATUSID] [int] NULL,
[ITEMACTIONID] [int] NULL,
[LASTACTIONDATE] [smalldatetime] NULL CONSTRAINT [DF_C_GENERAL_LASTACTIONDATE] DEFAULT (getdate()),
[LASTACTIONUSER] [int] NULL,
[NOTES] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASSIGNTO] [int] NULL,
[CONTACTTYPE] [int] NOT NULL CONSTRAINT [DF_C_GENERAL_CONTACTTYPE] DEFAULT (0)
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_C_GENERAL] ON [dbo].[C_GENERAL] ([JOURNALID], [LASTACTIONDATE] DESC) WITH FILLFACTOR=90 ON [PRIMARY]
GO
