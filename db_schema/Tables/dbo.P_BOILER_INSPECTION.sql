USE [RSLBHALive]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_BOILER_INSPECTION')
BEGIN

CREATE TABLE [dbo].[P_BOILER_INSPECTION](
	[BOILERINSPECTIONID] [int] IDENTITY(1,1) NOT NULL,
	[BOILERTYPEID] [bigint] NULL,
	[ISINSPECTED] [bit] NOT NULL,
	[COMBUSTIONREADING] [varchar](15) NULL,
	[OPERATINGPRESSURE] [varchar](15) NULL,
	[SAFETYDEVICEOPERATIONAL] [varchar](20) NULL,
	[SPILLAGETEST] [varchar](20) NULL,
	[SMOKEPELLET] [varchar](20) NULL,
	[ADEQUATEVENTILATION] [varchar](20) NULL,
	[FLUEVISUALCONDITION] [varchar](20) NULL,
	[SATISFACTORYTERMINATION] [varchar](20) NULL,
	[FLUEPERFORMANCECHECKS] [varchar](20) NULL,
	[BOILERSERVICED] [varchar](20) NULL,
	[BOILERSAFETOUSE] [varchar](20) NULL,
	[INSPECTIONDATE] [smalldatetime] NULL,
	[INSPECTEDBY] [int] NULL,
	[JOURNALID] [int] NULL,
	[OPERATINGPRESSUREUNIT] [varchar](10) NULL,
 CONSTRAINT [PK_P_BOILER_INSPECTION] PRIMARY KEY CLUSTERED 
(
	[BOILERINSPECTIONID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF


ALTER TABLE [dbo].[P_BOILER_INSPECTION]  WITH CHECK ADD  CONSTRAINT [FK_P_BOILER_INSPECTION_PA_PARAMETER_VALUE] FOREIGN KEY([BOILERTYPEID])
REFERENCES [dbo].[PA_PARAMETER_VALUE] ([ValueID])


ALTER TABLE [dbo].[P_BOILER_INSPECTION] CHECK CONSTRAINT [FK_P_BOILER_INSPECTION_PA_PARAMETER_VALUE]


 PRINT 'Table created successfully!';
END 
ELSE
BEGIN
	PRINT 'Table Already Exist';
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'HeatingMappingId'
      AND Object_ID = Object_ID(N'P_BOILER_INSPECTION'))
BEGIN
    ALTER TABLE [dbo].[P_BOILER_INSPECTION]
	ADD [HeatingMappingId] int null

	EXEC('UPDATE P_BOILER_INSPECTION
	SET [HeatingMappingId] = (SELECT MIN(PA_HeatingMapping.HeatingMappingId) 
								FROM AS_JOURNAL
								INNER JOIN PA_HeatingMapping on PA_HeatingMapping.PropertyId = AS_JOURNAL.PROPERTYID
								WHERE JOURNALID = P_BOILER_INSPECTION.JOURNALID AND PA_HeatingMapping.HeatingType=172
								GROUP BY JOURNALID)')

	PRINT 'Added Column [HeatingMappingId]'
END

	--===============================================
	-- 10828 -  Added Primary key constraint
	--===============================================

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE='PRIMARY KEY' AND TABLE_SCHEMA='dbo'
        AND TABLE_NAME = 'P_BOILER_INSPECTION' AND CONSTRAINT_NAME = 'PK_P_BOILER_INSPECTION')
	BEGIN
	   ALTER TABLE [dbo].[P_BOILER_INSPECTION] ADD CONSTRAINT [PK_P_BOILER_INSPECTION] PRIMARY KEY CLUSTERED  ([BOILERINSPECTIONID])
	   PRINT N'Creating primary key [PK_P_BOILER_INSPECTION] on [dbo].[P_BOILER_INSPECTION]'
	END
