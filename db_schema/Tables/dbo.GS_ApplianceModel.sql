CREATE TABLE [dbo].[GS_ApplianceModel]
(
[ModelID] [int] NOT NULL IDENTITY(1, 1),
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_ApplianceModel] ADD 
CONSTRAINT [PK_GS_ApplianceModel] PRIMARY KEY CLUSTERED  ([ModelID]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_GS_ApplianceModel] ON [dbo].[GS_ApplianceModel] ([Model]) ON [PRIMARY]

GO
