CREATE TABLE [dbo].[NL_BALANCESHEETGROUPING]
(
[GROUPID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NL_BALANCESHEETGROUPING] ADD CONSTRAINT [PK_NL_BALANCESHEETGROUPING] PRIMARY KEY CLUSTERED  ([GROUPID]) WITH FILLFACTOR=30 ON [PRIMARY]
GO
