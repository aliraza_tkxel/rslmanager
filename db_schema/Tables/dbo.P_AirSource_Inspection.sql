USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_AirSource_Inspection')
BEGIN

CREATE TABLE [dbo].[P_AirSource_Inspection](
	[InspectionId] [int] IDENTITY(1,1) NOT NULL,	
	[HeatingMappingId] [int] NULL,
	[JournalId] [int] NULL,
	[IsInspected] [bit] NULL,
	[InspectionDate] [smalldatetime] NULL,
	[InspectedBy] [int] NULL,
	[CheckWaterSupplyTurnedOff] [bit] NULL,
	[TurnedOffDetail] [nvarchar](300) NULL,
	[CheckWaterSupplyTurnedOn] [bit] NULL,
	[TurnedOnDetail] [nvarchar](300) NULL,
	[CheckValves] [bit] NULL,
	[ValveDetail] [nvarchar](300) NULL,
	[CheckSupplementaryBonding] [bit] NULL,
	[SupplementaryBondingDetail] [nvarchar](300) NULL,	
	[CheckFuse] [bit] NULL,
	[FuseDetail] [nvarchar](300) NULL,
	[CheckThermostat] [bit] NULL,
	[ThermostatDetail] [nvarchar](300) NULL,
	[CheckOilLeak] [bit] NULL,
	[OilLeakDetail] [nvarchar](300) NULL,
	[CheckWaterPipework] [bit] NULL,
	[PipeworkDetail] [nvarchar](300) NULL,
	[CheckElectricConnection] [bit] NULL,
	[ConnectionDetail] [nvarchar](300) NULL,

 CONSTRAINT [PK_P_AirSource_Inspection] PRIMARY KEY CLUSTERED 
(
	[InspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 