CREATE TABLE [dbo].[NL_MISCELLANEOUS_DTL]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[hdrid] [int] NULL,
[NlAccount] [int] NULL,
[NetAmount] [money] NULL,
[Gross] [money] NULL,
[Details] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [_dta_index_NL_MISCELLANEOUS_DTL_7_2032062325__K2_K1_4] ON [dbo].[NL_MISCELLANEOUS_DTL] ([hdrid], [id]) INCLUDE ([NetAmount]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[NL_MISCELLANEOUS_DTL] ADD CONSTRAINT [PK_NL_Miscellaneous_dtl] PRIMARY KEY CLUSTERED  ([id]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
