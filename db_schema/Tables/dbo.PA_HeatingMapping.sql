Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PA_HeatingMapping')
BEGIN
CREATE TABLE [dbo].[PA_HeatingMapping](
	[HeatingMappingId] [int] IDENTITY(1,1) NOT NULL,
	[HeatingType] [int] NULL,
	[IsActive] [bit] NULL,
	[PropertyId] [nvarchar](20) NULL,
	[SchemeID] [int] NULL,
	[BlockID] [int] NULL,
 CONSTRAINT [PK_PA_HeatingMapping] PRIMARY KEY CLUSTERED 
(
	[HeatingMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END


	IF EXISTS(	SELECT * 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE 
				 TABLE_NAME = 'PA_HeatingMapping' AND 
				 COLUMN_NAME = 'HeatingType' AND  DATA_TYPE = 'INT')
	BEGIN
			ALTER TABLE PA_HeatingMapping
			ALTER COLUMN HeatingType BIGINT	NULL

			PRINT 'HeatingType COLUMN UPDATED OF PA_HeatingMapping '
	END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 