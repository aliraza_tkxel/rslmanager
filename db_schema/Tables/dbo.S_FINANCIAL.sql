CREATE TABLE [dbo].[S_FINANCIAL]
(
[FINANCIALID] [int] NOT NULL IDENTITY(1, 1),
[ORGID] [int] NULL,
[CURRENTTURNOVER] [float] NULL,
[LATESTACCOUNTDATE] [smalldatetime] NULL,
[REVIEWEDBY] [int] NULL,
[DATEREVIEWED] [smalldatetime] NULL,
[TURNOVERLIMIT] [float] NULL,
[CONTRACTLIMIT] [float] NULL,
[APPROVEDBY] [int] NULL,
[DATEAPPROVED] [smalldatetime] NULL,
[APPROVALSTATUS] [int] NULL
) ON [PRIMARY]
GO
