CREATE TABLE [dbo].[AS_ActionHistory]
(
[ActionHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ActionId] [int] NOT NULL,
[StatusId] [int] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[IsEditable] [bit] NULL,
[Ctimestamp] [smalldatetime] NULL CONSTRAINT [DF__AS_Action__Ctime__4AF9A8C0] DEFAULT (getdate())
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_ActionHistory] ADD 
CONSTRAINT [PK_AS_ActionHistory] PRIMARY KEY CLUSTERED  ([ActionHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AS_ActionHistory] ADD CONSTRAINT [FK_AS_ACTIONHistory_AS_STATUS] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AS_Action] ([ActionId])
GO
