CREATE TABLE [dbo].[C_ADAPTATION]
(
[ADAPTATIONHISTORYID] [int] NOT NULL IDENTITY(1, 1),
[JOURNALID] [int] NULL,
[ITEMSTATUSID] [int] NULL,
[LASTACTIONDATE] [smalldatetime] NULL CONSTRAINT [DF_C_ADAPTATION_LASTACTIONDATE] DEFAULT (getdate()),
[LASTACTIONUSER] [int] NULL,
[NOTES] [nvarchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASSIGNTO] [int] NULL,
[ADAPTCATID] [int] NULL,
[ADAPTATIONID] [int] NULL
) ON [PRIMARY]
GO
