USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_PO_SendEmailNotification')
BEGIN
CREATE TABLE [dbo].[F_PO_SendEmailNotification](
	[SendEmailNotification] [int] IDENTITY(1,1) NOT NULL,
	[SendToEmployee] varchar(50),
	[SendToEmail] varchar(20) null,
	[PONumber] varchar(20) null,
	[Supplier] varchar(50) null,
	[ITEMNAME] varchar(100) null,
	[ITEMDESC] varchar(500) null,
	[GROSSCOST] varchar(50) null,
	[Status] varchar(20) null,
	[RaisedBy] varchar(50) null,
	[SendToEmpId] int null,
	[ORDERITEMID] int null
) ON [PRIMARY]



IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME = 'F_PO_SendEmailNotification'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE F_PO_SendEmailNotification
ADD CONSTRAINT pk_SendEmailNotification PRIMARY KEY (SendEmailNotification)
PRINT 'Primary key added'
END

END

ELSE
BEGIN

PRINT 'Table already exist..'

END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SendToEmployee'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [SendToEmployee] [varchar(100)] NULL
	PRINT 'Added Column [SendToEmployee]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [SendToEmployee] varchar(100);
end 

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SendToEmail'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [SendToEmail] [varchar(50)] NULL
	PRINT 'Added Column [SendToEmail]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [SendToEmail] varchar(50);
end 

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Supplier'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [Supplier] [varchar(100)] NULL
	PRINT 'Added Column [Supplier]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [Supplier] varchar(100);
end 

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ITEMDESC'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [ITEMDESC] [varchar(max)] NULL
	PRINT 'Added Column [ITEMDESC]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [ITEMDESC] varchar(max);
end 

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'GROSSCOST'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [GROSSCOST] [varchar(50)] NULL
	PRINT 'Added Column [GROSSCOST]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [GROSSCOST] varchar(50);
end 

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Status'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [Status] [varchar(25)] NULL
	PRINT 'Added Column [Status]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [Status] varchar(25);
end

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Status'
      AND Object_ID = Object_ID(N'F_PO_SendEmailNotification'))
BEGIN
    ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ADD [Status] [varchar(25)] NULL
	PRINT 'Added Column [Status]'
END
else 
begin
	ALTER TABLE [dbo].[F_PO_SendEmailNotification]
	ALTER COLUMN [Status] varchar(25);
end

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

