CREATE TABLE [dbo].[AM_Activity]
(
[ActivityId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[ActivityLookupId] [int] NULL,
[ReferralId] [int] NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OutComeLookupId] [int] NULL,
[Reason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusHistoryId] [int] NOT NULL,
[ActionHistoryId] [int] NOT NULL,
[IsDocumentAttached] [bit] NULL,
[RecordedBy] [int] NULL,
[RecordedDate] [datetime] NULL,
[CaseId] [int] NULL,
[CaseHistoryId] [int] NULL,
[IsLetterAttached] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_Activity] ADD 
CONSTRAINT [PK_AM_Activity] PRIMARY KEY CLUSTERED  ([ActivityId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Activity] ADD
CONSTRAINT [FK_AM_Activity_AM_CaseHistory] FOREIGN KEY ([CaseHistoryId]) REFERENCES [dbo].[AM_CaseHistory] ([CaseHistoryId])
ALTER TABLE [dbo].[AM_Activity] ADD
CONSTRAINT [FK_AM_Activity_AM_Referral] FOREIGN KEY ([ReferralId]) REFERENCES [dbo].[AM_Referral] ([ReferralId])
GO

ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [ActivityActionHistory] FOREIGN KEY ([ActionHistoryId]) REFERENCES [dbo].[AM_ActionHistory] ([ActionHistoryId])
GO
ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [FK_AM_Activity_AM_Action] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [FK_AM_Activity_AM_LookupCode] FOREIGN KEY ([ActivityLookupId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO

ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [FK_AM_Activity_AM_Case] FOREIGN KEY ([CaseId]) REFERENCES [dbo].[AM_Case] ([CaseId])
GO
ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [FK_AM_Activity_AM_LookupCode1] FOREIGN KEY ([OutComeLookupId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [FK_AM_Activity_AM_Resource] FOREIGN KEY ([RecordedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO

ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [ActivityStatusHistory] FOREIGN KEY ([StatusHistoryId]) REFERENCES [dbo].[AM_StatusHistory] ([StatusHistoryId])
GO
ALTER TABLE [dbo].[AM_Activity] ADD CONSTRAINT [FK_AM_Activity_AM_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
GO
