CREATE TABLE [dbo].[AS_UserPatchDevelopment]
(
[UserPatchDevelopmentId] [int] NOT NULL IDENTITY(1, 1),
[EmployeeId] [int] NULL,
[PatchId] [int] NULL,
[DevelopmentId] [int] NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_UserPatchDevelopment] ADD 
CONSTRAINT [PK_AS_UserPatchDevelopment] PRIMARY KEY CLUSTERED  ([UserPatchDevelopmentId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AS_UserPatchDevelopment] WITH NOCHECK ADD
CONSTRAINT [FK_AS_UserPatchDevelopment_AS_User] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[AS_USER] ([EmployeeId])

GO
