CREATE TABLE [dbo].[F_RENTJOURNAL_TO_TXNID]
(
[JOURNALID] [int] NULL,
[TXNID] [int] NULL,
[BATCH_TRANSACTION] [int] NULL CONSTRAINT [DF_Table_1_SINGLE_TRANSACTION] DEFAULT (0)
) ON [PRIMARY]
GO
