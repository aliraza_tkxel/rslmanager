CREATE TABLE [dbo].[NL_PROFITANDLOSSGROUPING_OLD]
(
[GROUPID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NL_PROFITANDLOSSGROUPING_OLD] ADD CONSTRAINT [PK_NL_PROFITANDLOSSGROUPING] PRIMARY KEY CLUSTERED  ([GROUPID]) WITH FILLFACTOR=60 ON [PRIMARY]
GO
