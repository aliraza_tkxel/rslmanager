USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_CaseAction')
BEGIN
CREATE TABLE [dbo].[ASB_CaseAction]
(
	[ActionId] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[DateRecorded] [smalldatetime] NOT NULL,
	[RecoredBy] [int] NOT NULL,
	[ActionType] [int] NOT NULL,
	[Notes] [nvarchar](max) NULL,
	[CaseId] [int] NOT NULL,
	[FileName] NVARCHAR(200) NULL,
	[FilePath] NVARCHAR(200) NULL,
	[StageId] int NULL,
	[SubCategoryId] int NULL,
	[TypeId] int NULL,
	[SubTypeId] int NULL,
	[ReviewDate] [smalldatetime] NULL
) 
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD  CONSTRAINT [FK_ASB_CaseAction_ASB_Case] FOREIGN KEY([CaseId])
REFERENCES [dbo].[ASB_Case] ([CaseId])
ALTER TABLE [dbo].[ASB_CaseAction] CHECK CONSTRAINT [FK_ASB_CaseAction_ASB_Case]
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD  CONSTRAINT [FK_ASB_CaseAction_ASB_CaseActionType] FOREIGN KEY([ActionType])
REFERENCES [dbo].[ASB_CaseActionType] ([ActionTypeId])
ALTER TABLE [dbo].[ASB_CaseAction] CHECK CONSTRAINT [FK_ASB_CaseAction_ASB_CaseActionType]
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD  CONSTRAINT [FK_ASB_CaseAction_E__EMPLOYEE] FOREIGN KEY([RecoredBy])
REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])
ALTER TABLE [dbo].[ASB_CaseAction] CHECK CONSTRAINT [FK_ASB_CaseAction_E__EMPLOYEE]
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([StageId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([TypeId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([SubTypeId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
PRINT 'Table Created'
END
ELSE
BEGIN
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'StageId'
      AND Object_ID = Object_ID(N'ASB_CaseAction'))
BEGIN
    ALTER TABLE [dbo].[ASB_CaseAction]
	ADD [StageId] int NULL
	ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([StageId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	
	PRINT 'Added Column StageId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SubCategoryId'
      AND Object_ID = Object_ID(N'ASB_CaseAction'))
BEGIN
    ALTER TABLE [dbo].[ASB_CaseAction]
	ADD [SubCategoryId] int NULL
	ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([SubCategoryId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	PRINT 'Added Column SubCategoryId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'TypeId'
      AND Object_ID = Object_ID(N'ASB_CaseAction'))
BEGIN
    ALTER TABLE [dbo].[ASB_CaseAction]
	ADD [TypeId] int NULL
	ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([TypeId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	PRINT 'Added Column TypeId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SubTypeId'
      AND Object_ID = Object_ID(N'ASB_CaseAction'))
BEGIN
    ALTER TABLE [dbo].[ASB_CaseAction]
	ADD [SubTypeId] int NULL
	ALTER TABLE [dbo].[ASB_CaseAction]  WITH CHECK ADD FOREIGN KEY([SubTypeId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	PRINT 'Added Column SubTypeId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ReviewDate'
      AND Object_ID = Object_ID(N'ASB_CaseAction'))
BEGIN
    ALTER TABLE [dbo].[ASB_CaseAction]
	ADD [ReviewDate] [smalldatetime] NULL
	PRINT 'Added Column ReviewDate'
END
PRINT 'Table already exist..'
END






IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH