BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Pdr_Status')
BEGIN
	CREATE TABLE [dbo].[PDR_STATUS](
		[STATUSID] [int] IDENTITY(1,1) primary key NOT NULL,
		[TITLE] [varchar](100) NOT NULL,
		[CREATIONDATE] [smalldatetime] NULL,
		[CREATEDBY] [int] NULL,
		[AppTitle] [varchar](100) NULL
		)
	  PRINT 'Table Created';
 END
 IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Pdr_Status')
 BEGIN
	
IF EXISTS (Select 1 from Pdr_Status where Title='Logged')
	BEGIN  
		PRINT 'Title already exist'
	END
ELSE IF NOT EXISTS (Select 1 from Pdr_Status where Title='Logged')
	BEGIN 	
	INSERT INTO Pdr_Status (TITLE,CREATIONDATE,CREATEDBY,AppTitle) VALUES('Logged',GETDATE(),Null,Null)
	PRINT 'New Title added successfully'
END

IF NOT EXISTS (Select 1 from Pdr_Status where Title='Accepted')
	BEGIN 	
	INSERT INTO Pdr_Status (TITLE,CREATIONDATE,CREATEDBY,AppTitle) VALUES('Accepted',GETDATE(),Null,'Accepted')
	PRINT 'New Title Accepted added successfully'
END

IF NOT EXISTS (Select 1 from Pdr_Status where Title='To Be Approved')
	BEGIN 	
	INSERT INTO Pdr_Status (TITLE,CREATIONDATE,CREATEDBY,AppTitle) VALUES('To Be Approved',GETDATE(),Null,'To Be Approved')
	PRINT 'New Title To Be Approved added successfully'
END

IF NOT EXISTS (Select 1 from Pdr_Status where Title='Approved')
	BEGIN 	
	INSERT INTO Pdr_Status (TITLE,CREATIONDATE,CREATEDBY,AppTitle) VALUES('Approved',GETDATE(),Null,'Approved')
	PRINT 'New Title Approved added successfully'
END

IF NOT EXISTS (Select 1 from Pdr_Status where Title='Rejected')
	BEGIN 	
	INSERT INTO Pdr_Status (TITLE,CREATIONDATE,CREATEDBY,AppTitle) VALUES('Rejected',GETDATE(),Null,'Rejected')
	PRINT 'New Title Rejected added successfully'
END
 
END -- 2nd if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH






