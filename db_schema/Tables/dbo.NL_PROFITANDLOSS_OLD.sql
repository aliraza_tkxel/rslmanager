CREATE TABLE [dbo].[NL_PROFITANDLOSS_OLD]
(
[PLID] [int] NOT NULL IDENTITY(1, 1),
[ACCOUNTID] [int] NULL,
[ACCOUNTNAME] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORDERID] [int] NULL,
[SUBGROUPID] [int] NULL,
[GROUPID] [int] NULL,
[SIGNAGE] [int] NULL CONSTRAINT [DF_NL_PROFITANDLOSS_SIGNAGE] DEFAULT (2),
[ACTIVE] [int] NOT NULL CONSTRAINT [DF_NL_PROFITANDLOSS_ACTIVE] DEFAULT (1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NL_PROFITANDLOSS_OLD] ADD CONSTRAINT [PK_NL_PROFITANDLOSS] PRIMARY KEY CLUSTERED  ([PLID]) WITH FILLFACTOR=60 ON [PRIMARY]
GO
