CREATE TABLE [dbo].[F_DDFILES]
(
[FILEID] [int] NOT NULL IDENTITY(1, 1),
[FILEDATE] [smalldatetime] NULL,
[FILENAME] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILETYPE] [int] NULL,
[FILECOUNT] [int] NULL,
[FILEAMOUNT] [float] NULL,
[PROCESSDATE] [smalldatetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[F_DDFILES] ADD CONSTRAINT [PK_F_DDFILES] PRIMARY KEY NONCLUSTERED  ([FILEID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
