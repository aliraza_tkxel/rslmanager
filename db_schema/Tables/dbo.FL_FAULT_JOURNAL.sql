CREATE TABLE [dbo].[FL_FAULT_JOURNAL]
(
[JournalID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FaultLogID] [int] NULL,
[CustomerID] [int] NULL,
[TenancyID] [int] NULL,
[PropertyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemID] [int] NULL,
[ItemNatureID] [int] NULL,
[FaultStatusID] [int] NULL,
[LetterAction] [int] NULL,
[CreationDate] [smalldatetime] NULL,
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastActionDate] [smalldatetime] NULL,
[NextActionDate] [smalldatetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_JOURNAL] ADD 
CONSTRAINT [PK_FL_FAULT_JOURNAL] PRIMARY KEY CLUSTERED  ([JournalID]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_FL_FAULT_JOURNAL_FAULTLOGID_ITEMNATUREID] ON [dbo].[FL_FAULT_JOURNAL] ([FaultLogID]) INCLUDE ([ItemNatureID]) WITH (FILLFACTOR=100) ON [PRIMARY]



GO
