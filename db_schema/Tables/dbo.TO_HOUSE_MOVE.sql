CREATE TABLE [dbo].[TO_HOUSE_MOVE]
(
[TransferID] [int] NOT NULL IDENTITY(1, 1),
[LocalAuthorityID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DevelopmentID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoOfBedrooms] [int] NULL,
[OccupantsNoGreater18] [int] NULL,
[OccupantsNoBelow18] [int] NULL,
[EnquiryLogID] [int] NULL,
[LookUpValueID] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[TO_HOUSE_MOVE] ADD 
CONSTRAINT [PK_TO_Transfer] PRIMARY KEY CLUSTERED  ([TransferID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[TO_HOUSE_MOVE] ADD
CONSTRAINT [FK_TO_Transfer_TO_ENQUIRY_LOG] FOREIGN KEY ([EnquiryLogID]) REFERENCES [dbo].[TO_ENQUIRY_LOG] ([EnquiryLogID])
ALTER TABLE [dbo].[TO_HOUSE_MOVE] ADD
CONSTRAINT [FK_TO_Transfer_TO_LOOKUP_VALUE] FOREIGN KEY ([LookUpValueID]) REFERENCES [dbo].[TO_LOOKUP_VALUE] ([LookUpValueID])
GO
