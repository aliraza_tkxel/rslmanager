CREATE TABLE [dbo].[TBL_PROPERTY_PARAMITEM_DATES]
(
[PropertyID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EstimatedCost] [money] NULL,
[LastDone] [datetime] NULL,
[DueDate] [datetime] NULL,
[Frequency] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemID] [int] NULL,
[ParameterID] [int] NULL,
[ValueID] [bigint] NULL
) ON [PRIMARY]
GO
