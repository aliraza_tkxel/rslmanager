USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AS_JOURNAL')
BEGIN
CREATE TABLE [dbo].[AS_JOURNAL](
	[JOURNALID] [int] IDENTITY(1,1) NOT NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[STATUSID] [int] NULL,
	[ACTIONID] [int] NULL,
	[INSPECTIONTYPEID] [smallint] NULL,
	[CREATIONDATE] [smalldatetime] NULL,
	[CREATEDBY] [int] NULL,
	[ISCURRENT] [bit] NULL,
 CONSTRAINT [PK_AS_JOURNAL] PRIMARY KEY CLUSTERED 
(
	[JOURNALID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[AS_JOURNAL]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNAL_AS_ACTION_ActionId] FOREIGN KEY([ACTIONID])
REFERENCES [dbo].[AS_Action] ([ActionId])


ALTER TABLE [dbo].[AS_JOURNAL] CHECK CONSTRAINT [FK_AS_JOURNAL_AS_ACTION_ActionId]


ALTER TABLE [dbo].[AS_JOURNAL]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNAL_AS_STATUS_StatusId] FOREIGN KEY([STATUSID])
REFERENCES [dbo].[AS_Status] ([StatusId])


ALTER TABLE [dbo].[AS_JOURNAL] CHECK CONSTRAINT [FK_AS_JOURNAL_AS_STATUS_StatusId]

PRINT 'Table Created'

END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'isLock'
      AND Object_ID = Object_ID(N'AS_JOURNAL'))
BEGIN
    ALTER TABLE [dbo].[AS_JOURNAL]
	ADD [isLock] [int] not NULL default (0)
	PRINT 'Added Column [isLock]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'isScheduled'
      AND Object_ID = Object_ID(N'AS_JOURNAL'))
BEGIN
    ALTER TABLE [dbo].[AS_JOURNAL]
	ADD [isScheduled] [int] not NULL default (0)
	PRINT 'Added Column [isScheduled]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'LockTime'
      AND Object_ID = Object_ID(N'AS_JOURNAL'))
BEGIN
    ALTER TABLE [dbo].[AS_JOURNAL]
	ADD [LockTime] [smalldatetime] NULL
	PRINT 'Added Column [LockTime]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'LockedBy'
      AND Object_ID = Object_ID(N'AS_JOURNAL'))
BEGIN
    ALTER TABLE [dbo].[AS_JOURNAL]
	ADD [LockedBy] [int] NULL
	PRINT 'Added Column [LockedBy]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Notes'
      AND Object_ID = Object_ID(N'AS_JOURNAL'))
BEGIN
    ALTER TABLE [dbo].[AS_JOURNAL]
	ADD [Notes] varchar(max) NULL
	PRINT 'Added Column [Notes]'
END

declare @datatype varchar(max) = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'AS_JOURNAL' AND 
     COLUMN_NAME = 'CREATIONDATE')
if  (@datatype = 'smalldatetime')
begin
	ALTER TABLE AS_JOURNAL
	ALTER COLUMN CREATIONDATE datetime
	Print 'Logged Date Data type changed'
END
IF COL_LENGTH('AS_JOURNAL', 'SchemeId') IS NULL
	BEGIN
			ALTER TABLE AS_JOURNAL ADD SchemeId int NULL 
			PRINT('COLUMN SchemeId CREATED')
	END

IF COL_LENGTH('AS_JOURNAL', 'BlockId') IS NULL
	BEGIN
			ALTER TABLE AS_JOURNAL ADD BlockId int NULL 
			PRINT('COLUMN BlockId CREATED')
	END


--=============================================================================
-- US439 - Adding new column ServicingTypeId
--=============================================================================
	DECLARE @AlternativeServicingTypeId INT
	DECLARE @GasServicingTypeId INT
	DECLARE @OilServicingTypeId INT

	SELECT @AlternativeServicingTypeId = ServicingTypeID FROM P_SERVICINGTYPE WHERE Description = 'Alternative Servicing'
	SELECT @OilServicingTypeId = ServicingTypeID FROM P_SERVICINGTYPE WHERE Description = 'Oil'
	SELECT @GasServicingTypeId = ServicingTypeID FROM P_SERVICINGTYPE WHERE Description = 'Gas'

IF NOT EXISTS(
    SELECT	*
    FROM	sys.columns 
    WHERE	Name  = N'ServicingTypeId' AND Object_ID = Object_ID(N'AS_JOURNAL'))
BEGIN

	ALTER TABLE AS_JOURNAL ADD ServicingTypeId INT NULL 
	PRINT('COLUMN ServicingTypeId CREATED for AS_JOURNAL')

	ALTER TABLE AS_JOURNALHISTORY ADD ServicingTypeId INT NULL 
	PRINT('COLUMN ServicingTypeId CREATED for AS_JOURNALHISTORY')

	EXEC('	UPDATE  AS_JOURNAL
	SET     AS_JOURNAL.ServicingTypeId =  CASE WHEN IsAlterNativeHeating = 1 THEN '+ @AlternativeServicingTypeId + '
									WHEN ValueDetail = ''Mains Gas'' THEN '+ @GasServicingTypeId + '
									WHEN ValueDetail IS NULL THEN '+ @GasServicingTypeId + '
									WHEN ValueDetail = ''Oil'' THEN  '+  @OilServicingTypeId + '
									ELSE NULL
								END
	FROM	AS_JOURNAL AJ
			LEFT JOIN (SELECT	JOURNALID, MIN(HeatingMappingId) HeatingMappingId
						FROM	AS_journalHeatingMapping
						GROUP BY JOURNALID
			) AS_journalHeatingMapping ON AJ.JOURNALID = AS_journalHeatingMapping.JOURNALID
			LEFT JOIN pa_heatingMapping on AS_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
			LEFT JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	 ')

		


    EXEC(' UPDATE  AS_JOURNALHISTORY
		SET     AS_JOURNALHISTORY.ServicingTypeId =  CASE WHEN IsAlterNativeHeating = 1 THEN '+ @AlternativeServicingTypeId + '
									WHEN ValueDetail = ''Mains Gas'' THEN '+ @GasServicingTypeId + '
									WHEN ValueDetail IS NULL THEN '+ @GasServicingTypeId + '
									WHEN ValueDetail = ''Oil'' THEN  '+  @OilServicingTypeId + '
									ELSE NULL
								END
	FROM	AS_JOURNALHISTORY AJ
			LEFT JOIN (SELECT	JOURNALID, MIN(HeatingMappingId) HeatingMappingId
						FROM	AS_journalHeatingMapping
						GROUP BY JOURNALID
			) AS_journalHeatingMapping ON AJ.JOURNALID = AS_journalHeatingMapping.JOURNALID
			LEFT JOIN pa_heatingMapping on AS_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
			LEFT JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	 ')


END

IF EXISTS (SELECT * from AS_JOURNAL
	WHERE ServicingTypeId IS NULL)
BEGIN
	UPDATE AS_JOURNAL
	SET	AS_JOURNAL.ServicingTypeId =  CASE WHEN PV.IsAlterNativeHeating = 1 THEN @AlternativeServicingTypeId
										WHEN PV.ValueDetail = 'Mains Gas' THEN @GasServicingTypeId
										WHEN PV.ValueDetail = 'Oil' THEN  @OilServicingTypeId 
										ELSE NULL
									END
	FROM AS_JOURNAL 
	INNER JOIN AS_JournalHeatingMapping JHM ON JHM.JournalId = AS_JOURNAL.JOURNALID
	INNER JOIN PA_HeatingMapping HM on HM.HeatingMappingId = JHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = HM.HeatingType
	WHERE AS_JOURNAL.ServicingTypeId IS NULL
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




