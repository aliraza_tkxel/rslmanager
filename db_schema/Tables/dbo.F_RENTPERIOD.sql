CREATE TABLE [dbo].[F_RENTPERIOD]
(
[PERIOD] [int] NOT NULL,
[STARTDATE] [smalldatetime] NULL,
[ENDDATE] [smalldatetime] NULL
) ON [PRIMARY]
GO
