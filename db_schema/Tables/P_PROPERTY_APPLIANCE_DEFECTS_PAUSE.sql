Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_PROPERTY_APPLIANCE_DEFECTS_PAUSE')
BEGIN

CREATE TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS_PAUSE](
	[PauseId] [int] IDENTITY(1,1) NOT NULL,
	[PauseBy] [int] NOT NULL,
	[PauseOn] [smalldatetime] NULL,
	[PauseReasonId] [int] NOT NULL,
	[PauseNote] [nvarchar](1000) NOT NULL,
	[DefectHistoryId] [int] NOT NULL,
 CONSTRAINT [PK_P_PROPERTY_APPLIANCE_DEFECTS_PAUSE] PRIMARY KEY CLUSTERED 
(
	[PauseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS_PAUSE]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PAUSE_E__EMPLOYEE] FOREIGN KEY([PauseBy])
REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])

ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS_PAUSE] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PAUSE_E__EMPLOYEE]

ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS_PAUSE]  WITH CHECK ADD  CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PAUSE_P_PROPERTY_APPLIANCE_DEFECTS_HISTORY] FOREIGN KEY([DefectHistoryId])
REFERENCES [dbo].[P_PROPERTY_APPLIANCE_DEFECTS_HISTORY] ([DefectHistoryId])

ALTER TABLE [dbo].[P_PROPERTY_APPLIANCE_DEFECTS_PAUSE] CHECK CONSTRAINT [FK_P_PROPERTY_APPLIANCE_DEFECTS_PAUSE_P_PROPERTY_APPLIANCE_DEFECTS_HISTORY]

END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_NATURE')
BEGIN
	
	ALTER TABLE P_PROPERTY_APPLIANCE_DEFECTS_PAUSE
	ALTER COLUMN PauseOn smalldatetime;

END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH