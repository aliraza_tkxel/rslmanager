USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_StagesLookup')
BEGIN
CREATE TABLE [dbo].[ASB_StagesLookup]
(
	[StagesLookupId] [int] IDENTITY(1,1)PRIMARY KEY NOT NULL,
	[Headline] [nvarchar](100) not null,
	[StageTypeId] [int] not null,
	[StageParentId] [int] null
) 

ALTER TABLE [dbo].[ASB_StagesLookup] WITH CHECK ADD FOREIGN KEY([StageTypeId])
REFERENCES [dbo].[ASB_StageCategory] ([StageCategoryId])

ALTER TABLE [dbo].[ASB_StagesLookup] WITH CHECK ADD FOREIGN KEY([StageParentId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])

INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'1. Initial Assessment', 1, null)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Vulnerability Screening', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Risk Assessment Matrix', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Action Plan', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Phone', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Text', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Email', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Letter', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Meeting', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Visit', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'2. Diagnosis', 1, null)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Noise App', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Log Sheets Issued', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Professional Witness', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Sound Monitoring Equipment', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Phone', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Text', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Email', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Letter', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Meeting', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Visit', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Other - Third Party Reports', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'3. Intervention', 1, null)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Early Intervention', 2, 25)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Win-Win Agreement', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Informal Warning', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Acceptable Behaviour Agreement', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Parenting Agreement', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Phone', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Text', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Email', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Letter', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Meeting', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Visit', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Other - Third Party Reports', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Evidence', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 3, 26)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Tenancy Related Action', 2, 25)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'NISP', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Eviction Request', 4, 41)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Undertaking', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Demotion', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Eviction Request', 4, 44)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'S.21', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Eviction Request', 4, 46)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Exclusion Order', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Closure Order', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Eviction', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Phone', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Text', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Email', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Letter', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Meeting', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Visit', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Other - Third Party Reports', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Evidence', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 3, 40)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Enforcement Action', 2, 25)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Civil Injunction', 3, 60)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Community Protection Notice', 3, 60)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Evidence', 3, 60)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 3, 60)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Referral Action', 2, 25)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Conflict Resolution ', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'ASBAG', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'MARAC', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Environmental Health', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Police', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Other Support Agency', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Evidence', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 3, 65)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'4. Legal', 1, null)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Application to court', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Court Hearing', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Bailiffs Warrant', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Re Issue Bailiffs Warrant', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Evidence', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Review', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Close Case', 2, 74)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Close Case', 2, 1)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Close Case', 2, 12)
INSERT [dbo].[ASB_StagesLookup] ([Headline],[StageTypeId],[StageParentId]) VALUES ( N'Close Case', 2, 25)


PRINT 'Table Created'
END

ELSE
BEGIN
PRINT 'Table already exist..'
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH