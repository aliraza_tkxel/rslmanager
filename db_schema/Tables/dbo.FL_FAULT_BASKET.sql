CREATE TABLE [dbo].[FL_FAULT_BASKET]
(
[FaultBasketID] [int] NOT NULL IDENTITY(1, 1),
[CustomerID] [int] NULL,
[PreferredContactDetails] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmitDate] [datetime] NULL,
[IamHappy] [bit] NULL,
[CancelReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_BASKET] ADD 
CONSTRAINT [PK_FL_FAULT_BASKET] PRIMARY KEY CLUSTERED  ([FaultBasketID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
