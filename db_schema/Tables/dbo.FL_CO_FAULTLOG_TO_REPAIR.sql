CREATE TABLE [dbo].[FL_CO_FAULTLOG_TO_REPAIR]
(
[FaultLogToRepairID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FaultLogID] [int] NULL,
[FaultRepairListID] [int] NULL,
[Quantity] [int] NULL,
[InspectionDate] [datetime] NULL,
[InspectionTime] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetCost] [float] NULL,
[DueDate] [datetime] NULL,
[Notes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Approved] [bit] NULL,
[IsAppointmentSaved] [bit] NULL,
[Reason] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Recharge] [bit] NULL,
[UserId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_CO_FAULTLOG_TO_REPAIR] ADD 
CONSTRAINT [PK_FL_CO_FUALT_TO_REPAIR] PRIMARY KEY CLUSTERED  ([FaultLogToRepairID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
