CREATE TABLE [dbo].[F_DEVELOPMENT_RATES]
(
[HEADID] [int] NOT NULL,
[INTERESTRATE] [float] NULL,
[GRANT] [float] NULL,
[LOAN] [float] NULL,
[GRANTFROM] [int] NULL
) ON [PRIMARY]
GO
