USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

DECLARE @LoginId INT, @password varchar(100)
DECLARE @LoginDetail CURSOR
SET @LoginDetail = CURSOR FOR
	SELECT LOGINID,PASSWORD FROM AC_LOGINS --where EMPLOYEEID  IN (68,113)
OPEN @LoginDetail
FETCH NEXT
	FROM @LoginDetail INTO @LoginId,@password
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		declare @source varbinary(max), @encodedPassword varchar(max)
		set @source = convert(varbinary(max), @password)
		set @encodedPassword = cast(''as xml).value('xs:string(sql:variable(''@source''))', 'varchar(max)')

		Update AC_LOGINS SET PASSWORD=@encodedPassword where LOGINID=@LoginId
		Print 'Password: '+ @password+'   EncryptedPassword: '+ @encodedPassword
		FETCH NEXT
		FROM @LoginDetail INTO @LoginId,@password
	END
CLOSE @LoginDetail
DEALLOCATE @LoginDetail


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

