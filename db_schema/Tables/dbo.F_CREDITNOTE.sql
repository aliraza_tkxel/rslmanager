BEGIN TRANSACTION 
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_CREDITNOTE')
BEGIN

		CREATE TABLE [dbo].[F_CREDITNOTE]
		(
		[CNID] [int] NOT NULL IDENTITY(1, 1),
		[ORDERID] [int] NULL,
		[CNNAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CNDATE] [smalldatetime] NULL,
		[CNNOTES] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[USERID] [int] NULL,
		[SUPPLIERID] [int] NULL,
		[ACTIVE] [int] NULL,
		[CNTYPE] [int] NULL,
		[CNSTATUS] [int] NULL,
		[CNTIMESTAMP] [datetime] NOT NULL CONSTRAINT [DF_F_CREDITNOTE_CNTIMESTAMP] DEFAULT (getdate()),
		[CompanyId] [int] NULL
		) ON [PRIMARY]

		ALTER TABLE [dbo].[F_CREDITNOTE] ADD CONSTRAINT [PK_F_CREDITNOTE] PRIMARY KEY CLUSTERED  ([CNID]) WITH FILLFACTOR=90 ON [PRIMARY]
END
		If NOT EXISTS (Select 1 from sys.columns
			WHERE Name =N'CancelBy' 
			And object_ID=object_ID(N'F_CREDITNOTE'))
			Begin 
			Alter table [dbo].[F_CREDITNOTE] Add CancelBy int  NUll default null;
			print ('Added CancelBy in F_CREDITNOTE.')
			END


		If NOT EXISTS (Select 1 from sys.columns
			WHERE Name =N'Reason' 
			And object_ID=object_ID(N'F_CREDITNOTE'))
			Begin 
			Alter table [dbo].[F_CREDITNOTE] Add Reason Nvarchar(200)  NUll default null;
			print ('Added Reason in F_CREDITNOTE.')
			END

		If NOT EXISTS (Select 1 from sys.columns
			WHERE Name =N'CancelDate' 
			And object_ID=object_ID(N'F_CREDITNOTE'))
			Begin 
			Alter table [dbo].[F_CREDITNOTE] Add CancelDate datetime NUll default null;
			print ('Added CancelDate in F_CREDITNOTE.')
			END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_CREDITNOTE'))
BEGIN

alter table dbo.F_CREDITNOTE add CompanyId [int] NULL

end
