BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_COSTCENTRE_ALLOCATION')
BEGIN

CREATE TABLE [dbo].[F_COSTCENTRE_ALLOCATION]
(
[CCBUDID] [int] NOT NULL IDENTITY(1, 1),
[COSTCENTREID] [int] NULL,
[FISCALYEAR] [int] NULL,
[MODIFIED] [smalldatetime] NULL,
[MODIFIEDBY] [int] NULL,
[COSTCENTREALLOCATION] [money] NULL,
[ACTIVE] [bit] NULL
) ON [PRIMARY]

END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


GO


IF not EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_COSTCENTRE_ALLOCATION'))
BEGIN

alter table F_COSTCENTRE_ALLOCATION add CompanyId INT

END