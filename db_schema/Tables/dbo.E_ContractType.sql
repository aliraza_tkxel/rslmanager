USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_ContractType')
BEGIN
	CREATE TABLE [dbo].E_ContractType(
		[ContractTypeId] [int] IDENTITY(1,1) NOT NULL,
		[ContractTypeDescription] [varchar](50) NULL,
	 CONSTRAINT [PK_E_ContractType] PRIMARY KEY CLUSTERED 
	(
		[ContractTypeId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY]

END   
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_ContractType')
BEGIN
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='BHA Post April 17')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('BHA Post April 17')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='BHA Post April 17 (BFS)')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('BHA Post April 17 (BFS)')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='BHA Post Jun 12')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('BHA Post Jun 12')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='BHA Pre Jun 12')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('BHA Pre Jun 12')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='BRS')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('BRS')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='CEO')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('CEO')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='Exec and BDS Pre April 2017')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('Exec and BDS Pre April 2017')
	END
	IF NOT EXISTS (Select 1 from E_ContractType where ContractTypeDescription='BDS Post April 2017')
	BEGIN 	
		INSERT INTO [E_ContractType] ([ContractTypeDescription])
		VALUES	('BDS Post April 2017')
	END
END 
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

