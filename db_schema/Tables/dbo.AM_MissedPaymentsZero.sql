CREATE TABLE [dbo].[AM_MissedPaymentsZero]
(
[ZeroMissedTenantId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NOT NULL,
[TenantId] [int] NOT NULL,
[PaymentPlanId] [int] NOT NULL,
[PaymentId] [int] NOT NULL,
[MissedPaymentDetectionDate] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_MissedPaymentsZero] ADD 
CONSTRAINT [PK_AM_MissedPaymentsZero] PRIMARY KEY CLUSTERED  ([ZeroMissedTenantId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
