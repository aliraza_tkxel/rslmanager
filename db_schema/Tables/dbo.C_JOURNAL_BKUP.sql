CREATE TABLE [dbo].[C_JOURNAL_BKUP]
(
[JOURNALID] [int] NOT NULL IDENTITY(1, 1),
[CUSTOMERID] [int] NULL,
[TENANCYID] [int] NULL,
[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEMID] [int] NULL,
[ITEMNATUREID] [int] NULL,
[CURRENTITEMSTATUSID] [int] NULL,
[CREATIONDATE] [smalldatetime] NULL,
[TITLE] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
