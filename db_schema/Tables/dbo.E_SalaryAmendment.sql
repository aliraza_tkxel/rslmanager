Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_SalaryAmendment')
BEGIN
 

		BEGIN TRANSACTION
		SET QUOTED_IDENTIFIER ON
		SET ARITHABORT ON
		SET NUMERIC_ROUNDABORT OFF
		SET CONCAT_NULL_YIELDS_NULL ON
		SET ANSI_NULLS ON
		SET ANSI_PADDING ON
		SET ANSI_WARNINGS ON
		COMMIT
		BEGIN TRANSACTION

		CREATE TABLE dbo.E_SalaryAmendment
			(
			SalaryAmendmentId int NOT NULL IDENTITY (1, 1),
			EmployeeId int NOT NULL,
			SalaryAmendmentFile nvarchar(500) NULL,
			Status bit NULL
			)  ON [PRIMARY]

		ALTER TABLE dbo.E_SalaryAmendment ADD CONSTRAINT
			PK_E_SalaryAmendment PRIMARY KEY CLUSTERED 
			(
			SalaryAmendmentId
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


		ALTER TABLE dbo.E_SalaryAmendment SET (LOCK_ESCALATION = TABLE)

		COMMIT
		select Has_Perms_By_Name(N'dbo.E_SalaryAmendment', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.E_SalaryAmendment', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.E_SalaryAmendment', 'Object', 'CONTROL') as Contr_Per 
 PRINT 'Table created successfully!';
END --if
ELSE

	BEGIN
	PRINT 'Table Already Exist';
	END
--========================================================================================================
--========================================================================================================
------Add AmendmentFileName column
--========================================================================================================
--========================================================================================================
 	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_SalaryAmendment]' ) AND name = 'AmendmentFileName')
	BEGIN
	Alter TABLE E_SalaryAmendment Add AmendmentFileName nvarchar(500)
	PRINT 'AmendmentFileName Column Added successfully';
	END
ELSE
	BEGIN
	PRINT 'AmendmentFileName Column Already Exist';
	END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 