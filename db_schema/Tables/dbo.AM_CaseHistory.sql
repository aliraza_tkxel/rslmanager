USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AM_CaseHistory')
    BEGIN

		CREATE TABLE [dbo].[AM_CaseHistory]
		(
		[CaseHistoryId] [int] NOT NULL IDENTITY(1, 1),
		[TennantId] [int] NOT NULL,
		[InitiatedById] [int] NOT NULL,
		[CaseManager] [int] NOT NULL,
		[CaseOfficer] [int] NOT NULL,
		[StatusId] [int] NOT NULL,
		[StatusHistoryId] [int] NOT NULL,
		[StatusReview] [datetime] NOT NULL,
		[StatusRecordedDate] [datetime] NULL,
		[RentBalance] [float] NULL,
		[RecoveryAmount] [float] NOT NULL,
		[NoticeIssueDate] [datetime] NULL,
		[NoticeExpiryDate] [datetime] NULL,
		[WarrantExpiryDate] [datetime] NULL,
		[HearingDate] [datetime] NULL,
		[ActionId] [int] NOT NULL,
		[ActionHistoryId] [int] NOT NULL,
		[ActionReviewDate] [datetime] NOT NULL,
		[ActionRecordedDate] [datetime] NULL,
		[IsPaymentPlan] [bit] NULL,
		[IsPaymentPlanUpdated] [bit] NULL,
		[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OutcomeLookupCodeId] [int] NULL,
		[Reason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedDate] [datetime] NOT NULL,
		[IsSuppressed] [bit] NOT NULL,
		[ModifiedBy] [int] NOT NULL,
		[CaseId] [int] NULL,
		[IsActive] [bit] NULL,
		[IsDocumentUpload] [bit] NULL,
		[IsDocumentAttached] [bit] NULL,
		[SuppressedReason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SuppressedBy] [int] NULL,
		[SuppressedDate] [datetime] NULL,
		[IsActionIgnored] [bit] NULL,
		[ActionIgnoreCount] [int] NULL CONSTRAINT [DF_AM_CaseHistory_ActionIgnoreCount] DEFAULT ((0)),
		[ActionIgnoreReason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActionRecorded] [bit] NULL,
		[RecordedActionDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ActionRecordedCount] [int] NULL CONSTRAINT [DF_AM_CaseHistory_ActionRecordedCount] DEFAULT ((0)),
		[IsActionPostponed] [bit] NULL,
		[IsPaymentPlanIgnored] [bit] NULL,
		[PaymentPlanIgnoreReason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PaymentPlanId] [int] NULL,
		[PaymentPlanHistoryId] [int] NULL,
		[IsCaseUpdated] [bit] NULL,
		[IsPaymentPlanClose] [bit] NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
		ALTER TABLE [dbo].[AM_CaseHistory] ADD 
		CONSTRAINT [PK_AM_CaseHistory] PRIMARY KEY CLUSTERED  ([CaseHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]


		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [CaseHistoryActionHistory] FOREIGN KEY ([ActionHistoryId]) REFERENCES [dbo].[AM_ActionHistory] ([ActionHistoryId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [ActionId] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [Case] FOREIGN KEY ([CaseId]) REFERENCES [dbo].[AM_Case] ([CaseId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [Outcome] FOREIGN KEY ([OutcomeLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [FK_AM_CaseHistory_AM_PaymentPlanHistory] FOREIGN KEY ([PaymentPlanHistoryId]) REFERENCES [dbo].[AM_PaymentPlanHistory] ([PaymentPlanHistoryId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [FK_AM_CaseHistory_AM_PaymentPlan] FOREIGN KEY ([PaymentPlanId]) REFERENCES [dbo].[AM_PaymentPlan] ([PaymentPlanId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [CaseHistoryStatusHistory] FOREIGN KEY ([StatusHistoryId]) REFERENCES [dbo].[AM_StatusHistory] ([StatusHistoryId])

		ALTER TABLE [dbo].[AM_CaseHistory] ADD CONSTRAINT [Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])

 END
ELSE

BEGIN
PRINT 'Table Already Exist';
END
    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_Tenant')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_Tenant
		  ON AM_CaseHistory (TennantId);  
	  PRINT 'IX_AM_CaseHistory_Tenant created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_Tenant Index Already Exist';
	  END
    --========================================================================================================
    --======================================================================================================== 
     --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_CaseManager')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_CaseManager
		  ON AM_CaseHistory (CaseManager);  
	  PRINT 'IX_AM_CaseHistory_CaseManager created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_CaseManager Index Already Exist';
	  END
	  
	  IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_CaseOfficer')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_CaseOfficer
		  ON AM_CaseHistory (CaseOfficer);  
	  PRINT 'IX_AM_CaseHistory_CaseOfficer created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_CaseOfficer Index Already Exist';
	  END
	  
	IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_StatusId')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_StatusId
		  ON AM_CaseHistory (StatusId);  
	  PRINT 'IX_AM_CaseHistory_StatusId created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_StatusId Index Already Exist';
	  END
	  
	  IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_ActionId')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_ActionId
		  ON AM_CaseHistory (ActionId);  
	  PRINT 'IX_AM_CaseHistory_ActionId created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_ActionId Index Already Exist';
	  END
	  
	 IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_ActionHistoryId')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_ActionHistoryId
		  ON AM_CaseHistory (ActionHistoryId);  
	  PRINT 'IX_AM_CaseHistory_ActionHistoryId created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_ActionId Index Already Exist';
	  END
	  
	IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_ActionRecordedCount')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_ActionRecordedCount
		  ON AM_CaseHistory (ActionRecordedCount);  
	  PRINT 'IX_AM_CaseHistory_ActionRecordedCount created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_ActionRecordedCount Index Already Exist';
	  END
	  
	  IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_CaseHistory_ActionIgnoreCount')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_CaseHistory_ActionIgnoreCount
		  ON AM_CaseHistory (ActionIgnoreCount);  
	  PRINT 'IX_AM_CaseHistory_ActionIgnoreCount created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_CaseHistory_ActionIgnoreCount Index Already Exist';
	  END
	  
    --========================================================================================================
    --======================================================================================================== 

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    
