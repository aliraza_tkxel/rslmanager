USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_Gift_Status')
BEGIN

	CREATE TABLE [dbo].[E_Gift_Status](
		[GiftStatusId] [int] IDENTITY(1,1) NOT NULL,
		[Description] [nvarchar](50) NULL
	 CONSTRAINT [PK_GiftStatus] PRIMARY KEY NONCLUSTERED 
	(
		[GiftStatusId] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

END

IF NOT EXISTS (SELECT * FROM E_Gift_Status WHERE Description = 'Rejected')
BEGIN
	insert into E_Gift_Status (Description) VALUES('Rejected')
	PRINT 'Rejected added successfully'
END

IF NOT EXISTS (SELECT * FROM E_Gift_Status WHERE Description = 'Approved')
BEGIN
	insert into E_Gift_Status (Description) VALUES('Approved')
	PRINT 'Approved added successfully'
END

IF NOT EXISTS (SELECT * FROM E_Gift_Status WHERE Description = 'Declared')
BEGIN
	insert into E_Gift_Status (Description) VALUES('Declared')
	PRINT 'Declared added successfully'
END

IF NOT EXISTS (SELECT * FROM E_Gift_Status WHERE Description = 'Pending Approval')
BEGIN
	insert into E_Gift_Status (Description) VALUES('Pending Approval')
	PRINT 'Pending Approval added successfully'
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 