USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_InstallationPipeWork')
BEGIN

CREATE TABLE [dbo].[P_InstallationPipeWork](
	[PipeWorkID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](20) NOT NULL,
	[EmergencyControl] [varchar](5) NOT NULL,
	[VisualInspection] [varchar](5) NOT NULL,
	[GasTightnessTest] [varchar](5) NOT NULL,
	[EquipotentialBonding] [varchar](5) NOT NULL,
	[DateStamp] [smalldatetime] NOT NULL,
	[Journalid] [int] NULL,
 CONSTRAINT [PK_P_InstallationPipeWork] PRIMARY KEY CLUSTERED 
(
	[PipeWorkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF


ALTER TABLE [dbo].[P_InstallationPipeWork]  WITH CHECK ADD  CONSTRAINT [FK_P_InstallationPipeWork_P__PROPERTY] FOREIGN KEY([PropertyId])
REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])


ALTER TABLE [dbo].[P_InstallationPipeWork] CHECK CONSTRAINT [FK_P_InstallationPipeWork_P__PROPERTY]

END


IF EXISTS (SELECT	* 
		   FROM		INFORMATION_SCHEMA.TABLES 
           WHERE	TABLE_NAME = N'P_InstallationPipeWork')
BEGIN


	--===============================================================================================================
	--  10828 - Add schemeId and blockId columns
	--===============================================================================================================
	IF COL_LENGTH('P_InstallationPipeWork', 'schemeId') IS NULL
	BEGIN
			ALTER TABLE P_InstallationPipeWork
			ADD schemeId int NULL
			PRINT('COLUMN schemeId CREATED')
	END

	IF COL_LENGTH('P_InstallationPipeWork', 'blockId') IS NULL
	BEGIN
			ALTER TABLE P_InstallationPipeWork
			ADD blockId int NULL
			PRINT('COLUMN blockId CREATED')
	END

	IF COL_LENGTH('P_InstallationPipeWork', 'heatingMappingId') IS NULL
	BEGIN
			ALTER TABLE P_InstallationPipeWork
			ADD heatingMappingId int NULL
			PRINT('COLUMN heatingMappingId CREATED')

			EXEC('	UPDATE  P_InstallationPipeWork
					SET		heatingMappingId = HeatingProperty.HeatingMappingId
					FROM    P_InstallationPipeWork
							LEFT JOIN (SELECT	PropertyId, MIN(HeatingMappingId) HeatingMappingId 
									   FROM		PA_HeatingMapping 
									   WHERE	propertyid IS NOT NULL 
									   GROUP BY PropertyId ) HeatingProperty ON P_InstallationPipeWork.PropertyId = HeatingProperty.PropertyId ')
	END

	

	IF EXISTS(	SELECT	*
				FROM	(	SELECT  is_nullable 
							FROM    sys.columns 
							WHERE   object_id = object_id('dbo.P_InstallationPipeWork') 
									AND name = 'PropertyId')  ConstraintCheck
				WHERE   ConstraintCheck.is_nullable ='0')
	BEGIN
	   ALTER TABLE P_InstallationPipeWork ALTER COLUMN PropertyId [nvarchar](20) NULL
	   PRINT('PropertyId SCHEMA UPDATED')
	END

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


