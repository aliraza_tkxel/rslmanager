CREATE TABLE [dbo].[REMAINING]
(
[THEID] [int] NULL,
[LOCATION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELEMENT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REAPIR DETAIL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIORITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COST] [money] NULL,
[CC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HEAD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EXP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ccid] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[headid] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[expid] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[repair] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[maintid] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
