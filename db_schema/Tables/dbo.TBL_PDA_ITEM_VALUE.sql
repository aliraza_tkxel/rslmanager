CREATE TABLE [dbo].[TBL_PDA_ITEM_VALUE]
(
[ValueID] [bigint] NOT NULL,
[ParameterID] [int] NOT NULL,
[ValueDetail] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IsDate] [bit] NOT NULL CONSTRAINT [DF_TBL_PDA_ITEM_VALUE_IsDate] DEFAULT ((0)),
[sorder] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_ITEM_VALUE] ADD 
CONSTRAINT [PK_TBL_PDA_ITEM_VALUE] PRIMARY KEY CLUSTERED  ([ValueID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_ITEM_VALUE] WITH NOCHECK ADD
CONSTRAINT [FK_TBL_PDA_ITEM_VALUE_TBL_PDA_ITEM_PARAMETER] FOREIGN KEY ([ParameterID]) REFERENCES [dbo].[TBL_PDA_ITEM_PARAMETER] ([ParameterID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
