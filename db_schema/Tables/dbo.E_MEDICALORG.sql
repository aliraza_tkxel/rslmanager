USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_MEDICALORG]     ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_MEDICALORG')
BEGIN
CREATE TABLE [dbo].[E_MEDICALORG]
(
[MEDICALORGID] [int] NOT NULL IDENTITY(1, 1),
[MEDICALORGNAME] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
END

IF NOT EXISTS (Select * from E_MEDICALORG where MEDICALORGNAME='Health Shiled')
BEGIN  
INSERT INTO E_MEDICALORG (MEDICALORGNAME)VALUES ('Health Shiled')
END 
IF NOT EXISTS (Select * from E_MEDICALORG where MEDICALORGNAME='Axa')
BEGIN  
INSERT INTO E_MEDICALORG (MEDICALORGNAME)VALUES ('Axa')
END 

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH