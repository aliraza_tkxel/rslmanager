USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AS_AbortReason')

BEGIN

	CREATE TABLE [dbo].[AS_AbortReason](
		[AbortReasonId] [int] IDENTITY(1,1) NOT NULL,
		[AbortReason] [varchar](50) NULL,
	 CONSTRAINT [PK_Abort_Reason] PRIMARY KEY CLUSTERED 
	(
		[AbortReasonId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	PRINT('AS_AbortReason Created Successfully')
END


IF not exists (select * FROM AS_AbortReason WHERE AbortReason = 'No Gas/ Electric')
BEGIN
	INSERT INTO AS_AbortReason VALUES('No Gas/ Electric')
END

IF not exists (select * FROM AS_AbortReason WHERE AbortReason = 'Threatening Behaviour')
BEGIN
	INSERT INTO AS_AbortReason VALUES('Threatening Behaviour')
END

IF not exists (select * FROM AS_AbortReason WHERE AbortReason = 'H&S')
BEGIN
	INSERT INTO AS_AbortReason VALUES('H&S')
END

IF not exists (select * FROM AS_AbortReason WHERE AbortReason = 'Property Condition')
BEGIN
	INSERT INTO AS_AbortReason VALUES('Property Condition')
END

IF not exists (select * FROM AS_AbortReason WHERE AbortReason = 'Customer Reason')
BEGIN
	INSERT INTO AS_AbortReason VALUES('Customer Reason')
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
