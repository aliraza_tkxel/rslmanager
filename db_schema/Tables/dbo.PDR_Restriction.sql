USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PDR_Restriction')
BEGIN
CREATE TABLE [dbo].[PDR_Restriction](
	[RestrictionId] [int] IDENTITY(1,1) NOT NULL,
	[PermittedPlanning] [nvarchar](500) NULL,
	[RelevantPlanning] [nvarchar](500) NULL,
	[RelevantTitle] [nvarchar](1000) NULL,
	[RestrictionComments] [nvarchar](1000) NULL,
	[AccessIssues] [nvarchar](1000) NULL,
	[MediaIssues] [nvarchar](1000) NULL,
	[ThirdPartyAgreement] [nvarchar](10) NULL,
	[SpFundingArrangements] [nvarchar](1000) NULL,
	[IsRegistered] [int] NULL,
	[ManagementDetail] [nvarchar](1000) NULL,
	[NonBhaInsuranceDetail] [nvarchar](1000) NULL,
	[PropertyId] [nvarchar](20) NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_PDR_Restriction] PRIMARY KEY CLUSTERED 
(
	[RestrictionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH