CREATE TABLE [dbo].[PS_Property2Appointment]
(
[Property2AppointId] [int] NOT NULL IDENTITY(1, 1),
[AppointId] [int] NOT NULL,
[PropertyId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerId] [int] NULL,
[TenancyId] [int] NULL,
[LastSurveyDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Property2Appointment] ADD 
CONSTRAINT [PK_PS_Property2SurveyForm] PRIMARY KEY CLUSTERED  ([Property2AppointId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Property2Appointment] ADD
CONSTRAINT [FK_PS_Property2Appointment_PS_Appointment] FOREIGN KEY ([AppointId]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO
