Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P__PROPERTY')
BEGIN
  PRINT 'Table Exists';
  IF EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[P__PROPERTY]' ) AND name = 'DEVELOPMENTID')
	  
	  BEGIN
			 PRINT 'Column DEVELOPMENTID Exists';
		  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_P__PROPERTY_P_DEVELOPMENT' )
				BEGIN
					ALTER TABLE [dbo].[P__PROPERTY] 
					DROP CONSTRAINT [FK_P__PROPERTY_P_DEVELOPMENT]
					
					ALTER TABLE [dbo].[P__PROPERTY]  WITH NOCHECK 
					ADD CONSTRAINT [FK_P__PROPERTY_PDR_DEVELOPMENT] FOREIGN KEY([DEVELOPMENTID])
					REFERENCES [dbo].[PDR_DEVELOPMENT] ([DEVELOPMENTID])
					PRINT 'CONSTRAINT on column DEVELOPMENTID  Modified Successfully'
				END--if
			ELSE
				BEGIN
				PRINT 'CONSTRAINT not exist'
				END
	 END
ELSE
BEGIN
PRINT 'column DEVELOPMENTID not exist'
END

declare @datatype varchar(max) = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'P__PROPERTY' AND 
     COLUMN_NAME = 'DATEBUILT')
if  (@datatype = 'smalldatetime')
begin
	ALTER TABLE P__PROPERTY
	ALTER COLUMN DATEBUILT datetime
	Print 'DATEBUILT Data type have changed.'
END
	 
		
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 