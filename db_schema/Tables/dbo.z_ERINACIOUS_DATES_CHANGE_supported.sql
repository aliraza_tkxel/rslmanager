CREATE TABLE [dbo].[z_ERINACIOUS_DATES_CHANGE_supported]
(
[ORDERID] [float] NULL,
[WORK_ORDER] [float] NULL,
[DEVELOPMENTNAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROPERTYID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HOUSENUMBER] [float] NULL,
[ADDRESS1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTCODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CDATE] [smalldatetime] NULL,
[COMPLETIONDATE] [smalldatetime] NULL,
[DIFF] [float] NULL,
[NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REPAIR] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
