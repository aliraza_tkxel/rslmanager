USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GET_DOCUMENT_SUBTYPES]    Script Date: 09-Aug-18 5:21:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Simon Rogers
-- Create date: 1st August 2014
-- Description:	Retrieve Document Types
-- =============================================
IF OBJECT_ID('dbo.P_GET_DOCUMENT_SUBTYPES') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.P_GET_DOCUMENT_SUBTYPES AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[P_GET_DOCUMENT_SUBTYPES]
	-- Add the parameters for the stored procedure here
	@DocumentTypeId INT ,
	@ShowActiveOnly BIT = 1,
	@reportFor varchar (max) = null
AS
BEGIN
		Declare @reportType varchar (8000)
		Declare @selectClause varchar (MAX)
		Declare @whereClause varchar (MAX)
		Declare @orderClause varchar (MAX)
		if @reportFor = 'Development'
			begin
				set @reportType = 'dt.IsDevelopment'
			end
		if @reportFor = 'Property'
			begin
				set @reportType = 'dt.IsProperty'
			end
		if @reportFor = 'Scheme'
			begin
				set @reportType = 'dt.IsScheme'
			end

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @ShowActiveOnly = 1 and @reportFor is not null
		begin
			set @selectClause = 'SELECT -1 as DocumentSubtypeId, 0 AS DocumentTypeId, ''Please select'' AS Title, 1 AS [Active], 0 AS [Order]
			UNION
			SELECT Distinct DocumentSubtypeId, DocumentTypeId, Title, [Active], [Order]
			FROM dbo.P_Documents_Subtype dt ' 
			set @whereClause = ' WHERE 1=1'
			set @whereClause = @whereClause +  ' and [Active] = 1 AND '+ @reportType + ' = 1 and DocumentTypeId = ' ++Convert(Varchar,@DocumentTypeId)+char(10) 
			set @orderClause = ' ORDER BY [Order], [Title]'

			Declare @mainQuery varchar (MAX)
			set @mainQuery = @selectClause +@whereClause+@orderClause
			Print(@mainQuery)
			exec(@mainQuery)
		end
	ELSE
		begin
			set @selectClause = 'SELECT -1 as DocumentSubtypeId, 0 AS DocumentTypeId, ''Please select'' AS Title, 1 AS [Active], 0 AS [Order]
			UNION
			SELECT DocumentSubtypeId, DocumentTypeId, Title, [Active], [Order]
			FROM dbo.P_Documents_Subtype dt ' 
			set @whereClause = ' WHERE 1=1 '
			set @whereClause = @whereClause +  ' and DocumentTypeId = ' ++Convert(Varchar,@DocumentTypeId)+char(10) 
			set @orderClause = ' ORDER BY [Order], [Title] '
			set @mainQuery = @selectClause +@whereClause+@orderClause
			Print(@mainQuery)
			exec(@mainQuery)
		end
	
END



