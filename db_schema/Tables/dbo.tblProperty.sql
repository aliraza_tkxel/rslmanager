CREATE TABLE [dbo].[tblProperty]
(
[ID] [int] NOT NULL,
[PropertyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkToSchemeID] [int] NULL,
[LinkToPatch] [int] NULL,
[Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BuildYear] [smallint] NULL,
[InsuranceValue] [money] NULL,
[Sap] [smallint] NULL,
[Nher] [float] NULL,
[Bepi] [smallint] NULL,
[CO2_Tonnes] [float] NULL,
[TotalRunningCosts] [money] NULL,
[Classification] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supported] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
