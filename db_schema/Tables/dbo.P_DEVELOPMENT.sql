BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_DEVELOPMENT')
BEGIN

		CREATE TABLE [dbo].[P_DEVELOPMENT]
		(
		[DEVELOPMENTID] [int] NOT NULL IDENTITY(1, 1),
		[PATCHID] [int] NULL,
		[DEVELOPMENTNAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SCHEMENAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SCHEMECODE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DEVELOPMENTTYPE] [int] NULL,
		[STARTDATE] [smalldatetime] NULL,
		[TARGETDATE] [smalldatetime] NULL,
		[ACTUALDATE] [smalldatetime] NULL,
		[DEFECTSPERIOD] [smalldatetime] NULL,
		[NUMBEROFUNITS] [float] NULL,
		[USETYPE] [int] NULL,
		[SUITABLEFOR] [int] NULL,
		[LOCALAUTHORITY] [int] NULL,
		[LEADDEVOFFICER] [int] NULL,
		[SUPPORTDEVOFFICER] [int] NULL,
		[PROJECTMANAGER] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CONTRACTOR] [int] NULL,
		[INSURANCEVALUE] [money] NULL,
		[AQUISITIONCOST] [money] NULL,
		[CONTRACTORCOST] [money] NULL,
		[OTHERCOST] [money] NULL,
		[SCHEMECOST] [money] NULL,
		[DEVELOPMENTADMIN] [money] NULL,
		[INTERESTCOST] [money] NULL,
		[BALANCE] [money] NULL,
		[HAG] [money] NULL,
		[MORTGAGE] [money] NULL,
		[BORROWINGRATE] [money] NULL,
		[NETTCOST] [money] NULL,
		[SURVEYOR] [int] NULL,
		[SUPPORTEDCLIENTS] [int] NULL
		) ON [PRIMARY]
		
		ALTER TABLE [dbo].[P_DEVELOPMENT] ADD CONSTRAINT [PK_P_DEVELOPMENT] PRIMARY KEY CLUSTERED  ([DEVELOPMENTID]) WITH (FILLFACTOR=60) ON [PRIMARY]
		
		CREATE NONCLUSTERED INDEX [IX_P_DEVELOPMENT_LOCALAUTHORITY] ON [dbo].[P_DEVELOPMENT] ([LOCALAUTHORITY]) WITH (FILLFACTOR=60) ON [PRIMARY]
		
		CREATE NONCLUSTERED INDEX [IX_P_DEVELOPMENT_PATCHID] ON [dbo].[P_DEVELOPMENT] ([PATCHID]) WITH (FILLFACTOR=60) ON [PRIMARY]
		
		GRANT SELECT ON  [dbo].[P_DEVELOPMENT] TO [rackspace_datareader]
		
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'P_DEVELOPMENT'))
BEGIN

alter table P_DEVELOPMENT add CompanyId [int] NULL

END



