Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_NATURE')
BEGIN

CREATE TABLE [dbo].[E_NATURE](
	[ITEMNATUREID] [int] IDENTITY(1,1) NOT NULL,
	[ITEMID] [int] NULL,
	[DESCRIPTION] [nvarchar](35) NULL,
	[EMPLOYEEONLY] [int] NULL,
	[APPROVALREQUIRED] [int] NULL,
 CONSTRAINT [PK_ITEMNATURE] PRIMARY KEY NONCLUSTERED 
(
	[ITEMNATUREID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 30) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[E_NATURE]  WITH NOCHECK ADD  CONSTRAINT [FK_ITEMNATURE_ITEM] FOREIGN KEY([ITEMID])
REFERENCES [dbo].[E_ITEM] ([ITEMID])


ALTER TABLE [dbo].[E_NATURE] NOCHECK CONSTRAINT [FK_ITEMNATURE_ITEM]

END


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_NATURE')
BEGIN


	--====================================================================================
	-- Ticket # US368 Added 'Adoption Leave', 'Shared Parental Leave' and 'Parental Leave'
	--====================================================================================
  
	IF NOT EXISTS (	SELECT	1 
					FROM	E_NATURE 
					WHERE	DESCRIPTION ='Adoption Leave')
	BEGIN 		
		INSERT INTO [dbo].[E_NATURE] ([ITEMID],[DESCRIPTION],[EMPLOYEEONLY],[APPROVALREQUIRED])
		VALUES (1,'Adoption Leave',1,1)		
		PRINT 'Adoption Leave successfully added.'
	END

	IF NOT EXISTS (	SELECT	1 
					FROM	E_NATURE 
					WHERE	DESCRIPTION ='Shared Parental Leave')
	BEGIN 		
		INSERT INTO [dbo].[E_NATURE] ([ITEMID],[DESCRIPTION],[EMPLOYEEONLY],[APPROVALREQUIRED])
		VALUES (1,'Shared Parental Leave',1,1)		
		PRINT 'Shared Parental Leave successfully added.'
	END

	IF NOT EXISTS (	SELECT	1 
					FROM	E_NATURE 
					WHERE	DESCRIPTION ='Parental Leave')
	BEGIN 		
		INSERT INTO [dbo].[E_NATURE] ([ITEMID],[DESCRIPTION],[EMPLOYEEONLY],[APPROVALREQUIRED])
		VALUES (1,'Parental Leave',1,1)		
		PRINT 'Parental Leave successfully added.'
	END

	--====================================================================================
	-- Ticket # US367 Update 'Maternity - Ord' to 'Maternity'
	--====================================================================================
	IF EXISTS (	SELECT	1 
				FROM	E_NATURE 
				WHERE	DESCRIPTION ='Maternity - Ord')
	BEGIN 		
		UPDATE	E_NATURE
		SET		DESCRIPTION	= 'Maternity'
		WHERE   DESCRIPTION = 'Maternity - Ord'

		PRINT 'Maternity - Ord successfully updated.'
	END


END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH