CREATE TABLE [dbo].[AS_DetectorType]
(
[DetectorTypeId] [int] NOT NULL IDENTITY(1, 1),
[DetectorType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_DetectorType] ADD 
CONSTRAINT [PK_AS_DetectorType] PRIMARY KEY CLUSTERED  ([DetectorTypeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
