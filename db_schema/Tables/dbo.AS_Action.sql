USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AS_Action')

BEGIN

CREATE TABLE [dbo].[AS_Action](
	[ActionId] [int] IDENTITY(1,1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Ranking] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[IsEditable] [bit] NULL,
 CONSTRAINT [PK_AS_Action] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[AS_Action]  WITH CHECK ADD  CONSTRAINT [FK_AS_ACTION_AS_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[AS_Status] ([StatusId])
END

IF NOT EXISTS (SELECT * FROM AS_ACTION  WHERE Title = N'Aborted')

BEGIN
	INSERT INTO AS_ACTION (StatusId,Title, Ranking,CreatedDate,CreatedBy,IsEditable) 
	VALUES ((select StatusId from AS_Status where Title = 'Aborted'),'Aborted',1,GETDATE(), 423 , 0)
END



--===================================================================================================================================
-- US439 - change status title of 'CP12 issued' to 'Certificate Issued' because now multiple certificates can exist
--===================================================================================================================================

IF EXISTS (SELECT * FROM AS_ACTION  WHERE Title = N'CP12 issued ')

BEGIN

	UPDATE	AS_ACTION
	SET		Title = 'Certificate issued'
	WHERE	Title = 'CP12 issued '

	PRINT 'Changed status title of CP12 issued to Certificate Issued'

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
