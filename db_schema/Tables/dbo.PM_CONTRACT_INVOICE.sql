CREATE TABLE [dbo].[PM_CONTRACT_INVOICE]
(
[INVOICEID] [int] NOT NULL IDENTITY(1, 1),
[INVOICEDATE] [smalldatetime] NOT NULL CONSTRAINT [DF_PM_CONTRACT_INVOICE_INVOICEDATE] DEFAULT (getdate()),
[BATCHID] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PM_CONTRACT_INVOICE] ADD 
CONSTRAINT [PK_PM_CONTRACT_INVOICE] PRIMARY KEY CLUSTERED  ([INVOICEID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
