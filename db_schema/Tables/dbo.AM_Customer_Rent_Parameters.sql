CREATE TABLE [dbo].[AM_Customer_Rent_Parameters]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TenancyId] [int] NULL,
[CustomerId] [int] NULL,
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerAddress] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RentBalance] [float] NULL,
[NextHB] [float] NULL,
[LastPayment] [float] NULL,
[LastPaymentDate] [datetime] NULL,
[SalesLedgerBalance] [float] NULL,
[EstimatedHBDue] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_Customer_Rent_Parameters] ADD 
CONSTRAINT [PK_AM_Rent1] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_AM_Customer_Rent_Parameters_CustomerId] ON [dbo].[AM_Customer_Rent_Parameters] ([CustomerId]) WITH (FILLFACTOR=100) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_AM_Customer_Rent_Parameters_TenancyId] ON [dbo].[AM_Customer_Rent_Parameters] ([TenancyId]) WITH (FILLFACTOR=100) ON [PRIMARY]






GO
