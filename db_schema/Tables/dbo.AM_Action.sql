CREATE TABLE [dbo].[AM_Action]
(
[ActionId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [int] NOT NULL,
[StatusHistoryId] [int] NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [int] NOT NULL,
[RecommendedFollowupPeriod] [int] NOT NULL,
[RecommendedFollowupPeriodFrequencyLookup] [int] NOT NULL,
[NextActionAlert] [int] NOT NULL,
[NextActionAlertFrequencyLookup] [int] NOT NULL,
[NextActionDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsProceduralAction] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[IsPaymentPlanMandotry] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_Action] ADD 
CONSTRAINT [PK_AM_Action] PRIMARY KEY CLUSTERED  ([ActionId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_Action] ADD CONSTRAINT [FK_AM_Action_AM_LookupCode] FOREIGN KEY ([NextActionAlertFrequencyLookup]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_Action] ADD CONSTRAINT [FK_AM_Action_AM_LookupCode1] FOREIGN KEY ([RecommendedFollowupPeriodFrequencyLookup]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_Action] ADD CONSTRAINT [ActionStatusHistory] FOREIGN KEY ([StatusHistoryId]) REFERENCES [dbo].[AM_StatusHistory] ([StatusHistoryId])
GO
ALTER TABLE [dbo].[AM_Action] ADD CONSTRAINT [StatusAction] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
GO
