USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_PAYMENTCARDCODES')
BEGIN
  PRINT 'Table Exists';
  

	CREATE TABLE [dbo].[F_PAYMENTCARDCODES]
	(
	[CODEID] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	) ON [PRIMARY]

	ALTER TABLE [dbo].[F_PAYMENTCARDCODES] ADD CONSTRAINT [PK_F_PAYMENTCARDCODES] PRIMARY KEY NONCLUSTERED  ([CODEID]) WITH FILLFACTOR=30 ON [PRIMARY]
END
IF COL_LENGTH('F_PAYMENTCARDCODES', 'FileExtensions') IS NULL
	BEGIN
			ALTER TABLE F_PAYMENTCARDCODES ADD FileExtensions [nvarchar] (4) NULL 
			PRINT('COLUMN FileExtensions CREATED')
	END


IF EXISTS (SELECT name FROM sys.key_constraints  
WHERE type = 'PK' AND OBJECT_NAME(parent_object_id) = N'F_PAYMENTCARDCODES')
BEGIN
	ALTER TABLE F_PAYMENTCARDCODES  
	DROP CONSTRAINT PK_F_PAYMENTCARDCODES; 
	 
	CREATE NONCLUSTERED INDEX IX_F_PAYMENTCARDCODES ON F_PAYMENTCARDCODES (CODEID);  
END
-------===============================Alter Table alter Description length========================================
DECLARE @ColumnLength int=0
SELECT @ColumnLength=max_length FROM sys.columns WHERE Name      = N'DESCRIPTION' AND Object_ID = Object_ID('F_PAYMENTCARDCODES')
IF (@ColumnLength = 100)
BEGIN
    ALTER TABLE F_PAYMENTCARDCODES
	ALTER COLUMN DESCRIPTION NVARCHAR(100);
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
	
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity, @ErrorState );
Print (@ErrorMessage)
END CATCH

GO


BEGIN TRANSACTION
BEGIN TRY 
--=================Post Office Payment===========================
IF EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='P')
	BEGIN
		Update F_PAYMENTCARDCODES Set DESCRIPTION = 'Post Office Payment',
		FileExtensions = '.PO'
		 WHERE CODEID='P'		
		PRINT('Post Office Payment Updated Successfully!')
	END
ELSE
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('P','Post Office Payment','.PO')
	END	

--=================Cash Adjustment Payment===========================
IF EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='C')
	BEGIN
		Update F_PAYMENTCARDCODES Set DESCRIPTION = 'Cash Adjustment',
		FileExtensions = '.CSH'
		 WHERE CODEID='C'		
		PRINT('Cash Adjustment Updated Successfully!')
	END
ELSE
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('C','Cash Adjustment','.CSH')
	END	
--=================PayPoint Payment Payment===========================
IF EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='T')
	BEGIN
		Update F_PAYMENTCARDCODES Set DESCRIPTION = 'PayPoint Payment',
		FileExtensions = '.PP'
		 WHERE CODEID='T'		
		PRINT('PayPoint Payment Updated Successfully!')
	END
ELSE
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('T','PayPoint Payment','.PP')
	END	
	
--=================DD Payment/Adjustment Payment===========================
IF EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='D')
	BEGIN
		Update F_PAYMENTCARDCODES Set DESCRIPTION = 'DD Payment/Adjustment',
		FileExtensions = '.DD'
		 WHERE CODEID='D'		
		PRINT('DD Payment/Adjustment Updated Successfully!')
	END
ELSE
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('D','DD Payment/Adjustment','.DD')
	END		
--=================DD Payment/Adjustment Payment===========================
IF EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='Q')
	BEGIN
		Update F_PAYMENTCARDCODES Set DESCRIPTION = 'Cheque Adjustment',
		FileExtensions = '.CQE'
		 WHERE CODEID='Q'		
		PRINT('Cheque Adjustment Updated Successfully!')
	END
ELSE
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('Q','Cheque Adjustment','.CQE')
	END		
--=================DD Payment/Adjustment Payment===========================
IF EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='N' AND FileExtensions IS NULL)
	BEGIN
		Update F_PAYMENTCARDCODES Set FileExtensions = '.txt'
		 WHERE CODEID='N' AND FileExtensions IS NULL		
	END

--=================Callpay (Cash/Cheque)===========================
IF Not EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='N' AND FileExtensions='.TC')
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('N','Callpay (Cash/Cheque)','.TC')	
	END
--=================Callpay (Cash/Cheque)===========================
IF Not EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='N' AND FileExtensions='.TCC')
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('N','Telephone/Internet/Text/Callpay/ App Payment (Credit Card)','.TCC')	
	END
--=================Callpay (Cash/Cheque)===========================
IF Not EXISTS( select * from F_PAYMENTCARDCODES WHERE CODEID='N' AND FileExtensions='.TDC')
	BEGIN
		INSERT INTO F_PAYMENTCARDCODES(CODEID,DESCRIPTION,FileExtensions)
		Values('N','Telephone/Internet/Text/Callpay/ App Payment (Debit Card)','.TDC')	
	END	

	
  --========================================================================================================
    ------Adding Index
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_F_PAYMENTCARDDATA_JournalId')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_F_PAYMENTCARDDATA_JournalId
		  ON F_PAYMENTCARDDATA (JOURNALID);  
	  PRINT 'IX_F_PAYMENTCARDDATA_JournalId created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_F_PAYMENTCARDDATA_JournalId Index Already Exist';
	  END	
	
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorInsertMessage NVARCHAR(4000);
	DECLARE @ErrorInsertSeverity INT;
	DECLARE @ErrorInsertState INT;

	SELECT @ErrorInsertMessage = ERROR_MESSAGE(),
	@ErrorInsertSeverity = ERROR_SEVERITY(),
	@ErrorInsertState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorInsertMessage,@ErrorInsertSeverity, @ErrorInsertState );
Print (@ErrorInsertMessage)
END CATCH