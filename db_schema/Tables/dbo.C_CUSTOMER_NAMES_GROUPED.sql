USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'C_CUSTOMER_NAMES_GROUPED')
    BEGIN

      CREATE TABLE [dbo].[C_CUSTOMER_NAMES_GROUPED] (
        [ID] [int] IDENTITY (1, 1) NOT NULL,
        [I] [int] NOT NULL,
        [VC] [varchar](300) NOT NULL,
        [LIST] [varchar](1000) NULL,
        [C] [varchar](80) NULL,
        CONSTRAINT [PK_C_CUSTOMER_NAMES_GROUPED] PRIMARY KEY CLUSTERED
        (
        [ID] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [PRIMARY]
      ) ON [PRIMARY]



      SET ANSI_PADDING OFF
    END
    ELSE

    BEGIN
      PRINT 'Table Already Exist';
    END

    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_C_CUSTOMER_NAMES_GROUPED')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_C_CUSTOMER_NAMES_GROUPED
		  ON C_CUSTOMER_NAMES_GROUPED (I);

		  PRINT 'IX_C_CUSTOMER_NAMES_GROUPED created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_C_CUSTOMER_NAMES_GROUPED Index Already Exist';
	  END

    

    --========================================================================================================
    --========================================================================================================

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH