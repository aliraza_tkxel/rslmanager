CREATE TABLE [dbo].[Z_AssuredRentIncreases]
(
[Scheme Number] [float] NULL,
[Local Authority] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[House Number] [float] NULL,
[Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewRent] [float] NULL,
[NewSupportedServiceCharge] [float] NULL,
[NewIneligibleServiceCharge] [float] NULL,
[NewOtherServiceCharge] [float] NULL,
[NewTotalServiceCharge] [float] NULL,
[NewCouncilTax] [float] NULL,
[NewWaterRate] [float] NULL,
[NewTotalRent] [float] NULL,
[Duplicate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
