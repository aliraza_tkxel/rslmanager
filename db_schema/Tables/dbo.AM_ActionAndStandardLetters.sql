CREATE TABLE [dbo].[AM_ActionAndStandardLetters]
(
[ActionAndStandardLettersId] [int] NOT NULL IDENTITY(1, 1),
[ActionId] [int] NOT NULL,
[StandardLetterId] [int] NOT NULL,
[ActionHistoryId] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[StandardLetterHistoryId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_ActionAndStandardLetters] ADD 
CONSTRAINT [PK_AM_ActionAndStandardLetters] PRIMARY KEY CLUSTERED  ([ActionAndStandardLettersId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_ActionAndStandardLetters] ADD
CONSTRAINT [FK_AM_ActionAndStandardLetters_AM_ActionAndStandardLetters] FOREIGN KEY ([ActionAndStandardLettersId]) REFERENCES [dbo].[AM_ActionAndStandardLetters] ([ActionAndStandardLettersId])
ALTER TABLE [dbo].[AM_ActionAndStandardLetters] ADD
CONSTRAINT [Relation_Action_StandardLetterHistory] FOREIGN KEY ([StandardLetterHistoryId]) REFERENCES [dbo].[AM_StandardLetterHistory] ([StandardLetterHistoryId])
ALTER TABLE [dbo].[AM_ActionAndStandardLetters] ADD
CONSTRAINT [Relation_StandardLetter_Id] FOREIGN KEY ([StandardLetterId]) REFERENCES [dbo].[AM_StandardLetters] ([StandardLetterId])
GO

ALTER TABLE [dbo].[AM_ActionAndStandardLetters] ADD CONSTRAINT [Relation_ActionHistoryId] FOREIGN KEY ([ActionHistoryId]) REFERENCES [dbo].[AM_ActionHistory] ([ActionHistoryId])
GO
ALTER TABLE [dbo].[AM_ActionAndStandardLetters] ADD CONSTRAINT [Relation_Action_Id] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])
GO
