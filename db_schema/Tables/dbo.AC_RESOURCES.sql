CREATE TABLE [dbo].[AC_RESOURCES]
(
[RESOURCEID] [int] NOT NULL IDENTITY(1, 1),
[PAGEID] [int] NULL,
[ACTIVE] [int] NULL,
[DESCRIPTION] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAGE] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AC_RESOURCES] ADD CONSTRAINT [PK_AC_RESOURCES] PRIMARY KEY NONCLUSTERED  ([RESOURCEID]) WITH FILLFACTOR=30 ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_AC_RESOURCES] ON [dbo].[AC_RESOURCES] ([PAGEID], [PAGE]) WITH FILLFACTOR=30 ON [PRIMARY]
GO
ALTER TABLE [dbo].[AC_RESOURCES] WITH NOCHECK ADD CONSTRAINT [FK_AC_RESOURCES_AC_PAGES] FOREIGN KEY ([PAGEID]) REFERENCES [dbo].[AC_PAGES] ([PAGEID])
GO
ALTER TABLE [dbo].[AC_RESOURCES] NOCHECK CONSTRAINT [FK_AC_RESOURCES_AC_PAGES]
GO
