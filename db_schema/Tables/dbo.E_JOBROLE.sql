USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_JOBROLE')
BEGIN
	CREATE TABLE [dbo].[E_JOBROLE](
		[JobRoleId] [int] IDENTITY(1,1) NOT NULL,
		[JobeRoleDescription] [varchar](50) NULL,
	 CONSTRAINT [PK_E_JOBROLE] PRIMARY KEY CLUSTERED 
	(
		[JobRoleId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY]

END   

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_JOBROLE')
BEGIN
	IF NOT EXISTS (Select 1 from E_JOBROLE where JobeRoleDescription='Interim Head of Asset Management')
	BEGIN 	
		INSERT INTO [E_JOBROLE] ([JobeRoleDescription])
		VALUES	('Interim Head of Asset Management')
	END
END 
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH



