BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_PURCHASEORDER')
BEGIN

		CREATE TABLE [dbo].[F_PURCHASEORDER]
		(
		[ORDERID] [int] NOT NULL IDENTITY(1, 1),
		[PONAME] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PODATE] [smalldatetime] NOT NULL,
		[EXPPODATE] [smalldatetime] NULL,
		[RECONCILEDATE] [smalldatetime] NULL,
		[PONOTES] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[USERID] [int] NULL,
		[SUPPLIERID] [int] NULL,
		[DEVELOPMENTID] [int] NULL,
		[BLOCKID] [int] NULL,
		[ACTIVE] [bit] NULL,
		[POTYPE] [int] NULL,
		[POSTATUS] [int] NULL,
		[POTIMESTAMP] [datetime] NOT NULL CONSTRAINT [DF_F_PURCHASEORDER_POTIMESTAMP] DEFAULT (getdate()),
		[GASSERVICINGYESNO] [bit] NULL,
		[CompanyId] [int] NULL
		) ON [PRIMARY]


		CREATE STATISTICS [_dta_stat_1933965966_11_13_8] ON [dbo].[F_PURCHASEORDER] ([ACTIVE], [POSTATUS], [SUPPLIERID])


		CREATE STATISTICS [_dta_stat_1933965966_8_11] ON [dbo].[F_PURCHASEORDER] ([ACTIVE], [SUPPLIERID])


		CREATE STATISTICS [_dta_stat_1933965966_13_11_7_8] ON [dbo].[F_PURCHASEORDER] ([POSTATUS], [ACTIVE], [USERID], [SUPPLIERID])


		CREATE STATISTICS [_dta_stat_1933965966_7_8_13_1] ON [dbo].[F_PURCHASEORDER] ([POSTATUS], [ORDERID], [USERID], [SUPPLIERID])


		CREATE STATISTICS [_dta_stat_1933965966_11_13_1_7_8] ON [dbo].[F_PURCHASEORDER] ([POSTATUS], [ORDERID], [USERID], [SUPPLIERID], [ACTIVE])


		CREATE STATISTICS [_dta_stat_1933965966_7_11] ON [dbo].[F_PURCHASEORDER] ([USERID], [ACTIVE])

		CREATE NONCLUSTERED INDEX [_dta_index_F_PURCHASEORDER_7_1933965966__K1D_K11_2_3_7_8_13] ON [dbo].[F_PURCHASEORDER] ([ORDERID] DESC, [ACTIVE]) INCLUDE ([PODATE], [PONAME], [POSTATUS], [SUPPLIERID], [USERID]) ON [PRIMARY]


		CREATE CLUSTERED INDEX [IX_PURCHASEORDER] ON [dbo].[F_PURCHASEORDER] ([ORDERID], [PODATE]) WITH FILLFACTOR=90 ON [PRIMARY]

END

IF NOT EXISTS(	SELECT	1 
				FROM	sys.columns 
				WHERE	Name = N'PropertyId' AND Object_ID = Object_ID(N'F_PURCHASEORDER'))
BEGIN

	ALTER TABLE F_PURCHASEORDER 
	ADD PropertyId [nvarchar] (40) NULL

END

IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'f_purchaseorder'))
BEGIN

alter table f_purchaseorder add CompanyId [int] NULL

END


IF (SELECT TOP 1 COMPANYID FROM F_PURCHASEORDER ) IS NULL
 BEGIN
 -- ROUGHLY TAKES AROUND 30 SECONDS TO RUN
UPDATE f_purchaseorder SET COMPANYID = 1

END

IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'SchemeId'
          AND Object_ID = Object_ID(N'f_purchaseorder'))
BEGIN

alter table f_purchaseorder add SchemeId [int] NULL

END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IsAmend'
          AND Object_ID = Object_ID(N'F_PURCHASEORDER'))
BEGIN
ALTER TABLE F_PURCHASEORDER ADD IsAmend BIT NOT NULL DEFAULT 0;
Print ('Added IsAmend in F_PURCHASEORDER.')
END


If NOT EXISTS (Select 1 from sys.columns
		WHERE Name =N'CancelBy' 
		And object_ID=object_ID(N'F_PURCHASEORDER'))
Begin 
Alter table F_PURCHASEORDER Add CancelBy int  NUll default null;
print ('Added CancelBy in F_PURCHASEORDER.')
END


If NOT EXISTS (Select 1 from sys.columns
		WHERE Name =N'Reason' 
		And object_ID=object_ID(N'F_PURCHASEORDER'))
Begin 
Alter table F_PURCHASEORDER Add Reason Nvarchar(200)  NUll default null;
print ('Added Reason in F_PURCHASEORDER.')
END

If NOT EXISTS (Select 1 from sys.columns
		WHERE Name =N'IsServiceChargePO' 
		And object_ID=object_ID(N'F_PURCHASEORDER'))
Begin 
Alter table F_PURCHASEORDER Add IsServiceChargePO Bit NUll;
print ('Added IsServiceChargePO in F_PURCHASEORDER.')
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




