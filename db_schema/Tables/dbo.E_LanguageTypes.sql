USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_LanguageTypes')
BEGIN

		CREATE TABLE [dbo].[E_LanguageTypes](
			[LanguageTypeId] [int] IDENTITY(1,1) NOT NULL,
			[Title] [nvarchar](500) NULL,
		 CONSTRAINT [PK_E_LanguageTypes] PRIMARY KEY CLUSTERED 
		(
			[LanguageTypeId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
END

    if not exists(Select * from E_LanguageTypes)
    BEGIN
    SET IDENTITY_INSERT [dbo].[E_LanguageTypes] ON 


		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (1, N'English   ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (2, N'Welsh     ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (3, N'Polish    ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (4, N'Punjabi')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (5, N'Urdu')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (6, N'Bengali')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (7, N'Gujarati')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (8, N'Arabic ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (9, N'French ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (10, N'All other Chinese')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (11, N'Portuguese ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (12, N'Spanish ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (13, N'Tamil ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (14, N'Turkish ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (15, N'Italian ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (16, N'Somali ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (17, N'Lithuanian ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (18, N'German ')

		INSERT [dbo].[E_LanguageTypes] ([LanguageTypeId], [Title]) VALUES (19, N'Other')

		SET IDENTITY_INSERT [dbo].[E_LanguageTypes] OFF
    END
    
    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    


