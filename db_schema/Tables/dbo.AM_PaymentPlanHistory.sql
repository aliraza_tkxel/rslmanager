CREATE TABLE [dbo].[AM_PaymentPlanHistory]
(
[PaymentPlanHistoryId] [int] NOT NULL IDENTITY(1, 1),
[PaymentPlanId] [int] NOT NULL,
[TennantId] [int] NOT NULL,
[PaymentPlanType] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NOT NULL,
[FrequencyLookupCodeId] [int] NOT NULL,
[ArrearsCollection] [float] NULL,
[AmountToBeCollected] [float] NOT NULL,
[WeeklyRentAmount] [float] NOT NULL,
[ReviewDate] [datetime] NOT NULL,
[FirstCollectionDate] [datetime] NULL,
[IsCreated] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NOT NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[RentBalance] [float] NULL,
[LastPayment] [float] NULL,
[LastPaymentDate] [datetime] NULL,
[RentPayable] [float] NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_PaymentPlanHistory] ADD 
CONSTRAINT [PK_AM_PaymentPlanHistory] PRIMARY KEY CLUSTERED  ([PaymentPlanHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_PaymentPlanHistory] ADD CONSTRAINT [PaymentPlanHistoryCreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_PaymentPlanHistory] ADD CONSTRAINT [PaymentPlanHistoryFrequencyLookupCode] FOREIGN KEY ([FrequencyLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_PaymentPlanHistory] ADD CONSTRAINT [PaymentPlanHistoryModifiedBy] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_PaymentPlanHistory] ADD CONSTRAINT [FK_AM_PaymentPlanHistory_AM_PaymentPlan] FOREIGN KEY ([PaymentPlanId]) REFERENCES [dbo].[AM_PaymentPlan] ([PaymentPlanId])
GO
ALTER TABLE [dbo].[AM_PaymentPlanHistory] ADD CONSTRAINT [FK_AM_PaymentPlanHistory_AM_LookupCode] FOREIGN KEY ([PaymentPlanType]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
