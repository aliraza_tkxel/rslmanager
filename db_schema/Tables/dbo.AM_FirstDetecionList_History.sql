CREATE TABLE [dbo].[AM_FirstDetecionList_History]
(
[HistoryListId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NULL,
[TenancyId] [int] NULL,
[FirstDetectionDate] [datetime] NULL,
[IsDefaulter] [bit] NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[CreatedDate] [datetime] NULL,
[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_FirstDetecionList_History] ADD 
CONSTRAINT [PK__AM_First__7FA2A8D9413B345C] PRIMARY KEY CLUSTERED  ([HistoryListId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
