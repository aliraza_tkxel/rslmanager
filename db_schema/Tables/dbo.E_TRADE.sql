CREATE TABLE [dbo].[E_TRADE]
(
[EmpTradeId] [int] NOT NULL IDENTITY(1, 1),
[EmpId] [int] NULL,
[TradeId] [int] NULL,
[LASTACTIONUSER] [int] NULL,
[LASTACTIONTIME] [datetime] NULL CONSTRAINT [DF_E_TRADE_LASTACTIONTIME] DEFAULT (getdate())
) ON [PRIMARY]
ALTER TABLE [dbo].[E_TRADE] ADD 
CONSTRAINT [PK_E_TRADE] PRIMARY KEY CLUSTERED  ([EmpTradeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE trigger [dbo].[tr_E_TRADE_AUDIT] on [dbo].[E_TRADE] for insert, update, delete
as

declare @bit int ,
	@field int ,
	@maxfield int ,
	@char int ,
	@fieldname varchar(128) ,
	@TableName varchar(128) ,
	@PKCols varchar(1000) ,
	@sql varchar(2000), 
	@UpdateDate varchar(21) ,
	@UserName varchar(128) ,
	@Type char(1) ,
	@PKSelect varchar(1000),
	@BatchId UNIQUEIDENTIFIER,
	@LastActionUser VARCHAR(500)
	
	
	set @BatchId = NEWID()
	select @TableName = 'E_TRADE'
	
	-- date and user
	select 	@UserName = system_user ,
			@UpdateDate = GETDATE()

	-- Action
	if exists (select * from inserted)
	BEGIN
		if exists (select * from deleted)
			select @Type = 'U'
		else
			select @Type = 'I'
		SET @LastActionUser = ',ISNULL(CAST(i.LastActionUser AS VARCHAR), NULL)'
	END
	ELSE
		begin
			select @Type = 'D'
			SET @LastActionUser = ',ISNULL(CAST(d.LastActionUser AS VARCHAR), NULL)'
		END
	
	-- get list of columns
	select * into #ins from inserted
	select * into #del from deleted
	
	-- Get primary key columns for full outer join
	select	@PKCols = coalesce(@PKCols + ' and', ' on') + ' i.' + c.COLUMN_NAME + ' = d.' + c.COLUMN_NAME
	from	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
	where 	pk.TABLE_NAME = @TableName
	and	CONSTRAINT_TYPE = 'PRIMARY KEY'
	and	c.TABLE_NAME = pk.TABLE_NAME
	and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
	
	-- Get primary key select for insert
	select @PKSelect = coalesce(@PKSelect+'+','') + '''<' + COLUMN_NAME + '=''+convert(varchar(100),coalesce(i.' + COLUMN_NAME +',d.' + COLUMN_NAME + '))+''>''' 
	from	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
	where 	pk.TABLE_NAME = @TableName
	and	CONSTRAINT_TYPE = 'PRIMARY KEY'
	and	c.TABLE_NAME = pk.TABLE_NAME
	and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
	
	if @PKCols is null
	begin
		raiserror('no PK on table %s', 16, -1, @TableName)
		return
	end
	
	select @field = 0, @maxfield = max(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName
	while @field < @maxfield
	begin
		select @field = min(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName and ORDINAL_POSITION > @field
		select @bit = (@field - 1 )% 8 + 1
		select @bit = power(2,@bit - 1)
		select @char = ((@field - 1) / 8) + 1
		if substring(COLUMNS_UPDATED(),@char, 1) & @bit > 0 or @Type in ('I','D')
		begin
			select @fieldname = COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName and ORDINAL_POSITION = @field
			select @sql = 		'Insert INTO E_TRADE_AUDIT (Type, TableName, PK, EmpTradeId, EmployeeId, FieldName, OldValue, NewValue, UpdateDate, UserName, LastActionUser, BatchId, AppName, HostName)'
			select @sql = @sql + 	' select ''' + @Type + ''''
			select @sql = @sql + 	',''' + @TableName + ''''
			select @sql = @sql + 	',' + @PKSelect
			select @sql = @sql +	', coalesce(i.EmpTradeId,d.EmpTradeId) ' 
			SELECT @sql = @sql +	', coalesce(i.EmpId,d.EmpId) ' 
			select @sql = @sql + 	',''' + @fieldname + ''''
			select @sql = @sql + 	',convert(varchar(1000),d.' + @fieldname + ')'
			select @sql = @sql + 	',convert(varchar(1000),i.' + @fieldname + ')'
			select @sql = @sql + 	',''' + @UpdateDate + ''''
			select @sql = @sql + 	',''' + @UserName + ''''
			select @sql = @sql + 	@LastActionUser
			select @sql = @sql + 	',CAST(''' + CAST(@BatchId AS VARCHAR(100)) + ''' AS UNIQUEIDENTIFIER)'
			select @sql = @sql + 	',APP_NAME()'
			select @sql = @sql + 	',HOST_NAME()'
			select @sql = @sql + 	' from #ins i full outer join #del d'
			select @sql = @sql + 	@PKCols
			select @sql = @sql + 	' where i.' + @fieldname + ' <> d.' + @fieldname 
			select @sql = @sql + 	' or (i.' + @fieldname + ' is null and  d.' + @fieldname + ' is not null)' 
			select @sql = @sql + 	' or (i.' + @fieldname + ' is not null and  d.' + @fieldname + ' is null)'
			--PRINT (@sql)
			exec (@sql)
		end
	end



GO

ALTER TABLE [dbo].[E_TRADE] ADD CONSTRAINT [FK_E_TRADE_E__EMPLOYEE] FOREIGN KEY ([EmpId]) REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])
GO
ALTER TABLE [dbo].[E_TRADE] ADD CONSTRAINT [FK_E_TRADE_G_TRADE] FOREIGN KEY ([TradeId]) REFERENCES [dbo].[G_TRADE] ([TradeId])
GO
