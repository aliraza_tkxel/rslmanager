CREATE TABLE [web].[neighbourhoodAuth]
(
[authorityId] [int] NOT NULL IDENTITY(1, 1),
[local_authority] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[website] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
