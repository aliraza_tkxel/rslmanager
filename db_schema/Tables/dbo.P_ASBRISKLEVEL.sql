USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY


	IF NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'P_ASBRISKLEVEL')
		BEGIN

	CREATE TABLE [dbo].[P_ASBRISKLEVEL]
	(
	[ASBRISKLEVELID] [int] NOT NULL IDENTITY(4, 1),
	[ASBRISKLEVELDESCRIPTION] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RISKLEVELID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	) ON [PRIMARY]
END
	ELSE

		BEGIN
			PRINT 'Table Already Exist';
		END
		
IF EXISTS(Select * from P_ASBRISKLEVEL where P_ASBRISKLEVEL.ASBRISKLEVELDESCRIPTION='Low Risk (Chrysotile)')
	BEGIN
		UPDATE P_ASBRISKLEVEL set ASBRISKLEVELDESCRIPTION='Chrysotile' where P_ASBRISKLEVEL.ASBRISKLEVELDESCRIPTION='Low Risk (Chrysotile)'
	END

IF EXISTS(Select * from P_ASBRISKLEVEL where P_ASBRISKLEVEL.ASBRISKLEVELDESCRIPTION='Medium Risk(Chrysotile/Amosite)')
	BEGIN
		UPDATE P_ASBRISKLEVEL set ASBRISKLEVELDESCRIPTION='Chrysotile/Amosite' where P_ASBRISKLEVEL.ASBRISKLEVELDESCRIPTION='Medium Risk(Chrysotile/Amosite)'
	END
IF EXISTS(Select * from P_ASBRISKLEVEL where P_ASBRISKLEVEL.ASBRISKLEVELDESCRIPTION='High Risk (Crocidolite/ Chrysotile/Amosite)')
	BEGIN
		UPDATE P_ASBRISKLEVEL set ASBRISKLEVELDESCRIPTION='Crocidolite/ Chrysotile/Amosite' where P_ASBRISKLEVEL.ASBRISKLEVELDESCRIPTION='High Risk (Crocidolite/ Chrysotile/Amosite)'
	END		
	
		
IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH


		
