USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT *
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FLS_FloorType') 
BEGIN
CREATE TABLE [dbo].[FLS_FloorType](
	[FloorTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_FLS_FloorType] PRIMARY KEY CLUSTERED 
(
	[FloorTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
INSERT [dbo].[FLS_FloorType] ([Description]) VALUES ('Ground')
INSERT [dbo].[FLS_FloorType] ([Description]) VALUES ('First')
INSERT [dbo].[FLS_FloorType] ([Description]) VALUES ('Second')
INSERT [dbo].[FLS_FloorType] ([Description]) VALUES ('Other')
END
ELSE
	BEGIN

	IF NOT EXISTS (SELECT
		1
	FROM FLS_FloorType
	WHERE Description = 'Ground') 
	BEGIN
	INSERT [dbo].[FLS_FloorType] ([Description])
		VALUES ( N'Ground')
	END

		IF NOT EXISTS (SELECT
		1
	FROM FLS_FloorType
	WHERE Description = 'First') 
	BEGIN
	INSERT [dbo].[FLS_FloorType] ([Description])
		VALUES ( N'First')
	END

		IF NOT EXISTS (SELECT
		1
	FROM FLS_FloorType
	WHERE Description = 'Second') 
	BEGIN
	INSERT [dbo].[FLS_FloorType] ([Description])
		VALUES ( N'Second')
	END

		IF NOT EXISTS (SELECT
		1
	FROM FLS_FloorType
	WHERE Description = 'Other') 
	BEGIN
	INSERT [dbo].[FLS_FloorType] ([Description])
		VALUES ( N'Other')
	END

		
END
IF @@TRANCOUNT > 0 BEGIN
COMMIT TRANSACTION;
PRINT ('Commit')
END

END TRY BEGIN CATCH

IF @@TRANCOUNT > 0 BEGIN

ROLLBACK TRANSACTION;

END
DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage,
@ErrorSeverity,
@ErrorState
);
PRINT (@ErrorMessage)

END CATCH