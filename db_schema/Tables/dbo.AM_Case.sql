USE RSLBHALive


BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AM_Case')
    BEGIN
			CREATE TABLE [dbo].[AM_Case]
			(
			[CaseId] [int] NOT NULL IDENTITY(1, 1),
			[TenancyId] [int] NOT NULL,
			[InitiatedBy] [int] NOT NULL,
			[CaseManager] [int] NOT NULL,
			[CaseOfficer] [int] NOT NULL,
			[StatusId] [int] NOT NULL,
			[StatusHistoryId] [int] NOT NULL,
			[StatusReview] [datetime] NOT NULL,
			[StatusRecordedDate] [datetime] NULL,
			[RentBalance] [float] NULL,
			[RecoveryAmount] [float] NOT NULL,
			[NoticeIssueDate] [datetime] NULL,
			[NoticeExpiryDate] [datetime] NULL,
			[WarrantExpiryDate] [datetime] NULL,
			[HearingDate] [datetime] NULL,
			[ActionId] [int] NOT NULL,
			[ActionHistoryId] [int] NOT NULL,
			[ActionReviewDate] [datetime] NOT NULL,
			[ActionRecordedDate] [datetime] NULL,
			[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
			[OutcomeLookupCodeId] [int] NULL,
			[Reason] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
			[CreatedDate] [datetime] NOT NULL,
			[ModifiedDate] [datetime] NOT NULL,
			[IsSuppressed] [bit] NOT NULL,
			[ModifiedBy] [int] NULL,
			[IsPaymentPlan] [bit] NULL,
			[IsPaymentPlanUpdated] [bit] NULL,
			[IsActive] [bit] NULL,
			[IsDocumentUpload] [bit] NULL,
			[IsDocumentAttached] [bit] NULL,
			[SuppressedReason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
			[SuppressedBy] [int] NULL,
			[SuppressedDate] [datetime] NULL,
			[IsActionIgnored] [bit] NULL,
			[ActionIgnoreCount] [int] NULL CONSTRAINT [DF_AM_Case_ActionIgnoreCount] DEFAULT ((0)),
			[ActionIgnoreReason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
			[IsActionRecorded] [bit] NULL,
			[RecordedActionDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
			[ActionRecordedCount] [int] NULL CONSTRAINT [DF_AM_Case_ActionRecordedCount] DEFAULT ((0)),
			[IsActionPostponed] [bit] NULL,
			[IsPaymentPlanIgnored] [bit] NULL,
			[PaymentPlanIgnoreReason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
			[PaymentPlanId] [int] NULL,
			[PaymentPlanHistoryId] [int] NULL,
			[IsCaseUpdated] [bit] NULL,
			[IsPaymentPlanClose] [bit] NULL
			) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
			ALTER TABLE [dbo].[AM_Case] ADD 
			CONSTRAINT [PK_AM_Case] PRIMARY KEY CLUSTERED  ([CaseId]) WITH (FILLFACTOR=100) ON [PRIMARY]
			CREATE NONCLUSTERED INDEX [IX_AM_Case_Tenancyid] ON [dbo].[AM_Case] ([TenancyId]) WITH (FILLFACTOR=100) ON [PRIMARY]



			

			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [CaseActionHistory] FOREIGN KEY ([ActionHistoryId]) REFERENCES [dbo].[AM_ActionHistory] ([ActionHistoryId])
			
			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [FK_AM_Case_AM_Action] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])
			
			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [FK_AM_Case_AM_LookupCode] FOREIGN KEY ([OutcomeLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
			
			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [FK_AM_Case_AM_PaymentPlanHistory] FOREIGN KEY ([PaymentPlanHistoryId]) REFERENCES [dbo].[AM_PaymentPlanHistory] ([PaymentPlanHistoryId])
			
			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [FK_AM_Case_AM_PaymentPlan] FOREIGN KEY ([PaymentPlanId]) REFERENCES [dbo].[AM_PaymentPlan] ([PaymentPlanId])
			
			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [CaseStatusHistory] FOREIGN KEY ([StatusHistoryId]) REFERENCES [dbo].[AM_StatusHistory] ([StatusHistoryId])
			
			ALTER TABLE [dbo].[AM_Case] ADD CONSTRAINT [FK_AM_Case_AM_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
			
 END
ELSE

BEGIN
PRINT 'Table Already Exist';
END
    --========================================================================================================
    ------Adding Index
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_Case_IsPaymentPlan')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_Case_IsPaymentPlan
		  ON AM_Case (IsPaymentPlan);  
	  PRINT 'IX_AM_Case_IsPaymentPlan created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_Case_IsPaymentPlan Index Already Exist';
	  END
    --========================================================================================================
    --======================================================================================================== 

	--------------------------------------------------------------------------------------
	-------------------------------------TICKET-RFC5--------------------------------------
	--------------------------------------------------------------------------------------


	IF COL_LENGTH('AM_Case', 'AdditionalContactID') IS NULL
		BEGIN
			ALTER TABLE dbo.AM_Case ADD AdditionalContactID int NOT NULL DEFAULT 0
			
			PRINT 'Column AdditionalContactID created';
		END
	ELSE
	BEGIN
		PRINT 'column AdditionalContactID exists'
	END

	IF COL_LENGTH('AM_Case', 'ThirdPartyContactID') IS NULL
		BEGIN
			ALTER TABLE dbo.AM_Case ADD ThirdPartyContactID int NOT NULL DEFAULT 0
			PRINT 'Column ThirdPartyContactID created';
		END
	ELSE
	BEGIN
		PRINT 'column ThirdPartyContactID exists'
	END
	
	IF COL_LENGTH('AM_Case', 'JointTenantLetterRequired') IS NULL
		BEGIN
			ALTER TABLE dbo.AM_Case ADD JointTenantLetterRequired int NOT NULL DEFAULT 0
			PRINT 'Column JointTenantLetterRequired created';
		END
	ELSE
	BEGIN
		PRINT 'column JointTenantLetterRequired exists'
	END

	--------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------
    
    
    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    

GO

BEGIN TRANSACTION
BEGIN TRY

Update dbo.AM_Case SET AdditionalContactID = 0 Where AdditionalContactID is NULL
Update dbo.AM_Case SET ThirdPartyContactID = 0 Where ThirdPartyContactID is NULL
Update dbo.AM_Case SET JointTenantLetterRequired = 0 Where JointTenantLetterRequired is NULL
		
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorInsertMessage NVARCHAR(4000);
	DECLARE @ErrorInsertSeverity INT;
	DECLARE @ErrorInsertState INT;

	SELECT @ErrorInsertMessage = ERROR_MESSAGE(),
	@ErrorInsertSeverity = ERROR_SEVERITY(),
	@ErrorInsertState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorInsertMessage,@ErrorInsertSeverity, @ErrorInsertState );
Print (@ErrorInsertMessage)
END CATCH