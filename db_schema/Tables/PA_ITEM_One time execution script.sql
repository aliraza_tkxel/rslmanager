BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS ( Select * from pa_item WHERE  itemName = 'Electrics' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
	BEGIN 	
		update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Services'), isActive = 1,IsForSchemeBlock=0 
		where itemid in (Select ItemID from pa_item WHERE  itemName = 'Electrics' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Electrics') )
	END
------------=--==========================================-==============================================================
IF NOT EXISTS ( Select * from pa_item WHERE  itemName = 'Electrics' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Electrics') )
	BEGIN 	
	DECLARE @ElectricsItemId int	
	
	INSERT INTO PA_ITEM(AreaID,ItemName,ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView, IsForSchemeBlock)
	VALUES((SELECT areaID FROM pa_area WHERE  AreaName = 'Electrics'),'Electrics',1,1,NULL,0,0,1) 	
		SET @ElectricsItemId = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER( ItemId,ParameterId,IsActive)
		Select @ElectricsItemId,ParameterId,IsActive from PA_ITEM_PARAMETER 
		where ItemId=(Select ItemID from pa_item WHERE  itemName = 'Electrics' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
	
	END	
---=======================================================================================================================================
	IF  EXISTS (SELECT	1 FROM	PA_ITEM WHERE ItemName IN ('Sundry') and IsActive=0)
		BEGIN 	
		Update PA_ITEM set IsActive = 1, IsForSchemeBlock=0, ShowInApp=0 where ItemName = 'Sundry'	
		END
---=======================================================================================================================================	
	IF  EXISTS (SELECT * from PA_ITEM where AreaID=(Select AreaID from PA_AREA where AreaName='Sundry')and ShowListView=0)
		BEGIN 	
		Update PA_ITEM set IsActive = 1, IsForSchemeBlock=1, ShowListView=1 where 
		ItemID in (SELECT ItemID from PA_ITEM where AreaID=(Select AreaID from PA_AREA where AreaName='Sundry')and ShowListView=0)
			
		END
---=======================================================================================================================================

IF NOT EXISTS ( Select * from pa_item WHERE  itemName = 'Smoke' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
	BEGIN 	
		update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Services'), isActive = 1,IsForSchemeBlock=0,
		ParentItemId = (Select ItemId from PA_ITEM where ItemName='Systems') 
		where itemid in (Select ItemID from pa_item WHERE  itemName = 'Smoke' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Building Systems') )
	END
------------=--==========================================-==============================================================
IF NOT EXISTS ( Select * from pa_item WHERE  itemName = 'Smoke' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Building Systems') )
	BEGIN 	
	DECLARE @SmokeItemId int	
	
	INSERT INTO PA_ITEM(AreaID,ItemName,ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView, IsForSchemeBlock)
	VALUES((SELECT areaID FROM pa_area WHERE  AreaName = 'Building Systems'),'Smoke',1,1,NULL,0,0,1) 	
		SET @SmokeItemId = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER( ItemId,ParameterId,IsActive)
		Select @SmokeItemId,ParameterId,IsActive from PA_ITEM_PARAMETER 
		where ItemId=(Select ItemID from pa_item WHERE  itemName = 'Smoke' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
	
	END	

---=======================================================================================================================================

IF NOT EXISTS ( Select * from pa_item WHERE  itemName = 'Meters' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
	BEGIN 	
	
		update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Services'), isActive = 1,IsForSchemeBlock=0
		,ShowInApp=1
		where itemid in (Select ItemID from pa_item WHERE  itemName = 'Meters' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Meters') )
		
	   update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Services'), isActive = 1,IsForSchemeBlock=0,ShowInApp=1
		
		where ParentItemId in (Select ItemID from pa_item WHERE  itemName = 'Meters' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
		
	END
------------=--==========================================-==============================================================
IF NOT EXISTS ( Select * from pa_item WHERE  itemName = 'Meters' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Meters') )
	BEGIN 	
	DECLARE @ElectricItemId int,@GasItemId int	
	Update PA_AREA set IsActive=1 where AreaID = (SELECT areaID FROM pa_area WHERE  AreaName = 'Meters')
	
		INSERT INTO PA_ITEM(AreaID,ItemName,ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView, IsForSchemeBlock)
		VALUES((SELECT areaID FROM pa_area WHERE  AreaName = 'Meters'),'Electric',1,1,NULL,0,0,1) 	
		SET @ElectricItemId = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER( ItemId,ParameterId,IsActive)
		Select @ElectricItemId,ParameterId,IsActive from PA_ITEM_PARAMETER 
		where ItemId=(Select ItemID from pa_item WHERE  itemName = 'Electric' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
		
		INSERT INTO PA_ITEM(AreaID,ItemName,ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView, IsForSchemeBlock)
		VALUES((SELECT areaID FROM pa_area WHERE  AreaName = 'Meters'),'Gas',1,1,NULL,0,0,1) 	
		SET @GasItemId = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER( ItemId,ParameterId,IsActive)
		Select @GasItemId,ParameterId,IsActive from PA_ITEM_PARAMETER 
		where ItemId=(Select ItemID from pa_item WHERE  itemName = 'Gas' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )
	
	END	





update pa_item set IsForSchemeBlock=0
where itemid in (Select ItemID from pa_item WHERE  itemName = 'Systems' and AreaID=(SELECT areaID FROM pa_area WHERE  AreaName = 'Services') )



	
	
	
	
	--IF  EXISTS ( Select 1 from pa_item WHERE  itemName = 'Meters' )
	--BEGIN 
	--	update pa_item set AreaiD = (SELECT areaID FROM pa_area WHERE  AreaName = 'Meters'), isActive = 1 
	--	where itemid in (Select itemid from pa_item WHERE  itemName = 'Meters')
	--END
	
	
	
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
	

END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
	