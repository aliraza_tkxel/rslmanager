CREATE TABLE [dbo].[P_ASSETTYPE_NROSH_SUB]
(
[Sid] [int] NOT NULL,
[MainCategoryId] [int] NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nrosh_Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_ASSETTYPE_NROSH_SUB] TO [rackspace_datareader]
GO
