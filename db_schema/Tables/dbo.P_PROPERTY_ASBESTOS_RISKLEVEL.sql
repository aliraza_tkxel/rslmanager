USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY


	IF NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'P_PROPERTY_ASBESTOS_RISKLEVEL')
		BEGIN
		CREATE TABLE [dbo].[P_PROPERTY_ASBESTOS_RISKLEVEL]
		(
		[PROPASBLEVELID] [int] NOT NULL IDENTITY(1158316, 1),
		[ASBRISKLEVELID] [int] NULL,
		[ASBESTOSID] [int] NULL,
		[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DateAdded] [smalldatetime] NULL,
		[DateRemoved] [smalldatetime] NULL,
		[UserID] [int] NULL,
		[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
	ELSE

		BEGIN
			PRINT 'Table Already Exist';
		END
		
	
IF COL_LENGTH('P_PROPERTY_ASBESTOS_RISKLEVEL', 'RiskLevelId') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_ASBESTOS_RISKLEVEL
			ADD RiskLevelId int NULL
	END	
	
	IF COL_LENGTH('P_PROPERTY_ASBESTOS_RISKLEVEL', 'IsUrgentActionRequired') IS NULL
	BEGIN
			ALTER TABLE P_PROPERTY_ASBESTOS_RISKLEVEL
			ADD IsUrgentActionRequired bit default 0 NULL
	END	
IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH


