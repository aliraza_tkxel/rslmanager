/****** Script for SelectTopNRows command from SSMS  ******/

USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

	-------------------------------- CREATING TABLE IF NOT EXISTS ------------------------------------------

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'PLANNED_REASON')
	BEGIN
	

		CREATE TABLE [dbo].[PLANNED_REASON]
		(
			[ReasonId] [int] NOT NULL IDENTITY(1, 1),
			[Reason] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
			[IsEditable] [bit] NOT NULL,
			[CreatedBy] [int] NOT NULL,
			[CreatedDate] [datetime2] (2) NOT NULL,
			[ModifiedBy] [int] NULL,
			[ModifiedDate] [datetime2] (2) NULL
		) ON [PRIMARY]

	END
	------------------------------------  ADD A NEW COLUMN FOR CANCELLED REASON ----------------------------------

	IF COL_LENGTH('PLANNED_REASON', 'CancelledReason') IS NULL
	BEGIN
		ALTER TABLE PLANNED_REASON
		ADD CancelledReason bit NULL
	END

	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
GO


BEGIN TRANSACTION
BEGIN TRY
	------------------------------------  ADD NEW VALUES FOR TICKET# 10840 ----------------------------------------

	IF NOT EXISTS (	SELECT	1 FROM	PLANNED_REASON WHERE Reason IN ('Tenant Refused Works'))
		BEGIN 	
			INSERT INTO [dbo].[PLANNED_REASON]([Reason],[IsEditable],[CreatedBy],[CreatedDate],[CancelledReason]) VALUES ('Tenant Refused Works',1,760,GETDATE(),1)
		END

	IF NOT EXISTS (	SELECT	1 FROM	PLANNED_REASON WHERE Reason IN ('Budgetary Constraints'))
		BEGIN 	
			INSERT INTO [dbo].[PLANNED_REASON]([Reason],[IsEditable],[CreatedBy],[CreatedDate],[CancelledReason]) VALUES ('Budgetary Constraints',1,760,GETDATE(),1)
		END

	IF NOT EXISTS (	SELECT	1 FROM	PLANNED_REASON WHERE Reason IN ('Approved in Error'))
		BEGIN 	
			INSERT INTO [dbo].[PLANNED_REASON]([Reason],[IsEditable],[CreatedBy],[CreatedDate],[CancelledReason]) VALUES ('Approved in Error',1,760,GETDATE(),1)
		END

	--------------------------------------- UPDATE NULL VALUES TO 0 --------------------------------------

	UPDATE PLANNED_REASON set CancelledReason = 0 WHERE CancelledReason IS NULL

	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH