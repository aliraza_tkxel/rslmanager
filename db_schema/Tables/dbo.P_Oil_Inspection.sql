USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_Oil_Inspection')
BEGIN

CREATE TABLE [dbo].[P_Oil_Inspection](
	[InspectionId] [int] IDENTITY(1,1) NOT NULL,	
	[HeatingMappingId] [int] NULL,
	[JournalId] [int] NULL,
	[IsInspected] [bit] NULL,
	[InspectionDate] [smalldatetime] NULL,
	[InspectedBy] [int] NULL,
	[OilStorage] [bit] NULL,
	[OilStorageDetail] [nvarchar](300) NULL,
	[OilSupplySystem] [bit] NULL,
	[OilSupplySystemDetail] [nvarchar](300) NULL,
	[AirSupply] [bit] NULL,
	[AirSupplyDetail] [nvarchar](300) NULL,
	[ChimneyFlue] [bit] NULL,
	[ChimneyFlueDetail] [nvarchar](300) NULL,	
	[ElectricalSafety] [bit] NULL,
	[ElectricalSafetyDetail] [nvarchar](300) NULL,
	[HeatExchanger] [bit] NULL,
	[HeatExchangerDetail] [nvarchar](300) NULL,
	[CombustionChamber] [bit] NULL,
	[CombustionChamberDetail] [nvarchar](300) NULL,
	[PressureJet] [bit] NULL,
	[PressureJetDetail] [nvarchar](300) NULL,
	[VaporisingBurner] [bit] NULL,
	[VaporisingBurnerDetail] [nvarchar](300) NULL,
	[WallflameBurner] [bit] NULL,
	[WallflameBurnerDetail] [nvarchar](300) NULL,
	[ApplianceSafety] [bit] NULL,
	[ApplianceSafetyDetail] [nvarchar](300) NULL,
	[ControlCheck] [bit] NULL,
	[ControlCheckDetail] [nvarchar](300) NULL,
	[HotWaterType] [bit] NULL,
	[HotWaterTypeDetail] [nvarchar](300) NULL,
	[WarmAirType] [bit] NULL,
	[WarmAirTypeDetail] [nvarchar](300) NULL,

 CONSTRAINT [PK_P_Oil_Inspection] PRIMARY KEY CLUSTERED 
(
	[InspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 