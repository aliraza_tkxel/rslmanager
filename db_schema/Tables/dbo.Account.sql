CREATE TABLE [dbo].[Account]
(
[ListID] [char] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeCreated] [datetime] NULL,
[TimeModified] [datetime] NULL,
[EditSequence] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullName] [char] (159) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NULL,
[ParentRefListID] [char] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentRefFullName] [char] (159) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sublevel] [int] NULL,
[AccountType] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountNumber] [char] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankNumber] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Desc] [char] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Balance] [decimal] (11, 2) NULL,
[TotalBalance] [decimal] (11, 2) NULL,
[OpenBalance] [decimal] (11, 2) NULL,
[OpenBalanceDate] [smalldatetime] NULL,
[CurrencyRefListID] [char] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyRefFullName] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCodeRefListID] [char] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCodeRefFullName] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
