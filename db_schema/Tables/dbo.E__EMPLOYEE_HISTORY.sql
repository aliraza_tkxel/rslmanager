USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E__EMPLOYEE_HISTORY')
BEGIN

	CREATE TABLE [dbo].[E__EMPLOYEE_HISTORY](
		[EMPLOYEEHISTORYID] [int] IDENTITY(1,1) NOT NULL,
		[EMPLOYEEID] [int]  NOT NULL,
		[CREATIONDATE] [smalldatetime] NULL,
		[TITLE] [int] NULL,
		[FIRSTNAME] [nvarchar](50) NULL,
		[MIDDLENAME] [nvarchar](50) NULL,
		[LASTNAME] [nvarchar](100) NULL,
		[DOB] [smalldatetime] NULL,
		[GENDER] [nvarchar](20) NULL,
		[MARITALSTATUS] [int] NULL,
		[ETHNICITY] [int] NULL,
		[ETHNICITYOTHER] [nvarchar](80) NULL,
		[RELIGION] [int] NULL,
		[RELIGIONOTHER] [nvarchar](80) NULL,
		[SEXUALORIENTATION] [int] NULL,
		[SEXUALORIENTATIONOTHER] [nvarchar](80) NULL,
		[DISABILITY] [nvarchar](80) NULL,
		[DISABILITYOTHER] [nvarchar](80) NULL,
		[BANK] [nvarchar](100) NULL,
		[SORTCODE] [nvarchar](30) NULL,
		[ACCOUNTNUMBER] [nvarchar](30) NULL,
		[ACCOUNTNAME] [nvarchar](100) NULL,
		[ROLLNUMBER] [nvarchar](30) NULL,
		[BANK2] [nvarchar](100) NULL,
		[SORTCODE2ND] [nvarchar](50) NULL,
		[ACCOUNTNUMBER2] [nvarchar](50) NULL,
		[ACCOUNTNAME2] [nvarchar](100) NULL,
		[ROLLNUMBER2] [nvarchar](50) NULL,
		[ORGID] [int] NULL,
		[IMAGEPATH] [nvarchar](250) NULL,
		[ROLEPATH] [nvarchar](250) NULL,
		[PROFILE] [varchar](max) NULL,
		[LASTLOGGEDIN] [smalldatetime] NULL,
		[SIGNATUREPATH] [nvarchar](250) NULL,
		[JobRoleTeamId] [int] NULL,
		[LASTACTIONTIME] [datetime] NULL,
		[LASTACTIONUSER] [int] NULL,
		[EmploymentContracts] [nvarchar](500) NULL,
		[AKA] [nvarchar](100) NULL,
		[NiNumber] [nvarchar](100) NULL,
		[DBS_Date] [datetime] NULL,
		[Reference] [nvarchar](100) NULL,
		[NextOfKin] [nvarchar](100) NULL,
	 CONSTRAINT [PK_EMPLOYEES_History] PRIMARY KEY NONCLUSTERED 
	(
		EMPLOYEEHISTORYID ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY]

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 