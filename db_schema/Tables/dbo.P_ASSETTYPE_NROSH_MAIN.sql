USE RSLBHALive
GO
BEGIN TRANSACTION
BEGIN TRY
--Check if table exists or not
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_ASSETTYPE_NROSH_MAIN')
BEGIN

	CREATE TABLE [dbo].[P_ASSETTYPE_NROSH_MAIN]
	(
	[Sid] [int] NOT NULL,
	[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	) ON [PRIMARY]

END


IF COL_LENGTH('P_ASSETTYPE_NROSH_MAIN', 'IsActive') IS NULL
BEGIN
	ALTER TABLE P_ASSETTYPE_NROSH_MAIN 
	ADD IsActive int NULL
	Print('IsActive added successfully  ') 
	
END

IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where IsActive=1)
	BEGIN 
		Update P_ASSETTYPE_NROSH_MAIN set IsActive=1
	END

	IF  EXISTS (Select * from P_ASSETTYPE_NROSH_MAIN where Description='Non Social Rented Housing')
	BEGIN 
		Update P_ASSETTYPE_NROSH_MAIN set IsActive=0 where Description='Non Social Rented Housing'
	END
	IF  EXISTS (Select * from P_ASSETTYPE_NROSH_MAIN where Description='Low Cost Home Ownership')
	BEGIN 
		Update P_ASSETTYPE_NROSH_MAIN set IsActive=0 where Description='Low Cost Home Ownership'
	END

	
IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Parking Space')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (5,'Parking Space',1)
		Print('Parking space added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Garage')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (6,'Garage',1)
		Print('Garage added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Commercial')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (7,'Commercial',1)
		Print('Commercial added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Residential')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (8,'Residential',1)
		Print('Residential added successfully  ') 
	END



IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Shared Ownership')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (9,'Shared Ownership',1)
		Print('Shared Ownership added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Older Persons Shared Ownership')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (10,'Older Persons Shared Ownership',1)
		Print('Older Persons Shared Ownership added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Shared Equity')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (11,'Shared Equity',1)
		Print('Shared Equity added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Outright Ownership')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (12,'Outright Ownership',1)
		Print('Outright Ownership added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Intermediate Rented')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (13,'Intermediate Rented',1)
		Print('Intermediate Rented added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Homelessness')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (14,'Homelessness',1)
		Print('Homelessness added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Domestic Abuse')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (15,'Domestic Abuse',1)
		Print('Domestic Abuse added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Mental Health')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (16,'Mental Health',1)
		Print('Mental Health added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Learning Disabilities')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (17,'Learning Disabilities',1)
		Print('Learning Disabilities added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Housing with Care')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (18,'Housing with Care',1)
		Print('Housing with Care added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Sheltered')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (19,'Sheltered',1)
		Print('Sheltered added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Care Leavers')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (20,'Care Leavers',1)
		Print(' Care Leavers added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Over 55�s')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (21,'Over 55�s',1)
		Print('Over 55�s added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='House in Multiple Occupation')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (22,'House in Multiple Occupation',1)
		Print('House in Multiple Occupation added successfully  ') 
	END
IF NOT EXISTS (Select 1 from P_ASSETTYPE_NROSH_MAIN where DESCRIPTION='Gypsy and Traveller')
	BEGIN  
		INSERT INTO P_ASSETTYPE_NROSH_MAIN (Sid, DESCRIPTION,IsActive)Values (23,'Gypsy and Traveller',1)
		Print('Gypsy and Traveller added successfully  ') 
	END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
