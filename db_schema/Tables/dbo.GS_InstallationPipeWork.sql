CREATE TABLE [dbo].[GS_InstallationPipeWork]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[AppointmentID] [int] NOT NULL,
[EmergencyControl] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VisualInspection] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GasTightnessTest] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EquipotentialBonding] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [smalldatetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_InstallationPipeWork] ADD 
CONSTRAINT [PK_GS_InstallationPipeWork] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[GS_InstallationPipeWork] ADD
CONSTRAINT [FK_GS_InstallationPipeWork_PS_Appointment] FOREIGN KEY ([AppointmentID]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO
