USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_ServicingType]    Script Date: 17-Jul-18 3:20:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT *
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'P_ServicingType') 
BEGIN
CREATE TABLE [dbo].[P_ServicingType](
	[ServicingTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
 CONSTRAINT [PK_P_ServicingType] PRIMARY KEY CLUSTERED 
(
	[ServicingTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT [dbo].[P_ServicingType] ([Description]) VALUES ('Gas')
INSERT [dbo].[P_ServicingType] ([Description]) VALUES ('Oil')
INSERT [dbo].[P_ServicingType] ([Description]) VALUES ('M&E Servicing')
INSERT [dbo].[P_ServicingType] ([Description]) VALUES ('PAT Testing')
INSERT [dbo].[P_ServicingType] ([Description]) VALUES ('Alternative Servicing')
END

ELSE
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM [dbo].[P_ServicingType]
		WHERE Description = 'Alternative Servicing') 
		BEGIN
		INSERT [dbo].[P_ServicingType] ([Description])
			VALUES ( N'Alternative Servicing')
			PRINT ('Added Alternative Servicing')
		END
	END
GO


