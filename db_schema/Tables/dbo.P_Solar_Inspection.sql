USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_Solar_Inspection')
BEGIN

CREATE TABLE [dbo].[P_Solar_Inspection](
	[InspectionId] [int] IDENTITY(1,1) NOT NULL,	
	[HeatingMappingId] [int] NULL,
	[JournalId] [int] NULL,
	[IsInspected] [bit] NULL,
	[InspectionDate] [smalldatetime] NULL,
	[InspectedBy] [int] NULL,
	[VesselProtectionInstalled] [nvarchar](20) NULL,
	[VesselCapacity] [nvarchar](20) NULL,	
	[MinPressure] [nvarchar](20) NULL,
	[FreezingTemp] [nvarchar](20) NULL,
	[SystemPressure] [nvarchar](20) NULL,
	[BackPressure] [nvarchar](300) NULL,	
	[DeltaOn] [nvarchar](20) NULL,
	[DeltaOff] [nvarchar](20) NULL,	
	[MaxTemperature] [nvarchar](20) NULL,
	[CalculationRate] [nvarchar](20) NULL,
	[ThermostatTemperature] [nvarchar](20) NULL,
	[AntiScaldingControl] [bit] NULL,	
	[AntiScaldingControlDetail] [nvarchar](300) NULL,
	[CheckDirection] [bit] NULL,	
	[DirectionDetail] [nvarchar](300) NULL,
	[CheckElectricalControl] [bit] NULL,
	[ElectricalControlDetail] [nvarchar](300) NULL,

 CONSTRAINT [PK_P_Solar_Inspection] PRIMARY KEY CLUSTERED 
(
	[InspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 