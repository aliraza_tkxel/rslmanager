CREATE TABLE [dbo].[I_PAGE]
(
[PageID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[WebsiteID] [int] NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NULL CONSTRAINT [DF_I_PAGE_Active] DEFAULT ((0)),
[IsBespoke] [bit] NULL,
[IsDefault] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE NONCLUSTERED INDEX [_dta_index_I_PAGE_5_745821769__K8_K1] ON [dbo].[I_PAGE] ([Active], [PageID]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_I_PAGE_5_745821769__K6_1] ON [dbo].[I_PAGE] ([FileName]) INCLUDE ([PageID]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[I_PAGE] ADD CONSTRAINT [PK_I_PAGE] PRIMARY KEY CLUSTERED  ([PageID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[I_PAGE] ADD CONSTRAINT [FK_I_PAGE_I_MENU] FOREIGN KEY ([MenuID]) REFERENCES [dbo].[I_MENU] ([MenuID])
GO
