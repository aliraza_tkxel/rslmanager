USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[V_RequiredWorksHistory]    Script Date: 18-Sep-17 3:22:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'V_RequiredWorksHistory')
BEGIN

CREATE TABLE [dbo].[V_RequiredWorksHistory](
	[RequiredWorksHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[RequiredWorksId] [int] NULL,
	[InspectionJournalId] [int] NULL,
	[IsMajorWorksRequired] [bit] NULL,
	[RoomId] [int] NULL,
	[OtherLocation] [varchar](100) NULL,
	[WorkDescription] [ntext] NULL,
	[VoidWorksNotes] [ntext] NULL,
	[TenantNeglectEstimation] [money] NULL,
	[ComponentId] [int] NULL,
	[ReplacementDue] [date] NULL,
	[Condition] [varchar](100) NULL,
	[WorksJournalId] [int] NULL,
	[IsTenantWorks] [bit] NULL,
	[IsBrsWorks] [bit] NULL,
	[StatusId] [int] NULL,
	[IsScheduled] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsCanceled] [bit] NULL,
	[Duration] [float] NULL,
	[IsVerified] [bit] NULL CONSTRAINT [DF__V_RequireHistory__IsVer__787031BF]  DEFAULT ((0)),
	[MajorWorkNotes] [nvarchar](500) NULL,
	[workType] [nvarchar](200) NULL,
	[faultAreaID] [int] NULL,
	[faultID] [int] NULL,
	[isRecurringProblem] [bit] NULL,
	[faultNotes] [ntext] NULL,
	[problemDays] [int] NULL,
	[JobsheetCompletionDate] [smalldatetime] NULL,
	[JobsheetCompletedAppVersion] [nvarchar](20) NULL,
	[JobsheetCurrentAppVersion] [nvarchar](20) NULL,
	[repairId] [int] NULL,
 CONSTRAINT [PK_V_RequiredWorks_History] PRIMARY KEY CLUSTERED 
(
	[RequiredWorksHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [dbo].[V_RequiredWorksHistory]  WITH CHECK ADD  CONSTRAINT [FK_V_RequiredWorksHistory_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])


ALTER TABLE [dbo].[V_RequiredWorksHistory] CHECK CONSTRAINT [FK_V_RequiredWorksHistory_PDR_JOURNAL]



END

declare @datatype varchar(max)
 set @datatype  = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'V_RequiredWorksHistory' AND 
     COLUMN_NAME = 'WorkDescription')
if  (@datatype = 'ntext')
begin
	ALTER TABLE V_RequiredWorksHistory
	ALTER COLUMN WorkDescription NVARCHAR(MAX)
	Print 'WorkDescription Data type changed'
END

 set @datatype  = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'V_RequiredWorksHistory' AND 
     COLUMN_NAME = 'VoidWorksNotes')
if  (@datatype = 'ntext')
begin
	ALTER TABLE V_RequiredWorksHistory
	ALTER COLUMN VoidWorksNotes NVARCHAR(MAX)
	Print 'VoidWorksNotes Data type changed'
END

 set @datatype  = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'V_RequiredWorksHistory' AND 
     COLUMN_NAME = 'faultNotes')
if  (@datatype = 'ntext')
begin
	ALTER TABLE V_RequiredWorksHistory
	ALTER COLUMN faultNotes NVARCHAR(MAX)
	Print 'faultNotes Data type changed'
END


--===============================================================
-- US714 - Added isLegionella column
--===============================================================
	IF COL_LENGTH('V_RequiredWorksHistory', 'isLegionella') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorksHistory
			ADD isLegionella bit NULL
	END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
