CREATE TABLE [dbo].[AM_LookupType]
(
[LookupTypeId] [int] NOT NULL IDENTITY(1, 1),
[TypeName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedBy] [int] NULL,
[CreatedDate] [datetime] NULL,
[ModifiedBy] [int] NULL,
[ModifiedDate] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_LookupType] ADD 
CONSTRAINT [PK_AM_LookupType] PRIMARY KEY CLUSTERED  ([LookupTypeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
