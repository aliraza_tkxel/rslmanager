BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_POSTATUS')
BEGIN
  PRINT 'Table NOT Exists';
 


CREATE TABLE [dbo].[F_POSTATUS]
(
[POSTATUSID] [int] NOT NULL,
[POSTATUSNAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POTYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

END --if

ELSE
BEGIN
IF NOT EXISTS (Select 1 from F_POSTATUS where POSTATUSNAME='Declined')
BEGIN
DECLARE @poStatusId INT
SELECT @poStatusId= MAX(POSTATUSID) FROM F_POSTATUS
SET @poStatusId = @poStatusId + 1
INSERT INTO F_POSTATUS (POSTATUSID,POSTATUSNAME,POTYPE) VALUES (@poStatusId,'Declined',NULL)
PRINT 'New PO status added successfully..'
END

IF NOT EXISTS (Select 1 from F_POSTATUS where POSTATUSNAME='Invoice Received - In Dispute')
BEGIN
SELECT @poStatusId= MAX(POSTATUSID) FROM F_POSTATUS
SET @poStatusId = @poStatusId + 1
INSERT INTO F_POSTATUS (POSTATUSID,POSTATUSNAME,POTYPE) VALUES (@poStatusId,'Invoice Received - In Dispute',NULL)
PRINT 'New PO status added successfully..'
END

END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH

GO

BEGIN TRANSACTION
BEGIN TRY



--===============================================================================
		-- RENAME COMPLETED REPORT
		--===============================================================================
		IF NOT EXISTS(SELECT * FROM	F_POSTATUS WHERE POSTATUSNAME = 'Part Paid')
							
		BEGIN
		DECLARE @SatusId INT
		Select @SatusId = MAX(POSTATUSID)+1 from F_POSTATUS
		
			INSERT INTO F_POSTATUS(POSTATUSID,POSTATUSNAME)
			VALUES(@SatusId,'Part Paid')
			Print('Part Paid added successfully')
		END  
		
		
		


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ScriptErrorMessage NVARCHAR(4000);
	DECLARE @ScriptErrorSeverity INT;
	DECLARE @ScriptErrorState INT;

	SELECT @ScriptErrorMessage = ERROR_MESSAGE(),
	@ScriptErrorSeverity = ERROR_SEVERITY(),
	@ScriptErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ScriptErrorMessage, @ScriptErrorSeverity, @ScriptErrorState);
Print (@ScriptErrorMessage)
END CATCH
		