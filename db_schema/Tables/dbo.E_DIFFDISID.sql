/****** Script for SelectTopNRows command from SSMS  ******/

USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

-------------------------------- CREATING TABLE IF NOT EXISTS ------------------------------------------

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			WHERE TABLE_NAME = N'E_DIFFDISID')
BEGIN

	CREATE TABLE [dbo].[E_DIFFDISID]
	(
	[DISABILITYID] [int] NOT NULL IDENTITY(1, 1),
	[EMPLOYEEID] [int] NULL,
	[DIFFDIS] [int] NULL,
	[NOTES] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LASTACTIONUSER] [int] NULL,
	[LASTACTIONTIME] [datetime] NULL CONSTRAINT [DF_E_DIFFDISID_LASTACTIONTIME] DEFAULT (getdate())
	) ON [PRIMARY]
	
END	
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ISLONGTERM'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
  
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD ISLONGTERM bit NULL
	PRINT 'Added Column ISLONGTERM'
END

if EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE TABLE_NAME = 'E_DIFFDISID' AND COLUMN_NAME = 'ISLONGTERM'
			AND DATA_TYPE = 'int')
BEGIN
	ALTER TABLE [dbo].[E_DIFFDISID] ALTER COLUMN ISLONGTERM bit NULL
	Print 'Change data type of ISLONGTERM'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IMPACTONWORK'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD IMPACTONWORK nvarchar(300)
	PRINT 'Added Column IMPACTONWORK'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ISOCCUPATIONALHEALTH'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD ISOCCUPATIONALHEALTH bit
	PRINT 'Added Column ISOCCUPATIONALHEALTH'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'REVIEWDATE'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD REVIEWDATE datetime
	PRINT 'Added Column REVIEWDATE'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DOCUMENTTITLE'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD DOCUMENTTITLE nvarchar(300)
	PRINT 'Added Column DOCUMENTTITLE'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DOCUMENTPATH'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD DOCUMENTPATH nvarchar(MAX)
	PRINT 'Added Column DOCUMENTPATH'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ISDELETE'
      AND Object_ID = Object_ID(N'E_DIFFDISID'))
BEGIN
    ALTER TABLE [dbo].[E_DIFFDISID]
	ADD ISDELETE bit
	EXEC('UPDATE E_DIFFDISID
			SET ISDELETE=0');
	PRINT 'Added Column ISDELETE'
END



IF COL_LENGTH('E_DIFFDISID', 'NotifiedDate') IS NULL
		BEGIN
			ALTER TABLE E_DIFFDISID 
			ADD NotifiedDate SMALLDATETIME NULL
			Print('Added NotifiedDate') 
		END	

IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH


