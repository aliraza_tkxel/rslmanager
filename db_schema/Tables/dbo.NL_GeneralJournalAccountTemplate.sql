USE [RSLBHALive]

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'NL_GeneralJournalAccountTemplate')
							
BEGIN
	
CREATE TABLE [dbo].[NL_GeneralJournalAccountTemplate](
	[TemplateId] [int] IDENTITY(1,1) NOT NULL,
	[GeneralJournalTemplateId] [int] NULL,
	[Details] [nvarchar](1000) NULL,
	[AccountId] [int] NULL,
	[IsDebit] [bit] NULL,
	[CostCenterId] [int] NULL,
	[HeadId] [int] NULL,
	[ExpenditureId] [int] NULL,
	[Reason] [nvarchar](4000) NULL,
	[DefaultDebitAmount] [money] NULL,
	[DefaultCreditAmount] [money] NULL,
	[Sequence] [int] NULL,
 CONSTRAINT [PK_NL_GeneralJournalAccountTemplate] PRIMARY KEY CLUSTERED 
(
	[TemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
				
END   


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
