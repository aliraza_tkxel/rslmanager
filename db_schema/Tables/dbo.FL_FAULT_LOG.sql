USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'FL_FAULT_LOG')
    BEGIN

CREATE TABLE [dbo].[FL_FAULT_LOG](
	[FaultLogID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[FaultID] [int] NULL,
	[FaultBasketID] [int] NULL,
	[SubmitDate] [datetime] NULL,
	[ORGID] [int] NULL,
	[StatusID] [int] NOT NULL,
	[IsReported] [bit] NOT NULL,
	[Quantity] [int] NULL,
	[ProblemDays] [int] NULL,
	[RecuringProblem] [bit] NULL,
	[CommunalProblem] [bit] NULL,
	[Notes] [varchar](4000) NULL,
	[JobSheetNumber] [varchar](50) NULL,
	[DueDate] [datetime] NULL,
	[IsSelected] [bit] NULL,
	[UserId] [int] NULL,
	[Recharge] [bit] NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[ContractorID] [smallint] NULL,
	[FaultTradeID] [int] NULL,
	[CompletedDate] [datetime] NULL,
	[FollowOnFaultLogId] [int] NULL,
	[Duration] [decimal](9, 2) NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[areaId] [int] NULL,
 CONSTRAINT [PK_FL_FAULT_LOG] PRIMARY KEY CLUSTERED 
(
	[FaultLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF


ALTER TABLE [dbo].[FL_FAULT_LOG]  WITH CHECK ADD  CONSTRAINT [FK_FL_FAULT_LOG_FL_AREA] FOREIGN KEY([areaId])
REFERENCES [dbo].[FL_AREA] ([AreaID])


ALTER TABLE [dbo].[FL_FAULT_LOG] CHECK CONSTRAINT [FK_FL_FAULT_LOG_FL_AREA]

 END
    ELSE

    BEGIN
      PRINT 'Table Already Exist';
    END

    --========================================================================================================
    ------Adding Index
    --========================================================================================================


    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_FL_FAULT_LOG_FollowonFaultLogID')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_FL_FAULT_LOG_FollowonFaultLogID
		  ON FL_FAULT_LOG (FollowonFaultLogID);

		  PRINT 'IX_FL_FAULT_LOG_FollowonFaultLogID created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_FL_FAULT_LOG_FollowonFaultLogID Index Already Exist';
	  END

    
    --========================================================================================================
	-- Addition of app version while completing appointment
    --========================================================================================================

	IF COL_LENGTH('FL_FAULT_LOG', 'JobsheetCompletedAppVersion') IS NULL
	BEGIN
			ALTER TABLE FL_FAULT_LOG
			ADD JobsheetCompletedAppVersion nvarchar(20) NULL
			PRINT('COLUMN JobsheetCompletedAppVersion CREATED')
	END

	IF COL_LENGTH('FL_FAULT_LOG', 'JobsheetCurrentAppVersion') IS NULL
	BEGIN
			ALTER TABLE FL_FAULT_LOG
			ADD JobsheetCurrentAppVersion nvarchar(20) NULL
			PRINT('COLUMN JobsheetCurrentAppVersion CREATED')
	END

declare @datatype varchar(max) = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'FL_FAULT_LOG' AND 
     COLUMN_NAME = 'SubmitDate')
if  (@datatype = 'smalldatetime')
begin
	ALTER TABLE FL_FAULT_LOG
	ALTER COLUMN SubmitDate datetime
	Print 'Logged Date Data type changed'
END

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH


