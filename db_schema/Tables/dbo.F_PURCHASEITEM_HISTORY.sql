BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_PURCHASEITEM_HISTORY')
BEGIN

	CREATE TABLE [dbo].[F_PURCHASEITEM_HISTORY]
	(
	[HISTORYID] [int] NOT NULL IDENTITY(1, 1),
	[ORDERID] [int] NULL,
	[ORDERITEMID] [int] NULL,
	[PIDATE] [smalldatetime] NULL,
	[EXPENDITUREID] [int] NULL,
	[VAT] [float] NULL,
	[VATTYPE] [int] NULL,
	[NETCOST] [float] NULL,
	[GROSSCOST] [float] NULL,
	[USERID] [int] NULL,
	[HISTORY_TIMESTAMP] [smalldatetime] NULL,
	[PITYPE] [int] NULL,
	[PISTATUS] [int] NULL
	) ON [PRIMARY]

END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IsAmend'
          AND Object_ID = Object_ID(N'F_PURCHASEITEM_HISTORY'))
BEGIN
ALTER TABLE F_PURCHASEITEM_HISTORY ADD IsAmend BIT NOT NULL DEFAULT 0;
Print ('Added IsAmend in F_PURCHASEITEM_HISTORY.')
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
