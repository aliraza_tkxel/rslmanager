USE RSLBHALive
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'F_EXPENDITURE_ALLOCATION')      
BEGIN
	PRINT 'Table Exists';
	DECLARE @CostCentreId INT
	DECLARE @MaxFiscalYearId INT
	DECLARE @MaxFiscalYearIdInAllocationTable INT
	SELECT @CostCentreId=COSTCENTREID FROM F_COSTCENTRE WHERE DESCRIPTION ='Services'
	SELECT @MaxFiscalYearId= MAX(YRange)FROM F_FISCALYEARS 
	
	Select 
	@MaxFiscalYearIdInAllocationTable =MAX(F_EXPENDITURE_ALLOCATION.FISCALYEAR)
	FROM F_EXPENDITURE_ALLOCATION
	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_EXPENDITURE_ALLOCATION.EXPENDITUREID
	INNER join F_HEAD ON F_HEAD.HEADID = F_EXPENDITURE.HEADID
	WHERE COSTCENTREID=@CostCentreId
	
	
	
	DECLARE @ExpenditureId INT
	DECLARE @ExpenditureAllocation INT
	DECLARE cur CURSOR FOR 
	
	Select 
	F_EXPENDITURE_ALLOCATION.EXPENDITUREID,  
	F_EXPENDITURE_ALLOCATION.EXPENDITUREALLOCATION 
	FROM F_EXPENDITURE_ALLOCATION
	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_EXPENDITURE_ALLOCATION.EXPENDITUREID
	INNER join F_HEAD ON F_HEAD.HEADID = F_EXPENDITURE.HEADID
	WHERE COSTCENTREID=@CostCentreId AND FISCALYEAR=@MaxFiscalYearIdInAllocationTable	
	
	OPEN cur

	FETCH NEXT FROM cur 
	INTO @ExpenditureId,@ExpenditureAllocation	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	--print @ExpenditureAllocation
	--print @ExpenditureId
	
	IF NOT EXISTS (SELECT 1 FROM F_EXPENDITURE_ALLOCATION WHERE FISCALYEAR=@MaxFiscalYearId AND EXPENDITUREID =@ExpenditureId)
	BEGIN
		INSERT INTO F_EXPENDITURE_ALLOCATION
		(F_EXPENDITURE_ALLOCATION.EXPENDITUREID,FISCALYEAR, EXPENDITUREALLOCATION, MODIFIED, MODIFIEDBY,ACTIVE)
		VALUES
		(@ExpenditureId,@MaxFiscalYearId,@ExpenditureAllocation,GETDATE(),113, 1)
	END
	
	
	FETCH NEXT FROM cur 
	INTO @ExpenditureId,@ExpenditureAllocation
	END
	CLOSE cur;
	DEALLOCATE cur;
	
	
END --if
ELSE
PRINT 'Table not Exists ';


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

IF not EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_HEAD_ALLOCATION'))
BEGIN
alter table F_EXPENDITURE_ALLOCATION add CompanyId INT
END
GO

IF NOT EXISTS (SELECT * FROM dbo.F_EXPENDITURE_ALLOCATION WHERE CompanyId = 1)
 BEGIN 

		UPDATE dbo.F_COSTCENTRE_ALLOCATION SET CompanyId = 1


		UPDATE dbo.F_EXPENDITURE_ALLOCATION SET CompanyId = 1


		UPDATE dbo.F_HEAD_ALLOCATION SET CompanyId = 1

END


IF not EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'VATControlActive'
          AND Object_ID = Object_ID(N'F_EXPENDITURE_ALLOCATION'))
BEGIN
alter table F_EXPENDITURE_ALLOCATION add VATControlActive bit


END
GO