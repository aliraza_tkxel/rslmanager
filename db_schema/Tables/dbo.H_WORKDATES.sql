CREATE TABLE [dbo].[H_WORKDATES]
(
[DateID] [int] NOT NULL IDENTITY(1, 1),
[StartDate] [datetime] NULL,
[Days] [int] NULL,
[DateComplete] [datetime] NULL,
[EstCompDate] [datetime] NULL,
[JobID] [int] NULL
) ON [PRIMARY]
GO
