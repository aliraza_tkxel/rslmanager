Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF Not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_GiftsAndHospitalities')
BEGIN


CREATE TABLE [dbo].[E_GiftsAndHospitalities](
	[Details] [nvarchar](500) NULL,
	[DateRecieved] [datetime] NULL,
	[DateGiven] [datetime] NULL,
	[OfferedBy] [nvarchar](50) NULL,
	[OfferedTo] [nvarchar](50) NULL,
	[ApproximateValue] [float] NULL,
	[Accepted] [bit] NULL,
	[NotApprovedDate] [datetime] NULL,
	[DateOfDeclaration] [datetime] NULL,
	[Notes] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[GiftId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[IsRecieved] [bit] NULL,
	[fiscalYearId] [int] NULL,
	[documentId] [int] NULL,
	[GiftStatusId] [int] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[IsAccepted] [int] NULL,
 CONSTRAINT [PK_E_GiftsAndHospitalities] PRIMARY KEY CLUSTERED 
(
	[GiftId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'GiftStatusId'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [GiftStatusId] [int] NULL
	PRINT 'Added Column [GiftStatusId]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'CreatedDate'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [CreatedDate] [datetime] NULL
	PRINT 'Added Column [CreatedDate]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'CreatedBy'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [CreatedBy] [int] NULL
	PRINT 'Added Column [CreatedBy]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'UpdatedDate'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [UpdatedDate] [datetime] NULL
	PRINT 'Added Column [UpdatedDate]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'UpdatedBy'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [UpdatedBy] [int] NULL
	PRINT 'Added Column [UpdatedBy]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DateGivenReceived'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [DateGivenReceived] [datetime] NULL
	PRINT 'Added Column [DateGivenReceived]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsGivenReceived'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [IsGivenReceived] [int] NULL
	PRINT 'Added Column [IsGivenReceived]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'OfferToBy'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [OfferToBy] [nvarchar](50) NULL
	PRINT 'Added Column [OfferToBy]'
END

-- drop column start
IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DateRecieved'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
	ALTER TABLE E_GiftsAndHospitalities
	DROP COLUMN DateRecieved;
	PRINT 'DROP Column [DateRecieved]'
END

IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DateGiven'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
	ALTER TABLE E_GiftsAndHospitalities
	DROP COLUMN DateGiven;
	PRINT 'DROP Column [DateGiven]'
END

IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsRecieved'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
	ALTER TABLE E_GiftsAndHospitalities
	DROP COLUMN IsRecieved;
	PRINT 'DROP Column [IsRecieved]'
END

IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'OfferedBy'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
	ALTER TABLE E_GiftsAndHospitalities
	DROP COLUMN OfferedBy;
	PRINT 'DROP Column [OfferedBy]'
END

IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'OfferedTo'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
	ALTER TABLE E_GiftsAndHospitalities
	DROP COLUMN OfferedTo;
	PRINT 'DROP Column [OfferedTo]'
END

-- drop column end

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Reason'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [Reason] [nvarchar](500) NULL
	PRINT 'Added Column [Reason]'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsAccepted'
      AND Object_ID = Object_ID(N'E_GiftsAndHospitalities'))
BEGIN
    ALTER TABLE [dbo].[E_GiftsAndHospitalities]
	ADD [IsAccepted] [int] NULL
	PRINT 'Added Column [IsAccepted]'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 





