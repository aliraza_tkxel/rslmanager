CREATE TABLE [dbo].[FL_CO_FAULT_LETTER_LOG]
(
[FaultLetterLogID] [int] NOT NULL IDENTITY(1, 1),
[FaultLogID] [int] NULL,
[NatureID] [int] NULL,
[SubItemID] [int] NULL,
[LetterCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LetterDesc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LetterText] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormID] [int] NULL,
[ToID] [int] NULL,
[TempLetter] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TempSig] [nvarchar] (1900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LetterSig] [nvarchar] (1900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[FL_CO_FAULT_LETTER_LOG] ADD 
CONSTRAINT [PK_FL_FAULT_LETTER] PRIMARY KEY CLUSTERED  ([FaultLetterLogID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
