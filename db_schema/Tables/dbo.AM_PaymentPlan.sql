USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AM_PaymentPlan')
    BEGIN		
			CREATE TABLE [dbo].[AM_PaymentPlan]
			(
			[PaymentPlanId] [int] NOT NULL IDENTITY(1, 1),
			[TennantId] [int] NOT NULL,
			[PaymentPlanType] [int] NULL,
			[StartDate] [datetime] NULL,
			[EndDate] [datetime] NOT NULL,
			[FrequencyLookupCodeId] [int] NOT NULL,
			[AmountToBeCollected] [float] NOT NULL,
			[ArrearsCollection] [float] NULL,
			[WeeklyRentAmount] [float] NOT NULL,
			[ReviewDate] [datetime] NOT NULL,
			[FirstCollectionDate] [datetime] NULL,
			[IsCreated] [bit] NOT NULL,
			[CreatedDate] [datetime] NOT NULL,
			[ModifiedDate] [datetime] NOT NULL,
			[CreatedBy] [int] NOT NULL,
			[ModifiedBy] [int] NOT NULL,
			[RentBalance] [float] NULL,
			[LastPayment] [float] NULL,
			[LastPaymentDate] [datetime] NULL,
			[RentPayable] [float] NULL,
			[IsActive] [bit] NULL
			) ON [PRIMARY]
			ALTER TABLE [dbo].[AM_PaymentPlan] ADD 
			CONSTRAINT [PK_AM_PaymentPlan] PRIMARY KEY CLUSTERED  ([PaymentPlanId]) WITH (FILLFACTOR=100) ON [PRIMARY]
			

			ALTER TABLE [dbo].[AM_PaymentPlan] ADD CONSTRAINT [FK_AM_PaymentPlan_AM_Resource] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
			
			ALTER TABLE [dbo].[AM_PaymentPlan] ADD CONSTRAINT [FK_AM_PaymentPlan_AM_LookupCode] FOREIGN KEY ([FrequencyLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
			
			ALTER TABLE [dbo].[AM_PaymentPlan] ADD CONSTRAINT [FK_AM_PaymentPlan_AM_Resource1] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
			
			ALTER TABLE [dbo].[AM_PaymentPlan] ADD CONSTRAINT [FK_AM_PaymentPlan_AM_LookupCode1] FOREIGN KEY ([PaymentPlanType]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
			

 END
ELSE

BEGIN
PRINT 'Table Already Exist';
END
    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_PaymentPlan_IsCreated')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_PaymentPlan_IsCreated
		  ON AM_PaymentPlan (IsCreated);  
	  PRINT 'IX_AM_PaymentPlan_IsCreated created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_PaymentPlan_IsCreated Index Already Exist';
	  END

   


    --========================================================================================================
    --======================================================================================================== 
    

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    