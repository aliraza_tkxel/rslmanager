USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'CM_ServiceItems')
BEGIN

	CREATE TABLE [dbo].[CM_ServiceItems](
		[ServiceItemId] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[ContractorId] [int] NULL,
	[ContractCommencement] [smalldatetime] NULL,
	[ContractPeriod] [int] NULL,
	[ContractPeriodType] [int] NULL,
	[Cycle] [int] NULL,
	[CycleType] [int] NULL,
	[CycleValue] [money] NULL,
	[PORef] [int] NULL,
	[StatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Active] [bit] NULL,
	[BFSCarryingOutWork] [bit] NOT NULL CONSTRAINT [D_CM_ServiceItems_BFSCarryingOutWork]  DEFAULT ((0)),
	[ChildAttributeMappingId] [int] NULL CONSTRAINT [D_CM_ServiceItems_ChildAttributeMappingId]  DEFAULT (NULL),
	[TotalValue] [nvarchar](100) NULL CONSTRAINT [D_CM_ServiceItems_TotalValue]  DEFAULT (NULL),
	[VAT] [nvarchar](100) NULL CONSTRAINT [D_CM_ServiceItems_VAT]  DEFAULT (NULL),
	[CustomCycleFrequency] [int] NULL CONSTRAINT [D_CM_ServiceItems_CustomCycleFrequency]  DEFAULT (NULL),
	[CustomCycleOccurance] [nvarchar](100) NULL CONSTRAINT [D_CM_ServiceItems_CustomCycleOccurance]  DEFAULT (NULL),
	[CustomCycleType] [nvarchar](20) NULL CONSTRAINT [D_CM_ServiceItems_CustomCycleType]  DEFAULT (NULL),
	 CONSTRAINT [PK_CM_ServicesItem] PRIMARY KEY CLUSTERED 
	(
		[ServiceItemId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




if exists(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME = 'CM_ServiceItems' AND COLUMN_NAME = 'CustomCycleOccurance'
AND DATA_TYPE = 'nvarchar' AND CHARACTER_MAXIMUM_LENGTH = 10)
BEGIN
	ALTER TABLE [CM_ServiceItems]
	ALTER COLUMN [CustomCycleOccurance] NVARCHAR(500)
	pRINT 'Changed CustomCycleOccurance datatype from nvarchar(10) to nvarchar(500)'
END
