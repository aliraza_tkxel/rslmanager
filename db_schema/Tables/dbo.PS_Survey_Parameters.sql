CREATE TABLE [dbo].[PS_Survey_Parameters]
(
[SID] [int] NOT NULL IDENTITY(1, 1),
[SurveyId] [int] NULL,
[ItemParamId] [int] NULL,
[ValueId] [bigint] NULL,
[PARAMETERVALUE] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCheckBoxSelected] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Parameters] ADD 
CONSTRAINT [pk_PS_Survey_Parameters] PRIMARY KEY CLUSTERED  ([SID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Parameters] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Parameters_PA_ITEM_PARAMETER] FOREIGN KEY ([ItemParamId]) REFERENCES [dbo].[PA_ITEM_PARAMETER] ([ItemParamID])
ALTER TABLE [dbo].[PS_Survey_Parameters] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Parameters_PA_PARAMETER_VALUE] FOREIGN KEY ([ValueId]) REFERENCES [dbo].[PA_PARAMETER_VALUE] ([ValueID])
ALTER TABLE [dbo].[PS_Survey_Parameters] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Parameters_PS_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[PS_Survey] ([SurveyId])
GO
