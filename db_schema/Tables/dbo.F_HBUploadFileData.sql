USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_HBUploadFileData')
BEGIN
CREATE TABLE [dbo].[F_HBUploadFileData](
	[HBUploadFileDataId] [int] IDENTITY(1,1) NOT NULL,
	[HbUploadId] [int] NULL,
	[HBRef] [nvarchar](200) NULL,	
	[Amount] [float] NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[TenantName] [nvarchar](200) NULL,
	[Address] [nvarchar](200) NULL,
	[IsExcluded] [bit] NULL,
	[HBRow] [int] NULL,
 CONSTRAINT [PK_F_HBUploadFileData] PRIMARY KEY CLUSTERED 
(
	[HBUploadFileDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[F_HBUploadFileData]  WITH CHECK ADD  CONSTRAINT [FK_F_HBUploadFileData_F_HBUpload] FOREIGN KEY([HbUploadId])
REFERENCES [dbo].[F_HBUpload] ([HbUploadId])

ALTER TABLE [dbo].[F_HBUploadFileData] CHECK CONSTRAINT [FK_F_HBUploadFileData_F_HBUpload]

END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
