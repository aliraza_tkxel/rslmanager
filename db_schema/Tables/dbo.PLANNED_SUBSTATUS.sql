USE rslbhalive 

go 

BEGIN TRANSACTION 

BEGIN try 
    IF NOT EXISTS (SELECT * 
                   FROM   information_schema.tables 
                   WHERE  table_name = N'planned_substatus') 
      BEGIN 
          CREATE TABLE [dbo].[planned_substatus] 
            ( 
               [substatusid] [SMALLINT] NOT NULL IDENTITY(1, 1), 
               [statusid]    [SMALLINT] NULL, 
               [title]       [NVARCHAR] (150) COLLATE 
               sql_latin1_general_cp1_ci_as 
               NOT 
               NULL 
            ) 
          ON [PRIMARY] 

          ALTER TABLE [dbo].[planned_substatus] 
            ADD CONSTRAINT [PK_PLANNED_SUBSTATUS] PRIMARY KEY CLUSTERED ( 
            [substatusid] 
            ) 
            WITH (FILLFACTOR=100) ON [PRIMARY] 

          ALTER TABLE [dbo].[planned_substatus] 
            ADD CONSTRAINT [FK__PLANNED_S__STATU__1B8A8CC0] FOREIGN KEY ( 
            [statusid]) 
            REFERENCES [dbo].[planned_status] ([statusid]) 
      END 

    --============================================== 
    -- #12941 Added accepted status successfully 
    --============================================== 
    IF NOT EXISTS (SELECT 1 
                   FROM   planned_substatus 
                   WHERE  title = 'Accepted') 
      BEGIN 
          DECLARE @ARRANGEDSTATUSID INT 

          SELECT @ARRANGEDSTATUSID = statusid 
          FROM   planned_status 
          WHERE  title = 'Arranged' 

          INSERT INTO [dbo].[planned_substatus] 
                      ([statusid], 
                       [title]) 
          VALUES      (@ARRANGEDSTATUSID, 
                       'Accepted') 

          PRINT 'Accepted status added successfully!' 
      END 
    ELSE 
      BEGIN 
          PRINT 'Accepted status already exits' 
      END 

    IF @@TRANCOUNT > 0 
      BEGIN 
          COMMIT TRANSACTION; 
      END 
END try 

BEGIN catch 
    IF @@TRANCOUNT > 0 
      BEGIN 
          ROLLBACK TRANSACTION; 
      END 

    DECLARE @ErrorMessage NVARCHAR(4000); 
    DECLARE @ErrorSeverity INT; 
    DECLARE @ErrorState INT; 

    SELECT @ErrorMessage = Error_message(), 
           @ErrorSeverity = Error_severity(), 
           @ErrorState = Error_state(); 

    -- Use RAISERROR inside the CATCH block to return  
    -- error information about the original error that  
    -- caused execution to jump to the CATCH block. 
    RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState ); 

    PRINT ( @ErrorMessage ) 
END catch 