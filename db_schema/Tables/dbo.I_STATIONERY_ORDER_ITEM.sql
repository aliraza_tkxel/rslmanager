CREATE TABLE [dbo].[I_STATIONERY_ORDER_ITEM]
(
[OrderItemId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[PageNo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemNo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemDesc] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Quantity] [smallint] NOT NULL,
[Size] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Colour] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Cost] [money] NOT NULL,
[PriorityId] [smallint] NOT NULL,
[StatusId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[I_STATIONERY_ORDER_ITEM] ADD 
CONSTRAINT [PK_JS_StationeryOrderItem] PRIMARY KEY CLUSTERED  ([OrderItemId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[I_STATIONERY_ORDER_ITEM] WITH NOCHECK ADD
CONSTRAINT [FK_JS_StationeryOrderItem_JS_StationeryOrder1] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[I_STATIONERY_ORDER] ([OrderId]) ON DELETE CASCADE
GO
