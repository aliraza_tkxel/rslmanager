
 
GO

/****** Object:  Table [dbo].[P_ADAPTATIONS]    Script Date: 18/11/2018 12:47:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].C_SERVICECOMPLAINT_SUBCATEGORY(
	SUBCATEGORYID [int] IDENTITY(1,1) NOT NULL,
	CATEGORYID [int] NOT NULL,
	[DESCRIPTION] [nvarchar](200) NULL,
 CONSTRAINT [PK_C_SERVICECOMPLAINT_SUBCATEGORY] PRIMARY KEY NONCLUSTERED 
(
	SUBCATEGORYID ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO


IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Staff')
 begin
 insert into C_SERVICECOMPLAINT_CATEGORY (DESCRIPTION) select 'Staff'
 end

 DECLARE @cATiD INT

 SET @cATiD = (SELECT CATEGORYID FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Staff')

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Communication' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Communication', @cATiD
 end

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Breach of professional boundaries' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Breach of professional boundaries', @cATiD
 end


 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Feedback on attitude/behaviours' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Feedback on attitude/behaviours', @cATiD
 end

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Failure to follow policy and procedure' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Failure to follow policy and procedure', @cATiD
 end


IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Allocations')
 begin
 insert into C_SERVICECOMPLAINT_CATEGORY (DESCRIPTION) select 'Allocations'
 end

 
  
 SET @cATiD = (SELECT CATEGORYID FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Allocations')

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Appeal refusal of property' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Appeal refusal of property', @cATiD
 end
  

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with policy or procedure decision' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with policy or procedure decision', @cATiD
 end

IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Income Recovery')
 begin
 insert into C_SERVICECOMPLAINT_CATEGORY (DESCRIPTION) select 'Income Recovery'
 end

 

 SET @cATiD = (SELECT CATEGORYID FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Income Recovery')

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with policy or procedure decision' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with policy or procedure decision', @cATiD
 end

IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'ASB')
 begin
 insert into C_SERVICECOMPLAINT_CATEGORY (DESCRIPTION) select 'ASB'
 end


 SET @cATiD = (SELECT CATEGORYID FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'ASB')

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with policy or procedure decision' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with policy or procedure decision', @cATiD
 end

 --'Rent and Service Charges'
IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Rent and Service Charges')
 begin
 insert into C_SERVICECOMPLAINT_CATEGORY (DESCRIPTION) select 'Rent and Service Charges'
 end
  
 
 SET @cATiD = (SELECT CATEGORYID FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Rent and Service Charges')

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with annual rent increase' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with annual rent increase', @cATiD
 end
  IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with service charge' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with service charge', @cATiD
 end
  IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with service being provided (includes ESO Service)' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with service being provided (includes ESO Service)', @cATiD
 end


 --//'Repairs and planned maintenance'
IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Repairs and planned maintenance')
 begin
 insert into C_SERVICECOMPLAINT_CATEGORY (DESCRIPTION) select 'Repairs and planned maintenance'
 end

  

 SET @cATiD = (SELECT CATEGORYID FROM C_SERVICECOMPLAINT_CATEGORY WHERE DESCRIPTION = 'Repairs and planned maintenance')

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Missed appointment' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Missed appointment', @cATiD
 end
  

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Failure to fix the repair at first visit' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Failure to fix the repair at first visit', @cATiD
 end
  

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with the time it took to complete the repair' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with the time it took to complete the repair', @cATiD
 end
  
 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with the quality of work to the repair' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with the quality of work to the repair', @cATiD
 end
  

 IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Damage to belongings or property due to repairs' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Damage to belongings or property due to repairs', @cATiD
 end

  IF NOT EXISTS (SELECT * FROM C_SERVICECOMPLAINT_SUBCATEGORY WHERE DESCRIPTION = 'Dissatisfied with policy or procedure decision (edited)' AND CATEGORYID = @cATiD)
 begin
 insert into C_SERVICECOMPLAINT_SUBCATEGORY (DESCRIPTION, categoryid) select 'Dissatisfied with policy or procedure decision (edited)', @cATiD
 end

  