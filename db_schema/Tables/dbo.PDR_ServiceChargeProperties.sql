USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PDR_ServiceChargeProperties')



	CREATE TABLE [dbo].[PDR_ServiceChargeProperties](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SchemeId] [int] NULL,
		[BlockId] [int] NULL,
		[PropertyId] NVARCHAR(MAX) NULL,
		[IsIncluded] [int] NULL,
		[ItemId] [int] null
	 CONSTRAINT [PK_PDR_ServiceChargeProperties] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

BEGIN TRANSACTION
BEGIN TRY
	IF COL_LENGTH('PDR_ServiceChargeProperties', 'IsActive') IS NULL
	BEGIN
		Alter Table PDR_ServiceChargeProperties Add IsActive bit default 1;
		PRINT ('Added IsActive column. Run the below commented command afterwards just once')
		-- for prior servicecharge properties this condition is true so it won't have any issue
		--Update dbo.PDR_ServiceChargeProperties SET IsActive=1 where IsActive is NULL;
	END 

	IF COL_LENGTH('PDR_ServiceChargeProperties', 'ChildAttributeMappingId') IS NULL
	BEGIN
		Alter Table PDR_ServiceChargeProperties Add ChildAttributeMappingId int default null;
		PRINT ('Added ChildAttributeMappingId column. Run the below commented command afterwards just once')
	END 


	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY


BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
	Print (@ErrorMessage)
END CATCH
