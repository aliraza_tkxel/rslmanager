USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_ViewingStatus')
BEGIN
CREATE TABLE [dbo].[FLS_ViewingStatus]
(
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
CONSTRAINT [PK_FLS_ViewingStatus] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
INSERT [dbo].[FLS_ViewingStatus] ([Title], [IsActive]) VALUES (N'Acceptance Pending', 1)
INSERT [dbo].[FLS_ViewingStatus] ([Title], [IsActive]) VALUES (N'Rejected', 1)
INSERT [dbo].[FLS_ViewingStatus] ([Title], [IsActive]) VALUES (N'Accepted', 1)
INSERT [dbo].[FLS_ViewingStatus] ([Title], [IsActive]) VALUES (N'Cancelled', 1)
SET IDENTITY_INSERT [dbo].[FLS_ViewingStatus] OFF
PRINT 'Table FLS_ViewingStatus created'
END
ELSE
BEGIN
PRINT 'Table FLS_ViewingStatus already exist'


END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
