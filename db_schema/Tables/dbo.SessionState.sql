CREATE TABLE [dbo].[SessionState]
(
[ID] [uniqueidentifier] NOT NULL,
[Data] [image] NOT NULL,
[Last_Accessed] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE CLUSTERED INDEX [_dta_index_SessionState_c_5_386868495__K1] ON [dbo].[SessionState] ([ID]) ON [PRIMARY]

ALTER TABLE [dbo].[SessionState] ADD CONSTRAINT [PK_SessionState] PRIMARY KEY NONCLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]

GO
