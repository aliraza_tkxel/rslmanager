CREATE TABLE [dbo].[C_SERVICECOMPLAINTS_RULES]
(
[RuleID] [int] NOT NULL IDENTITY(1, 1),
[LetterActionID] [int] NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseDays] [int] NULL
) ON [PRIMARY]
GO
