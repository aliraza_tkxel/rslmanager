--Script to Add a new Column : Category of Type Varchar for Property Documents in Prop > Search > Documents Tab .
USE [RSLBHALive]
	GO
	
	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO

	SET ANSI_PADDING ON
	GO


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_DOCUMENTS')
BEGIN
	

	CREATE TABLE [dbo].[P_Documents](
		[DocumentId] [int] IDENTITY(1,1) NOT NULL,
		[PropertyId] [nvarchar](20) NULL,
		[DocumentName] [varchar](500) NULL,
		[DocumentType] [varchar](500) NULL,
		[DocumentTypeId] [int] NOT NULL,
		[DocumentSubtypeId] [int] NOT NULL,
		[DocumentPath] [varchar](50) NULL,
		[Keywords] [varchar](255) NULL,
		[CreatedDate] [datetime] NULL,
		[DocumentSize] [varchar](50) NULL,
		[DocumentFormat] [varchar](50) NULL,
		[SchemeId] [int] NULL,
		[BlockId] [int] NULL,
		[DocumentDate] [datetime] NULL,
		[ExpiryDate] [datetime] NULL,
		[UploadedBy] [int] NULL,
		[EpcRating] [int] NULL,
		[CP12Number] [nvarchar](20) NULL,
		[Category] [varchar](15) NULL,
		 CONSTRAINT [PK_P_Documents] PRIMARY KEY CLUSTERED 
		(
			[DocumentId] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

		
		ALTER TABLE [dbo].[P_Documents]  WITH CHECK ADD  CONSTRAINT [FK_P_Documents_P_Documents_Type] FOREIGN KEY([DocumentTypeId])
		REFERENCES [dbo].[P_Documents_Type] ([DocumentTypeId])
		

		ALTER TABLE [dbo].[P_Documents] CHECK CONSTRAINT [FK_P_Documents_P_Documents_Type]
		

		ALTER TABLE [dbo].[P_Documents]  WITH CHECK ADD  CONSTRAINT [FK_P_Documents_PropId] FOREIGN KEY([PropertyId])
		REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])
		

		ALTER TABLE [dbo].[P_Documents] CHECK CONSTRAINT [FK_P_Documents_PropId]
		

		ALTER TABLE [dbo].[P_Documents] ADD  CONSTRAINT [DF_P_Documents_createdDate]  DEFAULT (getdate()) FOR [CreatedDate]

	  PRINT 'Table Created';
 END
 
 

IF COL_LENGTH('P_DOCUMENTS', 'Category') IS NULL
	BEGIN
			ALTER TABLE P_DOCUMENTS
			ADD Category Varchar(15) NULL
	END

GO	
--Update Existing document's category to Compliance.

UPDATE	dbo.P_Documents 
SET Category = 'Compliance'
Where	Category IS NULL

GO


IF COL_LENGTH('P_Documents', 'DocumentPath') IS NULL
	BEGIN
		ALTER TABLE P_Documents ADD DocumentPath Varchar(300) NULL
	END
ELSE
	BEGIN
		ALTER TABLE P_Documents ALTER COLUMN DocumentPath Varchar(300) NULL
	END


