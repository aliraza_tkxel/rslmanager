USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY
 
	IF NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'C_NATURE')
		BEGIN
			CREATE TABLE [dbo].[C_NATURE](
				[ITEMNATUREID] [int] NOT NULL,
				[ITEMID] [int] NULL,
				[DESCRIPTION] [nvarchar](35) NULL,
				[EMPLOYEEONLY] [int] NULL,
				[DISPLAYIMAGE] [nvarchar](50) NULL
			) ON [PRIMARY]

		END
	ELSE

		BEGIN
			PRINT 'Table Already Exist';
		END

	  --========================================================================================================
    --========================================================================================================

	IF NOT EXISTS (Select 1 from C_Nature where DESCRIPTION='Universal Credit')
		BEGIN 	
			declare @maxITEMNATUREID int
			Select @maxITEMNATUREID = max( ITEMNATUREID ) + 1
			From C_Nature 
			Insert into C_NATURE (ITEMNATUREID, ITEMID, DESCRIPTION,EMPLOYEEONLY,DISPLAYIMAGE)
			values (@maxITEMNATUREID,2,'Universal Credit',0,NULL)

			PRINT 'New row added'
		END
	ELSE
		BEGIN
			PRINT 'Row Already Exist'
		END


	  --========================================================================================================
    --========================================================================================================

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH


