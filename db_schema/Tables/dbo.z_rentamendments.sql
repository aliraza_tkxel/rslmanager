CREATE TABLE [dbo].[z_rentamendments]
(
[Propertyid] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[tenancy id] [float] NULL,
[tenancy start] [smalldatetime] NULL,
[housenumber] [float] NULL,
[address 1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address 2 ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[town] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[current rent total] [float] NULL,
[proposed] [float] NULL
) ON [PRIMARY]
GO
