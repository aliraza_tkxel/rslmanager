USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_DIFFDIS]******/
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_DIFFDIS')
BEGIN

	CREATE TABLE [dbo].[E_DIFFDIS](
	[DIFFDISID] [int] NOT NULL,
	[DESCRIPTION] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DTYPE] [int] NULL,
 	CONSTRAINT [PK_DISABILITY] PRIMARY KEY NONCLUSTERED 
	(
	     [DIFFDISID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON[PRIMARY]) 
	ON [PRIMARY]
END

If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Visual impairtment')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (1, 'Visual impairtment', 1)
	PRINT 'Visual impairtment added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Hearing impairment')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (2, 'Hearing impairment', 1)
	PRINT 'Hearing impairment added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Disability affecting mobility')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (3, 'Disability affecting mobility', 1)
	PRINT 'Disability affecting mobility added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Other physical disability')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (4, 'Other physical disability', 1)
	PRINT 'Other physical disability added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Other medical condition')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (5, 'Other medical condition', 1)
	PRINT 'Other medical condition added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Emotional / behavioural difficulties')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (6, 'Emotional / behavioural difficulties', 1)
	PRINT 'Emotional / behavioural difficulties added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Mental ill health')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (7, 'Mental ill health', 1)
	PRINT 'Mental ill health added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Temporary disability after illness or accident')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (8, 'Temporary disability after illness or accident', 1)
	PRINT 'Temporary disability after illness or accident added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Profound complex disabilities')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (9, 'Profound complex disabilities', 1)
	PRINT 'Profound complex disabilities added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Multiple disabilities')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (10, 'Multiple disabilities', 1)
	PRINT 'Multiple disabilities added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Other disability')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (11, 'Other disability', 1)
	PRINT 'Other disability added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'None')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (12, 'None', 0)
	PRINT 'None added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Moderate learning difficulty')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (13, 'Moderate learning difficulty', 2)
	PRINT 'Moderate learning difficulty added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Severe learning difficulty')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (14, 'Severe learning difficulty', 2)
	PRINT 'Severe learning difficulty added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Dyslexia')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (15, 'Dyslexia', 2)
	PRINT 'Dyslexia added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Dyscalculia')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (16, 'Dyscalculia', 2)
	PRINT 'Dyscalculia added'
End
If not EXISTS( select * from E_DIFFDIS where DESCRIPTION = 'Other learning difficulty')
	BEGIN
		INSERT INTO E_DIFFDIS VALUES (17, 'Other learning difficulty', 2)
	PRINT 'Other learning difficulty added'
End
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 