CREATE TABLE [dbo].[E_CLOSEDDAYS]
(
[closedDayId] [int] NOT NULL IDENTITY(1, 1),
[closedDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
