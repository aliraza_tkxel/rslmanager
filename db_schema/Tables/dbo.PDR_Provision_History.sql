USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[AS_APPOINTMENTS]    Script Date: 08/29/2016 14:23:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_NAME = N'PDR_Provision_History'
		)
BEGIN
	CREATE TABLE [dbo].[PDR_Provision_History] (
		[ProvisionHistoryId] [int] IDENTITY(1, 1) NOT NULL
		,[ProvisionId] [int] NOT NULL
		,[ProvisionName] [nvarchar](100) NOT NULL
		,[ProvisionParentId] [int] NULL
		,[Manufacturer] [nvarchar](100) NOT NULL
		,[Model] [nvarchar](50) NOT NULL
		,[SerialNumber] [nvarchar](100) NOT NULL
		,[InstalledDate] [smalldatetime] NOT NULL
		,[InstallationCost] [decimal](18, 0) NOT NULL
		,[LifeSpan] [int] NOT NULL
		,[CycleId] [int] NULL
		,[ReplacementDue] [smalldatetime] NOT NULL
		,[ConditionRating] [int] NOT NULL
		,[IsActive] [bit] NOT NULL
		,[SchemeId] [int] NULL
		,[BlockId] [int] NULL
		,[LastReplaced] [smalldatetime] NOT NULL
		,[UpdatedBy] [int] NULL
		,CONSTRAINT [PK_PDR_Provision_History] PRIMARY KEY CLUSTERED ([ProvisionHistoryId] ASC) WITH (
			PAD_INDEX = OFF
			,STATISTICS_NORECOMPUTE = OFF
			,IGNORE_DUP_KEY = OFF
			,ALLOW_ROW_LOCKS = ON
			,ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].[PDR_Provision_History]
		WITH CHECK ADD CONSTRAINT [FK_PDR_Provision_History_PDR_Provisions] FOREIGN KEY ([ProvisionId]) REFERENCES [dbo].[PDR_Provisions]([ProvisionId]) ON

	UPDATE CASCADE ON

	DELETE CASCADE

	ALTER TABLE [dbo].[PDR_Provision_History] CHECK CONSTRAINT [FK_PDR_Provision_History_PDR_Provisions]
END

BEGIN TRANSACTION

BEGIN TRY
	--- Added new columns
	IF COL_LENGTH('PDR_Provision_History', 'ProvisionDescription') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ProvisionDescription] [nvarchar] (100) NULL

		PRINT ('COLUMN ProvisionDescription Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ProvisionType') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ProvisionType] [int] NULL

		PRINT ('COLUMN ProvisionType Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ProvisionCategoryId') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ProvisionCategoryId] [int] NULL

		PRINT ('COLUMN ProvisionCategoryId Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'Quantity') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [Quantity] [int] NULL

		PRINT ('COLUMN Quantity Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ProvisionLocation') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ProvisionLocation] [nvarchar] (100) NULL

		PRINT ('COLUMN ProvisionLocation Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'SupplierId') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [SupplierId] [int] NULL

		PRINT ('COLUMN SupplierId Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'BHG') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [BHG] [nvarchar] (100) NULL

		PRINT ('COLUMN BHG Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'GasAppliance') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [GasAppliance] [bit] NULL

		PRINT ('COLUMN GasAppliance Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'Sluice') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [Sluice] [bit] NULL

		PRINT ('COLUMN Sluice Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'PurchasedDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [PurchasedDate] [smalldatetime] NULL

		PRINT ('COLUMN PurchasedDate Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'PurchasedCost') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [PurchasedCost] [decimal] (
			18
			,0
			) NULL

		PRINT ('COLUMN PurchasedCost Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'Warranty') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [Warranty] BIT NULL

		PRINT ('COLUMN Warranty Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'Owned') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [Owned] BIT NULL

		PRINT ('COLUMN Owned Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'LeaseExpiryDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [LeaseExpiryDate] [smalldatetime] NULL

		PRINT ('COLUMN LeaseExpiryDate Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'LeaseCost') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [LeaseCost] [decimal] (
			18
			,0
			) NULL

		PRINT ('COLUMN LeaseCost Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ProvisionCharge') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ProvisionCharge] BIT NULL

		PRINT ('COLUMN ProvisionCharge Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'AnnualApportionmentPC') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [AnnualApportionmentPC] [nvarchar] (100) NULL

		PRINT ('COLUMN AnnualApportionmentPC Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'BlockPC') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [BlockPC] [int] NULL

		PRINT ('COLUMN BlockPC Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'PATTesting') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [PATTesting] BIT NULL

		PRINT ('COLUMN PATTesting Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ServicingRequired') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ServicingRequired] BIT NULL

		PRINT ('COLUMN ServicingRequired Added.')
	END
	
	IF COL_LENGTH('PDR_Provision_History', 'ServicingProviderId') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ServicingProviderId] [int] NULL

		PRINT ('COLUMN ServicingProviderId Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'DuctServicing') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [DuctServicing] [nvarchar] (100) NULL

		PRINT ('COLUMN DuctServicing Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'lastDuctServicingDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [lastDuctServicingDate] [smalldatetime] NULL

		PRINT ('COLUMN lastDuctServicingDate Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'nextDuctServicingDate') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [nextDuctServicingDate] [smalldatetime] NULL

		PRINT ('COLUMN nextDuctServicingDate Added.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ductServiceCycle') IS NULL
	BEGIN
		ALTER TABLE PDR_Provision_History ADD [ductServiceCycle] [nvarchar] (100) NULL

		PRINT ('COLUMN ductServiceCycle Added.')
	END

	--- Updated Column Types
	IF COL_LENGTH('PDR_Provision_History', 'ReplacementDue') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provision_History

		ALTER COLUMN [ReplacementDue] [smalldatetime] NULL

		PRINT ('Update ReplacementDue to nullable.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'LastReplaced') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provision_History

		ALTER COLUMN [LastReplaced] [smalldatetime] NULL

		PRINT ('Update LastReplaced to nullable.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ConditionRating') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provision_History

		ALTER COLUMN [ConditionRating] [int] NULL

		PRINT ('Update ConditionRating to nullable.')
	END

	--- Drop Columns
	IF COL_LENGTH('PDR_Provision_History', 'ProvisionName') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provision_History

		DROP COLUMN [ProvisionName]

		PRINT ('Drop Coulmn ProvisionName.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'ProvisionParentId') IS NOT NULL
	BEGIN
		ALTER TABLE PDR_Provision_History

		DROP COLUMN [ProvisionParentId]

		PRINT ('Drop Coulmn ProvisionParentId.')
	END
	IF COL_LENGTH('PDR_Provision_History', 'Manufacturer') IS NOT NULL
	BEGIN
		Alter Table PDR_Provision_History Drop COLUMN  Manufacturer
		PRINT ('Update Manufacturer removed.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'InstalledDate') IS NOT NULL
	BEGIN
		Alter Table PDR_Provision_History Drop COLUMN  InstalledDate
		PRINT ('Update InstalledDate removed.')
	END

	IF COL_LENGTH('PDR_Provision_History', 'InstallationCost') IS NOT NULL
	BEGIN
		Alter Table PDR_Provision_History Drop COLUMN  InstallationCost
		PRINT ('Update InstallationCost removed.')
	END  
	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
	END

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE()
		,@ErrorSeverity = ERROR_SEVERITY()
		,@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (
			@ErrorMessage
			,@ErrorSeverity
			,@ErrorState
			);

	PRINT (@ErrorMessage)
END CATCH