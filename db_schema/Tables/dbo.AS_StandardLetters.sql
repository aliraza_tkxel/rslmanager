USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[AS_StandardLetters]    Script Date: 23-Jul-18 5:58:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT *
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'AS_StandardLetters') 
BEGIN
CREATE TABLE [dbo].[AS_StandardLetters](
	[StandardLetterId] [int] IDENTITY(1,1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ActionId] [int] NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Body] [varchar](max) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[SignOffLookupCode] [int] NULL,
	[TeamId] [int] NULL,
	[FromResourceId] [int] NULL,
	[IsPrinted] [bit] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_AS_StandardLetters] PRIMARY KEY CLUSTERED 
(
	[StandardLetterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

PRINT('AS_StandardLetters created successfully.')

ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_ACTION_ActionId] FOREIGN KEY([ActionId])
REFERENCES [dbo].[AS_Action] ([ActionId])

ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_ACTION_ActionId]

ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_LookupCode_SignOff] FOREIGN KEY([SignOffLookupCode])
REFERENCES [dbo].[AS_LookupCode] ([LookupCodeId])

ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_LookupCode_SignOff]

ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_STATUS_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[AS_Status] ([StatusId])

ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_STATUS_StatusId]

ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_USER_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[AS_USER] ([EmployeeId])

ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_USER_CreatedBy]

ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_USER_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[AS_USER] ([EmployeeId])

ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_USER_ModifiedBy]

END

IF COL_LENGTH('AS_StandardLetters', 'IsAlternativeServicing') IS NULL
	BEGIN
		ALTER TABLE AS_StandardLetters
		ADD IsAlternativeServicing Bit NULL 
		
		Print('IsAlternativeServicing in AS_StandardLetters added successfully.') 
END
