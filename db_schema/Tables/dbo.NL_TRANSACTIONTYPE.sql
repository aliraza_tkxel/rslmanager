/****** Object:  Table [dbo].[NL_TRANSACTIONTYPE]    ******/
Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
		WHERE TABLE_NAME = N'NL_TRANSACTIONTYPE')
	BEGIN
		CREATE TABLE [dbo].[NL_TRANSACTIONTYPE]
		(
		[TRANSACTIONTYPEID] [int] NOT NULL,
		[DESCRIPTION] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LONGDESCRIPTION] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[COMMENTS] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
		) ON [PRIMARY]
		ALTER TABLE [dbo].[NL_TRANSACTIONTYPE] ADD CONSTRAINT [PK_NL_TRANSACTIONTYPE] PRIMARY KEY CLUSTERED  ([TRANSACTIONTYPEID]) WITH FILLFACTOR=30 ON [PRIMARY]
	END
		
	
	--===============================================================================
	--Ticket# 12021 Adding GJR Type
	--===============================================================================
		
	IF NOT EXISTS (SELECT 1 FROM NL_TRANSACTIONTYPE WHERE DESCRIPTION='GJR')
	BEGIN  
		Declare @max_ID int = 0 
		Select @max_ID = max(TRANSACTIONTYPEID) FROM NL_TRANSACTIONTYPE
		INSERT INTO NL_TRANSACTIONTYPE (TRANSACTIONTYPEID, DESCRIPTION, LONGDESCRIPTION) 
		VALUES (@max_ID+1, 'GJR', 'General Journal Reversal')
	END 
	--===============================================================================
	--End Ticket# 12021 Adding GJR Type
	--===============================================================================

	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)
END CATCH