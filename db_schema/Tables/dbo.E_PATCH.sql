USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_PATCH]    Script Date: 17-Jul-18 3:20:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	IF NOT EXISTS (SELECT *
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_NAME = 'E_PATCH') 
	BEGIN
	CREATE TABLE [dbo].[E_PATCH]
	(
	[PATCHID] [int] NOT NULL IDENTITY(1, 1),
	[CREATIONDATE] [smalldatetime] NULL CONSTRAINT [DF_PATCH_CREATIONDATE] DEFAULT (CONVERT([datetime],CONVERT([varchar],getdate(),(103)),(103))),
	[LOCATION] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
	) ON [PRIMARY]
	ALTER TABLE [dbo].[E_PATCH] ADD CONSTRAINT [PK_PATCH] PRIMARY KEY NONCLUSTERED  ([PATCHID]) WITH (FILLFACTOR=30) ON [PRIMARY]
	GRANT SELECT ON  [dbo].[E_PATCH] TO [rackspace_datareader]
	END

	BEGIN TRANSACTION
	BEGIN TRY

	declare @patchLocation as varchar(10) ='All'

	IF Not EXISTS (	SELECT	1 FROM	E_patch WHERE LOCATION=@patchLocation)
	BEGIN 	
		INSERT INTO E_patch (creationdate,location)  
		VALUES (getdate(),@patchLocation);
		print('All inserted')
	END

	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
	END TRY
	BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)
	END CATCH
GO