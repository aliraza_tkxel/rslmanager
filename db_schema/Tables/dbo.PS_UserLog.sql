CREATE TABLE [dbo].[PS_UserLog]
(
[UserLogId] [int] NOT NULL IDENTITY(1, 1),
[LoggedInUserId] [int] NULL,
[LastLoggedInDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_UserLog] ADD 
CONSTRAINT [PK_PS_UserLog] PRIMARY KEY CLUSTERED  ([UserLogId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
