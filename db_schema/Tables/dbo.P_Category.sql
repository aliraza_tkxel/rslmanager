USE RSLBHALive
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'P_Category')
    BEGIN
		CREATE TABLE [dbo].[P_Category](
	[CATEGORYTYPEID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](8000) NULL,
	[IsDevelopment] [int] NULL,
	[IsScheme] [int] NULL,
	[IsProperty] [int] NULL,
		) ON [PRIMARY]

	Print('Table Created successfully')

	INSERT INTO P_Category (TITLE, IsDevelopment,IsScheme,IsProperty) VALUES ('Legal', 1,1,1)
	Print('Legal added successfully')
	INSERT INTO P_Category (TITLE, IsDevelopment,IsScheme,IsProperty) VALUES ('Compliance', 0,1,1)
	Print('Compliance added successfully')
	INSERT INTO P_Category (TITLE, IsDevelopment,IsScheme,IsProperty) VALUES ('General', 0,1,1)
	Print('General added successfully')

	END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

	
	


	