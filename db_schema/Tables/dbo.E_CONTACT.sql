
Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_CONTACT')
BEGIN

	CREATE TABLE [dbo].[E_CONTACT]
	(
	[CONTACTID] [int] NOT NULL IDENTITY(1, 1),
	[EMPLOYEEID] [int] NOT NULL,
	[ADDRESS1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ADDRESS2] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ADDRESS3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[POSTALTOWN] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[COUNTY] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[POSTCODE] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HOMETEL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MOBILE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WORKDD] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WORKEXT] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HOMEEMAIL] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WORKEMAIL] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EMERGENCYCONTACTNAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EMERGENCYCONTACTTEL] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EMERGENCYINFO] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WORKMOBILE] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LASTACTIONTIME] [datetime] NULL CONSTRAINT [DF_E_CONTACT_LASTACTIONTIME] DEFAULT (getdate()),
	[LASTACTIONUSER] [int] NULL
	) ON [PRIMARY]
	CREATE CLUSTERED INDEX [_dta_index_E_CONTACT_c_5_1896393825__K2] ON [dbo].[E_CONTACT] ([EMPLOYEEID]) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [_dta_index_E_CONTACT_5_1896393825__K2_10_11_14_18] ON [dbo].[E_CONTACT] ([EMPLOYEEID]) INCLUDE ([MOBILE], [WORKDD], [WORKEMAIL], [WORKMOBILE]) ON [PRIMARY]

	ALTER TABLE [dbo].[E_CONTACT] ADD CONSTRAINT [PK_CONTACT] PRIMARY KEY NONCLUSTERED  ([CONTACTID]) WITH (FILLFACTOR=60) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [IX_E_CONTACT_EMPLOYEEID] ON [dbo].[E_CONTACT] ([EMPLOYEEID]) ON [PRIMARY]

END --if



IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_CONTACT]' ) AND name = 'NextOfKin')
	BEGIN
		ALTER TABLE E_CONTACT ADD NextOfKin NVARCHAR(100) DEFAULT NULL
	PRINT 'NextOfKin added successfully!'
	END--if

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_CONTACT]' ) AND name = 'MobilePersonal')
	BEGIN
		ALTER TABLE E_CONTACT ADD MobilePersonal NVARCHAR(100) DEFAULT NULL
	PRINT 'MobilePersonal added successfully!'
	END--if

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_CONTACT]' ) AND name = 'EmergencyContactRelationship')
BEGIN
	ALTER TABLE E_CONTACT ADD EmergencyContactRelationship NVARCHAR(max) DEFAULT NULL
PRINT 'EmergencyContactRelationship added successfully!'
END--if

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 