CREATE TABLE [dbo].[AM_StatusHistory]
(
[StatusHistoryId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [int] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RentParameter] [int] NULL,
[RentParameterFrequencyLookupCodeId] [int] NULL,
[IsRentParameter] [bit] NULL,
[RentAmount] [float] NULL,
[IsRentAmount] [bit] NOT NULL,
[Ranking] [int] NOT NULL,
[ReviewPeriod] [int] NOT NULL,
[ReviewPeriodFrequencyLookupCodeId] [int] NOT NULL,
[NextStatusAlert] [int] NOT NULL,
[NextStatusAlertFrequencyLookupCodeId] [int] NOT NULL,
[NextStatusDetail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsNoticeParameterRequired] [bit] NULL,
[IsRecoveryAmount] [bit] NULL,
[IsNoticeIssueDate] [bit] NULL,
[IsNoticeExpiryDate] [bit] NULL,
[IsDocumentUpload] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedBy] [int] NOT NULL,
[ModifiedDate] [datetime] NULL,
[IsHearingDate] [bit] NULL,
[IsWarrantExpiryDate] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_StatusHistory] ADD 
CONSTRAINT [PK_AM_StatusHistory] PRIMARY KEY CLUSTERED  ([StatusHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_StatusHistory] ADD CONSTRAINT [StatusHistoryCreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_StatusHistory] ADD CONSTRAINT [StatusHistoryModifiedBy] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_StatusHistory] ADD CONSTRAINT [StatusHistoryNextStatusAlertFrequencyLookupCode] FOREIGN KEY ([NextStatusAlertFrequencyLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_StatusHistory] ADD CONSTRAINT [LookupCodeReviewPeriodFrequencyLookupCode] FOREIGN KEY ([ReviewPeriodFrequencyLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_StatusHistory] ADD CONSTRAINT [FK_AM_StatusHistory_AM_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
GO
