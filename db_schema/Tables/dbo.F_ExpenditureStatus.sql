
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_ExpenditureStatus')
BEGIN

CREATE TABLE [dbo].[F_ExpenditureStatus](
	[ExpenditureStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreatedDate] [smalldatetime] NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[LastModifiedDate] [smalldatetime] NULL,
	[DeletedBy] [uniqueidentifier] NULL,
	[DeletedDate] [smalldatetime] NULL,
	[DeletedFlag] [bit] NULL CONSTRAINT [DF_F_ExpenditureStatus_DeletedFlag]  DEFAULT ((0)),
 CONSTRAINT [PK_F_ExpenditureStatus] PRIMARY KEY CLUSTERED 
(
	[ExpenditureStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


GO

IF NOT EXISTS (SELECT * FROM [F_ExpenditureStatus] )
 BEGIN
  
 SET IDENTITY_INSERT dbo.F_ExpenditureStatus ON

	INSERT INTO F_ExpenditureStatus (ExpenditureStatusID,Description, DeletedFlag)
	SELECT -1, 'All Expenditures',0

	insert into [F_ExpenditureStatus] (ExpenditureStatusID,Description, CreatedDate)
	select 1,'Active Expenditures',getdate()

	insert into [F_ExpenditureStatus] (ExpenditureStatusID,Description, CreatedDate)
	select 2,'Non Active Expenditures',getdate()

	
SET IDENTITY_INSERT dbo.F_ExpenditureStatus off

 END






  