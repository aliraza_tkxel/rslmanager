CREATE TABLE [dbo].[FL_FAULT_01102014]
(
[FaultID] [int] NOT NULL,
[ElementID] [int] NULL,
[PriorityID] [int] NULL,
[Description] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetCost] [float] NULL,
[Vat] [float] NULL,
[Gross] [float] NULL,
[VatRateID] [int] NULL,
[EffectFrom] [datetime] NULL,
[Recharge] [bit] NULL,
[PreInspection] [bit] NULL,
[PostInspection] [bit] NULL,
[StockConditionItem] [bit] NULL,
[FaultActive] [bit] NULL,
[FaultAction] [bit] NULL,
[SubmitDate] [datetime] NULL,
[isInflative] [bit] NOT NULL,
[Accepted] [bit] NULL,
[AREAID] [int] NULL,
[isContractor] [bit] NULL,
[duration] [float] NULL,
[IsGasSafe] [smallint] NULL,
[IsOftec] [smallint] NULL
) ON [PRIMARY]
GO
