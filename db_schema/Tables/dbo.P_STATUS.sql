USE RSLBHALive
GO
BEGIN TRANSACTION
BEGIN TRY
--Check if table exists or not
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_STATUS')
BEGIN

	CREATE TABLE [dbo].[P_STATUS]
	(
	[STATUSID] [int] NOT NULL IDENTITY(1, 1),
	[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	) ON [PRIMARY]

END
SET IDENTITY_INSERT dbo.P_STATUS ON;
IF NOT EXISTS (Select 1 from P_STATUS where DESCRIPTION='For sale')
	BEGIN  
		INSERT INTO P_STATUS (STATUSID, DESCRIPTION)Values (12,'For sale')
		Print('For sale added successfully  ') 
	END



IF NOT EXISTS (Select 1 from P_STATUS where DESCRIPTION='Occupied')
	BEGIN  
		INSERT INTO P_STATUS (STATUSID, DESCRIPTION)Values (13,'Occupied')
		Print('Occupied added successfully  ') 
	END

IF NOT EXISTS (Select 1 from P_STATUS where DESCRIPTION='Let � commercial')
	BEGIN  
		INSERT INTO P_STATUS (STATUSID, DESCRIPTION)Values (14,'Let � commercial')
		Print('Let � commercial added successfully  ') 
	END


IF NOT EXISTS (Select 1 from P_STATUS where DESCRIPTION='Let � Tenancy')
	BEGIN  
		INSERT INTO P_STATUS (STATUSID, DESCRIPTION)Values (15,'Let � Tenancy')
		Print('Let � Tenancy added successfully  ') 
	END
IF NOT EXISTS (Select 1 from P_STATUS where DESCRIPTION='Let � Licence')
	BEGIN  
		INSERT INTO P_STATUS (STATUSID, DESCRIPTION)Values (16,'Let � Licence')
		Print('Let � Licence added successfully  ') 
	END
SET IDENTITY_INSERT dbo.P_STATUS OFF;
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
