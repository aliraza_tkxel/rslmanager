USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PDR_ProvisionChargeProperties]    Script Date: 10/29/2018 12:46:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION

BEGIN TRY

	IF NOT EXISTS (
			SELECT *
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_NAME = N'PDR_ProvisionChargeProperties'
			)
	BEGIN

		CREATE TABLE [dbo].[PDR_ProvisionChargeProperties](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[SchemeId] [int] NULL,
			[BlockId] [int] NULL,
			[PropertyId] [nvarchar](max) NULL,
			[IsIncluded] [int] NULL,
			[ItemId] [int] NULL,
		 CONSTRAINT [PK_PDR_ProvisionChargeProperties] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	END

	IF COL_LENGTH('PDR_ProvisionChargeProperties', 'IsActive') IS NULL
	BEGIN
		Alter Table PDR_ProvisionChargeProperties Add IsActive bit default '1';
		PRINT ('Added IsActive column')
	END 

	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
	END

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE()
		,@ErrorSeverity = ERROR_SEVERITY()
		,@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (
			@ErrorMessage
			,@ErrorSeverity
			,@ErrorState
			);

	PRINT (@ErrorMessage)
END CATCH