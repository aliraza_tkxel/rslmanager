BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'NL_YEARENDACCRAL')
BEGIN


	CREATE TABLE [dbo].[NL_YEARENDACCRAL]
	(
	[YENDACCRLID] [int] NOT NULL IDENTITY(1, 1),
	[ACCDATE] [datetime] NULL CONSTRAINT [DF_NL_YEARENDACCRAL_ACCDATE] DEFAULT (getdate()),
	[USERUD] [int] NULL,
	[AMOUNT] [money] NULL,
	[YENDACCRUALTYPE] [int] NULL,
	[ACTIVE] [bit] NULL CONSTRAINT [DF_NL_YEARENDACCRAL_ACTIVE] DEFAULT (0)
	) ON [PRIMARY]

	ALTER TABLE [dbo].[NL_YEARENDACCRAL] ADD CONSTRAINT [PK_NL_YEARENDACCRAL_1] PRIMARY KEY CLUSTERED  ([YENDACCRLID]) WITH FILLFACTOR=60 ON [PRIMARY]

	
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'NL_YEARENDACCRAL'))
BEGIN

alter table dbo.NL_YEARENDACCRAL add CompanyId [int] NULL

end