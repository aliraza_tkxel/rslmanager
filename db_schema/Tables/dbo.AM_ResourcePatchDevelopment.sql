USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'AM_ResourcePatchDevelopment')
    BEGIN

      CREATE TABLE [dbo].[AM_ResourcePatchDevelopment] (
        [ResourcePatchDevelopmentId] [int] IDENTITY (1, 1) NOT NULL,
        [ResourceId] [int] NULL,
        [PatchId] [int] NULL,
        [SCHEMEID] [int] NULL,
        [IsActive] [bit] NULL,
        [Address] [nvarchar](100) NULL,
        CONSTRAINT [PK_AM_ResourcePatchDevelopment] PRIMARY KEY CLUSTERED
        (
        [ResourcePatchDevelopmentId] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
      ) ON [PRIMARY]


      ALTER TABLE [dbo].[AM_ResourcePatchDevelopment] WITH CHECK ADD CONSTRAINT [FK_AM_ResourcePatchDevelopment_AM_Resource] FOREIGN KEY ([ResourceId])
      REFERENCES [dbo].[AM_Resource] ([ResourceId])


      ALTER TABLE [dbo].[AM_ResourcePatchDevelopment] CHECK CONSTRAINT [FK_AM_ResourcePatchDevelopment_AM_Resource]

    END
    ELSE

    BEGIN
      PRINT 'Table Already Exist';
    END

    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_AM_ResourcePatchDevelopment')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_AM_ResourcePatchDevelopment
		  ON AM_ResourcePatchDevelopment (PatchId);  
	  PRINT 'IX_AM_ResourcePatchDevelopment created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_AM_ResourcePatchDevelopment Index Already Exist';
	  END

   


    --========================================================================================================
    --========================================================================================================

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH