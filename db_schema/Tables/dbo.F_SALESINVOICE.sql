BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_PURCHASEORDER')
BEGIN


CREATE TABLE [dbo].[F_SALESINVOICE]
(
[SALEID] [int] NOT NULL IDENTITY(1, 1),
[SONAME] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SODATE] [smalldatetime] NOT NULL,
[SONOTES] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USERID] [int] NULL,
[TENANCYID] [int] NULL,
[SUPPLIERID] [int] NULL,
[EMPLOYEEID] [int] NULL,
[SALESCUSTOMERID] [int] NULL,
[ACTIVE] [bit] NULL,
[SOTYPE] [int] NULL,
[SOSTATUS] [int] NULL,
[SOTIMESTAMP] [datetime] NOT NULL CONSTRAINT [DF_F_SALESINVOICE_SOTIMESTAMP] DEFAULT (getdate())
) ON [PRIMARY]

ALTER TABLE [dbo].[F_SALESINVOICE] ADD CONSTRAINT [PK_F_SALESORDER] PRIMARY KEY CLUSTERED  ([SALEID]) WITH FILLFACTOR=60 ON [PRIMARY]


END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_SALESINVOICE'))
BEGIN

alter table [F_SALESINVOICE] add CompanyId [int] NULL

END
