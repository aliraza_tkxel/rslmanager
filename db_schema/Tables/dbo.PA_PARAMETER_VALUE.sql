USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
DECLARE @areaId INT, @itemId INT, @parameterId INT, @heatingFuel INT
Set @heatingFuel = 65

IF EXISTS(SELECT 1 FROM PA_AREA  WHERE AreaName = 'Services')
	BEGIN
	SELECT @areaId = AreaId FROM PA_AREA Where AreaName = 'Services'
	IF EXISTS(SELECT 1 FROM PA_ITEM Where AreaID = @areaId AND ItemName = 'Gas')
		BEGIN
		SELECT @itemId = ItemID FROM PA_ITEM WHERE AreaID = @areaId AND ItemName = 'Gas'
		
		SELECT @parameterId= pa.ParameterId FROM PA_ITEM_PARAMETER  ipa
		INNER JOIN PA_PARAMETER pa on ipa.ParameterId = pa.ParameterID
		WHERE ipa.ItemId = @itemId AND pa.ParameterName = 'Meter Type'
		
		Update PA_PARAMETER_VALUE Set IsActive = 0 WHERE ParameterID = @parameterId
		IF NOT EXISTS(SELECT 1 FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND ValueDetail='Standard')
			BEGIN
			INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)
			VALUES (@parameterId,'Standard',0,1)
			PRINT('Standard added successfully')
			END
		ELSE
			BEGIN
			Update PA_PARAMETER_VALUE Set IsActive = 1 WHERE ParameterID=@parameterId AND ValueDetail='Standard'
			END	
		
		IF NOT EXISTS(SELECT 1 FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND ValueDetail='Pre Pay')
			BEGIN
			INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)
			VALUES (@parameterId,'Pre Pay',1,1)
			PRINT('Pre Pay added successfully')
			END
		ELSE
			BEGIN
			Update PA_PARAMETER_VALUE Set IsActive = 1 WHERE ParameterID=@parameterId AND ValueDetail='Pre Pay'
			END	
		IF NOT EXISTS(SELECT 1 FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND ValueDetail='Smart Meter')
			BEGIN
			INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)
			VALUES (@parameterId,'Smart Meter',2,1)
			PRINT('Smart Meter added successfully')
			END	
		ELSE
			BEGIN
			Update PA_PARAMETER_VALUE Set IsActive = 1 WHERE ParameterID=@parameterId AND ValueDetail='Smart Meter'
			END	
		END
	END


IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Wet Electric' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'Wet Electric',12,1,1)
END
ELSE
BEGIN
	update PA_PARAMETER_VALUE set IsAlterNativeHeating = 1 where ValueDetail = 'Wet Electric' and ParameterID = @heatingFuel
END

IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Solar' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'Solar',8,1,1)
END
ELSE
BEGIN
	update PA_PARAMETER_VALUE set IsAlterNativeHeating = 1 where ValueDetail = 'Solar' and ParameterID = @heatingFuel
END

IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Air Source' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'Air Source',6,1,1)
END
ELSE
BEGIN
	update PA_PARAMETER_VALUE set IsAlterNativeHeating = 1 where ValueDetail = 'Air Source' and ParameterID = @heatingFuel
END

IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Ground Source' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'Ground Source',7,1,1)
END
ELSE
BEGIN
	update PA_PARAMETER_VALUE set IsAlterNativeHeating = 1 where ValueDetail = 'Ground Source' and ParameterID = @heatingFuel
END

IF EXISTS(Select * FROM PA_PARAMETER_VALUE PV
where PV.ValueID = dbo.PA_GetParameterValueId('E7' , 'Heating Fuel'))
BEGIN
	UPDATE PA_PARAMETER_VALUE
	SET IsActive = 0
	where PA_PARAMETER_VALUE.ValueID= dbo.PA_GetParameterValueId('E7' , 'Heating Fuel')
	PRINT ('Updated E7 Successfully')
END

IF EXISTS(Select * FROM PA_PARAMETER_VALUE PV
where PV.ValueID = dbo.PA_GetParameterValueId('Wind' , 'Heating Fuel'))
BEGIN
	UPDATE PA_PARAMETER_VALUE
	SET IsActive = 0
	where PA_PARAMETER_VALUE.ValueID = dbo.PA_GetParameterValueId('Wind' , 'Heating Fuel')
	PRINT ('Updated Wind Successfully')
END

IF EXISTS(Select * FROM PA_PARAMETER_VALUE PV
	where PV.ValueID = dbo.PA_GetParameterValueId('LPG' , 'Heating Fuel'))
BEGIN
	UPDATE PA_PARAMETER_VALUE
	SET IsActive = 0
	where PA_PARAMETER_VALUE.ValueID = dbo.PA_GetParameterValueId('LPG' , 'Heating Fuel')
	PRINT ('Updated LPG Successfully')
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END	
	
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorScriptMessage NVARCHAR(4000);


	SELECT @ErrorScriptMessage = ERROR_MESSAGE()

Print (@ErrorScriptMessage)
END CATCH