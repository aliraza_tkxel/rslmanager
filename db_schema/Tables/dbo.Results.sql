CREATE TABLE [dbo].[Results]
(
[PMID] [int] NOT NULL,
[DEVELOPMENTID] [int] NULL,
[BLOCK] [int] NULL,
[CONTRACTSTARTDATE] [smalldatetime] NULL,
[STARTDATE] [datetime] NULL,
[FREQUENCY] [int] NULL,
[CYCLE] [int] NULL,
[EXPIRYDATE] [smalldatetime] NULL,
[REPAIR] [int] NULL,
[CONTRACTOR] [int] NULL,
[LASTEXECUTED] [smalldatetime] NOT NULL,
[NOTES] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USERID] [int] NULL,
[PMTIMESTAMP] [smalldatetime] NOT NULL,
[PM_ACTIVE] [int] NULL
) ON [PRIMARY]
GO
