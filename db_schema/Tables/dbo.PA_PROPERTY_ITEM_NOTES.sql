USE [RSLBHALive]

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PA_PROPERTY_ITEM_NOTES')
							
BEGIN

CREATE TABLE [dbo].[PA_PROPERTY_ITEM_NOTES]
(
[SID] [int] NOT NULL IDENTITY(1, 1),
[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemId] [int] NULL,
[Notes] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [smalldatetime] NULL,
[CreatedBy] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PA_PROPERTY_ITEM_NOTES] ADD 
CONSTRAINT [PK_PA_PROPERTY_ITEM_NOTES] PRIMARY KEY CLUSTERED  ([SID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PA_PROPERTY_ITEM_NOTES] WITH NOCHECK ADD
CONSTRAINT [FK_PA_PROPERTY_ITEM_NOTES_PA_ITEM] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[PA_ITEM] ([ItemID])

END

IF COL_LENGTH('PA_PROPERTY_ITEM_NOTES', 'IsActive') IS NULL
BEGIN
	Alter Table PA_PROPERTY_ITEM_NOTES Add IsActive bit default 1;
	PRINT ('Added IsActive column')
	update PA_PROPERTY_ITEM_NOTES set IsActive =1 where ISACTIVE is null
END 

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
