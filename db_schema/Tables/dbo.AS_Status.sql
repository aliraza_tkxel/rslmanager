BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AS_Status')
BEGIN
  PRINT 'Table Exists';
 
 
IF EXISTS (Select 1 from AS_Status where Title='Assign to Contractor')
	BEGIN  
		PRINT 'Title already exist'
	END
IF EXISTS (Select 1 from AS_Status where Title='Assign to Contractor')
	BEGIN 	
		UPDATE AS_Status SET Title='Assigned to Contractor' WHERE Title='Assign to Contractor'
		
	PRINT 'Assign to Contractor text updated'
END


if not EXISTS( select * from AS_Status where Title = 'Aborted')
	BEGIN
		INSERT INTO AS_Status VALUES ('Aborted', 1, GETDATE(), 423, null, null, 1,0)
	PRINT 'Aborted status added'
End
 
if not EXISTS( select * from AS_Status where Title = 'Cancelled')
	BEGIN
		INSERT INTO AS_Status VALUES ('Cancelled', 1, GETDATE(), 423, null, null, 1,0)
	PRINT 'Cancelled status added'
End

if EXISTS( select * from AS_Status where Title = 'Cp12 issued')
	BEGIN
	UPDATE AS_Status 
	set Title = 'Certificate issued'
	WHERE Title = 'Cp12 issued'
	PRINT 'Updated status of Certificate Issued.'
End
 
END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH