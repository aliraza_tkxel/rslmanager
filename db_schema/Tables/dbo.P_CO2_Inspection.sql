CREATE TABLE [dbo].[P_CO2_Inspection]
(
[DectectorId] [int] NOT NULL IDENTITY(1, 1),
[PropertyId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetectorTest] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateStamp] [smalldatetime] NULL,
[Journalid] [int] NULL,
[IsInspected] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[P_CO2_Inspection] ADD 
CONSTRAINT [PK_P_CO2_Inspection] PRIMARY KEY CLUSTERED  ([DectectorId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[P_CO2_Inspection] ADD CONSTRAINT [FK_P_CO2_InspectionP__PROPERTY] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])
GO
GRANT SELECT ON  [dbo].[P_CO2_Inspection] TO [rackspace_datareader]
GO
