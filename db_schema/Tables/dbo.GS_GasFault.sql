CREATE TABLE [dbo].[GS_GasFault]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[AppointmentID] [int] NOT NULL,
[FaultCategory] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefectDesc] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RemedialAction] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isAdviceNoteIssued] [bit] NOT NULL,
[SerialNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WarningTagFixed] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_GasFault] ADD 
CONSTRAINT [PK_GS_GasFault] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[GS_GasFault] ADD
CONSTRAINT [FK_GS_GasFault_PS_Appointment] FOREIGN KEY ([AppointmentID]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO
