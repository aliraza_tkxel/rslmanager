Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_EquivalentLevel')
BEGIN
	CREATE TABLE [dbo].[E_EquivalentLevel](
	[EquivalentLevelId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_E_EquivalentLevel] PRIMARY KEY CLUSTERED 
(
	[EquivalentLevelId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END 

IF NOT EXISTS (SELECT * FROM E_EquivalentLevel)
BEGIN

SET IDENTITY_INSERT [dbo].[E_EquivalentLevel] ON
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (1, N'Entry')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (2, N'Level 1')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (3, N'Level 2')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (4, N'Level 3')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (5, N'Level 4')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (6, N'Level 5')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (7, N'Level 6')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (8, N'Level 7')
INSERT [dbo].[E_EquivalentLevel] ([EquivalentLevelId], [Description]) VALUES (9, N'Level 8')
SET IDENTITY_INSERT [dbo].[E_EquivalentLevel] OFF

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 


