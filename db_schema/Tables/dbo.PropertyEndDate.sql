CREATE TABLE [dbo].[PropertyEndDate]
(
[SID] [int] NOT NULL IDENTITY(1, 1),
[PROPERTYID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE1] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE4] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE5] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE6] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE7] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE8] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE9] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE10] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE11] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE12] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE13] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE14] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDDATE15] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
