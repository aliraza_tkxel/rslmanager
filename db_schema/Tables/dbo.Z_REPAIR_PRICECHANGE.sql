CREATE TABLE [dbo].[Z_REPAIR_PRICECHANGE]
(
[Cyclical ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Start Date] [smalldatetime] NULL,
[Cycle] [float] NULL,
[Frequency] [float] NULL,
[Cyclical Repair] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Cycle] [smalldatetime] NULL,
[Next Cycle] [smalldatetime] NULL,
[Next Insert] [smalldatetime] NULL,
[Cost] [money] NULL,
[Excl VAT] [money] NULL,
[Ran] [float] NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F15] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
