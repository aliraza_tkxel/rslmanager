CREATE TABLE [dbo].[PS_PropertyPicture]
(
[PropertyPictureId] [int] NOT NULL IDENTITY(1, 1),
[SurveyFormId] [int] NULL,
[PropertyId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PictureName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PictureTag] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefault] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_PropertyPicture] ADD 
CONSTRAINT [PK_PS_PropertyPicture] PRIMARY KEY CLUSTERED  ([PropertyPictureId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_PropertyPicture] ADD
CONSTRAINT [FK_PS_PropertyPicture_PS_SurveyForm] FOREIGN KEY ([SurveyFormId]) REFERENCES [dbo].[PS_SurveyForm] ([SurveyFormId])
GO
