USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'PDR_LandRegistryStatus')
BEGIN


	CREATE TABLE [dbo].[PDR_LandRegistryStatus](
		[RegistryStatusId] [int] IDENTITY(1,1) NOT NULL,
		[Description] [nvarchar](100) NULL,
	 CONSTRAINT [PK_PDR_LandRegistryStatus] PRIMARY KEY NONCLUSTERED 
	(
		[RegistryStatusId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 30) ON [PRIMARY]
	) ON [PRIMARY]


END


	--===============================================================================
	-- US578 - INSERT (yes/no/ With land registry waiting registration)
	--===============================================================================

	IF NOT EXISTS (	SELECT	1 
					FROM	PDR_LandRegistryStatus 
					WHERE	[DESCRIPTION] ='With land registry waiting registration')
		BEGIN  

			INSERT INTO [dbo].[PDR_LandRegistryStatus] ([DESCRIPTION])
			VALUES ('Yes')

			INSERT INTO [dbo].[PDR_LandRegistryStatus] ([DESCRIPTION])
			VALUES ('No')

			INSERT INTO [dbo].[PDR_LandRegistryStatus] ([DESCRIPTION])
			VALUES ('With land registry waiting registration')

		END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH