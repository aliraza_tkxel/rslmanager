CREATE TABLE [dbo].[P_KPI]
(
[KPIID] [int] NULL,
[KPITYPE] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KPINAME] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KPIDESCRIPTION] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KPILINK] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_KPI] TO [rackspace_datareader]
GO
