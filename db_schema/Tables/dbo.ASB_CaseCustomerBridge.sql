USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_CaseCustomerBridge')
BEGIN
CREATE TABLE [dbo].[ASB_CaseCustomerBridge]
(
	[CaseCustBridgeId] [int] IDENTITY(1,1)PRIMARY KEY NOT NULL,
	[CaseId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[PersonRole] [nvarchar](50) NOT NULL,
	[PersonType] [nvarchar](50) NOT NULL
)

ALTER TABLE [dbo].[ASB_CaseCustomerBridge]  WITH CHECK ADD  CONSTRAINT [FK_ASB_CaseCustomerBridge_ASB_Case] FOREIGN KEY([CaseId])
REFERENCES [dbo].[ASB_Case] ([CaseId])
ALTER TABLE [dbo].[ASB_CaseCustomerBridge] CHECK CONSTRAINT [FK_ASB_CaseCustomerBridge_ASB_Case]
PRINT 'Table Created'
END

ELSE
BEGIN
PRINT 'Table already exist..'
END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH