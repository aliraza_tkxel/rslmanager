CREATE TABLE [dbo].[AM_CaseAndStandardLetter]
(
[CaseAndStandardLetterId] [int] NOT NULL IDENTITY(1, 1),
[CaseId] [int] NOT NULL,
[StandardLetterId] [int] NOT NULL,
[CaseHistoryId] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[StandardLetterHistoryId] [int] NULL,
[MarketRent] [float] NULL,
[RentBalance] [float] NULL,
[CreateDate] [datetime] NULL,
[RentAmount] [float] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_CaseAndStandardLetter] ADD 
CONSTRAINT [PK_AM_CaseAndStandardLetter] PRIMARY KEY CLUSTERED  ([CaseAndStandardLetterId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_CaseAndStandardLetter] ADD
CONSTRAINT [Relation_CaseHistory] FOREIGN KEY ([CaseHistoryId]) REFERENCES [dbo].[AM_CaseHistory] ([CaseHistoryId])
ALTER TABLE [dbo].[AM_CaseAndStandardLetter] ADD
CONSTRAINT [Reation_Case_StandardLetterHistoryId] FOREIGN KEY ([StandardLetterHistoryId]) REFERENCES [dbo].[AM_StandardLetterHistory] ([StandardLetterHistoryId])
ALTER TABLE [dbo].[AM_CaseAndStandardLetter] ADD
CONSTRAINT [Relation_StandardLettersId] FOREIGN KEY ([StandardLetterId]) REFERENCES [dbo].[AM_StandardLetters] ([StandardLetterId])
GO

ALTER TABLE [dbo].[AM_CaseAndStandardLetter] ADD CONSTRAINT [Relation_CaseId] FOREIGN KEY ([CaseId]) REFERENCES [dbo].[AM_Case] ([CaseId])
GO
