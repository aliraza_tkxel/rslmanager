Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF Not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_PaypointSubmission')
BEGIN

CREATE TABLE [dbo].[E_PaypointSubmission](
	[PaypointId] [int] IDENTITY(1,1) NOT NULL,
	[Salary] [float] NULL,
	[Grade] [int] NULL,
	[GradePoint] [int] NULL,
	[DateProposed] [datetime] NULL,
	[DetailRationale] [nvarchar](3000) NULL,
	[Approved] [bit] NULL,
	[Supported] [bit] NULL,
	[Authorized] [bit] NULL,
	[ApprovedDate] [datetime] NULL,
	[SupportedDate] [datetime] NULL,
	[AuthorizedDate] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[SupportedBy] [int] NULL,
	[AuthorizedBy] [int] NULL,	
	[employeeId] [int] NULL,
	[paypointStatusId] [int] NULL,
	[fiscalYearId] [int] NULL,
	[IsNotSubmitted] [bit],
	[Reason] [nvarchar](500) NULL,
 CONSTRAINT [PK_E_PaypointSubmission] PRIMARY KEY CLUSTERED 
(
	[PaypointId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_PaypointSubmission]' ) AND name = 'Reason')
	BEGIN
		ALTER TABLE E_PaypointSubmission ADD Reason nvarchar(500) NULL
	PRINT 'Reason added successfully!'
	END--if

;WITH CTE AS(
   SELECT PaypointId, Salary,Grade,GradePoint,DateProposed,DetailRationale,Approved,Supported,Authorized,ApprovedDate,SupportedDate,AuthorizedDate,ApprovedBy,SupportedBy,AuthorizedBy,employeeId,paypointStatusId,fiscalYearId,Reason,
       RN = ROW_NUMBER()OVER(PARTITION BY employeeid ORDER BY PaypointId desc)
   FROM dbo.E_PaypointSubmission
   where PayPointStatusId = 5
)
DELETE FROM CTE WHERE  RN > 1

IF COL_LENGTH('E_PaypointSubmission','DetailRationale') < 6000
	BEGIN
		ALTER TABLE E_PaypointSubmission
		ALTER COLUMN DetailRationale nvarchar(3000);
	END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_PaypointSubmission]' ) AND name = 'IsNotSubmitted')
BEGIN
	ALTER TABLE E_PaypointSubmission ADD IsNotSubmitted bit
PRINT 'IsNotSubmitted added successfully!'
END--if


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 


