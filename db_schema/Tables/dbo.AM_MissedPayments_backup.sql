CREATE TABLE [dbo].[AM_MissedPayments_backup]
(
[MissedTenantId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NOT NULL,
[TenantId] [int] NOT NULL,
[PaymentPlanId] [int] NOT NULL,
[PaymentId] [int] NOT NULL,
[MissedPaymentDetectionDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
