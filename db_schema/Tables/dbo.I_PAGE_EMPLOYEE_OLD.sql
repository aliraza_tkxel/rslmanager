CREATE TABLE [dbo].[I_PAGE_EMPLOYEE_OLD]
(
[PageEmployeeID] [int] NOT NULL IDENTITY(1, 1),
[PageID] [int] NULL,
[EmployeeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[I_PAGE_EMPLOYEE_OLD] ADD CONSTRAINT [PK_I_PAGE_EMPLOYEE] PRIMARY KEY CLUSTERED  ([PageEmployeeID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[I_PAGE_EMPLOYEE_OLD] WITH NOCHECK ADD CONSTRAINT [FK_I_PAGE_EMPLOYEE_I_PAGE] FOREIGN KEY ([PageID]) REFERENCES [dbo].[I_PAGE] ([PageID])
GO
