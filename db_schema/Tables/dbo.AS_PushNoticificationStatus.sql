USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AS_PushNoticificationStatus')
BEGIN
CREATE TABLE [dbo].[AS_PushNoticificationStatus]
(
	[PushNoticificationId] [int] IDENTITY(1,1)PRIMARY KEY NOT NULL,
	[StatusImagePath] [nvarchar](MAX) null,
	[StatusDescription] [nvarchar](MAX) null
) 
INSERT [dbo].[AS_PushNoticificationStatus] ([StatusImagePath], [StatusDescription]) VALUES ( N'~/Images/PushNoticificationNotSent.png', 'Push Noticification Not Sent')
INSERT [dbo].[AS_PushNoticificationStatus] ([StatusImagePath], [StatusDescription]) VALUES ( N'~/Images/PushNoticificationSending.png', 'Push Noticification Sending')
INSERT [dbo].[AS_PushNoticificationStatus] ([StatusImagePath], [StatusDescription]) VALUES ( N'~/Images/PushNoticificationSent.png', 'Push Noticification Sent')
INSERT [dbo].[AS_PushNoticificationStatus] ([StatusImagePath], [StatusDescription]) VALUES ( N'~/Images/PushNoticificationFailed.png', 'Push Noticification Sending Failed')

PRINT 'Table Created'
END

ELSE
BEGIN
PRINT 'Table already exist..'
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH