CREATE TABLE [dbo].[PS_SurveyFormFields]
(
[SurveyFormFieldId] [int] NOT NULL IDENTITY(1, 1),
[SurveyFromFieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SurveyFormFieldValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SurveyFormId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[PS_SurveyFormFields] ADD 
CONSTRAINT [PK_PS_SurveyFormFields] PRIMARY KEY CLUSTERED  ([SurveyFormFieldId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_SurveyFormFields] ADD
CONSTRAINT [FK_PS_SurveyFormFields_PS_SurveyForm] FOREIGN KEY ([SurveyFormId]) REFERENCES [dbo].[PS_SurveyForm] ([SurveyFormId])
GO
