USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY
IF NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'P_AsbestosRiskLevel')
		BEGIN

CREATE TABLE [dbo].[P_AsbestosRiskLevel](
	[AsbestosLevelId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](500) NOT NULL,
 CONSTRAINT [PK_P_AsbestosRiskLevel] PRIMARY KEY CLUSTERED 
(
	[AsbestosLevelId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
	ELSE

		BEGIN
			PRINT 'Table Already Exist';
		END

	
		
IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH

IF NOT EXISTS(Select * from [P_AsbestosRiskLevel])
BEGIN


INSERT [dbo].[P_AsbestosRiskLevel] ( [Description]) VALUES ( N'High')

INSERT [dbo].[P_AsbestosRiskLevel] ( [Description]) VALUES ( N'Medium')

INSERT [dbo].[P_AsbestosRiskLevel] ( [Description]) VALUES ( N'Low')

INSERT [dbo].[P_AsbestosRiskLevel] ( [Description]) VALUES ( N'Very Low')

END