CREATE TABLE [dbo].[AM_StandardLetterHistory]
(
[StandardLetterHistoryId] [int] NOT NULL IDENTITY(1, 1),
[StandardLetterId] [int] NOT NULL,
[StatusId] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedBy] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedBy] [int] NULL,
[ModifiedDate] [datetime] NULL,
[PersonalizationLookUpCode] [int] NULL,
[SignOffLookUpCode] [int] NULL,
[TeamId] [int] NULL,
[FromResourceId] [int] NULL,
[IsPrinted] [bit] NULL,
[IsActive] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD 
CONSTRAINT [PK_AM_StandardLetterHistory] PRIMARY KEY CLUSTERED  ([StandardLetterHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD
CONSTRAINT [Relation_SLH_StandardLetterId] FOREIGN KEY ([StandardLetterId]) REFERENCES [dbo].[AM_StandardLetters] ([StandardLetterId])
GO

ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD CONSTRAINT [Relation_SLH_ActionId] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD CONSTRAINT [Relation_SLH_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD CONSTRAINT [Relation_SLH_ModifiedBy] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD CONSTRAINT [Relation_SLH_Personalization] FOREIGN KEY ([PersonalizationLookUpCode]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD CONSTRAINT [Relation_SLH_SignOff] FOREIGN KEY ([SignOffLookUpCode]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO

ALTER TABLE [dbo].[AM_StandardLetterHistory] ADD CONSTRAINT [Relation_SLH_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
GO
