USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_File')
BEGIN
CREATE TABLE [dbo].[ASB_File]
(
	[FileId] [int] IDENTITY(1,1)PRIMARY KEY NOT NULL,
	[FileName] [nvarchar](100) NULL,
	[FilePath] [nvarchar](100) NULL,
	[UploadedDate] [smalldatetime] NULL,
	[UploadedBy] INT NULL,
	[CaseId] [int] NOT NULL,
	[FileType] [nvarchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[FileNotes] [nvarchar](max) NULL
) 
ALTER TABLE [dbo].[ASB_File]  WITH CHECK ADD  CONSTRAINT [FK_ASB_File_ASB_Case] FOREIGN KEY([CaseId])
REFERENCES [dbo].[ASB_Case] ([CaseId])
ALTER TABLE [dbo].[ASB_File] CHECK CONSTRAINT [FK_ASB_File_ASB_Case]
PRINT 'Table Created'
END

ELSE
BEGIN
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'FileNotes'
      AND Object_ID = Object_ID(N'ASB_File'))
BEGIN
    ALTER TABLE [dbo].[ASB_File]
	ADD [FileNotes] [nvarchar](max) NULL
	PRINT 'Added Column FileNotes'
END
PRINT 'Table already exist..'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH