USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_DOI_Status')
BEGIN

	CREATE TABLE [dbo].[E_DOI_Status](
		[DOIStatusId] [int] IDENTITY(1,1) NOT NULL,
		[Description] [nvarchar](50) NULL
	 CONSTRAINT [PK_DOIStatus] PRIMARY KEY NONCLUSTERED 
	(
		[DOIStatusId] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

END

IF NOT EXISTS (SELECT * FROM E_DOI_Status WHERE Description = 'Submitted')
BEGIN
	insert into E_DOI_Status (Description) VALUES('Submitted')
	PRINT 'Submitted added successfully'
END

IF NOT EXISTS (SELECT * FROM E_DOI_Status WHERE Description = 'Approved')
BEGIN
	insert into E_DOI_Status (Description) VALUES('Approved')
	PRINT 'Approved added successfully'
END

--======================================================
-- US591 - Added new status 'Pending Review'
--======================================================
IF NOT EXISTS ( SELECT	* 
				FROM	E_DOI_Status 
				WHERE	Description = 'Pending Review')
BEGIN
	
	INSERT INTO E_DOI_Status (Description) VALUES('Pending Review')
	PRINT 'Pending Review added successfully'

	DECLARE @PendingReviewStatusId int = @@Identity

	DECLARE @SubmittedStatusId int
	SELECT  @SubmittedStatusId = DOIStatusId
	FROM	E_DOI_Status
	WHERE	[Description] = 'Submitted'

	-- Update existing 'Submitted' status DOIs to Pending
	UPDATE	E_DOI
	SET		Status = @PendingReviewStatusId
	WHERE	Status = @SubmittedStatusId

	UPDATE	E_DOI_HISTORY
	SET		Status = @PendingReviewStatusId
	WHERE	Status = @SubmittedStatusId

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
Print (@ErrorMessage)
END CATCH 