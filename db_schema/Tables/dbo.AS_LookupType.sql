CREATE TABLE [dbo].[AS_LookupType]
(
[LookupTypeId] [int] NOT NULL IDENTITY(1, 1),
[TypeName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_LookupType] ADD 
CONSTRAINT [PK_AS_LookupType] PRIMARY KEY CLUSTERED  ([LookupTypeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
