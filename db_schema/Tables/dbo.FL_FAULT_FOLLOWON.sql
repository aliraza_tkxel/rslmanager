USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'FL_FAULT_FOLLOWON')
    BEGIN


CREATE TABLE [dbo].[FL_FAULT_FOLLOWON](
	[FollowOnID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FaultLogId] [int] NULL,
	[RecordedOn] [smalldatetime] NULL,
	[FollowOnNotes] [nvarchar](500) NULL,
	[isFollowonScheduled] [bit] NOT NULL,
 CONSTRAINT [PK_FL_FAULT_FOLLOWON] PRIMARY KEY CLUSTERED 
(
	[FollowOnID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[FL_FAULT_FOLLOWON]  WITH CHECK ADD  CONSTRAINT [FL_FAULT_FOLLOWON_FL_FAULT_LOG] FOREIGN KEY([FaultLogId])
REFERENCES [dbo].[FL_FAULT_LOG] ([FaultLogID])


ALTER TABLE [dbo].[FL_FAULT_FOLLOWON] CHECK CONSTRAINT [FL_FAULT_FOLLOWON_FL_FAULT_LOG]


ALTER TABLE [dbo].[FL_FAULT_FOLLOWON] ADD  CONSTRAINT [DF__FL_FAULT___isFol__7A499486]  DEFAULT ((0)) FOR [isFollowonScheduled]


 END
    ELSE

    BEGIN
      PRINT 'Table Already Exist';
    END

    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_FL_FAULT_FOLLOWON_FaultLogID')
	  BEGIN
		  CREATE NONCLUSTERED INDEX IX_FL_FAULT_FOLLOWON_FaultLogID
		  ON FL_FAULT_FOLLOWON (FaultLogID);

		  PRINT 'IX_FL_FAULT_FOLLOWON_FaultLogID created successfully';
	  END
	  ELSE
	  BEGIN
		 PRINT 'IX_FL_FAULT_FOLLOWON_FaultLogID Index Already Exist';
	  END

    

    --========================================================================================================
    --========================================================================================================

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH
