CREATE TABLE [dbo].[AM_FindOverDueActionCase]
(
[OverdueActionCaseId] [int] NOT NULL IDENTITY(1, 1),
[TenancyId] [int] NULL,
[CustomerId] [int] NULL,
[CustomerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerName2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerAddress] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RentBalance] [float] NULL,
[OwedToBha] [float] NULL,
[StatusTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionTitle] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSuppressed] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentPlan] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuppressedDate] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalRent] [float] NULL,
[EstimatedHBDue] [float] NULL,
[ActionReviewDate] [date] NULL,
[JointTenancyCount] [int] NULL,
[CaseId] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created] [datetime] NULL,
[Updated] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_FindOverDueActionCase] ADD 
CONSTRAINT [PK_AM_OverDueActionCase] PRIMARY KEY CLUSTERED  ([OverdueActionCaseId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
