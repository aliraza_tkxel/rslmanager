CREATE TABLE [dbo].[AM_Referral]
(
[ReferralId] [int] NOT NULL IDENTITY(1, 1),
[DateOfReferral] [datetime] NOT NULL,
[FollowupDate] [datetime] NOT NULL,
[TeamId] [int] NULL,
[ReferralTypeLookupCodeId] [int] NULL,
[TeamMemberName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCustomerAware] [bit] NOT NULL,
[OrganisationSupplierModuleId] [int] NULL,
[TeamMemberEmail] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactId] [int] NULL,
[OrganisationId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Referral] ADD 
CONSTRAINT [PK_AM_Referral] PRIMARY KEY CLUSTERED  ([ReferralId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AM_Referral] ADD
CONSTRAINT [FK_AM_Referral_AM_SupplierModule] FOREIGN KEY ([OrganisationSupplierModuleId]) REFERENCES [dbo].[AM_SupplierModule] ([SupplierModuleId])
GO

ALTER TABLE [dbo].[AM_Referral] ADD CONSTRAINT [FK_AM_Referral_AM_LookupCode] FOREIGN KEY ([ReferralTypeLookupCodeId]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
