USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PDR_ProvisionCategories]    Script Date: 9/25/2018 5:57:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION

BEGIN TRY

	IF NOT EXISTS (
			SELECT *
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_NAME = N'PDR_ProvisionCategories'
			)
	BEGIN

		CREATE TABLE [dbo].[PDR_ProvisionCategories](
			[ProvisionCategoryId] [int] IDENTITY(1,1) NOT NULL,
			[CategoryName] [nvarchar](100) NOT NULL,
			[UpdatedBy] [int] NULL,
		 CONSTRAINT [PK_PDR_ProvisionCategories] PRIMARY KEY CLUSTERED 
		(
			[ProvisionCategoryId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	END

	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Furniture')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Furniture')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Curtains & Blinds')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Curtains & Blinds')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Laundry Equipment')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Laundry Equipment')	
	END

	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Kitchen Equipment')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Kitchen Equipment')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Garden Structures & Furniture')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Garden Structures & Furniture')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Floor Coverings')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Floor Coverings')	
	END


	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Assisted Baths')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Assisted Baths')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Cleaning Equipment')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Cleaning Equipment')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Fire Panel')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Fire Panel')	
	END

	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Door Entry')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Door Entry')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Emergency Lighting')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Emergency Lighting')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'CCTV')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('CCTV')	
	END

	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Stair Lift')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Stair Lift')	
	END
	IF NOT EXISTS(Select 1 from PDR_ProvisionCategories where CategoryName  = 'Fire Extinguisher')
	BEGIN
		 INSERT INTO PDR_ProvisionCategories (CategoryName)
			 VALUES('Fire Extinguisher')	
	END


	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END

END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
	END

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE()
		,@ErrorSeverity = ERROR_SEVERITY()
		,@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (
			@ErrorMessage
			,@ErrorSeverity
			,@ErrorState
			);

	PRINT (@ErrorMessage)
END CATCH

