CREATE TABLE [dbo].[FL_FAULT_PREINSPECTIONINFO]
(
[PREINSPECTONID] [int] NOT NULL IDENTITY(1, 1),
[USERID] [int] NULL,
[INSPECTIONDATE] [datetime] NULL,
[NETCOST] [float] NULL,
[DUEDATE] [datetime] NULL,
[NOTES] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORGID] [int] NULL,
[APPROVED] [bit] NULL,
[faultlogid] [int] NULL,
[Recharge] [bit] NULL
) ON [PRIMARY]
GO
