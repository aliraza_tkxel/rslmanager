CREATE TABLE [dbo].[PLANNED_Action]
(
[ActionId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [smallint] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[ModifiedBy] [int] NULL,
[IsEditable] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PLANNED_Action] ADD 
CONSTRAINT [PK_PLANNED_Action] PRIMARY KEY CLUSTERED  ([ActionId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PLANNED_Action] ADD CONSTRAINT [FK_PLANNED_ACTION_PLANNED_STATUS] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[PLANNED_STATUS] ([STATUSID])
GO
