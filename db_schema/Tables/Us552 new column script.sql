
-- =============================================
-- Author:		Raja Basiq
-- Create date: 28/08/2018
-- Description:	to get list of child Items
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetMultiAttributeListByItemId] 
@schemeId int=null, 
@blockId int=null,
@itemId int =null 
AS
BEGIN
	SET NOCOUNT ON;
	 select id from PA_ChildAttributeMapping where  (SchemeId= @schemeId or BlockId=@blockId)  
 and ItemId =@itemId and IsActive =1
END


--------------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION
BEGIN TRY
	--==============================================================================
	-- Ticket # US552 (Add Table 'PA_ChildAttributeMapping') 
	--==============================================================================	
CREATE TABLE PA_ChildAttributeMapping (
    Id int identity(1,1),
    ItemId int,
	AttributeName nvarchar(100),
    SchemeId int,
	BlockId int ,
	IsActive bit
);


	--==============================================================================
	-- Ticket # US552 (Add Column 'BFSCarryingOutWork' to CM_ServiceItems) 
	--==============================================================================	
IF COL_LENGTH('CM_ServiceItems', 'BFSCarryingOutWork') IS NULL
	BEGIN
		ALTER TABLE CM_ServiceItems 
		ADD BFSCarryingOutWork Bit NOT NULL 
		CONSTRAINT D_CM_ServiceItems_BFSCarryingOutWork DEFAULT 0
		Print('BFSCarryingOutWork added successfully  ') 
	END	

	--==============================================================================
	-- Ticket # US552 (Add Column 'ChildAttributeMappingId' in PA_PROPERTY_ATTRIBUTES) 
	--==============================================================================	
IF COL_LENGTH('PA_PROPERTY_ATTRIBUTES', 'ChildAttributeMappingId') IS NULL
	BEGIN
		ALTER TABLE [dbo].[PA_PROPERTY_ATTRIBUTES]
		ADD ChildAttributeMappingId int NULL 
		CONSTRAINT D_PA_PROPERTY_ATTRIBUTES_ChildAttributeMappingId DEFAULT null
		Print('ChildAttributeMappingId added successfully  ') 
	END	

	--==============================================================================
	-- Ticket # US552 (Add Column 'ChildAttributeMappingId' in PA_PROPERTY_ITEM_DATES) 
	--==============================================================================	
IF COL_LENGTH('PA_PROPERTY_ITEM_DATES', 'ChildAttributeMappingId') IS NULL
	BEGIN
		ALTER TABLE [dbo].[PA_PROPERTY_ITEM_DATES]
		ADD ChildAttributeMappingId int NULL 
		CONSTRAINT D_PA_PROPERTY_ITEM_DATES_ChildAttributeMappingId DEFAULT null
		Print('ChildAttributeMappingId added successfully  ') 
	END	
	--==============================================================================
	-- Ticket # US552 (Add Column 'ChildAttributeMappingId' in PA_PROPERTY_ITEM_NOTES) 
	--==============================================================================	
IF COL_LENGTH('PA_PROPERTY_ITEM_NOTES', 'ChildAttributeMappingId') IS NULL
	BEGIN
		ALTER TABLE [dbo].[PA_PROPERTY_ITEM_NOTES]
		ADD ChildAttributeMappingId int NULL 
		CONSTRAINT D_PA_PROPERTY_ITEM_NOTES_ChildAttributeMappingId DEFAULT null
		Print('ChildAttributeMappingId added successfully  ') 
	END	
	--==============================================================================
	-- Ticket # US552 (Add Column 'ChildAttributeMappingId' in PA_PROPERTY_ITEM_IMAGES) 
	--==============================================================================	

IF COL_LENGTH('PA_PROPERTY_ITEM_IMAGES', 'ChildAttributeMappingId') IS NULL
	BEGIN
		ALTER TABLE [dbo].[PA_PROPERTY_ITEM_IMAGES]
		ADD ChildAttributeMappingId int NULL 
		CONSTRAINT D_PA_PROPERTY_ITEM_IMAGES_ChildAttributeMappingId DEFAULT null
		Print('ChildAttributeMappingId added successfully  ') 
	END	
	--==============================================================================
	-- Ticket # US552 (Add Column 'ChildAttributeMappingId' in PDR_MSAT) 
--	--==============================================================================
IF COL_LENGTH('PDR_MSAT', 'ChildAttributeMappingId') IS NULL
	BEGIN	
		ALTER TABLE [dbo].[PDR_MSAT]
		ADD ChildAttributeMappingId int NULL 
		CONSTRAINT D_PDR_MSAT_ChildAttributeMappingId DEFAULT null
		Print('ChildAttributeMappingId added successfully  ') 
	END	
  	--==============================================================================
	-- Ticket # US552 (Add Column 'ChildAttributeMappingId' in CM_ServiceItems) 
	--==============================================================================	
IF COL_LENGTH('CM_ServiceItems', 'ChildAttributeMappingId') IS NULL
	BEGIN	
		ALTER TABLE [dbo].[CM_ServiceItems]
		ADD ChildAttributeMappingId int NULL 
		CONSTRAINT D_CM_ServiceItems_ChildAttributeMappingId DEFAULT null
		Print('ChildAttributeMappingId added successfully  ') 
	END	

	--==============================================================================
	-- Ticket # US552 (Add Column 'TotalValue' to CM_ServiceItems) 
	--==============================================================================	
IF COL_LENGTH('CM_ServiceItems', 'TotalValue') IS NULL
	BEGIN
		ALTER TABLE CM_ServiceItems 
		ADD TotalValue nvarchar(100) NULL 
		CONSTRAINT D_CM_ServiceItems_TotalValue DEFAULT null
		Print('TotalValue added successfully  ') 
	END	


	--==============================================================================
	-- Ticket # US552 (Add Column 'VAT' to CM_ServiceItems) 
	--==============================================================================	
IF COL_LENGTH('CM_ServiceItems', 'VAT') IS NULL
	BEGIN
		ALTER TABLE CM_ServiceItems 
		ADD VAT nvarchar(100) NULL 
		CONSTRAINT D_CM_ServiceItems_VAT DEFAULT null
		Print('VAT added successfully  ') 
	END	

	--==============================================================================
	-- Ticket # US552 (Add Column 'CustomCycleFrequency' to CM_ServiceItems) 
	--==============================================================================	
IF COL_LENGTH('CM_ServiceItems', 'CustomCycleFrequency') IS NULL
	BEGIN
		ALTER TABLE CM_ServiceItems 
		ADD CustomCycleFrequency int NULL 
		CONSTRAINT D_CM_ServiceItems_CustomCycleFrequency DEFAULT null
		Print('CustomCycleFrequency added successfully  ') 
	END	

	--==============================================================================
	-- Ticket # US552 (Add Column 'CustomCycleOccurance' to CM_ServiceItems) 
	--==============================================================================	
IF COL_LENGTH('CM_ServiceItems', 'CustomCycleOccurance') IS NULL
	BEGIN
		ALTER TABLE CM_ServiceItems 
		ADD CustomCycleOccurance nvarchar(10) NULL 
		CONSTRAINT D_CM_ServiceItems_CustomCycleOccurance DEFAULT null
		Print('CustomCycleOccurance added successfully  ') 
	END	

	--==============================================================================
	-- Ticket # US552 (Add Column 'CustomCycleType' to CM_ServiceItems) 
	--==============================================================================	
	IF COL_LENGTH('CM_ServiceItems', 'CustomCycleType') IS NULL
	BEGIN
		ALTER TABLE CM_ServiceItems 
		ADD CustomCycleType nvarchar(20) NULL 
		CONSTRAINT D_CM_ServiceItems_CustomCycleType DEFAULT null
		Print('CustomCycleType added successfully  ') 
	END	


	IF @@TRANCOUNT > 0
	BEGIN  
	print'commit'   
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH