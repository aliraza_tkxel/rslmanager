USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_EmployeeLanguageSkills')
BEGIN

CREATE TABLE [dbo].[E_EmployeeLanguageSkills](
	[LanguageId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	LanguageTypeId [int] NULL,
	[Language] [nvarchar](500) NULL,
	[IsSpoken] [bit] NULL,
	[IsWritten] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_E_EmployeeLanguageSkills] PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
  IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_EmployeeLanguageSkills]' ) AND name = 'LanguageTypeId')
	BEGIN
		ALTER TABLE E_EmployeeLanguageSkills ADD LanguageTypeId INT NULL
	PRINT 'LanguageTypeId added successfully!'
	END--if  
    
    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH    


