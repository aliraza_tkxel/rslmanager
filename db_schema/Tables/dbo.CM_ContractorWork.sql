USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'CM_ContractorWork')
BEGIN

CREATE TABLE [dbo].[CM_ContractorWork](
	[CMContractorId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceItemId] [int] NULL,
	[ContractorId] [int] NULL,
	[ContactId] [int] NULL,
	[ExpenditureId] [int] NULL,
	[ContractNetValue] [money] NULL,
	[VatId] [int] NULL,
	[Vat] [money] NULL,
	[ContractGrossValue] [money] NULL,
	[SendApprovalLink] [bit] NULL,
	[Commence] [datetime] NULL,
	[AssignedBy] [int] NULL,
	[AssignedDate] [datetime] NULL,
	[RequiredWorks] [nvarchar](1000) NULL,
	[PurchaseOrderId] [int] NULL,
 CONSTRAINT [PK_CM_ContractorWork] PRIMARY KEY CLUSTERED 
(
	[CMContractorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




