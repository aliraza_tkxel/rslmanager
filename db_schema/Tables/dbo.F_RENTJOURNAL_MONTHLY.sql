CREATE TABLE [dbo].[F_RENTJOURNAL_MONTHLY]
(
[JOURNALID] [int] NOT NULL,
[TENANCYID] [int] NOT NULL,
[TRANSACTIONDATE] [smalldatetime] NULL,
[AMOUNT] [money] NULL,
[RENT] [money] NULL,
[SERVICES] [money] NULL,
[COUNCILTAX] [money] NULL,
[WATERRATES] [money] NULL,
[INELIGSERV] [money] NULL,
[SUPPORTEDSERVICES] [money] NULL,
[GARAGE] [money] NULL,
[TOTALRENT] [money] NULL,
[PROPERTYID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RENTTYPE] [int] NULL,
[PROPERTYTYPE] [int] NULL
) ON [PRIMARY]
GO
