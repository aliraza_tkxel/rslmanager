CREATE TABLE [dbo].[C_REPAIRCOMPLETION]
(
[C_COMPLETIONID] [int] NOT NULL IDENTITY(1, 1),
[JOURNALID] [int] NULL,
[REPAIRHISTORYID] [int] NULL,
[ITEMDETAILID] [int] NULL,
[COMPLETIONDATE] [smalldatetime] NULL CONSTRAINT [DF_C_REPAIRCOMPLETION_COMPLETIONDATE] DEFAULT (getdate()),
[GASCERTIFICATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REPTIMESTAMP] [smalldatetime] NOT NULL CONSTRAINT [DF_C_REPAIRCOMPLETION_REPTIMESTAMP] DEFAULT (getdate())
) ON [PRIMARY]
GO
