CREATE TABLE [dbo].[Planned_Appointment_Type]
(
[Planned_Appointment_TypeId] [int] NOT NULL IDENTITY(1, 1),
[Planned_Appointment_Type] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Planned_Appointment_Type] ADD CONSTRAINT [PK_Planned_Appointment_Type] PRIMARY KEY CLUSTERED  ([Planned_Appointment_TypeId]) ON [PRIMARY]
GO
