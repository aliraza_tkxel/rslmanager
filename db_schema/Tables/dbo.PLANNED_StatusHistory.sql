CREATE TABLE [dbo].[PLANNED_StatusHistory]
(
[StatusHistoryId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [smallint] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [smallint] NOT NULL,
[CreatedDate] [smalldatetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedBy] [int] NOT NULL,
[ModifiedDate] [smalldatetime] NULL,
[IsEditable] [bit] NULL,
[Ctimestamp] [smalldatetime] NULL CONSTRAINT [DF__PLANNED_S__Ctime__1F5B1DA4] DEFAULT (getdate())
) ON [PRIMARY]
ALTER TABLE [dbo].[PLANNED_StatusHistory] ADD 
CONSTRAINT [PK_PLANNED_StatusHistory] PRIMARY KEY CLUSTERED  ([StatusHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PLANNED_StatusHistory] ADD CONSTRAINT [FK_PLANNED_StatusHistory_PLANNED_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[PLANNED_STATUS] ([STATUSID])
GO
