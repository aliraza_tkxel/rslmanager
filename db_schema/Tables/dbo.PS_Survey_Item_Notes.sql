CREATE TABLE [dbo].[PS_Survey_Item_Notes]
(
[NotesId] [int] NOT NULL IDENTITY(1, 1),
[SurveyId] [int] NULL,
[ItemId] [int] NULL,
[Notes] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotesDate] [smalldatetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Item_Notes] ADD 
CONSTRAINT [PK_PS_Survey_Item_Notes] PRIMARY KEY CLUSTERED  ([NotesId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Item_Notes] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Item_Notes_PA_ITEM] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[PA_ITEM] ([ItemID])
ALTER TABLE [dbo].[PS_Survey_Item_Notes] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Item_Notes_PS_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[PS_Survey] ([SurveyId])
GO
