USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_DETECTOR_HISTORY')
BEGIN

CREATE TABLE [dbo].[P_DETECTOR_HISTORY](
	[DetectorHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[DetectorId] [int] NOT NULL,
	[PropertyId] [nvarchar](40) NOT NULL,
	[DetectorTypeId] [int] NOT NULL,
	[Location] [nvarchar](200) NULL,
	[Manufacturer] [nvarchar](100) NULL,
	[SerialNumber] [nvarchar](100) NULL,
	[PowerSource] [int] NULL,
	[InstalledDate] [date] NULL,
	[InstalledBy] [int] NULL,
	[IsLandlordsDetector] [bit] NULL,
	[TestedDate] [date] NULL,
	[TestedBy] [int] NULL,
	[BatteryReplaced] [date] NULL,
	[Passed] [bit] NULL,
	[Notes] [nvarchar](1000) NULL,
	[JournalId] [int] NULL,
	[CTimeStamp] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_P_DETECTOR_HISTORY] PRIMARY KEY CLUSTERED 
(
	[DetectorHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[P_DETECTOR_HISTORY] ADD  CONSTRAINT [DF_P_DETECTOR_HISTORY_CTimeStamp]  DEFAULT (getdate()) FOR [CTimeStamp]


ALTER TABLE [dbo].[P_DETECTOR_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_P_DETECTOR_HISTORY_AS_DetectorType] FOREIGN KEY([DetectorTypeId])
REFERENCES [dbo].[AS_DetectorType] ([DetectorTypeId])


ALTER TABLE [dbo].[P_DETECTOR_HISTORY] CHECK CONSTRAINT [FK_P_DETECTOR_HISTORY_AS_DetectorType]


ALTER TABLE [dbo].[P_DETECTOR_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_P_DETECTOR_HISTORY_AS_JOURNAL] FOREIGN KEY([JournalId])
REFERENCES [dbo].[AS_JOURNAL] ([JOURNALID])


ALTER TABLE [dbo].[P_DETECTOR_HISTORY] CHECK CONSTRAINT [FK_P_DETECTOR_HISTORY_AS_JOURNAL]


ALTER TABLE [dbo].[P_DETECTOR_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_P_DETECTOR_HISTORY_E__EMPLOYEE] FOREIGN KEY([InstalledBy])
REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])


ALTER TABLE [dbo].[P_DETECTOR_HISTORY] CHECK CONSTRAINT [FK_P_DETECTOR_HISTORY_E__EMPLOYEE]


ALTER TABLE [dbo].[P_DETECTOR_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_P_DETECTOR_HISTORY_E__EMPLOYEE1] FOREIGN KEY([TestedBy])
REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])


ALTER TABLE [dbo].[P_DETECTOR_HISTORY] CHECK CONSTRAINT [FK_P_DETECTOR_HISTORY_E__EMPLOYEE1]


ALTER TABLE [dbo].[P_DETECTOR_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_P_DETECTOR_HISTORY_P_DETECTOR] FOREIGN KEY([DetectorId])
REFERENCES [dbo].[P_DETECTOR] ([DetectorId])


ALTER TABLE [dbo].[P_DETECTOR_HISTORY] CHECK CONSTRAINT [FK_P_DETECTOR_HISTORY_P_DETECTOR]


ALTER TABLE [dbo].[P_DETECTOR_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_P_DETECTOR_HISTORY_P_PowerSourceType] FOREIGN KEY([PowerSource])
REFERENCES [dbo].[P_PowerSourceType] ([PowerTypeId])


ALTER TABLE [dbo].[P_DETECTOR_HISTORY] CHECK CONSTRAINT [FK_P_DETECTOR_HISTORY_P_PowerSourceType]


END


	--===============================================================================================================
	--  10828 - Add schemeId and blockId columns
	--===============================================================================================================

	IF COL_LENGTH('P_DETECTOR_HISTORY', 'schemeId') IS NULL
	BEGIN
			ALTER TABLE P_DETECTOR_HISTORY
			ADD schemeId int NULL
			PRINT('COLUMN schemeId CREATED')
	END

	IF COL_LENGTH('P_DETECTOR_HISTORY', 'blockId') IS NULL
	BEGIN
			ALTER TABLE P_DETECTOR_HISTORY
			ADD blockId int NULL
			PRINT('COLUMN blockId CREATED')
	END


	IF EXISTS(	SELECT	*
				FROM	(	SELECT  is_nullable 
							FROM    sys.columns 
							WHERE   object_id = object_id('dbo.P_DETECTOR_HISTORY') 
									AND name = 'PropertyId')  ConstraintCheck
				WHERE   ConstraintCheck.is_nullable ='0')
	BEGIN
	   ALTER TABLE P_DETECTOR_HISTORY 
	   ALTER COLUMN PropertyId [nvarchar](20) NULL

	   PRINT('PropertyId SCHEMA UPDATED')
	END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


