BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_EXPENDITURE')
BEGIN
  PRINT 'Table Exists';
 
 
IF EXISTS (Select 1 from F_EXPENDITURE where DESCRIPTION='Voids')
	BEGIN  
		PRINT 'Description already exist'
	END
IF NOT EXISTS (Select 1 from F_EXPENDITURE where DESCRIPTION='Voids')
	BEGIN 	
	
	DECLARE @headId int
	SELECT @headId = HEADID FROM F_HEAD WHERE DESCRIPTION = 'Responsive'

	INSERT INTO F_EXPENDITURE (DESCRIPTION,EXPENDITUREALLOCATION_Z,EXPENDITUREDATE,USERID,ACTIVE_Z,LASTMODIFIED,
	HEADID,QBDEBITCODE,QBCONTROLCODE,OPENBALANCE) VALUES
	('Voids',NULL,GETDATE(),601,NULL,GETDATE(),@headId,5580,2400,0.00)

	PRINT 'New row added'
END
 
 
END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

IF not EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'VATControlAccount'
          AND Object_ID = Object_ID(N'F_EXPENDITURE'))
BEGIN
alter table F_EXPENDITURE add VATControlAccount NVARCHAR(7)
END
GO

