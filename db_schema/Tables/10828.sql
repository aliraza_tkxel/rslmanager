Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


	Update PA_ITEM SET IsForSchemeBlock=0 where PA_ITEM.ItemName IN ('Heating','Heating Controls')
DECLARE @ItemId INT,@parameterId int,@heatingType int

Select @ItemId=PA_ITEM.ItemID from PA_ITEM where PA_ITEM.ItemName ='Boiler Room' 
Select @heatingType=PA_PARAMETER.ParameterID from PA_PARAMETER where PA_PARAMETER.ParameterName='Heating Type'
IF NOT EXISTS(Select * from PA_ITEM_PARAMETER where PA_ITEM_PARAMETER.ItemId=@ItemId)
 BEGIN
	 INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp, ShowInAccomodation)
	 Select 'Boiler Type',DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp, ShowInAccomodation from PA_PARAMETER where PA_PARAMETER.ParameterName='Heating Type'
	 SET @parameterId = SCOPE_IDENTITY();
	 
 INSERT INTO PA_ITEM_PARAMETER(PA_ITEM_PARAMETER.ItemId,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive )
 Values(@ItemId,@parameterId,1)
 
 INSERT INTO PA_PARAMETER_VALUE(PA_PARAMETER_VALUE.ParameterID,PA_PARAMETER_VALUE.ValueDetail, PA_PARAMETER_VALUE.Sorder,PA_PARAMETER_VALUE.IsActive)
 Select @parameterId,PA_PARAMETER_VALUE.ValueDetail, PA_PARAMETER_VALUE.Sorder,PA_PARAMETER_VALUE.IsActive From PA_PARAMETER_VALUE Where PA_PARAMETER_VALUE.ParameterID =@heatingType
 
 INSERT INTO PA_ITEM_PARAMETER(PA_ITEM_PARAMETER.ItemId,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive )
 Select @ItemId, PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive from PA_ITEM_PARAMETER 
	INNER JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID and PA_PARAMETER.IsActive=1
	where PA_ITEM_PARAMETER.ItemId = 74 and (PA_ITEM_PARAMETER.ParameterValueId=172 OR PA_ITEM_PARAMETER.ParameterValueId is null)
	AND PA_ITEM_PARAMETER.IsActive=1 
	And PA_PARAMETER.ParameterName <> 'Heating Type'
	Order by PA_PARAMETER.ParameterSorder ASC

 END
--Update PA_PROPERTY_ATTRIBUTES SET HeatingMappingId=null where PA_PROPERTY_ATTRIBUTES.SchemeId IS NOT NULL and HeatingMappingId IS NOT NULL
--UPDATE PA_PROPERTY_ITEM_DATES SET HeatingMappingId=null where SchemeId IS NOT NULL and HeatingMappingId IS NOT NULL

--DELETE AS_JOURNALHISTORY where HeatingMappingId IS NOT NULL and AS_JOURNALHISTORY.PROPERTYID IS NULL
--DELETE From AS_JOURNAL where HeatingMappingId IS NOT NULL and AS_JOURNAL.PROPERTYID IS NULL
--DELETE FROM PA_HeatingMapping where PA_HeatingMapping.SchemeID IS NOT NULL
--Select @heatingFuel= PA_PARAMETER.ParameterID from PA_PARAMETER where PA_PARAMETER.ParameterName='Heating Fuel'
  --  Select * from AS_JOURNAL WHERE HeatingMappingId IS NOT NULL

IF @@TRANCOUNT > 0
	BEGIN  
	print'commit'   
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH