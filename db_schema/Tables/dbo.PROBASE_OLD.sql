CREATE TABLE [dbo].[PROBASE_OLD]
(
[uprn] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address1] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address2] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address3] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[town] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[county] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[postcode] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sap] [smallint] NULL
) ON [PRIMARY]
GO
