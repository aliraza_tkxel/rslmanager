CREATE TABLE [dbo].[F_PAYTHRU_GET_TOKEN_REQUEST_LOG]
(
[GetTokenRequestId] [int] NOT NULL IDENTITY(1, 1),
[TransactionId] [uniqueidentifier] NULL,
[RequestUrl] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Applet] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuccessUrl] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CancelUrl] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CallbackUrl] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Surname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Maid] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomTenancyRef] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCustomerRef] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomStaffId] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomStaffName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressPropertyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressTown] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressCounty] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressPostcode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressCountry] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateSubmitted] [datetime] NULL,
[DateCreated] [datetime] NULL CONSTRAINT [DF_F_PAYTHRU_GET_TOKEN_REQUEST_LOG_DateCreated] DEFAULT (getdate()),
[CreatedBy] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_F_PAYTHRU_GET_TOKEN_REQUEST_LOG_CreatedBy] DEFAULT (N'SYSTEM_USER'),
[Version] [timestamp] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[F_PAYTHRU_GET_TOKEN_REQUEST_LOG] ADD 
CONSTRAINT [PK_F_PAYTHRU_GET_TOKEN_REQUEST_LOG] PRIMARY KEY CLUSTERED  ([GetTokenRequestId]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_PAYTHRU_GET_TOKEN_REQUEST_LOG_TransactionId] ON [dbo].[F_PAYTHRU_GET_TOKEN_REQUEST_LOG] ([TransactionId]) WITH (FILLFACTOR=100) ON [PRIMARY]

GO
