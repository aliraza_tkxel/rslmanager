Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PA_PARAMETER')
BEGIN

CREATE TABLE [dbo].[PA_PARAMETER](
	[ParameterID] [int] IDENTITY(1,1) NOT NULL,
	[ParameterName] [varchar](50) NOT NULL,
	[DataType] [varchar](50) NULL,
	[ControlType] [varchar](50) NULL,
	[IsDate] [bit] NOT NULL,
	[ParameterSorder] [int] NULL,
	[IsActive] [bit] NULL,
	[ShowInApp] [bit] NULL,
	[ShowInAccomodation] [bit] NULL,
 CONSTRAINT [PK_PA_PARAMETER] PRIMARY KEY CLUSTERED 
(
	[ParameterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[PA_PARAMETER] ADD  CONSTRAINT [DF_PA_PARAMETER_IsDate]  DEFAULT ((0)) FOR [IsDate]
ALTER TABLE [dbo].[PA_PARAMETER] ADD  CONSTRAINT [DF__PA_PARAME__IsAct__4E20F985]  DEFAULT ((1)) FOR [IsActive]
ALTER TABLE [dbo].[PA_PARAMETER] ADD  CONSTRAINT [DF__PA_PARAME__ShowI__27E70D5B]  DEFAULT ((1)) FOR [ShowInApp]
ALTER TABLE [dbo].[PA_PARAMETER] ADD  DEFAULT ((0)) FOR [ShowInAccomodation]


END


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PA_PARAMETER')
BEGIN

	--==============================================================================
	-- Ticket # 8134 (Added 'Model' parameter in 'Heating' item
	-- placed below 'Boiler Manufacturer') 
	--==============================================================================	
  
	IF NOT EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='Model' AND ControlType = 'TextBox')
	BEGIN 	
	
		UPDATE	PA_PARAMETER
		SET		ParameterSorder = ParameterSorder -1
		FROM	PA_PARAMETER
		WHERE	ParameterName = 'Heating Type'

		DECLARE @manufacturerOrder int
		SELECT	@manufacturerOrder = ParameterSorder
		FROM	PA_PARAMETER
		WHERE	ParameterName = 'Boiler Manufacturer'

		UPDATE	PA_PARAMETER
		SET		ParameterSorder = @manufacturerOrder - 1
		WHERE	ParameterName = 'Boiler Manufacturer'

		UPDATE	PA_PARAMETER
		SET		ParameterSorder = @manufacturerOrder + 1
		WHERE	ParameterName = 'Serial No'
		
		INSERT INTO [PA_PARAMETER]
		([ParameterName],[DataType],[ControlType],[IsDate],[ParameterSorder],[IsActive],[ShowInApp],[ShowInAccomodation])
		VALUES ('Model','String','TextBox',0,@manufacturerOrder,1,1,0)
		PRINT 'Model parameter successfully added.'
	END
	ELSE
	BEGIN
		PRINT 'Model parameter already exist.'
	END
	
	
	--==============================================================================
	-- Ticket # 10830 (Parameter 'Heating Fuel' updated) 
	--==============================================================================	
  
	IF EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='Primary Heating Fuel')
	BEGIN 	
	
		UPDATE	PA_PARAMETER
		SET		ParameterName = 'Heating Fuel'
		FROM	PA_PARAMETER
		WHERE	ParameterName='Primary Heating Fuel'

	END
	ELSE
	BEGIN
		PRINT 'Parameter Heating Fuel already updated.'
	END
	
	--==============================================================================
	-- Ticket # 10830 (Parameter 'Heating Type' updated) 
	--==============================================================================	
  
	IF EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='Boiler Type')
	BEGIN 	
	
		UPDATE	PA_PARAMETER
		SET		ParameterName = 'Heating Type'
		FROM	PA_PARAMETER
		WHERE	ParameterName='Boiler Type'

	END
	ELSE
	BEGIN
		PRINT 'Parameter Heating Type already updated.'
	END

	--==============================================================================
	-- Ticket # 10875 (Added 'Gasket Set Replacement' parameter in 'Heating' item
	-- placed below 'Installed Date') 
	--==============================================================================	
  
	IF NOT EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='Gasket Set Replacement')
	BEGIN 		
		INSERT INTO [PA_PARAMETER]
		([ParameterName],[DataType],[ControlType],[IsDate],[ParameterSorder],[IsActive],[ShowInApp],[ShowInAccomodation])
		VALUES ('Gasket Set Replacement','Date','Date',1,8,1,1,0)
		PRINT 'Gasket Set Replacement parameter successfully added.'
	END
	ELSE
	BEGIN
		PRINT 'Gasket Set Replacement parameter already exist.'
	END

	--==============================================================================
	-- Ticket # ID79 (Added 'CUR Last Replaced' parameter)
	--==============================================================================	
  
	IF NOT EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='CUR Last Replaced')
	BEGIN 		
		INSERT INTO [dbo].[PA_PARAMETER]
		([ParameterName], [DataType], [ControlType], [IsDate], [ParameterSorder], [IsActive], [ShowInApp], [ShowInAccomodation])
		VALUES ('CUR Last Replaced','Date','Date', 1, 12, 1, 1, 0)

		PRINT 'CUR Last Replaced parameter successfully added.'
	END
	ELSE
	BEGIN
		PRINT 'CUR Last Replaced parameter already exist.'
	END

	--==============================================================================
	-- Ticket # 438 (Added 'Solar Type' parameter)
	--==============================================================================	
  
	IF NOT EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='Solar Type')
	BEGIN 		
		INSERT INTO PA_PARAMETER
		([ParameterName], [DataType], [ControlType], [IsDate], [ParameterSorder], [IsActive], [ShowInApp], [ShowInAccomodation])
		VALUES ('Solar Type','String','Dropdown', 0, 1, 1, 1, 0)

		PRINT 'Solar Type parameter successfully added.'

		DECLARE @solarTypeParamId INT 
		SELECT @solarTypeParamId = SCOPE_IDENTITY()

		INSERT INTO PA_PARAMETER_VALUE
		(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)
		VALUES (@solarTypeParamId,'Drain Back',1,1,NULL)
		PRINT('Drain Back added successfully')

		DECLARE @solarFuelTypeParamValueId INT 
		DECLARE @heatingFuelParamId INT

		SELECT @heatingFuelParamId = ParameterID FROM PA_PARAMETER
		WHERE ParameterName = 'Heating Fuel'

		SELECT @solarFuelTypeParamValueId = ValueID FROM PA_PARAMETER_VALUE 
		WHERE ValueDetail = 'Solar' AND ParameterID = @heatingFuelParamId AND IsAlterNativeHeating = 1

		DECLARE @heatingItemId INT
		SELECT @heatingItemId = ItemId FROM PA_ITEM
		WHERE ItemName = 'Heating'

		INSERT INTO [dbo].[PA_ITEM_PARAMETER]
		([ItemId], [ParameterId], [IsActive], [SchemeId], [BlockId],[ParameterValueId])
		VALUES
			(@heatingItemId,@solarTypeParamId,1,null,null,@solarFuelTypeParamValueId)

		PRINT 'Solar Type parameter successfully added in PA_ITEM_PARAMETER.'
	END

	--==============================================================================
	-- Ticket # US226 (Parameter 'Installed Date' updated) 
	--==============================================================================	
  
	IF EXISTS (	SELECT	1 
					FROM	PA_PARAMETER 
					WHERE	ParameterName='Installed Date')
	BEGIN 	
	
		UPDATE	PA_PARAMETER
		SET		ParameterName = 'Original Install Date'
		FROM	PA_PARAMETER
		WHERE	ParameterName='Installed Date'

	END
	ELSE
	BEGIN
		PRINT 'Parameter Installed Date already updated.'
	END

	



END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH