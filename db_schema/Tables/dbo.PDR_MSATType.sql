/****** Object:  Table [dbo].[PDR_MSATTYPE]    Script Date: 22-Oct-18 4:02:23 PM ******/
Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PDR_MSATType')
	BEGIN
		CREATE TABLE [dbo].[PDR_MSATType](
		[MSATTypeId] [int] NOT NULL,
		[MSATTypeName] [nvarchar](200) NULL,
		[IsActive] [bit] NULL,
		CONSTRAINT [PK_PDR_MSATType] PRIMARY KEY CLUSTERED 
		(
		[MSATTypeId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END

	IF NOT EXISTS (Select 1 from PDR_MSATType where MSATTypeName='Raise A PO')
	BEGIN 	
		INSERT INTO PDR_MSATType(MSATTypeId,MSATTypeName,IsActive)Values(12,'Raise A PO',1)
		PRINT 'Title Raise A PO added successfully'
	END
	

	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState);
	Print (@ErrorMessage)
END CATCH