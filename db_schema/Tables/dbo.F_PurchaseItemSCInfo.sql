USE [RSLBHALive]


/****** Object:  Table [dbo].[F_PurchaseItemSCInfo]    Script Date: 01-Jan-19 6:59:36 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_PurchaseItemSCInfo')
BEGIN
	CREATE TABLE [dbo].[F_PurchaseItemSCInfo](
		[SID] [int] IDENTITY(1,1) NOT NULL,
		[OrderId] [int] NULL,
		[OrderItemId] [int] NULL,
		[SchemeId] [int] NULL,
		[BlockId] [int] NULL,
		[PropertyId] [nvarchar](30) NULL,
		[IsActive] [BIT] NULL
	 CONSTRAINT [PK_F_PurchaseItemSCInfo] PRIMARY KEY CLUSTERED 
	(
		[SID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	PRINT 'Table F_PurchaseItemSCInfo Created';
END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH