USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[G_OFFICE]    Script Date: 04/07/2017 07:52:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'G_OFFICE')
BEGIN
CREATE TABLE [dbo].[G_OFFICE](
	[OFFICEID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
 CONSTRAINT [PK_G_OFFICES] PRIMARY KEY NONCLUSTERED 
(
	[OFFICEID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

END
ELSE
BEGIN
if NOT exists(select * from [dbo].[G_OFFICE]
              where [DESCRIPTION] = 'IVR')
BEGIN
	INSERT INTO [dbo].[G_OFFICE] VALUES ('IVR')
	Print 'Add row for IVR Local Office'
END
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH