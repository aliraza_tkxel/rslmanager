CREATE TABLE [dbo].[AC_PASSWORD_HISTORY]
(
[HistoryId] [int] NOT NULL IDENTITY(1, 1),
[LoginId] [int] NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AC_PASSWORD_HISTORY] ADD 
CONSTRAINT [PK_AC_PASSWORD_HISTORY] PRIMARY KEY CLUSTERED  ([HistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
