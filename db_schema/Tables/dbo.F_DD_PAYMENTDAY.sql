CREATE TABLE [dbo].[F_DD_PAYMENTDAY]
(
[PAYMENTDAYID] [int] NULL,
[PAYMENTDAY] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DAYALIAS] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
