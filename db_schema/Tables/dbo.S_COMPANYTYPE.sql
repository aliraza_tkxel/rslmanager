CREATE TABLE [dbo].[S_COMPANYTYPE]
(
[TYPEID] [int] NOT NULL IDENTITY(1, 1),
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[S_COMPANYTYPE] ADD CONSTRAINT [PK_S_COMPANYTYPE] PRIMARY KEY NONCLUSTERED  ([TYPEID]) WITH FILLFACTOR=30 ON [PRIMARY]
GO
