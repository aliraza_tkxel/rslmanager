USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_DOI_Relationship')
BEGIN
CREATE TABLE [dbo].[E_DOI_Relationship]
(
	[RelationshipId] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[DisplayOrder] [int] NULL
) 

PRINT 'Table Created'

END

ELSE
BEGIN

PRINT 'Table already exist..'
END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'E_DOI_Relationship')
BEGIN
	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Spouse')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Spouse','',1)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Parent')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Parent','',2)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Son')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Son','',3)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Daughter')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Daughter','',4)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Sibling')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Sibling','',5)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Other Family Member')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Other Family Member','',6)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Friend')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Friend','',7)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Work Colleague')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Work Colleague','',8)
	END

	IF NOT EXISTS (Select 1 from E_DOI_Relationship where Name='Supplier Colleague')
	BEGIN 	
		INSERT INTO [E_DOI_Relationship] ([Name],[Description],[DisplayOrder])
		VALUES	('Supplier Colleague','',9)
	END
END





IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

