USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'C_REFERRAL')
BEGIN
	CREATE TABLE [dbo].[C_REFERRAL](
	[REFERRALHISTORYID] [int] IDENTITY(1,1) NOT NULL,
	[JOURNALID] [int] NOT NULL,
	[ITEMSTATUSID] [int] NULL,
	[ITEMACTIONID] [int] NULL,
	[isTypeArrears] [bit] NULL,
	[isTypeDamage] [bit] NULL,
	[isTypeASB] [bit] NULL,
	[STAGEID] [tinyint] NULL,
	[SAFETYISSUES] [tinyint] NULL,
	[CONVICTION] [tinyint] NULL,
	[SUBSTANCEMISUSE] [tinyint] NULL,
	[SELFHARM] [tinyint] NULL,
	[NOTES] [nvarchar](1000) NULL,
	[ISTENANTAWARE] [tinyint] NULL,
	[NEGLECTNOTES] [nvarchar](1000) NULL,
	[OTHERNOTES] [nvarchar](1000) NULL,
	[MISCNOTES] [nvarchar](1000) NULL,
	[REASONNOTES] [nvarchar](1000) NULL,
	[LASTACTIONDATE] [smalldatetime] NOT NULL CONSTRAINT [DF_C_REFERRAL_LASTACTIONDATE]  DEFAULT (getdate()),
	[ASSIGNTO] [int] NULL,
	[LASTACTIONUSER] [int] NULL,
	 CONSTRAINT [PK_C_REFERRAL] PRIMARY KEY CLUSTERED 
	(
		[REFERRALHISTORYID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[C_REFERRAL]  WITH CHECK ADD  CONSTRAINT [FK__C_REFERRA__JOURN__7FC276BA] FOREIGN KEY([JOURNALID])
	REFERENCES [dbo].[C_JOURNAL] ([JOURNALID])
	ALTER TABLE [dbo].[C_REFERRAL] CHECK CONSTRAINT [FK__C_REFERRA__JOURN__7FC276BA]

	print 'Table C_REFERRAL created'
END
ELSE
BEGIN
print 'Table C_REFERRAL already exist'
IF COL_LENGTH('C_REFERRAL', 'AppVersion') IS NULL
	BEGIN
			ALTER TABLE C_REFERRAL
			ADD AppVersion NVARCHAR(100) NULL
			PRINT('COLUMN AppVersion Added')
	END
	
IF COL_LENGTH('C_REFERRAL', 'CreatedOnApp') IS NULL
	BEGIN
			ALTER TABLE C_REFERRAL
			ADD CreatedOnApp SMALLDATETIME NULL
			PRINT('COLUMN CreatedOnApp Added')
	END
								
IF COL_LENGTH('C_REFERRAL', 'CreatedOnServer') IS NULL
	BEGIN
			ALTER TABLE C_REFERRAL
			ADD CreatedOnServer SMALLDATETIME NULL
			PRINT('COLUMN CreatedOnServer Added')
	END
		
IF COL_LENGTH('C_REFERRAL', 'LastModifiedOnApp') IS NULL
	BEGIN
			ALTER TABLE C_REFERRAL
			ADD LastModifiedOnApp SMALLDATETIME NULL
			PRINT('COLUMN LastModifiedOnApp Added')
	END		
IF COL_LENGTH('C_REFERRAL', 'LastModifiedOnServer') IS NULL
	BEGIN
			ALTER TABLE C_REFERRAL
			ADD LastModifiedOnServer SMALLDATETIME NULL
			PRINT('COLUMN LastModifiedOnServer Added')
	END	
END


IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
