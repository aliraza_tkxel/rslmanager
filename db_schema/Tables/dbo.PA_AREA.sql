/****** Script for SelectTopNRows command from SSMS  ******/

USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

-------------------------------- CREATING TABLE IF NOT EXISTS ------------------------------------------

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			WHERE TABLE_NAME = N'PA_AREA')
BEGIN

		CREATE TABLE [dbo].[PA_AREA](
			[AreaID] [int] IDENTITY(1,1) NOT NULL,
			[AreaName] [varchar](20) NOT NULL,
			[LocationId] [smallint] NULL,
			[AreaSorder] [int] NULL,
			[ShowInApp] [bit] NULL,
			[IsActive] [bit] NULL,
			[IsForSchemeBlock] [bit] NULL,
		 CONSTRAINT [PK_PA_AREA] PRIMARY KEY CLUSTERED 
		(
			[AreaID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY]



		ALTER TABLE [dbo].[PA_AREA] ADD  CONSTRAINT [DF__PA_AREA__IsActiv__3911995D]  DEFAULT ((1)) FOR [ShowInApp]


		ALTER TABLE [dbo].[PA_AREA] ADD  CONSTRAINT [DF__PA_AREA__IsActiv__50CC54B9]  DEFAULT ((1)) FOR [IsActive]


		ALTER TABLE [dbo].[PA_AREA]  WITH NOCHECK ADD  CONSTRAINT [FK_PA_AREA_PA_LOCATION] FOREIGN KEY([LocationId])
		REFERENCES [dbo].[PA_LOCATION] ([LocationID])
		ON UPDATE CASCADE
		ON DELETE CASCADE


		ALTER TABLE [dbo].[PA_AREA] CHECK CONSTRAINT [FK_PA_AREA_PA_LOCATION]

END


	------------------------------------  Upadte values FOR TICKET# 10832 ----------------------------------------

	IF  EXISTS ( Select 1 from pa_area WHERE  AreaName = 'Services' )
	BEGIN 	
		UPDATE pa_area SET areaSorder = 7, locationid = 3, isForSchemeBlock =1
		WHERE AreaName = 'Services'
	END

	IF NOT EXISTS (SELECT 1 FROM pa_area WHERE  AreaName = 'Electrics')
	BEGIN
		INSERT INTO pa_area VALUES ('Electrics',NULL,8,1,0,1)
	END

	IF NOT EXISTS (SELECT 1 FROM pa_area WHERE  AreaName = 'Meters')
	BEGIN
		INSERT INTO pa_area VALUES ('Meters',NULL,9,1,0,1)
	END

	------------------------------------ END TICKET# 10832 ----------------------------------------

	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH

