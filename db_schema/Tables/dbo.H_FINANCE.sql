CREATE TABLE [dbo].[H_FINANCE]
(
[F_ID] [int] NOT NULL IDENTITY(1, 1),
[Quoted] [money] NULL,
[Actual_Dev_Cost] [money] NULL,
[Invoice_Amount] [money] NULL,
[Authorised] [int] NULL,
[Invoiced] [int] NULL,
[Hourly_Rate] [money] NULL,
[Invoice_No] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvDate] [datetime] NULL,
[JOB_ID] [int] NULL,
[NotApplicable] [int] NULL
) ON [PRIMARY]
GO
