USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_DOI_HISTORY')
BEGIN


CREATE TABLE [dbo].[E_DOI_HISTORY](
	[InterestHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[InterestId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[EmpName1] [nvarchar](100) NULL,
	[EmpRole1] [nvarchar](100) NULL,
	[EmpRel1] [int] NULL,
	[EmpName2] [nvarchar](100) NULL,
	[EmpRole2] [nvarchar](100) NULL,
	[EmpRel2] [int] NULL,
	[RoleName] [nvarchar](100) NULL,
	[RoleRole] [nvarchar](100) NULL,
	[RoleRel] [int] NULL,
	[ResidingAddress1] [nvarchar](500) NULL,
	[ResidingAddress2] [nvarchar](500) NULL,
	[ResidingPostcode] [nvarchar](50) NULL,
	[HomeName1] [nvarchar](100) NULL,
	[HomeAddress1] [nvarchar](500) NULL,
	[HomeRel1] [int] NULL,
	[HomeName2] [nvarchar](100) NULL,
	[HomeAddress2] [nvarchar](500) NULL,
	[HomeRel2] [int] NULL,
	[TypeOfEmployment] [nvarchar](100) NULL,
	[Employer] [nvarchar](100) NULL,
	[IsSecondaryEmployment] [bit] NULL,
	[ConOrganizationP6] [nvarchar](100) NULL,
	[ConService] [nvarchar](100) NULL,
	[IsConApprovedByLeadership] [bit] NULL,
	[LocalAuthName1] [nvarchar](100) NULL,
	[LocalAuthOrganization1] [nvarchar](100) NULL,
	[LocalAuthRel1] [int] NULL,
	[LocalAuthName2] [nvarchar](100) NULL,
	[LocalAuthOrganization2] [nvarchar](100) NULL,
	[LocalAuthRel2] [int] NULL,
	[IsDirectorP8] [bit] NULL,
	[ConOrganizationP8] [nvarchar](100) NULL,
	[ConIndvidual] [nvarchar](100) NULL,
	[ConRelP8] [int] NULL,
	[IsDirectorP9] [bit] NULL,
	[ConOrganizationP9] [nvarchar](100) NULL,
	[Trustee1] [nvarchar](100) NULL,
	[Trustee2] [nvarchar](100) NULL,
	[Trustee3] [nvarchar](100) NULL,
	[CountyTown] [nvarchar](100) NULL,
	[Appointments] [nvarchar](max) NULL,
	[BeneficialInterest] [nvarchar](max) NULL,
	[IsInterested] [bit] NULL,
	[OtherInterest] [nvarchar](max) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [DateTime] NOT NULL,
	[IsActive] [bit] NULL,FiscalYear int DEFAULT NULL,
	[DocumentId] int DEFAULT NULL,
	[Status] int DEFAULT 0,
	[UpdatedBy] int NULL,
	[UpdatedOn] [DateTime] NULL,
	[Comment] [nvarchar](500) NULL
 CONSTRAINT [PK_InterestHistory] PRIMARY KEY NONCLUSTERED 
(
	[InterestHistoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]


PRINT 'Table Created'

END

ELSE
BEGIN
PRINT 'Table already exist..'


IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_DOI_HISTORY]' ) AND name = 'UpdatedBy')
	BEGIN
		ALTER TABLE E_DOI_HISTORY ADD UpdatedBy int NULL
	PRINT 'UpdatedBy added successfully!'
	END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_DOI_HISTORY]' ) AND name = 'UpdatedOn')
	BEGIN
		ALTER TABLE E_DOI_HISTORY ADD UpdatedOn [DateTime] NULL
	PRINT 'UpdatedOn added successfully!'
	END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_DOI_HISTORY]' ) AND name = 'Comment')
	BEGIN
		ALTER TABLE E_DOI_HISTORY ADD Comment NVARCHAR(500) NULL
	PRINT 'Comment added successfully!'
	END

END


--============================================================
-- US591 - Added LineManagerReviewDate and LineManagerComments
--============================================================

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_DOI_HISTORY]' ) AND name = 'LineManagerReviewDate')
	BEGIN
		ALTER TABLE E_DOI_HISTORY ADD LineManagerReviewDate [DateTime] NULL
	PRINT 'Line Manager Review Date added successfully!'
	END
	

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[E_DOI_HISTORY]' ) AND name = 'LineManagerComments')
	BEGIN
		ALTER TABLE E_DOI_HISTORY ADD LineManagerComments NVARCHAR(500) NULL
	PRINT 'Line Manager Review Date added successfully!'
	
END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

