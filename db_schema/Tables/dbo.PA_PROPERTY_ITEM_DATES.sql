Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PA_PROPERTY_ITEM_DATES')
BEGIN

CREATE TABLE [dbo].[PA_PROPERTY_ITEM_DATES](
	[SID] [int] IDENTITY(1,1) NOT NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[ItemId] [int] NULL,
	[LastDone] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[Frequency] [nvarchar](30) NULL,
	[UPDATEDON] [smalldatetime] NULL,
	[UPDATEDBY] [int] NULL,
	[ParameterId] [int] NULL,
	[PLANNED_COMPONENTID] [smallint] NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[isVoidAppointmentAssociated] [bit] NULL,
 CONSTRAINT [PK_PA_PROPERTY_ITEM_DATES] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[PA_PROPERTY_ITEM_DATES] ADD  CONSTRAINT [DF__PA_PROPER__isVoi__14184C34]  DEFAULT (NULL) FOR [isVoidAppointmentAssociated]


ALTER TABLE [dbo].[PA_PROPERTY_ITEM_DATES]  WITH NOCHECK ADD  CONSTRAINT [FK_PA_PROPERTY_ITEM_DATES_PA_ITEM] FOREIGN KEY([ItemId])
REFERENCES [dbo].[PA_ITEM] ([ItemID])


ALTER TABLE [dbo].[PA_PROPERTY_ITEM_DATES] CHECK CONSTRAINT [FK_PA_PROPERTY_ITEM_DATES_PA_ITEM]


END


--===================================================================================================================================
-- ID79 - Updated Invalid DueDates associated with components
--===================================================================================================================================


--===================================================================================================================================
-- 1- CONSUMER UNIT
--		a. Update 'LastDone' based on cycle of component for those entries which have 'DueDates' existed LastDone = (DueDate - Cycle)
--		b. Update 'DueDate' 	
--===================================================================================================================================
IF EXISTS (	SELECT	1
			FROM	PA_PROPERTY_ITEM_DATES PPID
					INNER JOIN PA_PARAMETER PP ON PPID.PARAMETERID = PP.PARAMETERID
					INNER JOIN PA_ITEM PII ON PPID.ITEMID = PII.ITEMID
			WHERE	ITEMNAME = 'Electrics' 
					AND isforschemeblock = 0
					AND parametername = 'Cur Due')
		BEGIN

			IF EXISTS (	SELECT	1 
						FROM	PA_PARAMETER 
						WHERE	ParameterName='CUR Last Replaced')
				BEGIN 
						
						DECLARE @CurLastReplacedParameterId INT
						SELECT	@CurLastReplacedParameterId = parameterid 
						FROM	PA_PARAMETER 
						WHERE	ParameterName='CUR Last Replaced'

						UPDATE	PA_PROPERTY_ITEM_DATES
						SET		PA_PROPERTY_ITEM_DATES.LASTDONE = CASE 
																	WHEN PA_PROPERTY_ITEM_DATES.LASTDONE IS NULL AND PA_PROPERTY_ITEM_DATES.DUEDATE IS NOT NULL THEN
																			CASE 
																				WHEN PC.frequency = 'yrs'THEN Dateadd(year, -cycle, PA_PROPERTY_ITEM_DATES.DUEDATE) 
																				WHEN PC.frequency = 'mnths' THEN Dateadd(month, -cycle, PA_PROPERTY_ITEM_DATES.DUEDATE) 
																			END
																	ELSE
																			PA_PROPERTY_ITEM_DATES.LASTDONE
																	END,
								PA_PROPERTY_ITEM_DATES.DUEDATE = CASE 
																	WHEN PA_PROPERTY_ITEM_DATES.LASTDONE IS NOT NULL THEN
																			CASE 
																				WHEN PC.frequency = 'yrs'THEN Dateadd(year, cycle, PA_PROPERTY_ITEM_DATES.LASTDONE) 
																				WHEN PC.frequency = 'mnths' THEN Dateadd(month, cycle, PA_PROPERTY_ITEM_DATES.LASTDONE) 
																			END
																	ELSE
																			PA_PROPERTY_ITEM_DATES.DUEDATE
																	END,
								PA_PROPERTY_ITEM_DATES.PARAMETERID = @CurLastReplacedParameterId
														

															 
						FROM	PA_PROPERTY_ITEM_DATES
								INNER JOIN PLANNED_COMPONENT PC ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PC.COMPONENTID
								INNER JOIN PA_PARAMETER PP ON PA_PROPERTY_ITEM_DATES.PARAMETERID = PP.PARAMETERID
								INNER JOIN PA_ITEM PII ON PA_PROPERTY_ITEM_DATES.ITEMID = PII.ITEMID
						WHERE	ITEMNAME = 'Electrics' 
								AND isforschemeblock = 0
								AND parametername = 'Cur Due'	
		

						PRINT 'Cur Due replaced successfully'

					END
					ELSE
					BEGIN
						PRINT 'CUR Last Replaced does not exist in PA_PATAMETER table, please execute dbo.PA_PATAMETER.sql first.'
					END
		END

--===================================================================================================================================
-- 2- Rewire
--		a. Correct 'ComponentId'  
--		b. Update 'LastDone' based on cycle of component for those entries which have 'DueDates' existed LastDone = (DueDate - Cycle)
--===================================================================================================================================

IF EXISTS (	SELECT	1
			FROM	PA_PROPERTY_ITEM_DATES PPID
					INNER JOIN PLANNED_COMPONENT PC ON PPID.PLANNED_COMPONENTID = PC.COMPONENTID
					INNER JOIN PA_PARAMETER PP ON PPID.PARAMETERID = PP.PARAMETERID
					INNER JOIN PA_ITEM PII ON PPID.ITEMID = PII.ITEMID
			WHERE	ITEMNAME = 'Electrics' 
					AND isforschemeblock = 0
					AND parametername = 'Last Rewired'
					AND componentname <> 'Rewire')
		BEGIN

		--============================
		-- a. Correct 'ComponentId'
		--============================
		DECLARE @REWIRECOMPONENTID INT
		SELECT	@REWIRECOMPONENTID = COMPONENTID
		FROM	PLANNED_COMPONENT
		WHERE	COMPONENTNAME = 'Rewire'

		UPDATE	PA_PROPERTY_ITEM_DATES
		SET		PLANNED_COMPONENTID = @REWIRECOMPONENTID				
		FROM	PA_PROPERTY_ITEM_DATES
				INNER JOIN PLANNED_COMPONENT PC ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PC.COMPONENTID
				INNER JOIN PA_PARAMETER PP ON PA_PROPERTY_ITEM_DATES.PARAMETERID = PP.PARAMETERID
				INNER JOIN PA_ITEM PII ON PA_PROPERTY_ITEM_DATES.ITEMID = PII.ITEMID
		WHERE	ITEMNAME = 'Electrics' 
				AND isforschemeblock = 0
				AND parametername = 'Last Rewired'
				AND componentname <> 'Rewire'

		--===========================
		-- b. Update 'LastDone'
		--===========================
		UPDATE	PA_PROPERTY_ITEM_DATES
		SET		PA_PROPERTY_ITEM_DATES.LASTDONE = CASE 
														WHEN PC.frequency = 'yrs'THEN Dateadd(year, -cycle, PA_PROPERTY_ITEM_DATES.DUEDATE) 
														WHEN PC.frequency = 'mnths' THEN Dateadd(month, -cycle, PA_PROPERTY_ITEM_DATES.DUEDATE) 
										     	  END
		FROM	PA_PROPERTY_ITEM_DATES
				INNER JOIN PLANNED_COMPONENT PC ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PC.COMPONENTID
				INNER JOIN PA_PARAMETER PP ON PA_PROPERTY_ITEM_DATES.PARAMETERID = PP.PARAMETERID
				INNER JOIN PA_ITEM PII ON PA_PROPERTY_ITEM_DATES.ITEMID = PII.ITEMID
		WHERE	ITEMNAME = 'Electrics' 
				AND isforschemeblock = 0
				AND parametername = 'Last Rewired'
				AND componentname = 'Rewire'
				AND PA_PROPERTY_ITEM_DATES.LASTDONE IS NULL 
				AND PA_PROPERTY_ITEM_DATES.DUEDATE IS NOT NULL

		END

--===================================================================================================================================
-- 3- Electrical Upgrade
--		a. Correct 'ComponentId'  
--		b. Update 'LastDone' based on cycle of component for those entries which have 'DueDates' existed LastDone = (DueDate - Cycle)
--===================================================================================================================================

IF EXISTS (	SELECT	1
			FROM	PA_PROPERTY_ITEM_DATES PPID
					INNER JOIN PLANNED_COMPONENT PC ON PPID.PLANNED_COMPONENTID = PC.COMPONENTID
					INNER JOIN PA_PARAMETER PP ON PPID.PARAMETERID = PP.PARAMETERID
					INNER JOIN PA_ITEM PII ON PPID.ITEMID = PII.ITEMID
			WHERE	ITEMNAME = 'Electrics' 
					AND isforschemeblock = 0
					AND parametername = 'Electrical Upgrade Last Done'
					AND componentname <> 'Electrical Upgrade')
		BEGIN

		--============================
		-- a. Correct 'ComponentId'
		--============================
		DECLARE @ELECTRICALUPGRADECOMPONENTID INT
		SELECT	@ELECTRICALUPGRADECOMPONENTID = COMPONENTID
		FROM	PLANNED_COMPONENT
		WHERE	COMPONENTNAME = 'Electrical Upgrade'

		UPDATE	PA_PROPERTY_ITEM_DATES
		SET		PLANNED_COMPONENTID = @ELECTRICALUPGRADECOMPONENTID
		FROM	PA_PROPERTY_ITEM_DATES
				INNER JOIN PLANNED_COMPONENT PC ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PC.COMPONENTID
				INNER JOIN PA_PARAMETER PP ON PA_PROPERTY_ITEM_DATES.PARAMETERID = PP.PARAMETERID
				INNER JOIN PA_ITEM PII ON PA_PROPERTY_ITEM_DATES.ITEMID = PII.ITEMID
		WHERE	ITEMNAME = 'Electrics' 
				AND isforschemeblock = 0
				AND parametername = 'Electrical Upgrade Last Done'
				AND componentname <> 'Electrical Upgrade'

		--===========================
		-- b. Update 'LastDone'
		--===========================

		UPDATE	PA_PROPERTY_ITEM_DATES
		SET		PA_PROPERTY_ITEM_DATES.LASTDONE = CASE 
														WHEN PC.frequency = 'yrs'THEN Dateadd(year, -cycle, PA_PROPERTY_ITEM_DATES.DUEDATE) 
														WHEN PC.frequency = 'mnths' THEN Dateadd(month, -cycle, PA_PROPERTY_ITEM_DATES.DUEDATE) 
										     	  END
		FROM	PA_PROPERTY_ITEM_DATES
				INNER JOIN PLANNED_COMPONENT PC ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PC.COMPONENTID
				INNER JOIN PA_PARAMETER PP ON PA_PROPERTY_ITEM_DATES.PARAMETERID = PP.PARAMETERID
				INNER JOIN PA_ITEM PII ON PA_PROPERTY_ITEM_DATES.ITEMID = PII.ITEMID
		WHERE	ITEMNAME = 'Electrics' 
				AND isforschemeblock = 0
				AND parametername = 'Electrical Upgrade Last Done'
				AND componentname = 'Electrical Upgrade'
				AND PA_PROPERTY_ITEM_DATES.LASTDONE IS NULL 
				AND PA_PROPERTY_ITEM_DATES.DUEDATE IS NOT NULL

		END



CREATE TABLE #TMP_FILTEREDDUEDATES
		(TMP_SID INT, TMP_CURRENTDUEDATE DATETIME, TMP_ACTUALDUEDATE DATETIME)

INSERT	INTO #TMP_FILTEREDDUEDATES (TMP_SID, TMP_CURRENTDUEDATE,TMP_ACTUALDUEDATE) 
SELECT SID,CurrentDueDate, ActualDueDate
FROM   (SELECT	SID,				
               duedate           AS CurrentDueDate, 
               CASE 
                 WHEN pc.frequency = 'yrs'THEN Dateadd(year, cycle, lastdone) 
                 WHEN pc.frequency = 'mnths' THEN Dateadd(month, cycle, lastdone) 
               END               AS ActualDueDate

        FROM   PA_PROPERTY_ITEM_DATES PPID 
               INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PPID.planned_componentid = PCI.componentid 
               INNER JOIN PA_ITEM PII ON PPID.itemid = PII.itemid 
               INNER JOIN PLANNED_COMPONENT PC ON PPID.planned_componentid = PC.componentid 
        WHERE  lastdone IS NOT NULL AND ComponentName <> 'Consumer Unit') AS filteredDueDates 
WHERE  filteredDueDates.currentduedate <> filteredDueDates.actualduedate 



IF EXISTS (SELECT * FROM #TMP_FILTEREDDUEDATES)
BEGIN
	
	UPDATE	PA_PROPERTY_ITEM_DATES
	SET		PA_PROPERTY_ITEM_DATES.DueDate = #TMP_FILTEREDDUEDATES.TMP_ACTUALDUEDATE
	FROM	PA_PROPERTY_ITEM_DATES
			INNER JOIN #TMP_FILTEREDDUEDATES ON PA_PROPERTY_ITEM_DATES.SID = #TMP_FILTEREDDUEDATES.TMP_SID

	print  'Invalid Due Dates updated successfully'
END


DROP TABLE #TMP_FILTEREDDUEDATES


--===================================================================================================================================
-- BUG - 670
-- Fixed invalid entries which contains invalid planned component ids
--===================================================================================================================================

IF EXISTS (	SELECT	1
			FROM	PA_PROPERTY_ITEM_DATES PPID
					INNER JOIN PLANNED_COMPONENT_ITEM PCI ON PPID.PLANNED_COMPONENTID = PCI.COMPONENTID
					INNER JOIN PA_ITEM PII ON PPID.ITEMID = PII.ITEMID
					INNER JOIN PLANNED_COMPONENT pc ON ppid.PLANNED_COMPONENTID = pc.COMPONENTID
			WHERE	PPID.ITEMID <> PCI.ITEMID and PPID.ITEMID NOT IN (SELECT DISTINCT ItemId FROM PLANNED_COMPONENT_ITEM))
		BEGIN
			
			UPDATE  PA_PROPERTY_ITEM_DATES
			SET		PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = NULL
			FROM	PA_PROPERTY_ITEM_DATES 
					INNER JOIN PLANNED_COMPONENT_ITEM PCI ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PCI.COMPONENTID
					INNER JOIN PA_ITEM PII ON PA_PROPERTY_ITEM_DATES.ITEMID = PII.ITEMID
					INNER JOIN PLANNED_COMPONENT pc ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = pc.COMPONENTID
			WHERE	PA_PROPERTY_ITEM_DATES.ITEMID <> PCI.ITEMID and PA_PROPERTY_ITEM_DATES.ITEMID NOT IN (SELECT DISTINCT ItemId FROM PLANNED_COMPONENT_ITEM)

			PRINT 'Invalid entries corrected related to invalid compnent ids'

		END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH



