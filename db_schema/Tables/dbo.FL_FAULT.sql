ALTER TABLE FL_FAULT ADD TenantsOnline BIT

CREATE TABLE [dbo].[FL_FAULT]
(
[FaultID] [int] NOT NULL IDENTITY(1, 1),
[ElementID] [int] NULL,
[PriorityID] [int] NULL,
[Description] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetCost] [float] NULL,
[Vat] [float] NULL,
[Gross] [float] NULL,
[VatRateID] [int] NULL,
[EffectFrom] [datetime] NULL,
[Recharge] [bit] NULL,
[PreInspection] [bit] NULL CONSTRAINT [DF_preInspection_zero] DEFAULT ((0)),
[PostInspection] [bit] NULL,
[StockConditionItem] [bit] NULL,
[FaultActive] [bit] NULL,
[FaultAction] [bit] NULL,
[SubmitDate] [datetime] NULL,
[isInflative] [bit] NOT NULL CONSTRAINT [DF_FL_FAULT_isInflative] DEFAULT ((0)),
[Accepted] [bit] NULL,
[AREAID] [int] NULL,
[isContractor] [bit] NULL,
[duration] [float] NULL,
[IsGasSafe] [smallint] NULL,
[IsOftec] [smallint] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT] ADD 
CONSTRAINT [PK_FL_FAULT] PRIMARY KEY CLUSTERED  ([FaultID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT] ADD
CONSTRAINT [FK_FL_FAULT_FL_FAULT_PRIORITY] FOREIGN KEY ([PriorityID]) REFERENCES [dbo].[FL_FAULT_PRIORITY] ([PriorityID])
ALTER TABLE [dbo].[FL_FAULT] ADD
CONSTRAINT [FK_FL_FAULT_FL_ELEMENT] FOREIGN KEY ([ElementID]) REFERENCES [dbo].[FL_ELEMENT] ([ElementID])
GO

--scripts

update AC_PAGES set MENUID = 57 --51
where PAGEID in (	338,
					354,355,356,357)

--update AC_PAGES set ACTIVE  = 0 where pageid = 486

update AC_PAGES set AccessLevel = 1, --2
ParentPage  = null, ORDERTEXT = 6 --287
where PAGEID in (338)

update AC_PAGES set page = '~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=fm&mn=Admin' ,BridgeActualPage = '~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=fm&mn=Admin'
where pageid = 338

update AC_PAGES set  page = '~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=rdl&mn=Admin' ,BridgeActualPage = '~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=rdl&mn=Admin'
where pageid = 356

update AC_PAGES set  page = '~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=fm&mn=Admin' ,BridgeActualPage = '~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=fm&mn=Admin'
where pageid = 354

update AC_PAGES set  AccessLevel = 2, ParentPage = 338 where PAGEID in (354,355,356,357)
