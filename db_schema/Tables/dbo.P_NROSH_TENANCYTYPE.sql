CREATE TABLE [dbo].[P_NROSH_TENANCYTYPE]
(
[Sid] [int] NOT NULL,
[Description] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nrosh_Code] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenancyType] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_NROSH_TENANCYTYPE] TO [rackspace_datareader]
GO
