/****** Script for SelectTopNRows command from SSMS  ******/

USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY

	-------------------------------- CREATING TABLE IF NOT EXISTS ------------------------------------------
	--drop table [PLANNED_CANCEL_APPOINTMENTS]
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'PLANNED_CANCEL_APPOINTMENTS')
	BEGIN
	

		CREATE TABLE [dbo].[PLANNED_CANCEL_APPOINTMENTS]
		(
			[RejectPlannedWorkId] [int] NOT NULL IDENTITY(1, 1),
			[PlannedWorkPmo] [int] NOT NULL,
			[RejectId] [int] NOT NULL,
			[RejectNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
			[CreatedBy] int ,
			[CreatedDate] [datetime2] NULL
		) ON [PRIMARY]

	END

	
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH