-- update daystiming field 

update E_WorkingHours set DaysTimings = 
'{"day":"mon","StartTime":"09:00","EndTime":"17:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":"7.5"}_{"day":"tue","StartTime":"09:00","EndTime":"17:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":"7.5"}_{"day":"wed","StartTime":"09:00","EndTime":"17:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":"7.5"}_{"day":"thu","StartTime":"09:00","EndTime":"17:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":"7.5"}_{"day":"fri","StartTime":"09:00","EndTime":"17:00","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":"7"}_{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}_{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
where DaysTimings is null and Mon = 7.5 and Tue = 7.5 and Wed = 7.5 and Thu = 7.5 and Fri = 7 and Sat = 0 and Sun = 0


DECLARE @date DATETIME
SET @date='2010-10-01'
SET @date=@date+'09:00:00'

update E_WorkingHours set DaysTimings = 
	case WHEN Mon = 0 then '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Mon * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Mon) +'}' end +'_'+
	
	case WHEN Tue = 0 then '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Tue * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Tue) +'}' end +'_'+
	
	case WHEN Wed = 0 then '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Wed * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Wed) +'}' end +'_'+
	
	case WHEN Thu = 0 then '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Thu * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Thu) +'}' end +'_'+
	
	case WHEN Fri = 0 then '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Fri * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Fri) +'}' end +'_'+
	
	case WHEN Sat = 0 then '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Sat * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Sat) +'}' end +'_'+
	
	case WHEN Sun = 0 then '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}'
		else '{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, Sun * 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":'+convert(varchar, Sun) +'}' end 

where DaysTimings is null

-- update DaysTimings where DaysTimings is not null but some combination are not correct or incomplete 

update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Wed * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Thu * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Fri * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId in (select EmployeeId from E_WorkingHours where  CHARINDEX('__', DaysTimings, 1)<>0 
	and EmployeeId not in (2266, 1341, 1181, 994, 830, 820, 531, 508))


-- update DaysTimings where DaysTimings is not null but some combination are not correct or incomplete for specific employees (2266, 1341, 1181, 994, 830, 820, 531, 508)

update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Wed * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"08:30","EndTime":"17:00","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Fri * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 508


update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"08:30","EndTime":"17:00","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Thu * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Fri * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 531


update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Wed * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Thu * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"08:00","EndTime":"16:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"30","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 820


update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"08:00","EndTime":"16:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"30","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"08:00","EndTime":"16:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 830

update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Wed * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"08:00","EndTime":"16:00","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"30","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Fri * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 994

update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Wed * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Thu * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Fri * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 1181

update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Wed * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Thu * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"08:30","EndTime":"17:30","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"45","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 1341

update E_WorkingHours set DaysTimings = 
case WHEN (CHARINDEX('mon', DaysTimings, 1) = 0 AND Mon <> 0) then 
		'{"day":"mon","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Mon * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Mon) +'}'
	WHEN (CHARINDEX('mon', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%mon%')
	else '{"day":"mon","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('tue', DaysTimings, 1) = 0 AND Tue <> 0) then 
		'{"day":"tue","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Tue * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Tue) +'}'
	WHEN (CHARINDEX('tue', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%tue%')
	else '{"day":"tue","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('wed', DaysTimings, 1) = 0 AND Wed <> 0) then 
		'{"day":"wed","StartTime":"08:00","EndTime":"16:00","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"30","duration":'+convert(varchar, Wed) +'}'
	WHEN (CHARINDEX('wed', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%wed%')
	else '{"day":"wed","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('thu', DaysTimings, 1) = 0 AND Thu <> 0) then 
		'{"day":"thu","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Thu * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Thu) +'}'
	WHEN (CHARINDEX('thu', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%thu%')
	else '{"day":"thu","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('fri', DaysTimings, 1) = 0 AND Fri <> 0) then 
		'{"day":"fri","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Fri * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"45","duration":'+convert(varchar, Fri) +'}'
	WHEN (CHARINDEX('fri', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%fri%')
	else '{"day":"fri","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sat', DaysTimings, 1) = 0 AND Sat <> 0) then 
		'{"day":"sat","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sat * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sat) +'}'
	WHEN (CHARINDEX('sat', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sat%')
	else '{"day":"sat","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end +'_'+
	
case WHEN (CHARINDEX('sun', DaysTimings, 1) = 0 AND Sun <> 0) then 
		'{"day":"sun","StartTime":"09.00","EndTime":"'+ convert(VARCHAR(5), DATEADD(MINUTE, (Sun * 60) + 60, @date), 108)+'","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"60","duration":'+convert(varchar, Sun) +'}'
	WHEN (CHARINDEX('sun', DaysTimings, 1) <> 0 ) then 
		(select * from dbo.fnSplitString(DaysTimings, '_') where splitdata like '%sun%')
	else '{"day":"sun","StartTime":"","EndTime":"","LunchStartTime":"","LunchEndTime":"","LunchTimeInMinutes":"","duration":0}' end
 
 where EmployeeId = 2266