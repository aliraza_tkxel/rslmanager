Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AC_MENUS')
BEGIN

	CREATE TABLE [dbo].[AC_MENUS](
		[MENUID] [INT] IDENTITY(1,1) NOT NULL,
		[DESCRIPTION] [NVARCHAR](100) NULL,
		[MODULEID] [INT] NULL,
		[ACTIVE] [INT] NULL,
		[PAGE] [NVARCHAR](300) NULL,
		[MENUWIDTH] [INT] NULL,
		[SUBMENUWIDTH] [INT] NULL,
		[ORDERTEXT] [NVARCHAR](50) NULL,
	 CONSTRAINT [PK_A_MENU] PRIMARY KEY NONCLUSTERED 
	(
		[MENUID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 30) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[AC_MENUS]  WITH NOCHECK ADD  CONSTRAINT [FK_AC_MENUS_AC_MODULES] FOREIGN KEY([MODULEID])
	REFERENCES [dbo].[AC_MODULES] ([MODULEID])
	ALTER TABLE [dbo].[AC_MENUS] NOCHECK CONSTRAINT [FK_AC_MENUS_AC_MODULES]
	ALTER TABLE [dbo].[AC_MENUS] ADD  CONSTRAINT [DF_AC_MENUS_MENUWIDTH]  DEFAULT (100) FOR [MENUWIDTH]
	ALTER TABLE [dbo].[AC_MENUS] ADD  CONSTRAINT [DF_AC_MENUS_SUBMENUWIDTH]  DEFAULT (100) FOR [SUBMENUWIDTH]

END




IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AC_MENUS')
BEGIN

	--==============================================================================
	-- Ticket # 10593 (Enable Maintenance menu item) 
	--==============================================================================	
	UPDATE dbo.AC_MENUS
	SET PAGE = '~/../PropertyDataRestructure/Bridge.aspx?mn=Maintenance'
	WHERE DESCRIPTION = 'Maintenance'
END


--IF EXISTS (Select 1 from AC_MODULES where DESCRIPTION='HR')
--	BEGIN
--	DECLARE @moduleId int
--		Select @moduleId=MODULEID from AC_MODULES where DESCRIPTION='HR'
--		IF NOT EXISTS (Select 1 from AC_MENUS where DESCRIPTION='Dashboard' and MODULEID=@moduleId)
--		BEGIN
--			INSERT INTO AC_MENUS(DESCRIPTION,MODULEID,ACTIVE,PAGE,ORDERTEXT)
--			VALUES('Dashboard',@moduleId,1,'/RSLHRModuleWeb/Dashboard','1')
--		END
	
--		IF NOT EXISTS (Select 1 from AC_MENUS where DESCRIPTION='Employees' and MODULEID=@moduleId)
--		BEGIN
--			INSERT INTO AC_MENUS(DESCRIPTION,MODULEID,ACTIVE,PAGE,ORDERTEXT)
--			VALUES('Employees',@moduleId,1,'/RSLHRModuleWeb/Employees/EmployeesList','2')
--		END
		
--		IF NOT EXISTS (Select 1 from AC_MENUS where DESCRIPTION='Reports' and MODULEID=@moduleId)
--		BEGIN
--			INSERT INTO AC_MENUS(DESCRIPTION,MODULEID,ACTIVE,PAGE,ORDERTEXT)
--			VALUES('Reports',@moduleId,1,'/RSLHRModuleWeb/Reports/','3')
--		END
		
--		IF NOT EXISTS (Select 1 from AC_MENUS where DESCRIPTION='Admin' and MODULEID=@moduleId)
--		BEGIN
--			INSERT INTO AC_MENUS(DESCRIPTION,MODULEID,ACTIVE,PAGE,ORDERTEXT)
--			VALUES('Admin',@moduleId,1,'','4')
--		END
--	END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
