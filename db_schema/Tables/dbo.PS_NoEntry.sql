CREATE TABLE [dbo].[PS_NoEntry]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PropertyID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AppointmentID] [int] NOT NULL,
[isCardLeft] [bit] NOT NULL,
[isGasAvailable] [bit] NOT NULL,
[isPropertyAccessible] [bit] NOT NULL,
[Date] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_NoEntry] ADD 
CONSTRAINT [PK_PS_NoEntry] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
