CREATE TABLE [dbo].[Query]
(
[UPRN] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Town] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Postcode] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SAP] [smallint] NULL
) ON [PRIMARY]
GO
