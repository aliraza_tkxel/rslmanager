USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT
	*
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FLS_HouseType') BEGIN

CREATE TABLE [dbo].[FLS_HouseType]([HouseTypeId] [int] IDENTITY (1, 1) NOT NULL,
[Title] [nvarchar](500) NOT NULL,
CONSTRAINT [PK_FLS_HouseType] PRIMARY KEY CLUSTERED
(
[HouseTypeId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'House') BEGIN
INSERT [dbo].[FLS_HouseType] ([Title])
	VALUES ( N'House')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Flat') BEGIN

INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'Flat')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Maisonette') BEGIN
INSERT [dbo].[FLS_HouseType] ([Title])
	VALUES ( N'Maisonette')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Hostel') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES (N'Hostel')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Bedsit') BEGIN
INSERT [dbo].[FLS_HouseType] ([Title])
	VALUES ( N'Bedsit')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Mobile home') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'Mobile home')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Armed forces') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'Armed forces')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Bungalow') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'Bungalow')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Hospital') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'Hospital')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'No fixed above') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'No fixed above')
END
IF NOT EXISTS (SELECT
	1
FROM FLS_HouseType
WHERE Title = 'Other') BEGIN
INSERT [dbo].[FLS_HouseType] ( [Title])
	VALUES ( N'Other')
END


IF @@TRANCOUNT > 0 BEGIN
COMMIT TRANSACTION;
PRINT ('Commit')
END

END TRY BEGIN CATCH

IF @@TRANCOUNT > 0 BEGIN

ROLLBACK TRANSACTION;

END
DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage,
@ErrorSeverity,
@ErrorState
);
PRINT (@ErrorMessage)

END CATCH