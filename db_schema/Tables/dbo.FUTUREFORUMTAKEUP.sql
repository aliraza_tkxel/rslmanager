CREATE TABLE [dbo].[FUTUREFORUMTAKEUP]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[IAGmanagerID] [int] NULL,
[WUserID] [int] NULL,
[SIGNUPDATESTAMP] [smalldatetime] NULL CONSTRAINT [DF_FUTUREFORUMTAKEUP_SIGNUPDATESTAMP] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FUTUREFORUMTAKEUP] ADD CONSTRAINT [PK_FUTUREFORUMTAKEUP] PRIMARY KEY CLUSTERED  ([ID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
