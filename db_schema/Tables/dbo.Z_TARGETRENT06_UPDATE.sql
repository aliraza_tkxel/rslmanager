CREATE TABLE [dbo].[Z_TARGETRENT06_UPDATE]
(
[PropertyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rent] [money] NULL,
[SupportedServiceS] [money] NULL,
[Ineligible] [money] NULL,
[ServiceS] [money] NULL,
[CouncilTax] [money] NULL,
[Water] [money] NULL
) ON [PRIMARY]
GO
