USE [RSLBHALive]

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AC_PAGES')
							
BEGIN
	
	CREATE TABLE [dbo].[AC_PAGES](
		[PAGEID] [int] IDENTITY(1,1) NOT NULL,
		[DESCRIPTION] [nvarchar](100) NULL,
		[MENUID] [int] NULL,
		[ACTIVE] [int] NULL,
		[LINK] [int] NULL,
		[PAGE] [nvarchar](300) NULL,
		[ORDERTEXT] [nvarchar](50) NULL,
		[AccessLevel] [int] NULL,
		[ParentPage] [int] NULL,
		[BridgeActualPage] [varchar](300) NULL,
		[DisplayInTree] [bit] NULL,
	 CONSTRAINT [PK_A_PAGE] PRIMARY KEY NONCLUSTERED 
	(
		[PAGEID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[AC_PAGES] ADD  CONSTRAINT [AccessLevel_Default]  DEFAULT ((1)) FOR [AccessLevel]
	ALTER TABLE [dbo].[AC_PAGES]  WITH NOCHECK ADD  CONSTRAINT [FK_A_PAGE_A_MENU] FOREIGN KEY([MENUID])
	REFERENCES [dbo].[AC_MENUS] ([MENUID])
	ALTER TABLE [dbo].[AC_PAGES] NOCHECK CONSTRAINT [FK_A_PAGE_A_MENU]
				
END   

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AC_PAGES')
          
BEGIN
	
	DECLARE 
	@moduleID INT = -1,
	@menuID INT = -1,
	@pageID INT = -1,
	@orderTEXT INT = -1	

	PRINT 'Table Exists';
	 
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Provisioning')
		BEGIN  
			Update AC_PAGES set PAGE='~/../PropertyDataRestructure/Views/Reports/ProvisioningReport.aspx' where DESCRIPTION='Provisioning'
		END
			
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Service Charge')
		BEGIN 	
			Declare @ParentPageId int,@SERVICING_MENUID int 
			Select @ParentPageId= PAGEID,@SERVICING_MENUID = AC_MENUS.MENUID from AC_PAGES
			INNER JOIN AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			WHERE AC_PAGES.DESCRIPTION='Reports' And AC_MENUS.DESCRIPTION = 'Servicing'
			INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
			VALUES	('Service Charge',@SERVICING_MENUID,1,0,'~/../PropertyDataRestructure/Bridge.aspx?pg=servicecharge','1',8,@ParentPageId,'~/../PropertyDataRestructure/Views/Reports/ServiceChargeReport.aspx',1)
		END
		
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Service Charge' AND LINK=0)
		BEGIN 	
		DECLARE	@id INT
		SELECT @id= PAGEID FROM AC_PAGES WHERE DESCRIPTION = 'Service Charge'
			UPDATE AC_PAGES SET LINK =1 WHERE PAGEID=@id
			PRINT 'Link updated'
		END
		
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Repair Database')
		BEGIN  
			Update AC_PAGES set PAGE='~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=rdl' ,
			BridgeActualPage = '~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=rdl'
			where DESCRIPTION='Repair Database'
		END 
		
		--===============================================================================
		--Start ASB module integration
		--===============================================================================
		
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='ASB Management')
		BEGIN  
		INSERT INTO AC_PAGES (DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage
		,BridgeActualPage,DisplayInTree)VALUES ('ASB Management',29,1,1,'../RSLASBModuleWeb/?UserId=',NULL,1,NULL,NULL,1)
		END 
		--===============================================================================
		--End ASB module integration
		--===============================================================================

		--===============================================================================
		--Start BHG Finance module integration
		--===============================================================================
		
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Cost Centre (ALL)')
		BEGIN  
			Update AC_PAGES set PAGE='../BHGFinanceModule/Bridge?UserId=' 
			where DESCRIPTION='Cost Centre (ALL)'
		END 
		--===============================================================================
		--End BHG Finance module integration
		--===============================================================================
		
	
		--===============================================================================
		--Ticket # 10348 Rename 'Assign to Contractor' to 'Assigned to Contractor' in Servicing > Reports
		--===============================================================================
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Assign to Contractor')
		BEGIN  
			Update AC_PAGES set DESCRIPTION='Assigned to Contractor' 
			where DESCRIPTION='Assign to Contractor'
		END
	
		
		--===============================================================================
		-- RENAME COMPLETED REPORT
		--===============================================================================
		IF NOT EXISTS(SELECT * FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'Job Sheet Status')
							
		BEGIN
			UPDATE	AC_PAGES
			SET		DESCRIPTION = 'Job Sheet Status'
			WHERE	DESCRIPTION = 'Completed Works'     
				
		END       


		--===============================================================================
		-- INSERT Job Sheet Status Detail page
		--===============================================================================

		DECLARE @PLANNED_MENUID INT
		DECLARE @PLANNED_JOBSHEETSTATUS_PAGEID INT 

		SELECT	@PLANNED_MENUID = MENUID
		FROM	AC_MENUS 
		WHERE	DESCRIPTION = 'Planned'

		SELECT	@PLANNED_JOBSHEETSTATUS_PAGEID = PAGEID
		FROM	AC_PAGES 
		WHERE	DESCRIPTION = 'Job Sheet Status'
		
		IF NOT EXISTS(	SELECT	* FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'View Job Sheet Status Detail')
						
		BEGIN	
			INSERT INTO [AC_PAGES]([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
			VALUES ('View Job Sheet Status Detail',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ViewArrangedAppointments.aspx?src=rptcmpt','8',3,@PLANNED_JOBSHEETSTATUS_PAGEID,'',0)
		END 

		--===============================================================================
		-- INSERT Cancelled Planned Report Page
		--===============================================================================

		IF NOT EXISTS(	SELECT	* FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'Cancelled Planned Works')
					
		BEGIN	
			DECLARE @PLANNED_REPORT_PAGEID INT
			SELECT	@PLANNED_REPORT_PAGEID = PAGEID
			FROM	AC_PAGES
			WHERE	DESCRIPTION = 'Reports' AND MENUID = @PLANNED_MENUID

			INSERT INTO [AC_PAGES]([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
			VALUES ('Cancelled Planned Works',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/AppointmentsCancelled.aspx','8',2,@PLANNED_REPORT_PAGEID,'',1)
		END 
		--===============================================================================
		-- INSERT Cancelled Job Sheet Status Detail page
		--===============================================================================

		IF NOT EXISTS(	SELECT	* FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'View Cancelled Job Sheet Status Detail')
					
		BEGIN	
			INSERT INTO [AC_PAGES]([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
			VALUES ('View Cancelled Job Sheet Status Detail',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ViewCancelledAppointments.aspx','9',3,@PLANNED_JOBSHEETSTATUS_PAGEID,'',0)
		END 

		--===============================================================================
		-- Assign to contractor report
		--===============================================================================
		
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Assigned to Contractor')
		BEGIN	
			Select @ParentPageId= PAGEID,@SERVICING_MENUID = AC_MENUS.MENUID from AC_PAGES
			INNER JOIN AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			WHERE AC_PAGES.DESCRIPTION='Reports' And AC_MENUS.DESCRIPTION = 'Servicing'

			INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],
			[ParentPage],[BridgeActualPage],[DisplayInTree])
			VALUES	
			('Assigned to Contractor',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=atcr','2',2,@ParentPageId,'../RSLApplianceServicing/Views/Reports/AssignedToContractor/AssignedToContractor.aspx',1)
		END	

		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Defect Approval')
		BEGIN	
			Select @ParentPageId= PAGEID,@SERVICING_MENUID = AC_MENUS.MENUID from AC_PAGES
			INNER JOIN AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			WHERE AC_PAGES.DESCRIPTION='Reports' And AC_MENUS.DESCRIPTION = 'Servicing'

			INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],
			[ParentPage],[BridgeActualPage],[DisplayInTree])
			VALUES	
			('Defect Approval',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=dapr','8',2,@ParentPageId,'../RSLApplianceServicing/Views/Reports/DefectApproval/DefectApproval.aspx',1)
			Print 'Added Defect Approvaql Report'
		END	
		--===============================================================================
		--Start Handling issue of duplication of Assigned to Contractor Report
		--===============================================================================
			DECLARE @ServicingMenuId INT 
			DECLARE @PagesId INT
			DECLARE @pId INT
			DECLARE @jobRoleTeamId INT
			Select @ServicingMenuId = AC_MENUS.MENUID from AC_PAGES
			INNER JOIN AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			WHERE AC_PAGES.DESCRIPTION='Reports' And AC_MENUS.DESCRIPTION = 'Servicing'
			IF ((SELECT COUNT(PAGEID) FROM AC_PAGES WHERE DESCRIPTION ='Assigned to Contractor' AND MENUID=@ServicingMenuId) >1)
			BEGIN
			
			SELECT  TOP(1) @PagesId=PageId from AC_PAGES WHERE DESCRIPTION ='Assigned to Contractor' AND MENUID =@ServicingMenuId ORDER BY pageid ASC			 
		
			DECLARE db_cursor CURSOR FOR  
			SELECT pa.JobRoleTeamId ,p.PAGEID
			FROM AC_PAGES p
			INNER JOIN AC_PAGES_ACCESS pa
			ON p.PAGEID =pa.PageId
			WHERE  
			p.DESCRIPTION ='Assigned to Contractor' 
			AND p.MENUID=@ServicingMenuId
			ORDER BY pa.JobRoleTeamId ASC

			OPEN db_cursor   
			FETCH NEXT FROM db_cursor INTO @jobRoleTeamId, @pId  

			WHILE @@FETCH_STATUS = 0   
			BEGIN   
			      
				  IF(@pId !=@PagesId)
				  BEGIN
					IF EXISTS(Select 1 FROM AC_PAGES_ACCESS WHERE PageId=@PagesId AND JobRoleTeamId=@jobRoleTeamId)
						BEGIN
						--'Deleting duplicate rows'
						DELETE FROM AC_PAGES_ACCESS
						WHERE PageId=@pId AND JobRoleTeamId=@jobRoleTeamId
						END
					ELSE
						BEGIN
						--Updating duplicate row
						
						UPDATE AC_PAGES_ACCESS 
						SET AC_PAGES_ACCESS.PageId = @PagesId
						WHERE PageId =@pId AND JobRoleTeamId = @jobRoleTeamId
						END
				  END

				   FETCH NEXT FROM db_cursor INTO @jobRoleTeamId, @pId     
			END   

			CLOSE db_cursor   
			DEALLOCATE db_cursor
						
			DELETE FROM AC_PAGES WHERE PAGEID != @PagesId AND DESCRIPTION ='Assigned to Contractor' AND MENUID =@ServicingMenuId			
			
		END
		--===============================================================================
		--End Handling issue of duplication of Assigned to Contractor Report
		--===============================================================================
		
		
		--===============================================================================
		-- Compliance Documents Report in Reports > Compliance Documents
		--===============================================================================
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Compliance Documents')
		BEGIN	
			DECLARE @REPORT_MENUID INT
			Select @REPORT_MENUID = MENU.MENUID 
			from  AC_MENUS MENU INNER JOIN AC_MODULES MODULE ON MENU.MODULEID = MODULE.MODULEID
			WHERE MODULE.DESCRIPTION = 'Property' AND MENU.DESCRIPTION='Reports'

			INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Compliance Documents',@REPORT_MENUID,1,1,'~/../PropertyDataRestructure/Views/Reports/ComplianceDocuments.aspx',6,1,NULL,'',1)
		END
		
		--===============================================================================
		-- Update page description 'Gas/Electric Checks' to 'Gas/Electric Meter Checks' for Tixket#10065
		--===============================================================================
		IF EXISTS (Select 1 from AC_PAGES where  DESCRIPTION = 'Gas/Electric Checks')
		BEGIN	
			
			Update AC_Pages Set DESCRIPTION = 'Gas/Electric Meter Checks' Where DESCRIPTION = 'Gas/Electric Checks'			
		END
		
		
		--===============================================================================
		-- INSERT new page description 'Planned Job Sheet'
		--===============================================================================
		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Planned Job Sheet')
		BEGIN	
			INSERT INTO AC_Pages (DESCRIPTION,MENUID,ACTIVE,link,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Planned Job Sheet',50,1,0,'~/../RSLApplianceServicing/Views/Common/JobSheetSummary.aspx',5,3,324,'',1)		
		END
				--===============================================================================
		-- INSERT new page description 'Assigned To Contractor Job Sheet'
		--===============================================================================
		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Assigned To Contractor Job Sheet')
		BEGIN	
			INSERT INTO AC_Pages (DESCRIPTION,MENUID,ACTIVE,link,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Assigned To Contractor Job Sheet',50,1,0,'~/../RSLApplianceServicing/Views/Common/JobSheetSummaryAC.aspx',6,3,324,'',1)		
		END
		
		--==============================================================================
		-- Ticket # 10593 (Enable Maintenance dashboard) 
		--==============================================================================
		IF	EXISTS (	SELECT	1 
						FROM	AC_PAGES
								INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
								AND AC_PAGES.DESCRIPTION = 'Dashboard'	)
		BEGIN	
			
			UPDATE	AC_PAGES
			SET		AC_PAGES.PAGE = '~/../PropertyDataRestructure/Views/Maintenance/Dashboard/MaintenanceDashboard.aspx'
			FROM	AC_PAGES
					INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
			WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
					AND AC_PAGES.DESCRIPTION = 'Dashboard'		
					
		END
		
		-- US 438
		-- Update 'Fuel Servicing' to 'Gas Servicing'
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Fuel Servicing')
		BEGIN
			UPDATE AC_PAGES
			SET DESCRIPTION = 'Gas Servicing'
			WHERE DESCRIPTION = 'Fuel Servicing'
			Print ('Successfully update description from ''Fuel Servicing'' to ''Gas Servicing''. ')
		END 

		--===============================================================================
		-- Oil Servicing in Servicing > Scheduling > Oil Servicing
		--===============================================================================
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Oil Servicing')
		BEGIN
			Set @MODULEID  = -1;
			Set @MENUID  = -1;
			Set @PAGEID  = -1;
			Set @ORDERTEXT  = -1;
					

			SELECT @MODULEID = MODULEID FROM AC_MODULES WHERE DESCRIPTION = 'Property'
			PRINT @MODULEID

			SELECT @MENUID = MENUID FROM AC_MENUS WHERE MODULEID = @MODULEID AND DESCRIPTION = 'Servicing'
			PRINT @MENUID
			
			--SELECT PAGEID OF Scheduling PAGE
			SELECT @PAGEID = PAGEID FROM AC_PAGES WHERE MENUID = @MENUID AND DESCRIPTION = 'Scheduling'
			PRINT @PAGEID
		
			--Increment ORDERTEXT of all Pages except Fuel Scheduling to insert it below Gas Scheduling
			 UPDATE AC_PAGES SET ORDERTEXT = CAST (CAST(ORDERTEXT AS INT) + 1 AS NVARCHAR)
			 WHERE ParentPage = @PAGEID AND DESCRIPTION NOT IN ('Gas Servicing')
		 					 
			--Now insert the page
			IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Alternative Servicing')
			BEGIN
				INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
				VALUES('Alternative Servicing',@MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=alternativeservicing',2,2,@PAGEID,'~/../RSLApplianceServicing/Views/Scheduling/AlternativeServicing.aspx',1)
				Print 'Added Alternative Servicing Page'
			END

			--Now insert the page
			IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Oil Servicing')
			BEGIN
				INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
				VALUES('Oil Servicing',@MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=oilservicing',3,2,@PAGEID,'~/../RSLApplianceServicing/Views/Scheduling/OilServicing.aspx',1)
				Print 'Added OIL servicing Page'
			END

			--Update Already Existing Hierechy After Adding Alternative Servicing
			update AC_PAGES SET ORDERTEXT = 4
			WHERE DESCRIPTION = 'Void Servicing'

			update AC_PAGES SET ORDERTEXT = 5
			WHERE DESCRIPTION = 'M&E Servicing'

			update AC_PAGES SET ORDERTEXT = 6
			WHERE DESCRIPTION = 'PAT Testing'

			update AC_PAGES SET ORDERTEXT = 7
			WHERE DESCRIPTION = 'Defects'
		END
		
		--===============================================================================
		-- Void Scheduling in Servicing > Scheduling > Void Scheduling
		--===============================================================================
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Void Servicing')
		BEGIN
			Set @MODULEID  = -1;
			Set @MENUID  = -1;
			Set @PAGEID  = -1;
			Set @ORDERTEXT  = -1;
					

			SELECT @MODULEID = MODULEID FROM AC_MODULES WHERE DESCRIPTION = 'Property'
			PRINT @MODULEID

			SELECT @MENUID = MENUID FROM AC_MENUS WHERE MODULEID = @MODULEID AND DESCRIPTION = 'Servicing'
			PRINT @MENUID
			
			--SELECT PAGEID OF Scheduling PAGE
			SELECT @PAGEID = PAGEID FROM AC_PAGES WHERE MENUID = @MENUID AND DESCRIPTION = 'Scheduling'
			PRINT @PAGEID

		
			--Increment ORDERTEXT of all Pages except Fuel Scheduling to insert it below Fuel Scheduling
			 UPDATE AC_PAGES SET ORDERTEXT = CAST (CAST(ORDERTEXT AS INT) + 1 AS NVARCHAR)
			 WHERE ParentPage = @PAGEID AND DESCRIPTION NOT IN ('Fuel Servicing')
		 			
		 
		 --Now insert the page
			 INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Void Servicing',@MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=voidservicing',1,2,@PAGEID,'~/../RSLApplianceServicing/Views/Scheduling/VoidScheduling.aspx',1)
		END
		
			
		--===============================================================================
		-- Voids  > Reports > CP12 Status Report
		--===============================================================================
		
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='CP12 Status Report')
		BEGIN
			DECLARE @CP12_MODULEID INT = -1,
					@CP12_MENUID INT = -1,
					@CP12_PAGEID INT = -1,
					@CP12_ORDERTEXT INT = -1
					
			SELECT @CP12_MODULEID = MODULEID FROM AC_MODULES WHERE DESCRIPTION = 'Property'
			PRINT
			 @CP12_MODULEID
			
			--Select MENUID of Voids
			SELECT @CP12_MENUID = MENUID FROM AC_MENUS WHERE MODULEID = @CP12_MODULEID AND DESCRIPTION = 'Voids'
			PRINT @CP12_MENUID
			
			--SELECT PAGEID OF Reports PAGE
			SELECT @CP12_PAGEID = PAGEID FROM AC_PAGES WHERE MENUID = @CP12_MENUID AND DESCRIPTION = 'Reports'
			PRINT @CP12_PAGEID

			SELECT @CP12_ORDERTEXT = (MAX(CONVERT(INTEGER,ORDERTEXT))+1) FROM AC_PAGES WHERE ParentPage = @CP12_PAGEID
			
		 
		 --Now insert the page
			 INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('CP12 Status Report',@CP12_MENUID,1,1,'~/../PropertyDataRestructure/Views/Reports/CP12StatusReport.aspx',@CP12_ORDERTEXT,2,@CP12_PAGEID,'',1)
		END		

		--===============================================================================
		-- INSERT new page description 'Unviersal Credit Report'
		--===============================================================================
	
		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Universal Credit Report')
			BEGIN	
				declare @AC_Menu int
				Select @AC_Menu = MENUID
				From AC_Menus
				Where DESCRIPTION = 'Monitoring' 
				--print @AC_Menu
				INSERT INTO [dbo].[AC_PAGES]
				([DESCRIPTION] ,[MENUID] ,[ACTIVE],[LINK],[PAGE] ,[ORDERTEXT]  ,[AccessLevel] ,[ParentPage]  ,[BridgeActualPage] ,[DisplayInTree])
				VALUES
				('Universal Credit Report'  ,@AC_Menu  ,1  ,1 ,'UniversalCreditReport.asp' ,NULL   ,1 ,NULL  ,NULL ,1)

				PRINT 'New row added'
			END
		ELSE
			BEGIN
				PRINT 'Row Already Exist'
			END

		--===============================================================================
		-- INSERT new page description 'Status History'
		--===============================================================================

		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Status History')
			BEGIN  
				insert into AC_PAGES values('Status History',49,1,1,'~/Controls/Property/StatusHistory.ascx',8,2,269,NULL,1)
				PRINT('ADDED STATUS HISTORY')
			END



		--===============================================================================
		-- Moving Ad Hoc Works Scheduling to Maintenance > Scheduling > Ad Hoc Works
		--===============================================================================
		IF EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Ad Hoc Works')
		BEGIN
			
			Set @MODULEID  = -1;
			Set @MENUID  = -1;
			Set @PAGEID  = -1;
			Set @ORDERTEXT  = -1;	

			SELECT @MODULEID = MODULEID FROM AC_MODULES WHERE DESCRIPTION = 'Property'
			PRINT @MODULEID

			SELECT @MENUID = MENUID FROM AC_MENUS WHERE MODULEID = @MODULEID AND DESCRIPTION = 'Maintenance'
			PRINT @MENUID
			
			--SELECT PAGEID OF Scheduling PAGE
			SELECT @PAGEID = PAGEID FROM AC_PAGES WHERE MENUID = @MENUID AND DESCRIPTION = 'Scheduling'
			PRINT @PAGEID

			--Now update the Ad Hoc Works page
			Update AC_PAGES Set MENUID = @menuID, ParentPage = @pageID, ORDERTEXT = 2,
			PAGE = '~/../PlannedMaintenance/Bridge.aspx?pg=miscworks', 
			BridgeActualPage = '~/../PlannedMaintenance/Views/Scheduling/MiscWorks.aspx'
			Where description = 'Ad Hoc Works'

			--Now update the Misc Job Sheet page
			UPDATE AC_PAGES SET	MENUID = @MENUID
			Where description = 'Misc Job Sheet'

			Print 'Misc Job Sheet.'
			Print 'Ad Hoc Works Upadted.'
		END
			
		--===============================================================================
		-- INSERT new page description 'Scheme Record Page'
		--===============================================================================
    
		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Scheme Record')
			BEGIN    

				SELECT @MODULEID = MODULEID FROM AC_MODULES WHERE DESCRIPTION = 'Property'
				PRINT @MODULEID

				SELECT @MENUID = MENUID FROM AC_MENUS WHERE MODULEID = @MODULEID AND DESCRIPTION = 'Search'
				PRINT @MENUID
			
				--SELECT PAGEID OF Scheduling PAGE
				SELECT @PAGEID = PAGEID FROM AC_PAGES WHERE MENUID = @MENUID AND DESCRIPTION = 'Search'
				PRINT @PAGEID

				INSERT INTO [dbo].[AC_PAGES]
				([DESCRIPTION] ,[MENUID] ,[ACTIVE],[LINK],[PAGE] ,[ORDERTEXT]  ,[AccessLevel] ,[ParentPage]  ,[BridgeActualPage] ,[DisplayInTree])
				VALUES
				('Scheme Record'  ,@MENUID  ,1  ,0 ,'~/../PropertyDataRestructure/Bridge.aspx?pg=schemerecord' ,2   ,2 ,@PAGEID  ,'~/../PropertyDataRestructure/Views/Pdr/Scheme/Dashboard/SchemeRecord.aspx' ,1)

				PRINT 'Scheme Record Page added'
			END
		ELSE
			BEGIN
				PRINT 'Scheme Record Page Already Exist'
			END
			
		--===============================================================================
		-- Fixed issue related to visibility of Defect scheduling sub pages 
		--===============================================================================

			IF EXISTS (	SELECT	1
							FROM	AC_PAGES 
							WHERE	DESCRIPTION IN ('Defect Appointments','Defect Basket','Defect Job Sheet')
									AND LINK = 1 )
			BEGIN	

				UPDATE	AC_PAGES
				SET		link = 0
				WHERE	DESCRIPTION = 'Defect Appointments'

				UPDATE	AC_PAGES
				SET		link = 0
				WHERE	DESCRIPTION = 'Defect Basket'

				UPDATE	AC_PAGES
				SET		link = 0
				WHERE	DESCRIPTION = 'Defect Job Sheet'

			END

		--===============================================================================
		-- Fixes for RFC3 Replacing the links of calendar to global calendar 
		--===============================================================================

			declare @bridgeActualPage varchar(300)
			set @bridgeActualPage = '~/../BRSFaultLocator/Views/Calendar/PropertyCalendar.aspx'

			declare @calenderPage nvarchar(600) 
			select @calenderPage = AC_MENUS.PAGE 
			from AC_MENUS 														
			where AC_MENUS.DESCRIPTION='Calendar' 		

			if  (SELECT TOP (1) AC_PAGES.PAGE from AC_PAGES 
			 inner join AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			where AC_PAGES.DESCRIPTION like 'Calendar' and AC_MENUS.DESCRIPTION = 'Servicing') != @calenderPage+'&mn=Servicing'

			Begin

			print 'updating'

			Update AC_PAGES 
			Set PAGE = @calenderPage+'&mn=Servicing', BridgeActualPage = @bridgeActualPage+'?mn=Servicing'
		
			from AC_PAGES 
					Inner join AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			where AC_PAGES.DESCRIPTION='Calendar' and AC_MENUS.DESCRIPTION='Servicing' 
			
			End
				
			-- End for servicing
			-- Start for repairs
			
			if  (SELECT TOP (1) AC_PAGES.PAGE from AC_PAGES 
			 inner join AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			where AC_PAGES.DESCRIPTION like 'Calendar' and AC_MENUS.DESCRIPTION = 'Repairs') != @calenderPage+'&mn=Repairs'

			Begin
			
			Update AC_PAGES 
			Set PAGE = @calenderPage+'&mn=Repairs',  BridgeActualPage = @bridgeActualPage+'?mn=Repairs'
		
			from AC_PAGES 
					Inner join AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			where AC_PAGES.DESCRIPTION='Calendar' and AC_MENUS.DESCRIPTION='Repairs' 

			-- End for Repairs
			-- Start for void

			if (SELECT TOP(1) AC_PAGES.PAGE from AC_PAGES
			Inner join AC_MENUS  on AC_MENUS.MENUID = AC_PAGES.MENUID
			where AC_PAGES.DESCRIPTION = 'Calendar' and AC_MENUS.DESCRIPTION = 'Voids') != @calenderPage+'&mn=Voids'

			Begin
			update ac_pages
			set page = @calenderpage+'&mn=Voids' , BridgeActualPage = @bridgeActualPage+'?mn=Voids'
			from ac_pages
			inner join AC_MENUS on AC_PAGES.MENUID = AC_MENUS.MENUID
			where AC_PAGES.DESCRIPTION = 'Calendar' and AC_MENUS.DESCRIPTION = 'Voids'
			End

			-- End for void
			-- Start for Planned

			declare @plannedMenuId int
			select @plannedMenuId = AC_MENUS.MenuID from AC_MENUS where description = 'Planned' 
			

			if not exists (Select * from AC_Pages where MENUID = @plannedMenuId and DESCRIPTION = 'Calendar')
			Begin
				insert into AC_PAGES values ('Calendar',@plannedMenuId,1,1,@calenderPage, 3,1,null,@bridgeActualPage,1)			
			End
			
			if (Select TOP (1) AC_Pages.PAGE from AC_Pages
			inner join AC_Menus on AC_Pages.MENUID = AC_MENUS.MENUID
			where AC_Pages.Description = 'Calendar' and AC_MENUS.Description = 'Planned') != @calenderPage+'&mn=Planned'

			Begin
			update AC_PAGES
			SET Page = @calenderPage+'&mn=Planned', BridgeActualPage = @bridgeActualPage+'?mn=Planned'
			from AC_Pages inner join AC_MENUS on AC_Pages.MENUID = AC_MENUS.MENUID
			where AC_Pages.Description = 'Calendar' and AC_Menus.Description = 'Planned'
			End
			-- End for Planned
			-- Start for Maintainance

			if (Select TOP (1) AC_Pages.PAGE from AC_Pages
			inner join AC_Menus on AC_Pages.MENUID = AC_MENUS.MENUID
			where AC_Pages.Description = 'Calendar' and AC_MENUS.Description = 'Maintenance') != @calenderPage+'&mn=Maintenance'

			Begin
			update AC_PAGES
			SET Page = @calenderPage+'&mn=Maintenance', BridgeActualPage = @bridgeActualPage+'?mn=Maintenance'
			from AC_Pages inner join AC_MENUS on AC_Pages.MENUID = AC_MENUS.MENUID
			where AC_Pages.Description = 'Calendar' and AC_Menus.Description = 'Maintenance'
			End

			-- End for maintenence
	END 

		--===============================================================================
		-- RENAME Property Terrier
		--===============================================================================
		IF NOT EXISTS(SELECT * FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'Assets Register')
							
		BEGIN
			UPDATE	AC_PAGES
			SET		DESCRIPTION = 'Assets Register'
			WHERE	DESCRIPTION = 'Property Terrier'     
				
		END   

		--===============================================================================
		
		
		--===============================================================================
		-- RENAME  �Payment Cards� menu item to �Payment Upload�
		--===============================================================================
		IF EXISTS(SELECT 1 FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'Payment Cards')
							
		BEGIN
			UPDATE	AC_PAGES
			SET		DESCRIPTION = 'Payment Upload'
			WHERE	DESCRIPTION = 'Payment Cards'     
				
		END   
		IF EXISTS(SELECT 1 FROM	AC_PAGES WHERE	AC_PAGES.DESCRIPTION = 'Service Charge' AND AC_PAGES.MENUID = 50 AND AC_PAGES.ParentPage = 277)
							
		BEGIN
			Update AC_PAGES SET MENUID = 58, ORDERTEXT=7, AccessLevel=1, ParentPage=null
			where DESCRIPTION like 'Service Charge'
				
		END  

		--===============================================================================
		
		--To enable 10836 and HR Module on LIVE server, Just uncomment the below area
		
		--===============================================================================
		-- HR Module
		--===============================================================================
		Declare @hrModule INT, @heAdminMenu INT
		Select @hrModule=MODULEID from AC_MODULES where DESCRIPTION='HR'
		Select @heAdminMenu=MENUID from AC_MENUS where DESCRIPTION='Admin' and MODULEID=@hrModule
		
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Access Control' and MENUID=@heAdminMenu)
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,PAGE,ORDERTEXT,LINK,AccessLevel,DisplayInTree)
			VALUES('Access Control',@heAdminMenu,1,'AccessControl','1',1,1,1)
		END
		
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Annual Leave Rules' and MENUID=@heAdminMenu)
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,PAGE,ORDERTEXT,LINK,AccessLevel,DisplayInTree)
			VALUES('Annual Leave Rules',@heAdminMenu,1,'#','2',1,1,1)
		END
		else
		BEGIN
			update AC_PAGES set ORDERTEXT = '2' where DESCRIPTION = 'Annual Leave Rules' and MENUID=@heAdminMenu
		END

		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Group Training' and MENUID=@heAdminMenu)
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,PAGE,ORDERTEXT,LINK,AccessLevel,DisplayInTree)
			VALUES('Group Training',@heAdminMenu,1,'GroupTraining','3',1,1,1)
		END
		else
		BEGIN
			update AC_PAGES set ORDERTEXT = '3' where DESCRIPTION = 'Group Training' and MENUID=@heAdminMenu
		END
		
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Job Role Admin' and MENUID=@heAdminMenu)
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,PAGE,ORDERTEXT,LINK,AccessLevel,DisplayInTree)
			VALUES('Job Role Admin',@heAdminMenu,1,'JobRole','4',1,1,1)
		END
		else
		BEGIN
			update AC_PAGES set ORDERTEXT = '4' where DESCRIPTION = 'Job Role Admin' and MENUID=@heAdminMenu
		END

		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Team Setup' and MENUID=@heAdminMenu)
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,PAGE,ORDERTEXT,LINK,AccessLevel,DisplayInTree)
			VALUES('Team Setup',@heAdminMenu,1,'Teams','5',1,1,1)
		END
		else
		BEGIN
			update AC_PAGES set ORDERTEXT = '5' where DESCRIPTION = 'Team Setup' and MENUID=@heAdminMenu
		END

		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Bank Holiday' and MENUID=@heAdminMenu)
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,PAGE,ORDERTEXT,LINK,AccessLevel,DisplayInTree)
			VALUES('Bank Holiday',@heAdminMenu,1,'BankHoliday','6',1,1,1)
		END
		else
		BEGIN
			update AC_PAGES set ORDERTEXT = '6' where DESCRIPTION = 'Bank Holiday' and MENUID=@heAdminMenu
		END
		

		--===============================================================================
		-- Rearrange Void Works After termination date changes
		--===============================================================================
		Declare @voidMenu int, @voidParent INT
		Select @voidMenu=MENUID from AC_MENUS where DESCRIPTION='Voids'
		Select @voidParent=PAGEID from AC_PAGES where DESCRIPTION='Void Works'
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='ReArrange Void Works Required')
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,DisplayInTree,BridgeActualPage,ParentPage)
			VALUES('ReArrange Void Works Required',@voidMenu,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/ReArrangeVoidWorksRequired.aspx','1',3,0,'',@voidParent)

			Print 'Added ReArrange Void Works Required page'
		END
/*
		--===============================================================================
		
		--===============================================================================
		-- Ticket# 10836
		--===============================================================================
		IF	EXISTS (	SELECT	*
						FROM	AC_PAGES
								INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
								AND AC_PAGES.DESCRIPTION = 'Cyclic Maintenance'	)
		BEGIN	
			
			UPDATE	AC_PAGES
			SET		AC_PAGES.PAGE = '~/../PropertyDataRestructure/Views/CyclicalServices/CyclicalServices.aspx'
					,AC_PAGES.DESCRIPTION = 'Cyclical Services'
			FROM	AC_PAGES
					INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
			WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
					AND AC_PAGES.DESCRIPTION = 'Cyclic Maintenance'		
					
		END 
 
 IF	NOT EXISTS (	SELECT	*
						FROM	AC_PAGES
								INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
								AND AC_PAGES.DESCRIPTION = 'Unassigned Cyclical Services Report'	)
		BEGIN	
			DECLARE @ParentPage int,@MaintenanceMenu int
			SELECT	@ParentPage=AC_PAGES.PAGEID, @MaintenanceMenu=AC_PAGES.MENUID
						FROM	AC_PAGES
								INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
								AND AC_PAGES.DESCRIPTION = 'Reports'
								
								
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,AC_PAGES.ACTIVE,AC_PAGES.LINK,AC_PAGES.PAGE,AC_PAGES.ORDERTEXT,AC_PAGES.AccessLevel,AC_PAGES.ParentPage,
			AC_PAGES.DisplayInTree)
			VALUES('Unassigned Cyclical Services',@MaintenanceMenu,1,1,'~/../PropertyDataRestructure/Views/Reports/UnassignedCyclicalReport.aspx',1,2,@ParentPage,1)					
			Print ('Unassigned Cyclical Services Report  added Successfully.')
		END

 IF	NOT EXISTS (	SELECT	*
						FROM	AC_PAGES
								INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
								AND AC_PAGES.DESCRIPTION = 'Cancelled Order'	)
		BEGIN	
			DECLARE @ParentPage int,@MaintenanceMenu int
			SELECT	@ParentPage=AC_PAGES.PAGEID, @MaintenanceMenu=AC_PAGES.MENUID
						FROM	AC_PAGES
								INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
								AND AC_PAGES.DESCRIPTION = 'Reports'
								
								
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,AC_PAGES.ACTIVE,AC_PAGES.LINK,AC_PAGES.PAGE,AC_PAGES.ORDERTEXT,AC_PAGES.AccessLevel,AC_PAGES.ParentPage,
			AC_PAGES.DisplayInTree)
			VALUES('Cancelled Order',@MaintenanceMenu,1,1,'~/../PropertyDataRestructure/Views/Reports/CancelledOrderReport.aspx',1,2,@ParentPage,1)					
			Print ('Cancelled Order Report  added Successfully.')
			
			
			
		END		
 
*/
		
		--===============================================================================
		-- Ticket# 10828
		--===============================================================================

		Declare @ServicingMenu int
				Select @ServicingMenu=MENUID from AC_MENUS where DESCRIPTION='Servicing'
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Print Job Sheet Summary')
				BEGIN
					INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,DisplayInTree,BridgeActualPage,ParentPage)
					VALUES('Print Job Sheet Summary',@ServicingMenu,1,0,'~/../RSLApplianceServicing/Views/Reports/PrintJobSheetSummary.aspx','18',1,NULL,'',NULL)

					Print 'Added Print Job Sheet Summary page'
				END

		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'To Be Arranged Alternative Servicing')
		BEGIN
			Declare @ParentPage int
			Select @ParentPage=PageId from [dbo].[AC_PAGES] where DESCRIPTION='Alternative Servicing'
			INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('To Be Arranged Alternative Servicing',@ServicingMenu,1,0,'~/../RSLApplianceServicing/Views/Scheduling/ToBeArrangedAvailableAppointments.aspx',1,3,@ParentPage,'',0)
			Print 'Added To Be Arranged Alternative Servicing Page'
		END

		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Alternative Arranged Appointments')
		BEGIN
			Declare @ParentPage1 int
			Select @ParentPage1=PageId from [dbo].[AC_PAGES] where DESCRIPTION='Alternative Servicing'
			INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Alternative Arranged Appointments',@ServicingMenu,1,0,'~/../RSLApplianceServicing/Views/Scheduling/AlternativeArrangedAppointments.aspx',1,3,@ParentPage1,'',0)
			Print 'Added Alternative Arranged Appointments Page'
		END

		Declare @OilParentPage int
		Select @ParentPage=PageId from [dbo].[AC_PAGES] where DESCRIPTION='Oil Servicing'
		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'To Be Arranged Oil Servicing')
		BEGIN
			INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('To Be Arranged Oil Servicing',@ServicingMenu,1,0,'~/../RSLApplianceServicing/Views/Scheduling/OilToBeArrangedAvailableAppointments.aspx',1,3,@OilParentPage,'',0)
			Print 'Added To Be Arranged Oil Servicing Page'
		END

		IF NOT EXISTS (SELECT 1 FROM AC_PAGES WHERE  DESCRIPTION = 'Oil Arranged Appointments')
		BEGIN
			INSERT INTO [dbo].[AC_PAGES](DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Oil Arranged Appointments',@ServicingMenu,1,0,'~/../RSLApplianceServicing/Views/Scheduling/OilArrangedAppointments.aspx',1,3,@OilParentPage,'',0)
			Print 'Added Oil Arranged Appointments Page'
		END

		Declare @AdminMenu int
				Select @AdminMenu=MENUID from AC_MENUS where DESCRIPTION='Admin' and MODULEID = (
				SELECT MODULEID from AC_MODULES where DESCRIPTION = 'Property')
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='PDR Upload Document')
				BEGIN
					INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,DisplayInTree,BridgeActualPage,ParentPage)
					VALUES('PDR Upload Document',@AdminMenu,1,0,'~/../PropertyDataRestructure/Views/Common/UploadDocument.aspx','19',1,NULL,'',NULL)

					Print 'Added PDR Upload Document page'
				END


		Declare @PurchasingMenuId int
			Select @PurchasingMenuId=MENUID from AC_MENUS where DESCRIPTION='Purchasing' and MODULEID = (
			SELECT MODULEID from AC_MODULES where DESCRIPTION = 'Finance')
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Upload Invoice')
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Upload Invoice',@PurchasingMenuId,1,1,'UploadInvoice.asp',1,1,NULL,NULL,1)
			Print 'Added Upload Invoice page'
		END

		
			Select @PurchasingMenuId=MENUID from AC_MENUS where DESCRIPTION='Purchasing' and MODULEID = (
			SELECT MODULEID from AC_MODULES where DESCRIPTION = 'Finance')
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Cancelled Purchase Orders')
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Cancelled Purchase Orders',@PurchasingMenuId,1,1,'CancelledPurchaseOrders.asp',1,1,NULL,NULL,1)
			Print 'Added Canceled Purchase Orders page'
		END

			Select @PurchasingMenuId=MENUID from AC_MENUS where DESCRIPTION='Purchasing' and MODULEID = (
			SELECT MODULEID from AC_MODULES where DESCRIPTION = 'Finance')
		IF NOT EXISTS (Select 1 from AC_PAGES where DESCRIPTION='Cancelled Credit Notes')
		BEGIN
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree)
			VALUES('Cancelled Credit Notes',@PurchasingMenuId,1,1,'CancelledCreditNote.asp',1,1,NULL,NULL,1)
			Print 'Added Cancelled Credit Note.asp page'
		END
		
		

		--===============================================================================
		-- US578 - INSERT new page description 'Restriction'
		--===============================================================================

		IF NOT EXISTS (	SELECT	1 
						FROM	AC_PAGES 
						WHERE	DESCRIPTION ='Restriction')
			BEGIN  

				DECLARE @SearchMenuId INT
				DECLARE @SearchPageId INT

				SELECT  @SearchMenuId = AC_MENUS.MENUID 
				FROM	AC_MENUS
						INNER JOIN AC_MODULES ON AC_MODULES.MODULEID = AC_MENUS.MODULEID
				WHERE	AC_MENUS.DESCRIPTION = 'Search' AND AC_MODULES.DESCRIPTION = 'Property'

				SELECT  @SearchPageId = AC_PAGES.PAGEID 
				FROM	AC_PAGES 
						INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
						INNER JOIN AC_MODULES ON AC_MODULES.MODULEID = AC_MENUS.MODULEID
				WHERE	AC_MENUS.DESCRIPTION = 'Search' AND AC_MODULES.DESCRIPTION = 'Property' AND AC_PAGES.DESCRIPTION = 'Search'

				INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree) 
				VALUES('Restriction',@SearchMenuId,1,1,'~/Controls/Property/Restriction.ascx',9,2,@SearchPageId,NULL,1)
				PRINT('Added Restriction page')

			END

		--===============================================================================
		-- US766 - INSERT new page description 'Create PO SC'
		--===============================================================================
 
		IF NOT EXISTS (	SELECT	1 
						FROM	AC_PAGES 
						WHERE	DESCRIPTION ='Create Purchase Order SC')
		BEGIN 
			Declare @purchMenuId int
			Select @purchMenuId = MENUID from AC_MENUS where DESCRIPTION='Purchasing' and MODULEID = (
			SELECT MODULEID from AC_MODULES where DESCRIPTION = 'Finance')
			INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree) 
			VALUES('Create Purchase Order SC',@purchMenuId,1,1,'CreatePurchaseOrderSC.asp',1,1,NULL,NULL,1)
			PRINT('Added Create Purchase Order SC page')
		END
		
		
		--===============================================================================
		-- US596 - INSERT new page description 'HB Upload'
		--===============================================================================

		IF NOT EXISTS (	SELECT	1 
						FROM	AC_PAGES 
						WHERE	DESCRIPTION ='HB Upload')
			BEGIN  

				DECLARE @ValidationMenuId INT

				SELECT  @ValidationMenuId = AC_MENUS.MENUID 
				FROM	AC_MENUS						
				WHERE	AC_MENUS.DESCRIPTION = 'Validation'

				INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree) 
				VALUES('HB Upload',@ValidationMenuId,1,1,'../RSLHousingBenefit/Bridge?UserId=',NULL,1,NULL,NULL,1)
				PRINT('Added HB Upload page')

			END

		--===============================================================================
		-- US822 - INSERT new page description 'Journal Templates'
		--===============================================================================

		IF NOT EXISTS (	SELECT	1 
						FROM	AC_PAGES 
						WHERE	DESCRIPTION ='Journal Templates')
			BEGIN  

				DECLARE @NominalMenuId INT

				SELECT	@NominalMenuId = AC_MENUS.MENUID
				FROM	AC_MODULES
						INNER JOIN AC_MENUS ON AC_MODULES.MODULEID = AC_MENUS.MODULEID
				WHERE	AC_MODULES.DESCRIPTION = 'Accounts'
						AND AC_MENUS.DESCRIPTION = 'Nominal'

				INSERT INTO AC_PAGES(DESCRIPTION,MENUID,ACTIVE,LINK,PAGE,ORDERTEXT,AccessLevel,ParentPage,BridgeActualPage,DisplayInTree) 
				VALUES('Journal Templates',@NominalMenuId,1,1,'../BHGFinanceModule/Bridge?Module=Account&UserId=',NULL,1,NULL,NULL,1)
				PRINT('Added Journal Templates page')

			END
 
	
	END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
