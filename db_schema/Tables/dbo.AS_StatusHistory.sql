CREATE TABLE [dbo].[AS_StatusHistory]
(
[StatusHistoryId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [int] NOT NULL,
[Title] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ranking] [smallint] NOT NULL,
[CreatedDate] [smalldatetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedBy] [int] NOT NULL,
[ModifiedDate] [smalldatetime] NULL,
[InspectionTypeID] [smallint] NULL,
[IsEditable] [bit] NULL,
[Ctimestamp] [smalldatetime] NULL CONSTRAINT [DF__AS_Status__Ctime__472917DC] DEFAULT (getdate())
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_StatusHistory] ADD 
CONSTRAINT [PK_AS_StatusHistory] PRIMARY KEY CLUSTERED  ([StatusHistoryId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AS_StatusHistory] ADD CONSTRAINT [FK_AS_StatusHistory_AS_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AS_Status] ([StatusId])
GO
