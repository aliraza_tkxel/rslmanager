USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'PS_Appointment')
    BEGIN

CREATE TABLE [dbo].[PS_Appointment](
	[AppointId] [int] IDENTITY(1,1) NOT NULL,
	[AppointType] [varchar](50) NULL,
	[AppointProgStatus] [varchar](50) NOT NULL,
	[SurveyType] [varchar](50) NULL,
	[AppointTitle] [varchar](100) NULL,
	[AppointLocation] [varchar](100) NULL,
	[AppointStartDateTime] [datetime] NULL,
	[AppointEndDateTime] [datetime] NULL,
	[SurveyourStatus] [varchar](50) NOT NULL,
	[AppointNotes] [varchar](max) NULL,
	[AppointValidity] [bit] NULL,
	[SurveyourUserName] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[AppointmentAlert] [varchar](100) NULL,
	[AppointmentCalendar] [varchar](100) NULL,
	[addToCalendar] [bit] NULL,
 CONSTRAINT [PK_PS_Appointment] PRIMARY KEY CLUSTERED 
(
	[AppointId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[PS_Appointment] ADD  CONSTRAINT [DF__PS_Appoin__addTo__72936425]  DEFAULT ((0)) FOR [addToCalendar]
 END

    --========================================================================================================
	-- Addition of app version while completing appointment
    --========================================================================================================
	IF COL_LENGTH('PS_Appointment', 'AppointmentCompletionDate') IS NULL
	BEGIN
			ALTER TABLE PS_Appointment
			ADD AppointmentCompletionDate smalldatetime NULL
			PRINT('COLUMN AppointmentCompletionDate CREATED')
	END

	IF COL_LENGTH('PS_Appointment', 'AppointmentCompletedAppVersion') IS NULL
	BEGIN
			ALTER TABLE PS_Appointment
			ADD AppointmentCompletedAppVersion nvarchar(20) NULL
			PRINT('COLUMN AppointmentCompletedAppVersion CREATED')
	END

	IF COL_LENGTH('PS_Appointment', 'AppointmentCurrentAppVersion') IS NULL
	BEGIN
			ALTER TABLE PS_Appointment
			ADD AppointmentCurrentAppVersion nvarchar(20) NULL
			PRINT('COLUMN AppointmentCurrentAppVersion CREATED')
	END




  IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH
