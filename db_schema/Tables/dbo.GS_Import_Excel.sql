CREATE TABLE [dbo].[GS_Import_Excel]
(
[PropertyId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CP12Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IssueDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
