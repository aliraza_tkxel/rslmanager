CREATE TABLE [dbo].[FL_FAULT_LOG_HISTORY]
(
[FaultLogHistoryID] [int] NOT NULL IDENTITY(1, 1),
[JournalID] [int] NOT NULL,
[FaultStatusID] [int] NULL,
[ItemActionID] [int] NULL,
[LastActionDate] [datetime] NULL,
[LastActionUserID] [int] NULL,
[FaultLogID] [int] NULL,
[ORGID] [int] NULL,
[ScopeID] [int] NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROPERTYID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractorID] [smallint] NULL,
[Duration] [decimal] (9, 2) NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FL_FAULT_LOG_HISTORY] ADD 
CONSTRAINT [PK_FL_REPAIR_NEW] PRIMARY KEY CLUSTERED  ([FaultLogHistoryID]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_FL_FAULT_LOG_HISTORY_LASTACTIONDATE] ON [dbo].[FL_FAULT_LOG_HISTORY] ([LastActionDate]) ON [PRIMARY]

GO
