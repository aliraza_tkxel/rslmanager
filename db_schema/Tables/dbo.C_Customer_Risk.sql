USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'C_Customer_Risk')
BEGIN
CREATE TABLE [dbo].[C_Customer_Risk](
	[Sid] [int] IDENTITY(1,1) NOT NULL,
	[RiskHistoryId] [int] NULL,
	[JournalId] [int] NULL,
	[CustomerId] [int] NULL,
	[SubCategoryId] [int] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[C_Customer_Risk]  WITH CHECK ADD  CONSTRAINT [FK_C_Customer_Risk_C_JOURNAL] FOREIGN KEY([JournalId])
REFERENCES [dbo].[C_JOURNAL] ([JOURNALID])
ALTER TABLE [dbo].[C_Customer_Risk] CHECK CONSTRAINT [FK_C_Customer_Risk_C_JOURNAL]
ALTER TABLE [dbo].[C_Customer_Risk]  WITH CHECK ADD  CONSTRAINT [FK_C_Customer_Risk_C_RISK_SUBCATEGORY] FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[C_RISK_SUBCATEGORY] ([SubCategoryId])
ALTER TABLE [dbo].[C_Customer_Risk] CHECK CONSTRAINT [FK_C_Customer_Risk_C_RISK_SUBCATEGORY]
END

ELSE
BEGIN
PRINT 'Table already exist..'

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME = 'C_Customer_Risk'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE C_Customer_Risk
ADD CONSTRAINT pk_Sid PRIMARY KEY (Sid)
PRINT 'Primary key added'
END

END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

