CREATE TABLE [dbo].[H_UNITCOSTS]
(
[UNITPRICEID] [int] NOT NULL IDENTITY(1, 1),
[UPPERBAND] [int] NULL,
[PRICEPERUNIT] [decimal] (18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[H_UNITCOSTS] ADD CONSTRAINT [PK_H_UNITCOSTS] PRIMARY KEY CLUSTERED  ([UNITPRICEID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
