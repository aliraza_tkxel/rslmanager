CREATE TABLE [dbo].[PS_Survey_Item_Dates]
(
[DateId] [int] NOT NULL IDENTITY(1, 1),
[SurveyId] [int] NULL,
[ItemId] [int] NULL,
[LastDone] [smalldatetime] NULL,
[DueDate] [smalldatetime] NULL,
[ParameterId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Item_Dates] ADD 
CONSTRAINT [PK_PS_Survey_Item_Dates] PRIMARY KEY CLUSTERED  ([DateId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PS_Survey_Item_Dates] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Item_Dates_PA_ITEM] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[PA_ITEM] ([ItemID])
ALTER TABLE [dbo].[PS_Survey_Item_Dates] WITH NOCHECK ADD
CONSTRAINT [FK_PS_Survey_Item_Dates_PS_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[PS_Survey] ([SurveyId])
GO
