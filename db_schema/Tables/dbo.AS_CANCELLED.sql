USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AS_CANCELLED')
BEGIN

CREATE TABLE [dbo].[AS_CANCELLED](
	[CancelID] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[JOURNALID] [int] NULL,
	[RecordedOn] [smalldatetime] NULL,
	[Notes] [nvarchar](1000) NULL
 )


ALTER TABLE [dbo].[AS_CANCELLED]  WITH CHECK ADD  CONSTRAINT [AS_CANCELLED_AS_JOURNAL] FOREIGN KEY([JOURNALID])
REFERENCES [dbo].[AS_JOURNAL] ([JOURNALID])



ALTER TABLE [dbo].[AS_CANCELLED] CHECK CONSTRAINT [AS_CANCELLED_AS_JOURNAL]



PRINT 'Table Created'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH