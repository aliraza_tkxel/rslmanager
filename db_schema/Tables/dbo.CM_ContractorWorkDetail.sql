USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'CM_ContractorWorkDetail')
BEGIN

CREATE TABLE [dbo].[CM_ContractorWorkDetail](
	[WorkDetailId] [int] IDENTITY(1,1) NOT NULL,
	[CMContractorId] [int] NULL,
	[CycleDate] [smalldatetime] NULL,
	[NetCost] [money] NULL,
	[VatId] [int] NULL,
	[VAT] [money] NULL,
	[GrossCost] [money] NULL,
	[PurchaseOrderItemId] [int] NULL,
	[CycleCompleted] [smalldatetime] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_CM_ContractorWorkDetail] PRIMARY KEY CLUSTERED 
(
	[WorkDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




