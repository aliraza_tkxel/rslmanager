USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'V_RequiredWorks')
    BEGIN

      CREATE TABLE [dbo].[V_RequiredWorks] (
        [RequiredWorksId] [int] IDENTITY (1, 1) NOT NULL,
        [InspectionJournalId] [int] NULL,
        [IsMajorWorksRequired] [bit] NULL,
        [RoomId] [int] NULL,
        [OtherLocation] [varchar](100) NULL,
        [WorkDescription] [ntext] NULL,
        [VoidWorksNotes] [ntext] NULL,
        [TenantNeglectEstimation] [money] NULL,
        [ComponentId] [int] NULL,
        [ReplacementDue] [date] NULL,
        [Condition] [varchar](100) NULL,
        [WorksJournalId] [int] NULL,
        [IsTenantWorks] [bit] NULL,
        [IsBrsWorks] [bit] NULL,
        [StatusId] [int] NULL,
        [IsScheduled] [bit] NULL,
        [CreatedDate] [datetime] NULL,
        [ModifiedDate] [datetime] NULL,
        [IsCanceled] [bit] NULL,
        [Duration] [float] NULL,
        [IsVerified] [bit] NULL,
        [MajorWorkNotes] [nvarchar](500) NULL,
        [workType] [nvarchar](200) NULL,
        CONSTRAINT [PK_V_RequiredWorks] PRIMARY KEY CLUSTERED
        (
        [RequiredWorksId] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
      ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



      ALTER TABLE [dbo].[V_RequiredWorks] WITH CHECK ADD CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL] FOREIGN KEY ([InspectionJournalId])
      REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])


      ALTER TABLE [dbo].[V_RequiredWorks] CHECK CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL]


      ALTER TABLE [dbo].[V_RequiredWorks] WITH CHECK ADD CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL1] FOREIGN KEY ([WorksJournalId])
      REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])


      ALTER TABLE [dbo].[V_RequiredWorks] CHECK CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL1]


      ALTER TABLE [dbo].[V_RequiredWorks] ADD CONSTRAINT [DF__V_Require__IsVer__787031BF] DEFAULT ((0)) FOR [IsVerified]


	   PRINT 'Table Created';
    END
    ELSE

    BEGIN
      PRINT 'Table Already Exist';
    END

	IF COL_LENGTH('V_RequiredWorks', 'faultAreaID') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD faultAreaID int NULL
	END
	IF COL_LENGTH('V_RequiredWorks', 'faultID') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD faultID int NULL
	END
	IF COL_LENGTH('V_RequiredWorks', 'isRecurringProblem') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD isRecurringProblem bit NULL
	END
	IF COL_LENGTH('V_RequiredWorks', 'faultNotes') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD faultNotes ntext NULL
	END
	IF COL_LENGTH('V_RequiredWorks', 'problemDays') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD problemDays int NULL
	END

	IF COL_LENGTH('V_RequiredWorks', 'JobsheetCompletionDate') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD JobsheetCompletionDate smalldatetime NULL
			PRINT('COLUMN JobsheetCompletionDate CREATED')
	END

	IF COL_LENGTH('V_RequiredWorks', 'JobsheetCompletedAppVersion') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD JobsheetCompletedAppVersion nvarchar(20) NULL
			PRINT('COLUMN JobsheetCompletedAppVersion CREATED')
	END

	IF COL_LENGTH('V_RequiredWorks', 'JobsheetCurrentAppVersion') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD JobsheetCurrentAppVersion nvarchar(20) NULL
			PRINT('COLUMN JobsheetCurrentAppVersion CREATED')
	END

	IF COL_LENGTH('V_RequiredWorks', 'repairId') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD repairId int NULL
	END

	declare @datatype varchar(max)
 set @datatype  = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'V_RequiredWorks' AND 
     COLUMN_NAME = 'WorkDescription')
if  (@datatype = 'ntext')
begin
	ALTER TABLE V_RequiredWorks
	ALTER COLUMN WorkDescription NVARCHAR(MAX)
	Print 'WorkDescription Data type changed'
END

 set @datatype  = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'V_RequiredWorks' AND 
     COLUMN_NAME = 'VoidWorksNotes')
if  (@datatype = 'ntext')
begin
	ALTER TABLE V_RequiredWorks
	ALTER COLUMN VoidWorksNotes NVARCHAR(MAX)
	Print 'VoidWorksNotes Data type changed'
END

 set @datatype  = (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'V_RequiredWorks' AND 
     COLUMN_NAME = 'faultNotes')
if  (@datatype = 'ntext')
begin
	ALTER TABLE V_RequiredWorks
	ALTER COLUMN faultNotes NVARCHAR(MAX)
	Print 'faultNotes Data type changed'
END

--===============================================================
-- US714 - Added isLegionella column
--===============================================================
	IF COL_LENGTH('V_RequiredWorks', 'isLegionella') IS NULL
	BEGIN
			ALTER TABLE V_RequiredWorks
			ADD isLegionella bit NULL
	END

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH