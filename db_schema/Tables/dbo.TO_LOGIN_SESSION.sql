CREATE TABLE [dbo].[TO_LOGIN_SESSION]
(
[SessionId] [bigint] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NULL,
[LogInDateTime] [smalldatetime] NULL,
[LogOutDateTime] [smalldatetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[TO_LOGIN_SESSION] ADD 
CONSTRAINT [PK_TO_LOGIN_SESSION] PRIMARY KEY CLUSTERED  ([SessionId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
