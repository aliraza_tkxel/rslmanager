USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ASB_Case')
BEGIN
CREATE TABLE [dbo].[ASB_Case]
(
	[CaseId] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[DateRecorded] [smalldatetime] NULL,
	[DateReported] [smalldatetime] NOT NULL,
	[IncidentDate] [smalldatetime] NOT NULL,
	[IncidentTime][smalldatetime] NULL,
	[CaseOfficer] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[IncidentType] [int] NOT NULL,
	[RiskLevel] [int] NULL,
	[IncidentDescription] [nvarchar](max) NOT NULL,
	[PoliceNotified] [bit] NOT NULL,
	[CrimeCaseNumber] [int] NULL,
	[NextFollowupDate] [smalldatetime] NOT NULL,
	[FollowupDescription] [nvarchar](max) NULL,
	[ClosedDate] [smalldatetime] NULL,
	[ClosedDescription] [nvarchar](max) NULL,
	[CaseStatus] [int] NULL,
	[StageId] int NULL,
	[SubCategoryId] int NULL,
	[TypeId] int NULL,
	[SubTypeId] int NULL,
	[ReviewDate] [smalldatetime] NULL,
	[CaseOfficerTwo] [int] NULL
) 
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([CaseOfficer])
REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([CaseStatus])
REFERENCES [dbo].[ASB_CaseStatus] ([StatusId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([Category])
REFERENCES [dbo].[ASB_Category] ([CategoryId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([IncidentType])
REFERENCES [dbo].[ASB_IncidentType] ([IncidentTypeId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([RiskLevel])
REFERENCES [dbo].[ASB_RiskLevel] ([RiskLevelId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([StageId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([TypeId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([SubTypeId])
REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([CaseOfficerTwo])
REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])

PRINT 'Table Created'

END

ELSE
BEGIN
if exists(select * from [dbo].[ASB_Case]
              where [Category] = 3)
BEGIN
	UPDATE [RSLBHALive].[dbo].[ASB_Case]
	SET [Category]=2
	PRINT 'Categories changed from 3 to 2'
END
IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RiskLevel'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ALTER COLUMN RiskLevel int NULL
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'StageId'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ADD [StageId] INT NULL;

	ALTER TABLE [dbo].[ASB_Case] WITH CHECK 
	ADD FOREIGN KEY([StageId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId]);
	
	EXEC('UPDATE [dbo].[ASB_Case]
	SET [StageId]=1;
	');
	PRINT 'Added Column StageId';
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SubCategoryId'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ADD [SubCategoryId] int NULL
	ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([SubCategoryId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	PRINT 'Added Column SubCategoryId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'TypeId'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ADD [TypeId] int NULL
	ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([TypeId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	PRINT 'Added Column TypeId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SubTypeId'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ADD [SubTypeId] int NULL
	ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([SubTypeId])
	REFERENCES [dbo].[ASB_StagesLookup] ([StagesLookupId])
	PRINT 'Added Column SubTypeId'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ReviewDate'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ADD [ReviewDate] [smalldatetime] NULL
	PRINT 'Added Column ReviewDate'
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'CaseOfficerTwo'
      AND Object_ID = Object_ID(N'ASB_Case'))
BEGIN
    ALTER TABLE [dbo].[ASB_Case]
	ADD [CaseOfficerTwo] [int] NULL
	ALTER TABLE [dbo].[ASB_Case]  WITH CHECK ADD FOREIGN KEY([CaseOfficerTwo])
	REFERENCES [dbo].[E__EMPLOYEE] ([EMPLOYEEID])
	PRINT 'Added Column CaseOfficerTwo'
END
PRINT 'Table already exist..'
END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

