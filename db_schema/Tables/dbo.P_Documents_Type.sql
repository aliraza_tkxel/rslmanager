USE RSLBHALive
GO
BEGIN TRANSACTION
BEGIN TRY
--Check if table exists or not
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_Documents_Type')
BEGIN
	

	CREATE TABLE [dbo].[P_Documents_Type](
	[DocumentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	 CONSTRAINT [PK__P_Docume__DD7012645C5315FE] PRIMARY KEY CLUSTERED 
	(
		[DocumentTypeId] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]


	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[P_Documents_Type] ADD  CONSTRAINT [DF_P_Documents_Type_active]  DEFAULT ((1)) FOR [Active]

	ALTER TABLE [dbo].[P_Documents_Type] ADD  CONSTRAINT [DF_P_Documents_Type_order]  DEFAULT ((1000)) FOR [Order]

	  PRINT 'Table Created';
 END
 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_Documents_Type')
BEGIN
  PRINT 'Table Exists';
 
 
IF EXISTS (Select 1 from P_Documents_Type where Title='CP12')
	BEGIN  
		PRINT 'Title already exist'
	END
IF NOT EXISTS (Select 1 from P_Documents_Type where Title='CP12')
	BEGIN 	
	

	INSERT INTO P_Documents_Type (title, Active, [order]) VALUES('CP12',1,1000)
	PRINT 'New row added'
END
 
 
END --if


--Insert NHBC Document Type  
 
Declare @countOfSubTypes int =0, @NHBCDocumentTypeId int = -1

Select TOP(1) @NHBCDocumentTypeId = DocumentTypeId from dbo.P_Documents_Type Where Title like '%NHBC%'; 

IF (@NHBCDocumentTypeId = -1)
	BEGIN
		Insert Into P_Documents_Type (title, Active, [order])
		Values ('NHBC',1,1000) 	
		SELECT @NHBCDocumentTypeId  = SCOPE_IDENTITY()
	END

IF COL_LENGTH('P_Documents_Type','CategoryId') IS NULL
 BEGIN
	ALTER TABLE P_Documents_Type
	ADD CategoryId int;
	print('CategoryId is added')
 END

 IF COL_LENGTH('P_Documents_Type','IsDevelopment') IS NULL
 BEGIN
	ALTER TABLE P_Documents_Type
	ADD IsDevelopment int;
	print('IsDevelopment is added')
 END

 IF COL_LENGTH('P_Documents_Type','IsScheme') IS NULL
 BEGIN
	ALTER TABLE P_Documents_Type
	ADD IsScheme int;
	print('IsScheme is added')
 END

 IF COL_LENGTH('P_Documents_Type','IsProperty') IS NULL
 BEGIN
	ALTER TABLE P_Documents_Type
	ADD IsProperty int;
	print('IsProperty is added')
 END


 Declare @mainQuery nvarchar (max)

--- 1
set @mainQuery = 'if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Electrical'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Electrical''
		print(''1 updated'')
	end
--- 2
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Asbestos'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Asbestos''
		print(''2 updated'')
	end
--- 3
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Air Conditioning'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Air Conditioning''
		print(''3 updated'')
	end
--- 4
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Building Contract Documents'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Legal''),
		Isdevelopment = 1, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Building Contract Documents''
		print(''4 updated'')
	end
--- 5
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''CCTV'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''CCTV''
		print(''5 updated'')
	end
--- 6
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Communal Kitchen Equipment Inspection'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Communal Kitchen Equipment Inspection''
		print(''6 updated'')
	end
--- 7
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Communal Water Supply'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Communal Water Supply''
		print(''7 updated'')
	end
--- 8
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Contract'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''General''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Contract''
		print(''8 updated'')
	end

--- 9
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Damp Proof Course Certificate'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Damp Proof Course Certificate''
		print(''9 updated'')
	end
--- 10
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Disclaimer Form'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''General''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 0 
		where categoryid is null and title = ''Disclaimer Form''
		print(''10 updated'')
	end
--- 11
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Door Entry Systems'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Door Entry Systems''
		print(''11 updated'')
	end
--- 12
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Dry Lining Certificate'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 0 
		where categoryid is null and title = ''Dry Lining Certificate''
		print(''12 updated'')
	end
--- 13
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''EPC'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 0 
		where categoryid is null and title = ''EPC''
		print(''13 updated'')
	end
--- 14
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Fire Alarm Systems'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Fire Alarm Systems''
		print(''14 updated'')
	end

--- 15
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Fire Management'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''General''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Fire Management''
		print(''15 updated'')
	end
--- 16
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Grant / HCA Documents'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Legal''),
		Isdevelopment = 1, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Grant / HCA Documents''
		print(''16 updated'')
	end
--- 17
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Health & Safety'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1 
		where categoryid is null and title = ''Health & Safety''
		print(''17 updated'')
	end

--- 18
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Heating'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Heating''
		print(''18 updated'')
	end
--- 19
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Insulation'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Insulation''
		print(''19 updated'')
	end

--- 20
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Kitchens'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''General''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Kitchens''
		print(''20 updated'')
	end

--- 21
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Legal Documents'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Legal''),
		Isdevelopment = 1, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Legal Documents''
		print(''21 updated'')
	end

--- 22
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Lifts'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1 
		where categoryid is null and title = ''Lifts''
		print(''22 updated'')
	end

--- 23
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Permits'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''General''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 0
		where categoryid is null and title = ''Permits''
		print(''23 updated'')
	end

--- 24
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Sewage Treatment Plant / Pumps'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1
		where categoryid is null and title = ''Sewage Treatment Plant / Pumps''
		print(''24 updated'')
	end

--- 25
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Waste Management'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1
		where categoryid is null and title = ''Waste Management''
		print(''25 updated'')
	END

--- 26
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Windows and Doors'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 1
		where categoryid is null and title = ''Windows and Doors''
		print(''26 updated'')
	end
--- 27
if Not exists (select 1 from P_Documents_Type where title = ''Warden Call'' )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Warden Call'', 1, 1000, (select categorytypeid from P_category where title = ''Compliance''), 0,1,1)
		print(''Warden Call added'')
	end

--- 28
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Planning, Building Control, NHBC'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Legal''),
		Isdevelopment = 1, isProperty = 1, IsScheme = 1
		where categoryid is null and title = ''Planning, Building Control, NHBC''
		print(''28 updated'')
	END

--- 29
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Ventilation'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''General''),
		Isdevelopment = 0, isProperty = 1, IsScheme = 0
		where categoryid is null and title = ''Ventilation''
		print(''29 updated'')
	END
--- 30
if Not exists (select 1 from P_Documents_Type where title = ''Lifts'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Lifts'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,1)
		print(''new lifts added'')
	end
--- 31
if Not exists (select 1 from P_Documents_Type where title = ''Bathrooms'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Bathrooms'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,1)
		print(''Bathrooms added'')
	end
--- 32
if Not exists (select 1 from P_Documents_Type where title = ''Property Information'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Property Information'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,0,1)
		print(''Property Information'')
	end
--- 33
if Not exists (select 1 from P_Documents_Type where title = ''Drainage'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Drainage'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,0,1)
		print(''Drainage added'')
	end

--- 34
if Not exists (select 1 from P_Documents_Type where title = ''Heating'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Heating'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,1)
		print(''Heating added'')
	end

--- 35
if exists (select 1 from P_Documents_Type where categoryid is null and title = ''Assisted Baths and Overhead Hoists'' )
	begin
		Update  P_Documents_Type set categoryid = (select categorytypeid from P_category where title = ''Compliance''),
		Isdevelopment = 0, isProperty = 0, IsScheme = 1
		where categoryid is null and title = ''Assisted Baths and Overhead Hoists''
		print(''Assisted Baths and Overhead Hoists updated'')
	END

--- 36
if Not exists (select 1 from P_Documents_Type where title = ''Assisted Baths and Overhead Hoists'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Assisted Baths and Overhead Hoists'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,0)
		print(''Assisted Baths and Overhead Hoists added for General'')
	end

--- 37
if Not exists (select 1 from P_Documents_Type where title = ''Commercial EPC'' and categoryId = (select categorytypeid from P_category where title = ''Compliance'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Commercial EPC'', 1, 1000, (select categorytypeid from P_category where title = ''Compliance''), 0,1,0)
		print(''Commercial EPC added for Compliance'')
	END

--- 38
if Not exists (select 1 from P_Documents_Type where title = ''Fire Management'' and categoryId = (select categorytypeid from P_category where title = ''Compliance'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Fire Management'', 1, 1000, (select categorytypeid from P_category where title = ''Compliance''), 0,1,0)
		print(''Commercial EPC added for Compliance'')
	END

--- 39
if Not exists (select 1 from P_Documents_Type where title = ''CCTV'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''CCTV'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,0)
		print(''CCTV added for GENERAL SCHEME'')
	END

--- 40
if Not exists (select 1 from P_Documents_Type where title = ''Door Entry Systems'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Door Entry Systems'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,0)
		print(''Door Entry Systems for GENERAL SCHEME'')
	END

--- 40
if Not exists (select 1 from P_Documents_Type where title = ''Fire Alarm Systems'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Fire Alarm Systems'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,0)
		print(''Fire Alarm Systems for GENERAL SCHEME'')
	END

--- 41
if Not exists (select 1 from P_Documents_Type where title = ''Scheme Information'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Scheme Information'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,1,0)
		print(''Scheme Information GENERAL SCHEME'')
	END

-- 42	
	if Not exists (select 1 from P_Documents_Type where title = ''Housing'' and categoryId = (select categorytypeid from P_category where title = ''General'') )
	begin
		Insert into P_Documents_Type (Title, Active , [Order], CategoryId, IsDevelopment, IsScheme, IsProperty) Values (''Housing'', 1, 1000, (select categorytypeid from P_category where title = ''General''), 0,0,1)
		print (''Housing added'')
	END
	else
		begin 
			update P_Documents_Type set IsProperty = 1, IsScheme = 0, IsDevelopment= 0
			where title = ''Housing'' and categoryId = (select categorytypeid from P_category where title = ''General'')
			print (''Housing updated'')
		end
'

EXEC (@mainQuery)

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
