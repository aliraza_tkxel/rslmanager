USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'P_LGSR')
BEGIN
  PRINT 'Table Exists';
	CREATE TABLE [dbo].[P_LGSR](
		[LGSRID] [int] IDENTITY(1,1) NOT NULL,
		[PROPERTYID] [nvarchar](20) NULL,
		[ISSUEDATE] [smalldatetime] NULL,
		[CP12NUMBER] [nvarchar](20) NULL,
		[CP12ISSUEDBY] [int] NULL,
		[DOCUMENTTYPE] [nvarchar](50) NULL,
		[NOTES] [varchar](200) NULL,
		[DTIMESTAMP] [smalldatetime] NULL CONSTRAINT [DF_P_LGSR_DTIMESTAMP]  DEFAULT (getdate()),
		[CP12DOCUMENT] [image] NULL,
		[DOCTITLE] [nvarchar](200) NULL,
		[CP12UPLOADEDBY] [int] NULL,
		[CP12UPLOADEDON] [smalldatetime] NULL,
		[DOCFILE] [nvarchar](50) NULL,
		[BSAVE] [smallint] NULL,
		[BUPLOAD] [smallint] NULL,
		[RECEVIEDONBEHALF] [varchar](20) NULL,
		[RECEVIEDDATE] [smalldatetime] NULL,
		[TENANTHANDED] [bit] NULL,
		[INSPECTIONCARRIED] [bit] NULL,
		[JOURNALID] [int] NULL,
		[ISPRINTED] [bit] NULL,
		[IsAppointmentCompleted] [bit] NOT NULL CONSTRAINT [DF_P_LGSR_IsAppointmentCompleted]  DEFAULT ((1)),
		[CP12Renewal] [smalldatetime] NULL		
	 CONSTRAINT [PK__P_LGSR] PRIMARY KEY NONCLUSTERED 
	(
		[LGSRID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


END

IF COL_LENGTH('P_LGSR', 'CP12Passed') IS NULL
	BEGIN
			ALTER TABLE P_LGSR ADD CP12Passed int NULL 
			PRINT('COLUMN CP12Passed CREATED')
	END

IF COL_LENGTH('P_LGSR', 'SchemeId') IS NULL
	BEGIN
			ALTER TABLE P_LGSR ADD SchemeId int NULL 
			PRINT('COLUMN SchemeId CREATED')
	END

IF COL_LENGTH('P_LGSR', 'BlockId') IS NULL
	BEGIN
			ALTER TABLE P_LGSR ADD BlockId int NULL 
			PRINT('COLUMN BlockId CREATED')
	END
	
IF COL_LENGTH('P_LGSR', 'HeatingMappingId') IS NULL
	BEGIN
			ALTER TABLE P_LGSR ADD HeatingMappingId int NULL 
			PRINT('COLUMN BlockId CREATED')
	END

IF NOT EXISTS (SELECT * 
FROM sys.indexes 
WHERE name='IX_P_LGSR_JOURNALID' AND object_id = OBJECT_ID('dbo.P_LGSR'))
BEGIN

	CREATE NONCLUSTERED INDEX IX_P_LGSR_JOURNALID ON dbo.P_LGSR (JOURNALID)
	PRINT 'NONCLUSTERED INDEX CREATED ON JOURNALID'

END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity, @ErrorState );
Print (@ErrorMessage)
END CATCH

GO


BEGIN TRANSACTION
BEGIN TRY 

IF EXISTS( select * from P_LGSR WHERE CP12Passed is NULL)
	BEGIN
		UPDATE P_LGSR SET CP12Passed = 1 WHERE CP12Passed is NULL
		PRINT('CP12Passed Updated Successfully where WHERE CP12Passed is NULL')
	END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorInsertMessage NVARCHAR(4000);
	DECLARE @ErrorInsertSeverity INT;
	DECLARE @ErrorInsertState INT;

	SELECT @ErrorInsertMessage = ERROR_MESSAGE(),
	@ErrorInsertSeverity = ERROR_SEVERITY(),
	@ErrorInsertState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorInsertMessage,@ErrorInsertSeverity, @ErrorInsertState );
Print (@ErrorInsertMessage)
END CATCH
