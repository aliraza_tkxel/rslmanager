USE RSLBHALive
GO

BEGIN TRANSACTION
  BEGIN TRY

    IF NOT EXISTS (SELECT
        *
      FROM INFORMATION_SCHEMA.TABLES
      WHERE TABLE_NAME = N'PDR_DEVELOPMENT')
    BEGIN

      CREATE TABLE [dbo].[PDR_DEVELOPMENT] (
        [DEVELOPMENTID] [int] IDENTITY (1, 1) NOT NULL,
        [PATCHID] [int] NULL,
        [DEVELOPMENTNAME] [nvarchar](100) NULL,
        [DEVELOPMENTTYPE] [int] NULL,
        [STARTDATE] [smalldatetime] NULL,
        [TARGETDATE] [smalldatetime] NULL,
        [ACTUALDATE] [smalldatetime] NULL,
        [DEFECTSPERIOD] [smalldatetime] NULL,
        [NUMBEROFUNITS] [float] NULL,
        [USETYPE] [int] NULL,
        [SUITABLEFOR] [int] NULL,
        [LOCALAUTHORITY] [int] NULL,
        [LEADDEVOFFICER] [int] NULL,
        [SUPPORTDEVOFFICER] [int] NULL,
        [PROJECTMANAGER] [nvarchar](100) NULL,
        [CONTRACTOR] [int] NULL,
        [INSURANCEVALUE] [money] NULL,
        [AQUISITIONCOST] [money] NULL,
        [CONTRACTORCOST] [money] NULL,
        [OTHERCOST] [money] NULL,
        [SCHEMECOST] [money] NULL,
        [DEVELOPMENTADMIN] [money] NULL,
        [INTERESTCOST] [money] NULL,
        [BALANCE] [money] NULL,
        [HAG] [money] NULL,
        [MORTGAGE] [money] NULL,
        [BORROWINGRATE] [money] NULL,
        [NETTCOST] [money] NULL,
        [SURVEYOR] [int] NULL,
        [SUPPORTEDCLIENTS] [int] NULL,
        [ADDRESS1] [varchar](500) NULL,
        [ADDRESS2] [varchar](500) NULL,
        [TOWN] [varchar](50) NULL,
        [COUNTY] [varchar](50) NULL,
        [Architect] [varchar](50) NULL,
        [DefectsPeriodEnd] [smalldatetime] NULL,
        [LandValue] [float] NULL,
        [FundingSource] [int] NULL,
        [GrantAmount] [float] NULL,
        [BorrowedAmount] [float] NULL,
        [OutlinePlanningApplication] [smalldatetime] NULL,
        [DetailedPlanningApplication] [smalldatetime] NULL,
        [OutlinePlanningApproval] [smalldatetime] NULL,
        [DetailedPlanningApproval] [smalldatetime] NULL,
        [PostCode] [varchar](50) NULL,
        [PurchaseDate] [smalldatetime] NULL,
        [DEVELOPMENTSTATUS] [int] NULL,
        [LandPurchasePrice] [float] NULL,
        [ValueDate] [smalldatetime] NULL,
        CONSTRAINT [PK_PDR_DEVELOPMENT] PRIMARY KEY CLUSTERED
        (
        [DEVELOPMENTID] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
      ) ON [PRIMARY]
      SET ANSI_PADDING OFF
      ALTER TABLE [dbo].[PDR_DEVELOPMENT] ADD DEFAULT ((0)) FOR [LandPurchasePrice]
    END
    ELSE
		BEGIN
		  PRINT 'Table Already Exist';
		END

    --========================================================================================================
    --========================================================================================================
    ------Adding Index
    --========================================================================================================
    --========================================================================================================

    IF Not EXISTS (SELECT name FROM sys.indexes
      WHERE name = N'IX_PDR_DEVELOPMENT')
    BEGIN
      CREATE NONCLUSTERED INDEX IX_PDR_DEVELOPMENT
      ON PDR_DEVELOPMENT (PatchId);
	  PRINT 'IX_PDR_DEVELOPMENT Created Successfully!';
    END
	ELSE
	BEGIN
		PRINT 'IX_PDR_DEVELOPMENT Index Already Exist';
	END

    --========================================================================================================
    --========================================================================================================

    IF @@TRANCOUNT > 0
    BEGIN
    COMMIT TRANSACTION;
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
  BEGIN
    ROLLBACK TRANSACTION;
  END
  DECLARE @ErrorMessage nvarchar(4000);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  SELECT
    @ErrorMessage = ERROR_MESSAGE(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage,
  @ErrorSeverity,
  @ErrorState
  );
  PRINT (@ErrorMessage)
END CATCH

GO


--========================================================================================================
--========================================================================================================
------Adding Phase 1 to all Developments
--========================================================================================================
--========================================================================================================
IF EXISTS (SELECT *   FROM INFORMATION_SCHEMA.TABLES   WHERE TABLE_NAME = N'PDR_DEVELOPMENT')
	BEGIN
	
		IF EXISTS (	select 1  from PDR_DEVELOPMENT LEFT JOIN P_PHASE on PDR_DEVELOPMENT.Developmentid = P_PHASE.Developmentid
				where  PDR_DEVELOPMENT.Developmentid NOT IN (select PDR_DEVELOPMENT.Developmentid 
				from PDR_DEVELOPMENT INNER JOIN P_PHASE   on PDR_DEVELOPMENT.Developmentid = P_PHASE.Developmentid
				where  UPPER(P_PHASE.PhaseName) = 'PHASE 1') )
			BEGIN
				BEGIN TRAN 
					Declare @Developmentid INT = 0
					Declare @ID int = 0

					CREATE TABLE #TEMP(CID INT IDENTITY (1, 1), Developmentid INT)  
					INSERT INTO #TEMP(Developmentid)  
  
					SELECT DISTINCT PDR_DEVELOPMENT.Developmentid 
					FROM	PDR_DEVELOPMENT LEFT JOIN P_PHASE  ON PDR_DEVELOPMENT.Developmentid = P_PHASE.Developmentid
					WHERE  PDR_DEVELOPMENT.Developmentid NOT IN 
							(select PDR_DEVELOPMENT.Developmentid from PDR_DEVELOPMENT INNER JOIN P_PHASE 
							on PDR_DEVELOPMENT.Developmentid = P_PHASE.Developmentid
							WHERE  UPPER(P_PHASE.PhaseName) = 'PHASE 1')
					ORDER BY PDR_DEVELOPMENT.Developmentid  desc
   

					--Loop through each development and insert phase 1 for it
					DECLARE loop_PDR_DEVELOPMENT CURSOR
					STATIC FOR 
					SELECT CID, Developmentid from #Temp order by Developmentid DESC
					OPEN loop_PDR_DEVELOPMENT
					IF @@CURSOR_ROWS > 0
						BEGIN 
							FETCH NEXT FROM loop_PDR_DEVELOPMENT INTO @ID,@Developmentid
							WHILE @@Fetch_status = 0
								BEGIN
									Select @Developmentid = Developmentid
									FROM #Temp
									WHERE CID = @ID

									INSERT INTO P_PHASE (PHASENAME,DEVELOPMENTID)
									VALUES ( 'Phase 1', @Developmentid)
				
									PRINT 'Phase 1 Successfully Added for ' + Convert(varchar(20),@Developmentid)
									FETCH NEXT FROM loop_PDR_DEVELOPMENT INTO @ID,@Developmentid
								END
						END
					CLOSE loop_PDR_DEVELOPMENT
					DEALLOCATE loop_PDR_DEVELOPMENT
					Drop Table #Temp
					IF @@ERROR <> 0   
						BEGIN   
							ROLLBACK TRAN  
							RAISERROR ('ERROR WHILE INSERTING IN TO P_PHASE', 17, 1)  
							--RETURN -1
						END  
					COMMIT 
					SET ANSI_WARNINGS ON;
			END
			ELSE
				BEGIN
					PRINT 'Phase 1 Exists in  All Developments';
				END
	END
	
--========================================================================================================
--========================================================================================================

IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'PDR_DEVELOPMENT'))
BEGIN

alter table PDR_DEVELOPMENT add CompanyId [int] NULL

END

