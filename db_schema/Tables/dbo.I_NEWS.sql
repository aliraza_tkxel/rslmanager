CREATE TABLE [dbo].[I_NEWS]
(
[NewsID] [int] NOT NULL IDENTITY(1, 1),
[NewsCategory] [int] NULL,
[Headline] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImagePath] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_I_NEWS_DateCreated] DEFAULT (getdate()),
[NewsContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rank] [int] NULL,
[Active] [bit] NULL,
[IsHomePage] [bit] NULL,
[HomePageImage] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsTenantOnline] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1446348267_9_8_5] ON [dbo].[I_NEWS] ([IsHomePage], [Active], [DateCreated])

CREATE NONCLUSTERED INDEX [_dta_index_I_NEWS_5_1446348267__K8_K9_1] ON [dbo].[I_NEWS] ([Active], [IsHomePage]) INCLUDE ([NewsID]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_I_NEWS_5_1446348267__K5D_K8_1_2_3_4_7_9_10] ON [dbo].[I_NEWS] ([DateCreated] DESC, [Active]) INCLUDE ([Headline], [HomePageImage], [ImagePath], [IsHomePage], [NewsCategory], [NewsID], [Rank]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[I_NEWS] ADD CONSTRAINT [PK_I_NEWS] PRIMARY KEY CLUSTERED  ([NewsID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
