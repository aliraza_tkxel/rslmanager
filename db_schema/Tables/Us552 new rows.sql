Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY
DECLARE @ParameterId int, @FireItemId INT , @BuildingSystems  INT,  @CleaningItemId INT,
@CommunalFacilities int,@CommonAreas int,@SuppliedServices int, @servicesid int  

Select @BuildingSystems= AreaID from PA_AREA where AreaName = 'Building Systems'
IF NOT EXISTS(Select * from PA_ITEM where itemname='Air Conditioning')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock])
		 VALUES(@BuildingSystems,'Air Conditioning',7,1,1,0,1)
	END

IF NOT EXISTS(Select * from PA_ITEM where itemname='Fire')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock])
		 VALUES(@BuildingSystems,'Fire',7,1,1,0,1)
		SET @FireItemId  = SCOPE_IDENTITY()
			IF NOT EXISTS(Select * from PA_ITEM where itemname='Alarm' and parentItemId=@FireItemId)
			BEGIN
				INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
					VALUES(@BuildingSystems,'Alarm',1,1,1,0,1,@FireItemId)
			END
	
			IF NOT EXISTS(Select * from PA_ITEM where itemname='AOV')
			BEGIN
				INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
					VALUES(@BuildingSystems,'AOV',2,1,1,0,1,@FireItemId)
			END
			IF NOT EXISTS(Select * from PA_ITEM where itemname='Dry Risers')
			BEGIN
				INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
					VALUES(@BuildingSystems,'Dry Risers',3,1,1,0,1,@FireItemId)
			END
			IF NOT EXISTS(Select * from PA_ITEM where itemname='Extinguishers')
			BEGIN
				INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
				VALUES(@BuildingSystems,'Extinguishers',4,1,1,0,1,@FireItemId)
			END
	
			IF NOT EXISTS(Select * from PA_ITEM where itemname='Blankets')
			BEGIN
				INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
					VALUES(@BuildingSystems,'Blankets',5,1,1,0,1,@FireItemId)
			END
	IF  EXISTS(Select * from PA_ITEM where itemname='Extinguishers')
			BEGIN

				DECLARE	 @ParameterName VARCHAR(50) = 'Quantity'
				,@DataType VARCHAR(50) = 'Integer'
				,@ControlType VARCHAR(50) = 'TextBox'
				,@IsDate BIT = 0
				,@Active BIT = 1
				,@ShowInApp BIT = 1
				,@ItemId int
	
		IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
			BEGIN	
	
				Select @ItemId=itemId from PA_ITEM where itemname='Extinguishers'
				INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)
				VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, 1, @Active, @ShowInApp )
		
				SET @ParameterId  = SCOPE_IDENTITY()		
				INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive) VALUES ( @ItemId, @ParameterId, @Active)

				Select @ItemId=itemId from PA_ITEM where itemname='Blankets'
				INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)
				VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, 1, @Active, @ShowInApp )
		
				SET @ParameterId  = SCOPE_IDENTITY()		
				INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive) VALUES ( @ItemId, @ParameterId, @Active)
			
			END				



		END
	
	END

IF NOT EXISTS(Select * from PA_ITEM where itemname='Automatic Doors')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock])
		 VALUES(@BuildingSystems,'Automatic Doors',4,1,1,0,1)
	END
IF EXISTS (	SELECT	1 FROM	PA_ITEM WHERE	ItemName='Door Entry')
	BEGIN 	
	
		UPDATE	PA_ITEM SET		ItemName= 'Door Entry System' FROM	PA_ITEM WHERE	ItemName='Door Entry'

	END
	ELSE
	BEGIN
		PRINT 'Parameter Door Entry already updated.'
	END
---===================Supplied Services==============================
Select  @SuppliedServices=AreaID from PA_AREA where AreaName = 'Supplied Services'
IF NOT EXISTS(Select * from PA_ITEM where itemname='Water Meters')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock])
		 VALUES(@SuppliedServices,'Water Meters',7,1,1,0,1)
	END
Select @CleaningItemId=ItemId from PA_ITEM where itemname='Cleaning'

IF NOT EXISTS(Select * from PA_ITEM where itemname='Bin Cleaning')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
			VALUES(@SuppliedServices,'Bin Cleaning',1,1,1,0,1,@CleaningItemId)
	END
IF NOT EXISTS(Select * from PA_ITEM where itemname='Carpet Cleaning')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
			VALUES(@SuppliedServices,'Carpet Cleaning',2,1,1,0,1,@CleaningItemId)
	END
IF NOT EXISTS(Select * from PA_ITEM where itemname='General Clean')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
			VALUES(@SuppliedServices,'General Clean',3,1,1,0,1,@CleaningItemId)
	END
	IF NOT EXISTS(Select * from PA_ITEM where itemname='High Clean')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
			VALUES(@SuppliedServices,'High Clean',1,1,1,0,1,@CleaningItemId)
	END
	IF NOT EXISTS(Select * from PA_ITEM where itemname='Window Cleaning')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
			VALUES(@SuppliedServices,'Window Cleaning',1,1,1,0,1,@CleaningItemId)
	END
	--==========================Common Areas=================================
	Select  @CommonAreas=AreaID from PA_AREA where AreaName = 'Common Areas'


	IF EXISTS (	SELECT	* FROM	PA_ITEM WHERE	ItemName='Lift')
	BEGIN 	
	
		UPDATE	PA_ITEM SET	ItemName= 'Passenger Lift(s)' FROM	PA_ITEM WHERE ItemName='Lift'
		
	END
	
IF NOT EXISTS(Select * from PA_ITEM where itemname='Stair Lifts')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock])
		 VALUES(@CommonAreas,'Stair Lifts ',4,1,1,0,1)
	END

IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where valuedetail='Stair Lift')
	BEGIN
		Update PA_PARAMETER_VALUE set IsActive=0 where ValueId=179
	END


---=======================Services====================================

Select @servicesid=AreaId from PA_AREA where AreaName= 'Services'

IF NOT EXISTS(Select * from PA_ITEM where itemname='Sewage Treatment Pumps')
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock])
		 VALUES(@servicesid,'Sewage Treatment Pumps',4,1,1,0,1)
	END

---=======================Communal Facilities====================================
Declare @bathroomId int,@LaundryRoom int
Select @CommunalFacilities=AreaId from PA_AREA where AreaName= 'Communal Facilities'

IF  EXISTS(Select * from PA_ITEM where itemname='Bathroom' And AreaId=@CommunalFacilities)
BEGIN
	Select @bathroomId = ItemId from PA_ITEM where ItemName='Bathroom' And AreaId=@CommunalFacilities
	
	IF NOT EXISTS(Select * from PA_ITEM where itemname='Assisted Baths' And AreaId=@CommunalFacilities)
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
		VALUES(@CommunalFacilities,'Assisted Baths',1,1,1,0,1,@bathroomId)
	END
	IF NOT  EXISTS(Select * from PA_ITEM where itemname='Hoists' And AreaId=@CommunalFacilities)
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
		VALUES(@CommunalFacilities,'Hoists',1,1,1,0,1,@bathroomId)
	END

		
END
IF  EXISTS(Select * from PA_ITEM where itemname='Laundry Room' And AreaId=@CommunalFacilities)
BEGIN
Select @LaundryRoom= ItemId  from PA_ITEM where itemname='Laundry Room' And AreaId=@CommunalFacilities

IF  NOT EXISTS(Select * from PA_ITEM where itemname='Washing Machines' And AreaId=@CommunalFacilities)
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
		VALUES(@CommunalFacilities,'Washing Machines',1,1,1,0,1,@LaundryRoom)
	END

	IF NOT EXISTS(Select * from PA_ITEM where itemname='Tumble Dryers' And AreaId=@CommunalFacilities)
	BEGIN
		INSERT INTO [dbo].[PA_ITEM]([AreaID],[ItemName],[ItemSorder],[IsActive],[ShowInApp],[ShowListView],[IsForSchemeBlock],[ParentItemId])
		VALUES(@CommunalFacilities,'Tumble Dryers',1,1,1,0,1,@LaundryRoom)
	END

END




IF @@TRANCOUNT > 0
	BEGIN  
	print'commit'   
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH