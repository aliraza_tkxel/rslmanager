CREATE TABLE [dbo].[C_OLDTELEPHONENUMBERS]
(
[TENANCYID] [int] NOT NULL IDENTITY(1, 1),
[TelephoneNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MobileNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
