CREATE TABLE [dbo].[F_CYCLE]
(
[CYCLE_ID] [int] NOT NULL IDENTITY(1, 1),
[CYCLE_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CYCLE_LENGTH] [int] NULL,
[CYCLE_LENGTH_TYPE] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[F_CYCLE] ADD CONSTRAINT [PK_F_CYCLE] PRIMARY KEY CLUSTERED  ([CYCLE_ID]) WITH FILLFACTOR=90 ON [PRIMARY]
GO
