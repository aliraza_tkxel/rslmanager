Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF COL_LENGTH('PA_PROPERTY_ATTRIBUTES', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PA_PROPERTY_ATTRIBUTES
		ADD HeatingMappingId INT NULL 
		
		Print('HeatingMappingId added successfully  ') 
	END
	

IF COL_LENGTH('PA_PROPERTY_ITEM_DATES', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PA_PROPERTY_ITEM_DATES
		ADD HeatingMappingId INT NULL 
		
		Print('HeatingMappingId added successfully  ') 
	END	

IF COL_LENGTH('P_LGSR', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE P_LGSR
		ADD HeatingMappingId INT NULL 
		
		Print('HeatingMappingId in P_LGSR added successfully  ') 
	END	
	
IF COL_LENGTH('P_LGSR_HISTORY', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE P_LGSR_HISTORY
		ADD HeatingMappingId INT NULL 
		
		Print('HeatingMappingId in P_LGSR_HISTORY added successfully  ') 
	END		

IF COL_LENGTH('AS_JOURNAL', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE AS_JOURNAL
		ADD HeatingMappingId INT NULL 
		
		Print('HeatingMappingId in AS_JOURNAL added successfully  ') 
	END
IF COL_LENGTH('AS_JOURNALHISTORY', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE AS_JOURNALHISTORY
		ADD HeatingMappingId INT NULL 
		
		Print('HeatingMappingId in AS_JOURNALHISTORY added successfully  ') 
	END	
	

IF COL_LENGTH('PA_ITEM_PARAMETER', 'ParameterValueId') IS NULL
	BEGIN
		ALTER TABLE PA_ITEM_PARAMETER
		ADD ParameterValueId INT NULL 
		
		Print('ParameterValueId in PA_ITEM_PARAMETER added successfully  ') 
	END	
	
IF COL_LENGTH('PS_Survey_Parameters', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PS_Survey_Parameters
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PS_Survey_Parameters added successfully  ') 
	END	
	
IF COL_LENGTH('PS_Survey_Item_Dates', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PS_Survey_Item_Dates
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PS_Survey_Item_Dates added successfully  ') 
	END	
	
	
IF COL_LENGTH('PLANNED_CONDITIONWORKS', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PLANNED_CONDITIONWORKS
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PLANNED_CONDITIONWORKS added successfully  ') 
	END		
IF COL_LENGTH('PLANNED_CONDITIONWORKS_HISTORY', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PLANNED_CONDITIONWORKS_HISTORY
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PLANNED_CONDITIONWORKS_HISTORY added successfully  ') 
	END		

IF COL_LENGTH('PS_Survey_Item_Notes', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PS_Survey_Item_Notes
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PS_Survey_Item_Notes added successfully  ') 
	END	
IF COL_LENGTH('PA_PROPERTY_ITEM_NOTES', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PA_PROPERTY_ITEM_NOTES
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PA_PROPERTY_ITEM_NOTES added successfully  ') 
	END		
	
IF COL_LENGTH('PA_PROPERTY_ITEM_IMAGES', 'HeatingMappingId') IS NULL
	BEGIN
		ALTER TABLE PA_PROPERTY_ITEM_IMAGES
		ADD HeatingMappingId int NULL 
		
		Print('HeatingMappingId in PA_PROPERTY_ITEM_IMAGES added successfully  ') 
	END		
	
IF COL_LENGTH('PA_PARAMETER_VALUE', 'IsAlterNativeHeating') IS NULL
	BEGIN
		ALTER TABLE PA_PARAMETER_VALUE
		ADD IsAlterNativeHeating bit NULL 
		
		Print('IsAlterNativeHeating in PA_PARAMETER_VALUE added successfully  ') 
	END		
	
DECLARE @manufacturerId int	
Select @manufacturerId=PA_PARAMETER.ParameterID from PA_PARAMETER where PA_PARAMETER.ParameterName='Boiler Manufacturer'
-- Internal > Services > Heating > Remove GC
--Update PA_PARAMETER SET IsActive=0 where PA_PARAMETER.ParameterName='GC Number'	
---- Internal > Services > Heating > Remove Gasket Set Replacement
--Update PA_PARAMETER SET IsActive=0 where PA_PARAMETER.ParameterName='Gasket Set Replacement'		
--	-- Internal > Services > Heating > Remove Flue Type
--Update PA_PARAMETER SET IsActive=0 where PA_PARAMETER.ParameterName='Flue Type'	
	
-- Internal > Services > Heating > Update "Boiler Location" to "Location"
Update PA_PARAMETER SET ParameterName='Location' where PA_PARAMETER.ParameterName='Boiler Location'	
-- Internal > Services > Heating > Update "Boiler Manufacturer" to "Manufacturer"
Update PA_PARAMETER SET ParameterName='Manufacturer' where PA_PARAMETER.ParameterName='Boiler Manufacturer'		
-- Internal > Services > Heating > Update "Boiler Condition Rating" to "Condition Rating:"
Update PA_PARAMETER SET ParameterName='Condition Rating' where PA_PARAMETER.ParameterName='Boiler Condition Rating'	
IF NOT EXISTS(Select * from PA_ITEM where PA_ITEM.ItemName='Heating Controls')
BEGIN
	INSERT into PA_ITEM (PA_ITEM.AreaID,PA_ITEM.ItemName,PA_ITEM.ItemSorder,PA_ITEM.IsActive,PA_ITEM.ParentItemId,PA_ITEM.ShowInApp,PA_ITEM.ShowListView,PA_ITEM.IsForSchemeBlock)
	Select PA_ITEM.AreaID,'Heating Controls',PA_ITEM.ItemSorder,PA_ITEM.IsActive,PA_ITEM.ParentItemId,PA_ITEM.ShowInApp,PA_ITEM.ShowListView,PA_ITEM.IsForSchemeBlock from PA_ITEM where PA_ITEM.ItemName='Heating'
END	

	
DECLARE @ItemId INT;
DECLARE @SOrder AS INTEGER
DECLARE @ParameterId int,@heatingFuel int, @airSource int,@bioMass  int,@electric  int,@groundSource  int,
@mainsElectric  int,@mainsGas int,@MVHR int,@Oil int, @PV int, @unventedCylinder int,@wetElectric int,@solar int,@E7 int,@LPG int,@Wind int

Select @heatingFuel= PA_PARAMETER.ParameterID from PA_PARAMETER where PA_PARAMETER.ParameterName='Heating Fuel'


---==============================================================================================================
SELECT @ItemId=PA_ITEM.ItemID from PA_ITEM where ItemName = 'Heating'
----------- Internal > Services > Heating > Heating Fuel > Update "E7" to "Unvented cylinder"

--Update PA_PARAMETER_VALUE set ValueDetail='Unvented cylinder' where PA_PARAMETER_VALUE.ValueDetail='E7' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
--Update PA_PARAMETER_VALUE set ValueDetail='MVHR & Heat Recovery' where PA_PARAMETER_VALUE.ValueDetail='LPG' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
--Update PA_PARAMETER_VALUE set ValueDetail='PV' where PA_PARAMETER_VALUE.ValueDetail='Wind' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Unvented cylinder' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'Unvented cylinder',12,1,1)
END
IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='MVHR & Heat Recovery' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'MVHR & Heat Recovery',12,1,1)
END
IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='PV' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'PV',12,1,1)
END
IF NOT EXISTS(Select * from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Wet Electric' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel)
BEGIN
	INSERT into PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES( @heatingFuel,'Wet Electric',12,1,1)
END

--============================================================================================================================
Select @E7=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='E7' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @LPG=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='LPG' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @Wind=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Wind' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel

Select @mainsGas=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Mains Gas' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @airSource=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Air Source' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @bioMass=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Biomass' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @electric=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Electric' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @groundSource=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Ground Source' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @mainsElectric=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Mains Electric' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @MVHR=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='MVHR & Heat Recovery' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @Oil=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Oil' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @PV=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='PV' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @unventedCylinder=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Unvented cylinder' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @wetElectric=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Wet Electric' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel
Select @solar=PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Solar' and PA_PARAMETER_VALUE.ParameterID=@heatingFuel

------------------Populate manufacturer parameter value list------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Albion' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Albion',1,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Ariston' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Ariston',2,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Boulton' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Boulton',3,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Gledhill' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Gledhill',4,1,1)
END

IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Gledhill' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Gledhill',5,1,1)
END

IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Grandee' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Grandee',6,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='GAH' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'GAH',7,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Grant' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Grant',7,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Greenwood' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Greenwood',8,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Heatrae Sadia' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Heatrae Sadia',9,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Heatstar' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Heatstar',10,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Kingspan' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Kingspan',11,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Maxistove' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Maxistove',12,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='McKenzie' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'McKenzie',13,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Mitsubishi' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Mitsubishi',14,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Newark' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Newark',15,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='NIBE' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'NIBE',16,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Nuaire' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Nuaire',17,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Perlight' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Perlight',18,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Powerflow' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Powerflow',19,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Quantum' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Quantum',20,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Range Tribune' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Range Tribune',21,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Santon' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Santon',22,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Symphony' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Symphony',23,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Vaillant' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Vaillant',24,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Viridian' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Viridian',25,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Warmflow' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Warmflow',26,1,1)
END
IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Worcester' and PA_PARAMETER_VALUE.ParameterID=@manufacturerId and IsAlterNativeHeating=1)
BEGIN
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive,IsAlterNativeHeating)VALUES(@manufacturerId,'Worcester',27,1,1)
END

---=======================================================================================================================================

---=================Update PA_ITEM_PARAMETER for Main Gas===============================
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @mainsGas)
BEGIN
	UPDATE PA_ITEM_PARAMETER SET ParameterValueId = @mainsGas Where PA_ITEM_PARAMETER.ItemParamID IN (
	Select PA_ITEM_PARAMETER.ItemParamID from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel')
	)
END
---======================Insert data into PA_ITEM_PARAMETER for Air Source
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @airSource)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@airSource from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Solar
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @solar)
BEGIN

	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@solar from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Ground Source
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @groundSource)
BEGIN

	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@groundSource from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for MVHR & Heat Recovery
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @MVHR)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@MVHR from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Unvented Cylinder
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @unventedCylinder)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@unventedCylinder from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for PV
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @PV)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@PV from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Wet Electric
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @wetElectric)
BEGIN

	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@wetElectric from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Biomass
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @bioMass)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@bioMass from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Electrics
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @electric)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@electric from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for Mains Electrics

IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @mainsElectric)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@mainsElectric from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END

---======================Insert data into PA_ITEM_PARAMETER for Mains Electrics

IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @E7)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@E7 from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @LPG)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@LPG from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END

IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @Wind)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@Wind from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
---======================Insert data into PA_ITEM_PARAMETER for OIL
IF NOT EXISTS(Select 1 from PA_ITEM_PARAMETER where ParameterValueId = @Oil)
BEGIN
	INSERT INTO PA_ITEM_PARAMETER (ItemId,ParameterId, IsActive,ParameterValueId)
	Select @itemid as ItemID,PA_ITEM_PARAMETER.ParameterId,PA_ITEM_PARAMETER.IsActive,@Oil from PA_PARAMETER
	INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	where PA_ITEM_PARAMETER.ItemId=@itemid AND
	PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND PA_PARAMETER.ParameterName NOT IN ('Heating Fuel','GC Number','Gasket Set Replacement','Flue Type','Manufacturer','Model','Serial No')
	AND PA_ITEM_PARAMETER.ParameterValueId = @mainsGas
	Order by PA_PARAMETER.ParameterSorder ASC
END
-------------------Add Drain Back -----------------------
		DECLARE	@ParameterName VARCHAR(50) = 'Drain Back'
				,@DataType VARCHAR(50) = 'String'
				,@ControlType VARCHAR(50) = 'Checkboxes'
				,@IsDate BIT = 0
				,@Active BIT = 1
				,@ShowInApp BIT = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Installed date%' ) + 1
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)
			VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId) VALUES ( @ItemId, @ParameterId, @Active,@solar)
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId) VALUES ( @ItemId, @ParameterId, @Active,@MVHR)
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
				BEGIN
					INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'',1,1)
				END

		END
--------------------------------------------------Add Pressured Check Box----------------------------------------------------------------------------------	
	
			Select	@ParameterName  = 'Pressured'
				,@DataType  = 'String'
				,@ControlType  = 'Checkboxes'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
			SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Drain Back%' ) + 1
			
				INSERT INTO PA_PARAMETER(ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp) VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
			
			SET @ParameterId  = SCOPE_IDENTITY()
			
			INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@solar)
			INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId) VALUES ( @ItemId, @ParameterId, @Active,@MVHR)
			IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
				BEGIN
					INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'',1,1)
				END
		END
	
--------------------------------------------------Add No of Panel----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'No. Of Panels'
				,@DataType  = 'Integer'
				,@ControlType  = 'TextBox'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Drain Back%' ) + 2
		
			INSERT INTO PA_PARAMETER(ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp) VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@solar)
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId) VALUES ( @ItemId, @ParameterId, @Active,@MVHR)
		END
--------------------------------------------------Add Filter Set Replacement----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Filter Set Replacement'
				,@DataType  = 'Date'
				,@ControlType  = 'Date'
				,@IsDate  = 1
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Drain Back%' ) + 1
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@MVHR)
		END		

--------------------------------------------------Add Appliance Make----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Appliance Make'
				,@DataType  = 'String'
				,@ControlType  = 'Textbox'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 1
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)
		END		

--------------------------------------------------Add Appliance Model----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Appliance Model'
				,@DataType  = 'String'
				,@ControlType  = 'Textbox'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 2
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)
		END		

--------------------------------------------------Add Appliance Serial Number----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Appliance Serial Number'
				,@DataType  = 'String'
				,@ControlType  = 'Textbox'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 3
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)
		END		

--------------------------------------------------Add Burner Make----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Burner Make'
				,@DataType  = 'String'
				,@ControlType  = 'Textbox'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 6
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)
		END		
--------------------------------------------------Add Burner Model----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Burner Model'
				,@DataType  = 'String'
				,@ControlType  = 'Textbox'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 7
		
			INSERT INTO PA_PARAMETER(ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp) VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)
		END	

--------------------------------------------------Add Burner Type----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Burner Type'
				,@DataType  = 'String'
				,@ControlType  = 'Dropdown'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 8
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)
			VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		print (@ParameterId)
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)

		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='PJ' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'PJ',1,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Vap(S)' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Vap(S)',2,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Vap(P)' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Vap(P)',3,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Wallflame' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Wallflame',4,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Please Select' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Please Select',0,1)
			END	
			
			
		END	

--------------------------------------------------Add Burner Type----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Tank Type'
				,@DataType  = 'String'
				,@ControlType  = 'Dropdown'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 9
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)

		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Metal' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Metal',1,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Plastic' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Plastic',2,1)
			END
		
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Plastic & Bunded' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Plastic & Bunded',4,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Please Select' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Please Select',0,1)
			END	
			
			
		END	
--------------------------------------------------Add Tank Type----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Tank Type'
				,@DataType  = 'String'
				,@ControlType  = 'Dropdown'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 9
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)

		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Metal' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Metal',1,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Plastic' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Plastic',2,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Bunded' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Bunded',3,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Plastic & Bunded' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Plastic & Bunded',4,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Please Select' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Please Select',0,1)
			END	
			
			
		END	
--------------------------------------------------Add Flue Type----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Flue Type'
				,@DataType  = 'String'
				,@ControlType  = 'Dropdown'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (Select PA_ITEM_PARAMETER.ItemParamID from PA_PARAMETER
					INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
					where PA_ITEM_PARAMETER.ItemId=@ItemId AND
					PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND
					 PA_PARAMETER.ParameterName IN ('Flue Type') AND PA_ITEM_PARAMETER.ParameterValueId=@Oil
					)
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 9
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp) VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)

		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='CF' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'CF',1,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='BF' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'BF',2,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='LLD' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'LLD',3,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Please Select' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Please Select',0,1)
			END	
			
			
		END	
--------------------------------------------------Add Fuel Type----------------------------------------------------------------------------------	
		Select	@ParameterName  = 'Fuel Type'
				,@DataType  = 'String'
				,@ControlType  = 'Dropdown'
				,@IsDate  = 0
				,@Active  = 1
				,@ShowInApp  = 1
	
	IF NOT EXISTS (Select PA_ITEM_PARAMETER.ItemParamID from PA_PARAMETER
					INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
					where PA_ITEM_PARAMETER.ItemId=@ItemId AND
					PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 AND
					 PA_PARAMETER.ParameterName IN ('Fuel Type') AND PA_ITEM_PARAMETER.ParameterValueId=@Oil
					)
		BEGIN	
		SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%Heating Type%' ) + 9
		
			INSERT INTO PA_PARAMETER (ParameterName,DataType,ControlType,IsDate,ParameterSorder,IsActive,ShowInApp)VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
		
		INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId,IsActive,ParameterValueId)  VALUES ( @ItemId, @ParameterId, @Active,@Oil)

		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='C2' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'C2',1,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='D' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'D',2,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Biofuel' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Biofuel',3,1)
			END
		IF NOT EXISTS(SELECT 1 From PA_PARAMETER_VALUE where PA_PARAMETER_VALUE.ValueDetail='Please Select' and PA_PARAMETER_VALUE.ParameterID=@ParameterId )
			BEGIN
				INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive)VALUES(@ParameterId,'Please Select',0,1)
			END	
			
			
		END	

IF @@TRANCOUNT > 0
	BEGIN  
	print'commit'   
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH