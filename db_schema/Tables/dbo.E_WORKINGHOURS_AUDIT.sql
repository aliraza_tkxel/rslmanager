CREATE TABLE [dbo].[E_WORKINGHOURS_AUDIT]
(
[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PK] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wid] [int] NULL,
[EmployeeId] [int] NULL,
[FieldName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValue] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDate] [datetime] NULL,
[UserName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastActionUser] [int] NULL,
[BatchId] [uniqueidentifier] NULL,
[AppName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HostName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
