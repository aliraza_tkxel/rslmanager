CREATE TABLE [dbo].[H_FilterDates]
(
[DateID] [int] NOT NULL IDENTITY(1, 1),
[DateTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
