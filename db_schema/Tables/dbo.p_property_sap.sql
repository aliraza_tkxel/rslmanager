CREATE TABLE [dbo].[p_property_sap]
(
[sid] [int] NOT NULL IDENTITY(1, 1),
[propertyid] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[housenumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sap] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[p_property_sap] TO [rackspace_datareader]
GO
