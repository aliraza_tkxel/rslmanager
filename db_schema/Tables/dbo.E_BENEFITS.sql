Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_BENEFITS')
BEGIN


		CREATE TABLE [dbo].[E_BENEFITS]
		(
		[BENEFITID] [int] NOT NULL IDENTITY(1, 1),
		[EMPLOYEEID] [int] NOT NULL,
		[SALARYPERCENT] [float] NULL,
		[CONTRIBUTION] [money] NULL,
		[EMPLOYEECONTRIBUTION] [money] NULL,
		[EMPLOYERCONTRIBUTION] [money] NULL,
		[AVC] [money] NULL,
		[CONTRACTEDOUT] [int] NULL,
		[LISTPRICE] [money] NULL,
		[EXCESSCONTRIBUTION] [money] NULL,
		[CARALLOWANCE] [money] NULL,
		[CARLOANSTARTDATE] [smalldatetime] NULL,
		[TERM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VALUE] [money] NULL,
		[PROFESSIONALFEES] [money] NULL,
		[TELEPHONEALLOWANCE] [money] NULL,
		[FIRSTAIDALLOWANCE] [money] NULL,
		[CALLOUTALLOWANCE] [money] NULL,
		[MEMNUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SCHEME] [int] NULL,
		[SCHEMENUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CARMAKE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MODEL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CONTHIRECHARGE] [money] NULL,
		[EMPCONTRIBUTION] [money] NULL,
		[ENGINESIZE] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CARSTARTDATE] [smalldatetime] NULL,
		[CO2EMISSIONS] [float] NULL,
		[FUEL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[COMPEMPCONTRIBUTION] [money] NULL,
		[ALLOWANCE] [money] NULL,
		[MONTHLYREPAY] [money] NULL,
		[ACCOMODATIONRENT] [money] NULL,
		[COUNCILTAX] [money] NULL,
		[HEATING] [money] NULL,
		[LINERENTAL] [money] NULL,
		[ANNUALPREMIUM] [money] NULL,
		[TAXABLEBENEFIT] [money] NULL,
		[MEDICALORGID] [int] NULL,
		[ADDITIONALMEMBERS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ADDITIONALMEMBERS2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ADDITIONALMEMBERS3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ADDITIONALMEMBERS4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ADDITIONALMEMBERS5] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LOANINFORMATION] [int] NULL,
		[DRIVINGLICENCENO] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MOTCERTNO] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[INSURANCENO] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[INSURANCERENEWALDATE] [smalldatetime] NULL,
		[GROUPSCHEMEREF] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MEMBERSHIPNO] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LASTACTIONTIME] [datetime] NULL CONSTRAINT [DF_E_BENEFITS_LASTACTIONTIME] DEFAULT (getdate()),
		[LASTACTIONUSER] [int] NULL	
		) ON [PRIMARY]
END

IF COL_LENGTH('E_BENEFITS', 'DrivingLicenceImage') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD DrivingLicenceImage NVARCHAR(max) NULL
			Print('Added DrivingLicenceImage') 
		END	


IF COL_LENGTH('E_BENEFITS', 'ECUTopUp') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD ECUTopUp MONEY NULL
			Print('Added ECU Top Up') 
		END	

IF COL_LENGTH('E_BENEFITS', 'MOTRenewalDate') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD MOTRenewalDate SMALLDATETIME NULL
			Print('Added MOT Renewal Date') 
		END	
IF COL_LENGTH('E_BENEFITS', 'AdditionalDriver') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD AdditionalDriver NVARCHAR(500)
			Print('Added Additional Driver') 
		END	
IF COL_LENGTH('E_BENEFITS', 'InsuranceRenewalDate') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD InsuranceRenewalDate SMALLDATETIME NULL
			Print('Added MOT Renewal Date') 
		END	
IF COL_LENGTH('E_BENEFITS', 'ToolAllowance') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD ToolAllowance MONEY NULL
			Print('Added Tool Allowance') 
		END			
IF COL_LENGTH('E_BENEFITS', 'FirstAid') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD FirstAid MONEY NULL
			Print('Added First Aid') 
		END			

IF COL_LENGTH('E_BENEFITS', 'UnionSubscription') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD UnionSubscription MONEY NULL
			Print('Added Union Subscription') 
		END	

IF COL_LENGTH('E_BENEFITS', 'LifeAssurance') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD LifeAssurance MONEY NULL
			Print('Added Life Assurance') 
		END	

IF COL_LENGTH('E_BENEFITS', 'EnhancedHolidayEntitlement') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD EnhancedHolidayEntitlement INT NULL
			Print('Added Enhanced Holiday Entitlement') 
		END	

IF COL_LENGTH('E_BENEFITS', 'CarParkingFacilities') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD CarParkingFacilities MONEY NULL
			Print('Added Car Parking Facilities') 
		END	

IF COL_LENGTH('E_BENEFITS', 'EyeCareAssistance ') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD EyeCareAssistance MONEY NULL
			Print('Added Eye Care Assistance ') 
		END	
IF COL_LENGTH('E_BENEFITS', 'EmployeeAssistanceProgramme ') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD EmployeeAssistanceProgramme NVARCHAR(500) NULL
			Print('Added Employee Assistance Programme ') 
		END	

IF COL_LENGTH('E_BENEFITS', 'EnhancedSickPay ') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD EnhancedSickPay MONEY NULL
			Print('Added Enhanced Sick Pay ') 
		END	


IF COL_LENGTH('E_BENEFITS', 'LearningAndDevelopment') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD LearningAndDevelopment MONEY NULL
			Print('Added Learning and Development  ') 
		END	

IF COL_LENGTH('E_BENEFITS', 'ProfessionalSubscriptions') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD ProfessionalSubscriptions MONEY NULL
			Print('Added Professional Subscriptions  ') 
		END	
IF COL_LENGTH('E_BENEFITS', 'ChildcareVouchers') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD ChildcareVouchers MONEY NULL
			Print('Added Childcare Vouchers  ') 
		END
IF COL_LENGTH('E_BENEFITS', 'FluAndHepBJabs') IS NULL
		BEGIN
			ALTER TABLE E_BENEFITS 
			ADD FluAndHepBJabs MONEY NULL
			Print('Added Flu & Hep B Jabs  ') 
		END



		
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
