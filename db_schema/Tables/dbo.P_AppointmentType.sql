USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = 'P_AppointmentType')
BEGIN


CREATE TABLE [dbo].[P_AppointmentType]
(
	[AppointmentTypeId] [int] IDENTITY(0,1) PRIMARY KEY NOT NULL,
	[Description] [nvarchar](100) NULL
)

INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'All')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Adaptations')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Condition')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Gas')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Miscellaneous')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Planned')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Reactive')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Stock')
INSERT [dbo].[P_AppointmentType] ([Description]) VALUES (N'Voids')
PRINT 'Table Created'
END

ELSE
BEGIN
PRINT 'Table already exist..'
END



IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH