CREATE TABLE [dbo].[GS_PropertyAgent_to_Appointment]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PropertyAgentID] [int] NOT NULL,
[AppointmentID] [int] NOT NULL,
[Date] [smalldatetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] ADD 
CONSTRAINT [PK_GS_PropertyAgent_to_Appointment] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] ADD
CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_Appointment] FOREIGN KEY ([AppointmentID]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] ADD
CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_PropertyAgent] FOREIGN KEY ([PropertyAgentID]) REFERENCES [dbo].[PS_PropertyAgent] ([ID])
GO
