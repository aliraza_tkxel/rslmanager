/****** Object:  Table [dbo].[NL_GENERALJOURNAL]    Script Date: 16-Jan-17 5:39:23 PM ******/
Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PDR_AttributesType')
	BEGIN
		CREATE TABLE [dbo].[PDR_AttributesType](
		[AttributeTypeId] [int] NOT NULL,
		[AttributeType] [nvarchar](50) NULL,
		[IsActive] [bit] NULL,
		CONSTRAINT [PK_PDR_AttributesType] PRIMARY KEY CLUSTERED 
		(
		[AttributeTypeId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END

	IF NOT EXISTS (Select 1 from PDR_AttributesType where AttributeType='Attribute')
	BEGIN 	
		INSERT INTO PDR_AttributesType( AttributeTypeId,AttributeType,IsActive)Values(1,'Attribute',1)
		PRINT 'Title Attribute added successfully'
	END
	
	IF NOT EXISTS (Select 1 from PDR_AttributesType where AttributeType='Provision')
	BEGIN 	
		INSERT INTO PDR_AttributesType( AttributeTypeId,AttributeType,IsActive)Values(2,'Provision',1)
		PRINT 'Title Provision added successfully'
	END

	IF NOT EXISTS (Select 1 from PDR_AttributesType where AttributeType='Other')
	BEGIN 	
		INSERT INTO PDR_AttributesType( AttributeTypeId,AttributeType,IsActive)Values(3,'Other',1)
		PRINT 'Other added successfully'
	END

	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState);
	Print (@ErrorMessage)
END CATCH