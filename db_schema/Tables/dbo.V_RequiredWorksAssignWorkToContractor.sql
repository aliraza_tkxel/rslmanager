USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_RequiredWorksAssignWorkToContractor]    Script Date: 10/03/2017 13:36:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 25/08/2014
-- Description:	Assign Work to Contractor.
-- History:          25/08/2014 AR : Save Purchase order for Required works Assign Work to Contractor
-- =============================================

IF OBJECT_ID('dbo.[V_RequiredWorksAssignWorkToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[V_RequiredWorksAssignWorkToContractor] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[V_RequiredWorksAssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@pdrContractorId INT,
	@inspectionJournalId INT,
	@ContractorId INT,
	@ContactId INT,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@POStatus INT,
	@requiredWorkIds varchar(1000) ,
	@ContractorWorksDetail AS PDR_AssingToContractorWorksRequired READONLY,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


/* Working of this stored Procedure
** 1- Get Status Id of "Assigned To Contractor" from Planned_Status. In case a Status of "Assigned To Contractor" 
**    is not present add a new one and get its Id.

** 2- Insert a new record in F_PURCHASEORDER and get the identity value as Order Id.
**
** 3- Insert a new record in PDR_CONTRACTOR_WORK using the given input data and OrderId
**    and get Identity Value as PDRContractorId.
**
** Loop (Insert Purchase Items and Works Required Items.
**
**   4- Insert a Purchase Item in F_PURCHASEITEM from given an constant data
**      and get Identity Value PURCHASEORDERITEMID
**
**   5- Insert a new work required from given data and also insert PURCHASEORDERITEMID
**
** End Loop
*/


BEGIN TRANSACTION
BEGIN TRY

      -- =====================================================
      -- General Purpose Variable
      -- =====================================================
      DECLARE @PropertyId varchar(100) = NULL,
              @CustomerId int = NULL,
              @TenancyId int = NULL,
              @MSATTypeId int,
              @MSATId int,
              @journalId int,
              @terminationDate datetime,
              @reletDate datetime,
              @MsatType nvarchar(200),
              @DevelopmentId int = NULL,
              @schemeId int = NULL,
              @blockId int = NULL

      -- To save same time stamp in all records 
      DECLARE @CurrentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusId int = NULL

-- =====================================================
-- get status history id of "Assigned To Contractor"
-- =====================================================

      SELECT
        @newStatusId = STATUSID
      FROM PDR_STATUS
      WHERE PDR_STATUS.TITLE = 'Assigned To Contractor'
      -- =============================================    
      -- Get PropertyId,TenancyId and customerId by inspectionJournalId    
      -- =============================================  

      SELECT
        @PropertyId = PropertyId,
        @TenancyId = TenancyId,
        @CustomerId = CustomerId,
        @terminationDate = TerminationDate,
        @reletDate = ReletDate
      FROM PDR_MSAT
      INNER JOIN PDR_JOURNAL
        ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
      WHERE JOURNALID = @inspectionJournalId

      -- =============================================    
      -- Get status id of "Arranged"    and MSATTypeId
      -- =============================================     
      SELECT
        @MSATTypeId = MSATTypeId,
        @MsatType = MSATTypeName
      FROM PDR_MSATType
      WHERE MSATTypeName = 'Void Works'
      -- =============================================  

      SELECT
        @schemeId = SCHEMEID,
        @blockId = BLOCKID
      FROM P__PROPERTY
      WHERE P__PROPERTY.PROPERTYID = @PropertyId

      SELECT
        @DevelopmentId = S.DEVELOPMENTID
      FROM P_SCHEME S
      WHERE S.SCHEMEID = @schemeId

      -- ====================================================================================================    
      --          INSERT PDR_MSAT AND  PDR_JOURNAL    
      -- ====================================================================================================    	


      INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId, TerminationDate, ReletDate)
        VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId, @terminationDate, @reletDate)

SELECT
	@MSATId = SCOPE_IDENTITY()

INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
	VALUES (@MSATId, @newStatusId, GETDATE(), @userId)

      SELECT
        @journalId = SCOPE_IDENTITY()

      UPDATE V_RequiredWorks
      SET WorksJournalId = @journalId,
          IsScheduled = 1,
          StatusId = @newStatusId,
          ModifiedDate = GETDATE()
      WHERE RequiredWorksId IN (SELECT
        COLUMN1
      FROM dbo.SPLIT_STRING(@requiredWorkIds, ','))




IF (@PdrContractorId = -1)
BEGIN

-- =====================================================
-- Insert new Purchase Order
-- =====================================================

        DECLARE @Active bit = 1,
                @POTYPE int = (SELECT
                  POTYPEID
                FROM F_POTYPE
                WHERE POTYPENAME = 'Repair') -- 2 = 'Repair'

                -- To get Identity Value of Purchase Order.
                ,
                @purchaseOrderId int

        INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
        POTYPE, POSTATUS, GASSERVICINGYESNO, BLOCKID, DEVELOPMENTID)
          VALUES (UPPER(@MsatType + ' Work Order'), @CurrentDateTime, 'This purchase order was created for ' + @MsatType + ' from the new ' + @MsatType + ' process.', @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0, @blockId, @DevelopmentId)  

SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================

        INSERT INTO [PDR_CONTRACTOR_WORK] ([JournalId], [ContractorId], [ContactId], [AssignedDate], [AssignedBy], [Estimate], [EstimateRef], [PurchaseOrderId])
          VALUES (@journalId, @ContractorId, @ContactId, @CurrentDateTime, @userId, @Estimate, @EstimateRef, @PurchaseOrderId)
        SET @PdrContractorId = SCOPE_IDENTITY()

      END
      ELSE
      BEGIN
        UPDATE [PDR_CONTRACTOR_WORK]
        SET [ContractorId] = @ContractorId,
            [ContactId] = @ContactId,
            [AssignedDate] = @CurrentDateTime,
            [AssignedBy] = @userId,
            [Estimate] = @Estimate,
            [EstimateRef] = @EstimateRef

        WHERE PDRContractorId = @PdrContractorId
      END


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

      DECLARE worksRequiredCursor CURSOR FOR
      SELECT
        *
      FROM @ContractorWorksDetail
      OPEN worksRequiredCursor

      -- Declare Variable to use with cursor
      DECLARE @WorkDetailId int,
              @ServiceRequired nvarchar(4000),
              @NetCost smallmoney,
              @VatType int,
              @VAT smallmoney,
              @GROSS smallmoney,
              @PIStatus int,
              @ExpenditureId int,
              @CostCenterId int,
              @BudgetHeadId int

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(20) = @MsatType -- Title for Purchase Items, specially to inset in F_PurchaseItem


      -- =====================================================
      -- Loop (Start) through records and insert works required
      -- =====================================================		
      -- Fetch record for First loop iteration.
      FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId, @ServiceRequired, @NetCost, @VatType, @VAT,
      @GROSS, @PIStatus, @ExpenditureId, @CostCenterId, @BudgetHeadId
      WHILE @@FETCH_STATUS = 0
      BEGIN
        IF (@WorkDetailId = -1)
        BEGIN

		Select @CostCenterId=C.COSTCENTREID,  @BudgetHeadId=H.HEADID, @ExpenditureId=E.EXPENDITUREID from F_EXPENDITURE E
			Inner JOIN F_HEAD H on E.HEADID=H.HEADID
			INNER JOIN F_COSTCENTRE C ON H.COSTCENTREID = C.COSTCENTREID
          WHERE C.DESCRIPTION = 'Asset Management' AND H.DESCRIPTION='Responsive' AND E.DESCRIPTION='Voids'
        
		 
		  --SELECT
    --        @CostCenterId = COSTCENTREID
    --      FROM F_COSTCENTRE
    --      WHERE DESCRIPTION LIKE 'Asset Management'
    --      SELECT
    --        @BudgetHeadId = HEADID
    --      FROM F_HEAD
    --      WHERE DESCRIPTION LIKE 'Responsive'
         
		  --SELECT
    --        @ExpenditureId = EXPENDITUREID
    --      FROM F_EXPENDITURE
    --      WHERE DESCRIPTION LIKE 'Voids'
          -- =====================================================
          --Insert Values in F_PURCHASEITEM for each work required and get is identity value.
          -- =====================================================

          INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
          NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
            VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @ServiceRequired, @CurrentDateTime, @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

			DECLARE @ORDERITEMID int = SCOPE_IDENTITY()

			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================

          INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL] ([PDRContractorId], [ServiceRequired], [NetCost], [VatId], [Vat], [Gross], [ExpenditureId], [CostCenterId], [BudgetHeadId], [PURCHASEORDERITEMID])
            VALUES (@pdrContractorId, @ServiceRequired, @NetCost, @VatType, @VAT, @GROSS, @ExpenditureId, @CostCenterId, @BudgetHeadId, @ORDERITEMID)

        END

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId, @ServiceRequired, @NetCost, @VatType, @VAT,
				@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
END

      -- =====================================================
      -- Loop (End) through records and insert works required
      -- =====================================================



      -- =====================================================
      -- Old table(s)
      -- Insert new P_WORKORDER
      -- =====================================================
      DECLARE @Title nvarchar(50) = 'Assigned To Contractor'
      DECLARE @WOSTATUS int = 6-- 'IN PROGRESS
      IF (@POStatus = 0)
        SET @WOSTATUS = 12 --Pending
    

      INSERT INTO P_WORKORDER (ORDERID, TITLE, WOSTATUS, CREATIONDATE,  DEVELOPMENTID, BLOCKID, GASSERVICINGYESNO, SchemeId, PROPERTYID)
        VALUES (@purchaseOrderId, @TITLE, @WOSTATUS, @CurrentDateTime,  @DevelopmentId, @BlockId, 0, @SchemeId, @propertyId)

      DECLARE @WOID int = SCOPE_IDENTITY()

      -- =====================================================  
      -- Old table(s)  
      -- INSERT VALUE IN C_JOURNAL FOR EACH PROPERTY SELECTED ON WORKORDER  
      -- =====================================================  

      DECLARE @ITEMID int = 1 --'PROPERTY'  
      DECLARE @STATUS int = 2 --'ASSIGNED  
      IF (@POStatus = 0)
        SET @STATUS = 12 --Pending  


      INSERT INTO C_JOURNAL (CUSTOMERID, ITEMID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE)
        VALUES (@CustomerId, @ITEMID, @STATUS, @CurrentDateTime, @TITLE)

      DECLARE @CJOURNALID int = SCOPE_IDENTITY()

      -- =====================================================  
      -- Old table(s)  
      --INSERT VALUE IN C_REPAIR FOR EACH PROPERTY SELECTED ON WORKORDER  
      -- =====================================================  

      DECLARE @ITEMACTIONID int = 2 --'ASSIGNED TO CONTRACTOR'  

      IF (@POStatus = 0)
        SET @ITEMACTIONID = 12 --Pending  


      --ITEMDETAILID WILL BE NULL BECAUSE THE COST OF WORKORDER DEPENDS ON PROGRAMME OF PLANNED MAINTENANCE   
      --AND THE SCOPEID WILL ALSO BE NULL BECAUSE IT IS NOT A GAS SERVICING CONTRACT  

      INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, ITEMDETAILID,
      CONTRACTORID, TITLE, NOTES, SCOPEID)
        VALUES (@CJOURNALID, @STATUS, @ITEMACTIONID, @CurrentDateTime, @userId, NULL, @CONTRACTORID, 'FOR Scheme/Block', @TITLE, NULL)

      DECLARE @REPAIRHISTORYID int = SCOPE_IDENTITY()

      -- =====================================================  
      --INSERT VALUE IN P_WOTOREPAIR for each work required  
      -- =====================================================  

      INSERT INTO P_WOTOREPAIR (WOID, JOURNALID, ORDERITEMID)
        VALUES (@WOID, @CJOURNALID, @ORDERITEMID)






CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

      SELECT
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = @journalId

END
