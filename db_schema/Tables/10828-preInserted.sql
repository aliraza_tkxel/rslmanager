Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


DECLARE db_cursor CURSOR FOR 
SELECT DISTINCT PROP_ATTRIB.SchemeId,PROP_ATTRIB.BlockId,PROP_ATTRIB.VALUEID
	FROM PA_PROPERTY_ATTRIBUTES PROP_ATTRIB Where
								PROP_ATTRIB.ITEMPARAMID =
								(
									SELECT
										ItemParamID
									FROM
										PA_ITEM_PARAMETER
											INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
											INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
									WHERE
										ParameterName = 'Heating Fuel'
										AND ItemName = 'Heating'
										AND PARAMETERVALUE = 'Mains Gas'
								)
AND (PROP_ATTRIB.SchemeId IS NOT NULL OR PROP_ATTRIB.BlockId is not NULL)
 
DECLARE @propertyId VARCHAR(20);
DECLARE @schemeId INT;
DECLARE @blockId INT;
DECLARE @valueId INT;
DECLARE @heatingMappingId int;
declare @itemid int;
SELECT @itemid=PA_ITEM.ItemID from PA_ITEM where ItemName = 'Heating'

OPEN db_cursor;
FETCH NEXT FROM db_cursor INTO @schemeId,@blockId, @valueId;
WHILE @@FETCH_STATUS = 0  
BEGIN  
	
	--==========Insert into PA_HeatingMapping 
		
		  INSERT INTO PA_HeatingMapping(HeatingType,SchemeID,BlockID,IsActive)VALUES(@valueId,@schemeId,@blockId,1)

		  SET @heatingMappingId  = SCOPE_IDENTITY()
	--==========Update heating mapping id in PA_Property_Attributes
		UPDATE PA_PROPERTY_ATTRIBUTES   SET HeatingMappingId =@heatingMappingId
		Where PA_PROPERTY_ATTRIBUTES.ITEMPARAMID IN (Select PA_ITEM_PARAMETER.ItemParamID from PA_PARAMETER
			INNER JOIN PA_ITEM_PARAMETER on PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
			where PA_ITEM_PARAMETER.ItemId=@itemid AND
			PA_PARAMETER.IsActive=1 and PA_ITEM_PARAMETER.IsActive=1 
			AND PA_ITEM_PARAMETER.ParameterValueId = (Select PA_PARAMETER_VALUE.ValueID from PA_PARAMETER_VALUE where ValueDetail = 'Mains Gas' and PA_PARAMETER_VALUE.ParameterId =
			(Select ParameterId from PA_PARAMETER where PA_PARAMETER.ParameterName IN ('Heating Fuel'))
			 )
			 OR  PA_ITEM_PARAMETER. ParameterId=( Select ParameterId from PA_PARAMETER where PA_PARAMETER.ParameterName IN ('Heating Fuel'))		 
			)
			AND (PA_PROPERTY_ATTRIBUTES.SchemeId IS NOT NULL OR PA_PROPERTY_ATTRIBUTES.BlockId is not NULL)
	--==========Update heating mapping id in PA_PROPERTY_ITEM_DATES
		Update PA_PROPERTY_ITEM_DATES SET HeatingMappingId=@heatingMappingId where (PA_PROPERTY_ITEM_DATES.SchemeId IS NOT NULL OR PA_PROPERTY_ITEM_DATES.BlockId is not NULL)
		AND PA_PROPERTY_ITEM_DATES.ItemId=@itemid
		
	--==========Update heating mapping id in AS_Journal
	INSERT INTO AS_JOURNAL(AS_JOURNAL.STATUSID,AS_JOURNAL.ACTIONID,AS_JOURNAL.INSPECTIONTYPEID,AS_JOURNAL.CREATIONDATE,AS_JOURNAL.CREATEDBY,AS_JOURNAL.ISCURRENT,AS_JOURNAL.HeatingMappingId)
	Values(1,2,1,GETDATE(),423,1,@heatingMappingId)
	DECLARE @journalId int;
	SET @journalId  = SCOPE_IDENTITY()
	INSERT INTO AS_JOURNALHISTORY(JOURNALID,STATUSID,ACTIONID,INSPECTIONTYPEID,CREATIONDATE,CREATEDBY,HeatingMappingId)
	Values(@journalId,1,2,1,GETDATE(),423,@heatingMappingId)

	--Update AS_JOURNAL SET HeatingMappingId = @heatingMappingId where AS_JOURNAL.PROPERTYID=@propertyId and AS_JOURNAL.ISCURRENT=1
	--Update AS_JOURNALHISTORY set HeatingMappingId=@heatingMappingId where AS_JOURNALHISTORY.PROPERTYID =@propertyId
		
	--==========Update heating mapping id in P_LGSR
	--	Update P_LGSR SET HeatingMappingId= @heatingMappingId where P_LGSR.PROPERTYID=@propertyId 





	 FETCH NEXT FROM db_cursor INTO @schemeId,@blockId, @valueId;
END;
CLOSE db_cursor;
DEALLOCATE db_cursor;


IF @@TRANCOUNT > 0
	BEGIN  
	print'commit'   
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


Select * from AS_JOURNAL