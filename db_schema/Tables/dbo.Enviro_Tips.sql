CREATE TABLE [dbo].[Enviro_Tips]
(
[TipsId] [int] NOT NULL IDENTITY(1, 1),
[TipsTitle] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipsDescription] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreationDate] [smalldatetime] NULL,
[CreatedBy] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Enviro_Tips] ADD 
CONSTRAINT [PK_Enviro_Tips] PRIMARY KEY CLUSTERED  ([TipsId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
