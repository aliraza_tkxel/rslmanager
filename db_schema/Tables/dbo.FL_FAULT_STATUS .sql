Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Fl_FAULT_STATUS')
BEGIN
  PRINT 'Table Exists';
IF NOT EXISTS (SELECT 1 FROM Fl_FAULT_STATUS fs WHERE fs.Description = 'Accepcted by Contractor')
	BEGIN
		insert into Fl_FAULT_STATUS (Description,Active) values
			('Accepcted by Contractor',1)
	PRINT 'Accepcted by Contractor status added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Accepcted by Contractor status already exits'
	END	

IF NOT EXISTS (SELECT 1 FROM Fl_FAULT_STATUS fs WHERE fs.Description = 'Rejected by Contractor')
	BEGIN
		insert into Fl_FAULT_STATUS (Description,Active) values
			('Rejected by Contractor',1)
	PRINT 'Rejected by Contractor status added successfully!'
	END--if
ELSE
	BEGIN 
		PRINT 'Rejected by Contractor status already exits'
	END	


IF NOT EXISTS (	SELECT	1 
				FROM	Fl_FAULT_STATUS fs 
				WHERE fs.Description = 'Accepted')
	BEGIN
		INSERT INTO Fl_FAULT_STATUS (Description,Active) 
		VALUES ('Accepted',1)
	PRINT 'Accepted status added successfully!'
	END
ELSE
	BEGIN 
		PRINT 'Accepted status already exits'
	END	


END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 
 
 