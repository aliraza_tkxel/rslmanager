CREATE TABLE [dbo].[AS_NoEntry]
(
[NoEntryID] [int] NOT NULL IDENTITY(1, 1),
[JournalId] [int] NOT NULL,
[isCardLeft] [bit] NOT NULL,
[RecordedDate] [smalldatetime] NOT NULL,
[RecordedBy] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_NoEntry] ADD 
CONSTRAINT [PK_AS_NoEntry] PRIMARY KEY CLUSTERED  ([NoEntryID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
