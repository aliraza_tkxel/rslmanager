CREATE TABLE [dbo].[P_ATTTOSCHEME]
(
[PROPERTYID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SID] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_ATTTOSCHEME] TO [rackspace_datareader]
GO
