CREATE TABLE [web].[neighbourhoodTeam]
(
[id] [int] NOT NULL,
[scheme_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[area] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[town] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[county] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[postcode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[local_auth] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[area_office] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[area_manager] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[neighbourhood_officer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[scheme_manager] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[scheme_supervisor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[project_worker] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[project_scheme] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[local_area_advisor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[rent_officer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[supervisor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[gmo] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
