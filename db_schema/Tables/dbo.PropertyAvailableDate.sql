CREATE TABLE [dbo].[PropertyAvailableDate]
(
[SID] [int] NOT NULL IDENTITY(1, 1),
[PROPERTYID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE1] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE4] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE5] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE6] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE7] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE8] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE9] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE10] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE11] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE12] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE13] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE14] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETDATE15] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
