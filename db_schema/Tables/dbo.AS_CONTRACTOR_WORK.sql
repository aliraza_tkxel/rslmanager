
--check table existance
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AS_CONTRACTOR_WORK')
BEGIN
CREATE TABLE AS_CONTRACTOR_WORK
	(
		ApplianceServicingContractorId int primary key  not null identity(1,1),
		journalId int not null,
		ContractorId int not null,
		AssignedDate datetime not null,
		AssignedBy nvarchar(50) not null,
		EstimateRef nvarchar(400) null,
		ContactId int not null,
		PurchaseOrder int not null
	)
  PRINT 'Table Created';
END
--if table not exist
ELSE IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AS_CONTRACTOR_WORK')
BEGIN

PRINT 'Table already exist.'

END





