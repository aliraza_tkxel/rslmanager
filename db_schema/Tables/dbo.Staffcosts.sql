CREATE TABLE [dbo].[Staffcosts]
(
[idd] [int] NOT NULL IDENTITY(1, 1),
[thedate] [smalldatetime] NULL,
[cheqno] [int] NULL,
[amount] [money] NULL,
[expid] [int] NULL,
[sname] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[costcentre] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[head] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[expend] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORDERITEMID] [int] NULL
) ON [PRIMARY]
GO
