Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'G_TRADE')
BEGIN


CREATE TABLE [dbo].[G_TRADE](
	[TradeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_G_TRADE] PRIMARY KEY CLUSTERED 
(
	[TradeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


END


--==============================================================================
-- Ticket # 10923 Adding new trades ('Glazer','Drain Specialist','Roofer','Plumber (level access shower)')
--==============================================================================

IF  EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Glazer'))
		BEGIN 	
			Update G_TRADE set Description = 'Glazer (Full)' WHERE DESCRIPTION IN ('Glazer')
		END
IF  EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Drain Specialist'))
		BEGIN 	
			Update G_TRADE set Description = 'Drain Specialist (Full)' WHERE DESCRIPTION IN ('Drain Specialist')
		END
IF  EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Roofer'))
		BEGIN 	
			Update G_TRADE set Description = 'Roofer (Full)' WHERE DESCRIPTION IN ('Roofer')
		END
IF  EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Plumber (level access shower)'))
		BEGIN 	
			Update G_TRADE set Description = 'Plumber (level access shower) (Full)' WHERE DESCRIPTION IN ('Plumber (level access shower)')
		END


IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Plumber (level access shower) (Part)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Plumber (level access shower) (Part)')
		END
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Drain Specialist (Part)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Drain Specialist (Part)')
		END
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Glazer (Part)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Glazer (Part)')
		END
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Roofer (Part)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Roofer (Part)')	
		END
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Planned Kitchen'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Planned Kitchen')	
		END	
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Planned Bathroom'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Planned Bathroom')	
		END		


--If Executing script first time
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Plumber (level access shower) (Full)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Plumber (level access shower) (Full)')
		END
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Drain Specialist (Full)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Drain Specialist (Full)')
		END
IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Glazer (Full)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Glazer (Full)')
		END
		IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Roofer (Full)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Roofer (Full)')	
		END

		IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION = ('Oil (Part)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Oil (Part)')	
			Print ('Successfully Added Oil (Part)')
		END

		IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Oil (Full)'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Oil (Full)')
			Print ('Successfully Added Oil (Full)')	
		END

		IF NOT EXISTS (	SELECT	1 FROM	G_TRADE WHERE DESCRIPTION IN ('Renewable Qualified'))
		BEGIN 	
			INSERT INTO [dbo].[G_TRADE]([Description]) VALUES ('Renewable Qualified')
			Print ('Successfully Added Renewable Qualified')	
		END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH