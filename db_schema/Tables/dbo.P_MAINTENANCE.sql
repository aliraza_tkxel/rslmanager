CREATE TABLE [dbo].[P_MAINTENANCE]
(
[MAINTENANCEID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCHEME] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CATEGORY] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELEMENT] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TITLE] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUILDYEAR] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REHABYEAR] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LASTEXECUTED] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_MAINTENANCE] TO [rackspace_datareader]
GO
