CREATE TABLE [dbo].[z_tenancydates]
(
[Tenancyid] [float] NULL,
[Propertyid] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[startdate] [smalldatetime] NULL,
[enddate] [smalldatetime] NULL
) ON [PRIMARY]
GO
