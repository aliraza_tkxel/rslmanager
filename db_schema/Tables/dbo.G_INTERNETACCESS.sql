USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'G_INTERNETACCESS')
BEGIN
CREATE TABLE [dbo].[G_INTERNETACCESS](
	[INTERNETACCESSID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [nvarchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[INTERNETACCESSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Home Computer')
INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Work Computer')
INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'No Access')
INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Other')
INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Mobile Phone')
INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Tablet Device')
INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Digital TV')

PRINT 'Table G_INTERNETACCESS created'
END
ELSE
BEGIN
PRINT 'Table G_INTERNETACCESS already exist'
IF NOT EXISTS (Select 1 from G_INTERNETACCESS where DESCRIPTION= 'Public')
	BEGIN  
	INSERT [dbo].[G_INTERNETACCESS] ([DESCRIPTION]) VALUES (N'Public')
	PRINT 'DESCRIPTION Public added successfully'	
	END

END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH

