USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY
/*
   Thursday, December 15, 20162:05:37 PM
   User: TkXel
   Server: 10.0.2.19
   Database: RSLBHALive
   Application: 
*/

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'FLS_LandLordType')
	BEGIN

		/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
		BEGIN TRANSACTION
		SET QUOTED_IDENTIFIER ON
		SET ARITHABORT ON
		SET NUMERIC_ROUNDABORT OFF
		SET CONCAT_NULL_YIELDS_NULL ON
		SET ANSI_NULLS ON
		SET ANSI_PADDING ON
		SET ANSI_WARNINGS ON
		COMMIT
		BEGIN TRANSACTION

		CREATE TABLE dbo.FLS_LandLordType
			(
			LandLordTypeId int NOT NULL IDENTITY (1, 1),
			Title nvarchar(500) NOT NULL,
			IsActive bit NOT NULL
			)  ON [PRIMARY]

		ALTER TABLE dbo.FLS_LandLordType ADD CONSTRAINT
			DF_FLS_LandLordType_IsActive DEFAULT 0 FOR IsActive

		ALTER TABLE dbo.FLS_LandLordType ADD CONSTRAINT
			PK_FLS_LandLordType PRIMARY KEY CLUSTERED 
			(
			LandLordTypeId
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


		ALTER TABLE dbo.FLS_LandLordType SET (LOCK_ESCALATION = TABLE)
		COMMIT
End
ELSE
	BEGIN
	print 'FLS_LandLordType already Exist'
	END

IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
GO
--==========================================================================================
--============================ Insertion Script ============================================


BEGIN TRANSACTION
BEGIN TRY

	IF NOT EXISTS (	SELECT	1 FROM	FLS_LandLordType WHERE  Title = 'Council')
		BEGIN 	
			INSERT INTO FLS_LandLordType(Title, IsActive)
			VALUES('Council',1)
		END
		
	IF NOT EXISTS (	SELECT	1 FROM	FLS_LandLordType WHERE  Title = 'Private')
		BEGIN 	
			INSERT INTO FLS_LandLordType(Title, IsActive)
			VALUES('Private',1)
		END
	IF NOT EXISTS (	SELECT	1 FROM	FLS_LandLordType WHERE  Title = 'Social')
		BEGIN 	
			INSERT INTO FLS_LandLordType(Title, IsActive)
			VALUES('Social',1)
		END		
		
		
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH
