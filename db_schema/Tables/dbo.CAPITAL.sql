CREATE TABLE [dbo].[CAPITAL]
(
[capitalid] [int] NOT NULL IDENTITY(1, 1),
[Location] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[item] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[element] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Repair Details] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[priority] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[amount] [money] NULL,
[F7] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
