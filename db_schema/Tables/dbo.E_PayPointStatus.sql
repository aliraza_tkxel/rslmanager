Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'E_PayPointStatus')
BEGIN

CREATE TABLE [dbo].[E_PayPointStatus](
	[PayPointStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_E_PayPointStatus] PRIMARY KEY CLUSTERED 
(
	[PayPointStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END --if

IF NOT EXISTS (SELECT * FROM E_PayPointStatus WHERE Description = 'Submitted')
BEGIN
	insert into E_PayPointStatus (Description) VALUES('Submitted')
	PRINT 'Submitted added successfully'
END

IF NOT EXISTS (SELECT * FROM E_PayPointStatus WHERE Description = 'Declined')
BEGIN
	insert into E_PayPointStatus (Description) VALUES('Declined')
	PRINT 'Declined added successfully'
END

IF NOT EXISTS (SELECT * FROM E_PayPointStatus WHERE Description = 'Supported')
BEGIN
	insert into E_PayPointStatus (Description) VALUES('Supported')
	PRINT 'Supported added successfully'
END

IF NOT EXISTS (SELECT * FROM E_PayPointStatus WHERE Description = 'Waiting Submission')
BEGIN
	insert into E_PayPointStatus (Description) VALUES('Waiting Submission')
	PRINT 'Waiting Submission added successfully'
END

IF EXISTS (SELECT * FROM E_PayPointStatus WHERE Description = 'Authorized')
BEGIN
	Update E_PayPointStatus set Description = 'Authorised' WHERE Description = 'Authorized'
	PRINT 'Authorised updated successfully'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH 