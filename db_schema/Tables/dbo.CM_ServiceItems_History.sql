USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'CM_ServiceItems_History')
BEGIN

	CREATE TABLE [dbo].[CM_ServiceItems_History](
		[ServiceItemHistoryId] [int] IDENTITY(1,1) NOT NULL,
		[ServiceItemId] [int],
		[ItemId] [int] NULL,
		[SchemeId] [int] NULL,
		[BlockId] [int] NULL,
		[ContractorId] [int] NULL,
		[ContractCommencement] [smalldatetime] NULL,
		[ContractPeriod] [int] NULL,
		[ContractPeriodType] [int] NULL,
		[Cycle] [int] NULL,
		[CycleType] [int] NULL,
		[CycleValue] money NULL,
		[PORef] int NULL,
		[StatusId] int NULL,
		[CreatedDate] [datetime] NULL,
		[CreatedBy] [int] NULL,
		[Active] [bit] NULL,
	 CONSTRAINT [PK_CM_ServicesItem_History] PRIMARY KEY CLUSTERED 
	(
		[ServiceItemHistoryId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH




