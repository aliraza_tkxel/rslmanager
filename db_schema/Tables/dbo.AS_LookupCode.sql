CREATE TABLE [dbo].[AS_LookupCode]
(
[LookupCodeId] [int] NOT NULL IDENTITY(1, 1),
[LookupTypeId] [int] NOT NULL,
[CodeName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedBy] [int] NULL,
[CreatedDate] [smalldatetime] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AS_LookupCode] ADD 
CONSTRAINT [PK_AS_LookupCode] PRIMARY KEY CLUSTERED  ([LookupCodeId]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[AS_LookupCode] ADD
CONSTRAINT [FK_AS_LookupCode_AM_LookupType] FOREIGN KEY ([LookupTypeId]) REFERENCES [dbo].[AS_LookupType] ([LookupTypeId])
GO
