USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PLANNED_COMPONENT_ITEM')

BEGIN

CREATE TABLE [dbo].[PLANNED_COMPONENT_ITEM](
	[COMPONENTITEMID] [int] IDENTITY(1,1) NOT NULL,
	[COMPONENTID] [smallint] NOT NULL,
	[ITEMID] [int] NOT NULL,
	[ISACTIVE] [bit] NULL,
	[PARAMETERID] [int] NULL,
	[VALUEID] [int] NULL,
	[SubParameter] [int] NULL,
	[SubValue] [int] NULL,
 CONSTRAINT [PK_PLANNED_COMPONENT_ITEM] PRIMARY KEY CLUSTERED 
(
	[COMPONENTITEMID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[PLANNED_COMPONENT_ITEM]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_COMPONENT_PLANNED_COMPONENT_ITEM] FOREIGN KEY([COMPONENTID])
REFERENCES [dbo].[PLANNED_COMPONENT] ([COMPONENTID])

ALTER TABLE [dbo].[PLANNED_COMPONENT_ITEM] CHECK CONSTRAINT [FK_PLANNED_COMPONENT_PLANNED_COMPONENT_ITEM]

END

--1 ) Heating - Main Gas - Electric Unvented
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Main Gas - Electric Unvented') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Mains Gas') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Electric Unvented') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Main Gas - Electric Unvented'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Mains Gas'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Electric Unvented'))
PRINT 'ADDED  Main Gas - Electric Unvented'
END


--2 ) Heating - Main Gas - Oil Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Main Gas - Oil Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Mains Gas') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Main Gas - Oil Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Mains Gas'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler'))
	PRINT 'ADDED  Main Gas - Oil Boiler'
END 

--3 ) Heating - Mains Electric - Traditional Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Mains Electric - Traditional Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Traditional Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Mains Electric - Traditional Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Traditional Boiler'))
	PRINT 'ADDED  Mains Electric - Traditional Boiler'

END 

--4 ) Heating - Mains Electric - Fire
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Mains Electric - Fire') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Fire') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Mains Electric - Fire'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Fire'))
	PRINT 'ADDED  Mains Electric -Fire'

END 

--5 ) Heating - Mains Electric - Combi Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Mains Electric - Combi Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Combi Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Mains Electric - Combi Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Combi Boiler'))
PRINT 'ADDED  Mains Electric - Combi Boiler'

END

--6 ) Heating - Mains Electric - Oil Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Mains Electric - Oil Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Mains Electric - Oil Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Mains Electric'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler'))
PRINT 'ADDED  Mains Electric - Oil Boiler'
END

--7 ) Heating - LPG - Electric Unvented
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - LPG - Electric Unvented') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'LPG') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Electric Unvented') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - LPG - Electric Unvented'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'LPG'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Electric Unvented'))
PRINT 'ADDED  LPG - Electric Unvented'

END

--8 ) Heating - LPG - Oil Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - LPG - Oil Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'LPG') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - LPG - Oil Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'LPG'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler'))
PRINT 'ADDED  LPG - Oil Boiler'
END

--9 ) Heating - Oil - Fire
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Oil - Fire') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Oil') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Fire') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Oil - Fire'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Oil'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Fire'))
PRINT 'ADDED  Oil - Fire'
END

--10 ) Heating - Oil - Combi Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Oil - Combi Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Oil') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Combi Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Oil - Combi Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Oil'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Combi Boiler'))
PRINT 'ADDED  Oil - Combi Boiler'
END


--11 ) Heating - Oil - Wall Heater
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Oil - Wall Heater') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Oil') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Wall Heater') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Oil - Wall Heater'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Oil'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Wall Heater'))
PRINT 'ADDED Wall Heater'
END

--12 ) Heating - Oil - Electric Unvented
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Oil - Electric Unvented') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Oil') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Electric Unvented') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Oil - Electric Unvented'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Oil'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Electric Unvented'))
PRINT 'ADDED Electric Unvented'
END

--13 ) Heating - Oil - Oil Boiler
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Oil - Oil Boiler') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Oil') AND [SubParameter] = DBO.FN_GetParameterId('Heating Type') AND [SubValue] =  dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Oil - Oil Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Oil'),DBO.FN_GetParameterId('Heating Type'),dbo.FN_GetParameterValueId('Heating Type' ,'Oil Boiler'))
PRINT 'ADDED Oil Boiler'
END
--14 ) Heating - E7 - ALL
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - E7 - ALL') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'E7') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - E7 - ALL'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'E7'),null,null)
PRINT 'ADDED E7'

END
--15 ) Heating - Biomass - ALL
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Biomass - ALL') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Biomass') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Biomass - ALL'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Biomass'),null,null)
PRINT 'ADDED BIOMASS'
END

--16 ) Heating - Electric - ALL
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating - Electric - ALL') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Electric') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating - Electric - ALL'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Electric'),null,null)
PRINT 'ADDED Electric'
end

--17 ) Heating � Unvented Cylinder
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating � Unvented Cylinder') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Unvented Cylinder') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating � Unvented Cylinder'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Unvented Cylinder'),null,null)
PRINT 'ADDED Unvented Cylinder'
END

--18 ) Heating � MVHR & Heat Recovery
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating � MVHR & Heat Recovery') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'MVHR & Heat Recovery') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating � MVHR & Heat Recovery'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'MVHR & Heat Recovery'),null,null)
PRINT 'ADDED MVHR'
END
--19 ) Heating � Wet Electric
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating � Wet Electric') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'Wet Electric') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating � Wet Electric'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'Wet Electric'),null,null)
PRINT 'ADDED Wet Electric'
END

--20 ) Heating � PV (photo voltonic)
IF NOT EXISTS (SELECT * FROM PLANNED_COMPONENT_ITEM WHERE [COMPONENTID] = DBO.GetComponentId('Heating � PV (photo voltonic)') AND [ITEMID] = DBO.FN_GetItemId('Heating') AND [PARAMETERID] = DBO.FN_GetParameterId('heating fuel') AND [VALUEID] = dbo.FN_GetParameterValueId('heating fuel' ,'PV') )
BEGIN
	INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
	VALUES (DBO.GetComponentId('Heating � PV (photo voltonic)'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('heating fuel'),dbo.FN_GetParameterValueId('heating fuel' ,'PV'),null,null)
PRINT 'ADDED PV (photo voltonic)'
END

IF EXISTS (SELECT Item.COMPONENTITEMID FROM PLANNED_COMPONENT_ITEM Item
	 INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = Item.VALUEID
	 WHERE PV.ValueDetail = 'LPG' AND Item.ISACTIVE = 1)
BEGIN
	UPDATE PLANNED_COMPONENT_ITEM
	SET PLANNED_COMPONENT_ITEM.ISACTIVE = 0
	FROM PLANNED_COMPONENT_ITEM 
	INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = PLANNED_COMPONENT_ITEM.VALUEID
	WHERE PV.ValueDetail = 'LPG' AND PLANNED_COMPONENT_ITEM.ISACTIVE = 1
	PRINT 'In-active the LPG Fuel Type.'
END

IF EXISTS (SELECT Item.COMPONENTITEMID FROM PLANNED_COMPONENT_ITEM Item
	 INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = Item.VALUEID
	 WHERE PV.ValueDetail = 'Wind' AND Item.ISACTIVE = 1)
BEGIN
	UPDATE PLANNED_COMPONENT_ITEM
	SET PLANNED_COMPONENT_ITEM.ISACTIVE = 0
	FROM PLANNED_COMPONENT_ITEM 
	INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = PLANNED_COMPONENT_ITEM.VALUEID
	WHERE PV.ValueDetail = 'Wind' AND PLANNED_COMPONENT_ITEM.ISACTIVE = 1
	PRINT 'In-active the Wind Fuel Type.'
END

IF EXISTS (SELECT Item.COMPONENTITEMID FROM PLANNED_COMPONENT_ITEM Item
	 INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = Item.VALUEID
	 WHERE PV.ValueDetail = 'E7' AND Item.ISACTIVE = 1)
BEGIN
	UPDATE PLANNED_COMPONENT_ITEM
	SET PLANNED_COMPONENT_ITEM.ISACTIVE = 0
	FROM PLANNED_COMPONENT_ITEM 
	INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = PLANNED_COMPONENT_ITEM.VALUEID
	WHERE PV.ValueDetail = 'E7' AND PLANNED_COMPONENT_ITEM.ISACTIVE = 1
	PRINT 'In-active the E7 Fuel Type.'
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
