CREATE TABLE [dbo].[F_PAYTHRU_CALLBACK_LOG]
(
[CallbackId] [int] NOT NULL IDENTITY(1, 1),
[TransactionId] [uniqueidentifier] NULL,
[Payload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonFirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonSurname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonEmail] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonMobileNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonHomePhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionKey] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionTime] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionValue] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionAuthCode] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionClass] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionToken] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionCurrency] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionType] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionMaid] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Items0price] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Items0name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Items0quantity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Items0reference] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionIpAddress] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomTenancyRef] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCustomerRef] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomStaffId] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomStaffName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateReceived] [datetime] NULL,
[DateProcessed] [datetime] NULL,
[DateCreated] [datetime] NULL CONSTRAINT [DF_F_PAYTHRU_CALLBACK_LOG_DateCreated] DEFAULT (getdate()),
[CreatedBy] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_F_PAYTHRU_CALLBACK_LOG_CreatedBy] DEFAULT (N'SYSTEM_USER'),
[Version] [timestamp] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[F_PAYTHRU_CALLBACK_LOG] ADD 
CONSTRAINT [PK_F_PAYTHRU_CALLBACK_LOG] PRIMARY KEY CLUSTERED  ([CallbackId]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_F_PAYTHRU_CALLBACK_LOG_TransactionId] ON [dbo].[F_PAYTHRU_CALLBACK_LOG] ([TransactionId]) WITH (FILLFACTOR=100) ON [PRIMARY]

GO
