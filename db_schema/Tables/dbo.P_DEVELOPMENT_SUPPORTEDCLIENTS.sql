CREATE TABLE [dbo].[P_DEVELOPMENT_SUPPORTEDCLIENTS]
(
[Sid] [smallint] NOT NULL,
[Description] [nvarchar] (130) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NROSH_CODE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[P_DEVELOPMENT_SUPPORTEDCLIENTS] TO [rackspace_datareader]
GO
