CREATE TABLE [dbo].[AM_NegativeRentBalanceCount]
(
[CountId] [int] NOT NULL IDENTITY(1, 1),
[NegativeRentCount] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[AM_NegativeRentBalanceCount] ADD 
CONSTRAINT [PK_AM_NegativeRentBalanceCount] PRIMARY KEY CLUSTERED  ([CountId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
