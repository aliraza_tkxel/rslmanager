USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'P_RENTINCREASE_LETTERTEXT')
BEGIN

CREATE TABLE [dbo].[P_RENTINCREASE_LETTERTEXT](
	[LETTERTEXTID] [int] NULL,
	[LETTERTEXT] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'P_RENTINCREASE_LETTERTEXT')
BEGIN
	  	
		Update P_RENTINCREASE_LETTERTEXT set LETTERTEXT='[B]RE-ASSESSMENT OF RENT UNDER ASSURED TENANCIES[/B]<BR><BR>Every year Broadland Housing Association checks to make sure the amount of rent you pay each month is in line with our rent setting policy. <BR><BR>We are writing to tell you that this year, from 01/04/[D], there will be a change to how much rent you will need to pay us.<BR><BR>The table below shows a breakdown of your existing rent up until 31/03/[D]. You can also see what your new rent will be from 01/04/[D].<BR><BR>[R]<BR>The Council Tax cost is estimated as the council have not yet fixed the level of increase for [D]/[N]. We will notify you again if this changes.<BR>[PB]<BR><BR>It is really important that you keep rent payments up to date, the following tells you what you need to do to make sure that you pay the new rent. <BR>[DD]<BR><BR>[B]Customers Paying by Payment Card [/B]<BR>If you currently pay by Rent Payment Card please adjust your payment to the new monthly rent you need to pay to Broadland Housing Association.<BR><BR>[B]Customers Paying by Standing Order[/B]<BR>If you currently pay by Standing Order please contact your bank to instruct them to change your payments to the<BR>new monthly rent you need to pay to Broadland Housing Association. <BR><BR>[B]Customers who receive Housing Benefit[/B]<BR>If you claim Housing Benefit for all or part of your rent - it is your responsibility to take this letter to your Local Authority immediately to enable them to reassess your claim.  <BR><BR>[B]Customers who receive Universal Credit[/B]<BR>It is your responsibility to notify the Department for Work & Pensions of this rent change who will reassess the housing costs element of your claim. Failure to do so may result in your housing costs being stopped.<BR><BR>Do not hesitate to contact us if you have any questions regarding this letter or wish to change your payment method. Please make sure you have your tenancy reference number available from the top of this letter. '
END 
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH






