CREATE TABLE [dbo].[TBL_PDA_SURVEY]
(
[SurveyID] [int] NOT NULL,
[Title] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SurveyDate] [datetime] NULL,
[SurveyorName] [varchar] (80) COLLATE Latin1_General_CI_AS NOT NULL,
[Nature] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[TBL_PDA_SURVEY] ADD 
CONSTRAINT [PK_TBL_PDA_SURVEY] PRIMARY KEY CLUSTERED  ([SurveyID]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
