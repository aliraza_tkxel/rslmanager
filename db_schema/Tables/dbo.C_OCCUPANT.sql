USE [RSLBHALive]
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'C_OCCUPANT')
BEGIN
CREATE TABLE [dbo].[C_OCCUPANT](
	[OCCUPANTID] [int] IDENTITY(1,1) NOT NULL,
	[CUSTOMERID] [int] NULL,
	[TENANCYID] [int] NULL,
	[RELATIONSHIP] [int] NULL,
	[ECONOMICSTATUS] [int] NULL,
	[NOTES] [nvarchar](1000) NULL,
	[STARTDATE] [smalldatetime] NULL,
	[ENDDATE] [smalldatetime] NULL,
	[USERID] [int] NULL,
	[DATESTAMP] [smalldatetime] NOT NULL CONSTRAINT [DF_C_OCCUPANT_DATESTAMP]  DEFAULT (getdate()),
	[CONSENT] [int] NULL,
	[DISABILITY] [nvarchar](30) NULL
) ON [PRIMARY]
print 'C_OCCUPANT Table created'
END
ELSE
BEGIN
print 'C_OCCUPANT Table already exist'
IF COL_LENGTH('C_OCCUPANT', 'DisabilityOther') IS NULL
	BEGIN
			ALTER TABLE C_OCCUPANT
			ADD DisabilityOther NVARCHAR(1000) NULL
			PRINT('COLUMN DisabilityOther CREATED')
	END		
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME = 'C_OCCUPANT'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE C_OCCUPANT
ADD CONSTRAINT pk_OCCUPANTID PRIMARY KEY (OCCUPANTID)
PRINT 'Primary key added'
END
END
IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;  
			Print('Commit') 	
		END

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, 
					@ErrorSeverity, 
					@ErrorState 
				);
	Print (@ErrorMessage)

END CATCH