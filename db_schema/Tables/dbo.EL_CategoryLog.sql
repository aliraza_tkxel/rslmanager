CREATE TABLE [dbo].[EL_CategoryLog]
(
[CategoryLogID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[LogID] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[EL_CategoryLog] ADD 
CONSTRAINT [PK_EL_CategoryLog] PRIMARY KEY CLUSTERED  ([CategoryLogID]) WITH (FILLFACTOR=100) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [ixCategoryLog] ON [dbo].[EL_CategoryLog] ([LogID], [CategoryID]) WITH (FILLFACTOR=100) ON [PRIMARY]

ALTER TABLE [dbo].[EL_CategoryLog] ADD
CONSTRAINT [FK_EL_CategoryLog_EL_Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[EL_Category] ([CategoryID])
ALTER TABLE [dbo].[EL_CategoryLog] ADD
CONSTRAINT [FK_EL_CategoryLog_EL_Log] FOREIGN KEY ([LogID]) REFERENCES [dbo].[EL_Log] ([LogID])
GO
