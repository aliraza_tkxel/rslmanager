CREATE TABLE [dbo].[Employees]
(
[empid] [int] NOT NULL,
[mgrid] [int] NULL,
[empname] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[salary] [money] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employees] ADD CONSTRAINT [PK_Employees1] PRIMARY KEY CLUSTERED  ([empid]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
