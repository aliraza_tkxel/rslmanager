BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_SALESCUSTOMER')
BEGIN


CREATE TABLE [dbo].[F_SALESCUSTOMER]
(
[SCID] [int] NOT NULL IDENTITY(1, 1),
[CONTACT] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORGANISATION] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TOWNCITY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTCODE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELEPHONE1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELEPHONE2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WEBSITE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAYMENTTERMS] [int] NULL,
[CREDITLIMIT] [int] NULL,
[DESCRIPTION] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIVE] [int] NULL,
[CREATED] [smalldatetime] NULL,
[CREATEDBY] [int] NULL,
[MODIFIED] [smalldatetime] NULL,
[MODIFIEDBY] [int] NULL,
[CUSACTIVE] [bit] NULL CONSTRAINT [DF__F_SALESCU__CUSAC__0CC76C1D] DEFAULT ((1))
) ON [PRIMARY]


END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_SALESCUSTOMER'))
BEGIN

alter table [F_SALESCUSTOMER] add CompanyId [int] NULL

END
