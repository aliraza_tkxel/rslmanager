CREATE TABLE [dbo].[PLANNED_INSPECTION_APPOINTMENTS]
(
[InspectionID] [int] NOT NULL IDENTITY(1, 1),
[TENANCYID] [int] NULL,
[JournalId] [int] NOT NULL,
[JOURNALHISTORYID] [bigint] NULL,
[APPOINTMENTDATE] [smalldatetime] NULL,
[APPOINTMENTTIME] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIONID] [int] NULL,
[ASSIGNEDTO] [int] NOT NULL,
[CREATEDBY] [int] NULL,
[LOGGEDDATE] [smalldatetime] NULL,
[APPOINTMENTNOTES] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRADEID] [int] NULL,
[APPOINTMENTENDTIME] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PLANNED_INSPECTION_APPOINTMENTS] ADD 
CONSTRAINT [PK_PLANNED_INSPECTION_APPOINTMENTS] PRIMARY KEY CLUSTERED  ([InspectionID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[PLANNED_INSPECTION_APPOINTMENTS] ADD
CONSTRAINT [FK__PLANNED_I__TRADE__72A872BE] FOREIGN KEY ([TRADEID]) REFERENCES [dbo].[G_TRADE] ([TradeId])
GO

ALTER TABLE [dbo].[PLANNED_INSPECTION_APPOINTMENTS] ADD CONSTRAINT [FK_PLANNED_INSPECTION_APPOINTMENTS_PLANNED_JOURNAL] FOREIGN KEY ([JournalId]) REFERENCES [dbo].[PLANNED_JOURNAL] ([JOURNALID])
GO
