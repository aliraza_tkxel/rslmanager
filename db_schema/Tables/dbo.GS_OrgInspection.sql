CREATE TABLE [dbo].[GS_OrgInspection]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[AppointmentID] [int] NOT NULL,
[GasEngineerID] [int] NOT NULL,
[GSRENo] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InspectedDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GS_OrgInspection] ADD 
CONSTRAINT [PK_GS_OrgInspection] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=100) ON [PRIMARY]
ALTER TABLE [dbo].[GS_OrgInspection] ADD
CONSTRAINT [FK_GS_OrgInspection_PS_Appointment] FOREIGN KEY ([AppointmentID]) REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO
