USE [RSLBHALive]
GO


BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'FLS_ViewingCommentTypes')

BEGIN
	CREATE TABLE [dbo].[FLS_ViewingCommentTypes](
	[CommentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[CommentTypeTitle] [nvarchar](50) NULL,
	CONSTRAINT [PK_FLS_ViewingCommentTypes] PRIMARY KEY CLUSTERED 
		(
			[CommentTypeId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	END
	if NOT exists(select * from [dbo].[FLS_ViewingCommentTypes]
              where [CommentTypeTitle] = 'GeneralFeedback')
	BEGIN
		INSERT INTO [dbo].[FLS_ViewingCommentTypes] VALUES ('GeneralFeedback')
		Print 'Add row for General Comments'
	END

	if NOT exists(select * from [dbo].[FLS_ViewingCommentTypes]
              where [CommentTypeTitle] = 'EditFeedback')
	BEGIN
		INSERT INTO [dbo].[FLS_ViewingCommentTypes] VALUES ('EditFeedback')
		Print 'Add row for  Edit Comments'
	END

	if NOT exists(select * from [dbo].[FLS_ViewingCommentTypes]
              where [CommentTypeTitle] = 'RearrangeFeedback')
	BEGIN
		INSERT INTO [dbo].[FLS_ViewingCommentTypes] VALUES ('RearrangeFeedback')
		Print 'Add row for  Rearrange Comments'
	END

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY

BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH