CREATE TABLE [dbo].[AM_StandardLetters]
(
[StandardLetterId] [int] NOT NULL IDENTITY(1, 1),
[StatusId] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedBy] [int] NOT NULL,
[ModifiedBy] [int] NULL,
[CreatedDate] [datetime] NOT NULL,
[ModifiedDate] [datetime] NULL,
[PersonalizationLookupCode] [int] NULL,
[SignOffLookupCode] [int] NULL,
[TeamId] [int] NULL,
[FromResourceId] [int] NULL,
[IsPrinted] [bit] NULL,
[IsActive] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[AM_StandardLetters] ADD 
CONSTRAINT [PK_AM_StandardLetters] PRIMARY KEY CLUSTERED  ([StandardLetterId]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AM_StandardLetters] ADD CONSTRAINT [Relation_ActionId] FOREIGN KEY ([ActionId]) REFERENCES [dbo].[AM_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AM_StandardLetters] ADD CONSTRAINT [Relation_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_StandardLetters] ADD CONSTRAINT [Relation_ModifiedBy] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AM_Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[AM_StandardLetters] ADD CONSTRAINT [Relation_Personalization] FOREIGN KEY ([PersonalizationLookupCode]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_StandardLetters] ADD CONSTRAINT [Relation_SignOff] FOREIGN KEY ([SignOffLookupCode]) REFERENCES [dbo].[AM_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AM_StandardLetters] ADD CONSTRAINT [Relation_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AM_Status] ([StatusId])
GO
