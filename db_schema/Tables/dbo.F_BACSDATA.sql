BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'F_BACSDATA')
BEGIN

	CREATE TABLE [dbo].[F_BACSDATA]
	(
	[DATAID] [int] NOT NULL IDENTITY(1, 1),
	[FILEDATE] [smalldatetime] NULL,
	[FILENAME] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FILETYPE] [int] NULL,
	[FILECOUNT] [int] NULL,
	[INVOICECOUNT] [int] NULL,
	[TOTALVALUE] [float] NULL,
	[PROCESSDATE] [smalldatetime] NULL,
	[ISFILE] [int] NULL
	) ON [PRIMARY]
	CREATE NONCLUSTERED INDEX [_dta_index_F_BACSDATA_7_1867869721__K1_K6_K7_K3] ON [dbo].[F_BACSDATA] ([DATAID], [INVOICECOUNT], [TOTALVALUE], [FILENAME]) ON [PRIMARY]

	 

END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


IF not EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CompanyId'
          AND Object_ID = Object_ID(N'F_BACSDATA'))
BEGIN

alter table dbo.F_BACSDATA add CompanyId [int] NULL

end
