USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'C_TERMINATION')
BEGIN

CREATE TABLE [dbo].[C_TERMINATION](
	[TERMINATIONHISTORYID] [int] IDENTITY(1,1) NOT NULL,
	[JOURNALID] [int] NULL,
	[ITEMSTATUSID] [int] NULL,
	[ITEMACTIONID] [int] NULL,
	[LASTACTIONDATE] [smalldatetime] NULL CONSTRAINT [DF_C_TERMINATION_LASTACTIONDATE]  DEFAULT (convert(datetime,convert(varchar,getdate(),103),103)),
	[LASTACTIONUSER] [int] NULL,
	[TERMINATIONDATE] [smalldatetime] NULL,
	[REASON] [int] NULL,
	[NOTES] [nvarchar](2000) NULL,
	[TTIMESTAMP] [smalldatetime] NOT NULL CONSTRAINT [DF_C_TERMINATION_TTIMESTAMP]  DEFAULT (getdate()),
	[REASONCATEGORYID] [int] NULL,
 CONSTRAINT [PK_C_TERMINATION] PRIMARY KEY NONCLUSTERED 
(
	[TERMINATIONHISTORYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
PRINT 'Table Created'
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'AppVersion'
      AND Object_ID = Object_ID(N'C_TERMINATION'))
BEGIN
    ALTER TABLE [dbo].[C_TERMINATION]
	ADD [AppVersion] [nvarchar](100) NULL
	PRINT 'Column AppVersion Added successfully '
END

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RELETDATE'
      AND Object_ID = Object_ID(N'C_TERMINATION'))
BEGIN
    ALTER TABLE [dbo].[C_TERMINATION]
	ADD [RELETDATE] [smalldatetime]
	PRINT 'Column RELETDATE Added successfully '
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

