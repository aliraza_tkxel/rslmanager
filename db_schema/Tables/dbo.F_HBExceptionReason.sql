USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'F_HBExceptionReason')
BEGIN

CREATE TABLE [dbo].[F_HBExceptionReason](
	[ReasonId] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [nvarchar](200) NULL,
 CONSTRAINT [PK_F_HBExceptionReason] PRIMARY KEY CLUSTERED 
(
	[ReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

	INSERT INTO [dbo].[F_HBExceptionReason]([Reason])
	VALUES ('HB Ref mismatch')

	INSERT INTO [dbo].[F_HBExceptionReason]([Reason])
	VALUES ('Start date incorrect')

	INSERT INTO [dbo].[F_HBExceptionReason]([Reason])
	VALUES ('End date incorrect')

	INSERT INTO [dbo].[F_HBExceptionReason]([Reason])
	VALUES ('Amount incorrect')

	INSERT INTO [dbo].[F_HBExceptionReason]([Reason])
	VALUES ('Payment exists in RSL')

END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH
