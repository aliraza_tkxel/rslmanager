Use RSLBHALive
Go

BEGIN TRANSACTION
BEGIN TRY


IF NOT EXISTS (	SELECT	* 
				FROM	INFORMATION_SCHEMA.TABLES 
				WHERE	TABLE_NAME = N'F_IVR')
BEGIN


CREATE TABLE [dbo].[F_IVR](
	[IvrId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerTenancyRef] [int] NOT NULL,
	[CardLastFour] [nvarchar](20) NULL,
	[CardExpiryMonth] [nvarchar](10) NULL,
	[CardExpiryYear] [nvarchar](10) NULL,
	[ItemsName] [nvarchar](200) NULL,
	[ItemsPrice] [money] NULL,
	[ItemsQuantity] [int] NULL,
	[ItemsReference] [nvarchar](20) NULL,
	[TransactionKey] [nvarchar](50) NULL,
	[TransactionTime] [datetime] NULL,
	[TransactionStatus] [nvarchar](10) NULL,
	[TransactionValue] [money] NULL,
	[TransactionType] [nvarchar](50) NULL,
	[TransactionCurrency] [nvarchar](10) NULL,
	[TransactionAuthCode] [nvarchar](50) NULL,
	[TransactionClass] [nvarchar](50) NULL,
	[TransactionReference] [nvarchar](50) NULL,
	[TransactionToken] [nvarchar](50) NULL,
	[TransactionMaid] [nvarchar](50) NULL,
	[TransactionIpAddress] [nvarchar](20) NULL,
	[LoggedDate] [datetime] NULL,
	[RentJournalId] [int] NULL,
	[Balance] [money] NULL,
 CONSTRAINT [PK_F_IVR] PRIMARY KEY CLUSTERED 
(
	[IvrId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[F_IVR] ADD  CONSTRAINT [DF_F_IVR_LoggedDate]  DEFAULT (getdate()) FOR [LoggedDate]

END

	--===========================================================================================================
	-- Ticket # 11928 : Remove 'TransactionClass','TransactionReference','TransactionMaid','TransactionIpAddress' 
	--===========================================================================================================	
	IF EXISTS (	SELECT	1 
				FROM	sys.columns 
				WHERE	object_id = OBJECT_ID(N'[dbo].[F_IVR]' ) 
				AND		name = 'TransactionClass')
	BEGIN
		ALTER TABLE [dbo].[F_IVR]
		DROP COLUMN TransactionClass
		PRINT 'TransactionClass dropped'
    END

	IF EXISTS (	SELECT	1
				FROM	sys.columns 
				WHERE	object_id = OBJECT_ID(N'[dbo].[F_IVR]' ) 
				AND		name = 'TransactionReference')
	BEGIN
		ALTER TABLE [dbo].[F_IVR]
		DROP COLUMN TransactionReference
		PRINT 'TransactionReference dropped'
    END

	IF EXISTS (	SELECT	1
				FROM	sys.columns 
				WHERE	object_id = OBJECT_ID(N'[dbo].[F_IVR]' ) 
				AND		name = 'TransactionMaid')
	BEGIN
		ALTER TABLE [dbo].[F_IVR]
		DROP COLUMN TransactionMaid
		PRINT 'TransactionMaid dropped'
    END

	IF EXISTS (	SELECT	1 
					FROM	sys.columns 
					WHERE	object_id = OBJECT_ID(N'[dbo].[F_IVR]' ) 
					AND		name = 'TransactionIpAddress')
	BEGIN
		ALTER TABLE [dbo].[F_IVR]
		DROP COLUMN TransactionIpAddress
		PRINT 'TransactionIpAddress dropped'
    END

	--===========================================================================================================
	-- Ticket #12449 Correct wrong entries related to "Recharges" & "Legal Recharges"
	--===========================================================================================================
	IF EXISTS (	SELECT	1
	FROM	F_IVR 
	WHERE	ItemsName in ('LegalRecharge','Recharge'))
	BEGIN
		
		DECLARE @LegalRecharges_ItemTypeId int , @Recharges_ItemTypeId  int
		SELECT  @LegalRecharges_ItemTypeId =ITEMTYPEID FROM F_ITEMTYPE WHERE DESCRIPTION = 'Legal Recharges'
		SELECT  @Recharges_ItemTypeId =ITEMTYPEID FROM F_ITEMTYPE WHERE DESCRIPTION = 'Recharges'
		

		--=============================
		-- (1) Update F_RENTJOURNAL
		--=============================

		UPDATE	F_RENTJOURNAL  
		SET     F_RENTJOURNAL.ITEMTYPE =  CASE  
											WHEN F_IVR.ItemsName = 'LegalRecharge' THEN @LegalRecharges_ItemTypeId 
											WHEN F_IVR.ItemsName = 'Recharge' THEN @Recharges_ItemTypeId 								
										END 
		FROM	F_RENTJOURNAL
				INNER JOIN F_IVR ON F_RENTJOURNAL.JOURNALID = F_IVR.RentJournalId
		WHERE   F_IVR.ItemsName IN ('LegalRecharge', 'Recharge')

		--=============================
		-- (2) Update F_CASHPOSTING
		--=============================
		UPDATE	F_CASHPOSTING  
		SET     F_CASHPOSTING.ITEMTYPE =  CASE  
											WHEN F_IVR.ItemsName = 'LegalRecharge' THEN @LegalRecharges_ItemTypeId 
											WHEN F_IVR.ItemsName = 'Recharge' THEN @Recharges_ItemTypeId 								
										END 
		FROM	F_CASHPOSTING
				INNER JOIN F_IVR ON F_CASHPOSTING.JOURNALID = F_IVR.RentJournalId
		WHERE   F_IVR.ItemsName IN ('LegalRecharge', 'Recharge')


		--=============================
		-- (3) Update entries in F_IVR
		--=============================

		UPDATE	F_IVR 
		SET		ItemsName = 'Legal Recharges' 
		WHERE	ItemsName = 'LegalRecharge'

		UPDATE	F_IVR 
		SET		ItemsName = 'Recharges' 
		WHERE	ItemsName = 'Recharge'

    END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;  
		Print('Commit') 	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH


