USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_WorkingHours]    Script Date: 24-Jul-17 4:02:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRANSACTION
BEGIN TRY

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AS_JournalHeatingMapping')
BEGIN

CREATE TABLE [dbo].[AS_JournalHeatingMapping](
	[HeatingJournalMappingId] [int] IDENTITY(1,1) NOT NULL,
	[JournalId] [int] NULL,
	[HeatingMappingId] [int] NULL,
	[JournalHistoryId] [bigint] NULL,
	[Createdon] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_AS_JournalHeatingMapping] PRIMARY KEY CLUSTERED 
(
	[HeatingJournalMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END
ELSE
	BEGIN
	PRINT 'Table already exist..'
END

IF NOT EXISTS(SELECT * from AS_JournalHeatingMapping)
BEGIN
INSERT INTO AS_JournalHeatingMapping(JournalId,HeatingMappingId,JournalHistoryId,Createdon)
	SELECT AS_JOURNAL.JOURNALID, MIN(AS_JOURNAL.HeatingMappingId) AS HeatingMappingId, MAX(AS_JOURNALHISTORY.JOURNALHISTORYID) AS JOURNALHISTORYID,GETDATE()
	FROM AS_JOURNAL
	INNER JOIN AS_JOURNALHISTORY ON AS_JOURNAL.JOURNALID = AS_JOURNALHISTORY.JOURNALID
	Where AS_JOURNAL.HeatingMappingId IS NOT NULL
	GROUP BY AS_JOURNAL.JOURNALID
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH





