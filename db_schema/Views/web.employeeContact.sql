SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [web].[employeeContact]
AS
SELECT 
	LTRIM(RTRIM([e].[FIRSTNAME])) AS FIRSTNAME,
	LTRIM(RTRIM([e].[LASTNAME])) AS LASTNAME,
	LTRIM(RTRIM([e].[FIRSTNAME]))  + ' ' + LTRIM(RTRIM([e].[LASTNAME]))AS NAME,
	ec.WORKEMAIL, 
	ec.WORKDD
 FROM dbo.E__EMPLOYEE e
 INNER JOIN dbo.E_CONTACT ec ON (e.EMPLOYEEID = ec.EMPLOYEEID)
GO
