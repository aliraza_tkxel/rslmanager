SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[I_MENU_ACTIVE]
AS
SELECT     MenuID, PositionID, MenuTitle, ParentID, Active, Depth, Rank
FROM         dbo.I_MENU
WHERE     (Active = 1)
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'I_MENU_ACTIVE', NULL, NULL
GO
