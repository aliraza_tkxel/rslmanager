SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwPropertyApplianceCP12]
AS
SELECT     dbo.GS_PROPERTY_APPLIANCE.PROPERTYID AS PropertyID, dbo.GS_APPLIANCE_TYPE.APPLIANCETYPE AS Appliance, DATEADD(month, 12, 
                      dbo.GS_PROPERTY_APPLIANCE.ISSUEDATE) AS Expiry, dbo.GS_PROPERTY_APPLIANCE.MODEL AS Model, 
                      dbo.GS_PROPERTY_APPLIANCE.ISSUEDATE AS IssueDate, dbo.GS_PROPERTY_APPLIANCE.CP12NUMBER AS CP12Number, 
                      dbo.GS_PROPERTY_APPLIANCE.DATEINSTALLED AS DateInstalled, dbo.GS_MANUFACTURER.MANUFACTURER AS Manufacturer
FROM         dbo.GS_APPLIANCE_TYPE INNER JOIN
                      dbo.GS_PROPERTY_APPLIANCE ON dbo.GS_APPLIANCE_TYPE.APPLIANCETYPEID = dbo.GS_PROPERTY_APPLIANCE.APPLIANCETYPEID INNER JOIN
                      dbo.GS_MANUFACTURER ON dbo.GS_PROPERTY_APPLIANCE.MANUFACTURERID = dbo.GS_MANUFACTURER.MANUFACTURERID
GO
