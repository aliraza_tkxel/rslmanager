SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

ALTER VIEW [dbo].[FL_FAULT_LIST_COMPLETED_DETAILS]
AS
    SELECT  PS.SCHEMENAME DEVELOPMENTNAME ,
            P.PROPERTYID ,
            COALESCE(PD.PATCHID,27) AS PATCHID ,
            COALESCE(PTC.LOCATION,'Unknown') AS PATCH ,
            ST.DESCRIPTION AS STOCKTYPE ,
            P.ASSETTYPE ,
            FJ.JournalId ,
            FA.AppointmentId ,
            FL.FaultLogId ,
            FL.JobSheetNumber ,
            FL.SubmitDate ,
            a.AppointmentDate ,
            a.Time AS StartTime ,
            a.EndTime ,
            F.PriorityId ,
            ( CASE WHEN e.EMPLOYEEID IS NULL
                   THEN ISNULL(FLR.LastRepairDate, FLH.LastActionDate)
                   ELSE a.RepairCompletionDateTime
              END ) AS CompletedDate ,
            F.Description AS [FaultDescription] ,
            ( CASE WHEN e.EMPLOYEEID IS NOT NULL THEN 2129
                   ELSE s.ORGID
              END ) AS ORGID ,
            s.NAME AS SupplierName ,
            e.EMPLOYEEID ,
            ( ISNULL(e.FIRSTNAME, '') + ' ' + ISNULL(e.LASTNAME, '') ) AS Operative ,
            FLH.FaultLogHistoryID
    FROM    FL_FAULT_LOG FL
            INNER JOIN FL_FAULT_LIST_COMPLETED FLC ON FL.FaultLogID = FLC.FaultLogID
            INNER JOIN dbo.FL_FAULT_LOG_HISTORY FLH ON FLH.FaultLogHistoryID = flC.FaultLogHistoryId
            LEFT JOIN FL_FAULT_LIST_LAST_REPAIR_DATE FLR ON FLR.FaultLogID = FLC.FaultLogId
            LEFT JOIN FL_FAULT_APPOINTMENT fa ON fa.FaultLogId = FL.FaultLogID
            LEFT JOIN FL_CO_APPOINTMENT a ON a.AppointmentID = fa.AppointmentId
            LEFT JOIN E__EMPLOYEE e ON e.EMPLOYEEID = a.OperativeID
            LEFT JOIN S_ORGANISATION S ON S.ORGID = FL.ORGID
            INNER JOIN FL_FAULT_JOURNAL fj ON FL.FaultLogID = fj.FaultLogID
            LEFT JOIN P__PROPERTY P ON P.PROPERTYID = FJ.PROPERTYID
            LEFT JOIN P_SCHEME PS ON P.SCHEMEID = PS.SCHEMEID
            LEFT JOIN PDR_DEVELOPMENT PD ON P.DEVELOPMENTID = PD.DEVELOPMENTID
            LEFT JOIN dbo.E_PATCH PTC ON PTC.PATCHID = PD.PATCHID
            INNER JOIN dbo.FL_FAULT F ON F.FaultID = FL.FaultID
            LEFT JOIN FL_FAULT_NOENTRY_LAST_RECORDED_DATE NE ON NE.FaultLogId = fl.FaultLogID
            LEFT JOIN dbo.P_STOCKTYPE ST ON ST.STOCKTYPE = P.STOCKTYPE
            
    WHERE   NE.RecordedOn IS NULL
 -- exclude no entries


GO
