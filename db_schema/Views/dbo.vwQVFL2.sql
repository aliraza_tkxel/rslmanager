SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--DROP VIEW [vwQVFL2]
CREATE VIEW [dbo].[vwQVFL2] AS
SELECT f.FaultID, f.Description AS 'Fault Summary'
	, ISNULL(a.AreaName, 'NOT SPECIFIED') AS 'FaultArea'
	, ISNULL(l.LocationName, 'NOT SPECIFIED') AS 'FaultLocation'
	, fl.FaultLogID, fl.SubmitDate, fl.Notes AS 'Fault Details', fl.JobSheetNumber AS JSNumber
	, CONVERT(varchar, YEAR(fl.SubmitDate)) + ' ' + CONVERT(varchar, MONTH(fl.SubmitDate)) + ' - ' + CONVERT(varchar, DATENAME(month, fl.SubmitDate)) AS RepairSubmittedYearMonth
	, ISNULL(t.Description, 'NOT SPECIFIED') AS 'Trade'
	, fa.AppointmentID, CONVERT(DATE, fa.AppointmentDate) AS 'AppointmentDate', fa.AppointmentStatus, ISNULL(fa.Notes, '') AS 'AppointmentNotes', ISNULL(fa.RepairNotes, '') AS 'RepairNotes', fa.RepairCompletionDateTime, CONVERT(varchar, YEAR(fa.RepairCompletionDateTime)) + ' ' + CONVERT(varchar, MONTH(fa.RepairCompletionDateTime)) + ' - ' + CONVERT(varchar, DATENAME(month, fa.RepairCompletionDateTime)) AS RepairCompletionYearMonth
	, YEAR(fa.RepairCompletionDateTime) AS 'YearCompleted', MONTH(fa.RepairCompletionDateTime) AS 'MonthCompleted', LEFT(DATENAME(month, fa.RepairCompletionDateTime), 3) AS 'MonthCompletedName', DAY(fa.RepairCompletionDateTime) AS 'DayCompleted'
	, e.FIRSTNAME + ' ' + e.LASTNAME AS 'Operative'
	, p.PROPERTYID, p.HOUSENUMBER + ' ' + p.ADDRESS1 + ', ' + ISNULL(p.ADDRESS2, '') + ', ' + p.TOWNCITY AS 'FullAddress', p.COUNTY
	, patch.LOCATION AS 'Patch'
FROM FL_FAULT f
	INNER JOIN FL_AREA a ON f.AreaID = a.AreaID
	INNER JOIN FL_LOCATION l ON a.LocationID = l.LocationID
	INNER JOIN  FL_FAULT_LOG fl ON fl.FaultID = f.FaultID
	INNER JOIN FL_FAULT_TRADE tlink ON fl.FaultTradeID = tlink.FaultTradeId
	INNER JOIN G_TRADE t ON tlink.TradeId = t.TradeId
	--LEFT OUTER JOIN FL_FAULT_TRADE tlink ON f.FaultID = tlink.FaultId         -- Seems to be linked from FL_CO_APPOINTMENT now
	INNER JOIN FL_FAULT_APPOINTMENT falink ON fl.FaultLogID = falink.FaultLogId
	INNER JOIN FL_CO_APPOINTMENT fa ON falink.AppointmentId = fa.AppointmentID
	INNER JOIN E__EMPLOYEE e ON fa.OperativeID = e.EMPLOYEEID
	INNER JOIN P__PROPERTY p ON fl.PROPERTYID = p.PROPERTYID
	INNER JOIN E_PATCH patch ON p.PATCH = patch.PATCHID
	
	
GO
