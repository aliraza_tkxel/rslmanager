SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.vwLastPropertySurvey
AS
SELECT     M.SDID, M.PropertyID, M.Surveyed, M.SurveyType, dbo.TBL_PDA_SURVEY.Nature
FROM         dbo.TBL_PDA_SURVEY_DETAIL M INNER JOIN
                      dbo.TBL_PDA_SURVEY ON M.SurveyID = dbo.TBL_PDA_SURVEY.SurveyID
WHERE     (M.SDID IN
                          (SELECT     TOP 1 SDID
                            FROM          dbo.TBL_PDA_SURVEY_DETAIL AS S
                            WHERE      (Surveyed IS NOT NULL) AND (PropertyID = M.PropertyID)
                            ORDER BY Surveyed DESC))
GO
