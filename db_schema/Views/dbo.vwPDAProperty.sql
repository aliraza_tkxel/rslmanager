SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.vwPDAProperty
AS
SELECT DISTINCT 
                      TOP 100 PERCENT dbo.P__PROPERTY.PROPERTYID, dbo.vwLastPropertySurvey.Surveyed, 
                      CASE WHEN dbo.TBL_PROPERTY_ITEM_ASBESTOS.PropertyID IS NULL 
                      THEN '<img src="myImages/trafficlight_Green.gif" style="height:12px;width:12px;border-width:0px;" />' ELSE '<a href="" target="_blank"><img src="myImages/asbestos.gif" style="height:12px;width:12px;border-width:0px;" /></a>'
                       END AS Asbestos, dbo.P__PROPERTY.HOUSENUMBER, dbo.P__PROPERTY.FLATNUMBER, dbo.P__PROPERTY.ADDRESS1 AS AddressLine1, 
                      dbo.P__PROPERTY.ADDRESS2 AS AddressLine2, dbo.P__PROPERTY.ADDRESS3 AS AddressLine3, dbo.P__PROPERTY.TOWNCITY, 
                      dbo.P__PROPERTY.COUNTY, dbo.P__PROPERTY.POSTCODE, dbo.P__PROPERTY.STATUS, dbo.P__PROPERTY.SUBSTATUS, 
                      dbo.P__PROPERTY.PATCH, dbo.P__PROPERTY.DEVELOPMENTID, dbo.P__PROPERTY.STOCKTYPE, dbo.P__PROPERTY.SAP
FROM         dbo.P__PROPERTY LEFT OUTER JOIN
                      dbo.vwLastPropertySurvey ON dbo.P__PROPERTY.PROPERTYID = dbo.vwLastPropertySurvey.PropertyID LEFT OUTER JOIN
                      dbo.TBL_PROPERTY_ITEM_ASBESTOS ON dbo.P__PROPERTY.PROPERTYID = dbo.TBL_PROPERTY_ITEM_ASBESTOS.PropertyID
ORDER BY dbo.P__PROPERTY.PROPERTYID
GO
