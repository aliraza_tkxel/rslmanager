SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[CASHBOOK_INVOICE] AS
SELECT 	ISNULL(BR.GROUPINGID,-1) AS RECCED, 
	8 AS DTYPE,
	BD.DATAID AS THEID, 
	BD.INVOICECOUNT AS ITEMCOUNT, 
	CONVERT(VARCHAR, PB.PROCESSDATE, 103) AS BOOKDATE, 
	CASE WHEN PT.PAYMENTTYPEID = 9 THEN
	   PT.DESCRIPTION + ' File: ' + BD.FILENAME
	WHEN PT.PAYMENTTYPEID = 8 THEN
	    'CHQ: ' + CHEQUENUMBER		
	ELSE
	    PT.DESCRIPTION + '  '
	END AS DESCRIPTION,
	PB.PROCESSDATE AS TRANSACTIONDATE, 
	CASE WHEN SUM(PI.GROSSCOST) < 0 THEN ABS(SUM(ROUND(PI.GROSSCOST,2))) ELSE 0 END AS DEBIT,
	CASE WHEN SUM(PI.GROSSCOST) > 0 THEN SUM(ROUND(PI.GROSSCOST,2)) ELSE 0 END AS CREDIT
FROM 		F_BACSDATA BD 
		INNER JOIN F_POBACS PB ON PB.DATAID = BD.DATAID 
		INNER JOIN F_INVOICE INV ON INV.INVOICEID = PB.INVOICEID 
		INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.INVOICEID = INV.INVOICEID
		INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = FOTI.ORDERITEMID
		LEFT JOIN F_PAYMENTTYPE PT ON INV.PAYMENTMETHOD = PT.PAYMENTTYPEID
		LEFT JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND BR.RECTYPE = 2 
GROUP 	BY BD.DATAID, PB.PROCESSDATE, BD.INVOICECOUNT, BD.TOTALVALUE, 
		PB.CHEQUENUMBER,  PT.DESCRIPTION, BD.FILENAME, PT.PAYMENTTYPEID, BR.GROUPINGID




GO
