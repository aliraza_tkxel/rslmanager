SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.vwPDAPropertyCustomer
AS
SELECT DISTINCT 
                      TOP 100 PERCENT dbo.vwPDAPropertyCustomerContact.CUSTOMERID, dbo.vwPDAPropertyCustomerContact.PROPERTYID, 
                      CASE WHEN dbo.vwCustomerRisk.RiskID IS NULL 
                      THEN '<img src="myImages/trafficlight_Green.gif" style="height:12px;width:12px;border-width:0px;" />' ELSE '<a href="../Customer/popups/pCustomerDrating.asp?customerid='
                       + CONVERT(varchar(20), dbo.vwPDAPropertyCustomerContact.CUSTOMERID) 
                      + '" target="_blank"><img src="myImages/alertw.gif" style="height:12px;width:12px;border-width:0px;" /></a>' END AS Risk, 
                      dbo.vwPDAPropertyCustomerContact.COMMUNICATION, dbo.vwPDAPropertyCustomerContact.OtherCommunication
FROM         dbo.vwPDAPropertyCustomerContact LEFT OUTER JOIN
                      dbo.vwCustomerRisk ON dbo.vwPDAPropertyCustomerContact.CUSTOMERID = dbo.vwCustomerRisk.CustomerID
ORDER BY dbo.vwPDAPropertyCustomerContact.PROPERTYID
GO
