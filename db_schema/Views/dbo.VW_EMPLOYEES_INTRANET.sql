
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.VW_EMPLOYEES_INTRANET
AS
SELECT     T.TEAMID, ISNULL(T.TEAMNAME, 'Not Assigned to a team') AS TEAMNAME, ISNULL(JR.JobeRoleDescription, 'No Job Title available') AS JOBTITLE, 
                      ISNULL(ET.STARTDATE, '20080101') AS STARTDATE, E.EMPLOYEEID, E.CREATIONDATE, E.TITLE, E.FIRSTNAME, E.MIDDLENAME, E.LASTNAME, 
                      E.DOB, E.GENDER, E.MARITALSTATUS, E.ETHNICITY, E.ETHNICITYOTHER, E.RELIGION, E.RELIGIONOTHER, E.SEXUALORIENTATION, 
                      E.SEXUALORIENTATIONOTHER, E.DISABILITY, E.DISABILITYOTHER, E.BANK, E.ACCOUNTNUMBER, E.SORTCODE, E.ACCOUNTNAME, 
                      E.ROLLNUMBER, E.BANK2, E.SORTCODE2ND, E.ACCOUNTNUMBER2, E.ACCOUNTNAME2, E.ROLLNUMBER2, E.ORGID, E.IMAGEPATH, E.ROLEPATH, 
                      E.PROFILE, E.FIRSTNAME + SPACE(1) + E.LASTNAME AS FULLNAME, ET.LINEMANAGER, EC.WORKDD, EC.WORKEMAIL, EC.MOBILE, 
                      ET.OFFICELOCATION, EL.FIRSTNAME + SPACE(1) + EL.LASTNAME AS LINEMANAGERNAME, G.DESCRIPTION AS OFFICEDESC, EC.WORKMOBILE, 
                      L.LASTLOGGEDIN
FROM         dbo.E__EMPLOYEE AS E LEFT OUTER JOIN
                      dbo.E_JOBDETAILS AS ET ON E.EMPLOYEEID = ET.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E_TEAM AS T ON T.TEAMID = ET.TEAM LEFT OUTER JOIN
                      dbo.E_CONTACT AS EC ON E.EMPLOYEEID = EC.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E__EMPLOYEE AS EL ON ET.LINEMANAGER = EL.EMPLOYEEID LEFT OUTER JOIN
                      dbo.G_OFFICE AS G ON ET.OFFICELOCATION = G.OFFICEID LEFT OUTER JOIN 
                      dbo.AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID LEFT OUTER JOIN 
                      dbo.E_JOBROLE JR ON JR.JobRoleId = ET.JobRoleId
WHERE     (ET.ACTIVE = 1)
GO


EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "E"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 28
         End
         Begin Table = "ET"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T"
            Begin Extent = 
               Top = 6
               Left = 298
               Bottom = 121
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EC"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 361
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "EL"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 481
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "G"
            Begin Extent = 
               Top = 126
               Left = 303
               Bottom = 211
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
  ', 'SCHEMA', N'dbo', 'VIEW', N'VW_EMPLOYEES_INTRANET', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'       Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'VW_EMPLOYEES_INTRANET', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'VW_EMPLOYEES_INTRANET', NULL, NULL
GO
