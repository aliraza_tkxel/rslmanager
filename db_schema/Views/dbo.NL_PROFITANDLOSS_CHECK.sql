SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[NL_PROFITANDLOSS_CHECK] AS
--PARENT
SELECT PL.ACCOUNTID FROM NL_PROFITANDLOSS PL 
	INNER JOIN NL_ACCOUNT A ON PL.ACCOUNTID = A.ACCOUNTID AND PARENTREFLISTID IS NULL
UNION ALL
--CHILDREN OF PARENTS ABOVE
SELECT ACCOUNTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (
SELECT PL.ACCOUNTID FROM NL_PROFITANDLOSS PL 
	INNER JOIN NL_ACCOUNT A ON PL.ACCOUNTID = A.ACCOUNTID AND PARENTREFLISTID IS NULL
	)
UNION ALL
--CHILD KEYS
SELECT PL.ACCOUNTID FROM NL_PROFITANDLOSS PL 
	INNER JOIN NL_ACCOUNT A ON PL.ACCOUNTID = A.ACCOUNTID AND PARENTREFLISTID IS NOT NULL


GO
