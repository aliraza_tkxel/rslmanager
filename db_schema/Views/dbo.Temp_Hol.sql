SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Temp_Hol]
AS
SELECT 
	dbo.E_ABSENCE.ABSENCEHISTORYID
	,E_ABSENCE.STARTDATE as StartDate
	, E_ABSENCE.RETURNDATE	as EndDate
	, E_JOURNAL.employeeid as OperativeId
	,E_ABSENCE.HolType
	,E_ABSENCE.duration
	,CASE 
		WHEN HolType = 'M' THEN '08:00 AM'
		WHEN HolType = 'A' THEN '01:00 PM'
		WHEN HolType = 'F' THEN '00:00 AM'
		WHEN HolType = 'F-F' THEN '00:00 AM'
		WHEN HolType = 'F-M' THEN '00:00 AM'
		WHEN HolType = 'A-F' THEN '01:00 PM'
	END as StartTime
	,CASE 
		WHEN HolType = 'M' THEN '12:00 PM'
		WHEN HolType = 'A' THEN '05:00 PM'
		WHEN HolType = 'F' THEN '11:59 PM'
		WHEN HolType = 'F-F' THEN '11:59 PM'
		WHEN HolType = 'F-M' THEN '12:00 PM'
		WHEN HolType = 'A-F' THEN '11:59 PM'
	END as EndTime
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '08:00 AM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
	END as StartTimeInMin
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '12:00 PM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '05:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '12:00 PM',103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
	END as EndTimeInMin
	,n.DESCRIPTION
	FROM E_JOURNAL 
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID AND E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID)
	LEFT JOIN dbo.E_NATURE N ON N.ITEMNATUREID = dbo.E_JOURNAL.ITEMNATUREID
	INNER JOIN Temp_Op O ON O.EmployeeId = dbo.E_Journal.EMPLOYEEID 
	WHERE dbo.E_JOURNAL.itemnatureid in (2,3,4,5,6,8,9,10,11,13,14,15,16,32,43,47) 
	AND E_ABSENCE.ItemStatusId = 5
	AND E_ABSENCE.returndate >= CONVERT(DATE,getdate())
	

GO
