SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.CASHBOOK_PAYMENTSLIP
AS
SELECT     ISNULL(BR.GROUPINGID, - 1) AS RECCED, 3 AS THETYPE, PS.PAYMENTSLIPID AS THEID, COUNT(*) AS ITEMCOUNT, CONVERT(VARCHAR, 
                      CP.CREATIONDATE, 103) AS BOOKDATE, 'Payment Slip: ' + PS.SLIPNUMBER AS DESCRIPTION, CP.CREATIONDATE AS TRANSACTIONDATE, 
                      CASE WHEN SUM(CP.AMOUNT) > 0 THEN SUM(CP.AMOUNT) ELSE 0 END AS DEBIT, CASE WHEN SUM(CP.AMOUNT) 
                      <= 0 THEN ABS(SUM(CP.AMOUNT)) ELSE 0 END AS CREDIT
FROM         dbo.F_CASHPOSTING AS CP INNER JOIN
                      dbo.F_PAYMENTSLIP AS PS ON PS.PAYMENTSLIPID = CP.PAYMENTSLIPNUMBER LEFT OUTER JOIN
                      dbo.F_BANK_RECONCILIATION AS BR ON BR.RECCODE = CP.CASHPOSTINGID AND BR.RECTYPE = 7
WHERE     (CP.PAYMENTTYPE IN (5, 8, 92, 93, 25))
GROUP BY PS.SLIPNUMBER, CP.CREATIONDATE, PS.PAYMENTSLIPID, BR.GROUPINGID
UNION ALL
SELECT     - 1 AS RECCED, 23 AS THETYPE, CP.JOURNALID AS THEID, 1 AS ITEMCOUNT, CONVERT(VARCHAR, CP.CREATIONDATE, 103) AS BOOKDATE, 
                      'Cashier slip not attached to Slip' AS DESCRIPTION, CP.CREATIONDATE AS TRANSACTIONDATE, 
                      CASE WHEN CP.AMOUNT > 0 THEN CP.AMOUNT ELSE 0 END AS DEBIT, CASE WHEN CP.AMOUNT <= 0 THEN ABS(CP.AMOUNT) 
                      ELSE 0 END AS CREDIT
FROM         dbo.F_CASHPOSTING AS CP LEFT OUTER JOIN
                      dbo.F_PAYMENTSLIP AS PS ON PS.PAYMENTSLIPID = CP.PAYMENTSLIPNUMBER
WHERE     (CP.PAYMENTTYPE IN (5, 8, 92, 93, 25)) AND (PS.PAYMENTSLIPID IS NULL)
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 3
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2940
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 5
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'CASHBOOK_PAYMENTSLIP', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'CASHBOOK_PAYMENTSLIP', NULL, NULL
GO
