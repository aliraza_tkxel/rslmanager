SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [web].neighbourhoodAuthorities AS
SELECT [authorityId]
      ,[local_authority]
      ,[website]
  FROM [RSLBHALive].[web].[neighbourhoodAuth]
GO
