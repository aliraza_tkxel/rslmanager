SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.vwLastSurvey
AS
SELECT     dbo.TBL_PDA_SURVEY_DETAIL.PropertyID, dbo.TBL_PDA_SURVEY.SurveyDate, dbo.TBL_PDA_SURVEY_DETAIL.SurveyType
FROM         dbo.TBL_PDA_SURVEY INNER JOIN
                      dbo.TBL_PDA_SURVEY_DETAIL ON dbo.TBL_PDA_SURVEY.SurveyID = dbo.TBL_PDA_SURVEY_DETAIL.SurveyID
WHERE     (dbo.TBL_PDA_SURVEY.SurveyDate IN
                          (SELECT     TOP 1 a.SurveyDate
                            FROM          dbo.TBL_PDA_SURVEY AS a INNER JOIN
                                                   dbo.TBL_PDA_SURVEY_DETAIL AS b ON a.SurveyID = b.SurveyID
                            WHERE      (b.PropertyID = dbo.TBL_PDA_SURVEY_DETAIL.PropertyID)
                            ORDER BY a.SurveyDate DESC))
GO
