SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*WHERE 
a.OperativeID = 860
AND
p.ADDRESS1 LIKE '%Pleasance Close%'
AND
p.HOUSENUMBER = '7'
AND a.AppointmentDate >= '04/07/2013' AND a.AppointmentDate < '05/07/2013'*/
CREATE VIEW dbo.vwQVFL
AS
SELECT        TOP (100) PERCENT ISNULL(p.FLATNUMBER, '') AS [Flat No.], ISNULL(p.HOUSENUMBER, '') AS [House No.], ISNULL(p.ADDRESS1, '') AS [Address Line 1], 
                         ISNULL(p.ADDRESS2, '') AS [Address Line 2], ISNULL(p.ADDRESS3, '') AS [Address Line 3], ISNULL(p.TOWNCITY, '') AS [Town/City], ISNULL(p.POSTCODE, '') 
                         AS Postcode, ISNULL(p.COUNTY, '') AS County, fs.Description AS [Fault Status], a.AppointmentID, a.AppointmentDate, e.FIRSTNAME + ' ' + e.LASTNAME AS Operative, 
                         a.LetterID, a.AppointmentStageID, a.LastActionDate, a.Notes, a.Time, a.AppointmentStatus, a.EndTime, a.RepairNotes, a.RepairCompletionDateTime, 
                         YEAR(a.RepairCompletionDateTime) AS YearRepairComplete, DATENAME(month, a.RepairCompletionDateTime) AS MonthRepairComplete, 
                         MONTH(a.RepairCompletionDateTime) AS MonthNoRepairComplete, DAY(a.RepairCompletionDateTime) AS DayRepairComplete, 
                         { fn HOUR(a.RepairCompletionDateTime) } AS HourRepairComplete, a.isCalendarAppointment, fl.FaultLogID, fl.CustomerId, fl.FaultID, fl.FaultBasketID, fl.SubmitDate, 
                         fl.ORGID, fl.StatusID, fl.IsReported, fl.Quantity, fl.ProblemDays, fl.RecuringProblem, fl.CommunalProblem, fl.Notes AS Expr1, fl.JobSheetNumber, fl.DueDate, 
                         fl.IsSelected, fl.UserId, fl.Recharge, fl.PROPERTYID, fl.ContractorID, fl.FaultTradeID, patch.LOCATION AS Patch
FROM            dbo.FL_CO_APPOINTMENT AS a INNER JOIN
                         dbo.FL_FAULT_APPOINTMENT AS fa ON a.AppointmentID = fa.AppointmentId INNER JOIN
                         dbo.FL_FAULT_LOG AS fl ON fl.FaultLogID = fa.FaultLogId INNER JOIN
                         dbo.C__CUSTOMER AS c ON c.CUSTOMERID = fl.CustomerId INNER JOIN
                         dbo.C_CUSTOMERTENANCY AS ct ON ct.CUSTOMERID = c.CUSTOMERID INNER JOIN
                         dbo.C_TENANCY AS t ON t.TENANCYID = ct.TENANCYID INNER JOIN
                         dbo.P__PROPERTY AS p ON p.PROPERTYID = t.PROPERTYID INNER JOIN
                         dbo.FL_FAULT_STATUS AS fs ON fl.StatusID = fs.FaultStatusID LEFT OUTER JOIN
                         dbo.E_PATCH AS patch ON p.PATCH = patch.PATCHID LEFT OUTER JOIN
                         dbo.E__EMPLOYEE AS e ON a.OperativeID = e.EMPLOYEEID
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[17] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -136
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fa"
            Begin Extent = 
               Top = 6
               Left = 312
               Bottom = 118
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fl"
            Begin Extent = 
               Top = 6
               Left = 546
               Bottom = 135
               Right = 740
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 283
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 120
               Left = 312
               Bottom = 249
               Right = 528
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 138
               Left = 566
               Bottom = 267
               Right = 799
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 399
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         E', 'USER', N'dbo', 'VIEW', N'vwQVFL', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'nd
         Begin Table = "fs"
            Begin Extent = 
               Top = 252
               Left = 321
               Bottom = 364
               Right = 491
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 272
               Left = 564
               Bottom = 401
               Right = 809
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "patch"
            Begin Extent = 
               Top = 27
               Left = 812
               Bottom = 139
               Right = 987
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 49
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1950
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1845
         Width = 1500
         Width = 1500
         Width = 2205
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1635
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3240
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'USER', N'dbo', 'VIEW', N'vwQVFL', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'USER', N'dbo', 'VIEW', N'vwQVFL', NULL, NULL
GO
