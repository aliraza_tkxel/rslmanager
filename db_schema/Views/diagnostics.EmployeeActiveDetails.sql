SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW diagnostics.EmployeeActiveDetails

AS 

SELECT  
		 E.EMPLOYEEID
		,E.FIRSTNAME AS [First Name] 
		,E.LASTNAME AS Surname
		,ISNULL((E1.FirstName + ' ' + E1.LastName), '') AS [Line Manager]
FROM    E__EMPLOYEE E
        LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID
		LEFT JOIN dbo.E__EMPLOYEE E1 ON E1.EMPLOYEEID = j.LINEMANAGER                                    
WHERE   j.active = 1
        AND ( J.TEAM <> 1 -- Contractors
              OR J.TEAM IS NULL
            )

        
        
GO
