SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[tempWorkingPattern]

AS 

WITH CTE_WorkingPattern (EmployeeId, Mon, Tue, Wed, Thu, Fri, Sat, Sun, StartDate, EndDate)
AS 
(
SELECT EmployeeId, Mon, Tue, Wed, Thu, Fri, Sat, Sun, STARTDATE, CAST(GETDATE() + 365 AS DATE) AS EndDate FROM  dbo.E_WorkingHours
UNION ALL 
SELECT EmployeeId, Mon, Tue, Wed, Thu, Fri, Sat, Sun, STARTDATE, EndDate FROM  dbo.E_WorkingHours_History
)
SELECT DISTINCT EmployeeId, Mon, Tue, Wed, Thu, Fri, Sat, Sun, StartDate, EndDate FROM CTE_WorkingPattern WHERE StartDate IS NOT NULL 



GO
