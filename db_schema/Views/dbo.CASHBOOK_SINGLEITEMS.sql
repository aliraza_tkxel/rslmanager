SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.CASHBOOK_SINGLEITEMS
AS
SELECT     ISNULL(BR.GROUPINGID, - 1) AS RECCED, 1 AS THETYPE, RJ.JOURNALID AS THEID, 1 AS ITEMCOUNT, CONVERT(VARCHAR, RJ.TRANSACTIONDATE, 
                      103) AS BOOKDATE, P.DESCRIPTION, RJ.TRANSACTIONDATE, CASE WHEN RJ.AMOUNT < 0 THEN - RJ.AMOUNT ELSE 0 END AS DEBIT, 
                      CASE WHEN RJ.AMOUNT > 0 THEN RJ.AMOUNT ELSE 0 END AS CREDIT
FROM         dbo.F_RENTJOURNAL AS RJ LEFT OUTER JOIN
                      dbo.F_PAYMENTTYPE AS P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE LEFT OUTER JOIN
                      dbo.F_BANK_RECONCILIATION AS BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1
WHERE     (RJ.STATUSID <> 1) AND (RJ.PAYMENTTYPE IN (3, 7, 17, 18, 22, 25, 14, 19, 23, 24, 29, 55, 56, 57, 58, 60, 64, 65, 66, 67, 94))
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RJ"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "P"
            Begin Extent = 
               Top = 6
               Left = 262
               Bottom = 114
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BR"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'CASHBOOK_SINGLEITEMS', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'CASHBOOK_SINGLEITEMS', NULL, NULL
GO
