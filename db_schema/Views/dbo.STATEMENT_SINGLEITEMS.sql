SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[STATEMENT_SINGLEITEMS] AS
SELECT 	ISNULL(BR.GROUPINGID,-1) AS RECCED, 
	1 AS THETYPE, 
	RJ.JOURNALID AS THEID, 
	1 AS ITEMCOUNT, 
	CONVERT(VARCHAR, BS.STATEMENTDATE, 103) AS BOOKDATE, 
	P.DESCRIPTION, 
	BS.STATEMENTDATE,
	CASE WHEN RJ.AMOUNT < 0 THEN -RJ.AMOUNT ELSE 0 END AS DEBIT,
	CASE WHEN RJ.AMOUNT > 0 THEN RJ.AMOUNT ELSE 0 END AS CREDIT
FROM F_RENTJOURNAL RJ 
	LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE 
	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1
	INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID
WHERE RJ.STATUSID <> 1 
	AND RJ.PAYMENTTYPE IN (3,7,17,18,22,25)



GO
