SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.vw_TO_GETAPPOINTMENTS
AS
SELECT     'Reactive Repair' AS [Type], ct.TenancyId, /*fl.CustomerId ,*/ CAST(a.AppointmentDate AS DATE) AS [AppointmentDate], 
			CASE 
					WHEN CAST([time] AS TIME) >= '08:00:00' and CAST([time] AS TIME) < '13:00:00' THEN (SELECT 'Between 8am - 1pm') 
					WHEN CAST([time] AS TIME) >= '13:00:00' and CAST([time] AS TIME) < '17:00:00' THEN (SELECT 'Between 12noon - 5pm') 
					ELSE (SELECT 'Out of Hours') 
					END AS [AppointmentSlot],
					CAST(AppointmentDate + ' ' + [Time] AS DATETIME) AS [appstart]
FROM         dbo.FL_CO_APPOINTMENT a LEFT JOIN
                          (SELECT     AppointmentId, MAX(FaultLogId) AS FaultLogId
                            FROM          FL_FAULT_APPOINTMENT
                            GROUP BY AppointmentId) fa ON (fa.AppointmentId = a.AppointmentID) LEFT JOIN
                      FL_FAULT_LOG fl ON (fa.FaultLogId = fl.FaultLogID) LEFT JOIN
                      dbo.vw_PROPERTY_CURRENT_TENANTS_LIST ct ON (fl.CustomerId = ct.CUSTOMERID)
WHERE     1 = 1 AND a.AppointmentStatus <> '' AND a.AppointmentStatus != 'Complete' AND a.AppointmentDate >= CONVERT(DATE, GETDATE())
UNION ALL
SELECT     'Gas Servicing' AS [Type], ct.TenancyId, CAST(a.AppointmentDate AS DATE) AS [AppointmentDate], 
					CASE 
					WHEN CAST(APPOINTMENTSTARTTIME AS TIME) >= '08:00:00' and CAST(APPOINTMENTSTARTTIME AS TIME) < '13:00:00' THEN (SELECT 'Between 8am - 1pm') 
					WHEN CAST(APPOINTMENTSTARTTIME AS TIME) >= '13:00:00' and CAST(APPOINTMENTSTARTTIME AS TIME) < '17:00:00' THEN (SELECT 'Between 12noon - 5pm') 
					ELSE (SELECT 'Out of Hours') 
					END AS [AppointmentSlot],
					CAST(AppointmentDate + ' ' + APPOINTMENTSTARTTIME AS DATETIME) AS [appstart]
FROM         AS_APPOINTMENTS a LEFT JOIN
                      dbo.AS_JOURNAL j ON (a.JournalId = j.JOURNALID) LEFT JOIN
                          (SELECT     PropertyId, TenancyId, MAX(CUSTOMERID) AS CUSTOMERID
                            FROM          dbo.vw_PROPERTY_CURRENT_TENANTS_LIST
                            GROUP BY PropertyId, TenancyId) ct ON (j.PROPERTYID = ct.PropertyId)
WHERE     1 = 1 AND APPOINTMENTSTATUS != 'Finished' AND a.AppointmentDate >= CONVERT(DATE, GETDATE())
UNION ALL
SELECT     'Planned Works' AS [Type], ct.TenancyId, CAST(a.AppointmentDate AS DATE) AS [AppointmentDate], 
					CASE 
					WHEN CAST(APPOINTMENTSTARTTIME AS TIME) >= '08:00:00' and CAST(APPOINTMENTSTARTTIME AS TIME) < '13:00:00' THEN (SELECT 'Between 8am - 1pm') 
					WHEN CAST(APPOINTMENTSTARTTIME AS TIME) >= '13:00:00' and CAST(APPOINTMENTSTARTTIME AS TIME) < '17:00:00' THEN (SELECT 'Between 12noon - 5pm') 
					ELSE (SELECT 'Out of Hours') 
					END AS [AppointmentSlot],
					CAST(AppointmentDate + ' ' + APPOINTMENTSTARTTIME AS DATETIME) AS [appstart]
FROM         PLANNED_APPOINTMENTS a LEFT JOIN
                          (SELECT     TenancyId, MAX(CUSTOMERID) AS CUSTOMERID
                            FROM          dbo.vw_PROPERTY_CURRENT_TENANTS_LIST
                            GROUP BY TenancyId) ct ON (a.TENANCYID = ct.TenancyId)
WHERE     1 = 1 AND APPOINTMENTSTATUS != 'Finished' AND a.AppointmentDate >= CONVERT(DATE, GETDATE())
UNION ALL
SELECT     'Stock Survey' AS [Type], [as].TenancyId, CAST(a.AppointStartDateTime AS DATE) AS [AppointmentDate], 
					CASE 
					WHEN CAST(AppointStartDateTime AS TIME) >= '08:00:00' and CAST(AppointStartDateTime AS TIME) < '13:00:00' THEN (SELECT 'Between 8am - 1pm') 
					WHEN CAST(AppointStartDateTime AS TIME) >= '13:00:00' and CAST(AppointStartDateTime AS TIME) < '17:00:00' THEN (SELECT 'Between 12noon - 5pm') 
					ELSE (SELECT 'Out of Hours') 
					END AS [AppointmentSlot],
					CAST(AppointStartDateTime AS DATETIME) AS [appstart]
FROM         dbo.PS_Appointment a LEFT JOIN
                      dbo.PS_Property2Appointment[as] ON (a.AppointId = [as].AppointId)
WHERE     1 = 1 AND AppointStartDateTime >= CONVERT(DATE, GETDATE()) AND AppointProgStatus != 'Complete'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'USER', N'dbo', 'VIEW', N'vw_TO_GETAPPOINTMENTS', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'USER', N'dbo', 'VIEW', N'vw_TO_GETAPPOINTMENTS', NULL, NULL
GO
