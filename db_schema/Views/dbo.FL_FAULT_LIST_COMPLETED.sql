SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FL_FAULT_LIST_COMPLETED]
AS
    SELECT  MAX(FaultLogHistoryId) AS FaultLogHistoryId ,
            FaultLogId
    FROM    dbo.FL_FAULT_lOG_HISTORY
    WHERE   FaultStatusID = 17
    GROUP BY FaultLogID


GO
