SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[Temp_App]

AS
	
SELECT AppointmentDate
	,OperativeId
	,Time as StartTime
	,EndTime as EndTime
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + Time,103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,p__property.postcode as PostCode
	,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
	,P__PROPERTY.TownCity as TownCity    
	,P__PROPERTY.County as County  
	FROM FL_CO_Appointment 
	inner join fl_fault_appointment on FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
	inner join fl_fault_log on fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
	inner join p__property on fl_fault_log.propertyid = p__property.propertyid	
	INNER JOIN Temp_Op O ON O.EmployeeId = dbo.FL_CO_Appointment.OperativeID
	WHERE appointmentdate >= CONVERT(DATE,getdate())
	AND (AppointmentStageID <> 4 OR AppointmentStageID IS NULL)

	

GO
