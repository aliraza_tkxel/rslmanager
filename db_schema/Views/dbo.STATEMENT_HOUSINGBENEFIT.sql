SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


ALTER VIEW [dbo].[STATEMENT_HOUSINGBENEFIT] AS
SELECT 	ISNULL(BR.GROUPINGID,-1) AS RECCED, 
	4 AS THETYPE, 
	LA.LOCALAUTHORITYID AS THEID, 
	COUNT(*) AS ITEMCOUNT, 
	CONVERT(VARCHAR, BS.STATEMENTDATE, 103) AS BOOKDATE, 
	'HB: ' + CASE WHEN LEN( LA.DESCRIPTION) > 22 THEN LEFT(LA.DESCRIPTION , 17) + '...' ELSE LA.DESCRIPTION END AS DESCRIPTION, 
	BS.STATEMENTDATE,
	CASE WHEN SUM(RJ.AMOUNT) < 0 THEN ABS(SUM(RJ.AMOUNT))  ELSE 0 END AS DEBIT,
	CASE WHEN SUM(RJ.AMOUNT) > 0 THEN ABS(SUM(RJ.AMOUNT))  ELSE 0 END AS CREDIT 
FROM F_RENTJOURNAL RJ 
	INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID 
	INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID 
	INNER JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID 
	LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = DEV.LOCALAUTHORITY 
	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1
	INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID
WHERE 	RJ.STATUSID <> 1 AND RJ.PAYMENTTYPE IN (1,15,16)  
GROUP 	BY BS.STATEMENTDATE, LA.DESCRIPTION, LA.LOCALAUTHORITYID, BR.GROUPINGID




GO
