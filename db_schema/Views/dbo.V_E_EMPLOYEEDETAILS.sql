
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.V_E_EMPLOYEEDETAILS
AS
SELECT     E.EMPLOYEEID, LEFT(E.FIRSTNAME, 1) + ' ' + E.LASTNAME AS FULLNAME, ISNULL(CONVERT(NVARCHAR, E.DOB, 103), '') AS DOB, 
                      ISNULL(E.GENDER, '') AS GENDER, ISNULL(M.DESCRIPTION, '') AS MARITALSTATUS, ISNULL(ETH.DESCRIPTION, '') AS ETHNICITY, ISNULL(E.BANK, 
                      '') AS BANK, ISNULL(E.SORTCODE, '') AS SORTCODE, ISNULL(E.ACCOUNTNUMBER, '') AS ACCOUNTNUMBER, ISNULL(E.ACCOUNTNAME, '') 
                      AS ACCOUNTNAME, ISNULL(CONVERT(NVARCHAR, J.STARTDATE, 103), '') AS STARTDATE, ISNULL(CONVERT(NVARCHAR, J.ENDDATE, 103), '') 
                      AS ENDDATE, ISNULL(jr.JobeRoleDescription, '') AS JOBTITLE, ISNULL(J.REFERENCENUMBER, '') AS REFERENCENUMBER, ISNULL(J.DETAILSOFROLE, '') 
                      AS ROLE, ISNULL(T.TEAMNAME, '') AS TEAMNAME, ISNULL(P.LOCATION, '') AS PATCH, ISNULL(LINEMAN.FIRSTNAME + ' ' + LINEMAN.LASTNAME, '') 
                      AS LINEMANAGER, ISNULL(G.DESCRIPTION, '') AS GRADE, ISNULL(CAST(J.SALARY AS NVARCHAR), '') AS SALARY, ISNULL(J.TAXOFFICE, '') 
                      AS TAXOFFICE, ISNULL(J.TAXCODE, '') AS TAXCODE, ISNULL(J.PAYROLLNUMBER, '') AS PAYROLLNUMBER, ISNULL(J.DRIVINGLICENSENUMBER, '') 
                      AS DRIVINGLICENSENUMBER, ISNULL(J.NINUMBER, '') AS NINUMBER, ISNULL(CAST(J.HOLIDAYENTITLEMENTDAYS AS NVARCHAR), '0') 
                      AS HOLIDAYENTITLEMENTDAYS, ISNULL(CAST(J.HOLIDAYENTITLEMENTHOURS AS NVARCHAR), '0') AS HOLIDAYENTITLEMENTHOURS, 
                      ISNULL(CONVERT(NVARCHAR, J.REVIEWDATE, 103), '') AS REVIEWDATE, ISNULL(CONVERT(NVARCHAR, J.APPRAISALDATE, 103), '') 
                      AS APPRAISALDATE, ISNULL(PRO.DESCRIPTION, '') AS PROBATIONPERIOD, ISNULL(PAR.DESCRIPTION, '') AS PARTFULLTIME, 
                      ISNULL(CAST(J.HOURS AS NVARCHAR), '') AS HOURS, ISNULL(N.DESCRIPTION, '') AS NOTICEPERIOD, ISNULL(CONVERT(NVARCHAR, 
                      J.DATEOFNOTICE, 103), '') AS DATEOFNOTICE, ISNULL(J.FOREIGNNATIONALNUMBER, '') AS FOREIGNNATIONALNUMBER, 
                      ISNULL(BUDDY.FIRSTNAME + ' ' + BUDDY.LASTNAME, '') AS BUDDY, 
                      CASE WHEN j.ACTIVE = 1 THEN 'Yes' WHEN j.ACTIVE = 0 THEN 'No' ELSE '' END AS ACTIVE, 
                      CASE WHEN ISDIRECTOR = 1 THEN 'Yes' ELSE 'No' END AS DIRECTOR, CASE WHEN ISMANAGER = 1 THEN 'Yes' ELSE 'No' END AS MANAGER, 
                      ISNULL(C.ADDRESS1, '') AS ADDRESS1, ISNULL(C.ADDRESS2, '') AS ADDRESS2, ISNULL(C.ADDRESS3, '') AS ADDRESS3, ISNULL(C.POSTALTOWN, '') 
                      AS POSTALTOWN, ISNULL(C.COUNTY, '') AS COUNTY, ISNULL(C.POSTCODE, '') AS POSTCODE, ISNULL(C.HOMETEL, '') AS HOMETEL, 
                      ISNULL(C.MOBILE, '') AS MOBILE, ISNULL(C.WORKDD, '') AS WORKDD, ISNULL(C.WORKEXT, '') AS WORKEXT, ISNULL(C.HOMEEMAIL, '') 
                      AS HOMEEMAIL, ISNULL(C.WORKEMAIL, '') AS WORKEMAIL, ISNULL(C.EMERGENCYCONTACTNAME, '') AS EMERGENCYCONTACTNAME, 
                      ISNULL(C.EMERGENCYCONTACTTEL, '') AS EMERGENCYCONTACTTEL, ISNULL(CAST(B.MEMNUMBER AS NVARCHAR), '') AS MEMNUMBER, 
                      ISNULL(CAST(B.SCHEME AS NVARCHAR), '') AS SCHEME, ISNULL(CAST(B.SALARYPERCENT AS NVARCHAR), '') AS SALARYPERCENT, 
                      ISNULL(CAST(B.CONTRIBUTION AS NVARCHAR), '') AS CONTRIBUTION, ISNULL(CAST(B.AVC AS NVARCHAR), '') AS AVC, 
                      CASE WHEN B.CONTRACTEDOUT = 1 THEN 'Yes' WHEN B.CONTRACTEDOUT = 0 THEN 'No' ELSE '' END AS CONTRACTEDOUT, 
                      ISNULL(CAST(B.CARMAKE AS NVARCHAR), '') AS CARMAKE, ISNULL(CAST(B.MODEL AS NVARCHAR), '') AS MODEL, 
                      ISNULL(CAST(B.LISTPRICE AS NVARCHAR), '') AS LISTPRICE, ISNULL(CAST(B.CONTHIRECHARGE AS NVARCHAR), '') AS CONTHIRECHARGE, 
                      ISNULL(CAST(B.EMPCONTRIBUTION AS NVARCHAR), '') AS EMPCONTRIBUTION, ISNULL(CAST(B.EXCESSCONTRIBUTION AS NVARCHAR), '') 
                      AS EXCESSCONTRIBUTION, ISNULL(CAST(B.PROFESSIONALFEES AS NVARCHAR), '') AS PROFESSIONALFEES, 
                      ISNULL(CAST(B.TELEPHONEALLOWANCE AS NVARCHAR), '') AS TELEPHONEALLOWANCE, ISNULL(CAST(B.FIRSTAIDALLOWANCE AS NVARCHAR), '') 
                      AS FIRSTAIDALLOWANCE, ISNULL(CAST(B.CALLOUTALLOWANCE AS NVARCHAR), '') AS CALLOUTALLOWANCE, 
                      ISNULL(CAST(B.CARALLOWANCE AS NVARCHAR), '') AS CARALLOWANCE, ISNULL(CONVERT(NVARCHAR, B.CARLOANSTARTDATE, 103), '') 
                      AS CARLOANSTARTDATE, ISNULL(CAST(B.ENGINESIZE AS NVARCHAR), '') AS ENGINESIZE, 
                      CASE WHEN B.LOANINFORMATION = 1 THEN 'Yes' WHEN B.LOANINFORMATION = 0 THEN 'No' ELSE '' END AS LOANINFORMATION, 
                      ISNULL(CAST(B.TERM AS NVARCHAR), '') AS TERM, ISNULL(CAST(B.VALUE AS NVARCHAR), '') AS VALUE, 
                      ISNULL(CAST(B.MONTHLYREPAY AS NVARCHAR), '') AS MONTHLYREPAY, ISNULL(CAST(B.ACCOMODATIONRENT AS NVARCHAR), '') 
                      AS ACCOMODATIONRENT, ISNULL(CAST(B.COUNCILTAX AS NVARCHAR), '') AS COUNCILTAX, ISNULL(CAST(B.HEATING AS NVARCHAR), '') AS HEATING, 
                      ISNULL(CAST(B.LINERENTAL AS NVARCHAR), '') AS LINERENTAL, ISNULL(CAST(B.ANNUALPREMIUM AS NVARCHAR), '') AS ANNUALPREMIUM, 
                      ISNULL(CAST(B.TAXABLEBENEFIT AS NVARCHAR), '') AS TAXABLEBENEFIT, ISNULL(CAST(B.ADDITIONALMEMBERS AS NVARCHAR), '') 
                      AS ADDITIONALMEMBERS, ISNULL(CAST(B.ADDITIONALMEMBERS2 AS NVARCHAR), '') AS ADDITIONALMEMBERS2, 
                      ISNULL(CAST(B.ADDITIONALMEMBERS3 AS NVARCHAR), '') AS ADDITIONALMEMBERS3, ISNULL(CAST(B.ADDITIONALMEMBERS4 AS NVARCHAR), '') 
                      AS ADDITIONALMEMBERS4, ISNULL(CAST(B.ADDITIONALMEMBERS5 AS NVARCHAR), '') AS ADDITIONALMEMBERS5, ISNULL(LEFT(NOK.FNAME, 1) 
                      + ' ' + NOK.SNAME, 'N/A') AS NOK_FULLNAME, ISNULL(NOK.HOUSENUMBER + ' ' + NOK.STREET, 'N/A') AS NOK_STREET, ISNULL(NOK.POSTALTOWN, 
                      'N/A') AS NOK_TOWN, ISNULL(NOK.COUNTY, 'N/A') AS NOK_COUNTY, ISNULL(NOK.POSTCODE, 'N/A') AS NOK_POSTCODE, ISNULL(NOK.HOMETEL, 
                      'N/A') AS NOK_HOMETEL, ISNULL(NOK.MOBILE, 'N/A') AS NOK_MOBILE, ISNULL(CAST(B.GROUPSCHEMEREF AS NVARCHAR), '') 
                      AS GROUPSCHEMEREF, ISNULL(CAST(B.MEMBERSHIPNO AS NVARCHAR), '') AS MEMBERSHIPNO, E.LASTLOGGEDIN, ISNULL(J.HOLIDAYRULE, 0) 
                      AS HRULE
FROM         dbo.E__EMPLOYEE AS E LEFT OUTER JOIN
                      dbo.E_JOBDETAILS AS J ON E.EMPLOYEEID = J.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E_JOBROLE AS JR ON J.JobRoleId = JR.JobRoleId  LEFT OUTER JOIN
                      dbo.E__EMPLOYEE AS LINEMAN ON J.LINEMANAGER = LINEMAN.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E__EMPLOYEE AS BUDDY ON J.BUDDY = BUDDY.EMPLOYEEID LEFT OUTER JOIN
                      dbo.G_MARITALSTATUS AS M ON E.MARITALSTATUS = M.MARITALSTATUSID LEFT OUTER JOIN
                      dbo.G_ETHNICITY AS ETH ON E.ETHNICITY = ETH.ETHID LEFT OUTER JOIN
                      dbo.E_TEAM AS T ON J.TEAM = T.TEAMID LEFT OUTER JOIN
                      dbo.E_PATCH AS P ON J.PATCH = P.PATCHID LEFT OUTER JOIN
                      dbo.E_GRADE AS G ON J.GRADE = G.GRADEID LEFT OUTER JOIN
                      dbo.E_PROBATIONPERIOD AS PRO ON J.PROBATIONPERIOD = PRO.PROBATIONPERIODID LEFT OUTER JOIN
                      dbo.E_PARTFULLTIME AS PAR ON J.PARTFULLTIME = PAR.PARTFULLTIMEID LEFT OUTER JOIN
                      dbo.E_NOTICEPERIODS AS N ON J.NOTICEPERIOD = N.NOTICEPERIODID LEFT OUTER JOIN
                      dbo.E_PREVIOUSEMPLOYMENT AS PREV ON E.EMPLOYEEID = PREV.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E_CONTACT AS C ON E.EMPLOYEEID = C.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E_BENEFITS AS B ON E.EMPLOYEEID = B.EMPLOYEEID LEFT OUTER JOIN
                      dbo.E_NEXTOFKIN AS NOK ON E.EMPLOYEEID = NOK.EMPLOYEEID  
GO

EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "E"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 28
         End
         Begin Table = "J"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LINEMAN"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 361
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BUDDY"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 481
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "M"
            Begin Extent = 
               Top = 6
               Left = 298
               Bottom = 91
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ETH"
            Begin Extent = 
               Top = 96
               Left = 298
               Bottom = 181
               Right = 450
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T"
            Begin Extent = 
               Top = 186
               Left = 303
               Bottom = 301
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
    ', 'SCHEMA', N'dbo', 'VIEW', N'V_E_EMPLOYEEDETAILS', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'     End
         Begin Table = "P"
            Begin Extent = 
               Top = 306
               Left = 298
               Bottom = 406
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "G"
            Begin Extent = 
               Top = 408
               Left = 298
               Bottom = 508
               Right = 450
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PRO"
            Begin Extent = 
               Top = 486
               Left = 38
               Bottom = 571
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PAR"
            Begin Extent = 
               Top = 510
               Left = 265
               Bottom = 595
               Right = 430
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "N"
            Begin Extent = 
               Top = 576
               Left = 38
               Bottom = 661
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PREV"
            Begin Extent = 
               Top = 600
               Left = 244
               Bottom = 715
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 720
               Left = 38
               Bottom = 835
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "B"
            Begin Extent = 
               Top = 840
               Left = 38
               Bottom = 955
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NOK"
            Begin Extent = 
               Top = 720
               Left = 293
               Bottom = 835
               Right = 449
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'V_E_EMPLOYEEDETAILS', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'V_E_EMPLOYEEDETAILS', NULL, NULL
GO
