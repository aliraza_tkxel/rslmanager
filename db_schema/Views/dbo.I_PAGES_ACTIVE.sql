SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[I_PAGES_ACTIVE]
AS
SELECT     PageID, MenuID, WebsiteID, Title, Name, FileName, PageContent, Active, IsBespoke
FROM         dbo.I_PAGE
WHERE     (Active = 1)
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'I_PAGES_ACTIVE', NULL, NULL
GO
