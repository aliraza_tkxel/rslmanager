SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwP_SSIS_STOCK_SURVEY_DONE_PROPERTIES]
AS
    WITH    CTE_Property ( PropertyId )
              AS ( SELECT DISTINCT
                            s.PROPERTYID
                   FROM     dbo.PS_Survey s
                            INNER JOIN dbo.PS_Appointment2Survey ss ON s.SurveyId = ss.SurveyId
                            INNER JOIN dbo.PS_Appointment a ON ss.AppointId = a.AppointId
                            INNER JOIN dbo.P__PROPERTY p ON p.PROPERTYID = s.PROPERTYID
                   UNION
                   SELECT   PROPERTYID
                   FROM     SSIS_STOCK_SURVEY_INCLUDE_DONE -- specified by property team as done but possibly no survey record
                 )
    SELECT DISTINCT
            p.PROPERTYID ,
            p.HOUSENUMBER ,
            ISNULL(p.ADDRESS1, '') AddressLine1 ,
            ISNULL(p.ADDRESS2, '') AddressLine2 ,
            ISNULL(p.ADDRESS3, '') AddressLine3 ,
            ISNULL(p.TOWNCITY, '') CityTown ,
            ISNULL(p.COUNTY, '') County ,
            ISNULL(p.POSTCODE, '') Postcode ,
            ISNULL(a.AppointStartDateTime, '01/01/1900') AS [Appointment Start Date] ,
            ISNULL(a.AppointProgStatus, '') AS [Appointment Progress Status] ,
            ISNULL(a.AppointEndDateTime, '01/01/1900') AS AppointEndDateTime ,
            ISNULL(s.SurveyDate, '01/01/1900') AS SurveyDate ,
            ISNULL(a.SurveyourUserName, '') AS SurveyourUserName
    FROM    CTE_PROPERTY cte
            INNER JOIN P__PROPERTY p ON cte.PropertyId = p.PropertyId
            LEFT JOIN PS_Property2Appointment pa ON pa.PropertyId = p.PropertyId
            LEFT JOIN dbo.PS_Appointment a ON pa.AppointId = a.AppointId
            LEFT JOIN dbo.PS_Appointment2Survey ss ON ss.AppointId = a.AppointId
            LEFT JOIN dbo.PS_Survey s ON s.SurveyId = ss.SurveyId

GO
