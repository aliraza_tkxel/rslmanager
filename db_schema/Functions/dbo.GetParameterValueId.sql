SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE function [dbo].[GetParameterValueId]
(
@locationName as varchar(20)
,@areaName as varchar(20)
,@itemName as varchar(50)
,@parameterName as varchar(50)
,@parameterValue as varchar(50)
)              
 returns int              
 as               
 begin              
           
    declare @valueId as int              
          
	SELECT	@valueId = PA_PARAMETER_VALUE.ValueID 
    FROM	PA_PARAMETER_VALUE     
    INNER JOIN PA_PARAMETER ON  PA_PARAMETER_VALUE.ParameterID = PA_PARAMETER.ParameterID 
    INNER JOIN PA_ITEM_PARAMETER ON PA_PARAMETER.ParameterID = PA_ITEM_PARAMETER.ParameterId 
    INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID     
    INNER JOIN PA_AREA on PA_ITEM.AreaID = PA_AREA.AreaID 
	INNER JOIN PA_LOCATION on PA_AREA.LocationId = PA_LOCATION.LocationID 
	WHERE PA_PARAMETER_VALUE.isactive = 1 
	AND PA_PARAMETER.IsActive = 1
	AND PA_ITEM_PARAMETER.IsActive = 1
	AND PA_ITEM.IsActive = 1
	AND ValueDetail = @parameterValue
	AND ParameterName = @parameterName
	AND LocationName = @locationName
	AND AreaName = @areaName
	AND ItemName = @itemName
         
    return(@valueId)              
               
 end  

GO
