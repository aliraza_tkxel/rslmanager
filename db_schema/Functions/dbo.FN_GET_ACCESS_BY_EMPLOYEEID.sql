SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[FN_GET_ACCESS_BY_EMPLOYEEID]
(
	@EmployeeId AS INT
) 
RETURNS @ACCESS TABLE (
	EmployeeId INT,
	Module VARCHAR(150), 
	Menu  VARCHAR(150), 
	Page  VARCHAR(150)
) AS
BEGIN 

WITH CTE_Data (ModuleId, ModuleDesc, MenuId, MenuModuleId, MenuDesc, PageId, PageDesc, PageMenuId)
AS
(
SELECT  mo.MODULEID ,
        mo.DESCRIPTION ,
        mi.MENUID ,
        mi.MODULEID ,
        mi.DESCRIPTION ,
        p.PAGEID ,
        p.DESCRIPTION ,
        p.MENUID
FROM    AC_NOACCESS a
        LEFT JOIN dbo.AC_MODULES mo ON a.ACCESSITEM = mo.MODULEID
                                       AND a.ACCESSLEVEL = 1
        LEFT JOIN dbo.AC_MENUS mi ON a.ACCESSITEM = mi.MENUID
                                     AND a.ACCESSLEVEL = 2
        LEFT JOIN dbo.AC_PAGES P ON a.ACCESSITEM = P.PAGEID
                                    AND a.ACCESSLEVEL = 3
WHERE   EMPLOYEEID = @EmployeeId
),
CTE_Menu (ModuleId, ModuleDesc, MenuId, MenuModuleId, MenuModule, MenuDesc, PageId, PageDesc, PageMenuId )
AS 
(
SELECT dd.ModuleId, dd.ModuleDesc, dd.MenuId, dd.MenuModuleId, m.DESCRIPTION, dd.MenuDesc, dd.PageId, dd.PageDesc, dd.PageMenuId FROM CTE_Data dd
LEFT JOIN 
dbo.AC_MODULES m
ON dd.MenuModuleId = m.MODULEID
)
, CTE_PAGE ( ModuleId, 
	ModuleDesc, 
	MenuId, 
	MenuModuleId, 
	MenuModule, 
	MenuDesc, 
	MenuPageMenuId, 
	MenuPageModuleId, 
	MenuPageModule, 
	MenuPageDesc, 
	PageId, 
	PageDesc, 
	PageMenuId
	)
AS
(
SELECT
	m.ModuleId, 
	m.ModuleDesc, 
	m.MenuId, 
	m.MenuModuleId, 
	m.MenuModule, 
	m.MenuDesc, 
	m1.MenuId, 
	m1.MenuModuleId, 
	m1.MenuModule, 
	m1.MenuDesc, 
	m.PageId, 
	m.PageDesc, 
	m.PageMenuId
FROM
	CTE_Menu m
LEFT JOIN 
	CTE_Menu m1
ON 
	m.PageMenuId = m1.MenuId
),
CTE_Result (Module, Menu, Page)
AS 
(
SELECT 
COALESCE(ModuleDesc, MenuModule, MenuPageModule) AS Module,
ISNULL(COALESCE(MenuDesc, MenuPageDesc), '') AS Menu,
ISNULL(PageDesc, '') AS Page
FROM 
	CTE_PAGE
)
INSERT INTO @ACCESS (EmployeeId, Module, Menu, Page)
SELECT @EmployeeId, Module, Menu, Page FROM CTE_Result
ORDER BY MODULE, Menu, Page 


RETURN
END 




GO
