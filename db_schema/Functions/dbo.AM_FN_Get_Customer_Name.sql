SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_Get_Customer_Name] 
(
	@tenancyId	int
)
RETURNS varchar(200)
AS
BEGIN
	
DECLARE @result	VARCHAR(200)
DECLARE @count INT
DECLARE	@name1	VARCHAR(100)
DECLARE @name2	VARCHAR(100)

	SELECT @count = Count(*)
	FROM AM_Customer_Rent_Parameters
	WHERE	TenancyId = @tenancyId

	IF(@count > 1)
	BEGIN
		SELECT TOP 1 @name1 = ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '')
		FROM AM_Customer_Rent_Parameters
		WHERE TenancyId = @tenancyId
		ORDER BY CustomerId ASC

		SELECT TOP 1 @name2 = ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '')
		FROM AM_Customer_Rent_Parameters
		WHERE TenancyId = @tenancyId
		ORDER BY CustomerId DESC

		SET @result = @name1 + ' & ' + @name2
	END
	ELSE
	BEGIN

		SELECT @name1 = ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '')
		FROM AM_Customer_Rent_Parameters
		WHERE TenancyId = @tenancyId

		SET @result = @name1
	END

RETURN @result

END



GO
