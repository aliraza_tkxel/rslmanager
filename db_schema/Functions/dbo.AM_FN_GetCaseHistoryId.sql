SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_GetCaseHistoryId] 
(
	@caseId	int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @caseHistoryId int

	SELECT  Top 1 @caseHistoryId = CaseHistoryId
	FROM AM_CaseHistory
	WHERE Caseid = @caseId
	order by CaseHistoryId desc

	RETURN @caseHistoryId

END



GO
