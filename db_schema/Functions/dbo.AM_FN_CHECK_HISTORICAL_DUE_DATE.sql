SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_CHECK_HISTORICAL_DUE_DATE]
(
	@total	int,
	@duration	varchar(100),
	@recordedDate	datetime	
)
RETURNS varchar(50)
AS
BEGIN
	
	DECLARE @date	DateTime
	DECLARE	@result varchar(50)
	DECLARE @currentDate int
	DECLARE @computedDate int

	if(@duration = 'Days')
	BEGIN
		SELECT @date = DATEADD(dd, @total, @recordedDate)
	END
	else if(@duration = 'Weeks')
	BEGIN
		SELECT @date = DATEADD(wk, @total, @recordedDate)
	END
	else if(@duration = 'Months')
	BEGIN
		SELECT @date = DATEADD(mm, @total, @recordedDate)	
	END
	else if(@duration = 'Years')
	BEGIN
		SELECT @date = DATEADD(yy, @total, @recordedDate)	
	END

	--SET @currentDate = Convert(varchar(100),GETDATE(), 103)
	--SET @computedDate = Convert(varchar(100),@date, 103)

	SET @currentDate = FLOOR(Convert(real,GETDATE()))
	SET @computedDate = FLOOR(Convert(real, @date))


if(@computedDate >= @currentDate)
	BEGIN		
		SET @result = 'false'
	END
	else
	BEGIN
		SET @result = 'true'
	END

return @result
END



GO
