USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('dbo.GetBoilerName') IS NULL
 EXEC('CREATE FUNCTION dbo.GetBoilerName() RETURNS INT AS  BEGIN RETURN 1 END ;') 
GO

ALTER function [dbo].[GetBoilerName]
(
@heatingId as int
,@schemeId as int
,@blockId as int
,@propertyId as nvarchar(40)
)              
 returns nvarchar(100)              
 as               
 begin              
           
    DECLARE @boilerName as nvarchar(100)                 
          
	IF @schemeId IS NOT NULL
	BEGIN

			SELECT @boilerName = RowNum
			FROM (	SELECT	HeatingMappingId, 
					CASE 
						WHEN @propertyId IS NOT NULL THEN
						'Heating Type ' + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY HeatingMappingId))
						ELSE 
						 'Boiler ' + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY HeatingMappingId))
					END AS RowNum 
					FROM	PA_HeatingMapping 
					WHERE	IsActive = 1 AND SCHEMEID = @schemeId	) BoilerInfo
			WHERE BoilerInfo.HeatingMappingId = @heatingId

	END
	ELSE IF @blockId IS NOT NULL
	BEGIN

			SELECT @boilerName = RowNum
			FROM (	SELECT	HeatingMappingId, 
					CASE 
						WHEN @propertyId IS NOT NULL THEN
						'Heating Type ' + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY HeatingMappingId))
						ELSE 
						 'Boiler ' + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY HeatingMappingId))
					END AS RowNum 
					FROM	PA_HeatingMapping 
					WHERE	IsActive = 1 AND BLOCKID = @blockId	) BoilerInfo
			WHERE BoilerInfo.HeatingMappingId = @heatingId

	END 
	ELSE IF @propertyId IS NOT NULL
	BEGIN

			SELECT @boilerName = RowNum
			FROM (	SELECT	HeatingMappingId, 
					CASE 
						WHEN @propertyId IS NOT NULL THEN
						'Heating Type ' + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY HeatingMappingId))
						ELSE 
						 'Boiler ' + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY HeatingMappingId))
					END AS RowNum 
					FROM	PA_HeatingMapping 
					WHERE	IsActive = 1 AND PROPERTYID = @propertyId	) BoilerInfo
			WHERE BoilerInfo.HeatingMappingId = @heatingId

	END 
         
    return(@boilerName)              
               
 end  

