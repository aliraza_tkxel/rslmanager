USE [RSLBHALive]
GO
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[SelectChild]')
                  AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' )) 
  DROP FUNCTION [dbo].SelectChild
GO
create function [dbo].[SelectChild](@parent as varchar(99), @parentLEVEL AS INT, @thestate AS NVARCHAR(10), @company AS NVARCHAR(10), @openedAccountlEVEL1 INT = null, @openedAccountlEVEL2 INT = null, @openedAccountlEVEL3 INT = null) returns varchar(max)
BEGIN

	DECLARE @newa AS NVARCHAR(max)

	SET @newa = ''
  IF @parentLEVEL < 2
  BEGIN
			SET @newa =	' 
						{
						"thecode": "New",
						"title": "Nominal Code",
						"theid": "0",
						"theparentid": "' + @parent + '",
						"children": [

						]
						}'
			
  END
  
  IF (		   SELECT COUNT(1)
                FROM nl_account AS o
					INNER JOIN dbo.NL_ACCOUNTTYPE a ON a.ACCOUNTTYPEID = o.ACCOUNTTYPE
				WHERE o.SUBLEVEL = @parentLEVEL + 1 AND o.PARENTREFLISTID = @parent AND o.ISACTIVE = @thestate) > 0  
   BEGIN
		SET @newa = @newa + ','
   end

DECLARE @Level1 INT
DECLARE @Level2 INT 

--IF NOT @openedAccountId = '' 
-- BEGIN
   
--  SELECT @Level2 = a.ACCOUNTID  , @Level1 = a2.ACCOUNTID 
--	 FROM  dbo.NL_ACCOUNT a
--		left JOIN nl_account a2 ON a.PARENTREFLISTID = a2.ACCOUNTID
--	WHERE a.ACCOUNTID = @openedAccountId OR a2.ACCOUNTID = @openedAccountId

-- END 
return 
(
  	
  select ',"children":[' + @newa +
						  
           ISNULL(STUFF((
                 select ',{ "kparentid":"'+ ISNULL(CAST(o.PARENTREFLISTID as nvarchar(10)),'') +'"'
						   +',"theid": '+ CAST(o.ACCOUNTID   as nvarchar(10))
						     +',"thecode": "'+ o.ACCOUNTNUMBER+'"'
						    +',"title": "'+ o.NAME + '"'
							+',"expanded": ' + (CASE WHEN o.accountid IN (@openedAccountlEVEL1,@openedAccountlEVEL2,@openedAccountlEVEL3) THEN 'true' ELSE 'false' END)
						   +',"codetype":"'+ a.DESCRIPTION +'"'
						   + ISNULL(dbo.SelectChild(o.ACCOUNTID, o.SUBLEVEL, @thestate, @company, @openedAccountlEVEL1,@openedAccountlEVEL2,@openedAccountlEVEL3), '')
						  +'}'
                FROM nl_account AS o
					INNER JOIN dbo.NL_ACCOUNTTYPE a ON a.ACCOUNTTYPEID = o.ACCOUNTTYPE
					left JOIN (
					SELECT COUNT(1) AS CompanyCount , ac.ACCOUNTID
					FROM nl_account_to_company ac
					WHERE ac.COMPANYID IN (@company)
					GROUP BY ac.ACCOUNTID
					) b ON b.accountid = o.ACCOUNTID   
				WHERE o.SUBLEVEL = @parentLEVEL + 1 AND o.PARENTREFLISTID = @parent AND  (( b.CompanyCount > 0 OR @company = ''))  AND o.ISACTIVE = @thestate
				ORDER BY o.ACCOUNTNUMBER
                 for xml path(''), type
                 ).value('text()[1]', 'varchar(max)'), 1, 1, ''),'')
				+ ']'
)
end
GO