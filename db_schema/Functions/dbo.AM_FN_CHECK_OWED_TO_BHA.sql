SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_CHECK_OWED_TO_BHA]
(
	@RentBalance int,
	@EstHB int	
)
RETURNS varchar(50)
AS
BEGIN
	
	DECLARE	@result varchar(50)
    DECLARE @total int
    
    SET @total=@RentBalance-@EstHB

	if(@total>0)
    BEGIN
		SET @result='true'
	END
	ELSE
	BEGIN
		SET @result='false'
	END
		
		
return @result
END







GO
