SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- SELECT dbo.E_CALCULATE_WORKDAYS_IN_ABSENCE(502,'1/4/2010','6/4/2010')



CREATE FUNCTION [dbo].[E_CALCULATE_WORKDAYS_IN_ABSENCE]
(
	@EMPLOYEEID INT,
	@STARTDATE DATETIME,
	@ENDDATE DATETIME
)
RETURNS INT  AS

BEGIN

	-- SET NOCOUNT ON;
	DECLARE @EMP INT
	DECLARE @MON FLOAT
	DECLARE @TUE FLOAT
	DECLARE @WED FLOAT
	DECLARE @THU FLOAT
	DECLARE @FRI FLOAT
	DECLARE @SAT FLOAT
	DECLARE @SUN FLOAT
	DECLARE @DAYS_WORKED FLOAT		-- NO. OF DAYS WORKED
	DECLARE @HRS_WORKED FLOAT		-- HRS WORKED IN A WEEK
	DECLARE @HRULE INT
	DECLARE @DATECOUNTER DATETIME
	DECLARE @DAYSCOUNT INT
	DECLARE @RCOUNT INT

	DECLARE @EMP_WORKINGDAYS TABLE(SID INT  IDENTITY(1,1) ,WORKDAYS nvarchar(10))


	SET @DAYS_WORKED = 0
	SET @DAYSCOUNT = 0


	SELECT @MON=MON,@TUE=TUE,@WED=WED,@THU=THU,@FRI=FRI,@SAT=SAT,@SUN=SUN,@HRS_WORKED = TOTAL 
	FROM E_WORKINGHOURS W
	WHERE W.EMPLOYEEID=@EMPLOYEEID
	
	SELECT @HRULE=ISNULL(HOLIDAYRULE,0) FROM E_JOBDETAILS WHERE EMPLOYEEID=@EMPLOYEEID

	IF @MON > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Monday')		
		END
	IF @TUE > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Tuesday')		
		
		END
	IF @WED > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Wednesday')
		END
	IF @THU > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Thursday')
		END
	IF @FRI > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Friday')
		END
	IF @SAT > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Saturday')
		END
	IF @SUN > 0
		BEGIN
			SET @DAYS_WORKED = @DAYS_WORKED + 1
			INSERT INTO @EMP_WORKINGDAYS (WORKDAYS) VALUES ('Sunday')
		END

	SET @DATECOUNTER= @STARTDATE
	WHILE @DATECOUNTER<=@ENDDATE
		BEGIN
			SELECT @RCOUNT=COUNT(*) FROM @EMP_WORKINGDAYS WHERE WORKDAYS=DATENAME(DW,@DATECOUNTER)
			
			-- EXCLUDE SATURDAY AND SUNDAY AS THIS IS HANDLED BY ASP PAGE VB/HOLIDAYFUNCTIONS.ASP--
			IF @RCOUNT=0 AND (((DATENAME(DW,@DATECOUNTER)<>'SATURDAY')) AND ((DATENAME(DW,@DATECOUNTER)<>'SUNDAY')))
				BEGIN
					-- EXCLUDE ANY BANK HOLIDAYS AS BANK HOLIDAYS ARE HANDLED BY ASP PAGE VB/HOLIDAYFUNCTIONS.ASP--
					IF @HRULE NOT IN (6,7,8,9)
						BEGIN
							IF @DATECOUNTER NOT IN (SELECT BHDATE FROM G_BANKHOLIDAYS WHERE BHA=1)
								BEGIN
									SET @DAYSCOUNT = @DAYSCOUNT + 1
								END
						END
					ELSE
						BEGIN
							IF @DATECOUNTER NOT IN (SELECT BHDATE FROM G_BANKHOLIDAYS WHERE MERIDIANEAST=1)
								BEGIN
									SET @DAYSCOUNT = @DAYSCOUNT + 1
								END
						END
				END
			SET @DATECOUNTER= DATEADD(D,1,@DATECOUNTER)
		
		END


	RETURN @DAYSCOUNT
	

END







GO
