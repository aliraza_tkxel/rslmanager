SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[FN_TC_RESPONSECOUNT](
	@PROGRAMMEID INT
)
RETURNS INT
AS
BEGIN
--	DECLARE @PROGRAMMEID INT
--	SET @PROGRAMMEID = 9

    DECLARE @PCOUNT INT
	SET @PCOUNT = 0
	SELECT @PCOUNT = RESPONSECOUNT FROM dbo.vwTCProgrammeResponse
	WHERE PROGRAMMEID = @PROGRAMMEID
	--SELECT @PCOUNT
	RETURN @PCOUNT
END

----------------------------------
GO
