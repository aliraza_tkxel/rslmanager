SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--SELECT DBO.GETMISSINGPREPAYCOUNT(8,GETDATE())  
CREATE   FUNCTION [dbo].[GETMISSINGPREPAYCOUNT](@PREPAYID AS INT,@GETDATE AS DATETIME)                
 RETURNS INT                
 AS                 
 BEGIN                
    DECLARE @STARTDATE AS DATETIME                
               
    DECLARE @MISCOUNT AS INT                
    DECLARE @RUNCOUNT AS INT                
    DECLARE @NO_MONTHS AS INT                
                    
    SELECT @STARTDATE=STARTDATE,@RUNCOUNT=RUNCOUNT,@NO_MONTHS=NO_MONTHS FROM NL_PREPAYMENT WHERE PREPAYID=@PREPAYID               
    
                  
    set  @MISCOUNT=(abs(DATEDIFF(MONTH,@GETDATE,@STARTDATE))-@RUNCOUNT)   
      
    if @MISCOUNT>= @NO_MONTHS     
    begin    
   	 set @MISCOUNT=@NO_MONTHS    
    end    
    
    if @MISCOUNT= 0     
    begin    
    	set @MISCOUNT=1    
    end    

               
    RETURN(@MISCOUNT)                
                 
 END                
      
    
  



GO
