SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE FUNCTION [dbo].[ADDBankHolidays]
  ( @StartDate datetime,
    @EndDate datetime )
RETURNS int
AS
BEGIN
DECLARE @HOLIDAYCOUNT INT
	
set @Holidaycount = (SELECT Count(*) AS Alldates FROM h_bankholidays WHERE actual_date between @StartDate and @EndDate)
RETURN ( @Holidaycount)

END




GO
