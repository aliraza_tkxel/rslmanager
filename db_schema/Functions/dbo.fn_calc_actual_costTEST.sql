SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE FUNCTION [dbo].[fn_calc_actual_costTEST](@jobid int,@actualdevcost float)
RETURNS money
AS
BEGIN
declare @hourly_rate float
declare @total_cost float
declare @totalminutes float
set @hourly_rate = 100.00
set @totalminutes = (select sum(DATEDIFF(mi, timestarted, timeended)) 
from H_TIMESPENT AS T where T.JOBID = @jobid)


begin
set @total_cost = @actualdevcost
end
set @total_cost = isnull(@total_cost,0)
   RETURN(@total_cost)
END




GO
