SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_SP_GET_NEXT_HB] 
(
	-- Add the parameters for the function here
	@tenantId	int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @nextHB	float

	-- Add the T-SQL statements to compute the return value here
	SELECT @nextHB = F_HBACTUALSCHEDULE.HB
	FROM F_HBACTUALSCHEDULE
		  INNER JOIN dbo.F_HBINFORMATION ON dbo.F_HBACTUALSCHEDULE.HBID = dbo.F_HBINFORMATION.HBID
	WHERE F_HBINFORMATION.ACTUALENDDATE IS NULL
		  AND  F_HBACTUALSCHEDULE.HBROW = (SELECT MIN(HBROW) FROM dbo.F_HBACTUALSCHEDULE WHERE HBID=F_HBINFORMATION.HBID AND VALIDATED IS NULL)
		  AND TENANCYID=@tenantId--replace this with the tenancy id you want to get the Ant. HB
	ORDER BY STARTDATE ASC

	-- Return the result of the function
	RETURN @nextHB

END



GO
