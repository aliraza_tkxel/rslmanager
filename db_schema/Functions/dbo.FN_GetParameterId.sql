SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [dbo].[FN_GetParameterId](@parameterName as varchar(50))              
 returns int              
 as               
 begin              
           
    declare @parameterId as int              
          
	SELECT	@parameterId = ParameterID 
    FROM	PA_PARAMETER 
	WHERE PA_PARAMETER.isactive = 1 
	AND ParameterName = @parameterName
	         
    return(@parameterId)              
               
 end  

GO
