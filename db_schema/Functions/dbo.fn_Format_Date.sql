SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fn_Format_Date](@formatdate datetime,@currentdate datetime)
RETURNS varchar(100)
AS
BEGIN

declare @strformatted varchar(100)
set @strformatted  = (select 
CASE 
WHEN dbo.fn_Trun_Date(@formatdate)= dbo.fn_Trun_Date(@currentdate) THEN 'Today'
WHEN dbo.fn_Trun_Date(@formatdate)=  dbo.fn_Trun_Date(DATEADD(day, -1,@currentdate)) THEN 'Yesterday'

when 
datediff(d,@formatdate,@currentdate) >= 2 and
datediff(d,@formatdate,@currentdate) <= 6
then  DATENAME(w, @formatdate)

when 
datediff(d,@formatdate,@currentdate) >= 7 and
datediff(d,@formatdate,@currentdate) <= 13
then  'Last Week'

when 
datediff(d,@formatdate,@currentdate) >= 14 and
datediff(d,@formatdate,@currentdate) <= 20
then  '2 weeks ago'

when 
datediff(d,@formatdate,@currentdate) >= 21 and
datediff(d,@formatdate,@currentdate) <= 27
then  '3 weeks ago'

when 
datediff(d,@formatdate,@currentdate) >= 28 and
datediff(d,@formatdate,@currentdate) <= 55
then  'Last Month'

ELSE  'Older' end as timestatus)
RETURN @strformatted   
END






GO
