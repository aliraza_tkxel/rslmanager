SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[CONVERTCOMMA_TO_MANY_TO_ONE]() 
RETURNS @TBL TABLE (SAVEREPORTID INT,PROJECTID INT) AS
BEGIN 

	DECLARE @SAVEREPORTID INT
	DECLARE @PROJECTID VARCHAR(50)
	



	DECLARE PROJECT_CURSOR CURSOR FOR select SAVEREPORTID,PROJECTID from NS_REPORT_SAVE

	OPEN PROJECT_CURSOR 

	FETCH NEXT FROM PROJECT_CURSOR INTO @SAVEREPORTID,@PROJECTID



    -- Check @@FETCH_STATUS to see if there are any more rows to fetch. 
    WHILE @@FETCH_STATUS = 0 
    BEGIN 
	INSERT INTO @TBL (SAVEREPORTID,PROJECTID) 
	SELECT @SAVEREPORTID,* FROM dbo.CHARLIST_TO_TABLE_NUMERIC(@PROJECTID,',') 	
        FETCH NEXT FROM PROJECT_CURSOR INTO @SAVEREPORTID,@PROJECTID
    END 
 CLOSE PROJECT_CURSOR  
 DEALLOCATE PROJECT_CURSOR 


 RETURN 
END
GO
