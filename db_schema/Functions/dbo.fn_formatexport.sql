SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  FUNCTION [dbo].[fn_formatexport](@data sql_variant)
RETURNS varchar(250)
AS
BEGIN
RETURN '"' + Replace(Replace(replace(replace(CONVERT(VARCHAR,@data),'"','""'),char(13),' - '),',',' '),char(10),' - ') + '"'   
END
GO
