SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE   FUNCTION [dbo].[fn_IsBlank](@isblank varchar(100))
RETURNS varchar(100)
AS
BEGIN


IF @isblank is null
BEGIN
set @isblank = 'N/A'
END


RETURN @isblank
END


GO
