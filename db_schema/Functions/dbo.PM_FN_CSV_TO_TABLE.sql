SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PM_FN_CSV_TO_TABLE]
(
    @STRING VARCHAR(8000)
)
RETURNS @OUTPUT TABLE(
    DATA INT
)
BEGIN
    DECLARE @START INT, @END INT
    SELECT @START = 1, @END = CHARINDEX(',', @STRING)

    WHILE @START < LEN(@STRING) + 1 BEGIN
        IF @END = 0 
            SET @END = LEN(@STRING) + 1

        INSERT INTO @OUTPUT (DATA) 
        VALUES(CONVERT(INT,SUBSTRING(@STRING, @START, @END - @START)))
        SET @START = @END + 1
        SET @END = CHARINDEX(',', @STRING, @START)
    END

    RETURN

END
GO
