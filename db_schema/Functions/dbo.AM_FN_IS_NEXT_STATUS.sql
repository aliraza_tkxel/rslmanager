SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_IS_NEXT_STATUS]
(
	@StatusId	int,
	@ActionId	int
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result bit
	DECLARE @title varchar(100)
	-- Add the T-SQL statements to compute the return value here
	

	SELECT	@title = Title
	FROM	AM_Action
	WHERE	StatusId = @StatusId and ranking = (select (ranking + 1) from am_action where actionId = @actionId)

	if(@title is null)
	BEGIN
		
		SET @result = 1	

	END	
	ELSE
	BEGIN
		
		SET @result = 0

	END

	-- Return the result of the function
	RETURN @result

END



GO
