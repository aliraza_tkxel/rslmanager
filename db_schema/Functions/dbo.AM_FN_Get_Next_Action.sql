SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_Get_Next_Action]
(
	@StatusId	int,
	@actionId	int
	
)
RETURNS varchar(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @title varchar(100)

	SELECT	@title = Title
	FROM	AM_Action
	WHERE	StatusId = @StatusId and ranking = (select (ranking + 1) from am_action where actionId = @actionId)
--		select @title
	
	if(@title is null)
	begin
		SELECT top 1	@title = Title
		FROM	AM_Action
		WHERE	StatusId = (select statusId from am_status where ranking = (select (ranking + 1) from am_status where statusId = @StatusId))
	end
	if(@title is null)
	begin
		SELECT 	@title = Title
		FROM	AM_Action
		WHERE   actionId = @actionId
	end
	
--	select @title
	-- Return the result of the function
	RETURN ISNULL(@title, '')

END



GO
