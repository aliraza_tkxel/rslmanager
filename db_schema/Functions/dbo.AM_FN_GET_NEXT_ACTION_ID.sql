SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_GET_NEXT_ACTION_ID] 
(
	@StatusId	int,
	@actionId	int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @id int

	SELECT	@id = ActionId
	FROM	AM_Action
	WHERE	StatusId = @StatusId and ranking = (select (ranking + 1) from am_action where actionId = @actionId)
--		select @title
	
	if(@id is null)
	begin
		SELECT top 1	@id = ActionId
		FROM	AM_Action
		WHERE	StatusId = (select statusId from am_status where ranking = (select (ranking + 1) from am_status where statusId = @StatusId))
	end
	if(@id is null)
	begin
		SELECT 	@id = ActionId
		FROM	AM_Action
		WHERE   actionId = @actionId
	end
	
--	select @title
	-- Return the result of the function
	RETURN ISNULL(@id, 0)

END



GO
