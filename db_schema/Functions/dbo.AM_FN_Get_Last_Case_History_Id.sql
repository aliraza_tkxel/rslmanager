SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Zunair MInhas
-- Create date: 3rd Jabuary 2011
-- Description:	This Scalar function is responsible for finding out the second last case history id.
				--The last case history entry is same as the case entry in AM_CASE.
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_Get_Last_Case_History_Id]
(
	-- Add the parameters for the function here
	@caseId	int
)
RETURNS int
AS
BEGIN
	DECLARE @caseHistoryId int

	SELECT  Top 1 @caseHistoryId = CaseHistoryId
	FROM AM_CaseHistory
	WHERE Caseid = @caseId and IsActionIgnored = 'false'
	order by CaseHistoryId desc

	RETURN @caseHistoryId
END



GO
