SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION dbo.regexObjExecute
(
@objRegExp integer,
@source varchar(5000)
)
RETURNS varchar(5000) AS
BEGIN
DECLARE @hr integer
DECLARE @results varchar(5000)

EXECUTE @hr = sp_OAMethod @objRegExp, 'Execute', @results OUTPUT, @source
IF @hr <> 0 BEGIN
RETURN NULL
END

RETURN @results
END






GO
