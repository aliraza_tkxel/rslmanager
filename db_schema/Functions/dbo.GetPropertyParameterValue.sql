USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

-- ==========================================================================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <26/05/2016>
-- Description:	Returns Property attribute value
-- ==========================================================================================

IF OBJECT_ID('dbo.GetPropertyParameterValue') IS NULL
 EXEC('CREATE FUNCTION dbo.GetPropertyParameterValue() RETURNS INT AS  BEGIN RETURN 1 END ;') 
GO

ALTER FUNCTION [dbo].[GetPropertyParameterValue]
(
@propertyId AS VARCHAR(20)
,@itemName AS varchar(50)
,@parameterName AS varchar(50)
)              
 RETURNS NVARCHAR(300)
 AS               
 BEGIN              
           
    DECLARE @value AS NVARCHAR(300) = NULL           
					
	SELECT   @value =    
			CASE 
				WHEN PP.ControlType = 'Dropdown' THEN PPV.ValueDetail
				WHEN PP.ControlType = 'TextBox' THEN  PPA.PARAMETERVALUE
				WHEN PP.ControlType = 'Date' THEN  Convert(nvarchar(10),PPID.DueDate,103)				
				WHEN PP.ControlType = 'Checkboxes' THEN CASE 
															WHEN PPA.IsCheckBoxSelected IS NULL THEN 'No'
															WHEN PPA.IsCheckBoxSelected = 0 THEN 'No' 
															WHEN PPA.IsCheckBoxSelected = 1 THEN 'Yes'
														END 
			END 
					
	FROM    dbo.PA_ITEM_PARAMETER PIP
			INNER JOIN	dbo.PA_PARAMETER PP ON PIP.ParameterId = PP.ParameterID
			INNER JOIN	dbo.PA_ITEM PIT ON PIP.ItemId = PIT.ItemID 			
			LEFT JOIN	dbo.PA_PROPERTY_ATTRIBUTES PPA ON PPA.ITEMPARAMID = PIP.ItemParamID AND PPA.PROPERTYID = @propertyId
			LEFT JOIN	dbo.PA_PARAMETER_VALUE PPV ON PPA.VALUEID = PPV.ValueID
			LEFT JOIN	dbo.PA_PROPERTY_ITEM_DATES PPID ON PIP.ItemId = PPID.ItemId AND PIP.ParameterId = PPID.ParameterId AND PPID.PROPERTYID = @propertyId

			
	WHERE	PP.ParameterName= @parameterName
			AND PIT.ItemName = @itemName
			
     
    RETURN(@value)              
               
 END  

GO



