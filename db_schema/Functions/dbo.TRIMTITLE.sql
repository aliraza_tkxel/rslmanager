SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  FUNCTION [dbo].[TRIMTITLE]
(@TITLE NVARCHAR(500),
@length INT)
RETURNS NVARCHAR(500)
AS
BEGIN

  if len(@title) > @length
	BEGIN
	  SET @title = LEFT(@title,@length) + '...'
	END 
  RETURN @title
end

GO
