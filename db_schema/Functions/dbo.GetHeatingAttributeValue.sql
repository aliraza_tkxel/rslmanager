USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

-- ==========================================================================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <14/05/2018>
-- Description:	Returns Heating/Boiler attribute value
-- ==========================================================================================

IF OBJECT_ID('dbo.GetHeatingAttributeValue') IS NULL
 EXEC('CREATE FUNCTION dbo.GetHeatingAttributeValue() RETURNS INT AS  BEGIN RETURN 1 END ;') 
GO

ALTER FUNCTION [dbo].[GetHeatingAttributeValue]
(
@heatingId AS int
,@itemName AS varchar(50)
,@parameterName AS varchar(50)
)              
 RETURNS NVARCHAR(300)
 AS               
 BEGIN              
           
   DECLARE @value AS NVARCHAR(300) = NULL 
   SELECT @value =    
			CASE 
				WHEN PP.ControlType = 'Dropdown' THEN PPV.ValueDetail
				WHEN PP.ControlType = 'TextBox' THEN  PPA.PARAMETERVALUE			
				WHEN PP.ControlType = 'Checkboxes' THEN CASE 
															WHEN PPA.IsCheckBoxSelected IS NULL THEN 'No'
															WHEN PPA.IsCheckBoxSelected = 0 THEN 'No' 
															WHEN PPA.IsCheckBoxSelected = 1 THEN 'Yes'
														END 
			END 

  FROM		dbo.PA_PROPERTY_ATTRIBUTES PPA
			INNER JOIN  dbo.PA_ITEM_PARAMETER PIP ON PPA.ITEMPARAMID = PIP.ITEMPARAMID
			INNER JOIN  dbo.PA_PARAMETER PP ON PIP.ParameterId = PP.ParameterID
			INNER JOIN	dbo.PA_ITEM PIT ON PIP.ItemId = PIT.ItemID
			LEFT JOIN	dbo.PA_PARAMETER_VALUE PPV ON PPA.VALUEID = PPV.ValueID

  WHERE	PPA.HeatingMappingId = @heatingId
		AND PP.ParameterName= @parameterName
		AND PIT.ItemName = @itemName
			     
    RETURN(@value)              
               
 END  

GO



