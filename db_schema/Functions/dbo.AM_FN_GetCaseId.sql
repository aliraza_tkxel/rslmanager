SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_GetCaseId] 
(
	-- Add the parameters for the function here
	@TennantId int
)
RETURNS int
AS
BEGIN
	DECLARE @CaseId int

 SELECT @CaseId = CaseId 
 FROM AM_CaseHistory 
 WHERE TennantId=@TennantId and IsActive = 'true'	

return @CaseId

END



GO
