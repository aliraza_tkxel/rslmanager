USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PA_GetParameterValueId') IS NULL 
	EXEC('CREATE FUNCTION dbo.[PA_GetParameterValueId] (@input FLOAT)
	 RETURNS FLOAT AS BEGIN 
	 DECLARE @RET FLOAT
	 SET @RET = @input
	 RETURN @RET 
	 END;') 
GO

ALTER FUNCTION [dbo].[PA_GetParameterValueId] 
(
	@valueDetail nvarchar(100),
	@parameterName nvarchar(100)
)
RETURNS int
AS
BEGIN

	 DECLARE @ValueId int
	 
	 Select @ValueId = PV.ValueID FROM PA_PARAMETER_VALUE PV
	 INNER JOIN PA_PARAMETER P ON P.ParameterID = PV.ParameterID 
	 WHERE PV.ValueDetail= @valueDetail and PV.IsActive=1 AND P.ParameterName = @parameterName
	 
	 RETURN @valueId

END

GO


