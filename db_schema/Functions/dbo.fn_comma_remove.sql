SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fn_comma_remove](@commor varchar(4000))
RETURNS varchar(4000)
AS
BEGIN
set @commor = replace(replace(replace(@commor,char(13),' '),char(10), ' '),',', ' ') 

RETURN @commor
END                          


GO
