SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create function [dbo].[weekEnds](@start datetime, @end datetime) returns @days table(x varchar(20)) as
 begin
   while @start <= @end begin
     insert into @days values(datename(dw, @start))
     set @start = DATEADD(d, 1, @start)
   end
   return
 end

GO
