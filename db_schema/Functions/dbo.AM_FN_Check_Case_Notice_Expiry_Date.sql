SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_Check_Case_Notice_Expiry_Date]
(
	@noticeExpiryDate	DATETIME
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result	VARCHAR(50)
	DECLARE @date	DateTime
	DECLARE @noticeExpiry int
	DECLARE @computedDate int
	
	-- Add the T-SQL statements to compute the return value here
	SELECT @date = DATEADD(wk, 4, GETDATE())
	
	SET @computedDate = FLOOR(Convert(real, @date))
	SET @noticeExpiry = FLOOR(Convert(real, @noticeExpiryDate))

	IF(@noticeExpiry <= @computedDate)
	BEGIN		
		SET @result = 'True'
	END
	else
	BEGIN
		SET @result = 'False'
	END
	-- Return the result of the function
	RETURN @result

END



GO
