SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   FUNCTION [dbo].[fn_calc_actual_cost2_OLDB4JIM](@jobid int,@actualdevcost money)
RETURNS money
AS
BEGIN
declare @hourly_rate FLOAT
declare @total_cost FLOAT
declare @totalminutes int
set @hourly_rate = 105.00
set @totalminutes = (select sum(DATEDIFF(mi, timestarted, timeended)) 
from H_TIMESPENT AS T where T.JOBID = @jobid)

if @actualdevcost is null or @actualdevcost = 0
	
	
	begin
		set @total_cost = (@hourly_rate/60) * @totalminutes 
	end
else

	begin
		set @total_cost = @actualdevcost
	end

   set @total_cost = isnull(@total_cost,0)
   RETURN(@total_cost)

END







GO
