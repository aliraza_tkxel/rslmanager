USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('dbo.GetServicingTypeByHeating') IS NULL
 EXEC('CREATE FUNCTION dbo.GetServicingTypeByHeating() RETURNS INT AS  BEGIN RETURN 1 END ;') 
GO

ALTER function [dbo].[GetServicingTypeByHeating]
(
@heatingId as int
)              
 returns int              
 as               
 begin              
           
    DECLARE @servicingTypeId as int 
	
	DECLARE @AlternativeServicingTypeId INT
	DECLARE @GasServicingTypeId INT
	DECLARE @OilServicingTypeId INT

	SELECT @AlternativeServicingTypeId = ServicingTypeID FROM P_SERVICINGTYPE WHERE Description = 'Alternative Servicing'
	SELECT @OilServicingTypeId = ServicingTypeID FROM P_SERVICINGTYPE WHERE Description = 'Oil'
	SELECT @GasServicingTypeId = ServicingTypeID FROM P_SERVICINGTYPE WHERE Description = 'Gas'             
          
	SELECT @servicingTypeId =  CASE WHEN IsAlterNativeHeating = 1 THEN  @AlternativeServicingTypeId 
									WHEN ValueDetail = 'Mains Gas' THEN @GasServicingTypeId
									WHEN ValueDetail = 'Oil' THEN  @OilServicingTypeId 
									ELSE NULL
								END
	FROM	PA_HEATINGMAPPING 
			INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID
	WHERE   HeatingMappingId = @heatingId

         
    return(@servicingTypeId)              
               
 end  

