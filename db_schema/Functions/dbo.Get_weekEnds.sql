SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  FUNCTION [dbo].[Get_weekEnds](@STARTDATE DATETIME)   
returns DATETIME as  
BEGIN  
	
	DECLARE @ENDDATE DATETIME
	
	SET @ENDDATE=@STARTDATE
		
	
	
	IF DATENAME(dw, @STARTDATE) NOT IN ('Sunday')
	BEGIN 
		WHILE DATENAME(dw, @ENDDATE) NOT IN ('Sunday')
		BEGIN 
			SET @ENDDATE = DATEADD(dd, 1, @ENDDATE)  
		END
	END
	ELSE
	BEGIN
		SET @ENDDATE=@STARTDATE	
	END 

	RETURN( @ENDDATE )
 END  



--SELECT dbo.Get_weekEnds('20120221') as endatte









GO
