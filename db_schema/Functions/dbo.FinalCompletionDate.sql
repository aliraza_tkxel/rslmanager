SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE FUNCTION [dbo].[FinalCompletionDate]
  ( @StartDate datetime,
    @days int)
RETURNS datetime
AS
BEGIN

DECLARE @HOLIDAYCOUNT INT
DECLARE @ENDDATE DATETIME

SET @HOLIDAYCOUNT = 1
SET @ENDDATE = dbo.ADDWorkingDays(@StartDate,@days)
WHILE @HOLIDAYCOUNT > 0

	BEGIN

		
		SET @HOLIDAYCOUNT =  dbo.ADDBankHolidays(@StartDate,@ENDDATE)
		SET @ENDDATE = dbo.ADDWorkingDays(@ENDDATE,@HOLIDAYCOUNT)
		SET @STARTDATE = @ENDDATE
	END



RETURN ( @ENDDATE)

END




GO
