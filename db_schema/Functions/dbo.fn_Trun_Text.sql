SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE  FUNCTION [dbo].[fn_Trun_Text](@truntext varchar,@trun_length int)
RETURNS char(100)
AS
BEGIN
declare @lastspace int
declare @response char(100)

set @lastspace = CHARINDEX(' ', reverse(@truntext))
set @response = @lastspace
set @truntext = left(@truntext,@trun_length)
set @lastspace = CHARINDEX(' ', reverse(@truntext))
set @truntext = left(@truntext,len(@truntext)-@lastspace)
RETURN @response
END

GO
