SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION diagnostics.GetTimePart ( @datevalue DATETIME )
RETURNS VARCHAR(30)
AS -- place the body of the function here
    BEGIN
     
        DECLARE @TimePart VARCHAR(30)
     
        SET @TimePart = ( CASE WHEN LEN(RTRIM(LTRIM(SUBSTRING(CONVERT(VARCHAR(20), @datevalue, 9),
                                                              13, 5)))) = 4
                               THEN '0'
                                    + LTRIM(RTRIM(SUBSTRING(CONVERT(VARCHAR(20), @datevalue, 9),
                                                            13, 5)))
                               ELSE LTRIM(RTRIM(SUBSTRING(CONVERT(VARCHAR(20), @datevalue, 9),
                                                          13, 5)))
                          END ) + ' '
            + SUBSTRING(CONVERT(VARCHAR(30), @datevalue, 9), 25, 2)

        RETURN @TimePart
	
    END
GO
