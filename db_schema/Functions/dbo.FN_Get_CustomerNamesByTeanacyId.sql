SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Get_CustomerNamesByTeanacyId]
(
@TENANCYID INT
)
RETURNS NVARCHAR(max)
AS
BEGIN

-- Declare the return variable here
DECLARE @CustomerNames NVARCHAR(MAX)
SET @CustomerNames = ''

SELECT @CustomerNames = COALESCE(@CustomerNames+', ' ,'') + + REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(LTRIM(RTRIM(C.FIRSTNAME)), '') + ' ' + ISNULL(LTRIM(RTRIM(C.LASTNAME)), ''), '  ', '')
FROM C_CUSTOMERTENANCY CT
INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID
LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE
WHERE CT.ENDDATE IS NOT NULL AND CT.TENANCYID = @TENANCYID

IF LEN(@CustomerNames) > 0 
SET @CustomerNames = SUBSTRING(@CustomerNames, 3, LEN(@CustomerNames)) 

RETURN @CustomerNames

END


--SELECT dbo.FN_Get_CustomerNamesByTeanacyId(12513)
--SELECT * FROM C_CUSTOMERTENANCY WHERE TENANCYID  = 12513
--SELECT '|'+FIRSTNAME+'|', '|'+LASTNAME+'|' FROM dbo.C__CUSTOMER WHERE CUSTOMERID IN (21,7684)
--SELECT dbo.AM_FN_GET_TENANT_ADDRESS(10219)
--SELECT * FROM C_CUSTOMERTENANCY WHERE ENDDATE IS NOT NULL ORDER BY TENANCYID
GO
