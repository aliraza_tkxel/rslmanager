SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  FUNCTION [dbo].[fn_formattimespent] (@totalminutes int)
RETURNS varchar(250)
AS
BEGIN


declare @minutes int
declare @days int
declare @hours int

declare @strformatted_date varchar(250)


--set @days = (@totalminutes/450)
--set @minutes = @totalminutes % 450
set @hours = (@totalminutes/60)

set @minutes = @totalminutes % 60

IF @hours = 0 
BEGIN
set @strformatted_date = '0:' + convert(varchar,@minutes) 
END
ELSE
 
BEGIN
IF  @minutes = 0
set @strformatted_date = convert(varchar,@hours) + ':00'
ELSE
set @strformatted_date = convert(varchar,@hours) + ':' 

	IF @minutes > 0
	BEGIN
	set @strformatted_date = @strformatted_date + convert(varchar,@minutes)
	END
END






   RETURN(@strformatted_date)
END





GO
