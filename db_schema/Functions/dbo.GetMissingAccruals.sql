SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE function [dbo].[GetMissingAccruals](@ACCRUALID as int,@getDate as datetime)              
 returns int              
 as               
 begin              
    declare @startdate as datetime              
    declare @Enddate as datetime              
    declare @misCount as int              
    declare @RUNCOUNT as int              
    declare @no_months as int              
   
    select @startdate=STARTDATE,@RUNCOUNT=RUNCOUNT,@no_months=no_months from NL_ACCRUALS where ACCRUALID=@ACCRUALID              
    set @Enddate=dateAdd(Month,@no_months,@startdate)              
             
    if datediff(month,@getDate,@startdate) < 0 ---Past            
    begin     
	      
     set @misCount=abs(datediff(month,@getDate,@startdate))- @RUNCOUNT+1
	if   @misCount >   @no_months
	begin
		 set @misCount=@no_months-@RUNCOUNT+1
        end     
    end            
    else if datediff(month,@getDate,@startdate) = 0 ---Preset            
    begin     
	       
     set @misCount=1            
    end            
        
    return(@misCount)              
               
 end  
GO
