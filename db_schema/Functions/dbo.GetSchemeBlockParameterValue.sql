USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('dbo.GetSchemeBlockParameterValue') IS NULL
 EXEC('CREATE FUNCTION dbo.GetSchemeBlockParameterValue() RETURNS INT AS  BEGIN RETURN 1 END ;') 
GO

ALTER FUNCTION [dbo].[GetSchemeBlockParameterValue]
(
@HeatingMappingId AS int
,@itemName AS varchar(50)
,@parameterName AS varchar(50)
)              
 RETURNS NVARCHAR(300)
 AS               
 BEGIN              
           
    DECLARE @value AS NVARCHAR(300) = NULL           
					
	SELECT   @value =    
			CASE 
				WHEN PP.ControlType = 'Dropdown' THEN PPV.ValueDetail
				WHEN PP.ControlType = 'TextBox' THEN  PPA.PARAMETERVALUE
				WHEN PP.ControlType = 'Date' THEN  Convert(nvarchar(10),PPID.DueDate,103)				
				WHEN PP.ControlType = 'Checkboxes' THEN CASE 
															WHEN PPA.IsCheckBoxSelected IS NULL THEN 'No'
															WHEN PPA.IsCheckBoxSelected = 0 THEN 'No' 
															WHEN PPA.IsCheckBoxSelected = 1 THEN 'Yes'
														END 
			END 
					
	FROM    dbo.PA_ITEM_PARAMETER PIP
			INNER JOIN	dbo.PA_PARAMETER PP ON PIP.ParameterId = PP.ParameterID
			INNER JOIN	dbo.PA_ITEM PIT ON PIP.ItemId = PIT.ItemID 			
			LEFT JOIN	dbo.PA_PROPERTY_ATTRIBUTES PPA ON PPA.ITEMPARAMID = PIP.ItemParamID AND PPA.HeatingMappingId = @HeatingMappingId
			LEFT JOIN	dbo.PA_PARAMETER_VALUE PPV ON PPA.VALUEID = PPV.ValueID
			LEFT JOIN	dbo.PA_PROPERTY_ITEM_DATES PPID ON PIP.ItemId = PPID.ItemId AND PIP.ParameterId = PPID.ParameterId AND PPID.HeatingMappingId = @HeatingMappingId

			
	WHERE	PP.ParameterName= @parameterName
			AND PIT.ItemName = @itemName
			
     
    RETURN(@value)              
               
 END  

GO



