SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [dbo].[FN_GetItemId](@itemName as varchar(50))              
 returns int              
 as               
 begin              
           
    declare @itemId as int              
          
	SELECT @itemId = ItemID
    FROM PA_ITEM  
	WHERE PA_ITEM.isactive = 1 
	AND ItemName = @itemName
         
    return(@itemId)              
               
 end  

GO
