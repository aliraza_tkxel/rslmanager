SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE function [dbo].[GetComponentId](@componentName as varchar(100))              
 returns int              
 as               
 begin              
           
    declare @componentId as smallint              
            
    SELECT @componentId = COMPONENTID 
    FROM   PLANNED_COMPONENT 
    WHERE  COMPONENTNAME =@componentName              
    
        
    return(@componentId)              
               
 end  

GO
