USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('dbo.GetAttributeChildItemName') IS NULL
 EXEC('CREATE FUNCTION dbo.GetAttributeChildItemName() RETURNS INT AS  BEGIN RETURN 1 END ;') 
GO

ALTER FUNCTION [dbo].[GetAttributeChildItemName]
(
@itemId AS INT
,@childAttributeMappingId AS INT
,@schemeId AS INT
,@blockId AS INT
)              
 RETURNS NVARCHAR(100)              
 AS               
 BEGIN              

    DECLARE @childItemName as nvarchar(100)                 
    SET @childItemName = NULL

	IF @schemeId IS NOT NULL AND @schemeId > 0
	BEGIN

			SELECT @childItemName = RowNum
			FROM (	SELECT	Id, AttributeName +' ' +CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY Id)) AS RowNum 
					FROM	PA_ChildAttributeMapping 
					WHERE	IsActive = 1 AND SCHEMEID = @schemeId AND ItemId = @itemId	) ChildAttribute
			WHERE ChildAttribute.Id = @childAttributeMappingId

	END
	ELSE IF @blockId IS NOT NULL AND @blockId > 0
	BEGIN

			SELECT @childItemName = RowNum
			FROM (	SELECT	Id, AttributeName + CONVERT( nvarchar(100),ROW_NUMBER() OVER (ORDER BY Id)) AS RowNum 
					FROM	PA_ChildAttributeMapping 
					WHERE	IsActive = 1 AND BlockId = @blockId AND ItemId = @itemId ) ChildAttribute
			WHERE ChildAttribute.Id = @childAttributeMappingId

	END 	
         
    return(@childItemName)              
               
 end  

