SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,19/06/2014>
-- Description:	<Description,Get file name from URL>
-- =============================================

CREATE function [dbo].[FN_GetFileNameFromUrl](@url as varchar(200))              
 returns varchar(300)              
 as               
 begin              
           
    DECLARE @fileName as varchar(300)              
    SET @fileName = ''
      
    IF CHARINDEX('/', REVERSE(@url)) = 0 
		BEGIN
			SET @fileName = @url
		END
    ELSE
		BEGIN
			SET @fileName= REVERSE(SUBSTRING(REVERSE(@url),1,CHARINDEX('/', REVERSE(@url), 1)-1))
		END
     
    IF CHARINDEX('?', @fileName) = 0 
		BEGIN
			SET @fileName = @fileName
		END
    ELSE
		BEGIN
			SET @fileName= SUBSTRING(@fileName,1,CHARINDEX('?', @fileName, 1)-1)
		END
     
         
	RETURN  @fileName
               
 end  

GO
