SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_GET_TENANT_ADDRESS] 
(
	@tenantId	int
)
RETURNS varchar(300)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @address varchar(300)

	
	SELECT  @address=   ISNULL(P__PROPERTY.HOUSENUMBER,'') + ' ' + ISNULL(P__PROPERTY.ADDRESS1,'') + ' ' + ISNULL(P__PROPERTY.ADDRESS2,'') + ', ' +  ISNULL(P__PROPERTY.TOWNCITY,'')
	FROM         P__PROPERTY INNER JOIN
						  C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
	where C_TENANCY.TENANCYID = @tenantId
	-- Return the result of the function
	RETURN @address
END



GO
