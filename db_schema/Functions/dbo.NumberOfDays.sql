SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[NumberOfDays]
    (
      @From DATETIME ,
      @To DATETIME ,
      @DatePart INT
    )
RETURNS INT
AS 
    BEGIN
        DECLARE @Days INT
        SET @Days = 0
        WHILE @From <= @To 
            BEGIN
                IF DATEPART(dw, @From) = @DatePart 
                    SET @Days = @Days + 1
                SET @From = DATEADD(d, 1, @From)
            END
        RETURN (@Days)
    END
GO
