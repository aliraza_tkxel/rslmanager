SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[AM_FN_GET_TENANT_SCHEME] 
(
	@tenantId	int
)
RETURNS varchar(300)
AS
BEGIN

	DECLARE @scheme varchar(300)

	
	SELECT  @scheme = P_SCHEME.SCHEMENAME
	FROM         P__PROPERTY INNER JOIN
				 C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID INNER JOIN
				P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID

	where C_TENANCY.TENANCYID = @tenantId
	-- Return the result of the function
	RETURN @scheme

END



GO
