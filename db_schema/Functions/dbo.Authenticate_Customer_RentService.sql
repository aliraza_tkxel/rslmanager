SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- this function takes two parameters and returns boolean
-- =============================================
CREATE FUNCTION [dbo].[Authenticate_Customer_RentService]
(
	
	@mobile NVARCHAR(12),
	@tenancyId int
)
RETURNS int
AS
BEGIN

DECLARE @count INT
DECLARE @b int

	SELECT @count=COUNT(*)
	FROM dbo.C_CUSTOMERTENANCY CT  
	INNER JOIN C_ADDRESS CA ON CA.CUSTOMERID=CT.CUSTOMERID
	WHERE CA.ISDEFAULT=1 
	AND CT.ENDDATE IS NULL 
	AND TENANCYID=@tenancyId AND MOBILE=@MOBILE


IF @count > 0
BEGIN
	-- record found
	SET @b=1
END
ELSE
	BEGIN 
		-- record not found
		SET @b=0
	END

RETURN @b 

END
GO
