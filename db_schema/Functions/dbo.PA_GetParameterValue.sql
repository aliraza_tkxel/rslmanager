USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PA_GetParameterValue') IS NULL 
	EXEC('CREATE FUNCTION dbo.[PA_GetParameterValue] (@input FLOAT)
	 RETURNS FLOAT AS BEGIN 
	 DECLARE @RET FLOAT
	 SET @RET = @input
	 RETURN @RET 
	 END;') 
GO

ALTER FUNCTION [dbo].[PA_GetParameterValue] 
(
	@parameterId int,
	@valueDetail nvarchar(50)
)
RETURNS int
AS
BEGIN
	 DECLARE @valueId int

	 SELECT @valueId = ValueID 
	 FROM	pa_parameter_value 		
	 WHERE  parameterid = @parameterId and ValueDetail = @valueDetail

	RETURN @valueId

END




GO


