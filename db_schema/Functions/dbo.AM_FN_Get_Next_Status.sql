SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AM_FN_Get_Next_Status]
(
	@StatusId	int,
	@actionId	int
	
)
RETURNS varchar(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @title varchar(100)
	DECLARE @statusTitle varchar(100)

	SELECT	@title = Title
	FROM	AM_Action
	WHERE	StatusId = @StatusId and ranking = (select (ranking + 1) from am_action where actionId = @actionId)
--		select @title
	
	if(@title is null)
	begin
		SELECT 	@statusTitle = Title
		FROM	AM_Status
		WHERE	ranking = (select (ranking + 1) from am_status where statusId = @StatusId)
	end
	else
	BEGIN
		SELECT 	@statusTitle = Title
		FROM	AM_Status
		WHERE	StatusId = @StatusId
	END

	if(@statusTitle is null)
	begin
		SELECT 	@statusTitle = Title
		FROM	AM_Status
		WHERE	StatusId = @StatusId	
	end

	
--	select @title
	-- Return the result of the function
	RETURN ISNULL(@statusTitle, '')

END



GO
