 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
-- select dbo.[I_DASHBOARD_PanelAccessCheck] (113,443)
--[AC_ALLOWACCESS] 113,443
CREATE FUNCTION [dbo].[I_DASHBOARD_PanelAccessCheck]
(
	@EmployeeId	int, @PageId int
	
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Module int
 

SELECT 
    @Module = MODULEID
FROM SITELIST V,
(
    SELECT SITEUNITS.ACCESSLEVEL,
           SITEUNITS.ACCESSITEM,
           E.EMPLOYEEID
    FROM
        (
            SELECT 1 AS ACCESSLEVEL,
                   MA.ModuleId AS ACCESSITEM,
                   MA.JobRoleTeamId
            FROM AC_MODULES_ACCESS AS MA
                INNER JOIN AC_MODULES AS M
                    ON M.MODULEID = MA.ModuleId
            UNION ALL
            SELECT 2 AS ACCESSLEVEL,
                   MenuId,
                   JobRoleTeamId
            FROM AC_MENUS_ACCESS
            UNION ALL
            SELECT 3 AS ACCESSLEVEL,
                   PageId,
                   JobRoleTeamId
            FROM AC_PAGES_ACCESS
        ) AS SITEUNITS
        INNER JOIN E__EMPLOYEE AS E
            ON SITEUNITS.JobRoleTeamId = E.JobRoleTeamId
) N
WHERE N.ACCESSLEVEL = V.ACCESSLEVEL AND N.ACCESSITEM = V.ACCESSITEM
      AND EMPLOYEEID = @EmployeeId
      AND v.ACCESSITEM = @PageId
      OR (
             v.ACCESSITEM = @PageId
             AND ALLACCESS = 1
         );
		  	
	 

	
--	select @title
	-- Return the result of the function
	RETURN @Module
	 
END



