USE [RSLBHALive]
go
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[NL_SelectChild]')
                  AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
  DROP FUNCTION [dbo].NL_SelectChild
GO
USE [RSLBHALive]
GO
/****** Object:  UserDefinedFunction [dbo].[nl_SelectChild]    Script Date: 13/12/2017 21:24:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[nl_SelectChild](@parent as varchar(99), @parentLEVEL AS INT, @thestate AS NVARCHAR(10), @company AS NVARCHAR(10), @openedAccountlEVEL1 INT = null, @openedAccountlEVEL2 INT = null, @openedAccountlEVEL3 INT = null) returns varchar(max)
BEGIN

DECLARE @newa AS NVARCHAR(max)

	SET @newa = ''
	
/**/ 	  IF @parentLEVEL < 2  AND  @thestate = 1
  BEGIN
  	Declare @NewCodeLabel nvarchar(10)
	set @NewCodeLabel = ''

	If @parentLEVEL = 0 
	set @NewCodeLabel = 'Child '
				
	SET @newa =	'{"thecode": "New","title": "Child Nominal Code","theid": "0","theparentid": "' + @parent + '","children": []}'	

  END
 
  IF (		   SELECT COUNT(1)
                FROM nl_account AS o
					INNER JOIN dbo.NL_ACCOUNTTYPE a ON a.ACCOUNTTYPEID = o.ACCOUNTTYPE
					left JOIN (
								SELECT COUNT(1) AS CompanyCount , ac.ACCOUNTID
								FROM nl_account_to_company ac
								WHERE ac.COMPANYID IN (@company)
								GROUP BY ac.ACCOUNTID
							) b ON b.accountid = o.ACCOUNTID   
					LEFT JOIN ( 
								SELECT COUNT(1) AS acnt, a.ACCOUNTID
								FROM dbo.NL_ACCOUNT a
									INNER JOIN dbo.NL_ACCOUNT_TO_COMPANY C ON C.ACCOUNTID = a.ACCOUNTID AND (C.COMPANYID IN (@company) OR @company = '')
									INNER JOIN dbo.NL_ACCOUNT a2 ON a2.PARENTREFLISTID = a.ACCOUNTID 
								WHERE  @theState = 0  AND a.ISACTIVE = 1 
									AND (ISNULL(a2.ISACTIVE,-1) = 0 )   
								GROUP BY a.ACCOUNTID
							) ap ON ap.ACCOUNTID = o.ACCOUNTID
				WHERE o.SUBLEVEL = @parentLEVEL + 1 AND o.PARENTREFLISTID = @parent AND  (( b.CompanyCount > 0 OR @company = ''))  
						AND (o.ISACTIVE = @theState OR (@theState = 0 AND ISNULL(ap.acnt,0) > 0)
						)) > 0  AND  @thestate = 1
   BEGIN
		SET @newa = ',' +  @newa 

   end
    
DECLARE @Level1 INT
DECLARE @Level2 INT 

return 
(
  
  select ',"children":[' +
						  
           ISNULL(STUFF((
                 select ',{ "kparentid":"'+ ISNULL(CAST(o.PARENTREFLISTID as nvarchar(10)),'') +'"'
						   +',"theid": '+ CAST(o.ACCOUNTID   as nvarchar(10))
						     +',"thecode": "'+ o.ACCOUNTNUMBER+'"'
							 +',"isactive": "' + CAST(o.ISACTIVE AS NVARCHAR(10)) + '"'
							 +',"thelevel": "'+ CAST(o.SUBLEVEL AS NVARCHAR(10))+'"'
						    +',"title": "'+ o.NAME + '"'
							+',"expanded": ' + (CASE WHEN o.accountid IN (@openedAccountlEVEL1,@openedAccountlEVEL2,@openedAccountlEVEL3) THEN 'true' ELSE 'false' END)
						   +',"codetype":"'+ a.DESCRIPTION +'"'
						   + ISNULL(dbo.nl_SelectChild(o.ACCOUNTID, o.SUBLEVEL, @thestate, @company, @openedAccountlEVEL1,@openedAccountlEVEL2,@openedAccountlEVEL3), '')
						  +'}'
                FROM nl_account AS o
					INNER JOIN dbo.NL_ACCOUNTTYPE a ON a.ACCOUNTTYPEID = o.ACCOUNTTYPE
					left JOIN (
								SELECT COUNT(1) AS CompanyCount , ac.ACCOUNTID
								FROM nl_account_to_company ac
								WHERE ac.COMPANYID IN (@company)
								GROUP BY ac.ACCOUNTID
							) b ON b.accountid = o.ACCOUNTID   
					LEFT JOIN ( 
								SELECT COUNT(1) AS acnt, a.ACCOUNTID
								FROM dbo.NL_ACCOUNT a
									INNER JOIN dbo.NL_ACCOUNT_TO_COMPANY C ON C.ACCOUNTID = a.ACCOUNTID AND (C.COMPANYID IN (@company) OR @company = '')
									INNER JOIN dbo.NL_ACCOUNT a2 ON a2.PARENTREFLISTID = a.ACCOUNTID 
								WHERE  @theState = 0  AND a.ISACTIVE = 1 
									AND (ISNULL(a2.ISACTIVE,-1) = 0 )   
								GROUP BY a.ACCOUNTID
							) ap ON ap.ACCOUNTID = o.ACCOUNTID
				WHERE o.SUBLEVEL = @parentLEVEL + 1 AND o.PARENTREFLISTID = @parent AND  (( b.CompanyCount > 0 OR @company = ''))  
						AND (o.ISACTIVE = @theState OR (@theState = 0 AND ISNULL(ap.acnt,0) > 0)
						)
				ORDER BY o.ACCOUNTNUMBER
                 for xml path(''), type
                 ).value('text()[1]', 'varchar(max)'), 1, 1, ''),'')
				+  @newa + ']'
)
END


GO
