SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[Deduct_weekEnds](@start datetime, @end datetime)   
returns int as  
begin  
	declare @alldays_diff int
	declare @days int  
	set @days = 0
	set @alldays_diff = datediff(d,@start,@end)
	while @start <= @end begin  
		
		if datename(dw, @start) in ('saturday','Sunday')
		set @days =  @days + 1       
		set @start = DATEADD(d, 1, @start)  
	end  
	set @days = @alldays_diff - @days 
	return( @days  )
 end  

GO
