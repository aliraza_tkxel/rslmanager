SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE    FUNCTION [dbo].[fn_Trun_Date](@trundate datetime)
RETURNS datetime
AS
BEGIN
   RETURN CAST(CONVERT(VARCHAR, @trundate, 103) AS datetime)
END








GO
