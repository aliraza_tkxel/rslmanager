SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION diagnostics.CurrentFiscalYear
()
RETURNS 
@FiscalDates TABLE 
(
	-- Add the column definitions for the TABLE variable here
	StartDate datetime, 
	EndDate datetime
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
		DECLARE @Answer INT
		DECLARE @Now DATETIME 

		SET @Now = GETDATE()

			-- You define what you want here (September being your changeover month)
		IF ( MONTH(@Now) < 4 ) 
			SET @Answer = YEAR(@Now) - 1
		ELSE 
			SET @Answer = YEAR(@Now)

		INSERT INTO @FiscalDates (StartDate, EndDate) VALUES ('01/04/' + CAST(@Answer AS VARCHAR), '31/03/' + CAST(@Answer + 1 AS VARCHAR) + ' 23:59:59')
		
	RETURN 
END
GO
