SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   FUNCTION [dbo].[fn_calc_actual_cost2](@jobid int,@HOURLY_RATE FLOAT)
RETURNS money
AS
BEGIN

declare @total_cost FLOAT
declare @totalminutes int
IF @HOURLY_RATE IS NULL
 BEGIN
		set @hourly_rate = 105.00
 END 

set @totalminutes = (select sum(DATEDIFF(mi, timestarted, timeended)) 
from H_TIMESPENT AS T where T.JOBID = @jobid)


	set @total_cost = (@hourly_rate/60) * @totalminutes 

	set @total_cost = isnull(@total_cost,0)
   
	RETURN(@total_cost)

END







GO
