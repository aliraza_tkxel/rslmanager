--- this script was missing from the db_schema version. This is was not added by James Millican but altered on 16/07/2017

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO





CREATE    TRIGGER [dbo].[NL_PURCHASEORDER_RECONCILED]
ON [dbo].[F_INVOICE]
FOR UPDATE 
AS

DECLARE @TRANSACTIONTYPE INT
SET @TRANSACTIONTYPE = 1 -- THIS STANDS FOR RP0 (RECONCILED PURCHASE ORDER)

-------------------------------------------------------------------------------------------
-- Only run the trigger If column 11 has been modified (confirmed) and is equal to the   --
-- value 1. Also check the invoice does not already exist in the journal tables		 --
-------------------------------------------------------------------------------------------
IF SUBSTRING(COLUMNS_UPDATED(),2,1) = power(2,(3-1)) 
	AND (SELECT CONFIRMED FROM INSERTED) = 1
	AND (SELECT ISCREDIT FROM INSERTED) = 0
	AND (SELECT COUNT(*) FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = @TRANSACTIONTYPE AND TRANSACTIONID = (SELECT INVOICEID FROM INSERTED)) = 0
BEGIN
	DECLARE @BADNOMINALS INT          
	DECLARE @TIMESTAMP DATETIME
	DECLARE @ERRORDESC VARCHAR(4000)

	SET @TIMESTAMP = GETDATE()
	
	-----------------------------------------------------------------------------------
	-- Check if the appropriate nominal account exists or not. 			 --
	-----------------------------------------------------------------------------------

	SELECT @BADNOMINALS = COUNT(*) FROM INSERTED INV
		INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.INVOICEID = INV.INVOICEID
		INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = FOTI.ORDERITEMID
		INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID
		LEFT JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE
	WHERE AC.ACCOUNTID IS NULL

	IF @BADNOMINALS > 0
	BEGIN
		SET @ERRORDESC = 
			'The Debit Code was null for one of the 
			purchase items therefore the operation was aborted. InvoiceID : ' +
			CAST((SELECT INVOICEID FROM INSERTED) AS VARCHAR)
		
		INSERT INTO NL_ERRORS(TIMESTAMP, ERRORDESC)
		VALUES (@TIMESTAMP, @ERRORDESC)
	END 
	ELSE
	BEGIN

		DECLARE @TXNID INT
		DECLARE @TOTALAMOUNT MONEY
		DECLARE @TXNDATE DATETIME
		DECLARE @TRANSACTIONID INT
		DECLARE @MEMO VARCHAR(4000)
		DECLARE @COMPANYID INT
		DECLARE @DESCRIPTION VARCHAR(1000)
		DECLARE @CONTROLACCOUNT INT

		SET @CONTROLACCOUNT = (SELECT ACCOUNTID FROM NL_ACCOUNT WHERE ACCOUNTNUMBER = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'PURCHASECONTROLACCOUNTNUMBER'))		
		SET CONCAT_NULL_YIELDS_NULL OFF
		
		---------------------------------------------------------------------------
		-- Get the variables needed for inserts later				 --
		---------------------------------------------------------------------------
		SELECT @TRANSACTIONID = INV.INVOICEID, 
		       @TOTALAMOUNT = SUM(ROUND(PI.GROSSCOST,2)), 
		       @TXNDATE = INV.TAXDATE, 
		       @DESCRIPTION = LEFT('<b>PO</b> ' + REPLICATE('0',7 -LEN(CAST(INV.ORDERID AS VARCHAR))) + CAST(INV.ORDERID AS VARCHAR) + '&#13;<br>Supplier :</b> ' + O.NAME , 1000), 
		       @MEMO = LEFT('Supplier : ' + O.NAME,4000),--+ISNULL(PADDRESS.PROPERTY_ADDRESS,'')+ISNULL(ISNULL(PADDRESS.SCHEMECODE,PSCHEME.SCHEMECODE),'')
			   @COMPANYID = CompanyId
		FROM INSERTED INV
			 INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.INVOICEID = INV.INVOICEID
			 INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = FOTI.ORDERITEMID
			 INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID
			 INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID
			-- LEFT JOIN (
			--			SELECT ORDERITEMID,
			--				   ' FOR '+NULLIF(LTRIM(ISNULL(PP.HOUSENUMBER,'') + ' ' + PP.ADDRESS1 + ', ' 
			--				    + PP.TOWNCITY + ', ' + PP.POSTCODE),'') AS PROPERTY_ADDRESS,
			--				    ' SCHEME CODE : '+NULLIF(PD.SCHEMECODE,'') AS SCHEMECODE
			--			FROM P_WOTOREPAIR PWO 
			--			INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=PWO.JOURNALID
			--			INNER JOIN dbo.P__PROPERTY PP ON PP.PROPERTYID=CJ.PROPERTYID
			--			INNER JOIN dbo.P_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID
			--			) PADDRESS ON PI.ORDERITEMID=PADDRESS.ORDERITEMID
			--LEFT JOIN (
			--		   SELECT PORDER.ORDERID,' SCHEME CODE : '+NULLIF(PDEV.SCHEMECODE,'') AS SCHEMECODE 
			--		   FROM dbo.F_PURCHASEORDER PORDER
			--			LEFT JOIN dbo.P_DEVELOPMENT PDEV ON PORDER.DEVELOPMENTID=PDEV.DEVELOPMENTID
			--		  ) PSCHEME ON PI.ORDERID=PSCHEME.ORDERID	
		GROUP BY INV.INVOICEID, O.NAME, INV.TAXDATE, INV.INVOICENUMBER, INV.ORDERID, PO.COMPANYID--,PADDRESS.PROPERTY_ADDRESS,PADDRESS.SCHEMECODE,PSCHEME.SCHEMECODE		

		---------------------------------------------------------------------------
		-- Create the main journal entry and get the identity back		 --
		---------------------------------------------------------------------------
		INSERT INTO NL_JOURNALENTRY (TRANSACTIONTYPE, TRANSACTIONID, TIMECREATED, TIMEMODIFIED, TXNDATE, COMPANYID)
		VALUES (@TRANSACTIONTYPE, @TRANSACTIONID, @TIMESTAMP, @TIMESTAMP, @TXNDATE, @COMPANYID)

		SET @TXNID = SCOPE_IDENTITY()

		---------------------------------------------------------------------------	
		-- This part inserts the invoice into the control account, depending on  --
		-- the @TOTALAMOUNT determines which side the value will go		 --
		---------------------------------------------------------------------------
		IF (@TOTALAMOUNT > 0)
		BEGIN 
			INSERT INTO NL_JOURNALENTRYCREDITLINE 
				(TXNID, ACCOUNTID, TIMECREATED, TIMEMODIFIED, EDITSEQUENCE, TXNDATE,
				AMOUNT, [DESCRIPTION], MEMO, COMPANYID)
			VALUES (@TXNID, @CONTROLACCOUNT, @TIMESTAMP, @TIMESTAMP, 1, @TXNDATE, 
				@TOTALAMOUNT, @DESCRIPTION, @MEMO, @COMPANYID)
		END
		ELSE
		BEGIN
			SET @TOTALAMOUNT = ABS(@TOTALAMOUNT)

			INSERT INTO NL_JOURNALENTRYDEBITLINE 
				(TXNID, ACCOUNTID, TIMECREATED, TIMEMODIFIED, EDITSEQUENCE, TXNDATE,
				AMOUNT, [DESCRIPTION], MEMO, COMPANYID)
			VALUES (@TXNID, @CONTROLACCOUNT, @TIMESTAMP, @TIMESTAMP, 1, @TXNDATE, 
				@TOTALAMOUNT, @DESCRIPTION, @MEMO, @COMPANYID)
		END

		---------------------------------------------------------------------------	
		-- Finally insert the data into the respective expense accounts		 --
		-- One statement is for the debits and the other for the credits	 --
		---------------------------------------------------------------------------	
		INSERT INTO NL_JOURNALENTRYDEBITLINE 
			(TXNID, ACCOUNTID, TIMECREATED, TIMEMODIFIED, EDITSEQUENCE, TXNDATE,
			AMOUNT, [DESCRIPTION], MEMO, TRACKERID, COMPANYID)
		SELECT @TXNID, AC.ACCOUNTID, @TIMESTAMP, @TIMESTAMP, 2, @TXNDATE,
			ABS(ROUND(PI.GROSSCOST,2)), 
			LEFT('<b>PO</b> ' + REPLICATE('0',7 -LEN(CAST(PI.ORDERID AS VARCHAR))) + CAST(PI.ORDERID AS VARCHAR) + ' : ' + PI.ITEMNAME,1000), 
			LEFT('INV : ' + INV.INVOICENUMBER + '&#13;Supplier : ' + O.NAME + '&#13;' + PI.ITEMDESC+ '&#13;' +ISNULL(PADDRESS.PROPERTY_ADDRESS,'')+ '&#13;' +ISNULL(ISNULL(PADDRESS.SCHEMECODE,PSCHEME.SCHEMECODE),''), 4000),
			PI.ORDERITEMID,  PO.COMPANYID
		       FROM INSERTED INV
			  INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.INVOICEID = INV.INVOICEID
			  INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = FOTI.ORDERITEMID
			  INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID
			  INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID
			  INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID
			  INNER JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE
			  LEFT JOIN (
						SELECT ORDERITEMID,
							   ' FOR '+NULLIF(LTRIM(ISNULL(PP.HOUSENUMBER,'') + ' ' + PP.ADDRESS1 + ', ' 
							    + PP.TOWNCITY + ', ' + PP.POSTCODE),'') AS PROPERTY_ADDRESS,
							    ' SCHEME CODE : '+NULLIF(PD.SCHEMECODE,'') AS SCHEMECODE
						FROM P_WOTOREPAIR PWO 
						INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=PWO.JOURNALID
						INNER JOIN dbo.P__PROPERTY PP ON PP.PROPERTYID=CJ.PROPERTYID
						INNER JOIN dbo.P_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID
						) PADDRESS ON PI.ORDERITEMID=PADDRESS.ORDERITEMID
			 LEFT JOIN (
					   SELECT PORDER.ORDERID,' SCHEME CODE : '+NULLIF(PDEV.SCHEMECODE,'') AS SCHEMECODE 
					   FROM dbo.F_PURCHASEORDER PORDER
						LEFT JOIN dbo.P_DEVELOPMENT PDEV ON PORDER.DEVELOPMENTID=PDEV.DEVELOPMENTID
					   ) PSCHEME ON PI.ORDERID=PSCHEME.ORDERID
			WHERE PI.GROSSCOST >= 0

		INSERT INTO NL_JOURNALENTRYCREDITLINE 
			(TXNID, ACCOUNTID, TIMECREATED, TIMEMODIFIED, EDITSEQUENCE, TXNDATE,
			AMOUNT, [DESCRIPTION], MEMO, TRACKERID, COMPANYID)
		SELECT @TXNID, AC.ACCOUNTID, @TIMESTAMP, @TIMESTAMP, 2, @TXNDATE,
			ABS(ROUND(PI.GROSSCOST,2)), 
			LEFT('<b>PO</b> ' + REPLICATE('0',7 -LEN(CAST(PI.ORDERID AS VARCHAR))) + CAST(PI.ORDERID AS VARCHAR) + ' : ' + PI.ITEMNAME,1000), 
			LEFT('INV : ' + INV.INVOICENUMBER + '&#13;Supplier : ' + O.NAME + '&#13;' + PI.ITEMDESC+ '&#13;' +ISNULL(PADDRESS.PROPERTY_ADDRESS,'')+ '&#13;' +ISNULL(ISNULL(PADDRESS.SCHEMECODE,PSCHEME.SCHEMECODE),''), 4000),
			PI.ORDERITEMID,  PO.COMPANYID
		       FROM INSERTED INV
			  INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.INVOICEID = INV.INVOICEID
			  INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = FOTI.ORDERITEMID
			  INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID
			  INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID
			  INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID
			  INNER JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE
			  LEFT JOIN (
						SELECT ORDERITEMID,
							   ' FOR '+NULLIF(LTRIM(ISNULL(PP.HOUSENUMBER,'') + ' ' + PP.ADDRESS1 + ', ' 
							    + PP.TOWNCITY + ', ' + PP.POSTCODE),'') AS PROPERTY_ADDRESS,
							    ' SCHEME CODE : '+NULLIF(PD.SCHEMECODE,'') AS SCHEMECODE
						FROM P_WOTOREPAIR PWO 
						INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=PWO.JOURNALID
						INNER JOIN dbo.P__PROPERTY PP ON PP.PROPERTYID=CJ.PROPERTYID
						INNER JOIN dbo.P_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID
						) PADDRESS ON PI.ORDERITEMID=PADDRESS.ORDERITEMID
			  LEFT JOIN (
					   SELECT PORDER.ORDERID,' SCHEME CODE : '+NULLIF(PDEV.SCHEMECODE,'') AS SCHEMECODE 
					   FROM dbo.F_PURCHASEORDER PORDER
						LEFT JOIN dbo.P_DEVELOPMENT PDEV ON PORDER.DEVELOPMENTID=PDEV.DEVELOPMENTID
					   ) PSCHEME ON PI.ORDERID=PSCHEME.ORDERID
			WHERE PI.GROSSCOST < 0

	END

END






GO