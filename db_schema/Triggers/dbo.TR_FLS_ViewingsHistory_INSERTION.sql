
IF OBJECT_ID('dbo.TR_FLS_ViewingsHistory_INSERTION') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[TR_FLS_ViewingsHistory_INSERTION]
		ON  [dbo].[FLS_Viewings]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: jan 5 2017
-- Description:	INSERT/DELETE/UPDATE data in FLS_ViewingsHistory table
-- =============================================
ALTER TRIGGER TR_FLS_ViewingsHistory_INSERTION
   ON FLS_Viewings
   AFTER INSERT,UPDATE
AS 
BEGIN

    INSERT INTO FLS_ViewingsHistory 
    
    (ViewingId,CustomerId,PropertyId,HousingOfficerId,viewingStatusId,ViewingDate,ViewingTime,
		EndTime,Duration,ActualEndTime,CreatedById,IsActive,RecordingSource,
		AppVersion,CreatedOnApp,CreatedOnServer,LastModifiedOnApp,
		LastModifiedOnServer,Notes
	)
										
     (SELECT     
		ViewingId,CustomerId,PropertyId,HousingOfficerId,viewingStatusId,ViewingDate,ViewingTime,
		EndTime,Duration,ActualEndTime,CreatedById,IsActive,RecordingSource,
		AppVersion,CreatedOnApp,CreatedOnServer,LastModifiedOnApp,
		LastModifiedOnServer,Notes FROM INSERTED
            );


  
 

END
GO
