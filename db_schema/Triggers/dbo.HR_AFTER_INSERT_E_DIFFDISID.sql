USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_E_DIFFDISID') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_E_DIFFDISID]
		ON  [dbo].[E_DIFFDISID]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_E_DIFFDISID
   ON E_DIFFDISID
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO E_DIFFDISID_HISTORY(
			[DISABILITYID]
           ,[EMPLOYEEID]
           ,[DIFFDIS]
           ,[NOTES]
           ,[LASTACTIONUSER]
           ,[LASTACTIONTIME]
           ,[NotifiedDate]
		   ,[ISLONGTERM]
		   ,[IMPACTONWORK]
		   ,[ISOCCUPATIONALHEALTH]	
		   ,[REVIEWDATE]
		   ,[DOCUMENTTITLE]	
		   ,[DOCUMENTPATH]	
		   ,[ISDELETE])

(SELECT [DISABILITYID]
           ,[EMPLOYEEID]
           ,[DIFFDIS]
           ,[NOTES]
           ,[LASTACTIONUSER]
           ,[LASTACTIONTIME]
           ,[NotifiedDate] 
		   ,[ISLONGTERM]
		   ,[IMPACTONWORK]
		   ,[ISOCCUPATIONALHEALTH]	
		   ,[REVIEWDATE]
		   ,[DOCUMENTTITLE]	
		   ,[DOCUMENTPATH]	
		   ,[ISDELETE] from INSERTED)           
END