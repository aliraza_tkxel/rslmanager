USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_E_QUALIFICATIONSANDSKILLS') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_E_QUALIFICATIONSANDSKILLS]
		ON  [dbo].[E_QUALIFICATIONSANDSKILLS]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_E_QUALIFICATIONSANDSKILLS
   ON E_QUALIFICATIONSANDSKILLS
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO [dbo].[E_QUALIFICATIONSANDSKILLS_HISTORY]
           ([QUALIFICATIONSID]
           ,[EMPLOYEEID]
           ,[SUBJECT]
           ,[RESULT]
           ,[QUALIFICATIONDATE]
           ,[SPECIALISEDTRAINING]
           ,[PROFESSIONALQUALIFICATION]
           ,[KEYCOMPETENCIES1]
           ,[KEYCOMPETENCIES2]
           ,[KEYCOMPETENCIES3]
           ,[PERSONALBIO]
           ,[CPD]
           ,[LANGUAGESKILLS]
           ,[EXISTPLANNED]
           ,[LASTACTIONUSER]
           ,[LASTACTIONTIME]
           ,[QualLevelID]
           ,[SpokenLanguage]
           ,[OtherSpokenLanguage]
           ,[IsActive]
           ,[IsSpoken]
           ,[IsWritten]
           ,[EquivalentLevel])
    (Select [QUALIFICATIONSID]
           ,[EMPLOYEEID]
           ,[SUBJECT]
           ,[RESULT]
           ,[QUALIFICATIONDATE]
           ,[SPECIALISEDTRAINING]
           ,[PROFESSIONALQUALIFICATION]
           ,[KEYCOMPETENCIES1]
           ,[KEYCOMPETENCIES2]
           ,[KEYCOMPETENCIES3]
           ,[PERSONALBIO]
           ,[CPD]
           ,[LANGUAGESKILLS]
           ,[EXISTPLANNED]
           ,[LASTACTIONUSER]
           ,[LASTACTIONTIME]
           ,[QualLevelID]
           ,[SpokenLanguage]
           ,[OtherSpokenLanguage]
           ,[IsActive]
           ,[IsSpoken]
           ,[IsWritten]
           ,[EquivalentLevel] From INSERTED)

END
