CREATE TRIGGER TRG_UPDATE_F_HBACTUALSCHEDULE
ON F_HBACTUALSCHEDULE
FOR UPDATE
AS
BEGIN
INSERT INTO
      F_HBACTUALSCHEDULE_HISTORY
        (
            [HBROW],
            [HBID],
            [STARTDATE],
            [ENDDATE],
            [HB],
            [SENT],
            [VALIDATED],
			[JOURNALID],
			updateDate,
			OPERATION
        )
SELECT
			d.[HBROW],
			d.[HBID],
			d.[STARTDATE],
			d.[ENDDATE],
			d.[HB],
			d.[SENT],
			d.[VALIDATED],
			d.[JOURNALID],
			GETDATE(),
			'UPDATE'
FROM
    DELETED AS d
END