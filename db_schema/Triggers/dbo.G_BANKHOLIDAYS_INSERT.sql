USE [RSLBHALive]
GO
/****** Object:  Trigger [dbo].[LGSR_INSERT]    Script Date: 03/07/2018 12:44:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.G_BANKHOLIDAYS_INSERT') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[G_BANKHOLIDAYS_INSERT]
		ON  [dbo].[G_BANKHOLIDAYS]
		 AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER [dbo].[G_BANKHOLIDAYS_INSERT] ON [dbo].[G_BANKHOLIDAYS]  
   AFTER INSERT,UPDATE
   
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Exec [dbo].[E_UpdateBankHolidayAdjustment] 
END
