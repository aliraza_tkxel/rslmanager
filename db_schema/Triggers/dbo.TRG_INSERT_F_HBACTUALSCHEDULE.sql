CREATE TRIGGER TRG_INSERT_F_HBACTUALSCHEDULE
ON F_HBACTUALSCHEDULE
FOR INSERT
AS
BEGIN
INSERT INTO
      F_HBACTUALSCHEDULE_HISTORY
        (
            [HBROW],
            [HBID],
            [STARTDATE],
            [ENDDATE],
            [HB],
            [SENT],
            [VALIDATED],
			[JOURNALID],
			updateDate,
			OPERATION
        )
SELECT
			i.[HBROW],
			i.[HBID],
			i.[STARTDATE],
			i.[ENDDATE],
			i.[HB],
			i.[SENT],
			i.[VALIDATED],
			i.[JOURNALID],
			GETDATE(),
			'INSERT'
FROM
    inserted AS i
END