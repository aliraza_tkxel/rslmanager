USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_CM_ServiceItems') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_CM_ServiceItems]
		ON  [dbo].[CM_ServiceItems]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_CM_ServiceItems
   ON CM_ServiceItems
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO CM_ServiceItems_History(
			[ServiceItemId]
           ,[ItemId]
           ,[SchemeId]
           ,[BlockId]
           ,[ContractorId]
           ,[ContractCommencement]
           ,[ContractPeriod]
           ,[ContractPeriodType]
           ,[Cycle]
           ,[CycleType]
           ,[CycleValue]
           ,[PORef]
		   ,[StatusId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Active])

(SELECT [ServiceItemId]
           ,[ItemId]
           ,[SchemeId]
           ,[BlockId]
           ,[ContractorId]
           ,[ContractCommencement]
           ,[ContractPeriod]
           ,[ContractPeriodType]
           ,[Cycle]
           ,[CycleType]
           ,[CycleValue]
           ,[PORef]
		   ,[StatusId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Active] from INSERTED)            
END