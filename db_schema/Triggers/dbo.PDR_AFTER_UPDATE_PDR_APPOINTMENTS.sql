USE [RSLBHALive]
GO
/****** Object:  Trigger [dbo].[PDR_AFTER_UPDATE_PDR_APPOINTMENTS]    Script Date: 9/19/2017 6:07:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_AFTER_UPDATE_PDR_APPOINTMENTS') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[PDR_AFTER_UPDATE_PDR_APPOINTMENTS]
		ON  [dbo].[PDR_APPOINTMENTS]
		AFTER UPDATE
		AS SET NOCOUNT ON;') 
GO
--====================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,6th January,2015>
-- Description:	<Description,,This trigger 'll UPDATE the record in PDR_APPOINTMENTS_HISTORY after insertion in PDR_APPOINTMENTS
--====================================
ALTER TRIGGER [dbo].[PDR_AFTER_UPDATE_PDR_APPOINTMENTS]  
 ON [dbo].[PDR_APPOINTMENTS]   
AFTER UPDATE  
AS  
BEGIN  	
	INSERT INTO PDR_APPOINTMENT_HISTORY
            ([APPOINTMENTID]
           ,[TENANCYID]
           ,[JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]
           ,[CUSTOMERNOTES]
           ,[APPOINTMENTSTATUS]
           ,[APPOINTMENTALERT]
           ,[APPOINTMENTCALENDAR]
           ,[TRADEID]
           ,[DURATION])
           SELECT 
            i.[APPOINTMENTID]
           ,i.[TENANCYID]
           ,i.[JOURNALID] 
           ,i.[JOURNALHISTORYID]
           ,i.[APPOINTMENTSTARTDATE] 
           ,i.[APPOINTMENTENDDATE]
           ,i.[APPOINTMENTSTARTTIME]
           ,i.[APPOINTMENTENDTIME]
           ,i.[ASSIGNEDTO]
           ,i.[CREATEDBY]
           ,i.[LOGGEDDATE]
           ,i.[APPOINTMENTNOTES]
           ,i.[CUSTOMERNOTES]
           ,i.[APPOINTMENTSTATUS]
           ,i.[APPOINTMENTALERT]
           ,i.[APPOINTMENTCALENDAR]
           ,i.[TRADEID]
           ,i.[DURATION]
           FROM INSERTED i                         
 END
