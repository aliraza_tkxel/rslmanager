USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_E_PaypointSubmission') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_E_PaypointSubmission]
		ON  [dbo].[E_PaypointSubmission]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_E_PaypointSubmission
   ON E_PaypointSubmission
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO E_PaypointSubmission_HISTORY([PaypointId], [Salary], [Grade], [GradePoint],	[DateProposed],	[DetailRationale], [Approved], [Supported],	[Authorized],
			 [ApprovedDate], [SupportedDate], [AuthorizedDate], [ApprovedBy], [SupportedBy], [AuthorizedBy], [employeeId], [paypointStatusId], [fiscalYearId],
			 [Reason])

(SELECT [PaypointId], [Salary], [Grade], [GradePoint], [DateProposed],	[DetailRationale], [Approved], [Supported],	[Authorized],
			 [ApprovedDate], [SupportedDate], [AuthorizedDate], [ApprovedBy], [SupportedBy], [AuthorizedBy], [employeeId], [paypointStatusId], [fiscalYearId],
			 [Reason] from INSERTED)           
END