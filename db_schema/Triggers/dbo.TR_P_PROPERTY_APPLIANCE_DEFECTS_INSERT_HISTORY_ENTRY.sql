USE [RSLBHALive]
GO
/****** Object:  Trigger [dbo].[TR_P_PROPERTY_APPLIANCE_DEFECTS_INSERT_HISTORY_ENTRY]    Script Date: 05/09/2016 17:16:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Trigger Description: Add a history entry when a meter is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */

IF OBJECT_ID('dbo.TR_P_PROPERTY_APPLIANCE_DEFECTS_INSERT_HISTORY_ENTRY') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[TR_P_PROPERTY_APPLIANCE_DEFECTS_INSERT_HISTORY_ENTRY]
		ON  [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER [dbo].[TR_P_PROPERTY_APPLIANCE_DEFECTS_INSERT_HISTORY_ENTRY]
   ON  [dbo].[P_PROPERTY_APPLIANCE_DEFECTS]
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
	INSERT INTO dbo.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY (
					PropertyDefectId
					,PropertyId
					,CategoryId
					,JournalId
					,IsDefectIdentified
					,DefectNotes
					,IsActionTaken
					,ActionNotes
					,IsWarningIssued
					,ApplianceId
					,SerialNumber
					,IsWarningFixed
					,DefectDate
					,CreatedBy
					,DetectorTypeId
					,PhotoNotes
					,GasCouncilNumber
					,IsDisconnected
					,IsPartsrequired
					,IsPartsOrdered
					,PartsOrderedBy
					,PartsDue
					,PartsDescription
					,PartsLocation
					,IsTwoPersonsJob
					,ReasonFor2ndPerson
					,Duration
					,Priority
					,TradeId
					,IsCustomerHaveHeating
					,IsHeatersLeft
					,NumberOfHeatersLeft
					,IsCustomerHaveHotWater
					,DefectJobSheetStatus
					,ApplianceDefectAppointmentJournalId
					,NoEntryNotes
					,CancelNotes
					,CreatedDate
					,ModifiedBy
					,ModifiedDate
					,CTimeStamp
					,BoilerTypeId
					,warningNoteSerialNo
					,RejectionReasonId
					,RejectionReasonNotes
					,SchemeId
					,BlockId
					,HeatingMappingId
				)
		SELECT
			PropertyDefectId
			,PropertyId
			,CategoryId
			,JournalId
			,IsDefectIdentified
			,DefectNotes
			,IsActionTaken
			,ActionNotes
			,IsWarningIssued
			,ApplianceId
			,SerialNumber
			,IsWarningFixed
			,DefectDate
			,CreatedBy
			,DetectorTypeId
			,PhotoNotes
			,GasCouncilNumber
			,IsDisconnected
			,IsPartsrequired
			,IsPartsOrdered
			,PartsOrderedBy
			,PartsDue
			,PartsDescription
			,PartsLocation
			,IsTwoPersonsJob
			,ReasonFor2ndPerson
			,Duration
			,Priority
			,TradeId
			,IsCustomerHaveHeating
			,IsHeatersLeft
			,NumberOfHeatersLeft
			,IsCustomerHaveHotWater
			,DefectJobSheetStatus
			,ApplianceDefectAppointmentJournalId
			,NoEntryNotes
			,CancelNotes
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,@CTimeStamp
			,BoilerTypeId
			,warningNoteSerialNo
			,RejectionReasonId
			,RejectionReasonNotes
			,SchemeId
			,BlockId
			,HeatingMappingId
		FROM
			INSERTED
END
