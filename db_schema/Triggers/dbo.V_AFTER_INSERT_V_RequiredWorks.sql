USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.V_AFTER_INSERT_V_RequiredWorks') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[V_AFTER_INSERT_V_RequiredWorks]
		ON  [dbo].[V_RequiredWorks]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER V_AFTER_INSERT_V_RequiredWorks
   ON V_RequiredWorks
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO [dbo].[V_RequiredWorksHistory]
           ([RequiredWorksId]
           ,[InspectionJournalId]
           ,[IsMajorWorksRequired]
           ,[RoomId]
           ,[OtherLocation]
           ,[WorkDescription]
           ,[VoidWorksNotes]
           ,[TenantNeglectEstimation]
           ,[ComponentId]
           ,[ReplacementDue]
           ,[Condition]
           ,[WorksJournalId]
           ,[IsTenantWorks]
           ,[IsBrsWorks]
           ,[StatusId]
           ,[IsScheduled]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[IsCanceled]
           ,[Duration]
           ,[IsVerified]
           ,[MajorWorkNotes]
           ,[workType]
           ,[faultAreaID]
           ,[faultID]
           ,[isRecurringProblem]
           ,[faultNotes]
           ,[problemDays]
           ,[JobsheetCompletionDate]
           ,[JobsheetCompletedAppVersion]
           ,[JobsheetCurrentAppVersion]
		   ,[isLegionella]
           ,[repairId])

(SELECT [RequiredWorksId]
           ,[InspectionJournalId]
           ,[IsMajorWorksRequired]
           ,[RoomId]
           ,[OtherLocation]
           ,convert(NVARCHAR(MAX),[WorkDescription])
           ,convert(NVARCHAR(MAX),[VoidWorksNotes])
           ,[TenantNeglectEstimation]
           ,[ComponentId]
           ,[ReplacementDue]
           ,[Condition]
           ,[WorksJournalId]
           ,[IsTenantWorks]
           ,[IsBrsWorks]
           ,[StatusId]
           ,[IsScheduled]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[IsCanceled]
           ,[Duration]
           ,[IsVerified]
           ,[MajorWorkNotes]
           ,[workType]
           ,[faultAreaID]
           ,[faultID]
           ,[isRecurringProblem]
           ,convert(NVARCHAR(MAX), [faultNotes])
           ,[problemDays]
           ,[JobsheetCompletionDate]
           ,[JobsheetCompletedAppVersion]
           ,[JobsheetCurrentAppVersion]
		   ,[isLegionella]
           ,[repairId] from INSERTED)            
END