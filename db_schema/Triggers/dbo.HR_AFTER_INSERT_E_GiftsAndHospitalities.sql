USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_E_GiftsAndHospitalities') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_E_GiftsAndHospitalities]
		ON  [dbo].[E_GiftsAndHospitalities]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_E_GiftsAndHospitalities
   ON E_GiftsAndHospitalities
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO [dbo].[E_GiftsAndHospitalities_History]
           (
			[Details],
			[ApproximateValue],
			[Accepted],
			[NotApprovedDate],
			[DateOfDeclaration],
			[Notes],
			[CreatedDate],
			[GiftId],
			[EmployeeId],
			[fiscalYearId],
			[documentId],
			[GiftStatusId],
			[CreatedBy],
			[UpdatedDate],
			[UpdatedBy],
			[DateGivenReceived],
			[IsGivenReceived],
			[OfferToBy])
    (Select [Details],
			[ApproximateValue],
			[Accepted],
			[NotApprovedDate],
			[DateOfDeclaration],
			[Notes],
			[CreatedDate],
			[GiftId],
			[EmployeeId],
			[fiscalYearId],
			[documentId],
			[GiftStatusId],
			[CreatedBy],
			[UpdatedDate],
			[UpdatedBy],
			[DateGivenReceived],
			[IsGivenReceived],
			[OfferToBy] From INSERTED)

END
