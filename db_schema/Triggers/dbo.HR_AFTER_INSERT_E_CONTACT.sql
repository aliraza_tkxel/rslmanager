USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_E_CONTACT') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_E_CONTACT]
		ON  [dbo].[E_CONTACT]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_E_CONTACT
   ON E_CONTACT
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO E_CONTACT_HISTORY(
			[CONTACTID]	
		   ,[EMPLOYEEID]
           ,[ADDRESS1]
           ,[ADDRESS2]
           ,[ADDRESS3]
           ,[POSTALTOWN]
           ,[COUNTY]
           ,[POSTCODE]
           ,[HOMETEL]
           ,[MOBILE]
           ,[WORKDD]
           ,[WORKEXT]
           ,[HOMEEMAIL]
           ,[WORKEMAIL]
           ,[EMERGENCYCONTACTNAME]
           ,[EMERGENCYCONTACTTEL]
           ,[EMERGENCYINFO]
           ,[WORKMOBILE]
           ,[LASTACTIONTIME]
           ,[LASTACTIONUSER]
           ,[NextOfKin]
		   ,[MobilePersonal]
		   ,[EmergencyContactRelationship])

(SELECT [CONTACTID]	, [EMPLOYEEID]
           ,[ADDRESS1]
           ,[ADDRESS2]
           ,[ADDRESS3]
           ,[POSTALTOWN]
           ,[COUNTY]
           ,[POSTCODE]
           ,[HOMETEL]
           ,[MOBILE]
           ,[WORKDD]
           ,[WORKEXT]
           ,[HOMEEMAIL]
           ,[WORKEMAIL]
           ,[EMERGENCYCONTACTNAME]
           ,[EMERGENCYCONTACTTEL]
           ,[EMERGENCYINFO]
           ,[WORKMOBILE]
           ,[LASTACTIONTIME]
           ,[LASTACTIONUSER]
           ,[NextOfKin]
		   ,[MobilePersonal]
		   ,[EmergencyContactRelationship] from INSERTED)           
END