USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.HR_AFTER_INSERT_E_EmployeeTrainings') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[HR_AFTER_INSERT_E_EmployeeTrainings]
		ON  [dbo].[E_EmployeeTrainings]
		AFTER INSERT,UPDATE
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER HR_AFTER_INSERT_E_EmployeeTrainings
   ON E_EmployeeTrainings
   AFTER INSERT,UPDATE
AS 

BEGIN

INSERT INTO [dbo].[E_EmployeeTrainings_History]
           (
		   [TrainingId]
           ,[EmployeeId]
           ,[Justification]
           ,[Course]
           ,[StartDate]
           ,[EndDate]
           ,[TotalNumberOfDays]
           ,[ProviderName]
           ,[ProviderWebsite]
           ,[Location]
           ,[Venue]
           ,[TotalCost]
           ,[IsSubmittedBy]
           ,[IsMandatoryTraining]
           ,[Expiry]
           ,[AdditionalNotes]
           ,[Status]
           ,[Createdby]
           ,[CreatedDate]
           ,[Updatedby]
           ,[UpdateDate]
           ,[Active]
		   ,[GroupId]
		   ,[ProfessionalQualification])
    (Select [TrainingId]
           ,[EmployeeId]
           ,[Justification]
           ,[Course]
           ,[StartDate]
           ,[EndDate]
           ,[TotalNumberOfDays]
           ,[ProviderName]
           ,[ProviderWebsite]
           ,[Location]
           ,[Venue]
           ,[TotalCost]
           ,[IsSubmittedBy]
           ,[IsMandatoryTraining]
           ,[Expiry]
           ,[AdditionalNotes]
           ,[Status]
           ,[Createdby]
           ,[CreatedDate]
           ,[Updatedby]
           ,[UpdateDate]
           ,[Active]
		   ,[GroupId]
		   ,[ProfessionalQualification] From INSERTED)

END