USE [RSLBHALive]
GO
/****** Object:  Trigger [dbo].[TR_P_DETECTOR_INSERT_HISTORY_ENTRY]    Script Date: 5/21/2018 2:29:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.TR_P_DETECTOR_INSERT_HISTORY_ENTRY') IS NULL 
 EXEC('CREATE TRIGGER [dbo].[TR_P_DETECTOR_INSERT_HISTORY_ENTRY]
		ON  [dbo].[P_DETECTOR]
		AFTER INSERT
		AS SET NOCOUNT ON;') 
GO

ALTER TRIGGER [dbo].[TR_P_DETECTOR_INSERT_HISTORY_ENTRY]
   ON  [dbo].[P_DETECTOR]
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
INSERT INTO P_DETECTOR_HISTORY (
				DetectorId
				,PropertyId
				,DetectorTypeId
				,Location
				,Manufacturer
				,SerialNumber
				,PowerSource
				,InstalledDate
				,InstalledBy
				,IsLandlordsDetector
				,TestedDate
				,TestedBy
				,BatteryReplaced
				,Passed
				,Notes
				,CTimeStamp
				,SchemeId
				,BlockId
			)
	SELECT
		DetectorId
		,PropertyId
		,DetectorTypeId
		,Location
		,Manufacturer
		,SerialNumber
		,PowerSource
		,InstalledDate
		,InstalledBy
		,IsLandlordsDetector
		,TestedDate
		,TestedBy
		,BatteryReplaced
		,Passed
		,Notes
		,@CTimeStamp
		,SchemeId
		,BlockId
	FROM
		INSERTED

END
