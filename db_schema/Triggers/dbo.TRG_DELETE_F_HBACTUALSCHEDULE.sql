CREATE TRIGGER TRG_DELETE_F_HBACTUALSCHEDULE
ON F_HBACTUALSCHEDULE
FOR DELETE
AS
BEGIN
INSERT INTO
      F_HBACTUALSCHEDULE_HISTORY
        (
            [HBROW],
            [HBID],
            [STARTDATE],
            [ENDDATE],
            [HB],
            [SENT],
            [VALIDATED],
			[JOURNALID],
			updateDate,
			OPERATION
        )
SELECT
			d.[HBROW],
			d.[HBID],
			d.[STARTDATE],
			d.[ENDDATE],
			d.[HB],
			d.[SENT],
			d.[VALIDATED],
			d.[JOURNALID],
			GETDATE(),
			'DELETE'
FROM
    DELETED AS d
END