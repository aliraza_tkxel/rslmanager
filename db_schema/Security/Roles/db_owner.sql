
EXEC sp_addrolemember N'db_owner', N'BHG\ICT Admins'

EXEC sp_addrolemember N'db_owner', N'Tkxel'
EXEC sp_addrolemember N'db_owner', N'ASPNET'
GO
EXEC sp_addrolemember N'db_owner', N'BHG\Sec E Business'
GO
EXEC sp_addrolemember N'db_owner', N'BHG\SQLService'
GO
EXEC sp_addrolemember N'db_owner', N'BHG-CRMLIVE\ASPNET'
GO
EXEC sp_addrolemember N'db_owner', N'Broadland Dev'
GO
EXEC sp_addrolemember N'db_owner', N'build'
GO
EXEC sp_addrolemember N'db_owner', N'Developer'
GO
EXEC sp_addrolemember N'db_owner', N'IUSR_REIDMARK1-GW'
GO
EXEC sp_addrolemember N'db_owner', N'IWAM_REIDMARK1-GW'
GO
EXEC sp_addrolemember N'db_owner', N'NT AUTHORITY\IUSR'
GO
EXEC sp_addrolemember N'db_owner', N'robert.egan'
GO
EXEC sp_addrolemember N'db_owner', N'SQLServer'
GO
EXEC sp_addrolemember N'db_owner', N'SQLServerUser'
GO
EXEC sp_addrolemember N'db_owner', N'SYSTEM'
GO
