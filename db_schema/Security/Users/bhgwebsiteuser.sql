IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'bhgwebsiteuser')
CREATE LOGIN [bhgwebsiteuser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [bhgwebsiteuser] FOR LOGIN [bhgwebsiteuser] WITH DEFAULT_SCHEMA=[web]
GO
