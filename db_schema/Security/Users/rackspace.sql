IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'rackspace')
CREATE LOGIN [rackspace] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [rackspace] FOR LOGIN [rackspace]
GO
