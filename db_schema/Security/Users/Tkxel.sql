IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Tkxel')
CREATE LOGIN [Tkxel] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Tkxel] FOR LOGIN [Tkxel]
GO
