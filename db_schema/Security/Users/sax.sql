IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'sax')
EXEC sp_addlogin N'sax', 'p@ssw0rd'
GO
EXEC sp_grantdbaccess N'sax'
GO
