IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'smluser')
EXEC sp_addlogin N'smluser', 'p@ssw0rd'
GO
EXEC sp_grantdbaccess N'smluser'
GO
