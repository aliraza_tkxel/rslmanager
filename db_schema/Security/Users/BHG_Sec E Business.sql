IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'BHG\Sec E Business')
EXEC sp_grantlogin N'BHG\Sec E Business'
GO
EXEC sp_grantdbaccess N'BHG\Sec E Business', N'BHG\Sec E Business'
GO
