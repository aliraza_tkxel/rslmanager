IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'BHG\sqlservice')
EXEC sp_grantlogin N'BHG\sqlservice'
GO
EXEC sp_grantdbaccess N'BHG\sqlservice', N'BHG\SQLService'
GO
