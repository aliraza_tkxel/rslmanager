IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'NT AUTHORITY\IUSR')
EXEC sp_grantlogin N'NT AUTHORITY\IUSR'
GO
EXEC sp_grantdbaccess N'NT AUTHORITY\IUSR', N'NT AUTHORITY\IUSR'
GO
