IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'build')
EXEC sp_addlogin N'build', 'p@ssw0rd'
GO
EXEC sp_grantdbaccess N'build', N'build'
GO
