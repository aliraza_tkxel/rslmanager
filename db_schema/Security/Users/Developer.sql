IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Developer')
EXEC sp_addlogin N'Developer', 'p@ssw0rd'
GO
EXEC sp_grantdbaccess N'Developer'
GO
