IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Broadland Dev')
EXEC sp_addlogin N'Broadland Dev', 'p@ssw0rd'
GO
EXEC sp_grantdbaccess N'Broadland Dev'
GO
