SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[AC_MODULELIST] ( @USERID INT )


AS 

    SET NOCOUNT ON

    WITH    CTE_Modules ( ModuleId, DESCRIPTION, THEPATH, IMAGE, IMAGEOPEN, IMAGEMOUSEOVER, IMAGETAG, IMAGETITLE, ORDERTEXT )
              AS ( SELECT 
	DISTINCT                AC_MODULES.MODULEID ,
                            DESCRIPTION ,
                            '/' + AC_MODULES.DIRECTORY + '/' + AC_MODULES.PAGE AS THEPATH ,
                            IMAGE ,
                            IMAGEOPEN ,
                            IMAGEMOUSEOVER ,
                            IMAGETAG ,
                            IMAGETITLE ,
                            ORDERTEXT
                   FROM     E__EMPLOYEE
                            INNER JOIN AC_MODULES_ACCESS ON E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId
                            INNER JOIN AC_MODULES ON AC_MODULES_ACCESS.ModuleId = AC_MODULES.MODULEID
                   WHERE    ( ACTIVE = 1
                              AND DISPLAY = 1
                              AND EMPLOYEEID = @USERID
                            )
                   UNION
                   SELECT 
	DISTINCT                AC_MODULES.MODULEID ,
                            DESCRIPTION ,
                            '/' + AC_MODULES.DIRECTORY + '/' + AC_MODULES.PAGE AS THEPATH ,
                            IMAGE ,
                            IMAGEOPEN ,
                            IMAGEMOUSEOVER ,
                            IMAGETAG ,
                            IMAGETITLE ,
                            ORDERTEXT
                   FROM     E__EMPLOYEE
                            INNER JOIN AC_MODULES_ACCESS ON E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId
                            INNER JOIN AC_MODULES ON AC_MODULES_ACCESS.ModuleId = AC_MODULES.MODULEID
                   WHERE    ( ALLACCESS = 1
                              AND DISPLAY = 1
                            )
                 )
        SELECT  ModuleId ,
                DESCRIPTION ,
                THEPATH ,
                IMAGE ,
                IMAGEOPEN ,
                IMAGEMOUSEOVER ,
                IMAGETAG ,
                IMAGETITLE ,
                ORDERTEXT
        FROM    CTE_Modules
        ORDER BY ORDERTEXT




GO
