SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[FL_CO_GETSEARCHRECORDS] 

/* ===========================================================================
 '   NAME:           FL_CO_MANAGEAPPOINTMENT_COUNTSEARCHRESULTS
 '   DATE CREATED:   26 Dec 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To count rows based on Search
 '   IN:            @locationId, @areaId, @elementId,@teamId,@userId,@patchId,@schemeId, @priorityId, @statusId,@stageId,@due,@postCode,@ORGID,
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	        @locationId	int	=  NULL,
		@areaId	        int	=  NULL,
		
		@elementId	int = NULL,
		@teamId	int = NULL,
		@userId	int = NULL,
		@patchId	int = NULL,
		@schemeId	int = NULL,
		@due                varchar(20)=NULL,
		@postCode 	varchar(1000)=NULL,
		@priorityId	int = NULL,
		@statusId 	int = NULL,  
		@stageId	int = NULL,  
		@ORGID           int = NULL,
		
	
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @statusId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@statusId)) + ' AND'  
   IF @patchId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY .PATCH = '+ LTRIM(STR(@patchId)) + ' AND'  
    IF @schemeId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY .DEVELOPMENTID  = '+ LTRIM(STR(@schemeId)) + ' AND'    
    IF @postCode IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY.POSTCODE = '+ LTRIM(STR(@postCode)) + ' AND'  
    IF @due IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                               'Convert(varchar(10),FL_FAULT_LOG.DueDate,103) ='''+ Convert(varchar,@due,120) +''' AND' 
    IF @ORGID IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_LOG.ORGID = '+ LTRIM(STR(@ORGID)) + ' AND' 
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
	          CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID AS FaultLogID, FL_FAULT_LOG.JobSheetNumber AS JSNumber,' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.SubmitDate AS Logged,C__CUSTOMER.FIRSTNAME AS Name,C__CUSTOMER.NINUMBER AS CustomerNo,C__CUSTOMER.TENANCY AS TenancyRef,' +
	          CHAR(10) + CHAR(9) + 'C_ADDRESS.ADDRESS1 AS CustomerAddress,C_ADDRESS.TEL AS CustomerTel,' +
                        CHAR(10) + CHAR(9) + 'P__PROPERTY.ADDRESS1 AS Address,' +
                        CHAR(10) + CHAR(9) + ' FL_FAULT.Description AS Description,' +
	           CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT.AppointmentDate AS Appointment,' +
	           CHAR(10) + CHAR(9) + 'E__EMPLOYEE.FIRSTNAME AS Operative,' +
	           CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE.StageName AS Stage,' +
	           CHAR(10) + CHAR(9) + 'Case  FL_FAULT_PRIORITY.Days'+
	           CHAR(10) +CHAR(9) +' When 0 then ''Hour(s)'' when 1 then ''Day(s)''  end as Type ' 
                       
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
  SET @FromClause =CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT INNER JOIN FL_FAULT_LOG ' +
                      CHAR(10) + CHAR(9) + 'ON FL_CO_APPOINTMENT.JobSheetNumber = FL_FAULT_LOG.JobSheetNumber ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_ELEMENT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.ElementID = FL_ELEMENT.ElementID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_AREA ' +
                      CHAR(10) + CHAR(9) + 'ON FL_ELEMENT.AreaID = FL_AREA.AreaID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_LOCATION ' +
                      CHAR(10) + CHAR(9) + 'ON FL_AREA.LocationID = FL_LOCATION.LocationID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN E__EMPLOYEE ' +
                      CHAR(10) + CHAR(9) + 'ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID ' +
	         CHAR(10) + CHAR(9) + 'INNER JOIN E_JOBDETAILS ' +
                      CHAR(10) + CHAR(9) + 'ON  E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID ' +
	         CHAR(10) + CHAR(9) + 'INNER JOIN E_TEAM ' +
                      CHAR(10) + CHAR(9) + 'ON  E_JOBDETAILS.TEAM = E_TEAM.TEAMID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C__CUSTOMER ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID ' +
	         CHAR(10) + CHAR(9) + 'INNER JOIN C_ADDRESS ' +
                      CHAR(10) + CHAR(9) + 'ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN'+ 
		      CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID '+
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PRIORITY ' +
	         CHAR(10) + CHAR(9) + 'ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID '+
	         CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_STATUS ' +
	         CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID '+
	         CHAR(10) + CHAR(9) + 'INNER JOIN FL_CO_APPOINTMENT_STAGE ' +
	         CHAR(10) + CHAR(9) + 'ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID '
	        
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.FaultLogID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FL_FAULT_LOG.FaultLogID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FL_FAULT_LOG.FaultLogID NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FL_FAULT_LOG.FaultLogID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'WHERE 1=1 AND FL_FAULT_STATUS.DESCRIPTION=''Assigned To Contractor''  AND'+ @SearchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)



GO
