SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO















CREATE     PROC [dbo].[FL_ELEMENT_AMENDNAME](
/* ===========================================================================
 '   NAME:         FL_ELEMENT_AMENDNAME
 '   DATE CREATED:  23 OCTOBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   AMEND AN ELEMENT NAME IN FL_ELEMENTTABLE
 '
 '   IN:	   @ELEMENTID
 '   IN:	   @ELEMENTNAME
 '
 '   OUT:           @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@ELEMENTID INT,
@ELEMENTNAME NVARCHAR(50),
@RESULT  INT OUTPUT
)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN

UPDATE    FL_ELEMENT
SET              ElementName =@ELEMENTNAME
WHERE     (ElementID = @ELEMENTID)

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	
SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN













GO
