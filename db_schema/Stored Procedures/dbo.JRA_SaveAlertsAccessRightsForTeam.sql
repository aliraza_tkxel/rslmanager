
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@return_value int

--DECLARE @alertIdList AS TEAMJOBROLE_ALERTS_ACCESSRIGHTS;
--INSERT INTO @alertIdList(JobRoleTeamId,AlertId)
--VALUES (1,1)

--EXEC	@return_value = [dbo].[JRA_SaveAlertsAccessRightsForTeam]
--		@alertIdList = @alertIdList,
--		@teamJobRoleId = 21

--SELECT	'Return Value' = @return_value
-- Author:<Ahmed Mehmood>
-- Create date: <Create Date,,26/12/2012>
-- Description:	<Save alerts access rights for team>
-- Web Page: Alerts.ascx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_SaveAlertsAccessRightsForTeam](
@teamJobRoleId int
,@userId int
,@alertIdList as TEAMJOBROLE_ALERTS_ACCESSRIGHTS readonly
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--====================================================
--			E_TEAMALERTS INSERTION
--====================================================

	-- Create temprary tables: #ins and #del
	SELECT * INTO #ins FROM E_TEAMALERTS WHERE 1 = 0
	SELECT * INTO #del FROM E_TEAMALERTS WHERE 1 = 0

	-- Delete the record if any and insert into temp table #del
	DELETE FROM E_TEAMALERTS
	OUTPUT DELETED.JobRoleTeamId,DELETED.AlertId INTO #del
	WHERE E_TEAMALERTS.JobRoleTeamId = @teamJobRoleId
	AND AlertId NOT IN (SELECT AlertId FROM @alertIdList)

	-- Insert any new record, and insert in to temp table #ins
	INSERT INTO E_TEAMALERTS( AlertId ,JobRoleTeamId )
	OUTPUT INSERTED.JobRoleTeamId,INSERTED.AlertId INTO #ins
	SELECT AlertId,JobRoleTeamId 
	FROM @alertIdList
	WHERE AlertId NOT IN(SELECT AlertId FROM E_TEAMALERTS WHERE JobRoleTeamId = @teamJobRoleId)

    ------------------------------------------------------
	-----Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
    Declare @jobRoleActionId int  

    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Access Rights Amended - Alerts'

	DECLARE @alertId INT = NULL
		
	-- Note deleted records in history
	-- Select first record
	SELECT @alertId = MIN(AlertId) FROM #del	
	WHILE @alertId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
			   ([JobRoleActionId]
			   ,[JobRoleTeamId]
			   ,[Detail]
			   ,[CreatedBy]
			   ,[CreatedDate])
		 VALUES
			   (@jobRoleActionId
			   ,@teamJobRoleId
			   ,(SELECT AlertName FROM E_ALERTS WHERE AlertID = @alertId)  + ', removed'
			   ,@userId
			   ,GETDATE())
			   
		-- Select next record
		SELECT @alertId = MIN(AlertId) FROM #del WHERE AlertId > @alertId
	END

	-- Note inserted records in history
	SET @alertId = NULL
	-- Select first record
	SELECT @alertId = MIN(AlertId) FROM #ins	
	WHILE @alertId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
			   ([JobRoleActionId]
			   ,[JobRoleTeamId]
			   ,[Detail]
			   ,[CreatedBy]
			   ,[CreatedDate])
		 VALUES
			   (@jobRoleActionId
			   ,@teamJobRoleId
			   ,(SELECT AlertName FROM E_ALERTS WHERE AlertID = @alertId)  + ', added'
			   ,@userId
			   ,GETDATE())
			   
		-- Select next record
		SELECT @alertId = MIN(AlertId) FROM #ins WHERE AlertId > @alertId
	END
	
	--Remove the temp tables
	IF OBJECT_ID('tempdb..#ins') IS NOT NULL DROP TABLE #ins
	IF OBJECT_ID('tempdb..#del') IS NOT NULL DROP TABLE #del
---------------------------------------------------------------   
           
END
GO
