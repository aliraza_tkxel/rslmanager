SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_Region_Amount] 
	@resourceId	int
AS
BEGIN

SELECT     SUM(AM_HistoricalBalanceList.RentBalance) AS TotalRent, MAX(HistoricalBalanceListId) AS Id, AM_HistoricalBalanceList.[month] AS months,
			MAX(Datepart(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))) AS years
FROM    AM_HistoricalBalanceList 
		INNER JOIN	C_TENANCY ON AM_HistoricalBalanceList.TenancyId = C_TENANCY.TenancyId 
		INNER JOIN  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN  E_PATCH ON P__PROPERTY.PATCH = E_PATCH.PATCHID

WHERE P__PROPERTY.PATCH IS NOT NULL 
	  AND  P__PROPERTY.PATCH IN (SELECT PatchId 
								 FROM AM_ResourcePatchDevelopment
								 WHERE (ResourceId = CASE WHEN @resourceId = -1 THEN ResourceId ELSE @resourceId END))
	  AND AM_HistoricalBalanceList.[month] IN (SELECT [month] FROM(
													SELECT DISTINCT TOP 6 [month], MAX(HistoricalBalanceListId) AS Id 
													FROM AM_HistoricalBalanceList
													WHERE [month] is not null  
													GROUP BY [month] 
													ORDER BY Id DESC) AS Temp)
GROUP BY AM_HistoricalBalanceList.[month]
ORDER BY  Id DESC
	
END


--exec AM_SP_Region_Amount -1






GO
