USE [RSLBHALive]
GO
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.FLS_GET_INCOME_RECOVERY_OFFICER') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_GET_INCOME_RECOVERY_OFFICER AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FLS_GET_INCOME_RECOVERY_OFFICER]
	-- Add the parameters for the stored procedure here
	@TENANCYID INT 
AS
BEGIN

SET NOCOUNT ON;
	Declare @schemeId int

	IF 1 = 2
  BEGIN
	SELECT '' as FullName, '' as HomeTel, '' as WorkEmail, '' as WorkMobile
	WHERE
    1 = 2 
  END

	DECLARE @RecoveryOfficer TABLE (
		FullName nvarchar(100)
		,HomeTel nvarchar(100)
		,WorkEmail nvarchar(100)
		,WorkMobile nvarchar(100)						
	)

-- Insert statements for procedure here
	INSERT INTO @RecoveryOfficer 
	Select RE.FIRSTNAME + ' ' + RE.LASTNAME as FullName, RE.HOMETEL, RE.WORKEMAIL, RE.WORKMOBILE
	FROM 	C_TENANCY T  
	                INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID  
	                INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	 
	                INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID AND A.ISDEFAULT = 1  
	                LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID  
	                INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  
					OUTER Apply(SELECT top 1 RE.FIRSTNAME,RE.LASTNAME,EC.WORKEMAIL, EC.WORKMOBILE, EC.HOMETEL FROM AM_ResourcePatchDevelopment RPD 
					INNER JOIN AM_Resource R ON RPD.ResourceId = R.ResourceId AND R.IsActive=1 
					INNER JOIN E__EMPLOYEE RE ON R.EmployeeId=RE.EMPLOYEEID 
					INNER JOIN E_CONTACT EC ON RE.EMPLOYEEID = EC.EMPLOYEEID WHERE RPD.SCHEMEID = P.SCHEMEID 
					AND RPD.IsActive=1 
					order by ResourcePatchDevelopmentId DESC) AS RE 	

      Where T.TENANCYID = @TENANCYID

	SELECT FullName, HomeTel, WorkEmail, WorkMobile FROM @RecoveryOfficer
END
GO
