USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.AS_GetPropertyLocations') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetPropertyLocations AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetPropertyLocations] 
AS
BEGIN

	SELECT LocationID, LocationName FROM PA_LOCATION

END