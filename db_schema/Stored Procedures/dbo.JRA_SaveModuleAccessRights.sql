
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit

--DECLARE @moduleIdList AS TEAMJOBROLE_MODULES_ACCESSRIGHTS;
--INSERT INTO @moduleIdList(JobRoleTeamId,ModuleId)
--VALUES (1,1)

--DECLARE @menuIdList AS TEAMJOBROLE_MENU_ACCESSRIGHTS;
--INSERT INTO @menuIdList(JobRoleTeamId,MenuId)
--VALUES (1,1)

--DECLARE @pageIdList AS TEAMJOBROLE_PAGE_ACCESSRIGHTS;
--INSERT INTO @pageIdList(JobRoleTeamId,PageId)
--VALUES (1,1)


--EXEC	@return_value = [dbo].[JRA_SaveModuleAccessRights]
--		@teamJobRoleId = 21,
--		@userId = 615,
--		@moduleIdList = @moduleIdList,
--		@menuIdList=@menuIdList,
--		@pageIdList=@pageIdList,
--		@isSaved = @isSaved OUTPUT

--SELECT	@isSaved as N'@isSaved'
--SELECT	'Return Value' = @return_value

-- Author:		Ahmed Mehmood
-- Create date: <2/1/2014>
-- Last Modified: <2/1/2014>
-- Description:	<Save Team Job Role. >
-- Web Page: JobRoles.aspx

-- =============================================
CREATE PROCEDURE [dbo].[JRA_SaveModuleAccessRights]
@teamJobRoleId int
,@userId int
,@moduleIdList as TEAMJOBROLE_MODULES_ACCESSRIGHTS readonly
,@menuIdList as TEAMJOBROLE_MENU_ACCESSRIGHTS readonly
,@pageIdList as TEAMJOBROLE_PAGE_ACCESSRIGHTS readonly
,@isSaved bit out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

	Declare @jobRoleActionId int

    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Access Rights Amended - Modules'

--====================================================
--			AC_MODULES_ACCESS INSERTION
--====================================================

-- Create temprary tables: #insModules and #delModules
SELECT * INTO #insModules FROM AC_MODULES_ACCESS WHERE 1 = 0
SELECT * INTO #delModules FROM AC_MODULES_ACCESS WHERE 1 = 0

-- Delete the record if any and insert into temp table #delModules
DELETE FROM AC_MODULES_ACCESS
OUTPUT DELETED.ModuleId, DELETED.JobRoleTeamId INTO #delModules
WHERE AC_MODULES_ACCESS.JobRoleTeamId = @teamJobRoleId
AND ModuleId NOT IN (SELECT ModuleId FROM @moduleIdList)

-- Insert any new record, and insert in to temp table #insModules
INSERT INTO AC_MODULES_ACCESS(ModuleId ,JobRoleTeamId )
OUTPUT INSERTED.ModuleId, INSERTED.JobRoleTeamId INTO #insModules
SELECT ModuleId,JobRoleTeamId 
FROM @moduleIdList
WHERE ModuleId NOT IN (SELECT ModuleId FROM AC_MODULES_ACCESS WHERE JobRoleTeamId = @teamJobRoleId)

	------------------------------------------------------
	-----Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
	DECLARE @moduleId INT = NULL
		
	-- Note deleted records in history
	-- Select first record
	SELECT @moduleId = MIN(ModuleId) FROM #delModules	
	WHILE @moduleId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
		VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           , (SELECT [DESCRIPTION] FROM AC_MODULES WHERE MODULEID = @moduleId ) + ', module removed'
           ,@userId
           ,GETDATE())
			   
		-- Select next record
		SELECT @moduleId = MIN(ModuleId) FROM #delModules WHERE ModuleId > @moduleId
	END
	
	-- Note deleted records in history
	SET @moduleId = NULL
	-- Select first record
	SELECT @moduleId = MIN(ModuleId) FROM #insModules	
	WHILE @moduleId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           , (SELECT [DESCRIPTION] FROM AC_MODULES WHERE MODULEID = @moduleId ) + ', module added'
           ,@userId
           ,GETDATE())
			   
		-- Select next record
		SELECT @moduleId = MIN(ModuleId) FROM #insModules WHERE ModuleId > @moduleId
	END
    
--Remove the temp tables
IF OBJECT_ID('tempdb..#insModules') IS NOT NULL DROP TABLE #insModules
IF OBJECT_ID('tempdb..#delModules') IS NOT NULL DROP TABLE #delModules

--====================================================
--			AC_MENUS_ACCESS INSERTION
--====================================================

-- Create temprary tables: #insMenus and #delMenus
SELECT * INTO #insMenus FROM AC_MENUS_ACCESS WHERE 1 = 0
SELECT * INTO #delMenus FROM AC_MENUS_ACCESS WHERE 1 = 0

-- Delete the record if any and insert into temp table #delMenus
DELETE FROM AC_MENUS_ACCESS
OUTPUT DELETED.MenuId, DELETED.JobRoleTeamId INTO #delMenus
WHERE AC_MENUS_ACCESS.JobRoleTeamId = @teamJobRoleId
AND MenuId NOT IN (SELECT MenuId FROM @menuIdList)

-- Insert any new record, and insert in to temp table #insMenus
INSERT INTO AC_MENUS_ACCESS(MenuId ,JobRoleTeamId )
OUTPUT INSERTED.MenuId, INSERTED.JobRoleTeamId INTO #insMenus
SELECT MenuId,JobRoleTeamId 
FROM @menuIdList
WHERE MenuId NOT IN (SELECT MenuId FROM AC_MENUS_ACCESS WHERE JobRoleTeamId = @teamJobRoleId)

	------------------------------------------------------
	-----Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
	DECLARE @menuId INT = NULL
		
	-- Note deleted records in history
	-- Select first record
	SELECT @menuId = MIN(MenuId) FROM #delMenus	
	WHILE @menuId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
		VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           , (SELECT ISNULL(MD.[DESCRIPTION] + '>','') + MN.[DESCRIPTION] 
				FROM AC_MENUS MN LEFT OUTER JOIN AC_MODULES MD ON MN.MODULEID = MD.MODULEID
				WHERE MN.MENUID = @menuId ) + ', menu removed'
           ,@userId
           ,GETDATE())

		-- Select next record
		SELECT @menuId = MIN(MenuId) FROM #delMenus WHERE MenuId > @menuId
	END
	
	-- Note deleted records in history
	SET @menuId = NULL
	-- Select first record
	SELECT @menuId = MIN(MenuId) FROM #insMenus	
	WHILE @menuId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
		VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           , (SELECT ISNULL(MD.[DESCRIPTION] + '>','') + MN.[DESCRIPTION] 
				FROM AC_MENUS MN LEFT OUTER JOIN AC_MODULES MD ON MN.MODULEID = MD.MODULEID
				WHERE MN.MENUID = @menuId ) + ', menu added'
           ,@userId
           ,GETDATE())

		-- Select next record
		SELECT @menuId = MIN(MenuId) FROM #insMenus WHERE MenuId > @menuId
	END

--Remove the temp tables
IF OBJECT_ID('tempdb..#insMenus') IS NOT NULL DROP TABLE #insMenus
IF OBJECT_ID('tempdb..#delMenus') IS NOT NULL DROP TABLE #delMenus

--====================================================
--			AC_PAGES_ACCESS INSERTION
--====================================================

-- Create temprary tables: #insPages and #delPages
SELECT * INTO #insPages FROM AC_PAGES_ACCESS WHERE 1 = 0
SELECT * INTO #delPages FROM AC_PAGES_ACCESS WHERE 1 = 0

-- Delete the record if any and insert into temp table #delPages
DELETE FROM AC_PAGES_ACCESS
OUTPUT DELETED.PageId, DELETED.JobRoleTeamId INTO #delPages
WHERE AC_PAGES_ACCESS.JobRoleTeamId = @teamJobRoleId
AND PageId NOT IN (SELECT PageId FROM @pageIdList)

-- Insert any new record, and insert in to temp table #insPages
INSERT INTO AC_PAGES_ACCESS(PageId,JobRoleTeamId )
OUTPUT INSERTED.PageId, INSERTED.JobRoleTeamId INTO #insPages
SELECT PageId,JobRoleTeamId 
FROM @pageIdList
WHERE PageId NOT IN (SELECT PageId FROM AC_PAGES_ACCESS WHERE JobRoleTeamId = @teamJobRoleId)
           
    ------------------------------------------------------
	-----Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
	DECLARE @pageId INT = NULL
		
	-- Note deleted records in history
	-- Select first record
	SELECT @pageId = MIN(PageId) FROM #delPages	
	WHILE @pageId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
		VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           , (SELECT ISNULL(MD.[DESCRIPTION] + '>','') + ISNULL(MN.[DESCRIPTION] + '>', '') + P.[DESCRIPTION]
				FROM AC_PAGES P LEFT OUTER JOIN AC_MENUS MN ON P.MENUID = MN.MENUID LEFT OUTER JOIN AC_MODULES MD ON MN.MODULEID = MD.MODULEID
				WHERE P.PageId = @pageId ) +  + ', page removed'
           ,@userId
           ,GETDATE())

		-- Select next record
		SELECT @pageId = MIN(PageId) FROM #delPages WHERE PageId > @pageId
	END
	
	-- Note deleted records in history
	SET @pageId = NULL
	-- Select first record
	SELECT @pageId = MIN(PageId) FROM #insPages	
	WHILE @pageId IS NOT NULL
	BEGIN	                   
		INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
		VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           , (SELECT ISNULL(MD.[DESCRIPTION] + '>','') + ISNULL(MN.[DESCRIPTION] + '>', '') + P.[DESCRIPTION]
				FROM AC_PAGES P LEFT OUTER JOIN AC_MENUS MN ON P.MENUID = MN.MENUID LEFT OUTER JOIN AC_MODULES MD ON MN.MODULEID = MD.MODULEID
				WHERE P.PageId = @pageId ) + ', page added'
           ,@userId
           ,GETDATE())

		-- Select next record
		SELECT @pageId = MIN(PageId) FROM #insPages WHERE PageId > @pageId
	END
           
--Remove the temp tables
IF OBJECT_ID('tempdb..#insPages') IS NOT NULL DROP TABLE #insPages
IF OBJECT_ID('tempdb..#delPages') IS NOT NULL DROP TABLE #delPages
---------------------------------------------------------------


END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END
GO
