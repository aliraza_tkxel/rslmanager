USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_PropertyActivities]    Script Date: 1/25/2019 3:30:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetServiceChargeReportData') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeReportData AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeReportData]
		@filterSchemeId int=-1,
		@filterBlockId int=-1,
		@filterItemId int=-1,	

		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'DateCreated', 
		@sortOrder varchar (5) = 'desc',
		@fetchAllRecords bit = 0,
		@fiscalYear int,
		@totalCount int = 0 output
AS            
BEGIN      
              
              
 DECLARE 
   @GeneralJournalSelectClause NVARCHAR(MAX),              
   @GeneralJournalFromClause NVARCHAR(MAX),              
   @GeneralJournalWhereClause NVARCHAR(MAX),              
   @GeneralJournalSearchCriteria NVARCHAR(MAX),
   
   @MSATSelectClause NVARCHAR(MAX),            
   @MSATFromClause NVARCHAR(MAX),            
   @MSATWhereClause NVARCHAR(MAX),    
   @MSATSearchCriteria NVARCHAR(MAX),       
                    
   @PurchaseItemSelectClause NVARCHAR(MAX),            
   @PurchaseItemFromClause NVARCHAR(MAX),            
   @PurchaseItemWhereClause NVARCHAR(MAX),  
   @PurchaseItemSearchCriteria NVARCHAR(MAX),
   
   @finalQuery NVARCHAR(MAX),            
   @rowNumberQuery NVARCHAR(MAX),                       
   @orderClause  NVARCHAR(MAX),                 
   @unionClause NVARCHAR(MAX),              
   @mainSelectQuery NVARCHAR(MAX),   
   @YStart NVARCHAR(100),
   @YEnd NVARCHAR(100),
   @offset int,
   @limit int,
   @cancelledPOStatus int

--=================================================================================================================================              
--  PAGINATION & FILTER SETUP 
--================================================================================================================================= 

SELECT	@YStart=YStart,@YEnd=YEnd 
FROM	F_FISCALYEARS 
WHERE	YRange=@fiscalYear

SET @offset = 1+(@pageNumber-1) * @pageSize
SET @limit = (@offset + @pageSize)-1

SELECT	@cancelledPOStatus = POSTATUSID
FROM	F_POSTATUS 
WHERE	POSTATUSNAME = 'Cancelled'

--=================================================================================================================================              
--  GENERAL JOURNAL SERVICE CHARGE DATA
--=================================================================================================================================              
     

SET @GeneralJournalSearchCriteria = ' NGJ.isIncSC = 1 
									  AND NJE.TIMECREATED BETWEEN CONVERT(DATETIME, ''' + @YStart +''', 120) and CONVERT(DATETIME, ''' + @YEnd +''', 120) AND (S.SchemeId > 0 OR B.BlockId > 0 ) '

IF(@filterItemId > 0)
BEGIN						
	SET @GeneralJournalSearchCriteria = @GeneralJournalSearchCriteria + CHAR(10) +' AND 1 = 0 '+  CHAR(10)
END	

IF(@filterBlockId > 0)
BEGIN						
	SET @GeneralJournalSearchCriteria = @GeneralJournalSearchCriteria + CHAR(10) +' AND B.BlockId = '+  CONVERT(NVARCHAR, @filterBlockId) + CHAR(10)
END	
ELSE IF(@filterSchemeId > 0)
BEGIN						
	SET @GeneralJournalSearchCriteria = @GeneralJournalSearchCriteria + CHAR(10) +' AND  ( S.SchemeId = '+  CONVERT(NVARCHAR, @filterSchemeId) + CHAR(10) + ' OR BS.SchemeId = '+  CONVERT(NVARCHAR, @filterSchemeId) + CHAR(10) + ') '+ CHAR(10)
END	


SET @GeneralJournalSelectClause = ' 

SELECT	 COALESCE ( BS.SCHEMENAME, S.SCHEMENAME,''-'') as SchemeName
		, ISNULL(B.BLOCKNAME,''-'') as BlockName
		, PropCount.InPropCount AS InPropCount
		, ISNULL(NJE.REFNUMBER,''-'')  AS Details
		, ''-'' as ItemName
		, ''-'' as Budget 
		, ''-'' as ApportionmentBudget
		, ISNULL(''�'' + CONVERT(VARCHAR,JournalCredit.Total,1),''-'') AS ActualTotal
		, ISNULL(''�'' + CONVERT(VARCHAR,CAST(JournalCredit.Total AS MONEY)/ NULLIF(PropCount.InPropCount,0),1), ''-'')   AS ApportionmentActual	
		, PropCount.InPropCount AS PropertyCount													
		, 0 AS ItemId
		, PropCount.ExPropCount AS ExPropCount
		, ISNULL(S.SCHEMEID,0) AS SchemeId
		, ISNULL(B.BLOCKID,0) AS BlockId		
		, NJE.TIMECREATED AS DateCreated
		, NJE.TXNId AS Ref
		, CONVERT(BIT, 0) AS IsServiceChargePO
		, ''General Journal'' as Type
		, NULL AS BudgetSort
		, NULL AS ApportionmentBudgetSort
		, JournalCredit.Total AS ActualTotalSort
		, CAST(JournalCredit.Total AS MONEY)/ NULLIF(PropCount.InPropCount,0) AS ApportionmentActualSort
		, 0 AS ChildItemId 
' + CHAR(10)      
      
SET @GeneralJournalFromClause = CHAR(10) + '

FROM	NL_GENERALJOURNAL NGJ
		INNER JOIN NL_JOURNALENTRY NJE ON NGJ.GJID = NJE.TRANSACTIONID
		LEFT JOIN P_SCHEME S ON NJE.SCHEMEID = S.SCHEMEID
		LEFT JOIN P_BLOCK B ON NJE.BLOCKID = B.BLOCKID
		LEFT JOIN P_SCHEME BS ON B.SCHEMEID = BS.SCHEMEID

		OUTER APPLY (
					SELECT	SUM (ISNULL(Amount,0)) Total
					FROM	NL_JOURNALENTRYCREDITLINE NJEC
					WHERE   NJEC.TXNID = NJE.TXNID
					) JournalCredit

		INNER JOIN (
					SELECT	TXNID
							, CASE 
								WHEN NL_JOURNALENTRY.PROPERTYID IS NOT NULL THEN
									1 
								WHEN NL_JOURNALENTRY.BLOCKID IS NOT NULL AND NL_JOURNALENTRY.BLOCKID > 0 THEN
									ISNULL(BlockProps.PropCount, 0)
								ELSE	
									ISNULL(SchemeProps.PropCount, 0)
								END AS InPropCount
							, CASE 
								WHEN NL_JOURNALENTRY.PROPERTYID IS NOT NULL THEN
									CASE 
									WHEN NL_JOURNALENTRY.BLOCKID IS NOT NULL AND NL_JOURNALENTRY.BLOCKID > 0 THEN
										ISNULL(BlockProps.PropCount, 0) - 1 
									ELSE	
										ISNULL(SchemeProps.PropCount, 0) - 1
									END
								ELSE	
									0
								END AS ExPropCount

					FROM	NL_JOURNALENTRY
							LEFT JOIN 
							(SELECT SCHEMEID, COUNT(PROPERTYID) PropCount
							 FROM   P__PROPERTY
							 WHERE  SCHEMEID IS NOT NULL
							 GROUP BY SCHEMEID) SchemeProps ON NL_JOURNALENTRY.SCHEMEID = SchemeProps.SCHEMEID

							 LEFT JOIN 
							(SELECT BLOCKID, COUNT(PROPERTYID) PropCount
							 FROM   P__PROPERTY
							 WHERE  BLOCKID IS NOT NULL
							 GROUP BY BLOCKID) BlockProps ON NL_JOURNALENTRY.blockId = BlockProps.blockId	
							 							 
					 					
		) PropCount ON PropCount.TXNID = NJE.TXNID
									
									 '   + CHAR(10)      
      
SET @GeneralJournalWhereClause = CHAR(10) + ' WHERE 1=1 AND ' + CHAR(10) + @GeneralJournalSearchCriteria       

  -- --PRINT '--===================================================================='  
	 --PRINT  @GeneralJournalSelectClause 
	 --PRINT  @GeneralJournalFromClause
	 --PRINT  @GeneralJournalWhereClause  
 

--=============================================================================================================================================            
-- MSAT SERVICE CHARGE DATA      
--=============================================================================================================================================            


SET @MSATSearchCriteria = ' MSATTypeName IN (''Service Charge'', ''Raise A PO'')
							AND (CSI.SchemeId > 0 OR CSI.BlockId > 0)
							AND ((CSI.ChildAttributeMappingId IS NULL) OR (CSI.ChildAttributeMappingId > 0 AND PCAM.ISACTIVE = 1))
							AND ((MSATTypeName = ''Service Charge'' AND IsRequired = 1) OR (MSATTypeName = ''Raise A PO'' AND INCSC = 1)) '	+ CHAR(10)					
														

IF(@filterItemId > 0)
BEGIN						
	SET @MSATSearchCriteria = @MSATSearchCriteria + CHAR(10) +' AND CSI.ItemId = '+  CONVERT(NVARCHAR, @filterItemId) + CHAR(10)
END	

IF(@filterBlockId > 0)
BEGIN						
	SET @MSATSearchCriteria = @MSATSearchCriteria + CHAR(10) +' AND CSI.BlockId = '+  CONVERT(NVARCHAR, @filterBlockId) + CHAR(10)
END	
ELSE IF(@filterSchemeId > 0)
BEGIN						
	SET @MSATSearchCriteria = @MSATSearchCriteria + CHAR(10) +' AND  ( S.SchemeId = '+  CONVERT(NVARCHAR, @filterSchemeId) + CHAR(10) + ' OR BS.SchemeId = '+  CONVERT(NVARCHAR, @filterSchemeId) + CHAR(10) + ') '+ CHAR(10)
END	

SET @MSATSelectClause = ' 
		
		SELECT	
		COALESCE ( BS.SCHEMENAME, S.SCHEMENAME,''-'') AS SchemeName
		, ISNULL(B.BLOCKNAME,''-'') AS BlockName
		, IncProp.PropCount AS InPropCount
		, CASE WHEN MSATTypeName = ''Service Charge'' THEN 
					ISNULL( ''PO '' + CONVERT(VARCHAR,ServiceChargePOInfo.OrderId) , ''-'') 
			   ELSE ISNULL( ''PO '' + CONVERT(VARCHAR,RaisePOInfo.OrderId) , ''-'') 
		  END AS Details			
		, CASE WHEN  CSI.ChildAttributeMappingId IS NULL THEN
				ISNULL(PII.ItemName,''-'')
			ELSE
					ISNULL(PII.ItemName + '' > ''+ [dbo].[GetAttributeChildItemName](CSI.ITEMID, CSI.ChildAttributeMappingId, CSI.SCHEMEID, CSI.BLOCKID),''-'') 
			END
		AS ItemName		
		, ISNULL(''�'' + CONVERT(VARCHAR, CAST(CSI.AnnualApportionment AS money), 1), ''-'')  AS Budget 			
		, ISNULL(''�'' + CONVERT(VARCHAR, CONVERT(VARCHAR,CAST(CSI.AnnualApportionment AS MONEY)/ NULLIF(IncProp.PropCount,0),1) ) , ''-'') AS ApportionmentBudget	
		, CASE WHEN MSATTypeName = ''Service Charge'' THEN 
					ISNULL(''�'' + CONVERT(VARCHAR, CAST(ServiceChargePOInfo.ActualTotal AS money), 1), ''-'') 
			   ELSE 
					ISNULL(''�'' + CONVERT(VARCHAR, CAST(RaisePOInfo.ActualTotal AS money), 1), ''-'') 
		  END AS ActualTotal
							
		, CASE WHEN MSATTypeName = ''Service Charge'' THEN 
					ISNULL(''�'' + CONVERT(VARCHAR,CONVERT(VARCHAR,CAST(ServiceChargePOInfo.ActualTotal AS MONEY)/ NULLIF(IncProp.PropCount ,0),1) ) , ''-'') 
			   ELSE 
					ISNULL(''�'' + CONVERT(VARCHAR,CONVERT(VARCHAR,CAST(RaisePOInfo.ActualTotal AS MONEY) / NULLIF(IncProp.PropCount,0),1) ) , ''-'') 
		  END AS ApportionmentActual
		    		
		, CASE 
			WHEN CSI.BLOCKID IS NOT NULL AND CSI.BLOCKID > 0 THEN
				ISNULL(BlockProps.count, 0)
			ELSE	
				ISNULL(SchemeProps.count, 0)
			END  AS PropertyCount
		, PII.ItemID AS ItemId
		,CASE 
			WHEN CSI.BLOCKID IS NOT NULL AND CSI.BLOCKID > 0 THEN
				ISNULL(BlockExcludedProp.count, 0)
			ELSE	
				ISNULL(SchemeExcludedProp.count, 0)
			END AS ExPropCount	
		, CSI.SCHEMEID AS SchemeId
		, CSI.BLOCKID AS BlockId												
		, CASE WHEN MSATTypeName = ''Service Charge'' THEN ServiceChargePOInfo.PoDate 
			   ELSE RaisePOInfo.PoDate
		  END AS DateCreated							
		, CASE WHEN MSATTypeName = ''Service Charge'' THEN ServiceChargePOInfo.OrderId 
			   ELSE RaisePOInfo.OrderId
		  END AS Ref
		, CONVERT(BIT, 0) AS IsServiceChargePO 
		, CASE WHEN MSATTypeName = ''Service Charge'' THEN ''Cyclic Services''
			   ELSE ''Raise a PO''
		  END AS Type			
		, CAST(CSI.AnnualApportionment AS MONEY) AS BudgetSort
		, CAST(CSI.AnnualApportionment AS MONEY) / NULLIF(IncProp.PropCount ,0)  AS ApportionmentBudgetSort	

		, CASE WHEN MSATTypeName = ''Service Charge'' THEN 
					ServiceChargePOInfo.ActualTotal 
			   ELSE 
					RaisePOInfo.ActualTotal
		  END AS ActualTotalSort

		, CASE WHEN MSATTypeName = ''Service Charge'' THEN 					
					CAST(ServiceChargePOInfo.ActualTotal AS MONEY) / NULLIF(IncProp.PropCount,0)
			   ELSE 
					CAST(RaisePOInfo.ActualTotal AS MONEY) / NULLIF(IncProp.PropCount,0)					
		  END AS ApportionmentActualSort
		  , ISNULL(CSI.ChildAttributeMappingId,0) AS ChildItemId 
							' + CHAR(10)    
    
SET @MSATFromClause = CAST('' as nvarchar(MAX)) + '  FROM	PDR_MSAT CSI
		INNER JOIN PDR_MSATTYPE PST ON  CSI.MSATTypeId = PST.MSATTypeId
		INNER JOIN PA_ITEM PII ON CSI.ITEMID = PII.ITEMID		
		INNER JOIN PDR_AttributesType PAT ON CSI.attributeTypeId = PAT.AttributeTypeId
		LEFT JOIN PA_ChildAttributeMapping PCAM ON CSI.ChildAttributeMappingId = PCAM.ID
		LEFT JOIN (	 SELECT	PJ.MsatId, FPO.OrderId , FPO.PoDate
							, CASE WHEN (FPS.POSTATUSNAME = ''Reconciled'' OR FPS.POSTATUSNAME = ''Paid By Cheque'' OR FPS.POSTATUSNAME = ''Paid By BACS'' 
										OR FPS.POSTATUSNAME = ''Paid by Direct Debit'' OR FPS.POSTATUSNAME = ''Paid'' OR FPS.POSTATUSNAME = ''Paid By Cash'' 
										OR FPS.POSTATUSNAME = ''Part Paid'') THEN  
											CAST(PurchaseTotalInfo.ActualTotal AS money)
							   ELSE
										NULL
							END AS ActualTotal
					 FROM	PDR_JOURNAL PJ
							INNER JOIN PDR_CONTRACTOR_WORK PCW ON PJ.JOURNALID = PCW.JournalId
							INNER JOIN F_PURCHASEORDER FPO ON PCW.PurchaseOrderId = FPO.ORDERID
							INNER JOIN F_POSTATUS FPS ON FPO.POSTATUS = FPS.POSTATUSID
							OUTER APPLY (SELECT	SUM(FPI.GROSSCOST) AS ActualTotal
										 FROM	F_PURCHASEITEM FPI
										 WHERE	FPI.ORDERID = FPO.ORDERID 
										 AND FPI.PISTATUS != '+  CONVERT(varchar, @cancelledPOStatus) + ' 
										 GROUP BY FPI.ORDERID) AS PurchaseTotalInfo
					 WHERE	 FPO.PODATE BETWEEN CONVERT(DATETIME, ''' + @YStart +''' , 120) AND CONVERT(DATETIME, ''' + @YEnd +''' , 120)   
							 AND FPO.POSTATUS != '+  CONVERT(varchar, @cancelledPOStatus) +' AND FPO.IsServiceChargePO IS NULL
				 ) RaisePOInfo ON RaisePOInfo.MSATID = CSI.MSATId

		OUTER APPLY (	 SELECT	FPO.OrderId , FPO.PoDate
						, CASE WHEN (FPS.POSTATUSNAME = ''Reconciled'' OR FPS.POSTATUSNAME = ''Paid By Cheque'' OR FPS.POSTATUSNAME = ''Paid By BACS'' 
						 			 OR FPS.POSTATUSNAME = ''Paid by Direct Debit'' OR FPS.POSTATUSNAME = ''Paid'' OR FPS.POSTATUSNAME = ''Paid By Cash'' 
									 OR FPS.POSTATUSNAME = ''Part Paid'') THEN  
											CAST(PurchaseTotalInfo.ActualTotal AS money)
							   ELSE
									NULL
							END AS ActualTotal
					 FROM	CM_ServiceItems CM
							INNER JOIN F_PURCHASEORDER FPO ON CM.PORef = FPO.ORDERID
							INNER JOIN F_POSTATUS FPS ON FPO.POSTATUS = FPS.POSTATUSID
							OUTER APPLY (SELECT	SUM(FPI.GROSSCOST) AS ActualTotal
										 FROM	F_PURCHASEITEM FPI
										 WHERE	FPI.ORDERID = FPO.ORDERID 
										 AND FPI.PISTATUS != '+  CONVERT(varchar, @cancelledPOStatus) + ' GROUP BY FPI.ORDERID) AS PurchaseTotalInfo
					 WHERE	 FPO.PODATE BETWEEN CONVERT(DATETIME, ''' + @YStart +''' , 120) AND CONVERT(DATETIME, ''' + @YEnd +''' , 120) 
							 AND FPO.POSTATUS != '+  CONVERT(varchar, @cancelledPOStatus)+ ' AND FPO.IsServiceChargePO IS NULL
							AND CSI.ItemId = CM.ITEMID
							AND (CSI.ChildAttributeMappingId IS NULL OR CSI.ChildAttributeMappingId = CM.ChildAttributeMappingId)
							AND ((CSI.SCHEMEID IS NOT NULL AND CSI.SCHEMEID = CM.SCHEMEID)OR(CSI.BLOCKID IS NOT NULL AND CSI.BLOCKID = CM.BLOCKID))
				 ) ServiceChargePOInfo 

		LEFT JOIN P_SCHEME S ON CSI.SchemeId = S.SCHEMEID
		LEFT JOIN P_BLOCK B ON CSI.BLOCKID = B.BLOCKID
		LEFT JOIN P_SCHEME BS ON B.SCHEMEID = BS.SCHEMEID
		OUTER APPLY ( 		
					SELECT COUNT(*) AS Count
					FROM   P__PROPERTY P
		            WHERE  P.schemeid = CSI.SchemeId AND P.PROPERTYID NOT IN (	SELECT	DISTINCT PROPERTYID
																				FROM	dbo.PDR_ServiceChargeProperties P1 
																				WHERE	P1.IsIncluded = 1 
																						AND P1.IsActive = 1 
																						AND P1.SCHEMEID = CSI.SchemeId 
																						AND P1.ItemID = CSI.ItemID
																						AND (CSI.ChildAttributeMappingId IS NULL OR CSI.ChildAttributeMappingId = P1.ChildAttributeMappingId))					 							 							 				 
					 ) AS SchemeExcludedProp

		OUTER APPLY (	SELECT COUNT(*) AS Count
						FROM   P__PROPERTY P
						WHERE  P.BlockId = CSI.BlockId  AND P.PROPERTYID NOT IN (SELECT	DISTINCT PROPERTYID
																				FROM	dbo.PDR_ServiceChargeProperties P1 
																				WHERE	P1.IsIncluded = 1 
																						AND P1.IsActive = 1 
																						AND P1.BlockId = CSI.BlockId 
																						AND P1.ItemID = CSI.ItemID
																						AND (CSI.ChildAttributeMappingId IS NULL OR CSI.ChildAttributeMappingId = P1.ChildAttributeMappingId))			 
					 ) AS BlockExcludedProp

		OUTER APPLY ( SELECT COUNT(*) AS Count					 
					  FROM	 P__PROPERTY
					  WHERE	 SCHEMEID = CSI.SCHEMEID 	
							 AND SCHEMEID IS NOT NULL 		 
					 ) AS SchemeProps

		OUTER APPLY ( SELECT COUNT(*) AS Count					 
					  FROM	 P__PROPERTY
					  WHERE	 blockid = CSI.blockid 	
							 AND blockid IS NOT NULL 		 
					 ) AS BlockProps

		INNER JOIN (
					SELECT	MSATId
							, CASE 
								WHEN PDR_MSAT.SchemeProperty IS NOT NULL AND PDR_MSAT.SchemeProperty != ''-1'' THEN
									1 
								WHEN PDR_MSAT.BLOCKID IS NOT NULL AND PDR_MSAT.BLOCKID > 0 THEN
									ISNULL(BlockIncludedProp.Count, 0)
								ELSE	
									ISNULL(SchemeIncludedProp.Count, 0)
								END AS PropCount

					FROM	PDR_MSAT
							OUTER APPLY ( SELECT COUNT(*) AS Count					 
										  FROM	 PDR_ServiceChargeProperties
										  WHERE	 isincluded=1 AND IsActive=1 
												 AND PDR_MSAT.SCHEMEID = SCHEMEID 
												 AND PDR_MSAT.ITEMID = ItemId
												 AND (PDR_MSAT.ChildAttributeMappingId IS NULL OR PDR_MSAT.ChildAttributeMappingId = PDR_ServiceChargeProperties.ChildAttributeMappingId)				 
										 ) AS SchemeIncludedProp

							 OUTER APPLY ( SELECT COUNT(*) AS Count					 
										  FROM	 PDR_ServiceChargeProperties
										  WHERE	 isincluded=1 AND IsActive=1 
												 AND PDR_MSAT.BlockId = BlockId
												 AND PDR_MSAT.ITEMID = ItemId
												 AND (PDR_MSAT.ChildAttributeMappingId IS NULL OR PDR_MSAT.ChildAttributeMappingId = PDR_ServiceChargeProperties.ChildAttributeMappingId)				 
										 ) AS BlockIncludedProp	
							 							 
					 					
		) IncProp ON IncProp.MSATId = CSI.MSATId

     ' + CHAR(10)    
    

SET @MSATWhereClause = CHAR(10) + ' WHERE 1=1 AND ' + CHAR(10) + @MSATSearchCriteria    
  
     
    PRINT '--===================================================================='  
    PRINT @MSATSelectClause 
    PRINT @MSATFromClause
    PRINT @MSATWhereClause  
    



--=================================================================================================================================================
--   PURCHASE ITEM SERVICE CHARGE DATA   
--=================================================================================================================================================

DECLARE @allPropertiesCount INT , @itemIncPropertyCount INT , @itemExcPropertyCount INT
		, @itemSchemeCount INT, @minSchemeId INT, @blockProperties INT, @schemeProperties INT, @orderItemId INT
		, @SchemeId INT, @BlockId INT, @PropertyId NVARCHAR(40), @BlockScheme INT, @isItemIncluded BIT


SET @PurchaseItemSearchCriteria = ' FPO.IsServiceChargePO = 1 
									AND  FPO.PODATE BETWEEN CONVERT(DATETIME, ''' + @YStart +''' , 120) AND CONVERT(DATETIME, ''' + @YEnd +''' , 120) 
									AND #ItemPropertyInfo.isItemIncluded = 1 '
									+ ' AND FPO.POSTATUS != '+  CONVERT(NVARCHAR, @cancelledPOStatus) + CHAR(10)
									+ ' AND FPI.PISTATUS != '+  CONVERT(NVARCHAR, @cancelledPOStatus) + CHAR(10)


IF(@filterItemId > 0)
BEGIN						
	SET @PurchaseItemSearchCriteria = @PurchaseItemSearchCriteria + CHAR(10) +' AND 1 = 0 '+  CHAR(10)
END	


--============================================================
-- CALCULATING PURCHASE ITEM INC/EXC PROPERTIES, TOTAL SCHEMES
--============================================================

		CREATE TABLE #ItemPropertyInfo
		(
			orderItemId INT,
			propertyIncluded INT, 
			propertyExcluded INT,
			schemeCount INT,
			isItemIncluded BIT,
			minSchemeId INT
		)

		DECLARE PurchaseItem_Cursor CURSOR FOR
		SELECT	ORDERITEMID
		FROM	F_PURCHASEORDER FPO
				INNER JOIN F_PURCHASEITEM FPI ON FPO.ORDERID = FPI.ORDERID
		WHERE	FPO.IsServiceChargePO = 1 
				AND  FPO.PODATE BETWEEN CONVERT(DATETIME, @YStart , 120) and CONVERT(DATETIME, @YEnd , 120)

		OPEN PurchaseItem_Cursor

		FETCH NEXT FROM PurchaseItem_Cursor
		INTO @orderItemId

		WHILE @@FETCH_STATUS = 0
		BEGIN

			SET @itemIncPropertyCount = 0
			SET @itemExcPropertyCount = 0
			SET @itemSchemeCount = 0
			SET @isItemIncluded = 0
			
			--===========================================
			-- CALCULATE TOTAL EXCLUDED PROPERTIES
			--===========================================
			SELECT	@itemExcPropertyCount = COUNT(PROPERTYID) 
			FROM	F_ServiceChargeExProperties 
			WHERE	PurchaseItemId = @orderitemid

			--===========================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES
			--===========================================
	
			DECLARE PropertyCount_Cursor CURSOR FOR
			SELECT	SchemeId, BlockId, PropertyId
			FROM	F_PurchaseItemSCInfo
			WHERE   OrderItemId = @orderitemid AND IsActive = 1

			OPEN PropertyCount_Cursor
	

			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId

			WHILE @@FETCH_STATUS = 0
			BEGIN

				IF @PropertyId != '-1'
				BEGIN

					IF NOT EXISTS(  SELECT  1
									FROM	F_ServiceChargeExProperties 
									WHERE	PurchaseItemId = @orderitemid 
											AND PropertyId = @PropertyId)
					BEGIN
						SET @itemIncPropertyCount = @itemIncPropertyCount + 1
					END
			
				END
				ELSE IF @BlockId != -1 AND @BlockId IS NOT NULL
				BEGIN
					 SET @blockProperties = 0
					  SET @BlockScheme = 0

					 SELECT @blockProperties = COUNT(PROPERTYID) , @BlockScheme = MAX(SchemeId) 
					 FROM	P__PROPERTY 
					 WHERE  BLOCKID = @BlockId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

					 SET @itemIncPropertyCount = @itemIncPropertyCount + @blockProperties
				END
				ELSE IF @SchemeId != -1
				BEGIN
					 SET @schemeProperties = 0

					 SELECT @schemeProperties = COUNT(PROPERTYID) 
					 FROM	P__PROPERTY 
					 WHERE  SCHEMEID = @SchemeId AND PropertyId NOT IN (SELECT  PROPERTYID 
																		FROM	F_ServiceChargeExProperties 
																		WHERE	PurchaseItemId = @orderitemid)

					 SET @itemIncPropertyCount = @itemIncPropertyCount + @schemeProperties
				END
				ELSE IF @SchemeId = -1
				BEGIN
						SELECT	@allPropertiesCount = COUNT(propertyId)
						FROM	P__PROPERTY 
						WHERE	(SCHEMEID != -1 OR SCHEMEID IS NOT NULL )
								AND PropertyId NOT IN (	SELECT	PROPERTYID 
														FROM	F_ServiceChargeExProperties 
														WHERE	PurchaseItemId = @orderitemid)
			
						SET @itemIncPropertyCount = @itemIncPropertyCount + @allPropertiesCount

				END


			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId
			END

			CLOSE  PropertyCount_Cursor
			DEALLOCATE PropertyCount_Cursor


			--===========================================
			-- CALCULATE TOTAL NUMBER OF SCHEMES 
			--===========================================

			 SELECT	@minSchemeId = MIN(schemeid) 
			 FROM	F_PurchaseItemSCInfo 
			 WHERE	OrderItemId = @orderItemId 
					AND IsActive = 1

			 IF @minSchemeId = -1
				BEGIN
					
					SELECT	@itemSchemeCount = COUNT(*)
					FROM	P_SCHEME

					SET @isItemIncluded = 1

				END
			ELSE
				BEGIN

					SELECT	@itemSchemeCount = COUNT(DISTINCT SCHEMEID)
					FROM	F_PurchaseItemSCInfo
					WHERE	OrderItemId = @orderItemId AND IsActive = 1



					IF @filterSchemeId IS NOT NULL AND @filterSchemeId > 0
					BEGIN

						IF EXISTS (	SELECT	1
									FROM	F_PurchaseItemSCInfo
									WHERE	OrderItemId = @orderItemId AND IsActive = 1 AND SCHEMEID = @filterSchemeId)
						BEGIN
							SET @isItemIncluded = 1
						END
						ELSE
						BEGIN
							SET @isItemIncluded = 0
						END

					END
					ELSE
					BEGIN
						SET @isItemIncluded = 1
					END



					IF @filterBlockId IS NOT NULL AND @filterBlockId > 0
					BEGIN


						SELECT	@BlockScheme = SCHEMEID
						FROM	P_BLOCK
						WHERE	BLOCKID = @filterBlockId  


						IF EXISTS (	SELECT	1
									FROM	F_PurchaseItemSCInfo
									WHERE	OrderItemId = @orderItemId 
											AND IsActive = 1 
											AND (SCHEMEID = @BlockScheme AND (BLOCKID IS NULL OR BLOCKID = -1 OR BLOCKID = @filterBlockId)))
						BEGIN
							SET @isItemIncluded = 1
						END
						ELSE
						BEGIN
							SET @isItemIncluded = 0
						END

					END
					

				END

			INSERT INTO #ItemPropertyInfo (orderItemId, propertyIncluded, propertyExcluded, schemeCount,isItemIncluded,minSchemeId)
			VALUES (@orderitemid, @itemIncPropertyCount, @itemExcPropertyCount, @itemSchemeCount,@isItemIncluded,@minSchemeId)


		FETCH NEXT FROM PurchaseItem_Cursor
		INTO @orderItemId

		END

		CLOSE PurchaseItem_Cursor
		DEALLOCATE PurchaseItem_Cursor



             
SET @PurchaseItemSelectClause = ' SELECT 
									  CASE WHEN #ItemPropertyInfo.schemeCount > 1  THEN 
											''Multiple Schemes Selected''
									  ELSE
											PS.SCHEMENAME
									  END AS SchemeName							  
							, ''-'' AS BlockName
							, #ItemPropertyInfo.propertyIncluded AS InPropCount
							, ''PO ''+CONVERT(VARCHAR,FPO.ORDERID)  AS Details
							, FPI.ITEMNAME AS ItemName
							, ''-'' AS Budget
							, ''-'' AS ApportionmentBudget
							, CASE WHEN (FPS.POSTATUSNAME = ''Reconciled''
										 OR FPS.POSTATUSNAME = ''Paid By Cheque''
										 OR FPS.POSTATUSNAME = ''Paid By BACS''
										 OR FPS.POSTATUSNAME = ''Paid by Direct Debit''
										 OR FPS.POSTATUSNAME = ''Paid''
										 OR FPS.POSTATUSNAME = ''Paid By Cash''
										 OR FPS.POSTATUSNAME = ''Part Paid''										 
										 ) THEN 
								''�''+ CONVERT(varchar,CONVERT(NUMERIC(38, 2), FPI.GROSSCOST/(#ItemPropertyInfo.schemeCount*1.0))) 
							  ELSE
									''-''
							  END AS ActualTotal			
							, CASE WHEN (FPS.POSTATUSNAME = ''Reconciled''
										 OR FPS.POSTATUSNAME = ''Paid By Cheque''
										 OR FPS.POSTATUSNAME = ''Paid By BACS''
										 OR FPS.POSTATUSNAME = ''Paid by Direct Debit''
										 OR FPS.POSTATUSNAME = ''Paid''
										 OR FPS.POSTATUSNAME = ''Paid By Cash''
										 OR FPS.POSTATUSNAME = ''Part Paid''										 
										 ) THEN 
									''�''+ CONVERT(varchar,CONVERT(NUMERIC(38, 2), (FPI.GROSSCOST/(#ItemPropertyInfo.schemeCount*1.0))/(#ItemPropertyInfo.propertyIncluded*1.0)))
							  ELSE
									''-''
							  END AS ApportionmentActual								  														  	
							, ISNULL(#ItemPropertyInfo.propertyIncluded,0) as PropertyCount
							, FPI.ORDERITEMID as ItemId
							, #ItemPropertyInfo.propertyExcluded AS ExPropCount
							,NULL AS SchemeId
							,NULL AS BlockId							
							,FPO.PODATE AS DateCreated
							, FPO.ORDERID AS Ref
							,FPO.IsServiceChargePO
	  
							,''Purchase Item'' as Type
							,NULL AS BudgetSort
							,NULL AS ApportionmentBudgetSort
							, CASE WHEN (FPS.POSTATUSNAME = ''Reconciled''
										 OR FPS.POSTATUSNAME = ''Paid By Cheque''
										 OR FPS.POSTATUSNAME = ''Paid By BACS''
										 OR FPS.POSTATUSNAME = ''Paid by Direct Debit''
										 OR FPS.POSTATUSNAME = ''Paid''
										 OR FPS.POSTATUSNAME = ''Paid By Cash''
										 OR FPS.POSTATUSNAME = ''Part Paid''										 
										 ) THEN  
								CONVERT(NUMERIC(38, 2), FPI.GROSSCOST/(#ItemPropertyInfo.schemeCount*1.0))
							  ELSE
									NULL
							  END AS ActualTotalSort

							 , CASE WHEN (FPS.POSTATUSNAME = ''Reconciled''
										 OR FPS.POSTATUSNAME = ''Paid By Cheque''
										 OR FPS.POSTATUSNAME = ''Paid By BACS''
										 OR FPS.POSTATUSNAME = ''Paid by Direct Debit''
										 OR FPS.POSTATUSNAME = ''Paid''
										 OR FPS.POSTATUSNAME = ''Paid By Cash''
										 OR FPS.POSTATUSNAME = ''Part Paid''										 
										 ) THEN  
									CONVERT(NUMERIC(38, 2), (FPI.GROSSCOST/(#ItemPropertyInfo.schemeCount*1.0))/(#ItemPropertyInfo.propertyIncluded*1.0))
							  ELSE
									NULL
							  END AS ApportionmentActualSort	
							  , 0 AS ChildItemId 


	  ' + CHAR(10)      
      
SET @PurchaseItemFromClause = '  FROM	F_PURCHASEITEM FPI
					INNER JOIN F_PURCHASEORDER FPO ON FPI.ORDERID = FPO.ORDERID
					INNER JOIN F_POSTATUS FPS ON FPO.POSTATUS = FPS.POSTATUSID
					INNER JOIN #ItemPropertyInfo ON FPI.ORDERITEMID = #ItemPropertyInfo.orderItemId
					LEFT JOIN P_SCHEME PS ON  #ItemPropertyInfo.minSchemeId = PS.SCHEMEID
   '      
    
SET @PurchaseItemWhereClause = CHAR(10) + ' WHERE 1=1 AND ' + CHAR(10) + @PurchaseItemSearchCriteria 
  
-- PRINT '--===================================================================='  
--PRINT @PurchaseItemSelectClause + @PurchaseItemFromClause + @PurchaseItemWhereClause  
  
--=============================================================================================================              
           
IF @sortColumn = 'Budget'
BEGIN
	SET @sortColumn = 'BudgetSort'
END

IF @sortColumn = 'ApportionmentBudget'
BEGIN
	SET @sortColumn = 'ApportionmentBudgetSort'
END

IF @sortColumn = 'ActualTotal'
BEGIN
	SET @sortColumn = 'ActualTotalSort'
END

IF @sortColumn = 'ApportionmentActual'
BEGIN
	SET @sortColumn = 'ApportionmentActualSort'
END

SET @unionClause = CHAR(10) + CHAR(9) + 'UNION ALL' + CHAR(10) + CHAR(9)      
SET @OrderClause = ' ORDER BY ' + @sortColumn + ' ' + @sortOrder + CHAR(10) 
  
    
 SET @mainSelectQuery = 
 @GeneralJournalSelectClause + @GeneralJournalFromClause + @GeneralJournalWhereClause    
 + @unionClause  
 + @PurchaseItemSelectClause + @PurchaseItemFromClause + @PurchaseItemWhereClause     
 + @unionClause    
 + @MSATSelectClause + @MSATFromClause + @MSATWhereClause

  


--=============================================================================================================
-- FINAL QUERY
--=============================================================================================================


IF @fetchAllRecords = 1 
BEGIN

	SET @rowNumberQuery = '  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
						FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	--PRINT(@rowNumberQuery)		
	EXEC (@rowNumberQuery)
END
ELSE
BEGIN

SET @rowNumberQuery =	'  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
						FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		

SET @finalQuery  =  ' SELECT *
					FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
					WHERE
					Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)	

	--PRINT(@finalQuery)		
	EXEC (@finalQuery)
END


--==============================================================================================================
		
Declare @selectCount NVARCHAR(MAX), 
@parameterDef NVARCHAR(500)
		
SET @parameterDef = '@totalCount int OUTPUT';
SET @selectCount=  ' SELECT @totalCount =  COUNT(*) FROM ( ' + @mainSelectQuery + ' ) ServiceChargeCount'


EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
--========================================================================================	
 
  

  DROP TABLE #ItemPropertyInfo
    
END

