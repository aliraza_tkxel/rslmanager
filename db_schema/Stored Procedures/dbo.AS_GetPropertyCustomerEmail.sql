USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyCustomerEmail]    Script Date: 14/04/2017 09:51:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC AS_GetPropertyCustomerEmail
		--@letterHistoryId=20791
		
-- Author:		Saud Ahmed
-- Create date: 14/04/2017
-- Description:	<This stored procedure returns the Customer Email that appointment was create for>
-- Parameters:	
		--@@letterHistoryId int = 0
-- [AS_GetPropertyCustomerEmail] 20791	
-- =============================================

IF OBJECT_ID('dbo.AS_GetPropertyCustomerEmail') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetPropertyCustomerEmail AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_GetPropertyCustomerEmail]
( 
		@letterHistoryId int = 0
		
)
AS
BEGIN
SELECT 
AS_APPOINTMENTS.CustomerEmail

FROM AS_SAVEDLETTERS -- AS_JOURNALHISTORY 

INNER JOIN AS_StandardLetters on AS_StandardLetters.StandardLetterId = AS_SAVEDLETTERS.LETTERID
INNER JOIN AS_JOURNALHISTORY ON  AS_SAVEDLETTERS.JOURNALHISTORYID = AS_JOURNALHISTORY.JOURNALHISTORYID
Inner JOIN AS_APPOINTMENTS on AS_APPOINTMENTS.JOURNALHISTORYID = AS_JOURNALHISTORY.JOURNALHISTORYID
Where AS_SAVEDLETTERS.SAVEDLETTERID =@letterHistoryId

END

