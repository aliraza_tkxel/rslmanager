USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetsAssociatedFaultsList]    Script Date: 02/02/2016 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 3/1/2016
-- Description:	Display associated faults list
-- EXEC FL_GetsAssociatedFaultsList 527
-- =============================================

IF OBJECT_ID('dbo.FL_GetsAssociatedFaultsList') IS NULL 
 EXEC('CREATE PROCEDURE dbo.FL_GetsAssociatedFaultsList AS SET NOCOUNT ON;')
GO


ALTER PROCEDURE [dbo].[FL_GetsAssociatedFaultsList]
	@repairId int
	AS
BEGIN
	
	SET NOCOUNT ON;
SELECT  DISTINCT  fault.description as FaultDescription,
	 frl.Description as RepairDescription
	,fault.FaultId
	,far.repairid
FROM FL_FAULT_ASSOCIATED_REPAIR far  
     INNER JOIN FL_FAULT fault ON fault.faultid = far.faultid     
     INNER JOIN FL_FAULT_REPAIR_LIST frl on frl.FaultRepairListId = far.RepairId
WHERE
 far.repairid = @repairId
 End