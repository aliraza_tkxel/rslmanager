SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetNegativeTenancyBalanceCount]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @TotalCount int
declare @tablecount int
declare @countId int
select @TotalCount= count(*) from c_tenancy where c_tenancy.enddate is null and dbo.fn_get_tenant_balance(c_tenancy.tenancyid) < 0

select @tablecount= count(*) from AM_NegativeRentBalanceCount

if(@tablecount>0)

begin
	select top 1 @countId= AM_NegativeRentBalanceCount.CountId from AM_NegativeRentBalanceCount 
	update AM_NegativeRentBalanceCount set NegativeRentCount=@TotalCount where CountId=@countId
end
else
	begin
		insert into AM_NegativeRentBalanceCount (NegativeRentCount)values (@TotalCount)
	end

select NegativeRentCount from AM_NegativeRentBalanceCount where CountId = @countId

END

--exec AM_SP_GetNegativeTenancyBalanceCount





GO
