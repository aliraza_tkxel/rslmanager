SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_Customer_GasServicingAppointmentDate]
	/*	===============================================================
	'   NAME:           FL_Customer_GasServicingAppointmentDate
	'   DATE CREATED:   21 april 2009
	'   CREATED BY:     Ad Mirza
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To retrieve the Customer Gas Servicing Appointment Date to be displayed on welcome page of the application
	'   IN:             @TenancyId 
	'     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@CustomerId INT
	)
AS

DECLARE @TENANCYID INT

SELECT @TENANCYID=TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID=@CustomerId AND ENDDATE IS NOT NULL

SELECT APPOINTMENTDATE as GasServicingAppointmentDate
              FROM C_TENANCY CT  
                 LEFT JOIN ( 
                            SELECT MIN(DATEADD(MM,12,ISSUEDATE)) AS ISSUDATE,PROPERTYID 
                            FROM GS_PROPERTY_APPLIANCE 
                            GROUP BY PROPERTYID 
                            ) RN ON RN.PROPERTYID=CT.PROPERTYID 
                  LEFT JOIN ( 
			                  SELECT GJ.APPOINTMENTDATE,CJ.PROPERTYID,CJ.JOURNALID	
			                  FROM  C_REPAIRTOGASSERVICING GJ 
			              	   INNER JOIN C_REPAIR CR ON CR.REPAIRHISTORYID=GJ.REPAIRHISTORYID 
			           	   INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID 
			                  WHERE  GJ.REPAIRHISTORYID =(SELECT MAX( REPAIRHISTORYID) 
			                                              FROM C_REPAIR 
			                     						   WHERE JOURNALID=CJ.JOURNALID 
			                       						 ) 
 			                  ) AP ON AP.PROPERTYID=CT.PROPERTYID 
              WHERE CT.TENANCYID= @TENANCYID


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
