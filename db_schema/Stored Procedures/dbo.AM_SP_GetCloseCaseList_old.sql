SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Zunair Minhas
-- Create date: <Create Date,31 March 2010,>
-- Description:	<Description,,>
-- =============================================

create PROCEDURE [dbo].[AM_SP_GetCloseCaseList_old]
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle	varchar(100),
		@skipIndex	int = 0,
		@pageSize	int = 10,
        @surname    varchar(50),
		@sortBy     varchar(100),
        @sortDirection varchar(10)

AS
BEGIN


declare @orderbyClause varchar(50)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @RegionSuburbClause varchar(8000)
declare @WhereClause varchar(8000)



IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END


SET	@WhereClause = 'Where (AM_Case.CaseOfficer = (case when 0 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when 0 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME = case when '''' = '''+ @surname +''' then customer.LASTNAME else '''+ @surname +''' end) 
						  AND AM_Case.IsActive = 0 
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)'


SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
SET @query = 
'SELECT TOP('+convert(varchar(10),@pageSize)+')  
						Max(AM_Action.Title) AS ActionTitle, 
						Max(AM_Case.CaseId) as CaseId, 
						Max(AM_Status.Title) AS StatusTitle, 
						
						(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
							WHERE TenancyId = AM_Case.TENANCYID
							ORDER BY CustomerId ASC) as CustomerName,

					    (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
							WHERE TenancyId = AM_Case.TENANCYID
							ORDER BY CustomerId DESC) as CustomerName2,

					    (SELECT Count(DISTINCT CustomerId)
							FROM AM_Customer_Rent_Parameters
							WHERE	TenancyId = AM_Case.TENANCYID) as JointTenancyCount,						

                  		Max(customer.CustomerAddress) AS CustomerAddress, 
						Max(convert(varchar(10), AM_Case.IsPaymentPlan)) AS PaymentPlan, 
						AM_Case.TenancyId as TenancyId, 
						Max(convert(varchar(10), AM_Case.IsSuppressed)) as IsSuppressed,
						Max(ISNULL(convert(varchar(100),AM_Case.SuppressedDate, 103), '''')) as SuppressedDate, 
						Max(customer.CUSTOMERID) as CustomerId,
						Max(customer.RentBalance) AS RentBalance,
 				  		Max(customer.EstimatedHBDue) AS EstimatedHBDue,
						--Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
						Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,
						MAX(AM_Case.ModifiedDate) as ModifiedDate,
						convert(float, ISNULL((SELECT P_FINANCIAL.Totalrent 
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
													 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
													 WHERE C_TENANCY.TENANCYID = AM_Case.TenancyId), 0.00)) as totalRent

						FROM         AM_Action INNER JOIN
											  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
											  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
											  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
											  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
											  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                              INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID '
					+ @WhereClause + '   
					AND AM_Case.TenancyId  NOT IN ('
							SET @subQuery ='SELECT TenancyId FROM(
													SELECT TOP ('+convert(varchar(10),@skipIndex)+') 
																Max(AM_Action.Title) AS ActionTitle, 
															    Max(AM_Case.CaseId) as CaseId, 
																Max(AM_Status.Title) AS StatusTitle, 
																
																(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
																	FROM AM_Customer_Rent_Parameters
																	WHERE TenancyId = AM_Case.TENANCYID
																	ORDER BY CustomerId ASC) as CustomerName,

																(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
																	FROM AM_Customer_Rent_Parameters
																	WHERE TenancyId = AM_Case.TENANCYID
																	ORDER BY CustomerId DESC) as CustomerName2,

																(SELECT Count(DISTINCT CustomerId)
																	FROM AM_Customer_Rent_Parameters
																	WHERE	TenancyId = AM_Case.TENANCYID) as JointTenancyCount,

																Max(customer.CustomerAddress) AS CustomerAddress, 
																Max(convert(varchar(10), AM_Case.IsPaymentPlan)) AS PaymentPlan, 
																AM_Case.TenancyId as TenancyId, 
																Max(convert(varchar(10), AM_Case.IsSuppressed)) as IsSuppressed,
																Max(ISNULL(convert(varchar(100),AM_Case.SuppressedDate, 103), '''')) as SuppressedDate, 
																Max(customer.CUSTOMERID) as CustomerId,
																Max(customer.RentBalance) AS RentBalance,
																Max(customer.EstimatedHBDue) AS EstimatedHBDue,
																--Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
                                                                Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,
																MAX(AM_Case.ModifiedDate) as ModifiedDate,
																convert(float, ISNULL((SELECT P_FINANCIAL.Totalrent 
																							 FROM C_TENANCY INNER JOIN
																								  P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
																								  P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
																							 WHERE C_TENANCY.TENANCYID = AM_Case.TenancyId), 0.00)) as totalRent

													FROM         AM_Action INNER JOIN
																 AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
																 AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
																 AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
																 C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
																 P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
                                                                 INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID '
												+ @WhereClause +   
												' GROUP BY AM_Case.TenancyId 
												'  + @orderbyClause + ') as Temp
									) GROUP BY AM_Case.TenancyId 
									'
					--order by AM_Case.ModifiedDate ASC


print(@query + @subQuery + @orderbyClause);
exec(@query + @subQuery + @orderbyClause);	



END











GO
