SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[AM_SP_GetCaseListCount_old] 
		@postCode varchar(50)='',
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle  varchar(100),
        @surname varchar(50), 
		@IsNoticeExpiryCheck	bit,
        @overdue varchar(50)=''
AS
BEGIN
	

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

	IF(@caseOwnedBy = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId )+ ') ' 
	END
    ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN

		IF(@regionId = -1 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = '(P__PROPERTY.DEVELOPMENTID IN (SELECT DevelopmentId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true'' AND PatchId is null) OR P__PROPERTY.PATCH IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true'' AND DevelopmentId is null))'
		END
		ELSE IF(@regionId > 0 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
		END
		ELSE IF(@regionId > 0 and @suburbId > 0)
		BEGIN
			SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
		END
         ELSE IF(@regionId = -1 and @suburbId > 0)
			BEGIN
         		SET @RegionSuburbClause = 'P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) 
			END
END

	if(@IsNoticeExpiryCheck = 1)
	BEGIN
		
SET @query=
		  'SELECT COUNT(*) as recordCount FROM( SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
			Where (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  ' + @RegionSuburbClause + ' AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END
						  AND (customer.LASTNAME = case when '''' = '''+ @surname +''' then customer.LASTNAME else '''+ @surname +''' end) 
						  AND AM_Case.IsActive = 1 
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN(select CaseId
												FROM AM_CASE
												WHERE NoticeExpiryDate IS NOT NULL 
														AND IsActive = ''True'' 
														AND dbo.AM_FN_Check_Case_Notice_Expiry_Date(AM_Case.NoticeExpiryDate) = ''True'')
			GROUP BY AM_Case.TenancyId) as TEMP'

	END
    ELSE IF(@overdue='Actions')
    BEGIN
    SET @query=
		  'SELECT COUNT(*) as recordCount FROM( SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
                      
					Where (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME = case when '''' = '''+ @surname +''' then customer.LASTNAME else '''+ @surname +''' end) 
						  AND AM_Case.IsActive = 1 
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
													FROM AM_CaseHistory INNER JOIN
														 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
														 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
														 AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								                         E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								                         AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

			                                                     WHERE AM_CaseHistory.IsActive = 1 and (AM_CaseHistory.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_CaseHistory.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END))
				                                                    	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = ''true'' 
				                                                    	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId)) 
								GROUP BY AM_Case.TenancyId) as TEMP'

    
    
    END
    
	ELSE IF(@overdue='Stages')
    BEGIN
    SET @query=
		  'SELECT COUNT(*) as recordCount FROM (SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
                      
					Where (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  ' + @RegionSuburbClause + ' 
						  AND (customer.LASTNAME = case when '''' = '''+ @surname +''' then customer.LASTNAME else '''+ @surname +''' end) 
						  AND AM_Case.IsActive = 1 
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
						  AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
													FROM AM_CaseHistory INNER JOIN
														 AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
														 AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
														 AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								                         E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								                         C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								                         C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								                         AM_LookupCode ON AM_Status.NextStatusAlertFrequencyLookupCodeId = AM_LookupCode.LookupCodeId

			                                                     WHERE AM_CaseHistory.IsActive = 1 and (AM_CaseHistory.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_CaseHistory.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END))
				                                                    	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Status.NextStatusAlert, AM_LookupCode.CodeName, AM_CaseHistory.StatusReview ) = ''true'' 
				                                                    	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId))
                              GROUP BY AM_Case.TenancyId) as TEMP'
    END
	
	ELSE
	BEGIN
SET @query=
		  'SELECT COUNT(*) as recordCount FROM(
			SELECT COUNT(*) as recordCount

		  FROM  AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                      INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
			Where (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  ' + @RegionSuburbClause + ' AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END
						  AND (customer.LASTNAME = case when '''' = '''+ @surname +''' then customer.LASTNAME else '''+ @surname +''' end) 
						  AND AM_Case.IsActive = 1 
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
			GROUP BY AM_Case.TenancyId) as TEMP'

	END	  
PRINT(@query);
EXEC(@query);

END
















GO
