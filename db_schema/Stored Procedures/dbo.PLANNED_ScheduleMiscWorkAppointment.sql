USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_ScheduleMiscWorkAppointment]    Script Date: 12/19/2016 3:29:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[PLANNED_ScheduleMiscWorkAppointment]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_ScheduleMiscWorkAppointment] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_ScheduleMiscWorkAppointment]    
 -- Add the parameters for the stored procedure here    
@PropertyId varchar(20) 
,@schemeId int
,@blockId int   
,@userId int    
,@customerNotes varchar(1000)    
,@appointmentNotes varchar(1000)    
,@tenancyId int    
,@appointmentDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int    
,@tradeId int    
,@parameterId int    
,@parameterValueId int    
,@appointmentTypeId INT    
,@PMO INT    
,@duration INT    
,@fromScheduling bit
,@isSaved int = 0 out    
,@journalIdOut int = -1 out    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE     
    
@ArrangedId int    
,@JournalId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
    
    
    
SELECT  @ArrangedId = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='Arranged'    
    
BEGIN TRANSACTION;    
BEGIN TRY    
-- =============================================    
-- get status history id of "Arranged"    
-- =============================================    
  Declare @ArrangedHistoryId int    
  SELECT @ArrangedHistoryId= MAX(StatusHistoryId) FROM PLANNED_StatusHistory WHERE StatusId =@ArrangedId    

-- =============================================    
-- get scheme in case of blocks  
-- =============================================
  if(@blockId is not null)
  BEGIN 
	SELECT @schemeId=SchemeId from P_BLOCK where BLOCKID = @blockId
  end 
  else if(@schemeId is not null)
  BEGIN
	SET @blockId = 0
  END
-- ====================================================================================================    
--          INSERTION (PLANNED_JOURNAL)    
-- ====================================================================================================    
     
 -- This script block has been disabled as PMO/JournalId is now comming from front end, from PMOs     
 -- dropdown on misc work screen.    
 IF @PMO = -1     
 BEGIN    
  INSERT PLANNED_JOURNAL    
   ([PROPERTYID]    
           ,[COMPONENTID]    
           ,[STATUSID]    
           ,[ACTIONID]    
           ,[CREATIONDATE]    
           ,[CREATEDBY]
	   ,[AppointmentTypeId]
	   ,[SchemeId]
	   ,[BlockId]   )     
  VALUES  (@PropertyId,null,@ArrangedId,null,GETDATE(),@userId,@appointmentTypeId,@schemeId,@blockId)    
  SELECT @JournalId = SCOPE_IDENTITY()    
 END    
 ELSE      
  SET @JournalId = @PMO    
     
 PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@JournalId )    
      
      
-- ====================================================================================================    
--          INSERTION (PLANNED_JOURNAL_HISTORY)    
-- ====================================================================================================    
 IF @PMO = -1     
 BEGIN      
  INSERT PLANNED_JOURNAL_HISTORY    
  ([JOURNALID]    
           ,[PROPERTYID]    
           ,[COMPONENTID]    
           ,[STATUSID]    
           ,[ACTIONID]    
           ,[CREATIONDATE]    
           ,[CREATEDBY]    
           ,[NOTES]    
           ,[ISLETTERATTACHED]    
           ,[StatusHistoryId]    
           ,[ActionHistoryId]    
           ,[IsDocumentAttached]
		   ,[SchemeId]
		   ,[BlockId]) VALUES    
   (@JournalId,@PropertyId,NULL,@ArrangedId,null,GETDATE(),@userId,NULL,0,@ArrangedHistoryId,NULL,0,@schemeId,@blockId)    
       
  SELECT @JournalHistoryId = SCOPE_IDENTITY()    
 END    
 ELSE    
 BEGIN      
  SELECT @JournalHistoryId = MAX(JOURNALHISTORYID)    
  FROM PLANNED_JOURNAL_HISTORY    
  WHERE JOURNALID = @JournalId    
 END    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)    
    
-- ====================================================================================================    
--          INSERTION (PLANNED_APPOINTMENTS)    
-- ====================================================================================================    
    
    
INSERT INTO PLANNED_APPOINTMENTS    
           ([TENANCYID]    
           ,[JournalId]    
           ,[JOURNALHISTORYID]    
         ,[APPOINTMENTDATE]    
           ,[APPOINTMENTSTARTTIME]    
           ,[APPOINTMENTENDTIME]    
           ,[ASSIGNEDTO]    
           ,[CREATEDBY]    
           ,[LOGGEDDATE]    
           ,[APPOINTMENTNOTES]    
           ,[CUSTOMERNOTES]             ,[ISPENDING]    
           ,[JOURNALSUBSTATUS]    
           ,[APPOINTMENTALERT]    
           ,[APPOINTMENTCALENDER]    
           ,[SURVEYOURSTATUS]    
           ,[APPOINTMENTSTATUS]    
           ,[SURVEYTYPE]    
           ,[COMPTRADEID]    
           ,[isMiscAppointment]    
           ,[APPOINTMENTENDDATE]    
           ,[Planned_Appointment_TypeId]
		   ,[MiscTrade])    
     VALUES    
           (@tenancyId    
           ,@JournalId    
           ,@JournalHistoryId    
           ,@appointmentDate    
           ,@startTime    
           ,@endTime    
           ,@operativeId    
           ,@userId    
           ,GETDATE()    
           ,@appointmentNotes    
           ,@customerNotes    
           ,1    
           ,NULL    
           ,NULL    
           ,NULL    
           ,NULL    
           ,'NotStarted'    
           ,NULL    
           ,NULL    
           ,1    
           ,@appointmentEndDate    
           ,@appointmentTypeId,
		   @tradeId)    
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
       

	If (@fromScheduling = 0)
		Begin
			update PLANNED_APPOINTMENTS 
			set ISPENDING = 0
			where APPOINTMENTID = @AppointmentId 
		End

-- ====================================================================================================    
--          INSERTION (PLANNED_MISC_TRADES)    
-- ====================================================================================================    
   IF @parameterId = -1 or @parameterId=0 
   BEGIN
   SET @parameterId = NULL
   END 
       
   IF @parameterValueId = -1 or @parameterValueId=0 
   BEGIN
   SET @parameterValueId = NULL
   END     
   INSERT INTO PLANNED_MISC_TRADE    
           ([AppointmentId]    
           ,[TradeId]    
           ,[Duration]    
           ,[ParameterId]    
           ,[ParameterValueId])    
     VALUES    
           (@AppointmentId    
           ,@tradeId    
           ,@duration    
           ,@parameterId    
           ,@parameterValueId
		   )       
     UPDATE PLANNED_MISC_TRADE
	 SET APPOINTMENTNOTES=@customerNotes , CUSTOMERNOTES= @appointmentNotes
	 where JournalId=@PMO AND TradeId=@tradeId
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
    
    
SET @journalIdOut = @JournalId    
    
    
END