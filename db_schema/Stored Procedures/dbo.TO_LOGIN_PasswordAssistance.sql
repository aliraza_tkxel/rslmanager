SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.[TO_LOGIN_PasswordAssistance]
	
	/*	===============================================================
	'   NAME:           TO_LOGIN_PASSWORD_ASSISTANCE
	'   DATE CREATED:   26 MAY 2008
	'   CREATED BY:     Munawar Nadeem
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        Customer Password is returned conditioned to email gets verified
	'   SELECT:         TO_Login.Password
	'   IN:             @email
	'   OUT: 	        Nothing    
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/


    @email varchar(50)

    
AS

	SET NOCOUNT ON

	
	SELECT     
	       Login.Password
	       
    FROM
           C__CUSTOMER AS Customer INNER JOIN C_ADDRESS AS CustomerAddress
           ON Customer.CUSTOMERID = CustomerAddress.CUSTOMERID
           
           INNER JOIN TO_LOGIN AS Login 
           ON Customer.CUSTOMERID = Login.CustomerID
           
WHERE     
          (CustomerAddress.EMAIL = @email) AND (CustomerAddress.ISDEFAULT = 1)
          AND (Login.Active = 1)




GO
