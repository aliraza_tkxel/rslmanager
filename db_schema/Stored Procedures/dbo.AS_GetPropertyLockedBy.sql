USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- =============================================
IF OBJECT_ID('dbo.[AS_GetPropertyLockedBy]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetPropertyLockedBy] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].AS_GetPropertyLockedBy
(@pId varchar(100),
@appointmentType varchar(100),
@LockedUser int out )  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @appointmentType = 'Property'
	begin
		SELECT @LockedUser = AS_JOURNAL.LockedBy
		FROM AS_JOURNAL
		WHERE AS_JOURNAL.PropertyId=@pId
		AND AS_JOURNAL.ISCURRENT=1
		AND (STATUSID=1 OR STATUSID=2 OR STATUSID=3 OR STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
	end
	else if @appointmentType = 'Scheme'
	begin
		SELECT @LockedUser = AS_JOURNAL.LockedBy
		FROM AS_JOURNAL
			WHERE AS_JOURNAL.SchemeID=@pId
			AND AS_JOURNAL.ISCURRENT=1
		AND AS_JOURNAL.ISCURRENT=1
		AND (STATUSID=1 OR STATUSID=2 OR STATUSID=3 OR STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
	end
	else if @appointmentType = 'Block'
	begin
		SELECT @LockedUser = AS_JOURNAL.LockedBy
		FROM AS_JOURNAL
			WHERE AS_JOURNAL.BlockId=@pId
		AND AS_JOURNAL.ISCURRENT=1
		AND (STATUSID=1 OR STATUSID=2 OR STATUSID=3 OR STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
	end
END
