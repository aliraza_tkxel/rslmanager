SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_GET_FAULT_REPAIR_LIST]
/* ===========================================================================
 '   NAME:           FL_GET_FAULT_REPAIR_LIST
 '   DATE CREATED:   28 Oct, 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve fault repair list
 
 '   IN:             @Description
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
	@Description VARCHAR(100),
	@SCI INT
)


AS
declare @abc varchar(1000)
IF @SCI = 2 
	BEGIN 
	SELECT *,  case convert(varchar(10),PostInspection) when 1 then 'Yes' when 0 then 'No' end as PostInspect,			   
			   case convert(varchar(10),StockConditionItem) when 1 then 'Yes' when 0 then 'No' end as StockCondition,
			   case convert(varchar(10),RepairActive) when 1 then 'Yes' when 0 then 'No' end as IsActive
			   FROM FL_FAULT_REPAIR_LIST AS FL 		
		
	WHERE Description LIKE '%'+@Description+'%' 
	
	ORDER BY FaultRepairListID DESC
	END
ELSE IF @SCI = 0 OR @SCI = 1
	BEGIN

	SELECT *,  case convert(varchar(10),PostInspection) when 1 then 'Yes' when 0 then 'No' end as PostInspect,			   
			   case convert(varchar(10),StockConditionItem) when 1 then 'Yes' when 0 then 'No' end as StockCondition,
			   case convert(varchar(10),RepairActive) when 1 then 'Yes' when 0 then 'No' end as IsActive
			   FROM FL_FAULT_REPAIR_LIST AS FL 		
		
	WHERE Description LIKE '%'+@Description+'%' AND StockConditionItem = @SCI
	
	ORDER BY FaultRepairListID DESC
	END



/****** Object:  StoredProcedure [dbo].[FL_TBL_ALL_AREAS_GETLOOKUP]    Script Date: 08/25/2009 12:23:20 ******/
SET ANSI_NULLS ON
GO
