
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Ali Raza>
-- Create date: <Create Date,03/12/2013>
-- Description:	<Description,Update Property Images Path>
-- =============================================
CREATE PROCEDURE [dbo].[AS_UtilitlyUpdatePropertyImagesPath_UpdateData]
	-- Add the parameters for the stored procedure here
	@UpdateData as AS_UtilitlyUpdatePropertyImagesPath readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 DECLARE @sid INT , @propertyId nvarchar(100),@type nvarchar(100),@imagePath nvarchar(300),@imageName nvarchar(300)
    DECLARE @ItemToInsertCursor CURSOR
     --Initialize cursor
Set @ItemToInsertCursor = CURSOR FAST_FORWARD FOR SELECT [SId] , PropertyId,[Type],ImagePath,ImageName  FROM  @UpdateData;
   --Open cursor
   OPEN @ItemToInsertCursor
---fetch row from cursor
   FETCH NEXT FROM @ItemToInsertCursor INTO @sid, @propertyId,@type, @imagePath, @imageName  
   
    ---Iterate cursor to get record row by row
		WHILE @@FETCH_STATUS = 0
		BEGIN
		---Check images for property
			IF @type='gas'
				BEGIN
					 Update PA_PROPERTY_ITEM_IMAGES SET ImagePath= @imagePath, ImageName = @imageName
					 Where SID = @sid 
				END
		--- Check images for survey
			IF @type='stock'
				BEGIN
					 Update PS_Survey_Item_Images SET ImagePath= @imagePath, ImageName = @imageName
					 Where ImagesId= @sid 
				END
		---Check images for defect
			IF @type='defect'
				BEGIN
					 Update P_PROPERTY_APPLIANCE_DEFECTS_IMAGES SET ImagePath= @imagePath, ImageTitle= @imageName
					 Where PropertyDefectImageId= @sid 
				END
		 FETCH NEXT FROM @ItemToInsertCursor INTO @sid, @propertyId,@type, @imagePath, @imageName  
		END
--close & deallocate cursor		
CLOSE @ItemToInsertCursor
DEALLOCATE @ItemToInsertCursor
 
   
   
   
END
GO
