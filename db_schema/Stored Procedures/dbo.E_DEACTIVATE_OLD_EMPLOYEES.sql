SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jimmy Millican
-- Create date: 17th july 2007
-- Description:	employees who's end date + notice period has hit 
--				need to be de-activated
-- =============================================
CREATE PROCEDURE [dbo].[E_DEACTIVATE_OLD_EMPLOYEES]
AS
BEGIN

	update ac_logins set active = 0
	--select *
	from	e_jobdetails j
			left join ac_logins l on l.employeeid = j.employeeid
	where	j.enddate is not null
			and l.active = 1
			and dateadd(d,isnull(j.noticeperiod,0),j.enddate) <= getdate()

	update e_jobdetails set active = 0
	where	enddate is not null
			and active = 1
			and dateadd(d,isnull(noticeperiod,0),enddate) <= getdate()
		

END
GO
