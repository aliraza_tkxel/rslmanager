USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_UpdateStandardLetterTemplate]    Script Date: 07/05/2018 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[AS_UpdateStandardLetterTemplate]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_UpdateStandardLetterTemplate] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_UpdateStandardLetterTemplate]
	-- Add the parameters for the stored procedure here
	@LetterId					int,
	@StatusId					int,
	@ActionId					int,
	@Title						varchar(50),
	@Code						varchar(50),
	@Body						varchar(MAX),
	@ModifiedBy					int,
	@isAlternativeServicing		bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    UPDATE 
		AS_StandardLetters   
	SET 
		StatusId				=	@StatusId,
		ActionId				=	@ActionId,
		Title					=	@Title,
		Code					=	@Code,
		Body					=	@Body,
		ModifiedBy				=	@ModifiedBy,
		ModifiedDate			=	GETDATE(),
		IsAlternativeServicing	= @isAlternativeServicing
	Where 
		StandardLetterId = @LetterId 
   
END
