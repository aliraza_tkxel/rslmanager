USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_AmendAttributeItemDetail]    Script Date: 08/02/2017 15:36:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

   /* =================================================================================    
    Page Description:    save the detail of tree leaf node controls

    Author: Ali Raza
    Creation Date: jan-14-2015  

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         jan-14-2015      Ali Raza            save the detail of tree leaf node controls


  =================================================================================*/
  IF OBJECT_ID('dbo.PDR_AmendAttributeItemDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_AmendAttributeItemDetail AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_AmendAttributeItemDetail]   
  -- Add the parameters for the stored procedure here          
  @schemeId		 INT=null, 
  @blockId		 INT=null,  
  @ItemId        INT,   
  @UpdatedBy     INT, 
  @ContractorId	 INT=NULL,
  @Commencment	 SmallDateTime=NULL,
  @ContractPeriod	INT= NULL,
  @ContractPeriodType INT= NULL,
  @Cycle  int=null,
  @CycleType int=null,
  @CycleValue Money=null,
  @HeatingMappingId INT=NULL,
  @BFSCarryingOutWork bit =0,
  @AttributeName nvarchar(100)=null,
  @ChildAttributeMappingId int =null,
  @VAT nvarchar(100) =null,
  @TotalValue int =null,
  @CustomCycleFrequency int =null,
  @CustomCycleOccurance nvarchar(500)=null,
  @CustomCycleType nvarchar(20) =null,
  @ItemDetail    AS AS_ITEMDETAILS readonly,   
  @ItemDates     AS AS_ITEMDATES readonly,   
  @ConditionWork AS AS_CONDITIONRATINGLIST readonly ,
  @MSATDetail AS AS_MSATDETAIL readonly,   
  @outputChildAttributeMappingId int =-1 output
AS   
  BEGIN   
      -- SET NOCOUNT ON added to prevent extra result sets from          
      -- interfering with SELECT statements.          
      SET nocount ON;  
	   
DECLARE @journalId int, @journalHistoryId int, @HeatingType nvarchar(100), @MainsGasCount int, @AlternativeCount int, @ApplianceInspectionTypeId int , @IsAlterNativeHeating bit

IF	@blockId = 0
		SET @blockId =NULL
	IF	@schemeId = 0
		SET @schemeId =NULL

SET @outputChildAttributeMappingId = @ChildAttributeMappingId
IF (@AttributeName is not null AND (@ChildAttributeMappingId <= 0 OR @ChildAttributeMappingId IS NULL))
BEGIN
	INSERT INTO PA_ChildAttributeMapping (ItemId, AttributeName, SchemeId,BlockId,IsActive)  
	VALUES (@ItemId, @AttributeName, @schemeId,@blockId,1);
	SET @ChildAttributeMappingId = Scope_identity()   
	SET @outputChildAttributeMappingId = @ChildAttributeMappingId
END

		
IF @HeatingMappingId IS NOT NULL 
BEGIN      

	IF @ItemId = (SELECT PA_ITEM.ItemID 
				  FROM PA_ITEM 
				  WHERE PA_ITEM.ItemName='Boiler Room')
		BEGIN
	
			IF @HeatingMappingId = -1 OR @HeatingMappingId IS NULL
			BEGIN
				
				INSERT INTO PA_HeatingMapping(SchemeID,BlockID,HeatingType,IsActive)
				Values	(@schemeId,@blockId,(SELECT top 1 valueid FROM @ItemDetail),1)
				SET	@HeatingMappingId = Scope_identity();	


				--===============================================================================================
				-- Fetch Heating type description and check Alternative Heating status
				-- If Mains Gas already exists in certain scheme/block them same journalid will be utilized
				-- If Alternative Heating already exists in certain scheme/block them same journalid will be utilized
				--===============================================================================================

				SELECT	@HeatingType =  ValueDetail, @IsAlterNativeHeating = IsAlterNativeHeating
				FROM	PA_HeatingMapping
						INNER JOIN PA_PARAMETER_VALUE ON PA_HeatingMapping.HeatingType = PA_PARAMETER_VALUE.ValueID
				WHERE   HeatingMappingId = @HeatingMappingId

				SELECT		@MainsGasCount = count(*)
				FROM		AS_journalHeatingMapping
							INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
							INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
							INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
				WHERE		ValueDetail = 'Mains Gas' 
							AND AS_JOURNAL.ISCURRENT = 1 
							AND ((AS_JOURNAL.SchemeId=@schemeId AND @blockId is NULL) 
								OR (AS_JOURNAL.BlockId=@blockId and @schemeId IS NULL ))


				SELECT		@AlternativeCount = count(*)
				FROM		AS_journalHeatingMapping
							INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
							INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
							INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
				WHERE		IsAlterNativeHeating = 1 
							AND AS_JOURNAL.ISCURRENT = 1 
							AND ((AS_JOURNAL.SchemeId=@schemeId AND @blockId is NULL) 
								OR (AS_JOURNAL.BlockId=@blockId and @schemeId IS NULL ))



				IF @MainsGasCount > 0 AND @HeatingType = 'Mains Gas'
				BEGIN

					SELECT		@journalId = AS_journalHeatingMapping.JOURNALID, @journalHistoryId = AS_journalHeatingMapping.JournalHistoryId
					FROM		AS_journalHeatingMapping
								INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
								INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
								INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
					WHERE		ValueDetail = 'Mains Gas' 
								AND AS_JOURNAL.ISCURRENT = 1 
								AND ((AS_JOURNAL.SchemeId=@schemeId AND @blockId is NULL) 
									OR (AS_JOURNAL.BlockId=@blockId and @schemeId IS NULL ))
				END
				ELSE IF @AlternativeCount > 0 AND @IsAlterNativeHeating = 1
				BEGIN
					
					SELECT		@journalId = AS_journalHeatingMapping.JOURNALID, @journalHistoryId = AS_journalHeatingMapping.JournalHistoryId
					FROM		AS_journalHeatingMapping
								INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
								INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
								INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
					WHERE		IsAlterNativeHeating = 1 
								AND AS_JOURNAL.ISCURRENT = 1 
								AND ((AS_JOURNAL.SchemeId=@schemeId AND @blockId is NULL) 
									OR (AS_JOURNAL.BlockId=@blockId and @schemeId IS NULL ))
				END
				ELSE
				BEGIN


					IF EXISTS(  SELECT	1
								FROM	AS_JOURNAL AJ
										LEFT JOIN AS_JournalHeatingMapping AJHM ON AJ.JournalId = AJHM.JournalId
								WHERE	ISCURRENT = 1 AND ((AJ.SchemeId=@schemeId AND @blockId is NULL) 
										OR (AJ.BlockId=@blockId and @schemeId IS NULL )) AND AJHM.HeatingJournalMappingId IS NULL)
					BEGIN

						  SELECT	@journalId = AJ.JOURNALID
						  FROM		AS_JOURNAL AJ
									LEFT JOIN AS_JournalHeatingMapping AJHM ON AJ.JournalId = AJHM.JournalId
						  WHERE		ISCURRENT = 1 
									AND ((AJ.SchemeId=@schemeId AND @blockId is NULL) 
										OR (AJ.BlockId=@blockId and @schemeId IS NULL )) 
									AND AJHM.HeatingJournalMappingId IS NULL

						  UPDATE	AS_JOURNAL
						  SET		ISCURRENT = 0
						  WHERE	    JOURNALID = @journalId

					END

			
					SELECT	@ApplianceInspectionTypeId = InspectionTypeID
					FROM	P_INSPECTIONTYPE 
					WHERE	[Description] = 'Appliance Servicing'
					
					INSERT INTO AS_JOURNAL(STATUSID,ACTIONID,INSPECTIONTYPEID,ServicingTypeId,CREATIONDATE,CREATEDBY,ISCURRENT,SchemeID,BlockID)
					VALUES (1,1,@ApplianceInspectionTypeId,dbo.GetServicingTypeByHeating(@HeatingMappingId),GETDATE(),@UpdatedBy,1,@schemeId,@blockId)	
					Set @journalId = Scope_identity();
					
					INSERT INTO AS_JOURNALHISTORY(JOURNALID,STATUSID,ACTIONID,INSPECTIONTYPEID,ServicingTypeId,CREATIONDATE,CREATEDBY,SchemeID,BlockID)
					VALUES (@journalId,1,1,@ApplianceInspectionTypeId,dbo.GetServicingTypeByHeating(@HeatingMappingId),GETDATE(),@UpdatedBy,@schemeId,@blockId)	
					Set @journalHistoryId = Scope_identity();	

				END

				IF EXISTS(SELECT  1
					FROM	AS_JOURNAL
					WHERE	JOURNALID = @journalId AND ServicingTypeId IS NULL)
					BEGIN
						UPDATE	AS_JOURNAL
						SET		ServicingTypeId = dbo.GetServicingTypeByHeating(@HeatingMappingId)
						WHERE	JOURNALID = @journalId
					END

				IF EXISTS(SELECT  1
					FROM	AS_JOURNALHISTORY
					WHERE	JOURNALHISTORYID = @journalHistoryId AND ServicingTypeId IS NULL)
					BEGIN
						UPDATE	AS_JOURNALHISTORY
						SET		ServicingTypeId = dbo.GetServicingTypeByHeating(@HeatingMappingId)
						WHERE	JOURNALHISTORYID = @journalHistoryId
					END
										
				INSERT INTO AS_JournalHeatingMapping(JournalId,HeatingMappingId,JournalHistoryId,Createdon)
				VALUES(@journalId,@HeatingMappingId,@journalHistoryId,GETDATE())

			END
		END
END		
      ---Begin Iterate  @ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES        
      --=================================================================================      
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES          
      DECLARE @minItemParamId     INT,   
              @attributeId        INT,   
              @itemParamId        INT,   
              @parameterValue     NVARCHAR(1000),   
              @valueId            INT,   
              @isCheckBoxSelected BIT,   
              @Count              INT,   
              @ControlType        NVARCHAR(50),   
              @ParameterName      NVARCHAR(50)   
      ---Declare Cursor variable          
      DECLARE @ItemToInsertCursor CURSOR   

      --Initialize cursor          
      SET @ItemToInsertCursor = CURSOR fast_forward   
      FOR SELECT itemparamid,   
                 parametervalue,   
                 valueid,   
                 ischeckboxselected   
          FROM   @ItemDetail;   

      --Open cursor          
      OPEN @ItemToInsertCursor   

      ---fetch row from cursor          
      FETCH next FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue,   
      @valueId, @isCheckBoxSelected   

      ---Iterate cursor to get record row by row          
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
            --- Select Control type  of parameter            
            SELECT @ControlType = controltype,   
                   @ParameterName = parametername   
            FROM   pa_parameter   
                   INNER JOIN pa_item_parameter   
                           ON pa_item_parameter.parameterid =   
                              pa_parameter.parameterid   
            WHERE  itemparamid = @itemParamId   

            IF @ControlType = 'TextBox'   
                OR @ControlType = 'TextArea'   
              BEGIN   
                  SET @valueId=NULL   

                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId IS NULL    
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)  
                         AND itemparamid = @itemParamId   
                         AND VALUEID IS NULL  
                          AND PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId 
              END   
            ELSE IF @ControlType = 'CheckBoxes'   
              BEGIN   
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId =@valueId    
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)   
                         AND itemparamid = @itemParamId   
                         AND valueid = @valueId
                          AND PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId   
              END   
            ELSE   
              BEGIN   
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId     
                  SELECT @attributeId = attributeid   
                  FROM   pa_property_attributes   
                  WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)   
                         AND itemparamid = @itemParamId   
                          AND PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId
              END   

            -- if record exist update that row otherwise insert a new record             
            IF @attributeId > 0   
              BEGIN   

                  UPDATE pa_property_attributes   
                  SET    itemparamid = @itemParamId,   
                         parametervalue = @parameterValue,   
                         valueid = @valueId,   
                         updatedon = Getdate(),   
                         updatedby = @UpdatedBy,   
                         ischeckboxselected = @isCheckBoxSelected ,
                         HeatingMappingId=@HeatingMappingId    
                  WHERE  attributeid = @attributeId   
              END   
            ELSE   
              BEGIN   
                  DECLARE @latestAttributeId INT   

                  INSERT INTO pa_property_attributes   
                              (propertyid,   
                               itemparamid,   
                               parametervalue,   
                               valueid,   
                               updatedon,   
                               updatedby,   
                               ischeckboxselected,   
                               SchemeId,
                               BlockId,HeatingMappingId )   
                  VALUES      (NULL,   
                               @itemParamId,   
                               @parameterValue,   
                               @valueId,   
                               Getdate(),   
                               @UpdatedBy,   
                               @isCheckBoxSelected,
                               @schemeId,
                               @blockId ,@HeatingMappingId )   

                  SET @latestAttributeId = Scope_identity()   


              END   

            FETCH next FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue,@valueId, @isCheckBoxSelected   
        END   

      --close & deallocate cursor            
      CLOSE @ItemToInsertCursor   

      DEALLOCATE @ItemToInsertCursor   

      --Strart Iterate ItemDates to insert or update data in           
      --=====================================================================================          
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES          
      DECLARE @sid         INT,   
              @parameterId INT,   
              @lastDone    DATETIME,   
              @dueDate     DATETIME,   
              @componentId INT,   
              @psid        INT   


      ---Declare Cursor variable          
      DECLARE @ItemToInsertDateCursor CURSOR   

      --Initialize cursor          
      SET @ItemToInsertDateCursor = CURSOR fast_forward   
      FOR SELECT ParameterId,   
                  convert( Datetime, LastDone, 103 )as LastDone,   
                 convert( Datetime,DueDate, 103 )as DueDate,   
                 ComponentId   
          FROM   @ItemDates;   

      --Open cursor          
      OPEN @ItemToInsertDateCursor   

      ---fetch row from cursor          
      FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone,   
      @dueDate, @componentId   

      ---Iterate cursor to get record row by row          
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
           IF @lastDone = '1900-01-01 00:00:00.000'   
               OR @lastDone IS NULL   
              SET @lastDone=NULL   

            IF @dueDate = '1900-01-01 00:00:00.000'   
               OR @dueDate IS NULL   
              SET @dueDate=NULL   

            SELECT @sid = [sid]   
            FROM   pa_property_item_dates   
            WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)     
                   AND itemid = @ItemId   
                   AND  parameterid = @parameterId  
                   AND PA_PROPERTY_ITEM_DATES.HeatingMappingId=@HeatingMappingId  

            SELECT @psid = [sid]   
            FROM   pa_property_item_dates   
            WHERE  (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL)     
                   AND itemid = @ItemId   
                   AND ( parameterid = 0   
                          OR parameterid IS NULL )   
                   AND ( lastdone IS NOT NULL   
                   AND duedate IS NOT NULL ) 
                   AND PA_PROPERTY_ITEM_DATES.HeatingMappingId=@HeatingMappingId 

            IF @psid > 0 And ( @parameterId = 0 OR @parameterId IS NULL )   
              BEGIN   

                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,   
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy  ,
                               HeatingMappingId= @HeatingMappingId
                        WHERE  [sid] = @psid   

              END   
            ELSE IF @sid > 0   
              BEGIN   
                  IF @lastDone IS NOT NULL   
                     AND @dueDate IS NOT NULL   
                    BEGIN   
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,   
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy,
                               HeatingMappingId= @HeatingMappingId                                  
                        WHERE  [sid] = @sid   
                    END   
                  ELSE IF @dueDate IS NOT NULL   
                    BEGIN   
                        UPDATE pa_property_item_dates   
                        SET    lastdone = @lastDone,    
                               duedate = @dueDate,    
                               updatedon = Getdate(),   
                               updatedby = @UpdatedBy  ,
								HeatingMappingId= @HeatingMappingId
                        WHERE  [sid] = @sid   
                    END   
              END   
            ELSE   
              BEGIN   
                  IF @lastDone IS NOT NULL   
                     AND @dueDate IS NOT NULL   
                    BEGIN   
                        INSERT INTO pa_property_item_dates   
                                    (propertyid,   
                                     itemid,   
                                     lastdone,   
                                     duedate,   
                                     updatedon,   
                                     updatedby,   
                                     parameterid,   
                                     SchemeId,
                                     BlockId,HeatingMappingId )   
                        VALUES      ( NULL,   
                                      @ItemId,   
                                      @lastDone,   
                                      @dueDate,   
                                      Getdate(),   
                                      @UpdatedBy,   
                                      @parameterId,   
                                      @schemeId,
                                      @blockId,@HeatingMappingId)   
                    END   
                  ELSE IF @dueDate IS NOT NULL   
                    BEGIN   
                        INSERT INTO pa_property_item_dates   
                                    (propertyid,   
                                     itemid,   
                                     duedate,   
                                     updatedon,   
                                     updatedby,   
                                     parameterid,
                                     SchemeId,
                                     BlockId,HeatingMappingId   
                                     )   
                        VALUES      ( NULL,   
                                      @ItemId,   
                                       @dueDate,    
                                      Getdate(),   
                                      @UpdatedBy,   
                                      @parameterId,
                                      @schemeId,
                                      @blockId ,@HeatingMappingId  
                                      )   
                    END   
              END   

            FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone,   
            @dueDate, @componentId   
        END   

      --close & deallocate cursor            
      CLOSE @ItemToInsertDateCursor   

      DEALLOCATE @ItemToInsertDateCursor 

---///////////////////////////////////////////////////////////////////////////////////
IF @ContractorId  >= 0
BEGIN
DECLARE @statusId int = 0;
 IF EXISTS(Select CM_ServiceItems.StatusId from CM_ServiceItems where ItemId=@ItemId AND (SchemeId= @schemeId Or SchemeId is null) 
 And (BlockId=@blockId or (BlockId is null And @blockId is null)) And  
  ( (ChildAttributeMappingId=@ChildAttributeMappingId AND @ChildAttributeMappingId > 0)   OR ChildAttributeMappingId IS NULL))
	BEGIN
	Select @statusId = CM_ServiceItems.StatusId from CM_ServiceItems where ItemId=@ItemId AND (SchemeId= @schemeId Or SchemeId is null) And (BlockId=@blockId or (BlockId is null And @blockId is null))
	if @statusId =1 OR @statusId= 4 OR @statusId=5  OR @statusId=6  OR @statusId=7 
		BEGIN
			 Update CM_ServiceItems set ContractorId =@ContractorId,ContractCommencement=@Commencment, ContractPeriod=@ContractPeriod,StatusId=1,
			 ContractPeriodType=@ContractPeriodType,Cycle=@Cycle,CycleType=@CycleType,CycleValue=@CycleValue,CreatedBy = @UpdatedBy,CreatedDate=getdate(),BFSCarryingOutWork=@BFSCarryingOutWork,ChildAttributeMappingId=@ChildAttributeMappingId,VAT=@VAT,TotalValue=@TotalValue,CustomCycleFrequency =@CustomCycleFrequency,CustomCycleOccurance =@CustomCycleOccurance,CustomCycleType =@CustomCycleType
			 WHERE ItemId=@ItemId AND (SchemeId= @schemeId Or SchemeId is null) And (BlockId=@blockId or (BlockId is null And @blockId is null))And  
  ( (ChildAttributeMappingId=@ChildAttributeMappingId AND @ChildAttributeMappingId > 0)   OR ChildAttributeMappingId IS NULL)
		 END
	END
ELSE
	BEGIN
		INSERT INTO CM_ServiceItems(ItemId,SchemeId,BlockId,ContractorId,ContractCommencement,ContractPeriod,ContractPeriodType,Cycle,CycleType,CycleValue,CreatedDate,CreatedBy,Active,StatusId,BFSCarryingOutWork,ChildAttributeMappingId,VAT,TotalValue,CustomCycleFrequency ,CustomCycleOccurance ,CustomCycleType )
		VALUES(@ItemId,@schemeId,@blockId,@ContractorId,@Commencment,@ContractPeriod,@ContractPeriodType,@Cycle,@CycleType,@CycleValue,getdate(),@UpdatedBy,1,1,@BFSCarryingOutWork,@ChildAttributeMappingId,@VAT,@TotalValue,@CustomCycleFrequency,@CustomCycleOccurance,@CustomCycleType)
	END	
END 
---///////////////////////////////////////////////////////////////////////////////////
      Exec PDR_AmendpMSTAItemDetail NULL,@schemeId,@blockId,@ItemId,@UpdatedBy,@ChildAttributeMappingId,@MSATDetail 
	  
  END   
