SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stp_Send_Email_Assignee]

@JobID int

AS

Select RJ.assigned_to, HL.EMAIL, RJ.request_title from h_request_journal RJ
INNER JOIN H_LOGIN HL ON HL.USER_ID = RJ.assigned_to
where job_id = @JobID
GO
