USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AmendAsbestosInformation]    Script Date: 10/17/2017 18:14:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[AS_AmendAsbestosInformation]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_AmendAsbestosInformation] AS SET NOCOUNT ON;') 
GO 
-- =============================================  
-- Author:  Abdullah Saeed  
-- Create date: July 7,2014  
-- Description: Amend Asbestos Information  
-- =============================================  
ALTER  PROCEDURE [dbo].[AS_AmendAsbestosInformation]    
 -- Add the parameters for the stored procedure here  
   
 @propasbLevelId varchar(50),  
 @asbestosLevelId int,    
 @asbestosElementId int,    
 @riskId varchar(50),    
 @addedDate date,    
 @removedDate varchar(50),    
 @notes varchar(250), 
 @riskLevel int=null,
 @UrgentActionRequired bit, 
 @userId int    
     
   
AS  
BEGIN  
if @removedDate IS NOT NULL OR @removedDate != ''  
BEGIN  
UPDATE P_PROPERTY_ASBESTOS_RISKLEVEL  
  SET ASBRISKLEVELID = @asbestosLevelId, ASBESTOSID = @asbestosElementId, DateAdded = @addedDate, DateRemoved = @removedDate, Notes = @notes, UserID = @userId  ,
  RiskLevelId = @riskLevel,IsUrgentActionRequired=@UrgentActionRequired
  WHERE PROPASBLEVELID = @propasbLevelId  
END  
ELSE  
BEGIN  
  
UPDATE P_PROPERTY_ASBESTOS_RISKLEVEL  
  SET ASBRISKLEVELID = @asbestosLevelId, ASBESTOSID = @asbestosElementId, DateAdded = @addedDate, Notes = @notes, UserID = @userId ,
   RiskLevelId = @riskLevel,IsUrgentActionRequired=@UrgentActionRequired 
  WHERE PROPASBLEVELID = @propasbLevelId  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
  
END  
  
UPDATE P_PROPERTY_ASBESTOS_RISK  
  SET ASBRISKID = @riskId  
  WHERE PROPASBLEVELID = @propasbLevelId  
  
END  