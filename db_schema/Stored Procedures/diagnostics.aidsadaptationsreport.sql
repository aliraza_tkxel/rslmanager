SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Rogers
-- Create date: Jan 2015
-- Description:	Get all the open Aids and Adaptations cases (with latest statuses)
-- =============================================
CREATE PROCEDURE diagnostics.aidsadaptationsreport
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH cte_latest (journalid, adaptationhistoryid) 
	AS 
	(
	SELECT JOURNALID, MAX(ADAPTATIONHISTORYID) AS AdaptationHistoryId
	FROM dbo.C_ADAPTATION 
	GROUP BY JOURNALID
	)
	SELECT cust.FIRSTNAME, cust.LASTNAME, 
		   ISNULL(p.HOUSENUMBER+', ','') + ISNULL(p.ADDRESS1+', ','') + ISNULL(p.ADDRESS2+',', '') + ISNULL(p.ADDRESS3+', ','') + ISNULL(p.TOWNCITY+', ','') + ISNULL(p.POSTCODE,'') AS [Address],
		   c.TITLE, a.NOTES, a.LASTACTIONDATE, e.FIRSTNAME + ' ' + e.LASTNAME AS [UpdatedBy]
	FROM dbo.C_JOURNAL c 
	LEFT JOIN cte_latest l ON (l.JOURNALID = c.JOURNALID)
	LEFT JOIN dbo.C_ADAPTATION a ON (l.adaptationhistoryid = a.ADAPTATIONHISTORYID AND a.JOURNALID = c.JOURNALID)
	LEFT JOIN dbo.C__CUSTOMER cust ON (c.CUSTOMERID = cust.CUSTOMERID)
	INNER JOIN dbo.vw_PROPERTY_CURRENT_TENANTS_LIST ct ON (ct.CUSTOMERID = c.CUSTOMERID)
	LEFT JOIN dbo.P__PROPERTY p ON (p.PROPERTYID = ct.PropertyId)
	LEFT JOIN dbo.E__EMPLOYEE e ON (e.EMPLOYEEID = a.LASTACTIONUSER)
	WHERE c.ITEMID = 2 AND c.ITEMNATUREID  = 5 AND CURRENTITEMSTATUSID = 13
	ORDER BY a.LASTACTIONDATE ASC
END
GO
