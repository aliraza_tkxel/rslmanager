USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetCountVoidProperties]    Script Date: 06/03/2015 14:36:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
EXEC	[dbo].[AS_GetCountVoidProperties]
		@FuelType = N'',
		@PatchId = -1,
		@DevelopmentId = -1

-- ============================================= */

IF OBJECT_ID('dbo.AS_GetCountVoidProperties') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCountVoidProperties AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetCountVoidProperties]
-- Add the parameters for the stored procedure here
(
	@FuelType varchar(25) = '',
	@PatchId int = -1,
	@DevelopmentId	int = -1
)
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int

		

		--=====================Search Criteria===============================
		SET @SEARCHCRITERIA = ' 1=1 '
		
		
		IF(@FuelType <> '' AND @FuelType IS NOT NULL AND @FuelType <> 'Please Select')
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND T.PARAMETERVALUE = ''' + @FuelType	+ ''''+CHAR(10)
		END
		
		
		IF (@PatchId > 0 )	--NOT EQUALS TO 0 OR 1	
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ( PATCH.PATCHID = ' + CONVERT(VARCHAR(10),@PatchId) + ' ) '
		END
		
		IF (@DevelopmentId > 0)
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND S.SCHEMEID = ' + CONVERT(VARCHAR(10),@DevelopmentId) + CHAR(10)
		END
		
		
		
		SET @SEARCHCRITERIA = @SEARCHCRITERIA + ' AND  ( ( P.STATUS = 1 AND (P.SUBSTATUS <> 21 OR P.SUBSTATUS IS NULL) ) OR 
		(P.Status=2 AND P.SUBSTATUS = 22 ) ) 
		AND P.PROPERTYTYPE NOT IN (SELECT PROPERTYTYPEID FROM P_PROPERTYTYPE WHERE DESCRIPTION IN (''Garage'',''Car Port'',''Car Space''))
		AND T.PARAMETERVALUE = ''Mains Gas''
		
		) AS TABS'
		
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'SELECT COUNT(TABS.REF) FROM ( Select 
							  P.PROPERTYID As Ref,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address, P.TOWNCITY, P.POSTCODE  
,ISNULL(S.SCHEMENAME,''-'') as Scheme,ISNULL(B.BLOCKNAME,''-'') AS Block,ISNULL(Convert(Varchar(50), T.TerminationDate,103),''-'') as Termination,
convert(DATETIME, T.TerminationDate,103) as TerminationDate,
ISNULL(Convert(Varchar(50),T.ReletDate,103),''-'') as Relet,ISNULL(ST.DESCRIPTION,''-'') AS Status
,ISNULL(SUB.DESCRIPTION,''-'') as SubStatus'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM P__PROPERTY P  

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
LEFT JOIN E_PATCH PATCH ON P.PATCH = PATCH.PATCHID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
LEFT JOIN P_LGSR CP12 ON P.PROPERTYID = CP12.PROPERTYID

Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P.PropertyID
AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
 GROUP BY PROPERTYID) as CJournal
CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination
Cross Apply (SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
		Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
		ELSE Convert(Varchar(50), M.ReletDate,103)END as ReletDate,J.JOURNALID,A.PARAMETERVALUE
		FROM P__PROPERTY P
	LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P.PROPERTYID	AND
										A.ITEMPARAMID =
										(
											SELECT
												ItemParamID
											FROM
												PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
											WHERE
												ParameterName = ''Heating Fuel''
												AND ItemName = ''Heating''
										)
    INNER JOIN C_JOURNAL J ON CJournal.journalID=J.JOURNALID
	INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
	INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
	
 ) As  T-- ON P.PROPERTYID=T.PROPERTYID

		'
							
		
		
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause
		
		
			print(@mainSelectQuery)
			EXEC (@mainSelectQuery)
		
		
		--========================================================================================
		-- Begin building Count Query 
		
		
END
