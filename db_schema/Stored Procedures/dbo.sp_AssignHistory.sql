SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE    PROCEDURE [dbo].[sp_AssignHistory]

@AssignedFrom int,
@ActualStartDate datetime,
@Quoted money,
@Days int,
@RequestType int,
@AssignedTo int,
@AuthorisedBy int,
@FontColor int,
@AssignmentMadeBy int,
@JOBID int,
@Reminder int output

AS

DECLARE @ASSIGNED_TO INT
--SET @JOBID = 1261
SET @ASSIGNED_TO = (SELECT ASSIGNED_TO FROM H_REQUEST_JOURNAL WHERE JOB_ID = @JOBID)
SET NOCOUNT ON
--PRINT @ASSIGNED_TO

INSERT INTO H_ASSIGNHISTORY (AssignedFrom, ActualStartDate, Quoted, Days, requestType, AssignedTo, AuthorisedBy, FontColor, AssignmentMadeBy, JOB_ID)
VALUES (@ASSIGNED_TO, @ActualStartDate, @Quoted, @Days, @RequestType, @AssignedTo, @AuthorisedBy, @FontColor, @AssignmentMadeBy, @JOBID)

SET @Reminder = (SELECT COUNT(*) FROM H_ASSIGNHISTORY WHERE JOB_ID = @JOBID) 

SET NOCOUNT OFF



GO
