SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_UpdateDevelopmentId @employeeId = @employeeid , @developmentId = @developmentId
-- Author:		<Salman Nazir>
-- Create date: <10/02/2012>
-- Description:	<Update the Scheme drop down values against user>
-- WEb Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdateDevelopmentId](
	-- Add the parameters for the stored procedure here
	@employeeId int,
	@developmentId int 
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE AS_UserPatchDevelopment SET DevelopmentId=@developmentId,IsActive=1
	 WHERE 	EmployeeId=@employeeId
END
GO
