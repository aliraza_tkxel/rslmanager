
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[P_GetSearchPropertyAndScheme] 

/* ===========================================================================
 ' NAME: P_GetSearchPropertyAndScheme
-- EXEC	 [dbo].[P_GetSearchPropertyAndScheme] '1 ship'
--		
-- Author:		<Ahmed Mehmood>
-- Create date: <14/06/2013>
-- Description:	< Returns the search results for property and scheme >
-- Web Page: SearchProperty.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@searchText varchar (MAX) = ''
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),		                
	        @SearchCriteria varchar(8000),
	        @mainSelectQuery varchar(8000)
	        

    --========================================================================================
	--							PROPERTY SEARCH
    --========================================================================================
         
    SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.PROPERTYID LIKE ''%'+ LTRIM(@searchText) +'%'' OR'
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.FLATNUMBER LIKE ''%'+ LTRIM(@searchText) +'%'' OR'  
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.HouseNumber LIKE ''%'+ LTRIM(@searchText) +'%'' OR'
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.ADDRESS1 LIKE ''%'+ LTRIM(@searchText) +'%'' OR'
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.ADDRESS2 LIKE ''%'+ LTRIM(@searchText) +'%'' OR' 
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.ADDRESS3 LIKE ''%'+ LTRIM(@searchText) +'%'' OR' 
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+ LTRIM(@searchText) +'%'' AND'                        
                                                                                                                                                                                         
    --SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
    --                         '(FREETEXT(P__PROPERTY.HouseNumber ,'''+LTRIM(@searchText)+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+LTRIM(@searchText)+''')  OR FREETEXT(P__PROPERTY.ADDRESS2, '''+LTRIM(@searchText)+''')  OR FREETEXT(P__PROPERTY.ADDRESS3, '''+LTRIM(@searchText)+''')) AND'    
                             
    -- Building Main Query for Property Search
                             
    SET @SelectClause = 'SELECT TOP(100)' +                      
    CHAR(10) + 'P__PROPERTY.PROPERTYID PropertyId,P_PROPERTYTYPE.DESCRIPTION TypeDescription,ISNULL(P__PROPERTY.FLATNUMBER,'''')+'' ''+ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + 'P__PROPERTY INNER JOIN' +
                      CHAR(10) + 'P_PROPERTYTYPE ON p__property.PROPERTYTYPE = P_PROPERTYTYPE.PROPERTYTYPEID' 	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	    
			
	
    --========================================================================================
	--							SCHEME SEARCH
    --========================================================================================							
	
	SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P_DEVELOPMENT.SCHEMENAME LIKE ''%'+ LTRIM(@searchText) +'%'' AND'                                                          
    
                             
    -- Building Main Query for Property Search
                             
    SET @SelectClause = 'SELECT DISTINCT TOP(100)' +                      
    CHAR(10) + 'P_DEVELOPMENT.DEVELOPMENTID DevelopmentId,P_DEVELOPMENT.SCHEMENAME DevelopmentName,P_DEVELOPMENTTYPE.DESCRIPTION Description'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + 'P__PROPERTY' +
                      CHAR(10) + 'INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID'+
                      CHAR(10) + 'INNER JOIN P_DEVELOPMENTTYPE ON P_DEVELOPMENT.DEVELOPMENTTYPE = P_DEVELOPMENTTYPE.DEVELOPMENTTYPEID' 	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	 











GO
