SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_TEAM_SELECT]
AS
	SET NOCOUNT ON;
SELECT     TEAMID, CREATIONDATE, TEAMNAME, MANAGER, DIRECTOR, MAINFUNCTION, DESCRIPTION, ACTIVE
FROM         E_TEAM
WHERE		ACTIVE = 1
GO
