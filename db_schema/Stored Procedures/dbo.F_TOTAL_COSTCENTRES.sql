SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[F_TOTAL_COSTCENTRES]

AS

SELECT CONVERT(VARCHAR,Sum(F_CostCentre.CostCentreAllocation) ,1)  AS TotalCostCentre
FROM dbo.F_CostCentre
where Active = 1






GO
