SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC  [dbo].[RD_GetCountCompletedFaults]
-- Author:  <Ali Raza>
-- Create date: <24/07/2013>
-- Description: <This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetCountCompletedFaults_Repairs]
 -- Add the parameters for the stored procedure here

 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 
 DECLARE @StartDate DATETIME
 DECLARE @EndDate DATETIME
 
 SET @StartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ) )
 SET @EndDate = DATEADD(SS,-1,DATEADD(mm,12,@StartDate))

print(@StartDate)
print(@EndDate)

 SET NOCOUNT ON;
 SELECT   count(*)  
 from ( SELECT distinct FL_FAULT_LOG.FaultLogID FaultLogID,FL_FAULT_LOG.StatusID ,CONVERT(VARCHAR(10),C_REPAIRS.LASTACTIONDATE, 120)  CompletedDate
   FROM FL_FAULT_LOG    
   INNER JOIN FL_FAULT_JOURNAL on FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID
   LEFT JOIN FL_FAULTJOURNAL_TO_CJOURNAL on FL_FAULT_JOURNAL.JournalID = FL_FAULTJOURNAL_TO_CJOURNAL.FJOURNALID
   LEFT JOIN (SELECT * FROM C_REPAIR WHERE C_REPAIR.ItemStatusId=11 and C_REPAIR.ItemActionId=6) C_REPAIRS on FL_FAULTJOURNAL_TO_CJOURNAL.CJOURNALID = C_REPAIRS.JOURNALID
   where FL_FAULT_LOG.StatusID =17  ) CompletedFaults
 where CompletedFaults.CompletedDate BETWEEN CONVERT(VARCHAR(10),@StartDate, 120)  AND CONVERT(VARCHAR(10),@EndDate, 120)  
END
GO
