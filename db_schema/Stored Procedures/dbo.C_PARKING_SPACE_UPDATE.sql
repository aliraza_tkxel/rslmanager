SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE     PROC [dbo].[C_PARKING_SPACE_UPDATE](
/* ===========================================================================
 '   NAME:         C_PARKING_SPACE_CREATE
 '   DATE CREATED:  28 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
Creates a new record by making entry in C_PARKING_SPACE and
 'updates     C_JOURNAL tables		 
 '   IN:             @CUSTOMERID
 '   IN:             @TENANCYID
 '   IN:             @ITEMID
 '   IN:             @ITEMNATUREID
 '   IN:             @TITLE
 '   IN:             @LASTACTIONUSER
 '   IN:             @LOOKUPVALUEID
 '   IN:             @OTHERINFO
 '
 '   OUT:            	    Nothing    
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@HISTORYID INT , 
@CURRENTITEMSTATUSID INT,
@LASTACTIONUSER INT ,
@NOTES VARCHAR(4000), 
@LookUpValueID INT
)
AS
BEGIN 

DECLARE @JOURNALID INT
DECLARE @PARKINGSPACEHISTORYID INT

SET NOCOUNT ON
	
	SELECT 
		@JOURNALID = G.JOURNALID
	FROM C_PARKING_SPACE G
	WHERE G.PARKINGSPACEHISTORYID = @HISTORYID

 BEGIN TRAN
	
	
	UPDATE C_JOURNAL SET 	CURRENTITEMSTATUSID = @CURRENTITEMSTATUSID 
	WHERE JOURNALID = @JOURNALID

	-- if insertion fails, goto handle error blcok
	 --IF @@ERROR <> 0 GOTO HANDLE_ERROR
	
	INSERT INTO C_PARKING_SPACE (JOURNALID, LookUpValueID , AdditionalInfo, LASTACTIONDATE,LASTACTIONUSER,ITEMSTATUSID)
	VALUES (@JOURNALID, @LookUpValueID, @NOTES,  GETDATE(), @LastActionUser,@CURRENTITEMSTATUSID)

	SELECT 	@PARKINGSPACEHISTORYID = SCOPE_IDENTITY()

	 -- if insertion fails, goto handle error blcok
	 --IF @@ERROR <> 0 GOTO HANDLE_ERROR
	SELECT @JOURNALID AS JOURNALID , @PARKINGSPACEHISTORYID AS SERVICEID
COMMIT TRAN 

	
--	RETURN @JOURNALID 
--
--	/*'=================================*/
--
--	HANDLE_ERROR:
--
--	   ROLLBACK TRAN
--
--	   RETURN -1

END




GO
