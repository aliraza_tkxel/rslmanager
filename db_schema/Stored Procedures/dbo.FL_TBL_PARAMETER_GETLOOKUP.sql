SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[FL_TBL_PARAMETER_GETLOOKUP]
/* ===========================================================================
 '   NAME:          [FL_TBL_PARAMETER_GETLOOKUP]
 '   DATE CREATED:   16 OCT 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Location FL_AREA table which will be shown as lookup value in presentation pages
 '   IN:             @ItemID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@ItemID as Int
AS
	SELECT ParameterID AS id, Name AS val
	FROM TBL_PDA_ITEM_PARAMETER
	WHERE ItemID=@ItemID
	ORDER BY Name ASC



/****** Object:  StoredProcedure [dbo].[FL_ADD_NEW_REPAIR]    Script Date: 08/25/2009 12:23:19 ******/
SET ANSI_NULLS ON
GO
