USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetCP12StatusReport]    Script Date: 06/03/2015 14:36:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get available Properties for Report 
    Author: Ali Raza
    Creation Date: June-28-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         June-28-2015      Ali Raza         Get available Properties for Report 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetCP12StatusReport]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
IF OBJECT_ID('dbo.V_GetCP12StatusReport') IS NULL 
	EXEC('CREATE PROCEDURE dbo.V_GetCP12StatusReport AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[V_GetCP12StatusReport]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		@propertyStatus VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Termination', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(5000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = '  ( (( PROPERTY.STATUS = 1 AND (PROPERTY.SUBSTATUS <> 21 OR PROPERTY.SUBSTATUS IS NULL) ) OR 
		(PROPERTY.Status=2 AND PROPERTY.SUBSTATUS = 22 ) )
		AND PROPERTY.PROPERTYTYPE NOT IN (SELECT PROPERTYTYPEID FROM P_PROPERTYTYPE WHERE DESCRIPTION IN (''Garage'',''Car Port'',''Car Space''))
		)
		AND	ParameterName = ''Heating Fuel''
		AND ItemName = ''Heating''
		AND PV.ValueDetail = ''Mains Gas''
		
		'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ( PROPERTY.PROPERTYID LIKE ''%' + @searchText + '%'' ) OR ( ISNULL(PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+PROPERTY.ADDRESS2, '''')  LIKE ''%' + @searchText + '%''))'
		END	
		
		IF(@propertyStatus <> '' OR @propertyStatus IS NOT NULL OR @propertyStatus <> -1)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND (PROPERTY.STATUS = (SELECT STATUSID FROM P_STATUS WHERE DESCRIPTION = ''' +@propertyStatus+''' ))' + CHAR(10)
		END
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'SELECT TOP ('+convert(varchar(10),@limit)+')PROPERTY.PROPERTYID AS Ref,
	ISNULL(SCHEME.SCHEMENAME,''-'') AS Scheme,
	ISNULL(PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+PROPERTY.ADDRESS2, '''') AS Address,
	ISNULL(ST.DESCRIPTION,''-'') AS Status,
	--ISNULL(SUB.DESCRIPTION,''-'') AS SubStatus,
	
	ISNULL(CONVERT(VARCHAR(50), T.TERMINATIONDATE,103),''-'') AS Termination,
	T.TERMINATIONDATE AS TerminationDate,
	
	ISNULL(CONVERT(VARCHAR(50),T.RELETDATE,103),''-'') AS Relet,
	CONVERT(DATETIME, T.RELETDATE,103) AS ReletDate,
	
	ISNULL(CONVERT(varchar,AS_APPOINTMENTS.APPOINTMENTDATE,103)+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME+''-''+ AS_APPOINTMENTS.APPOINTMENTENDTIME ,''-'') AS ServiceAppointment,
	CONVERT(DATETIME,AS_APPOINTMENTS.APPOINTMENTDATE+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME) AS ServiceAppointmentDate,
	
	CONVERT(VARCHAR, CP12.ISSUEDATE,103) AS CP12Issued,
	CONVERT(DATETIME, CP12.ISSUEDATE,103) AS CP12IssuedDate,
	CASE
    WHEN CP12.CP12Renewal IS NULL THEN 
		ISNULL(Convert(varchar(100),DATEADD(YEAR,1,CP12.ISSUEDATE),103), ''N/A'')
		
	ELSE
		ISNULL(CONVERT(NVARCHAR, CP12.CP12Renewal, 103), ''N/A'') 
	END AS  CP12Renewal  ,
	
	CASE
    WHEN CP12.CP12Renewal IS NULL THEN 
		Convert(DateTime,DATEADD(YEAR,1,CP12.ISSUEDATE))
		
	ELSE
		CONVERT(DateTime, CP12.CP12Renewal) 
	END AS CP12RenewalDate,
	CASE 
		WHEN CP12.LGSRID IS NULL THEN
			-1
		ELSE 
			CP12.LGSRID
		END AS LGSRID,
	
	PV.ValueDetail,
	 ISNULL(Convert(Varchar(50),TENANCY.STARTDATE,103),''-'') as ActualRelet,
 TENANCY.STARTDATE  as ActualRelet_col, 
 ISNULL(Convert(Varchar(50),VoidWorks.WorksCompletionDate,103),''-'') as WorksCompletionDate,
 VoidWorks.WorksCompletionDate as WorksCompletionDate_col ' 
	
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM P__PROPERTY PROPERTY  

LEFT JOIN P_BLOCK BLOCK ON PROPERTY.BLOCKID=BLOCK.BLOCKID
LEFT JOIN P_SCHEME SCHEME ON PROPERTY.SCHEMEID=SCHEME.SCHEMEID
INNER JOIN P_STATUS ST ON PROPERTY.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON PROPERTY.SUBSTATUS = SUB.SUBSTATUSID
LEFT JOIN P_LGSR CP12 ON PROPERTY.PROPERTYID = CP12.PROPERTYID

LEFT JOIN AS_JOURNAL ON AS_JOURNAL.JOURNALID = CP12.JOURNALID
LEFT JOIN AS_APPOINTMENTS ON AS_JOURNAL.JOURNALID = AS_APPOINTMENTS.JOURNALID

INNER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = PROPERTY.PROPERTYID	
Inner join PA_ITEM_PARAMETER PIP on A.ITEMPARAMID = PIP.ItemParamID
Inner JOIN PA_PARAMETER P on PIP.ParameterId = P.ParameterID
Inner JOIN PA_ITEM I ON PIP.ItemId = I.ItemID
INNER JOIN PA_PARAMETER_VALUE PV ON P.ParameterID = PV.ParameterID AND A.VALUEID = PV.ValueID


CROSS APPLY(
	SELECT MAX(J.JOURNALID) AS JOURNALID FROM C_JOURNAL J 
	WHERE J.PROPERTYID = PROPERTY.PROPERTYID 
	AND  J.ITEMNATUREID = 27 AND J.CURRENTITEMSTATUSID IN(13,14,15) 
	GROUP BY PROPERTYID) AS CJOURNAL
 
	CROSS APPLY (SELECT MAX(TERMINATIONHISTORYID) AS TERMINATIONHISTORY FROM  C_TERMINATION WHERE JOURNALID=CJOURNAL.JOURNALID)AS CTERMINATION
	
	CROSS APPLY (
		SELECT PROPERTY.PROPERTYID,
		CONVERT(VARCHAR(50), 
		T.TERMINATIONDATE,103) AS TERMINATIONDATE,
		CASE 
			WHEN M.RELETDATE IS NULL THEN 
			CONVERT(NVARCHAR(50),DATEADD(DAY,7,T.TERMINATIONDATE), 103)
		ELSE
			 CONVERT(VARCHAR(50), M.RELETDATE,103)
		END AS RELETDATE,
		JOURNAL.JOURNALID,
		A.PARAMETERVALUE
		
		FROM P__PROPERTY PROPERTY
		INNER JOIN C_JOURNAL JOURNAL ON CJOURNAL.JOURNALID=JOURNAL.JOURNALID
		INNER JOIN C_TERMINATION T ON CTERMINATION.TERMINATIONHISTORY=T.TERMINATIONHISTORYID
		INNER JOIN C__CUSTOMER C ON JOURNAL.CUSTOMERID = C.CUSTOMERID
		INNER JOIN C_TENANCY CT ON JOURNAL.TENANCYID=CT.TENANCYID
		INNER JOIN C_CUSTOMERTENANCY ON C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID AND CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
		INNER JOIN PDR_MSAT M ON PROPERTY.PROPERTYID = JOURNAL.PROPERTYID AND M.CUSTOMERID = JOURNAL.CUSTOMERID  AND M.TENANCYID = JOURNAL.TENANCYID AND M.MSATTYPEID=5	
	
 ) AS  T-- ON P.PROPERTYID=T.PROPERTYID

		LEFT JOIN ( SELECT CT.STARTDATE, T.PROPERTYID 
FROM C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON T.TENANCYID = CT.TENANCYID  
WHERE CT.ENDDATE is NULL AND ( CUSTOMERTENANCYID = ( SELECT max( CUSTOMERTENANCYID ) FROM C_CUSTOMERTENANCY C_CT WHERE C_CT.TENANCYID = CT.TENANCYID ) )
) AS TENANCY ON  TENANCY.PROPERTYID = PROPERTY.PROPERTYID

LEFT JOIN 
(	select M.PropertyId, max(V.JobsheetCompletionDate) as WorksCompletionDate
	From PDR_JOURNAL J    
	INNER JOIN PDR_MSAT M   ON J.MSATID = M.MSATId   
	INNER JOIN V_RequiredWorks V   ON J.JOURNALID = V.WorksJournalId 
	where V.JobsheetCompletionDate is not null AND
	M.PropertyId NOT IN 
		( select DISTINCT PDR_MSAT.PropertyId From  PDR_JOURNAL     
		INNER JOIN PDR_MSAT   ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId   
		INNER JOIN V_RequiredWorks ON PDR_JOURNAL.JOURNALID = V_RequiredWorks.WorksJournalId
		where V_RequiredWorks.JobsheetCompletionDate is Null ) 
	group by  M.PropertyId
) AS VoidWorks ON  VoidWorks.PROPERTYID = PROPERTY.PROPERTYID 
   
 '
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Ref' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TerminationDate' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'RELETDATE' 	
			
		END

		IF(@sortColumn = 'ActualRelet')  
		BEGIN  
			SET @sortColumn = CHAR(10)+ 'ActualRelet_col'    
		END  

		IF(@sortColumn = 'WorksCompletionDate')  
		BEGIN  
			SET @sortColumn = CHAR(10)+ 'WorksCompletionDate_col'    
		END 
		
		
		IF(@sortColumn = 'CP12ISSUED')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'CP12ISSUEDDATE' 	
			
		END
		
		IF(@sortColumn = 'CP12RENEWAL')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'CP12RENEWALDATE' 	
			
		END
			
		IF(@sortColumn = 'ServiceAppointment')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'SERVICEAPPOINTMENTDATE' 	
			
		END	
		
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT PROPERTY.PropertyID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
