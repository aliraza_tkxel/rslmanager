
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_Statuses
-- Author:		<Salman Nazir>
-- Create date: <23/09/2012>
-- Description:	<This Stored Proceedure shows the statuses on the Resoucres Page, Status Listing >
-- =============================================
CREATE PROCEDURE [dbo].[AS_Statuses]


AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AS_Status.Title,AS_Status.Ranking,AS_Status.StatusId,InspectionTypeID,IsEditable FROM AS_Status
	Order BY Ranking
END
GO
