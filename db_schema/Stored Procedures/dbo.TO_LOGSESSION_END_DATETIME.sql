SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[TO_LOGSESSION_END_DATETIME]
	/*	===============================================================
	'   NAME:           TO_LOGSESSION_DATETIME]
	'   DATE ALTERD:   4 nov 2008
	'   ALTERD BY:     Adnan
	'   ALTERD FOR:    Broadland Housing
	'   PURPOSE:        To maintain Log in time for customers
	'   IN:             @CustomerId 
    '	OUT:			@SessionId
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

	(
	
	@SessionId INT 
	)
	
AS

BEGIN 
	SET NOCOUNT ON;
	UPDATE TO_LOGIN_SESSION SET LOGOUTDATETIME=GETDATE() WHERE SESSIONID=@SessionId
	
END 


GO
