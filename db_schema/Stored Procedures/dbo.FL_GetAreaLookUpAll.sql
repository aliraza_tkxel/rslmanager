SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[FL_GetAreaLookUpAll]
/* ===========================================================================
 --	EXEC FL_GetAreaLookUpAll		
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To Get List of AreaName and Area ID for LookUp/DropDown Lists
--  Webpage:		View/Reports/ReportArea.aspx (For Add/Amend Fault Modal Popup)
 '==============================================================================*/

AS
	SELECT AreaID AS id,AreaName AS val
	FROM FL_AREA	
	ORDER BY AreaName ASC
GO
