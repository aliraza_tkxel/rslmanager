SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Umair Naeem
-- Create date: 07 July 2010
-- Description:	This procedure gets the 
--				list of SECTIONS
-- =============================================
--
-- EXEC [MEFF_GET_SECTION] @MEFESECTIONID=1
--
CREATE PROCEDURE [dbo].[MEFF_GET_SECTION]
	-- Add the parameters for the stored procedure here
	@MEFESECTIONID INT=NULL,
	@MEFSECTION NVARCHAR(50) =NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT MEFESECTIONID,DESCRIPTION AS MEFSection  FROM dbo.MEF_SECTION  
	WHERE  MEFESECTIONID=COALESCE(@MEFESECTIONID, MEFESECTIONID)
		AND DESCRIPTION=COALESCE(@MEFSECTION, DESCRIPTION)	
	
	SET NOCOUNT OFF;
END 



















GO
