USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =====================================================
-- EXEC [dbo].[FL_SaveContractorAppointmentDetails]

	--@ORGID =1270,
	--@TempFaultID =1487,
	--@UserId =760
-- Author:		<Ahmed Mehmood>
-- Create date: <3/4/2015>
-- Description:	<Saves all appointment information>
-- Web Page: JobSheetSummarySubcontractor.aspx
-- =====================================================

IF OBJECT_ID('dbo.FL_SaveContractorAppointmentDetails') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_SaveContractorAppointmentDetails AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[FL_SaveContractorAppointmentDetails]
( 
	@ORGID int,	
	@TempFaultID int,
	@UserId int,
	@jsn varchar(50) OUTPUT,	
	@result INT OUTPUT
	
)
AS
BEGIN
	-------- Declaring variables -------------------------------------------------------
	------------------------------------------------------------------------------------
		
	Declare @SelectStmt as nvarchar(3000)
	Declare @Sql as nvarchar(3200)
	Declare @StatusID as int
	Declare @IsReported as bit
	Declare @SubmitDate as DateTime
	Declare @FaultLogID as int
	Declare @JournalID as int
	Declare @JSNumberPrefix as varchar(10)
	Declare @ResponseTime as int
	Declare @Days as int
	Declare @ItemID as int
	Declare @ItemNatureID as int
	Declare @TenancyID as int
	Declare @DueDate as DateTime
	Declare @ItemActionId as int
	Declare @JobSheetNumber as varchar(50)
	
	Declare	@FaultId as int
	Declare	@Quantity as int
	Declare	@ProblemDays as int
	Declare	@RecuringProblem as bit
	Declare	@CommunalProblem bit
	Declare	@Notes varchar(4000)
	Declare	@FaultTradeId int
	Declare	@Recharge bit
	Declare	@isRecall BIT
	Declare	@OriginalFaultLogId int
	Declare	@CustomerId int
	Declare	@AreaId int
	Declare	@PropertyId varchar(20)

	-------- Populating variables ------------------------------------------------------
	------------------------------------------------------------------------------------
	
	SELECT	@FaultId = FTF.FAULTID
			,@Quantity = FTF.Quantity
			,@ProblemDays = FTF.ProblemDays
			,@RecuringProblem = FTF.RecuringProblem
			,@CommunalProblem = FTF.CommunalProblem
			,@Notes = FTF.NOTES
			,@FaultTradeId = FTF.FaultTradeId
			,@Recharge = FTF.Recharge
			,@isRecall = FTF.isRecall
			,@OriginalFaultLogId = FTF.OriginalFaultLogId
			,@PropertyId = FTF.PropertyId
			,@CustomerId = FTF.CustomerId
			,@AreaId = ftf.areaId
	FROM	FL_TEMP_FAULT AS FTF
	WHERE	FTF.TempFaultId = @TempFaultID
	
	
	
	if @ORGID= -1
		set @ORGID=NULL
		
	SET @JSNumberPrefix  = 'JS'
	SET @SubmitDate = GETDATE()
	
	SELECT @ResponseTime = ResponseTime, @Days = Days
	FROM FL_FAULT_PRIORITY 
	INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PriorityID = FL_FAULT.PriorityID 
	WHERE FaultID = @FaultId
	
	If @Days = 0
	SET @DueDate = DateAdd(HH,@ResponseTime,@SubmitDate)
	ELSE
	SET @DueDate = DateAdd(DD,@ResponseTime,@SubmitDate)
	
	
	-- Get Tenancy Detail

	SELECT	@TenancyId = CT.TenancyId
	FROM	C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON 
			( T.TENANCYID = CT.TENANCYID ) AND 
			( T.ENDDATE >= CONVERT(date, GETDATE() ) OR 
             T.ENDDATE IS NULL ) AND
			( CT.ENDDATE >= CONVERT(date, GETDATE() )	OR 
             CT.ENDDATE IS NULL )
	WHERE	( T.PROPERTYID = @PropertyId ) AND 
			( CT.CUSTOMERTENANCYID = (	SELECT max( CUSTOMERTENANCYID ) 
										FROM C_CUSTOMERTENANCY C_CT 
										WHERE C_CT.TENANCYID = CT.TENANCYID ) )
	
	
	SET @ItemId = NULL
	SET @ItemNatureId = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION = 'Reactive Repair')
	SET @IsReported = 1
	SET @StatusID = 2
	SET @ItemActionId = 2
	
	if @OriginalFaultLogId = -1
		set @OriginalFaultLogId = NULL
	

	-------- Inserting into Tables -----------------------------------------------------
	------------------------------------------------------------------------------------
	
	-- FL_FAULT_LOG TABLE 
	
	INSERT INTO FL_FAULT_LOG
	(FaultID, CustomerID, Quantity , ProblemDays, RecuringProblem, CommunalProblem, Notes,
	ORGID, Recharge, PropertyId, StatusID, IsReported, SubmitDate, DueDate,FaultTradeId , USERID,areaId)
	VALUES
	(@FaultID, @CustomerId, @Quantity , @ProblemDays, @RecuringProblem, @CommunalProblem,
	@Notes, @ORGID, @Recharge,@PropertyId, @StatusID, @IsReported, @SubmitDate, @DueDate,@FaultTradeId,@UserId,@AreaId)	
	
	SET @FaultLogID = @@IDENTITY
	
	-- UPDATE JOBSHEETNUMBER
	
	UPDATE	FL_FAULT_LOG 
	SET		JobSheetNumber = (@JSNumberPrefix + CONVERT(VARCHAR,@FaultLogID)) 
	WHERE	FaultLogId = @FaultLogID
	
	SET @JobSheetNumber= (@JSNumberPrefix + CONVERT(VARCHAR,@FaultLogID)) 	
	SET @jsn = @JobSheetNumber
	
	-- JOURNAL TABLE
	
	INSERT INTO FL_FAULT_JOURNAL
	(FaultLogID,  PropertyId, TenancyID, ItemID, ItemNatureID, FaultStatusID, CreationDate, LastActionDate)
	VALUES
	(@FaultLogID, @PropertyId, @TenancyID, @ItemID, @ItemNatureID, @StatusID, @SubmitDate, @SubmitDate)
		
	SET @JournalID = @@IDENTITY
	
	-- FL_FAULT_LOG_HISTORY
	
	-- 'Repair Reported'
	set @StatusID = 1
	INSERT INTO FL_FAULT_LOG_HISTORY
	(JournalID, FaultLogID, FaultStatusID, Notes, ORGID, ItemActionID, PROPERTYID, LastActionDate, LastActionUserID)
	VALUES
	(@JournalID, @FaultLogID, @StatusID, @Notes, @ORGID, @ItemActionId, @PropertyId, @SubmitDate,@UserId)
	
	-- 'Assign to Subcontractor'
	set @StatusID = 2
	INSERT INTO FL_FAULT_LOG_HISTORY
	(JournalID, FaultLogID, FaultStatusID, Notes, ORGID, ItemActionID, PropertyId, LastActionDate,LastActionUserID)
	VALUES
	(@JournalID, @FaultLogID, @StatusID, @Notes, @ORGID, @ItemActionId, @PropertyId, @SubmitDate,@UserId)
	
	-- UPDATE FL_TEMP_FAULT
	
	UPDATE	FL_TEMP_FAULT
	SET		isAppointmentConfirmed = 1
			,FaultLogId = @FaultLogID
			,ORGID = @ORGID 
	WHERE	TempFaultID = @TempFaultID	
	
	If @isRecall = 1
	BEGIN
		INSERT INTO FL_FAULT_RECALL 
		VALUES (@FaultLogID,@OriginalFaultLogId)
	END
	
	SET @result = @JournalID
    
END

