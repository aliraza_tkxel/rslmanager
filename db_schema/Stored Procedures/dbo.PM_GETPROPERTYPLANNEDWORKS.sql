SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*----------------------------------------------*/
--UPDATED BY:UMAIR
--UPDATE DATE:12 MAY 2009
--UPDATE REASON:THE PROCEDURE WAS NOT SHOWING THOSE
--				PROPERTIES WHICH WERE NOT ALLOCATED
/*----------------------------------------------*/

CREATE PROCEDURE [dbo].[PM_GETPROPERTYPLANNEDWORKS]
	@PROPERTYID VARCHAR(20)
AS
BEGIN

	SELECT	PM_PROGRAMME.PROGRAMMEID, PM_PROGRAMME.PROGRAMMENAME, PM_PROGRAMME.NETUNITCOST, 
			CASE WHEN S_ORGANISATION.NAME IS NULL THEN 'N/A' ELSE S_ORGANISATION.NAME END AS [NAME], 
			PM_PROGRAMME_ASSIGNMENT.PROPERTYID, PM_WORKORDER_STATUS.DESCRIPTION, PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID, 
			CASE WHEN PM_WORKORDER_CONTRACT.WORKORDERID IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),PM_WORKORDER_CONTRACT.WORKORDERID) END AS WORKORDERID
	FROM    PM_PROGRAMME_ASSIGNMENT 
		INNER JOIN PM_PROGRAMME ON PM_PROGRAMME_ASSIGNMENT.PROGRAMMEID = PM_PROGRAMME.PROGRAMMEID 
		LEFT JOIN PM_WORKORDER_CONTRACT ON PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID = PM_WORKORDER_CONTRACT.PROPERTYLISTID 
		INNER JOIN P_WORKORDER ON PM_WORKORDER_CONTRACT.WORKORDERID =P_WORKORDER.WOID  
		INNER JOIN F_PURCHASEORDER ON P_WORKORDER.ORDERID=F_PURCHASEORDER.ORDERID
		INNER JOIN S_ORGANISATION ON S_ORGANISATION.ORGID = F_PURCHASEORDER.SUPPLIERID
		INNER JOIN P_WOTOREPAIR ON P_WORKORDER.WOID=P_WOTOREPAIR.WOID
		INNER JOIN C_JOURNALTOPLANNEDMAINTENANCE ON P_WOTOREPAIR.JOURNALID=C_JOURNALTOPLANNEDMAINTENANCE.JOURNALID 
			AND C_JOURNALTOPLANNEDMAINTENANCE.PROPERTYLISTID=PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID
		INNER JOIN C_REPAIR ON P_WOTOREPAIR.JOURNALID=C_REPAIR.JOURNALID
		INNER JOIN C_REPAIRTOPLANNEDMAINTENANCE ON C_REPAIR.REPAIRHISTORYID=C_REPAIRTOPLANNEDMAINTENANCE.REPAIRHISTORYID
		INNER JOIN PM_WORKORDER_STATUS ON C_REPAIRTOPLANNEDMAINTENANCE.STATUSID=PM_WORKORDER_STATUS.STATUSID 
				AND dbo.PM_FN_GETCURRENTWORKSTATUS(P_WOTOREPAIR.JOURNALID) =PM_WORKORDER_STATUS.STATUSID
	WHERE     (PM_PROGRAMME.STATUSID = 1) AND PM_PROGRAMME_ASSIGNMENT.PROPERTYID = @PROPERTYID
		AND PM_PROGRAMME_ASSIGNMENT.STATUSID=2
		AND C_REPAIR.REPAIRHISTORYID =(SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR A WHERE A.JOURNALID=P_WOTOREPAIR.JOURNALID)
	--update start
	UNION
	SELECT	PM_PROGRAMME.PROGRAMMEID, PM_PROGRAMME.PROGRAMMENAME, PM_PROGRAMME.NETUNITCOST,'N/A' AS [NAME], 
			PM_PROGRAMME_ASSIGNMENT.PROPERTYID, PM_WORKORDER_STATUS.DESCRIPTION, PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID, 
			'N/A' AS WORKORDERID
	FROM    PM_PROGRAMME_ASSIGNMENT 
		INNER JOIN PM_PROGRAMME ON PM_PROGRAMME_ASSIGNMENT.PROGRAMMEID = PM_PROGRAMME.PROGRAMMEID 
		LEFT JOIN PM_WORKORDER_STATUS ON dbo.PM_FN_GETCURRENTWORKSTATUSBYPROPERTYLISTID(PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID) =PM_WORKORDER_STATUS.STATUSID
	WHERE     (PM_PROGRAMME.STATUSID = 1) AND PM_PROGRAMME_ASSIGNMENT.PROPERTYID = @PROPERTYID
		AND PM_PROGRAMME_ASSIGNMENT.STATUSID=1
	--update end

END



	



GO
