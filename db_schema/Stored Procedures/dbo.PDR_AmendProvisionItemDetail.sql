USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_AmendProvisionItemDetail]    Script Date: 10/29/2018 3:53:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

   /* =================================================================================    
    Page Description:    save the detail of Provision Item

    Shaheen Tariq
    Creation Date: Nov-23-2015  
  =================================================================================*/

IF OBJECT_ID('dbo.[PDR_AmendProvisionItemDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_AmendProvisionItemDetail] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_AmendProvisionItemDetail]   
(   
  -- Add the parameters for the stored procedure here   
  @description   nvarchar(100),  
  @type     INT,  
  @provisionCategoryId INT,  
  @quantity    INT,  
  @location    nvarchar(100),  
  @supplierId   INT,  
  @model    nvarchar(100),  
  @serialNumber   nvarchar(100),  
  @bhg     nvarchar(100),  
  @gasAppliance   nvarchar(100),  
  @sluice    nvarchar(100),  
  @ductServicing  nvarchar(100),  
  
  @purchaseDate   smalldatetime,  
  @purchaseCost   decimal(18,0),  
  @warranty    nvarchar(100),  
  @owned    nvarchar(100),  
  @leaseExpiryDate  smalldatetime,  
  @leaseCost   decimal(18,0),  
  @lifeSpan    INT,  
  @lastReplaced   smalldatetime,  
  @replacementDue  smalldatetime,  
  @condition   INT,  
  @patTesting   bit,  
  
  @ProvisionCharge  nvarchar(100),  
  @annualApportionment  nvarchar(100),  
  @blockIdPC   INT,  
    
  @servicingProviderId   INT,  
  @servicingRequired bit,  
  @lastDuctServicingDate smalldatetime,  
  @ductServiceCycle  nvarchar(100),  
  @nextDuctServicingDate smalldatetime,  
    
  @provisionParentId INT Null,  
  
  @schemeId    INT=null,   
  @blockId    INT=null,    
  @updatedBy   INT,     
  @existingProvisionId  INT= null,  
  @ProvisionId INT output,  
  @MSATDetail AS AS_MSATDETAIL readonly  
 )     
AS     
  BEGIN     
      -- SET NOCOUNT ON added to prevent extra result sets from            
      -- interfering with SELECT statements.            
      SET nocount ON;     
  BEGIN TRANSACTION  
  BEGIN TRY    
   IF @existingProvisionId <= 0 OR @existingProvisionId IS NULL   
    BEGIN  
     -- Insert statements for procedure herer  
     INSERT INTO PDR_Provisions(ProvisionDescription,ProvisionType,ProvisionCategoryId,Quantity,ProvisionLocation,SupplierId,Model,SerialNumber,BHG,  
     GasAppliance,Sluice,PurchasedDate,PurchasedCost,Warranty,Owned,LeaseExpiryDate,LeaseCost,LifeSpan, LastReplaced, ReplacementDue,ConditionRating,PATTesting,  
     ProvisionCharge, AnnualApportionmentPC, BlockPC,DuctServicing, ServicingProviderId,ServicingRequired,lastDuctServicingDate,ductServiceCycle,nextDuctServicingDate, IsActive,SchemeId,BlockId,UpdatedBy )  
     VALUES (@description, @type, @provisionCategoryId, @quantity, @location, @supplierId,@model,@serialNumber,@bhg, @gasAppliance, @sluice, @purchaseDate,   
     @purchaseCost,@warranty, @owned, @leaseExpiryDate, @leaseCost, @lifeSpan, @lastReplaced,@replacementDue,@condition, @patTesting,@ProvisionCharge,@annualApportionment,  
      @blockIdPC, @ductServicing, @servicingProviderId,@servicingRequired, @lastDuctServicingDate,@ductServiceCycle, @nextDuctServicingDate, 1, @schemeId,@blockId,@updatedBy)  
  
     SELECT @ProvisionId = SCOPE_IDENTITY()  
       
     INSERT INTO PDR_Provision_History(ProvisionId,ProvisionDescription,ProvisionType,ProvisionCategoryId,Quantity, ProvisionLocation,SupplierId,Model,SerialNumber,BHG,  
     GasAppliance,Sluice,PurchasedDate,PurchasedCost,Warranty,Owned,LeaseExpiryDate,LeaseCost,LifeSpan, LastReplaced, ReplacementDue,ConditionRating,PATTesting,  
     ProvisionCharge, AnnualApportionmentPC, BlockPC,DuctServicing, ServicingProviderId,ServicingRequired,lastDuctServicingDate,ductServiceCycle,nextDuctServicingDate, IsActive,SchemeId,BlockId, UpdatedBy )  
     VALUES (@ProvisionId ,@description, @type, @provisionCategoryId, @quantity, @location, @supplierId,@model,@serialNumber,@bhg, @gasAppliance, @sluice, @purchaseDate,   
     @purchaseCost,@warranty, @owned, @leaseExpiryDate, @leaseCost, @lifeSpan, @lastReplaced,@replacementDue,@condition,@patTesting,@ProvisionCharge,@annualApportionment,  
      @blockIdPC,@ductServicing, @servicingProviderId,@servicingRequired,@lastDuctServicingDate,@ductServiceCycle,@nextDuctServicingDate, 1,@schemeId,@blockId,@updatedBy)  
     END  
   ELSE  
   BEGIN  
     
     Update PDR_Provisions   
     Set ProvisionDescription= @description,ProvisionType=@type,ProvisionCategoryId=@provisionCategoryId,Quantity=@quantity,  
     ProvisionLocation=@location,SupplierId=@supplierId,Model=@model,SerialNumber=@serialNumber,BHG=@bhg, GasAppliance=@gasAppliance,Sluice=@sluice,  
     PurchasedDate=@purchaseDate,PurchasedCost=@purchaseCost,Warranty=@warranty,Owned=@owned, LeaseExpiryDate=@leaseExpiryDate,LeaseCost=@leaseCost,  
     LifeSpan=@lifeSpan, LastReplaced=@lastReplaced, ReplacementDue=@replacementDue,ConditionRating=@condition, PATTesting=@patTesting,ProvisionCharge=@ProvisionCharge,  
     AnnualApportionmentPC=@annualApportionment, BlockPC=@blockIdPC, /*DuctServicing=@ductServicing,*/  
     ServicingProviderId=@servicingProviderId,DuctServicing=@ductServicing,lastDuctServicingDate=@lastDuctServicingDate, ductServiceCycle=@ductServiceCycle,ServicingRequired=@servicingRequired,nextDuctServicingDate=@nextDuctServicingDate,  
     IsActive=1,SchemeId=@schemeId,BlockId=@blockId,UpdatedBy=@updatedBy  
  
     WHERE ProvisionId = @existingProvisionId  
  
     SET @ProvisionId =@existingProvisionId  
       
     INSERT INTO PDR_Provision_History(ProvisionId,ProvisionDescription,ProvisionType,ProvisionCategoryId,Quantity, ProvisionLocation,SupplierId,Model,SerialNumber,BHG,  
     GasAppliance,Sluice,PurchasedDate,PurchasedCost,Warranty,Owned,LeaseExpiryDate,LeaseCost,LifeSpan, LastReplaced, ReplacementDue,ConditionRating,PATTesting,  
     ProvisionCharge, AnnualApportionmentPC, BlockPC, DuctServicing,ServicingProviderId,ServicingRequired, lastDuctServicingDate,ductServiceCycle,nextDuctServicingDate, IsActive,SchemeId,BlockId, UpdatedBy )  
     VALUES (@ProvisionId ,@description, @type, @provisionCategoryId, @quantity, @location, @supplierId,@model,@serialNumber,@bhg, @gasAppliance, @sluice, @purchaseDate,   
     @purchaseCost,@warranty, @owned, @leaseExpiryDate, @leaseCost, @lifeSpan, @lastReplaced,@replacementDue,@condition,@patTesting, @ProvisionCharge,@annualApportionment,  
      @blockIdPC,@ductServicing,@servicingProviderId,@servicingRequired, @lastDuctServicingDate,@ductServiceCycle, @nextDuctServicingDate, 1,@schemeId,@blockId,@updatedBy)  
       
       
       
   END  
     
   Exec PDR_AmendpMSTAItemDetailForProvisions @ProvisionId,@schemeId,@blockId,@UpdatedBy,@MSATDetail       
 END TRY   
 BEGIN CATCH    
  IF @@TRANCOUNT >0     
  BEGIN      
     PRINT 'Some thing went wrong with this transaction'  
   SET @ProvisionId= -1  
   ROLLBACK TRANSACTION;   
  END   
  DECLARE @ErrorMessage NVARCHAR(4000);  
  DECLARE @ErrorSeverity INT;  
  DECLARE @ErrorState INT;  
  
  SELECT @ErrorMessage = ERROR_MESSAGE(),  
  @ErrorSeverity = ERROR_SEVERITY(),  
  @ErrorState = ERROR_STATE();  
  
  -- Use RAISERROR inside the CATCH block to return   
  -- error information about the original error that   
  -- caused execution to jump to the CATCH block.  
  RAISERROR (@ErrorMessage, -- Message text.  
  @ErrorSeverity, -- Severity.  
  @ErrorState -- State.  
  );  
 END CATCH    
  
 IF @@TRANCOUNT >0  
  BEGIN  
   PRINT 'Transaction completed successfully'   
   COMMIT TRANSACTION;  
  END   
END  
        
  