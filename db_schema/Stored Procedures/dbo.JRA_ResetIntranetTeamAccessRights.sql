SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_ResetIntranetTeamAccessRights @jobRoleTeamId=12
-- Author:<Ahmed Mehmood>
-- Create date: <Create Date,,3/1/2012>
-- Description:	<Reset Intranet Team Access Rights>
-- Web Page: JobRoles.aspx
-- =============================================
create PROCEDURE [dbo].[JRA_ResetIntranetTeamAccessRights](
@jobRoleTeamId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM I_PAGE_TEAMJOBROLE
	WHERE I_PAGE_TEAMJOBROLE.TeamJobRoleId = @jobRoleTeamId
           
END
GO
