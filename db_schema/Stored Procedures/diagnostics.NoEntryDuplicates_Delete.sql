SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE diagnostics.NoEntryDuplicates_Delete
AS ;
    WITH    CTE_Dup ( TotalRecords, MinNoEntryId, FaultLogId, Notes, isNoEntryScheduled )
              AS ( SELECT   COUNT(NoEntryID) AS TotalRecords ,
                            MIN(NoEntryID) AS MinNoEntryId ,
                            FaultLogId ,
                            Notes ,
                            isNoEntryScheduled
                   FROM     dbo.FL_FAULT_NOENTRY
                   WHERE    RecordedOn >= '01/01/2013'
                   GROUP BY FaultLogId ,
                            FaultLogId ,
                            Notes ,
                            isNoEntryScheduled
                   HAVING   COUNT(NoEntryID) > 1
                 )
        DELETE  ne
        FROM    dbo.FL_FAULT_NOENTRY ne
                INNER JOIN CTE_Dup dup ON NE.FaultLogId = dup.FaultLogId
                                          AND Ne.NoEntryID <> dup.MinNoEntryId

GO
