USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_SetJobSheetAppointmentStatus]    Script Date: 11/30/2016 2:14:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PLANNED_SetJobSheetAppointmentStatus') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_SetJobSheetAppointmentStatus AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_SetJobSheetAppointmentStatus]
/* ===========================================================================
--	EXEC PLANNED_SetJobSheetAppointmentStatus
		@AppointmentJSNId = 343,		
		@AppointmentStatusId = 14,
		@Notes = 'The Entry was not possible',
		@LastActionDate GetDate(),
		@LastActionUserID 65
--  Author:			Muhammad Awais
--  DATE CREATED:	03 February 2015
--  Description:	To Set Appointment Status to Cancel/Complete, and Insert into Table NoEntry, 
--					 and Insert Tranction into History
--  Webpage:		View/Reports/AssignedToContractor.aspx 
 '==============================================================================*/
@AppointmentJSNId INT,
@AppointmentStatusId INT,
@Notes NVARCHAR(MAX),
@LastActionDate DATETIME,
@OrderId INT,
@LastActionUserID INT,
@Result INT OUTPUT
	
AS
BEGIN
	SET NOCOUNT ON
	
--	BEGIN TRAN
	
		DECLARE @PROPERTYID NVARCHAR(MAX), @COMPONENTID INT, @SCHEMEID INT, @BLOCKID INT
		SELECT @PROPERTYID = PLANNED_JOURNAL.PROPERTYID,
			   @COMPONENTID = PLANNED_JOURNAL.COMPONENTID,
			   @SCHEMEID = PLANNED_JOURNAL.SchemeId,
			   @BLOCKID = PLANNED_JOURNAL.BlockId
		  FROM PLANNED_JOURNAL 
		 WHERE PLANNED_JOURNAL.JOURNALID = @AppointmentJSNId

		---- Update Status (Cancel StatusID) in PLANNED_JOURNAL
		--UPDATE PLANNED_JOURNAL
		--   SET StatusID = @AppointmentStatusId
		-- WHERE PLANNED_JOURNAL.JOURNALID = @AppointmentJSNId

		-- Insert the Cancel tranction in History Table (FL_FAULT_LOG_HISTORY)
		INSERT INTO PLANNED_JOURNAL_HISTORY (JOURNALID, PROPERTYID, COMPONENTID, STATUSID, ACTIONID, CREATIONDATE, 
					CREATEDBY, NOTES, ISLETTERATTACHED, StatusHistoryId ,ActionHistoryId ,IsDocumentAttached, SchemeId, BlockId )
		VALUES (@AppointmentJSNId, @PROPERTYID, @COMPONENTID, @AppointmentStatusId, NULL, @LastActionDate,
					 @LastActionUserID, @Notes, 0, NULL, NULL, 0 , @SCHEMEID, @BLOCKID )


		-- Update Status (To be Arranged StatusID) in PLANNED_JOURNAL
		DECLARE @StatusId INT  
		 SELECT @StatusId = STATUSID  
		   FROM PLANNED_STATUS WHERE Lower( PLANNED_STATUS.TITLE ) = lower( 'To be Arranged' )

		UPDATE PLANNED_JOURNAL  
		   SET PLANNED_JOURNAL.STATUSID = @StatusId  
		 WHERE PLANNED_JOURNAL.JOURNALID = @AppointmentJSNId  
  
		-- Insert the Cancel tranction in History Table (FL_FAULT_LOG_HISTORY)
		INSERT INTO PLANNED_JOURNAL_HISTORY (JOURNALID, PROPERTYID, COMPONENTID, STATUSID, ACTIONID, CREATIONDATE, 
					CREATEDBY, NOTES, ISLETTERATTACHED, StatusHistoryId ,ActionHistoryId ,IsDocumentAttached, SchemeId, BlockId )
		VALUES (@AppointmentJSNId, @PROPERTYID, @COMPONENTID, @StatusId, NULL, @LastActionDate,
					 @LastActionUserID, @Notes, 0, NULL, NULL, 0 , @SCHEMEID, @BLOCKID )

		DECLARE @FREQUENCY NVARCHAR(MAX), @CYCLE INT
		SELECT @FREQUENCY=PLANNED_COMPONENT.FREQUENCY, 
			   @CYCLE=PLANNED_COMPONENT.CYCLE
		FROM   PLANNED_COMPONENT
		WHERE PLANNED_COMPONENT.COMPONENTID=@COMPONENTID

		DECLARE @YearOrMonth NVARCHAR(max)
		select @YearOrMonth=CASE when @FREQUENCY='yrs' then 'year' else 'month' end

		IF(@PROPERTYID IS NOT NULL)
		BEGIN
			UPDATE PA_PROPERTY_ITEM_DATES
			   SET LastDone=convert(date,@LastActionDate), DueDate=convert(date,(select CASE when @FREQUENCY='yrs' then DATEADD(year, @CYCLE, @LastActionDate) else DATEADD(month, @CYCLE, @LastActionDate) end))
			 WHERE PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID=@COMPONENTID AND PA_PROPERTY_ITEM_DATES.PROPERTYID=@PROPERTYID
		END
		ELSE IF(@BLOCKID > 0)
		BEGIN
			UPDATE PA_PROPERTY_ITEM_DATES
			   SET LastDone=convert(date,@LastActionDate), DueDate=convert(date,(select CASE when @FREQUENCY='yrs' then DATEADD(year, @CYCLE, @LastActionDate) else DATEADD(month, @CYCLE, @LastActionDate) end))
			 WHERE PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID=@COMPONENTID AND PA_PROPERTY_ITEM_DATES.BlockId=@BLOCKID
		END
		ELSE IF (@SCHEMEID > 0)
		BEGIN
			UPDATE PA_PROPERTY_ITEM_DATES
			   SET LastDone=convert(date,@LastActionDate), DueDate=convert(date,(select CASE when @FREQUENCY='yrs' then DATEADD(year, @CYCLE, @LastActionDate) else DATEADD(month, @CYCLE, @LastActionDate) end))
			 WHERE PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID=@COMPONENTID AND PA_PROPERTY_ITEM_DATES.SchemeId=@SCHEMEID
		END


		IF @OrderId > 0 
		BEGIN
			-- Update Status (StatusID) in PLANNED_JOURNAL
			UPDATE F_PURCHASEORDER 
			   SET F_PURCHASEORDER.POSTATUS = 2
			 WHERE F_PURCHASEORDER.ORDERID = @OrderId
		END
		--------------------------------
		-- If insertion fails, goto HANDLE_ERROR block
		--IF @@ERROR <> 0 GOTO HANDLE_ERROR
		
		--COMMIT TRAN
		SET @RESULT = 1
		--RETURN
END

--HANDLE_ERROR:
	--ROLLBACK TRAN  
	--SET @RESULT=-1
--RETURN

