
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_Customer_GetCustomerInfo] 
/*
===============================================================
'   NAME:           TO_Customer_GetCustomerInfo
'   DATE CREATED:   26 MAY 2008
'   CREATED BY:     Naveed Iqbal
'   CREATED FOR:    Broadland Housing
'   PURPOSE:        To retrieve the Customer information to be displayed on welcome page of the application
'   IN:             @CustomerId 
'   OUT: 		    No     
'   RETURN:         Nothing    
'   VERSION:        1.0           
'   COMMENTS:       
'   MODIFIED:       [02/05/2013]  [Nataliya Alexander]  Added EMPLOYMENTSTATUS column.
'===============================================================
*/ ( @CustomerId INT )
AS 
    SELECT  
	        CustomerAddress.ADDRESS1 ,
	        CustomerAddress.ADDRESS2 ,
	        CustomerAddress.ADDRESS3 ,
	        Customer.BANKFACILITY ,
	        Customer.BANKFACILITYOTHER ,
	        Customer.BENEFIT ,
	        Customer.CARER ,
	        Customer.COMMUNICATION ,
	        Customer.COMMUNICATIONOTHER ,
	        CustomerAddress.COUNTY ,
	        Customer.CUSTOMERID ,
	        Customer.DISABILITY ,
	        Customer.DISABILITYOTHER ,
	        Customer.DOB ,
	        CustomerAddress.EMAIL ,
	        Customer.EMAILYESNO ,
	        Customer.EMPADDRESS1 ,
	        Customer.EMPADDRESS2 ,
	        Customer.EMPLOYERNAME ,
	        Customer.EMPLOYMENTSTATUS ,
	        Customer.EMPPOSTCODE ,
	        Customer.EMPTOWNCITY ,
	        Customer.ETHNICORIGIN ,
	        Customer.ETHNICORIGINOTHER ,
	        Customer.FIRSTLANGUAGE ,
	        Customer.FIRSTNAME ,
	        Customer.GENDER ,
	        Customer.HOUSEINCOME ,
	        CustomerAddress.HOUSENUMBER ,
	        Customer.INTERNETACCESS ,
	        Customer.INTERNETACCESSOTHER ,
	        Customer.LASTNAME ,
	        Customer.LOCALAUTHORITY ,
	        Customer.MARITALSTATUS ,
	        Customer.MIDDLENAME ,
	        CustomerAddress.MOBILE ,
	        CustomerAddress.MOBILE2 ,
	        Customer.NATIONALITY ,
	        Customer.NATIONALITYOTHER ,
	        Customer.NINUMBER ,
	        Customer.OCCUPANTSADULTS ,
	        Customer.OCCUPANTSCHILDREN ,
	        Customer.OCCUPATION ,
	        CustomerAddress.POSTCODE ,
	        Customer.PREFEREDCONTACT ,
	        Customer.RELIGION ,
	        Customer.RELIGIONOTHER ,
	        Customer.SEXUALORIENTATION ,
	        Customer.SEXUALORIENTATIONOTHER ,
	        Customer.SUPPORTAGENCIES ,
	        Customer.SUPPORTAGENCIESOTHER ,
	        Customer.TAKEHOMEPAY ,
	        CustomerAddress.TEL ,
	        CustomerAddress.TELRELATIONSHIP ,
	        CustomerAddress.TELRELATIVE ,
	        CustomerAddress.TELWORK ,
	        Customer.TEXTYESNO ,
	        Customer.TITLE ,
	        Customer.TITLEOTHER ,
	        CustomerAddress.TOWNCITY ,
	        Customer.WHEELCHAIR ,
	        Customer.CONSENT ,
	        Customer.OWNTRANSPORT
    FROM    
			C__CUSTOMER Customer INNER JOIN C_ADDRESS CustomerAddress ON Customer.CUSTOMERID = CustomerAddress.CUSTOMERID
    WHERE   
			Customer.CUSTOMERID = @CustomerId
            AND 
            CustomerAddress.ISDEFAULT = 1



GO
