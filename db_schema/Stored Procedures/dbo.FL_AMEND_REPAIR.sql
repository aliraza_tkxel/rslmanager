SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[FL_AMEND_REPAIR](
/* ===========================================================================
 '   NAME:          FL_AMEND_REPAIR
 '   DATE CREATED:  2nd Dec, 2008
 '   CREATED BY:    Noor Muhammad
 '   CREATED FOR:   Broadland Housing
 '   AMEND A FAULT IN FL_FAULT_REPAIR_LIST TABLE
 '
 '   IN:	       @FAULTREPAIRLISTID INT , 
 '   IN:		   @NETCOST FLOAT ,
 '   IN:           @Vat FLOAT,
 '   IN:	       @GROSS FLOAT,
 '   IN:	       @VATRATEID INT ,
 '   IN:	       @POSTINSPECTION BIT,
 '   IN:	       @STOCKCONDITIONITEM BIT,
 '   IN:	       @FAULTRPAIR BIT,
 '
 '   OUT:          @RESULT    
 '   RETURN:       Nothing    
 '   VERSION:      1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@FAULTREPAIRLISTID INT,
@DESCRIPTION VARCHAR(500),
@NETCOST FLOAT ,
@Vat FLOAT,
@GROSS FLOAT,
@VATRATEID INT ,
@POSTINSPECTION BIT,
@STOCKCONDITIONITEM BIT,
@REPAIRACTIVE BIT,
@PARAMETERID INT,
@RESULT  INT OUTPUT
)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN

UPDATE FL_FAULT_REPAIR_LIST SET 
	DESCRIPTION =@DESCRIPTION, NETCOST=@NETCOST, VAT=@VAT,GROSS=@GROSS, VATRATEID=@VATRATEID, POSTINSPECTION=@POSTINSPECTION,STOCKCONDITIONITEM=@STOCKCONDITIONITEM,RepairActive=@REPAIRACTIVE, PMParameterID = @PARAMETERID
	WHERE FaultRepairListID= @FAULTREPAIRLISTID

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	

SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN





/****** Object:  StoredProcedure [dbo].[FL_REPAIR_GETVALUES]    Script Date: 08/25/2009 17:02:10 ******/
SET ANSI_NULLS ON
GO
