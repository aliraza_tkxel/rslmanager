SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[stp_CustomerUpdateStatus]

@statusid int,
@jobid int

AS

UPDATE H_REQUEST_JOURNAL SET STATUS_ID = @statusid WHERE JOB_ID = @jobid

GO
