SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC: [dbo].[AS_GetFromUserByTeamID]
--	@TeamId=1
-- Author:		Hussain Ali
-- Create date: 10/15/2012
-- Description:	This SP returns all the users who are active against a team id
-- Web page: view Letter
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetFromUserByTeamID]
	-- Add the parameters for the stored procedure here
	@TeamID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		E__EMPLOYEE.FIRSTNAME,
		E__EMPLOYEE.LASTNAME,
		E__EMPLOYEE.EMPLOYEEID
	FROM
		E__EMPLOYEE
		INNER JOIN AC_LOGINS ON E__EMPLOYEE.EMPLOYEEID = AC_LOGINS.EMPLOYEEID
		INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
		INNER JOIN E_TEAM ON	E_JOBDETAILS.TEAM = E_TEAM.TEAMID
	WHERE
		E_TEAM.TEAMID = @TeamID AND
		AC_LOGINS.ACTIVE = 1
END
GO
