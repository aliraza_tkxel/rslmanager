SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_GET_C_REPORT_NATURE]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GET_C_REPORT_NATURE
 '   DATE CREATED:   17 JUNE 2008
 '   CREATED BY:     Zahid Ali
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get nature id  and its description for populating nature dropdown on
 '					 customer enquiry page
 '   IN:             Nothing
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	AS
	
	SELECT  
			C_NATURE.ITEMNATUREID As id,
			C_NATURE.DESCRIPTION As val
            
            
	FROM
	       C_NATURE 
	       
	WHERE
	
	     (C_NATURE.ITEMID = 4)
    ORDER BY C_NATURE.DESCRIPTION ASC 	     

GO
