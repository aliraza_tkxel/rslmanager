
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ARREARS_CUSTOMER_BALANCE_AND_HB_ALL]
AS 
/* ===========================================================================
 '   NAME:           ARREARS_CUSTOMER_BALANCE_AND_HB_ALL
 '   DATE CREATED:   24 JUNE 2013
 '   CREATED BY:     Nataliya Alexander
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select net, gross and housing benefit anticipated or 
 '                   paid in advance                    
 '   IN:             
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       The logic to calculate housing benefit amount was copied over
 '                   from ARREARS_GET_SINGLE_CUSTOMER_HB
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 
    BEGIN 

        SET NOCOUNT ON

        DECLARE @THEDATE SMALLDATETIME
        DECLARE @REQUIREDDATE SMALLDATETIME
        DECLARE @CURRENTDATESTRING VARCHAR(40)
        DECLARE @COMPAREDATE SMALLDATETIME
        DECLARE @NEXTMONTHDATE SMALLDATETIME
        DECLARE @RESPECTIVEENDDATE SMALLDATETIME
        DECLARE @YEAREND SMALLDATETIME
        DECLARE @YEARSTART SMALLDATETIME
        DECLARE @HB_DR MONEY
        DECLARE @TENANCYSTART SMALLDATETIME
        DECLARE @LASTPAYMENTENDDATE SMALLDATETIME
        DECLARE @ADJ_MONTH INT
        DECLARE @NON_REQUIRED_DAYS INT
        DECLARE @A_12TH_OF_A_YEAR FLOAT
        DECLARE @TOTAL_HB_OWED_ON_DATE MONEY
        DECLARE @HB_OWED MONEY
        DECLARE @ADJ_YEARSTART SMALLDATETIME
        DECLARE @ADJ_YEAREND SMALLDATETIME
        DECLARE @MASTERAMOUNT MONEY
        DECLARE @CUSTOMERID INT
        DECLARE @ROW INT
        DECLARE @TENANCYID INT


        SET @MASTERAMOUNT = 0
        SET @THEDATE = GETDATE()


        DECLARE @tblBaseData TABLE
            (
              CUSTOMERID INT ,
              TENANCYID INT ,
              TENANCYSTART DATETIME ,
              LASTPAYMENTENDDATE DATETIME ,
              HB_DR FLOAT
            )

        DECLARE @tblTenancyData TABLE
            (
              ROW INT IDENTITY(1, 1)
                      NOT NULL ,
              CUSTOMERID INT ,
              TENANCYID INT ,
              TENANCYSTART DATETIME ,
              LASTPAYMENTENDDATE DATETIME ,
              HB_DR FLOAT
            )

        DECLARE @tblResult TABLE
            (
              TENANCYID INT ,
              ANTHB MONEY ,
              ADVHB MONEY
            )

        WITH    CTE_BaseData ( CUSTOMERID, TENANCYID, TENANCYSTART, LASTPAYMENTENDDATE, HB_DR )
                  AS ( SELECT   HBI.CUSTOMERID ,
                                T.TENANCYID ,
                                HBI.initialSTARTDATE ,
                                ISNULL(HBA.ENDDATE,
                                       DATEADD(D, -1, HBI.INITIALSTARTDATE)) ,
                                HB_DR = CASE WHEN HBA.HBID IS NOT NULL
                                             THEN ( (ABS(HBA.HB)
                                                    / ( DATEDIFF(D,
                                                              HBA.STARTDATE,
                                                              HBA.ENDDATE) + 1 )) )
                                             WHEN HBA.HBID IS NULL
                                             THEN ( (ABS(HBI.INITIALPAYMENT)
                                                    / ( DATEDIFF(D,
                                                              HBI.INITIALSTARTDATE,
                                                              HBI.INITIALENDDATE)
                                                        + 1 )) )
                                        END
                       FROM     C_TENANCY T
                                INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID
                                INNER JOIN F_HBINFORMATION HBI ON HBI.TENANCYID = T.TENANCYID
                                LEFT JOIN F_HBACTUALSCHEDULE HBA ON HBA.HBID = HBI.HBID
                                                              AND HBA.HBROW = ( SELECT
                                                              MAX(HBROW)
                                                              FROM
                                                              F_HBACTUALSCHEDULE
                                                              WHERE
                                                              HBID = HBA.HBID
                                                              AND VALIDATED = 1
                                                              )
                                LEFT JOIN F_HBACTUALSCHEDULE HBA2 ON HBA2.HBID = HBI.HBID
                                                              AND HBA2.HBROW = ( SELECT
                                                              MIN(HBROW)
                                                              FROM
                                                              F_HBACTUALSCHEDULE
                                                              WHERE
                                                              HBID = HBA2.HBID
                                                              AND VALIDATED IS NULL
                                                              )
                       WHERE    HBI.ACTUALENDDATE IS NULL
                                AND ( (( T.ENDDATE IS NULL
                                         OR T.ENDDATE >= ''
                                         + CAST(@THEDATE AS VARCHAR) + ''
                                       )
                                      AND ( T.STARTDATE < ''
                                            + CAST(@THEDATE AS VARCHAR) + '' ))
                                    )
                     )
            INSERT  INTO @tblBaseData
                    ( CUSTOMERID ,
                      TENANCYID ,
                      TENANCYSTART ,
                      LASTPAYMENTENDDATE ,
                      HB_DR
                    )
                    SELECT  CUSTOMERID ,
                            TENANCYID ,
                            TENANCYSTART ,
                            LASTPAYMENTENDDATE ,
                            HB_DR
                    FROM    CTE_BaseData
	

	
-- LOOP THROUGH EACH TENANCY
        WHILE ( SELECT  COUNT(*)
                FROM    @tblBaseData
              ) > 0 
            BEGIN
	
                SET @TENANCYID = ( SELECT TOP ( 1 )
                                            TENANCYID
                                   FROM     @tblBaseData
                                 )
                PRINT CAST(@TENANCYID AS VARCHAR)
	
                INSERT  INTO @tblTenancyData
                        ( CUSTOMERID ,
                          TENANCYID ,
                          TENANCYSTART ,
                          LASTPAYMENTENDDATE ,
                          HB_DR
                        )
                        SELECT  CUSTOMERID ,
                                TENANCYID ,
                                TENANCYSTART ,
                                LASTPAYMENTENDDATE ,
                                HB_DR
                        FROM    @tblBaseData
                        WHERE   TENANCYID = @TENANCYID
	
                INSERT  INTO @tblResult
                        ( TENANCYID, ANTHB, ADVHB )
                VALUES  ( @TENANCYID, 0, 0 )
	

-- LOOP THROUGH EACH CUSTOMER IN THE SELECTED TENANCY
--	: WE DO THIS BECAUSE EACH CUSTOMER MIGHT HAVE THERE OWN HB SET UP
-- 	: WE TRY TO NOT ADD IN AN HB VALUE WHICH HAS NOT BEEN ENDED ON AN ENDED TENANCY WHOS CUSTOMER HAVE SET UP A NEW TENANCY AND HB SCHEDULE


                WHILE ( SELECT  COUNT(*)
                        FROM    @tblTenancyData
                      ) > 0 
                    BEGIN
	     
 
                        SELECT TOP ( 1 )
                                @CUSTOMERID = CUSTOMERID ,
                                @TENANCYSTART = TENANCYSTART ,
                                @LASTPAYMENTENDDATE = LASTPAYMENTENDDATE ,
                                @HB_DR = HB_DR ,
                                @ROW = ROW
                        FROM    @tblTenancyData
                        ORDER BY ROW ASC
	     
                        PRINT CAST(@TENANCYID AS VARCHAR) + ' - '
                            + CAST(@ROW AS VARCHAR)
	
                        SET @MASTERAMOUNT = 0
				
                        SET @REQUIREDDATE = '1 ' + DATENAME(M, GETDATE())
                            + ' ' + CAST(DATEPART(YYYY, GETDATE()) AS VARCHAR)
				
                        SET @CURRENTDATESTRING = '1 ' + DATENAME(M,
                                                              @TENANCYSTART)
				
                        SET @COMPAREDATE = @CURRENTDATESTRING COLLATE DATABASE_DEFAULT + ' '
                            + CAST(DATEPART(YYYY, GETDATE()) AS VARCHAR) COLLATE DATABASE_DEFAULT
				
                        SET @NEXTMONTHDATE = DATEADD(M, 1, @REQUIREDDATE)
				
							
                        IF ( @REQUIREDDATE ) >= ( @COMPAREDATE ) 
                            BEGIN
                                SET @YEARSTART = @COMPAREDATE					
                                SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1,
                                                              @YEARSTART)	
                                SET @RESPECTIVEENDDATE = DATEADD(D, -1,
                                                              @RESPECTIVEENDDATE)
                                SET @YEAREND = @RESPECTIVEENDDATE					
				
                            END
                        ELSE 
                            BEGIN
                                SET @YEARSTART = @CURRENTDATESTRING COLLATE DATABASE_DEFAULT
                                    + CAST(( DATEPART(YYYY, GETDATE()) - 1 ) AS VARCHAR)
                                SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1,
                                                              @YEARSTART)
                                SET @RESPECTIVEENDDATE = DATEADD(D, -1,
                                                              @RESPECTIVEENDDATE)				
                                SET @YEAREND = @RESPECTIVEENDDATE
				    
                            END
				
				
                        SET @ADJ_MONTH = DATEDIFF(M, @YEARSTART, GETDATE())
                            + 1
				
								
                        IF ( DATEADD(D, 1, @LASTPAYMENTENDDATE) ) < @YEARSTART 
                            BEGIN
					
                                SET @NON_REQUIRED_DAYS = 0
                                SET @A_12TH_OF_A_YEAR = 0.0
                                SET @TOTAL_HB_OWED_ON_DATE = 0
                                SET @A_12TH_OF_A_YEAR = ( DATEDIFF(D,
                                                              @YEARSTART,
                                                              @YEAREND) + 1 )
                                SET @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12
							
                                SET @HB_OWED = ( ( @ADJ_MONTH
                                                   * @A_12TH_OF_A_YEAR )
                                                 - @NON_REQUIRED_DAYS )
                                    * @HB_DR
					
                                SET @ADJ_YEARSTART = DATEADD(YYYY, -1,
                                                             @YEARSTART)
                                SET @ADJ_YEAREND = DATEADD(YYYY, -1, @YEAREND)
					
					--loop whilst we still have whole years to the LAST_PAYMENT_END
                                WHILE ( @LASTPAYMENTENDDATE < @ADJ_YEARSTART ) 
                                    BEGIN
                                        SET @HB_OWED = @HB_OWED + ( @HB_DR
                                                              * ( DATEDIFF(D,
                                                              @ADJ_YEARSTART,
                                                              @ADJ_YEAREND)
                                                              + 1 ) )			
                                        SET @ADJ_YEARSTART = DATEADD(YYYY, -1,
                                                              @ADJ_YEARSTART)
                                        SET @ADJ_YEAREND = DATEADD(YYYY, -1,
                                                              @ADJ_YEAREND)

                                    END		
						
                                SET @NON_REQUIRED_DAYS = DATEDIFF(D,
                                                              @ADJ_YEARSTART,
                                                              @LASTPAYMENTENDDATE)
						
                                SET @A_12TH_OF_A_YEAR = DATEDIFF(D,
                                                              @ADJ_YEARSTART,
                                                              @ADJ_YEAREND)
                                    + 1
                                SET @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12
						
                                SET @TOTAL_HB_OWED_ON_DATE = @HB_OWED
                                    + ( ( ( 12 * @A_12TH_OF_A_YEAR )
                                          - @NON_REQUIRED_DAYS ) * @HB_DR )
				 		
                                SET @MASTERAMOUNT = @MASTERAMOUNT
                                    + ISNULL(@TOTAL_HB_OWED_ON_DATE, 0)
											
						
                                SET @TOTAL_HB_OWED_ON_DATE = 0
                                SET @HB_OWED = 0.00
                            END
                        ELSE 
                            BEGIN
                                SET @A_12TH_OF_A_YEAR = 0.0
                                SET @NON_REQUIRED_DAYS = DATEDIFF(D,
                                                              @YEARSTART,
                                                              @LASTPAYMENTENDDATE)
                                    + 1
					
                                SET @A_12TH_OF_A_YEAR = ( DATEDIFF(D,
                                                              @YEARSTART,
                                                              @YEAREND) + 1 )
                                SET @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12
                                SET @TOTAL_HB_OWED_ON_DATE = ( ( ( @ADJ_MONTH
                                                              * @A_12TH_OF_A_YEAR )
                                                              - @NON_REQUIRED_DAYS )
                                                              * @HB_DR )
                                SET @MASTERAMOUNT = @MASTERAMOUNT
                                    + ISNULL(@TOTAL_HB_OWED_ON_DATE, 0)
					
                                SET
                                    SET @TOTAL_HB_OWED_ON_DATE = 0
										  
                            END
				-- IF ANY HB HAS BEEN PAID FOR THE FUTURE THEN WE NEED TO ADD THIS TO THE GROSS COST
				-- SO THEN WE DONT ADD IT TO THE ANTICPATED HB
                        IF ISNULL(@MASTERAMOUNT, 0) < 0 
                            BEGIN
                                UPDATE  @tblResult
                                SET     ADVHB = ISNULL(ADVHB, 0)
                                        + ISNULL(ABS(@MASTERAMOUNT), 0) ,
                                        ANTHB = ISNULL(ANTHB, 0)
                                WHERE   TENANCYID = @TENANCYID
                            END
                        ELSE 
                            BEGIN		
                                UPDATE  @tblResult
                                SET     ANTHB = ISNULL(ANTHB, 0)
                                        + ISNULL(ABS(@MASTERAMOUNT), 0) ,
                                        ADVHB = ISNULL(ADVHB, 0)
                                WHERE   TENANCYID = @TENANCYID
                            END 

			   
                        SET @MASTERAMOUNT = 0
   
   -- DELETE PROCESSED CUSTOMER ROW
                        DELETE  @tblTenancyData
                        WHERE   ROW = @ROW
					
                    END			


-- DELETE PROCESSED TENANCY ROW
                DELETE  @tblBaseData
                WHERE   TENANCYID = @TENANCYID
	
            END


        WITH    CTE_BALANCE ( AMOUNT, TENANCYID )
                  AS ( SELECT   SUM(ISNULL(J.AMOUNT, 0)) AS AMOUNT ,
                                J.TENANCYID
                       FROM     F_RENTJOURNAL J
                                LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID
                                LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID
                                LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID
                       WHERE    ( J.STATUSID NOT IN ( 1, 4 )
                                  OR J.PAYMENTTYPE IN ( 17, 18 )
                                )
                                AND J.ITEMTYPE IN ( 1, 8, 9, 10 )
                       GROUP BY J.TENANCYID
                     )
            SELECT  B.TENANCYID ,
                    B.AMOUNT AS GROSSRENTBALANCE,
                    ISNULL(R.ANTHB, 0) AS ESTIMATEDHB ,
                    ISNULL(ADVHB, 0) AS ADVANCEDHB ,
                    ( CASE WHEN ISNULL(R.ANTHB, 0) > 0
                           THEN B.AMOUNT - ISNULL(R.ANTHB, 0)
                           ELSE B.AMOUNT + ISNULL(ADVHB, 0)
                      END ) AS NETRENTBALANCE
            FROM    CTE_BALANCE B
                    LEFT JOIN @tblResult R ON b.TENANCYID = r.TenancyId

    END



GO
GRANT EXECUTE ON  [dbo].[ARREARS_CUSTOMER_BALANCE_AND_HB_ALL] TO [QlikView]
GO
