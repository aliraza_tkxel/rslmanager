USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.DF_GetSchemeBlockDefectsList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetSchemeBlockDefectsList AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[DF_GetSchemeBlockDefectsList]
	-- Add the parameters for the stored procedure heres
		@propertyId int, 
		@requestType varchar(20),
		@applianceType varchar(500) = 'All',  
		@year INT = -1,  
		@defectType varchar(500) = '-1'
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE      
		@SelectClause NVARCHAR(MAX),  
        @fromClause   NVARCHAR(MAX),  
        @GroupClause  NVARCHAR(MAX),  
        @whereClause  NVARCHAR(MAX),           
        @orderClause  NVARCHAR(MAX),   
        @mainSelectQuery NVARCHAR(MAX),          
        @rowNumberQuery NVARCHAR(MAX),  
        @finalQuery NVARCHAR(MAX),  
        @subquerySearchCriteria NVARCHAR(MAX)



 
  --===========================================================================  
  -- SELECT CLAUSE
  --=========================================================================== 
  
    SET @SelectClause = ' SELECT 
    ISNULL(P_DEFECTS_CATEGORY.Description,''N/A'') AS Category 
	,ISNULL(	CASE 
										WHEN P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 THEN 
											dbo.GetPropertyParameterValue(P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId,''Boiler Room'',''Boiler Type'')
										ELSE
											GS_APPLIANCE_TYPE.APPLIANCETYPE 
										END, ''N/A'' ) AS ApplianceType
	,ISNULL(CONVERT(NVARCHAR,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME),''N/A'') AS RecordedBy
	,CASE WHEN P_PROPERTY_APPLIANCE_DEFECTS.IsActionTaken =0 THEN ''No'' WHEN  P_PROPERTY_APPLIANCE_DEFECTS.IsActionTaken =1 THEN ''Yes'' ELSE ''N/A'' END As IsActionTaken
	,CASE WHEN P_PROPERTY_APPLIANCE_DEFECTS.IsDisconnected=0 THEN ''No'' WHEN P_PROPERTY_APPLIANCE_DEFECTS.IsDisconnected=1 THEN ''Yes'' ELSE ''N/A'' END As IsDiconnected
	,CASE WHEN P_PROPERTY_APPLIANCE_DEFECTS.IsPartsOrdered=0 THEN ''No'' WHEN P_PROPERTY_APPLIANCE_DEFECTS.IsPartsOrdered=1 THEN ''Yes'' ELSE ''N/A'' END As IsPartsOrdered
	,''N/A'' As PartsFitted
	
	  '  	
  --===========================================================================  
  -- FROM CLAUSE
  --=========================================================================== 	
	  SET @fromClause = CHAR(10) +'	FROM P_PROPERTY_APPLIANCE_DEFECTS
	LEFT JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
	LEFT JOIN GS_APPLIANCE_TYPE ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_APPLIANCE_TYPE.APPLIANCETYPEID
	LEFT JOIN PA_PARAMETER_VALUE ON P_PROPERTY_APPLIANCE_DEFECTS.BOILERTYPEID = PA_PARAMETER_VALUE.VALUEID
	INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
	LEFT JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
' 	
  
  --===========================================================================  
  -- SEARCH CRITERIA
  --===========================================================================
  if @requestType = 'Scheme'
  begin
	SET @subquerySearchCriteria = '	1 = 1  
									AND (((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
									AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
									AND CONVERT(VARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.SchemeId) = '+ Convert(varchar,@propertyId) + ''
  end
  else
  begin
	SET @subquerySearchCriteria = '	1 = 1  
									AND (((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
									AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
									AND CONVERT(VARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.BlockId) = '+  Convert(varchar,@propertyId) +''
  end
  
  
  IF @year <> -1  
  BEGIN  
   SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) +' AND DATEPART(yyyy, P_PROPERTY_APPLIANCE_DEFECTS.DefectDate)= ' + CONVERT(varchar(4),@year) 
  END 

 
  
   IF @defectType = 'appliance'
		BEGIN
		
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
			END
			ELSE
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 '
			END
		
		END
		ELSE IF @defectType = 'boiler'
		BEGIN
			
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')'
			END
			ELSE
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 '
			END
			
		END
		ELSE
		BEGIN
				
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND  ( (LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')) OR
																							(LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%''))
																						   ) '
			END
				
		END	
		
  --===========================================================================  
  -- WHERE CLAUSE
  --===========================================================================  
    
  SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @subquerySearchCriteria  
  
  --===========================================================================  
  -- ORDER BY CLAUSE
  --=========================================================================== 
  
  SET @orderClause =  CHAR(10) + ' Order By PropertyDefectId DESC '
  
  --===========================================================================  
  -- MAIN QUERY
  --===========================================================================  
   Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
   
   PRINT @mainSelectQuery
   EXEC (@mainSelectQuery)
   
 
END
