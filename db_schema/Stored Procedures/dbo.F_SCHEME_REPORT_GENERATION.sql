SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Martin L
-- Create date: 6 June 07
-- Description:	A report of income and expense broken down by scheme and accountnumber
-- Example: EXEC [F_SCHEME_REPORT_GENERATION] 9
-- Updated by: Umair 
-- Update Date :30 May 2007
-- Reason : Giving incorrect values 
-- =============================================
ALTER PROCEDURE [dbo].[F_SCHEME_REPORT_GENERATION]
	-- Add the parameters for the stored procedure here
	@FISCALYEAR INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @STARTDATE SMALLDATETIME, @ENDDATE SMALLDATETIME

--	SET @STARTDATE = '01 APR 06'	--SET @ENDDATE = '31 MAR 07'
	SELECT @STARTDATE = YSTART, @ENDDATE = YEND 
	FROM F_FISCALYEARS 
	WHERE YRange = @FISCALYEAR

	DELETE FROM F_SCHEME_REPORT_DATA WHERE FISCALYEARID = @FISCALYEAR

	/*  --TEMPLATE
	SELECT J.TXNID, J.TRANSACTIONID,A.ACCOUNTNUMBER,J.TXNDATE,T.DESCRIPTION AS TRANSACTIONTYPE, U.DEBIT, U.CREDIT, J.DESCRIPTION, U.MEMO
	FROM NL_JOURNALENTRY J 
		INNER JOIN ( 	
			SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYDEBITLINE  			
			UNION ALL 	
			SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYCREDITLINE 
		) U ON J.TXNID = U.TXNID 
		INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, FULLNAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
		LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 
	WHERE J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
	*/

--	INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID)
--	SELECT ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID
--	FROM (

		-- TRANSACTIONTYPE = 7 - RD RENT DUE!
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4020' AS ACCOUNTNUMBER,'RD' AS TRANSACTIONTYPE, 0 AS DEBIT, ISNULL(SUM(RENT),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4030','RD', 0 AS DEBIT, ISNULL(SUM(GARAGE),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4040','RD', 0 AS DEBIT, ISNULL(SUM(SERVICES),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4050','RD', 0 AS DEBIT, ISNULL(SUM(SUPPORTEDSERVICES),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4060','RD', 0 AS DEBIT, ISNULL(SUM(INELIGSERV),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4070','RD', 0 AS DEBIT, ISNULL(SUM(COUNCILTAX),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4080','RD', 0 AS DEBIT, ISNULL(SUM(WATERRATES),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_MONTHLY RJ
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RJ.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
		--END RENT DUE

--		UNION ALL

		--VOID RENTS
		-- BEAR IN MIND THAT THERE ARE TWO PARTS!!
		-- 1) F_RENTJOURNAL_VOIDS
		-- 2) P_LETTINGVOIDENTRIES
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4020','VR', 0 AS DEBIT, ISNULL(SUM(RENT),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4520','VR', ISNULL(SUM(RENT),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID

--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4030','VR', 0 AS DEBIT, ISNULL(SUM(GARAGE),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4530','VR', ISNULL(SUM(GARAGE),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID

--		UNION ALL 

		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4040','VR', 0 AS DEBIT, ISNULL(SUM(SERVICES),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4540','VR', ISNULL(SUM(SERVICES),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID

--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4050','VR', 0 AS DEBIT, ISNULL(SUM(SUPPORTEDSERVICES),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4550','VR', ISNULL(SUM(SUPPORTEDSERVICES),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID

--		UNION ALL 

		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4060','VR', 0 AS DEBIT, ISNULL(SUM(INELIGSERV),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4560','VR', ISNULL(SUM(INELIGSERV),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID


--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4070','VR', 0 AS DEBIT, ISNULL(SUM(COUNCILTAX),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4570','VR', ISNULL(SUM(COUNCILTAX),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID

--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4080','VR', 0 AS DEBIT, ISNULL(SUM(WATERRATES),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4580','VR', ISNULL(SUM(WATERRATES),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM F_RENTJOURNAL_VOIDS RV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON RV.PROPERTYID = P.PROPERTYID
		WHERE TRANSACTIONDATE BETWEEN @STARTDATE AND @ENDDATE
			AND STATUS IN (1,2)
		GROUP BY P.SCHEMEID


		-- ADD IN P_LETTINGVOID ENTRIES
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4020','VR', 0 AS DEBIT, ISNULL(SUM(RENT_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4520','VR', ISNULL(SUM(RENT_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4030','VR', 0 AS DEBIT, ISNULL(SUM(GARAGE_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4530','VR', ISNULL(SUM(GARAGE_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4040','VR', 0 AS DEBIT, ISNULL(SUM(SERVICES_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4540','VR', ISNULL(SUM(SERVICES_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4050','VR', 0 AS DEBIT, ISNULL(SUM(SUPPORTEDSERVICES_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4550','VR', ISNULL(SUM(SUPPORTEDSERVICES_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4060','VR', 0 AS DEBIT, ISNULL(SUM(INELIGSERV_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4560','VR', ISNULL(SUM(INELIGSERV_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4070','VR', 0 AS DEBIT, ISNULL(SUM(COUNCILTAX_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4570','VR', ISNULL(SUM(COUNCILTAX_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4080','VR', 0 AS DEBIT, ISNULL(SUM(WATERRATES_PART),0) AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID
--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT '4580','VR', ISNULL(SUM(WATERRATES_PART),0) AS DEBIT, 0 AS CREDIT, P.SCHEMEID, @FISCALYEAR
		FROM P_LETTINGVOIDENTRIES LV
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON LV.PROPERTYID = P.PROPERTYID
		WHERE LVDATE BETWEEN @STARTDATE AND @ENDDATE
		GROUP BY P.SCHEMEID 
		--END VOID RENTS

--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, D.SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

			LEFT JOIN F_RENTJOURNAL_ERRORS_MONTHLY RJ ON j.transactionid = RJ.JOURNALID
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = RJ.PROPERTYID
			LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID
		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 29		-- TRANSACTIONTYPE EP - MONTHLY RENT ERRORS
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION, D.SCHEMEID

--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, D.SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

			LEFT JOIN (SELECT JOURNALID, TENANCYID FROM F_RENTJOURNAL) RJ ON J.TRANSACTIONID = RJ.JOURNALID
			LEFT JOIN (SELECT TENANCYID, PROPERTYID FROM C_TENANCY) CT ON CT.TENANCYID = RJ.TENANCYID 
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = CT.PROPERTYID
			LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID
		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 8		-- RENT RECEIPTS
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION, D.SCHEMEID

--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, D.SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

			LEFT JOIN (SELECT JOURNALID, TENANCYID FROM F_RENTJOURNAL) RJ ON J.TRANSACTIONID = RJ.JOURNALID
			LEFT JOIN (SELECT TENANCYID, PROPERTYID FROM C_TENANCY) CT ON CT.TENANCYID = RJ.TENANCYID 
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = CT.PROPERTYID
			LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID
		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 11
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION, D.SCHEMEID

--		UNION ALL 
		-- Year End Accruals
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT ACCOUNTNUMBER, 'YRA' AS TRANSACTIONTYPE, SUM(DEBIT) AS DEBIT, SUM(CREDIT) AS CREDIT, SCHEMEID, @FISCALYEAR
		FROM (

			SELECT --J.TXNID, J.TRANSACTIONID,A.ACCOUNTNUMBER,J.TXNDATE,T.DESCRIPTION AS TRANSACTIONTYPE, U.DEBIT, U.CREDIT, J.DESCRIPTION, U.MEMO
					--A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE
					--, COALESCE(W.SCHEMEID, PO.SCHEMEID, D.SCHEMEID, B.SCHEMEID) AS SCHEMEID, PI.ORDERITEMID
					A.ACCOUNTNUMBER ,'YRA' AS TRANSACTIONTYPE, 
					CASE WHEN TXNDATE = @ENDDATE THEN DTL.PIGROSSCOST ELSE 0 END AS DEBIT,
					CASE WHEN TXNDATE = @STARTDATE THEN DTL.PIGROSSCOST ELSE 0 END AS CREDIT, 
					COALESCE(W.SCHEMEID, PO.SCHEMEID, D.SCHEMEID, B.SCHEMEID) AS SCHEMEID,
					DTL.PIGROSSCOST, TXNDATE
			FROM NL_YEARENDACCRAL Y 
				INNER JOIN NL_YEARENDACCRAL_GOUP G ON G.YACCRALID = Y.YENDACCRLID
				INNER JOIN NL_YEARENDACCRAL_DTL DTL ON DTL.ACCGROUPID = G.ACCGROUPID
				INNER JOIN (
					SELECT J.TRANSACTIONID, J.TXNDATE
					FROM NL_JOURNALENTRY J 
						INNER JOIN ( 	
							SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYDEBITLINE  			
							UNION ALL 	
							SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYCREDITLINE 
						) U ON J.TXNID = U.TXNID 
						INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, FULLNAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTID = U.ACCOUNTID
					WHERE J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE AND J.TRANSACTIONTYPE = 28
					GROUP BY J.TRANSACTIONID, J.TXNDATE
				) J ON J.TRANSACTIONID = Y.YENDACCRLID
				--) J ON J.TRANSACTIONID = YA.YENDACCRLID
				INNER JOIN (SELECT ORDERITEMID, ORDERID FROM F_PURCHASEITEM) PI ON PI.ORDERITEMID = DTL.POORDERITEMITD
				--INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = Y.PIITEMID
				INNER JOIN (SELECT EXPENDITUREID, QBDEBITCODE AS ACCOUNTNUMBER FROM F_EXPENDITURE) EX ON EX.EXPENDITUREID = DTL.EXPENDITUREID
				INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, FULLNAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTNUMBER = EX.ACCOUNTNUMBER
					
				LEFT JOIN (SELECT ORDERITEMID, WOID FROM P_WOTOREPAIR) PW ON DTL.POORDERITEMITD = PW.ORDERITEMID
				LEFT JOIN (SELECT ORDERID,SCHEMEID,BLOCKID FROM F_PURCHASEORDER) PO ON PI.ORDERID = PO.ORDERID
				LEFT JOIN (SELECT BLOCKID, SCHEMEID FROM P_BLOCK) B ON B.BLOCKID = PO.BLOCKID

				LEFT JOIN (SELECT WOID, PROPERTYID, SCHEMEID FROM P_WORKORDER) W ON W.WOID = PW.WOID
				LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = W.PROPERTYID
				LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID
		) YRA
		GROUP BY ACCOUNTNUMBER, SCHEMEID


--		UNION ALL 
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT 
			EXA.ACCOUNTNUMBER,'CN' AS TRANSACTIONTYPE, SUM(PI.DEBIT) AS DEBIT, SUM(PI.CREDIT) AS CREDIT
			,COALESCE(W.SCHEMEID, PO.SCHEMEID, D.SCHEMEID, B.SCHEMEID) AS SCHEMEID, @FISCALYEAR
		FROM F_CREDITNOTE C
			INNER JOIN ( 
				SELECT TRANSACTIONID FROM NL_JOURNALENTRY JE
				INNER JOIN ( 	
					SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYDEBITLINE  			
					UNION ALL 	
					SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYCREDITLINE 
				) U ON JE.TXNID = U.TXNID 
				INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, FULLNAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTID = U.ACCOUNTID
				WHERE JE.TXNDATE BETWEEN @STARTDATE AND @ENDDATE
					AND JE.TRANSACTIONTYPE = 20		--CN
				GROUP BY JE.TRANSACTIONID
			) J ON J.TRANSACTIONID = C.CNID
			INNER JOIN (SELECT CNID, ORDERITEMID FROM F_CREDITNOTE_TO_PURCHASEITEM) CN ON C.CNID = CN.CNID

			INNER JOIN (
						SELECT ORDERITEMID, ORDERID, EXPENDITUREID 
								,CASE WHEN GROSSCOST > 0 THEN GROSSCOST ELSE 0 END AS DEBIT
								,CASE WHEN GROSSCOST <= 0 THEN -GROSSCOST ELSE 0 END AS CREDIT
								
						FROM F_PURCHASEITEM
				) PI ON PI.ORDERITEMID = CN.ORDERITEMID
			--INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = CN.ORDERITEMID
			INNER JOIN (SELECT EXPENDITUREID, QBDEBITCODE AS ACCOUNTNUMBER FROM F_EXPENDITURE) EX ON EX.EXPENDITUREID = PI.EXPENDITUREID
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, FULLNAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) EXA ON EXA.ACCOUNTNUMBER = EX.ACCOUNTNUMBER
				
			LEFT JOIN (SELECT ORDERITEMID, WOID FROM P_WOTOREPAIR) PW ON CN.ORDERITEMID = PW.ORDERITEMID
			LEFT JOIN (SELECT ORDERID,SCHEMEID,BLOCKID FROM F_PURCHASEORDER) PO ON C.ORDERID = PO.ORDERID
			LEFT JOIN (SELECT BLOCKID, SCHEMEID FROM P_BLOCK) B ON B.BLOCKID = PO.BLOCKID

			LEFT JOIN (SELECT WOID, PROPERTYID, SCHEMEID FROM P_WORKORDER) W ON W.WOID = PW.WOID
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = W.PROPERTYID
			LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID

		GROUP BY EXA.ACCOUNTNUMBER,COALESCE(W.SCHEMEID, PO.SCHEMEID, D.SCHEMEID, B.SCHEMEID) 

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, D.SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

			--LEFT JOIN (SELECT ORDERITEMID FROM F_PURCHASEITEM) PI ON PI.ORDERITEM = J.TRANSACTIONID
			LEFT JOIN (SELECT ORDERITEMID, CHQID FROM  F_PURCHASEITEM_TO_CHQ) PC ON PC.ORDERITEMID = J.TRANSACTIONID
			LEFT JOIN (SELECT CHQID, TENANCY_ID FROM F_PURCHASEITEM_CHQ) CH ON CH.CHQID = PC.CHQID
			LEFT JOIN (SELECT TENANCYID, PROPERTYID FROM C_TENANCY) CT ON CT.TENANCYID = CH.TENANCY_ID
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = CT.PROPERTYID
			LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID
		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 21	-- TENANT REINBURSEMENTS
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION, D.SCHEMEID

--		UNION ALL
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT  EX.ACCOUNTNUMBER, 'RPO' AS TRANSACTIONTYPE, ISNULL(SUM(PI.GROSSCOST),0) AS DEBIT, 0 AS CREDIT, COALESCE(W.SCHEMEID, PO.SCHEMEID, D.SCHEMEID, B.SCHEMEID) AS SCHEMEID, @FISCALYEAR
		FROM F_INVOICE I 
			INNER JOIN (
				SELECT J.TRANSACTIONID, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT
					--J.TXNID, J.TRANSACTIONID,A.ACCOUNTNUMBER,J.TXNDATE,T.DESCRIPTION AS TRANSACTIONTYPE, U.DEBIT, U.CREDIT, J.DESCRIPTION, U.MEMO
				FROM NL_JOURNALENTRY J 
					INNER JOIN ( 	
						SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYDEBITLINE  			
						UNION ALL 	
						SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID, MEMO FROM NL_JOURNALENTRYCREDITLINE 
					) U ON J.TXNID = U.TXNID 
					INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, FULLNAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTID = U.ACCOUNTID
					LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 
				WHERE J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
					AND J.TRANSACTIONTYPE = 1
				GROUP BY J.TRANSACTIONID
			) J ON J.TRANSACTIONID = I.INVOICEID
			INNER JOIN F_ORDERITEM_TO_INVOICE OI ON OI.INVOICEID = I.INVOICEID
			INNER JOIN F_PURCHASEITEM PI ON (PI.ORDERITEMID = OI.ORDERITEMID AND pi.ACTIVE = 1)
			LEFT JOIN (SELECT ORDERID,SCHEMEID, BLOCKID FROM F_PURCHASEORDER) PO ON PI.ORDERID = PO.ORDERID
			LEFT JOIN (SELECT BLOCKID, SCHEMEID FROM P_BLOCK) B ON B.BLOCKID = PO.BLOCKID

			LEFT JOIN (SELECT EXPENDITUREID, QBDEBITCODE AS ACCOUNTNUMBER FROM F_EXPENDITURE) EX ON EX.EXPENDITUREID = PI.EXPENDITUREID
			--LEFT JOIN (SELECT ACCOUNTNUMBER FROM NL_ACCOUNT) AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE	

			LEFT JOIN (SELECT ORDERITEMID, WOID FROM P_WOTOREPAIR) WR ON PI.ORDERITEMID = WR.ORDERITEMID
			LEFT JOIN (SELECT WOID, PROPERTYID, SCHEMEID FROM P_WORKORDER) W ON W.WOID = WR.WOID
			LEFT JOIN (SELECT PROPERTYID, SCHEMEID FROM P__PROPERTY) P ON P.PROPERTYID = W.PROPERTYID
			LEFT JOIN (SELECT SCHEMEID, SCHEMENAME FROM P_SCHEME) D ON D.SCHEMEID = P.SCHEMEID
		GROUP BY EX.ACCOUNTNUMBER, COALESCE(W.SCHEMEID, PO.SCHEMEID, D.SCHEMEID, B.SCHEMEID)
		 
		 

--		UNION ALL
		-- PETTY CASH ... DOESN'T SEEM TO BE POSSIBLE TO BREAK THIS DOWN! SO NULL FOR DEV ID!
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, NULL AS SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

			--LEFT JOIN NL_JOURNAL2PURCHASEITEM J2P ON J2P.TRANSACTIONID = J.TRANSACTIONID 
			--LEFT JOIN F_PURCHASEITEM PI ON (PI.ORDERITEMID = J2P.ORDERITEMID AND PI.PITYPE=3)

		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 5	--PETTY CASH
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION

--		UNION ALL
		-- MISC PAYMENTS CAN'T BE BROKEN DOWN TO DEVELOPMENT
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, NULL AS SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 22	--MISC PAYMENTS
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION


--		UNION ALL
		-- MISC INCOME CAN'T BE BROKEN DOWN TO DEVELOPMENT
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, NULL AS SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

		WHERE	J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE 
			AND J.TRANSACTIONTYPE = 15	--MISC INCOME
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION

--		UNION ALL
		-- GENERAL JOURNALS CAN'T BE BROKEN DOWN TO DEVELOPMENT
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, NULL AS SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264,219)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

		WHERE	(J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE )
			AND J.TRANSACTIONTYPE = 13	--GJ
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION

--		UNION ALL
		--BDW SHOULD THEORETICALLY BE VERY POSSIBLE TO BREAK DOWN BY DEVELOPMENT
		--IT WILL TAKE A GREATER MAN THAN THOUGH!
		INSERT INTO F_SCHEME_REPORT_DATA (ACCOUNTNUMBER, TRANSACTIONTYPE, DEBIT, CREDIT, SCHEMEID, FISCALYEARID)
		SELECT A.ACCOUNTNUMBER,T.DESCRIPTION AS TRANSACTIONTYPE, SUM(U.DEBIT) AS DEBIT, SUM(U.CREDIT) AS CREDIT, NULL AS SCHEMEID, @FISCALYEAR
		FROM NL_JOURNALENTRY J 
			INNER JOIN ( 	
				SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE  			
				UNION ALL 	
				SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE 
			) U ON J.TXNID = U.TXNID 
			INNER JOIN (SELECT ACCOUNTID, ACCOUNTNUMBER, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IN (192,200,242,264)) A ON A.ACCOUNTID = U.ACCOUNTID
			LEFT JOIN (SELECT TRANSACTIONTYPEID, DESCRIPTION FROM NL_TRANSACTIONTYPE) T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE 

		WHERE	(J.TXNDATE BETWEEN @STARTDATE AND @ENDDATE )
			AND J.TRANSACTIONTYPE = 23	--BDW
		GROUP BY A.ACCOUNTNUMBER, T.DESCRIPTION
		 
--	) BREAKDOWN


END

GO
