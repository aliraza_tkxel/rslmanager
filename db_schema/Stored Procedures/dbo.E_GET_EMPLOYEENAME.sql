SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Umair Naeem
-- Create date: 07 July 2010
-- Description:	This procedure gets the 
--				name of employee
-- =============================================
--
-- EXEC [E_GET_EMPLOYEENAME] @EMPLOYEEID=100
--
CREATE PROCEDURE [dbo].[E_GET_EMPLOYEENAME]
	-- Add the parameters for the stored procedure here
	@EMPLOYEEID INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT  E__EMPLOYEE.EMPLOYEEID,G_TITLE.DESCRIPTION AS TITLE,E__EMPLOYEE.FIRSTNAME,
			dbo.E__EMPLOYEE.MIDDLENAME,E__EMPLOYEE.LASTNAME,
			--ISNULL(FIRSTNAME,'')+ISNULL(' '+MIDDLENAME,'')+ISNULL(' '+LASTNAME,'') AS FULLNAME 
			ISNULL(FIRSTNAME,'')+ISNULL(' '+LASTNAME,'') AS FULLNAME 
	FROM dbo.E__EMPLOYEE 
		INNER JOIN dbo.G_TITLE ON dbo.G_TITLE.TITLEID=dbo.E__EMPLOYEE.TITLE 
	WHERE EMPLOYEEID=COALESCE(@EMPLOYEEID, EMPLOYEEID)
	
	SET NOCOUNT OFF;
END 





















GO
