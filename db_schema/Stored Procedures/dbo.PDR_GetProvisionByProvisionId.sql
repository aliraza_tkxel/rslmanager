SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_GetProvisionByProvisionId]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetProvisionByProvisionId] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetProvisionByProvisionId]
(
	@provisionId INT
)
AS
BEGIN

	SELECT * from PDR_Provisions
	WHERE ProvisionId = @provisionId

END
GO
