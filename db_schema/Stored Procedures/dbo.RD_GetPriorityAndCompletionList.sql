USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GetPriorityAndCompletionList]    Script Date: 05/18/2015 19:55:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--DECLARE @totalCount int
--EXEC	[dbo].[RD_GetPriorityAndCompletionList]
--		@searchedText = NULL,
--		@totalCount = @totalCount OUTPUT		
-- Author:		<Ahmed Mehmood>
-- Create date: <4/30/2014>
-- Description:	<Get list of Completed Faults>
-- Web Page: ReportsArea.aspx
-- =============================================

IF OBJECT_ID('dbo.RD_GetPriorityAndCompletionList') IS NULL
 EXEC('CREATE PROCEDURE dbo.RD_GetPriorityAndCompletionList AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[RD_GetPriorityAndCompletionList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int,
		@fromDate varchar(20),
		@toDate varchar(20),
		@searchedText varchar(8000),
		@isFullList bit,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(200) = 'FaultLogID',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(3000),	        
        @orderClause  varchar(1000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(3000),
        @dataLimitation varchar(50) ,
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		
		SET @searchCriteria = ' 1=1 ' 
		
		IF @schemeId > 0
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P__PROPERTY.SCHEMEID = '+CONVERT(nvarchar(10), @schemeId) 	
		END 	
		
		IF((@fromDate != '') AND (@toDate != '') )
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( CONVERT(DATE, FL_FAULT_LOG.SubmitDate, 103) BETWEEN  CONVERT(DATE, '''+@fromDate+''', 103)  AND CONVERT(DATE, '''+@toDate+''', 103) ) '
		END	
		
		IF((@fromDate != '') AND (@toDate = '') )
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND CONVERT(DATE, FL_FAULT_LOG.SubmitDate, 103) >= CONVERT(DATE, '''+@fromDate+''', 103) '
		END	
		
		IF((@fromDate = '') AND (@toDate != '') )
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND CONVERT(DATE, FL_FAULT_LOG.SubmitDate, 103) <= CONVERT(DATE, '''+@toDate+''', 103) '
		END	
		
		IF(LTRIM(@searchedText) <> '')
		BEGIN									
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( RIGHT(''0000000''+ CONVERT(VARCHAR,FL_FAULT_LOG.FaultLogID),7) LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PROPERTYADDRESS.Address LIKE ''%' + @searchedText + '%'') '
		END				
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		
		if (@isFullList = 1) 
			set @dataLimitation = ''
		else
			set @dataLimitation = 'top ('+convert(varchar(10),@limit)+')'
		
		SET @selectClause = 'SELECT '+@dataLimitation+' 			
		RIGHT(''0000000''+ CONVERT(VARCHAR,FL_FAULT_LOG.FaultLogID),7) AS JSN
		,CONVERT(nvarchar(10),FL_FAULT_LOG.SubmitDate, 103) as Reported
		,ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '', '' + ISNULL(P__PROPERTY.TOWNCITY,'''') AS Address
		,FL_FAULT.Description AS Description
		,FL_FAULT_PRIORITY.Priority AS Priority
		,CONVERT(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103) AS Scheduled
		,SUBSTRING(E__EMPLOYEE.FIRSTNAME , 1, 1) +'' ''+ E__EMPLOYEE.LASTNAME AS Operative
		,CONVERT(varchar,CompletedFaults.CompletedDate, 103) AS Completed
		,DATEDIFF(DAY,DATEADD(DAY,FL_FAULT_PRIORITY.PriorityDays,FL_FAULT_LOG.SubmitDate) ,CompletedFaults.CompletedDate ) AS TimeFrame
		
		-- For Sorting
		,FL_FAULT_LOG.SubmitDate AS ReportedSort
		,FL_CO_APPOINTMENT.AppointmentDate AS ScheduleSort
		,CompletedFaults.CompletedDate AS CompletedSort
		,P__PROPERTY.HOUSENUMBER AS HouseNumber
		,P__PROPERTY.ADDRESS1 AS PrimaryAddress
		,FL_FAULT_LOG.FaultLogID AS FaultLogID
		,FL_FAULT_PRIORITY.PriorityDays AS PrioritySort'
		
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'		
		FROM 
		(	SELECT	FaultLogID 
					,max(LastActionDate)	as CompletedDate					
			FROM	FL_FAULT_LOG_HISTORY 
			WHERE	FAULTSTATUSID IN (17 ,7)
			GROUP BY FaultLogID
		  ) as CompletedFaults
		INNER JOIN FL_FAULT_LOG on CompletedFaults.FaultLogID = FL_FAULT_LOG.FaultLogId		 
		LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogId = FL_FAULT_APPOINTMENT.FaultLogId 
		LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
		LEFT JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeId = E__EMPLOYEE.EMPLOYEEID 
		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN (
					SELECT	P__PROPERTY.PROPERTYID as PID
							,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
					FROM	P__PROPERTY
					)AS PROPERTYADDRESS ON P__PROPERTY.PROPERTYID = PROPERTYADDRESS.PID 
		INNER JOIN FL_FAULT ON  FL_FAULT_LOG.FaultID= FL_FAULT.FaultID 
		INNER JOIN 
		(SELECT	 PRIORITYID
			,CASE 
			WHEN	FL_FAULT_PRIORITY.Days = 1 THEN
					CONVERT(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+'' days'' 
			ELSE
					CONVERT(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+'' hours'' 
			END		AS Priority
			,CASE 
			WHEN	FL_FAULT_PRIORITY.HRS = 1 THEN
					FL_FAULT_PRIORITY.ResponseTime /24
			ELSE
					FL_FAULT_PRIORITY.ResponseTime 
			END		AS PriorityDays
		FROM	FL_FAULT_PRIORITY)
		FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID 
		INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID'
		-- End building From clause
		--======================================================================================== 														  
		
				
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
				
		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
		END
		
		IF(@sortColumn = 'Reported')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ReportedSort' 		
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' cast(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)' 	
			
		END
		
		IF(@sortColumn = 'Priority')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'PrioritySort' 		
		END

		IF(@sortColumn = 'Scheduled')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ScheduleSort' 		
		END
		
		IF(@sortColumn = 'Completed')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'CompletedSort' 		
		END

		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
			
		if (@isFullList = 1) 
			BEGIN
				print(@mainSelectQuery)
				EXEC (@mainSelectQuery)
			END		
		else
			BEGIN	
				print(@finalQuery)
				EXEC (@finalQuery)
			END
																										
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							
END



