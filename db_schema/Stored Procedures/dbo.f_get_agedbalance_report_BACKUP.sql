SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec f_get_agedbalance_report 0,0  
CREATE proc [dbo].[f_get_agedbalance_report_BACKUP](@viewBY as int,@getSum as bit)  
as  
begin  
 set nocount on  
 create table #rprt   
 (  
  TENANCYID  int,  
  SUPPLIERID  int,  
  EMPLOYEEID  int,  
  InvoiveAmt  money,  
  payment  money,  
  Balance  Money,  
  Cat  varchar(10),  
  cname  varchar(100)  
 )  
  
  /* Invoice part*/  
  insert into #rprt  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID, sum(isnull(SI.GROSSCOST,0)) as invAmout,0 as openingBalance,0 as payment,'Tenant' as cat, CNGV.LIST cname   
   FROM F_SALESINVOICE  s  
   inner join F_SALESINVOICEITEM si on si.SALEID =s.SALEID  
   inner join C_TENANCY t on t.TENANCYID=s.TENANCYID   
   inner join C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = S.TENANCYID   
   where s.TENANCYID is not null and s.SUPPLIERID is null and s.EMPLOYEEID is null   
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,CNGV.LIST    
  union  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID, sum(isnull(SI.GROSSCOST,0)) as invAmout,0 as openingBalance,0 as payment,'Supplier' as cat,o.[NAME] cname  
   FROM F_SALESINVOICE  s  
   inner join F_SALESINVOICEITEM si on si.SALEID =s.SALEID   
   inner join S_ORGANISATION o on o.orgid=s.SUPPLIERID   
   where s.TENANCYID is null and s.SUPPLIERID is not null and s.EMPLOYEEID is null  
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,o.[NAME]  
  union  
  select  s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID, sum(isnull(SI.GROSSCOST,0)) as invAmout,0 as openingBalance,0 as payment,'Employee' as cat,e.firstname + ' ' + e.lastname as cname   
   FROM F_SALESINVOICE  s  
   inner join F_SALESINVOICEITEM si on si.SALEID =s.SALEID   
   inner join E__EMPLOYEE e on e.EMPLOYEEID=s.EMPLOYEEID    
   where s.TENANCYID is null and s.SUPPLIERID is  null and s.EMPLOYEEID is not null  
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,e.firstname , e.lastname  
    
    
  /*payment part*/    
  insert into #rprt  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,0 as invAmout,  
   isnull(sum(s.AMOUNT),0)  as  payment,  
   0 as openingBalance,  
   'Tenant' as cat,  
   CNGV.LIST as cname  FROM F_SALESPAYMENT  s  
   LEFT join C_TENANCY t on t.TENANCYID=s.TENANCYID   
   LEFT join C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = S.TENANCYID   
   where s.TENANCYID is not null and s.SUPPLIERID is null and s.EMPLOYEEID is null and s.itemtype <>12  
   group by  s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,CNGV.LIST  
  union  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,0 as invAmout,  
   isnull(sum(s.AMOUNT),0)  as  payment,  
   0  openingBalance,  
   'Suppler' as cat,  
   o.[NAME] cname  FROM F_SALESPAYMENT  s  
   LEFT join S_ORGANISATION o on o.orgid=s.SUPPLIERID   
   where s.TENANCYID is null and s.SUPPLIERID is not null and s.EMPLOYEEID is null and s.itemtype <>12  
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,o.[NAME]  
  union  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,0 as invAmout,  
   isnull(sum(s.AMOUNT),0)  as  payment,  
   0  openingBalance,  
   'Employee' as cat,  
   e.firstname + ' ' + e.lastname as cname   FROM F_SALESPAYMENT s  
   LEFT join E__EMPLOYEE e on e.EMPLOYEEID=s.EMPLOYEEID    
   where s.TENANCYID is null and s.SUPPLIERID is  null and s.EMPLOYEEID is not null and s.itemtype <>12  
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,e.firstname , e.lastname   
    
  /*Opening balance*/  
  insert into #rprt  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,0 as invAmout,  
   0  payment,  
   isnull(sum(AMOUNT),0)  as  openingBalance,  
   'Tenant' as cat,CNGV.LIST as cname  FROM F_SALESPAYMENT  s  
   LEFT join C_TENANCY t on t.TENANCYID=s.TENANCYID   
   LEFT join C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = S.TENANCYID   
   where s.TENANCYID is not null and s.SUPPLIERID is null and s.EMPLOYEEID is null and TRANSACTIONDATE ='1 apr 2004 00:00:00' and itemtype=12  
   group by  s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,CNGV.LIST  
  union  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,0 as invAmout,  
   0  payment,  
   isnull(sum(AMOUNT),0)  as  openingBalance,  
   'Suppler' as cat,  
   o.[NAME] cname  FROM F_SALESPAYMENT  s  
   LEFT join S_ORGANISATION o on o.orgid=s.SUPPLIERID   
   where s.TENANCYID is null and s.SUPPLIERID is not null and s.EMPLOYEEID is null and TRANSACTIONDATE ='1 apr 2004 00:00:00' and itemtype=12  
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,o.[NAME]  
  
  union  
  select s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,0 as invAmout,  
   0  payment,  
   isnull(sum(AMOUNT),0)  as  openingBalance,  
   'Employee' as cat,  
   e.firstname + ' ' + e.lastname as cname   FROM F_SALESPAYMENT s  
   LEFT join E__EMPLOYEE e on e.EMPLOYEEID=s.EMPLOYEEID    
   where s.TENANCYID is null and s.SUPPLIERID is  null and s.EMPLOYEEID is not null and TRANSACTIONDATE ='1 apr 2004 00:00:00' and itemtype=12  
   group by s.TENANCYID,s.SUPPLIERID, s.EMPLOYEEID,e.firstname , e.lastname   
  
     
  
  /*Sumup*/  
  select  distinct rslt.TENANCYID,rslt.SUPPLIERID,rslt.EMPLOYEEID,rslt.InvoiveAmt,rslt.payment,rslt.OpeningBalance  
  ,rslt.InvoiveAmt+rslt.OpeningBalance-abs(rslt.payment) as outstanding,isnull(r.cat,'') as cat,isnull(r.cname,'N/A') as cname  
  into #rprt2  
  from   
  (select TENANCYID,SUPPLIERID,EMPLOYEEID,sum(InvoiveAmt) as InvoiveAmt,sum(payment) as payment,  
  sum(Balance) as OpeningBalance  
  from #rprt    
  group by TENANCYID,SUPPLIERID,EMPLOYEEID) rslt  
  left join   #rprt r on ((r.TENANCYID=rslt.TENANCYID   
        or r.SUPPLIERID=rslt.SUPPLIERID  
      or r.EMPLOYEEID=rslt.EMPLOYEEID)  
  and len(r.cat) >0)  
  
  
  
  
    
  /*Fetch all rows*/  
  if @viewBY=3 and @getSum=0 --TENANCYID  
  begin  
   select distinct * from #rprt2 where TENANCYID is not null order by cname  
  end  
  
  if @viewBY=2 and @getSum=0 --SUPPLIERID  
  begin  
   select distinct * from #rprt2 where SUPPLIERID is not null order by cname  
  end  
  
  if @viewBY=1 and @getSum=0 --EMPLOYEEID  
  begin  
   select distinct * from #rprt2 where EMPLOYEEID is not null order by cname  
  end  
  
  if @viewBY=0 and @getSum=0 --All  
  begin  
   select distinct * from #rprt2 order by cname  
  end  
  
  
  /*Sum of individual column*/  
  
  if @viewBY=3 and @getSum=1 --TENANCYID  
  begin  
   select isnull(sum(InvoiveAmt),0) InvoiveAmtSum,isnull(sum(payment),0) as paymentSum,  
   isnull(sum(OpeningBalance),0) OpeningBalanceSum,   
   isnull(sum(outstanding),0) as outstandingSum from #rprt2  
   where TENANCYID is not null  
  end  
  
  if @viewBY=2 and @getSum=1 --SUPPLIERID  
  begin  
   select isnull(sum(InvoiveAmt),0) InvoiveAmtSum,isnull(sum(payment),0) as paymentSum,  
   isnull(sum(OpeningBalance),0) OpeningBalanceSum,   
   isnull(sum(outstanding),0) as outstandingSum from #rprt2  
   where SUPPLIERID is not null  
  end  
  
  if @viewBY=1 and @getSum=1 --EMPLOYEEID  
  begin  
   select isnull(sum(InvoiveAmt),0) InvoiveAmtSum,isnull(sum(payment),0) as paymentSum,  
   isnull(sum(OpeningBalance),0) OpeningBalanceSum,   
   isnull(sum(outstanding),0) as outstandingSum from #rprt2  
   where EMPLOYEEID is not null  
  end  
  
  if @viewBY=0 and @getSum=1 --All  
  begin  
   select isnull(sum(InvoiveAmt),0) InvoiveAmtSum,isnull(sum(payment),0) as paymentSum,  
   isnull(sum(OpeningBalance),0) OpeningBalanceSum,   
   isnull(sum(outstanding),0) as outstandingSum from #rprt2  
  end  
    
  
  
  
  drop table #rprt2  
  drop table #rprt  
  
end  

GO
