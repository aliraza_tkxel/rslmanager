USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetMultiAttributeListByItemId]    Script Date: 9/14/2018 3:33:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Raja Basiq
-- Create date: 28/08/2018
-- Description:	to get list of child Items
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetMultiAttributeListByItemId] 
@schemeId int=null, 
@blockId int=null,
@itemId int =null 
AS
BEGIN
	SET NOCOUNT ON;
	 select id,AttributeName from PA_ChildAttributeMapping  
	 where  (SchemeId= @schemeId or BlockId=@blockId)  
		and ItemId =@itemId and IsActive =1
		order by id ASC
 
END
