SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_SaveDevelopmentID @employeeId=1,	@developmentId=4
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,28/09/2012>
-- Description:	<Description,,Save the patch drop values against user>
-- WEb Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveDevelopmentID]
	-- Add the parameters for the stored procedure here
	@employeeId int,
	@developmentId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO AS_UserPatchDevelopment (EmployeeId,DevelopmentId,IsActive) VALUES
						(@employeeId,@developmentId,1)
END
GO
