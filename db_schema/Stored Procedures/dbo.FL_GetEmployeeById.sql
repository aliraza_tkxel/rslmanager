USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetEmployeeById]    Script Date: 08/29/2016 10:58:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC FL_GetEmployeeById 	@employeeId = 943
-- Author:		<Noor Muhammad>
-- Create date: <02/04/2013>
-- Description:	<This Stored Proceedure get the Employee by id >
-- Web Page: Bridge.aspx
-- =============================================
IF OBJECT_ID('dbo.FL_GetEmployeeById') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetEmployeeById AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[FL_GetEmployeeById](
	@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT E__EMPLOYEE.EMPLOYEEID as EmployeeId, E__EMPLOYEE.FirstName +' ' + E__EMPLOYEE.LastName as FullName, AC_LOGINS.Active as IsActive
	,(Select Description 
		From E__employee 
		INNER JOIN AS_USER ON E__EMPLOYEE.EMPLOYEEID = AS_USER.EMPLOYEEID
		INNER JOIN AS_USERType ON AS_USER.UserTypeId = AS_USERType.UserTypeId
		Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
	) as UserType
	FROM E__EMPLOYEE 	
	INNER JOIN AC_LOGINS on E__EMPLOYEE.EmployeeId = AC_LOGINS.EmployeeId
	Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
END
