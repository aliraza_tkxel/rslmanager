-- Stored Procedure  
IF OBJECT_ID('dbo.AS_DeleteHeating') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_DeleteHeating AS SET NOCOUNT ON;') 
GO   
ALTER PROCEDURE [dbo].[AS_DeleteHeating]   
@heatingMappingId int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
Update PA_HeatingMapping set IsActive=0 where PA_HeatingMapping.HeatingMappingId=@heatingMappingId

update AS_JOURNAL
SET ISCURRENT = 0
WHERE JOURNALID = (SELECT J.JOURNALID FROM AS_JournalHeatingMapping JHM
INNER JOIN AS_JOURNAL J on J.JOURNALID = JHM.JournalId
WHERE JHM.HeatingMappingId = @heatingMappingId)

END  