SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROC [dbo].[F_PAYTHRU_GET_TOKEN_REQUEST_LOG_Insert] 
    @TransactionId uniqueidentifier,
    @RequestUrl varchar(MAX),
    @Payload varchar(MAX),
    @Applet varchar(50),
    @SuccessUrl varchar(MAX),
    @CancelUrl varchar(MAX),
    @CallbackUrl varchar(MAX),
    @FirstName varchar(50),
    @Surname varchar(50),
    @Email varchar(500),
    @Maid varchar(250),
    @CustomTenancyRef varchar(250),
    @CustomCustomerRef varchar(250),
    @CustomStaffId varchar(250),
    @CustomStaffName varchar(500),
    @AddressPropertyName varchar(50),
    @Address1 varchar(100),
    @Address2 varchar(100),
    @Address3 varchar(100),
    @AddressTown varchar(80),
    @AddressCounty varchar(50),
    @AddressPostcode varchar(10),
    @AddressCountry varchar(100),
    @DateSubmitted datetime,
    @CreatedBy varchar(256)
AS 
	SET NOCOUNT ON 
	
    INSERT  INTO [dbo].[F_PAYTHRU_GET_TOKEN_REQUEST_LOG]
            ( [TransactionId] ,
              [RequestUrl] ,
              [Payload] ,
              [Applet] ,
              [SuccessUrl] ,
              [CancelUrl] ,
              [CallbackUrl] ,
              [FirstName] ,
              [Surname] ,
              [Email] ,
              [Maid] ,
              [CustomTenancyRef] ,
              [CustomCustomerRef] ,
              [CustomStaffId] ,
              [CustomStaffName] ,
              [AddressPropertyName] ,
              [Address1] ,
              [Address2] ,
              [Address3] ,
              [AddressTown] ,
              [AddressCounty] ,
              [AddressPostcode] ,
              [AddressCountry] ,
              [DateSubmitted] ,
              [CreatedBy]
            )
            SELECT  @TransactionId ,
                    @RequestUrl ,
                    @Payload ,
                    @Applet ,
                    @SuccessUrl ,
                    @CancelUrl ,
                    @CallbackUrl ,
                    @FirstName ,
                    @Surname ,
                    @Email ,
                    @Maid ,
                    @CustomTenancyRef ,
                    @CustomCustomerRef ,
                    @CustomStaffId ,
                    @CustomStaffName ,
                    @AddressPropertyName ,
                    @Address1 ,
                    @Address2 ,
                    @Address3 ,
                    @AddressTown ,
                    @AddressCounty ,
                    @AddressPostcode ,
                    @AddressCountry ,
                    @DateSubmitted ,
                    @CreatedBy

		SELECT SCOPE_IDENTITY() AS GetTokenRequestId



GO
