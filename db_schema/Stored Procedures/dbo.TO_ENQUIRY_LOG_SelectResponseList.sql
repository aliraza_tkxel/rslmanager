SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create PROCEDURE [dbo].[TO_ENQUIRY_LOG_SelectResponseList]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_SelectResponseList
 '   DATE CREATED:   03 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display the enquiries on Enquiry Management Area Page
 '   IN:             @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(	    
		-- These Parameters used to limit and sort records
		@noOfRows  int = 50,
		@offSet    int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'TO_ENQUIRY_LOG.EnquiryLogID',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	-- variables used to build whole query
	DECLARE @SelectClause varchar(2000),
	        @FromClause   varchar(2000),
	        @WhereClause  varchar(2000),
	        @OrderClause  varchar(2000)
	

	        
	        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT' + 
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'TO_ENQUIRY_LOG.EnquiryLogID, TO_ENQUIRY_LOG.CreationDate, ' +
                        CHAR(10) + CHAR(9) + 'TO_ENQUIRY_LOG.TenancyID,    C_NATURE.DESCRIPTION AS Nature,TO_ENQUIRY_LOG.CustomerId, ' +
                        CHAR(10) + CHAR(9) + 'C__CUSTOMER.FIRSTNAME, C__CUSTOMER.LASTNAME, ' +
                        CHAR(10) + CHAR(9) + 'C_ITEM.DESCRIPTION AS Item ,CR.CustResponseId AS RESPONSEID,CR.Notes AS RESPONSENOTES,TO_ENQUIRY_LOG.JOURNALID '                        
    -- End building SELECT clause
    
       
    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'TO_ENQUIRY_LOG INNER JOIN C__CUSTOMER ' +
                      CHAR(10) + CHAR(9) + 'ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN TO_CUSTOMER_RESPONSE CR ' +
                      CHAR(10) + CHAR(9) + 'ON TO_ENQUIRY_LOG.EnquiryLogId = CR.EnquiryLogId ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C_NATURE ' +
                      CHAR(10) + CHAR(9) + 'ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C_ITEM ' + 
                      CHAR(10) + CHAR(9) + 'ON C_NATURE.ITEMID = C_ITEM.ITEMID'
    -- End building FROM clause
                            
    
    -- Begin building OrderBy clause
    SET @OrderClause =  CHAR(10) + 'Order By ' + @sortColumn + ' ' + @sortOrder
    -- End building OrderBy clause


    -- Begin building WHERE clause
    -- This Where clause contains subquery to exclude already displayed records 
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( CR.Archived=0 AND ' +
                        CHAR(10) + CHAR (9) + @sortColumn + ' NOT IN' + 
                        CHAR(10) + CHAR (9) + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' ' + @sortColumn +
                        + @FromClause + @OrderClause + ')'+
                        CHAR(10) + CHAR (9) + ' AND  1=1  )'                 
     
    -- End building WHERE clause


    --PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)

    
    EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
    

RETURN




GO
