SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















CREATE PROCEDURE dbo.FL_GETLETTERLOOKUP
/* ===========================================================================
 '   NAME:          FL_GETLETTERLOOKUP
 '   DATE CREATED:  29 Dec. 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get letter C_LETTERS_SAVED table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	/* OLD CODE*/
	--SELECT CLETTERID AS id,Replace(Replace(Replace(Replace(Replace(Replace(LETTERSIG ,'<DIV id=SigNote>', ' '), '<DIV>', ''), '<B>', ''),'</B>', ''),'<BR>',''),'</DIV>', '') As Val
	--FROM C_LETTERS_SAVED
	--ORDER BY LETTERSIG ASC
	--Get and set the Item nature id 
	DECLARE @ItemNatureId INT
	SET @ItemNatureId = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION='Reactive Repair')
	
	SELECT ACTIONID AS id, DESCRIPTION AS Val FROM C_LETTERACTION WHERE NATURE = (SELECT ITEMNATUREID FROM C_NATURE WHERE ITEMNATUREID = @ItemNatureId)







GO
