SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO










CREATE PROCEDURE dbo.FL_ADD_PREINSPECTION_TEMP
/* ===========================================================================
 '   NAME:          FL_ADD_PREINSPECTION_TEMP
 '   DATE CREATED:   30th NOV 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a faultLog against a particular faultLogID from FL_FAULT and TempFault Table table which will be used to update 
 '		     a fault values
 '   IN:            @UserID as Int,
 '   IN:	    @InspectionDate as DateTime,
 '   IN:	    @NetCost as float,
 '   IN:	    @DueDate as datetime,
 '   IN:	    @Notes as nvarchar,
 '   IN:	    @ORGID as int,
		    @Approved as int,
		    @TempFaultId as int
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@UserID as Int,
@InspectionDate as DateTime,
@NetCost as float,
@DueDate as datetime,
@Notes as nvarchar(500),
@ORGID as int = NULL,
@Approved as int,
@TempFaultId as int,
@result as int output
AS
	INSERT INTO FL_FAULT_PREINSPECTIONINFO
                      (USERID, INSPECTIONDATE, NETCOST, DUEDATE, NOTES, ORGID, APPROVED,faultlogid)
VALUES     (@UserID,@InspectionDate,@NetCost,@DueDate,@Notes,@ORGID,@Approved,@TempFaultId)


/* Update TempFault Value of Contractor*/

IF @ORGID IS NOT NULL
BEGIN
Update FL_Temp_Fault set ORGID=@ORGID where TempFaultId=@TempFaultId
END







GO
