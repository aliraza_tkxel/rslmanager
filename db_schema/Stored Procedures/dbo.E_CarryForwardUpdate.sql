USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_CarryForwardUpdate]    Script Date: 09/13/2018 11:57:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.E_CarryForwardUpdate') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_CarryForwardUpdate AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_CarryForwardUpdate]
AS
BEGIN

	
DECLARE @ENDDATE Datetime, @HolidayIndicator INT, @ISBRS INT, @EMPLOYEEID INT, @LEAVE_AVAILABLE FLOAT, @TOTALLEAVE FLOAT
Create TABLE #test_table(
	Unit nvarchar(100),
	StartDate Datetime,
	LeaveBooked FLOAT,
	LeaveRequested FLOAT,
	LeaveTaken FLOAT, 
	LEAVE_AVAILABLE FLOAT,
	ANNUAL_LEAVE_DAYS FLOAT ,
	CARRY_FWD FLOAT,
	BNK_HD FLOAT ,
	TOTAL_LEAVE FLOAT,
	TIME_OFF_IN_LIEU_OWED FLOAT, 
	PART_FULL FLOAT,
	TOIL FLOAT
)


	DECLARE Notification_CURSOR 
	CURSOR FOR 
	SELECT emp.EMPLOYEEID as EMPLOYEEID, JD.HolidayIndicator as HolidayIndicator, JD.ISBRS as ISBRS
	FROM E__EMPLOYEE emp
		INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = emp.EMPLOYEEID
	WHERE JD.ACTIVE = 1

	OPEN Notification_CURSOR

	FETCH NEXT FROM Notification_CURSOR	
	into @EMPLOYEEID, @HolidayIndicator, @ISBRS

	WHILE @@FETCH_STATUS = 0 
    BEGIN 
			
		insert into #test_table exec E_LeaveStats @EMPLOYEEID, null, true 

		select @LEAVE_AVAILABLE = LEAVE_AVAILABLE from #test_table

		-- get end date of annual leave year
		SELECT @ENDDATE = ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(@EMPLOYEEID)

		-- set carry forward value according to BRS/BHA and Hours/Days
		IF CONVERT (date, @ENDDATE) = CONVERT (date, getdate())
		BEGIN
			if @HolidayIndicator = 1 -- days (CARRYFORWARD)
			BEGIN
				if @ISBRS = 1 -- 40 total hour 
				BEGIN
					if @LEAVE_AVAILABLE >= 5
					BEGIN
					--select @TOTALLEAVE =  SUM(5, BANKHOLIDAY, HOLIDAYENTITLEMENTDAYS) from E_JOBDETAILS where EMPLOYEEID = @EMPLOYEEID
						update E_JOBDETAILS set CARRYFORWARD = 5, TOTALLEAVE = 5+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTDAYS, 0) where EMPLOYEEID = @EMPLOYEEID
					END
					ELSE -- 37 total hour 
						update E_JOBDETAILS set CARRYFORWARD = @LEAVE_AVAILABLE, TOTALLEAVE = isnull(@LEAVE_AVAILABLE, 0)+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTDAYS, 0) where EMPLOYEEID = @EMPLOYEEID
				END
				ELSE -- 37 total hour 
					BEGIN
					if @LEAVE_AVAILABLE >= 5
					BEGIN
						update E_JOBDETAILS set CARRYFORWARD = 5, TOTALLEAVE = 5+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTDAYS, 0) where EMPLOYEEID = @EMPLOYEEID
					END
					ELSE -- 37 total hour 
						update E_JOBDETAILS set CARRYFORWARD = @LEAVE_AVAILABLE, TOTALLEAVE = isnull(@LEAVE_AVAILABLE, 0)+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTDAYS, 0) where EMPLOYEEID = @EMPLOYEEID
				END
			END
			ELSE -- @HolidayIndicator = 2 -- hours (CARRYFORWARDHOURS)
			BEGIN
				if @ISBRS = 1 -- 40 total hour 
				BEGIN
					if @LEAVE_AVAILABLE >= 40 -- (5 * 8) = 40
					BEGIN
						update E_JOBDETAILS set CARRYFORWARDHOURS = 40, TOTALLEAVEHOURS = 40+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTHOURS, 0) where EMPLOYEEID = @EMPLOYEEID
					END
					ELSE -- 37 total hour 
						update E_JOBDETAILS set CARRYFORWARDHOURS = @LEAVE_AVAILABLE, TOTALLEAVEHOURS = isnull(@LEAVE_AVAILABLE, 0)+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTHOURS, 0) where EMPLOYEEID = @EMPLOYEEID
				END
				ELSE -- 37 total hour 
					BEGIN
					if @LEAVE_AVAILABLE >= 37 -- (5 * 7.4) = 40
					BEGIN
						update E_JOBDETAILS set CARRYFORWARDHOURS = 37, TOTALLEAVEHOURS = 37+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTHOURS, 0) where EMPLOYEEID = @EMPLOYEEID
					END
					ELSE -- 37 total hour 
						update E_JOBDETAILS set CARRYFORWARDHOURS = @LEAVE_AVAILABLE, TOTALLEAVEHOURS = isnull(@LEAVE_AVAILABLE, 0)+isnull(BANKHOLIDAY, 0)+isnull(HOLIDAYENTITLEMENTHOURS, 0) where EMPLOYEEID = @EMPLOYEEID
				END
			END
		END

		delete from #test_table

		FETCH NEXT FROM Notification_CURSOR INTO @EMPLOYEEID, @HolidayIndicator, @ISBRS
			 
    END 
 CLOSE Notification_CURSOR  
 DEALLOCATE Notification_CURSOR 

 drop table #test_table

END	

	
