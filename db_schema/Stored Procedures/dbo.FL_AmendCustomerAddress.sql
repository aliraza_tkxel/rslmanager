SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================
--EXEC	[dbo].[FL_AmendCustomerAddress]
--		@customerId = 1,
--		@telephone = N'123',
--		@mobile = N'456',
--		@email = N'a@h.com'
--      @result = @result OUTPUT
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <28/1/2013>
-- Description:	<Update telephone, mobile and email in C_ADDRESS table>
-- Web Page: AppointmentSummary.aspx
-- =====================================================================
CREATE PROCEDURE [dbo].[FL_AmendCustomerAddress] 
	-- Add the parameters for the stored procedure here
	@customerId int,
	@telephone nvarchar(20),
	@mobile nvarchar(20),
	@email nvarchar(150),
	@result bit OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE C_ADDRESS
	SET TEL = @telephone, MOBILE = @mobile, EMAIL = @email
	WHERE CUSTOMERID = @customerId AND ISDEFAULT = 1
	
	SET @result = 1		 
END

GO
