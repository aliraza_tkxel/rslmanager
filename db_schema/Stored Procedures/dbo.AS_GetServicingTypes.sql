USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetServicingTypes]    Script Date: 09/09/2018 18:56:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
IF OBJECT_ID('dbo.[AS_GetServicingTypes]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[AS_GetServicingTypes] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

-- =============================================
-- Create date: <Create Date,09/09/2018>
-- Description:	<Description,,Populate Servicing Type on Appliance Dashboard>
-- EXEC AS_GetServicingTypes
-- =============================================

ALTER PROCEDURE [dbo].[AS_GetServicingTypes]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM P_SERVICINGTYPE 
	Where Description Not IN ('M&E Servicing','PAT Testing')
END

