USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[TS_GetOperativeJobsByAppointmenttType]    Script Date: 2/13/2017 10:20:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[TS_GetOperativeJobsByAppointmenttType]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[TS_GetOperativeJobsByAppointmenttType] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[TS_GetOperativeJobsByAppointmenttType]
	-- Add the parameters for the stored procedure here
	@operativeId	INT,
	@fromDate		DATETIME,
	@toDate			DATETIME,
	@aptType		NVARCHAR(100),
	--Parameters which would help in sorting and paging
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'JsRef',
	@sortOrder varchar (5) = 'ASC',
	@totalCount int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	DECLARE @offset int, @limit int, @totalRecord int
	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1
   
	IF @sortColumn = 'JsRef'
	BEGIN
		SET @sortColumn = 'JsSort'
	END
   
   
   IF @aptType = 'Repairs'
   BEGIN
   
    --=============================
    -- REPAIR APPOINTMENTS
    --=============================
    
    
     SELECT *
     FROM(
			 SELECT *, row_number() OVER (ORDER BY JsSort DESC) AS ROW
			 FROM (
		 
					SELECT	FL_FAULT_LOG.JobSheetNumber As JsRef
							,FL_FAULT.description As Title
							,'Fault Repair' As InspectionType  
							,CONVERT(DECIMAL(10,2),SUM(DATEDIFF(mi, FL_FAULT_JOBTIMESHEET.StartTime, FL_FAULT_JOBTIMESHEET.EndTime)) / 60.0) As Hours
							,FL_FAULT_JOBTIMESHEET.faultlogid AS JsSort
							,'' As ComponentName
							,CASE
								WHEN FL_FAULT_LOG.PROPERTYID IS NOT NULL THEN
									ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')+ ', ' + ISNULL(P__PROPERTY.postcode, '')
								WHEN FL_FAULT_LOG.SchemeId IS NOT NULL THEN
									ISNULL(P_SCHEME.SCHEMENAME , '') + ISNULL( ', '+P_SCHEME.SCHEMECODE, '')		
								ELSE
									ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
									+ ISNULL(P_BLOCK.ADDRESS1 , '') 
									+ ISNULL(', '+P_BLOCK.ADDRESS2, '') 
									+ ISNULL( ' '+P_BLOCK.ADDRESS3, '')
									+ ISNULL( ', '+P_BLOCK.POSTCODE, '')
							END AS AppointmentAddress
							
					FROM	FL_CO_APPOINTMENT
							INNER JOIN FL_FAULT_JOBTIMESHEET ON FL_FAULT_JOBTIMESHEET.appointmentid = FL_CO_APPOINTMENT.appointmentid
							INNER JOIN FL_FAULT_LOG ON FL_FAULT_LOG.faultlogid=FL_FAULT_JOBTIMESHEET.faultlogid
							INNER JOIN FL_FAULT ON FL_FAULT.faultid = FL_FAULT_LOG.faultid

							LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
							LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
								Case 
									WHEN FL_FAULT_LOG.SchemeId is NULL THEN P__PROPERTY.SchemeId 
									ELSE 		
									FL_FAULT_LOG.SchemeId 	
								END
							LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID =
								Case WHEN FL_FAULT_LOG.BlockId is NULL THEN P__PROPERTY.BLOCKID 
									ELSE 		
									FL_FAULT_LOG.BlockId 	
								END		

					WHERE	FL_CO_APPOINTMENT.operativeid=@operativeId
							AND (FL_FAULT_JOBTIMESHEET.StartTime >=@fromDate)
							AND (FL_FAULT_JOBTIMESHEET.EndTime <= @toDate)
					GROUP BY  FL_FAULT_JOBTIMESHEET.FaultLogId, FL_FAULT_LOG.jobSheetNumber,FL_CO_APPOINTMENT.operativeid,FL_FAULT.description,
							FL_FAULT_LOG.PROPERTYID, P__PROPERTY.HouseNumber, P__PROPERTY.ADDRESS1, P__PROPERTY.ADDRESS2, P__PROPERTY.ADDRESS3,
							P__PROPERTY.postcode, FL_FAULT_LOG.SchemeId, P_SCHEME.SCHEMENAME, P_SCHEME.SCHEMECODE,
							P_BLOCK.BLOCKNAME, P_BLOCK.ADDRESS1, P_BLOCK.ADDRESS2, P_BLOCK.ADDRESS3, P_BLOCK.POSTCODE

			
			)AS Records
			
	) AS Result 
	WHERE Result.ROW BETWEEN @offset AND @limit
	ORDER BY JsSort DESC
	
					
	-- REPAIRS TOTAL COUNT				
					
					SELECT  @totalRecord = COUNT(*) OVER ()
					
					FROM	FL_CO_APPOINTMENT
							INNER JOIN FL_FAULT_JOBTIMESHEET ON FL_FAULT_JOBTIMESHEET.appointmentid = FL_CO_APPOINTMENT.appointmentid
							INNER JOIN FL_FAULT_LOG ON FL_FAULT_LOG.faultlogid=FL_FAULT_JOBTIMESHEET.faultlogid
							INNER JOIN FL_FAULT ON FL_FAULT.faultid = FL_FAULT_LOG.faultid
					WHERE	FL_CO_APPOINTMENT.operativeid=@operativeId
							AND (FL_FAULT_JOBTIMESHEET.StartTime >=@fromDate)
							AND (FL_FAULT_JOBTIMESHEET.EndTime <= @toDate)
					GROUP BY  FL_FAULT_JOBTIMESHEET.FaultLogId, FL_FAULT_LOG.jobSheetNumber,FL_CO_APPOINTMENT.operativeid,FL_FAULT.description
	
   
   END
   ELSE IF @aptType = 'Fuel Servicing'
   BEGIN
   
   	--=======================================================================================================
    -- APPLIANCE APPOINTMENTS
    -- NOTE : Currently no table exist to track appointment actual start and end time that is why appointment 
	-- start and end time in AS_APPOINTMENTS has been used in ticket # 9542. It is legacy issue
    --=======================================================================================================
    
	SELECT * FROM(
	SELECT *, row_number() OVER (ORDER BY JsSort DESC) AS ROW	
	FROM (
    
			SELECT	'JSG' + Convert( NVarchar, AA.JSGNUMBER ) As JsRef
					,'Appliance Servicing' As Title
					,'Appliance Servicing' As InspectionType
					,CONVERT(DECIMAL(10,2),DATEDIFF(MINUTE, AS_JOBTIMESHEET.StartTime, AS_JOBTIMESHEET.EndTime) / 60.0) As Hours
					,AA.JSGNUMBER AS JsSort	
					,'' As ComponentName
					,ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS1, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS2, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS3, '') + ', '
					+ ISNULL(P__PROPERTY.POSTCODE, '') AS AppointmentAddress
			FROM
					AS_APPOINTMENTS AS AA
					Inner Join AS_JOBTIMESHEET on AS_JOBTIMESHEET.AppointmentId = AA.APPOINTMENTID
					INNER JOIN AS_JOURNAL ON AA.JOURNALID = AS_JOURNAL.JOURNALID
                    INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
					

			WHERE
					AA.ASSIGNEDTO = @operativeId
					AND AA.appointmentstatus NOT IN ('NotStarted','No Entry')
					AND CONVERT(DATE, AA.APPOINTMENTDATE) BETWEEN @fromDate AND @toDate
					
			
			
	)AS Records
	) AS Result 
	WHERE Result.ROW BETWEEN @offset AND @limit
	ORDER BY JsSort DESC
	
	
	-- APPLIANCE TOTAL COUNT
	
	SELECT  @totalRecord = COUNT(*)
	FROM
			AS_APPOINTMENTS AS AA
	WHERE
			AA.ASSIGNEDTO = @operativeId
			AND AA.appointmentstatus NOT IN ('NotStarted','No Entry')
			AND CONVERT(DATE, AA.APPOINTMENTDATE) BETWEEN @fromDate AND @toDate
	

   END
   ELSE IF @aptType = 'Planned'
   BEGIN
   
    --=============================
    -- PLANNED APPOINTMENTS
    --=============================    
 	SELECT * FROM(
	SELECT *, row_number() OVER (ORDER BY JsSort DESC) AS ROW	
	FROM (   
	
			SELECT
				'JSN' + Convert( NVarchar, PLANNED_APPOINTMENTS.APPOINTMENTID ) AS JsRef 
				,Planned_Appointment_Type.Planned_Appointment_Type As Title
				,Planned_Appointment_Type.Planned_Appointment_Type AS InspectionType  
				,PLANNED_JOB_HOURS.Hours  As Hours
				,PLANNED_APPOINTMENTS.APPOINTMENTID AS JsSort

				,CASE WHEN PAT.Planned_Appointment_Type='Misc'    
					THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + '(misc)', 'Miscellaneous') 
					WHEN PAT.Planned_Appointment_Type='Adaptation'    
					THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + '(misc)', 'Miscellaneous')    
					ELSE ISNULL(PLANNED_COMPONENT.COMPONENTNAME, 'Miscellaneous')    
				END  AS ComponentName 

				--,PLANNED_COMPONENT.COMPONENTNAME As ComponentName
				--,ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
    --            + ISNULL(P__PROPERTY.ADDRESS1, '') + ' '
    --            + ISNULL(P__PROPERTY.ADDRESS2, '') + ' '
    --            + ISNULL(P__PROPERTY.ADDRESS3, '') + ', '
    --            + ISNULL(P__PROPERTY.POSTCODE, '') AS AppointmentAddress
				,CASE
					WHEN PLANNED_JOURNAL.PROPERTYID IS NOT NULL THEN
						ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')+ ', ' + ISNULL(P__PROPERTY.postcode, '')
					WHEN PLANNED_JOURNAL.BlockId > 0 THEN
						ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
						+ ISNULL(P_BLOCK.ADDRESS1 , '') 
						+ ISNULL(', '+P_BLOCK.ADDRESS2, '') 
						+ ISNULL( ' '+P_BLOCK.ADDRESS3, '')
						+ ISNULL( ', '+P_BLOCK.POSTCODE, '')		
					ELSE
						ISNULL(P_SCHEME.SCHEMENAME , '') + ISNULL( ', '+P_SCHEME.SCHEMECODE, '')
				END AS AppointmentAddress
				
			FROM
				(	SELECT	AppointmentId
							,CONVERT(DECIMAL(10,2),SUM(DATEDIFF(MINUTE, PJ.StartTime,PJ.EndTime)) / 60.0)  As Hours
					FROM
							PLANNED_JOBTIMESHEET AS PJ
					WHERE	CONVERT(DATE, PJ.StartTime) >= @fromDate
							AND CONVERT(DATE, PJ.EndTime) <= @toDate
					GROUP BY AppointmentId
				) AS PLANNED_JOB_HOURS 
				INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOB_HOURS.AppointmentId = PLANNED_APPOINTMENTS.AppointmentId
				INNER JOIN Planned_Appointment_Type ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = Planned_Appointment_Type.Planned_Appointment_TypeId
				INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
				LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID

				LEFT JOIN PLANNED_APPOINTMENT_TYPE PAT ON PLANNED_JOURNAL.APPOINTMENTTYPEID = PAT.Planned_Appointment_TypeId 
				LEFT JOIN 
					(SELECT	PLANNED_COMPONENT.COMPONENTID AS COMPONENTID , TRADES  = 
					STUFF((SELECT ', ' + G_TRADE.Description 
					   FROM PLANNED_COMPONENT_TRADE COMP_TRADE_B 
					   INNER JOIN G_TRADE ON COMP_TRADE_B.TRADEID = G_TRADE.TradeId 
					   WHERE COMP_TRADE_B.COMPONENTID  = COMP_TRADE_A.COMPONENTID
					   ORDER BY COMP_TRADE_B.SORDER ASC
					  FOR XML PATH('')), 1, 2, '')
					  ,SUM (DURATION ) AS DURATION
						FROM	PLANNED_COMPONENT 
					LEFT JOIN PLANNED_COMPONENT_TRADE AS COMP_TRADE_A ON PLANNED_COMPONENT.COMPONENTID = COMP_TRADE_A.COMPONENTID 
					GROUP BY PLANNED_COMPONENT.COMPONENTID,COMP_TRADE_A.COMPONENTID)
					 as ComponentStat ON PLANNED_JOURNAL.COMPONENTID = ComponentStat.COMPONENTID
				 

				--INNER JOIN P__PROPERTY ON  P__PROPERTY.PROPERTYID = PLANNED_JOURNAL.PROPERTYID
				LEFT JOIN P__PROPERTY on PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
				LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
					Case 
						WHEN PLANNED_JOURNAL.SchemeId is NULL THEN P__PROPERTY.SchemeId 
						ELSE 		
						PLANNED_JOURNAL.SchemeId 	
					END
				LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID =
					Case WHEN PLANNED_JOURNAL.BlockId is NULL THEN P__PROPERTY.BLOCKID 
						ELSE 		
						PLANNED_JOURNAL.BlockId 	
					END		
				
			WHERE
				PLANNED_APPOINTMENTS.assignedto = @OperativeId			
			
	)AS Records
	) AS Result 
	WHERE Result.ROW BETWEEN @offset AND @limit
	ORDER BY JsSort DESC
	
	
	-- PLANNED COUNT
	SELECT  @totalRecord = COUNT(*)
	FROM
		(	SELECT	AppointmentId
					,CONVERT(DECIMAL(10,2),SUM(DATEDIFF(MINUTE, PJ.StartTime,PJ.EndTime)) / 60.0)  As Hours
			FROM
					PLANNED_JOBTIMESHEET AS PJ
			WHERE	CONVERT(DATE, PJ.StartTime) >= @fromDate
					AND CONVERT(DATE, PJ.EndTime) <= @toDate
			GROUP BY AppointmentId
		) AS PLANNED_JOB_HOURS 
		INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOB_HOURS.AppointmentId = PLANNED_APPOINTMENTS.AppointmentId
		INNER JOIN Planned_Appointment_Type ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = Planned_Appointment_Type.Planned_Appointment_TypeId
		INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
		LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID
		
	WHERE
		PLANNED_APPOINTMENTS.assignedto = @OperativeId
	
   
   END
   ELSE IF @aptType = 'Void'
   BEGIN
   
    --=======================================================================================================
    -- VOID APPOINTMENTS
    -- NOTE : Currently no table exist to track appointment actual start and end time that is why appointment 
	-- start and end time in PDR_APPOINTMENTS has been used in ticket # 9542. It is legacy issue
    --=======================================================================================================
    
    SELECT * FROM(
	SELECT *, row_number() OVER (ORDER BY JsSort DESC) AS ROW	
	FROM (  

	SELECT	DISTINCT
		Case When V_RequiredWorks.RequiredWorksId IS NULL Then 		
		ISNULL(( 'JSV' + Convert( NVarchar, PDR_JOURNAL.JournalID )),'N/A' )
		ELSE 
		ISNULL(( 'JSV' + Convert( NVarchar, V_RequiredWorks.RequiredWorksId  )),'N/A' )
		END
		 AS JsRef
				,ISNULL(PDR_MSATType.MSATTypeName,'') As Title
				,ISNULL(PDR_MSATType.MSATTypeName,'') As InspectionType  
				,CONVERT(DECIMAL(10,2),SUM(DATEDIFF(MINUTE, V_JOBTIMESHEET.StartTime, V_JOBTIMESHEET.EndTime)) / 60.0) As Hours
				,'' As JsSort
				,'' As ComponentName
				,ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')+ ', ' + ISNULL(P__PROPERTY.postcode, '')
			    AS AppointmentAddress

		FROM  PDR_APPOINTMENTS
	
				INNER JOIN V_JOBTIMESHEET ON V_JOBTIMESHEET.AppointmentId = PDR_APPOINTMENTS.APPOINTMENTID 
				INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
				Left JOIN V_RequiredWorks ON PDR_JOURNAL.JOURNALID =  V_RequiredWorks.WorksJournalId  
				INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATID

				INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTYPEID = PDR_MSATTYPE.MSATTYPEID
				LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
				LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
				LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID

		WHERE	PDR_APPOINTMENTS.Assignedto = @OperativeId
				AND PDR_APPOINTMENTS.appointmentstatus NOT IN ('NotStarted','No Entry')
				AND CONVERT(DATE, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE) >= @fromDate
				AND CONVERT(DATE, PDR_APPOINTMENTS.APPOINTMENTENDDATE) <= @toDate

		GROUP BY  V_JOBTIMESHEET.JobSheetId, PDR_JOURNAL.JournalID,V_RequiredWorks.RequiredWorksId,PDR_APPOINTMENTS.ASSIGNEDTO,PDR_MSATType.MSATTypeName,
							PDR_MSAT.PropertyId, P__PROPERTY.HouseNumber, P__PROPERTY.ADDRESS1, P__PROPERTY.ADDRESS2, P__PROPERTY.ADDRESS3,
							P__PROPERTY.postcode, PDR_MSAT.SchemeId, P_SCHEME.SCHEMENAME, P_SCHEME.SCHEMECODE,
							P_BLOCK.BLOCKNAME, P_BLOCK.ADDRESS1, P_BLOCK.ADDRESS2, P_BLOCK.ADDRESS3, P_BLOCK.POSTCODE
		)AS Records
	) AS Result 
	WHERE Result.ROW BETWEEN @offset AND @limit
	ORDER BY JsSort DESC
	
	
	-- VOID TOTLA COUNT
	SELECT  @totalRecord = COUNT(*) OVER ()
	FROM  PDR_APPOINTMENTS	
				INNER JOIN V_JOBTIMESHEET ON V_JOBTIMESHEET.AppointmentId = PDR_APPOINTMENTS.APPOINTMENTID 
				INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
				Left JOIN V_RequiredWorks ON PDR_JOURNAL.JOURNALID =  V_RequiredWorks.WorksJournalId  
				INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATID
				INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTYPEID = PDR_MSATTYPE.MSATTYPEID
	WHERE	PDR_APPOINTMENTS.Assignedto = @OperativeId
			AND V_RequiredWorks.Duration IS NOT NULL
			AND PDR_APPOINTMENTS.appointmentstatus NOT IN ('NotStarted','No Entry')
			AND CONVERT(DATE, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE) >= @fromDate
			AND CONVERT(DATE, PDR_APPOINTMENTS.APPOINTMENTENDDATE) <= @toDate
	GROUP BY  V_JOBTIMESHEET.JobSheetId, PDR_JOURNAL.JournalID,V_RequiredWorks.RequiredWorksId,PDR_APPOINTMENTS.ASSIGNEDTO,PDR_MSATType.MSATTypeName
	
   END
   
   SET @totalCount = @totalRecord
   
   
END

GO

