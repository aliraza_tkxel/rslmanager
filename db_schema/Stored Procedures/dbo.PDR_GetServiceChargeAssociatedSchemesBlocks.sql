USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetServiceChargeReport]    Script Date: 23/10/2018 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetServiceChargeAssociatedSchemesBlocks') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeAssociatedSchemesBlocks AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeAssociatedSchemesBlocks]
		@orderItemId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	
	DECLARE  @itemIncPropertyCount INT , @minSchemeId INT, @blockProperties INT, @schemeProperties INT
		, @SchemeId INT, @BlockId INT, @PropertyId NVARCHAR(40), @BlockScheme INT


	SELECT	@minSchemeId = MIN(schemeid) 
	FROM	F_PurchaseItemSCInfo 
	WHERE	OrderItemId = @orderItemId 
			AND IsActive = 1

	 IF @minSchemeId = -1
		BEGIN
			
			--===============================================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES WHEN ALL SCHEMES SELECTED
			--===============================================================

			SELECT	  @orderItemId AS OrderItemId
					, P_SCHEME.Schemeid AS SchemeId
					, SchemeName
					, ISNULL(BlockInfo.BlockCount,0) BlockCount
					, ISNULL(PropInc.PropCount,0) PropInCount
					, ISNULL(PropeExc.PropCount,0) PropExcCount
			FROM	P_SCHEME
					LEFT JOIN (SELECT	SchemeId, COUNT(propertyid) PropCount
							   FROM		P__PROPERTY
							   WHERE	PROPERTYID NOT IN (	SELECT	propertyid 
															FROM	F_ServiceChargeExProperties 
															WHERE	PurchaseItemId = @orderItemId )
										
							   GROUP BY SCHEMEID) PropInc ON  P_SCHEME.SCHEMEID = PropInc.SchemeId
					LEFT JOIN (SELECT	SchemeId, COUNT(DISTINCT blockid) BlockCount
							   FROM		P__PROPERTY
							   WHERE	PROPERTYID NOT IN (	SELECT	propertyid 
															FROM	F_ServiceChargeExProperties 
															WHERE	PurchaseItemId = @orderItemId )
										AND  (BLOCKID IS NOT NULL AND BLOCKID > 0)
							   GROUP BY SCHEMEID) BlockInfo ON  P_SCHEME.SCHEMEID = BlockInfo.SchemeId

					LEFT JOIN (	SELECT	PP.SchemeId, COUNT(FSEP.PROPERTYID) PropCount
								FROM	F_ServiceChargeExProperties FSEP
										INNER JOIN P__PROPERTY PP ON FSEP.PROPERTYID = PP.PROPERTYID
								WHERE	PurchaseItemId = @orderItemId 
								GROUP BY PP.SchemeId
								) PropeExc ON  P_SCHEME.SCHEMEID = PropeExc.SchemeId
			ORDER BY CAST(SUBSTRING(SCHEMENAME, 1,CASE	WHEN PATINDEX('%[^0-9]%',SCHEMENAME) > 0 THEN 
												PATINDEX('%[^0-9]%',SCHEMENAME) - 1 
											ELSE LEN(SCHEMENAME) 
											END
											) AS INT) ASC, SCHEMENAME ASC

		END
	ELSE
		BEGIN

			--============================================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES WHEN SELECTED SCHEMES 
			--============================================================

			CREATE TABLE #ItemPropertyInfo
			(
				schemeId INT,
				propertyIncluded INT
			)

			CREATE TABLE #PropertyInc
			(
				propertyId NVARCHAR(40)
			)


			DECLARE PropertyCount_Cursor CURSOR FOR
			SELECT	SchemeId, BlockId, PropertyId
			FROM	F_PurchaseItemSCInfo
			WHERE   OrderItemId = @orderitemid AND IsActive = 1

			OPEN PropertyCount_Cursor
	

			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId

			WHILE @@FETCH_STATUS = 0
			BEGIN

				SET @itemIncPropertyCount = 0	

				IF @PropertyId != '-1'
				BEGIN

					IF NOT EXISTS(  SELECT  1
									FROM	F_ServiceChargeExProperties 
									WHERE	PurchaseItemId = @orderitemid 
											AND PropertyId = @PropertyId)
					BEGIN
						SET @itemIncPropertyCount = @itemIncPropertyCount + 1
					END

					INSERT INTO #PropertyInc (propertyId)
					VALUES (@PropertyId)
			
				END
				ELSE IF @BlockId != -1 AND @BlockId IS NOT NULL
				BEGIN
					 SET @blockProperties = 0					 

					 SELECT @blockProperties = COUNT(PROPERTYID)
					 FROM	P__PROPERTY 
					 WHERE  BLOCKID = @BlockId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

					INSERT INTO #PropertyInc (propertyId)
					SELECT PROPERTYID
					FROM   P__PROPERTY 
					WHERE  BLOCKID = @BlockId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)
				
					 SET @itemIncPropertyCount = @itemIncPropertyCount + @blockProperties

				END
				ELSE IF @SchemeId != -1
				BEGIN
					 SET @schemeProperties = 0

					 SELECT @schemeProperties = COUNT(PROPERTYID) 
					 FROM	P__PROPERTY 
					 WHERE  SCHEMEID = @SchemeId AND PropertyId NOT IN (SELECT  PROPERTYID 
																		FROM	F_ServiceChargeExProperties 
																		WHERE	PurchaseItemId = @orderitemid)


					INSERT INTO #PropertyInc (propertyId)
					SELECT PROPERTYID
					FROM   P__PROPERTY 
					WHERE  SCHEMEID = @SchemeId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

					 SET @itemIncPropertyCount = @itemIncPropertyCount + @schemeProperties
				END
				
				IF EXISTS (SELECT	1
						   FROM		#ItemPropertyInfo
						   WHERE	SchemeId = @SchemeId)
						   BEGIN
								UPDATE	#ItemPropertyInfo
								SET		propertyIncluded = propertyIncluded + @itemIncPropertyCount
								WHERE   SchemeId = @SchemeId
						   END
						   ELSE
						   BEGIN
								INSERT INTO #ItemPropertyInfo (schemeId, propertyIncluded)
								VALUES (@SchemeId, @itemIncPropertyCount)
						   END

				
			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId
			END

			CLOSE  PropertyCount_Cursor
			DEALLOCATE PropertyCount_Cursor

			
			SELECT	@orderItemId AS OrderItemId
					,#ItemPropertyInfo.SchemeId
					, SchemeName
					,ISNULL(BlockProp.BlockCount,0) BlockCount 
					,ISNULL(#ItemPropertyInfo.propertyIncluded,0) PropInCount
					, ISNULL(PropeExc.PropCount,0) PropExcCount 
			FROM	#ItemPropertyInfo
					INNER JOIN P_SCHEME ON #ItemPropertyInfo.SCHEMEID = P_SCHEME.SCHEMEID
					LEFT JOIN (	SELECT	PP.SchemeId, COUNT(FSEP.PROPERTYID) PropCount
								FROM	F_ServiceChargeExProperties FSEP
										INNER JOIN P__PROPERTY PP ON FSEP.PROPERTYID = PP.PROPERTYID
								WHERE	PurchaseItemId = @orderItemId 
								GROUP BY PP.SchemeId
								) PropeExc ON  P_SCHEME.SCHEMEID = PropeExc.SchemeId
					LEFT JOIN (	SELECT	SCHEMEID, COUNT(DISTINCT PP.BLOCKID) BlockCount
								FROM	#PropertyInc
										INNER JOIN P__PROPERTY PP ON  #PropertyInc.PROPERTYID = PP.PROPERTYID COLLATE SQL_Latin1_General_CP1_CI_AS
								WHERE	SCHEMEID IS NOT NULL
								GROUP BY SCHEMEID								
								) BlockProp ON P_SCHEME.SCHEMEID = BlockProp.SCHEMEID
			ORDER BY CAST(SUBSTRING(SCHEMENAME, 1,CASE	WHEN PATINDEX('%[^0-9]%',SCHEMENAME) > 0 THEN 
												PATINDEX('%[^0-9]%',SCHEMENAME) - 1 
											ELSE LEN(SCHEMENAME) 
											END
											) AS INT) ASC, SCHEMENAME ASC

			DROP TABLE #ItemPropertyInfo
			DROP TABLE #PropertyInc

		END


END
