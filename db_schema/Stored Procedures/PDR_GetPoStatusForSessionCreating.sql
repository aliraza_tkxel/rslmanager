USE [RSLBHALive]
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'PDR_GetPoStatusForSessionCreating'))
  DROP PROCEDURE PDR_GetPoStatusForSessionCreating

 GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <11/12/2015>
-- Description:	<Get PO status for opening PO link>
-- Web Page:	<Email to schedular>
-- EXEC PDR_GetPoStatusForSessionCreating  265606
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetPoStatusForSessionCreating]
	@orderId int
AS
BEGIN


 SELECT Description As PoStatus
 FROM FL_FAULT_STATUS WHERE faultstatusid =  (select statusid from   FL_FAULT_LOG 
 WHERE FaultLogID = (select FL.FaultLogId from FL_FAULT_JOURNAL J
 INNER JOIN FL_FAULT_LOG FL ON  J.FaultLogId = FL.FaultLogId
 INNER JOIN  FL_CONTRACTOR_WORK  CW   ON CW.JournalId  =J.JournalId
 
  WHERE CW.PURCHASEORDERID =  @orderId))
	
END
