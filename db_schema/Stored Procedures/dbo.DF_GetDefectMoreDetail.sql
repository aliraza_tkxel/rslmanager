USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectMoreDetail]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.[DF_GetDefectMoreDetail]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[DF_GetDefectMoreDetail] AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[DF_GetDefectMoreDetail]
@defectId INT,
@appointmentType varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--===================================================================
	-- Property Detail
	--===================================================================

	if @appointmentType = 'Property'
	BEGIN

		Select	
			ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme,
			ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') as Address,
			ISNULL(P__PROPERTY.TOWNCITY,'N/A') as TownCity,
			P__PROPERTY.PROPERTYID as PropertyId,
			ISNULL(P__PROPERTY.COUNTY,'N/A') as County,
			ISNULL(P__PROPERTY.POSTCODE,'N/A') as Postcode,

			ISNULL(C__CUSTOMER.FIRSTNAME+ ' ' + C__CUSTOMER.LASTNAME ,'N/A') as Customer,		
			ISNULL(C_ADDRESS.MOBILE ,'N/A') as Mobile,
			ISNULL(C_ADDRESS.TEL ,'N/A') as Telephone,
			ISNULL(C_ADDRESS.EMAIL,'N/A') as Email

		From	P_PROPERTY_APPLIANCE_DEFECTS 
			INNER JOIN P__PROPERTY ON P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID = P__PROPERTY.PROPERTYID 
			LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
			LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
											AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																						FROM	C_CUSTOMERTENANCY 
																						WHERE	TENANCYID=C_TENANCY.TENANCYID 
																						AND C_TENANCY.ENDDATE IS NULL)
			LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
			LEFT JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_ADDRESS.ISDEFAULT = 1  
			LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
			LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 

		WHERE 
			PropertyDefectId=@defectId

	END
	ELSE if @appointmentType = 'Scheme'
	BEGIN

		Select	
			ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme,
			ISNULL(P_SCHEME.SCHEMENAME,'')  as Address,
			'N/A' as TownCity,
			CONVERT(VARCHAR,P_SCHEME.SCHEMEID) as PropertyId,
			'N/A' as County,
			'N/A' as Postcode,

			'N/A' as Customer,		
			'N/A' as Mobile,
			'N/A' as Telephone,
			'N/A' as Email

		From	P_PROPERTY_APPLIANCE_DEFECTS 
			INNER JOIN P_SCHEME ON P_PROPERTY_APPLIANCE_DEFECTS.SchemeId = P_SCHEME.SCHEMEID 
		WHERE 
			PropertyDefectId=@defectId

	END
	ELSE if @appointmentType = 'Block'
	BEGIN
		Select	
			ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme,
			ISNULL(P_BLOCK.BLOCKNAME,'') +' '+ ISNULL(P_BLOCK.ADDRESS1,'') +' '+ ISNULL(P_BLOCK.ADDRESS2,'') +' '+ ISNULL(P_BLOCK.ADDRESS3,'') as Address,
			ISNULL(P_BLOCK.TOWNCITY,'N/A') as TownCity,
			CONVERT(VARCHAR,P_BLOCK.BLOCKID) as PropertyId,
			ISNULL(P_BLOCK.COUNTY,'N/A') as County,
			ISNULL(P_BLOCK.POSTCODE,'N/A') as Postcode,

			'N/A' as Customer,		
			'N/A' as Mobile,
			'N/A' as Telephone,
			'N/A' as Email

		From	P_PROPERTY_APPLIANCE_DEFECTS 
			INNER JOIN P_Block ON P_Block.BLOCKID = P_PROPERTY_APPLIANCE_DEFECTS.BlockId 
			LEFT JOIN P_SCHEME on P_Block.SCHEMEID = P_SCHEME.SCHEMEID 

		WHERE 
			PropertyDefectId=@defectId
	END
	--===================================================================
	-- Condition Work Detail
	--===================================================================

	 SELECT 
			D.JournalId									AS JournalId,
		d.PropertyDefectId as DefectId
		,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
		,COALESCE(D.ModifiedDate,D.DateCreated,d.DefectDate) AS CategoryUpdated
		,ISNULL(E.FIRSTNAME +' ' +E.LASTNAME, 'N/A') AS CategoryUpdatedBy
		,s.TITLE AS Approval
		,'N/A' AS Appointment
		,COALESCE(d.DefectNotes, d.ActionNotes) AS Notes
		, 'N/A' AS Status
 
 FROM
	P_PROPERTY_APPLIANCE_DEFECTS AS D
	LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
	LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
	LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
	INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
	LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
	--INNER JOIN P__PROPERTY P ON P.PROPERTYID = d.PropertyId	
	inner join PDR_STATUS S on s.STATUSID=d.DefectJobSheetStatus
where 
	 D.PropertyDefectId = @defectId

END
