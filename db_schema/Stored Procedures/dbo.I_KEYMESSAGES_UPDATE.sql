SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_KEYMESSAGES_UPDATE]
(
	@KeyMessageTitle varchar(50),
	@KeyMessageText varchar(150),
	@Original_KeyMessageID int,
	@KeyMessageID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_KEYMESSAGES] SET [KeyMessageTitle] = @KeyMessageTitle, [KeyMessageText] = @KeyMessageText WHERE (([KeyMessageID] = @Original_KeyMessageID));
	
SELECT KeyMessageID, KeyMessageTitle, KeyMessageText FROM I_KEYMESSAGES WHERE (KeyMessageID = @KeyMessageID)
GO
