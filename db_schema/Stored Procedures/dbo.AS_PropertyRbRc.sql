SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- EXEC AS_PropertyRentBalance 
	--@propertyId = 'A010060001'
	--@rentBalance  = 0
	--@rentCharge = 0
-- Author:		<NoorMuhammad>
-- Create date: <05/12/2012>
-- Description:	<AS_PropertyRentBalance >
-- Web Page: PropertyRecord.aspx => Get the property Rent Balance & Rent Charge while adding letter
-- =============================================
CREATE PROCEDURE [dbo].[AS_PropertyRbRc](
	@propertyId varchar(12),
	@rentBalance float output,
	@rentCharge float output
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 		
		@rentBalance = ISNULL(SUM(F_RENTJOURNAL.AMOUNT), 0) 
	FROM  C_TENANCY 
		INNER JOIN F_RENTJOURNAL ON C_TENANCY.TenancyID = F_RENTJOURNAL.TenancyID
	WHERE  
		(F_RENTJOURNAL.STATUSID NOT IN (1,4) OR F_RENTJOURNAL.PAYMENTTYPE IN (17,18))
		AND C_TENANCY.ENDDATE IS NULL 
		AND C_TENANCY.PROPERTYID = @propertyId	
		
		
	SELECT @rentCharge = Totalrent FROM P_FINANCIAL WHERE PROPERTYID =@propertyId

END
GO
