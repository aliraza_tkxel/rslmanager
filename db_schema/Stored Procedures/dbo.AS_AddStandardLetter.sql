USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_StandardLetters]    Script Date: 07/05/2018 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[AS_StandardLetters]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_StandardLetters] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_AddStandardLetter]
	-- Add the parameters for the stored procedure here
	@StatusId					int,
	@ActionId					int,
	@Title						varchar(50),
	@Code						varchar(50),
	@IsAlternativeServicing		bit, 
	@Body						varchar(MAX),
	@CreatedBy					int,
	@ModifiedBy					int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [RSLBHALive].[dbo].[AS_StandardLetters]
           ([StatusId]
           ,[ActionId]
           ,[Title]
           ,[Code]
           ,[Body]
           ,[CreatedBy]
           ,[ModifiedBy]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[SignOffLookupCode]
           ,[TeamId]
           ,[FromResourceId]
           ,[IsPrinted]
           ,[IsActive]
		   ,[IsAlternativeServicing])
     VALUES
           (@StatusId,
           @ActionId,
           @Title,
           @Code,
           @Body,
           @CreatedBy,
           @ModifiedBy,
           GETDATE(),
           GETDATE(),
           null,
           null,
           null,
           0,
           1,
		   @IsAlternativeServicing)
END
