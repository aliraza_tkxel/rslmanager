SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_GetFaultManagementSearchResultsCount] 

/* ===========================================================================
-- 	EXEC FL_GetFaultManagementSearchResultsCount 
			@SearchTerm = ''
--  NAME:           FL_GetFaultManagementSearchResultsCount
--  DATE CREATED:   1 March 2013
--  Author:     	Aamir Waheed
--	Webpage:		View/Reports/ReportArea.aspx  
--  PURPOSE:        To count rows based on Search from Fault Management Page
--  IN:				@searchTerm
--  OUT:            Nothing '	
 '==============================================================================*/
	(	
		-- Quick find parameter is passed as Search Criteria
		@searchTerm VARCHAR(max) = NULL				
	)		
AS
	
	DECLARE @selectClause VARCHAR(8000),
	        @fromClause   VARCHAR(8000),
	        @whereClause  VARCHAR(8000),	        
	        --@orderClause  VARCHAR(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @searchCriteria VARCHAR(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @searchCriteria = '' 
        
    IF @searchTerm IS NOT NULL AND @searchTerm != ''
       SET @searchCriteria = @searchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT.Description Like ''%'+ LTRIM(@searchTerm) + '%'' AND' 
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

    --========================================================================================	        
    -- Begin building SELECT clause
      SET @selectClause = 'SELECT' +                      
                       CHAR(10) + CHAR(9) + 'COUNT(FL_FAULT.FaultID) As numOfRows '           
                        
    -- End building SELECT clause
    --========================================================================================    

    --========================================================================================    
    -- Begin building FROM clause
    
    SET @fromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT INNER JOIN' +                      
                      CHAR(10) + CHAR(9) + 'FL_AREA ON FL_FAULT.AreaID = FL_AREA.AreaID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_LOCATION ON FL_AREA.LocationID = FL_LOCATION.LocationID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID'                    
    
    -- End building FROM clause
    --========================================================================================                                

    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @whereClause =  CHAR(10)+  CHAR(10) + 'WHERE (' + 
                       
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @searchCriteria +
                        
                        -- select only those enquiries which are not removed logically i.e. RemoveFlag = False
                        CHAR(10) + CHAR(9) + ' 1 =1 )'
                        
    -- End building WHERE clause
    --========================================================================================
--PRINT (@selectClause + @fromClause + @whereClause )
    
EXEC (@selectClause + @fromClause + @whereClause)
GO
