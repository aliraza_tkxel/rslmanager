USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[CM_PurchaseOrderContractorDetails]') IS NULL		
	EXEC('CREATE PROCEDURE dbo.[CM_PurchaseOrderContractorDetails] AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[CM_PurchaseOrderContractorDetails]    Script Date: 	29/08/2017 14:21:37 ******/

-- =============================================
-- Author:		Ali Raza
-- Create date:	07/09/2017
-- Description:	Assign Cyclical Services to  Contractor
-- WebPage:		PropertyDataRestructure/Views/CyclicalServices/AcceptCyclicalPurchaseOrder.aspx
-- =============================================
ALTER PROCEDURE [dbo].[CM_PurchaseOrderContractorDetails]
	@orderId int 			
AS
BEGIN
SELECT cw.PurchaseOrderId,cwd.PurchaseOrderItemId,
       ISNULL(SCH.SCHEMENAME,'-') AS SchemeName,
       ISNULL(B.BLOCKNAME,'-') AS BlockName,
       I.ItemName AS ServiceRequired,
       'Every '+cast(s.Cycle AS NVARCHAR)+' '+ CT.CycleType AS CYCLE,
       CONVERT(NVARCHAR(20),s.ContractCommencement,103)AS Commencement,
       Cast(CONVERT(DECIMAL(10,2),s.CycleValue) AS NVARCHAR) AS CycleValue,
       Cast(CONVERT(DECIMAL(10,2),cw.ContractNetValue) AS NVARCHAR) AS ContractorNetCost,
       Cast(CONVERT(DECIMAL(10,2),cw.Vat) AS NVARCHAR) AS ContractorVat,
       Cast(CONVERT(DECIMAL(10,2),cw.ContractGrossValue) AS NVARCHAR) AS ContractorGrossCost,
       ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS OrderedBy,
       CONVERT(NVARCHAR(20),cw.AssignedDate,103) AS OrderedOn,
       ISNULL(con.FIRSTNAME, '') + ISNULL(' ' + con.LASTNAME, '') AS Contact,
       O.NAME AS Contractor,
       CONVERT(NVARCHAR(20),cwd.CycleDate,103) AS CycleDate,
       CM_Status.Title As [Status],
       Cast(CONVERT(DECIMAL(10,2),cwd.NetCost) AS NVARCHAR) AS NetCost,
       Cast(CONVERT(DECIMAL(10,2),cwd.VAT) AS NVARCHAR) AS Vat,
       Cast(CONVERT(DECIMAL(10,2),cwd.GrossCost) AS NVARCHAR) AS GrossCost,
       cw.CMContractorId,cwd.WorkDetailId, cw.RequiredWorks as MoreDetail,cw.SendApprovalLink,
       Case WHEN B.BLOCKID is NULL then D.PostCode ELSE B.POSTCODE END AS PostCode,
      ISNULL( CONVERT(NVARCHAR(20),cwd.CycleCompleted,103),'-') AS CycleCompleted, cwds.Title as CWStatus,
      ISNULL(Invoice.PurchaseItemImage,'') AS PurchaseItemImage,
       ROW_NUMBER() OVER(ORDER BY cwd.PurchaseOrderItemId ASC)as [Row]
       
FROM CM_ContractorWork cw
--INNER JOIN CM_ContractorWork cw ON s.ServiceItemId = cw.ServiceItemId
Cross APPLY(Select top 1 *  from CM_ServiceItems_History I where I.PORef = cw.PurchaseOrderId Order by I.ServiceItemHistoryId DESC ) s
INNER JOIN CM_ContractorWorkDetail cwd ON cw.CMContractorId = cwd.CMContractorId
INNER JOIN PA_ITEM I ON s.ItemId = I.ItemID
LEFT JOIN P_BLOCK B ON s.BlockId=B.BLOCKID
LEFT JOIN P_SCHEME SCH ON s.SchemeId=SCH.SCHEMEID
OR B.SchemeId=SCH.SCHEMEID
left Join PDR_DEVELOPMENT D on sch.DEVELOPMENTID = D.DEVELOPMENTID
INNER JOIN PDR_CycleType CT ON s.CycleType=CT.CycleTypeId
INNER JOIN E__EMPLOYEE E ON cw.AssignedBy = E.EMPLOYEEID
INNER JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID
Left JOIN E__EMPLOYEE con ON cw.ContactId = con.EMPLOYEEID
INNER JOIN S_ORGANISATION O ON cw.ContractorId = O.ORGID
INNER JOIN CM_Status ON s.StatusId = CM_Status.StatusId 
Left JOIN CM_Status cwds ON cwd.StatusId = cwds.StatusId 
OUTER APPLY (SELECT TOP 1 image_url AS PurchaseItemImage FROM F_PURCHASEITEM_LOG PIL WHERE PIL.ORDERITEMID = cwd.PurchaseOrderItemId AND PIL.image_url IS NOT NULL ORDER BY PIL.TIMESTAMP DESC) Invoice

WHERE cw.PurchaseOrderId=@orderId


---======================================================================
---Get scheme/Block Asbestos Information
---======================================================================
Declare @blockId INT, @schemeId INT

Select @blockId = s.BlockId,@schemeId=s.SchemeId  FROM CM_ServiceItems s where s.PORef = @orderId

SELECT    PRI.ASBRISKID AS Risk ,P.RISKDESCRIPTION AS ElementDescription 
                      
                       
from P_ASBRISK PRI, P_PROPERTY_ASBESTOS_RISKLEVEL PR  
                                LEFT JOIN P_PROPERTY_ASBESTOS_RISK PAR ON PR.PROPASBLEVELID= PAR.PROPASBLEVELID   
                                INNER JOIN P_ASBRISKLEVEL PL ON PR.ASBRISKLEVELID = PL.ASBRISKLEVELID  
                                INNER JOIN P_ASBESTOS P ON PR.ASBESTOSID = P.ASBESTOSID   
                                LEFT JOIN dbo.E__EMPLOYEE E ON PR.UserId = E.EMPLOYEEID
where PAR.ASBRISKID = PRI.ASBRISKID  AND P.Other = 0
AND(PR.SchemeId=@schemeId or @schemeId is NULL) AND (PR.BlockId=@blockId or @blockId is NULL)AND (@schemeId is not NULL or @blockId is not NULL)
AND PR.DateRemoved is NULL
END