SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_AssignTODropDown
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/02/2014>
-- Description:	<Description,,Populate the assign to dropdown on Assign PopUp>
-- WebPage: TenancySupportReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_AssignTODropDown]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	DECLARE @JobRole INT
	
	SELECT @JobRole = CAST(ISNULL(DEFAULTVALUE, '0') AS INT)
	FROM RSL_DEFAULTS
	WHERE DEFAULTNAME = 'TenancySupportAssignee'
	
	Select (ISNULL(E__EMPLOYEE.FIRSTNAME,' ')+ISNULL(' ' + E__EMPLOYEE.LASTNAME,' ')) as AssigneName,E_JOBDETAILS.EMPLOYEEID as AssigneeId,E_CONTACT.WORKEMAIL as Email  
	FROM E_CONTACT 
	INNER JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
	INNER JOIN E_JOBDETAILS on E_JOBDETAILS.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID
	LEFT JOIN E_JOBROLE JR ON E_JOBDETAILS.JobRoleId = JR.JobRoleId
	
	where E_JOBDETAILS.active=1
		 AND (E_JOBDETAILS.JobRoleId = @JobRole)
END
GO
