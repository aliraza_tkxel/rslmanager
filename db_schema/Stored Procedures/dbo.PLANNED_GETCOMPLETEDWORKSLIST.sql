USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GETCOMPLETEDWORKSLIST]    Script Date: 03/09/2016 13:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,18/03/2015>
-- Description:	<Description,,Get the completed planned works list for completed works report in reports area of Planned> 
-- EXEC PLANNED_GETCOMPLETEDWORKSLIST -1,-1,''
-- =============================================


IF OBJECT_ID('dbo.PLANNED_GETCOMPLETEDWORKSLIST') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_GETCOMPLETEDWORKSLIST AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PLANNED_GETCOMPLETEDWORKSLIST] 
@schemeId int =-1,
@plannedType int =-1,
@searchText VARCHAR(200),
--Parameters which would help in sorting and paging
@pageSize int = 30,
@pageNumber int = 1,
@sortColumn varchar(500) = 'Address', 
@sortOrder varchar (5) = 'DESC',
@totalCount int = 0 output	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --Completed Status
        @completedStatus varchar(100),
        @cancelledStatus varchar(100),
        
        --variables for paging
        @offset int,
		@limit int
		
				--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SET @completedStatus = '''COMPLETED'''
		SET @cancelledStatus = '''Cancelled'''
		
		--=======================================================
		
		
		CREATE TABLE #temp_tbl_locations
		(
		[ItemID]	int,
		[ItemName]	nvarchar(200)
		)
		INSERT #temp_tbl_locations EXEC PLANNED_GetLocations


		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		SET @searchCriteria = '1 = 1 AND PA.APPOINTMENTSTATUS <> ' + @cancelledStatus
		--SET @searchCriteria = '1 = 1 '
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PLANNED_JOURNAL.JOURNALID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PA.APPOINTMENTID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PJS.SCHEMENAME LIKE ''%' + @searchText + '%'''   
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PJB.BLOCKNAME LIKE ''%' + @searchText + '%'''  
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') + ISNULL('', ''+P__PROPERTY.TOWNCITY, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E.FIRSTNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E.LASTNAME LIKE ''%' + @searchText + '%'') '
		END	
		
		
		IF @schemeId > -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (P__PROPERTY.SCHEMEID = '+ CONVERT(VARCHAR(10),@schemeId)+' OR PJS.SCHEMEID = '+CONVERT(nvarchar(10), @schemeId)+')'	
		END
		
		IF @plannedType > -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PA.Planned_Appointment_TypeId = '+ CONVERT(VARCHAR(10),@plannedType)
		END

		-- End building SearchCriteria clause   
		--========================================================================================
		


				
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here		
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') 
		PLANNED_JOURNAL.JOURNALID as Ref
		,APPOINTMENT_AID.JSN as JSN 
		,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address	
        ,ISNULL(PAT.Planned_Appointment_Type,''n/a'') as [Type]        
        , ISNULL( PP.ItemName,''n/a'' ) AS LOCATION			
		, ISNULL( PV.ValueDetail,''n/a'' ) AS [DESCRIPTION]         
		,coalesce(MISC_TRADE.DESCRIPTION,COMP_TRADE.DESCRIPTION,''n/a'') as Trades
		,CONVERT(VARCHAR(10),PA.APPOINTMENTDATE,103) AS APPOINTMENT
		,PA.APPOINTMENTENDDATE AS COMPLETEDBYDATE
		,ISNULL(E.FIRSTNAME,'''')+'' ''+ ISNULL(E.LASTNAME,'''') AS COMPLETEDBYNAME		
		,CONVERT(VARCHAR(10),PA.APPOINTMENTENDDATE,103)+''  ''+SUBSTRING(ISNULL(E.FIRSTNAME,''''),1,1)+'' ''+ ISNULL(E.LASTNAME,'''') AS COMPLETEDBY
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  P__PROPERTY.PropertyId
		when PLANNED_JOURNAL.BlockId > 0 THEN  CONVERT(nvarchar(10), PJB.blockid)
		when PLANNED_JOURNAL.SchemeId > 0 THEN CONVERT(nvarchar(10), PJS.schemeid) 
		end as PropertyId ,ISNULL(PLANNED_JOURNAL.COMPONENTID,''0'') AS COMPONENTID,PA.APPOINTMENTID
		,ISNULL(PA.APPOINTMENTSTATUS,''n/a'') AS AppointmentStatus
		,COALESCE(PLANNED_SUBSTATUS.TITLE, PA.APPOINTMENTSTATUS) AS PlannedSubStatus 		
		,ISNULL(CASE 
			WHEN PAT.PLANNED_APPOINTMENT_TYPE = ''Condition'' THEN
				CASE 
					WHEN PLANNED_CONDITIONWORKS.COMPONENTID IS NOT NULL THEN
						CONDITION_COMPONENT.COMPONENTNAME
					ELSE
						CONDITION_ITEM.ITEMNAME
				END
			WHEN PAT.PLANNED_APPOINTMENT_TYPE = ''Planned'' THEN
				PLANNED_COMPONENT.COMPONENTNAME
			END,''n/a'') AS Component
			
		,P__PROPERTY.HouseNumber 
		, P__PROPERTY.ADDRESS1
		,PA.APPOINTMENTDATE
		,PA.APPOINTMENTENDDATE
		,ISNULL(PJS.SCHEMENAME,'''') as PJScheme
		,ISNULL(PJB.BLOCKNAME, '''')   AS PJBlock
	   , CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  ''Property''
		when PLANNED_JOURNAL.BlockId > 0 THEN  ''Block''
		when PLANNED_JOURNAL.SchemeId > 0 THEN ''Scheme''
		end as AppointmentType
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  P__PROPERTY.ADDRESS1
		when PLANNED_JOURNAL.BlockId > 0 THEN  PJB.ADDRESS1
		when PLANNED_JOURNAL.SchemeId > 0 THEN PJS.sCHEMENAME 
		end as PrimaryAddress  
		,ISNULL(CASE WHEN PLANNED_JOURNAL.PROPERTYID IS NULL THEN PJB.POSTCODE
		WHEN PLANNED_JOURNAL.PROPERTYID IS NOT NULL THEN P__PROPERTY.POSTCODE
		END, '''') as Postcode    	
		'

	
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) + '	FROM 
		PLANNED_APPOINTMENTS PA
		INNER JOIN PLANNED_JOURNAL	 ON PA.JournalId = PLANNED_JOURNAL.JOURNALID
		LEFT JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		LEFT JOIN P_SCHEME PJS ON PLANNED_JOURNAL.SchemeId = PJS.SCHEMEID  
		LEFT JOIN P_BLOCK PJB ON PLANNED_JOURNAL.BlockId = PJB.BLOCKID     
		INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PA.ASSIGNEDTO 
		LEFT JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
		INNER JOIN (SELECT PLANNED_APPOINTMENTS.APPOINTMENTID as AID , ''JSN'' +RIGHT(''0000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),4) as JSN    
					FROM PLANNED_APPOINTMENTS ) as APPOINTMENT_AID ON PA.APPOINTMENTID = APPOINTMENT_AID.AID
		LEFT JOIN PLANNED_APPOINTMENT_TYPE PAT ON  PAT.Planned_Appointment_TypeId   =PA.Planned_Appointment_TypeId				
		LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID
		LEFT JOIN PLANNED_CONDITIONWORKS ON  PLANNED_JOURNAL.JOURNALID =  PLANNED_CONDITIONWORKS.JOURNALID
		LEFT JOIN PLANNED_COMPONENT CONDITION_COMPONENT ON PLANNED_CONDITIONWORKS.COMPONENTID =  CONDITION_COMPONENT.COMPONENTID
		LEFT JOIN PA_PROPERTY_ATTRIBUTES PPA ON PLANNED_CONDITIONWORKS.ATTRIBUTEID = PPA.ATTRIBUTEID
		LEFT JOIN PA_ITEM_PARAMETER PIP ON PPA.ITEMPARAMID = PIP.ITEMPARAMID
		LEFT JOIN PA_ITEM CONDITION_ITEM ON PIP.ITEMID = CONDITION_ITEM.ITEMID
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID	
		
		LEFT JOIN PLANNED_SUBSTATUS ON PA.JOURNALSUBSTATUS = PLANNED_SUBSTATUS.SUBSTATUSID
					
		LEFT JOIN PLANNED_MISC_TRADE PM ON PA.APPOINTMENTID = PM.APPOINTMENTID
		LEFT JOIN G_TRADE MISC_TRADE ON PM.TRADEID = MISC_TRADE.TRADEID
		LEFT JOIN PLANNED_COMPONENT_TRADE PCT ON PA.COMPTRADEID = PCT.COMPTRADEID
		LEFT JOIN G_TRADE COMP_TRADE ON PCT.TRADEID = COMP_TRADE.TRADEID
		LEFT JOIN #temp_tbl_locations PP ON PM.ParameterId = PP.ItemID			
		LEFT JOIN PA_PARAMETER_VALUE PV ON PM.ParameterValueId = PV.ValueID	
			          
          '
      		-- End building From clause
		--======================================================================================== 														  
		
				
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10) +' cast(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else len(HouseNumber) end) as int)'  + CHAR(10)+ @sortOrder + ', ADDRESS1 '  + CHAR(10) 					
		END
		
		IF(@sortColumn = 'PMO')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'PMO' 		
		END
		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'JSN' 		
		END
		IF(@sortColumn = 'TYPE')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TYPE' 		
		END		
		IF(@sortColumn = 'LOCATION')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'LOCATION' 		
		END
		IF(@sortColumn = 'DESCRIPTION')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'DESCRIPTION' 		
		END
		IF(@sortColumn = 'TRADE')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TRADE' 		
		END
		IF(@sortColumn = 'APPOINTMENT')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'APPOINTMENTDATE' 		
		END
		
		IF(@sortColumn = 'COMPLETEDBY')
		BEGIN	
			SET @sortColumn = CHAR(10)+ 'COMPLETEDBYNAME, COMPLETEDBYDATE' 		
		END
		
		IF(@sortColumn = 'COMPLETEDBY')
		BEGIN
			SET @orderClause =  CHAR(10) + ' Order By COMPLETEDBYNAME'+CHAR(10) + @sortOrder +','+ 'COMPLETEDBYDATE '+ CHAR(10) + @sortOrder
		END
		ELSE
		BEGIN
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		END		
		
				
		
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
			
		DROP TABLE #temp_tbl_locations	
		-- End building the Count Query
		--========================================================================================	    
				
END
