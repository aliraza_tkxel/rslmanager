USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_VoidAppointmentsArranged]    Script Date: 03/14/2016 11:45:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_VoidAppointmentsArranged') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_VoidAppointmentsArranged AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_VoidAppointmentsArranged] 
	(
		-- Add the parameters for the stored procedure here
		
		@searchedText varchar(8000)='',
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'TerminationSortDate',
		@sortOrder varchar (5) = 'ASC',
		@status varchar (150)='',
		@patchId		int=-1,
		@schemeId	    int=-1,
		@totalCount int=0 output
	)
AS
BEGIN
	DECLARE @SelectClause nvarchar(MAX),
        @fromClause   nvarchar(MAX),
        @whereClause  nvarchar(MAX),	        
        @orderClause  nvarchar(MAX),	
        @mainSelectQuery nvarchar(MAX),        
        @rowNumberQuery nvarchar(MAX),
        @finalQuery nvarchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- BEGIN BUILDING SEARCHCRITERIA CLAUSE
		-- THESE CONDITIONS WILL BE ADDED INTO WHERE CLAUSE BASED ON SEARCH CRITERIA PROVIDED
		
		SET @SEARCHCRITERIA = ' 1=1 '
		IF(@SEARCHEDTEXT != '' OR @SEARCHEDTEXT != NULL)
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + 'AND (P.PostCode LIKE ''%'+@SEARCHEDTEXT+'%'' 							
				OR (P.PROPERTYID + ISNULL(P.FLATNUMBER,'''')+ISNULL('' ''+P.HouseNumber,'''') + ISNULL('' ''+P.ADDRESS1,'''') + ISNULL('' ''+P.ADDRESS2,'''') + ISNULL('' ''+P.ADDRESS3,'''') + ISNULL(''
				''+P.TOWNCITY,''''))
				LIKE ''%'+@SEARCHEDTEXT+'%'')'
		END 
				
			
		--THESE CONDITIONS WÍLL BE USED IN EVERY CASE
		
		SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ( ( P.STATUS = 1 AND (P.SUBSTATUS <> 21 OR P.SUBSTATUS IS NULL) ) 
																	OR (P.Status=2 AND P.SUBSTATUS = 22 ) ) --And AS_APPOINTMENTS.IsVoid =1
															AND AS_APPOINTMENTS.APPOINTMENTSTATUS NOT IN (''InProgress'')
															AND P.PROPERTYTYPE NOT IN (	SELECT PROPERTYTYPEID 
																						FROM P_PROPERTYTYPE 
																						WHERE DESCRIPTION IN (''Garage'',''Car Port'',''Car Space'')
																					  )
		'
	
		-- END BUILDING SEARCHCRITERIA CLAUSE   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause			
		
		SET @SelectClause = 'SELECT TOP (' + CONVERT(NVARCHAR(10),@limit) + ') 
							AS_APPOINTMENTS.JSGNUMBER AS JSGNUMBER
							,CONVERT(varchar,AS_APPOINTMENTS.APPOINTMENTDATE,103)+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME+''-''+ AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENT
							,AS_APPOINTMENTS.ASSIGNEDTO
							,AS_APPOINTMENTS.LOGGEDDATE as LOGGEDDATE,
							LEFT(E__EMPLOYEE.FIRSTNAME, 1)+LEFT(E__EMPLOYEE.LASTNAME,1) AS ENGINEER,
							ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'''') +'' ''+ ISNULL(P.ADDRESS3,'''')+ ISNULL('' , ''+P.TOWNCITY,'''')  AS ADDRESS ,  
							P.HouseNumber as HouseNumber,
							P.ADDRESS1 as ADDRESS1,
							P.ADDRESS2 as ADDRESS2,
							P.ADDRESS3 as ADDRESS3,							
							P.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,CP12.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,CP12.ISSUEDATE)) AS DAYS,
							P_FUELTYPE.FUELTYPE AS FUEL,
							--ISNULL(PV.ValueDetail, ''-'')AS FUEL,
							P.PROPERTYID,
							T.TENANCYID,
							AS_APPOINTMENTS.APPOINTMENTID,
							AS_APPOINTMENTS.JournalId,							
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3 AND 
								YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
								
							ISNULL(T.MOBILE,''N/A'') as Mobile,
							T.NAME AS NAME,
							
							ISNULL(ST.DESCRIPTION, ''N/A'') AS PropertyStatus,
							PATCH.PatchId as PatchId,
							PATCH.Location as PatchName,
							T.TELEPHONE as Telephone,
							CONVERT(DATETIME,AS_APPOINTMENTS.APPOINTMENTDATE+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME) as AppointmentSortDate,
							CP12.ISSUEDATE as LGSRDate
							,AS_APPOINTMENTS.NOTES as Notes
							,T.CUSTOMERID as CustomerId
							,CONVERT(varchar,T.TerminationDate,103) as Termination
							,T.TerminationSortDate as TerminationSortDate,
							AS_EmailStatus.EmailStatusId as EmailStatusId,
							AS_EmailStatus.StatusImagePath as EmailImagePath,
							AS_APPOINTMENTS.EmailDescription as EmailDescription,
							AS_EmailStatus.StatusDescription as EmailStatusDescription,
							AS_SmsStatus.SmsStatusId as SmsStatusId,
							AS_SmsStatus.StatusImagePath as SmsImagePath,
							AS_APPOINTMENTS.SmsDescription as SmsDescription,
							AS_SmsStatus.StatusDescription as SmsStatusDescription,
							AS_APPOINTMENTS.JournalHistoryId as JournalHistoryId,
							AS_PushNoticificationStatus.PushNoticificationId as PushNoticificationStatusId,
							AS_PushNoticificationStatus.StatusImagePath as PushNoticificationImagePath,
							AS_APPOINTMENTS.PushNoticificationDescription as PushNoticificationDescription,
							AS_PushNoticificationStatus.StatusDescription as PushNoticificationStatusDescription,
							AS_APPOINTMENTS.APPOINTMENTDATE as ApDate
							'
				
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause				
		
		SET @fromClause = CHAR(10) + ' FROM P__PROPERTY P  

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
LEFT JOIN E_PATCH PATCH ON P.PATCH = PATCH.PATCHID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
LEFT JOIN P_LGSR CP12 ON P.PROPERTYID = CP12.PROPERTYID
LEFT JOIN P_FUELTYPE ON P.FUELTYPE = P_FUELTYPE.FUELTYPEID

Cross Apply(
			Select Max(j.JOURNALID) as journalID from C_JOURNAL j 
			where j.PropertyId=P.PropertyID AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
			GROUP BY PROPERTYID
			) as CJournal
			
CROSS APPLY (
			Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination
			
			Cross Apply (
				SELECT P.PROPERTYID,
				Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
				T.TERMINATIONDATE as TerminationSortDate,
				Case 
					When M.ReletDate IS NULL Then 
						CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
					ELSE
						Convert(Varchar(50), M.ReletDate,103)
				END as ReletDate,
				J.JOURNALID,
				CT.TENANCYID,
				ISNULL(A.TEL,''N/A'') AS TELEPHONE,
				C.FIRSTNAME  + '' '' + C.LASTNAME AS NAME,
				C.CUSTOMERID,
				A.MOBILE,
				PROP_ATTRIB.PARAMETERVALUE
				
				
			FROM P__PROPERTY P
			LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES PROP_ATTRIB ON PROP_ATTRIB.PROPERTYID = P.PROPERTYID	AND
										PROP_ATTRIB.ITEMPARAMID =
										(
											SELECT
												ItemParamID
											FROM
												PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
											WHERE
												ParameterName = ''Heating Fuel''
												AND ItemName = ''Heating''
												AND PARAMETERVALUE = ''Mains Gas''
										)
			INNER JOIN C_JOURNAL J ON CJournal.journalID=J.JOURNALID
			INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID
			INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
			INNER JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1
			INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
			INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
			INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
	
			 ) As  T -- ON P.PROPERTYID=T.PROPERTYID

INNER JOIN (
	SELECT P2.PROPERTYID,J2.JOURNALID, J2.STATUSID,APP.APPOINTMENTID FROM P__PROPERTY P2 
	INNER JOIN AS_JOURNAL J2 ON P2.PROPERTYID = J2.PROPERTYID
	INNER JOIN AS_APPOINTMENTS APP ON APP.JournalId = J2.JOURNALID
	WHERE J2.ISCURRENT = 1 --AND J2.STATUSID = 2
	GROUP BY P2.PROPERTYID,J2.JOURNALID,J2.STATUSID,APP.APPOINTMENTID
) AS AS_JOURNAL ON AS_JOURNAL.PROPERTYID = P.PROPERTYID

INNER JOIN AS_STATUS ON AS_STATUS.STATUSID = AS_JOURNAL.STATUSID
INNER JOIN AS_APPOINTMENTS ON AS_JOURNAL.APPOINTMENTID = AS_APPOINTMENTS.APPOINTMENTID
INNER JOIN E__EMPLOYEE ON AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
INNER JOIN AS_EmailStatus on AS_APPOINTMENTS.EmailStatusId=AS_EmailStatus.EmailStatusId
INNER JOIN AS_SmsStatus on AS_APPOINTMENTS.SmsStatusId=AS_SmsStatus.SmsStatusId
INNER JOIN AS_PushNoticificationStatus on AS_APPOINTMENTS.PushNoticificationId=AS_PushNoticificationStatus.PushNoticificationId
										'
		
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
		END		
		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(MAX)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							
END
