SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO




CREATE PROCEDURE [dbo].[TO_HOUSE_MOVE_ENQUIRYLOG_REQUESTTRANSFER]

/* ===========================================================================
 '   NAME:           TO_HOUSE_MOVE_ENQUIRYLOG_REQUESTTRANSFER
 '   DATE CREATED:   24 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display Enquiry Detail i.e. TRANSFER Part of C_Address fields 
					are also selected as subset as these need to be displayed on View enquiry pop up.  
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
            G_LOCALAUTHORITY.DESCRIPTION as LocalAuthority,
            P_DEVELOPMENT.DEVELOPMENTNAME as Development,
            TO_HOUSE_MOVE.NOOFBEDROOMS,
            TO_HOUSE_MOVE.OCCUPANTSNOGREATER18,
            TO_HOUSE_MOVE.OCCUPANTSNOBELOW18,
			C_ADDRESS.HOUSENUMBER, 
			C_ADDRESS.ADDRESS1, 
			C_ADDRESS.ADDRESS2, 
			C_ADDRESS.ADDRESS3, 
			C_ADDRESS.TOWNCITY, 
            C_ADDRESS.POSTCODE, 
            C_ADDRESS.COUNTY, 
            C_ADDRESS.TEL 
            
	FROM
	       TO_ENQUIRY_LOG LEFT JOIN TO_HOUSE_MOVE
	       ON TO_ENQUIRY_LOG.EnquiryLogID = TO_HOUSE_MOVE.EnquiryLogID
	       LEFT JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID 
	       LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	       LEFT JOIN G_LOCALAUTHORITY ON TO_HOUSE_MOVE.LocalAuthorityID = G_LOCALAUTHORITY.LOCALAUTHORITYID
	       LEFT JOIN P_DEVELOPMENT ON TO_HOUSE_MOVE.DevelopmentID = P_DEVELOPMENT.DEVELOPMENTID
	     
	       
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)
	     AND (C_ADDRESS.ISDEFAULT = 1)
GO
