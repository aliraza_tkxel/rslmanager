USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GetFaultsToBeArrangeList]    Script Date: 11/17/2015 20:01:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 16/11/2015
-- Description:	display faults having appointment to be arranged status
 --EXEC RD_GetFaultsToBeArrangeList 
	--	@searchedText = '',
	--	@pageSize  = 30,
	--	@pageNumber  = 1,
	--	@sortColumn = 'ReportedDate',
	--	@sortOrder  = 'DESC',		
	--	@totalCount = 0 

--Modified By:Saud Ahmed   
--Modification Date: <22/02/2017>      
--
-- remove "FL_TEMP_FAULT.TempFaultID" in Select clause
-- and "ReportedDate" position changed in Order by clause
--      
-- =============================================

IF OBJECT_ID('dbo.RD_GetFaultsToBeArrangeList') IS NULL
 EXEC('CREATE PROCEDURE dbo.RD_GetFaultsToBeArrangeList AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[RD_GetFaultsToBeArrangeList]
		@searchedText VARCHAR(200) = '',
		@pageSize INT = 30,
		@pageNumber INT = 1,
		@sortColumn VARCHAR(50) = 'ReportedDate',
		@sortOrder VARCHAR (5) = 'DESC',		
		@totalCount INT = 0 OUTPUT
AS
BEGIN
	DECLARE @SelectClause VARCHAR(2000),
        @fromClause   VARCHAR(2000),
        @whereClause  VARCHAR(2000),
        @orderClause  VARCHAR(500),	
        @mainSelectQuery VARCHAR(MAX),        
        @rowNumberQuery VARCHAR(MAX),
        @finalQuery VARCHAR(MAX),
        @total INT,

        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria VARCHAR(2000),

        --variables for paging
        @offset int,
		@limit int

	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1
	
	--========================================================================================
	-- Begin building SearchCriteria clause for InProgress
	-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteria = ' 1=1 '
	
	IF(@searchedText != '' OR @searchedText IS NOT NULL)
	BEGIN
		SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (FL_FAULT.Description LIKE ''%' + @searchedText + '%'''		
		SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_SCHEME.SCHEMENAME LIKE ''%' + @searchedText + '%'''
		SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_BLOCK.BLOCKNAME LIKE ''%' + @searchedText + '%'''
		SET @searchCriteria = @searchCriteria + CHAR(10) +' OR ISNULL(P__PROPERTY.HOUSENUMBER,'''') +'' ''+ ISNULL(''''  + P__PROPERTY.ADDRESS1,'''') LIKE ''%' + @searchedText + '%'''
		SET @searchCriteria = @searchCriteria + CHAR(10) +' OR ISNULL(P_BLOCK.ADDRESS1,'''')+'' ''+ISNULL('' ''+P_BLOCK.ADDRESS2,'''') LIKE ''%' + @searchedText + '%'')'
		
	END
			-- Add checks for Scheme and Block 
	
	
	  
	--========================================================================================
	SET NOCOUNT ON;
	--========================================================================================	        
	-- Begin building SELECT clause
	
	SET @SelectClause = '
						 ISNULL(C__Customer.FirstName + '' '' + C__Customer.LastName,''-'')  As TenantName, 
						CASE 
						WHEN LEN(P__PROPERTY.PROPERTYID) > 0 THEN
							ISNULL(P__PROPERTY.HOUSENUMBER,'''') +'' ''+ ISNULL(''''  + P__PROPERTY.ADDRESS1,'''') 
						ELSE 
							CASE WHEN P_BLOCK.ADDRESS1 IS NULL AND P_BLOCK.ADDRESS2 IS NULL THEN
									''-''
								 ELSE
									ISNULL(P_BLOCK.ADDRESS1,'''')+ISNULL('' ''+P_BLOCK.ADDRESS2,'''')

							END
							 
						END AS Address
						,ISNULL(P_SCHEME.SCHEMENAME,''-'') AS Scheme
						,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block
						,COALESCE(P__PROPERTY.postcode,P_BLOCK.POSTCODE,''-'') As PostCode					
						,ISNULL(C_ADDRESS.TEL,''-'') As Telephone
						,ISNULL(C_ADDRESS.Mobile,''-'') As Mobile,
						FL_FAULT.Description As Fault,
						ISNULL(CONVERT(NVARCHAR,FL_TEMP_FAULT.submitDate),''N/A'') As ReportedDate,
						ISNULL(CONVERT(NVARCHAR,LEFT(E__EMPLOYEE.Firstname, 1) + '''' + LEFT(E__EMPLOYEE.LASTNAME, 1)),''N/A'') As ReportedBy,
						C__Customer.CustomerId As CustomerId 
						,P__PROPERTY.propertyid As PropertyId
						,FL_TEMP_FAULT.SchemeId
						,FL_TEMP_FAULT.BlockId
						,FL_TEMP_FAULT.FaultId
						,Case 
						When FL_TEMP_FAULT.PROPERTYID IS NULL THEN	
							''SbFault''	
						Else 
							''Fault''	
						End	AS FaultType 

						,FL_TEMP_FAULT.submitDate as ReportedDateSort
						'
						
	
	
	
	-- End building SELECT clause
	--======================================================================================== 	
	
	--========================================================================================    
	-- Begin building FROM clause	
	
	SET @fromClause = ' FROM FL_TEMP_FAULT   
						
						LEFT JOIN P__PROPERTY  ON FL_TEMP_FAULT.propertyid = P__PROPERTY.propertyid
						LEFT JOIN C__Customer ON FL_TEMP_FAULT.customerid  = C__Customer.Customerid
						LEFT JOIN C_ADDRESS  ON  C_ADDRESS.CUSTOMERID = C__Customer.Customerid and ISDEFAULT=1

						LEFT JOIN P_SCHEME ON FL_TEMP_FAULT.SchemeId = P_SCHEME.SCHEMEID 
						LEFT JOIN P_BLOCK ON FL_TEMP_FAULT.BlockId  = P_BLOCK.BLOCKID

						INNER JOIN FL_FAULT  ON FL_TEMP_FAULT.FaultId =FL_FAULT.FaultId 
						INNER Join FL_FAULT_TRADE  On FL_TEMP_FAULT.FaultTradeId  = FL_FAULT_TRADE.FaultTradeId 
						INNER Join G_TRADE  On FL_FAULT_TRADE.TradeId = G_TRADE.TradeId 
						INNER Join FL_AREA  On FL_TEMP_FAULT.AREAID = FL_AREA.AreaID 
						INNER Join FL_FAULT_PRIORITY On FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID 
						LEFT JOIN E__EMPLOYEE On FL_TEMP_FAULT.USERID = E__EMPLOYEE.EMPLOYEEID 
						 '					
	-- End building From clause
	--========================================================================================
	
	
	--========================================================================================
	-- Begin building OrderBy clause

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Reported')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'ReportedDateSort' 		
	END

	IF(@sortColumn = 'TenantName')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'TenantName' 		
	END

	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Address' 		
	END

	IF(@sortColumn = 'Postcode')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Postcode' 		
	END


	IF(@sortColumn = 'Telephone')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Telephone' 		
	END	

	IF(@sortColumn = 'Mobile')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Mobile' 		
	END	

	IF(@sortColumn = 'Fault')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Fault' 		
	END

	IF(@sortColumn = 'ReportedBy')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'ReportedBy' 		
	END
	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

	-- End building OrderBy clause
	--========================================================================================
	
	--========================================================================================
	-- Begin building WHERE clause

	-- This Where clause contains subquery to exclude already displayed records			  

	SET @whereClause = 	CHAR(10) + 'WHERE FL_TEMP_FAULT.itemstatusid = 3  AND (FL_TEMP_FAULT.IsAppointmentConfirmed=0 OR FL_TEMP_FAULT.IsAppointmentConfirmed IS NULL) AND' + CHAR(10) + @searchCriteria 

	-- End building WHERE clause
	--========================================================================================
	
	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery ='SELECT DISTINCT top ('+convert(varchar(10),@limit)+') '+ @SelectClause +@fromClause+@whereClause + @orderClause 

	-- End building the main select Query
	--========================================================================================																																			

	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	print(@finalQuery)
	
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================		
	Declare @selectCount nvarchar(MAX), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT  @totalCount = COUNT(*) FROM ( SELECT DISTINCT '+ @SelectClause  + @fromClause + @whereClause +' )as record'

	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;							

END
