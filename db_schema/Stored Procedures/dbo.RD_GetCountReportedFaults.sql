SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC  [dbo].[RD_GetCountReportedFaults]
-- Author:  <Adnan Mirza>
-- Create date: <21/10/2013>
-- Description: <This stored procedure gets the count of all 'Reported Faults'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetCountReportedFaults]
 -- Add the parameters for the stored procedure here

 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 
 DECLARE @StartDate DATETIME
 DECLARE @EndDate DATETIME
 
 SET @StartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ) )
 SET @EndDate = DATEADD(SS,-1,DATEADD(mm,12,@StartDate))


 SET NOCOUNT ON;
 SELECT   count(*)  
 from ( 
		SELECT distinct FL_FAULT_LOG.FaultLogID FaultLogID,FL_FAULT_LOG.StatusID ,CONVERT(VARCHAR(10),dbo.FL_FAULT_LOG.SubmitDate, 120)  ReportedDate
		FROM FL_FAULT_LOG 
	  ) ReportedFaults    
 where ReportedFaults.ReportedDate BETWEEN CONVERT(VARCHAR(10),@StartDate, 120)  AND CONVERT(VARCHAR(10),@EndDate, 120)  

END

GO
