USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_UpdateFaultNote]    Script Date: 11/11/2016 5:28:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[F_INSERT_GJ_Template]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[F_INSERT_GJ_Template] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[F_INSERT_GJ_Template]
	@gjId		int,
	@templateName nvarchar(100),
	@userId int,
	@isUpdated int OUTPUT
AS
BEGIN

BEGIN TRANSACTION

	BEGIN TRY
	
	DECLARE @TemplateId INT, @CompanyId INT,  @SchemeId INT,  @BlockId INT , @PropertyId nvarchar(40)

	SELECT	@CompanyId = CompanyId, @SchemeId = SchemeId, @BlockId = BlockId, @PropertyId = PropertyId
	FROM	NL_JOURNALENTRY
	WHERE	TXNID = @gjId

	INSERT INTO NL_GeneralJournalTemplate (TemplateName, CompanyId, CreatedDate,CreatedBy, IsActive,SchemeId, BlockId, PropertyId)
	VALUES		(@templateName, @CompanyId, GETDATE(), @userId, 1,@SchemeId,@BlockId, @PropertyId);

	SET @TemplateId = SCOPE_IDENTITY()

	INSERT INTO NL_GeneralJournalAccountTemplate (GeneralJournalTemplateId, Details, AccountId, IsDebit, CostCenterId, HeadId, ExpenditureId, Reason, DefaultDebitAmount, DefaultCreditAmount, Sequence)
	SELECT		@TemplateId,SUBSTRING(NL_JOURNALENTRYDEBITLINE.Description,CHARINDEX(' ',NL_JOURNALENTRYDEBITLINE.Description)+1,LEN(NL_JOURNALENTRYDEBITLINE.Description)) 
				, NL_JOURNALENTRYDEBITLINE.AccountId, 1, NL_JOURNALENTRYDEBITLINE.CostCentreId, NL_JOURNALENTRYDEBITLINE.HEADID
				, NL_JOURNALENTRYDEBITLINE.EXPENDITUREID, SUBSTRING(NL_JOURNALENTRYDEBITLINE.MEMO,CHARINDEX(' ',NL_JOURNALENTRYDEBITLINE.MEMO)+1,LEN(NL_JOURNALENTRYDEBITLINE.MEMO))
				,0.00,0.00, NL_JOURNALENTRYDEBITLINE.EditSequence
	FROM		NL_JOURNALENTRYDEBITLINE
	WHERE		TXNID = @gjId

	INSERT INTO NL_GeneralJournalAccountTemplate (GeneralJournalTemplateId, Details, AccountId, IsDebit, CostCenterId, HeadId, ExpenditureId, Reason, DefaultDebitAmount, DefaultCreditAmount,Sequence)
	SELECT		@TemplateId,SUBSTRING(NL_JOURNALENTRYCREDITLINE.Description,CHARINDEX(' ',NL_JOURNALENTRYCREDITLINE.Description)+1,LEN(NL_JOURNALENTRYCREDITLINE.Description)) 
				, NL_JOURNALENTRYCREDITLINE.AccountId, 0, NL_JOURNALENTRYCREDITLINE.CostCentreId, NL_JOURNALENTRYCREDITLINE.HEADID, NL_JOURNALENTRYCREDITLINE.EXPENDITUREID
				, SUBSTRING(NL_JOURNALENTRYCREDITLINE.MEMO,CHARINDEX(' ',NL_JOURNALENTRYCREDITLINE.MEMO)+1,LEN(NL_JOURNALENTRYCREDITLINE.MEMO)) 
				, 0.00,0.00,NL_JOURNALENTRYCREDITLINE.EditSequence
	FROM		NL_JOURNALENTRYCREDITLINE
	WHERE		TXNID = @gjId

	END TRY
	
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isUpdated = 0        
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
					@ErrorSeverity, -- Severity.
					@ErrorState -- State.
				);
	END CATCH;

	IF @@TRANCOUNT > 0
		BEGIN  
			COMMIT TRANSACTION;  
			SET @isUpdated = 1
		END
END



