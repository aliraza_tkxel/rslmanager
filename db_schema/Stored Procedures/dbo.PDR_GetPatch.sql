USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPatch]    Script Date: 20/4/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PDR_GetPatch') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetPatch AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Rehan Baber
-- Create date: 20/4/2017
-- Description:	Get all Patches  
-- EXEC PDR_GetPatch 

-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetPatch]			
AS
BEGIN
	
	SELECT PATCHID AS PATCHID, LOCATION FROM E_PATCH
	ORDER BY LOCATION							
END
