SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[NL_LEDGERDETAILS_ITEM] ( @PAGENUM INT = 1 ,
    @PERPAGE INT = 50 , @TXNID INT )
AS 
    BEGIN 
        SET NOCOUNT ON 
        
        DECLARE @TRANSACTIONDATE SMALLDATETIME
        DECLARE @TRANSACTIONTYPE INT
        SELECT  @TRANSACTIONDATE = TXNDATE ,
                @TRANSACTIONTYPE = TRANSACTIONTYPE
        FROM    DBO.NL_JOURNALENTRY
        WHERE   TXNID = @TXNID

           DECLARE @UBOUND INT ,
            @LBOUND INT ,
            @PAGES INT ,
            @ROWS INT

        DECLARE @TXNDATE SMALLDATETIME
         ,@LINEID INT
           -- ,@TABLEORDER INT

                --PRINT '1'
                IF @TRANSACTIONTYPE = 10
                BEGIN 
                                SELECT  @ROWS = COUNT(1)
        FROM    DBO.F_RENTJOURNAL_VOIDS FRJ
                            INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
        WHERE   CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                AND STATUS = 1
                END
                ELSE
                BEGIN
        SELECT  @ROWS = COUNT(1)
        FROM    DBO.F_RENTJOURNAL FRJ
                INNER JOIN F_ITEMTYPE FIT ON FRJ.ITEMTYPE = FIT.ITEMTYPEID
                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
        WHERE   CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                AND FIT.ITEMTYPEID = 1
                AND PAYMENTTYPE IS NULL
                --AND USERID IS NULL
                END
                SET @PAGES = @ROWS / @PERPAGE 
                
                IF @ROWS % @PERPAGE != 0 
                    SET @PAGES = @PAGES + 1 
                IF @PAGENUM < 1 
                    SET @PAGENUM = 1 
                IF @PAGENUM > @PAGES 
                    SET @PAGENUM = @PAGES 

                    -- GET THE REQUIRED INFORMATION FROM THE PAGING SET
                SELECT  CURRENTPAGE = @PAGENUM ,
                        TOTALPAGES = @PAGES ,
                        TOTALROWS = @ROWS 
                 
                SET @UBOUND = @PERPAGE * @PAGENUM  
                SET @LBOUND = @UBOUND - ( @PERPAGE - 1 ) - 1
                
                    -- THIS METHOD DETERMINES THE STRING VALUES 
                    -- FOR THE FIRST DESIRED ROW, THEN SETS THE 
                    -- ROWCOUNT TO GET IT, PLUS THE NEXT N ROWS
                    
                     IF ( @LBOUND <= 0 )                
                    BEGIN
                    --PRINT '2'
                        SET @TXNID = 0
                        SET @LINEID = 0
                        --SET @TABLEORDER = 0
                    END
                ELSE 
                    BEGIN
                    --PRINT '3'
                        SET ROWCOUNT @LBOUND 
                        
  IF @TRANSACTIONTYPE = 10
                BEGIN 
                                SELECT  @LINEID = FRJ.JOURNALID
        FROM    DBO.F_RENTJOURNAL_VOIDS FRJ
                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
        WHERE   CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                AND STATUS = 1
                END
                ELSE
                BEGIN                      
                        SELECT  --@TXNID = J.TXNID ,
                                --@TXNDATE = CAST(FLOOR(CAST(J.TIMECREATED AS FLOAT)) AS DATETIME) ,
                                @LINEID = FRJ.JOURNALID
                                --,@TABLEORDER = TABLEORDER
                                
--        SELECT  CAST(FLOOR(CAST(FRJ.ACCOUNTTIMESTAMP AS FLOAT)) AS DATETIME) AS TIMECREATED ,
--                NLTT.DESCRIPTION ,
--                FIT.DESCRIPTION ,
--                FRJ.TENANCYID ,
--                FRJ.AMOUNT
        FROM    DBO.F_RENTJOURNAL FRJ
                INNER JOIN F_ITEMTYPE FIT ON FRJ.ITEMTYPE = FIT.ITEMTYPEID
                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
        WHERE   CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                AND FIT.ITEMTYPEID = 1
                AND PAYMENTTYPE IS NULL
                --AND USERID IS NULL
                    END   
                  END
                  
                
                SET ROWCOUNT @PERPAGE 
                --PRINT '4'
                IF @TRANSACTIONTYPE = 10
                BEGIN 
                                SELECT  CAST(FLOOR(CAST(FRJ.TRANSACTIONDATE AS FLOAT)) AS DATETIME) AS TIMECREATED ,
                NLTT.DESCRIPTION ,
                'Void Rent' AS [DESCRIPTION] ,
                PROPERTYID AS TENANCYID ,
               FRJ.TOTALRENT AS AMOUNT
                                FROM    DBO.F_RENTJOURNAL_VOIDS FRJ
                                                                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
                                WHERE   CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                                                                AND STATUS = 1
                                                                AND JOURNALID > @LINEID
                END
                ELSE
                BEGIN 
                    SELECT  CAST(FLOOR(CAST(FRJ.ACCOUNTTIMESTAMP AS FLOAT)) AS DATETIME) AS TIMECREATED ,
                NLTT.DESCRIPTION ,
                FIT.DESCRIPTION ,
                FRJ.TENANCYID,
                FRJ.AMOUNT
        FROM    DBO.F_RENTJOURNAL FRJ
                INNER JOIN F_ITEMTYPE FIT ON FRJ.ITEMTYPE = FIT.ITEMTYPEID
                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
        WHERE   CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                AND FIT.ITEMTYPEID = 1
                AND PAYMENTTYPE IS NULL
                --AND USERID IS NULL
                AND JOURNALID > @LINEID
                
                SET ROWCOUNT 0 
            END
        END
GO
