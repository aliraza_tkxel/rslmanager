SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_DeleteUserPages @employeeId = 1
-- Author:		<Salman Nazir>
-- Create date: <30/10/2012>
-- Description:	<Delete all pages of an employee before add new pages>
-- Web Page: Access.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_DeleteUserPages](
@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE 
	FROM AS_User_Pages
	WHERE EmployeeId = @employeeId
END
GO
