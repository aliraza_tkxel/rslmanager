USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyByBlockId]    Script Date: 10/30/2017 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_GetPropertyByBlock]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetPropertyByBlock] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetPropertyByBlock]
	@schemeId int,
	@blockId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if(@blockId >= 1)
	BEGIN
		SELECT PROPERTYID, ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2,'') AS PROPERTYNAME
		FROM P__PROPERTY
		WHERE BLOCKID = @blockId
		ORDER BY PROPERTYNAME
	END
	ELSE
	BEGIN
		SELECT PROPERTYID, ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2,'') AS PROPERTYNAME
		FROM P__PROPERTY
		WHERE BLOCKID IN (SELECT BLOCKID FROM P_BLOCK WHERE SchemeId=@schemeId) or schemeid= @schemeId 
		ORDER BY PROPERTYNAME
	END
	
END
