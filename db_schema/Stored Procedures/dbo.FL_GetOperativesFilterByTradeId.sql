USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  PropertyCalendar.aspx (for operative drop down list)
 
    Author: Aamir Waheed
    Creation Date:  11/06/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         11/06/2015     Aamir Waheed       Created:FL_GetOperativesFilterByTradeId
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int


EXEC	@return_value = [dbo].[FL_GetOperativesFilterByTradeId]
						@tradeId = -1

SELECT	@return_value as N'@return_value'

----------------------------------------------------
*/
IF OBJECT_ID('dbo.[FL_GetOperativesFilterByTradeId]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[FL_GetOperativesFilterByTradeId] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FL_GetOperativesFilterByTradeId]
	@tradeId INT = -1
AS
BEGIN	
	SELECT
		U.EmployeeId					AS operativeId
		,E.FIRSTNAME + ' ' + E.LASTNAME	AS operativeName
	FROM
		AS_USER AS U
			INNER JOIN E__EMPLOYEE AS E ON U.EmployeeId = E.EMPLOYEEID
			INNER JOIN AS_USERTYPE ON U.UserTypeID = AS_USERTYPE.UserTypeID
			LEFT JOIN E_TRADE AS T ON E.EMPLOYEEID = T.EmpId AND T.TradeId = @tradeId
			INNER JOIN E_JOBDETAILS ON U.EmployeeId = E_JOBDETAILS.EMPLOYEEID 
	WHERE
		(@tradeId = -1 OR T.TradeId IS NOT NULL)  AND U.IsActive=1 AND (E_JOBDETAILS.ACTIVE = 1)
	ORDER BY operativeName
END