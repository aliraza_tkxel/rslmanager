
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_ScheduledAppointmentsDetail @startDate='31/12/2013',@endDate='04/01/2013'
-- Author:		Aqib Javed
-- Create date: <09/23/2012>
-- Last Modified: <10/05/2012>
-- Description:	<This Stored Proceedure shows the Appointment details on the Calendar Page>
-- Webpage : FuelScheduling.aspx 

-- =============================================
CREATE PROCEDURE [dbo].[AS_ScheduledAppointmentsDetail]
@startDate date,
@endDate date	
AS
BEGIN

	-----------------------------------SELECT Gas / Appliance Servicing Appointments -----------
	--------------------------------------------------------------------------------------------------------
	SELECT DISTINCT 
		AS_APPOINTMENTS.APPOINTMENTSHIFT AS APPOINTMENTSHIFT,
		CONVERT(varchar, APPOINTMENTDATE,103) as AppointmentDate, 
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress,
		AS_APPOINTMENTS.ASSIGNEDTO AS ASSIGNEDTO,
		P__PROPERTY.POSTCODE AS PostCode,
		AS_Status.Title AS Title,
		AS_APPOINTMENTS.APPOINTMENTSTARTTIME AS APPOINTMENTSTARTTIME,
		AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENTENDTIME,
		AS_APPOINTMENTS.APPOINTMENTID AS APPOINTMENTID,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,CONVERT(varchar, APPOINTMENTDATE,103) as ValidateAppointmentDate
		,'Gas' as AppointmentType,
		C_TENANCY.TENANCYID AS TenancyId,
		P_STATUS.[DESCRIPTION] AS PropertyStatus


		FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
		WHERE AS_JOURNAL.IsCurrent = 1
		AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate and @endDate
		-- Added Condition to filter only the appointment of Active gas operatives
		AND AS_APPOINTMENTS.ASSIGNEDTO IN ( SELECT DISTINCT EmployeeId FROM AS_USER WHERE UserTypeID = 3 AND IsActive  = 1 )
	
	-----------------------------------UNION SELECT Reactive Repair Appointments ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL
    
	SELECT DISTINCT substring(Time,6,len(Time)) as APPOINTMENTSHIFT
		,CONVERT(varchar(20), APPOINTMENTDATE,103) as AppointmentDate
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') 
				+' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,FL_CO_APPOINTMENT.OperativeID as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,FL_FAULT_STATUS.[Description] as Title
		,FL_CO_APPOINTMENT.[Time] as APPOINTMENTSTARTTIME
		,FL_CO_APPOINTMENT.EndTime as APPOINTMENTENDTIME
		,FL_CO_APPOINTMENT.AppointmentID
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + [Time],103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
		,CONVERT(varchar(20), APPOINTMENTDATE,103) as ValidateAppointmentDate
		,'Reactive Repair' as AppointmentType	
		,C_TENANCY.TENANCYID
		,P_STATUS.[DESCRIPTION] 
		from FL_CO_APPOINTMENT 
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId 
		INNER JOIN	FL_FAULT_LOG  ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID 
		INNER JOIN	FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID  = FL_FAULT_STATUS.FaultStatusID 
		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID=P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
		
	WHERE 

		FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		AND FL_FAULT_STATUS.[Description]!='Cancelled'
		-- Added Condition to filter only the appointment of Active gas operatives
		AND FL_CO_APPOINTMENT.OPERATIVEID IN (SELECT DISTINCT EmployeeId FROM AS_USER WHERE UserTypeID = 3 AND IsActive  = 1)
		
	
	-----------------------------------UNION SELECT Stock / Void Appointments ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL
	
	SELECT 
		RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),2) as APPOINTMENTSHIFT
		,PS_Appointment.AppointStartDateTime as AppointmentDate		
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,PS_Appointment.CreatedBy as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,'' as Title
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),8) as APPOINTMENTSTARTTIME
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointEndDateTime,100),8) as APPOINTMENTENDTIME		
		,PS_Appointment.AppointId as APPOINTMENTID
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as EndTimeInSec	
		,CONVERT(varchar(20), AppointStartDateTime,103) as ValidateAppointmentDate	
		,PS_Appointment.AppointType as AppointmentType
		,C_TENANCY.TENANCYID  	
		,P_STATUS.[DESCRIPTION]  
	FROM PS_Appointment 
		INNER JOIN PS_Property2Appointment ON PS_Appointment.AppointId = PS_Property2Appointment.AppointId		
		INNER JOIN P__PROPERTY ON PS_Property2Appointment.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	
	
	WHERE 
		(PS_Appointment.AppointType = 'Stock' OR AppointType ='Void')	
		AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		-- Added Condition to filter only the appointment of Active gas operatives
		AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		AND PS_Appointment.SurveyourUserName IN (	SELECT DISTINCT PS_Appointment.SurveyourUserName 
													FROM PS_Appointment 
														INNER JOIN AC_LOGINS ON PS_Appointment.SurveyourUserName = AC_LOGINS.LOGIN 
														INNER JOIN AS_USER ON AC_LOGINS.EMPLOYEEID = AS_USER.EMPLOYEEID 
													WHERE AS_USER.UserTypeID = 3 AND AS_USER.IsActive  = 1 )
													
		
		-- Order By Caluse for all record from UNION(S) ALL
		ORDER BY StartTimeInSec ASC
	
	-- Old Stored Procedure	
	/*

	-----------------------------------Create temporaray appointment table-----------------------------------
	---------------------------------------------------------------------------------------------------------
	CREATE TABLE #AllAppointments(
		APPOINTMENTSHIFT nvarchar(5)
		,AppointmentDate date 		
		,CustAddress nvarchar(1000)
		,ASSIGNEDTO int
		,PostCode nvarchar(10)
		,Title nvarchar(150)
		,APPOINTMENTSTARTTIME nvarchar(10)
		,APPOINTMENTENDTIME nvarchar(10)
		,APPOINTMENTID int
		,StartTimeInSec int
		,EndTimeInSec int
		,ValidateAppointmentDate date
		,AppointmentType varchar(20)
		,TenancyId int
		,PropertyStatus varchar(50)
		
	)
	
	-----------------------------------Insert Gas / Appliance Servicing Appointments In Temp Table-----------
	---------------------------------------------------------------------------------------------------------
	INSERT INTO #AllAppointments
	(	
		APPOINTMENTSHIFT
		,AppointmentDate
		,CustAddress
		,ASSIGNEDTO
		,PostCode
		,Title
		,APPOINTMENTSTARTTIME
		,APPOINTMENTENDTIME
		,APPOINTMENTID
		,StartTimeInSec
		,EndTimeInSec
		,ValidateAppointmentDate
		,AppointmentType
		,TenancyId
		,PropertyStatus 
	)
Select 
distinct 
AS_APPOINTMENTS.APPOINTMENTSHIFT ,
CONVERT(varchar, APPOINTMENTDATE,103) as AppointmentDate, 
ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress,
AS_APPOINTMENTS.ASSIGNEDTO,
P__PROPERTY.POSTCODE,
AS_Status.Title,
AS_APPOINTMENTS.APPOINTMENTSTARTTIME,
AS_APPOINTMENTS.APPOINTMENTENDTIME,
AS_APPOINTMENTS.APPOINTMENTID,
datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec,
datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
,CONVERT(varchar, APPOINTMENTDATE,103) as ValidateAppointmentDate
,'Gas' as AppointmentType,
C_TENANCY.TENANCYID,
P_STATUS.[DESCRIPTION]


FROM AS_APPOINTMENTS 
INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
WHERE AS_JOURNAL.IsCurrent = 1
AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate and @endDate

	--Select distinct AS_APPOINTMENTS.APPOINTMENTSHIFT
	--,CONVERT(varchar, APPOINTMENTDATE,103) as AppointmentDate
	--,C__CUSTOMER.FIRSTNAME+'  '+C__CUSTOMER.LASTNAME AS Name
	--,C_ADDRESS.MOBILE,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
	--,AS_APPOINTMENTS.ASSIGNEDTO
	--,P__PROPERTY.POSTCODE,AS_Status.Title
	--,AS_APPOINTMENTS.APPOINTMENTSTARTTIME
	--,AS_APPOINTMENTS.APPOINTMENTENDTIME
	--,AS_APPOINTMENTS.APPOINTMENTID
	--,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec
	--,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
	--,CONVERT(varchar, APPOINTMENTDATE,103) as ValidateAppointmentDate
	--,'Gas' as AppointmentType
	 
	--FROM AS_APPOINTMENTS 
	--INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
	--INNER JOIN	AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
	--INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
	--INNER JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID
	--INNER JOIN C_CUSTOMERTENANCY ON C_TENANCY.TENANCYID = C_CUSTOMERTENANCY.TENANCYID
	--INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID 
	--INNER JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID WHERE 
	--AS_JOURNAL.IsCurrent = 1
 --   AND C_ADDRESS.ISDEFAULT = 1 
 --   AND C_CUSTOMERTENANCY.ENDDATE IS NULL
 --   AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID= (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID 
 --   AND C_TENANCY.ENDDATE IS NULL)
 --   AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate and @endDate  	
    
	
	-----------------------------------Insert Reactive Repair Appointments In Temp Table---------------------
	---------------------------------------------------------------------------------------------------------
	INSERT INTO #AllAppointments
	(	
		APPOINTMENTSHIFT
		,AppointmentDate		
		,CustAddress
		,ASSIGNEDTO
		,PostCode
		,Title
		,APPOINTMENTSTARTTIME
		,APPOINTMENTENDTIME
		,APPOINTMENTID
		,StartTimeInSec
		,EndTimeInSec
		,ValidateAppointmentDate
		,AppointmentType
		,TenancyId 		
		,PropertyStatus 
	)
	
	Select distinct substring(Time,6,len(Time)) as APPOINTMENTSHIFT
	,CONVERT(varchar(20), APPOINTMENTDATE,103) as AppointmentDate
	,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
	,FL_CO_APPOINTMENT.OperativeID as ASSIGNEDTO
	,P__PROPERTY.POSTCODE
	,FL_FAULT_STATUS.[Description] as Title
	,FL_CO_APPOINTMENT.[Time] as APPOINTMENTSTARTTIME
	,FL_CO_APPOINTMENT.EndTime as APPOINTMENTENDTIME
	,FL_CO_APPOINTMENT.AppointmentID
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + [Time],103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,CONVERT(varchar(20), APPOINTMENTDATE,103) as ValidateAppointmentDate
	,'Reactive Repair' as AppointmentType	
	,C_TENANCY.TENANCYID
	,P_STATUS.[DESCRIPTION] 
	from FL_CO_APPOINTMENT 
	INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId 
	INNER JOIN	FL_FAULT_LOG  ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID 
	INNER JOIN	FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID  = FL_FAULT_STATUS.FaultStatusID 
	INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID=P__PROPERTY.PROPERTYID
	INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
	LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())

	--INNER JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID
	--INNER JOIN C_CUSTOMERTENANCY ON C_TENANCY.TENANCYID = C_CUSTOMERTENANCY.TENANCYID 
	--INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID 
	--INNER JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	
	WHERE 
--	C_ADDRESS.ISDEFAULT = 1 
  --  AND C_CUSTOMERTENANCY.ENDDATE IS NULL
  --  AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID= (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID 
 --   AND C_TENANCY.ENDDATE IS NULL)
    FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
    AND FL_FAULT_STATUS.[Description]!='Cancelled'
    order by FL_CO_APPOINTMENT.AppointmentID 
	
	
	-----------------------------------Insert Stock / Void Appointments In Temp Table---------------------
	---------------------------------------------------------------------------------------------------------
	INSERT INTO #AllAppointments
	(	
		APPOINTMENTSHIFT
		,AppointmentDate
		,CustAddress
		,ASSIGNEDTO
		,PostCode
		,Title
		,APPOINTMENTSTARTTIME
		,APPOINTMENTENDTIME
		,APPOINTMENTID
		,StartTimeInSec
		,EndTimeInSec
		,ValidateAppointmentDate
		,AppointmentType
		,TenancyId 
		,PropertyStatus 
	)
	
	SELECT 
		RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),2) as APPOINTMENTSHIFT
		,PS_Appointment.AppointStartDateTime as AppointmentDate		
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,PS_Appointment.CreatedBy as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,'' as Title
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),8) as APPOINTMENTSTARTTIME
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointEndDateTime,100),8) as APPOINTMENTENDTIME		
		,PS_Appointment.AppointId as APPOINTMENTID
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as EndTimeInSec	
		,CONVERT(varchar(20), AppointStartDateTime,103) as ValidateAppointmentDate	
		,PS_Appointment.AppointType as AppointmentType
		,C_TENANCY.TENANCYID  	
		,P_STATUS.[DESCRIPTION]  
	FROM PS_Appointment 
	INNER JOIN PS_Property2Appointment ON PS_Appointment.AppointId = PS_Property2Appointment.AppointId
	--INNER JOIN C__CUSTOMER ON PS_Property2Appointment.CustomerId = C__CUSTOMER.CUSTOMERID
	--INNER JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
	INNER JOIN P__PROPERTY ON PS_Property2Appointment.PROPERTYID = P__PROPERTY.PROPERTYID
	INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
	LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	
	--INNER JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	--INNER JOIN C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
	--INNER JOIN C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
	--INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
	WHERE 
	(PS_Appointment.AppointType = 'Stock' OR AppointType ='Void')
	--AND C_TENANCY.Enddate is null 
	--AND C_Address.isDefault = 1  
	AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)

	-----------------------------------Select All appointments from Temp Table-------------------------------
	---------------------------------------------------------------------------------------------------------
	
	SELECT 
		APPOINTMENTSHIFT
		,AppointmentDate		
		,CustAddress
		,ASSIGNEDTO
		,PostCode
		,Title
		,APPOINTMENTSTARTTIME
		,APPOINTMENTENDTIME
		,APPOINTMENTID
		,StartTimeInSec
		,EndTimeInSec
		,ValidateAppointmentDate
		,AppointmentType
		,TenancyId
		,PropertyStatus 
	FROM #AllAppointments
	
	*/
	-- End Old Stored Procedure
END
GO
