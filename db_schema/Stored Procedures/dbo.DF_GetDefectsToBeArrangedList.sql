USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectsToBeArrangedList]    Script Date: 06/24/2016 15:11:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
/* =================================================================================      
    Page Description:  DefectScheduling.aspx  
   
    Author: Aamir Waheed  
    Creation Date:  04/08/2015  
 Description: Get Defect To Be Arranged List for defect scheduling.  
    Change History:      
      
Execution Command:  
----------------------------------------------------  
  
DECLARE @return_value int,  
  @totalCount int  
  
EXEC @return_value = [dbo].[DF_GetDefectsToBeArrangedList]  
  @totalCount = @totalCount OUTPUT  
  
SELECT @totalCount as N'@totalCount'  
  
SELECT 'Return Value' = @return_value  
  
----------------------------------------------------  
*/  
IF OBJECT_ID('dbo.DF_GetDefectsToBeArrangedList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetDefectsToBeArrangedList AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[DF_GetDefectsToBeArrangedList](  
    
  @searchText varchar(5000)='',  
  @defectCategoryId INT = -1,  
  @schemeId int = -1,  
  --Parameters which would help in sorting and paging  
  @pageSize int = 30,  
  @pageNumber int = 1,  
  @sortColumn varchar(50) = 'DefectDate',  
  @sortOrder varchar (5) = 'DESC',  
  @getOnlyCount bit=0,  
  @totalCount int=0 output  
)  
AS  
BEGIN  
DECLARE   
   
	@SelectClause varchar(max),
	@SelectClauseCount varchar(max),
    @fromClause   varchar(max),
    @whereClause  varchar(max),
	@GroupClause  varchar(max),      
	@SelectClauseScheme varchar(max),
	@SelectClauseSchemeCount varchar(max),
    @fromClauseScheme   varchar(max),
    @whereClauseScheme  varchar(max),	
	@GroupClauseScheme  varchar(max),      
	@SelectClauseBlock varchar(max),
	@SelectClauseBlockCount varchar(max),
    @fromClauseBlock   varchar(max),
    @whereClauseBlock  varchar(max),
	@GroupClauseBlock  varchar(max),      		     
	@orderClause  varchar(max),   
	@mainSelectQuery varchar(max),          
	@rowNumberQuery varchar(max),  
	@finalQuery varchar(max),  
	-- used to add in conditions in WhereClause based on search criteria provided  
	@searchCriteria varchar(5000),  
	@searchCriteriaScheme varchar(5000),
	@searchCriteriaBlock varchar(5000),
    @unionQuery varchar(1000),
          
    --variables for paging  
	@offset int,  
	@limit int,  
	@checksRequiredType varchar(200),  
	@MSATTypeId int,  
	@ArrangedStatusId int  
	--Paging Formula  
	SET @offset = 1+(@pageNumber-1) * @pageSize  
	SET @limit = (@offset + @pageSize)-1  
    
  
	--=====================Search Criteria===============================  
    
	DECLARE @defect_starting_Date DATETIME  
	SET     @defect_starting_Date = CONVERT( DATETIME, '7 Jan 2016', 106 )  
    
	SET @searchCriteria = ' 1 = 1 
		AND (AD.detectortypeid IS NULL OR AD.detectortypeid = 0) 
		AND DC.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'',''Other'')    
		AND AD.DefectDate >= ''' + CONVERT(VARCHAR(20),@defect_starting_Date, 106) + '''  
		AND (((AD.ApplianceId IS NOT NULL AND AD.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (AD.ApplianceId IS NULL or AD.ApplianceId = 0 ))   
		AND PendingDefectsCount > 0  
         '  
	SET @searchCriteriaScheme = @searchCriteria

	SET @searchCriteriaBlock = @searchCriteria

    
	IF(@searchText != '' OR @searchText != NULL)  
	BEGIN        
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ((ISNULL(P.HouseNumber, '''') + ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')  
						OR P.POSTCODE LIKE ''%' + @searchText + '%'' ' 
		SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND (ISNULL(P.SCHEMENAME, '''') LIKE ''%' + @searchText + '%'''
		SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND ((ISNULL(P.BLOCKNAME, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																		OR P.POSTCODE LIKE ''%' + @searchText + '%'' ' 
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
		SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )' 
		SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'  
	END  
    
	IF @defectCategoryId != -1  
	BEGIN  
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND AD.CategoryId = ' + CONVERT(VARCHAR, @defectCategoryId)  
		SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND AD.CategoryId = ' + CONVERT(VARCHAR, @defectCategoryId) 
		SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND AD.CategoryId = ' + CONVERT(VARCHAR, @defectCategoryId) 
	END  
	IF @schemeId != -1  
	BEGIN  
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
		SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
		SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
	END  
	--============================From Clause============================================  
	SET @fromClause = CHAR(10) +'FROM  
			P_PROPERTY_APPLIANCE_DEFECTS AD  
			INNER JOIN P__PROPERTY P ON AD.PropertyId = P.PROPERTYID
			LEFT JOIN	GS_PROPERTY_APPLIANCE ON AD.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
			INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId  
			CROSS APPLY (SELECT COUNT(CASE WHEN iD.ApplianceDefectAppointmentJournalId IS NULL THEN 1 ELSE NULL END) PendingDefectsCount  
				FROM P_PROPERTY_APPLIANCE_DEFECTS iD  
				INNER JOIN P_DEFECTS_CATEGORY iDC ON iD.CategoryId = iDC.CategoryId
				LEFT JOIN	GS_PROPERTY_APPLIANCE ON iD.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
				WHERE iD.JournalId = AD.JournalId 
				AND (iD.detectortypeid IS NULL OR iD.detectortypeid = 0)
				AND (((iD.ApplianceId IS NOT NULL AND iD.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (iD.ApplianceId IS NULL or iD.ApplianceId = 0 )) 
				AND iDC.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'',''Other'')  
				AND ID.DefectJobSheetStatus IN (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = ''Approved'')
				AND AD.DefectDate >= ''' + CONVERT(VARCHAR(20),@defect_starting_Date, 106) + '''    
				) DefectAppointment  
                
				LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P.PROPERTYID AND C_TENANCY.ENDDATE IS NULL  
			LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID   
			AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = ( SELECT MIN(CUSTOMERTENANCYID)  
						FROM C_CUSTOMERTENANCY   
						WHERE TENANCYID=C_TENANCY.TENANCYID   
						)  
			LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID  
			LEFT JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_ADDRESS.ISDEFAULT = 1      
			'  

	SET @fromClauseScheme = CHAR(10) +' FROM  
			P_PROPERTY_APPLIANCE_DEFECTS AD  
			INNER JOIN P_SCHEME P ON AD.SchemeId = P.SCHEMEID
			LEFT JOIN	GS_PROPERTY_APPLIANCE ON AD.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
			INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId  
			CROSS APPLY (SELECT COUNT(CASE WHEN iD.ApplianceDefectAppointmentJournalId IS NULL THEN 1 ELSE NULL END) PendingDefectsCount  
				FROM P_PROPERTY_APPLIANCE_DEFECTS iD  
				INNER JOIN P_DEFECTS_CATEGORY iDC ON iD.CategoryId = iDC.CategoryId
				LEFT JOIN	GS_PROPERTY_APPLIANCE ON iD.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
				WHERE iD.JournalId = AD.JournalId 
				AND (iD.detectortypeid IS NULL OR iD.detectortypeid = 0)
				AND (((iD.ApplianceId IS NOT NULL AND iD.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (iD.ApplianceId IS NULL or iD.ApplianceId = 0 )) 
				AND iDC.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'',''Other'')  
				AND ID.DefectJobSheetStatus IN (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = ''Approved'')
				AND AD.DefectDate >= ''' + CONVERT(VARCHAR(20),@defect_starting_Date, 106) + '''    
				) DefectAppointment  
			'  

	SET @fromClauseBlock = CHAR(10) +' FROM P_PROPERTY_APPLIANCE_DEFECTS AD  
			INNER JOIN P_BLOCK P ON AD.BlockId = P.BLOCKID
			LEFT JOIN	GS_PROPERTY_APPLIANCE ON AD.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
			INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId  
			CROSS APPLY (SELECT COUNT(CASE WHEN iD.ApplianceDefectAppointmentJournalId IS NULL THEN 1 ELSE NULL END) PendingDefectsCount  
				FROM P_PROPERTY_APPLIANCE_DEFECTS iD  
				INNER JOIN P_DEFECTS_CATEGORY iDC ON iD.CategoryId = iDC.CategoryId
				LEFT JOIN	GS_PROPERTY_APPLIANCE ON iD.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
				WHERE iD.JournalId = AD.JournalId 
				AND (iD.detectortypeid IS NULL OR iD.detectortypeid = 0)
				AND (((iD.ApplianceId IS NOT NULL AND iD.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (iD.ApplianceId IS NULL or iD.ApplianceId = 0 )) 
				AND iDC.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'',''Other'')  
				AND ID.DefectJobSheetStatus IN (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = ''Approved'')
				AND AD.DefectDate >= ''' + CONVERT(VARCHAR(20),@defect_starting_Date, 106) + '''    
				) DefectAppointment      
			'  
           
	--================================= Where Clause ================================  
    
	SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria + ' '    
	SET @whereClauseScheme = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaScheme + ' '    
	SET @whereClauseBlock = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaBlock + ' '    
    
	 --=======================Select Clause=============================================  
	--SELECT TOP ('+convert(VARCHAR(10),@limit)+')
	SET @SelectClause = ' 
          AD.JournalId                   AS JournalId  
         ,P.HOUSENUMBER + ISNULL('' '' + P.ADDRESS1, '''') + ISNULL('', '' + P.TOWNCITY, '''') AS Address  
         ,P.POSTCODE                    AS POSTCODE  
         ,AD.PropertyId                   AS PROPERTYID  
         ,MIN(AD.PartsDue)                  AS PartsDue  
         ,MIN(AD.DefectDate)                  AS DefectDate  
         ,SUM(CASE  
          AD.IsHeatersLeft  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isHeatersLeft  
         ,SUM(CASE  
          AD.IsCustomerHaveHeating  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isHeatingAvailable  
         ,SUM(CASE  
          AD.IsDisconnected  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isCapped  
         ,ISNULL(P.PATCH,-1)                  AS PatchId  
         ,P.COUNTY                    AS COUNTY  
         ,P.TOWNCITY                    AS TOWNCITY  
         ,ISNULL(C_ADDRESS.TEL ,''NA'')               As Telephone  
		 ,  ''''	AS Scheme
		, ''''	AS Block	
		,''Property'' AS AppointmentType
       '  

	SET @SelectClauseCount = '  SELECT  ' + @SelectClause
	SET @SelectClause = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClause
    
	SET @SelectClauseScheme = '  
          AD.JournalId                   AS JournalId  
         ,'''' AS Address  
         ,''''                 AS POSTCODE  
         ,CONVERT(VARCHAR,AD.SchemeId)                   AS PROPERTYID  
         ,MIN(AD.PartsDue)                  AS PartsDue  
         ,MIN(AD.DefectDate)                  AS DefectDate  
         ,SUM(CASE  
          AD.IsHeatersLeft  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isHeatersLeft  
         ,SUM(CASE  
          AD.IsCustomerHaveHeating  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isHeatingAvailable  
         ,SUM(CASE  
          AD.IsDisconnected  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isCapped  
         ,-1                  AS PatchId  
         ,''''                    AS COUNTY  
         ,''''                    AS TOWNCITY  
         ,''NA''             As Telephone  
		 ,  ISNULL(P.SCHEMENAME,'''')	AS Scheme
		, ''''	AS Block	
		,''Scheme'' AS AppointmentType
       '  
	SET @SelectClauseSchemeCount = '  SELECT  ' + @SelectClauseScheme
	SET @SelectClauseScheme = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseScheme

	SET @SelectClauseBlock = '   
          AD.JournalId                   AS JournalId  
         ,'''' AS Address  
         ,P.POSTCODE                    AS POSTCODE  
         ,CONVERT(VARCHAR,AD.BlockId)                   AS PROPERTYID  
         ,MIN(AD.PartsDue)                  AS PartsDue  
         ,MIN(AD.DefectDate)                  AS DefectDate  
         ,SUM(CASE  
          AD.IsHeatersLeft  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isHeatersLeft  
         ,SUM(CASE  
          AD.IsCustomerHaveHeating  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isHeatingAvailable  
         ,SUM(CASE  
          AD.IsDisconnected  
          WHEN 1  
           THEN 1  
          ELSE 0  
         END)                     AS isCapped  
         ,-1                  AS PatchId  
         ,P.COUNTY                    AS COUNTY  
         ,P.TOWNCITY                    AS TOWNCITY  
         ,''NA''           As Telephone  
		 ,  ''''	AS Scheme
		, P.BLOCKNAME + ISNULL('' '' + P.ADDRESS1, '''') + ISNULL('', '' + P.TOWNCITY, '''')	AS Block	
		,''Block'' AS AppointmentType
       '  

	SET @SelectClauseBlockCount = '  SELECT  ' + @SelectClauseBlock
	SET @SelectClauseBlock = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseBlock

	--============================Group Clause==========================================  
	SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY  
				, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY,C_ADDRESS.TEL '  
	SET @GroupClauseScheme = CHAR(10) + ' GROUP BY JournalId, P.SCHEMENAME, AD.SchemeId '  		
	SET @GroupClauseBlock = CHAR(10) + ' GROUP BY JournalId, P.BLOCKNAME, P.ADDRESS1, P.TOWNCITY  
             , P.POSTCODE, AD.BlockId, P.COUNTY '  	
       
     
	--============================Order Clause==========================================    
         
	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder  

	--============================Group Clause==========================================
	SET @unionQuery = CHAR(10) + ' UNION ALL ' + CHAR(10)

	IF(@getOnlyCount=0)  
		BEGIN  
     
		--===============================Main Query ====================================  
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @GroupClause + @unionQuery + 
								@SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @GroupClauseScheme + @unionQuery + 
								@SelectClauseBlock +@fromClauseBlock + @whereClauseBlock + @GroupClauseBlock + @orderClause   
		PRINT @selectClause +@fromClause + @whereClause + @GroupClause + @unionQuery 
		PRINT @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @GroupClauseScheme  + @unionQuery
		PRINT @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock + @GroupClauseBlock  + @orderClause 
		print CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)
		--PRINT @mainSelectQuery  
     
		--=============================== Row Number Query =============================  
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row   
				FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'  
		-- PRINT @rowNumberQuery  
		--============================== Final Query ===================================  
		Set @finalQuery  =' SELECT *  
			FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result   
			WHERE  
			Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)      
		PRINT @mainSelectQuery  
		--============================ Exec Final Query =================================  
        Print 'Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)
		EXEC (@finalQuery)  
	END  
    
  --========================================================================================  
  -- Begin building Count Query   
    
	Declare @selectCount nvarchar(max),   
	@parameterDef NVARCHAR(500)  
    
	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@SelectClauseCount+@fromClause+@whereClause+@GroupClause+@unionQuery+@SelectClauseSchemeCount+@fromClauseScheme+
													@whereClauseScheme+@GroupClauseScheme+@unionQuery+@SelectClauseBlockCount+@fromClauseBlock+@whereClauseBlock+@GroupClauseBlock+') as Records ) '
		
	--print @selectCount  
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;  
      
	-- End building the Count Query  
	--========================================================================================  
  
END  