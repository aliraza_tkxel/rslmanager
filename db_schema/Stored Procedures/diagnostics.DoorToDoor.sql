SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE diagnostics.DoorToDoor
AS 
    SELECT  ct.PropertyId ,
            ct.TenancyId ,
            ct.CustomerId ,
            ( CASE WHEN gt.TITLEID IS NULL THEN ''
                   WHEN gt.TITLEID = 6 THEN ISNULL(c.TITLEOTHER, '')
                   ELSE ISNULL(gt.DESCRIPTION, '')
              END ) AS Title ,
            c.FirstName ,
            c.LastName ,
            ISNULL(a.HOUSENUMBER, '') AS HouseNumber ,
            ISNULL(a.ADDRESS1, '') AS AddressLine1 ,
            ISNULL(a.ADDRESS2, '') AS AddressLine2 ,
            ISNULL(a.ADDRESS3, '') AS AddressLine3 ,
            ISNULL(a.TOWNCITY, '') AS TownCity ,
            ISNULL(a.POSTCODE, '') AS Postcode ,
            ISNULL(a.EMAIL, '') AS Email
    FROM    dbo.vw_PROPERTY_CURRENT_TENANTS_LIST ct
            LEFT JOIN dbo.C_ADDRESS a ON ct.CUSTOMERID = a.CUSTOMERID
                                         AND a.ISDEFAULT = 1
            LEFT JOIN dbo.C__CUSTOMER c ON a.CUSTOMERID = c.CUSTOMERID
            LEFT JOIN dbo.G_TITLE gt ON gt.TITLEID = c.TITLE
GO
