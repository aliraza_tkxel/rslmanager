SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_JOBS_UPDATE]
(
	@TeamId bigint,
	@Description nvarchar(2500),
	@TypeId bigint,
	@OtherType nvarchar(50),
	@Title nvarchar(200),
	@Ref nvarchar(30),
	@LocationId bigint,
	@Duration nvarchar(30),
	@StartDate nvarchar(30),
	@Salary nvarchar(30),
	@ContactName nvarchar(30),
	@ApplicationId int,
	@ClosingDate datetime,
	@JobId bigint
)
AS
SET NOCOUNT OFF;

DECLARE @CLOSINGDATE_5PM DATETIME

-- Ensure closing date is set to 5pm
SET @CLOSINGDATE_5PM = DATEADD(hh,17, DATEADD(dd, 0, DATEDIFF(dd,0,@ClosingDate)))

UPDATE [I_JOB] SET [TeamId] = @TeamId, [Description] = @Description, [TypeId] = @TypeId, [OtherType] = @OtherType, [Title] = @Title, [Ref] = @Ref, [LocationId] = @LocationId, [Duration] = @Duration, [StartDate] = @StartDate, [ClosingDate] = @CLOSINGDATE_5PM, [Salary] = @Salary, [ContactName] = @ContactName, [ApplicationId] = @ApplicationId WHERE ([JobId] = @JobId);
	
SELECT     JobId, TeamId, Description, TypeId, Title, Ref, LocationId, Duration, StartDate, ClosingDate, Salary, ContactName, OtherType
FROM         I_JOB
WHERE     (JobId = @JobId)
GO
