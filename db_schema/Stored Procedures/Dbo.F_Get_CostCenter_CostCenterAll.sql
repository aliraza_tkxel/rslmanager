USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_Get_CostCenter_CostCenterAll]    Script Date: 22/3/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.F_Get_CostCenter_CostCenterAll') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_Get_CostCenter_CostCenterAll AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Saud Ahmed
-- Create date: 22/3/2017
-- Description:	Get all cost center accounts details 
-- WebPage: BHGFinanceModule/Views/CostCenter/Index
-- EXEC F_Get_CostCenterAll '01/04/2016 00:00', '31/03/2017 23:59' 

-- =============================================
ALTER PROCEDURE [dbo].[F_Get_CostCenter_CostCenterAll]				
		@fromDate datetime,
		@toDate datetime
AS
BEGIN
	
	-- Cost Center
	SELECT CC.DESCRIPTION As CostCenterName, CC.COSTCENTREID, 
		ISNULL(COSTCENTRE_BUDGET.BUDGET,0) as BUDGET
		,(ISNULL(Sum(TOPLEVEL.BALANCE), 0) + ISNULL(Sum(dl.BALANCE) ,0) - ISNULL(Sum(cl.BALANCE) ,0)) AS NL
		,isNull(sum(ORDERED_PURCHASES),0) as PURCHASES 
		,ISNULL(ISNULL(COSTCENTRE_BUDGET.BUDGET, 0) 
				- (ISNULL(Sum(TOPLEVEL.BALANCE) ,0) + ISNULL(Sum(dl.BALANCE) ,0)) - ISNULL(Sum(cl.BALANCE) ,0)
				- isNull(Sum(ORDERED_PURCHASES),0),0) AS BAL

	FROM F_COSTCENTRE CC 
		Inner JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID
		Inner JOIN F_HEAD ON CC.COSTCENTREID = F_HEAD.COSTCENTREID
		Inner JOIN F_EXPENDITURE EX on F_HEAD.HEADID = EX.HEADID
		inner join F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID 

		LEFT JOIN (  
				SELECT SUM(CCA.COSTCENTREALLOCATION) as BUDGET, CCA.COSTCENTREID
					FROM F_COSTCENTRE_ALLOCATION CCA
					INNER JOIN F_COSTCENTRE ON CCA.COSTCENTREID = F_COSTCENTRE.COSTCENTREID AND CCA.FISCALYEAR in  (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
				GROUP BY CCA.COSTCENTREID ) 
				COSTCENTRE_BUDGET ON CC.COSTCENTREID = COSTCENTRE_BUDGET.COSTCENTREID

		LEFT JOIN (  
					SELECT	SUM(GROSSCOST) as ORDERED_PURCHASES, F_EXPENDITURE.EXPENDITUREID 
					FROM	F_PURCHASEITEM  
							INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID  
							INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = F_EXPENDITURE.EXPENDITUREID
							INNER JOIN F_HEAD ON F_EXPENDITURE.HEADID = F_HEAD.HEADID
							INNER JOIN F_POSTATUS ON  F_PURCHASEITEM.PISTATUS = F_POSTATUS.POSTATUSID
					WHERE	F_POSTATUS.POSTATUSNAME IN ('Goods Ordered', 'Work Ordered', 'Work Completed', 'Part Completed', 'Goods Received', 'Goods Approved','Invoice Received','Invoice Approved')   
							AND F_PURCHASEITEM.ACTIVE = 1 AND PIDATE between @fromDate AND @toDate
							ANd EXA.ACTIVE =1 and FISCALYEAR in (
								SELECT	YRange 
								FROM	F_FISCALYEARS  
								WHERE	YStart between @fromDate AND @toDate OR
										YEnd between @fromDate AND @toDate OR 
										@fromDate between YStart and YEnd OR 
										@toDate between YStart and YEnd)
					GROUP BY F_EXPENDITURE.EXPENDITUREID ) 
				ORDERED_PURCHASES ON EX.EXPENDITUREID = ORDERED_PURCHASES.EXPENDITUREID 
		LEFT JOIN (
						SELECT	F_EXPENDITURE.EXPENDITUREID ,SUM(GROSSCOST) as Balance
						FROM	F_PURCHASEITEM
								INNER JOIN F_POSTATUS ON  F_PURCHASEITEM.PISTATUS = F_POSTATUS.POSTATUSID
								INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID
						WHERE	F_POSTATUS.POSTATUSNAME IN ('Invoice Paid','Reconciled','Paid By Cheque','Paid By BACS','Paid by Direct Debit','Paid','Paid By Cash','Part Paid')  
								AND F_PURCHASEITEM.ACTIVE = 1 
								AND PIDATE BETWEEN @fromDate AND @toDate
						GROUP BY F_EXPENDITURE.EXPENDITUREID 
				) TOPLEVEL ON TOPLEVEL.EXPENDITUREID = EX.EXPENDITUREID 
		LEFT JOIN (
						select GJDL.ExpenditureId, sum(GJDL.amount) as Balance from NL_JOURNALENTRY nlj
						inner join  NL_JOURNALENTRYDEBITLINE GJDL on nlj.TXNID = GJDL.TXNID 
						inner join NL_TRANSACTIONTYPE TT on nlj.TRANSACTIONTYPE = TT.TRANSACTIONTYPEID
						where GJDL.ExpenditureId is not null
						AND TT.LONGDESCRIPTION = 'General Journal Reversal' OR TT.LONGDESCRIPTION = 'General Journal' 
						ANd nlj.TXNDATE BETWEEN @fromDate AND @toDate
						group by GJDL.ExpenditureId
				) DL ON DL.EXPENDITUREID = EX.EXPENDITUREID 
		LEFT JOIN (
						select GJCL.ExpenditureId, sum(GJCL.amount) as Balance from NL_JOURNALENTRY nlj
						inner join  NL_JOURNALENTRYCREDITLINE GJCL on nlj.TXNID = GJCL.TXNID 
						inner join NL_TRANSACTIONTYPE TT on nlj.TRANSACTIONTYPE = TT.TRANSACTIONTYPEID
						where GJCL.ExpenditureId is not null
						AND TT.LONGDESCRIPTION = 'General Journal Reversal' OR TT.LONGDESCRIPTION = 'General Journal' 
						ANd nlj.TXNDATE BETWEEN @fromDate AND @toDate
						group by GJCL.ExpenditureId
				) CL ON CL.EXPENDITUREID = EX.EXPENDITUREID  
	WHERE CC.COSTCENTREID NOT IN ((SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID')) 
	AND CCA.ACTIVE = 1 and CCA.FISCALYEAR  in  (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
							ANd EXA.ACTIVE =1 and EXA.FISCALYEAR in (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
	group by CC.COSTCENTREID, CC.DESCRIPTION, COSTCENTRE_BUDGET.BUDGET
	ORDER BY CC.DESCRIPTION
	

								
END
