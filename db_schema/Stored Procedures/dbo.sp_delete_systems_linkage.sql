SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[sp_delete_systems_linkage]
@org_id int 
as 

Delete from H_ORG_SYSTEM_LINKAGE where ORG_ID = @org_id


GO
