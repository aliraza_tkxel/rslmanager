SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_PAGE_INSERT]
(
	@MenuID int,
	@WebsiteID int,
	@Title varchar(50),
	@Name varchar(50),
	@FileName varchar(50),
	@PageContent text,
	@Active bit,
	@IsBespoke bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [I_PAGE] ([MenuID], [WebsiteID], [Title], [Name], [FileName], [PageContent], [Active], [IsBespoke]) VALUES (@MenuID, @WebsiteID, @Title, @Name, @FileName, @PageContent, @Active, @IsBespoke);
	
SELECT PageID, MenuID, WebsiteID, Title, Name, FileName, PageContent, Active, IsBespoke FROM I_PAGE WHERE (PageID = SCOPE_IDENTITY())
GO
