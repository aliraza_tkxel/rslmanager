USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.PDR_GetPropertyDetailforEmailToContractor') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyDetailforEmailToContractor AS SET NOCOUNT ON;') 
GO  

ALTER PROCEDURE [dbo].[PDR_GetPropertyDetailforEmailToContractor]  
  
 @journalId INT  
 ,@empolyeeId INT  
 ,@inspectionJournalId INT = NULL    
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  
  
DECLARE @purchaseOrder INT = -1  
  
--=================================================  
--Get Contractor Detail(s)  
--=================================================  
SELECT  
 ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],  
 ISNULL(C.WORKEMAIL, '') AS [Email]  
FROM E__EMPLOYEE E  
INNER JOIN E_CONTACT C  
 ON E.EMPLOYEEID = C.EMPLOYEEID  
  
WHERE E.EMPLOYEEID = @empolyeeId  
  
--=================================================  
--Get Property Detail(s)  
--=================================================  

SELECT  
 PROPERTY.PROPERTYID,  
 HOUSENUMBER,  
 FLATNUMBER,  
 PROPERTY.ADDRESS1,  
 PROPERTY.ADDRESS2,  
 PROPERTY.ADDRESS3,  
 ISNULL(PROPERTY.TOWNCITY,'N/A') AS TOWNCITY,  
 PROPERTY.COUNTY,  
 ISNULL(PROPERTY.POSTCODE,'N/A') AS POSTCODE,  
 ISNULL(NULLIF(ISNULL('Flat No:' + PROPERTY.FLATNUMBER + ', ', '') + ISNULL(PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + PROPERTY.ADDRESS1, '')  
 + ISNULL(' ' + PROPERTY.ADDRESS2, '') + ISNULL(' ' + PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress]  
 ,ISNULL(P_BLOCK.BLOCKNAME,'N/A') AS Block  
 ,ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme  
 ,ISNULL(PROPERTY.HOUSENUMBER,'') + '', '' + ISNULL(PROPERTY.ADDRESS1,'') + '' + ISNULL(PROPERTY.ADDRESS2,'') as Property
 ,ISNULL(BEDROOMS.BEDROOMS,'N/A') AS Bedrooms  
 FROM PDR_JOURNAL  
  INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
  INNER JOIN P__PROPERTY PROPERTY ON PDR_MSAT.schemeproperty = PROPERTY.PROPERTYID
  LEFT JOIN P_SCHEME ON PROPERTY.SchemeId = P_SCHEME.SCHEMEID  
  LEFT JOIN P_BLOCK ON PROPERTY.BlockId =  P_BLOCK.BLOCKID

  LEFT JOIN  
		  (SELECT   
		   PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID,  
		   ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS  
     
		   FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES   
		   LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId  
		   LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID  
		   LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID  
		   WHERE  
			PARAMETER.ParameterName = 'Quantity'   
			AND  ITEM.ItemName = 'Bedrooms'  
		  ) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID  

  WHERE JOURNALID =  @journalId   
  
  
--=================================================  
--Get Purchase Order Details.  
--=================================================  
  
SELECT   
 CONTRACTOR_WORK.PurchaseOrderId AS ORDERID,  
 PURCHASE_ITEM.NETCOST AS NETCOST,  
 PURCHASE_ITEM.VAT AS VAT,  
 CONVERT(VARCHAR(15), T.ReletDate,103) AS RELETDATE,  
 CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,ISNULL(JOURNAL.JOURNALID,-1)),4)) AS OurRef,  
 PURCHASE_ITEM.ITEMDESC AS ItemNotes  
  
   
FROM PDR_CONTRACTOR_WORK CONTRACTOR_WORK   
INNER JOIN F_PURCHASEITEM PURCHASE_ITEM ON CONTRACTOR_WORK.PurchaseOrderId = PURCHASE_ITEM.ORDERID  
INNER JOIN PDR_JOURNAL JOURNAL ON CONTRACTOR_WORK.JournalId = JOURNAL.JOURNALID  
INNER JOIN PDR_MSAT MSAT ON JOURNAL.MSATID = MSAT.MSATId  
INNER JOIN P__PROPERTY P ON MSAT.PropertyId = P.PROPERTYID
Cross Apply(SELECT	Max(j.JOURNALID) as journalID 
			FROM	C_JOURNAL j 
			WHERE	j.PropertyId=P.PropertyID AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,15) 
			GROUP BY PROPERTYID) as CJournal
Cross APPLY (SELECT MAX(TERMINATIONHISTORYID) as terminationhistory 
			FROM  C_TERMINATION 
			WHERE JournalID=CJournal.journalID)as CTermination	 
INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
  
WHERE CONTRACTOR_WORK.JournalId = @journalId  

--=================================================  
--GET FIELD : Supervisor seperated by /  
--=================================================  
  
SELECT   
 ISNULL(EMPLOYEE.FIRSTNAME,'') + ' ' + ISNULL(EMPLOYEE.LASTNAME,'') AS SUPERVISOR  
 FROM   
 PDR_APPOINTMENTS APPOINTMENTS  
INNER JOIN  E__EMPLOYEE EMPLOYEE ON APPOINTMENTS.ASSIGNEDTO = EMPLOYEE.EMPLOYEEID  
  
WHERE JOURNALID = @inspectionJournalId  
  
END  