
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC RD_GetOperativesReserveToday
-- Author:		<Ahmed Mehmood>
-- Create date: <24/7/2013>
-- Description:	<Returns todays appointments of an operative>
-- =============================================
CREATE PROCEDURE [dbo].[RD_GetOperativesReserveToday] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from

	SET NOCOUNT ON;

	SELECT DISTINCT E__EMPLOYEE.EMPLOYEEID EmployeeId , E__EMPLOYEE.FIRSTNAME +' '+ E__EMPLOYEE.LASTNAME  Name	
	FROM	FL_CO_APPOINTMENT 
			INNER JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID
			INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=  FL_FAULT_APPOINTMENT.AppointmentId
			INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId =  FL_FAULT_LOG.FaultLogID   		
    WHERE	CONVERT(VARCHAR(10),FL_CO_APPOINTMENT.AppointmentDate,110) = CONVERT(VARCHAR(10),GETDATE(),110)     		
       		AND StatusID <> 13
END

GO
