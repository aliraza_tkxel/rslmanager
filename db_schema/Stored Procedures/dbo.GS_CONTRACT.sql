-- Stored Procedure

-- =============================================
-- Author:		Umair Naeem
-- Create date: 15 May 2008
-- Description:	This procedure gets the 
--				list of Gas Servicing 
--				contract for a contractor 
-- =============================================
--
-- NOTE: REMEMBER TO ADD IN EXTRA INDEXES AND REBUILD!
-- EXEC [GS_CONTRACT] @ORGID=1270,@PATCHID=26,@DEVELOPMENTID=293
--
ALTER PROCEDURE [dbo].[GS_CONTRACT]
	-- Add the parameters for the stored procedure here
	@ORGID INT,
	@PATCHID INT=NULL,
	@DEVELOPMENTID INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT S.SCOPEID,S.CONTRACTNAME,SA.DESCRIPTION AS AREAOFWORK,SC.COSTTYPE,S.ESTIMATEDVALUE 
		   ,DATEADD(MONTH,-12,S.RENEWALDATE) AS STARTDATE,EP.LOCATION AS PATCH,PD.SCHEMENAME AS SCHEME 
	FROM S_SCOPE S 
             INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID 
             INNER JOIN S_AREAOFWORK SA ON SA.AREAOFWORKID=S.AREAOFWORK 
             INNER JOIN S_COSTTYPE SC ON SC.COSTTYPEID=S.COSTTYPEID 
             INNER JOIN E_PATCH EP ON EP.PATCHID=SS.PATCHID 
             LEFT JOIN P_SCHEME PD ON PD.SCHEMEID=SS.SchemeId 
             LEFT JOIN ( 
		  	                SELECT DISTINCT ITEMACTIONID AS STATUS,CR.SCOPEID 
							FROM C_REPAIR CR 
		           				INNER JOIN P_WOTOREPAIR PWR ON PWR.JOURNALID=CR.JOURNALID 
		               			INNER JOIN P_WORKORDER PW ON PW.WOID=PWR.WOID 
		  	                WHERE PW.GASSERVICINGYESNO=1 
		  						AND CR.REPAIRHISTORYID=(SELECT MAX(REPAIRHISTORYID) 
														FROM C_REPAIR WHERE JOURNALID=CR.JOURNALID) 
		                 ) PROPERTYSTATUS ON PROPERTYSTATUS.SCOPEID=S.SCOPEID 
	WHERE S.ORGID= COALESCE(@ORGID, S.ORGID)
				AND (SS.PATCHID= @PATCHID OR @PATCHID IS NULL)
				AND (SS.SchemeId= @DEVELOPMENTID OR @DEVELOPMENTID IS NULL)
				AND PROPERTYSTATUS.STATUS=2 AND S.SCOPEID IS NOT NULL 

	SET NOCOUNT OFF;
END 



















GO