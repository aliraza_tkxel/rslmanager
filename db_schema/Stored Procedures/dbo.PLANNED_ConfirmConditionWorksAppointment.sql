SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit

--EXEC	@return_value = [dbo].[PLANNED_ConfirmConditionWorksAppointment]
--		@journalId = 2,
--		@userId = 615,
--		@isSaved = @isSaved OUTPUT

--SELECT	@isSaved as N'@isSaved'
-- Author:		Aamir Waheed
-- Create date: 11 Aug 2014
-- Description:	This stored procedure 'll update the Condition Works appointment status from pending to confirm

-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_ConfirmConditionWorksAppointment]	
	@journalId as integer, 	
	@userId as integer,	
	@isSaved as bit output
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
BEGIN TRANSACTION
BEGIN TRY
DECLARE @newStatusHistoryId int
DECLARE @newStatusId int
-- =============================================
--get status id of "Condition Arrnaged"
-- =============================================
SELECT
	@newStatusId = STATUSID
FROM PLANNED_STATUS
WHERE PLANNED_STATUS.TITLE = 'Condition Arranged'
--Print 'Got Status Id'
-- =============================================
-- get status history id of "Arranged"
-- =============================================
SELECT
	@newStatusHistoryId = MAX(StatusHistoryId)
FROM PLANNED_StatusHistory
WHERE StatusId = @newStatusId
--Print 'Got Status History Id'
-- =============================================
-- update appointments status with is pending = 0
-- =============================================
UPDATE PLANNED_APPOINTMENTS
SET ISPENDING = 0
WHERE JournalId = @journalId
--Print 'Appointments Updated'
-- =============================================
--update the planned journal
-- =============================================
UPDATE PLANNED_JOURNAL
SET STATUSID = @newStatusId
WHERE JOURNALID = @journalId
--Print 'Updated Journal/PMO Status'
-- =============================================
--insert into journal history
-- =============================================
INSERT PLANNED_JOURNAL_HISTORY ([JOURNALID]
, [PROPERTYID]
, [COMPONENTID]
, [STATUSID]
, [ACTIONID]
, [CREATIONDATE]
, [CREATEDBY]
, [NOTES]
, [ISLETTERATTACHED]
, [StatusHistoryId]
, [ActionHistoryId]
, [IsDocumentAttached])
	SELECT
		JOURNALID,
		PROPERTYID,
		COMPONENTID,
		@newStatusId,
		NULL,
		GETDATE(),
		@userId,
		NULL,
		0,
		@newStatusHistoryId,
		NULL,
		0
	FROM PLANNED_JOURNAL
	WHERE JOURNALID = @journalId
--Print 'Inserted Entery in Journal History'
-- =============================================
--insert into planned_appointments_History using the following trigger 
--PLANNED_AFTER_UPDATE_PLANNED_APPAOINTMENTS
-- =============================================

END TRY BEGIN CATCH

IF @@TRANCOUNT > 0 BEGIN
ROLLBACK TRANSACTION;
SET @isSaved = 0;
END
DECLARE @ErrorMessage nvarchar(4000);
DECLARE @ErrorSeverity int;
DECLARE @ErrorState int;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
@ErrorSeverity, -- Severity.
@ErrorState -- State.
);
END CATCH

IF @@TRANCOUNT > 0 BEGIN
COMMIT TRANSACTION;
SET @isSaved = 1
END
END
GO
