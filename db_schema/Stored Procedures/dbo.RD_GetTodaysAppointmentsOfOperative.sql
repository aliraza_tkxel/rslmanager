
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
 --EXEC [dbo].[RD_GetTodaysAppointmentsOfOperative] @operativeId ='615'
-- Author:		<Ahmed Mehmood>
-- Create date: <24/7/2013>
-- Description:	<Returns todays appointments of an operative>
-- =============================================
CREATE PROCEDURE [dbo].[RD_GetTodaysAppointmentsOfOperative] 
	-- Add the parameters for the stored procedure here
	(
	@operativeId int
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from

	SET NOCOUNT ON;

	SELECT  FL_FAULT_LOG.JobSheetNumber JSN			
			,ISNULL(P__PROPERTY.HouseNumber,'') + ISNULL(' '+ P__PROPERTY.ADDRESS1,'') + ISNULL(' '+ P__PROPERTY.ADDRESS2,' ') + ISNULL(' '+ P__PROPERTY.ADDRESS3,'') as Address
			,P__PROPERTY.POSTCODE Postcode
			,FL_FAULT_STATUS.Description Status
			,CONVERT(char(5),CAST(FL_CO_APPOINTMENT.Time as datetime),108) StartTime
			,CONVERT(char(5),CAST(FL_CO_APPOINTMENT.EndTime  as datetime),108) EndTime
			,CONVERT(char(5),CAST(FL_CO_APPOINTMENT.Time as datetime),108)+' - '+CONVERT(char(5),CAST(FL_CO_APPOINTMENT.EndTime  as datetime),108) Time,P__PROPERTY.TOWNCITY TownCity,P__PROPERTY.COUNTY County
			,ISNULL(STUFF(
					(
						SELECT
						', ' + FL_FAULT_LOG.JobSheetNumber 
						FROM
						FL_CO_APPOINTMENT 
						INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId 
       					INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID			
						WHERE
							FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID							
							AND FL_FAULT_LOG.StatusID <> 13 
							AND FL_FAULT_STATUS.ACTIVE =1
							AND CONVERT(VARCHAR(10),FL_CO_APPOINTMENT.AppointmentDate,110) = CONVERT(VARCHAR(10),GETDATE(),110)
							AND FL_CO_APPOINTMENT.OperativeID= @operativeId
						FOR XML PATH('')					
					)
					,1,1,''), '') AS JSNGrouped
	
	FROM	FL_CO_APPOINTMENT 
			INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId 
       		INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID 
       		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID 
       		INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
       		
    WHERE	FL_FAULT_LOG.StatusID <> 13 
			AND FL_FAULT_STATUS.ACTIVE =1
			AND CONVERT(VARCHAR(10),FL_CO_APPOINTMENT.AppointmentDate,110) = CONVERT(VARCHAR(10),GETDATE(),110)
			AND FL_CO_APPOINTMENT.OperativeID= @operativeId  
       		 	
END


GO
