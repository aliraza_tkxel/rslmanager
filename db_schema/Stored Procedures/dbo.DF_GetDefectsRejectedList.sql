USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAppliancesDefectsByJournalId]    Script Date: 28-Nov-17 3:02:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.DF_GetDefectsRejectedList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetDefectsRejectedList AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[DF_GetDefectsRejectedList](
	-- Add the parameters for the stored procedure here
		@searchText varchar(200),
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0
		,@totalCount int = 0 output	
)
AS
BEGIN
SET NOCOUNT ON;
DECLARE 
	
		@SelectClause nvarchar(max),
		@SelectClauseCount nvarchar(max),
        @fromClause   nvarchar(max),
        @whereClause  nvarchar(max),	    
		@SelectClauseScheme nvarchar(max),
		@SelectClauseSchemeCount nvarchar(max),
        @fromClauseScheme   nvarchar(max),
        @whereClauseScheme  nvarchar(max),	
		@SelectClauseBlock nvarchar(max),
		@SelectClauseBlockCount nvarchar(max),
        @fromClauseBlock   nvarchar(max),
        @whereClauseBlock  nvarchar(max),	    
        @orderClause  nvarchar(max),	
        @mainSelectQuery nvarchar(max),        
        @rowNumberQuery nvarchar(max),
        @finalQuery nvarchar(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria nvarchar(max),
		@searchCriteriaScheme nvarchar(max),
		@searchCriteriaBlock nvarchar(max),
        @unionQuery nvarchar(max),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType nvarchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		SET @searchCriteria = ' 1 = 1 
								AND (D.detectortypeid IS NULL OR D.detectortypeid = 0)
								AND (((D.ApplianceId IS NOT NULL AND D.ApplianceId <> 0 ) AND APP.ISACTIVE =1) OR (D.ApplianceId IS NULL or D.ApplianceId = 0 ))   
								AND D.DefectDate >= CONVERT( DATETIME, ''7 Jan 2016'', 106 ) 
								AND DC.Description IN (''RIDDOR'', ''Immediately Dangerous'', ''At Risk'', ''Not to Current Standards'',''Other'')
								and D.DefectJobSheetStatus = (SELECT STATUSID FROM PDR_STATUS WHERE TITLE LIKE ''Rejected'')'

		SET @searchCriteriaScheme = @searchCriteria

		SET @searchCriteriaBlock = @searchCriteria

		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ((ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'') '
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND (ISNULL(P.SCHEMENAME, '''') LIKE ''%' + @searchText + '%'')'
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND ((ISNULL(P.BLOCKNAME, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'') '
		END

		SET @fromClause = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AS D
										LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
										LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
										INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
										LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
										LEFT JOIN E__EMPLOYEE AS AB ON AB.EMPLOYEEID =  D.ModifiedBy 
										LEFT JOIN DF_RejectionReasons DFR ON DFR.ReasonId = D.RejectionReasonId
										INNER JOIN P__PROPERTY P ON P.PROPERTYID = d.PropertyId										
									'

		SET @fromClauseScheme = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AS D
										LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
										LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
										INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
										LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
										LEFT JOIN E__EMPLOYEE AS AB ON AB.EMPLOYEEID =  D.ModifiedBy 
										LEFT JOIN DF_RejectionReasons DFR ON DFR.ReasonId = D.RejectionReasonId
										INNER JOIN P_SCHEME P ON P.SCHEMEID = d.SchemeId										
									'

		SET @fromClauseBlock = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AS D
										LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
										LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
										INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
										LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
										LEFT JOIN E__EMPLOYEE AS AB ON AB.EMPLOYEEID =  D.ModifiedBy 
										LEFT JOIN DF_RejectionReasons DFR ON DFR.ReasonId = D.RejectionReasonId
										INNER JOIN P_BLOCK P ON P.BLOCKID = d.BlockId							
									'

		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 	
		SET @whereClauseScheme =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaScheme 	
		SET @whereClauseBlock =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaBlock 	

		--=======================Select Clause=============================================
		SET @SelectClause = '
								D.JournalId									AS JournalId,
							d.PropertyDefectId as DefectId
							,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
							,DC.Description										AS DefectCategory
							,E.EMPLOYEEID										AS EmployeeId
							,E.FIRSTNAME + ISNULL('' ''+E.LASTNAME, '''')			AS EmployeeName
							,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, D.DefectDate,103), '''')  AS RecordedBy
							,P.HOUSENUMBER + ISNULL('' '' + P.ADDRESS1, '''') + ISNULL('', '' + P.TOWNCITY, '''')	AS Address
							,P.POSTCODE AS POSTCODE,
							P.PROPERTYID as PropertyId	
							,P.HouseNumber																AS HouseNumber									
							,P.ADDRESS1																	AS Address1	
							,ISNULL(LEFT(AB.FIRSTNAME,1) + LEFT(AB.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, D.ModifiedDate,103), '''')  AS RejectedByAt		
							,DFR.Description AS RejectionReason, D.RejectionReasonNotes AS RejectionNotes	
							, ''''	AS Scheme
							, ''''	AS Block	
							,''Property'' AS AppointmentType
							,CAST(SUBSTRING(HouseNumber, 1,CASE when patindex(''%[^0-9]%'',HouseNumber) > 0 THEN PATINDEX(''%[^0-9]%'',HouseNumber) - 1 ELSE LEN(HouseNumber) END) AS INT) as SortAddress																										
					'
		SET @SelectClauseCount = '  SELECT  ' + @SelectClause
		SET @SelectClause = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClause

		SET @SelectClauseScheme = '
							D.JournalId									AS JournalId,
									d.PropertyDefectId as DefectId
									,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
									,DC.Description										AS DefectCategory
									,E.EMPLOYEEID										AS EmployeeId
									,E.FIRSTNAME + ISNULL('' ''+E.LASTNAME, '''')			AS EmployeeName
									,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, D.DefectDate,103), '''')  AS RecordedBy
									, '''' 	AS Address
								,'''' AS POSTCODE,
									Convert(varchar,P.SCHEMEID) as PropertyId	
								,	P.SCHEMENAME																AS HouseNumber									
								,P.SCHEMENAME													AS Address1		
									,ISNULL(LEFT(AB.FIRSTNAME,1) + LEFT(AB.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, D.ModifiedDate,103), '''')  AS RejectedByAt		
									,DFR.Description AS RejectionReason, D.RejectionReasonNotes AS RejectionNotes	
									,  ISNULL(P.SCHEMENAME, '''')	AS Scheme
								, ''''	AS Block
								,''Scheme'' AS AppointmentType	
								, CAST(SUBSTRING(P.SCHEMENAME, 1,CASE when patindex(''%[^0-9]%'',P.SCHEMENAME) > 0 THEN PATINDEX(''%[^0-9]%'',P.SCHEMENAME) - 1 ELSE LEN(P.SCHEMENAME) END) AS INT) as SortAddress										
					'

		SET @SelectClauseSchemeCount = '  SELECT  ' + @SelectClauseScheme
		SET @SelectClauseScheme = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseScheme

		SET @SelectClauseBlock = '
								D.JournalId									AS JournalId,
									d.PropertyDefectId as DefectId
									,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
									,DC.Description										AS DefectCategory
									,E.EMPLOYEEID										AS EmployeeId
									,E.FIRSTNAME + ISNULL('' ''+E.LASTNAME, '''')			AS EmployeeName
									,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, D.DefectDate,103), '''')  AS RecordedBy
									,''''	AS Address
								,P.POSTCODE AS POSTCODE,
								CONVERT(VARCHAR,P.BLOCKID) as PropertyId	
									,P.BLOCKNAME																AS HouseNumber									
									,P.ADDRESS1																	AS Address1	
									,ISNULL(LEFT(AB.FIRSTNAME,1) + LEFT(AB.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, D.ModifiedDate,103), '''')  AS RejectedByAt		
									,DFR.Description AS RejectionReason, D.RejectionReasonNotes AS RejectionNotes	
									, ''''	AS Scheme
								, ISNULL(P.BLOCKNAME, '''') + ISNULL('', '' + P.TOWNCITY, '''')	AS Block	
								,''Block'' AS AppointmentType
								, CAST(SUBSTRING(P.BLOCKNAME, 1,CASE when patindex(''%[^0-9]%'',P.BLOCKNAME) > 0 THEN PATINDEX(''%[^0-9]%'',P.BLOCKNAME) - 1 ELSE LEN(P.BLOCKNAME) END) AS INT)  as SortAddress									
					'
		SET @SelectClauseBlockCount = '  SELECT  ' + @SelectClauseBlock
		SET @SelectClauseBlock = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseBlock
		
							--============================Group Clause==========================================
		SET @unionQuery = CHAR(10) + ' UNION ALL ' + CHAR(10)

		IF(@getOnlyCount=0)
		BEGIN
			
			--============================Order Clause==========================================
			
			IF (@sortColumn = 'Address') BEGIN
				SET @sortColumn = CHAR(10) + ' SortAddress '
			END
							
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @unionQuery
									+  @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery 
									+  @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock 
									+ @orderClause 
			--PRINT @mainSelectQuery
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+ CHAR(10) + @sortColumn + CHAR(10) + @sortOrder +CHAR(10) + ') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			-- PRINT @rowNumberQuery
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			PRINT @finalQuery
			--============================ Exec Final Query =================================
						
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(max), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@SelectClauseCount+@fromClause+@whereClause+@unionQuery+@SelectClauseSchemeCount+@fromClauseScheme+
													@whereClauseScheme+@unionQuery+@SelectClauseBlockCount+@fromClauseBlock+@whereClauseBlock+') as Records ) '
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================


END