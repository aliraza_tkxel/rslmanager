
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_GetTenancySupportFormData @referralHistoryId = 1
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,01/30/2014>
-- Description:	<Description,,Get Tenancy Support Form Data>
-- WebPage: .aCustomerDetailspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_GetTenancySupportFormData] 
(
@referralHistoryId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
Declare @journalId int

SELECT	@journalId = C_REFERRAL.JOURNALID 
FROM	C_REFERRAL
WHERE	C_REFERRAL.REFERRALHISTORYID = @referralHistoryId


--===========================================
-- REFERRAL STAGES
--===========================================

SELECT	C_REFERRAL_STAGES.STAGEID as id , C_REFERRAL_STAGES.DESCRIPTION as value 
		,CASE WHEN REFERRAL_STAGE.StageId IS NOT NULL THEN
		1 ELSE
		0 END AS IsChecked
FROM	C_REFERRAL_STAGES
		LEFT JOIN ( SELECT DISTINCT STAGEID AS StageId
					FROM C_REFERRAL
					WHERE C_REFERRAL.REFERRALHISTORYID = @referralHistoryId) AS REFERRAL_STAGE
					ON C_REFERRAL_STAGES.STAGEID = REFERRAL_STAGE.StageId 
				 

--===========================================
-- YES/NO/UNKNOWN
--===========================================
SELECT	G_YESNO_UNKNOWN.YESNOID as id , G_YESNO_UNKNOWN.DESCRIPTION as value
FROM	G_YESNO_UNKNOWN

--===========================================
-- YES/NO
--===========================================
SELECT	G_YESNO.YESNOID as id , G_YESNO.DESCRIPTION as value
FROM	G_YESNO

--===========================================
-- HELP CATEGORIES
--===========================================
SELECT	ROW_NUMBER() OVER (ORDER BY DESCRIPTION) AS Row,CATEGORYID as id ,DESCRIPTION as value 
FROM	C_REFERRAL_HELPCATEGORY 

--===========================================
-- HELP SUBCATEGORIES
--===========================================
SELECT	C_REFERRAL_HELPSUBCATEGORY.CATEGORYID AS CategoryId
		,C_REFERRAL_HELPSUBCATEGORY.SUBCATEGORYID AS SubCategoryId 
		,C_REFERRAL_HELPSUBCATEGORY.description AS Description
		,CASE WHEN C_REFERRAL_CUSTOMER.SubCategoryId IS NOT NULL THEN
		1 ELSE
		0 END AS IsChecked
FROM	C_REFERRAL_HELPSUBCATEGORY
		LEFT JOIN ( SELECT	C_REFERRAL_CUSTOMER_HELPSUBCATEGORY.SUBCATEGORYID AS SubCategoryId 
					FROM	C_REFERRAL_CUSTOMER_HELPSUBCATEGORY
					WHERE	C_REFERRAL_CUSTOMER_HELPSUBCATEGORY.JOURNALID = @journalId ) AS C_REFERRAL_CUSTOMER
					ON  C_REFERRAL_HELPSUBCATEGORY.SUBCATEGORYID = C_REFERRAL_CUSTOMER.SubCategoryId

--===========================================
-- FORM FILLED DATA
--===========================================

SELECT
E__EMPLOYEE.FIRSTNAME +' '+ E__EMPLOYEE.LASTNAME AS Referrer
,CONVERT(VARCHAR(10), C_REFERRAL.LASTACTIONDATE, 103) AS RefDate
,C_JOURNAL.TENANCYID AS TenancyId
,C__CUSTOMER.FIRSTNAME +' '+ C__CUSTOMER.LASTNAME AS CustomerName
,CONVERT(VARCHAR(10), C__CUSTOMER.DOB , 103) AS DateOfBirth
,ISNULL(C_ADDRESS.HOUSENUMBER,'') 
+' '+ ISNULL(C_ADDRESS.ADDRESS1,'')+' '+ ISNULL(C_ADDRESS.ADDRESS2,'')
+' '+ ISNULL(C_ADDRESS.ADDRESS3,'')+', '+ ISNULL(C_ADDRESS.TOWNCITY,'')
+', '+ ISNULL(C_ADDRESS.COUNTY,'')+', '+ ISNULL(C_ADDRESS.POSTCODE,'') AS Address
,ISNULL(C_ADDRESS.TEL,'') AS Telephone
,ISNULL(G_PREFEREDCONTACT.DESCRIPTION,'') AS PreferedContact
,ISNULL(C_REFERRAL.isTypeArrears,0) AS isTypeArrears
,ISNULL(C_REFERRAL.isTypeDamage,0) AS isTypeDamage
,ISNULL(C_REFERRAL.isTypeASB,0) AS isTypeASB
,C_REFERRAL.STAGEID
-- Household
-- ,SUPPORTAGENCIES
,C_REFERRAL.SAFETYISSUES AS SafetyIssues
,C_REFERRAL.CONVICTION AS Conviction
,C_REFERRAL.SUBSTANCEMISUSE AS SubstanceMisuse
,C_REFERRAL.SELFHARM AS SelfHarm
,C_REFERRAL.NOTES AS YesNotes
,C_REFERRAL.NEGLECTNOTES AS NeglectNotes
,C_REFERRAL.OTHERNOTES AS OtherNotes
,C_REFERRAL.MISCNOTES AS MiscNotes
,C_REFERRAL.ISTENANTAWARE AS IsTenantAware
FROM	C_REFERRAL 
		INNER JOIN C_JOURNAL ON C_REFERRAL.JOURNALID = C_JOURNAL.JOURNALID 
		INNER JOIN C_NATURE ON C_JOURNAL.ITEMNATUREID = C_NATURE.ITEMNATUREID 
		INNER JOIN C_STATUS ON C_REFERRAL.ITEMSTATUSID = C_STATUS.ITEMSTATUSID 
		INNER JOIN E__EMPLOYEE ON C_REFERRAL.LASTACTIONUSER = E__EMPLOYEE.EMPLOYEEID 
		INNER JOIN C__CUSTOMER ON C_JOURNAL.CUSTOMERID = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_ADDRESS ON C_JOURNAL.CUSTOMERID = C_ADDRESS.CUSTOMERID 
		LEFT JOIN G_PREFEREDCONTACT ON C__CUSTOMER.PREFEREDCONTACT = G_PREFEREDCONTACT.PREFEREDCONTACTID 
	
WHERE	C_REFERRAL.REFERRALHISTORYID = @referralHistoryId AND C_ADDRESS.ISDEFAULT = 1


--===========================================
-- Additional occupant/ Details of House hold e.g. parents, children etc
--===========================================
-- Note for developer:
-- Implement the select for detail of house hold here and change code accordingly

--===========================================
-- SUPPORT AGENCIES
--===========================================

DECLARE @CustomerIdAgencies INT
		,@SupportAgenciesCustomer NVARCHAR(70)
		
SELECT @CustomerIdAgencies = C.CUSTOMERID, @SupportAgenciesCustomer = C.SUPPORTAGENCIES
FROM C_REFERRAL R
	INNER JOIN C_JOURNAL J ON R.JOURNALID = J.JOURNALID
	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID
WHERE	R.REFERRALHISTORYID = @referralHistoryId

SELECT 
 CASE [DESCRIPTION]
 WHEN 'Other' THEN (SELECT 'Other: ' + ISNULL(SUPPORTAGENCIESOTHER,'') FROM C__CUSTOMER WHERE CUSTOMERID = @CustomerIdAgencies )
 ELSE [DESCRIPTION] END AS SUPPORTAGENCY
FROM G_SUPPORTAGENCIES
WHERE AGID IN (SELECT COLUMN1
				FROM dbo.SPLIT_STRING(@SupportAgenciesCustomer,','))

END
GO
