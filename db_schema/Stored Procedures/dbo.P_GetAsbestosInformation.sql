SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@asbestosCount int
--EXEC	[dbo].[P_GetAsbestosInformation]
--		@propertyId = N'A010060001',
--		@asbestosCount = @asbestosCount OUTPUT
--SELECT	@asbestosCount as N'@asbestosCount'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,, 16 Sep, 2013>
-- Description:	<Description,,This procedure 'll get the asbestos information >
-- =============================================
CREATE PROCEDURE [dbo].[P_GetAsbestosInformation] 
	@propertyId as varchar(20),	
	@asbestosCount as int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	@asbestosCount = count(*)
	FROM P__PROPERTY 
	INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P__PROPERTY.PROPERTYID = P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID
	INNER JOIN P_PROPERTY_ASBESTOS_RISK ON (P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID) = P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID
	INNER JOIN P_ASBESTOS ON P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = (P_ASBESTOS.ASBESTOSID)
	INNER JOIN P_ASBRISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.ASBRISKLEVELID = P_ASBRISKLEVEL.ASBRISKLEVELID
	WHERE P__PROPERTY.propertyId = @propertyId
		
END
GO
