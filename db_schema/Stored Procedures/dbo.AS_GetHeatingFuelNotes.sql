USE [RSLBHALive] 

go 

/****** Object:  StoredProcedure [dbo].[AS_GetHeatingFuelNotes]    Script Date: 18/04/2017 11:43:21 ******/ 
SET ansi_nulls ON 

go 

SET quoted_identifier ON 

go 

IF Object_id('dbo.AS_GetHeatingFuelNotes') IS NULL -- Check if SP Exists  
  EXEC('CREATE PROCEDURE dbo.AS_GetHeatingFuelNotes AS SET nocount ON;') -- Create dummy/empty SP  
go 

ALTER PROCEDURE [dbo].[AS_GetHeatingFuelNotes] 
@schemeId   AS INT, 
@blockId    AS INT
AS 
  BEGIN 
      IF @schemeId IS NOT NULL and @schemeId <> 0
        BEGIN 
            SELECT (SELECT dbo.GetBoilerName(HM.HeatingMappingId,@schemeId,NULL,NULL)) + ': ' +  PA.areaname + ' -> ' + I.itemname As AttributeName,
                   PN.notes	As AttributeNotes			   
            FROM   pa_property_item_notes PN 
                   INNER JOIN pa_item I ON PN.itemid = I.itemid AND I.isactive = 1 
                   INNER JOIN pa_area PA ON PA.areaid = I.areaid 
				   Left Join PA_HeatingMapping HM on HM.HeatingMappingId = PN.HeatingMappingId
            WHERE  PN.schemeid = @schemeId AND PN.ShowInScheduling=1 AND PN.IsActive=1 AND PN.HeatingMappingId IS NOT NULL 

			UNION ALL

			SELECT PA.areaname + ' -> ' + I.itemname As AttributeName,
                   PN.notes	As AttributeNotes			   
            FROM   pa_property_item_notes PN 
                   INNER JOIN pa_item I ON PN.itemid = I.itemid AND I.isactive = 1 
                   INNER JOIN pa_area PA ON PA.areaid = I.areaid 
            WHERE  PN.schemeid = @schemeId AND PN.ShowInScheduling=1 and PN.IsActive=1 AND PN.HeatingMappingId IS NULL

        END 
      IF @blockId IS NOT NULL and @blockId <> 0
        BEGIN 
            SELECT (SELECT dbo.GetBoilerName(HM.HeatingMappingId,NULL,@blockId,NULL)) + ': ' +  PA.areaname + ' -> ' + I.itemname As AttributeName,  
                   PN.notes As AttributeNotes
            FROM   pa_property_item_notes PN 
                   INNER JOIN pa_item I ON PN.itemid = I.itemid AND I.isactive = 1 
                   INNER JOIN pa_area PA ON PA.areaid = I.areaid 
				   Left Join PA_HeatingMapping HM on HM.HeatingMappingId = PN.HeatingMappingId
            WHERE  PN.blockid = @blockId AND PN.ShowInScheduling=1 AND PN.IsActive=1 AND PN.HeatingMappingId IS NOT NULL

			UNION ALL

			SELECT PA.areaname + ' -> ' + I.itemname As AttributeName,  
                   PN.notes As AttributeNotes
            FROM   pa_property_item_notes PN 
                   INNER JOIN pa_item I ON PN.itemid = I.itemid AND I.isactive = 1 
                   INNER JOIN pa_area PA ON PA.areaid = I.areaid 
            WHERE  PN.blockid = @blockId AND PN.ShowInScheduling=1 AND PN.IsActive=1 AND PN.HeatingMappingId IS NULL
        END 
		
  END 

go 