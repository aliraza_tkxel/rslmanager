
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetReviewList]
			@caseOfficerId	int,
			@statusId	int,
			@overdue	bit,
			@skipIndex	int = 0,
			@pageSize	int = 10,
			@sortBy     varchar(100),
			@sortDirection varchar(10)
AS
BEGIN
		
		DECLARE @RegionSuburbClause varchar(8000)
		DECLARE @overdueClause varchar(8000)
		DECLARE @statusClause varchar(8000)
		DECLARE @query varchar(8000)
		DECLARE @subQuery varchar(8000)
		DECLARE @overdueset varchar(50)
		DECLARE @orderbyClause varchar(100)
	
		IF(@caseOfficerId <= 0 )
		BEGIN
			SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
		END 
		ELSE
		BEGIN
			SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOfficerId )+ ' AND IsActive=''true'') 
											OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOfficerId )+ 'AND IsActive=''true''))'
		END
		
		IF(@overdue=0) 
		BEGIN 
			SET @overdueset='false'
			SET @overdueClause= 'dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, ''Days'', AM_Case.ActionReviewDate ) = ''false''' 
		END
		ELSE
		BEGIN 
			SET @overdueset='true'
			SET @overdueClause= 'dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, ''Days'', AM_Case.ActionReviewDate ) = ''true''' 
		END
		
		IF(@statusId<=0) 
		BEGIN 
			SET @statusClause=''
		END
		ELSE
		BEGIN 
			SET @statusClause='AND AM_Case.StatusId=' + convert(varchar(10), @statusId)
		END
		
		IF(@sortBy='ActionReviewDate')
			SET @orderbyClause = 'ORDER BY ' + ' MAX(CONVERT(SMALLDATETIME, ' + @sortBy + ' ,103)) ' + @sortDirection
		ELSE	
			SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
		
		
	    SET @query='SELECT TOP('+convert(varchar(10),@pageSize)+')  
					Max(AM_Case.TenancyId) as TenantId,
					Max(AM_Status.Title) as Status,
				    Max(dbo.AM_FN_Get_Next_Action(AM_Status.StatusId, AM_Action.ActionId)) AS NextAction, 
				    MAx(AM_Action.Title) AS LastAction,
				    Max(isnull(E__EMPLOYEE.FIRSTNAME, '''') + '' '' + isnull(E__EMPLOYEE.LASTNAME, '''')) as Owned, 
				    
					(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
					 FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
					 WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
					ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

					(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
					FROM AM_Customer_Rent_Parameters
						INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
					WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
					ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

					(SELECT Count(DISTINCT CustomerId)
					FROM AM_Customer_Rent_Parameters
					WHERE	TenancyId = AM_Case.TENANCYID) as JointTenancyCount, 				
					Max(convert(varchar(100), AM_Case.ActionRecordeddate, 103)) as ActionRecordedDate, 
					Max(dbo.AM_FN_GetCaseHistoryId(AM_Case.Caseid)) as CaseHistoryid,Max(convert(varchar(50), '''+ @overdueset+ ''' )) as IsOverdue,
					Max(AM_Case.Caseid) as CaseId, 
					Max(customer.CUSTOMERID) as CustomerId, 
					dbo.AM_FN_IS_NEXT_STATUS(MAX(AM_Status.StatusId), MAX(AM_Action.ActionId)) as IsNextStatus, 
					Max(convert(varchar(100), AM_Case.StatusRecordedDate, 103)) as StatusRecordedDate,
					convert(varchar(50),(SELECT  TOP 1 innerAction.RecommendedFollowupPeriod 
										 FROM AM_Action as innerAction
										Where innerAction.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(MAX(AM_Status.StatusId), MAX(AM_Action.ActionId)))) + '';'' + 
					(SELECT TOP 1 AM_LookupCode.CodeName 
					FROM AM_LookupCode 
						INNER JOIN AM_Action as innerAction2 ON AM_LookupCode.LookupCodeId = innerAction2.RecommendedFollowupPeriodFrequencyLookup
					WHERE innerAction2.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(MAX(AM_Status.StatusId), MAX(AM_Action.ActionId))) as nextActionAlert,
					MAX(AM_Action.ActionId) as curentactionId,
					Max(Convert(varchar(100), AM_Case.ActionReviewDate , 103)) as ActionReviewDate
			  		FROM  AM_Action 
			  			INNER JOIN AM_Case ON AM_Action.ActionId = AM_Case.ActionId 
			  			INNER JOIN AM_Status ON AM_Case.StatusId = AM_Status.StatusId 
			  			INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID 
			  			INNER JOIN AM_Resource ON AM_Case.CaseOfficer = AM_Resource.ResourceId 
						INNER JOIN E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID 
			  			INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
			  			INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
			  			LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
						INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
						INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
					 Where 1=1 
					   AND  ' + @RegionSuburbClause + ' 
					   AND AM_Case.IsActive = 1 
					   AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
					   ' + @statusClause + '
					   AND AM_Case.CaseId IN (
											   SELECT AM_CaseHistory.CaseId 		
											   FROM AM_CaseHistory 
													INNER JOIN AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId 
													INNER JOIN AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId 
													INNER JOIN AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId 
													INNER JOIN E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID 
													INNER JOIN C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID 
													INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID 
													INNER JOIN AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId
                                               WHERE AM_CaseHistory.IsActive = 1 
                                               	 and ' + @overdueClause +' 
                                                 and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_SECOND_Last_Case_History_Id(AM_CaseHistory.CaseId)) 
                       
                       AND AM_Case.TenancyId NOT IN ('                          
                                                 
	SET @subQuery=    'SELECT temp.TenantId FROM(
					                      SELECT TOP('+convert(varchar(10),@skipIndex)+')  
					Max(AM_Case.TenancyId) as TenantId,
					Max(AM_Status.Title) as Status,
				    Max(dbo.AM_FN_Get_Next_Action(AM_Status.StatusId, AM_Action.ActionId)) AS NextAction, 
				    MAx(AM_Action.Title) AS LastAction,
				    Max(isnull(E__EMPLOYEE.FIRSTNAME, '''') + '' '' + isnull(E__EMPLOYEE.LASTNAME, '''')) as Owned, 
				    
					(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
					 FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
					 WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
					ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

					(SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
					FROM AM_Customer_Rent_Parameters
						INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
					WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
					ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

					(SELECT Count(DISTINCT CustomerId)
					FROM AM_Customer_Rent_Parameters
					WHERE	TenancyId = AM_Case.TENANCYID) as JointTenancyCount, 				
					Max(convert(varchar(100), AM_Case.ActionRecordeddate, 103)) as ActionRecordedDate, 
					Max(dbo.AM_FN_GetCaseHistoryId(AM_Case.Caseid)) as CaseHistoryid,Max(convert(varchar(50), '''+ @overdueset+ ''' )) as IsOverdue,
					Max(AM_Case.Caseid) as CaseId, 
					Max(customer.CUSTOMERID) as CustomerId, 
					dbo.AM_FN_IS_NEXT_STATUS(MAX(AM_Status.StatusId), MAX(AM_Action.ActionId)) as IsNextStatus, 
					Max(convert(varchar(100), AM_Case.StatusRecordedDate, 103)) as StatusRecordedDate,
					convert(varchar(50),(SELECT  TOP 1 innerAction.RecommendedFollowupPeriod 
										 FROM AM_Action as innerAction
										Where innerAction.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(MAX(AM_Status.StatusId), MAX(AM_Action.ActionId)))) + '';'' + 
					(SELECT TOP 1 AM_LookupCode.CodeName 
					FROM AM_LookupCode 
						INNER JOIN AM_Action as innerAction2 ON AM_LookupCode.LookupCodeId = innerAction2.RecommendedFollowupPeriodFrequencyLookup
					WHERE innerAction2.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(MAX(AM_Status.StatusId), MAX(AM_Action.ActionId))) as nextActionAlert,
					MAX(AM_Action.ActionId) as curentactionId,
					Max(Convert(varchar(100), AM_Case.ActionReviewDate , 103)) as ActionReviewDate
			  		FROM  AM_Action 
			  			INNER JOIN AM_Case ON AM_Action.ActionId = AM_Case.ActionId 
			  			INNER JOIN AM_Status ON AM_Case.StatusId = AM_Status.StatusId 
			  			INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID 
			  			INNER JOIN AM_Resource ON AM_Case.CaseOfficer = AM_Resource.ResourceId 
						INNER JOIN E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID 
			  			INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
			  			INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
			  			LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
						INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
						INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID
					 Where 1=1 
					   AND  ' + @RegionSuburbClause + ' 
					   AND AM_Case.IsActive = 1 
					   AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
					   ' + @statusClause + '
					   AND AM_Case.CaseId IN (
											   SELECT AM_CaseHistory.CaseId 		
											   FROM AM_CaseHistory 
													INNER JOIN AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId 
													INNER JOIN AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId 
													INNER JOIN AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId 
													INNER JOIN E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID 
													INNER JOIN C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID 
													INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID 
													INNER JOIN AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId
                                               WHERE AM_CaseHistory.IsActive = 1 
                                               	 and ' + @overdueClause +' 
                                                 and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_SECOND_Last_Case_History_Id(AM_CaseHistory.CaseId)
                                               ) 
						GROUP BY dbo.AM_Case.TenancyId
						'  + @orderbyClause + ' ) as Temp
					)GROUP BY AM_Case.TenancyId
					 '
		
print(@query + @subQuery + @orderbyClause);
--print(@subQuery);
exec(@query + @subQuery + @orderbyClause);				
--exec(@query);				
END

--exec AM_SP_GetReviewList_OLD -1, -1, 1, 0 , 10
--exec AM_SP_GetReviewList -1, -1, 0, 0, 10,'ActionReviewDate','ASC'













GO
