
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC PLANNED_GetAllSchemes
-- Author:		<Ahmed Mehmood>
-- Create date: <21/10/2013>
-- Description:	<Returns All Schemes>
-- Webpage: ReplacementList.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetAllSchemes] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN


SELECT	P_DEVELOPMENT.DEVELOPMENTID as DevelopmentId, P_DEVELOPMENT.SCHEMENAME as SchemeName				
FROM	P_DEVELOPMENT 
ORDER BY SchemeName ASC

END
GO
