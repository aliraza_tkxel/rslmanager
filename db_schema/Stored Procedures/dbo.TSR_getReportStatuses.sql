
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC TSR_getReportStatuses
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,>
-- Description:	<Description,,get Statuses for Report>
-- WebPage : TenancySupportReferral.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_getReportStatuses]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select ITEMSTATUSID,DESCRIPTION as StatusName from C_STATUS Where ITEMID = 2
	AND DESCRIPTION IN ('Pending','Open','Closed')
END
GO
