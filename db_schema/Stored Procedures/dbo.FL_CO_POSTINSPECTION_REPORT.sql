SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_CO_POSTINSPECTION_REPORT] 

/* ===========================================================================
 '   NAME:           FL_CO_POSTINSPECTION_REPORT
 '   DATE CREATED:   25TH FEB 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist reported fault based on search criteria provided
 '   IN:             @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	   		
	
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLOGID',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
    
   
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
                        CHAR(10) + CHAR(9) + 'Distinct TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID, FL_FAULT_LOG.DueDate, C__CUSTOMER.FIRSTNAME, C__CUSTOMER.LASTNAME, FL_AREA.AreaName, ' +
                        CHAR(10) + CHAR(9) + 'FL_ELEMENT.ElementName, FL_FAULT.Description, FL_FAULT.Recharge,  FL_FAULT_LOG.Quantity, FL_FAULT_PRIORITY.PriorityName, Convert(varchar, FL_FAULT_PRIORITY.ResponseTime) + '' '' + Case  FL_FAULT_PRIORITY.Days When 0 then ''Hour(s)'' when 1 then ''Day(s)'' end AS PriorityTime,' +
						CHAR(10) + CHAR(9) + 'ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '','' +  ISNULL(P__PROPERTY.ADDRESS1,'''') + '','' +  ISNULL(P__PROPERTY.ADDRESS2,'''') + '','' + ISNULL(P__PROPERTY.TOWNCITY,'''') + '','' +    ISNULL(P__PROPERTY.POSTCODE,'''') + '', '' + P__PROPERTY.COUNTY AS COMPLETEADDRESS, ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.Description AS Status, S_ORGANISATION.NAME AS Contractor, C_ADDRESS.ADDRESS1 as Address, FL_FAULT_LOG.JobSheetNumber, Convert(varchar, FL_CO_FAULTLOG_TO_REPAIR.InspectionDate, 103) + '' '' +  FL_CO_FAULTLOG_TO_REPAIR. INSPECTIONTIME as AppointmentDate,' + 
						CHAR(10) + CHAR(9) + 'FL_FAULT_REPAIR_LIST.Description AS RepairDetails, case when FL_CO_FAULTLOG_TO_REPAIR.IsAppointmentSaved IS NULL THEN ''False'' WHEN FL_CO_FAULTLOG_TO_REPAIR.IsAppointmentSaved = 0 THEN ''False'' ELSE ''True'' END AS IsAppointmentSaved, ' + 
						CHAR(10) + CHAR(9) + 'E__EMPLOYEE.FIRSTNAME + '' '' + E__EMPLOYEE.LASTNAME  AS Surveyor, FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_REPAIR_LIST.NetCost, FL_CO_FAULTLOG_TO_REPAIR.Recharge as NewRecharge' 
						
						
                      
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
							CHAR(10) + CHAR(10)+ 'FL_CO_FAULTLOG_TO_REPAIR INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_FAULT_REPAIR_LIST ON FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID = FL_FAULT_REPAIR_LIST.FaultRepairListID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_FAULT_LOG ON FL_CO_FAULTLOG_TO_REPAIR.FaultLogID = FL_FAULT_LOG.FaultLogID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_FAULT  ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID LEFT JOIN ' + 
							CHAR(10) + CHAR(10)+ 'E__EMPLOYEE ON FL_CO_FAULTLOG_TO_REPAIR.USERID = E__EMPLOYEE.EmployeeID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID  INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND ISDEFAULT=1 INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'S_ORGANISATION ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID INNER JOIN ' + 
							CHAR(10) + CHAR(10)+ 'FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID ' 
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.FaultLOGID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) +  @sortOrder+
					' , FL_FAULT_LOG.FaultLOGID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + 'Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE '+
						
						CHAR(10)+  CHAR(10) + ' ( FL_FAULT_LOG.FaultLOGID NOT IN' + 
                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) + ' FL_FAULT_LOG.FaultLOGID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'WHERE 1=1 AND'+ @SearchCriteria + 
                        CHAR(10)+CHAR(9)+' FL_FAULT_REPAIR_LIST.PostInspection = 1 AND'+
                        CHAR(10) + CHAR(9) + '1 = 1 ' +
                        CHAR(10) + CHAR(9) + @OrderClause + ')' +
                        CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        CHAR(10)+CHAR(9)+' FL_FAULT_REPAIR_LIST.PostInspection = 1 AND'+
                        CHAR(10) + CHAR(9)  + ' 1=1'+
                        CHAR(10) + CHAR(9)  + ')'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause )--+ @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)


GO
