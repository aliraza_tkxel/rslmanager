
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROC [dbo].[E__EMPLOYEE_GET_BY_TEAMID_PAGEID]
    @TEAMID INT ,
    @PAGEID INT
AS 
    WITH    CTE_PAGE_TEAM ( PageId, TeamId, EmployeeId )
              AS ( SELECT DISTINCT
                            TJ.PageId ,
                            jrt.TeamId ,
                            e.EMPLOYEEID
                   FROM     DBO.I_PAGE_TEAMJOBROLE tj
                            INNER JOIN dbo.E_JOBROLETEAM jrt ON tj.TeamJobRoleId = jrt.JobRoleTeamId
                            INNER JOIN dbo.E__EMPLOYEE e ON e.JobRoleTeamId = jrt.JobRoleTeamId
                   WHERE    jrt.isDeleted = 0
                            AND jrt.IsActive = 1
                 )
        SELECT  E.FIRSTNAME ,
                E.LASTNAME ,
                E.EMPLOYEEID ,
                E.FIRSTNAME + SPACE(1) + E.LASTNAME AS FULLNAME ,
                ( SELECT    COUNT(*)
                  FROM      CTE_PAGE_TEAM PE
                  WHERE     PE.EMPLOYEEID = E.EMPLOYEEID
                            AND PE.PAGEID = @PAGEID
                ) AS HASPERMISSION
        FROM    E__EMPLOYEE E
                INNER JOIN E_JOBDETAILS ET ON E.EMPLOYEEID = ET.EMPLOYEEID
        WHERE   ET.TEAM = @TEAMID
                AND ET.ACTIVE = 1




GO
