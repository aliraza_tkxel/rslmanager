USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
/*
EXEC	[dbo].[FL_GetSubcontractorList]
		@schemeId= 0,
		@blockId = 0,
		@financialYear =2015,
		@statusId = 20,
		@searchedText = '',
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = 'FaultLogID',
		@sortOrder = 'DESC'		
*/
-- Author:		<Ahmed Mehmood>
-- Create date: <14/3/2013>
-- Description:	<Get list of Subcontractors Works>
-- Web Page: ReportsArea.aspx
-- =============================================
IF OBJECT_ID('dbo.FL_GetSubcontractorList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetSubcontractorList AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[FL_GetSubcontractorList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@financialYear INT,
		@statusId int = -1,		
		@searchedText varchar(8000) = '',
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Reported',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @statusQueryClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),

    --variables for paging
    @offset int,
	@limit int

	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1

	--========================================================================================
	-- Begin building SearchCriteria clause
	-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteria = ' 1=1'


	IF(@searchedText <> '' AND @searchedText IS NOT NULL)
	BEGIN		
		
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  
														  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+''')) 
														  OR  FL_FAULT_LOG.JobSheetNumber LIKE ''%'+@searchedText +'%'''
	END 	
	
	
	-- Filter on the basis of BlockId and SchemeId
	IF(@schemeId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.SchemeId IS NULL 
																	THEN	P__PROPERTY.SchemeId 
																	ELSE 		
																			FL_FAULT_LOG.SchemeId 	
																	END = '+convert(varchar(10),@schemeId)
		END
	IF(@blockId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.BlockId IS NULL 
																	THEN P__PROPERTY.BLOCKID 
																	ELSE 		
																		FL_FAULT_LOG.BlockId 	
																	END = '+convert(varchar(10),@blockId)
		END
	IF (@financialYear != -1 AND LEN(@financialYear) = 4)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND FL_FAULT_LOG.SubmitDate BETWEEN ''' + CONVERT(VARCHAR,@financialYear) + '0401'' AND ''' + CONVERT(VARCHAR,@financialYear+1) + '0331 23:59:59.997'''
		END
	
	IF(@statusId > 0)
		BEGIN
			SET @statusQueryClause = CHAR(10) +' SELECT FL_FAULT_LOG.FaultLogID faultLogID
												 FROM FL_FAULT_LOG
												 WHERE FL_FAULT_LOG.StatusID='+convert(varchar(10),@statusId)
		END
	Else if (@statusId < 0)
		BEGIN
			SET @statusQueryClause = CHAR(10) +' SELECT FL_FAULT_LOG.FaultLogID faultLogID
												 FROM FL_FAULT_LOG
												 WHERE FL_FAULT_LOG.StatusID=2'
		END						

	-- End building SearchCriteria clause   
	--========================================================================================

	SET NOCOUNT ON;
	--========================================================================================	        
	-- Begin building SELECT clause
	-- Insert statements for procedure here

	SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') FL_FAULT_LOG.FaultLogID AS FaultLogID, FL_FAULT_LOG.JobSheetNumber as JSN,
	ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
	CONVERT(nvarchar(17),FL_FAULT_LOG.SubmitDate, 103) as Reported, FL_FAULT_LOG.SubmitDate AS ReportedSort,case 
		WHEN LEN(P__PROPERTY.PROPERTYID) > 0 THEN
		ISNULL(P__PROPERTY.HOUSENUMBER,'''') +'' ''+ ISNULL(''''  + P__PROPERTY.ADDRESS1,'''') 
		ELSE 
		ISNULL(P_BLOCK.ADDRESS1,'''')+'' ''+ISNULL('' ''+P_BLOCK.ADDRESS2,'''') 
		END as Address,
	FL_AREA.AreaName Location , FL_FAULT.Description Description ,S_ORGANISATION.NAME Subcontractor ,
	CONVERT(nvarchar(17),FL_FAULT_LOG.SubmitDate, 103) Assigned,FL_FAULT_LOG.SubmitDate AS AssignedSort,
	ISNULL(NULLIF(ISNULL(E__EMPLOYEE.Firstname, '''') + ISNULL('' '' + E__EMPLOYEE.LASTNAME, ''''),''''),''N/A'') as UserInitials,
	CONVERT(nvarchar(17),FL_FAULT_LOG.DueDate, 103)  CompletionDue, FL_FAULT_LOG.DueDate AS CompletionDueSort,
	FL_FAULT_STATUS.Description Status, 
	Case When FL_FAULT_LOG.PROPERTYID IS NULL THEN ''NA'' ELSE FL_FAULT_LOG.PROPERTYID END AS PROPERTYID
	'
	-- End building SELECT clause
	--======================================================================================== 							


	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause =	  CHAR(10) +'FROM FL_FAULT_LOG
	INNER JOIN ('+ CHAR(10)+ @statusQueryClause + CHAR(10) +') SubcontractorsFaults on FL_FAULT_LOG.FaultLogID=SubcontractorsFaults.faultLogID
	LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
	INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
	LEFT JOIN FL_AREA on FL_FAULT_LOG.AREAID = FL_AREA.AreaID
	INNER JOIN S_ORGANISATION on FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID
	LEFT OUTER JOIN E__EMPLOYEE on FL_FAULT_LOG.UserID = E__EMPLOYEE.EMPLOYEEID
	INNER JOIN FL_FAULT_STATUS on  FL_FAULT_LOG.StatusID=FL_FAULT_STATUS.FaultStatusID
	LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
	Case WHEN FL_FAULT_LOG.SchemeId is NULL THEN P__PROPERTY.SchemeId 
		ELSE 		FL_FAULT_LOG.SchemeId 	END
	LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID	=
	Case WHEN FL_FAULT_LOG.BlockId is NULL THEN P__PROPERTY.BLOCKID 
		ELSE 		FL_FAULT_LOG.BlockId 	END	 '
	-- End building From clause
	--======================================================================================== 														  



	--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Address' 		
	END

	IF(@sortColumn = 'Reported')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'ReportedSort' 		
	END		
	IF(@sortColumn = 'Scheme')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Scheme' 		
	END	

	IF(@sortColumn = 'Block')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Block' 		
	END

	IF(@sortColumn = 'CompletionDue')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'CompletionDue' 		
	END	
	IF(@sortColumn = 'JSN')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'JSN' 		
	END	

	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

	-- End building OrderBy clause
	--========================================================================================								

	--========================================================================================
	-- Begin building WHERE clause

	-- This Where clause contains subquery to exclude already displayed records			  

	SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

	-- End building WHERE clause
	--========================================================================================

	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

	-- End building the main select Query
	--========================================================================================																																			

	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	--print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	----========================================================================================
	---- Begin building Count Query 

	Declare @selectCount nvarchar(2000), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT @totalCount =  count(FL_FAULT_LOG.FaultLogID) ' + @fromClause + @whereClause 

	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

	---- End building the Count Query
	----========================================================================================	

END


