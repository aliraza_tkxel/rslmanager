SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_JOB_SHORTLIST_SELECTBYEMPID]
(@empId int)
AS
SET NOCOUNT ON;

SELECT     J.JobId, J.TeamId, J.Description, J.TypeId, TY.JobType, J.Title, J.Ref, J.LocationId, J.Duration, J.StartDate, J.DatePosted, J.ClosingDate, J.Expired, J.Salary, J.ContactName, J.Active, J.ApplicationId, OFC.DESCRIPTION as Location
FROM         I_JOB_SHORTLIST AS S INNER JOIN
                      I_JOBS_CURRENT AS J ON J.JobId = S.JobId INNER JOIN
                      I_JOB_TYPE AS TY ON J.TypeId = TY.JobTypeId INNER JOIN
                      G_OFFICE AS OFC ON J.LocationId = OFC.OFFICEID INNER JOIN
                      E_TEAM AS TM ON J.TeamId = TM.TEAMID
WHERE      S.EmpId = @empId 
--AND J.EXPIRED = 0
GO
