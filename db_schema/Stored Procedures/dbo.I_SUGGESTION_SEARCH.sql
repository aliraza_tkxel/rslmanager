SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[I_SUGGESTION_SEARCH]
@TOPIC VARCHAR(60) = NULL,
@DATETO DATETIME = NULL,
@DATEFROM DATETIME = NULL,  
@TEAMID INT = NULL  
AS
DECLARE @SQL NVARCHAR(4000), @PARAMLIST  NVARCHAR(4000)
                     
SET NOCOUNT ON 
                                                              
SELECT @SQL =	'SELECT E.FIRSTNAME + SPACE(1) + E.LASTNAME AS FULLNAME, T.TEAMNAME,
						S.SUGGESTIONID, S.USERID, S.DATESUBMITTED, S.TOPIC, S.SUGGESTION, S.ACTION
				FROM I_SUGGESTIONS S
					INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = S.USERID
					INNER JOIN E_JOBDETAILS EJ ON E.EMPLOYEEID = EJ.EMPLOYEEID
					INNER JOIN E_TEAM T ON  EJ.TEAM = T.TEAMID
				WHERE  1 = 1 '  
        
IF @TOPIC IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND S.TOPIC LIKE ''%' +  @TOPIC + '%'''             

IF @DATEFROM IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND S.DATESUBMITTED >= @DATEFROM '                                                                     
        
                                                                   
IF @DATETO IS NOT NULL                                             
   SELECT @SQL = @SQL + ' AND S.DATESUBMITTED <= @DATETO '              

IF @TEAMID IS NOT NULL
   SELECT @SQL = @SQL + ' AND T.TEAMID = @TEAMID '              

SELECT @PARAMLIST =	'@TOPIC VARCHAR(60),@DATETO DATETIME,
					@DATEFROM DATETIME,@TEAMID INT'
SET NOCOUNT OFF 
EXEC SP_EXECUTESQL @SQL , @PARAMLIST,
@TOPIC,                               
@DATETO,
@DATEFROM,
@TEAMID
GO
