USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetMake]    Script Date: 18-Jan-17 2:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetMake
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,02/11/2012>
-- Description:	<Description,,Get all make types>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
IF OBJECT_ID('dbo.[AS_GetMake]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetMake] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetMake]
(@prefix varchar(100) )  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.MANUFACTURERID as id, a.MANUFACTURER as title
	FROM GS_MANUFACTURER a
	INNER JOIN
  (SELECT MANUFACTURER, MIN(MANUFACTURERID) as Id 
   FROM GS_MANUFACTURER
   GROUP BY MANUFACTURER) AS b
   ON a.MANUFACTURER=b.MANUFACTURER
   AND a.MANUFACTURERID=b.Id
   Where a.MANUFACTURER like'%'+@prefix+'%'  
END
GO
