SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_GET_CUSTOMERID]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GET_CUSTOMERID
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Zahid Ali
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get customer ID against given equirylogid  
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enquiryLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.CustomerID 
            
            
	FROM
	       TO_ENQUIRY_LOG 
	       
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enquiryLogID)




GO
