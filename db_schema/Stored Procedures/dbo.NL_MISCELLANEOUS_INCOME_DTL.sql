SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROC [dbo].[NL_MISCELLANEOUS_INCOME_DTL]
	(
	 @HDRID AS INT,
	 @NLACCOUNT AS INT,
	 @NETAMOUNT AS MONEY,
	 @DETAILS AS VARCHAR(250),
  	 @TXNID AS INT,
	 @DATE AS DATETIME)

AS 
	BEGIN
	BEGIN TRAN
		
		DECLARE @DT SMALLDATETIME
		DECLARE @GROSS AS MONEY
	
		SET @GROSS=@NETAMOUNT
		SET @DT=CONVERT(SMALLDATETIME,@DATE,103)
	
		EXEC INSERT_NL_MISCELLANEOUS_DTL @HDRID, @NLACCOUNT, @NETAMOUNT, @DETAILS
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN @@ERROR
		END

		EXEC F_INSERT_NL_JOURNALENTRYCREDITLINE @TXNID, @NLACCOUNT, NULL, NULL, @DT, @GROSS, @DETAILS, NULL

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN @@ERROR
		END
	COMMIT		
	RETURN
	END
GO
