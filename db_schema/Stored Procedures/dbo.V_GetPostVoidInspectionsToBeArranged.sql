USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetFirstInspectionsToBeArranged]    Script Date: 5/25/2015 12:14:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get Post Void Inspections To Be Arranged List 
    Author: Ali Raza
    Creation Date: June-18-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========			===========================
    v1.0        June-18-2015      Ali Raza			Get Post Void Inspections To Be Arranged List 

  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPostVoidInspectionsToBeArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
IF OBJECT_ID('dbo.[V_GetPostVoidInspectionsToBeArranged]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_GetPostVoidInspectionsToBeArranged] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[V_GetPostVoidInspectionsToBeArranged]
-- Add the parameters for the stored procedure here
		@patch int = 0,
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        @filterCriteria varchar(200)='',
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int,
		@propertyStatus int,
		@propertySubStatus int,
		@propertyStatusForUnavailable int,
		@propertyStatusForAvailableToRent int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Post Void Inspection'
		SELECT  @ToBeArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To be Arranged'
		SELECT @propertyStatus=STATUSID from P_STATUS WHERE DESCRIPTION='Let'
		SELECT @propertySubStatus=SUBSTATUSID from P_SUBSTATUS WHERE DESCRIPTION='Pending Termination'

		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' ((P_STATUS.DESCRIPTION =''Let'' AND P_SUBSTATUS.DESCRIPTION=''Pending Termination'') 
		OR (P_STATUS.DESCRIPTION =''Available to rent'')
		OR (P_STATUS.DESCRIPTION =''Unavailable'' AND P_SUBSTATUS.DESCRIPTION=''Under Major Repair''))   '	

		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)+''
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END
		
		--=================================	Filter Criteria================================
		IF(@patch > 0 )
		BEGIN
		SET @filterCriteria = @filterCriteria + 'AND PDR_DEVELOPMENT.PATCHID = ' + CONVERT (VARCHAR,@patch) 
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,	P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.FIRSTNAME,''''))+'' ''+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),T.TerminationDate, 103) as Termination	
		,Case When T.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,T.TerminationDate), 103) Else	CONVERT(nvarchar(50),T.ReletDate, 103) END as Relet		
		,P__PROPERTY.PropertyId as PropertyId
		,C__CUSTOMER.CUSTOMERID  as CustomerId
		,C_TENANCY.TENANCYID as TenancyId
		,T.TerminationDate as TerminationDate
		,Case When T.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,T.TerminationDate), 103) Else	CONVERT(nvarchar(50),T.ReletDate, 103) END as ReletDate
		,P_STATUS.DESCRIPTION as PropertyStatus
		,P_SUBSTATUS.DESCRIPTION as PropertySubstatus
		'
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		INNER JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
		Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P__PROPERTY.PropertyID
		AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
		GROUP BY PROPERTYID) as CJournal
		Cross APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
		INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
		INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DevelopmentId = PDR_DEVELOPMENT.DevelopmentID
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		LEFT JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
		LEFT JOIN P_SUBSTATUS ON  P__PROPERTY.SUBSTATUS = P_SUBSTATUS.SUBSTATUSID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TerminationDate' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ReletDate' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
