USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetArrangedAppointmentsDetailByPmo]    Script Date: 10/10/2016 11:06:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[PLANNED_GetCancelledAppointmentsDetailByPmo]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetCancelledAppointmentsDetailByPmo] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetCancelledAppointmentsDetailByPmo]  
 @pmo int  
AS  
BEGIN

 DECLARE @propertyId varchar(12)
 DECLARE @schemeId int
 DECLARE @blockId int   
  
 SELECT ROW_NUMBER() OVER (ORDER BY PLANNED_APPOINTMENTS.APPOINTMENTID) AS Row  
   ,PLANNED_APPOINTMENTS.JournalId  AS PMO  
   ,CASE isMiscAppointment WHEN 1  
    THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + '(misc)', 'Miscellaneous')  
    ELSE ISNULL(PLANNED_COMPONENT.COMPONENTNAME, 'N/A') END AS Component  
   ,ISNULL(PLANNED_COMPONENT.COMPONENTID,-1) AS ComponentId  
   ,ISNULL(PLANNED_COMPONENT_TRADE.COMPTRADEID, -1) AS ComponentTradeId  
   ,ISNULL(CASE   
   WHEN PLANNED_APPOINTMENTS.isMiscAppointment = 1 THEN  
    MISC_TRADE.Description   
   ELSE   
    COMP_TRADE.Description   
   END,'N/A') AS Trade  
   ,CASE   
   WHEN PLANNED_APPOINTMENTS.isMiscAppointment = 1 THEN  
    MISC_TRADE.TradeId    
   ELSE   
    COMP_TRADE.TradeId    
   END AS TradeId     
   ,RIGHT('0000'+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),4) AS JSN  
   ,'JSN'+RIGHT('0000'+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),4) AS JSNSearch  
   ,PLANNED_STATUS.TITLE AS Status  
   ,E__EMPLOYEE.FIRSTNAME+' '+ E__EMPLOYEE.LASTNAME as Operative  
   ,LEFT(E__EMPLOYEE.FIRSTNAME, 1)+' '+ E__EMPLOYEE.LASTNAME as OperativeShortName  
   ,CONVERT(char(5),cast(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME as datetime),108) AS StartTime  
   ,CONVERT(char(5),cast(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME  as datetime),108) AS EndTime  
   ,CASE   
   WHEN PLANNED_APPOINTMENTS.isMiscAppointment = 1 THEN  
    ISNULL(NULLIF(PLANNED_MISC_TRADE.DURATION,0),  
    DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME), CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME)))  
   ELSE   
    ISNULL(NULLIF(PLANNED_APPOINTMENTS.DURATION ,0),  
    DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME), CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME)))  
   
   END AS Duration  
     
   ,CASE   
   WHEN PLANNED_APPOINTMENTS.isMiscAppointment = 1 THEN  
    DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME), CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME))  
   ELSE   
    (SELECT SUM(COALESCE(PLANNED_APPOINTMENTS.DURATION,DATEDIFF(HOUR, CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS TIME), CAST(PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS TIME)),0))  
    FROM PLANNED_APPOINTMENTS  
    WHERE PLANNED_APPOINTMENTS.JournalId = @pmo )  
   END AS TotalDuration  
      
   ,CONVERT(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103) AS StartDate  
   ,CONVERT(varchar(10),PLANNED_APPOINTMENTS.APPOINTMENTENDDATE, 103) AS EndDate  
     
   ,PLANNED_APPOINTMENTS.CUSTOMERNOTES AS CustomerNotes  
   ,PLANNED_APPOINTMENTS.APPOINTMENTNOTES AS JobsheetNotes  
   ,PLANNED_APPOINTMENTS.isMiscAppointment AS IsMiscAppointment  
   ,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  PLANNED_JOURNAL.PROPERTYID
	when PLANNED_JOURNAL.BlockId > 0 THEN  convert(nvarchar(max),PLANNED_JOURNAL.BlockId)
	when PLANNED_JOURNAL.SchemeId > 0 THEN convert(nvarchar(max),PLANNED_JOURNAL.SchemeId)
	end as PropertyId 
   ,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  'Property'
	when PLANNED_JOURNAL.BlockId > 0 THEN  'Block'
	when PLANNED_JOURNAL.SchemeId > 0 THEN 'Scheme'
	end as Type  
   ,PLANNED_APPOINTMENTS.APPOINTMENTID AS AppointmentId  
   ,PLANNED_APPOINTMENTS.APPOINTMENTSTATUS as InterimStatus  
   ,PLANNED_APPOINTMENTS.ISPENDING as IsPending  
     
   -- AppointmentDateFormat1 => Tuesday 26 Nov 2013
   ,SUBSTRING(DATENAME(weekday, PLANNED_APPOINTMENTS.APPOINTMENTDATE),1,3)+' '+ CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 106) AppointmentDateFormat1  
   ,E__EMPLOYEE.EMPLOYEEID OperativeId
  
 FROM PLANNED_APPOINTMENTS  
   INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID   
   LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID   
   INNER JOIN PLANNED_STATUS ON  PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID   
     
   LEFT JOIN PLANNED_COMPONENT_TRADE ON PLANNED_APPOINTMENTS.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID   
   LEFT JOIN G_TRADE AS COMP_TRADE ON PLANNED_COMPONENT_TRADE.TRADEID = COMP_TRADE.TradeId   
     
   LEFT JOIN PLANNED_MISC_TRADE ON PLANNED_APPOINTMENTS.APPOINTMENTID = PLANNED_MISC_TRADE.AppointmentId   
   LEFT JOIN G_TRADE AS MISC_TRADE ON PLANNED_MISC_TRADE.TRADEID = MISC_TRADE.TradeId  
     
   INNER JOIN E__EMPLOYEE ON  PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
      
 WHERE PLANNED_APPOINTMENTS.JournalId = @pmo  
		AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS ='Cancelled'
 --AND APPOINTMENTSTATUS = 'Cancelled'
 Order BY PLANNED_COMPONENT_TRADE.sorder Asc

 SELECT @propertyId=PLANNED_JOURNAL.PROPERTYID, @schemeId=PLANNED_JOURNAL.SchemeId, @blockId=PLANNED_JOURNAL.BlockId
 FROM PLANNED_JOURNAL
 WHERE JOURNALID= @pmo
   
 if(@propertyId is not null)
 BEGIN
	 EXEC PLANNED_GetPropertyDetail @propertyId  
 END  
IF(@blockId > 0)
BEGIN
	EXEC PLANNED_GetBlockDetail @blockId  
END
ELSE IF(@schemeId > 0) 
BEGIN
	EXEC PLANNED_GetSchemeDetail @schemeId  
END 
   
END  
