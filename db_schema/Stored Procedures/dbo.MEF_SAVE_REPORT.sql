SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Umair Naeem
-- Create date: 07 July 2010
-- Description:	This funtion insert the 
--				values in MEF_Report table
-- =============================================
--
-- EXEC [MEF_SAVE_REPORT] @EMPLOYEEID=100,@YRANGE=10,@MRANGE=2,@UPLOADDATE='20100601'
--
CREATE PROCEDURE [dbo].[MEF_SAVE_REPORT]
	-- Add the parameters for the stored procedure here
	@EMPLOYEEID INT=NULL,
	@YRANGE INT=NULL,
	@MRANGE INT=NULL,
	@MEFREPORT_NAME NVARCHAR(200)=NULL,
	@MEREPORT_XLS  VARBINARY(MAX)=NULL,
	@UPLOADDATE SMALLDATETIME=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @MEFREPORTID INT 

	INSERT INTO dbo.MEF_REPORT(EMPLOYEEID,YRANGE,MRANGE,MEFREPORT_NAME,MEREPORT_XLS,UPLOADDATE)
	VALUES  ( @EMPLOYEEID , @YRANGE ,@MRANGE ,@MEFREPORT_NAME,@MEREPORT_XLS ,@UPLOADDATE)
	
	SELECT @@IDENTITY AS MEFREPORTID
	
	SET NOCOUNT OFF;
END 

























GO
