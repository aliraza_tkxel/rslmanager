USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_getSchemeBlockDefectImages') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_getSchemeBlockDefectImages AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_getSchemeBlockDefectImages](  
@requestType Varchar(200),
@id int
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 --SELECT *  
 --FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES  
 --WHERE PropertyDefectId = @propertyDefectId 
	SELECT PPADM.PropertyDefectImageId, PPADM.PropertyDefectId, PPADM.ImageTitle, PPADM.ImagePath, PPADM.ImageIdentifier, PPADM.isBefore FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES PPADM 
	INNER Join P_PROPERTY_APPLIANCE_DEFECTS On PPADM.PropertyDefectId =P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId
	WHERE  P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId = @id

END  