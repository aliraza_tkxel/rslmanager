USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_ChangeDefectStatus]    Script Date: 30-Nov-17 3:02:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.DF_ChangeDefectStatus') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_ChangeDefectStatus AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[DF_ChangeDefectStatus](
	-- Add the parameters for the stored procedure here
		@defectId int,
		@userId int,
		@rejectionReasonId int,
		@rejectionReasonNotes nvarchar(max),
		@status varchar(255)
)
AS
BEGIN
SET NOCOUNT ON;
DECLARE 
	
	@StatusId int

	SELECT @StatusId=STATUSID FROM PDR_STATUS WHERE TITLE LIKE @status

	if(@defectId is not null)
	BEGIN
		if(@rejectionReasonId <> 0)
		BEGIN
			UPDATE P_PROPERTY_APPLIANCE_DEFECTS SET DefectJobSheetStatus=@StatusId, ModifiedBy=@userId, ModifiedDate=GETDATE(), RejectionReasonId=@rejectionReasonId
													,RejectionReasonNotes=@rejectionReasonNotes
											WHERE PropertyDefectId=@defectId
		END
		ELSE
		BEGIN
			UPDATE P_PROPERTY_APPLIANCE_DEFECTS SET DefectJobSheetStatus=@StatusId, ModifiedBy=@userId, ModifiedDate=GETDATE()
											WHERE PropertyDefectId=@defectId
		END
	END

END