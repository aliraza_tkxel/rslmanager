SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
















CREATE PROCEDURE dbo.FL_IS_FAULT_DISAPPROVED_BY_CONTRACTOR
/* ===========================================================================
 '   NAME:           FL_IS_FAULT_APPROVED_BY_CONTRACTOR
 '   DATE CREATED:   9 Mar, 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve fault repair list
 
 '   IN:             @Description
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
	@FaultLogId INT,
	@IsDisApproved INT output
)
AS
Declare @IsApproved int
Declare @OrgId int

	SELECT @IsApproved = Approved FROM FL_FAULT_PREINSPECTIONINFO 
		
	WHERE FaultLogId = @FaultLogId
	
	--SELECT @OrgId = @ORGID FROM FL_FAULT_LOG
		
	--WHERE FaultLogId = @FaultLogId

IF @IsApproved = 1 --If Approved
BEGIN
	SET @IsDisApproved = 0
END
ELSE IF @IsApproved = 0 --If not Approved
BEGIN
	SET @IsDisApproved = 1
END
--ELSE IF @IsApproved IS NULL AND @OrgId IS NOT NULL /*If Pre-Inspection is false*/
--BEGIN 
--	SET @IsDisApproved = 0
--END
--ELSE IF @IsApproved IS NULL AND @OrgId IS NULL /*If Pre-Inspection is false*/
--BEGIN 
--	SET @IsDisApproved = 0
---END	
return @IsDisApproved

GO
