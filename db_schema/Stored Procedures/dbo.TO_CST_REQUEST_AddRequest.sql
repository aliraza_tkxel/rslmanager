SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.TO_CST_REQUEST_AddRequest
/* ===========================================================================
 '   NAME:           TOs_CST_REQUEST_AddRequest
 '   DATE CREATED:   30 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add CST Request record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 termination record in TO_CST_REQUEST
 '   IN:             @RequestNatureID
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '
 '   OUT:            @CstRequestId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(4000),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	@RequestNatureID INT,
	@CstRequestId INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_CST_REQUEST(
		RequestNatureID,
		EnquiryLogID
		)
		VALUES(
		@RequestNatureID,
		@EnquiryLogID
		)
		
		SELECT @CstRequestId=CstRequestId
	FROM TO_CST_REQUEST
	WHERE CstRequestId = @@IDENTITY

				IF @CstRequestId<0
					BEGIN
						SET @CstRequestId=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @CstRequestId=-1
			ROLLBACK TRAN
			END
	
	
	
	
	
	
	
	
	
	




GO
