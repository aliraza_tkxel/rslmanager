SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetPaymentPlanMonitoringCount_BACKUP] 
				@postCode varchar(50)='',
				@assignedToId	int = 0,
				@regionId	int = 0,
				@suburbId	int = 0,
				@allAssignedFlag	bit,
				@allRegionFlag	bit,
				@allSuburbFlag	bit,
				@missedCheck varchar(15)=''
AS
BEGIN
	
declare @RegionSuburbClause varchar(8000)
declare @query	varchar(8000)

IF(@assignedToId = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
     ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		--SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
		--												FROM AM_ResourcePatchDevelopment 
		--												WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ 'AND IsActive=''true'')'
		SET @RegionSuburbClause ='-1=-1'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
END

		if(@missedCheck = '')
		BEGIN

		SET @query =
			   'SELECT COUNT(*) as recordCount FROM(SELECT COunt(Distinct customer.CustomerId) as TotalRecords
				FROM         AM_PaymentPlan INNER JOIN
							 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
							 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId 
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
                             INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID

				Where AM_PaymentPlan.IsCreated = ''true''
					  AND '+ @RegionSuburbClause +'
					  AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
					  AND AM_PaymentPlan.CreatedBy = (CASE WHEN -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_PaymentPlan.CreatedBy ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
                      GROUP BY AM_PaymentPlan.TennantId) as TEMP'

		END
		ELSE
		BEGIN
		SET @query =
				'SELECT COUNT(*) as recordCount FROM(SELECT COunt(Distinct customer.CustomerId) as TotalRecords
				FROM         AM_PaymentPlan INNER JOIN
							 AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN							 
							 C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId 
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
                             INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
							 
				Where AM_PaymentPlan.IsCreated = ''true''
					  AND '+ @RegionSuburbClause +'
					  AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
					  AND AM_PaymentPlan.CreatedBy = (CASE WHEN -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_PaymentPlan.CreatedBy ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
					  AND AM_PaymentPlan.PaymentPlanId IN (select distinct AM_MissedPayments.PaymentPlanId 
															 FROM AM_MissedPayments INNER JOIN
																	AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId
															  WHERE (AM_Case.CaseOfficer = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																	OR AM_Case.CaseManager = (case when -1 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId)+ ' END) ) 
																	) 
					GROUP BY AM_PaymentPlan.TennantId) as TEMP'
														


		END

PRINT(@query);
EXEC(@query);


END











GO
