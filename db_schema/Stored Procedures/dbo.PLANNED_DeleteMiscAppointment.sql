USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_DeleteMiscAppointment]    Script Date: 11/16/2016 4:44:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@isSaved bit
--EXEC	[dbo].[PLANNED_DeleteMiscAppointment]
--		@appointmentId = 1000000,
--		@isSaved = @isSaved OUTPUT
--		SELECT	@isSaved as N'@isSaved'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,7 Dec,2013>
-- Description:	<Description,,This procedure 'll delete the pending appointments>
-- =============================================
IF OBJECT_ID('dbo.[PLANNED_DeleteMiscAppointment]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_DeleteMiscAppointment] AS SET NOCOUNT ON;') 
GO
ALTER  PROCEDURE [dbo].[PLANNED_DeleteMiscAppointment] 
	@appointmentId int,
	@isSaved bit OUT
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY	
		DELETE FROM PLANNED_APPOINTMENTS_HISTORY WHERE APPOINTMENTID =@appointmentId 
		DELETE FROM PLANNED_MISC_TRADE WHERE APPOINTMENTID =@appointmentId
		DELETE FROM PLANNED_APPOINTMENTS WHERE APPOINTMENTID =@appointmentId
		set @isSaved = 1
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END 
    
END
