SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC PLANNED_GetActionRankingByStatusId @statusId = 1
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,04/11/2013>
-- Description:	<Description,,get Action ranking against StatusId>
-- WebPage: Status.aspx
-- =============================================

CREATE PROCEDURE [dbo].[PLANNED_GetActionRankingByStatusId](
@statusId int
)

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ActionId ,StatusId  ,Title, Ranking,IsEditable
	FROM PLANNED_Action
	WHERE StatusId = @statusId
END
GO
