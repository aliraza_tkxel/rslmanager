SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC PLANNED_GetConditionArrangedAppointmentsDetailByPmo
-- @pmo =1
-- Author:		Ahmed Mehmood
-- Create date: <16/8/2014>
-- Last Modified: <16/8/2014>
-- Description:	<Description,Return appointments associated with pmo >
-- Webpage : ViewConditionArrangedAppointments.aspx

-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetConditionArrangedAppointmentsDetailByPmo]
	@pmo int
AS
BEGIN

	
	DECLARE @propertyId varchar(12)

	SELECT		ROW_NUMBER() OVER (ORDER BY PA.APPOINTMENTID) AS Row
			,PA.JournalId  AS PMO
			,ISNULL(I.ItemName,'N/A' ) AS Component
				
			,ISNULL(I.ItemID ,-1) AS ComponentId
			,ISNULL(PMT.MiscTradeId , -1) AS ComponentTradeId
			,MISC_TRADE.Description  AS Trade
			,MISC_TRADE.TradeId AS TradeId

			,RIGHT('0000'+ CONVERT(VARCHAR,PA.APPOINTMENTID),4) AS JSN
			,'JSN'+RIGHT('0000'+ CONVERT(VARCHAR,PA.APPOINTMENTID),4) AS JSNSearch
			,PLANNED_STATUS.TITLE AS Status
			,E.FIRSTNAME+' '+ E.LASTNAME as Operative
			,LEFT(E.FIRSTNAME, 1)+' '+ E.LASTNAME as OperativeShortName
			,CONVERT(char(5),cast(PA.APPOINTMENTSTARTTIME as datetime),108) AS StartTime
			,CONVERT(char(5),cast(PA.APPOINTMENTENDTIME  as datetime),108) AS EndTime
			,ISNULL(NULLIF(PMT.DURATION,0),DATEDIFF(HOUR, CAST(PA.APPOINTMENTSTARTTIME AS TIME), CAST(PA.APPOINTMENTENDTIME AS TIME))) AS Duration			
			,DATEDIFF(HOUR, CAST(PA.APPOINTMENTSTARTTIME AS TIME), CAST(PA.APPOINTMENTENDTIME AS TIME)) TotalDuration
				
			,PA.APPOINTMENTDATE AS StartDate
			,PA.APPOINTMENTENDDATE AS EndDate
			,PA.CUSTOMERNOTES AS CustomerNotes
			,PA.APPOINTMENTNOTES AS JobsheetNotes
			,PA.isMiscAppointment AS IsMiscAppointment
			,PLANNED_JOURNAL.PROPERTYID  AS PropertyId
			,PA.APPOINTMENTID AS AppointmentId
			,PA.APPOINTMENTSTATUS as InterimStatus
			,PA.ISPENDING as IsPending
			
			,SUBSTRING(DATENAME(weekday, PA.APPOINTMENTDATE),1,3)+' '+ CONVERT(VARCHAR(11), PA.APPOINTMENTDATE, 106) AppointmentDateFormat1

			
FROM		PLANNED_CONDITIONWORKS PCW
			INNER JOIN PLANNED_APPOINTMENTS PA ON PCW.JournalId = PA.JournalId 
			INNER JOIN PLANNED_JOURNAL ON PA.JournalId = PLANNED_JOURNAL.JOURNALID
			INNER JOIN PA_PROPERTY_ATTRIBUTES PPA ON  PCW.AttributeId = PPA.ATTRIBUTEID
			INNER JOIN PA_ITEM_PARAMETER PIP ON  PPA.ITEMPARAMID = PIP.ItemParamID
			INNER JOIN PA_ITEM I ON  PIP.ItemId = I.ItemID
			  
			LEFT JOIN PLANNED_MISC_TRADE PMT ON PA.APPOINTMENTID = PMT.AppointmentId 
			LEFT JOIN G_TRADE AS MISC_TRADE ON PMT.TRADEID = MISC_TRADE.TradeId  
			INNER JOIN E__EMPLOYEE E ON  PA.ASSIGNEDTO = E.EMPLOYEEID 
			  
			  
			INNER JOIN PLANNED_STATUS ON  PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID 

			
				
	WHERE	PA.JournalId = @pmo
	
	
	SELECT	@propertyId=PLANNED_JOURNAL.PROPERTYID
	FROM	PLANNED_JOURNAL 
	WHERE	JOURNALID= @pmo
	
	EXEC PLANNED_GetPropertyDetail @propertyId
	
END
GO
