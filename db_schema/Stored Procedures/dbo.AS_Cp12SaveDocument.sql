USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_Cp12SaveDocument]    Script Date: 12/03/2015 10:47:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  

IF OBJECT_ID('dbo.[AS_Cp12SaveDocument]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_Cp12SaveDocument] AS SET NOCOUNT ON;') 
GO
-- =============================================  
ALTER PROCEDURE [dbo].[AS_Cp12SaveDocument]  
 @heatingId as int,  
 @journalId as int,  
 @cp12Doc as image,
 @updatedBy as int,
 @documentName as nvarchar(500)
AS  
BEGIN  

 SET NOCOUNT ON;  
 
 DECLARE @hisId int


 	DECLARE @propertyId nvarchar(20)
	DECLARE @schemeId int
	DECLARE @blockId int
	DECLARE @heatingFuel int

	SELECT  @propertyId = PropertyId
			,@schemeId = SchemeID
			,@blockId = BlockID
			,@heatingFuel = HeatingType
	FROM	PA_HeatingMapping
	WHERE   HeatingMappingId = @heatingId

 --==========================================
 -- Update P_LGSR
 --==========================================

 UPDATE [P_LGSR]  
 SET	[CP12DOCUMENT] = @cp12Doc          
 WHERE  HeatingMappingId= @heatingId 
		AND journalId= @journalId  
 

 --==========================================
 -- Update P_LGSR_HISTORY
 --==========================================
 UPDATE P_LGSR_HISTORY 
 SET	CP12DOCUMENT = @cp12Doc 
		,CP12UPLOADEDBY = @updatedBy
 WHERE	LGSRHISTORYID=(	SELECT	MAX(LGSRHISTORYID) 
						FROM	P_LGSR_HISTORY 
						WHERE	HeatingMappingId = @heatingId)
 
   
 --==========================================
 -- Update P_Documents
 --==========================================

 DECLARE @documentTypeId int,@documentSubTypeId int,@expiryDate SMALLDATETIME ,@documentDate SMALLDATETIME,@CP12Number NVARCHAR(100)
 
 SELECT @documentTypeId=DocumentTypeId 
 FROM	P_Documents_Type 
 WHERE  Title='CP12'

 SELECT @documentSubTypeId= DocumentSubtypeId 
 FROM	P_Documents_SubType 
 WHERE  DocumentTypeId=@documentTypeId and Title='CP12 Certificate'


 SELECT @expiryDate = CP12Renewal,@documentDate=ISSUEDATE,@CP12Number= CP12NUMBER  
 FROM  P_LGSR 
 WHERE HeatingMappingId = @heatingId and journalId= @journalId  


INSERT INTO P_Documents (PropertyId,SchemeId,BlockId,Category,DocumentName,DocumentTypeId,DocumentSubtypeId,UploadedBy ,ExpiryDate ,DocumentDate,CP12Number,CreatedDate,KeyWords )  
VALUES  (@propertyId,@schemeId,@blockId,'Compliance',@documentName ,@documentTypeId,@documentSubTypeId,@updatedBy,@expiryDate,@documentDate,@CP12Number,GETDATE(),'CP12 Populated')  

 
END  