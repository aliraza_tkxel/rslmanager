USE [RSLBHALive]
GO 
/****** Object:  StoredProcedure [dbo].[AS_AppointmentsToBeArranged]    Script Date: 01/15/2016 11:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
 --EXEC AS_AppointmentsToBeArranged
	--		@check56Days = 0,		
	--		@searchedText = NULL,
			@pageSize = 10,
			@pageNumber = 1,
			@sortColumn = 'Address',
			@sortOrder = 'ASC',
			@totalCount = 0
		
-- Author:		<Salman Nazir>
-- Modified By: <Noor Muhammad>, <Aamir Waheed, March 25, 2013>, <Aamir Waheed, May 30, 2013>
-- Create date: <15 Sep 2012>
-- Description:	<This stored procedure returns the appointments that have been arranged against the property>
-- Parameters:	
		--@check56Days bit,
		--@searchedText varchar(8000),
		--@pageSize int = 30,
		--@pageNumber int = 1,
		--@sortColumn varchar(50) = 'AS_JOURNAL.JOURNALID',
		--@sortOrder varchar (5) = 'DESC',
		--@totalCount int=0 output
-- ============================================= */


IF OBJECT_ID('dbo.AS_AppointmentsToBeArranged') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_AppointmentsToBeArranged AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_AppointmentsToBeArranged]
( 
		--These parameters will be used for search
		@check56Days bit=0,				
		@searchedText varchar(5000)='',		
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'JOURNALID',
		@sortOrder varchar (5) = 'DESC',
		@status varchar (150)='',
		@patchId		int = -1,
		@developmentId	int = -1,
		@totalCount int = 0 output
)
AS
BEGIN
		DECLARE @SelectClause varchar(MAX),
        @fromClause   varchar(MAX),
        @whereClause  varchar(max),	        
        @orderClause  varchar(max),	
        @mainSelectQuery varchar(max),     
		@mainSelectQueryScheme varchar(max),  
		@mainSelectQueryBlock varchar(max),  
		@groupByClauseScheme varchar(max),
		@groupByClauseBlock varchar(max),   
        @rowNumberQuery varchar(max),
        @finalQuery varchar(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(max),
        
        --variables for paging
        @offset int,
		@limit int,
		@SelectClauseScheme varchar(MAX),
        @fromClauseScheme   varchar(MAX),
        @whereClauseScheme  varchar(max),	    
		@SelectClauseBlock varchar(MAX),
        @fromClauseBlock   varchar(MAX),
        @whereClauseBlock  varchar(max),
		@selectClaseForCountProperty varchar(max),
		@selectClaseForCountScheme varchar(max),
		@selectClaseForCountBlock varchar(max),
		@searchCriteriaScheme varchar(max),
		@searchCriteriaBlock varchar(max),
		@resultingQuery varchar(max),
		@unionQuery varchar(max),
		@MainsGasFuelId INT
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
		SET @searchCriteriaScheme = ' 1=1 '
		SET @searchCriteriaBlock = ' 1=1 '
		SET @unionQuery = char(10) + ' UNION ALL ' + char(10)
		SET @MainsGasFuelId = (SELECT VALUEID FROM PA_PARAMETER_VALUE PV
								INNER JOIN PA_PARAMETER PA on PA.ParameterID = PV.ParameterID
								WHERE ValueDetail = 'Mains Gas' AND PV.IsActive = 1 and PA.ParameterName = 'Heating Fuel')

		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (P__PROPERTY.ADDRESS1 LIKE ''%'+@searchedText+'%''  OR P__PROPERTY.ADDRESS2 LIKE ''%'+@searchedText+'%''  OR P__PROPERTY.ADDRESS3 LIKE ''%@'+@searchedText+'%''  OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+@searchedText+'%'')'
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'AND (P_SCHEME.SCHEMENAME LIKE ''%'+@searchedText+'%'')'
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'AND (P_BLOCK.ADDRESS1 LIKE ''%'+@searchedText+'%''  OR P_BLOCK.ADDRESS2 LIKE ''%'+@searchedText+'%''  OR P_BLOCK.ADDRESS3 LIKE ''%@'+@searchedText+'%''  OR P_BLOCK.BLOCKNAME + '' '' + P_BLOCK.ADDRESS1 LIKE ''%'+@searchedText+'%'')'
		END 

		IF(@status != '' OR @status!= NULL)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND AS_Status.Title LIKE ''%' + @status + '%'''
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) +'AND AS_Status.Title LIKE ''%' + @status + '%'''
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) +'AND AS_Status.Title LIKE ''%' + @status + '%'''
		END

		If (@patchId <>-1) 
		Begin
			SET @searchCriteria = @searchCriteria + ' AND PDR_DEVELOPMENT.PATCHID = ' + CONVERT(varchar, (@patchId)) + CHAR(10)	
			SET @searchCriteriaScheme = @searchCriteriaScheme + ' AND PDR_DEVELOPMENT.PATCHID = ' + CONVERT(varchar, (@patchId)) + CHAR(10)	
			SET @searchCriteriaBlock = @searchCriteriaBlock + ' AND PDR_DEVELOPMENT.PATCHID = ' + CONVERT(varchar, (@patchId)) + CHAR(10)	
		End

		If (@developmentId <>-1) 
		Begin
			SET @searchCriteria = @searchCriteria + ' AND P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, (@developmentId)) + CHAR(10)	
			SET @searchCriteriaScheme = @searchCriteriaScheme + ' AND P_SCHEME.DEVELOPMENTID = ' + CONVERT(varchar, (@developmentId)) + CHAR(10)	
			SET @searchCriteriaBlock = @searchCriteriaBlock + ' AND P_BLOCK.DEVELOPMENTID = ' + CONVERT(varchar, (@developmentId)) + CHAR(10)	
		End
				
		IF(@check56Days = 1)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <=56 '
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <=56 '
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <=56 '
																
		END
		
		--These conditions w�ll be used in every case
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (AS_JOURNAL.STATUSID  IN (select StatusId from AS_Status WHERE Title LIKE ''Appointment to be arranged'' OR Title LIKE ''No Entry'' OR Title LIKE ''Aborted'' OR TITLE LIKE ''Cancelled''))		
															AND AS_JOURNAL.IsCurrent = 1
															AND dbo.P__PROPERTY.STATUS IN (SELECT STATUSID FROM P_STATUS WHERE DESCRIPTION IN(''Let'',''Available to rent'',''Unavailable'')) 
															AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)	
															AND AS_JOURNAL.ServicingTypeId = (Select ServicingTypeID from P_ServicingType where Description = ''Gas'')
															AND PV.ValueId = '+ Convert(varchar,@MainsGasFuelId) +''

		SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'AND (AS_JOURNAL.STATUSID  IN (select StatusId from AS_Status WHERE Title LIKE ''Appointment to be arranged'' OR Title LIKE ''No Entry'' OR Title LIKE ''Legal Proceedings'' OR Title LIKE ''Aborted'' OR TITLE LIKE ''Cancelled''))		
															AND AS_JOURNAL.IsCurrent = 1
															AND AS_JOURNAL.ServicingTypeId = (Select ServicingTypeID from P_ServicingType where Description = ''Gas'')
															AND PV.ValueId =' + Convert(varchar,@MainsGasFuelId) +''

		SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'AND (AS_JOURNAL.STATUSID  IN (select StatusId from AS_Status WHERE Title LIKE ''Appointment to be arranged'' OR Title LIKE ''No Entry'' OR Title LIKE ''Legal Proceedings'' OR Title LIKE ''Aborted'' OR TITLE LIKE ''Cancelled''))		
															AND AS_JOURNAL.IsCurrent = 1
															AND AS_JOURNAL.ServicingTypeId = (Select ServicingTypeID from P_ServicingType where Description = ''Gas'')
															AND PV.ValueId =' + Convert(varchar,@MainsGasFuelId) +''	
	
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @SelectClause = 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+')
							ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''')+ ISNULL('' , ''+P__PROPERTY.TOWNCITY,'''')  AS ADDRESS ,  
							P__PROPERTY.HouseNumber as HouseNumber,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							P__PROPERTY.ADDRESS3 as ADDRESS3,	
							P__PROPERTY.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,							
							--P_FUELTYPE.FUELTYPE AS FUEL,
							Case When ISNULL(PV.ValueDetail, ''-'') = ''Please Select'' Then ''-''
							ELSE ISNULL(PV.ValueDetail, ''-'') end AS FUEL,
							P__PROPERTY.PROPERTYID,
							C_TENANCY.TENANCYID,
							AS_JOURNAL.JOURNALID,
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3  
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
							ISNULL(P_STATUS.DESCRIPTION, ''N/A'') AS PropertyStatus,
							E_PATCH.PatchId as PatchId,
							E_PATCH.Location as PatchName,
							ISNULL(CA.Tel,''N/A'') as Telephone,
							CUST.FIRSTNAME  + '' '' + CUST.LASTNAME AS NAME,
							CONVERT(varchar,P_LGSR.ISSUEDATE,103)AS ISSUEDATE,
							P_LGSR.ISSUEDATE as LGSRDate,
							CT.CUSTOMERID as CustomerId,
							ISNULL(APP.APPOINTMENTID, 0) AS APPOINTMENTID
							,riskCounter.risk, CA.EMAIL, P__PROPERTY.TOWNCITY, P__PROPERTY.COUNTY
							,1 as BoilersCount
							,''Property'' as AppointmentType'

		SET @selectClaseForCountProperty = 'SELECT DISTINCT 
							ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''')+ ISNULL('' , ''+P__PROPERTY.TOWNCITY,'''')  AS ADDRESS ,  
							P__PROPERTY.HouseNumber as HouseNumber,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							P__PROPERTY.ADDRESS3 as ADDRESS3,	
							P__PROPERTY.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,							
							--P_FUELTYPE.FUELTYPE AS FUEL,
							Case When ISNULL(PV.ValueDetail, ''-'') = ''Please Select'' Then ''-''
							ELSE ISNULL(PV.ValueDetail, ''-'') end AS FUEL,
							P__PROPERTY.PROPERTYID,
							C_TENANCY.TENANCYID,
							AS_JOURNAL.JOURNALID,
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3  
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
			
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
							ISNULL(P_STATUS.DESCRIPTION, ''N/A'') AS PropertyStatus,
							E_PATCH.PatchId as PatchId,
							E_PATCH.Location as PatchName,
							ISNULL(CA.Tel,''N/A'') as Telephone,
							CUST.FIRSTNAME  + '' '' + CUST.LASTNAME AS NAME,
							CONVERT(varchar,P_LGSR.ISSUEDATE,103)AS ISSUEDATE,
							P_LGSR.ISSUEDATE as LGSRDate,
							CT.CUSTOMERID as CustomerId,
							ISNULL(APP.APPOINTMENTID, 0) AS APPOINTMENTID
							,riskCounter.risk, CA.EMAIL, P__PROPERTY.TOWNCITY, P__PROPERTY.COUNTY
							,1 as BoilersCount
							,''Property'' as AppointmentType'


		SET @SelectClauseScheme = 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+')
							ISNULL(P_SCHEME.SCHEMENAME,'''')  AS ADDRESS ,  
							P_SCHEME.SCHEMENAME as HouseNumber,
							'''' as ADDRESS1,
							'''' as ADDRESS2,
							'''' as ADDRESS3,	
							'''' as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,							
							--P_FUELTYPE.FUELTYPE AS FUEL,
							Case When ISNULL(PV.ValueDetail, ''-'') = ''Please Select'' Then ''-''
							ELSE ISNULL((PV.ValueDetail), ''-'') end AS FUEL,
							cONVERT(VARCHAR, (P_SCHEME.SCHEMEID)) AS PROPERTYID,
							-1 AS TENANCYID,
							(AS_JOURNAL.JOURNALID) AS JOURNALID,
							Case (AS_Status.Title) WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId=3  
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')''
							else 
								(AS_Status.Title) end AS StatusTitle,
							''N/A'' AS PropertyStatus,
							(E_PATCH.PatchId) as PatchId,
							(E_PATCH.Location) as PatchName,
							''N/A'' as Telephone,
							'''' AS NAME,
							CONVERT(varchar,(P_LGSR.ISSUEDATE),103)AS ISSUEDATE,
							(P_LGSR.ISSUEDATE) as LGSRDate,
							-1 as CustomerId,
							ISNULL((APP.APPOINTMENTID), 0) AS APPOINTMENTID
							,-1 AS risk, '''' AS EMAIL, '''' TOWNCITY, '''' COUNTY
							,HeatingMapping.BOILERSCOUNT as BoilersCount
							,''Scheme'' as AppointmentType'

		SET @selectClaseForCountScheme = 'SELECT DISTINCT 
							ISNULL(P_SCHEME.SCHEMENAME,'''')  AS ADDRESS ,  
							P_SCHEME.SCHEMENAME as HouseNumber,
							'''' as ADDRESS1,
							'''' as ADDRESS2,
							'''' as ADDRESS3,	
							'''' as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,							
							--P_FUELTYPE.FUELTYPE AS FUEL,
							Case When ISNULL(PV.ValueDetail, ''-'') = ''Please Select'' Then ''-''
							ELSE ISNULL((PV.ValueDetail), ''-'') end AS FUEL,
							cONVERT(VARCHAR, (P_SCHEME.SCHEMEID)) AS PROPERTYID,
							-1 AS TENANCYID,
							(AS_JOURNAL.JOURNALID) AS JOURNALID,
							Case (AS_Status.Title) WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId=3 
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')''
							else 
								(AS_Status.Title) end AS StatusTitle,
							''N/A'' AS PropertyStatus,
							(E_PATCH.PatchId) as PatchId,
							(E_PATCH.Location) as PatchName,
							''N/A'' as Telephone,
							'''' AS NAME,
							CONVERT(varchar,(P_LGSR.ISSUEDATE),103)AS ISSUEDATE,
							(P_LGSR.ISSUEDATE) as LGSRDate,
							-1 as CustomerId,
							ISNULL((APP.APPOINTMENTID), 0) AS APPOINTMENTID
							,-1 AS risk, '''' AS EMAIL, '''' TOWNCITY, '''' COUNTY
							,HeatingMapping.BOILERSCOUNT as BoilersCount
							,''Scheme'' as AppointmentType'

		SET @SelectClauseBlock = 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+')
							ISNULL((P_BLOCK.BLOCKNAME),'''') +'' ''+ ISNULL((P_BLOCK.ADDRESS1),'''') +'' ''+ ISNULL((P_BLOCK.ADDRESS2),'''') +'' ''+ ISNULL((P_BLOCK.ADDRESS3),'''')+ ISNULL('' , ''+(P_BLOCK.TOWNCITY),'''')  AS ADDRESS ,  
							(P_BLOCK.BLOCKNAME) as HouseNumber,
							(P_BLOCK.ADDRESS1) as ADDRESS1,
							(P_BLOCK.ADDRESS2) as ADDRESS2,
							(P_BLOCK.ADDRESS3) as ADDRESS3,	
							(P_BLOCK.POSTCODE) as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,(P_LGSR.ISSUEDATE)),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,(P_LGSR.ISSUEDATE))) AS DAYS,							
							--P_FUELTYPE.FUELTYPE AS FUEL,
							Case When ISNULL((PV.ValueDetail), ''-'') = ''Please Select'' Then ''-''
							ELSE ISNULL((PV.ValueDetail), ''-'') end AS FUEL,
							CONVERT(VARCHAR, (P_BLOCK.BLOCKID)) AS PROPERTYID,
							-1 AS TENANCYID,
							(AS_JOURNAL.JOURNALID) AS JOURNALID,
							Case (AS_Status.Title) WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId=3  
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())

								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'')  
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								(AS_Status.Title) end AS StatusTitle,
							''N/A'' AS PropertyStatus,
							(E_PATCH.PatchId) as PatchId,
							(E_PATCH.Location) as PatchName,
							''N/A'' as Telephone,
							'''' AS NAME,
							CONVERT(varchar,(P_LGSR.ISSUEDATE),103)AS ISSUEDATE,
							(P_LGSR.ISSUEDATE) as LGSRDate,
							-1 as CustomerId,
							ISNULL((APP.APPOINTMENTID), 0) AS APPOINTMENTID
							,-1 AS risk, '''' AS EMAIL, '''' TOWNCITY, '''' COUNTY
							, HeatingMapping.BOILERSCOUNT as BoilersCount
							,''Block'' as AppointmentType'

		SET @selectClaseForCountBlock = 'SELECT DISTINCT 
							ISNULL((P_BLOCK.BLOCKNAME),'''') +'' ''+ ISNULL((P_BLOCK.ADDRESS1),'''') +'' ''+ ISNULL((P_BLOCK.ADDRESS2),'''') +'' ''+ ISNULL((P_BLOCK.ADDRESS3),'''')+ ISNULL('' , ''+(P_BLOCK.TOWNCITY),'''')  AS ADDRESS ,  
							(P_BLOCK.BLOCKNAME) as HouseNumber,
							(P_BLOCK.ADDRESS1) as ADDRESS1,
							(P_BLOCK.ADDRESS2) as ADDRESS2,
							(P_BLOCK.ADDRESS3) as ADDRESS3,	
							(P_BLOCK.POSTCODE) as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,(P_LGSR.ISSUEDATE)),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,(P_LGSR.ISSUEDATE))) AS DAYS,							
							--P_FUELTYPE.FUELTYPE AS FUEL,
							Case When ISNULL((PV.ValueDetail), ''-'') = ''Please Select'' Then ''-''
							ELSE ISNULL((PV.ValueDetail), ''-'') end AS FUEL,
							CONVERT(VARCHAR, (P_BLOCK.BLOCKID)) AS PROPERTYID,
							-1 AS TENANCYID,
							(AS_JOURNAL.JOURNALID) AS JOURNALID,
							Case (AS_Status.Title) WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId=3 
								--AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = (AS_JOURNAL.JOURNALID)
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								(AS_Status.Title) end AS StatusTitle,
							''N/A'' AS PropertyStatus,
							(E_PATCH.PatchId) as PatchId,
							(E_PATCH.Location) as PatchName,
							''N/A'' as Telephone,
							'''' AS NAME,
							CONVERT(varchar,(P_LGSR.ISSUEDATE),103)AS ISSUEDATE,
							(P_LGSR.ISSUEDATE) as LGSRDate,
							-1 as CustomerId,
							ISNULL((APP.APPOINTMENTID), 0) AS APPOINTMENTID
							,-1 AS risk, '''' AS EMAIL, '''' TOWNCITY, '''' COUNTY
							, HeatingMapping.BOILERSCOUNT as BoilersCount
							,''Block'' as AppointmentType'
		
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		
		SET @fromClause = CHAR(10) + 'FROM P__PROPERTY 
										CROSS APPLY (SELECT COUNT(*) AS BOILERSCOUNT, PROPERTYID AS PROPERTYID, MAX(PA_HeatingMapping.HeatingMappingId) AS HeatingMappingId
													FROM PA_HeatingMapping
													INNER JOIN PA_PARAMETER_VALUE PV ON Pv.ValueID = PA_HeatingMapping.HeatingType
													WHERE PROPERTYID = P__PROPERTY.PROPERTYID and PA_HeatingMapping.IsActive = 1
													AND PV.ValueDetail = ''Mains Gas''
													GROUP BY PROPERTYID) AS HeatingMapping
										INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HeatingMapping.HeatingMappingId
										INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID AND AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')
										LEFT JOIN AS_APPOINTMENTS APP ON APP.JOURNALID = dbo.AS_JOURNAL.JOURNALID								
										INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
										--INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
										LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
										LEFT JOIN P_LGSR on P_LGSR.HeatingMappingId = HeatingMapping.HeatingMappingId
										INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
										LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
										INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
										
										
										OUTER Apply(SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
														Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
														ELSE Convert(Varchar(50), M.ReletDate,103)END as ReletDate,J.JOURNALID
													FROM P__PROPERTY P
											INNER JOIN C_JOURNAL J ON C_TENANCY.TENANCYID =J.TENANCYID
											INNER JOIN C_TERMINATION T ON J.JOURNALID = T.JOURNALID 
											INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
											INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
											INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
											INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
											WHERE ((DATEADD(YEAR,1,P_LGSR.ISSUEDATE) BETWEEN T.TTIMESTAMP AND T.TerminationDate)
													OR (DATEADD(YEAR,1,P_LGSR.ISSUEDATE) > T.TerminationDate))
										 ) As  T
										

										Left JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = C_TENANCY.TENANCYID
										AND CT.CUSTOMERTENANCYID = (SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					)

										Left JOIN C_ADDRESS CA ON CA.CUSTOMERID = CT.CUSTOMERID And CA.ISDEFAULT=1
										Left JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = CT.CUSTOMERID
										LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P__PROPERTY.PROPERTYID
										AND A.ITEMPARAMID = (	SELECT	ItemParamID
																FROM	PA_ITEM_PARAMETER
																		INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
																		INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
																WHERE	ParameterName = ''Heating Fuel'' AND ItemName = ''Heating'')
										LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
										Outer Apply (SELECT count( RISKHISTORYID) as risk
										FROM C_JOURNAL J 
										INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID
										WHERE CUSTOMERID = CUST.CUSTOMERID AND ITEMNATUREID=63 
										AND CR.RISKHISTORYID = (SELECT MAX(RISKHISTORYID) FROM C_RISK IN_CR WHERE IN_CR.JOURNALID=J.JOURNALID)                 
										AND CR.ITEMSTATUSID <> 14) riskCounter 
										 '
		
		SET @fromClauseScheme = CHAR(10) + ' FROM P_SCHEME 
											CROSS APPLY (SELECT COUNT(*) AS BOILERSCOUNT, SchemeID SCHEMEID, MAX(PA_HeatingMapping.HeatingMappingId) AS HeatingMappingId
														FROM PA_HeatingMapping
														INNER JOIN PA_PARAMETER_VALUE PV ON Pv.ValueID = PA_HeatingMapping.HeatingType
														WHERE SchemeID = P_SCHEME.SCHEMEID and PA_HeatingMapping.IsActive = 1
														AND PV.ValueDetail = ''Mains Gas''
														GROUP BY SchemeID ) AS HeatingMapping
											INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HeatingMapping.HeatingMappingId
											INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.JOURNALID=JHM.JournalId AND AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')
											LEFT JOIN AS_APPOINTMENTS APP ON APP.JournalId = AS_JOURNAL.JOURNALID								
											INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
											LEFT JOIN P_LGSR on HeatingMapping.HeatingMappingId = P_LGSR.HeatingMappingId
											INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
											INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
											LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.SchemeId = P_SCHEME.SCHEMEID AND A.HeatingMappingId = HeatingMapping.HeatingMappingId
											AND A.ITEMPARAMID = (	SELECT	ItemParamID
																	FROM	PA_ITEM_PARAMETER
																			INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
																			INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
																	WHERE	ParameterName = ''Heating Fuel'' AND ItemName = ''Boiler Room'')
											LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
										 '

		SET @fromClauseBlock = CHAR(10) + ' FROM P_BLOCK 
											CROSS APPLY (SELECT COUNT(*) AS BOILERSCOUNT, BlockId BLOCKID, MAX(PA_HeatingMapping.HeatingMappingId) AS HeatingMappingId
														FROM PA_HeatingMapping
														INNER JOIN PA_PARAMETER_VALUE PV ON Pv.ValueID = PA_HeatingMapping.HeatingType
														WHERE BlockId = P_BLOCK.BLOCKID and PA_HeatingMapping.IsActive = 1
														AND PV.ValueDetail = ''Mains Gas''
														GROUP BY BLOCKID) AS HeatingMapping
											INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HeatingMapping.HeatingMappingId
											INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.JOURNALID=JHM.JournalId AND AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')
											LEFT JOIN AS_APPOINTMENTS APP ON APP.JournalId = AS_JOURNAL.JOURNALID								
											INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
											LEFT JOIN P_LGSR on HeatingMapping.HeatingMappingId = P_LGSR.HeatingMappingId
											INNER JOIN PDR_DEVELOPMENT ON P_BLOCK.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
											INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
											LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.BlockId = P_BLOCK.BLOCKID AND A.HeatingMappingId = HeatingMapping.HeatingMappingId
											AND A.ITEMPARAMID = (	SELECT	ItemParamID
																	FROM	PA_ITEM_PARAMETER
																			INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
																			INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
																	WHERE	ParameterName = ''Heating Fuel'' AND ItemName = ''Boiler Room'')
											LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
										 '
		-- End building From clause
		--======================================================================================== 														  

		--======================================================================================== 														  
		--Begin Building Group By Clause
		SET @groupByClauseScheme = CHAR(10) + ' GROUP BY P_SCHEME.SCHEMEID ' + CHAR(10)
		SET @groupByClauseBlock = CHAR(10) + ' GROUP BY P_BLOCK.BLOCKID ' + CHAR(10)
										 
		-- End building From clause
		--========================================================================================
		
		--========================================================================================    
		-- Begin building OrderBy clause						
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
		END		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building WHERE clause
	    			  				
		SET @whereClause =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteria 
		SET @whereClauseScheme = CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteriaScheme 
		SET @whereClauseBlock =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteriaBlock 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause + @fromClause + @whereClause
		Set @mainSelectQueryScheme = @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme 
		Set @mainSelectQueryBlock = @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock 
		print @mainSelectQuery
		--print @mainSelectQueryScheme
		--print @mainSelectQueryBlock
		SET @resultingQuery = @mainSelectQuery + @unionQuery + @mainSelectQueryScheme + @unionQuery + @mainSelectQueryBlock + @orderClause

		--print(@resultingQuery)
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@resultingQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================	
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		--print(@resultingQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================		
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@selectClaseForCountProperty+@fromClause+@whereClause+@unionQuery+@selectClaseForCountScheme+@fromClauseScheme+
													@whereClauseScheme+@unionQuery+@selectClaseForCountBlock+@fromClauseBlock+@whereClauseBlock+') as Records ) '
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================									
		--========================================================================================								
END


