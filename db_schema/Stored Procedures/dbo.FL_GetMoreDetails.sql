USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetMoreDetails]    Script Date: 04/19/2013 20:53:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec FL_GetMoreDetails 665
-- Author:		<Behroz Sikander>
-- Create date: <21/02/2013>
-- Description:	<This stored procedure fetches data based on fault id for Screen#5>
-- Webpage: MoreDetails.aspx
-- =============================================

IF object_id('FL_GetMoreDetails') IS NULL
  EXEC ('create procedure dbo.FL_GetMoreDetails as select 1')
GO

ALTER PROCEDURE [dbo].[FL_GetMoreDetails]
(
	@faultId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- ===============================
	-- FAULT INFORMATION
	-- ===============================
	
	Select 
		FL_FAULT.FAULTID
		,FL_FAULT.Description 
		,CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as	ResponseTime
		,FL_FAULT.Gross
		,FL_FAULT.Recharge as Recharge
		,FL_FAULT_PRIORITY.PriorityName 
	from 
		FL_FAULT 
		INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT_PRIORITY.PriorityID = FL_FAULT.PriorityID
	where 
		FaultId = @faultId
		
		
		-- ===============================
		-- FAULT AREA LIST
		-- ===============================
		SELECT	Distinct FL_AREA.AreaID AS areaId
				,FL_AREA.AreaName AS area
		FROM	FL_AREA
		ORDER BY FL_AREA.AreaName ASC
		

END
