SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_FAULT_GETVALUES
/* ===========================================================================
 '   NAME:          FL_FAULT_GETVALUES
 '   DATE CREATED:   22 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a fault against a particular faultID from FL_FAULT table which will be used to update 
 '		     a fault values
 '   IN:            @FaultID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@FaultID as Int
AS
	SELECT  FL_FAULT.FAULTID, FL_LOCATION.LocationName, FL_AREA.AreaName, FL_ELEMENT.ElementName, FL_FAULT.Description, FL_FAULT.PriorityID,FL_FAULT.NetCost, FL_FAULT.Vat, 
                      FL_FAULT.Gross, FL_FAULT.VatRateID, FL_FAULT.EffectFrom, FL_FAULT.Recharge, FL_FAULT.PreInspection, FL_FAULT.PostInspection, 
                      FL_FAULT.StockConditionItem, FL_FAULT.FaultActive
FROM         FL_FAULT INNER JOIN
                      FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID INNER JOIN
                      FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID INNER JOIN
                      FL_LOCATION ON FL_AREA.LocationID = FL_LOCATION.LocationID
WHERE     (FL_FAULT.FaultID = @FaultID)













GO
