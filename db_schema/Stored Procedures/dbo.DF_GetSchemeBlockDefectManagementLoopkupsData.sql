USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetSchemeBlockDefectManagementLoopkupsData]    Script Date: 03-Apr-18 2:30:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.DF_GetSchemeBlockDefectManagementLoopkupsData') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetSchemeBlockDefectManagementLoopkupsData AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[DF_GetSchemeBlockDefectManagementLoopkupsData]
@requestType Varchar(200),
@id int
AS
BEGIN

-- 1- Get Defect Categories
	SELECT
		CategoryId
		,[Description]	AS CategoryName
	FROM
		P_DEFECTS_CATEGORY

	-- 2- Get Scheme/Block Appliances by PropertyId
	if @requestType = 'block'
	begin
	SELECT
		A.PROPERTYAPPLIANCEID
		,AT.APPLIANCETYPE	AS APPLIANCE
	FROM
		GS_PROPERTY_APPLIANCE A
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		INNER JOIN PA_ITEM ON A.ItemId = PA_ITEM.ItemID
	WHERE
		A.BlockID = @id 
		AND A.ISACTIVE = 1
		AND ItemName = 'Appliances'
	end
	ELSE
	BEGIN
		SELECT
		A.PROPERTYAPPLIANCEID
		,AT.APPLIANCETYPE	AS APPLIANCE
	FROM
		GS_PROPERTY_APPLIANCE A
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		INNER JOIN PA_ITEM ON A.ItemId = PA_ITEM.ItemID
	WHERE
		A.SchemeID = @id 
		AND A.ISACTIVE = 1
		AND ItemName = 'Appliances'
	END

	-- 3- Get Employees List (for Parts order by)
	SELECT
		E.EMPLOYEEID
		,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')	AS EmployeeName
	FROM
		E__EMPLOYEE E
			INNER JOIN E_JOBDETAILS JD ON E.EMPLOYEEID = JD.EMPLOYEEID
				AND
				JD.ACTIVE = 1

	-- 4- Get Defect Priorities List.
	SELECT
		P.PriorityID
		,P.PriorityName
	FROM
		P_DEFECTS_PRIORITY P

	-- 5- Get Trades List
	SELECT
		T.TradeId
		,T.[Description]	AS Trade
	FROM
		G_TRADE T

	-- 6. Get Heating types added for scheme/block
	if @requestType = 'block'
	begin
		select AS_JOURNAL.JOURNALID, 'Boilder ' + convert(varchar,ROW_NUMBER() OVER(ORDER BY PA_HeatingMapping.HeatingMappingId ASC)) + ': ' + PA_PARAMETER_VALUE.ValueDetail as BoilerNmber from PA_HeatingMapping
		inner join PA_PARAMETER_VALUE on PA_PARAMETER_VALUE.ValueID=PA_HeatingMapping.HeatingType
		inner join AS_JOURNAL on AS_JOURNAL.HeatingMappingId = PA_HeatingMapping.HeatingMappingId
		where PA_HeatingMapping.BlockID = @id and PA_HeatingMapping.IsActive=1
		order by PA_HeatingMapping.HeatingMappingId
	END
	ELSE
	BEGIN
		select AS_JOURNAL.JOURNALID, 'Boilder ' + convert(varchar,ROW_NUMBER() OVER(ORDER BY PA_HeatingMapping.HeatingMappingId ASC)) + ': ' + PA_PARAMETER_VALUE.ValueDetail as BoilerNmber from PA_HeatingMapping
		inner join PA_PARAMETER_VALUE on PA_PARAMETER_VALUE.ValueID=PA_HeatingMapping.HeatingType
		inner join AS_JOURNAL on AS_JOURNAL.HeatingMappingId = PA_HeatingMapping.HeatingMappingId
		where PA_HeatingMapping.SchemeID = @id and PA_HeatingMapping.IsActive=1
		order by PA_HeatingMapping.HeatingMappingId
	END

END