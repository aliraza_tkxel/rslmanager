USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_MyStaffLeaveOfToil]    Script Date: 6/6/2017 4:33:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Moiz
-- Create date: April 27, 2018
-- =============================================

IF OBJECT_ID('dbo.E_MyStaffLeaveOfToil') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_MyStaffLeaveOfToil AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_MyStaffLeaveOfToil] 
	-- Add the parameters for the stored procedure here
	@empId INT
	
AS
BEGIN

	DECLARE @MyStaffEmpIds TABLE(
		EmpId int
	)

	DECLARE @MyStaffLeavesData TABLE(
		StartDate smalldatetime null,
		ReturnDate smalldatetime null,
		Duration float null,
		Unit varchar(255),
		LeaveStatus varchar(255),
		EmpId int null,
		ItemNatureId int null,
		NatureDescription varchar(255),
		FirstName varchar(255),
		LastName varchar(255),
		AbsenceHistoryId int,
		HolType varchar(255)
	)

	DECLARE @staffId INT
	DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME		

	INSERT INTO @MyStaffEmpIds (EmpId)
	(SELECT E.EMPLOYEEID 
	FROM	E__EMPLOYEE E   
			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
			WHERE	 	J.LINEMANAGER =  @empId   AND J.ACTIVE = 1)

	DECLARE myStaffCursor CURSOR  FOR
	SELECT EmpId from @MyStaffEmpIds

	OPEN myStaffCursor

	FETCH NEXT FROM myStaffCursor INTO @staffId
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE 
		FROM	EMPLOYEE_ANNUAL_START_END_DATE(@staffId)

		INSERT INTO @MyStaffLeavesData (StartDate, ReturnDate, Duration, Unit, LeaveStatus, EmpId, ItemNatureId, NatureDescription, FirstName, LastName, AbsenceHistoryId, HolType)

		SELECT	A.STARTDATE,a.RETURNDATE,
		cast(A.DURATION as decimal (16, 2)) as duration 
		, isNull(A.DURATION_TYPE, Case when j.PARTFULLTIME=2 then 'hrs' else 'days' end)  as unit 
		,'Pending' AS Status
		,J.EMPLOYEEID,JNAL.ITEMNATUREID, en.DESCRIPTION, E.FIRSTNAME as FirstName, E.LASTNAME as LastName, A.ABSENCEHISTORYID as AbsenceHistoryId, A.HolType
					FROM 	E__EMPLOYEE E  
							LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
							LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID  AND JNAL.CURRENTITEMSTATUSID = (SELECT ITEMSTATUSID from E_STATUS WHERE DESCRIPTION = 'Pending')
							--AND JNAL.ITEMNATUREID = 1  
							inner JOIN E_NATURE en on JNAL.ITEMNATUREID = en.ITEMNATUREID
							INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = (
											SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
											WHERE JOURNALID = JNAL.JOURNALID AND A.STARTDATE BETWEEN @BHSTART AND @BHEND  --AND E_ABSENCE.STARTDATE < GETDATE()
										  ) 
					WHERE 	E.EMPLOYEEID =  @staffId --and a.STARTDATE != NULL
					and JNAL.ITEMNATUREID  IN (SELECT ITEMNATUREID from E_NATURE 
											where DESCRIPTION  in ('BRS Time Off in Lieu') )

		FETCH NEXT FROM myStaffCursor INTO @staffId
	END

	CLOSE myStaffCursor
	DEALLOCATE myStaffCursor

	SELECT StartDate, ReturnDate, Duration, Unit, LeaveStatus, EmpId, ItemNatureId, NatureDescription, FirstName, LastName, AbsenceHistoryId, HolType
	FROM @MyStaffLeavesData

	
	
END