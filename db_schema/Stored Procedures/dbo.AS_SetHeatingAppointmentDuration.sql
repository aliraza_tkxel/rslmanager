USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[AS_SetHeatingAppointmentDuration]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_SetHeatingAppointmentDuration] AS SET NOCOUNT ON;') 
GO  
-- Stored Procedure  
  
-- =============================================  
-- Author:  Shehriyar Zafar 
-- Create date: 12/08/2018 
-- =============================================  
Alter PROCEDURE [dbo].[AS_SetHeatingAppointmentDuration]   
 -- Add the parameters for the stored procedure here  
 @AppointmentId INT,
 @HeatingApptDurationDetail AS AS_AppointmentHeatingFuelDuration READONLY
 ,@isSaved BIT = 0 OUTPUT

AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  
    
BEGIN TRANSACTION  
BEGIN TRY  
 
-- =====================================================  
-- Declare a cursor to enter works requied,  
--  loop through record and instert in table  
-- =====================================================  
  
DECLARE heatingApptDurationDetail CURSOR FOR SELECT  
 *  
FROM @HeatingApptDurationDetail  
OPEN heatingApptDurationDetail  
  
-- Declare Variable to use with cursor  
DECLARE 
@HeatingTypeId INT,
@Duration INT,  
@TradeId INT
  
-- =====================================================  
-- Loop (Start) through records and insert works required  
-- =====================================================    
  -- Fetch record for First loop iteration.  
  FETCH NEXT FROM heatingApptDurationDetail INTO @HeatingTypeId, @Duration, @TradeId 
  WHILE @@FETCH_STATUS = 0 BEGIN  
  
  INSERT INTO AS_HeatingAppointmentDuration ( AppointmentId, Duration,HeatingTypeId, TradeId)  
  VALUES ( @AppointmentId, @Duration,@HeatingTypeId, @TradeId) 
  
-- Fetch record for next loop iteration.  
FETCH NEXT FROM heatingApptDurationDetail INTO @HeatingTypeId, @Duration, @TradeId
END  
  
-- =====================================================  
-- Loop (End) through records and insert works required  
-- =====================================================  
  
CLOSE heatingApptDurationDetail  
DEALLOCATE heatingApptDurationDetail  
  
END TRY  
BEGIN CATCH   
 IF @@TRANCOUNT > 0  
 BEGIN       
  ROLLBACK TRANSACTION;     
  SET @isSaved = 0          
 END  
 DECLARE @ErrorMessage NVARCHAR(4000);  
 DECLARE @ErrorSeverity INT;  
 DECLARE @ErrorState INT;  
  
 SELECT @ErrorMessage = ERROR_MESSAGE(),  
 @ErrorSeverity = ERROR_SEVERITY(),  
 @ErrorState = ERROR_STATE();  
  
 -- Use RAISERROR inside the CATCH block to return   
 -- error information about the original error that   
 -- caused execution to jump to the CATCH block.  
 RAISERROR (@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState -- State.  
   );  
END CATCH;  
  
IF @@TRANCOUNT > 0  
 BEGIN    
  COMMIT TRANSACTION;    
  SET @isSaved = 1  
 END  
    
END  