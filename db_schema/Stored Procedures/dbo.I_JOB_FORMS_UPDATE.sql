SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_JOB_FORMS_UPDATE]
(
	@ApplicationId int,
	@Name nvarchar(20)
	--@FormData varbinary(MAX)
)
AS
	SET NOCOUNT OFF;
--UPDATE I_JOB_FORMS SET Name = @Name, FormData = @FormData WHERE ApplicationId = @ApplicationId
UPDATE I_JOB_FORMS SET Name = @Name WHERE ApplicationId = @ApplicationId
	

GO
