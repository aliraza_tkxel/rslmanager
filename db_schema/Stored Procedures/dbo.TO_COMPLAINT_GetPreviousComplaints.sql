SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.TO_COMPLAINT_GetPreviousComplaints
/* ===========================================================================
 '   NAME:           TO_COMPLAINT_GetPreviousComplaints
 '   DATE CREATED:   03 JULY 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Get the Previous complaints made by specified customerId from
 '   IN:             @CustomerID
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CustomerID INT
	)
	
AS

SELECT 
ENQ.CreationDate,
ENQ.Description,
C.DESCRIPTION AS Category
FROM TO_ENQUIRY_LOG ENQ 
INNER JOIN TO_COMPLAINT COM ON ENQ.EnquiryLogID=COM.EnquiryLogID
LEFT JOIN C_SERVICECOMPLAINT_CATEGORY C ON COM.CategoryID=C.CATEGORYID
WHERE ENQ.CustomerID=@CustomerID
ORDER BY ENQ.CreationDate DESC,COM.ComplaintID DESC

GO
