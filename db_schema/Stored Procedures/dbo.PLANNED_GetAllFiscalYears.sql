SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
-- EXEC PLANNED_GetAllFiscalYears
-- Author:  <Muhammad Awais>  
-- Create date: <14-Jan-2015>  
-- Description: <This Stored Proceedure shows the list of all fiscal years>  
-- =============================================  
CREATE PROCEDURE [dbo].[PLANNED_GetAllFiscalYears]
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
    -- Insert statements for procedure here  
   SELECT F_FISCALYEARS.YRange As YRange,
          F_FISCALYEARS.YStart As YearStartDate,
          F_FISCALYEARS.YEnd As YearEndDate,
          Year( F_FISCALYEARS.YStart ) As YearStart,
          Year( F_FISCALYEARS.YEnd ) As YearEnd,
          ( rtrim( Year( F_FISCALYEARS.YStart ) ) + ' - ' + rtrim( Year( F_FISCALYEARS.YEnd ) ) ) As FiscalYear
     FROM F_FISCALYEARS
 Order BY F_FISCALYEARS.YStart DESC,
          F_FISCALYEARS.YEnd DESC
 
END  
GO
