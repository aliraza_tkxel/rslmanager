USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetContractorEmailDetail]    Script Date: 13-Jul-17 11:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_GetContractorEmailDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetContractorEmailDetail AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetContractorEmailDetail]  
 (  
  @journalId  VARCHAR(100)
  ,@purchaseOrderId int  
 )  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @contractorId INT
Declare @BlockId INT	
Declare @propertyId NVARCHAR(20)   

select @contractorId=AW.ContactId, @propertyId=aj.PROPERTYID from AS_JOURNAL AJ
	inner join AS_CONTRACTOR_WORK AW on AW.JournalId=AJ.JOURNALID
	where AJ.JOURNALID=@journalId AND AW.PurchaseOrder=@purchaseOrderId

--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
	INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @contractorId

--=================================================
--Get Property Detail(s)
--=================================================
SELECT
	ISNULL(P.PROPERTYID,'') PROPERTYID,
	ISNULL(HOUSENUMBER,'') HOUSENUMBER,
	ISNULL(FLATNUMBER,'') FLATNUMBER,
	ISNULL(ADDRESS1,'') ADDRESS1,
	ISNULL(ADDRESS2,'') ADDRESS2,
	ISNULL(ADDRESS3,'') ADDRESS3,
	ISNULL(TOWNCITY,'') TOWNCITY,
	ISNULL(COUNTY,'') COUNTY,
	ISNULL(POSTCODE,'') POSTCODE,
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, '') + ISNULL(' ' + TOWNCITY, '')
	+ ISNULL(', ' + COUNTY, '') + ISNULL(', ' + POSTCODE, ''), ''), 'N/A') AS [FullAddress],
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress]
FROM AS_JOURNAL AJ
left join P__PROPERTY P on P.PROPERTYID=AJ.PROPERTYID
WHERE AJ.JOURNALID = @journalId


--=================================================
--Get Block Detail(s)
--=================================================
SELECT
	ISNULL(P.BLOCKID,0) as BlockId, ISNULL(P.BLOCKNAME,'') AS BlockName
FROM AS_JOURNAL AJ
left join P__PROPERTY PR on PR.PROPERTYID=AJ.PROPERTYID
LEFT JOIN P_BLOCK P on  P.BLOCKID=PR.BLOCKID 
WHERE AJ.JOURNALID = @journalId
 
--=================================================
--Get PO and Work Required Details
--=================================================

select fwd.ApplianceServicingWorkDetailId as WorkDetailId,
 fwd.WorkRequired as WorkRequired,
 fw.PurchaseOrder as POID,
 CONVERT(DECIMAL(10,2),FWD.NetCost) AS NetCost,
 CONVERT(DECIMAL(10,2),FWD.Vat) AS VAT,
 CONVERT(DECIMAL(10,2),FWD.Gross) AS Gross
from AS_JOURNAL fj
inner join AS_CONTRACTOR_WORK fw on fw.JournalId=fj.JournalID
inner join AS_CONTRACTOR_WORK_DETAIL fwd on fwd.ContractorId=fw.ApplianceServicingContractorId
where fj.JOURNALID=@journalId and fw.PurchaseOrder = @purchaseOrderId

END