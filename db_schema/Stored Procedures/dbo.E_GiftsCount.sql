USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_GiftsCount]    Script Date: 12/09/2018 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_GiftsCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_GiftsCount AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Saud Ahmed
-- Create date:      12/09/2018
-- Description:      Calculate alert count of pending approval of gifts on whiteboard  
-- exec  E_GiftsCount 78
-- =============================================

ALTER PROCEDURE  [dbo].[E_GiftsCount]
	@LineMgr INT 
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT PendingGifts as LeavesCount From (
SELECT count(*) AS PendingGifts
from E_GiftsAndHospitalities gifts
    join E__EMPLOYEE emp on gifts.EmployeeId = emp.EMPLOYEEID
    join E_JOBDETAILS jd on emp.EMPLOYEEID = jd.EMPLOYEEID
    join E__EMPLOYEE lm on jd.LINEMANAGER = lm.EMPLOYEEID
    left join E_Gift_Status gStatus on gifts.GiftStatusId = gStatus.GiftStatusId
	CROSS APPLY (SELECT TOP 1 YRange FROM F_FISCALYEARS ff ORDER BY ff.YRange DESC) AS yRange
WHERE gifts.fiscalYearId = yRange.YRange 
	AND gStatus.Description = 'Pending Approval' AND lm.EMPLOYEEID = @LineMgr
)  totalResult

END
