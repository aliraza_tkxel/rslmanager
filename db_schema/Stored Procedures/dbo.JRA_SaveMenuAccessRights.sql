SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_SaveMenuAccessRights @teamId=1,@jobRole = 'Administrator'
-- Author:		Ahmed Mehmood
-- Create date: <2/1/2014>
-- Last Modified: <2/1/2014>
-- Description:	<Save menus access rights. >
-- Web Page: JobRoles.aspx

-- =============================================
CREATE PROCEDURE [dbo].[JRA_SaveMenuAccessRights]
@teamJobRoleId int
,@menuId int
,@isSaved bit out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

--====================================================
--			AC_MENUS_ACCESS INSERTION
--====================================================

	
	IF NOT EXISTS (	SELECT	1
	FROM	AC_MENUS_ACCESS
	WHERE	AC_MENUS_ACCESS.MenuId = @menuId AND AC_MENUS_ACCESS.JobRoleTeamId = @teamJobRoleId)
	BEGIN
		
		INSERT INTO AC_MENUS_ACCESS
           ([MenuId]
           ,[JobRoleTeamId])
		VALUES
           (@menuId
           ,@teamJobRoleId)
           
	END
      

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END
GO
