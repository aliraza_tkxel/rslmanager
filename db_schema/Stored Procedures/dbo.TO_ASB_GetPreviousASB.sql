SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_ASB_GetPreviousASB
/* ===========================================================================
 '   NAME:           TO_ASB_GetPreviousASB
 '   DATE CREATED:   03 JULY 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Get the previous ASB request against the provided customerId from
 '      ASB record in TO_ASB table and TO_ENQUIRY_LOG
 '   IN:             @CustomerID
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
 @CustomerID INT
 )
 
AS

SELECT 
ENQ.CreationDate,
ENQ.Description,
S.DESCRIPTION AS STATUS,
C.DESCRIPTION AS Category

FROM TO_ENQUIRY_LOG ENQ 
INNER JOIN TO_ASB ASB ON ENQ.EnquiryLogID=ASB.EnquiryLogID
LEFT JOIN C_STATUS S ON ENQ.ItemStatusID=S.ITEMSTATUSID
LEFT JOIN C_ASB_CATEGORY C ON ASB.CategoryID=C.CATEGORYID
WHERE ENQ.CustomerID=@CustomerID
ORDER BY ASB.AsbID DESC


SET QUOTED_IDENTIFIER ON 
GO
