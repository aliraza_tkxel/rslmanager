SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_STATIONERY_ORDER_INSERT]
(
	@RequestedDate datetime,
	@Description nvarchar(50),	
	@RequestedByEmpId int,	
	@RequestedForEmpId int,
	@DeliveryLocation nvarchar(300),
	@StatusId int
)
AS
SET NOCOUNT OFF;
INSERT INTO I_STATIONERY_ORDER
                      (RequestedDate, Description, RequestedByEmpId, RequestedForEmpId, DeliveryLocation, StatusId )
VALUES     (@RequestedDate, @Description,  @RequestedByEmpId, @RequestedForEmpId,@DeliveryLocation, @StatusId)

GO
