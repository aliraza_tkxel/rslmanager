SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[TO_LOOKUP_VALUE_GetParkingLookUpValue]

	/* ===========================================================================
 '   NAME:           TO_LOOKUP_VALUE_GetParkingLookUpValue
 '   DATE CREATED:   09 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get garage/carparking lookup value TO_LOOKUP_VALUE table which will be shown
 '					 as lookup value in presentation page
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT LookUpValueID AS id,Name AS val
	FROM TO_LOOKUP_VALUE
	WHERE LookUpCodeID=9
	ORDER BY Name ASC


GO
