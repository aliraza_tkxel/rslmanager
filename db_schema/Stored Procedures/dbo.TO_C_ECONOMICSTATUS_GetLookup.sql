SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_C_ECONOMICSTATUS_GetLookup 
/* ===========================================================================
 '   NAME:           TO_C_ECONOMICSTATUS_GetLookup 
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get economic status records from C_ECONOMICSTATUS table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT ECONOMICSTATUSID AS id,DESCRIPTION AS val
	FROM C_ECONOMICSTATUS
	ORDER BY DESCRIPTION ASC

GO
