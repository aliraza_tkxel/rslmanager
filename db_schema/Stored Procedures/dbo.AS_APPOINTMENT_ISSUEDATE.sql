SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================

-- =============================================
CREATE PROCEDURE [dbo].[AS_APPOINTMENT_ISSUEDATE]
	-- Add the parameters for the stored procedure here
	@TenancyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @PROPERTYID NVARCHAR(12)
	
	SET @PROPERTYID = (SELECT PROPERTYID FROM dbo.C_TENANCY WHERE TENANCYID=@TenancyId)	

	SELECT J.JOURNALID,J.PROPERTYID,DATEADD(MM,12,ISSUEDATE) AS ISSUDATE,APPOINTMENTDATE FROM dbo.AS_JOURNAL J
	LEFT JOIN dbo.AS_APPOINTMENTS AP ON AP.JournalId = J.JOURNALID
	LEFT JOIN (SELECT propertyid,issuedate FROM p_lgsr) P ON P.PROPERTYID = J.PROPERTYID
	WHERE J.ISCURRENT=1 AND J.PROPERTYID =@PROPERTYID

END


--EXEC AS_APPOINTMENT_ISSUEDATE 976929
GO
