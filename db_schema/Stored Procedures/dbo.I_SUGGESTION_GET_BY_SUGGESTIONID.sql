SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[I_SUGGESTION_GET_BY_SUGGESTIONID]
@SUGGESTIONID INT
AS
	SET NOCOUNT ON;

SELECT E.FIRSTNAME + SPACE(1) + E.LASTNAME AS FULLNAME, T.TEAMNAME,
S.SUGGESTIONID, S.USERID, S.DATESUBMITTED, S.TOPIC, S.SUGGESTION, S.ACTION
	FROM I_SUGGESTIONS S
INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = S.USERID
INNER JOIN E_JOBDETAILS EJ ON E.EMPLOYEEID = EJ.EMPLOYEEID
INNER JOIN E_TEAM T ON  EJ.TEAM = T.TEAMID
	WHERE  SUGGESTIONID = @SUGGESTIONID
GO
