USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetItemNotesTrail]    Script Date: 10/10/2018 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_GetItemNotesTrail 1171
-- Create date: <10/11/2018>  
-- Description: <Get the property Notes trail against Notes Id>  
-- WebPage: PropertRecord.aspx => Attributes Tab  
-- =============================================  
IF OBJECT_ID('dbo.[AS_GetItemNotesTrail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetItemNotesTrail] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetItemNotesTrail] (  
@notesId int
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
	SELECT Notes,  CreatedOn As CreatedOn,
	(e.FIRSTNAME + ' ' + e.LASTNAME)  CreatedBy,
	NotesStatus as NotesStatus,
	ShowInScheduling,
	ShowOnApp
	FROM PA_PROPERTY_ITEM_NOTES_HISTORY
	LEFT JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy
	 WHERE SID = @notesId
END