SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Martin L
-- Create date: 1 Mar 2007
-- Description:	Rebuild Indexes
-- =============================================
CREATE PROCEDURE [dbo].[RSL_REBUILD_INDEXES_2] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DBCC DBREINDEX (DAYTODAY , ' ', 90);
	DBCC DBREINDEX (DOC_DOCUMENT , ' ', 90);
	DBCC DBREINDEX (DOC_DOCUMENT_HISTORY , ' ', 90);
	DBCC DBREINDEX (DOC_FOLDER , ' ', 90);
	DBCC DBREINDEX (dtproperties , ' ', 90);
	DBCC DBREINDEX (E__EMPLOYEE , ' ', 90);
	DBCC DBREINDEX (E_ABSENCE , ' ', 90);
	DBCC DBREINDEX (E_ABSENCE_backup , ' ', 90);
	DBCC DBREINDEX (E_ACTION , ' ', 30);
	DBCC DBREINDEX (E_BENEFITS , ' ', 60);
	DBCC DBREINDEX (E_BENTYPE , ' ', 30);
	DBCC DBREINDEX (E_CONTACT , ' ', 60);
	DBCC DBREINDEX (E_DETAILS , ' ', 60);
	DBCC DBREINDEX (E_DIFFDIS , ' ', 60);
	DBCC DBREINDEX (E_DIFFDISID , ' ', 60);
	DBCC DBREINDEX (E_EXPENSES , ' ', 90);
	DBCC DBREINDEX (E_EXPIRE_LOG , ' ', 90);
	DBCC DBREINDEX (E_GRADE , ' ', 30);
	DBCC DBREINDEX (E_GRADE_POINT , ' ', 30);
	DBCC DBREINDEX (E_HISTORICALHOLIDAYENTITLEMENT , ' ', 90);
	DBCC DBREINDEX (E_HOLIDAYENTITLMENT_SNAPSHOT_FOR2004 , ' ', 90);
	DBCC DBREINDEX (E_ITEM , ' ', 30);
	DBCC DBREINDEX (E_JOBDETAILS , ' ', 90);
	DBCC DBREINDEX (E_JOURNAL , ' ', 90);
	DBCC DBREINDEX (E_MEDICALORG , ' ', 30);
	DBCC DBREINDEX (E_NATURE , ' ', 30);
	DBCC DBREINDEX (E_NEXTOFKIN , ' ', 30);
	DBCC DBREINDEX (E_NOTES , ' ', 90);
	DBCC DBREINDEX (E_NOTICEPERIODS , ' ', 90);
	DBCC DBREINDEX (E_PARTFULLTIME , ' ', 90);
	DBCC DBREINDEX (E_PATCH , ' ', 30);
	DBCC DBREINDEX (E_PREVIOUSEMPLOYMENT , ' ', 60);
	DBCC DBREINDEX (E_PROBATIONPERIOD , ' ', 30);
	DBCC DBREINDEX (E_QUALIFICATIONSANDSKILLS , ' ', 30);
	DBCC DBREINDEX (E_QUALIFICATIONTYPE , ' ', 30);
	DBCC DBREINDEX (E_SHIFT , ' ', 30);
	DBCC DBREINDEX (E_SHIFTDETAIL , ' ', 90);
	DBCC DBREINDEX (E_STATUS , ' ', 30);
	DBCC DBREINDEX (E_TEAM , ' ', 60);
	DBCC DBREINDEX (F_APSCHEDULE , ' ', 90);
	DBCC DBREINDEX (F_BACSDATA , ' ', 90);
	DBCC DBREINDEX (F_BANK_GROUPING , ' ', 90);
	DBCC DBREINDEX (F_BANK_RECONCILIATION , ' ', 90);
	DBCC DBREINDEX (F_BANK_RECONCILIATION_OLD , ' ', 90);
	DBCC DBREINDEX (F_BANK_RECONCILIATION_OTHERACCOUNTS , ' ', 90);
	DBCC DBREINDEX (F_BANK_STATEMENTS , ' ', 90);
	DBCC DBREINDEX (F_BANK_STATEMENTS_OLD , ' ', 90);
	DBCC DBREINDEX (F_BANK_TOTALS , ' ', 90);
	DBCC DBREINDEX (F_BANKDETAILS , ' ', 90);
	DBCC DBREINDEX (F_BANKRECTYPE , ' ', 90);
	DBCC DBREINDEX (F_BUDGETRTRANSFER , ' ', 90);
	DBCC DBREINDEX (F_CASHPOSTING , ' ', 90);
	DBCC DBREINDEX (F_COSTCENTRE , ' ', 90);
	DBCC DBREINDEX (F_COSTCENTRE_ALLOCATION , ' ', 90);
	DBCC DBREINDEX (F_CREDITNOTE , ' ', 90);
	DBCC DBREINDEX (F_CREDITNOTE_TO_PURCHASEITEM , ' ', 90);
	DBCC DBREINDEX (F_CYCLE , ' ', 90);
	DBCC DBREINDEX (F_DDFILES , ' ', 90);
	DBCC DBREINDEX (F_DDHISTORY , ' ', 90);
	DBCC DBREINDEX (F_DDHISTORY_COPY , ' ', 90);
	DBCC DBREINDEX (F_DDSCHEDULE , ' ', 90);
	DBCC DBREINDEX (F_DDSCHEDULE_COPY , ' ', 90);
	DBCC DBREINDEX (F_DEVELOPMENT_HEAD_LINK , ' ', 90);
	DBCC DBREINDEX (F_DEVELOPMENT_RATES , ' ', 90);
	DBCC DBREINDEX (F_DEVELOPMENT_REPORT_GROUPING , ' ', 90);
	DBCC DBREINDEX (F_DEVELOPMENTCODES , ' ', 90);
	DBCC DBREINDEX (F_DEVLIMITS , ' ', 60);
	DBCC DBREINDEX (F_DIRECTPOSTITEMS , ' ', 30);
	DBCC DBREINDEX (F_DIRECTPOSTLINKING , ' ', 90);
	DBCC DBREINDEX (F_EMPLOYEELIMITS , ' ', 90);
	DBCC DBREINDEX (F_EXPENDITURE , ' ', 60);
	DBCC DBREINDEX (F_EXPENDITURE_ALLOCATION , ' ', 60);
	DBCC DBREINDEX (F_EXPENDITURE_VIREMENT , ' ', 60);
	DBCC DBREINDEX (F_FISCALYEARS , ' ', 30);
	DBCC DBREINDEX (F_HBACTUALSCHEDULE , ' ', 90);
	DBCC DBREINDEX (F_HBEND_LOG , ' ', 90);
	DBCC DBREINDEX (F_HBINFORMATION , ' ', 90);
	DBCC DBREINDEX (F_HBSCHEDULE , ' ', 90);
	DBCC DBREINDEX (F_HEAD , ' ', 90);
	DBCC DBREINDEX (F_HEAD_ALLOCATION , ' ', 90);
	DBCC DBREINDEX (F_INVOICE , ' ', 90);
	DBCC DBREINDEX (F_ITEMTYPE , ' ', 30);
	DBCC DBREINDEX (F_LABACS , ' ', 60);
	DBCC DBREINDEX (F_LADATA , ' ', 60);
	DBCC DBREINDEX (F_MISPOSTITEMS , ' ', 30 );
	DBCC DBREINDEX (F_ORDERITEM_TO_INVOICE , ' ', 90);
	DBCC DBREINDEX (F_PAYMENTCARDCODES , ' ', 30);
	DBCC DBREINDEX (F_PAYMENTCARDDATA , ' ', 90);
	DBCC DBREINDEX (F_PAYMENTCARDFILES , ' ', 90);
	DBCC DBREINDEX (F_PAYMENTSLIP , ' ', 90);
	DBCC DBREINDEX (F_PAYMENTTYPE , ' ', 30);
	DBCC DBREINDEX (F_PAYMENTTYPE_GROUPING , ' ', 90);
	DBCC DBREINDEX (F_PAYMENTTYPE_TO_PAYMENTTYPE , ' ', 90);
	DBCC DBREINDEX (F_PETTYCASHDEBIT , ' ', 90);
	DBCC DBREINDEX (F_POBACS , ' ', 90);
	DBCC DBREINDEX (F_POSTATUS , ' ', 30);
	DBCC DBREINDEX (F_POTYPE , ' ', 30);
	DBCC DBREINDEX (F_PURCHASEITEM , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEITEM_CHQ , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEITEM_HISTORY , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEITEM_LOG , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEITEM_TO_CHQ , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEORDER , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEORDER_LOG , ' ', 90);
	DBCC DBREINDEX (F_PURCHASEORDER_OLD , ' ', 90);
	DBCC DBREINDEX (F_RENTDAILYSNAPSHOT , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNAL , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNAL_ERRORS_MONTHLY , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNAL_IR , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNAL_MISPOSTED , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNAL_MONTHLY , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNAL_VOIDS , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNALAMEND , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNALDDSCHED , ' ', 90);
	DBCC DBREINDEX (F_RENTJOURNALSOSCHED , ' ', 90);
	DBCC DBREINDEX (F_RENTPERIOD , ' ', 30);
	DBCC DBREINDEX (F_SALEITEM_TO_PURCHASEITEM , ' ', 90);
	DBCC DBREINDEX (F_SALESCAT , ' ', 30);
	DBCC DBREINDEX (F_SALESCREDITNOTE , ' ', 60);
	DBCC DBREINDEX (F_SALESCREDITNOTEITEM , ' ', 60);
	DBCC DBREINDEX (F_SALESCUSTOMER , ' ', 60);
	DBCC DBREINDEX (F_SALESINVOICE , ' ', 60);
	DBCC DBREINDEX (F_SALESINVOICE_LOG , ' ', 60);
	DBCC DBREINDEX (F_SALESINVOICEITEM , ' ', 60);
	DBCC DBREINDEX (F_SALESINVOICEITEM_HISTORY , ' ', 60);
	DBCC DBREINDEX (F_SALESINVOICEITEM_LOG , ' ', 60);
	DBCC DBREINDEX (F_SALESPAYMENT , ' ', 60);
	DBCC DBREINDEX (F_SCHEDULE , ' ', 90);
	DBCC DBREINDEX (F_SDPTOTENANCY , ' ', 90);
	DBCC DBREINDEX (F_SO_HISTORY , ' ', 90);
	DBCC DBREINDEX (F_SOSCHEDULE , ' ', 90);
	DBCC DBREINDEX (F_SOSTATUS , ' ', 30);
	DBCC DBREINDEX (F_STANDINGORDERDATA , ' ', 90);
	DBCC DBREINDEX (F_STANDINGORDERFILES , ' ', 90);
	DBCC DBREINDEX (F_TENANTBACS , ' ', 90);
	DBCC DBREINDEX (F_TENANTBACSFILES , ' ', 90);
	DBCC DBREINDEX (F_TENANTREFUNDS , ' ', 90);
	DBCC DBREINDEX (F_TRANSACTIONSTATUS , ' ', 30);
	DBCC DBREINDEX (F_UNLETDAILYSNAPSHOT , ' ', 90);
	DBCC DBREINDEX (F_VAT , ' ', 30);
	DBCC DBREINDEX (F_WRITEOFF_TO_RENTJOURNAL , ' ', 90);
	DBCC DBREINDEX (FORMER , ' ', 90);
	DBCC DBREINDEX (G_BANKHOLIDAYS , ' ', 30);
	DBCC DBREINDEX (G_COMMUNICATION , ' ', 30);
	DBCC DBREINDEX (G_DISABILITY , ' ', 30);
	DBCC DBREINDEX (G_DOCUMENT , ' ', 30);
	DBCC DBREINDEX (G_DOCUMENTTYPE , ' ', 30);
	DBCC DBREINDEX (G_ERRORMESSAGES , ' ', 30);
	DBCC DBREINDEX (G_ETHNICITY , ' ', 30);
	DBCC DBREINDEX (G_ETHNICITY2 , ' ', 30);
	DBCC DBREINDEX (G_GRANT , ' ', 30);
	DBCC DBREINDEX (G_LEGALSCRATCHPAD , ' ', 30);
	DBCC DBREINDEX (G_LOCALAUTHORITY , ' ', 30);
	DBCC DBREINDEX (G_MARITALSTATUS , ' ', 30);
	DBCC DBREINDEX (G_MESSAGEHISTORY , ' ', 30);
	DBCC DBREINDEX (G_MESSAGEJOURNAL , ' ', 30);
	DBCC DBREINDEX (G_MESSAGES , ' ', 30);
	DBCC DBREINDEX (G_MONTH , ' ', 30);
	DBCC DBREINDEX (G_NEWS , ' ', 30);
	DBCC DBREINDEX (G_OFFICE , ' ', 30);
	DBCC DBREINDEX (G_OTHERCOMMUNICATION , ' ', 30);
	DBCC DBREINDEX (G_PERFORMANCEINDICATOR , ' ', 30);
	DBCC DBREINDEX (G_PREFEREDCONTACT , ' ', 30);
	DBCC DBREINDEX (G_RELIGION , ' ', 90);
	DBCC DBREINDEX (G_SCHEME , ' ', 30);
	DBCC DBREINDEX (G_SCRATCHPAD , ' ', 30);
	DBCC DBREINDEX (G_SEXUALORIENTATION , ' ', 30);
	DBCC DBREINDEX (G_STANDARDPERIODS , ' ', 30);
	DBCC DBREINDEX (G_TASKS , ' ', 30);
	DBCC DBREINDEX (G_TEAMCODES , ' ', 30);
	DBCC DBREINDEX (G_TIMING , ' ', 30);
	DBCC DBREINDEX (G_TITLE , ' ', 30);
	DBCC DBREINDEX (KPI_CATEGORY , ' ', 30);
	DBCC DBREINDEX (KPI_STATUSTRACKER , ' ', 90);
	DBCC DBREINDEX (KPI_TOPIC , ' ', 90);
	DBCC DBREINDEX (L_CYCLICALLOG , ' ', 90);
	DBCC DBREINDEX (L_DD_LOGFILE , ' ', 90);
	DBCC DBREINDEX (L_DD_MASTERLOG , ' ', 90);
	DBCC DBREINDEX (L_ERRORS , ' ', 90);
	DBCC DBREINDEX (L_HB_MASTERLOG , ' ', 90);
	DBCC DBREINDEX (L_SO_LOGFILE , ' ', 90);
	DBCC DBREINDEX (L_SO_MASTERLOG , ' ', 90);
	DBCC DBREINDEX (LOGIN_HISTORY , ' ', 90);
	DBCC DBREINDEX (LOGIN_HOURS , ' ', 90);
	DBCC DBREINDEX (MAJOR , ' ', 90);
	DBCC DBREINDEX (MPI_VOIDRANGE , ' ', 90);
	


END
GO
