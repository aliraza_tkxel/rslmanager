SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_TBL_ITEM_GETLOOKUP]
/* ===========================================================================
 '   NAME:          FL_TBL_ITEM_GETLOOKUP
 '   DATE CREATED:   16 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Location FL_AREA table which will be shown as lookup value in presentation pages
 '   IN:            @AreaID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@AreaID as Int
AS
	SELECT ItemID AS id, Name AS val
	FROM TBL_PDA_ITEM
	WHERE AreaID=@AreaID
	ORDER BY Name ASC



GO
