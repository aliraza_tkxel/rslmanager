USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_GetPayPointReport]    Script Date: 28/02/2019 7:00:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.E_GetPayPointReport') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_GetPayPointReport AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_GetPayPointReport] 
		@searchText NVARCHAR(500) = '',
		@status INT = 0,
		@teamId INT = null,
		@directorate INT = NULL,
		@fiscalyear INT = 0,
		@startDateRange DATETIME = NULL,
		@endDateRange DATETIME = NULL,
		@notSubmitted bit = 0
AS
BEGIN
	
	if(@notSubmitted = 1)
	begin
		SELECT DISTINCT
			CASE WHEN T.TEAMNAME IS NULL THEN 'No Team' ELSE T.TEAMNAME END AS team,
			CASE WHEN D.DIRECTORATENAME IS NULL THEN 'No Directorate' ELSE D.DIRECTORATENAME END AS directorate,
			CASE WHEN g.DESCRIPTION IS NULL THEN ge.DESCRIPTION ELSE g.DESCRIPTION END AS gradeDescription,
			CASE WHEN gp.POINTDESCRIPTION IS NULL THEN gpe.POINTDESCRIPTION ELSE gp.POINTDESCRIPTION END AS gradePointDescription,
			e.FIRSTNAME + ' ' + e.LASTNAME AS name,
			lm.FIRSTNAME + ' ' + lm.LASTNAME AS submittedByName,
			CASE WHEN j.PayPointReviewDate IS NULL THEN '-' ELSE CONVERT(VARCHAR(10), j.PayPointReviewDate, 103) END AS reviewDate,
			j.PayPointReviewDate AS paypointReviewDate,
			CASE WHEN s.Description = 'Submitted' THEN 'Submitted by Manager' 
					WHEN s.Description = 'Supported' THEN 'Supported by Director' 
					WHEN s.Description = 'Authorised' THEN 'Authorised by Exec' 
					WHEN s.Description = 'Declined' AND p.SupportedBy IS NOT NULL AND p.AuthorizedBy IS NULL THEN 'Declined by Director' 
					WHEN s.Description = 'Declined' AND p.AuthorizedBy IS NOT NULL THEN 'Declined by Exec' 
					ELSE s.Description
				END AS statusDisplay,
			s.Description AS status,
			e.EMPLOYEEID AS employeeId,
			p.PaypointId as payPointId,
			p.SupportedBy AS supportedBy,
			p.AuthorizedBy AS authorizedBy,
			p.Reason AS reason,
			e.IMAGEPATH As imagePath
		from E_PaypointSubmission p 
			join E__EMPLOYEE e on p.employeeId = e.EMPLOYEEID
			join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
			join E_GRADE ge on j.GRADE = ge.GRADEID
			join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
			left join E_GRADE g on p.Grade = g.GRADEID
			left join E_GRADE_POINT gp on p.GradePoint = gp.POINTID
			left join E__EMPLOYEE lm on p.ApprovedBy = lm.EMPLOYEEID
			left join E_TEAM T on j.TEAM = T.TEAMID
			left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
			left join E_PayPointStatus s on p.paypointStatusId = s.PayPointStatusId
		where 
			(e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
			AND (s.PayPointStatusId = @status OR @status = 0) 
			AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
			AND (j.TEAM = @teamId OR @teamId IS null) 
			AND (p.IsNotSubmitted = @notSubmitted OR @notSubmitted = 0) 
			AND j.PayPointReviewDate IS not null 
			AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0)
	end
	ELSE IF (@status = 5)
	BEGIN
	   SELECT DISTINCT
			CASE WHEN T.TEAMNAME IS NULL THEN 'No Team' ELSE T.TEAMNAME END AS team,
			CASE WHEN D.DIRECTORATENAME IS NULL THEN 'No Directorate' ELSE D.DIRECTORATENAME END AS directorate,
			CASE WHEN g.DESCRIPTION IS NULL THEN ge.DESCRIPTION ELSE g.DESCRIPTION END AS gradeDescription,
			CASE WHEN gp.POINTDESCRIPTION IS NULL THEN gpe.POINTDESCRIPTION ELSE gp.POINTDESCRIPTION END AS gradePointDescription,
			e.FIRSTNAME + ' ' + e.LASTNAME AS name,
			lm.FIRSTNAME + ' ' + lm.LASTNAME AS submittedByName,
			CASE WHEN j.PayPointReviewDate IS NULL THEN '-' ELSE CONVERT(VARCHAR(10), j.PayPointReviewDate, 103) END AS reviewDate,
			j.PayPointReviewDate AS paypointReviewDate,
			CASE WHEN s.Description = 'Submitted' THEN 'Submitted by Manager' 
					WHEN s.Description = 'Supported' THEN 'Supported by Director' 
					WHEN s.Description = 'Authorised' THEN 'Authorised by Exec' 
					WHEN s.Description = 'Declined' AND p.SupportedBy IS NOT NULL AND p.AuthorizedBy IS NULL THEN 'Declined by Director' 
					WHEN s.Description = 'Declined' AND p.AuthorizedBy IS NOT NULL THEN 'Declined by Exec' 
					ELSE s.Description
				END AS statusDisplay,
			s.Description AS status,
			e.EMPLOYEEID AS employeeId,
			p.PaypointId as payPointId,
			p.SupportedBy AS supportedBy,
			p.AuthorizedBy AS authorizedBy,
			p.Reason AS reason,
			e.IMAGEPATH As imagePath
		from E_PaypointSubmission p 
			join E__EMPLOYEE e on p.employeeId = e.EMPLOYEEID
			join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
			join E_GRADE ge on j.GRADE = ge.GRADEID
			join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
			left join E_GRADE g on p.Grade = g.GRADEID
			left join E_GRADE_POINT gp on p.GradePoint = gp.POINTID
			left join E__EMPLOYEE lm on p.ApprovedBy = lm.EMPLOYEEID
			left join E_TEAM T on j.TEAM = T.TEAMID
			left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
			left join E_PayPointStatus s on p.paypointStatusId = s.PayPointStatusId
		where 
			(e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
			AND (s.PayPointStatusId = @status OR @status = 0) 
			AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
			AND (j.TEAM = @teamId OR @teamId IS null) 
			AND (p.IsNotSubmitted = @notSubmitted OR @notSubmitted = 0) 
			AND j.PayPointReviewDate IS not null 
			AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0)
		UNION ALL
		SELECT  DISTINCT
			CASE WHEN T.TEAMNAME IS NULL THEN 'No Team' ELSE T.TEAMNAME END AS team,
			CASE WHEN D.DIRECTORATENAME IS NULL THEN 'No Directorate' ELSE D.DIRECTORATENAME END AS directorate,
			ge.DESCRIPTION AS gradeDescription,
			gpe.POINTDESCRIPTION AS gradePointDescription,
			e.FIRSTNAME + ' ' + e.LASTNAME AS name,
			'' AS submittedByName,
			CASE WHEN j.PayPointReviewDate IS NULL THEN '' ELSE CONVERT(VARCHAR(10), j.PayPointReviewDate, 103) END AS reviewDate,
			j.PayPointReviewDate AS paypointReviewDate,
			'Waiting Submission' AS statusDisplay,
			'Waiting Submission' AS status,
			e.EMPLOYEEID AS employeeId,
			0 as payPointId,
			0 AS SupportedBy,
			0 AS AuthorizedBy,
			'' AS reason,
			e.IMAGEPATH As imagePath
		from E__EMPLOYEE e 
			join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
			left join E_GRADE ge on j.GRADE = ge.GRADEID
			left join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
			left join E_TEAM T on j.TEAM = T.TEAMID
			left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
			join E__EMPLOYEE lm on j.LINEMANAGER = lm.employeeId
		where 
			j.ACTIVE = 1
			AND (e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
			AND (@status = 5 OR @status = 0) 
			AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
			AND (j.TEAM = @teamId OR @teamId IS null) 
			AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0)
			AND e.EMPLOYEEID NOT IN 
				(select e.EMPLOYEEID from E_PaypointSubmission p 
					join E__EMPLOYEE e on p.employeeId = e.EMPLOYEEID
					join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
					join E_GRADE ge on j.GRADE = ge.GRADEID
					join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
					left join E_GRADE g on p.Grade = g.GRADEID
					left join E_GRADE_POINT gp on p.GradePoint = gp.POINTID
					left join E__EMPLOYEE lm on p.ApprovedBy = lm.EMPLOYEEID
					left join E_TEAM T on j.TEAM = T.TEAMID
					left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
					left join E_PayPointStatus s on p.paypointStatusId = s.PayPointStatusId
				where 
					(e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
					AND (s.PayPointStatusId <= @status) 
					AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
					AND (j.TEAM = @teamId OR @teamId IS null) 
					AND j.PayPointReviewDate IS not null 
					AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0))
 		ORDER by j.PayPointReviewDate 
	END
	else
	begin
		SELECT DISTINCT
			CASE WHEN T.TEAMNAME IS NULL THEN 'No Team' ELSE T.TEAMNAME END AS team,
			CASE WHEN D.DIRECTORATENAME IS NULL THEN 'No Directorate' ELSE D.DIRECTORATENAME END AS directorate,
			CASE WHEN g.DESCRIPTION IS NULL THEN ge.DESCRIPTION ELSE g.DESCRIPTION END AS gradeDescription,
			CASE WHEN gp.POINTDESCRIPTION IS NULL THEN gpe.POINTDESCRIPTION ELSE gp.POINTDESCRIPTION END AS gradePointDescription,
			e.FIRSTNAME + ' ' + e.LASTNAME AS name,
			lm.FIRSTNAME + ' ' + lm.LASTNAME AS submittedByName,
			CASE WHEN j.PayPointReviewDate IS NULL THEN '-' ELSE CONVERT(VARCHAR(10), j.PayPointReviewDate, 103) END AS reviewDate,
			j.PayPointReviewDate AS paypointReviewDate,
			CASE WHEN s.Description = 'Submitted' THEN 'Submitted by Manager' 
					WHEN s.Description = 'Supported' THEN 'Supported by Director' 
					WHEN s.Description = 'Authorised' THEN 'Authorised by Exec' 
					WHEN s.Description = 'Declined' AND p.SupportedBy IS NOT NULL AND p.AuthorizedBy IS NULL THEN 'Declined by Director' 
					WHEN s.Description = 'Declined' AND p.AuthorizedBy IS NOT NULL THEN 'Declined by Exec' 
					ELSE s.Description
				END AS statusDisplay,
			s.Description AS status,
			e.EMPLOYEEID AS employeeId,
			p.PaypointId as payPointId,
			p.SupportedBy AS supportedBy,
			p.AuthorizedBy AS authorizedBy,
			p.Reason AS reason,
			e.IMAGEPATH As imagePath
		from E_PaypointSubmission p 
			join E__EMPLOYEE e on p.employeeId = e.EMPLOYEEID
			join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
			join E_GRADE ge on j.GRADE = ge.GRADEID
			join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
			left join E_GRADE g on p.Grade = g.GRADEID
			left join E_GRADE_POINT gp on p.GradePoint = gp.POINTID
			left join E__EMPLOYEE lm on p.ApprovedBy = lm.EMPLOYEEID
			left join E_TEAM T on j.TEAM = T.TEAMID
			left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
			left join E_PayPointStatus s on p.paypointStatusId = s.PayPointStatusId
		where 
			(e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
			AND (s.PayPointStatusId = @status OR @status = 0) 
			AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
			AND (j.TEAM = @teamId OR @teamId IS null) 
			AND (p.IsNotSubmitted = @notSubmitted OR @notSubmitted = 0) 
			AND j.PayPointReviewDate IS not null 
			AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0)
		UNION ALL
		SELECT  DISTINCT
			CASE WHEN T.TEAMNAME IS NULL THEN 'No Team' ELSE T.TEAMNAME END AS team,
			CASE WHEN D.DIRECTORATENAME IS NULL THEN 'No Directorate' ELSE D.DIRECTORATENAME END AS directorate,
			ge.DESCRIPTION AS gradeDescription,
			gpe.POINTDESCRIPTION AS gradePointDescription,
			e.FIRSTNAME + ' ' + e.LASTNAME AS name,
			'' AS submittedByName,
			CASE WHEN j.PayPointReviewDate IS NULL THEN '' ELSE CONVERT(VARCHAR(10), j.PayPointReviewDate, 103) END AS reviewDate,
			j.PayPointReviewDate AS paypointReviewDate,
			'Waiting Submission' AS statusDisplay,
			'Waiting Submission' AS status,
			e.EMPLOYEEID AS employeeId,
			0 as payPointId,
			0 AS SupportedBy,
			0 AS AuthorizedBy,
			'' AS reason,
			e.IMAGEPATH As imagePath
		from E__EMPLOYEE e 
			join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
			left join E_GRADE ge on j.GRADE = ge.GRADEID
			left join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
			left join E_TEAM T on j.TEAM = T.TEAMID
			left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
			join E__EMPLOYEE lm on j.LINEMANAGER = lm.employeeId
		where 
			j.ACTIVE = 1
			AND (e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
			AND (@status = 5 OR @status = 0) 
			AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
			AND (j.TEAM = @teamId OR @teamId IS null) 
			AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0)
			AND e.EMPLOYEEID NOT IN 
				(select e.EMPLOYEEID from E_PaypointSubmission p 
					join E__EMPLOYEE e on p.employeeId = e.EMPLOYEEID
					join E_JOBDETAILS j on e.EMPLOYEEID = j.EMPLOYEEID
					join E_GRADE ge on j.GRADE = ge.GRADEID
					join E_GRADE_POINT gpe on j.GRADEPOINT = gpe.POINTID
					left join E_GRADE g on p.Grade = g.GRADEID
					left join E_GRADE_POINT gp on p.GradePoint = gp.POINTID
					left join E__EMPLOYEE lm on p.ApprovedBy = lm.EMPLOYEEID
					left join E_TEAM T on j.TEAM = T.TEAMID
					left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
					left join E_PayPointStatus s on p.paypointStatusId = s.PayPointStatusId
				where 
					(e.FIRSTNAME + ' ' + e.LASTNAME) LIKE '%' +@searchText+'%' 
					AND (s.PayPointStatusId = @status OR @status = 0) 
					AND (T.DIRECTORATEID = @directorate OR @directorate IS null)  
					AND (j.TEAM = @teamId OR @teamId IS null) 
					AND j.PayPointReviewDate IS not null 
					AND ((j.PayPointReviewDate >= @startDateRange and j.PayPointReviewDate <= @endDateRange)  OR @fiscalyear = 0))
		ORDER by j.PayPointReviewDate 
	end
END