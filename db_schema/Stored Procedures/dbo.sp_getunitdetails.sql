SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  procedure [dbo].[sp_getunitdetails]
@unitid int 
as
SELECT 'Status' = 
CASE WHEN pending = 0 and credit <> 0 THEN 'Pending Unit Request' 
WHEN pending = 1 and credit <> 0 THEN 'Authorized Unit Request' 
WHEN debit <> 0 THEN 'Completed Request' END
,* from H_SUPPORT_UNITS WHERE row_id =@unitid 


GO
