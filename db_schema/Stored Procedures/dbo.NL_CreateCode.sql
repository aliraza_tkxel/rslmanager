USE [RSLBHALive]
GO
IF OBJECT_ID('dbo.NL_CreateCode') IS NULL 
	EXEC('CREATE PROCEDURE dbo.NL_CreateCode AS SET NOCOUNT ON;') 
GO

alter  PROCEDURE [dbo].[NL_CreateCode]
( @Name NVARCHAR(100),
           @AccountNumber NVARCHAR(10),
           @accounttype INT,
           @parentid INT,
           @taxcode int  ,       
		    @desc NVARCHAR(100),
           @isBankable INT,
		   @CompanyBankDefault NVARCHAR(10),
		   @CompanyPurchaseControlDefault NVARCHAR(10),
		   @AllocateExpenditures BIT,
           @Company NVARCHAR(10) )
as
BEGIN
	
	IF NOT EXISTS ( SELECT * FROM  dbo.NL_ACCOUNT WHERE ACCOUNTNUMBER = @AccountNumber)
	 BEGIN

		DECLARE @level INT
		DECLARE @AccountId INT
		DECLARE @parentName NVARCHAR(100)

		SET @AccountId = (SELECT MAX(accountid) + 1 FROM dbo.NL_ACCOUNT)
		SET @parentName  = (SELECT FULLNAME FROM dbo.NL_ACCOUNT WHERE ACCOUNTID = @parentid)
		SET @level = ISNULL((SELECT SUBLEVEL FROM dbo.NL_ACCOUNT WHERE ACCOUNTID = @parentid),-1) + 1


		INSERT INTO dbo.NL_ACCOUNT
		(
		    ACCOUNTID,
		    TIMECREATED,
		    TIMEMODIFIED,
		    NAME,
		    FULLNAME,
		    ISACTIVE,
		    PARENTREFLISTID,
		    PARENTREFFULLNAME,
		    SUBLEVEL,
		    ACCOUNTTYPE,
		    ACCOUNTNUMBER,
		    [DESC],
		    TAXCODE,
		    ISBANKABLE,
			AllocateExpenditures
		)
		VALUES
		(   @AccountId,         -- ACCOUNTID - int
		    GETDATE(), -- TIMECREATED - datetime
		    GETDATE(), -- TIMEMODIFIED - datetime
		    @Name,       -- NAME - nvarchar(100)
		    @Name,       -- FULLNAME - nvarchar(250)
		    1,      -- ISACTIVE - bit
		    @parentid,         -- PARENTREFLISTID - int
		    @parentName,       -- PARENTREFFULLNAME - nvarchar(150)
		    @level,         -- SUBLEVEL - int
		    @accounttype,         -- ACCOUNTTYPE - int
		    @AccountNumber,       -- ACCOUNTNUMBER - nvarchar(12)
		    @Name,       -- DESC - nvarchar(50)
		    @taxcode,         -- TAXCODE - int
		    @isBankable ,        -- ISBANKABLE - char(10)
			@AllocateExpenditures
		)

		while len(@Company) > 0
		begin
		  --print left(@S, charindex(',', @S+',')-1) 
		  INSERT INTO dbo.NL_ACCOUNT_TO_COMPANY ( ACCOUNTID, COMPANYID)
		  VALUES (   @AccountId,  left(@Company, charindex(',', @Company+',')-1)   )

		
		  set @Company = stuff(@Company, 1, charindex(',', @Company+','), '')
		END
        
		-- default bank account - 1 per company
		while len(@CompanyBankDefault) > 0
		BEGIN
        
		  DELETE FROM NL_ACCOUNT_TO_COMPANY_BANK_DEFAULT WHERE COMPANYID = left(@CompanyBankDefault, charindex(',', @CompanyBankDefault+',')-1)
		  INSERT INTO dbo.NL_ACCOUNT_TO_COMPANY_BANK_DEFAULT ( ACCOUNTID, COMPANYID , createdDate)
		  VALUES (   @AccountId,  left(@CompanyBankDefault, charindex(',', @CompanyBankDefault+',')-1), GETDATE())

		  UPDATE dbo.RSL_DEFAULTS SET DEFAULTVALUE = @AccountNumber WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTNUMBER' + @CompanyBankDefault

		  set @CompanyBankDefault = stuff(@CompanyBankDefault, 1, charindex(',', @CompanyBankDefault+','), '')
		end

		-- default purchase control account - 1 per company
		while len(@CompanyPurchaseControlDefault) > 0
		BEGIN
        
		  DELETE FROM NL_ACCOUNT_TO_COMPANY_PURCHASECONTROL_DEFAULT WHERE COMPANYID = left(@CompanyPurchaseControlDefault, charindex(',', @CompanyPurchaseControlDefault+',')-1)
		  --print left(@S, charindex(',', @S+',')-1) 
		  INSERT INTO dbo.NL_ACCOUNT_TO_COMPANY_PURCHASECONTROL_DEFAULT ( ACCOUNTID, COMPANYID , createdDate)
		  VALUES (   @AccountId,  left(@CompanyPurchaseControlDefault, charindex(',', @CompanyPurchaseControlDefault+',')-1), GETDATE())

		  UPDATE dbo.RSL_DEFAULTS SET DEFAULTVALUE = @AccountNumber WHERE DEFAULTNAME = 'PURCHASECONTROLACCOUNTNUMBER' + @CompanyPurchaseControlDefault

		  set @CompanyPurchaseControlDefault = stuff(@CompanyPurchaseControlDefault, 1, charindex(',', @CompanyPurchaseControlDefault+','), '')
		end
		

	 END

	 RETURN @AccountId     





	         

END

GO

