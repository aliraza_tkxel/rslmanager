SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umair Naeem
-- Create date: 08 JULY 2008
-- Updated date : NULL
-- Updated By : NULL
-- Description:	Created to delete a customer list 
--				against a programme
-- =============================================
-- EXEC [TC_CUSTOMERLIST_DELETION] @PROGRAMMEID=3

CREATE PROCEDURE [dbo].[TC_CUSTOMERLIST_DELETION] (
	-- Add the parameters for the stored procedure here
	@PROGRAMMEID INT
)
AS
BEGIN 
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
		
	DELETE FROM TC_CUSTOMER_LIST 
	WHERE CUSTOMERLISTID  IN (
								SELECT TCL.CUSTOMERLISTID 
								FROM TC_CUSTOMER_LIST TCL
								INNER JOIN (
											SELECT MAX(CUSTOMERLISTHISTORYID) AS MAXHISTORYID,CUSTOMERLISTID 
											FROM TC_CUSTOMER_LIST_JOURNAL 
											WHERE CUSTOMERLISTSTATUSID=7
											GROUP BY CUSTOMERLISTID
											) MAXSTATUS ON MAXSTATUS.CUSTOMERLISTID=TCL.CUSTOMERLISTID
								)
	AND PROGRAMMEID=@PROGRAMMEID 	
	
	SET NOCOUNT OFF;
END 
GO
