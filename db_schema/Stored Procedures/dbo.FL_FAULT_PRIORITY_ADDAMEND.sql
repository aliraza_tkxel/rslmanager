SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE     PROC [dbo].[FL_FAULT_PRIORITY_ADDAMEND](
/* ===========================================================================
 '   NAME:         FL_FAULT_PRIORITY_ADDAMEND
 '   DATE CREATED:  31 OCTOBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   ADD OR AMEND A PRIORITY IN FL_FAULT_PRIORITY TABLES
 '
 '   IN:	   @PRIORITYID INT ,
 '   IN:	   @PRIORITYNAME VARCHAR ,
 '   IN:           @RESPONSETIME INT ,
 '   IN:           @STATUS BIT,
 '   IN:	   @DAYS BIT,
 '   IN:	   @HRS BIT 
 '
 '   OUT:           @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@PRIORITYID INT,
@PRIORITYNAME NVARCHAR(50),
@RESPONSETIME INT ,
@STATUS BIT,
@DAYS BIT,
@HRS BIT,
@RESULT INT OUTPUT

)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN

IF EXISTS(SELECT * FROM FL_FAULT_PRIORITY WHERE PRIORITYID=@PRIORITYID)
UPDATE    FL_FAULT_PRIORITY
SET              PriorityName =@PRIORITYNAME, ResponseTime =@RESPONSETIME, Status =@STATUS, Days =@DAYS, Hrs =@HRS
WHERE PRIORITYID=@PRIORITYID

ELSE


INSERT INTO FL_FAULT_PRIORITY
                      (PriorityName, ResponseTime, Status, Days, Hrs)
VALUES     (@PRIORITYNAME,@RESPONSETIME,@STATUS,@DAYS,@HRS)


-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	
SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN













GO
