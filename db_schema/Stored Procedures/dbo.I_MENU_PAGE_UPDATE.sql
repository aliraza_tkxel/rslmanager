SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_MENU_PAGE_UPDATE]
(
	@MenuID int,
	@PageID int,
	@Rank int,
	@Original_MultiMenuID int,
	@MultiMenuID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_MENU_PAGE] SET [MenuID] = @MenuID, [PageID] = @PageID, [Rank] = @Rank WHERE (([MultiMenuID] = @Original_MultiMenuID));
	
SELECT MultiMenuID, MenuID, PageID, Rank FROM I_MENU_PAGE WHERE (MultiMenuID = @MultiMenuID)
GO
