
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================    
-- Exec AS_SavePropertyCurrentRentInformation    
    --@rentDateSet = 4/30/2014,    
    --@rentEffectiveSet = 5/30/2014,    
    --@rentType =2,    
 --@rent =145.00,    
 --@services=12.00,    
 --@councilTax=11.00,    
 --@water=10.00,    
 --@inetlig=10.00,    
 --@water=10.00,    
 --@inetlig=10.00,    
 --@supp=10.00,    
 --@garage=10.00,    
 --@totalRent=10.00,    
 --@propertyId='BHA0000020',    
 --@userId=605    
-- Author:  <Aqib Javed>    
-- Create date: <25-4-2014>    
-- Description: <This store procedure will save the information of Current Rent>    
-- Web Page: CurrentRentTab.ascx    
-- =============================================    
CREATE PROCEDURE [dbo].[AS_SavePropertyCurrentRentInformation]    
 @rentDateSet date,    
 @rentEffectiveSet date,    
 @rentType int,    
 @rent DECIMAL(9,2),
 @services DECIMAL(9,2),    
 @councilTax DECIMAL(9,2),    
 @water DECIMAL(9,2),    
 @inetlig DECIMAL(9,2),    
 @supp DECIMAL(9,2),    
 @garage DECIMAL(9,2),    
 @totalRent DECIMAL(9,2),    
 @propertyId VARCHAR(100),    
 @userId INT    
AS    
BEGIN    
DECLARE @isRecordExist INT    
SELECT @isRecordExist =COUNT(PROPERTYID) FROM P_Financial WHERE PROPERTYID =@propertyId    
IF (@isRecordExist<=0)    
BEGIN    
     INSERT INTO P_FINANCIAL(DATERENTSET,RENTEFFECTIVE,RENTTYPE,RENT,[SERVICES],COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,PROPERTYID,PFUSERID)    
     VALUES(@rentDateSet,@rentEffectiveSet,@rentType,@rent,@services,@councilTax,@water,@inetlig,@supp,@garage,@totalRent,@propertyId,@userId)         
END                
ELSE    
 BEGIN    
     UPDATE P_FINANCIAL SET     
            DATERENTSET=@rentDateSet,    
            RENTEFFECTIVE=@rentEffectiveSet,    
            RENTTYPE=@rentType,    
            RENT=@rent,    
   [SERVICES]=@services,    
   COUNCILTAX=@councilTax,    
   WATERRATES=@water,    
   INELIGSERV=@inetlig,    
   SUPPORTEDSERVICES=@supp,    
   GARAGE=@garage,    
   TOTALRENT=@totalRent,    
      PFUSERID=@userId    
   WHERE PROPERTYID=@propertyId    
END      

 INSERT INTO P_FINANCIAL_HISTORY(DATERENTSET,RENTEFFECTIVE,RENTTYPE,RENT,[SERVICES],COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,PROPERTYID,PFUSERID,PFTIMESTAMP)    
     VALUES(@rentDateSet,@rentEffectiveSet,@rentType,@rent,@services,@councilTax,@water,@inetlig,@supp,@garage,@totalRent,@propertyId,@userId,GETDATE())    
          
END 
GO
