
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 10/10/2012
-- Description:	<This SP would save a letter into the databse>
-- =============================================
CREATE PROCEDURE [dbo].[AS_AddSavedLetter] 
	-- Add the parameters for the stored procedure here
	@JournalHistoryId	int,
	@LetterBody			varchar(MAX),
	@LetterTitle		varchar(MAX),
	@LetterId			int,
	@TeamId				int,
	@FromResourceId		int,
	@SignOffLookupCode	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [RSLBHALive].[dbo].[AS_SAVEDLETTERS]
           ([JOURNALHISTORYID]
           ,[LETTERCONTENT]
           ,[LETTERID]
           ,[LETTERTITLE]
           ,[TEAMID]
           ,[FromResourceId]
           ,[SignOffLookupCode])
    VALUES
           (@JournalHistoryId
           ,@LetterBody
           ,@LetterId
           ,@LetterTitle
           ,@TeamId
           ,@FromResourceId
           ,@SignOffLookupCode)
END
GO
