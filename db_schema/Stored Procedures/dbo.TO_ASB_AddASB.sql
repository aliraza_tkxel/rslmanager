SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_ASB_AddASB
/* ===========================================================================
 '   NAME:           TO_ASB_AddASB
 '   DATE CREATED:   05 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add ASB(AntiSocialBehaviour) record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 ASB record in TO_ASB table
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '   IN:             @CategoryId
 '
 '   OUT:            @AsbID
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(4000),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	
	@CategoryID INT,
	@AsbID INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_ASB(
		CategoryID,
		EnquiryLogID
		)
		VALUES(
		@CategoryID,
		@EnquiryLogID
		)
		
		SELECT @AsbID=AsbID
	FROM TO_ASB
	WHERE AsbID = @@IDENTITY

				IF @AsbID<0
					BEGIN
						SET @AsbID=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @AsbID=-1
			ROLLBACK TRAN
			END

GO
