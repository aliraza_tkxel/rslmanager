SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_ENVIROTIPS_UPDATE]
(
	@TipsTitle varchar(100),
	@TipsDescription varchar(2000),
	@Original_TipsID int,
	@TipsId int
)
AS
	SET NOCOUNT OFF;
UPDATE [ENVIRO_TIPS] SET [TipsTitle] = @TipsTitle, [TipsDescription] = @TipsDescription WHERE (([TipsID] = @Original_TipsID));
	
SELECT TipsId,TipsTitle,TipsDescription FROM Enviro_Tips WHERE (TipsId=@TipsId)
GO
