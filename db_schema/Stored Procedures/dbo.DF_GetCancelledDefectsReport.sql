USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetCancelledDefectsReport]    Script Date: 09/22/2015 16:45:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Get Cancelled Defects Report
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------


DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetCancelledDefectsReport]
		@searchText = '',
		@schemeId = 1,
		@applianceType = 'All',
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'SchemeName',
		@sortOrder = N'ASC',
		@getOnlyCount = 0,
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

IF OBJECT_ID('dbo.DF_GetCancelledDefectsReport') IS NULL 
 EXEC('CREATE PROCEDURE dbo.DF_GetCancelledDefectsReport AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[DF_GetCancelledDefectsReport](
		
		@searchText varchar(5000)='',
		@schemeId INT = -1,
		@applianceType varchar(500) = 'All',
		@defectType varchar(500) = '-1',
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'SchemeName',
		@sortOrder varchar (5) = 'ASC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 
								AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
								AND ((	(P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) 
										OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
		'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' )'						
		END
		
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
		

		IF @defectType = 'appliance'
		BEGIN
		
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
			END
			ELSE
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 '
			END
		
		END
		ELSE IF @defectType = 'boiler'
		BEGIN
			
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')'
			END
			ELSE
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 '
			END
			
		END
		ELSE
		BEGIN
				
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND  ( (LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')) OR
																							(LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%''))
																						   ) '
			END
				
		END
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM
										P_PROPERTY_APPLIANCE_DEFECTS
										INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
										INNER JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
										INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
										INNER JOIN PDR_CANCELLED_JOBS ON PDR_APPOINTMENTS.APPOINTMENTID = PDR_CANCELLED_JOBS.AppointmentId
										LEFT JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
										INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId
										LEFT JOIN P_SCHEME ON P__PROPERTY.SchemeId = P_SCHEME.SCHEMEID
										
										LEFT JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
										LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE ON P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId = PA_PARAMETER_VALUE.ValueID				
										
										'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')									  
									 P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''')	AS Address
									,P__PROPERTY.POSTCODE AS PostCode
									,P__PROPERTY.PropertyId	AS PropertyId 
									,P_SCHEME.SCHEMENAME AS SchemeName
									,P__PROPERTY.SchemeId AS SchemeId
									
									,ISNULL(	CASE 
										WHEN P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 THEN 
											dbo.GetPropertyParameterValue(P__PROPERTY.PropertyId,''Heating'',''Heating Type'')
										ELSE
											GS_APPLIANCE_TYPE.APPLIANCETYPE 
										END, '''' ) AS ApplianceType
									
									,P_DEFECTS_CATEGORY.Description AS Category
									,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME AS RecordedBy
									,LEFT(CONVERT(VARCHAR(5),PDR_APPOINTMENTS.APPOINTMENTSTARTTIME,114),5)
									+'' ''+ 
									LEFT(DATENAME(WEEKDAY, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(3),DATEPART(DAY,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) 
									+'' ''+ 
									LEFT(DATENAME(MONTH, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(4),DATEPART(YEAR,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) AS AppointmentDate 
									,P_PROPERTY_APPLIANCE_DEFECTS.CancelNotes AS CancelNotes
									,PDR_APPOINTMENTS.APPOINTMENTID as AppointmentId
									,(	SELECT LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME 
										FROM PDR_APPOINTMENTS 
											INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO
										WHERE PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID) AS CancelledBy
									,PDR_CANCELLED_JOBS.RecordedOn AS CancelledDate'
							
			--============================Group Clause==========================================
			--SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
			--										, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '								
			--============================Order Clause==========================================		
								
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--=================================	Where Clause ================================
			
			SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
			IF(@getOnlyCount=0)
			BEGIN
				print(@finalQuery)
				EXEC (@finalQuery)
			END
			
			--========================================================================================
			-- Begin building Count Query 
			
			Declare @selectCount nvarchar(4000), 
			@parameterDef NVARCHAR(500)
			
			SET @parameterDef = '@totalCount int OUTPUT';
			SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
			
			--print @selectCount
			EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		END					
			-- End building the Count Query
			--========================================================================================	

END