USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetSchemeDetail]    Script Date: 1/10/2017 7:01:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

IF OBJECT_ID('dbo.[PLANNED_GetSchemeDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetSchemeDetail] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetSchemeDetail](
	@schemeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select 
		-1 as TenancyId,		
		'N/A' as TenantName,
		ISNULL(P_SCHEME.SCHEMENAME,'') as Address,
		'' AS HOUSENUMBER,		
		ISNULL(P_SCHEME.SCHEMENAME,'') as ADDRESS1,
		'' as ADDRESS2,
		'' as TOWNCITY,
		'' as COUNTY,
		'' as POSTCODE,
		'N/A' as TenantNameSalutation,
		'N/A' as Mobile,
		'N/A' as Telephone,
		ISNULL(P_SCHEME.SCHEMENAME,'') as SchemeName,
		'N/A' as Email,
		-1 as CustomerId,
		'N/A' AS Block

	From		
		P_SCHEME 

	WHERE P_SCHEME.SCHEMEID = @schemeId
END
