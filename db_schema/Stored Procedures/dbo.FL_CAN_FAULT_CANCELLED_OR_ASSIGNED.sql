SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
















CREATE PROCEDURE dbo.FL_CAN_FAULT_CANCELLED_OR_ASSIGNED
/* ===========================================================================
 '   NAME:           FL_CAN_FAULT_CANCELLED_OR_ASSIGNED
 '   DATE CREATED:   9 Mar, 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve fault repair list
 
 '   IN:             @Description
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
	@FaultLogId INT,
	@IsAllowed INT output
)
AS
Declare @IsApproved int
Declare @OrgId int

	SELECT @IsApproved = Approved FROM FL_FAULT_PREINSPECTIONINFO 
		
	WHERE FaultLogId = @FaultLogId
	

IF @IsApproved IS NULL --If Approved is null
BEGIN
	SET @IsAllowed = 1	--allowed to cancel or change contractor
END
ELSE IF @IsApproved = 0 --If not Approved
BEGIN
	SET @IsAllowed = 0	--not allowed to cancel or change contractor
END
ELSE IF @IsApproved = 1 --If Approved
BEGIN
	SET @IsAllowed = 1	--not allowed to cancel or change contractor
END

return @IsAllowed

GO
