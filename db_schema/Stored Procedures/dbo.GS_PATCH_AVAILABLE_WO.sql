SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ===================================================
-- Author:		Rob
-- Create date: 28/04/2008
-- Description: This procedure gets the list of patch
--				for work order creation.
-- Update by :  Umair
-- Update Date: 15/05/2008
-- ===================================================
CREATE PROCEDURE [dbo].[GS_PATCH_AVAILABLE_WO]
@ORGID INT = NULL

--EXEC GS_PATCH_AVAILABLE_WO 1670

--THIS GETS THE PATCH FOR THE WORK ORDER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT E.PATCHID, E.LOCATION
FROM E_PATCH E 
WHERE PATCHID IN( 
					SELECT PATCHID FROM S_SCOPETOPATCHANDSCHEME SS 
					INNER JOIN S_SCOPE S  ON S.SCOPEID=SS.SCOPEID 
					WHERE ORGID = COALESCE(@ORGID, ORGID) 
					AND S.RENEWALDATE > GETDATE()
					AND S.SCOPEID NOT IN (SELECT DISTINCT SCOPEID FROM C_REPAIR WHERE SCOPEID IS NOT NULL)
										
				) 
	--AND PATCHID NOT IN ( 
	--						SELECT WOP.PATCHID
	--						FROM P_WORKORDER WO 
	--							INNER JOIN P_WORKORDERTOPATCH WOP ON WOP.WOID=WO.WOID 
	--							INNER JOIN P_WOTOREPAIR WOT ON WOT.WOID=WO.WOID	
	--							INNER JOIN C_REPAIR CR ON CR.JOURNALID=WOT.JOURNALID
	--							INNER JOIN S_SCOPE S ON S.SCOPEID=CR.SCOPEID		
	--						WHERE WOSTATUS<>11 
	--							AND 
	--							WO.DEVELOPMENTID IS NULL 
	--							AND GASSERVICINGYESNO=1
	--							AND ((DATEADD(MM,1,S.RENEWALDATE) < GETDATE()) OR S.RENEWALDATE IS NULL)	
	--						GROUP BY WOP.PATCHID,WO.DEVELOPMENTID,CR.SCOPEID,S.RENEWALDATE	
	--					)  
END



GO
