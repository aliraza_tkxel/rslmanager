
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 11 Aug 2014
-- Description:	Insert/Update PLANNED_Journal, PLANNED_Appointments
-- =============================================

CREATE PROCEDURE [dbo].[PLANNED_ScheduleConditionWorksAppointment]
	-- Add the parameters for the stored procedure here
@userId int
,@journalId int
,@tenancyId int
,@tradeId int
,@startDate date
,@endDate date
,@startTime varchar(10)
,@endTime varchar(10)
,@operativeId int
,@customerNotes varchar(1000)
,@appointmentNotes varchar(1000)
,@duration int
--,@isPending int
,@isSaved int = 0 out
,@appointmentIdOut int = -1 out
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @ConditionToBeArrangedId int,
@JournalHistoryId int
, @AppointmentId int
, @AppointmentHistoryId int
, @AppointmentTypeId int = NULL
, @CurrentDateTime datetime2 = GETDATE()

--SELECT  @ConditionToBeArrangedId = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='Condition To Be Arranged'


SELECT
	@AppointmentTypeId = Planned_Appointment_TypeId
FROM Planned_Appointment_Type
WHERE Planned_Appointment_Type = 'Condition'

IF @tenancyId = -1 BEGIN
SET @tenancyId = NULL
END

-- ====================================================================================================
--										INSERTION (PLANNED_JOURNAL_HISTORY)
-- ====================================================================================================

SELECT  @ConditionToBeArrangedId = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='Condition to be Arranged'

-- =============================================
-- get status history id of "Condition to be Arranged"
-- =============================================
DECLARE @statusHistoryId int
SELECT
	@statusHistoryId = MAX(StatusHistoryId)
FROM PLANNED_StatusHistory
WHERE StatusId = @ConditionToBeArrangedId

INSERT PLANNED_JOURNAL_HISTORY ([JOURNALID]
, [PROPERTYID]
, [COMPONENTID]
, [STATUSID]
, [ACTIONID]
, [CREATIONDATE]
, [CREATEDBY]
, [NOTES]
, [ISLETTERATTACHED]
, [StatusHistoryId]
, [ActionHistoryId]
, [IsDocumentAttached])
SELECT JOURNALID, PROPERTYID, COMPONENTID, @ConditionToBeArrangedId, NULL, @CurrentDateTime, @userId, NULL, 0, @statusHistoryId, NULL,0
FROM PLANNED_JOURNAL
WHERE JOURNALID = @journalId

SELECT
	@JournalHistoryId = SCOPE_IDENTITY()
PRINT 'JOURNALHISTORYID = ' + CONVERT(varchar, @JournalHistoryId)

BEGIN TRANSACTION;
BEGIN TRY

-- ====================================================================================================
--										INSERTION (PLANNED_APPOINTMENTS)
-- ====================================================================================================

INSERT INTO PLANNED_APPOINTMENTS ([TENANCYID]
, [JournalId]
, [JOURNALHISTORYID]
, [APPOINTMENTDATE]
, [APPOINTMENTENDDATE]
, [APPOINTMENTSTARTTIME]
, [APPOINTMENTENDTIME]
, [ASSIGNEDTO]
, [CREATEDBY]
, [LOGGEDDATE]
, [APPOINTMENTNOTES]
, [CUSTOMERNOTES]
, [ISPENDING]
, [JOURNALSUBSTATUS]
, [APPOINTMENTALERT]
, [APPOINTMENTCALENDER]
, [SURVEYOURSTATUS]
, [APPOINTMENTSTATUS]
, [SURVEYTYPE]
, [COMPTRADEID]
, [isMiscAppointment]
, [Planned_Appointment_TypeId])
	VALUES (@tenancyId, @journalId, @JournalHistoryId, @startDate, @endDate, @startTime, @endTime, @operativeId, @userId, @CurrentDateTime, @appointmentNotes, @customerNotes, 1, NULL, NULL, NULL, NULL, 'NotStarted', NULL, NULL, 0, @AppointmentTypeId)

SELECT
	@AppointmentId = SCOPE_IDENTITY()
PRINT 'APPOINTMENTID = ' + CONVERT(varchar, @AppointmentId)

-- ====================================================================================================
--				INSERTION (PLANNED_MISC_TRADES), Insert Condition Works tradeId and Duration
-- ====================================================================================================

INSERT INTO PLANNED_MISC_TRADE ([AppointmentId]
, [TradeId]
, [Duration])
	VALUES (@AppointmentId, @tradeId, @duration)

-- =============================================
--insert into planned_appointments_History using the following trigger 
--PLANNED_AFTER_INSERT_PLANNED_APPAOINTMENTS
-- =============================================

END TRY BEGIN CATCH
IF @@TRANCOUNT > 0 BEGIN
ROLLBACK TRANSACTION;
SET @isSaved = 0
END

DECLARE @ErrorMessage nvarchar(4000);
DECLARE @ErrorSeverity int;
DECLARE @ErrorState int;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
@ErrorSeverity, -- Severity.
@ErrorState -- State.
);
END CATCH;

IF @@TRANCOUNT > 0 BEGIN
COMMIT TRANSACTION;
SET @isSaved = 1
END

SET @appointmentIdOut = @AppointmentId

END
GO
