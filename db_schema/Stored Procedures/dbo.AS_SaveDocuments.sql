
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[AS_SaveDocuments]
--		@journalHistoryId = 1,
--		@documentName = N'abc.pdf'
--		@documentPath = N'C:\AppliancesDocs\Documents'

--SELECT	'Return Value' = @return_value

-- Author:		<Noor Muhammad>
-- Create date: <23 Oct, 2012>
-- Description:	<This stored procedure will save the reccord in as_savedletter with journalhistory id>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveDocuments] 
	-- Add the parameters for the stored procedure here
	@journalHistoryId int,
	@documentName nvarchar(50),
	@documentPath nvarchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [RSLBHALive].[dbo].[AS_Documents]
           ([DocumentName]
           ,[JournalHistoryId]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[DocumentPath]           
           )
     VALUES
           (@documentName 
           ,@journalHistoryId            
           ,CURRENT_TIMESTAMP 
           ,CURRENT_TIMESTAMP 
           ,@documentPath
           )
END
GO
