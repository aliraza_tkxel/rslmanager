SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[I_STATIONERY_ORDERITEM_UPDATE]
(
	@OrderId int,
	@PageNo varchar(10),
	@ItemNo varchar(10),
	@ItemDesc varchar(250),
	@Quantity smallint,
	@Size varchar(50),
	@Colour varchar(50),
	@Cost money,
	@PriorityId smallint,
	@StatusId smallint,
	@OrderItemId int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_STATIONERY_ORDER_ITEM] SET [OrderId] = @OrderId, [PageNo] = @PageNo, [ItemNo] = @ItemNo, [ItemDesc] = @ItemDesc, [Quantity] = @Quantity, [Size] = @Size, [Colour] = @Colour, [Cost] = @Cost, [PriorityId] = @PriorityId, [StatusId] = @StatusId WHERE (([OrderItemId] = @OrderItemId));
	
SELECT OrderItemId, OrderId, PageNo, ItemNo, ItemDesc, Quantity, Size, Colour, Cost, PriorityId, StatusId FROM I_STATIONERY_ORDER_ITEM WHERE (OrderItemId = @OrderItemId)
GO
