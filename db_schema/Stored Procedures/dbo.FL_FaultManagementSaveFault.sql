--ALTER TABLE FL_FAULT ADD TenantsOnline BIT
go
USE [RSLBHALive]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Save Fault Information
    Author: Ahmed Mehmood
    Creation Date: Dec-11-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-11-2015   Ahmed Mehmood       Checks whether selected faultTradeId is in use 
 ===========================================================================
--	EXEC FL_FaultManagementSaveFault
		@faultId = 1,
		@description = 'Tap is Broken',
		@priorityId = 1,
		@duration = 0.5,290
		@recharge = 1, -- Ture
		@netcost = 100,
		@vatRateId = 1,
		@vat = 20,
		@gross = 120,
		@isContractor = 1, --True
		@isFaultActive = 1, --True
		@tenantsonline = 1
		@isGasSafe = 1, --True
		@isOftec = 1, --True
		@userId =65,
		@submitDate = '2013-03-08',
		@faultTrades = '',
		@faultRepairs = '',
		@isSaved  = 0,
		@resultFaultId OUTPUT
 '==============================================================================*/
 
IF OBJECT_ID('dbo.FL_FaultManagementSaveFault') IS NULL 
 EXEC('CREATE PROCEDURE dbo.FL_FaultManagementSaveFault AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE dbo.FL_FaultManagementSaveFault
@faultId INT,
@description NVARCHAR(200),
@priorityId INT,
@duration FLOAT,
@recharge BIT,
@netcost FLOAT = NULL,
@vatRateId INT = NULL,
@vat FLOAT = NULL,
@gross FLOAT = NULL,
@isContractor BIT,
@isFaultActive BIT,
@tenantsonline BIT  = 0,
@isGasSafe BIT,
@isOftec BIT,
@userId INT,
@submitDate DATETIME,
@faultTrades AS FL_FaultTradeInfo READONLY,
@faultRepairs AS FL_FaultRepairInfo READONLY,
@isSaved  INT = 0 OUTPUT,
@resultFaultId  INT = 0 OUTPUT
AS
BEGIN 

SET NOCOUNT ON

BEGIN TRANSACTION
BEGIN TRY

--=================================================================================
-- (1) SAVE FAULT GENERAL INFO
--=================================================================================

IF @faultId <= 0
BEGIN
	--============
	-- New Fault
	--============
	INSERT INTO FL_FAULT
	(AREAID, DESCRIPTION, PRIORITYID, duration, RECHARGE, NETCOST, VATRATEID, VAT, GROSS, isContractor, FAULTACTIVE, IsGasSafe, IsOftec, SUBMITDATE , tenantsonline) 	
	VALUES 	
	(NULL, @description, @priorityId, @duration, @recharge, @netcost, @vatRateId, @vat, @gross, @isContractor, @isFaultActive, @isGasSafe, @isOftec, CONVERT(DATETIME,@submitDate,103), @tenantsonline) 
	
	SET  @faultId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	--============
	-- Update Fault
	--============
	UPDATE FL_FAULT SET
		Description = @DESCRIPTION,
		PRIORITYID = @PRIORITYID,
		duration = @DURATION,
		Recharge = @RECHARGE,
		NETCOST = @NETCOST,
		VATRATEID = @VATRATEID,
		VAT = @VAT,
		GROSS = @GROSS,
		isContractor = @ISCONTRACTOR,	
		FAULTACTIVE=@isFaultActive,
		IsGasSafe = @ISGASSAFE,
		IsOftec = @ISOFTEC,
		tenantsonline = @tenantsonline
	WHERE FAULTID=@faultId
 
END


--=================================================================================
-- (2) SAVE FAULT TRADE INFO
--=================================================================================


DECLARE faultTradeCursor CURSOR FOR 
SELECT *
FROM @faultTrades
OPEN faultTradeCursor

-- Declare Variable to use with cursor
DECLARE 
@FaultTradeId int,
@TradeId int,
@TradeName nvarchar(50),
@IsDeleted bit

-- Loop (Start) through records and insert Fault Trades
	FETCH NEXT FROM faultTradeCursor INTO @FaultTradeId, @TradeId, @TradeName, @IsDeleted
	WHILE @@FETCH_STATUS = 0 
	BEGIN
	
		IF @FaultTradeId = -1	-- IF FAULTTRADEID = -1 THEN INSERT
		BEGIN	
			INSERT INTO [FL_FAULT_TRADE]([FaultId],[TradeId])
			VALUES (@faultId,@TradeId)	
		END
		ELSE IF @IsDeleted = 1	-- IF ISDELETED = 1 THEN DELETE
		BEGIN
			DELETE FROM	 [FL_FAULT_TRADE]
			WHERE FaultTradeId = @FaultTradeId
		END

	FETCH NEXT FROM faultTradeCursor INTO @FaultTradeId, @TradeId, @TradeName, @IsDeleted
	END
	
CLOSE faultTradeCursor
DEALLOCATE faultTradeCursor


--=================================================================================
-- (3) SAVE FAULT REPAIRS INFO
--=================================================================================

DECLARE faultRepairCursor CURSOR FOR 
SELECT *
FROM @faultRepairs
OPEN faultRepairCursor

-- Declare Variable to use with cursor
DECLARE 
@faultRepairId int,
@repairId int ,
@repair nvarchar(50),
@IsRepairDeleted bit

-- Loop (Start) through records and insert Fault Trades
	FETCH NEXT FROM faultRepairCursor INTO @faultRepairId, @repairId, @repair, @IsRepairDeleted
	WHILE @@FETCH_STATUS = 0 BEGIN
	
	IF @faultRepairId = -1	-- IF @faultRepairId = -1 THEN INSERT
	BEGIN	
		INSERT INTO [FL_FAULT_ASSOCIATED_REPAIR]([FaultId],[RepairId])
		VALUES (@faultId,@repairId)
	END
	ELSE IF @IsRepairDeleted = 1	-- IF @IsDeleted = 1 THEN DELETE
	BEGIN
		DELETE FROM	 [FL_FAULT_ASSOCIATED_REPAIR]
		WHERE FaultRepairId = @faultRepairId
	END

	FETCH NEXT FROM faultRepairCursor INTO @faultRepairId, @repairId, @repair, @IsRepairDeleted
	END
	
CLOSE faultRepairCursor
DEALLOCATE faultRepairCursor

	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
		SET @resultFaultId = @faultId
		EXEC  FL_FAULT_TRANSACTIONLOG_ADD @FAULTID,@USERID,1,@SUBMITDATE
	END

END