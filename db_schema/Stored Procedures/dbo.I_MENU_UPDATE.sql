SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_MENU_UPDATE]
(
	@PositionID int,
	@MenuTitle varchar(50),
	@ParentID int,
	@Active bit,
	@Depth int,
	@Rank int,
	@Original_MenuID int,
	@MenuID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_MENU] SET [PositionID] = @PositionID, [MenuTitle] = @MenuTitle, [ParentID] = @ParentID, [Active] = @Active, [Depth] = @Depth, [Rank] = @Rank WHERE (([MenuID] = @Original_MenuID));
	
SELECT MenuID, PositionID, MenuTitle, ParentID, Active, Depth, Rank FROM I_MENU WHERE (MenuID = @MenuID)
GO
