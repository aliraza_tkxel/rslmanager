SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[PM_GETAPPOINTMENTJOURNALBYPROPERTYID]
	@PROPERTYID NVARCHAR(20) 
AS
BEGIN

 
	SELECT  F_PURCHASEORDER.ORDERID,PM_WORKORDER_CONTRACT.WORKORDERID, F_PURCHASEORDER.PODATE AS ORDERDATE, PM_PROGRAMME.PROGRAMMENAME, C_REPAIRTOPLANNEDMAINTENANCE.APPOINTMENTDATE, 
		   PM_PROGRAMME_ASSIGNMENT.PROPERTYID, PM_WORKORDER_STATUS.DESCRIPTION AS SUBSTATUS, 
			CASE 
				WHEN PM_WORKORDER_CONTRACT.WORKORDERID IS NULL THEN 'Created' 
				ELSE C_STATUS.DESCRIPTION 
			END AS STATUS, C_REPAIR.LASTACTIONDATE,C_REPAIRTOPLANNEDMAINTENANCE.STATUSID,PM_PROGRAMME_ASSIGNMENT. PROPERTYLISTID,  
            ISNULL(C_REPAIR.NOTES,'') AS NOTES, S_ORGANISATION.NAME AS ORGANISATION,C_REPAIR.REPAIRHISTORYID,C_REPAIR.LASTACTIONUSER
	FROM    PM_PROGRAMME_ASSIGNMENT 
		INNER JOIN PM_PROGRAMME ON PM_PROGRAMME_ASSIGNMENT.PROGRAMMEID = PM_PROGRAMME.PROGRAMMEID 
		INNER JOIN PM_WORKORDER_CONTRACT ON PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID = PM_WORKORDER_CONTRACT.PROPERTYLISTID 
		INNER JOIN P_WORKORDER ON PM_WORKORDER_CONTRACT.WORKORDERID =P_WORKORDER.WOID  
		INNER JOIN F_PURCHASEORDER ON P_WORKORDER.ORDERID=F_PURCHASEORDER.ORDERID
		INNER JOIN S_ORGANISATION ON S_ORGANISATION.ORGID = F_PURCHASEORDER.SUPPLIERID
		INNER JOIN P_WOTOREPAIR ON P_WORKORDER.WOID=P_WOTOREPAIR.WOID
		INNER JOIN C_JOURNALTOPLANNEDMAINTENANCE ON P_WOTOREPAIR.JOURNALID=C_JOURNALTOPLANNEDMAINTENANCE.JOURNALID AND C_JOURNALTOPLANNEDMAINTENANCE.PROPERTYLISTID=PM_PROGRAMME_ASSIGNMENT.PROPERTYLISTID
		INNER JOIN C_REPAIR ON P_WOTOREPAIR.JOURNALID=C_REPAIR.JOURNALID
		LEFT JOIN C_STATUS ON C_STATUS.ITEMSTATUSID=C_REPAIR.ITEMSTATUSID
		INNER JOIN C_REPAIRTOPLANNEDMAINTENANCE ON C_REPAIR.REPAIRHISTORYID=C_REPAIRTOPLANNEDMAINTENANCE.REPAIRHISTORYID
		INNER JOIN PM_WORKORDER_STATUS ON C_REPAIRTOPLANNEDMAINTENANCE.STATUSID=PM_WORKORDER_STATUS.STATUSID AND dbo.PM_FN_GETCURRENTWORKSTATUS(P_WOTOREPAIR.JOURNALID) =PM_WORKORDER_STATUS.STATUSID
	WHERE   PM_PROGRAMME_ASSIGNMENT.STATUSID=2 AND (PM_PROGRAMME_ASSIGNMENT.PROPERTYID = @PROPERTYID)
		AND C_REPAIR.REPAIRHISTORYID =(SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR A WHERE A.JOURNALID=P_WOTOREPAIR.JOURNALID)
END







GO
