USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.AS_GetHeatingAppointmentDuration') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetHeatingAppointmentDuration] AS SET NOCOUNT ON;') 
GO  
-- Stored Procedure  
  
-- =============================================  
-- Author:  Shehriyar Zafar 
-- Create date: 12/08/2018  
-- =============================================  
Alter PROCEDURE [dbo].[AS_GetHeatingAppointmentDuration]
-- Add the parameters for the stored procedure here  
 @appointmentId INT,
 @journalId INT

AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

SELECT 
	(SELECT ValueDetail FROM PA_PARAMETER_VALUE WHERE ValueId = HPD.HeatingTypeId) As FuelType,
	HPD.Duration AS Duration,
	(SELECT Description FROM G_TRADE WHERE TradeId = HPD.TradeId) As Trade,
	(SELECT AS_Status.Title FROM AS_JOURNAL
				INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
				WHERE AS_JOURNAL.JOURNALID = @journalId) AS Status
	,HPD.HeatingTypeId AS HeatingTypeId
	,HPD.TradeId AS TradeId
	FROM AS_HeatingAppointmentDuration HPD
WHERE HPD.AppointmentId = @appointmentId

END  