SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE PROCEDURE [dbo].[KPI_URGENT]
    (
      @YearStartDate DATETIME ,
      @YearEndDate DATETIME ,
      @Month INT
    )
AS 
    SELECT  DATENAME(month, CompletedDate) AS [Month] ,
            ISNULL(SupplierName, '') AS NAME ,
            OPERATIVE ,
            JobSheetNumber ,
            DEVELOPMENTNAME AS [Scheme] ,
            PROPERTYID AS [Property] ,
            [FaultDescription] AS [Repair/Fault] ,
            SUBMITDATE AS LoggedDate ,
   			DATEADD(mi,  DATEPART(mi, CAST(EndTime AS DATETIME)), DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)), AppointmentDate)) AS AppointmentEndDateTime,
			DATEDIFF(mi, SUBMITDATE, DATEADD(mi,  DATEPART(mi, CAST(EndTime AS DATETIME)), DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)), AppointmentDate))) - 10080 AS [Logged vs AppointmentEndDate],
            AppointmentDate AS AppointmentDate,
            StartTime,
            EndTime,
            CompletedDate AS CompletionDate ,
            DATEDIFF(Day, SUBMITDATE, CompletedDate) AS [Days]
    FROM    dbo.FL_FAULT_LIST_COMPLETED_DETAILS
    WHERE   CompletedDate >= @YearStartDate
            AND CompletedDate <= @YearEndDate
            AND (ASSETTYPE IN ( 1 ) )
            AND MONTH(CompletedDate) = @Month
            AND 1 = 1
            AND (DATEDIFF(D, SUBMITDATE, CompletedDate) ) > 7
            AND PriorityId IN ( 2 )
	ORDER BY MONTH(CompletedDate)







GO
