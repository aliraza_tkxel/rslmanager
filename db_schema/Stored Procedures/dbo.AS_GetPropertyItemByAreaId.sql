USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.AS_GetPropertyItemByAreaId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetPropertyItemByAreaId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetPropertyItemByAreaId](
@areaId int
)
AS
BEGIN

	SELECT ItemID, ItemName
	FROM PA_ITEM
	WHERE AreaID=@areaId AND IsActive=1
	and (ParentItemId not in (SELECT ItemID FROM PA_ITEM WHERE ItemName LIKE 'Sundry') or ParentItemId is null)

END