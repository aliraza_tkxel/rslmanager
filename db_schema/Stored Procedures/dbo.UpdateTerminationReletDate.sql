IF OBJECT_ID('dbo.UpdateTerminationReletDate') IS NULL 
	EXEC('CREATE PROCEDURE dbo.UpdateTerminationReletDate AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[UpdateTerminationReletDate]
	@journalId int = -1,
	@reletDate DateTime
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@PropertyId varchar(100) = NULL,
		@CustomerId int = NULL,
		@TenancyId int = NULL,
		@TerminationDate DateTime = NULL,
		@MSATId  int = NULL

	--Find Termination Date of Last Termination
	SELECT @TerminationDate = TERMINATIONDATE FROM C_TERMINATION 
		WHERE TERMINATIONHISTORYID = (
					SELECT MAX(TERMINATIONHISTORYID)
					FROM C_TERMINATION 
					WHERE JOURNALID = @journalId
					)
		PRINT (@TerminationDate)

	--Find Related info to query in PDR_MSAT 	
	SELECT @PropertyId = PROPERTY.PROPERTYID, @CustomerId = CUSTOMER.CUSTOMERID, @TenancyId = JOURNAL.TENANCYID	 
		FROM C_JOURNAL JOURNAL INNER JOIN P__PROPERTY PROPERTY ON JOURNAL.PROPERTYID = PROPERTY.PROPERTYID
		INNER JOIN C__CUSTOMER CUSTOMER ON CUSTOMER.CUSTOMERID = JOURNAL.CUSTOMERID
		INNER JOIN C_TENANCY TENANCY ON JOURNAL.TENANCYID = TENANCY.TENANCYID
		WHERE JOURNAL.JOURNALID = @journalId

	--Find MSATId to update ReletDate
	SELECT @MSATId = MAX(MSATId) FROM PDR_MSAT 
		WHERE PropertyId = @PropertyId and CustomerId = @CustomerId AND TenancyId = @TenancyId 

	--Update Relet Date	
	UPDATE PDR_MSAT 
		SET ReletDate = @reletDate, TerminationDate = @TerminationDate
		WHERE MSATId = @MSATId
	PRINT (@TerminationDate)


END