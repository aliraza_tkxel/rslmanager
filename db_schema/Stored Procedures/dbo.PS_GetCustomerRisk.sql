SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Noor Muhammad>
-- Create date: <28th Oct,2011>
-- Description:	<This procdure 'll call the risk funciton to get the results of risk >
-- =============================================
CREATE PROCEDURE [dbo].[PS_GetCustomerRisk] 
	-- Add the parameters for the stored procedure here
	@customerId int 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT RISKHISTORYID,CATDESC,SUBCATDESC FROM dbo.RISK_CATS_SUBCATS(@customerId)
END
GO
