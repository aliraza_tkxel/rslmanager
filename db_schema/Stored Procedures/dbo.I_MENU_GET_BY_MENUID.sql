SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[I_MENU_GET_BY_MENUID]
@MENUID INT
AS
SET NOCOUNT ON;
SELECT MENUID, POSITIONID, MENUTITLE, PARENTID, ACTIVE,RANK
FROM I_MENU WHERE MENUID = @MENUID


GO
