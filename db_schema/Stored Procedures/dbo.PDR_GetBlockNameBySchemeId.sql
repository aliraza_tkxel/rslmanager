USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlockNameBySchemeId]    Script Date: 12/12/2018 12:14:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    
    Execution Command:
    
    Exec dbo.PDR_GetBlockNameBySchemeId 2
  =================================================================================*/
 IF OBJECT_ID('dbo.PDR_GetBlockNameBySchemeId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetBlockNameBySchemeId AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetBlockNameBySchemeId]
( @schemeId INT,
@itemId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Distinct isNull(bl.BLOCKID,0) As BlockId,
	Case 
	When Pcp.BlockId = 0 Then 'All'
	ELSE ( bl.BLOCKNAME) END As BlockName
	FROM PDR_ProvisionChargeProperties Pcp
	LEFT JOIN P_BLOCK bl ON Pcp.BlockId = bl.BLOCKID
	WHERE Pcp.SchemeId = @schemeId and Pcp.ItemId = @itemId and Pcp.IsActive = 1
END
