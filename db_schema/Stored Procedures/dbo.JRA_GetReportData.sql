
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC	@return_value = [dbo].[JRA_GetReportData]
--		@searchText = N' ',
--		@actionId = -1,
--		@userId = -1,
--		@selectedDate = N' ',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'ActionName',
--		@sortOrder = N'desc',
--		@allowPaging = 0,
--		@totalCount = @totalCount OUTPUT
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,12/26/2013>
-- Description:	<Description,,Get the Job Role Audit report data>
-- WebPage: JobRoleAuditReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetReportData](
@searchText varchar(500),
@actionId int,
@userId int,
@selectedDate varchar(10),
@pageSize int,
@pageNumber int,
@sortColumn varchar(100),
@sortOrder varchar(100),
@allowPaging bit
,@totalCount int = 0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(2000),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(1000),	
        @mainSelectQuery varchar(6000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(8000),
       
        @searchCriteria varchar(1500),
        
        @offset int,
		@limit int,
	
		@dataLimitation varchar(10)
	
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		
		SET @searchCriteria = ' 1=1 '
		
	
		IF @actionId != -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND E_JOBROLEAUDITHISTORY.JobRoleActionId ='+CONVERT(nvarchar(10), @actionId)
		
		IF @userId != -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND E__EMPLOYEE.EMPLOYEEID = '+CONVERT(nvarchar(10), @userId) 
			
		IF (@selectedDate != '' OR @selectedDate != NULL)
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND CONVERT(VARCHAR(10),E_JOBROLEAUDITHISTORY.CreatedDate, 103)  ='''+CONVERT(varchar(10), @selectedDate,103)+''''  
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND E_JOBROLEACTIONS.ActionTitle LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E_TEAM.TEAMNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E_JOBROLE.JobeRoleDescription LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E__EMPLOYEE.FirstName LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR E__EMPLOYEE.LASTNAME LIKE ''%' + @searchText + '%'''
			
		END	
		
		
		if (@allowPaging = 0) 
			set @dataLimitation = ''
		else
			set @dataLimitation = 'top ('+convert(varchar(10),@limit)+')'
		
	                     
		 SET @selectClause = 'SELECT '+@dataLimitation+ ' 
		[JobRoleAuditHistoryId]
		,E_JOBROLEAUDITHISTORY.[JobRoleActionId]
		,E_JOBROLEAUDITHISTORY.[JobRoleTeamId]
		,[Detail]
		,[CreatedBy] 
		,(CONVERT(VARCHAR(10), CreatedDate, 103)+CHAR(10)+ CONVERT(VARCHAR(5), CreatedDate, 108)) as CreatedDate
		,CreatedDate as CreatedDateSort
		--,(CONVERT(VARCHAR(50), CreatedDate, 101)+CHAR(10)+ RIGHT(CONVERT(VARCHAR, CreatedDate, 100), 7))as CreatedDate  
		,E_JOBROLE.JobeRoleDescription as JobRoleName
		,E_JOBROLEACTIONS.ActionTitle as ActionName
		,E_TEAM.TEAMNAME as TeamName 
		,(SUBSTRING(E__EMPLOYEE.FirstName, 1, 1) +SUBSTRING(E__EMPLOYEE.LASTNAME, 1, 1)) as UserName,  
		E_JOBROLE.JobRoleId as JobRoleId '  
 
		 SET @fromClause =   CHAR(10) +'   
		FROM dbo.E_JOBROLEAUDITHISTORY  
		INNER JOIN E_JOBROLETEAM on E_JOBROLETEAM.JobRoleTeamId = E_JOBROLEAUDITHISTORY.JobRoleTeamId  
		INNER JOIN E_JOBROLE on E_JOBROLE.JobRoleId = E_JOBROLETEAM.JobRoleId   
		INNER JOIN E_JOBROLEACTIONS on E_JOBROLEACTIONS.JobRoleActionId = E_JOBROLEAUDITHISTORY.JobRoleActionId  
		INNER Join E_TEAM on E_TEAM.TEAMID = E_JOBROLETEAM.TEAMID  
		INNER JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = E_JOBROLEAUDITHISTORY.CreatedBy
		'  

--========================================================================================    
		-- Begin building OrderBy clause		
		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		print(@orderClause)
		-- End building OrderBy clause
		--========================================================================================	
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	--========================================================================================
		-- Begin - Execute the Query 
	
		if (@allowPaging = 0) 
			BEGIN
			print(@mainSelectQuery)
			EXEC (@mainSelectQuery)
			END		
		else
			BEGIN	
			print(@finalQuery)
			EXEC (@finalQuery)
			END	
																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
	
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
		
END
GO
