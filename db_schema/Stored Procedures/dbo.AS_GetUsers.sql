
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetUsers
-- Author:		<Salman Nazir>
-- Create date: <20/09/2012>
-- Description:	<This Stored Proceedure shows the Users on the Resources Page, User Listing>
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetUsers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT E__EMPLOYEE.FIRSTNAME,E_JOBDETAILS.JOBTITLE,E__EMPLOYEE.EMPLOYEEID,AS_USERTYPE.UserTypeID,AS_USERTYPE.Description,E__EMPLOYEE.LASTNAME
	FROM AS_USER INNER JOIN
	AS_USERTYPE ON AS_USER.UserTypeID = AS_USERTYPE.UserTypeID INNER JOIN
	E__EMPLOYEE ON AS_USER.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN 
	E_JOBDETAILS ON AS_USER.EmployeeId = E_JOBDETAILS.EMPLOYEEID 
	WHERE AS_USER.IsActive=1 AND (E_JOBDETAILS.ACTIVE = 1)
	ORDER BY E__EMPLOYEE.FIRSTNAME, E__EMPLOYEE.LASTNAME
END
GO
