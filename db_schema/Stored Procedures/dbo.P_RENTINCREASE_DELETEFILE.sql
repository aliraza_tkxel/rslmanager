SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[P_RENTINCREASE_DELETEFILE] (@FILEID INT)
AS

DELETE FROM P_RENTINCREASE_FILES WHERE FILEID = @FILEID

DELETE FROM P_PRE_TARGETRENT_TBL WHERE FILEID = @FILEID

DELETE FROM P_TARGETRENT WHERE FILEID = @FILEID 

DELETE FROM P_TARGETRENT_ADDITIONALASSET WHERE FILEID = @FILEID


GO
