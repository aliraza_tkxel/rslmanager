
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_SetFaultStatusSubContractorUpdate]  
/* ===========================================================================  
-- EXEC FL_SetFaultStatusSubContractorUpdate  
  @FaultLogID = 343,    
  @FaultStatusID = 14,  
  @Notes = 'The Entry was not possible',  
  @LastActionDate GetDate(),  
  @LastActionUserID 65  
--  Author:   Aamir Waheed  
--  DATE CREATED: 15 March 2013  
--  Description: To Set Fault Status to No Entry, and Insert into Table NoEntry,   
--      and Insert Tranction into History  
--  Webpage:  View/Reports/ReportArea.aspx   
 '==============================================================================*/  
@FaultLogID INT,  
@FaultStatusID INT,  
@Notes NVARCHAR(MAX),  
@LastActionDate DATETIME,  
@NoEntryDate DATETIME,  
@LastActionUserID INT,  
@isFollowon bit,  
@FollowOnNotes varchar(500),
@CompletedDate DATETIME = NULL,
@Result INT OUTPUT
   
AS  
BEGIN
SET NOCOUNT ON
BEGIN TRAN
DECLARE @JournalID int, @PROPERTYID nvarchar(max), @ORDID int
SELECT
	@JournalID = FL_FAULT_JOURNAL.JournalID,
	@PROPERTYID = FL_FAULT_LOG.PROPERTYID,
	@ORDID = FL_FAULT_LOG.ORGID
FROM FL_FAULT_LOG
INNER JOIN FL_FAULT_JOURNAL
	ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID
WHERE FL_FAULT_LOG.FaultLogID = @FaultLogID

-- Update Status (StatusID)  in FL_FAULT_LOG  
UPDATE FL_FAULT_LOG
SET StatusID = @FaultStatusID
	,CompletedDate = ISNULL(@CompletedDate,CompletedDate)
WHERE FaultLogID = @FaultLogID

-- Insert the tranction in History Table (FL_FAULT_LOG_HISTORY)  
INSERT INTO FL_FAULT_LOG_HISTORY (JournalID, FaultStatusID, LastActionDate, LastActionUserID,
FaultLogID, ORGID, Notes, PROPERTYID)
	VALUES (@JournalID, @FaultStatusID, @LastActionDate, @LastActionUserID, @FaultLogID, @ORDID, @Notes, @PROPERTYID)

-- Update Status (StatusID)  in FL_FAULT_LOG  
UPDATE FL_FAULT_JOURNAL
SET FaultStatusID = @FaultStatusID
WHERE FaultLogID = @FaultLogID

-- If the fault status is "No Entry" add an entry FL_FAULT_NOENTRY and  
-- After that set status to Complete and add an entry to FL_FAULT_LOG_HISTORY  
IF @FaultStatusID = (SELECT
	FaultStatusID
FROM FL_FAULT_STATUS
WHERE Description = 'No Entry') BEGIN
INSERT INTO FL_FAULT_NOENTRY (FaultLogId, RecordedOn, Notes)
	VALUES (@FaultLogID, @NoEntryDate, @Notes)

-- Start - Get Fault Status ID for Complete Status  
DECLARE @FaultStatusIDComplete int = 17
SELECT
	@FaultStatusIDComplete = FaultStatusID
FROM FL_FAULT_STATUS
WHERE Description = 'Complete'
-- End - Get Fault Status ID for Complete Status   

-- Set Fault Status To Complete in FL_FAULT_LOG  
UPDATE FL_FAULT_LOG
SET	StatusID = @FaultStatusIDComplete,
	CompletedDate = ISNULL(@CompletedDate,CompletedDate)
WHERE FaultLogID = @FaultLogID

-- Insert an entry for Status Complete in FL_FAULT_LOG_HISTORY     
INSERT INTO FL_FAULT_LOG_HISTORY (JournalID, FaultStatusID, LastActionDate, LastActionUserID,
FaultLogID, ORGID, Notes, PROPERTYID)
	VALUES (@JournalID, @FaultStatusIDComplete, @LastActionDate, @LastActionUserID, @FaultLogID, @ORDID, @Notes, @PROPERTYID)

-- Set Fault Status To Complete in FL_FAULT_JOURNAL  
UPDATE FL_FAULT_JOURNAL
SET FaultStatusID = @FaultStatusIDComplete
WHERE FaultLogID = @FaultLogID

END ELSE IF @FaultStatusID = (SELECT
	FaultStatusID
FROM FL_FAULT_STATUS
WHERE Description = 'Cancelled') BEGIN
INSERT INTO FL_FAULT_CANCELLED (FaultLogId, RecordedOn, Notes)
	VALUES (@FaultLogID, @LastActionDate, @Notes)
END

---------- Add Faults FollowOn notes----------------------  
IF @isFollowon = 1 BEGIN
INSERT INTO FL_FAULT_FOLLOWON ([FaultLogId]
, [RecordedOn]
, [FollowOnNotes]
, [isFollowonScheduled])
	VALUES (@FaultLogID, CONVERT(smalldatetime, GETDATE()), @FollowOnNotes, 0)
END
--------------------------------  
-- If insertion fails, goto HANDLE_ERROR block  
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN
SET @RESULT = 1
RETURN

END

/*'=================================*/

HANDLE_ERROR:
ROLLBACK TRAN
SET @RESULT = -1
RETURN
GO
