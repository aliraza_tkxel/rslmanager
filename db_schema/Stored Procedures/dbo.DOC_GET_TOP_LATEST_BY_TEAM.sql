SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DOC_GET_TOP_LATEST_BY_TEAM]
@EMPID INT
AS
DECLARE @TEAMID INT


SELECT @TEAMID = TEAMID FROM VW_EMPLOYEES_INTRANET WHERE EMPLOYEEID = @EMPID

SELECT TOP 3 *,DBO.PATH_BY_DOCID(D.DOCUMENTID) AS FULLPATH  FROM DBO.FN_GET_ALL_DOC() TD
	LEFT JOIN G_TEAMCODES TC ON TC.TEAMCODE COLLATE DATABASE_DEFAULT = TD.DOCFOR COLLATE DATABASE_DEFAULT
	LEFT JOIN DOC_DOCUMENT D ON TD.DOCUMENTID = D.DOCUMENTID
WHERE TC.TEAMID = @TEAMID
ORDER BY TD.DOCUMENTID DESC
GO
