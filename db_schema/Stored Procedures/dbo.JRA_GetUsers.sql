
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,12/26/2013>
-- Description:	<Description,,Get users name for JRA Reports>
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetUsers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
    SELECT EMPLOYEEID UserID,(FIRSTNAME+' '+ LASTNAME) As UserName  from E__EMPLOYEE Order by FIRSTNAME
END
GO
