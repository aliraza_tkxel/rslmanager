USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_SaveFaultCancellation]    Script Date: 8/24/2017 4:25:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- EXEC [dbo].[FL_SaveFaultCancellation]
--		@faultsList = N'112,179',
--		@notes = N'The clent has fixed the problem',
--		@result = @result OUTPUT
-- Author:		<Abdul Moiz>
-- Create date: <24/08/2017>
-- Description:	<Saves fault cancellation information>
-- Web Page: CancellationAppointmentSummary.aspx
-- =====================================================
IF OBJECT_ID('dbo.AS_GasServicingCancellation') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GasServicingCancellation AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GasServicingCancellation]
( 
	@faultsList nvarchar(500),	
	@notes nvarchar(MAX),	
	@result bit OUTPUT,
	@userId Integer
	 
)
AS
BEGIN
Declare @SelectStmt as nvarchar(3000)
Declare @Sql as nvarchar(3200)

Declare @FaultLogID as int
Declare @JournalHistoryID as int
Declare @FaultStatusID as int
Declare @FaultNotes as varchar(4000)
Declare @PROPERTYID as nvarchar(20)
Declare @SchemeId as int
Declare @BlockId as int
Declare @JournalID as int
Declare @AppointmentID as int
Declare @JSGNumber as int
Declare @tenancyId as int
Declare @appointmentshift as nvarchar(max)
Declare @appointmentDate as smalldatetime
Declare @appointmentStartTime as nvarchar(max)
Declare @appointmentEndTime as nvarchar(max)
Declare @assignedTo as int
Declare @createdBy as int
Declare @LOGGEDDATE as datetime
Declare @APPOINTMENTALERT as nvarchar(max)
Declare @APPOINTMENTCALENDER as nvarchar(max)
Declare @SURVEYOURSTATUS as nvarchar(max)
Declare @APPOINTMENTSTATUS as nvarchar(max)
Declare @SURVEYTYPE as nvarchar(max)
Declare @IsVoid as int
Declare @Duration as Int
Declare @appointmentEndDate as smalldatetime
 

SELECT @FaultStatusID = StatusId
FROM AS_Status
WHERE Title = 'Cancelled'

-- this is a cursor variable and is needed so we can use dynamic-sql which we need to deal with the list of TempFaultIDs
Declare @FaultCursor CURSOR

SELECT @FaultLogID=AS_JOURNAL.JOURNALID,
	@FaultNotes=AS_JOURNAL.Notes, @PROPERTYID=AS_JOURNAL.PROPERTYID, @SchemeId=AS_JOURNAL.SchemeId, @BlockId=AS_JOURNAL.BlockId
	From AS_JOURNAL
	WHERE AS_JOURNAL.JOURNALID IN (@faultsList)

SELECT top 1 @AppointmentID=APPOINTMENTID,@JSGNumber=JSGNUMBER, @tenancyId=TENANCYID, @appointmentDate=APPOINTMENTDATE, @appointmentshift=APPOINTMENTSHIFT,
			@appointmentStartTime=APPOINTMENTSTARTTIME, @appointmentEndTime=APPOINTMENTENDTIME, @assignedTo=ASSIGNEDTO, @createdBy=CREATEDBY
			, @APPOINTMENTALERT=APPOINTMENTALERT, @APPOINTMENTCALENDER=APPOINTMENTCALENDER, @SURVEYOURSTATUS=SURVEYOURSTATUS,
			@SURVEYTYPE=SURVEYTYPE, @IsVoid=IsVoid, @Duration=Duration, @appointmentEndDate=APPOINTMENTENDDATE
FROM AS_APPOINTMENTS
WHERE JournalId = @FaultLogID order by 1 desc

	INSERT INTO AS_CANCELLED (JOURNALID, Notes, RecordedOn)
	VALUES (@FaultLogID, @notes, GETDATE())
	
	UPDATE AS_JOURNAL
	SET StatusID = @FaultStatusID
	WHERE AS_JOURNAL.JOURNALID = @FaultLogID	
	
	INSERT INTO AS_JOURNALHISTORY (JournalID, PROPERTYID, SchemeId, BlockId, STATUSID, ACTIONID,
	INSPECTIONTYPEID, CREATIONDATE, CREATEDBY, NOTES, ISLETTERATTACHED, StatusHistoryId, ActionHistoryId, IsDocumentAttached)
	VALUES (@FaultLogID, @PROPERTYID, @SchemeId, @BlockId, @FaultStatusID, NULL, 1, GETDATE(),
	@userId, @FaultNotes, NULL, NULL, NULL, NULL)	

	select @JournalHistoryID=SCOPE_IDENTITY()

	UPDATE AS_APPOINTMENTS
	SET APPOINTMENTSTATUS = (SELECT Title FROM AS_Status WHERE StatusId = @FaultStatusID)
	WHERE APPOINTMENTID = @AppointmentID

	Declare @emailStatus INT

	SELECT @emailStatus = EmailStatusId FROM AS_APPOINTMENTS
	WHERE APPOINTMENTID = @AppointmentID

	insert INTO AS_APPOINTMENTSHISTORY 
	(
			[APPOINTMENTID],
            [JSGNUMBER],
			[TENANCYID]
           ,[JournalId]
           ,[JOURNALHISTORYID]
		   ,[APPOINTMENTDATE]
           ,[APPOINTMENTSHIFT]
		   ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
		   ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[NOTES]
		   ,[APPOINTMENTALERT]
		   ,[APPOINTMENTCALENDER]
		   ,[SURVEYOURSTATUS]
		   ,[APPOINTMENTSTATUS]
		   ,[SURVEYTYPE]
		   ,[IsVoid]
		   ,[EmailStatusId]
		   ,[APPOINTMENTENDDATE]
		   ,[Duration]
	)
	 VALUES (
		@AppointmentID,
		@JSGNumber,
		@tenancyId,
		@FaultLogID,
		@JournalHistoryID,
		@appointmentDate,
		@appointmentshift,
		@appointmentStartTime,
		@appointmentEndTime,
		@assignedTo,
		@createdBy,
		GETDATE(), 
		@notes,
		@APPOINTMENTALERT,
		@APPOINTMENTCALENDER,
		@SURVEYOURSTATUS,
		'Cancelled',
		@SURVEYTYPE,
		@IsVoid, 
		@emailStatus,
		@appointmentEndDate, 
		@Duration)


SET @result = 1

END

