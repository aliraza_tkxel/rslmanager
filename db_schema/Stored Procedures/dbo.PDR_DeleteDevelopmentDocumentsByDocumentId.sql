USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_DeleteDevelopmentDocumentsByDocumentId]    Script Date: 23-Jun-16 10:21:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================  
-- EXEC PDR_DeleteDevelopmentDocumentsByDocumentId  
-- @documentId = 4  
-- Author:  <Junaid Nadeem>  
-- Create date: <23-Jun-2016>  
-- Description: <Delete Development Document By Document Id>  
-- WebPage: AddNewDevelopment.aspx > Document Tab  
-- =============================================  


IF OBJECT_ID('dbo.[PDR_DeleteDevelopmentDocumentsByDocumentId]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_DeleteDevelopmentDocumentsByDocumentId] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PDR_DeleteDevelopmentDocumentsByDocumentId]  
  
 @documentId int,  
 @documentName nvarchar(1000) OUTPUT,
 @documentPath nvarchar(1000) OUTPUT
   
AS  
BEGIN  
 --  Get path of document to delete it from hard disk.  
 SELECT @documentName = DocumentName, @documentPath = DocumentPath FROM PDR_DOCUMENTS WHERE DocumentId = @documentId  
   
 -- Delete the document record from database.   
 DELETE FROM PDR_DOCUMENTS WHERE DocumentId = @documentId 
 DELETE FROM P_DEVELOPMENTDOCUMENTS WHERE DocumentId = @documentId  
  
END