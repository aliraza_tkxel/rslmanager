USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetExpired]    Script Date: 10/7/2016 10:59:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Hussain Ali
-- Modified By:		<Noor Muhammad>
-- Create date: 31/10/2012
-- Modified date: <12/11/2012>
-- Description:	This stored procedure returns the "Expired" properties
-- Usage: Dashboard
-- Exec [dbo].[AS_GetExpired]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- =============================================
IF OBJECT_ID('dbo.[AS_GetExpired]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetExpired] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetExpired]
	@PatchId		int,
	@DevelopmentId	int,
	-- parameters for sorting and paging		
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Address',
	@sortOrder varchar (5) = 'ASC',
	@totalCount int=0 output,
	@FuelType varchar (8000)
AS
BEGIN

	declare @offset int
	declare @limit int

	set @offset = 1+(@pageNumber-1) * @pageSize
	set @limit = (@offset + @pageSize)-1	

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	nvarchar(max),
			@selectCountClause	nvarchar(max),
			@fromClause		nvarchar(max),

			@SelectClauseScheme nvarchar(max),
			@fromClauseScheme nvarchar(max),
			@whereClauseScheme	nvarchar(max),
			@SelectClauseBlock nvarchar(max),
			@fromClauseBlock nvarchar(max),
			@whereClauseBlock	nvarchar(max),
			@mainGasUnionQuery nvarchar(max),

			@orderClause  nvarchar(max),	
			@whereClause	nvarchar(max),			
			@mainSelectQuery nvarchar(max),        
			@rowNumberQuery nvarchar(max),
			@finalQuery nvarchar(max)     
---------------------------------------------------- FOR GAS --------------------------------------------			   
if @FuelType = 'Gas'
begin

	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								'

	SET @fromClause		= 'From 
	AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID
	INNER JOIN P__PROPERTY P__PROPERTY on P__PROPERTY.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_LGSR ON P_LGSR.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P__PROPERTY.ASSETTYPE'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + '1=1 
								AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
								AND P__PROPERTY.STATUS NOT IN (9,5,6,7) 
								AND PV.ValueDetail=''Mains Gas''
								AND PHM.IsActive = 1
								AND AT.description not in (''Shared Ownership'')
								AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
								AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')'

	
	SET @SelectClauseScheme	= 'SELECT
								ISNULL(P.SCHEMENAME,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								'

	SET @fromClauseScheme= ' FROM dbo.P_SCHEME P 
	INNER JOIN PA_HeatingMapping PHM ON P.SchemeID = PHM.SchemeID
	LEFT JOIN P_LGSR P_LGSR ON P_LGSR.HeatingMappingId=PHM.HeatingMappingId
	LEFT JOIN AS_JOURNAL J ON J.SchemeId=P.SCHEMEID AND J.ISCURRENT=1
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	'
	SET @whereClauseScheme = ' WHERE 1=1 
						AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
						AND PV.ValueDetail=''Mains Gas''
						AND PHM.IsActive = 1
						AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')' 

	SET @SelectClauseBlock	= 'SELECT
								ISNULL(P.BlockName,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								'

	SET @fromClauseBlock= ' FROM dbo.P_BLOCK P 
							INNER JOIN PA_HeatingMapping PHM ON P.BlockId = PHM.BlockId
							LEFT JOIN P_LGSR P_LGSR ON P_LGSR.HeatingMappingId=PHM.HeatingMappingId
							LEFT JOIN P_SCHEME PD ON PD.SCHEMEID=P.SCHEMEID 
							LEFT JOIN AS_JOURNAL J ON J.BlockId=P.BlockId AND J.ISCURRENT=1
							LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
							INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
							LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
							'

	SET @whereClauseBlock = ' WHERE 1=1 
						AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
						AND PV.ValueDetail=''Mains Gas''
						AND PHM.IsActive = 1
						AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')' 
end


------------------------------ FOR OIL ----------------------------------------------------------------------
if @FuelType = 'Oil'
begin
-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT top ('+convert(varchar(10),@limit)+') 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								'

	SET @fromClause		= 'From AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID
	INNER JOIN P__PROPERTY P__PROPERTY on P__PROPERTY.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_LGSR ON P_LGSR.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P__PROPERTY.ASSETTYPE
	'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + '1=1 
								AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
								AND P__PROPERTY.STATUS NOT IN (9,5,6,7) 
								AND PV.ValueDetail=''Oil''
								AND PHM.IsActive = 1
								AND AT.description not in (''Shared Ownership'')
								AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
								AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Oil'')
								'
end
------------------------------ FOR Alternative servicing ----------------------------------------------------------------------
-- Initalizing the variables to be used in this query
if @FuelType = 'Alternative Servicing'
begin
	SET @selectClause	= 'SELECT distinct top ('+convert(varchar(10),@limit)+') 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								'

	SET @fromClause		= 'From AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID

	INNER JOIN P__PROPERTY P__PROPERTY on P__PROPERTY.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_LGSR ON P_LGSR.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P__PROPERTY.ASSETTYPE
	'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + '1=1 
								AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
								AND P__PROPERTY.STATUS NOT IN (9,5,6,7) 
								AND AT.description not in (''Shared Ownership'')
								AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
								AND PHM.IsActive = 1
								AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Alternative Servicing'')'
end
	

	SET @selectCountClause = ' SELECT distinct
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								'
	
	IF (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	IF (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	IF (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	IF (@DevelopmentId <> -1)
	BEGIN
		SET @whereClauseBlock = @whereClauseBlock + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
		SET @whereClauseScheme = @whereClauseScheme + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
	END	

	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
	--========================================================================================
	-- Begin building the main select Query
	Declare @unionQuery varchar(100)
	SET @unionQuery = char(10) + ' UNION ALL ' + CHAR(10)
	if @fuelType = 'Gas'
	Begin
		Set @mainGasUnionQuery = @selectClause +@fromClause + @whereClause + @unionQuery +
								@SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery +
								@SelectClauseBlock + @fromClauseBlock + @whereClauseBlock

		Set @mainSelectQuery = ' SELECT TOP ('+convert(VARCHAR(10),@limit)+') * from ( ' + @mainGasUnionQuery + ' ) as TopResult  ' + 
						@orderClause + '  '
	END 
	else
	Begin
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause
	End

	--print(@mainSelectQuery)

	-- End building the main select Query
	--========================================================================================			
	
	
	--========================================================================================
	-- Begin building the row number query
	--print(@mainSelectQuery)
	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	--print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	Declare @countClauseEnd nvarchar(2000)
	SET @countClauseEnd = ') AS CertifictaeExpired'

	Declare @selectCount nvarchar(max), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	--print( @fromClause + @whereClause + @countClauseEnd)
	if(@fuelType = 'Gas')
	Begin
		SET @selectCount= 'SELECT @totalCount =  count(*) From ( ' + @selectClause + @fromClause + @whereClause + @unionQuery +
								@SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery +
								@SelectClauseBlock + @fromClauseBlock + @whereClauseBlock + @countClauseEnd
	End
	else
	Begin
		SET @selectCount= 'SELECT @totalCount =  count(*) From ( ' + @selectCountClause + @fromClause + @whereClause + @countClauseEnd
	End
	
	print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		
	-- End building the Count Query
	--========================================================================================	 	 	
   
END



