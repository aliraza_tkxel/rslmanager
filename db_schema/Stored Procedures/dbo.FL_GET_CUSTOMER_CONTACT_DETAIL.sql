SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE [dbo].[FL_GET_CUSTOMER_CONTACT_DETAIL]
	/*	===============================================================
	'   NAME:           FL_GET_CUSTOMER_CONTACT_DETAIL
	'   DATE CREATED:   29 Oct,2008
	'   CREATED BY:     Noor Muhammad
	'   CREATED FOR:    Tenants Online
	'   PURPOSE:        To retrieve the Customer information to be displayed on fault submission page
	'   IN:             @CustomerId 
	'   OUT: 		    No     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@CustomerId INT
	)
AS

SELECT 

Customer.FIRSTNAME AS CustomerFirstName ,
Customer.MIDDLENAME AS CustomerMiddleName ,
Customer.LASTNAME AS CustomerLastName ,

CustomerAddress.EMAIL AS Email,
CustomerAddress.TEL AS Telephone,
CustomerAddress.MOBILE as mobile,
CustomerAddress.HOUSENUMBER AS HouseNumber,
CustomerAddress.ADDRESS1 AS Address1,
CustomerAddress.ADDRESS2 AS Address2,
CustomerAddress.ADDRESS3 AS Address3,
CustomerAddress.TOWNCITY AS TownCity,
CustomerAddress.COUNTY AS County,
CustomerAddress.POSTCODE AS Postcode

FROM C__CUSTOMER Customer INNER JOIN
C_ADDRESS CustomerAddress ON Customer.CUSTOMERID=CustomerAddress.CUSTOMERID

where Customer.CUSTOMERID=@CustomerId  AND CustomerAddress.ISDEFAULT=1

















GO
