SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[sp_getuserdetails]
@user_id int
AS 

SELECT  dbo.H_USER_TYPE.DESCRIPTION, dbo.H_LOGIN.*
FROM         dbo.H_LOGIN INNER JOIN
                      dbo.H_USER_TYPE ON dbo.H_LOGIN.USER_TYPE_ID = dbo.H_USER_TYPE.USER_TYPE_ID
where [user_id] = @user_id



GO
