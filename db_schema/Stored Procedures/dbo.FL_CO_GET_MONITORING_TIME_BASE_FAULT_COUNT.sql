SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















CREATE PROCEDURE [dbo].[FL_CO_GET_MONITORING_TIME_BASE_FAULT_COUNT] 

/* ===========================================================================
 '   NAME:           FL_CO_GET_MONITORING_TIME_BASE_FAULT_COUNT
 '   DATE CREATED:   30TH DECEMBER 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist monitoring grid faults based on date criteria provided
 '   IN:             @selectedDateOption ,@orgId				
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria	    
	    @orgId int,			
		
		--output 
		@allJobsCount INT= 0 OUTPUT,
		@overDueJobsCount INT = 0 OUTPUT,
		@sevenDaysJobsCount INT = 0 OUTPUT,
		@oneFourDaysJobsCount INT = 0 OUTPUT,
		@twoOneDaysJobsCount INT = 0 OUTPUT,
		@twoEightDaysJobsCount INT = 0 OUTPUT
		
	)
	
as	                        
	
	Declare @abc as varchar(1000)   
	
	--========================================================================================      
    --Found Count of ------ALL Jobs-------
    SELECT @allJobsCount = COUNT(*) FROM 	
    FL_FAULT INNER JOIN 
	FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN 
	FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN
	C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN 
              C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN
              C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN	
              P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN
	FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN
	E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN
	FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID
	WHERE FL_FAULT_LOG.ORGID = @orgId
	
	--======================================================================================== 
	--Found Count of -------"Over Due Jobs"-------- 
	
	SELECT @overDueJobsCount = COUNT(*) FROM 	
    FL_FAULT INNER JOIN 
	FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN 
	FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN
	C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN 
	   C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN
              C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN	
              P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN
	FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN
	E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN
	FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID
	WHERE Convert(varchar(10),FL_FAULT_LOG.DueDate,120) < Convert(varchar(11),GETDATE(),120) AND FL_FAULT_LOG.ORGID = @orgId
	
	--========================================================================================    	
	-- Found Count of ------"Jobs Due in Next 7 Days"-------       
	
	SELECT @sevenDaysJobsCount = COUNT(*) FROM 	
    FL_FAULT INNER JOIN 
	FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN 
	FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN
	C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN 
	   C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN
              C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN	
              P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN
	FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN
	E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN
	FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID
	WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,120) between Convert(varchar(11),GETDATE(),120) and Convert(varchar(11),DATEADD(d,7,GETDATE()),120)) AND FL_FAULT_LOG.ORGID = @orgId
	--WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,101) <= Convert(varchar,DATEADD(d,7,GETDATE()),103)
	--AND Convert(varchar(10),FL_FAULT_LOG.DueDate,101) >= Convert(varchar,GETDATE(),103)) AND FL_FAULT_LOG.ORGID = @orgId
	

	--========================================================================================    	
	-- Found Count of ------"Jobs Due in Next 14 Days"-------       
	
	SELECT @oneFourDaysJobsCount = COUNT(*) FROM 	
    FL_FAULT INNER JOIN 
	FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN 
	FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN
	C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN 
	   C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN
              C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN	
              P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN
	FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN
	E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN
	FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID
	WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,120) between Convert(varchar(11),GETDATE(),120) and Convert(varchar(11),DATEADD(d,14,GETDATE()),120)) AND FL_FAULT_LOG.ORGID = @orgId
	--WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,103) <= Convert(varchar,DATEADD(d,14,GETDATE()),103)
	--AND Convert(varchar(10),FL_FAULT_LOG.DueDate,103) >= Convert(varchar,GETDATE(),103)) AND FL_FAULT_LOG.ORGID = @orgId     
	
	--========================================================================================    	
	-- Found Count of ------"Jobs Due in Next 21 Days"-------       
	
	SELECT @twoOneDaysJobsCount = COUNT(*) FROM 	
    FL_FAULT INNER JOIN 
	FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN 
	FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN
	C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN 
	   C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN
              C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN	
              P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN
	FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN
	E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN
	FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID
	WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,120) between Convert(varchar(11),GETDATE(),120) and Convert(varchar(11),DATEADD(d,21,GETDATE()),120)) AND FL_FAULT_LOG.ORGID = @orgId
	--WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,103) <= Convert(varchar,DATEADD(d,21,GETDATE()),120)
	--AND Convert(varchar(10),FL_FAULT_LOG.DueDate,103) >= Convert(varchar,GETDATE(),120)) AND FL_FAULT_LOG.ORGID = @orgId 

	--========================================================================================    	
	-- Found Count of ------"Jobs Due in Next 28 Days"-------       
	
	SELECT @twoEightDaysJobsCount = COUNT(*) FROM 	
    FL_FAULT INNER JOIN 
	FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN 
	FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN
	C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN 
	   C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN
              C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN	
              P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN
	FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN
	E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN
	FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID
	WHERE (Convert(varchar(10),FL_FAULT_LOG.DueDate,120) between Convert(varchar(11),GETDATE(),120) and Convert(varchar(11),DATEADD(d,28,GETDATE()),120)) AND FL_FAULT_LOG.ORGID = @orgId






GO
