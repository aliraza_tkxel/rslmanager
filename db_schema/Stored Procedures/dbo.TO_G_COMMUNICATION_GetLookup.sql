SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_G_COMMUNICATION_GetLookup]
/* ===========================================================================
 '   NAME:           TO_G_COMMUNICATION_GetLookup
 '   DATE CREATED:   09 May 2013
 '   CREATED BY:     Nataliya Alexander
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get communication records from G_COMMUNICATION table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
 SELECT COMMUNICATIONID AS id ,
        [DESCRIPTION] AS val
 FROM   G_COMMUNICATION
 ORDER BY [DESCRIPTION] ASC



GO
