USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetSchemeData]    Script Date: 14-Mar-17 2:56:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: Get Scheme Data
 
    Author: Salman Nazir
    Creation Date: FEb-03-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         FEb-03-2015      Salman Nazir         Get Scheme Data for scheme main detail page
    EXEC PDR_GetSchemeData 1
  =================================================================================*/

IF OBJECT_ID('dbo.PDR_GetSchemeData') IS NULL 
EXEC('CREATE PROCEDURE dbo.PDR_GetSchemeData AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetSchemeData]
@schemeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Get Scheme Info from P_SCHEME
Select S.SCHEMEID,ISNULL(S.SCHEMENAME,'') as SCHEMENAME,ISNULL(S.SCHEMECODE,'') as SCHEMECODE,ISNULL(D.DEVELOPMENTNAME,'') as DEVELOPMENTNAME,D.DEVELOPMENTID,ISNULL(S.PHASEID,'') as PHASEID
,ISNULL(P.PhaseName,'') as PhaseName
from P_SCHEME S
INNER JOIN PDR_DEVELOPMENT D on D.DEVELOPMENTID = S.DEVELOPMENTID
LEFT JOIN P_PHASE P on P.PHASEID = S.PHASEID

Where S.SCHEMEID = @schemeId


-- Get Blocks for Scheme
SELECT B.BLOCKID, B.BLOCKNAME FROM P_SCHEME S
INNER JOIN P_BLOCK B on B.SCHEMEID = S.SCHEMEID

Where S.SCHEMEID = @schemeId


-- Get Properties for Scheme
Select p.PROPERTYID, (ISNULL(P.HOUSENUMBER,' ')+' '+ISNULL(P.ADDRESS1,' ')) PropertyAddress from P_SCHEME S
INNER JOIN P__PROPERTY P on P.SCHEMEID = S.SCHEMEID

Where S.SCHEMEID = @schemeId
END
