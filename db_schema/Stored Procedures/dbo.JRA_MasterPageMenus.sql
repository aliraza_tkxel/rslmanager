
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_MasterPageMenus @employeeId = 143
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/06/2014>
-- Description:	<Description,,TO the menus for master page according to the employee rights>
-- WebPage: JobRoleAccess.Master
-- =============================================
CREATE PROCEDURE [dbo].[JRA_MasterPageMenus](
@employeeId int)
AS
BEGIN

SELECT
	AC_MENUS.DESCRIPTION AS Menu,
	AC_MENUS.PAGE AS MenuPagePath,
	AC_MENUS.MENUID AS MenuId
FROM E__EMPLOYEE 
		INNER JOIN AC_MENUS_ACCESS ON E__EMPLOYEE.JobRoleTeamId = AC_MENUS_ACCESS.JobRoleTeamId 
		INNER JOIN AC_MENUS ON AC_MENUS_ACCESS.MenuId = AC_MENUS.MENUID 
		INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID 
WHERE AC_MENUS.ACTIVE = 1 
		AND EMPLOYEEID = @employeeId 
		AND AC_MODULES.MODULEID = 1
ORDER BY AC_MENUS.ORDERTEXT, AC_MENUS.DESCRIPTION
END
GO
