
-- =============================================  
-- Exec AS_ItemByAreaId @areaId=1  
-- Author: <Salman Nazir>  
-- Create date: <25/10/2012>  
-- Description: <Get Items by Area id for Attributes Tree>  
-- Web Page: PropertyRecord.aspx  
IF OBJECT_ID('dbo.AS_ItemByAreaId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_ItemByAreaId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
-- =============================================  
ALTER PROCEDURE [dbo].[AS_ItemByAreaId](  
@areaId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  SELECT DISTINCT item.* ,ISNULL( Si.SubItem ,0) as  SubItem
 From PA_ITEM item 
 Outer APPLY (Select Count(I.ItemId)as SubItem  From PA_ITEM I Where I.ParentItemId= item.ItemID and I.IsActive = 1 Group By I.ItemId  ) as SI
 Where item.AreaID = @areaId and item.IsActive = 1  and item.ParentItemId is null 
 ORDER BY ItemSorder ASC 
END  