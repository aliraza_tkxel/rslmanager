SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umair Naeem
-- Create date: 29-April-2008
-- Description:	This procedure will get the journal item detail information of a survey against 
--				SDID of a property which will be displayed as journal tab in Portfolio Area
-- EXEC SP_PDA_SURVEY_PROPERTYJOURNAL_DETAIL 48
-- =============================================
create PROCEDURE [dbo].[SP_PDA_SURVEY_PROPERTYJOURNAL_DETAIL] 
	-- parameters for the stored procedure 
	@SDID NVARCHAR(20) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- statements for procedure
	SELECT	PS.TITLE,
			CASE PSD.SURVEYSTATUS 
				WHEN 0 THEN 'In Progress'
				WHEN 1 THEN 'Completed'
			END AS CURRENT_STATUS,	
			ISNULL(CONVERT(NVARCHAR, PS.SURVEYDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), PS.SURVEYDATE, 108) ,'') AS [DATE],
			PSD.NOTES AS [NOTES],PS.SURVEYORNAME
	FROM TBL_PDA_SURVEY PS
		INNER JOIN TBL_PDA_SURVEY_DETAIL PSD ON PSD.SURVEYID=PS.SURVEYID	
	WHERE PSD.SDID=COALESCE(@SDID, PSD.SDID)
END
GO
