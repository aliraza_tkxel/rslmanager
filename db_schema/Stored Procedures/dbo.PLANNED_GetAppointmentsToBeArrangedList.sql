USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@totalCount int

--SELECT	@totalCount = 0

--EXEC	@return_value = [dbo].[PLANNED_GetAppointmentsToBeArrangedList]
--		@schemeId = -1,
--		@searchText = NULL,
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'PMO',
--		@sortOrder = N'Desc',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,11/7/2013>
-- Description:	<Description,Update/Insert PLANNED_JOURNAL,PLANNED_INSPECTION_APPOINTMENTS
--Last modified Date:11/7/2013
-- =============================================
 
IF OBJECT_ID('dbo.[PLANNED_GetAppointmentsToBeArrangedList]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetAppointmentsToBeArrangedList] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetAppointmentsToBeArrangedList]
	-- Add the parameters for the stored procedure here
	
		@schemeId INT = -1,
		@searchText VARCHAR(200) = '',
		@componentId INT = -1,
		@type VARCHAR(200) = '',

	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE 
		
		
		@SelectClause varchar(MAX),
        @fromClause   varchar(MAX),
        @whereClause  varchar(MAX),	        
        @orderClause  varchar(MAX),	
        @mainSelectQuery varchar(MAX),        
        @rowNumberQuery varchar(MAX),
        @finalQuery varchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(MAX),
        
        --variables for paging
        @offset int,
		@limit int
				
		,@toBeArranged varchar(100)	
		SET @toBeArranged = '''To be Arranged'''
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
									
		SET @searchCriteria = ' 1=1 AND (PLANNED_JOURNAL.COMPONENTID IS NOT NULL OR PLANNED_JOURNAL.APPOINTMENTTYPEID IS NOT NULL) AND PLANNED_STATUS.TITLE='+ @toBeArranged
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ''PMO''+CONVERT(VARCHAR,PLANNED_JOURNAL.JOURNALID) LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PJS.SCHEMENAME LIKE ''%' + @searchText + '%'''   
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PJB.BLOCKNAME LIKE ''%' + @searchText + '%'''  
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PLANNED_COMPONENT.COMPONENTNAME LIKE ''%' + @searchText + '%'''			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PROPERTYADDRESS.Address LIKE ''%' + @searchText + '%'') '
		END	
		
		IF @schemeId != -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (P__PROPERTY.SCHEMEID = '+CONVERT(nvarchar(10), @schemeId)+' OR PJS.SCHEMEID = '+CONVERT(nvarchar(10), @schemeId)+')'	  	

		IF NOT @componentId = -1
			SET @SearchCriteria = @SearchCriteria + CHAR(10) +' AND PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' '

		IF(@type != '' OR @type != NULL)   
		  begin
			declare @appointmmenttype int 

			select @appointmmenttype = Planned_Appointment_TypeId from PLANNED_APPOINTMENT_TYPE where PLANNED_APPOINTMENT_TYPE = @type
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PAT.Planned_Appointment_TypeId = '+ CONVERT(VARCHAR(10),@appointmmenttype) + ' '    
		  end

		-- End building SearchCriteria clause   
		--========================================================================================
		
		
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') 
		PLANNED_JOURNAL.JOURNALID as PMO 
		,ISNULL(CASE WHEN PLANNED_JOURNAL.PROPERTYID IS NULL THEN ISNULL(PJS.SCHEMENAME,'''')
		WHEN PLANNED_JOURNAL.PROPERTYID IS NOT NULL THEN ISNULL(P_SCHEME.SCHEMENAME,'''')
		END, '''') as Scheme 
		,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address	
		,ISNULL(CASE WHEN PLANNED_JOURNAL.PROPERTYID IS NULL THEN PJB.POSTCODE
		WHEN PLANNED_JOURNAL.PROPERTYID IS NOT NULL THEN P__PROPERTY.POSTCODE
		END, '''') as Postcode   
		,CASE WHEN PAT.Planned_Appointment_Type=''Misc''    
		THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + ''(misc)'', ''Miscellaneous'') 
		WHEN PAT.Planned_Appointment_Type=''Adaptation''    
		THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + ''(misc)'', ''Miscellaneous'')    
		ELSE ISNULL(PLANNED_COMPONENT.COMPONENTNAME, ''N/A'')    
		END  AS Component 
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  YEAR(PA_PROPERTY_ITEM_DATES.DueDate)
	when PLANNED_JOURNAL.BlockId > 0 THEN  YEAR(PA_PROPERTY_ITEM_DATESBlock.DueDate)
	when PLANNED_JOURNAL.SchemeId > 0 THEN  YEAR(PA_PROPERTY_ITEM_DATESScheme.DueDate)
	end as Due
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  PA_PROPERTY_ITEM_DATES.DueDate
	when PLANNED_JOURNAL.BlockId > 0 THEN  PA_PROPERTY_ITEM_DATESBlock.DueDate
	when PLANNED_JOURNAL.SchemeId > 0 THEN  PA_PROPERTY_ITEM_DATESScheme.DueDate
	end as DueDateFull
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  PA_PROPERTY_ITEM_DATES.ItemId
	when PLANNED_JOURNAL.BlockId > 0 THEN  PA_PROPERTY_ITEM_DATESBlock.ItemId
	when PLANNED_JOURNAL.SchemeId > 0 THEN  PA_PROPERTY_ITEM_DATESScheme.ItemId
	end as ItemId
		,coalesce(pmt.TRADES,ComponentStat.TRADES,''N/A'') as Trades
        ,ISNULL(PAT.Planned_Appointment_Type,''Planned'') as AppointmentType
		,ISNULL(PAT.Planned_Appointment_TypeId, 4) as AppointmentTypeID
		,ISNULL(
		CASE 
		WHEN ComponentDuration.DURATION > 0 THEN
			CASE 
				WHEN ComponentDuration.DURATION > 1 THEN
				CONVERT(VARCHAR ,ComponentDuration.DURATION)+'' hours ''
				ELSE 
				CONVERT(VARCHAR ,ComponentDuration.DURATION)+'' hour '' 
			END
		ELSE
			CASE 
				WHEN pmt.Duration > 1 THEN
				CONVERT(VARCHAR ,pmt.Duration)+'' hours ''
				ELSE 
				CONVERT(VARCHAR ,pmt.Duration)+'' hour '' 
			END
		END,''N/A'') as  Duration
		--,ISNULL(convert(varchar,ComponentDuration.DURATION),''N/A'') as Duration
		,PLANNED_STATUS.TITLE as Status	
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  P__PROPERTY.HouseNumber
		when PLANNED_JOURNAL.BlockId > 0 THEN  PJB.BLOCKNAME
		when PLANNED_JOURNAL.SchemeId > 0 THEN ''''
		end as HouseNumber  
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  P__PROPERTY.ADDRESS1
		when PLANNED_JOURNAL.BlockId > 0 THEN  PJB.ADDRESS1
		when PLANNED_JOURNAL.SchemeId > 0 THEN PJS.sCHEMENAME 
		end as PrimaryAddress 
		,CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  P__PROPERTY.PropertyId
		when PLANNED_JOURNAL.BlockId > 0 THEN  CONVERT(nvarchar(10), PJB.blockid)
		when PLANNED_JOURNAL.SchemeId > 0 THEN CONVERT(nvarchar(10), PJS.schemeid) 
		end as PropertyId 
		,P_SCHEME.SCHEMEID as SchemeId
		,PLANNED_COMPONENT.ComponentId as ComponentId
		,ISNULL(ComponentDuration.DURATION,0) as DurationSort
		,ISNULL(PJS.SCHEMENAME,'''') as PJScheme
	   , ISNULL(PJB.BLOCKNAME, '''') AS PJBlock
	   , CASE WHEN PLANNED_JOURNAL.PROPERTYID is not null then  ''Property''
		when PLANNED_JOURNAL.BlockId > 0 THEN  ''Block''
		when PLANNED_JOURNAL.SchemeId > 0 THEN ''Scheme''
		end as Type  		
		'
		
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'		
		FROM 
		PLANNED_JOURNAL	
		LEFT JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		LEFT JOIN P_SCHEME PJS ON PLANNED_JOURNAL.SchemeId = PJS.SCHEMEID  
		LEFT JOIN P_BLOCK PJB ON PLANNED_JOURNAL.BlockId = PJB.BLOCKID     
		LEFT JOIN (SELECT	P__PROPERTY.PROPERTYID as PID,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
					FROM	P__PROPERTY)AS PROPERTYADDRESS ON P__PROPERTY.PROPERTYID = PROPERTYADDRESS.PID
		LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
		LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
        LEFT JOIN PLANNED_APPOINTMENT_TYPE PAT ON PLANNED_JOURNAL.APPOINTMENTTYPEID = PAT.Planned_Appointment_TypeId     
		LEFT JOIN 
		 (	SELECT	PID.SID AS SID
					,PID.PROPERTYID AS PROPERTYID
					,PID.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
					,PID.DueDate  as DueDate
					,PID.LastDone AS LastDone
					,PID.ItemId as ItemId
			FROM	PA_PROPERTY_ITEM_DATES AS PID
			INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID.PLANNED_COMPONENTID = PCI.COMPONENTID AND PID.ItemId = PCI.ItemId  
									) AS PA_PROPERTY_ITEM_DATES ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID =  PLANNED_JOURNAL.COMPONENTID 
			AND PA_PROPERTY_ITEM_DATES.PROPERTYID   = PLANNED_JOURNAL.PROPERTYID
		LEFT JOIN 
		 (	SELECT	PID.SID AS SID
					,PID.SchemeId AS PROPERTYID
					,PID.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
					,PID.DueDate  as DueDate
					,PID.LastDone AS LastDone
					,PID.ItemId as ItemId
			FROM	PA_PROPERTY_ITEM_DATES AS PID
			INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID.PLANNED_COMPONENTID = PCI.COMPONENTID AND PID.ItemId = PCI.ItemId  
									) AS PA_PROPERTY_ITEM_DATESScheme ON PA_PROPERTY_ITEM_DATESScheme.PLANNED_COMPONENTID =  PLANNED_JOURNAL.COMPONENTID 
			AND PA_PROPERTY_ITEM_DATESScheme.PROPERTYID   = PLANNED_JOURNAL.SchemeId  

		LEFT JOIN 
		 (	SELECT	PID.SID AS SID
					,PID.BlockId AS PROPERTYID
					,PID.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
					,PID.DueDate  as DueDate
					,PID.LastDone AS LastDone
					,PID.ItemId as ItemId
			FROM	PA_PROPERTY_ITEM_DATES AS PID
			INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID.PLANNED_COMPONENTID = PCI.COMPONENTID AND PID.ItemId = PCI.ItemId  
									) AS PA_PROPERTY_ITEM_DATESBlock ON PA_PROPERTY_ITEM_DATESBlock.PLANNED_COMPONENTID =  PLANNED_JOURNAL.COMPONENTID 
			AND PA_PROPERTY_ITEM_DATESBlock.PROPERTYID   = PLANNED_JOURNAL.BlockId 
		LEFT JOIN 
		(SELECT	PLANNED_COMPONENT.COMPONENTID AS COMPONENTID , TRADES  = 
		STUFF((SELECT '', '' + G_TRADE.Description 
           FROM PLANNED_COMPONENT_TRADE COMP_TRADE_B 
           INNER JOIN G_TRADE ON COMP_TRADE_B.TRADEID = G_TRADE.TradeId 
           WHERE COMP_TRADE_B.COMPONENTID  = COMP_TRADE_A.COMPONENTID
           ORDER BY COMP_TRADE_B.SORDER ASC
          FOR XML PATH('''')), 1, 2, '''')
          ,SUM (DURATION ) AS DURATION
			FROM	PLANNED_COMPONENT 
		LEFT JOIN PLANNED_COMPONENT_TRADE AS COMP_TRADE_A ON PLANNED_COMPONENT.COMPONENTID = COMP_TRADE_A.COMPONENTID 
		GROUP BY PLANNED_COMPONENT.COMPONENTID,COMP_TRADE_A.COMPONENTID)
		 as ComponentStat ON PLANNED_JOURNAL.COMPONENTID = ComponentStat.COMPONENTID
		 LEFT JOIN 
		(SELECT	PJ.JOURNALID AS JOURNALID,SUM(COALESCE(PA.DURATION,PCT.DURATION)) AS DURATION
		FROM	PLANNED_JOURNAL AS PJ
		INNER JOIN PLANNED_COMPONENT_TRADE AS PCT ON PJ.COMPONENTID = PCT.COMPONENTID
		LEFT JOIN PLANNED_APPOINTMENTS AS PA ON PJ.JOURNALID  = PA.JournalId AND PCT.COMPTRADEID = PA.COMPTRADEID AND PA.APPOINTMENTSTATUS = ''Not Started'' AND PA.ISPENDING = 1
		LEFT JOIN PLANNED_REMOVED_SCHEDULING_TRADES AS PRST ON PRST.JOURNALID = PJ.JOURNALID AND PRST.COMPTRADEID = PCT.COMPTRADEID 
		WHERE	PRST.JOURNALID IS NULL
		GROUP BY  PJ.JOURNALID) ComponentDuration on PLANNED_JOURNAL.JOURNALID = ComponentDuration.JOURNALID 		
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
		Outer Apply (Select SUM(Duration) as Duration, TRADES  = 
		STUFF((SELECT '', '' + G_TRADE.Description 
           FROM PLANNED_MISC_TRADE INNER JOIN G_TRADE ON G_TRADE.TradeId  = PLANNED_MISC_TRADE.TradeId
           WHERE PLANNED_MISC_TRADE.JOURNALID = pmt.JOURNALID
          FOR XML PATH('''')), 1, 2, '''') from PLANNED_MISC_TRADE AS pmt  Where pmt.JOURNALID = PLANNED_JOURNAL.JOURNALID group by pmt.JOURNALID) pmt '
		-- End building From clause
		--======================================================================================== 														  
		
				
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)' 	
			
		END
		
		IF(@sortColumn = 'Duration')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'DurationSort' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		--print(@finalQuery)
		print(@selectClause)
		print(@fromClause)
		print(@whereClause)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(MAX)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
		
END