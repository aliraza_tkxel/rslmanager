SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC [dbo].[RD_GetCountAssignToContractor]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx

-- =============================================
Create PROCEDURE [dbo].[RD_GetCountAssignToContractor]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	
	SET NOCOUNT ON;
	SELECT   count(*)  
	FROM FL_FAULT_LOG
		INNER JOIN (SELECT FL_FAULT_LOG.FaultLogID faultLogID
					FROM FL_FAULT_LOG
					WHERE FL_FAULT_LOG.StatusID=2) SubcontractorsFaults on FL_FAULT_LOG.FaultLogID=SubcontractorsFaults.faultLogID
		INNER JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		INNER JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		INNER JOIN S_ORGANISATION on FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID
		LEFT OUTER JOIN E__EMPLOYEE on FL_FAULT_LOG.UserID = E__EMPLOYEE.EMPLOYEEID
		INNER JOIN FL_FAULT_STATUS on  FL_FAULT_LOG.StatusID=FL_FAULT_STATUS.FaultStatusID
END




GO
