SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_PREINSPECTION_REPORT_ROWCOUNT] 

/* ===========================================================================
 '   NAME:           FL_REPORTEDFAULTSEARCHLIST
 '   DATE CREATED:   20TH February, 2009
 '   CREATED BY:     Tahir Gul 
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display the reported fault by the customer
 '   IN:             @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(	    
		-- These Parameters used to limit and sort records
		
		@noOfRows  int = 50,
		@offSet    int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID',
		@sortOrder varchar (5) = 'DESC',
		@UserId INT = null
				
	)
	
		
AS
	-- variables used to build whole query
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),
	        @OrderClause  varchar(8000),
	        @EMPLOYEE_FILTER VARCHAR(300) 
	


	IF @UserId<>NULL OR @userid <>''
		BEGIN
			SET @EMPLOYEE_FILTER = 'AND E__EMPLOYEE.EmployeeID=' +  CONVERT(VARCHAR(10),@USERID)
		End	        
	 ELSE
		BEGIN
			SET @EMPLOYEE_FILTER = ''
		END  
	

	        
	        
    -- Begin building SELECT clause
                              
                    SET @SelectClause = 'SELECT' +                      
                       CHAR(10) + CHAR(9) + 'COUNT(*) As numOfRows '       
 
    -- End building SELECT clause
    
       
    
    -- Begin building FROM clause
SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                    CHAR(10) + CHAR(9) + 'FL_FAULT_LOG INNER JOIN C__CUSTOMER ' +
                    CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID ' +
	         CHAR(10) + CHAR(9) + 'INNER JOIN C_CUSTOMERTENANCY ' + 
	         CHAR(10) + CHAR(9) + 'ON C__CUSTOMER.CustomerID = C_CUSTOMERTENANCY.CustomerID ' + 
	         CHAR(10) + CHAR(9) + 'INNER JOIN C_TENANCY  ' + 
	         CHAR(10) + CHAR(9) + 'ON C_CUSTOMERTENANCY.TenancyID = C_TENANCY.TENANCYID ' + 
	         CHAR(10) + CHAR(9) + 'INNER JOIN P__PROPERTY ' + 
	         CHAR(10) + CHAR(9) + 'ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID ' + 
					CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PREINSPECTIONINFO ' + 
					CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultLogID = FL_FAULT_PREINSPECTIONINFO.faultlogid ' +
					CHAR(10)  + CHAR(9) + 'INNER JOIN E__EMPLOYEE ' + 
					CHAR(10) + CHAR(9) + 'ON FL_FAULT_PREINSPECTIONINFO.USERID = E__EMPLOYEE.EmployeeID ' +
                    CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_STATUS ' +
                    CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID ' +
                    CHAR(10) + CHAR(9) + 'LEFT JOIN S_ORGANISATION ' +
                    CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID' +
                    CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ' +
                    CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID ' +
                    CHAR(10) + CHAR(9) + 'INNER JOIN FL_ELEMENT ' +
                    CHAR(10) + CHAR(9) + 'ON FL_FAULT.ElementID = FL_ELEMENT.ElementID ' +
                    CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PRIORITY ' +
                    CHAR(10) + CHAR(9) + 'ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID ' +
                    CHAR(10) + CHAR(9) + 'INNER JOIN FL_AREA ' +
                    CHAR(10) + CHAR(9) + 'ON FL_ELEMENT.AreaID = FL_AREA.AreaID ' 
	      
	      
                     
    -- End building FROM clause
                            
    
    -- Begin building OrderBy clause
    SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder
    -- End building OrderBy clause


    -- Begin building WHERE clause
    -- This Where clause contains subquery to exclude already displayed records 
    SET @WhereClause = CHAR(10)+  CHAR(10) + 'WHERE (' +
                        CHAR(10) + CHAR (9) + @sortColumn + ' NOT IN' + 
                        CHAR(10) + CHAR (9) + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' ' + @sortColumn +
                        + @FromClause + @OrderClause + ')'+
                        CHAR(10) + CHAR (9) + 'AND 1 = 1 AND FL_FAULT.PreInspection = 1' + ' AND ' + ' FL_FAULT_LOG.faultlogid IN (SELECT faultlogid FROM FL_FAULT_PREINSPECTIONINFO) '+
                         CHAR(10) + CHAR (9) + 'AND (FL_FAULT_LOG.STATUSID <> 11) and FL_FAULT.FAULTACTIVE=1 ' + 
                        CHAR(10) + CHAR (9) + 'AND (FL_FAULT_LOG.ORGID IS NULL OR FL_FAULT_PREINSPECTIONINFO.APPROVED IS NULL)) ' + @EMPLOYEE_FILTER
    -- End building WHERE clause
     
    -- End building WHERE clause


    PRINT (@SelectClause + @FromClause + @OrderClause)

    
    EXEC (@SelectClause + @FromClause + @WhereClause)
    
    

RETURN


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON



GO
