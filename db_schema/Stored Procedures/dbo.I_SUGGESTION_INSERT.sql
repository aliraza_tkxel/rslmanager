SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_SUGGESTION_INSERT]
(
	@UserID int,
	@DateSubmitted smalldatetime,
	@Topic varchar(50),
	@Suggestion varchar(1500),
	@Action varchar(50)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [I_SUGGESTIONS] ([UserID], [DateSubmitted], [Topic], [Suggestion], [Action]) VALUES (@UserID, @DateSubmitted, @Topic, @Suggestion, @Action);
	
SELECT SuggestionID, UserID, DateSubmitted, Topic, Suggestion, Action FROM I_SUGGESTIONS WHERE (SuggestionID = SCOPE_IDENTITY())
GO
