SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PSA_GetOperativesWithOverdueAppointment]

/* =================================================================================    
    Page Description: Available Appointments
 
    Author: Aamir Waheed
    Creation Date: Dec-19-2014

    Change History:

    Version      Date           By                  Description
    =======     ============    ========			===========================
    v1.0         Jan-20-2015    Aamir Waheed		Get the operatives having overdue appointments
  =================================================================================*/
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here
DECLARE @PushNotificationAfter NVARCHAR(2000)

SELECT
	@PushNotificationAfter = ISNULL(
	(
		SELECT
			DEFAULTVALUE
		FROM
			RSL_DEFAULTS
		WHERE
			DEFAULTNAME = 'PushNotificationAfter'
	)
	, '30')

DECLARE @DateToCompare DATETIME
SET @DateToCompare = DATEADD(MINUTE, -CAST(@PushNotificationAfter AS NUMERIC), GETDATE())

WITH
	overDueOperatives
	(
		OPERATIVEID)
	AS
		(
			SELECT
				OperativeID
			FROM
				FL_CO_APPOINTMENT A
			WHERE
				LOWER(AppointmentStatus) NOT IN ('complete', 'completed', 'noentry', 'no entry')
				AND CONVERT(DATETIME, COALESCE(A.AppointmentEndDate, A.AppointmentDate) + EndTime) <= @DateToCompare UNION SELECT
				ASSIGNEDTO
			FROM
				AS_APPOINTMENTS A
			WHERE
				LOWER(APPOINTMENTSTATUS) NOT IN ('finished', 'noentry', 'no entry')
				AND CONVERT(DATETIME, APPOINTMENTDATE + APPOINTMENTENDTIME) <= @DateToCompare UNION SELECT
				ASSIGNEDTO
			FROM
				PLANNED_APPOINTMENTS
			WHERE
				LOWER(APPOINTMENTSTATUS) NOT IN ('complete', 'completed', 'noentry', 'no entry')
				AND CONVERT(DATETIME, APPOINTMENTDATE + APPOINTMENTENDTIME) <= @DateToCompare UNION SELECT
				AC.EMPLOYEEID
			FROM
				PS_Appointment A
					INNER JOIN AC_LOGINS AC ON AC.LOGIN = A.SurveyourUserName
			WHERE
				LOWER(A.AppointProgStatus) NOT IN ('finished', 'noentry', 'no entry')
				AND CONVERT(DATETIME, AppointEndDateTime) <= @DateToCompare
		)

SELECT
	O.OPERATIVEID
	,AC.DEVICETOKEN
FROM
	overDueOperatives O
		INNER JOIN AC_LOGINS AC ON AC.EMPLOYEEID = O.OPERATIVEID
WHERE
	ISNULL(AC.DEVICETOKEN,'') NOT IN ('0', '', '(null)')
END
GO
