USE [RSLBHALive]
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'PDR_PurchaseOrderContractorDetails'))
  DROP PROCEDURE PDR_PurchaseOrderContractorDetails

 GO
-- =============================================
-- Author:           Muhammad Uzzair
-- Create date:      21/10/2015
-- Description:      get Purchase Order Data
--Modified by :		 Raja Aneeq
-- Modified date:      2/11/2015
-- Description:      introduce two new joins and get asbestos data
-- EXEC PDR_PurchaseOrderContractorDetails 265336 
-- =============================================
CREATE PROCEDURE [dbo].[PDR_PurchaseOrderContractorDetails] 
	-- Add the parameters for the stored procedure here
	 
	@orderId int
AS
BEGIN 
	
	SET NOCOUNT ON;

	
select PO.ORDERID,
'PO ' +RIGHT('00000'+ CONVERT(VARCHAR,PO.ORDERID),6) AS PORef
,FL.JobSheetNumber AS JSRef,
--,'JS' +RIGHT('00000'+ CONVERT(VARCHAR,CW.JournalId),6) AS JSRef,
J.FaultLogID As FaultLogId
,FLS.Description As PoStatus,
O.NAME,contact.FIRSTNAME+' '+ contact.LASTNAME as ContactPerson
,C.WorkDD as DirectDial 
,CA.Tel as Telephone,
ISNULL(PR.HouseNumber,'') +' '+ ISNULL(PR.ADDRESS1,'') +' '+ ISNULL(PR.ADDRESS2,'') +' '+ ISNULL(PR.ADDRESS3,'')+''+ISNULL('</br>'+PR.TOWNCITY ,'') +' </br>'+ISNULL(PR.POSTCODE  ,'') as Address,
		ISNULL(PR.HOUSENUMBER, '') AS HOUSENUMBER,		
		ISNULL(PR.ADDRESS1,'') as ADDRESS1,
		ISNULL(PR.ADDRESS2,'') as ADDRESS2,
		ISNULL(PR.TOWNCITY,'') as TOWNCITY,
		ISNULL(PR.COUNTY,'') as COUNTRY,
		ISNULL(PR.POSTCODE,'') as POSTCODE
,E.FIRSTNAME+' '+ E.LASTNAME as OrderedBy
,C.WORKEMAIL as Email,
ISNULL(CA.HouseNumber,'') +' '+ ISNULL(CA.ADDRESS1,'') +' '+ ISNULL(CA.ADDRESS2,'') +' '+ ISNULL(CA.ADDRESS3,'')+ISNULL(', '+CA.TOWNCITY ,'') +' '+ISNULL(CA.POSTCODE  ,'') as TenantAddress,
ISNULL(CUS.FIRSTNAME,'') +' ' +' '+ ISNULL(CUS.LASTNAME,'') as TenantName

FROM F_PURCHASEORDER PO
Inner join FL_CONTRACTOR_WORK CW  ON PO.ORDERID = CW.PurchaseORDERID
inner Join S_ORGANISATION O ON CW.ContractorId = O.ORGID
--inner Join E__EMPLOYEE contact ON CW.AssignedBy= contact.EMPLOYEEID
inner Join E__EMPLOYEE contact ON CW.ContactId= contact.EmployeeId
left join E_CONTACT C on contact.EmployeeId= C.EmployeeId 
INNER JOIN FL_FAULT_JOURNAL J ON CW.JournalId=J.JournalId
Inner join FL_FAULT_LOG FL ON  J.FaultLogID  = FL.FaultLogID
INNER JOIN FL_FAULT_STATUS FLS ON FLS.FaultStatusId = FL.StatusId
Inner join P__Property PR ON FL.PROPERTYID = PR.PROPERTYID 
INNER JOIN E__EMPLOYEE E ON CW.AssignedBy = E.EMPLOYEEID
Cross Apply (Select MAX(TenancyId)as TenancyId from C_TENANCY T where T.PROPERTYID = PR.PROPERTYID) Ten   
LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = Ten.TENANCYID 
										AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=Ten.TENANCYID 
																					)
		LEFT JOIN C__CUSTOMER  CUS ON CUS.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
		LEFT JOIN C_ADDRESS CA ON CA.CUSTOMERID = CUS.CUSTOMERID AND CA.ISDEFAULT = 1 
		
		
Where PO.ORDERID=@orderId




DECLARE @CUSTOMERID INT, @VULHISTORYID INT
DECLARE @propertyId nvarchar(20)
SELECT  @CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID,
@propertyId=P__PROPERTY.PROPERTYID
		
FROM	F_PURCHASEORDER  PO
		Inner join FL_CONTRACTOR_WORK CW  ON PO.ORDERID = CW.PurchaseORDERID
		INNER JOIN FL_FAULT_JOURNAL J ON CW.JournalId=J.JournalId
		Inner join FL_FAULT_LOG FL ON   J.FaultLogID   = FL.FaultLogID
		INNER JOIN  P__PROPERTY ON  FL.PROPERTYID = P__PROPERTY.PROPERTYID  
		Cross Apply (Select MAX(TenancyId)as TenancyId from C_TENANCY T where T.PROPERTYID = P__PROPERTY.PROPERTYID) Ten   
		LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = Ten.TENANCYID 
										AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=Ten.TENANCYID 
																					)
		LEFT JOIN C__CUSTOMER  CUS ON CUS.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
		
WHERE   PO.ORDERID= @orderId
 


--=========================================================	
-- RISK INFO
--=========================================================	


SELECT RISKHISTORYID,CATDESC,SUBCATDESC FROM dbo.RISK_CATS_SUBCATS (@CUSTOMERID)




--=========================================================	
-- VULNERABILITY INFO
--=========================================================	

SELECT	@VULHISTORYID=VULNERABILITYHISTORYID
FROM	C_JOURNAL J 
		INNER JOIN C_VULNERABILITY CV ON CV.JOURNALID = J.JOURNALID 
WHERE	CUSTOMERID = @CUSTOMERID   AND ITEMNATUREID=61 
		AND CV.VULNERABILITYHISTORYID = (	SELECT MAX(VULNERABILITYHISTORYID) 
											FROM C_VULNERABILITY IN_CV 
											WHERE IN_CV.JOURNALID=J.JOURNALID ) 
		AND CV.ITEMSTATUSID <> 14 
		
		
		
		
		
EXECUTE VULNERABILITY_CAT_SUBCAT @VULHISTORYID

--=======================================================
-- Work Required
--=======================================================

 Select DISTINCT CWD.WorkRequired as WorksRequired,
FL.Description as  ReportedFault,
CWD.FaultWOrkDetailId,
CWD.NetCost as NetCost,
CWD.Vat as Vat,
CWD.Gross as Total,
CW.AssignedDate as DueDate,
Cw.Estimate as EstimatedCompletion,
totals.TotalNetCost,
totals.TotalVAT,
totals.TotalGross
 from F_PURCHASEORDER PO
 INNER JOIN F_PURCHASEITEM  PI ON PO.ORDERID = PI.ORDERID
 INNER JOIN FL_CONTRACTOR_WORK CW  ON PO.ORDERID = CW.PurchaseORDERID 
 INNER JOIN FL_CONTRACTOR_WORK_DETAIL CWD ON CW.FaultContractorId= CWD.FaultContractorId
 INNER JOIN FL_FAULT_JOURNAL J ON CW.JournalId=J.JournalId
 Inner join FL_FAULT_LOG FLG ON   J.FaultLogID   = FLG.FaultLogID
 INNER JOIN FL_FAULT FL ON FLG.FaultID = FL.FaultId
 
 INNER JOIN
 (
	SELECT FaultContractorId,	SUM(NetCost) AS TotalNetCost ,SUM(VAT) AS TotalVAT   ,SUM(Gross) AS TotalGross     
	FROM	FL_CONTRACTOR_WORK_DETAIL 
	GROUP BY     FaultContractorId
 )AS totals
	ON totals.FaultContractorId = CW.FaultContractorId 
 Where PO.ORDERID=@orderId
 ORDER BY CWD.FaultWOrkDetailId ASC



--=======================================================
-- Asbestos Data
--=======================================================

EXEC AS_GetAsbestosInformation @propertyId
END