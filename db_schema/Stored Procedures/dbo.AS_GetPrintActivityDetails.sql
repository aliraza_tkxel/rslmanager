
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	AS_GetPrintActivityDetails		@propertyId = N'A010060001'
-- Author:		<Salman Nazir>
-- Create date: <30/11/2012>
-- Description:	<Get Print Activity Details>
-- WebPage: PropertyRecord.aspx => Activities Tab
-- Modification: <April 19, 2013, Aamir Waheed - Added house no to address and replace 0 with '' in ISNULL() Function>
-- =============================================

CREATE PROCEDURE [dbo].[AS_GetPrintActivityDetails] 
(
@propertyId varchar (500)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
SELECT  (HOUSENUMBER + ' '+  ISNULL(ADDRESS1,'')+' ' + ISNULL(ADDRESS2,'')+' ' + ISNULL(ADDRESS3,'')) AS ADDRESS
		,POSTCODE
		,DEVELOPMENTNAME AS Dev
		,SCHEMENAME AS SCHEME
		,CASE 
			WHEN P_STATUS.DESCRIPTION = 'Let' 
				THEN P_STATUS.DESCRIPTION + ' ( ' +
					ISNULL(STUFF((SELECT DISTINCT (' and ' + RTRIM(LTRIM(ISNULL(TIT.DESCRIPTION, '') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,''))))
									FROM C_TENANCY T						
									INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID
									INNER JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID
									INNER JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE
									INNER JOIN C_ADDITIONALASSET A ON A.TENANCYID = T.TENANCYID AND (A.ENDDATE > GETDATE() OR A.ENDDATE IS NULL)
									WHERE (T.PROPERTYID = P__PROPERTY.PROPERTYID OR A.PROPERTYID = P__PROPERTY.PROPERTYID)
										AND (  (CT.ENDDATE > GETDATE() OR CT.ENDDATE IS NULL )
										AND (T.ENDDATE > GETDATE() OR T.ENDDATE IS NULL))
									FOR XML PATH('')
						   ),1,5,''), 'Tenant info N/A')
						+  ' ) '
			ELSE P_STATUS.DESCRIPTION END  As STATUS
		,P_PROPERTYTYPE.DESCRIPTION AS TYPE
		,P__PROPERTY.PROPERTYID As ReferenceNumber
FROM P__PROPERTY
	INNER JOIN P_PROPERTYTYPE on P_PROPERTYTYPE.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	INNER JOIN P_DEVELOPMENT on P_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
	INNER JOIN P_STATUS on P_STATUS.StatusId = P__PROPERTY.STATUS
	
WHERE P__PROPERTY.PROPERTYID = @propertyId
END
GO
