USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_SaveRestrictions]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_SaveRestrictions] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_SaveRestrictions] 
	-- Add the parameters for the stored procedure here
	@RestrictionId INT,
	@PermittedPlanning nvarchar(500),
	@RelevantPlanning nvarchar(500),
	@RelevantTitle nvarchar(1000),
	@RestrictionComments nvarchar(1000),
	@AccessIssues nvarchar(1000),
	@MediaIssues nvarchar(1000),
	@ThirdPartyAgreement nvarchar(10),
	@SpFundingArrangements nvarchar(1000),
	@IsRegistered INT,
	@ManagementDetail nvarchar(1000),
	@NonBhaInsuranceDetail nvarchar(1000),
	@BlockId INT = NULL,	
	@SchemeId INT = NULL,	
	@PropertyId nvarchar(20) = NULL,
	@UpdatedBy INT,	
	@IsSaved BIT = 0 OUTPUT,
	@RestrictionIdOut INT OUTPUT
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRANSACTION
BEGIN TRY

if(@IsRegistered < = 0)
BEGIN
	SET @IsRegistered = NULL
END


IF (@RestrictionId < = 0)
BEGIN

		INSERT INTO [PDR_Restriction]
			   ([PermittedPlanning],[RelevantPlanning],[RelevantTitle],[RestrictionComments]
			   ,[AccessIssues],[MediaIssues],[ThirdPartyAgreement],[SpFundingArrangements] 
			   ,[IsRegistered],[ManagementDetail],[NonBhaInsuranceDetail],[PropertyId],[SchemeId],[BlockId],[CreatedBy],[CreationDate])
		VALUES
			   (@PermittedPlanning,@RelevantPlanning,@RelevantTitle,@RestrictionComments,@AccessIssues,@MediaIssues
			   ,@ThirdPartyAgreement,@SpFundingArrangements,@IsRegistered,@ManagementDetail,@NonBhaInsuranceDetail,@PropertyId,@SchemeId,@BlockId,@UpdatedBy,GETDATE())
		SET @RestrictionId = SCOPE_IDENTITY()   
	            
END
ELSE
BEGIN
		UPDATE [PDR_Restriction]
		SET 
		[PermittedPlanning] = @PermittedPlanning
		,[RelevantPlanning] = @RelevantPlanning
		,[RelevantTitle] = @RelevantTitle
		,[RestrictionComments] = @RestrictionComments
		,[AccessIssues] = @AccessIssues
		,[MediaIssues] = @MediaIssues
		,[ThirdPartyAgreement] = @ThirdPartyAgreement
		,[SpFundingArrangements] = @SpFundingArrangements
		,[IsRegistered] = @IsRegistered
		,[ManagementDetail] = @ManagementDetail
		,[NonBhaInsuranceDetail] = @NonBhaInsuranceDetail
		,[CreatedBy] = @UpdatedBy
		,[CreationDate] = GETDATE()
		WHERE RestrictionId = @RestrictionId
END

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @RestrictionIdOut = @RestrictionId

END
