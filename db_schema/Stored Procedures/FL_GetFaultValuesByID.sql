USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ===========================================================================
--	EXEC FL_GetFaultValuesByID
		@FaultID = 1265
--	CREATE DATE:	1 March 2013
--	Author:			Aamir Waheed--  
--	Description:	To get the values of a fault selected by faultID.
--	Webpage:		View/Reports/ReportArea.aspx (For Amend Fault Modal Popup)
 '==============================================================================*/
 
IF OBJECT_ID('dbo.FL_GetFaultValuesByID') IS NULL 
 EXEC('CREATE PROCEDURE dbo.FL_GetFaultValuesByID AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[FL_GetFaultValuesByID]
 
@FaultID as Int
AS

--=============================
-- Fault General Information
--=============================

SELECT  FL_Fault.FAULTID AS FaultID,
		AreaID,		
		FL_FAULT.Description AS Description,
		PriorityID,
		duration,
		Recharge,
		NetCost,
		VatRateID,
		Vat,
		Gross,
		isContractor,
		FaultActive,
		IsGasSafe,
		IsOftec
FROM    FL_FAULT 
WHERE   FL_Fault.FaultID = @FaultID

--=============================
-- Fault Trades Information
--=============================
SELECT  FaultTradeId
		,G_TRADE.TradeId AS TradeId
		,G_TRADE.Description AS TradeName
		,0 AS IsDeleted 		
FROM    FL_FAULT_TRADE  
		LEFT OUTER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
WHERE   FL_FAULT_TRADE.FaultID = @FaultID

--=============================
-- Fault Repairs Information
--=============================

SELECT	FaultRepairId AS faultRepairId
		,ISNULL(FL_FAULT_ASSOCIATED_REPAIR.RepairId,-1) AS repairId
		,ISNULL(FL_FAULT_REPAIR_LIST.Description,'N/A') as repair
		,0 AS isDeleted 
FROM	FL_FAULT_ASSOCIATED_REPAIR		
		LEFT OUTER JOIN FL_FAULT_REPAIR_LIST ON FL_FAULT_ASSOCIATED_REPAIR.RepairId = FL_FAULT_REPAIR_LIST.FaultRepairListID
WHERE	FL_FAULT_ASSOCIATED_REPAIR.FaultId = @FaultID