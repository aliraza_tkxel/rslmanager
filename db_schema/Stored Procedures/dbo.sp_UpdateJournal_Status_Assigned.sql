SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateJournal_Status_Assigned]

@AssignTo int,
@JobID int

AS

update h_request_journal set assigned_to = @AssignTo WHERE JOB_ID = @JobID

GO
