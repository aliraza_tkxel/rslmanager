SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Simon Rogers
-- Create date: 1st October 2014
-- Description:	Stored Procedure to CancelCyclicalOrders
-- =============================================
CREATE PROCEDURE diagnostics.CancelCyclicalOrders
	-- Add the parameters for the stored procedure here
	@JournalId INT, 
	@SystemsEmployeeID INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--+---------------------------------------------------------------
	--+ Update Customer Journal
	--+---------------------------------------------------------------
	SELECT  *
	FROM    dbo.C_JOURNAL
	WHERE   JOURNALID = @JournalId

	UPDATE  dbo.C_JOURNAL
	SET     CURRENTITEMSTATUSID = 5
	WHERE   JOURNALID = @JournalId

	SELECT  *
	FROM    dbo.C_JOURNAL
	WHERE   JOURNALID = @JournalId

	--+---------------------------------------------------------------
	--+ Update Purchase Item
	--+---------------------------------------------------------------
	SELECT  *
	FROM    dbo.F_PURCHASEITEM
			INNER JOIN P_WOTOREPAIR WTR ON ( dbo.F_PURCHASEITEM.ORDERITEMID = WTR.ORDERITEMID )
	WHERE   WTR.JOURNALID = @JournalId

	UPDATE  F_PURCHASEITEM
	SET     ACTIVE = 0
	FROM    F_PURCHASEITEM PI ,
			P_WOTOREPAIR WTR
	WHERE   PI.ORDERITEMID = WTR.ORDERITEMID
			AND WTR.JOURNALID = @JournalId

	SELECT  *
	FROM    dbo.F_PURCHASEITEM
			INNER JOIN P_WOTOREPAIR WTR ON ( dbo.F_PURCHASEITEM.ORDERITEMID = WTR.ORDERITEMID )
	WHERE   WTR.JOURNALID = @JournalId

	--+---------------------------------------------------------------
	--+ Update Purchase Work Order
	--+---------------------------------------------------------------
	DECLARE @wold AS INT = ( SELECT WOID
							 FROM   dbo.F_PURCHASEITEM
									INNER JOIN P_WOTOREPAIR WTR ON ( dbo.F_PURCHASEITEM.ORDERITEMID = WTR.ORDERITEMID )
							 WHERE  WTR.JOURNALID = @JournalId
						   )

	SELECT  *
	FROM    P_WORKORDER
	WHERE   woid = @wold

	UPDATE  P_WORKORDER
	SET     WOSTATUS = 5
	WHERE   woid = @wold

	SELECT  *
	FROM    P_WORKORDER
	WHERE   woid = @wold


	--+---------------------------------------------------------------
	--+ Update Purchase Item Log
	--+---------------------------------------------------------------.
	DECLARE @orderid AS INT	= ( SELECT  ORDERID
								FROM    P_WORKORDER
								WHERE   woid = @wold
							  )

	SELECT * FROM dbo.F_PURCHASEORDER_LOG WHERE ORDERID = @orderid

	INSERT  INTO F_PURCHASEORDER_LOG
			(IDENTIFIER,
			ORDERID,
			ACTIVE,
			POTYPE,
			POSTATUS, 
			TIMESTAMP,
			ACTIONBY,
			ACTION)
			SELECT  replace(convert(NVARCHAR, getdate(), 103), '/', '') + replace(convert(NVARCHAR, getdate(), 108), ':', '') + '_' + CONVERT(NVARCHAR, @SystemsEmployeeID) ,
					ORDERID ,
					ACTIVE ,
					POTYPE ,
					POSTATUS ,
					GETDATE() ,
					@SystemsEmployeeID,
					'AUTOMATED REPAIR STATUS CANCEL'
			FROM    F_PURCHASEORDER
			WHERE   ORDERID = @orderid 

	SELECT * FROM dbo.F_PURCHASEORDER_LOG WHERE ORDERID = @orderid

	SELECT * FROM dbo.F_PURCHASEORDER WHERE ORDERID = @orderid 
	UPDATE F_PURCHASEORDER SET ACTIVE = 0 WHERE ORDERID = @orderid
	SELECT * FROM dbo.F_PURCHASEORDER WHERE ORDERID = @orderid 

	--+---------------------------------------------------------------
	--+ UPDATE 
	--+---------------------------------------------------------------

	DECLARE @repairid AS INT = ( SELECT MAX(REPAIRHISTORYID)
								 FROM   dbo.C_REPAIR
								 WHERE  JOURNALID = @JournalId
								 GROUP BY JOURNALID
							   )

	SELECT * FROM dbo.C_REPAIR WHERE REPAIRHISTORYID = @repairid

	UPDATE C_REPAIR SET ITEMSTATUSID = 5, ITEMACTIONID = 11, NOTES = 'CANCELLED BY IST TEAM ON REQUEST'
	WHERE REPAIRHISTORYID = @repairid

	SELECT * FROM dbo.C_REPAIR WHERE REPAIRHISTORYID = @repairid  

END
GO
