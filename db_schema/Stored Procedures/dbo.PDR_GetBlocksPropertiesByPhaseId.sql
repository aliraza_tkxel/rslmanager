USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocksPropertiesByPhaseId]    Script Date: 27-Feb-17 10:39:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
Page Description: Create Scheme Page 
 
Author: Salman Nazir
Creation Date: Jan-07-2015

Change History:

Version      Date             By                      Description
=======     ============    ========           ===========================
v1.0         Jan-07-2015      Salman         Get Blocks and Properties by PhaseId 
=================================================================================*/
-- Exec PDR_GetBlocksPropertiesByPhaseId 0, 203
-- =============================================

IF OBJECT_ID('dbo.PDR_GetBlocksPropertiesByPhaseId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetBlocksPropertiesByPhaseId AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetBlocksPropertiesByPhaseId]
@phaseId int = -1,
@developmentId int = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @whereClause NVarchar(1000) = '';
	DECLARE @blockQuery NVarchar(1000) = '';
	DECLARE @propertyQuery NVarchar(1000) = '';

	IF(@phaseId != -1)
		BEGIN
			Set @whereClause = @whereClause + ' WHERE PhaseId = ' + CONVERT(NVARCHAR, @phaseId)
		END
	ELSE IF(@phaseId = -1 AND @developmentId != -1)
		BEGIN
			Set @whereClause = @whereClause + ' WHERE PhaseId IN  (	Select B.PHASEID from P_PHASE B Where B.DEVELOPMENTID = ' + CONVERT(NVARCHAR, @developmentId) + ' )'

		END
	-- Insert statements for procedure here
	Set @blockQuery = 'Select BLOCKID,ISNULL(BLOCKNAME,'' '') as BlockName from P_BLOCk ' + @whereClause
	Set @propertyQuery = 'Select PROPERTYID,(ISNULL(HOUSENUMBER,'' '')+'' ''+ISNULL(ADDRESS1,'' '')) PropertyAddress from P__PROPERTY ' + @whereClause
	
	PRINT @blockQuery    
	EXEC (@blockQuery) 
	
	PRINT @propertyQuery    
	EXEC (@propertyQuery) 
END