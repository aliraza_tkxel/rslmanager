USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[AS_GetSchemeBlockDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetSchemeBlockDetail] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetSchemeBlockDetail](
	@Id int,
	@RequestType varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @RequestType = 'Scheme' 
	begin
		Select 
			-1 as TenancyId,		
			'N/A' as TenantName,
			P_SCHEME.SCHEMENAME as Address,
			'' AS HOUSENUMBER,		
			P_SCHEME.SCHEMENAME as ADDRESS1,
			'' as ADDRESS2,
			'' as TOWNCITY,
			'' as COUNTY,
			'' as POSTCODE,
			'' as TenantNameSalutation,
			'' as Mobile,
			'' as Telephone,
			ISNULL(P_SCHEME.SCHEMENAME,'') as SchemeName,
			'' as Email,
			-1 as CustomerId,
			''  AS Block

		From		
			P_SCHEME 

		WHERE P_SCHEME.SCHEMEID = @Id
	end
	else IF @RequestType = 'Block' 
	begin
		Select 
			-1 as TenancyId,		
			'N/A' as TenantName,
			ISNULL(P_Block.BLOCKNAME, '') + ' ' + ISNULL(P_Block.ADDRESS1, '') + ' ' + ISNULL(P_Block.ADDRESS2, '') + ' ' + ISNULL(P_Block.ADDRESS3, '') as Address,
			'' AS HOUSENUMBER,		
			P_SCHEME.SCHEMENAME as ADDRESS1,
			'' as ADDRESS2,
			'' as TOWNCITY,
			'' as COUNTY,
			'' as POSTCODE,
			'' as TenantNameSalutation,
			'' as Mobile,
			'' as Telephone,
			ISNULL(P_SCHEME.SCHEMENAME,'') as SchemeName,
			'' as Email,
			-1 as CustomerId,
			 ISNULL(P_Block.BLOCKNAME, '') + ' ' + ISNULL(P_Block.ADDRESS1, '') + ' ' + ISNULL(P_Block.ADDRESS2, '') + ' ' + ISNULL(P_Block.ADDRESS3, '') AS Block

		From		
			P_BLOCK
			INNER JOIN P_SCHEME ON P_SCHEME.SCHEMEID = P_BLOCK.SchemeId 

		WHERE P_BLOCK.BLOCKID = @Id
	end
END
