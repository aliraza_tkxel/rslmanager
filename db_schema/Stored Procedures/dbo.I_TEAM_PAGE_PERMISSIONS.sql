
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[I_TEAM_PAGE_PERMISSIONS]
@PAGEID INT
AS
SET NOCOUNT ON;

WITH    CTE_PAGE_TEAM ( PageId, TeamId )
          AS ( SELECT DISTINCT
                        TJ.PageId ,
                        jrt.TeamId
               FROM     DBO.I_PAGE_TEAMJOBROLE tj
                        INNER JOIN dbo.E_JOBROLETEAM jrt ON tj.TeamJobRoleId = jrt.JobRoleTeamId
               WHERE    jrt.isDeleted = 0
                        AND jrt.IsActive = 1
             )
    SELECT  T.TEAMID ,
            T.CREATIONDATE ,
            T.TEAMNAME ,
            T.MANAGER ,
            T.DIRECTOR ,
            T.MAINFUNCTION ,
            T.DESCRIPTION ,
            T.ACTIVE ,
            ( SELECT    COUNT(*)
              FROM      CTE_PAGE_TEAM PT
              WHERE     PT.TEAMID = T.TEAMID
                        AND PT.PAGEID = @PAGEID
            ) AS HASPERMISSION
    FROM    E_TEAM T
    WHERE   T.ACTIVE = 1

--SELECT     T.TEAMID, T.CREATIONDATE, T.TEAMNAME, T.MANAGER, T.DIRECTOR, T.MAINFUNCTION, T.DESCRIPTION, T.ACTIVE, 
--(SELECT COUNT(*) FROM I_PAGE_TEAM PT WHERE PT.TEAMID = T.TEAMID AND PT.PAGEID = @PAGEID) AS HASPERMISSION
--FROM         E_TEAM T
--WHERE		T.ACTIVE = 1


GO
