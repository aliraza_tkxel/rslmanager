SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_G_LOCALAUTHORITY_GetLookUpValue
/* ===========================================================================
 '   NAME:           TO_G_LOCALAUTHORITY_GetLookUpValue
 '   DATE CREATED:   27 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get LOCALAUTHORITY lookup value from LOCALAUTHORITY table which will be shown
 '					 as lookup value on move house page
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT LOCALAUTHORITYID AS id,DESCRIPTION AS val
	FROM G_LOCALAUTHORITY
	ORDER BY DESCRIPTION ASC

GO
