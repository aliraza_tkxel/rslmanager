USE [RSLBHALive]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_WARNINGS  OFF
GO
/* =============================================
-- Author:		Junaid Nadeem
-- Create date: 18 January 2017
-- Description:	To reverse General Journals That have reversal date set as today and making a new Journal which
--				performs the opposite posting from the original journal.
-- EXEC	[dbo].[AC_RevertGeneralJournal] @GeneralJournalID = 5055
-- ============================================= */

IF OBJECT_ID('dbo.[PLANNED_GetContactDropDownValuesbyContractorId]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetContactDropDownValuesbyContractorId] AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 22/10/2014
-- Description:	Get the employeeId and employeeName as contacts of the organization.
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetContactDropDownValuesbyContractorId] 
	-- Add the parameters for the stored procedure here
	@contractorId INT

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here

SELECT
	E.EMPLOYEEID AS [id],
    ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') AS [description]
    FROM E__EMPLOYEE E
    INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID AND JD.ACTIVE = 1
    WHERE E.ORGID = @contractorId
END
