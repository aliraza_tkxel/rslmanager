SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[INSERT_NL_MISCELLANEOUS_DTL](
	@hdrid as int, 
	@NlAccount int,
	@NetAmount money,
	@Details varchar(250)
	)
AS 
BEGIN

	declare @Gross money
	set @Gross=@NetAmount+ @NetAmount/100*17.5
	INSERT INTO NL_Miscellaneous_dtl( hdrid, NlAccount, NetAmount, Gross, Details)
	VALUES( @hdrid, @NlAccount, @NetAmount, @Gross, @Details)

END

GO
