
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC FL_GetPatchID_TradeID_ByAppointmentID @AppointmentID = 64248
-- Author:		<Aamir Waheed>
-- Create date: <22/08/2013>
-- Description:	<Get PatchID and TradeID to show appropriate calendar for the current appointment.>
-- Webpage : SchedulingCalendar.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetPatchID_TradeID_ByAppointmentID] 
@AppointmentID int

AS
BEGIN
SELECT DISTINCT
		ISNULL(E_JOBDETAILS.PATCH, 0) AS PatchID,
		ISNULL(FL_FAULT_TRADE.TradeId, 0) AS TradeID
		
FROM FL_CO_APPOINTMENT
--Thes join is for patch id
INNER JOIN E_JOBDETAILS ON FL_CO_APPOINTMENT.OperativeID = E_JOBDETAILS.EMPLOYEEID

--These joins are for trade id
INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId
INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
INNER JOIN FL_FAULT_TRADE ON FL_FAULT_LOG.FaultTradeId = FL_FAULT_TRADE.FaultTradeId

WHERE FL_CO_APPOINTMENT.AppointmentID = @AppointmentID
END
GO
