SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE dbo.FL_CO_GET_FAULT_REPORTS_COUNT 
/* ===========================================================================
 '   NAME:           FL_CO_GET_FAULT_REPORTS_COUNT
 '   DATE CREATED:   16TH Feb 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist faults related to contractor and specific to status
 '   IN:             orgId, fault status					                      
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria	   
		@orgId		int = NULL,
		@faultStatus int= 0

	)
	
		
AS
	
IF @faultStatus = 0

	SELECT COUNT(*) As numOfRows
	FROM FL_FAULT_LOG AS F 
	INNER JOIN FL_FAULT_STATUS AS FS ON F.StatusID = FS.FaultStatusID
	INNER JOIN FL_FAULT AS FD ON F.FaultID = FD.FaultID
	INNER JOIN FL_FAULT_PRIORITY AS FP ON FD.PriorityID = FP.PriorityID
	WHERE F.OrgID = @orgId 

	
ELSE
	
	SELECT COUNT(*) As numOfRows
	FROM FL_FAULT_LOG AS F 
	INNER JOIN FL_FAULT_STATUS AS FS ON F.StatusID = FS.FaultStatusID
	INNER JOIN FL_FAULT AS FD ON F.FaultID = FD.FaultID
	INNER JOIN FL_FAULT_PRIORITY AS FP ON FD.PriorityID = FP.PriorityID
	WHERE F.OrgID = @orgId 
	AND F.StatusID=@faultStatus 










GO
