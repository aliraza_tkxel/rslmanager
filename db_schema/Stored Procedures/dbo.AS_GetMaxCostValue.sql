USE [RSLBHALive ]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_GetMaxCostValue'))
  DROP PROCEDURE AS_GetMaxCostValue

GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 2/3/2016
-- Description:	Get Max value of cost
-- EXEC 
-- =============================================
CREATE PROCEDURE AS_GetMaxCostValue
	-- Add the parameters for the stored procedure here
	@ExpenditureId INT,
	@MaxAmount INT OUTPUT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
    DECLARE @val INT 
    DECLARE @standardVatRate int   
    
    SELECT @standardVatRate  = VatRate  FROM F_VAT WHERE VATNAME= 'Standard'
    SELECT @val=
    MAX(isnull(LIMIT,0)) FROM 
     F_EMPLOYEELIMITS EL INNER JOIN E__EMPLOYEE E ON 
     E.EMPLOYEEID = EL.EMPLOYEEID WHERE EXPENDITUREID =   28  
     SET @val = @val-(@val*(@standardVatRate))/100
   SET @MaxAmount = @val 
     
END
GO
