
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetAction
-- Author:		<Salman Nazir>
-- Create date: <09/23/2012>
-- Description:	<This Stored Proceedure gets Actions on Status Page >
-- Web Page: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetAction]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ActionId ,StatusId  ,Title, Ranking,IsEditable
	FROM AS_Action
END
GO
