SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FL_CUSTOMER_RISK]
	/*	===============================================================
	'   NAME:           FL_CUSTOMER_RISK
	'   DATE CREATED:   28 APR 2009
	'   CREATED BY:    Adnan Mirza
	'   CREATED FOR:    Broadland Housing, Tenants Online
	'   PURPOSE:        To retrieve the Customer Disability information to be displayed on My Details page
	'   IN:             @CustomerId 
	'   OUT: 		    No     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@CustomerId INT
	)
AS

--SELECT RISKDESC FROM C_PERSONALRISK WHERE CUSTOMERID IN (@CustomerId)
SELECT CATDESC,SUBCATDESC FROM dbo.RISK_CATS_SUBCATS(@CustomerId)
GO
