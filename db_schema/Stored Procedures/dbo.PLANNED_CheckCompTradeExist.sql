SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- EXEC PLANNED_GetPropertyDetail
	--@propertyId = 'A010060001'

-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,30/Oct/2013>
-- Description:	<Description,,PLANNED_GetPropertyDetail >
-- Web Page: ReplacementList.aspx => Get property information 
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_CheckCompTradeExist](
	@tradeId int	
	,@componentId int
	,@duration float 
	,@check int = 0 out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
		
	DECLARE @aptsCount int
	
	SELECT	@aptsCount = COUNT(*)
	From	PLANNED_APPOINTMENTS_HISTORY
	WHERE   PLANNED_APPOINTMENTS_HISTORY.COMPTRADEID IN (SELECT PLANNED_COMPONENT_TRADE.COMPTRADEID COMPTRADE
														 FROM  PLANNED_COMPONENT_TRADE
														 WHERE PLANNED_COMPONENT_TRADE.COMPONENTID = @componentId 
																AND PLANNED_COMPONENT_TRADE.DURATION = @duration 
																AND PLANNED_COMPONENT_TRADE.TRADEID = @tradeId  )
																
	if(@aptsCount > 0)
	begin
		set @check = 1
	end
	else
	begin
		set @check = 0
	end
		
					

END
GO
