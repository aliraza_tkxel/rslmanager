USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDetectorByPropertyId]    Script Date: 04/17/2017 15:33:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================
-- Author:          Ali Raza
-- Create date:      08/10/2015
-- Description:      Detectors list
-- History:          08/10/2015 AR : get detectors list for schemes and blocks

IF OBJECT_ID('dbo.PDR_GetDetectorBySchemeBlockId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetDetectorBySchemeBlockId AS SET NOCOUNT ON;') 
GO
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetDetectorBySchemeBlockId]
	(
	@SchemeId int, @BlockId int,
	@detectorType nvarchar(10)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE @propertyId nvarchar(20)='C390100007',@detectorType nvarchar(10)='CO'
SELECT
DetectorId,PropertyId,AS_DetectorType.DetectorTypeId,Location,Manufacturer,SerialNumber,PowerSource
 ,CONVERT(NVARCHAR, InstalledDate, 103) as Installed
 ,InstalledBy
 ,IsLandlordsDetector
 ,CONVERT(NVARCHAR, InstalledDate, 103)as TestedDate
 ,TestedBy
 ,CONVERT(NVARCHAR, BatteryReplaced, 103)as BatteryReplaced
 ,Passed
 ,Notes 
 ,AS_DetectorType.DetectorType
 ,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')			AS InstalledByPerson
 ,P_PowerSourceType.PowerType as [Power]
 FROM
  P_DETECTOR  
  Left JOIN P_PowerSourceType ON P_DETECTOR.PowerSource = P_PowerSourceType.PowerTypeId And IsActive=1 
  INNER JOIN  AS_DetectorType ON P_DETECTOR.DetectorTypeId = AS_DetectorType.DetectorTypeId
  LEFT JOIN E__EMPLOYEE AS E ON P_DETECTOR.InstalledBy = E.EMPLOYEEID
  where (SchemeId =@SchemeId OR @SchemeId = -1 )AND (BlockId= @BlockId OR @BlockId =-1)
  
   AND AS_DetectorType.DetectorType like '%'+@detectorType+'%'  
  
Select PowerTypeId, PowerType from P_PowerSourceType WHERE IsActive=1
END
