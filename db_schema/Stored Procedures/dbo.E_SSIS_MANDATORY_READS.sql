SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [E_SSIS_MANDATORY_READS]

AS 

--+-------------------------------------------------------------------------------------------
--+ Mandatory read report showing which and how many are unread, for property directorate.
--+-------------------------------------------------------------------------------------------

-- Declare variables in memory

DECLARE @i INT 
DECLARE @numrows INT
DECLARE @docid INT
DECLARE @docname NVARCHAR(100)
DECLARE @doclist TABLE
    (
      idx INT PRIMARY KEY
              IDENTITY(1, 1) ,
      docid INT ,
      docname NVARCHAR(100)
    )

DECLARE @overall TABLE
    (
      idx INT PRIMARY KEY IDENTITY(1, 1) ,
      document NVARCHAR(100) ,
      employeeid INT ,
      jobdetailsid INT ,
      jobtitle NVARCHAR(100) ,
      firstname NVARCHAR(100) ,
      lastname NVARCHAR(100) ,
      [read] NVARCHAR(100)
    )

-- INSERT variables into memory 

INSERT  INTO @doclist
        ( docid ,
          docname
        )
        SELECT  DOCUMENTID ,
                DOCNAME
        FROM    [RSLBHALive].[dbo].[DOC_DOCUMENT]
        WHERE   ( DOCFOR LIKE '%PROPD%'
                  OR DOCFOR LIKE '%PROPD'
                  OR DOCFOR LIKE 'PROPD%'
                )
                AND MANDATORY = 1
                AND DATECREATED > '2013-07-01'
        ORDER BY DATECREATED DESC

-- Set variables for loop

SET @i = 1
SET @numrows = ( SELECT COUNT(*)
                 FROM   @doclist
               )

IF @numrows > 0 
    WHILE ( @i <= ( SELECT  MAX(idx)
                    FROM    @doclist
                  ) ) 
        BEGIN
      
            SET @docid = ( SELECT   docid
                           FROM     @doclist
                           WHERE    idx = @i
                         ) 
            SET @docname = ( SELECT docname
                             FROM   @doclist
                             WHERE  idx = @i
                           ) 
      
            INSERT  INTO @overall
                    ( document ,
                      employeeid ,
                      jobdetailsid ,
                      jobtitle ,
                      firstname ,
                      lastname ,
                      [read]
                    )
                    SELECT  @docname AS [document] ,
                            [j].EMPLOYEEID ,
                            j.JOBDETAILSID ,
                            j.JOBTITLE ,
                            e.FIRSTNAME ,
                            e.LASTNAME ,
                            CASE WHEN m.DOCEMPID > 0 THEN 'Yes'
                                 ELSE 'No'
                            END AS [READ]
                    FROM    [RSLBHALive].[dbo].[G_TEAMCODES] AS [T]
                            LEFT JOIN [RSLBHALive].[dbo].[E_JOBDETAILS] AS [J] ON ( T.TEAMID = J.TEAM )
                            LEFT JOIN [RSLBHALive].[dbo].[E__EMPLOYEE] AS [E] ON ( J.EMPLOYEEID = E.EMPLOYEEID )
                            LEFT JOIN ( SELECT  *
                                        FROM    [RSLBHALive].[dbo].[I_MANDATORY_READ]
                                        WHERE   documentid = @docid
                                      ) AS [M] ON ( E.EMPLOYEEID = M.EMPLOYEEID )
                    WHERE   [T].[TEAMCODE] IN ( 'PROPD' )
                            AND J.active = 1
                            AND ( J.TEAM <> 1 -- Contractors
                                  OR J.TEAM IS NULL
                                )
                    ORDER BY E.LASTNAME ,
                            E.FIRSTNAME;
   
            SET @i = @i + 1
   
        END

SELECT  document AS Document,
        firstname AS [First Name],
        lastname AS [Last Name],
        [read] AS [Read]
FROM    @overall

GO
