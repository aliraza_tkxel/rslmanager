SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AM_SP_GetNoticeToVacateCases]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct isnull(G_TITLE.DESCRIPTION, '') +' '+isnull(C__CUSTOMER.FIRSTNAME, '') + isnull(C__CUSTOMER.LASTNAME, '') as CustomerName,
			isnull(C_ADDRESS.ADDRESS1, '') +', '+ isnull(C_ADDRESS.TOWNCITY, '') as CustomerAddress,AM_Case.NoticeIssueDate,AM_Case.NoticeExpiryDate,AM_Case.RecoveryAmount,P_FINANCIAL.TOTALRENT, 
			isnull(AM_PaymentPlan.AmountToBeCollected, 0.0)as PaymentAgreed, ISNULL(dbo.FN_GET_TENANT_BALANCE(AM_Case.TenancyId), 0.00) as RentBalance
	FROM AM_Case
	INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.CUSTOMERTENANCYID = AM_Case.TenancyId
	INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID	
	INNER JOIN  C_ADDRESS ON C__CUSTOMER.CustomerId = C_ADDRESS.CustomerId and C_ADDRESS.IsDefault = 1
	Left JOIN  G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId
	INNER JOIN  AM_Action ON AM_Action.ActionId = AM_Case.ActionId
	INNER JOIN	C_TENANCY ON C_TENANCY.TenancyId = AM_Case.TenancyId
	INNER JOIN	P_FINANCIAL ON C_TENANCY.PROPERTYID = P_FINANCIAL.PROPERTYID
	LEFT JOIN AM_PaymentPlan ON AM_PaymentPlan.TennantId = AM_Case.TenancyId
	WHERE AM_Action.Title = 'Notice To Vacate';

END





GO
