USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_AnnualLeaveCancelWorkingPatternChange]    Script Date: 05/05/2016 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_AnnualLeaveCancelWorkingPatternChange') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_AnnualLeaveCancelWorkingPatternChange AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Raja Aneeq
-- Create date:      21/10/2015
-- Description:      Calculate alert count of LEAVE REQUEST on whiteboard  
-- exec  E_AnnualLeaveCount 113
-- =============================================

ALTER PROCEDURE  [dbo].[E_AnnualLeaveCancelWorkingPatternChange]
	@empId INT 
AS
BEGIN
	select abs.ABSENCEHISTORYID
	from E__EMPLOYEE emp 
	join E_JOURNAL jnal on emp.EMPLOYEEID = jnal.EMPLOYEEID
    join E_STATUS es on jnal.CURRENTITEMSTATUSID = es.ITEMSTATUSID
    join E_NATURE en on jnal.ITEMNATUREID = en.ITEMNATUREID
    join E_ABSENCE abs on jnal.JOURNALID = abs.JOURNALID AND abs.ABSENCEHISTORYID = (
										SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
										WHERE JOURNALID = JNAL.JOURNALID AND
										emp.EMPLOYEEID = @empId 
										and (es.DESCRIPTION = 'Pending' OR es.DESCRIPTION = 'Approved')
										and en.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,43,52,53,54)
										  ) 
    where emp.EMPLOYEEID = @empId AND 
		((abs.STARTDATE > Getdate() AND es.DESCRIPTION = 'Approved') OR (es.DESCRIPTION = 'Pending'))
		AND en.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,43,52,53,54)

END
