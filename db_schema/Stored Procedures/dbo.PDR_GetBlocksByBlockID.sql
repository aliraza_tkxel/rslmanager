USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocksByBlockID]    Script Date: 12/12/2018 12:14:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:   Get Blocks By BlockID 
 
    Author: Ali Raza
    Creation Date: Dec-17-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-17-2014      Ali Raza           Get Blocks By BlockID 
    
    Execution Command:
    
    Exec PDR_GetBlocksByBlockID 2
  =================================================================================*/
 IF OBJECT_ID('dbo.[PDR_GetBlocksByBlockID]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetBlocksByBlockID] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetBlocksByBlockID]
( @blockId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select BLOCKNAME + ', ' + ADDRESS1 + ', ' + ADDRESS2 + ', ' + TOWNCITY + ', ' + COUNTY + ', ' + POSTCODE As BlockName, ADDRESS1,ADDRESS2,TOWNCITY,COUNTY,POSTCODE,
	 IsNull(SchemeId,0) as schemeId from P_BLOCK
	WHERE BLOCKID =@blockId
END
