SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE PROCEDURE dbo.FL_CO_GET_JOB_SHEET_FAULT_STATUS
	/* ===========================================================================
 '   NAME:           FL_CO_GET_JOB_SHEET_FAULT_STATUS
 '   DATE CREATED:   31st Dec 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get fault status from FL_FAULT_STATUS table which will be shown as lookup value in contractor portal pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT FaultStatusID AS id,DESCRIPTION AS val
	FROM FL_FAULT_STATUS WHERE FL_FAULT_STATUS.DESCRIPTION='Assigned To Contractor'











GO
