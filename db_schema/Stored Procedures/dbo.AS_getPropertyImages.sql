/* =================================================================================      
    Page Description:    Get attribute Images for scheme Block  
   
    Author: Ali Raza  
    Creation Date: jan-2-2015    
  
    Change History:  
  
    Version      Date             By                      Description  
    =======     ============    ========           ===========================  
    v1.0         jan-2-2015      Ali Raza           Get attribute Images for scheme Block  
      
    Execution Command:  
      
    Exec AS_getPropertyImages  
  =================================================================================*/   
    IF OBJECT_ID('dbo.AS_getPropertyImages') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_getPropertyImages AS SET NOCOUNT ON;') 
GO  
ALTER PROCEDURE [dbo].[AS_getPropertyImages] (    
@proerptyId Varchar(100)=null,   
@schemeId int=null,   
@blockId int=null,     
@itemId int,
@heatingMappingId int = null     
)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
  DECLARE @searchCriteria NVARCHAR(2000),@selectClause NVARCHAR(2000),@whereClause NVARCHAR(1000),@MainQuery NVARCHAR(4000)  
  --========================================================================================      
  -- Begin building SearchCriteria clause      
  -- These conditions will be added into where clause based on search criteria provided   
IF @schemeId > 0    
   BEGIN    
    SET @searchCriteria = +'  AND SchemeId = ' + CONVERT(varchar(10), @schemeId)      
   END       
  IF @blockId >0    
   BEGIN    
    SET @searchCriteria = +'  AND BlockId = ' +  CONVERT(varchar(10), @blockId)      
   END   
     
  IF @proerptyId != '' OR @proerptyId != NULL    
   BEGIN    
    SET @searchCriteria = +'  AND PROPERTYID = ''' +  @proerptyId + ''''      
   END   
    IF @heatingMappingId IS NOT NULL AND @heatingMappingId > 0      
   BEGIN      
    SET @searchCriteria = +'  AND heatingMappingId = ' + CONVERT(varchar(10), @heatingMappingId) + ' '        
   END     
  -- End building SearchCriteria clause         
  --========================================================================================      
      
   
   
 SET @selectClause = ' SELECT [SID],PROPERTYID,ItemId,i.ImagePath,ImageName,CONVERT(varchar,CreatedOn,103 ) CreatedOn,LEFT(e.FIRSTNAME, 1)+'' ''+ e.LASTNAME  CreatedBy,i.Title    
 FROM PA_PROPERTY_ITEM_IMAGES i   
  INNER JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy    '      
      
 -- Begin building WHERE clause      
      
  -- This Where clause contains subquery to exclude already displayed records           
       
  SET @whereClause = CHAR(10) + ' WHERE ItemId ='+  CONVERT(varchar(10), @itemId) + CHAR(10) + @searchCriteria      
      
  -- End building WHERE clause      
    
   -- Begin building the main select Query      
      
  SET @MainQuery = @selectClause  + @whereClause    
      
  -- End building the main select Query      
 PRINT(@MainQuery)  
 EXEC(@MainQuery)   
    -- Insert statements for procedure here    
  
 --WHERE PROPERTYID = @proerptyId AND ItemId = @itemId    
END 