
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_GetHistoricalBalanceListCount]	
            @caseOwnedById int=0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@customerStatus int = 0,
			@assetType int =0,
            @months int =0,
            @years int =0
            
AS
BEGIN
	declare @RegionSuburbClause varchar(8000)
	declare @query varchar(8000)


IF(@caseOwnedById = 0 )
BEGIN
	IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END
ELSE
BEGIN

IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'')'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'') AND PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'') AND PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END
		
			SET @query =
				'SELECT COUNT(*) as recordCount FROM(SELECT Count(*) as TotalRecords
				FROM
                    AM_HistoricalBalanceList 
					INNER JOIN  C__CUSTOMER ON AM_HistoricalBalanceList.CustomerId = C__CUSTOMER.CustomerId		
					INNER JOIN  G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId
					INNER JOIN C_TENANCY ON AM_HistoricalBalanceList.TENANCYID = C_TENANCY.TenancyId
					INNER JOIN P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID 
					LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				    INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID	
				WHERE 
			           Datepart(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))=CASE WHEN 0='+Convert(varchar(50), @years)+' THEN DATEPART(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0)) ELSE '+Convert(varchar(50), @years)+' END 
                       and Datepart(mm,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))=CASE WHEN 0 = '+Convert(varchar(50), @months)+' THEN DATEPART(mm,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0)) ELSE '+Convert(varchar(50), @months)+' END 
                       and '+ @RegionSuburbClause +' 
                       and C__CUSTOMER.CUSTOMERTYPE = CASE WHEN 0='+Convert(varchar(50), @customerStatus)+' THEN C__CUSTOMER.CUSTOMERTYPE ELSE '+Convert(varchar(50), @customerStatus)+' END 
					   and P__PROPERTY.ASSETTYPE=CASE WHEN 0= '+Convert(varchar(50), @assetType)+'  THEN P__PROPERTY.ASSETTYPE ELSE '+Convert(varchar(50), @assetType)+' END 
                      GROUP BY AM_HistoricalBalanceList.TenancyId) as TEMP'

EXEC(@query);
END










GO
