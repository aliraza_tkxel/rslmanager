USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyLevel]    Script Date: 21/03/2019 11:41:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property Level for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Level for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyLevel
  =================================================================================*/
 IF OBJECT_ID('dbo.PDR_GetPropertyLevel') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyLevel AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[PDR_GetPropertyLevel]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LEVELID, DESCRIPTION FROM P_LEVEL  order BY DESCRIPTION ASC
	
END
