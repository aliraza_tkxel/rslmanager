SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.TO_ENQUIRY_LOG_AddDirectDebit
/* ===========================================================================
 '   NAME:          TO_ENQUIRY_LOG_AddDirectDebit
 '   DATE CREATED:   27 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add "Direct Debit" record, it adds rocord in TO_ENQUIRY_LOG table
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '
 '   OUT:            @EnquiryLogId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(500),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	@EnquiryLogId INT OUTPUT
	)
	
AS
	
	

	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

				IF @EnquiryLogID<0
					BEGIN
						SET @EnquiryLogID=-1	
				END
					

	
	
	
	
	
	
	
	
	




GO
