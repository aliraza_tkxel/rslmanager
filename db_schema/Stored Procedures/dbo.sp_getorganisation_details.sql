SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[sp_getorganisation_details]
@org_id int
AS 

select  
CONVERT(VARCHAR,startdate, 103) as gb_startdate,
CONVERT(VARCHAR,enddate, 103) as gb_enddate,*
from h_organisation where org_id = @org_id



GO
