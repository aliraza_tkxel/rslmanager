SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Abdullah Saeed
-- Create date: August 13,2014
-- Description:	Save PDF document of Record Stock Inspection
-- =============================================
CREATE PROCEDURE [dbo].[PROPERTYINSPECTION_SaveDetailsDocument]
	-- Add the parameters for the stored procedure here
(
@propertyId as varchar(20),
@surveyId as int,
@InspectionDocument as image,
@createdBy as varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @inspectionDate as datetime
	select @inspectionDate= SurveyDate  from PS_Survey where SurveyId= @surveyId
	Insert into PA_Property_Inspection_Record ( PropertyId, SurveyId,InspectionDocument,
	inspectionDate,CreatedDate,createdBy)
	Values(@propertyId,@surveyId, @InspectionDocument, @inspectionDate,GETDATE(),@createdBy)              
	
END
GO
