USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDevelopmentDocumentInformationById]    Script Date: 22-Jun-16 5:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
-- EXEC PDR_GetDevelopmentDocumentInformationById @documentId = 7415
-- Author:		<Junaid Nadeem>
-- Create date: <22-Jun-2016>
-- Description:	<Returns Develpment Document Info By Document Id>
-- WebPage: AddNewDevelopment.aspx > Document Tab
-- =============================================
IF OBJECT_ID('dbo.[PDR_GetDevelopmentDocumentInformationById]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetDevelopmentDocumentInformationById] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetDevelopmentDocumentInformationById]( 
	@documentId int	
	)
	
AS
BEGIN

	-- Get Document Complete Information to show document on client side.
	SELECT	DocumentId, ISNULL(DocumentName,'N/A') AS DocumentName, DocumentPath, DocumentDate,DocumentTypeId
	FROM	PDR_DOCUMENTS
	WHERE DocumentId = @documentId

END
