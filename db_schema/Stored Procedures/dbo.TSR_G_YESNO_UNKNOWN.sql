SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_G_YESNO_UNKNOWN
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/16/2014>
-- Description:	<Description,,get the stages for dropdown>
-- WebPage: CustomerDetail.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_G_YESNO_UNKNOWN]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from G_YESNO_UNKNOWN
END
GO
