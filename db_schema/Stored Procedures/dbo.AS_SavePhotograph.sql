USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePhotograph]    Script Date: 12/01/2016 06:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[AS_SavePhotograph]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_SavePhotograph] AS SET NOCOUNT ON;') 
GO 
ALTER PROCEDURE [dbo].[AS_SavePhotograph] (    
@propertyId varchar(500)=null,    
@itemId int,    
@title varchar(500),    
@uploadDate smalldatetime,    
@imagePath varchar(1000),    
@imageName varchar(500),    
@createdBy int,  
@schemeId int=null, 
@blockId int=null, 
@isDefaultImage bit = null ,
@heatingMappingId int = null  
)    
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  

	DECLARE @propertyPicId INT 
	 
	INSERT INTO PA_PROPERTY_ITEM_IMAGES  
		([PROPERTYID], [ItemId], [ImagePath], [ImageName], [CreatedOn], [CreatedBy], [Title],HeatingMappingId,SchemeId,BlockId)  
	VALUES  
		(@propertyId, @itemId, @imagePath, @imageName, @uploadDate, @createdBy, @title,@heatingMappingId,@schemeId,@blockId)  
	
	SET @propertyPicId=SCOPE_IDENTITY()
	IF @isDefaultImage = 1 And @propertyPicId > 0
		 Begin 
			 Update P__PROPERTY SET PropertyPicId = @propertyPicId WHERE PROPERTYID = @propertyId  
		 END
END  

