
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[FL_SAVE_FAULT_TEMPORARILY] 
/* ===========================================================================
 '   NAME:           FL_SAVE_FAULT_TEMPORARILY
 '   DATE CREATED:   20th Oct, 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online Fault Basket
 '   PURPOSE:        To save faults in fault table
 
 '   IN:             @FaultID
 '   IN:             @CustomerId
 '   IN:             @Quantity
 '   IN:             @ProblemDays
 '   IN:             @RecuringProblem
 '   IN:             @CommunalProblem
 '   IN:             @Notes
 '
 '   OUT:            @TempFaultId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
	@FaultID INT,
	@CustomerId INT,
	@Quantity INT,
	@ProblemDays INT,
	@RecuringProblem INT,
	@CommunalProblem INT,
	@Recharge INT,
	@Notes nvarchar(4000),
	@TempFaultId INT OUTPUT
  )
AS
	BEGIN TRAN
	INSERT INTO FL_TEMP_FAULT(
		FaultID,
		CustomerId,
		Quantity,
		ProblemDays,
		RecuringProblem,
		CommunalProblem,
		Recharge,
		Notes,
		ItemStatusID
		
	)

	VALUES(
		@FaultID,
		@CustomerId,
		@Quantity,
		@ProblemDays,
		@RecuringProblem,
		@CommunalProblem,
		@Recharge,
		@Notes,
		1
		
	)

SELECT @TempFaultId=TempFaultId
FROM FL_TEMP_FAULT
WHERE TempFaultId = @@IDENTITY

IF @TempFaultId<0
	BEGIN
		SET @TempFaultId=-1
		ROLLBACK TRAN
	END
	ELSE
		SET @TempFaultId=1
		COMMIT TRAN
GO
