SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_GetRowCount]

/* ===========================================================================
 '   NAME:           TO_GETROWCOUNT
 '   DATE CREATED:   03 JUNE 2008
 '   CREATED BY:     
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        A Genric utility to count total no. of rows in particular table
 '   IN:             @tableName
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(	    
		-- tableName
		@tableName varchar (50)	= 'ACCOUNT'			
	)
		
		
AS

  DECLARE @Query varchar (100)

  SET @Query = 'SELECT COUNT(*) As numOfRows FROM ' + @tableName
    
    
  EXEC (@Query)

GO
