SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_GetCustomersEqnuiries]
/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GetCustomersEqnuiries
 '   DATE CREATED:   03 JULY 2008
 '   CREATED BY:     NAVEED IQBAL
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve customer enquiries against specified CustomerID and Response Status
 '   IN:             @customerID
 '   IN:             @ResponseStatus
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
@CustomerID INT,
@ResponseStatus VARCHAR(50)
)
AS

DECLARE
@WhereClause VARCHAR(8000),
@SelectClause VARCHAR(8000),
@OrderClause VARCHAR(8000)


SET @OrderClause=' Order By CreationDate DESC, EnquiryLogID DESC'

SET @SelectClause = 'Select EnquiryLogID,CreationDate,JournalID,ItemNatureID,NATURE,Item,ResponseStatus,ImageKeyFlag,EnqDescription ' + CHAR(10) +

' FROM ( ' + CHAR(10) +
' SELECT DISTINCT  A.EnquiryLogID, A.CreationDate, A.JournalID, A.ItemNatureID,N.DESCRIPTION AS NATURE,I.DESCRIPTION as Item,A.DESCRIPTION as EnqDescription, ' + CHAR(10) +
' (CASE WHEN A.ItemStatusId=29 THEN ''Response Sent'' ELSE CASE WHEN A.CloseFlag=1 THEN ''Closed'' ELSE CASE WHEN B.EnquiryLogID IS NULL THEN ''Awaiting Response'' ELSE ''Response Received'' END END END) ' + CHAR(10) +
' AS ResponseStatus, ' + CHAR(10) +

' (CASE WHEN '+ CHAR(10) +
' (SELECT TOP 1 ISNULL( EnquiryLogID, ''True'') ' + CHAR(10) +
' FROM TO_RESPONSE_LOG AS C ' + CHAR(10) +
' WHERE '+ CHAR(10) +
' (EnquiryLogID = A.EnquiryLogID) AND (ReadFlag = 0)) IS NULL ' + CHAR(10) +
' AND B.EnquiryLogID IS NULL THEN ''0'' ' + CHAR(10) +
' WHEN' + CHAR(10) +
' (SELECT TOP 1 ISNULL(EnquiryLogID, ''True'') ' + CHAR(10) +
' FROM TO_RESPONSE_LOG AS C' + CHAR(10) +
' WHERE ' + CHAR(10) +
' (EnquiryLogID = A.EnquiryLogID) AND (ReadFlag = 0)) IS NULL ' + CHAR(10) +
' AND B.EnquiryLogID IS NOT NULL THEN ''1'' ELSE ''2'' END) ' + CHAR(10) +
' AS ImageKeyFlag ' + CHAR(10) +
' FROM ' + CHAR(10) +

' TO_ENQUIRY_LOG AS A LEFT OUTER JOIN' + CHAR(10) +

' TO_RESPONSE_LOG AS B ON A.EnquiryLogID = B.EnquiryLogID ' + CHAR(10) +
' LEFT OUTER JOIN C_NATURE N ON A.ITEMNATUREID=N.ITEMNATUREID ' + CHAR(10) +
' LEFT OUTER JOIN C_ITEM I ON N.ITEMID=I.ITEMID ' + CHAR(10) +
' WHERE ' + CHAR(10) +

' A.CustomerId = ' + CHAR(10) + Convert(varchar, @CustomerID) --+ CHAR(10) + ') Z '


IF @ResponseStatus IS NOT NULL
	BEGIN
		IF 	@ResponseStatus='Closed'	
			BEGIN
				SET @WhereClause=' AND A.CloseFlag = 1 ' + CHAR(10) + ') Z '
				SET @SelectClause= @SelectClause + @WhereClause
				EXEC(@SelectClause + @OrderClause)
			END
		ELSE
			BEGIN
			SET @SelectClause= @SelectClause + ' ) Z where ResponseStatus = ''' + @ResponseStatus + ''''
			EXEC(@SelectClause + @OrderClause)
			END
	END			
ELSE
	BEGIN
			SET @SelectClause= @SelectClause + ') Z'
			EXEC(@SelectClause + @OrderClause)
	END


GO
