SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[I_PAGE_ACCESSCHECKER]
@PAGENAME VARCHAR(250),
@EMPID INT,
@ACCESSCOUNT INT OUTPUT
AS
DECLARE @PAGEID INT
SELECT @PAGEID = PAGEID FROM I_PAGE WHERE FILENAME = @PAGENAME

SELECT  @ACCESSCOUNT = COUNT(*)
FROM    E__EMPLOYEE E
        INNER JOIN dbo.I_PAGE_TEAMJOBROLE PJR ON e.JobRoleTeamId = PJR.TeamJobRoleId
WHERE   PJR.PageId = @PAGEID
        AND e.EMPLOYEEID = @EMPID

--SELECT @ACCESSCOUNT = COUNT(*) FROM I_PAGE_EMPLOYEE WHERE PAGEID = @PAGEID AND EMPLOYEEID = @EMPID
SET @ACCESSCOUNT =  ISNULL(@ACCESSCOUNT,0)

GO
