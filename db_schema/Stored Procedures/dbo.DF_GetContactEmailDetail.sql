

USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetContactEmailDetail]    Script Date: 06/07/2016 15:56:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.DF_GetContactEmailDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetContactEmailDetail AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 22/08/2017
-- Description:	Get Contact Email Detail

-- =============================================
ALTER PROCEDURE [dbo].[DF_GetContactEmailDetail]
	@employeeId INT
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
	INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @employeeId AND  C.WORKEMAIL IS NOT NULL



END