USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetApplianceType]    Script Date: 18-Jan-17 2:26:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetApplianceType
-- Author:		<Noor Muhammad>
-- Create date: <02/11/2012>
-- Description:	<Get all appliance types>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
IF OBJECT_ID('dbo.[AS_GetApplianceType]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetApplianceType] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetApplianceType]
(@prefix varchar(100) )  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.APPLIANCETYPEID as id, a.APPLIANCETYPE as title
	FROM GS_Appliance_Type a
	INNER JOIN
  (SELECT APPLIANCETYPE, MIN(APPLIANCETYPEID) as Id 
   FROM GS_Appliance_Type
   GROUP BY APPLIANCETYPE) AS b
   ON a.APPLIANCETYPE=b.APPLIANCETYPE
   AND a.APPLIANCETYPEID=b.Id
	WHERE ISACTIVE = 1 And a.APPLIANCETYPE like '%'+@prefix+'%' 
	order by a.APPLIANCETYPE asc
END
GO
