SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Umair Naeem
-- Create date: 07 Sep 2010
-- Description:	This procedure gets the 
--				list of last five ENVIROMENTAL TIPS
-- =============================================
--
-- EXEC [ENVIRO_GET_LATEST_FIVE_TIPS]
--
create PROCEDURE [dbo].[ENVIRO_GET_LATEST_FIVE_TIPS]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 5 TIPSID,TIPSDESCRIPTION,CREATIONDATE,CREATEDBY FROM ENVIRO_TIPS ORDER BY TIPSID DESC 

	SET NOCOUNT OFF;
END 



















GO
