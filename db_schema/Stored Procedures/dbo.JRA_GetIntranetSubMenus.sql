SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_GetIntranetSubMenus @teamJobRoleId = 616,@mainmenuId=11
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Get intranet menus>
-- Web Page: JobRoles.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetIntranetSubMenus](
@teamJobRoleId int
,@mainmenuId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
     --============================================================
    --					SUB MENUS
    --============================================================
    
    SELECT	I_MENU.MenuID AS SubMenuId, I_MENU.MenuTitle as SubMenuName , I_MENU.ParentID as ParentId
    FROM	I_MENU
    WHERE	I_MENU.Active = 1 AND  ParentID =@mainmenuId
      
	
END
GO
