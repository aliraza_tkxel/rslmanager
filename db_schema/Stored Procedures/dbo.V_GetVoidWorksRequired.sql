USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
/* =================================================================================    
    Page Description: Get First Inspections To Be Arranged List 
    Author: Ali Raza
    Creation Date: May-15-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         May-15-2015      Ali Raza         Get First Inspections To Be Arranged List 
  =================================================================================*/
--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[V_GetVoidWorksRequired]
--		@JournalId = 485,
--		@isRearrange = 1,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@sortColumn = N'RequiredWorksId',
--		@sortOrder = N'Desc',
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================


IF OBJECT_ID('dbo.V_GetVoidWorksRequired') IS NULL 
	EXEC('CREATE PROCEDURE dbo.V_GetVoidWorksRequired AS SET NOCOUNT ON;') 
GO  

Alter PROCEDURE [dbo].[V_GetVoidWorksRequired]
-- Add the parameters for the stored procedure here
		@JournalId int,
		@isRearrange BIT = 0,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'RequiredWorksId', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' (V_RequiredWorks.IsCanceled IS NULL OR V_RequiredWorks.IsCanceled=0) AND (V_RequiredWorks.ISMAJORWORKSREQUIRED is null or V_RequiredWorks.ISMAJORWORKSREQUIRED =0) '
		IF(@isRearrange=1)
			BEGIN
					SET @searchCriteria =@searchCriteria + CHAR(10) +  ' AND  V_RequiredWorks.WorksJournalId= '+convert(varchar(10),@JournalId)+''

			END
		ELSE 
			BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) + '  AND  V_RequiredWorks.InspectionJournalId= '+convert(varchar(10),@JournalId)+''

			END
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 	 RequiredWorksId,PDR_JOURNAL.JOURNALID,Case WHEN PDR_JOURNAL.JOURNALID  IS NULL THEN ''-'' ELSE ISNULL( ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,RequiredWorksId),4)),''-'')END AS Ref,FL_Area.AreaName AS Location,V_RequiredWorks.WorkDescription,
ISNULL(CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+''</br> ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) +''(''+LEFT(E__EMPLOYEE.Firstname, 1)+''''+LEFT(E__EMPLOYEE.LASTNAME, 1)+'')'',''-'')
 as Appointment,ISNULL(TenantNeglectEstimation,0)as Neglect,ISNULL(IsTenantWorks,0) as IsTenantWorks,ISNULL(S_ORGANISATION.NAME,''-'') as Contractor,
 case When V_RequiredWorks.IsScheduled=1 OR IsTenantWorks=1 Then 0 Else 1 END as Checked,V_RequiredWorks.IsScheduled,
 V_RequiredWorks.faultAreaId As LocationId,ISNULL(E__EMPLOYEE.Firstname+'' ''+E__EMPLOYEE.LASTNAME,''-'') as Operative,
 ISNULL(CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+'' ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108),''-'') as AppointmentStartDateTime
,PDR_APPOINTMENTS.APPOINTMENTNOTES As AppointmentNotes,PDR_APPOINTMENTS.CUSTOMERNOTES as JobSheetNotes,ISNULL(PDR_Status.Title,''-'') AS WorkStatus,
PDR_APPOINTMENTS.AppointmentId AS AppointmentId,T.TerminationDate as Termination ,
Case When T.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,T.TerminationDate), 103) Else	CONVERT(nvarchar(50),T.ReletDate, 103) END as ReletDate,
ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
,InspectionJournalId
,ISNULL(V_RequiredWorks.workType, '''') as WorkType
,V_RequiredWorks.DURATION as Duration
 '
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'  FROM
V_RequiredWorks
LEFT JOIN PDR_JOURNAL ON V_RequiredWorks.WorksJournalId= PDR_JOURNAL.JOURNALID 
LEFT JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId 
left JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
outer Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P__PROPERTY.PropertyID 
AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15)  
	GROUP BY PROPERTYID) as CJournal 
outer APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
left JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
LEFT JOIN PDR_APPOINTMENTS ON PDR_APPOINTMENTS.JOURNALID= PDR_JOURNAL.JOURNALID
LEFT JOIN FL_Area ON V_RequiredWorks.faultAreaID = FL_Area.AreaID
LEFT JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
LEFT JOIN PDR_CONTRACTOR_WORK ON PDR_CONTRACTOR_WORK.JournalId = PDR_JOURNAL.JOURNALID 
LEFT JOIN S_ORGANISATION ON PDR_CONTRACTOR_WORK.ContractorId = S_ORGANISATION.ORGID
LEFT JOIN PDR_Status ON V_RequiredWorks.StatusId= PDR_Status.StatusId AND PDR_Status.TITLE <> ''Cancelled''
		'
							
		--============================Order Clause==========================================
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		select PDR_JOURNAL.JOURNALID,'JSV'+CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,PDR_JOURNAL.JOURNALID),4)) AS Ref,
			ISNULL(P__PROPERTY.HouseNumber, '')	+ ISNULL(' '+P__PROPERTY.ADDRESS1, '') 	+ ISNULL(', '+P__PROPERTY.ADDRESS2, '') AS Address 
			,CONVERT(nvarchar(50),T.TERMINATIONDATE, 103) as Termination	,
		Case When t.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,T.TerminationDate), 103) Else	CONVERT(nvarchar(50),T.ReletDate, 103) END as ReletDate
			,E__EMPLOYEE.Firstname+' '+E__EMPLOYEE.LASTNAME as Recorded,CONVERT(nvarchar(50),CreatedDate, 103) as CreatedDate,P__PROPERTY.PATCH as PatchId
 FROM 

		V_AppointmentRecordedData
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID= V_AppointmentRecordedData.InspectionJournalId
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P__PROPERTY.PropertyID 
		AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,15)  
			GROUP BY PROPERTYID) as CJournal 
		CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
		INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
		LEFT JOIN E__EMPLOYEE ON V_AppointmentRecordedData.CreatedBy = E__EMPLOYEE.EMPLOYEEID
where V_AppointmentRecordedData.InspectionJournalId=@JournalId
				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
