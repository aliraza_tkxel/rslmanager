SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- EXEC MPI_4_TOTAL_PAYMENTAGREEMENTS_OPEN '','','',6
CREATE    PROCEDURE [dbo].[MPI_4_TOTAL_PAYMENTAGREEMENTS_OPEN] (
	@DEVELOPMENTID VARCHAR(10), 
	@ASSETTYPE VARCHAR(10), 
	@MONTH VARCHAR(10), 
	@YEAR INT
	 )
AS

DECLARE @SQL VARCHAR (4000)
DECLARE @SQL_DEV_CRITERIA VARCHAR (200)
DECLARE @SQL_ASSETTYPE_CRITERIA VARCHAR (200)
DECLARE @SQL_STARTDATE_CRITERIA VARCHAR (20)
DECLARE @SQL_ENDDATE_CRITERIA VARCHAR (20)
DECLARE @FYSTARTDATE SMALLDATETIME 
DECLARE @FYENDDATE SMALLDATETIME
DECLARE @MONTHYEAR INT
DECLARE @LASTDAYOFMONTH INT

SET @SQL_DEV_CRITERIA = ''
SET @SQL_ASSETTYPE_CRITERIA = ''

-- GET START AND ENDDATES OF CHOSEN FISCAL YEAR
SELECT @FYSTARTDATE = YSTART, @FYENDDATE = YEND FROM F_FISCALYEARS WHERE YRANGE = @YEAR

-- FIRST WE SET THE DATE CRITERIA
IF @MONTH = '' 
	BEGIN
	   SET @SQL_STARTDATE_CRITERIA = @FYSTARTDATE
	   SET @SQL_ENDDATE_CRITERIA = @FYENDDATE
	END
IF @MONTH <> '' 
	BEGIN
 	   -- FIRST GET THE CORRECT YEAR TO USE
	   IF @MONTH IN ('JAN', 'FEB', 'MAR')
		SET @MONTHYEAR = YEAR(@FYENDDATE)
	   ELSE
		SET @MONTHYEAR = YEAR(@FYSTARTDATE)
	   -- THEN BUILD DATE STRINGS
	   SET @SQL_STARTDATE_CRITERIA = '1 ' + @MONTH + ' ' + CAST(@MONTHYEAR AS VARCHAR)
  	   SET @LASTDAYOFMONTH = DATEPART(DAY,DATEADD(d, -DAY(DATEADD(m,1,@SQL_STARTDATE_CRITERIA)),DATEADD(m,1,@SQL_STARTDATE_CRITERIA)))
	   SET @SQL_ENDDATE_CRITERIA = CAST(@LASTDAYOFMONTH AS VARCHAR) + ' '  + @MONTH + ' ' + CAST(@MONTHYEAR AS VARCHAR)
	END
--SELECT @SQL_STARTDATE_CRITERIA
--SELECT @SQL_ENDDATE_CRITERIA

-- NOW SET OTHER CRITERIA
IF @DEVELOPMENTID <> ''
	   SET @SQL_DEV_CRITERIA = ' AND P.DEVELOPMENTID = ' + @DEVELOPMENTID
IF @ASSETTYPE <> ''
	   SET @SQL_ASSETTYPE_CRITERIA = ' AND P.ASSETTYPE = ' + @ASSETTYPE

SET @SQL = '
SELECT 	ISNULL(COUNT(*),0) AS NUM
FROM 	C_JOURNAL J
	INNER JOIN  ( -- GET LAST ACTIVITY ONLY
	    SELECT MAX(ARREARSHISTORYID) AS MAXHIST, JOURNALID 
	    FROM   C_ARREARS WHERE ITEMACTIONID = 10 
	    GROUP BY JOURNALID
	   ) MH ON MH.JOURNALID = J.JOURNALID
	INNER JOIN C_ARREARS G ON G.ARREARSHISTORYID = MH.MAXHIST
	INNER JOIN C_TENANCY T ON T.TENANCYID = J.TENANCYID
	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID
WHERE	J.CURRENTITEMSTATUSID = 13 AND J.CREATIONDATE >= ''' + @SQL_STARTDATE_CRITERIA + '''
	AND J.CREATIONDATE <= ''' + @SQL_ENDDATE_CRITERIA + '''' + @SQL_DEV_CRITERIA + @SQL_ASSETTYPE_CRITERIA 

EXECUTE (@SQL)



GO
