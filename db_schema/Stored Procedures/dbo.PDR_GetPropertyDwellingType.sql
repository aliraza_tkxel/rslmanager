USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyDwellingType]    Script Date: 21/03/2019 11:40:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property DwellingType for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property DwellingType for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyDwellingType
  =================================================================================*/
  IF OBJECT_ID('dbo.PDR_GetPropertyDwellingType') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyDwellingType AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[PDR_GetPropertyDwellingType]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DTYPEID, DESCRIPTION FROM P_DWELLINGTYPE  order by DESCRIPTION ASC
	
END
