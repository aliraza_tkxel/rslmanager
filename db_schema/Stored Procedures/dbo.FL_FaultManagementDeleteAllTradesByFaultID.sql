SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_FaultManagementDeleteAllTradesByFaultID]
/* ===========================================================================
--	EXEC FL_FaultManagementDeleteAllTradesByFaultID
		@FAULTID = 1,		
		@RESULT OUTPUT
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To Delete all faults for a FaultID
--  Webpage:		View/Reports/ReportArea.aspx (For Amend Fault Modal Popup)
 '==============================================================================*/

@FAULTID INT,
@RESULT  INT OUTPUT

AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN

BEGIN
DELETE FL_FAULT_TRADE	
	WHERE FAULTID= @FAULTID
END

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN

SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN














GO
