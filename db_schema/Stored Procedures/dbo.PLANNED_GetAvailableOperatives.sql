USE [RSLBHALive]
GO

SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
--EXEC	[dbo].[PLANNED_GetAvailableOperatives]
--		@tradeIds = N'1,2,3',
--		@propertyId = N'A720040007',
--		@startDate = N'' 
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,4 Feb,2013>
-- Last Updated By: Noor Muhammad
-- Last Updated Date: 18th Nov 2013
-- Description:	<Description,,This stored procedure fetch  the employees >
-- WebPage: AvailableAppointments.aspx
-- ============================================= */
IF OBJECT_ID('dbo.PLANNED_GetAvailableOperatives') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_GetAvailableOperatives AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetAvailableOperatives]
	-- Add the parameters for the stored procedure here
	@tradeIds		AS VARCHAR(MAX)
	,@propertyId	AS VARCHAR(20)
	,@startDate		AS DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @operativeIdStr NVARCHAR(MAX)

	--=================================================================================================================
	------------------------------------------------------ Step 0------------------------------------------------------
	--Create Temporary Table


	CREATE TABLE #AvailableOperatives (
		EmployeeId INT
		,FirstName NVARCHAR(50)
		,LastName NVARCHAR(50)
		,FullName NVARCHAR(100)
		,PatchId INT
		,PatchName NVARCHAR(20)
		,Distance NVARCHAR(10)
		,TradeId INT
	)

	DECLARE @InsertClause VARCHAR(200)
	DECLARE @SelectClause VARCHAR(800)
	DECLARE @FromClause VARCHAR(800)
	DECLARE @WhereClause VARCHAR(MAX)
	DECLARE @MainQuery VARCHAR(MAX)
	Declare @startDateString nvarchar(20)
	--=================================================================================================================
	------------------------------------------------------ Step 1------------------------------------------------------
	--This query fetches the employees which matches with the following criteria
	--Trade of employee is same as trade of component 
	--User type is Planned		
	--Exclude the sick leaves

	SET @InsertClause = 'INSERT INTO #AvailableOperatives(EmployeeId,FirstName,LastName,FullName, PatchId, PatchName, TradeId)'

	SET @SelectClause = 'SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + '' ''+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	,E_TRADE.TradeId AS TradeId '

	SET @FromClause = 'FROM  E__EMPLOYEE 
	INNER JOIN E_TRADE ON E_TRADE.EmpId = E__EMPLOYEE.EmployeeId
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID '

	SET @WhereClause = 'WHERE P_INSPECTIONTYPE.Description  =''Planned'' 	
	AND E_JOBDETAILS.Active=1		
	AND E_TRADE.tradeid in (' + @tradeIds + ') '

	---------------------------------------------------------
	set @startDateString = Convert(nvarchar(20), @startDate, 103)
	-- Filter to skip out operative(s) of sick leave.
	SET @WhereClause = @WhereClause + CHAR(10) + ' AND E__EMPLOYEE.EmployeeId NOT IN (
																						SELECT 
																							EMPLOYEEID 
																							FROM E_JOURNAL
																							INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
																							AND E_ABSENCE.ABSENCEHISTORYID IN (
																																SELECT 
																																	MAX(ABSENCEHISTORYID) 
																																	FROM E_ABSENCE 
																																	GROUP BY JOURNALID
																																)
																							WHERE ITEMNATUREID =1
																							AND E_ABSENCE.RETURNDATE IS NULL
																							AND E_ABSENCE.ITEMSTATUSID = 1
																						UNION ALL	
																						SELECT DISTINCT
																							E_JOURNAL.EMPLOYEEID
																						FROM E_JOURNAL
																						INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
																							AND E_ABSENCE.ABSENCEHISTORYID IN (
																																SELECT 
																																	MAX(ABSENCEHISTORYID) 
																																	FROM E_ABSENCE 
																																	GROUP BY JOURNALID
																																)
																						INNER JOIN E_STATUS ON  E_ABSENCE.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
																						INNER JOIN E_NATURE ON  E_JOURNAL.ITEMNATUREID = E_NATURE.ITEMNATUREID
																						INNER JOIN E_JOBDETAILS ej ON E_JOURNAL.EMPLOYEEID = ej.EMPLOYEEID
																						LEFT JOIN E_HOLIDAYRULE hl ON ej.HOLIDAYRULE = hl.EID
																						WHERE E_ABSENCE.startdate <= CONVERT(DATE,''' + @startDateString +''') and 
																							  E_ABSENCE.RETURNDATE >= CONVERT(DATE, '''+ @startDateString +''' ) and
																							E_NATURE.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,48,43,52,53,54)
																							AND E_ABSENCE.ITEMSTATUSID = 3 and
																							ej.ISBRS=1
																						)	'
	---------------------------------------------------------				
	--Combine the query clauses to make the one / main query
	SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10) + @FromClause + CHAR(10) + @WhereClause


	--print @InsertClause+Char(10)
	PRINT @SelectClause + CHAR(10)
	PRINT @FromClause + CHAR(10)
	PRINT @WhereClause

	EXEC (@MainQuery)

	SELECT DISTINCT
		EmployeeId
		,FirstName
		,LastName
		,FullName
		,PatchId
		,PatchName
		,Distance
		,TradeID
	FROM
		#AvailableOperatives
	--=================================================================================================================
	------------------------------------------------------ Step 3------------------------------------------------------						
	--This query selects the leaves of employees i.e the employess which we get in step1
	--M : morning - 08:00 AM - 12:00 PM
	--A : mean after noon - 01:00 PM - 05:00 PM
	--F : Single full day
	--F-F : Multiple full days
	--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
	--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days


	SELECT
		E_ABSENCE.STARTDATE		AS StartDate
		,CASE
			WHEN
				(E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.HOLTYPE NOT IN ('M','F-M','A-M')
				AND E_ABSENCE.RETURNDATE IS NOT NULL AND DATEDIFF(day,E_ABSENCE.STARTDATE,E_ABSENCE.RETURNDATE)>0) THEN 
				DATEADD(DD,-1,E_ABSENCE.RETURNDATE)	
			ELSE
				E_ABSENCE.RETURNDATE 
			END 	
		 AS EndDate
		,E_JOURNAL.employeeid	AS OperativeId
		,E_ABSENCE.HolType		AS HolType
		,E_ABSENCE.duration		AS Duration
		,CASE
			WHEN (E_ABSENCE.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE
				.STARTDATE)))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, E_ABSENCE.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
			WHEN HolType = '' or HOLTYPE is null 
				THEN '08:00 AM'
			WHEN HolType = 'M'
				THEN '09:00 AM'
			WHEN HolType = 'A'
				THEN '01:00 PM'
			WHEN HolType = 'F'
				THEN '00:00 AM'
			WHEN HolType = 'F-F'
				THEN '00:00 AM'
			WHEN HolType = 'F-M'
				THEN '00:00 AM'
			WHEN HolType = 'A-F'
				THEN '01:00 PM'
			WHEN HolType = 'A-M'
				THEN '01:00 PM'
		END						AS StartTime
		,CASE
			WHEN
				(E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.HOLTYPE NOT IN ('M','F-M','A-M') AND E_ABSENCE.RETURNDATE IS NOT NULL 
				AND DATEDIFF(day,E_ABSENCE.STARTDATE,E_ABSENCE.RETURNDATE)>0) THEN

				CASE WHEN (DATEADD(DD,-1,E_ABSENCE.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,E_ABSENCE.RETURNDATE))))
					THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 0), 8)), 'AM', ' AM'), 'PM', ' PM')
				WHEN HolType = '' or HOLTYPE is null 
					THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00'
				WHEN HolType = 'M'
					THEN '12:00 PM'
				WHEN HolType = 'A'
					THEN '05:00 PM'
				WHEN HolType = 'F'
					THEN '11:59 PM'
				WHEN HolType = 'F-F'
					THEN '11:59 PM'
				WHEN HolType = 'F-M'
					THEN '12:00 PM'
				WHEN HolType = 'A-F'
					THEN '11:59 PM'
			END 
		 ELSE
			CASE
				WHEN
					(E_ABSENCE.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE
					.RETURNDATE)))
					THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, E_ABSENCE.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
				WHEN HolType = '' or HOLTYPE is null 
					THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00'
				WHEN HolType = 'M'
					THEN '12:00 PM'
				WHEN HolType = 'A'
					THEN '05:00 PM'
				WHEN HolType = 'F'
					THEN '11:59 PM'
				WHEN HolType = 'F-F'
					THEN '11:59 PM'
				WHEN HolType = 'F-M'
					THEN '12:00 PM'
				WHEN HolType = 'A-F'
					THEN '11:59 PM'
				WHEN HolType = 'A-M'
					THEN '12:00 PM'						
			END	
		END			AS EndTime
		,CASE
			WHEN
				(E_ABSENCE.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.STARTDATE)))
				THEN DATEDIFF(mi, '1970-01-01', E_ABSENCE.STARTDATE)
			WHEN HolType = '' or HOLTYPE is null 
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '09:00 AM', 103))
			WHEN HolType = 'A'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '01:00 PM', 103))
			WHEN HolType = 'F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103), 103))
			WHEN HolType = 'F-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103), 103))
			WHEN HolType = 'F-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103), 103))
			WHEN HolType = 'A-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '01:00 PM', 103))
			WHEN HolType = 'A-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '01:00 PM', 103))
		END						AS StartTimeInMin
		,CASE
			WHEN
				(E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.HOLTYPE NOT IN ('M','F-M','A-M') AND E_ABSENCE.RETURNDATE IS NOT NULL 
				AND DATEDIFF(day,E_ABSENCE.STARTDATE,E_ABSENCE.RETURNDATE)>0) THEN
				CASE
					WHEN
						(DATEADD(DD,-1,E_ABSENCE.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,E_ABSENCE.RETURNDATE))))
						THEN DATEDIFF(mi, '1970-01-01', DATEADD(DD,-1,E_ABSENCE.RETURNDATE))
					WHEN HolType = '' or HOLTYPE is null 
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00', 103))
					WHEN HolType = 'M'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
					WHEN HolType = 'A'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '05:00 PM', 103))
					WHEN HolType = 'F'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
					WHEN HolType = 'F-F'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
					WHEN HolType = 'F-M'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
					WHEN HolType = 'A-F'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
				END 	
			ELSE
				CASE
					WHEN
						(E_ABSENCE.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.RETURNDATE)))
						THEN DATEDIFF(mi, '1970-01-01', E_ABSENCE.RETURNDATE)
					WHEN HolType = '' or HOLTYPE is null 
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00', 103))
					WHEN HolType = 'M'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
					WHEN HolType = 'A'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
					WHEN HolType = 'F'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
					WHEN HolType = 'F-F'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
					WHEN HolType = 'F-M'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
					WHEN HolType = 'A-F'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
					WHEN HolType = 'A-M'
						THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))		
				END	
			END	AS EndTimeInMin
	FROM
		E_JOURNAL
			INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID
				AND
				E_ABSENCE.ABSENCEHISTORYID IN
				(
					SELECT
						MAX(ABSENCEHISTORYID)
					FROM
						E_ABSENCE
					GROUP BY
						JOURNALID
				)
				INNER JOIN E_JOBDETAILS  ON E_JOURNAL.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
  LEFT JOIN E_HOLIDAYRULE hl ON E_JOBDETAILS.HOLIDAYRULE = hl.EID 
	WHERE
		 (
		-- To filter for planned i.e annual leaves etc. where approval is needed
		(E_ABSENCE.ITEMSTATUSID = 5 AND itemnatureid >= 2  AND itemnatureid <> 47)  
			OR
		-- To filter for sickness leaves. where the operative is now returned to work.
		(ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.RETURNDATE IS NOT NULL )
		 OR (E_ABSENCE.ItemStatusId =3 AND ITEMNATUREID<>47 AND E_JOBDETAILS.ISBRS=1)
		)
		AND CONVERT(DATE, E_ABSENCE.RETURNDATE) >= CONVERT(DATE, @startDate)
		AND E_JOURNAL.employeeid IN
		(
			SELECT
				employeeid
			FROM
				#AvailableOperatives
		)
	UNION ALL
	SELECT
		CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS StartDate
		,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS EndDate
		,o.EmployeeId																																		AS OperativeId
		,'F'																																				AS HolType
		,1																																					AS duration
		,'00:00 AM'																																			AS StartTime
		,'11:59 PM'																																			AS EndTime
		,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
		,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin
	FROM
		G_BANKHOLIDAYS bh
			CROSS JOIN #AvailableOperatives o
	WHERE
		bh.BHDATE >= CONVERT(DATE, GETDATE())		
		AND bh.BHA = 1
	--=================================================================================================================
	------------------------------------------------------ Step 4------------------------------------------------------						
	--This query selects the appointments of employees i.e the employess which we get in step1
	--Fault Appointments
	--Gas Appointments
	--Planned Appointments
	-----------------------------------------------------------------------------------------------------------------------
	-- Fault Appointments
	SELECT DISTINCT
		AppointmentDate																																					AS AppointmentStartDate
		,AppointmentDate																																				AS AppointmentEndDate
		,OperativeId																																					AS OperativeId
		,Time																																							AS StartTime
		,EndTime																																						AS EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103) + ' ' + Time, 103))														AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103) + ' ' + EndTime, 103))													AS EndTimeInSec
		,p__property.postcode																																			AS PostCode
		,CASE  WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS2, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS3, '')  
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(P_BLOCK.ADDRESS2, '') + ', '  
          + ISNULL(P_BLOCK.ADDRESS3, '')  
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
          
      END	AS Address
		,CASE  WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.TownCity, '')
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.TownCity, '')  
          
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ''
          
      END																																	AS TownCity
		,CASE  WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.County, '')
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.County, '')  
          
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ''
          
      END 																																AS County
		,'Fault Appointment'																																			AS AppointmentType			
		, DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LastActionDate)) 																		AS CreationDate
	FROM
		FL_CO_Appointment
			INNER JOIN fl_fault_appointment ON FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
			INNER JOIN fl_fault_log ON fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
			INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
			LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID  
			LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
			LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID 

	WHERE
		1 = 1
		AND appointmentdate >= CONVERT(DATE, @startDate)
		AND (FL_FAULT_STATUS.Description <> 'Cancelled'
			AND FL_FAULT_STATUS.Description
		<> 'Complete')
		AND operativeid IN
		(
			SELECT
				employeeid
			FROM
				#AvailableOperatives
		)
	-----------------------------------------------------------------------------------------------------------------------
	--Appliance Appointments	
	UNION ALL
	SELECT DISTINCT
		AS_APPOINTMENTS.AppointmentDate																																	AS AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate																																AS AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO																																		AS OperativeId
		,APPOINTMENTSTARTTIME																																			AS StartTime
		,APPOINTMENTENDTIME																																				AS EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,p__property.postcode																																			AS PostCode
		,ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')	AS Address
		,P__PROPERTY.TownCity																																			AS TownCity
		,P__PROPERTY.County																																				AS County
		,'Gas Appointment'																																				AS AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE))																				AS CreationDate		
	FROM
		AS_APPOINTMENTS
			INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
			INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
			INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
			INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
			LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
				AND
				(DBO.C_TENANCY.ENDDATE IS NULL
					OR DBO.C_TENANCY.ENDDATE > GETDATE())
	WHERE
		AS_JOURNAL.IsCurrent = 1
		AND CONVERT(DATE, AS_APPOINTMENTS.AppointmentDate, 103) >= CONVERT(DATE, @startDate
		, 103)
		AND AS_APPOINTMENTS.ASSIGNEDTO IN
		(
			SELECT
				employeeid
			FROM
				#AvailableOperatives
		)
		--*********change By Salman   
		--Cancelled Appointment Operatives not visible   
		--Ticket # 6407  
		-- AND (APPOINTMENTSTATUS <> 'Cancelled' AND APPOINTMENTSTATUS <> 'Complete')    
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
	UNION ALL
	SELECT
		AS_APPOINTMENTS.AppointmentDate																																	AS AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate																																AS AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO																																		AS OperativeId
		,APPOINTMENTSTARTTIME																																			AS StartTime
		,APPOINTMENTENDTIME																																				AS EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,'' 																																			AS PostCode
		,ISNULL(P_SCHEME.SCHEMENAME, '') AS Address
		,''																																		AS TownCity
		,''																																			AS County
		,'Gas Appointment'																																				AS AppointmentType
		 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM
		AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P_SCHEME ON AS_JOURNAL.SchemeId = P_SCHEME.SCHEMEID
	WHERE
		AS_JOURNAL.IsCurrent = 1
		AND CONVERT(DATE, AS_APPOINTMENTS.AppointmentDate, 103) >= CONVERT(DATE, @startDate
		, 103)
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
		AND AS_APPOINTMENTS.ASSIGNEDTO IN
		(
			SELECT
				employeeid
			FROM
				#AvailableOperatives
		)
	UNION ALL
	SELECT
		AS_APPOINTMENTS.AppointmentDate																																	AS AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate																																AS AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO																																		AS OperativeId
		,APPOINTMENTSTARTTIME																																			AS StartTime
		,APPOINTMENTENDTIME																																					AS EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,P_BLOCK.POSTCODE																																			AS PostCode
		,ISNULL(P_BLOCK.BLOCKNAME, '') + ' ' + ISNULL(P_BLOCK.ADDRESS1, '') + ' ' + ISNULL(P_BLOCK.ADDRESS2, '') + ' ' + ISNULL(P_BLOCK.ADDRESS3, '')	AS Address
		,P_BLOCK.TownCity																																			AS TownCity
		,P_BLOCK.County																																				AS County
		,'Gas Appointment'																																				AS AppointmentType
		 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM
		AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P_BLOCK ON AS_JOURNAL.BLOCKID = P_BLOCK.BLOCKID
	WHERE
		AS_JOURNAL.IsCurrent = 1
		AND CONVERT(DATE, AS_APPOINTMENTS.AppointmentDate, 103) >= CONVERT(DATE, @startDate
		, 103)
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
		AND AS_APPOINTMENTS.ASSIGNEDTO IN
		(
			SELECT
				employeeid
			FROM
				#AvailableOperatives
		)
----------------------------------------------------------------------------------------------------------------------- 
--Trainings
	UNION ALL 
	SELECT 
		EET.STARTDATE as AppointmentStartDate
		,EET.ENDDATE as AppointmentEndDate
		,EET.EMPLOYEEID  as OperativeId
		,'00:00 AM' as StartTime
		,'11:59 PM' as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), EET.STARTDATE,103) + ' ' + '00:00 AM' ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), EET.ENDDATE,103) + ' ' + '11:59 PM',103)) as EndTimeInSec
		,ISNULL(EET.POSTCODE, ' ')  AS PostCode  
		,ISNULL(EET.LOCATION, '')  AS Address
		,ISNULL(EET.VENUE, '') AS TownCity																																			
		,'' AS County
		,'Training' as AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME,EET.CREATEDDATE)) AS CreationDate
	FROM 
	    E_EmployeeTrainings EET	
	WHERE
		((Convert(date,EET.STARTDATE,103) <= Convert(date,@startDate,103)
		AND Convert(date,EET.ENDDATE,103) >= Convert(date,@startDate,103)))
		AND EET.Active=1 AND EET.Status in (select StatusId from E_EmployeeTrainingStatus where Title = 'Requested' OR Title = 'Manager Supported' OR Title = 'Exec Supported' OR Title = 'Exec Approved' OR Title = 'HR Approved')	
		AND EET.EMPLOYEEID in (SELECT employeeid FROM  #AvailableOperatives) 
----------------------------------------------------------------------------------------------------------------------- 
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Appointments
	UNION ALL
	SELECT DISTINCT
		APPOINTMENTDATE																																					AS AppointmentStartDate
		,APPOINTMENTENDDATE																																				AS AppointmentEndDate
		,ASSIGNEDTO																																						AS OperativeId
		,APPOINTMENTSTARTTIME																																			AS StartTime
		,APPOINTMENTENDTIME																																				AS EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103))										AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))									AS EndTimeInSec
		,p__property.postcode																																			AS PostCode
		,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS2, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS3, '')  
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(P_BLOCK.ADDRESS2, '') + ', '  
          + ISNULL(P_BLOCK.ADDRESS3, '')  
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
          
      END	AS Address
		,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.TownCity, '')
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.TownCity, '')  
          
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ''
          
      END																																	AS TownCity																																			
		,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.County, '')
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.County, '')  
          
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ''
          
      END AS County																																			
		,'Planned Appointment'																																			AS AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME,LOGGEDDATE))																																								AS CreationDate
	FROM
		PLANNED_APPOINTMENTS
			INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId
			INNER JOIN PLANNED_STATUS ON PLANNED_STATUS.STATUSID = PLANNED_JOURNAL.STATUSID
			LEFT JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID  
			LEFT JOIN P_SCHEME ON PLANNED_JOURNAL.SchemeId = P_SCHEME.SCHEMEID  
			LEFT JOIN P_BLOCK ON PLANNED_JOURNAL.BlockId = P_BLOCK.BLOCKID  
	WHERE
		(Convert(date,APPOINTMENTDATE,103) >= Convert(date,@startDate,103)
		OR Convert(date,APPOINTMENTENDDATE,103) >= Convert(date,@startDate,103))
		--OR ((Convert(date,APPOINTMENTDATE,103) >= Convert(date,@startDate,103)	AND
		--Convert(date,APPOINTMENTEndDATE,103) <= Convert(date,@startDate,103)))
		AND PLANNED_APPOINTMENTS.ASSIGNEDTO IN
		(
			SELECT
				employeeid
			FROM
				#AvailableOperatives
		)

		--*********change By Salman   
		--Cancelled Appointment Operatives not visible   
		--Ticket # 6407  
		AND (PLANNED_STATUS.TITLE <> 'Cancelled'
			AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
		)

		-----------------------------------------------------------------------------------------------------------------------    
--M&E Servicing Appointments    
UNION ALL 
SELECT DISTINCT
	APPOINTMENTSTARTDATE																																			AS AppointmentStartDate
	,APPOINTMENTENDDATE																																				as AppointmentEndDate    
	,ASSIGNEDTO																																						AS OperativeId
	,APPOINTMENTSTARTTIME																																			AS StartTime
	,APPOINTMENTENDTIME																																				AS EndTime
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTSTARTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103))								AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))									AS EndTimeInSec
	,COALESCE(p__property.postcode,P_SCHEME.SCHEMECODE,P_BLOCK.POSTCODE)																							AS PostCode	
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		ISNULL(P_SCHEME.SCHEMENAME , '') 		
	ELSE
		ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
		+ ISNULL(P_BLOCK.ADDRESS1 , '') 
		+ ISNULL(', '+P_BLOCK.ADDRESS2, '') 
		+ ISNULL( ' '+P_BLOCK.ADDRESS3, '')
	END																																								AS Address
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		P__PROPERTY.TOWNCITY
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		'' 
	ELSE 
		P_BLOCK.TOWNCITY
	END																																								AS TownCity	
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		P__PROPERTY.COUNTY
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		''
	ELSE 
		P_BLOCK.COUNTY
	END 																																							AS County
	,PDR_MSATType.MSATTypeName+' Appointment'																														AS AppointmentType
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE))																				AS CreationDate
FROM
		PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
		LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
		LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
		INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
WHERE
	(Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)		
		OR Convert(date,APPOINTMENTEndDATE,103) >= Convert(date,@startDate,103))
		--OR ((Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)	AND
		--Convert(date,APPOINTMENTEndDATE,103) <= Convert(date,@startDate,103)))
	AND PDR_APPOINTMENTS.ASSIGNEDTO IN
	(
		SELECT
			employeeid
		FROM
			#AvailableOperatives
	)
	AND (PDR_STATUS.TITLE <> 'Cancelled'
		AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
	)


	DROP TABLE #AvailableOperatives

END