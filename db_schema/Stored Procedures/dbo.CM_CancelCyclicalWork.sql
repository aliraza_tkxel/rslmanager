USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[CM_CancelCyclicalWork]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CM_CancelCyclicalWork] AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[CM_CancelCyclicalWork]    Script Date: 	29/08/2017 14:21:37 ******/

-- =============================================
-- Author:		Ali Raza
-- Create date:	07/09/2017
-- Description:	Assign Cyclical Services to  Contractor
-- WebPage:		PropertyDataRestructure/Views/CyclicalServices/AcceptCyclicalPurchaseOrder.aspx
-- =============================================
ALTER PROCEDURE [dbo].[CM_CancelCyclicalWork]
	 
	@orderId int,
	@reason nvarchar(500),
	@notes nvarchar(1000),
	@cancelBy int,
	@transactionIdentity varchar(100),	
	@isSaved BIT = 0 OUTPUT 	
			
AS
BEGIN
---===============Begin Trasaction===================
BEGIN TRANSACTION
BEGIN TRY


--=============Declare Variables ================
DECLARE @CancelledStatusId int,@serviceItemId int, @cmContractorId int, @poStatus int
SELECT  @CancelledStatusId =  StatusId FROM CM_Status WHERE TITLE = 'Cancelled'
Select @poStatus=F_POSTATUS.POSTATUSID from F_POSTATUS where F_POSTATUS.POSTATUSNAME='Cancelled'
Select @serviceItemId=CM_ContractorWork.ServiceItemId,@cmContractorId=CM_ContractorWork.CMContractorId 
from CM_ContractorWork where CM_ContractorWork.PurchaseOrderId = @orderId
--=============Update Service Item and related tables ================
Update CM_ServiceItems set StatusId = @CancelledStatusId,Active=0 where CM_ServiceItems.ServiceItemId = @serviceItemId
UPDATE CM_ContractorWorkDetail set StatusId = @CancelledStatusId where CM_ContractorWorkDetail.CMContractorId=@cmContractorId
--=============Update Purchase Order and Purchase Item  tables ================

UPDATE F_PURCHASEORDER set POSTATUS = @poStatus,ACTIVE=0  WHERE F_PURCHASEORDER.ORDERID=@orderId
INSERT INTO F_PURCHASEORDER_LOG (IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION) 
SELECT @transactionIdentity, ORDERID, ACTIVE, POTYPE, POSTATUS, GETDATE(), @cancelBy, 'CANCEL' FROM F_PURCHASEORDER WHERE F_PURCHASEORDER.ORDERID = @orderId


Update F_PURCHASEITEM SET PISTATUS=@poStatus, ACTIVE=0 Where F_PURCHASEITEM.ORDERID=@orderId

INSERT INTO F_PURCHASEITEM_LOG (IDENTIFIER,ORDERITEMID,ORDERID,ACTIVE,PITYPE, PISTATUS,TIMESTAMP,ACTIONBY,ACTION) 
SELECT @transactionIdentity, ORDERITEMID, ORDERID, ACTIVE, PITYPE, PISTATUS, GETDATE(), @cancelBy, 'CANCEL' 
FROM F_PURCHASEITEM WHERE F_PURCHASEITEM.ORDERID = @orderId

 	
Update CM_ServiceItems set PORef=NULL where CM_ServiceItems.ServiceItemId = @serviceItemId

INSERT INTO CM_Cancelled(CMContractorId,Reason,Notes,CancelBy,CancelOn)
Values(@cmContractorId,@reason,@notes,@cancelBy,GETDATE())

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;
		SET @isSaved = 1;    	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION; 
		SET @isSaved = 0;    	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

END