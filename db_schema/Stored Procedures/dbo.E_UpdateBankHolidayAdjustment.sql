USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_UpdateBankHolidayAdjustment]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Saud Ahmed
-- Create date: November 28, 2018
-- =============================================

IF OBJECT_ID('dbo.E_UpdateBankHolidayAdjustment') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_UpdateBankHolidayAdjustment AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_UpdateBankHolidayAdjustment] 
AS
BEGIN


	DECLARE @EMPLOYEEID int, @ISBRS int, @HolidayIndicator int,@HOURS float,@TOTALLEAVEHOURS float,@BANKHOLIDAY float,@CARRYFORWARD float,@CARRYFORWARDHOURS float,
@HOLIDAYENTITLEMENTDAYS float,@HOLIDAYENTITLEMENTHOURS float, @BankHolidayCount int, @updatedTotalLeave float, @formula float;

DECLARE @BHSTART SMALLDATETIME,	 @BHEND SMALLDATETIME	

DECLARE db_cursor CURSOR FOR SELECT EMPLOYEEID, ISBRS, HolidayIndicator,HOURS,TOTALLEAVEHOURS,BANKHOLIDAY,CARRYFORWARD,CARRYFORWARDHOURS,HOLIDAYENTITLEMENTDAYS,HOLIDAYENTITLEMENTHOURS 
FROM E_JOBDETAILS where ACTIVE=1 And ISBRS IS NOT NULL;

OPEN db_cursor;
FETCH NEXT FROM db_cursor INTO @EMPLOYEEID, @ISBRS, @HolidayIndicator,@HOURS,@TOTALLEAVEHOURS,@BANKHOLIDAY,@CARRYFORWARD,@CARRYFORWARDHOURS,@HOLIDAYENTITLEMENTDAYS
,@HOLIDAYENTITLEMENTHOURS ;
WHILE @@FETCH_STATUS = 0  
BEGIN  

SET @HOURS=ISNULL(@HOURS,0)
SET @BANKHOLIDAY=ISNULL(@BANKHOLIDAY,0)
SET @CARRYFORWARD=ISNULL(@CARRYFORWARD,0)
SET @CARRYFORWARDHOURS=ISNULL(@CARRYFORWARDHOURS,0)
SET @HOLIDAYENTITLEMENTDAYS=ISNULL(@HOLIDAYENTITLEMENTDAYS,0)
SET @HOLIDAYENTITLEMENTHOURS=ISNULL(@HOLIDAYENTITLEMENTHOURS,0)
-----Fetch Annual Leave Start Date and EndDaate
     SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	 FROM	EMPLOYEE_ANNUAL_START_END_DATE_ByYear(@EMPLOYEEID, GETDATE())

--------Fetch Bank Holiday count

Select @BankHolidayCount = Count(*) from G_BANKHOLIDAYS bh where bh.BHDATE >= @BHSTART and bh.BHDATE <= @BHEND AND (bh.BHA=1 OR bh.MeridianEast=1)

IF (@HolidayIndicator =1 and @ISBRS = 1)
	BEGIN
		Set @formula = (@BankHolidayCount * @HOURS)/40;
		if(@formula > @BankHolidayCount) 
		begin
			set @formula = @BankHolidayCount
		end
		set @formula = cast(round(Ceiling(@formula * 2) / 2, 2) as numeric(36,2))

		SET @updatedTotalLeave = @HOLIDAYENTITLEMENTDAYS + @CARRYFORWARD + @formula;
		set @updatedTotalLeave = cast(round(@updatedTotalLeave, 2) as numeric(36,2))

		update E_JOBDETAILS set TOTALLEAVE = @updatedTotalLeave, BANKHOLIDAY = @formula where EMPLOYEEID = @EMPLOYEEID
	END
ELSE IF (@HolidayIndicator = 1 and @ISBRS = 0)
	BEGIN
		Set @formula = (@BankHolidayCount * @HOURS)/37;
		if(@formula > @BankHolidayCount) 
		begin
			set @formula = @BankHolidayCount
		end
		set @formula = cast(round(Ceiling(@formula * 2) / 2, 2) as numeric(36,2))

		SET @updatedTotalLeave = @HOLIDAYENTITLEMENTDAYS + @CARRYFORWARD + @formula;
		set @updatedTotalLeave = cast(round(@updatedTotalLeave, 2) as numeric(36,2))
		update E_JOBDETAILS set TOTALLEAVE = @updatedTotalLeave, BANKHOLIDAY = @formula where EMPLOYEEID = @EMPLOYEEID
		
	END

ELSE IF (@HolidayIndicator=2  and @ISBRS = 1)
	BEGIN
		Set @formula = (@BankHolidayCount * @HOURS)/40;
		set @formula = @formula * 8
		if(@formula > (@BankHolidayCount * 8)) 
		begin
			set @formula = @BankHolidayCount * 8
		end
		set @formula = cast(round(CEILING(@formula), 2) as numeric(36,2))

		SET @updatedTotalLeave = @HOLIDAYENTITLEMENTHOURS + @CARRYFORWARDHOURS + @formula;
		set @updatedTotalLeave = cast(round(@updatedTotalLeave, 2) as numeric(36,2))
		update E_JOBDETAILS set TOTALLEAVEHOURS = @updatedTotalLeave, BANKHOLIDAY = @formula where EMPLOYEEID = @EMPLOYEEID
	END
ELSE IF (@HolidayIndicator =2 and @ISBRS = 0)
	BEGIN
		Set @formula = (@BankHolidayCount * @HOURS)/37;
		set @formula = @formula * 7.4
		if(@formula > (@BankHolidayCount * 7.4)) 
		begin
			set @formula = @BankHolidayCount * 7.4
		end
		set @formula = cast(round(CEILING(@formula), 2) as numeric(36,2))

		SET @updatedTotalLeave = @HOLIDAYENTITLEMENTHOURS + @CARRYFORWARDHOURS + @formula;
		set @updatedTotalLeave = cast(round(@updatedTotalLeave, 2) as numeric(36,2))
		update E_JOBDETAILS set TOTALLEAVEHOURS = @updatedTotalLeave, BANKHOLIDAY = @formula where EMPLOYEEID = @EMPLOYEEID
	END
	--Print('-----------------------')
	--Print(@EMPLOYEEID)
	--Print (@formula)
	--Print (@updatedTotalLeave)


FETCH NEXT FROM db_cursor INTO @EMPLOYEEID, @ISBRS, @HolidayIndicator,@HOURS,@TOTALLEAVEHOURS,@BANKHOLIDAY,@CARRYFORWARD,@CARRYFORWARDHOURS,@HOLIDAYENTITLEMENTDAYS,@HOLIDAYENTITLEMENTHOURS;
END;
CLOSE db_cursor;
DEALLOCATE db_cursor;
END