USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionChargeExProperties]    Script Date: 12/10/2018 5:41:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 IF OBJECT_ID('dbo.[PDR_GetProvisionChargeExProperties]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetProvisionChargeExProperties] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetProvisionChargeExProperties]
(
	@schemeId		INT=null, 
	@blockId		INT=null,
	@itemid			INT=null)
AS
BEGIN
	
	IF(@schemeId > 0)
	BEGIN
		 SELECT ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
		 FROM   P__PROPERTY P
		                        where P.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ProvisionChargeProperties P1 
								where P1.IsIncluded=1 AND P1.SCHEMEID=@schemeId AND IsActive=1 AND P1.ItemID=@itemid
								GROUP BY P1.BLOCKID, P1.ItemId, P1.PROPERTYID)	and P.SCHEMEID=@schemeId
	END
	ELSE
	BEGIN
		SELECT ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
		FROM   P__PROPERTY P
		                        where P.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ProvisionChargeProperties P1 
								where P1.IsIncluded=1 AND P1.BLOCKID=@blockId AND IsActive=1 AND P1.ItemID=@itemid
								GROUP BY P1.BLOCKID, P1.ItemId, P1.PROPERTYID)	and P.BLOCKID=@blockId
	END

	

END



GO


