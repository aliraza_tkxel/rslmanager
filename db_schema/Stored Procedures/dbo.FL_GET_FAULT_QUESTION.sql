SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_GET_FAULT_QUESTION
/* ===========================================================================
 '   NAME:           FL_GET_FAULT_QUESTION
 '   DATE CREATED:   14th Oct 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the fault question
 '   IN:             @QuestionId
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
	@QuestionID INT	
	)	
	
AS 	

SELECT * 
FROM FL_FAULT_Question
WHERE FQuestionId = @QuestionID













GO
