SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC [dbo].[RD_GETFAULTSTATUS]
-- Author:		<Ahmed Mehmood>
-- Create date: <24/7/2013>
-- Description:	<Returns todays appointments of an operative>
-- =============================================
CREATE PROCEDURE [dbo].[RD_GETFAULTSTATUS]

AS
	SELECT FaultStatusID AS id,Description AS val
	FROM FL_FAULT_STATUS 
	WHERE FL_FAULT_STATUS.ACTIVE =1
	














GO
