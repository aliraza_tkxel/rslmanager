SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
















CREATE PROCEDURE dbo.FL_CO_GETREPAIRDATA
/* ===========================================================================
 '   NAME:          FL_CO_GETREPAIRDATA
 '   DATE CREATED:   02 Jan  2009
 '   CREATED BY:     Waseem Hassan	
 '   CREATED FOR:    RSL
 '   PURPOSE:        To get repair description from  FL_FAULT_REPAIR_LIST table which will used for autocomplete
  '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

AS
	SELECT FaultRepairListID AS id,Description AS val
	FROM FL_FAULT_REPAIR_LIST












GO
