USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E__EmployeeJournal]    Script Date: 6/6/2017 3:39:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: July 14, 2017
-- =============================================

IF OBJECT_ID('dbo.E__EmployeeJournal') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E__EmployeeJournal AS SET NOCOUNT ON;') 
GO
--///////////////////////////////////////////////

ALTER PROCEDURE E__EmployeeJournal
	(
	@employeeId int
	)
	
AS
BEGIN
Select * from (

SELECT         
       J.EMPLOYEEID as employeeId,
       ISNULL(I.DESCRIPTION, 'N/A') AS ITEM,
       ISNULL(N.DESCRIPTION, 'N/A') AS NATURE,
       CASE J.ITEMNATUREID
           WHEN 1 THEN LTRIM(ISNULL(ER.DESCRIPTION,'-') + ' ' + ISNULL(EA.REASON,''))
           ELSE ISNULL(J.TITLE, 'N/A')
       END TITLE,
       EA.NOTES,
       ISNULL(S.DESCRIPTION, 'N/A') AS STATUS ,
	   Case when J.ITEMID = 1 then 'mnuAbsence'
	   else 'mnuJournal' end AS REDIR
	   ,ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103),'-') AS CreationDate
	   ,J.CREATIONDATE as CreationDateString
	   ,ISNULL(CONVERT(NVARCHAR, EA.LASTACTIONDATE, 103),'-') AS LastActionDate
	   ,EA.LASTACTIONDATE as LastActionDateString
	   , '-' AS DocumentPath
	   , '-' AS DoiStatus
	   , '-' AS ItemId

FROM E_JOURNAL J
LEFT JOIN E_ITEM I ON J.ITEMID = I.ITEMID
LEFT JOIN E_STATUS S ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID
LEFT JOIN E_NATURE N ON J.ITEMNATUREID = N.ITEMNATUREID
LEFT JOIN E_ABSENCE EA ON EA.JOURNALID=J.JOURNALID
AND EA.ABSENCEHISTORYID=
  (SELECT MAX(ABSENCEHISTORYID)
   FROM E_ABSENCE
   WHERE JOURNALID=J.JOURNALID
     AND J.EMPLOYEEID= @employeeId
     AND J.ITEMID=1)
LEFT JOIN E_ABSENCEREASON ER ON ER.SID = EA.REASONID
WHERE J.EMPLOYEEID = @employeeId
  AND YEAR(J.CREATIONDATE) = YEAR(GETDATE())
 
--/////////////////////////////////////////////////////
UNION ALL
--///////////////////Personal Details//////////////////////////////
SELECT E.EMPLOYEEID,'Personal Details','-','-','-','-','mnuPersonalDetail'
,ISNULL(CONVERT(NVARCHAR, Emp_LogData.CreationDate, 103),'-') CreationDate
,Emp_LogData.CreationDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, EH.LASTACTIONTIME, 103),'-') LastActionDate
,EH.LASTACTIONTIME as LastActionDateString 
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E__EMPLOYEE E
INNER JOIN E__EMPLOYEE_HISTORY EH On E.EMPLOYEEID = EH.EMPLOYEEID
INNER JOIN (SELECT	employeeId,  MIN(LASTACTIONTIME) CreationDate
			FROM	E__EMPLOYEE_HISTORY 
			GROUP BY employeeId) Emp_LogData on  E.EMPLOYEEID = Emp_LogData.EMPLOYEEID
WHERE E.EMPLOYEEID = @employeeId
--///////////////////////////////////////////////////////////
UNION ALL
--/////////////////////////////Contact Info///////////////////////////////////
SELECT E.EMPLOYEEID,'Contact Info','-','-','-','-','mnuPersonalDetail'
,ISNULL(CONVERT(NVARCHAR, Emp_LogData.CreationDate, 103),'-') CreationDate
,Emp_LogData.CreationDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, EH.LASTACTIONTIME, 103),'-') LastActionDate
,EH.LASTACTIONTIME as LastActionDateString 
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E_CONTACT E
INNER JOIN E_CONTACT_HISTORY EH On E.CONTACTID = EH.CONTACTID
INNER JOIN (SELECT	employeeId,  MIN(LASTACTIONTIME) CreationDate
			FROM	E_CONTACT_HISTORY 
			GROUP BY employeeId) Emp_LogData on  E.EMPLOYEEID = Emp_LogData.EMPLOYEEID
WHERE E.EMPLOYEEID = @employeeId
--//////////////////////////////////////////////////////////////
UNION ALL
--//////////////////////Job Details///////////////////////////////////
SELECT E.EMPLOYEEID,'Job Details','-','-','-','-','mnuPost'
,ISNULL(CONVERT(NVARCHAR, Emp_LogData.CreationDate, 103),'-') CreationDate
,Emp_LogData.CreationDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, EH.LASTACTIONTIME, 103),'-') LastActionDate
,EH.LASTACTIONTIME as LastActionDateString 
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E_JOBDETAILS E
INNER JOIN E_JOBDETAILS_HISTORY EH On E.JOBDETAILSID = EH.JOBDETAILSID
INNER JOIN (SELECT	employeeId,  MIN(LASTACTIONTIME) CreationDate
			FROM	E_JOBDETAILS_HISTORY 
			GROUP BY employeeId) Emp_LogData on  E.EMPLOYEEID = Emp_LogData.EMPLOYEEID
WHERE E.EMPLOYEEID = @employeeId

UNION ALL
--//////////////////////Benefit Details///////////////////////////////////
SELECT E.EMPLOYEEID,'Benefits','-','-','-','-','mnuBenefit'
,ISNULL(CONVERT(NVARCHAR, Emp_LogData.CreationDate, 103),'-') CreationDate
,Emp_LogData.CreationDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, EH.LASTACTIONTIME, 103),'-') LastActionDate
,EH.LASTACTIONTIME as LastActionDateString 
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E_BENEFITS E
INNER JOIN E_BENEFITSHISTORY EH On E.BENEFITID = EH.BENEFITID
INNER JOIN (SELECT	BENEFITID,  MIN(LASTACTIONTIME) CreationDate
			FROM	E_BENEFITSHISTORY 
			GROUP BY BENEFITID) Emp_LogData on  EH.BENEFITID = Emp_LogData.BENEFITID
WHERE E.EMPLOYEEID = @employeeId

UNION ALL
--//////////////////////Trainings///////////////////////////////////
SELECT distinct E.EMPLOYEEID
,'Training','N/A' as Nature,EH.Course as Title,EH.AdditionalNotes as Notes,s.title,'mnuTraining'
,ISNULL(CONVERT(NVARCHAR, EH.CreatedDate, 103),'-') CreationDate
,EH.CreatedDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, coalesce(EH.UpdateDate,EH.CreatedDate), 103),'-') LastActionDate
,coalesce(EH.UpdateDate,EH.CreatedDate) as LastActionDateString 
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E_EmployeeTrainings E 
INNER JOIN E_EmployeeTrainings_History EH On E.EMPLOYEEID = EH.EMPLOYEEID
INNER JOIN E_EmployeeTrainingStatus s on EH.Status = s.StatusId
WHERE E.EMPLOYEEID =  @employeeId

UNION ALL
--//////////////////////Skills///////////////////////////////////
SELECT E.EMPLOYEEID,'Skills and Quals','-','-','-','-','mnuSkills'
,ISNULL(CONVERT(NVARCHAR, Emp_LogData.CreationDate, 103),'-') CreationDate
,Emp_LogData.CreationDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, EH.LASTACTIONTIME, 103),'-') LastActionDate
,EH.LASTACTIONTIME as LastActionDateString  
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E_QUALIFICATIONSANDSKILLS E
INNER JOIN E_QUALIFICATIONSANDSKILLS_HISTORY EH On E.QUALIFICATIONSID = EH.QUALIFICATIONSID
INNER JOIN (SELECT	QUALIFICATIONSID,  MIN(LASTACTIONTIME) CreationDate
			FROM	E_QUALIFICATIONSANDSKILLS_HISTORY 
			GROUP BY QUALIFICATIONSID) Emp_LogData on  EH.QUALIFICATIONSID = Emp_LogData.QUALIFICATIONSID
WHERE E.EMPLOYEEID = @employeeId

UNION ALL
--//////////////////////Health Tab///////////////////////////////////
SELECT E.EMPLOYEEID,'Health',
--case when ED.DTYPE=1 then 'Disability or Health Condition'
--else 'Learning Difficulties' end
'-'
,'-','-', Case when ED.DTYPE=1 and EH.ISDELETE=1 then 'Deleted' when ED.DTYPE=1 and (EH.ISDELETE=0 OR EH.ISDELETE IS NULL) then 'Added' else '-' end,'mnuHealth'
,ISNULL(CONVERT(NVARCHAR, Emp_LogData.CreationDate, 103),'-') CreationDate
,Emp_LogData.CreationDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, EH.LASTACTIONTIME, 103),'-') LastActionDate
,EH.LASTACTIONTIME as LastActionDateString  
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E__EMPLOYEE E
INNER JOIN E_DIFFDISID_HISTORY EH On E.EMPLOYEEID = EH.EMPLOYEEID
INNER JOIN E_DIFFDIS ED On ED.DIFFDISID=EH.DIFFDIS
INNER JOIN (SELECT	DISABILITYID,  MIN(LASTACTIONTIME) CreationDate
			FROM	E_DIFFDISID_HISTORY 
			GROUP BY DISABILITYID) Emp_LogData on  EH.DISABILITYID = Emp_LogData.DISABILITYID
WHERE E.EMPLOYEEID = @employeeId


UNION ALL
--//////////////////////Pay Point///////////////////////////////////
SELECT PPH.EMPLOYEEID,'Pay Point' AS ITEM
,'request for pay point increase' AS NATURE
,'application made by '+LM.FIRSTNAME +' '+ LM.LASTNAME+'' AS TITLE
, 'Application submitted by '+LM.FIRSTNAME +' '+ LM.LASTNAME+' on '+ISNULL(CONVERT(NVARCHAR(20), PPH.ApprovedDate, 103),'-')+' and '+S.Description+' on '+
		Case when S.Description='Submitted' and PPH.Approved=1 then ''+ISNULL(CONVERT(NVARCHAR(20), PPH.ApprovedDate, 103),'-')+' by Line Manager'
	  when S.Description='Supported' and PPH.Supported=1 then ''+ISNULL(CONVERT(NVARCHAR(20), PPH.SupportedDate, 103),'-')+' by Director' 
	  when S.Description='Authorized' and PPH.Authorized=1 then ''+ISNULL(CONVERT(NVARCHAR(20), PPH.AuthorizedDate, 103),'-')+' by CEO'
	  when S.Description='Declined' and PPH.SupportedBy is not null and PPH.AuthorizedBy is null then ''+ISNULL(CONVERT(NVARCHAR(20), PPH.SupportedDate, 103),'-')+' by Director'  
	  when S.Description='Declined' and PPH.AuthorizedBy is not null then ''+ISNULL(CONVERT(NVARCHAR(20), PPH.AuthorizedDate, 103),'-')+' by CEO' else '-' end AS NOTES
, Case when S.Description='Submitted' and PPH.Approved=1 then 'Submitted by Manager'
	  when S.Description='Supported' and PPH.Supported=1 then 'Approved by Director' 
	  when S.Description='Authorized' and PPH.Authorized=1 then 'Authorised by Exec'
	  when S.Description='Declined' and PPH.SupportedBy is not null and PPH.AuthorizedBy is null then 'Declined by Director'  
	  when S.Description='Declined' and PPH.AuthorizedBy is not null then 'Declined by Exec' else S.Description end as Status
,'mnuPersonalDetail' AS REDIR
,ISNULL(CONVERT(VARCHAR, PPH.DateProposed, 103), '-') CreationDate
,PPH.ApprovedDate as CreationDateString
, ISNULL(CONVERT(VARCHAR, PPH.DateProposed, 103), '-') LastActionDate
,PPH.ApprovedDate as LastActionDateString 
, '-' AS DocumentPath
, '-' AS DoiStatus
, '-' AS ItemId

FROM E_PaypointSubmission_HISTORY PPH
	inner join E_PayPointStatus S on PPH.paypointStatusId = S.paypointStatusId 
	Outer apply (Select * from E__Employee e where e.employeeid=PPH.ApprovedBy and PPH.Approved=1) LM
	Outer apply (Select * from E__Employee e where e.employeeid=PPH.SupportedBy and PPH.Supported=1) D
	Outer apply (Select * from E__Employee e where e.employeeid=PPH.AuthorizedBy and PPH.Authorized=1) CEO
	LEFT JOIN E_JOBDETAILS jd on PPH.EMPLOYEEID = jd.EMPLOYEEID
	LEFT join E_JOBROLETEAM JRT on LM.JobRoleTeamId = JRT.JobRoleTeamId
WHERE PPH.EMPLOYEEID = @employeeId

UNION ALL
--//////////////////////Declaration of Interests Tab///////////////////////////////////
SELECT E.EMPLOYEEID
, 'Declaration of Interests' AS ITEM
, eds.Description AS NATURE
, 'N/A' AS TITLE
, '-' AS NOTES
, Case when eds.Description='Submitted' then 'Pending'
	  when eds.Description='Approved' then 'Approved' else eds.Description end as Status
, 'mnuPersonalDetail' AS REDIR
,ISNULL(CONVERT(NVARCHAR, ed1.CreatedOn, 103),'-') CreationDate
,ed1.CreatedOn  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, ed1.UpdatedOn, 103),'-') LastActionDate
,ed1.UpdatedOn as LastActionDateString
, hd.DocumentPath AS DocumentPath
, (Select s.description from E_DOI ed INNER JOIN E_DOI_Status s ON ed.Status = s.DOIStatusId WHERE ed.InterestId = ed1.InterestId) AS DoiStatus
, '-' AS ItemId

FROM E__EMPLOYEE E
INNER JOIN E_DOI_HISTORY ed1 On E.EMPLOYEEID = ed1.EMPLOYEEID
INNER JOIN E_DOI_Status eds On ed1.Status = eds.DOIStatusId
LEFT JOIN HR_Documents hd ON ed1.DocumentId = hd.DocumentId
WHERE E.EMPLOYEEID = @employeeId


UNION ALL
--////////////////////// Gift & Hospitality ///////////////////////////////////
SELECT E.EMPLOYEEID
, 'Gift & Hospitality' AS ITEM
, Case when gh.IsGivenReceived=1 then 'Receiver'
	  when gh.IsGivenReceived=2 then 'Given' else CONVERT(NVARCHAR, gh.IsGivenReceived) end  AS NATURE
, gh.Details + ' value �'+ CONVERT(NVARCHAR, gh.ApproximateValue) AS TITLE
, gh.Notes AS NOTES
, GS.Description as Status
, 'mnuPersonalDetail' AS REDIR
,ISNULL(CONVERT(NVARCHAR, gh.CreatedDate, 103),'-') CreationDate
,gh.CreatedDate  as CreationDateString
,ISNULL(CONVERT(NVARCHAR, gh.UpdatedDate, 103),'-') LastActionDate
,gh.UpdatedDate as LastActionDateString
, '-' AS DocumentPath
, '-' AS DoiStatus
, ISNULL(CONVERT(NVARCHAR, gh.GiftId),'-') AS ItemId

FROM E__EMPLOYEE E
INNER JOIN E_GiftsAndHospitalities_History gh On E.EMPLOYEEID = gh.EMPLOYEEID
INNER JOIN E_Gift_Status GS On gh.GiftStatusId = GS.GiftStatusId
WHERE E.EMPLOYEEID = @employeeId

) as result
order by result.LastActionDateString

END
GO