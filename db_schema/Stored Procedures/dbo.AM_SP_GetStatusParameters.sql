SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AM_SP_GetStatusParameters]
 
AS
BEGIN

 SELECT   Top 1 Convert(Varchar(100), AM_StatusHistory.ModifiedDate, 103) as StatusModifiedDate, 
    (SELECT CodeName FROM AM_LookupCode WHERE LookupCodeId = AM_StatusHistory.RentParameter) as RentParameter, 
      AM_StatusHistory.IsRentParameter, AM_StatusHistory.RentAmount, AM_StatusHistory.IsRentAmount, AM_LookupCode.CodeName,
    AM_StatusHistory.StatusHistoryId
 
 FROM         AM_StatusHistory LEFT OUTER JOIN 
    AM_LookupCode on AM_LookupCode.LookupCodeId = AM_StatusHistory.RentParameterFrequencyLookupCodeId
 
 WHERE AM_StatusHistory.Title = 'First Detection'

 order by AM_StatusHistory.StatusHistoryId DESC

END





GO
