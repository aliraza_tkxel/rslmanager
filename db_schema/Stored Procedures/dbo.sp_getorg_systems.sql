SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[sp_getorg_systems]
@org_id int
AS 

SELECT H_SYSTEM.*,
(select count(org_id) from H_ORG_SYSTEM_LINKAGE
where H_ORG_SYSTEM_LINKAGE.org_id = @org_id
and H_ORG_SYSTEM_LINKAGE.system_id = H_SYSTEM.system_id ) 
as systemcount
from
H_SYSTEM 


GO
