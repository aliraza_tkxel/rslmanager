USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[PDR_SaveSchemeBlockPhotograph]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_SaveSchemeBlockPhotograph] AS SET NOCOUNT ON;') 
GO 
ALTER PROCEDURE [dbo].[PDR_SaveSchemeBlockPhotograph] (    
@Id int=null,    
@itemId int,    
@title varchar(500),    
@uploadDate smalldatetime,    
@imagePath varchar(1000),    
@imageName varchar(500),    
@createdBy int,  
@requestType varchar(20), 
@isDefaultImage bit = null 
)    
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  
	 
	if(@requestType = 'block')
	begin
		INSERT INTO PA_PROPERTY_ITEM_IMAGES  
			([BlockId], [ItemId], [ImagePath], [ImageName], [CreatedOn], [CreatedBy], [Title])  
		VALUES  
			(@Id, @itemId, @imagePath, @imageName, @uploadDate, @createdBy, @title) 
	END
	ELSE
	BEGIN
		INSERT INTO PA_PROPERTY_ITEM_IMAGES  
			([SchemeId], [ItemId], [ImagePath], [ImageName], [CreatedOn], [CreatedBy], [Title])  
		VALUES  
			(@Id, @itemId, @imagePath, @imageName, @uploadDate, @createdBy, @title) 
	END 

END  

