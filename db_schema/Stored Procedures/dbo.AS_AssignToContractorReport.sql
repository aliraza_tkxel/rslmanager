USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AssignToContractorReport]    Script Date: 03/24/2016 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.AS_AssignToContractorReport') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_AssignToContractorReport AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 19/2/2016
-- Description:	Display the list of Appliances having Assign to contractor status
-- WebPage: RSLApplianceServicing/Views/Reports
-- EXEC AS_AssignToContractorReport '',30000000,1,'Address','DESC'

-- =============================================
ALTER PROCEDURE [dbo].[AS_AssignToContractorReport]				
		@searchedText varchar(5000),		
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Address',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int=0 output
AS
BEGIN
		DECLARE @SelectClause varchar(MAX),
		@selectClaseForCountProperty varchar(MAX),
        @fromClause   varchar(MAX),
        @whereClause  varchar(1000),	
		@SelectClauseScheme varchar(MAX),
		@selectClaseForCountScheme varchar(MAX),
        @fromClauseScheme   varchar(MAX),
        @whereClauseScheme  varchar(1000),	
		@SelectClauseBlock varchar(MAX),
		@selectClaseForCountBlock varchar(MAX),
        @fromClauseBlock   varchar(MAX),
        @whereClauseBlock  varchar(1000),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(8000),   
		@mainSelectQueryScheme varchar(8000), 
		@mainSelectQueryBlock varchar(8000),      
        @rowNumberQuery varchar(8000),
        @finalQuery varchar(8000),
		@unionQuery varchar(1000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1000),
		@searchCriteriaScheme varchar(1000),
		@searchCriteriaBlock varchar(1000),
		@resultingQuery varchar(max),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		--SET @searchCriteria = 'AS_JOURNAL.STATUSID = 6  AND ' 
		SELECT @searchCriteria = ' 1=1 ', @searchCriteriaScheme = ' 1=1 ', @searchCriteriaBlock = ' 1=1 '
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (P__PROPERTY.ADDRESS1 LIKE ''%'+@searchedText+'%''  OR P__PROPERTY.ADDRESS2 LIKE ''%'+@searchedText+'%''  OR P__PROPERTY.ADDRESS3 LIKE ''%@'+@searchedText+'%''  OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+@searchedText+'%'')'
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'AND (P_SCHEME.SCHEMENAME LIKE ''%'+@searchedText+'%'')'
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'AND (P_BLOCK.ADDRESS1 LIKE ''%'+@searchedText+'%''  OR P_BLOCK.ADDRESS2 LIKE ''%'+@searchedText+'%''  OR P_BLOCK.ADDRESS3 LIKE ''%@'+@searchedText+'%''  OR P_BLOCK.BLOCKNAME LIKE ''%'+@searchedText+'%'')'
		END 
				
		
		
		--These conditions wíll be used in every case
		
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND AS_JOURNAL.STATUSID = 6 
									AND AS_JOURNAL.IsCurrent = 1
									AND PV.ValueDetail = ''Mains Gas'' 
									AND dbo.P__PROPERTY.STATUS NOT IN (9,5,6) 
									AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
									AND FP.POSTATUS NOT IN (SELECT POSTATUSID FROM F_POSTATUS WHERE F_POSTATUS.POSTATUSNAME LIKE ''Cancelled'')'

		SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'AND AS_JOURNAL.STATUSID = 6 
									AND AS_JOURNAL.IsCurrent = 1
									AND PV.ValueDetail = ''Mains Gas'' 
									AND FP.POSTATUS NOT IN (SELECT POSTATUSID FROM F_POSTATUS WHERE F_POSTATUS.POSTATUSNAME LIKE ''Cancelled'')'

		SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'AND AS_JOURNAL.STATUSID = 6 
									AND AS_JOURNAL.IsCurrent = 1
									AND PV.ValueDetail = ''Mains Gas'' 
									AND FP.POSTATUS NOT IN (SELECT POSTATUSID FROM F_POSTATUS WHERE F_POSTATUS.POSTATUSNAME LIKE ''Cancelled'')'
	
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		SET @SelectClause = 
		'SELECT DISTINCT top ('+convert(varchar(10),@limit)+')
		''JS'' + CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,							
		ISNULL(NULLIF(ISNULL(''Flat No:'' + P__PROPERTY.FLATNUMBER + '','', '''') +
		 ISNULL(P__PROPERTY.HOUSENUMBER, '''') + 
		ISNULL('' '' + P__PROPERTY.ADDRESS1, '''')+
		ISNULL('' '' + P__PROPERTY.ADDRESS2, '''') + ISNULL('' '' + P__PROPERTY.ADDRESS3, '''') 
		+ ISNULL('' '' + P__PROPERTY.TOWNCITY, '''') , ''''), ''N/A'') AS [Address],
		ISNULL(P__PROPERTY.POSTCODE,'''') POSTCODE,
		ISNULL(pin.Notes,''N/A'') AS Description,
		org.NAME AS contractor,
		acw.AssignedDate  AS AssignedDate,
		(ee.FIRSTNAME + '' '' + ee.LASTNAME) AS AssignedBy,
		DATEADD(YEAR,1,pl.ISSUEDATE)  AS CP12Expiry,
		AS_Status.Title AS Status,
		acw.ApplianceServicingContractorId as ASContractorID
		,''Property'' as AppointmentType '

		SET @selectClaseForCountProperty = 
		'SELECT DISTINCT
		''JS'' + CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,							
		ISNULL(NULLIF(ISNULL(''Flat No:'' + P__PROPERTY.FLATNUMBER + '','', '''') +
		 ISNULL(P__PROPERTY.HOUSENUMBER, '''') + 
		ISNULL('' '' + P__PROPERTY.ADDRESS1, '''')+
		ISNULL('' '' + P__PROPERTY.ADDRESS2, '''') + ISNULL('' '' + P__PROPERTY.ADDRESS3, '''') 
		+ ISNULL('' '' + P__PROPERTY.TOWNCITY, '''') , ''''), ''N/A'') AS [Address],
		ISNULL(P__PROPERTY.POSTCODE,'''') POSTCODE,
		ISNULL(pin.Notes,''N/A'') AS Description,
		org.NAME AS contractor,
		acw.AssignedDate  AS AssignedDate,
		(ee.FIRSTNAME + '' '' + ee.LASTNAME) AS AssignedBy,
		DATEADD(YEAR,1,pl.ISSUEDATE)  AS CP12Expiry,
		AS_Status.Title AS Status,
		acw.ApplianceServicingContractorId as ASContractorID
		,''Property'' as AppointmentType '

		SET @SelectClauseScheme = 
		'SELECT DISTINCT top ('+convert(varchar(10),@limit)+')
		''JS'' + CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,							
		 ISNULL(P_SCHEME.SCHEMENAME, '''') AS [Address],
		'''' POSTCODE,
		ISNULL(pin.Notes,''N/A'') AS Description,
		org.NAME AS contractor,
		AS_CONTRACTOR_WORK.AssignedDate AS AssignedDate,
		(ee.FIRSTNAME + '' '' + ee.LASTNAME) AS AssignedBy,
		DATEADD(YEAR,1,pl.ISSUEDATE)  AS CP12Expiry,
		AS_Status.Title AS Status,
		AS_CONTRACTOR_WORK.ApplianceServicingContractorId as ASContractorID
		,''Scheme'' as AppointmentType '

		SET @selectClaseForCountScheme = 
		'SELECT DISTINCT 
		''JS'' + CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,							
		 ISNULL(P_SCHEME.SCHEMENAME, '''') AS [Address],
		'''' POSTCODE,
		ISNULL(pin.Notes,''N/A'') AS Description,
		org.NAME AS contractor,
		AS_CONTRACTOR_WORK.AssignedDate AS AssignedDate,
		(ee.FIRSTNAME + '' '' + ee.LASTNAME) AS AssignedBy,
		DATEADD(YEAR,1,pl.ISSUEDATE)  AS CP12Expiry,
		AS_Status.Title AS Status,
		AS_CONTRACTOR_WORK.ApplianceServicingContractorId as ASContractorID
		,''Scheme'' as AppointmentType '

		SET @SelectClauseBlock = 
		'SELECT DISTINCT top ('+convert(varchar(10),@limit)+')
		''JS'' + CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,							
		 ISNULL(P_BLOCK.BLOCKNAME, '''') + ISNULL(P_BLOCK.ADDRESS1, '''') + ISNULL(P_BLOCK.ADDRESS3, '''') + ISNULL(P_BLOCK.ADDRESS3, '''') 
		 AS [Address],
		ISNULL(P_BLOCK.POSTCODE, '''') POSTCODE,
		ISNULL(pin.Notes,''N/A'') AS Description,
		org.NAME AS contractor,
		AS_CONTRACTOR_WORK.AssignedDate AS AssignedDate,
		(ee.FIRSTNAME + '' '' + ee.LASTNAME) AS AssignedBy,
		DATEADD(YEAR,1,pl.ISSUEDATE)  AS CP12Expiry,
		AS_Status.Title AS Status,
		AS_CONTRACTOR_WORK.ApplianceServicingContractorId as ASContractorID 
		,''Block'' as AppointmentType '

		SET @selectClaseForCountBlock = 
		'SELECT DISTINCT 
		''JS'' + CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,							
		 ISNULL(P_BLOCK.BLOCKNAME, '''') + ISNULL(P_BLOCK.ADDRESS1, '''') + ISNULL(P_BLOCK.ADDRESS3, '''') + ISNULL(P_BLOCK.ADDRESS3, '''') 
		 AS [Address],
		ISNULL(P_BLOCK.POSTCODE, '''') POSTCODE,
		ISNULL(pin.Notes,''N/A'') AS Description,
		org.NAME AS contractor,
		AS_CONTRACTOR_WORK.AssignedDate AS AssignedDate,
		(ee.FIRSTNAME + '' '' + ee.LASTNAME) AS AssignedBy,
		DATEADD(YEAR,1,pl.ISSUEDATE)  AS CP12Expiry,
		AS_Status.Title AS Status,
		AS_CONTRACTOR_WORK.ApplianceServicingContractorId as ASContractorID
		,''Block'' as AppointmentType '
		
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause = CHAR(10) + ' FROM P__PROPERTY  
		INNER JOIN dbo.AS_JOURNAL  ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID
		CROSS APPLY (SELECT TOP 1 * FROM AS_JournalHeatingMapping WHERE JournalId = AS_JOURNAL.JOURNALID) AS AS_JournalHeatingMapping
		Left JOIN PA_PROPERTY_ITEM_NOTES pin ON pin.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				INNER JOIN AS_CONTRACTOR_WORK acw ON acw.journalId = 	AS_JOURNAL.JOURNALID
				inner join F_PURCHASEORDER FP ON FP.ORDERID=acw.PurchaseOrder						
				INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
				INNER JOIN S_ORGANISATION org ON org.ORGID = acw.ContractorId
				INNER JOIN E__EMPLOYEE ee ON ee.EMPLOYEEID = acw.AssignedBy
				LEFT JOIN P_LGSR pl ON pl.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.HeatingMappingId = AS_JournalHeatingMapping.HeatingJournalMappingId
										AND
										A.ITEMPARAMID =
										(
											SELECT
												ItemParamID
											FROM
												PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
											WHERE
												ParameterName = ''Heating Fuel''
												AND ItemName = ''Heating''
										)
										LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
				'

		SET @fromClauseScheme = CHAR(10) + ' FROM P_SCHEME  
				INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.SchemeId=dbo.P_SCHEME.SCHEMEID
				CROSS APPLY (SELECT TOP 1 * FROM AS_JournalHeatingMapping WHERE JournalId = AS_JOURNAL.JOURNALID) AS AS_JournalHeatingMapping
				Left JOIN PA_PROPERTY_ITEM_NOTES pin ON pin.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				INNER JOIN AS_CONTRACTOR_WORK ON AS_JOURNAL.JOURNALID = AS_CONTRACTOR_WORK.journalId
				inner join F_PURCHASEORDER FP ON FP.ORDERID=AS_CONTRACTOR_WORK.PurchaseOrder						
				INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
				INNER JOIN S_ORGANISATION org ON org.ORGID = AS_CONTRACTOR_WORK.ContractorId
				INNER JOIN E__EMPLOYEE ee ON ee.EMPLOYEEID = AS_CONTRACTOR_WORK.AssignedBy
				LEFT JOIN P_LGSR pl ON pl.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				AND
				A.ITEMPARAMID =
				(
					SELECT
						ItemParamID
					FROM
						PA_ITEM_PARAMETER
							INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
							INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
					WHERE
						ParameterName = ''Heating Fuel''
						AND ItemName = ''Boiler Room''
				)
				LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
				'
		SET @fromClauseBlock = CHAR(10) + ' FROM P_BLOCK  
				INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.BlockId=dbo.P_BLOCK.BLOCKID
				CROSS APPLY (SELECT TOP 1 * FROM AS_JournalHeatingMapping WHERE JournalId = AS_JOURNAL.JOURNALID) AS AS_JournalHeatingMapping
				Left JOIN PA_PROPERTY_ITEM_NOTES pin ON pin.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				INNER JOIN AS_CONTRACTOR_WORK ON AS_JOURNAL.JOURNALID = AS_CONTRACTOR_WORK.journalId
				inner join F_PURCHASEORDER FP ON FP.ORDERID=AS_CONTRACTOR_WORK.PurchaseOrder						
				INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
				INNER JOIN S_ORGANISATION org ON org.ORGID = AS_CONTRACTOR_WORK.ContractorId
				INNER JOIN E__EMPLOYEE ee ON ee.EMPLOYEEID = AS_CONTRACTOR_WORK.AssignedBy
				LEFT JOIN P_LGSR pl ON pl.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
				AND
				A.ITEMPARAMID =
				(
					SELECT
						ItemParamID
					FROM
						PA_ITEM_PARAMETER
							INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
							INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
					WHERE
						ParameterName = ''Heating Fuel''
						AND ItemName = ''Boiler Room''
				)
				LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
				'														

		
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause						
		IF(@sortColumn = 'FullAddress')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FullAddress'		
		END		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building WHERE clause
	    			  				
		SET @whereClause =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteria 
		SET @whereClauseScheme =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteriaScheme 
		SET @whereClauseBlock =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteriaBlock 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		SET @unionQuery = char(10) + ' UNION ALL ' + char(10)

		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause
		Set @mainSelectQueryScheme = @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme
		Set @mainSelectQueryBlock = @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock

		SET @resultingQuery = @mainSelectQuery + @unionQuery + @mainSelectQueryScheme + @unionQuery + @mainSelectQueryBlock + @orderClause
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@resultingQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================	
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================		
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@selectClaseForCountProperty+@fromClause+@whereClause+@unionQuery+@selectClaseForCountScheme+@fromClauseScheme+
													@whereClauseScheme+@unionQuery+@selectClaseForCountBlock+@fromClauseBlock+@whereClauseBlock+') as Records ) '
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================									
END
