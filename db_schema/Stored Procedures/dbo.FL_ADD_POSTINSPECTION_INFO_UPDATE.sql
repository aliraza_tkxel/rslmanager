SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE PROCEDURE dbo.FL_ADD_POSTINSPECTION_INFO_UPDATE
/* ===========================================================================
 '   NAME:          FL_ADD_POSTINSPECTION_INFO_UPDATE
 '   DATE CREATED:   4th May, 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To Update values of Post Inspection Info Table in Post Inspection Report Grid
 '		     a fault values
 '   IN:            @UserID as Int,
 '   IN:	    @InspectionDate as DateTime,
 '   IN:	    @NetCost as float,
 '   IN:	    @DueDate as datetime,
 '   IN:	    @Notes as nvarchar,
 '		    @Approved as int,
 '		    @FaultLogId as int
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@UserID as Int,
@InspectionDate as DateTime,
@time as nvarchar(100) = NULL,
@NetCost as float,
@DueDate as datetime,
@Notes as nvarchar(500),
@Approved as int = NULL,
@FaultLogId as int,
@reason as nvarchar(2000) = NULL,
@RepairId as int,
@Recharge bit,
@RESULT  INT = 1  OUTPUT 

AS
declare 
@journalId as int,
@orgId as int,
@logid as int,
@faultStatus as int,
@scopeId as int,
@areaofWork as int,
@TotalRecords as int,
@ApprovedRecords as int

	--BEGIN TRANSACTION 

UPDATE FL_CO_FAULTLOG_TO_REPAIR

SET USERID = @UserID,
INSPECTIONDATE = @InspectionDate, 
INSPECTIONTIME = @time,
NETCOST = @NetCost,
DUEDATE = @DueDate,
NOTES = @Notes,
APPROVED = @Approved,
Recharge = @Recharge,
Reason = @reason

WHERE faultlogid = @FaultLogId
AND FaultRepairListID = @RepairId
	
	
SET @ApprovedRecords = (SELECT count(*) FROM  FL_CO_FAULTLOG_TO_REPAIR INNER JOIN 
FL_FAULT_REPAIR_LIST ON FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID = FL_FAULT_REPAIR_LIST.FaultRepairListID 
WHERE faultlogid = @FaultLogId
AND Approved =1)
	

SET @TotalRecords = (SELECT count(*) FROM FL_CO_FAULTLOG_TO_REPAIR INNER JOIN 
FL_FAULT_REPAIR_LIST ON FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID = FL_FAULT_REPAIR_LIST.FaultRepairListID 
WHERE faultlogid = @FaultLogId
AND FL_FAULT_REPAIR_LIST.PostInspection = 1)

PRINT @TotalRecords		
	
PRINT @ApprovedRecords
	
IF(@ApprovedRecords = @TotalRecords)
BEGIN

update FL_FAULT_LOG SET StatusID=(SELECT FaultStatusID FROM FL_FAULT_STATUS WHERE (Description = 'Post Inspected')) where faultlogid=@FaultLogId
update FL_FAULT_JOURNAL SET FaultStatusID=(SELECT FAULTSTATUSID FROM FL_FAULT_STATUS WHERE (DESCRIPTION='Post Inspected')) where faultlogid=@FaultLogId

SELECT @journalId=FL_FAULT_JOURNAL.JournalID , @logid=FL_FAULT_JOURNAL.FaultLogID ,@faultStatus= FL_FAULT_JOURNAL.FaultStatusID ,@orgid=FL_FAULT_LOG.ORGID FROM FL_FAULT_JOURNAL INNER JOIN FL_FAULT_LOG ON FL_FAULT_JOURNAL.FaultLogID = FL_FAULT_LOG.FaultLogID where fl_fault_log.faultlogid=@FaultLogId 
SELECT     @areaofWork=AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair')
SELECT @scopeId=SCOPEID FROM S_SCOPE WHERE ORGID=@orgid AND AREAOFWORK=@areaofWork
INSERT INTO FL_FAULT_LOG_HISTORY (JournalID, FaultStatusID,LastActionDate,FaultLogID, ORGID,ScopeID)VALUES(@journalId,@faultStatus,Getdate(),@logid,@orgid,@scopeId)
END

GO
