USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_CancelPurchaseOrderForVoidWorks]    Script Date: 10/7/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PDR_CancelPurchaseOrderForVoidWorks') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_CancelPurchaseOrderForVoidWorks AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Rehan Baber
-- Create date: 27/2/2018
-- Description:	Cancels a purchase order on cancelling the respective Assign To Contractor Job Sheet
-- EXEC CancelPurchaseOrderForVoidWorks 

-- =============================================
ALTER PROCEDURE [dbo].[PDR_CancelPurchaseOrderForVoidWorks]
@InspectionJournalId int,
@UserId int

AS
BEGIN
	
	if @InspectionJournalId is not null 
	begin

		UPDATE F_PURCHASEORDER 
		SET POSTATUS = (SELECT POSTATUSID
						FROM F_POSTATUS
						WHERE POSTATUSNAME LIKE '%Cancelled%')
		WHERE ORDERID IN (select DISTINCT(PurchaseOrderId) PurchaseOrderId from V_RequiredWorks 
						   INNER JOIN PDR_CONTRACTOR_WORK ON V_RequiredWorks.WorksJournalId = PDR_CONTRACTOR_WORK.JournalId 
						   WHERE InspectionJournalId = @InspectionJournalId)
			 AND POSTATUS IN (SELECT POSTATUSID 
								FROM F_POSTATUS
								WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Work Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))
	
	
		DECLARE @IDENTIFIER VARCHAR
		SELECT @IDENTIFIER = Replace(Replace(Replace((CONVERT(VARCHAR(10), GETDATE(), 103) + ' '  + convert(VARCHAR(8), GETDATE(), 14)), ':', ''), '/', ''),' ','') + '_' + CAST(@UserId AS VARCHAR)
	
		INSERT INTO F_PURCHASEORDER_LOG
		(IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION, image_url)
		(SELECT @IDENTIFIER, PO.ORDERID, 1, PO.POTYPE, PO.POSTATUS, GETDATE(), @UserId, 'Cancelled', NULL 
		FROM F_PURCHASEORDER PO 
		WHERE ORDERID IN (select DISTINCT(PurchaseOrderId) PurchaseOrderId from V_RequiredWorks 
							INNER JOIN PDR_CONTRACTOR_WORK ON V_RequiredWorks.WorksJournalId = PDR_CONTRACTOR_WORK.JournalId 
							WHERE InspectionJournalId = @InspectionJournalId)
		AND POSTATUS IN (SELECT POSTATUSID 
			FROM F_POSTATUS
			WHERE POSTATUSNAME IN ('Cancelled')))

		UPDATE F_PURCHASEITEM 
		SET PISTATUS = (SELECT POSTATUSID
						FROM F_POSTATUS
						WHERE POSTATUSNAME LIKE '%Cancelled%')
		WHERE ORDERID IN (select DISTINCT(PurchaseOrderId) PurchaseOrderId 
							from V_RequiredWorks 
							INNER JOIN PDR_CONTRACTOR_WORK ON V_RequiredWorks.WorksJournalId = PDR_CONTRACTOR_WORK.JournalId 
							WHERE InspectionJournalId = @InspectionJournalId)
		AND PISTATUS IN (SELECT POSTATUSID 
						FROM F_POSTATUS
						WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Work Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))
	end

END
