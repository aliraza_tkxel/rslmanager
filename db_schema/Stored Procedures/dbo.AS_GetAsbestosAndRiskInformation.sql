SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
--EXEC [dbo].[P_GetAsbestosAndRiskInformation]  
-- Author:  <Aqib Javed>  
-- Create date: <18-Apr-2014>  
-- Description: <This procedure 'll get the asbestos level, elements and risk information >  
-- Web Page: AsbestosTab.ascx  
IF OBJECT_ID('dbo.AS_GetAsbestosAndRiskInformation') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetAsbestosAndRiskInformation AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_GetAsbestosAndRiskInformation]   
    
AS  
BEGIN  
 Select ASBRISKLEVELID,ASBRISKLEVELDESCRIPTION from P_ASBRISKLEVEL order by ASBRISKLEVELID  
 SELECT     ASBESTOSID, RISKDESCRIPTION FROM P_ASBESTOS WHERE (Other = 0)  order by sorder asc
 Select ASBRISKID from P_ASBRISK order by P_ASBRISK.ORDERBY ASC
 SELECT AsbestosLevelId,Description from P_AsbestosRiskLevel    
END
GO
