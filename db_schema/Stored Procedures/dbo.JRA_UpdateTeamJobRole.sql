
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_UpdateTeamJobRole @teamId=1,@userId =615, @jobRole = 'Administrator',@IsActive=0,@teamJobRoleId=12
-- Author:		Ahmed Mehmood
-- Create date: <26/12/2013>
-- Last Modified: <26/12/2013>
-- Description:	<Update Team Job Role. >
-- Web Page: JobRoles.aspx

-- =============================================
CREATE PROCEDURE [dbo].[JRA_UpdateTeamJobRole]
@teamId int
,@userId int
,@jobRole varchar(50)
,@IsActive bit
,@teamJobRoleId int
,@isSaved bit out
AS
BEGIN

DECLARE @jobRoleId int
,@jobRoleActionId int

BEGIN TRANSACTION;
BEGIN TRY

	SELECT	@jobRoleId = E_JOBROLETEAM.JobRoleId 
	FROM	E_JOBROLETEAM
	WHERE	E_JOBROLETEAM.JobRoleTeamId = @teamJobRoleId
	
	UPDATE E_JOBROLE
	SET E_JOBROLE.JobeRoleDescription = @jobRole
	WHERE E_JOBROLE.JobRoleId = @jobRoleId

	    
    UPDATE E_JOBROLETEAM
	SET E_JOBROLETEAM.JobRoleId = @jobRoleId
	,E_JOBROLETEAM.TeamId = @teamId
    ,E_JOBROLETEAM.IsActive = @IsActive
    WHERE E_JOBROLETEAM.JobRoleTeamId = @teamJobRoleId
    
                    
    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Job Role Amended'
          
    INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           ,'Amended to ' + @jobRole
           ,@userId
           ,GETDATE())
    
              
END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END
GO
