SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_P_DEVELOPMENT_GetLookUpValue
/* ===ssssssssss========================================================================
 '   NAME:           TO_P_DEVELOPMENT_GetLookUpValue
 '   DATE CREATED:   27 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get P_DEVELOPMENT lookup value from P_DEVELOPMENT table which will be shown
 '					 as lookup value on move house page
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
	@DEVELOPMENTID INT
)
AS
	SELECT DEVELOPMENTID AS id,DEVELOPMENTNAME AS val
	FROM P_DEVELOPMENT
	WHERE LOCALAUTHORITY=@DEVELOPMENTID
	ORDER BY DEVELOPMENTNAME ASC

GO
