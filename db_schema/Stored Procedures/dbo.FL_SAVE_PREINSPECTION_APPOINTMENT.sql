SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE PROCEDURE dbo.FL_SAVE_PREINSPECTION_APPOINTMENT 

	/* ===========================================================================
 '   NAME:           ADDINSPECTIONRECORD
 '   DATE CREATED:   26th Dec., 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Tenants Online Reported Faults
 '   PURPOSE:        To save inspection data  in FL_FAULT_PREINSPECTIONINFO table
 
 '   IN:             @userId
 '   IN:             @orgId
 '   IN:             @notes
 '   IN:             @inspectionDate
 '   IN:             @dueDate
 '   IN:             @netCost
 '   IN:             @approved
 '   IN:             @faultLogId
 '
 '   OUT:            @result
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
  (
  
	@userId INT,
	@orgId INT = null,
	@notes nvarchar(300),
	@inspectionDate DATETIME,
	@dueDate DATETIME = null,
	@netCost float,
	@approved INT = null,	
	@faultLogId INT,
	@result INT OUTPUT	
  ) 
AS

DECLARE 
		@JournalId int,
        @ScopeId int,
		@IsApproved bit,
		@itemId int,
		@faultStatus int,
		@AssginedToContractorStatusId INT,
		@WorksCompletedStatusId INT
	
	--Get and set the repair assigned to contractor status
	SET @AssginedToContractorStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Assigned To Contractor')


	--Get and set the works completed status
	SET @WorksCompletedStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Works Completed')		

	INSERT INTO FL_FAULT_PREINSPECTIONINFO(
		USERID,
		INSPECTIONDATE,
		NETCOST,
		DUEDATE,
		NOTES,
		ORGID,
		Approved,
		faultlogid
	)

	VALUES(
		@userId,
		@inspectionDate,
		@netCost,
		@dueDate,
		@notes,
		@orgId,
		@approved,
		@faultLogId
	)

SELECT @result=PREINSPECTONID
FROM FL_FAULT_PREINSPECTIONINFO
WHERE PREINSPECTONID = @@IDENTITY

--IF(@orgId IS NOT NULL)
--BEGIN
--UPDATE FL_FAULT_LOG 
	--SET 	
	--ORGID = @orgId	    	
	--WHERE FaultLogID = @faultLogId
--END

--IF(@approved = 0)
--BEGIN
--set @faultStatus=@WorksCompletedStatusId
--UPDATE FL_FAULT_LOG
	--SET 
	--StatusId = @WorksCompletedStatusId
	--WHERE faultLogId = @faultLogid
--UPDATE FL_FAULT_JOURNAL
	--SET 
	--FaultStatusId = @WorksCompletedStatusId
	--WHERE faultLogId = @faultLogid

--END
--ELSE
--BEGIN
--set @faultStatus=@AssginedToContractorStatusId
--UPDATE FL_FAULT_LOG
	--SET 
	--StatusId = @AssginedToContractorStatusId
--	WHERE faultLogId = @faultLogid

--UPDATE FL_FAULT_JOURNAL
	--SET 
	--FaultStatusId = @AssginedToContractorStatusId
	--WHERE faultLogId = @faultLogid
--END


--SELECT @ScopeID=SCOPEID FROM S_SCOPE WHERE ORGID=@OrgId AND AREAOFWORK= (SELECT AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair'))

--SELECT  @IsApproved= APPROVED, @Notes = Notes  FROM FL_FAULT_PREINSPECTIONINFO WHERE faultlogid = @FaultLogId 

--IF @IsApproved = 1 

--Select @faultStatus = StatusId FROM FL_FAULT_LOG 
--WHERE faultLogId = @faultLogid

--Select @JournalId = JournalId, @itemId = ItemId FROM FL_FAULT_JOURNAL 
--WHERE faultLogid = @faultLogid


--INSERT INTO FL_FAULT_LOG_HISTORY(JournalId,FaultStatusId,ItemActionId, LastActionDate, LastActionUserID, FaultLogId,OrgId,ScopeId,Title,Notes)
--VALUES(@JournalId,  @faultStatus , @itemId, GETDATE(), Null, @FaultLogId, @orgId,@SCOPEID, '', @Notes )

GO
