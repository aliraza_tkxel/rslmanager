SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_UpdatePatchId @employeeId = @employeeid , @patchId = @patchid
-- Author:	<Salman Nazir>
-- Create date: <10/02/2012>
-- Description:	<Update the patch drop values against user>
-- WEb Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdatePatchId](
	-- Add the parameters for the stored procedure here
	-- Add the parameters for the stored procedure here
	@employeeId int,
	@patchId int 
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update AS_UserPatchDevelopment SET  PatchId=@patchId ,IsActive=1 
			Where			EmployeeId =@employeeId
END
GO
