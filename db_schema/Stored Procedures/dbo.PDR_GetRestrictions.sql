USE [RSLBHALive]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('dbo.PDR_GetRestrictions') IS NULL
 EXEC('CREATE PROCEDURE dbo.PDR_GetRestrictions AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PDR_GetRestrictions] 
@schemeId   AS INT = NULL, 
@blockId    AS INT = NULL,
@propertyId   AS nvarchar(20) = NULL
AS
  BEGIN
  
	   IF @propertyId IS NOT NULL 
        BEGIN 

            SELECT 	RestrictionId, PermittedPlanning , RelevantPlanning, RelevantTitle, RestrictionComments, AccessIssues, MediaIssues , ThirdPartyAgreement, SpFundingArrangements
			, IsRegistered, ManagementDetail, NonBhaInsuranceDetail
            FROM   PDR_Restriction 
            WHERE  propertyId = @propertyId
			ORDER BY CreationDate DESC 
        
		END 
	   ELSE IF @schemeId IS NOT NULL and @schemeId <> 0
        BEGIN 
            SELECT 	RestrictionId, PermittedPlanning , RelevantPlanning, RelevantTitle, RestrictionComments, AccessIssues, MediaIssues , ThirdPartyAgreement, SpFundingArrangements
			, IsRegistered, ManagementDetail, NonBhaInsuranceDetail
            FROM   PDR_Restriction 
            WHERE  schemeid = @schemeId 
			ORDER BY CreationDate DESC 
        END 
      ELSE IF @blockId IS NOT NULL and @blockId <> 0
        BEGIN 
             SELECT 	RestrictionId, PermittedPlanning , RelevantPlanning, RelevantTitle, RestrictionComments, AccessIssues, MediaIssues , ThirdPartyAgreement, SpFundingArrangements
			, IsRegistered, ManagementDetail, NonBhaInsuranceDetail
            FROM   PDR_Restriction 
            WHERE  blockId = @blockId 
			ORDER BY CreationDate DESC 
        END 
  END 

GO


