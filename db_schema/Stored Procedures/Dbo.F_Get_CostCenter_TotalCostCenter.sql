USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_Get_CostCenter_TotalCostCenter]    Script Date: 22/3/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.F_Get_CostCenter_TotalCostCenter') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_Get_CostCenter_TotalCostCenter AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Saud Ahmed
-- Create date: 22/3/2017
-- Description:	Get all cost center accounts details 
-- WebPage: BHGFinanceModule/Views/CostCenter/Index
-- EXEC F_Get_CostCenterAll '01/04/2016 00:00', '31/03/2017 23:59' 

-- =============================================
ALTER PROCEDURE [dbo].[F_Get_CostCenter_TotalCostCenter]				
		@fromDate datetime,
		@toDate datetime
AS
BEGIN
	-- TotalCostCenter 
	SELECT CONVERT(VARCHAR,ISNULL(Sum(CCA.CostCentreAllocation),0) ,1)  AS TotalCostCentre
	FROM dbo.F_COSTCENTRE CC INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.FISCALYEAR in (
		select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
			YEnd between @fromDate AND @toDate
	)
	WHERE CC.COSTCENTREID  NOT IN ((SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'))
	
									
END
