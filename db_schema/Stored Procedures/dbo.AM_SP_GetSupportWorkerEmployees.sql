SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adeel Ahmed>
-- Create date: <28/06/2010>
-- Description:	<Getting Support Worker Employeess>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetSupportWorkerEmployees] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT E__EMPLOYEE.EMPLOYEEID,isnull(E__EMPLOYEE.FIRSTNAME, '')+ isnull(E__EMPLOYEE.LASTNAME, '') as EmployeeName
	FROM E__EMPLOYEE
	INNER JOIN E_JOBDETAILS ON E_JOBDETAILS.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID
	
	WHERE E_JOBDETAILS.JOBTITLE = 'Support Worker' 			  
END





GO
