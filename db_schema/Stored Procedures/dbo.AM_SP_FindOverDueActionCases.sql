SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Noor Muhammad>
-- Create date: <29 Aug 2012>
-- Description:	<This stored procedure is bascially a schedule job, the whole query is written by bha >
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_FindOverDueActionCases] 		
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	--SELECT TOP(@pageSize)  
	SELECT  
		Max(AM_Action.Title) AS ActionTitle, 
		Max(AM_Case.CaseId) as CaseId, 
		Max(AM_Status.Title) AS StatusTitle, 
			
		(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
			FROM AM_Customer_Rent_Parameters
			INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
			WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
			ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC
		 ) as CustomerName,

		(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
			FROM AM_Customer_Rent_Parameters
			INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
			WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
			ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC
		) as CustomerName2,

		(SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
			FROM AM_Customer_Rent_Parameters
			INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
			WHERE	AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID  
			AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
		) as JointTenancyCount,						

		Max(customer.CustomerAddress) AS CustomerAddress, 
		Max(convert(varchar(10), AM_Case.IsPaymentPlan)) AS PaymentPlan, 
		AM_Case.TenancyId as TenancyId, 
		Max(convert(varchar(10), AM_Case.IsSuppressed)) as IsSuppressed,
		Max(ISNULL(convert(varchar(100),AM_Case.SuppressedDate, 103), '')) as SuppressedDate, 
		Max(customer.CUSTOMERID) as CustomerId,
		--isnull((SELECT F_HBINFORMATION.CUSTOMERID 
		--			FROM F_HBACTUALSCHEDULE
		--			INNER JOIN dbo.F_HBINFORMATION ON dbo.F_HBACTUALSCHEDULE.HBID = dbo.F_HBINFORMATION.HBID AND STATUS=1
		--			INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C_CUSTOMERTENANCY.CUSTOMERID=F_HBINFORMATION.CUSTOMERID  
		--			WHERE F_HBINFORMATION.ACTUALENDDATE IS NULL
		--			AND  F_HBACTUALSCHEDULE.HBROW = (SELECT MIN(HBROW) FROM dbo.F_HBACTUALSCHEDULE WHERE HBID=F_HBINFORMATION.HBID AND VALIDATED IS NULL)
		--			AND F_HBINFORMATION.TENANCYID =AM_Case.TENANCYID  
		--			AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
		--		),(SELECT  MAX(ACRP.CustomerId)
		--				FROM AM_Customer_Rent_Parameters ACRP
		--				INNER JOIN C_CUSTOMERTENANCY CCT ON  CCT.CustomerId=ACRP.CustomerId
		--				WHERE	ACRP.TenancyId = AM_Case.TENANCYID  
		--				AND (CCT.ENDDATE IS NULL OR CCT.ENDDATE>GETDATE())
		--			)
		--		) AS CustomerId,
			Max(customer.RentBalance) AS RentBalance,
 			Max(customer.EstimatedHBDue) AS EstimatedHBDue,
			--Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
			Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,
			MAX(AM_Case.ModifiedDate) as ModifiedDate,
			MAX(AM_Case.ActionReviewDate) as ActionReviewDate,
			convert(float, ISNULL((SELECT P_FINANCIAL.Totalrent 
										FROM C_TENANCY INNER JOIN
										P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
										P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
										WHERE C_TENANCY.TENANCYID = AM_Case.TenancyId), 0.00									
									)
					) as totalRent,
			MAX(customer.FirstName) as FirstName,
			MAX(customer.LastName) as LastName										
			
	FROM       
		AM_Action INNER JOIN
		AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
		AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
		AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
		C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
		----commented - started just optimize
		--INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
		--INNER JOIN P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID
		--INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID 
		----commented - end just optimize
    WHERE												
		1=1
		----commented - started just optimize
		--AND  P_DEVELOPMENT.PATCHID = P_DEVELOPMENT.PATCHID 						  
		----commented - end just optimize
		AND AM_Case.IsActive = 1 
		AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)																	  				
		AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
									FROM 
										AM_CaseHistory INNER JOIN
										AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
										AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
										E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
										C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
										C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
										AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

									WHERE 
									AM_CaseHistory.IsActive = 1 
									--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = ''true'' 
										and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_CaseHistory.ActionReviewDate ) = 'true'
										and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId)
										
							)
		--AND AM_Case.TenancyId NOT IN (
		--								SELECT TenancyId FROM(
		--														SELECT TOP(@skipIndex)  
		--															Max(AM_Action.Title) AS ActionTitle, 
		--															Max(AM_Case.CaseId) as CaseId, 
		--															Max(AM_Status.Title) AS StatusTitle, 
								
		--															(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
		--																FROM AM_Customer_Rent_Parameters
		--																INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
		--																WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
		--																ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC
		--															) as CustomerName,

		--															(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
		--																FROM AM_Customer_Rent_Parameters
		--																INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
		--																WHERE AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
		--																ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC
		--															) as CustomerName2,

		--															(SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
		--																	FROM AM_Customer_Rent_Parameters
		--																	INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
		--																	WHERE	AM_Customer_Rent_Parameters.TenancyId = AM_Case.TENANCYID  
		--																	AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
		--															) as JointTenancyCount,						
																	
  --            														Max(customer.CustomerAddress) AS CustomerAddress, 
		--															Max(convert(varchar(10), AM_Case.IsPaymentPlan)) AS PaymentPlan, 
		--															AM_Case.TenancyId as TenancyId, 
		--															Max(convert(varchar(10), AM_Case.IsSuppressed)) as IsSuppressed,
		--															Max(ISNULL(convert(varchar(100),AM_Case.SuppressedDate, 103), '')) as SuppressedDate, 
		--															--Max(customer.CUSTOMERID) as CustomerId,
		--															isnull((SELECT F_HBINFORMATION.CUSTOMERID 
		--																		FROM F_HBACTUALSCHEDULE
		--																		INNER JOIN dbo.F_HBINFORMATION ON dbo.F_HBACTUALSCHEDULE.HBID = dbo.F_HBINFORMATION.HBID AND STATUS=1
		--																		INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C_CUSTOMERTENANCY.CUSTOMERID=F_HBINFORMATION.CUSTOMERID  
		--																		WHERE F_HBINFORMATION.ACTUALENDDATE IS NULL
		--																		AND  F_HBACTUALSCHEDULE.HBROW = (SELECT MIN(HBROW) FROM dbo.F_HBACTUALSCHEDULE WHERE HBID=F_HBINFORMATION.HBID AND VALIDATED IS NULL)
		--																		AND F_HBINFORMATION.TENANCYID =AM_Case.TENANCYID  
		--																		AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
		--																	),(SELECT  MAX(ACRP.CustomerId)
		--																			FROM AM_Customer_Rent_Parameters ACRP
		--																			INNER JOIN C_CUSTOMERTENANCY CCT ON  CCT.CustomerId=ACRP.CustomerId
		--																			WHERE	ACRP.TenancyId = AM_Case.TENANCYID  
		--																			AND (CCT.ENDDATE IS NULL OR CCT.ENDDATE>GETDATE()))
		--																	) AS CustomerId,
		--																	Max(customer.RentBalance) AS RentBalance,
		--	  																Max(customer.EstimatedHBDue) AS EstimatedHBDue,
		--																	--Max(ABS(ISNUll(ABS(ISNULL(customer.RentBalance, 0.0)) - ABS(ISNULL(customer.EstimatedHBDue, 0.0)),0.0))) as OwedToBHA,
		--																	Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,
		--																	MAX(AM_Case.ModifiedDate) as ModifiedDate,
		--																	MAX(AM_Case.ActionReviewDate) as ActionReviewDate,
		--																	convert(float, ISNULL((SELECT P_FINANCIAL.Totalrent 
		--																							FROM C_TENANCY INNER JOIN
		--																							P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
		--																							P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
		--																							WHERE C_TENANCY.TENANCYID = AM_Case.TenancyId), 0.00
		--																						)
		--																			) as totalRent

		--																FROM         
		--																	AM_Action INNER JOIN
		--																	AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
		--																	AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
		--																	AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
		--																	C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
		--																	----commented - started just optimize
		--																	--INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
		--																	--INNER JOIN P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID
		--																	--INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID 
		--																	----commented - end just optimize	
		--																	WHERE
		--																	1=1
		--																	----commented - started just optimize
		--																	--AND  P_DEVELOPMENT.PATCHID = P_DEVELOPMENT.PATCHID 						  
		--																	----commented - end just optimize					  
		--																	AND AM_Case.IsActive = 1 
		--																	AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)
		--																	AND AM_Case.CaseId IN (SELECT AM_CaseHistory.CaseId 		
		--																								FROM 
		--																									AM_CaseHistory INNER JOIN
		--																									AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
		--																									AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
		--																									AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
		--																									E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
		--																									C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
		--																									C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
		--																									AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

		--																								WHERE 
		--																								AM_CaseHistory.IsActive = 1 
		--																								--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = ''true'' 
		--																								--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_CaseHistory.ActionReviewDate ) = 'true'
		--																								--and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId)
		--																								and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Status.NextStatusAlert, AM_LookupCode.CodeName, AM_CaseHistory.StatusReview ) = 'true'
		--		                                                    									and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId)
		--																						)
							  
		--																GROUP BY AM_Case.TenancyId 
		--																ORDER BY  tenancyid asc) as Temp
		--								) 
														
										GROUP BY AM_Case.TenancyId 
									
		ORDER BY  tenancyid asc
END

GO
