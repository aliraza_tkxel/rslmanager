USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_PendingReviewDeclarationOfInterestCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_PendingReviewDeclarationOfInterestCount AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Ahmed Mehmood
-- Create date:      13/06/2018
-- Description:      Calculate alert count of Pending ReviewDeclaration Of Interest on whiteboard  
-- exec  E_PendingReviewDeclarationOfInterestCount 423
-- =============================================

ALTER PROCEDURE  [dbo].[E_PendingReviewDeclarationOfInterestCount]
	@LineMgr INT 
	
AS
BEGIN
	
	SET NOCOUNT ON;
   
	SELECT	count(*) AS PendingReviewDoiCount
	FROM	E_DOI doi
			INNER JOIN E_JOBDETAILS jd on doi.EMPLOYEEID = jd.EMPLOYEEID
			INNER JOIN E__EMPLOYEE lm on jd.LINEMANAGER = lm.EMPLOYEEID
			INNER JOIN E_DOI_Status s on doi.Status = s.DOIStatusId
			CROSS APPLY (SELECT TOP 1 YRange 
						 FROM	F_FISCALYEARS ff 
						 ORDER BY ff.YRange DESC) AS yRange
	WHERE	doi.FiscalYear = yRange.YRange 
			AND s.Description = 'Pending Review' 
			AND lm.EMPLOYEEID = @LineMgr

END
