USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetPurchaseItemExProperties') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPurchaseItemExProperties AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_GetPurchaseItemExProperties] 
	@orderItemId INT
AS
BEGIN
	
	SELECT	ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
	FROM	F_ServiceChargeExProperties FSCP
			INNER JOIN P__PROPERTY P ON FSCP.PropertyId = P.PropertyId
	WHERE	PurchaseItemId = @orderItemId
	ORDER BY CAST(SUBSTRING(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,''), 1,CASE	WHEN PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) > 0 THEN 
												PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) - 1 
											ELSE LEN(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) 
											END
											) AS INT) ASC, ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') ASC
	

END
