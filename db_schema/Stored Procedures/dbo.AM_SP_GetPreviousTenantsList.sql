-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
ALTER PROCEDURE [dbo].[AM_SP_GetPreviousTenantsList]  
   @caseOwnedById int=0,  
   @regionId int = 0,  
   @suburbId int = 0,  
   @allRegionFlag bit,  
   @allSuburbFlag bit,  
   @surname  varchar(100),  
   @skipIndex int = 0,  
   @pageSize int = 10,  
   @sortBy     varchar(100),  
            @sortDirection varchar(10),  
            @address1 varchar(200),  
            @tenancyid VARCHAR(15)  
AS  
BEGIN  
  
  
declare @orderbyClause varchar(100)  
declare @query varchar(8000)  
declare @subQuery varchar(8000)  
declare @RegionSuburbClause varchar(8000) , 
 
 @rowNumberQuery varchar(MAX),
		@finalQuery varchar(MAX),
		@offset int,
		@limit int  
		
	SET @offset = 1+(@skipIndex-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1 
 
  
IF(@caseOwnedById = -1 )  
BEGIN  
  
 IF(@regionId = -1 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId)   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
  
END  
ELSE   
BEGIN  
  
IF(@regionId = -1 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
              FROM AM_ResourcePatchDevelopment   
              WHERE ResourceId = ' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'')'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SchemeID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
END  
  
  
SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection  
SET @query =   
'SELECT TOP ('+convert(varchar(10),@limit)+' )  
      AM_Customer_Rent_Parameters.TENANCYID,   
      AM_Customer_Rent_Parameters.CUSTOMERID,     
      ISNULL(Convert(varchar(100),(AM_Customer_Rent_Parameters.LastPaymentDate), 103), '''') AS TRANSACTIONDATE,   
      ISNULL((AM_Customer_Rent_Parameters.LastPayment), 0.0) AS LastCPAY,   
      ISNULL(AM_Customer_Rent_Parameters.Title,'''') + '' '' + ISNULL(AM_Customer_Rent_Parameters.FIRSTNAME, '''')+'' ''+ ISNULL(AM_Customer_Rent_Parameters.LASTNAME, '''') AS CustomerName,   
                  ISNULL(AM_Customer_Rent_Parameters.RentBalance,0.0) - ISNULL(AM_Customer_Rent_Parameters.EstimatedHBDue,0.0) As OwedToBHA,  
     AM_Customer_Rent_Parameters.CustomerAddress,  
                    
      AM_Customer_Rent_Parameters.RentBalance AS RentBalance,  
                     AM_Customer_Rent_Parameters.EstimatedHBDue As EstimatedHBDue,  
         
       
         
     (SELECT ISNULL(P_FINANCIAL.Totalrent , 0)  
              FROM C_TENANCY INNER JOIN  
              P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN  
              P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID   
              WHERE C_TENANCY.TENANCYID = AM_Customer_Rent_Parameters.TENANCYID) AS TotalRent,  
     AM_CASE.CASEID  
     FROM         AM_Customer_Rent_Parameters   
          INNER JOIN C_TENANCY ON AM_Customer_Rent_Parameters.TENANCYID = C_TENANCY.TENANCYID  
          INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID   
          LEFT JOIN AM_CASE ON  AM_CASE.TENANCYID=AM_CUSTOMER_RENT_PARAMETERS.TENANCYID                
		  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
		  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 	
 WHERE '+@RegionSuburbClause+'   
     AND C_TENANCY.ENDDATE IS NOT NULL  
     AND AM_Customer_Rent_Parameters.LASTNAME LIKE '''' +  CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN AM_Customer_Rent_Parameters.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END  + ''%''  
     AND (  
    (P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.HOUSENUMBER + '' ''
 + P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')    
    
    )  
     AND AM_Customer_Rent_Parameters.TenancyId='''' + CASE WHEN '''' = '''+ REPLACE(@tenancyid,'''','''''') +''' THEN AM_Customer_Rent_Parameters.TenancyId ELSE '''+ REPLACE(@tenancyid,'''','''''') +''' END + ''''  
     '
  
  --=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortBy+ CHAR(10) +@sortDirection+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@query+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
  	       
  
print(@finalQuery);  
exec(@finalQuery);       
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  