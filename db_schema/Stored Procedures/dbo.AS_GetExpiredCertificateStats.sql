USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetExpiredCertificateStats]    Script Date: 10/7/2016 10:46:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------
-- FILTERS -------------
------------------------
-- 0	      EXPIRED 
-- 1	<=	  1 WEEK
-- 2	1-4   WEEKS
-- 3	4-8   WEEKS								     
-- 4	8-12  WEEKS
-- 5	12-16 WEEKS
-- 6	16-52 WEEKS
-- 7	>  52 WEEKS
-- 8		  NO ISSUE DATE
-- =============================================
  --EXEC  AS_GetExpiredCertificateStats
		--@fuelType = 1,
		--@propertyType = -1,
		--@patch = -1,
		--@scheme = -1,
		--@stage = -1,
		--@UPRN = ''
		
-- Author:		<Aqib Javed>
-- Create date: <13/12/2012>
-- Description:	<The stored procedure shall provide the stats {total counts} of Certificate Expiry Report >
-- WebPage : CertificateExpiry.aspx
-- Parameters : 
--@fuelType	INT,
--@propertyType INT,
--@patch INT,
--@scheme INT,
--@stage INT,
--@UPRN NVARCHAR(50)

IF OBJECT_ID('dbo.[AS_GetExpiredCertificateStats]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetExpiredCertificateStats] AS SET NOCOUNT ON;') 
GO
-- =============================================
ALTER PROCEDURE  [dbo].[AS_GetExpiredCertificateStats]
@fuelType	INT,
@propertyType INT,
@patch INT,
@scheme INT ,
@stage INT,
@UPRN NVARCHAR(50),
@sortColumn varchar(50) = 'Address',
@sortOrder varchar (5) = 'DESC',
@pageSize int = 50,
@pageNumber int = 1,
@certType int = 0,
@fromDate varchar (50),
@toDate varchar (50),
@heatingTypeId INT,
@selectCount int=0 output,
@selectCountExpired int=0 output,
@selectCount2Weeks int=0 output,
@selectCount4Weeks int=0 output,
@selectCount8Weeks int=0 output,
@selectCount12Weeks int=0 output,
@selectCount16Weeks int=0 output,
@selectCount52Weeks int=0 output,
@selectCount52PlusWeeks int=0 output,
@selectCountNoWeeks int=0 output
AS
BEGIN
DECLARE 
        @SelectClause nvarchar(max),
		@SelectClauseCount nvarchar(max),
		@SelectClauseScheme nvarchar(max),
		@SelectClauseSchemeCount nvarchar(max),
		@SelectClauseBlock nvarchar(max),
		@SelectClauseBlockCount nvarchar(max),
        	@fromClause   nvarchar(max),
		@fromClauseScheme   nvarchar(max),
		@fromClauseBlock   nvarchar(max),
        	@searchCriteria nvarchar(max),
		@searchCriteriaScheme nvarchar(max),
		@searchCriteriaBlock nvarchar(max),
		@certCriteria nvarchar(max),
        	@orderClause  nvarchar(max),	  
		@unionQuery nvarchar(max),
		@rowNumberQuery nvarchar(max),
		@finalQuery nvarchar(max),
		@totalFirstQuery nvarchar(max),
		@totalTopQuery nvarchar(max),
		@selectCountQ nvarchar(max),
		@selectCountE nvarchar(max),
		@selectCount2 nvarchar(max),
		@selectCount4 nvarchar(max),
		@selectCount8 nvarchar(max),
		@selectCount12 nvarchar(max),
		@selectCount16 nvarchar(max),
		@selectCount52 nvarchar(max),
		@selectCount52p nvarchar(max),
		@selectCountno nvarchar(max),
		@parameterDef NVARCHAR(500)    ,
		@offset int,
		@limit int,
		@JournalServicingType nvarchar(max)


--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1 
		
		--print(@offset) 
		--print(@limit)

--========================================================================================	        

	SET @JournalServicingType = ' 1 = 1 '
	IF (@fuelType <> - 1)
	BEGIN
		IF (@fuelType = (SELECT ValueID
					FROM PA_PARAMETER_VALUE
					WHERE ValueDetail = 'Mains Gas'
						AND ParameterID = (
							SELECT ParameterID
							FROM PA_PARAMETER
							WHERE ParameterName = 'Heating Fuel')
					)
				)
		BEGIN
			SET @JournalServicingType = @JournalServicingType + 'AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'') '
		END

		IF (@fuelType = (SELECT ValueID FROM PA_PARAMETER_VALUE
					WHERE ValueDetail = 'Oil'
						AND ParameterID = (
							SELECT ParameterID
							FROM PA_PARAMETER
							WHERE ParameterName = 'Heating Fuel')
					)
				)
		BEGIN
			SET @JournalServicingType =  @JournalServicingType + 'AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Oil'') '
		END

		IF (@fuelType IN (SELECT ValueID FROM PA_PARAMETER_VALUE
					WHERE (
							ValueDetail = 'Solar'
							OR ValueDetail = 'Air Source'
							OR ValueDetail = 'Unvented Cylinder'
							OR ValueDetail = 'PV'
							OR ValueDetail = 'MVHR & Heat Recovery'
							OR ValueDetail = 'Wet Electric'
							OR ValueDetail = 'Ground Source'
							)
						AND ParameterID = (
							SELECT ParameterID
							FROM PA_PARAMETER
							WHERE ParameterName = 'Heating Fuel')
					)
				)
		BEGIN
			SET @JournalServicingType =  @JournalServicingType + 'AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Alternative Servicing'') '
		END
	END

	-- Begin building SELECT clause
    SET @selectClause = '
    CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0  
	THEN 0
	END AS Expired,

	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>=0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14  
	THEN 2
	END AS Weeks2,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28  
	THEN 4
	END AS Weeks4,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56  
	THEN 8
	END AS Weeks8,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84  
	THEN 12
	END AS Weeks12,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112  
	THEN 16
	END AS Weeks16,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364  
	THEN 52
	END AS Weeks52,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364  
	THEN 53	
	END AS Weeks52Plus,
	
	CASE WHEN 
		((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL)) 
	THEN -1	
	END AS NoWeeks,
	LGSR.CP12NUMBER,P.PROPERTYID,PV.ValueDetail as FTYPE,
	PROPSTATUS.DESCRIPTION PropertyStatus,
	
		ISNULL(Convert(varchar(100),DATEADD(YEAR,1,LGSR.ISSUEDATE),103),''-'')
	
	AS EXPIRYDATE,
	DATEADD(YEAR,1,LGSR.ISSUEDATE) AS EXPIRYDATESORT,
	PT.DESCRIPTION AS PTYPE,AT.DESCRIPTION AS ATYPE,
	ISNULL(P.FLATNUMBER,'''') +'' ''+ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'''') +'' ''+ ISNULL(P.ADDRESS3,'''')  AS ADDRESS,
	'''' AS SchemeName,
	''''  AS BlockName,
	''Property'' AS ReqType,
	Case 
	 WHEN ST.StatusId=1 then ''Appointment to be arranged (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=1))+'')''
	 WHEN ST.StatusId=2 then ''Appointment (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=2))+'')''
	 WHEN ST.StatusId=3 then ''No Entry (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=3))+'')''
	 WHEN ST.StatusId=4 then ''Legal Proceedings (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=4))+'')''
	else ST.Title end AS Stage'
	SET @SelectClauseCount = ' SELECT DISTINCT ' + @SelectClause
	SET @SelectClause = ' SELECT DISTINCT ' + @SelectClause
	--SELECT TOP ('+convert(VARCHAR(10),@limit)+')

	SET @SelectClauseScheme = '
    CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0  
	THEN 0
	END AS Expired,

	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>=0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14  
	THEN 2
	END AS Weeks2,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28  
	THEN 4
	END AS Weeks4,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56  
	THEN 8
	END AS Weeks8,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84  
	THEN 12
	END AS Weeks12,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112  
	THEN 16
	END AS Weeks16,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364  
	THEN 52
	END AS Weeks52,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364  
	THEN 53	
	END AS Weeks52Plus,
	
	CASE WHEN 
		((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL)) 
	THEN -1	
	END AS NoWeeks,
	LGSR.CP12NUMBER,CONVERT(VARCHAR,P.SCHEMEID) as PROPERTYID,PV.ValueDetail as FTYPE,
	'''' PropertyStatus,
	
		ISNULL(Convert(varchar(100),DATEADD(YEAR,1,LGSR.ISSUEDATE),103),''-'')
	
	AS EXPIRYDATE,
	DATEADD(YEAR,1,LGSR.ISSUEDATE) AS EXPIRYDATESORT,
	'''' AS PTYPE,'''' AS ATYPE,
	''''  AS ADDRESS,
	ISNULL(P.SCHEMENAME,'''') AS SchemeName,
	''''  AS BlockName,
	''Scheme'' AS ReqType,
	Case 
	 WHEN ST.StatusId=1 then ''Appointment to be arranged (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=1))+'')''
	 WHEN ST.StatusId=2 then ''Appointment (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=2))+'')''
	 WHEN ST.StatusId=3 then ''No Entry (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=3))+'')''
	 WHEN ST.StatusId=4 then ''Legal Proceedings (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=4))+'')''
	else ST.Title end AS Stage'
	SET @SelectClauseSchemeCount = ' SELECT DISTINCT ' + @SelectClauseScheme
	SET @SelectClauseScheme = ' SELECT DISTINCT ' + @SelectClauseScheme

	SET @SelectClauseBlock = '
    CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0  
	THEN 0
	END AS Expired,

	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>=0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14  
	THEN 2
	END AS Weeks2,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28  
	THEN 4
	END AS Weeks4,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56  
	THEN 8
	END AS Weeks8,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84  
	THEN 12
	END AS Weeks12,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112  
	THEN 16
	END AS Weeks16,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364  
	THEN 52
	END AS Weeks52,
	
	CASE WHEN DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364  
	THEN 53	
	END AS Weeks52Plus,
	
	CASE WHEN 
		((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL)) 
	THEN -1	
	END AS NoWeeks,
	LGSR.CP12NUMBER,CONVERT(VARCHAR,P.BLOCKID) as PROPERTYID,PV.ValueDetail as FTYPE,
	'''' PropertyStatus,
	
		ISNULL(Convert(varchar(100),DATEADD(YEAR,1,LGSR.ISSUEDATE),103),''-'')
	
	AS EXPIRYDATE,
	DATEADD(YEAR,1,LGSR.ISSUEDATE) AS EXPIRYDATESORT,
	'''' AS PTYPE,'''' AS ATYPE,
	''''  AS ADDRESS,
	''''  AS SchemeName,
	ISNULL(P.BLOCKNAME,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'''') +'' ''+ ISNULL(P.ADDRESS3,'''')  AS BlockName,
	''Block'' AS ReqType,
	Case 
	 WHEN ST.StatusId=1 then ''Appointment to be arranged (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=1))+'')''
	 WHEN ST.StatusId=2 then ''Appointment (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=2))+'')''
	 WHEN ST.StatusId=3 then ''No Entry (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=3))+'')''
	 WHEN ST.StatusId=4 then ''Legal Proceedings (''+ convert(varchar(10),(SELECT Count(*)FROM AS_JournalHistory Where AS_JournalHistory.JournalId = J.JournalId AND AS_JournalHistory.StatusId=4))+'')''
	else ST.Title end AS Stage'
	SET @SelectClauseBlockCount = ' SELECT DISTINCT ' + @SelectClauseBlock
	SET @SelectClauseBlock = ' SELECT DISTINCT ' + @SelectClauseBlock
	


  -- End building SELECT clause
--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause= ' 
	From AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID
	INNER JOIN P__PROPERTY P on P.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P.PROPERTYID
	LEFT JOIN P_SCHEME PD ON PD.SCHEMEID=P.SCHEMEID 
	LEFT JOIN P_LGSR LGSR ON LGSR.HeatingMappingId = PHM.HeatingMappingId
	LEFT JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	INNER JOIN P_STATUS PROPSTATUS ON PROPSTATUS.STATUSID = P.STATUS
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P.ASSETTYPE
	'

	SET @fromClauseScheme= ' FROM dbo.P_SCHEME P 
	INNER JOIN PA_HeatingMapping PHM ON P.SchemeID = PHM.SchemeID
	LEFT JOIN P_LGSR LGSR ON LGSR.HeatingMappingId=PHM.HeatingMappingId
	LEFT JOIN AS_JOURNAL J ON J.SchemeId=P.SCHEMEID AND J.ISCURRENT=1
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	'

	SET @fromClauseBlock= ' FROM dbo.P_BLOCK P 
	INNER JOIN PA_HeatingMapping PHM ON P.BlockId = PHM.BlockId
	LEFT JOIN P_LGSR LGSR ON LGSR.HeatingMappingId=PHM.HeatingMappingId
	LEFT JOIN P_SCHEME PD ON PD.SCHEMEID=P.SCHEMEID 
	LEFT JOIN AS_JOURNAL J ON J.BlockId=P.BlockId AND J.ISCURRENT=1
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	'
    -- End building From clause
	--======================================================================================== 														  
	-- Begin building Search clause
	
	SET @searchCriteria = 'Where 1 = 1'
	SET @searchCriteriaScheme = 'Where 1 = 1'
	SET @searchCriteriaBlock = 'Where 1 = 1'
	
	If (@fuelType <> -1) 
    	Begin
		SET @searchCriteria = 'Where' + @JournalServicingType + ' AND PHM.IsActive = 1 AND PV.ValueId IN ( '+CONVERT(varchar, (@fuelType)) +') '
		SET @searchCriteriaScheme = 'Where' + @JournalServicingType + ' AND PHM.IsActive = 1 AND PV.ValueId IN ( '+CONVERT(varchar, (@fuelType)) +') '
		SET @searchCriteriaBlock = 'Where' + @JournalServicingType + ' AND PHM.IsActive = 1 AND PV.ValueId IN ( '+CONVERT(varchar, (@fuelType)) +') '
	End 

    If (@propertyType<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.PROPERTYTYPE ='+CONVERT(varchar, (@propertyType)) 
    End 
      If (@patch<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.PATCH ='+CONVERT(varchar, (@patch)) 
    End
      If (@scheme<>-1) 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND PD.SCHEMEID ='+CONVERT(varchar, (@scheme)) 
	  SET @searchCriteriaScheme=@searchCriteriaScheme+ ' AND P.SCHEMEID ='+CONVERT(varchar, (@scheme))
	  SET @searchCriteriaBlock=@searchCriteriaBlock+ ' AND PD.SCHEMEID ='+CONVERT(varchar, (@scheme))
    End
    If (@stage<>-1) 
    Begin
     
		SET @searchCriteria=@searchCriteria+ ' AND J.STATUSID ='+CONVERT(varchar, (@stage)) 
		SET @searchCriteriaScheme=@searchCriteriaScheme+ ' AND J.STATUSID ='+CONVERT(varchar, (@stage)) 
		SET @searchCriteriaBlock=@searchCriteriaBlock+ ' AND J.STATUSID ='+CONVERT(varchar, (@stage)) 
    End
    If (@UPRN<>'') 
    Begin
      SET @searchCriteria=@searchCriteria+ ' AND P.PROPERTYID='''+@UPRN+''''
	  SET @searchCriteriaScheme=@searchCriteriaScheme+ ' AND P.SchemeId='''+@UPRN+''''
	  SET @searchCriteriaBlock=@searchCriteriaBlock+ ' AND P.BlockId='''+@UPRN+''''
    End

    If (@fromDate<>'') 
    Begin
      SET @searchCriteria=@searchCriteria+ 'AND ISSUEDATE >= ''' +@fromDate+''''
	  SET @searchCriteriaScheme=@searchCriteriaScheme+ 'AND ISSUEDATE >= ''' +@fromDate+''''
	  SET @searchCriteriaBlock=@searchCriteriaBlock+ 'AND ISSUEDATE >= ''' +@fromDate+''''
    End

    If (@toDate<>'') 
    Begin
      SET @searchCriteria=@searchCriteria+ 'AND ISSUEDATE <= ''' +@toDate+''''
	  SET @searchCriteriaScheme=@searchCriteriaScheme+ 'AND ISSUEDATE >= ''' +@fromDate+''''
	  SET @searchCriteriaBlock=@searchCriteriaBlock+ 'AND ISSUEDATE >= ''' +@fromDate+''''
    End
	    
    --These conditions wíll be used in every case
		
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' 
														AND PROPSTATUS.DESCRIPTION NOT IN (''Sold'',''Demolished'',''Other Losses'',
														''Transfer'') 
														 AND AT.description not in (''Shared Ownership'')
														AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
														'		
	
	----------------------------------------- WANT ALL ----------------------------------------
	If (@certType=0) 
    Begin
      SET @certCriteria = ' AND 1 = 1 ' 
    End

	----------------------------------------- WANT EXPIRED ----------------------------------------
	If (@certType=1) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0   '
    End

	----------------------------------------- WANT 2 WEEKS ----------------------------------------
	If (@certType=2) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14 '
	 + ' '
    End

	----------------------------------------- WANT 4 WEEKS ----------------------------------------
	If (@certType=3) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28 '
	 + ' '
    End 

	----------------------------------------- WANT 8 WEEKS ----------------------------------------
	If (@certType=4) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56 '
	 + ' '
    End

	----------------------------------------- WANT 12 WEEKS ----------------------------------------
	If (@certType=5) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84 '
	 + ' '
    End 

	----------------------------------------- WANT 16 WEEKS ----------------------------------------
	If (@certType=6) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112 '
	 + ' '
    End

	----------------------------------------- WANT 52 WEEKS ----------------------------------------
	If (@certType=7) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364 '
	 + ' '
    End 

	----------------------------------------- WANT 52 PLUS WEEKS ----------------------------------------
	If (@certType=8) 
    Begin
      SET @certCriteria =
	  ' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364 '
    End

	----------------------------------------- WANT NO WEEKS ----------------------------------------
	If (@certType=9) 
    Begin
      SET @certCriteria =
	  ' and ((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL))'
	 + ''
    End 

	If (@sortColumn='EXPIRYDATE') 
    Begin
      SET @sortColumn = 'EXPIRYDATESORT'
	  
    End 
	
	----------------------------------------- To(End) Date ----------------------------------------
	    
--	P.FUELTYPE = COALESCE(@FUELID, P.FUELTYPE)
--		AND PD.DEVELOPMENTID = COALESCE(@DEVELOPMENTID, PD.DEVELOPMENTID)
--		AND PD.PATCHID = COALESCE(@PATCHID, PD.PATCHID)
--		AND P.PROPERTYID=COALESCE(@UPRN, P.PROPERTYID)
--ORDER BY EXPIRYDATE	
  -- End building Search clause
	--======================================================================================== 														  
	-- Begin building Order clause
	SET @orderClause= ' ORDER BY ' + @sortColumn + CHAR(10) + @sortOrder
	-- End building Order clause
	--======================================================================================== 	

print (@selectClause + @fromClause + @searchCriteria + @certCriteria)
--print (@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme + @certCriteria)
--print (@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock )
	SET @unionQuery = char(10) + ' UNION ALL ' + CHAR(10)
set @totalFirstQuery = @selectClause + @fromClause + @searchCriteria + @certCriteria + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme + @certCriteria + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock + @certCriteria
						-- + 
						--@orderClause

--EXEC (@totalFirstQuery)

set @totalTopQuery =  '  SELECT TOP ('+convert(VARCHAR(10),@limit)+') * from ( ' + @totalFirstQuery + ' ) as TopResult  ' + 
						@orderClause + '  '
						

--print(@selectClause + @fromClause + @searchCriteria + @certCriteria)

--print(@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme + @certCriteria)

--print(@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock + @certCriteria)



Set @rowNumberQuery ='  SELECT  *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@totalTopQuery+CHAR(10)+')AS Records'

									--print @rowNumberQuery
			
			

			--============================== Final Query ===================================
			Set @finalQuery  = ' SELECT  * FROM ('
								+ CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
				EXEC (@finalQuery)
	--print(@finalQuery)

	SET @parameterDef = '@selectCount int OUTPUT';
	SET @selectCountQ= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria  + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme  + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock  +') as Records ) '

	

	EXECUTE sp_executesql @selectCountQ, @parameterDef, @selectCount OUTPUT;
	--print(@selectCount)

	SELECT DATEADD(YY,1,GETDATE())

	SET @selectCountE= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(YY,1,ISSUEDATE))<=0    ) as Records ) '
	EXECUTE sp_executesql @selectCountE, @parameterDef, @selectCountExpired OUTPUT;
	--print( @parameterDef)

	SET @selectCount2= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>=0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>=0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>=0 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=14    ) as Records ) '
	 + ' '
	EXECUTE sp_executesql @selectCount2, @parameterDef, @selectCount2Weeks OUTPUT;

	SET @selectCount4= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>14 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=28    ) as Records ) '
	 + ' '
	EXECUTE sp_executesql @selectCount4, @parameterDef, @selectCount4Weeks OUTPUT;

	SET @selectCount8= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>28 and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=56    ) as Records ) '
	 + ' '
	EXECUTE sp_executesql @selectCount8, @parameterDef, @selectCount8Weeks OUTPUT;

	SET @selectCount12= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>56 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=84    ) as Records ) '
	 + ' '
	EXECUTE sp_executesql @selectCount12, @parameterDef, @selectCount12Weeks OUTPUT;

	SET @selectCount16= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>84 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=112    ) as Records ) '
	 + ' '
	EXECUTE sp_executesql @selectCount16, @parameterDef, @selectCount16Weeks OUTPUT;

	SET @selectCount52= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>112 AND DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))<=364    ) as Records ) '
	 + ' '
	EXECUTE sp_executesql @selectCount52, @parameterDef, @selectCount52Weeks OUTPUT;

	SET @selectCount52p= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and DATEDIFF(D,GETDATE(),DATEADD(DD,364,ISSUEDATE))>364    ) as Records ) '
	EXECUTE sp_executesql @selectCount52p, @parameterDef, @selectCount52PlusWeeks OUTPUT;

	
	SET @selectCountno= 'SELECT @selectCount =  ( select count(*) FROM ('+ @selectClause + @fromClause + @searchCriteria +CHAR(10)  +
													' and ((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL))    ' + @unionQuery +
						@SelectClauseScheme + @fromClauseScheme + @searchCriteriaScheme +CHAR(10)  +
													' and ((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL))    ' + @unionQuery +
						@SelectClauseBlock + @fromClauseBlock + @searchCriteriaBlock +CHAR(10)  +
													' and ((CP12NUMBER IS NULL AND ISSUEDATE IS NULL) OR (CP12NUMBER IS NOT NULL AND ISSUEDATE IS NULL))    ) as Records ) '
	 + ''
	EXECUTE sp_executesql @selectCountno, @parameterDef, @selectCountNoWeeks OUTPUT;
END
