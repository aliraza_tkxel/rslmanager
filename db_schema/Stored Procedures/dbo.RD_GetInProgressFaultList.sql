USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- Stored Procedure

-- =============================================
--DECLARE	@totalCount int
--EXEC	[dbo].[RD_GetInProgressFaultList]
--		@searchedText = NULL	,	
--		@totalCount = @totalCount OUTPUT		
-- Author:		Ali Raza
-- Create date: <7/35/2013>
-- Description:	<Get list of InProgress Faults>
-- Web Page: ReportsArea.aspx
-- =============================================

IF OBJECT_ID('dbo.RD_GetInProgressFaultList') IS NULL
 EXEC('CREATE PROCEDURE dbo.RD_GetInProgressFaultList AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[RD_GetInProgressFaultList]

( 

	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@financialYear INT,
		@searchedText VARCHAR(8000) = '',
		--@operativeId int,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Recorded',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @SelectClauseInProgress varchar(2000),
		@fromClauseInProgress   varchar(1500),
		@SelectClausePaused varchar(2000),
		@fromClausePaused   varchar(1500),
		@unionClause varchar(100),
		@whereClauseInProgress  varchar(1500),
		@whereClausePaused  varchar(1500),	        
		@orderClause  varchar(100),	
		@mainSelectQuery varchar(5500),        
		@rowNumberQuery varchar(6000),
		@finalQuery varchar(6500),
		@totalInProgress INT,
		@totalPaused INT,
		-- used to add in conditions in WhereClause based on search criteria provided
		@searchCriteriaInProgress varchar(1500),
		 @searchCriteriaPaused varchar(1500),

		--variables for paging
		@offset int,
		@limit int

	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1

	--========================================================================================
	-- Begin building SearchCriteria clause for InProgress
	-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteriaInProgress = 'StatusID =15'

	IF(@searchedText != '' AND  @searchedText IS NOT NULL)
	BEGIN			
		--SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+'''))'
		SET @searchCriteriaInProgress = @searchCriteriaInProgress + CHAR(10) +'AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
	END	
	-- Add checks for Scheme and Block 
	IF(@schemeId > 0)
		BEGIN
			SET @searchCriteriaInProgress = @searchCriteriaInProgress + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.SchemeId IS NULL 
																	THEN	P__PROPERTY.SchemeId 
																	ELSE 		
																			FL_FAULT_LOG.SchemeId 	
																	END = '+convert(varchar(10),@schemeId)
		END
	IF(@blockId > 0)
		BEGIN
			SET @searchCriteriaInProgress = @searchCriteriaInProgress + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.BlockId IS NULL 
																	THEN P__PROPERTY.BLOCKID 
																	ELSE 		
																		FL_FAULT_LOG.BlockId 	
																	END = '+convert(varchar(10),@blockId)
		END

	IF (@financialYear != -1 AND LEN(@financialYear) = 4)
		BEGIN
			SET @searchCriteriaInProgress = @searchCriteriaInProgress + CHAR(10) + ' AND FL_FAULT_LOG.SubmitDate BETWEEN ''' + CONVERT(VARCHAR,@financialYear) + '0401'' AND ''' + CONVERT(VARCHAR,@financialYear+1) + '0331 23:59:59.997'''
		END

	-- End building SearchCriteria clause   
	--========================================================================================
	
	--========================================================================================
	-- Begin building SearchCriteria clause for Paused
	-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteriaPaused = ' StatusID =16 '

	IF(@searchedText != '' AND @searchedText IS NOT NULL)
	BEGIN			
		--SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+'''))'
		SET @searchCriteriaPaused = @searchCriteriaPaused + CHAR(10) +'AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
	END	

	IF(@schemeId > 0)
		BEGIN
			SET @searchCriteriaPaused = @searchCriteriaPaused + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.SchemeId IS NULL 
																	THEN	P__PROPERTY.SchemeId 
																	ELSE 		
																			FL_FAULT_LOG.SchemeId 	
																	END = '+convert(varchar(10),@schemeId)
		END
	IF(@blockId > 0)
		BEGIN
			SET @searchCriteriaPaused = @searchCriteriaPaused + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.BlockId IS NULL 
																	THEN P__PROPERTY.BLOCKID 
																	ELSE 		
																		FL_FAULT_LOG.BlockId 	
																	END = '+convert(varchar(10),@blockId)
		END
	IF (@financialYear != -1 AND LEN(@financialYear) = 4)
		BEGIN
			SET @searchCriteriaPaused = @searchCriteriaPaused + CHAR(10) + ' AND FL_FAULT_LOG.SubmitDate BETWEEN ''' + CONVERT(VARCHAR,@financialYear) + '0401'' AND ''' + CONVERT(VARCHAR,@financialYear+1) + '0331 23:59:59.997'''
		END

	-- End building SearchCriteria clause   
	--========================================================================================
	
	--========================================================================================	        
	-- Begin building SELECT clause
	-- SELECT statements for procedure here

	SET @SelectClauseInProgress = 'SELECT  DISTINCT TOP ('+convert(varchar(10),@limit)+') FL_FAULT_LOG.JobSheetNumber as JSN,
	CONVERT(nvarchar(50),FL_FAULT_LOG.SubmitDate, 103) as Recorded,
	ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
	ISNULL(P__PROPERTY.HOUSENUMBER,'''') + ISNULL('' '' + P__PROPERTY.ADDRESS1,'''') as Address,
	FL_AREA.AreaName as Location, FL_FAULT.Description as Description,FL_FAULT_STATUS.Description AS Status,
	FL_FAULT_PRIORITY.PriorityName AS Priority,convert(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103)+'' ''+ convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentDate,  P__PROPERTY.HouseNumber as HouseNumber,
	P__PROPERTY.ADDRESS1 as Address1, FL_FAULT_LOG.SubmitDate as RecordedOn,
	FL_FAULT.duration as Interval, FL_FAULT_LOG.FaultLogID as FaultLogID
	,FL_CO_APPOINTMENT.AppointmentDate +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentSort
	,Case 
		When FL_FAULT_LOG.PROPERTYID IS NULL THEN	''SbFault''	
		Else ''Fault''	End	AS AppointmentType
	'

	-- End building SELECT clause
	--======================================================================================== 							

	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClauseInProgress =	  CHAR(10) +'FROM FL_FAULT_LOG
	JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
	LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
	JOIN FL_AREA on FL_FAULT_LOG.AREAID = FL_AREA.AreaID
	JOIN FL_FAULT_TRADE on FL_FAULT.FaultID = FL_FAULT_TRADE.FaultId
	JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
	JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
	JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
	JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
	JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId
	LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
	Case WHEN FL_FAULT_LOG.SchemeId is NULL THEN P__PROPERTY.SchemeId 
	ELSE 		FL_FAULT_LOG.SchemeId 	END
	LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID	=
	Case WHEN FL_FAULT_LOG.BlockId is NULL THEN P__PROPERTY.BLOCKID 
	ELSE 		FL_FAULT_LOG.BlockId 	END		
	'
	-- End building From clause
	--======================================================================================== 														  

	--========================================================================================	        
	-- Begin building SELECT clause
	-- SELECT statements for procedure here

	SET @SelectClausePaused = 'SELECT TOP ('+convert(varchar(10),@limit)+') FL_FAULT_LOG.JobSheetNumber as JSN,
		CONVERT(nvarchar(50),FL_FAULT_PAUSED.PausedOn, 103) as Recorded,
		ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
		ISNULL(P__PROPERTY.HOUSENUMBER,'''') + ''''  + ISNULL(P__PROPERTY.ADDRESS1,'''') as Address,
		FL_AREA.AreaName as Location, FL_FAULT.Description as Description,FL_FAULT_STATUS.Description AS Status,
		FL_FAULT_PRIORITY.PriorityName AS Priority,convert(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103)+'' ''+ convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentDate,  P__PROPERTY.HouseNumber as HouseNumber,
		P__PROPERTY.ADDRESS1 as Address1, FL_FAULT_PAUSED.PausedOn as RecordedOn,
		FL_FAULT.duration as Interval, FL_FAULT_LOG.FaultLogID as FaultLogID
		,FL_CO_APPOINTMENT.AppointmentDate +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentSort
		,Case 
			When FL_FAULT_LOG.PROPERTYID IS NULL THEN	''SbFault''	
			Else ''Fault''	End	AS AppointmentType '+CHAR(10)

	-- End building SELECT clause
	--======================================================================================== 							

	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClausePaused =	  CHAR(10) +'FROM FL_FAULT_PAUSED		
		INNER JOIN FL_FAULT_LOG on FL_FAULT_PAUSED.FaultLogId = FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		INNER JOIN FL_AREA on FL_FAULT_LOG.AREAID = FL_AREA.AreaID
		INNER JOIN (SELECT max(FL_FAULT_JOBTIMESHEET.TimeSheetID) TimeSheetID,FL_FAULT_JOBTIMESHEET.FaultLogId, MAX(FL_FAULT_JOBTIMESHEET.StartTime) StartTime
					FROM FL_FAULT_JOBTIMESHEET 
					group by FL_FAULT_JOBTIMESHEET.FaultLogId ) TimeSheetTable on FL_FAULT_PAUSED.FaultLogId = TimeSheetTable.FaultLogId		
		INNER JOIN E__EMPLOYEE on FL_FAULT_PAUSED.PausedBy = E__EMPLOYEE.EMPLOYEEID 
		LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID	
		LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
		Case WHEN FL_FAULT_LOG.SchemeId is NULL THEN P__PROPERTY.SchemeId 
		ELSE 		FL_FAULT_LOG.SchemeId 	END
		LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID	=
		Case WHEN FL_FAULT_LOG.BlockId is NULL THEN P__PROPERTY.BLOCKID 
		ELSE 		FL_FAULT_LOG.BlockId 	END		
		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
		JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
		JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId '+CHAR(10)
	-- End building From clause
	--======================================================================================== 														  


	--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'ADDRESS1' 		
	END

	IF(@sortColumn = 'Recorded')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
	END

	IF(@sortColumn = 'JSN')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
	END

	IF(@sortColumn = 'Scheme')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Scheme' 		
	END	

	IF(@sortColumn = 'Block')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Block' 		
	END	

	IF(@sortColumn = 'AppointmentDate')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'AppointmentSort' 		
	END

	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

	-- End building OrderBy clause
	--========================================================================================								

	--========================================================================================
	-- Begin building WHERE clause

	-- This Where clause contains subquery to exclude already displayed records			  

	SET @whereClauseInProgress =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteriaInProgress 

	-- End building WHERE clause
	--========================================================================================
	--========================================================================================
	-- Begin building WHERE clause

	-- This Where clause contains subquery to exclude already displayed records			  

	SET @whereClausePaused =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteriaPaused

	-- End building WHERE clause
	--========================================================================================
	--========================================================================================
	--Set union Clause
	SET @unionClause=CHAR(10)+CHAR(9) +'UNION ALL'+CHAR(10)+CHAR(9)  
	--========================================================================================
	--========================================================================================

	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery = @SelectClauseInProgress +@fromClauseInProgress+@whereClauseInProgress +@unionClause+@SelectClausePaused+@fromClausePaused + @whereClausePaused + @orderClause 
	--Set @mainSelectQuery = @SelectClauseInProgress +@fromClauseInProgress+@whereClauseInProgress --+@unionClause+@SelectClausePaused+@fromClausePaused + @whereClausePaused + @orderClause 

	-- End building the main select Query
	--========================================================================================																																			

	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	-- Declaring the variables to be used in In Progress Count Query
	Declare @selectInProgressCount nvarchar(MAX),
			@parameterDefInProgressCount NVARCHAR(500)

	SET @parameterDefInProgressCount = '@totalInProgress int OUTPUT';
	SET @selectInProgressCount = 'SELECT  @totalInProgress = COUNT(DISTINCT FL_FAULT_LOG.FaultLogID) ' + @fromClauseInProgress + @whereClauseInProgress

	--print @selectInProgressCount
	EXECUTE sp_executesql @selectInProgressCount, @parameterDefInProgressCount, @totalInProgress OUTPUT;

	-- Declaring the variables to be used in In Progress Count Query
	Declare @selectPausedCount nvarchar(MAX),
			@parameterDefPausedCount NVARCHAR(500)

	SET  @parameterDefPausedCount= '@totalPaused INT OUTPUT';
	SET  @selectPausedCount = 'SELECT  @totalPaused = COUNT(DISTINCT FL_FAULT_LOG.FaultLogID) ' + @fromClausePaused + @whereClausePaused

	--print @selectInProgressCount
	EXECUTE sp_executesql @selectPausedCount, @parameterDefPausedCount, @totalPaused OUTPUT;

	SELECT @totalCount = ISNULL(@totalInProgress,0) + ISNULL(@totalPaused,0)

	-- End building the Count Query
	--========================================================================================							
END