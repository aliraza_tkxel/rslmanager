SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Rogers	
-- Create date: 16th October 2014
-- Description:	Retrieves data for the establishment section of RSL
-- =============================================
CREATE PROCEDURE [dbo].[E_ESTABLISHMENT_TEAM]
	-- Add the parameters for the stored procedure here
    @teamId INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @holrequest INT

        DECLARE @employees TABLE
            (
              ROWID INT IDENTITY(1, 1)
                        NOT NULL ,
              TEAMNAME VARCHAR(50) ,
              EMPLOYEEID INT ,
              FULLNAME VARCHAR(255) ,
              JOBTITLE VARCHAR(255) ,
              GRADE VARCHAR(255) ,
              REMAININGLEAVE INT ,
              APPRAISALDATE VARCHAR(255) ,
              HOLREQUEST VARCHAR(10)
            )

        DECLARE @leaveTbl TABLE
            (
              dayshours VARCHAR(MAX) ,
              leave_available INT ,
              annual_leave_days INT ,
              carry_fwd INT ,
              bnk_hd INT ,
              total_leave INT ,
              time_off_in_lieu_owed INT ,
              part_full INT
            )

        INSERT  INTO @employees
                ( TEAMNAME ,
                  EMPLOYEEID ,
                  FULLNAME ,
                  JOBTITLE ,
                  GRADE ,
                  REMAININGLEAVE ,
                  APPRAISALDATE ,
                  HOLREQUEST
                )
                SELECT  T.TEAMNAME ,
                        E.EMPLOYEEID ,
                        E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME ,
                        ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE ,
                        ISNULL(CAST(G.DESCRIPTION AS NVARCHAR), 'N/A') AS GRADE ,
                        '' AS REMAININGLEAVE ,
                        '' AS APPRAISALDATE ,
                        '' AS HOLREQUEST
                FROM    E__EMPLOYEE AS E
                        LEFT JOIN E_JOBDETAILS AS J ON E.EMPLOYEEID = J.EMPLOYEEID
                        LEFT JOIN E_TEAM AS T ON T.TEAMID = J.TEAM
                        LEFT JOIN E_GRADE AS G ON J.GRADE = G.GRADEID
                WHERE   ( J.TEAM = @teamid )
                        AND ( J.ACTIVE = 1 )
                ORDER BY e.LASTNAME ,
                        e.FIRSTNAME
   
--SELECT * FROM @employees

        DECLARE @i INT = 1 
        DECLARE @total INT = ( SELECT   COUNT(*)
                               FROM     @employees
                             );

        WHILE ( @i <= @total ) 
            BEGIN 
                DECLARE @employeeId INT = ( SELECT  employeeid
                                            FROM    @employees
                                            WHERE   rowid = @i
                                          )
                INSERT  INTO @leaveTbl
                        ( dayshours ,
                          leave_available ,
                          annual_leave_days ,
                          carry_fwd ,
                          bnk_hd ,
                          total_leave ,
                          time_off_in_lieu_owed ,
                          part_full
                        )
                        EXEC dbo.E_HOLIDAY_DETAIL @EMP = @employeeId, @Print = 0
	
                SET @holrequest = ( SELECT  COUNT(*)
                                    FROM    dbo.E_JOURNAL
                                    WHERE   EMPLOYEEID = @employeeId
                                            AND ITEMID = 1
                                            AND ITEMNATUREID = 2
                                            AND CURRENTITEMSTATUSID = 3
                                  )
	
                UPDATE  @employees
                SET     REMAININGLEAVE = ( SELECT   leave_available
                                           FROM     @leaveTbl
                                         ) ,
                        APPRAISALDATE = NULL ,
                        HOLREQUEST = ( SELECT   CASE WHEN @holrequest = 1
                                                     THEN 'Yes'
                                                     ELSE 'No'
                                                END
                                     )
                WHERE   ROWID = @i
	
                DELETE  FROM @leaveTbl
                SET @i = @i + 1
            END

        SELECT  ROWID,
				TEAMNAME ,
                EMPLOYEEID ,
                FULLNAME ,
                JOBTITLE ,
                GRADE ,
                REMAININGLEAVE ,
                APPRAISALDATE ,
                HOLREQUEST
        FROM    @employees
    END
GO
