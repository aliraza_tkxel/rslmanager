
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_UserTypes
-- Author:		<Salman Nazir>
-- Create date: <23/09/2012>
-- Description:	<This Stored Proceedure shows all the user type in the drop down by clicking on Edit button on Resources Page User Listing>
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_UserTypes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM AS_USERTYPE
END
GO
