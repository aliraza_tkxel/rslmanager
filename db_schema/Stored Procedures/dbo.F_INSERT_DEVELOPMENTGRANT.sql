SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[F_INSERT_DEVELOPMENTGRANT]
	(@TXNID int,
	 @ACCOUNTID int,
	 @DATE datetime,
	 @AMOUNT money,
	 @DESCRIPTION varchar(250),
	 @USERID int,
	 @dev int)
as
	begin
		
		INSERT INTO P_DEVLOPMENTGRANT(TXNID, 
					      ACCOUNTID, 
                                              [DATE], 
                                              AMOUNT, 
                                              [DESCRIPTION], 
                                              USERID,DEVCENTREID)
		VALUES(@TXNID, @ACCOUNTID, @DATE, @AMOUNT, @DESCRIPTION, @USERID,@dev)
	end
GO
