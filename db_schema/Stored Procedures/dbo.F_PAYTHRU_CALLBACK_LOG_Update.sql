SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[F_PAYTHRU_CALLBACK_LOG_Update] 
    @CallbackId int,
    @TransactionId uniqueidentifier,
    @Payload varchar(MAX),
    @PersonTitle varchar(50),
    @PersonFirstName varchar(50),
    @PersonSurname varchar(50),
    @PersonEmail varchar(250),
    @PersonMobileNumber varchar(50),
    @PersonHomePhone varchar(50),
    @TransactionKey varchar(250),
    @TransactionTime varchar(250),
    @TransactionStatus varchar(50),
    @TransactionValue varchar(250),
    @TransactionAuthCode varchar(250),
    @TransactionClass varchar(250),
    @TransactionToken varchar(250),
    @TransactionCurrency varchar(250),
    @TransactionType varchar(MAX),
    @TransactionMaid varchar(250),
    @Items0price varchar(250),
    @Items0name varchar(250),
    @Items0quantity varchar(50),
    @Items0reference varchar(250),
    @TransactionIpAddress varchar(250),
    @CustomTenancyRef varchar(250),
    @CustomCustomerRef varchar(250),
    @CustomStaffId varchar(250),
    @CustomStaffName varchar(250),
    @DateReceived datetime,
    @DateProcessed datetime,
    @DateCreated datetime,
    @CreatedBy varchar(256)
AS 
	SET NOCOUNT ON 
	
    UPDATE  [dbo].[F_PAYTHRU_CALLBACK_LOG]
    SET     
            [TransactionId] = @TransactionId ,
            [Payload] = @Payload ,
            [PersonTitle] = @PersonTitle ,
            [PersonFirstName] = @PersonFirstName ,
            [PersonSurname] = @PersonSurname ,
            [PersonEmail] = @PersonEmail ,
            [PersonMobileNumber] = @PersonMobileNumber ,
            [PersonHomePhone] = @PersonHomePhone ,
            [TransactionKey] = @TransactionKey ,
            [TransactionTime] = @TransactionTime ,
            [TransactionStatus] = @TransactionStatus ,
            [TransactionValue] = @TransactionValue ,
            [TransactionAuthCode] = @TransactionAuthCode ,
            [TransactionClass] = @TransactionClass ,
            [TransactionToken] = @TransactionToken ,
            [TransactionCurrency] = @TransactionCurrency ,
            [TransactionType] = @TransactionType ,
            [TransactionMaid] = @TransactionMaid ,
            [Items0price] = @Items0price ,
            [Items0name] = @Items0name ,
            [Items0quantity] = @Items0quantity ,
            [Items0reference] = @Items0reference ,
            [TransactionIpAddress] = @TransactionIpAddress ,
            [CustomTenancyRef] = @CustomTenancyRef ,
            [CustomCustomerRef] = @CustomCustomerRef ,
            [CustomStaffId] = @CustomStaffId ,
            [CustomStaffName] = @CustomStaffName ,
            [DateReceived] = @DateReceived ,
            [DateProcessed] = @DateProcessed ,
            [DateCreated] = @DateCreated ,
            [CreatedBy] = @CreatedBy 
    WHERE   [CallbackId] = @CallbackId




GO
