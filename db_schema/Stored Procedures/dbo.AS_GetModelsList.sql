USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetModelsList]    Script Date: 18-Jan-17 12:35:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Exec AS_Locations  
-- Author:  <Muhammad Awais>  
-- Create date: <04-Apr-2014>  
-- Description: <Get All models>  
-- Web Page: Appliance.ascx  
-- =============================================  
IF OBJECT_ID('dbo.[AS_GetModelsList]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetModelsList] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetModelsList]  
(@prefix varchar(100) )  

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 
 SELECT a.ModelID as id, a.Model as title  
 FROM GS_ApplianceModel a
 INNER JOIN
  (SELECT Model, MIN(ModelID) as Id 
   FROM GS_ApplianceModel
   GROUP BY Model) AS b
   ON a.Model=b.Model
   AND a.ModelID=b.Id
where a.Model like'%'+@prefix+'%' 
  
END  
GO
