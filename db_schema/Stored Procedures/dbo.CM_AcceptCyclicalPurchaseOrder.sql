USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[CM_AcceptCyclicalPurchaseOrder]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CM_AcceptCyclicalPurchaseOrder] AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[CM_AcceptCyclicalPurchaseOrder]    Script Date: 	29/08/2017 14:21:37 ******/

-- =============================================
-- Author:		Ali Raza
-- Create date:	07/09/2017
-- Description:	Assign Cyclical Services to  Contractor
-- WebPage:		PropertyDataRestructure/Views/CyclicalServices/AcceptCyclicalPurchaseOrder.aspx
-- =============================================
ALTER PROCEDURE [dbo].[CM_AcceptCyclicalPurchaseOrder]
	@orderId int,
	@status NVARCHAR(100),
	@isSaved BIT = 0 OUTPUT,
	@contactName nvarchar(200) = '' OUTPUT,
	@email nvarchar(200) ='' OUTPUT 			
AS
BEGIN
BEGIN TRANSACTION
BEGIN TRY
		DECLARE @statusId INT,@CMContractorId int 	
		SELECT @statusId = StatusId  FROM CM_Status where Title = @status
		SELECT @CMContractorId=CMContractorId from CM_ContractorWork where PurchaseOrderId = @orderId

		Update CM_ServiceItems set StatusId = @statusId where PORef = @orderId

		Update CM_ContractorWorkDetail Set StatusId = @statusId where CMContractorId=@CMContractorId
		
		SELECT @email=C.WORKEMAIL,@contactName=E.FIRSTNAME + ' '+E.LASTNAME   from E__EMPLOYEE E
		INNER JOIN E_CONTACT C ON E.EMPLOYEEID= C.EMPLOYEEID
		INNER JOIN CM_ContractorWork cm on E.EMPLOYEEID = cm.ContactId
		 where cm.PurchaseOrderId = @orderId

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;
		SET @isSaved = 1;    	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION; 
		SET @isSaved = 0;    	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

END