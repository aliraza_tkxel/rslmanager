SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_Delete]

/*	===============================================================
	'   NAME:           TO_C_CUSTOMER_EMAILCHANGE
	'   DATE CREATED:   08 June 2008
	'   CREATED BY:     Munawar Nadeem
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To delete equirylog entry of customer
	'   UPDATE:         TO_ENQUIRY_LOG.RemoveFlag
	'   IN:             @enquiryLogID 
	'   OUT: 	        
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

    @enquiryLogID int
	    
    
    

AS

 
 
 /* ' update removeflag and set it to true */
                      
	UPDATE    
	          TO_ENQUIRY_LOG
	
    SET
              TO_ENQUIRY_LOG.ItemStatusID = '28' ,
              TO_ENQUIRY_LOG.CloseFlag = CONVERT(varchar, (CONVERT(VARBINARY(1),'1'))) 

    WHERE 
              ( TO_ENQUIRY_LOG.EnquiryLogID= @enquiryLogID )
              


GO
