SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_CO_REACTIVEREPAIR_GRID]
(
   @customerId INT,
   @tenancyId INT
)

/* ===========================================================================
 '   NAME:           FL_REPORTEDFAULTSEARCHLIST
 '   DATE CREATED:   20TH February, 2009
 '   CREATED BY:     Tahir Gul 
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To Populate Reactive Repair Grid
 '   IN:             @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	-- variables used to build whole query
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),
	        @OrderClause  varchar(8000)

	
        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT' + 
						CHAR(10) + CHAR(9) + 'FL_FAULT_JOURNAL.CreationDate as LastActionDate , FL_FAULT_STATUS.DESCRIPTION as FaultStatus,'+
						CHAR(10) + CHAR(9) + 'CONVERT(varchar,  FL_FAULT_LOG.DueDate, 103) as DueDate, FL_FAULT_LOG.FaultLogID, FL_FAULT.FaultID, FL_FAULT_LOG.StatusID, ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.[Description] AS FaultDescription,FL_FAULT.NetCost,FL_FAULT.PreInspection, Convert(varchar, FL_FAULT_LOG.SubmitDate, 103) as SubmitDate,' +
	         			CHAR(10) + CHAR(9) +  'C_ITEM.ItemID, C_ITEM.[Description] AS ItemDescription, C_NATURE.[Description] as NatureDescription, C_NATURE.ITEMNATUREID '  
		--CHAR (10) + CHAR(9) + 'FL_FAULT_PREINSPECTIONINFO.faultlogid as PreInspectionInforFaultLogId'
                       
                    
    -- End building SELECT clause
      
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 

		CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS INNER JOIN FL_FAULT_JOURNAL' +
		CHAR(10) + CHAR(9) + 'ON FL_FAULT_STATUS.FaultStatusId = FL_FAULT_JOURNAL.FaultStatusID' +
		CHAR(10) + CHAR(9) + 'INNER JOIN C_ITEM' + 
		CHAR(10) + CHAR(9) + 'ON FL_FAULT_JOURNAL.ItemId = C_ITEM.ItemId'+
		CHAR(10) + CHAR(9) + 'INNER JOIN C_NATURE ON' + 
		CHAR(10) + CHAR(9) + 'C_ITEM.ItemId = C_NATURE.ItemId' + 
		CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_LOG ON'+ 
		CHAR(10) + CHAR(9) + 'FL_FAULT_JOURNAL.FaultLogId = FL_FAULT_LOG.FaultLogId' + 
		CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ON '+
		CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultId = FL_FAULT.FaultId' +
		CHAR(10) + CHAR(9) + 'INNER JOIN C__CUSTOMER ON FL_FAULT_LOG.CUSTOMERID = C__CUSTOMER.CUSTOMERID '  +
		CHAR(10) + CHAR(9) + 'INNER JOIN C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL ' + 
		CHAR(10) + CHAR(9) + 'INNER JOIN C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL'  
			
		--CHAR(10) + CHAR(9) + 'LEFT JOIN FL_FAULT_PREINSPECTIONINFO' + 
		--CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultLogId = FL_FAULT_PREINSPECTIONINFO.faultlogid '
                  
                     
    -- End building FROM clause
     
    -- Begin building WHERE clause
      --SET @WhereClause = CHAR(10)+  CHAR(10) + 'WHERE (' +
      -- 		 CHAR(10) + CHAR (9) + '  C_ITEM.ItemID =1' +  ' AND ' + 'FL_FAULT_LOG.CustomerId = ' + CONVERT(varchar, @customerId) + ' AND ' +  ' C_NATURE.DESCRIPTION = ''REACTIVE REPAIR'' )' +
	--	     CHAR(10) + CHAR (9) + 'ORDER BY FL_FAULT_LOG.FaultLogId DESC'   
		     
	SET @WhereClause = CHAR(10)+  CHAR(10) + 'WHERE (' +
       		 CHAR(10) + CHAR (9) + '  C_ITEM.ItemID =1' +  ' AND ' + 'C_TENANCY.TENANCYID = ' + CONVERT(varchar, @tenancyId) + ' AND ' +  ' C_NATURE.DESCRIPTION = ''REACTIVE REPAIR'' )' +
		     CHAR(10) + CHAR (9) + 'ORDER BY FL_FAULT_LOG.FaultLogId DESC'	     
    -- End building WHERE clause
   /*BEGIN
   Select  FL_FAULT_LOG.FaultLogId,  FL_FAULT_PREINSPECTIONINFO.faultlogid
   FROM FL_FAULT_PREINSPECTIONINFO INNER JOIN FL_FAULT_LOG
   ON  FL_FAULT_LOG.FaultLogId = FL_FAULT_PREINSPECTIONINFO.faultlogid
  WHERE FL_FAULT_LOG.FaultLogId = FL_FAULT_PREINSPECTIONINFO.faultlogid

  IF @@ERROR < 0
  SET   @result = -1 
  ELSE
  SET @result = 1
  END  */
  
    PRINT (@SelectClause + @FromClause +@WhereClause)

    
    EXEC (@SelectClause + @FromClause + @WhereClause)
    
    

RETURN



GO
