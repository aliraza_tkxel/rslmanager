USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[P_GetSchemeBlockImages]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[P_GetSchemeBlockImages] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[P_GetSchemeBlockImages] 
	@requestType Varchar(200),
	@id int
	,@count int out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @requestType = 'block'
	begin
		SELECT 
			PA_PROPERTY_ITEM_IMAGES.ImagePath 
			,ImageName
			,'Stock' as Type
			,COnvert(varchar(10),CreatedOn,103) as CreatedOn
			,(ISNULL(E__EMPLOYEE.FirstNAME,'')+' '+ISNULL(E__EMPLOYEE.LastName,'')) as CreatedBy
			,ISNULL(PA_PROPERTY_ITEM_IMAGES.Title,'') Title
		FROM
			PA_PROPERTY_ITEM_IMAGES
			INNER JOIN E__EMPLOYEE on PA_PROPERTY_ITEM_IMAGES.CreatedBy = E__EMPLOYEE.EmployeeId	
		WHERE 
			PA_PROPERTY_ITEM_IMAGES.BlockId=@id
			
			
			
		SET @count =(	SELECT	COUNT(*) 
						FROM	PA_PROPERTY_ITEM_IMAGES y
						INNER JOIN E__EMPLOYEE on y.CreatedBy = E__EMPLOYEE.EmployeeId	
						WHERE 	y.BlockId=@id	)
	end
	else
	begin
		SELECT 
			PA_PROPERTY_ITEM_IMAGES.ImagePath 
			,ImageName
			,'Stock' as Type
			,COnvert(varchar(10),CreatedOn,103) as CreatedOn
			,(ISNULL(E__EMPLOYEE.FirstNAME,'')+' '+ISNULL(E__EMPLOYEE.LastName,'')) as CreatedBy
			,ISNULL(PA_PROPERTY_ITEM_IMAGES.Title,'') Title
		FROM
			PA_PROPERTY_ITEM_IMAGES
			INNER JOIN E__EMPLOYEE on PA_PROPERTY_ITEM_IMAGES.CreatedBy = E__EMPLOYEE.EmployeeId	
		WHERE 
			PA_PROPERTY_ITEM_IMAGES.SchemeId=@id
			
			
			
		SET @count =(	SELECT	COUNT(*) 
						FROM	PA_PROPERTY_ITEM_IMAGES y
						INNER JOIN E__EMPLOYEE on y.CreatedBy = E__EMPLOYEE.EmployeeId	
						WHERE 	y.SchemeId=@id	)
	end
	 	 	 	 
END
