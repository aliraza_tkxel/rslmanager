USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[CONTRACTOR_GetSearchProperty]    Script Date: 01/26/2017 11:59:12 ******/



IF OBJECT_ID('dbo.[CONTRACTOR_GetSearchProperty]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CONTRACTOR_GetSearchProperty] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[CONTRACTOR_GetSearchProperty] 
	@searchText varchar (MAX) = ''
AS  
BEGIN  
  DECLARE	
			@PropertySelectClause Nvarchar(Max),
			@PropertyFromClause   Nvarchar(Max),
			@PropertyWhereClause  Nvarchar(Max),		                
			@PropertySearchCriteria Nvarchar(Max),

			@SchemeSelectClause Nvarchar(Max),
			@SchemeFromClause   Nvarchar(Max),
			@SchemeWhereClause  Nvarchar(Max),		                
			@SchemeSearchCriteria Nvarchar(Max),

			@BlockSelectClause Nvarchar(Max),
			@BlockFromClause   Nvarchar(Max),
			@BlockWhereClause  Nvarchar(Max),		                
			@BlockSearchCriteria Nvarchar(Max),

			@mainSelectQuery Nvarchar(Max),
			@unionAll varchar(500) = 'Union all'
	        

    --========================================================================================
	--							PROPERTY SEARCH
    --========================================================================================
         
    SET @PropertySearchCriteria = ''                              
	
	SET @PropertySearchCriteria = @PropertySearchCriteria + CHAR(10) + CHAR(9) + 
							' SearchProperty.Address LIKE ''%'+ LTRIM(@searchText) +'%'' OR '

	SET @PropertySearchCriteria = @PropertySearchCriteria + CHAR(10) + CHAR(9) + 
							' SearchProperty.PROPERTYID  LIKE ''%' + @searchText + '%'' AND'
    
	-- Building Main Query for Property Search
                             
    SET @PropertySelectClause = 'SELECT ' +                      
    CHAR(10) + 'SearchProperty.PropertyId as Id, SearchProperty.Address as Address, SearchProperty.TypeDescription as TypeDescription, ''Property'' as PropertyType'                     
                           
    SET @PropertyFromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '( SELECT	P__PROPERTY.PROPERTYID PropertyId,P_PROPERTYTYPE.DESCRIPTION TypeDescription,ISNULL(P__PROPERTY.FLATNUMBER,'''')+'' ''+ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
									FROM	P__PROPERTY INNER JOIN
											P_PROPERTYTYPE ON p__property.PROPERTYTYPE = P_PROPERTYTYPE.PROPERTYTYPEID Left JOIN 
											P_BLOCK B ON  P__PROPERTY.BLOCKID = B.BLOCKID LEFT JOIN 
											P_SCHEME S On P__PROPERTY.SCHEMEID = S.SCHEMEID ) SearchProperty'  	                      
      
    SET @PropertyWhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @PropertySearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @PropertySelectClause +@PropertyFromClause + @PropertyWhereClause     
	-- Union All        
	--========================================================================================
	Set @mainSelectQuery = @mainSelectQuery + CHAR(10) +  @unionAll    
	--========================================================================================
	print('--PROPERTY SEARCH')
    print(@mainSelectQuery)
	--EXEC (@mainSelectQuery)	    

	
			
			
	--========================================================================================
	--							SCHEME SEARCH
    --========================================================================================
	SET @SchemeSearchCriteria = ''
    SET @SchemeSearchCriteria = @SchemeSearchCriteria + CHAR(10) + CHAR(9) + '( SearchScheme.Address LIKE ''%'+ LTRIM(@searchText) +'%'' ) AND'


    SET @SchemeSelectClause = 'SELECT DISTINCT ' +                      
    CHAR(10) + ' CONVERT(varchar(10), SearchScheme.SCHEMEID) as Id, SearchScheme.Address as Address, ''Scheme'' as TypeDescription, ''Scheme'' as PropertyType '
                           
    SET @SchemeFromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '(  SELECT S.SCHEMEID AS SCHEMEID, ISNULL(S.SCHEMENAME,'''') Address  
									FROM P_SCHEME S  ) SearchScheme'  	                      
      
    SET @SchemeWhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SchemeSearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @mainSelectQuery + CHAR(10) + @SchemeSelectClause +@SchemeFromClause + @SchemeWhereClause             
	
	-- Union All        
	--========================================================================================
	Set @mainSelectQuery = @mainSelectQuery + CHAR(10) +  @unionAll    
	--========================================================================================
	print('--SCHEME SEARCH')
    print(@mainSelectQuery)
	--EXEC (@mainSelectQuery)

    --========================================================================================
	--							BLOCK SEARCH
    --========================================================================================

	SET @BlockSearchCriteria = ''
    SET @BlockSearchCriteria = @BlockSearchCriteria + CHAR(10) + CHAR(9) + '( SearchBlock.Address LIKE ''%'+ LTRIM(@searchText) +'%'' ) AND'

    SET @BlockSelectClause = 'SELECT' +
    CHAR(10) + 'CONVERT(varchar(10), SearchBlock.BLOCKID ) as Id, SearchBlock.Address as Address, ''Block'' as TypeDescription, ''Block'' as PropertyType '

    SET @BlockFromClause = CHAR(10) + 'FROM ' +
                      CHAR(10) + '( SELECT B.BLOCKID as BLOCKID, ISNULL(b.BLOCKNAME,'''') +'' ''+ ISNULL(b.ADDRESS1,'''') +'' ''+ ISNULL(b.ADDRESS2,'''') +'' ''+ ISNULL(b.ADDRESS3,'''') as Address
									FROM	P_BLOCK b  ) SearchBlock'

    SET @BlockWhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +
                        CHAR(10) + CHAR(10) + @BlockSearchCriteria +
                        CHAR(10) + CHAR(9) + ' 1=1 )'

    Set @mainSelectQuery = @mainSelectQuery + CHAR(10) + @BlockSelectClause +@BlockFromClause + @BlockWhereClause
	
	print('--BLOCK SEARCH')
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)
  
END
