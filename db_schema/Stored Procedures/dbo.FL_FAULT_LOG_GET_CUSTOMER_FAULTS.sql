SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE    PROCEDURE dbo.FL_FAULT_LOG_GET_CUSTOMER_FAULTS
/* ===========================================================================
 '   NAME:           FL_FAULT_LOG_GETCUSTOMERFAULTS
 '   DATE CREATED:   20 Oct. 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve customer faults against specified CustomerID and Response Status
 '   IN:             @customerID
 '   IN:             @ResponseStatus
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
	@CustomerID INT,
	@FaultStatus VARCHAR(50)='0'
)
AS

IF @FaultStatus = '0' 

	SELECT F.FaultLogID,F.CustomerId,F.FaultID,F.FaultBasketID,F.SubmitDate,FD.ElementID,F.DueDate,F.ORGID,FP.ResponseTime,FS.DESCRIPTION As Status,FD.Description,FD.PriorityID,
	(CONVERT(VARCHAR, FP.ResponseTime) + ' ' + CASE FP.Days WHEN 1 THEN  'Day(s)'  WHEN 0 THEN 'Hour(s)' END) AS FResponseTime,
	CASE FD.Recharge WHEN 1 THEN CONVERT(VARCHAR,FD.Gross) WHEN 0 THEN '-' END AS FRecharge,	
	CASE SO.NAME WHEN NULL THEN 'To Be Confirmed' ELSE SO.NAME END AS ContractorName, 
	AP.AppointmentDate,case PRE.INSPECTIONDATE when null then '-' else PRE.INSPECTIONDATE end AS PREINSPECTIONDATE
	FROM FL_FAULT_LOG AS F 
	INNER JOIN FL_FAULT_STATUS AS FS ON F.StatusID = FS.FaultStatusID
	INNER JOIN FL_FAULT AS FD ON F.FaultID = FD.FaultID
	INNER JOIN FL_FAULT_PRIORITY AS FP ON FD.PriorityID = FP.PriorityID
	LEFT OUTER JOIN FL_CO_APPOINTMENT AS AP ON AP.JobSheetNumber = F.JobSheetNumber	
	LEFT OUTER JOIN S_ORGANISATION  AS SO ON SO.ORGID = F.ORGID
	INNER JOIN FL_FAULT_PREINSPECTIONINFO AS PRE ON F.FAULTLOGID=PRE.FAULTLOGID
	WHERE F.CustomerId = @CustomerID 
	ORDER By F.SubmitDate DESC, F.FaultLogID DESC
	
ELSE
	
	SELECT F.FaultLogID,F.CustomerId,F.FaultID,F.FaultBasketID,F.SubmitDate,FD.ElementID,F.DueDate,F.ORGID,FP.ResponseTime,FS.DESCRIPTION As Status,FD.Description,FD.PriorityID,
	(CONVERT(VARCHAR, FP.ResponseTime) + ' ' + CASE FP.Days WHEN 1 THEN  'Day(s)'  WHEN 0 THEN 'Hour(s)' END) AS FResponseTime,
	CASE FD.Recharge WHEN 1 THEN CONVERT(VARCHAR,FD.Gross) WHEN 0 THEN '-' END AS FRecharge,
	CASE SO.NAME WHEN NULL THEN 'To Be Confirmed' ELSE SO.NAME END AS ContractorName, 
	AP.AppointmentDate,case PRE.INSPECTIONDATE when null then '-' else PRE.INSPECTIONDATE end AS PREINSPECTIONDATE
	FROM FL_FAULT_LOG AS F 
	INNER JOIN FL_FAULT_STATUS AS FS ON F.StatusID = FS.FaultStatusID
	INNER JOIN FL_FAULT AS FD ON F.FaultID = FD.FaultID
	INNER JOIN FL_FAULT_PRIORITY AS FP ON FD.PriorityID = FP.PriorityID
	LEFT OUTER JOIN FL_CO_APPOINTMENT AS AP ON AP.JobSheetNumber = F.JobSheetNumber	
	LEFT OUTER JOIN S_ORGANISATION  AS SO ON SO.ORGID = F.ORGID
	INNER JOIN FL_FAULT_PREINSPECTIONINFO AS PRE ON F.FAULTLOGID=PRE.FAULTLOGID
	WHERE F.CustomerId = @CustomerID
	AND F.StatusID=@FaultStatus 
	ORDER By F.SubmitDate DESC, F.FaultLogID DESC




/*SET @OrderClause=' Order By F.SubmitDate DESC, F.FaultLogID DESC'

SET @SelectClause = 'Select F.FaultLogID,F.CustomerId,F.FaultID,F.FaultBasketID,F.SubmitDate,FD.ElementID,F.ContractorID,F.ResponseTime,FS.Status,FD.Description,FD.PriorityID,'+ CHAR(10) + 
' (FP.ResponseTime'+''','''+' FP.Days) as tays, '+ CHAR(10) +
' case FP.Days'+ CHAR(10) +
' when 1 then ''Day(s)'' when 0 then ''Hour(s)'' End As PType,' + CHAR(10) +
' case FD.Recharge'+ CHAR(10) +
' when 1 then FD.Gross when 0 then '''' End As FRecharge'+ CHAR(10) +
' FROM ' + CHAR(10) +
' FL_FAULT_LOG AS F '+ CHAR(10) +
' INNER JOIN FL_FAULT_STATUS AS FS'+ CHAR(10) + 
' ON F.StatusID = FS.StatusId'+ CHAR(10) + 
' INNER JOIN FL_FAULT AS FD'+ CHAR(10) + 
' ON F.FaultID = FD.FaultID'+ CHAR(10) + 
' INNER JOIN FL_FAULT_PRIORITY AS FP'+ CHAR(10) + 
' ON FD.PriorityID = FP.PriorityID'+ CHAR(10) + 
' Where F.CustomerId ='+ CHAR(10) + Convert(varchar, @CustomerID)*/
/*print(@SelectClause + @OrderClause)
EXEC(@SelectClause + @OrderClause)*/

/*IF @FaultStatus = '0' 
	EXEC(@SelectClause + @OrderClause)
ELSE
	BEGIN
			SET @SelectClause= @SelectClause + 'and F.StatusID='+ CHAR(10) + Convert(varchar, @FaultStatus)
			EXEC(@SelectClause + @OrderClause)
	END*/

GO
