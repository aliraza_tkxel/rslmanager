SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC diagnostics.FaultPrioritiesList

AS 

SELECT  fl.FaultID ,
        fl.Description ,
        e.ElementName ,
        a.AreaName ,
        fp.*
FROM    dbo.FL_FAULT FL
        LEFT JOIN dbo.FL_FAULT_PRIORITY FP ON FL.PriorityID = FP.PriorityID
        LEFT JOIN dbo.FL_ELEMENT e ON e.ElementID = fl.ElementID
        LEFT JOIN dbo.FL_AREA a ON e.AreaID = a.AreaID
ORDER BY fl.FaultID


GO
