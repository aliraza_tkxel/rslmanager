
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[F_SSIS_EMPLOYEE_LIMITS]
AS

SET NOCOUNT ON

DECLARE @YRange INT = ( SELECT  YRange
                        FROM    dbo.F_FISCALYEARS
                        WHERE   GETDATE() BETWEEN YStart AND YEnd
                      )

SELECT 
		E.FIRSTNAME + ' ' + E.LASTNAME AS [EMPLOYEE NAME] ,
        C.DESCRIPTION AS [COST CENTRE] ,
        H.DESCRIPTION AS [HEAD] ,
        X.DESCRIPTION AS [EXPENDITURE] ,
        CAST(L.LIMIT AS VARCHAR) AS [LIMIT (£)] ,
        ISNULL(CAST(CAST(XA.EXPENDITUREALLOCATION AS NUMERIC(10, 2)) AS VARCHAR),
               '') AS [EXPENDITURE ALLOCATION (£)]
FROM    dbo.F_EMPLOYEELIMITS l
        INNER JOIN E__EMPLOYEE e ON l.EMPLOYEEID = e.EMPLOYEEID
        INNER JOIN dbo.F_EXPENDITURE x ON X.EXPENDITUREID = L.EXPENDITUREID
        INNER JOIN dbo.F_HEAD H ON X.HEADID = H.HEADID
        INNER JOIN dbo.F_COSTCENTRE C ON C.COSTCENTREID = H.COSTCENTREID
        INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = C.COSTCENTREID
                                                  AND FISCALYEAR = @YRange
                                                  AND CCA.ACTIVE = 1
        INNER JOIN dbo.F_HEAD_ALLOCATION HA ON H.HEADID = HA.HEADID
                                               AND HA.ACTIVE = 1
                                               AND HA.FISCALYEAR = @YRange
        INNER JOIN dbo.F_EXPENDITURE_ALLOCATION XA ON XA.EXPENDITUREID = X.EXPENDITUREID
                                                      AND XA.ACTIVE = 1
                                                      AND XA.FISCALYEAR = @YRange
WHERE   H.COSTCENTREID <> 9 -- Excluding Development        
ORDER BY c.DESCRIPTION,
		 h.DESCRIPTION,
		 x.DESCRIPTION,
		 e.FIRSTNAME,
		 e.LASTNAME
GO
