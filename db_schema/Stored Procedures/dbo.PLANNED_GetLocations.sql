SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
-- Author:  <Author,,Zahid Ali>  
-- Create date: <Create Date,,15/07/2014>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[PLANNED_GetLocations]        
  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 IF OBJECT_ID('tempdb..#temp_locations') IS NOT NULL DROP TABLE #temp_locations  
 select PA_ITEM.ItemID, PA_ITEM.ParentItemId, PA_AREA.AreaName as [ItemName],PA_ITEM_PARAMETER.ParameterId into #temp_locations from PA_ITEM   
 inner join PA_AREA on PA_ITEM.AreaID = PA_AREA.AreaID   
 inner join PA_ITEM_PARAMETER ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId  
 where PA_ITEM.IsActive=1 and ItemName like '%aids &%'  
  
  
  
 UPDATE #temp_locations SET ItemName = CASE ParentItemId   
 WHEN 76 THEN 'Kitchen'  
 WHEN 75 THEN 'Bathroom'  
 ELSE ItemName  
 END   
  
 select ParameterId as ItemID, ItemName from #temp_locations  
END  
GO
