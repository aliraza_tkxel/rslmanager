SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE     PROC [dbo].[FL_REPAIRHISTORY_ADD](
/* ===========================================================================
 '   NAME:         FL_REPAIRHISTORY_ADD
 '   DATE CREATED:  21 NOV 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   Add a repairt history in FL_New_Repair table
 '
 '   IN:	   @JOURNALID
 '   IN:	   @ITEMSTATUSID
 '   IN:           @ITEMACTIONID
 '   IN:           @LASTACTIONDATE
 '   IN:           @LASTACTIONUSERID
 '   IN:           @FAULTLOGID
 '   IN:           @ORGID
 '   IN:           @SCOPEID
 '   IN:           @TITLE
 '   IN:           @NOTES
 '
 '   OUT:           @RESULT    JOURNALID
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

 @JOURNALID  AS INT,
 @ITEMSTATUSID AS INT,
 @ITEMACTIONID AS INT,
 @LASTACTIONDATE AS DATETIME,
 @LASTACTIONUSERID AS INT,
 @FAULTLOGID AS INT,
 @ORGID AS INT,
 @SCOPEID AS INT,
 @TITLE AS NVARCHAR(1000),
 @NOTES AS NVARCHAR(500),
 @RESULT AS INT OUT
)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN
INSERT INTO FL_FAULT_LOG_HISTORY
                      (JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
VALUES     (@JOURNALID,@ITEMSTATUSID,@ITEMACTIONID,@LASTACTIONDATE,@LASTACTIONUSERID,@FAULTLOGID,@ORGID,@SCOPEID,@TITLE,@NOTES)

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	
SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN












GO
