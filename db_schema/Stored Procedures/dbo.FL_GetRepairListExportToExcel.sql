Use RSLBHALive
GO
-- =============================================
-- Author:		<Author,Ali Raza>
-- Create date: <Create Date,May-12-2016>
-- Description:	<Description,get repair detail for export to excel >
-- =============================================


IF OBJECT_ID('dbo.FL_GetRepairListExportToExcel') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetRepairListExportToExcel AS SET NOCOUNT ON;') 
GO

Alter PROCEDURE dbo.FL_GetRepairListExportToExcel
(
@searchedText nvarchar(1000) = ''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT	DISTINCT 
	FL.FaultRepairListID,FL.[Description] as RepairDescription
	,FL.NetCost
	,ISNULL(F_VAT.VATNAME,'-') As VATRate
	,FL.Vat
	,FL.Gross	
	,Convert(nvarchar(50), case  when ISNULL( FL.PostInspection,0) =1 then 'Yes' else 'No'  end )as PostInspection
	,Convert(nvarchar(50), case  when ISNULL( FL.StockConditionItem,0) =1 then 'Yes' else 'No'  end )as SCI	
	,ISNULL(F.[Description],'-') As Fault	
    ,Convert(nvarchar(50), case  when ISNULL( RepairActive,0) =1 then 'Yes' else 'No'  end )as IsActive
	,ISNULL( A.AreaName,'-') as Location	
						
FROM	FL_FAULT_REPAIR_LIST AS FL 

Left JOIN FL_FAULT_ASSOCIATED_REPAIR FAR on FL.FaultRepairListID = FAR.RepairId
Left JOIN FL_FAULT F ON FAR.FaultId = F.FaultID
Left JOIN FL_AREA A ON F.AREAID = A.AreaID
LEFT JOIN F_VAT on F.VatRateID = VATID
Where (FL.[Description]  LIke '%'+@searchedText+'%' OR F.Description Like '%'+@searchedText+'%' OR @searchedText = '' )

 Order By RepairDescription
ASC

END
GO
