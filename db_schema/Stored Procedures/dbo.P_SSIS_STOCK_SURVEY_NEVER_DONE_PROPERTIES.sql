SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[P_SSIS_STOCK_SURVEY_NEVER_DONE_PROPERTIES]

AS 

SELECT DISTINCT
        P.PROPERTYID  AS [Property Ref],
        p.HOUSENUMBER AS [House Number],
        ISNULL(p.ADDRESS1, '') AS [Address Line 1] ,
        ISNULL(p.ADDRESS2, '') AS [Address Line 2] ,
        ISNULL(p.ADDRESS3, '') AS [Address Line 3] ,
        ISNULL(p.TOWNCITY, '') AS [City/Town] ,
        ISNULL(p.COUNTY, '') County ,
        ISNULL(p.POSTCODE, '') Postcode
FROM    P__PROPERTY P
        LEFT JOIN vwP_SSIS_STOCK_SURVEY_DONE_PROPERTIES S ON P.PROPERTYID = S.pROPERTYiD
WHERE   p.STATUS NOT IN ( 10, 5, 6, 7, 9 )
        AND P.PROPERTYTYPE NOT IN ( 6, 8, 9, 21, 19, 20 )
        AND S.PropertyId IS NULL 
 



GO
