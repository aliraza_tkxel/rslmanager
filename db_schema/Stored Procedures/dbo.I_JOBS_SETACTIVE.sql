SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[I_JOBS_SETACTIVE]
(
	@JobId bigint,
	@Active smallint
)
AS
	SET NOCOUNT OFF;
UPDATE [I_JOB] SET Active = @Active,DatePosted = GETDATE() WHERE ([JobId] = @JobId);


GO
