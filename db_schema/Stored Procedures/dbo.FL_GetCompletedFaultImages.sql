SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC FL_GetCompletedFaultImages
--@jobsheetnumber ='JS20171'
-- Author:		<Ahmed Mehmood>
-- Create date: <11/4/2014>
-- Description:	<Returns completed fault images detail>
-- Webpage:FaultBasket.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetCompletedFaultImages] 
	-- Add the parameters for the stored procedure here
	(
	@jobsheetnumber varchar(50)
	)
AS
BEGIN


SELECT	PropertyId
		,JobSheetNumber
		,ImageName
		,CASE	
			WHEN	IsBeforeImage = 1 THEN
					'Before'  
			ELSE
					'After' 
		END AS Status
			
FROM	FL_FAULT_REPAIR_IMAGES 
		
WHERE	JobSheetNumber = @jobsheetnumber

END

GO
