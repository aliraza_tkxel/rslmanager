SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[FL_TEAM_GETLOOKUP]
/* ===========================================================================
 '   NAME:          FL_TEAM_GETLOOKUP
 '   DATE CREATED:   30 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get team from E_Team table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT TEAMID AS id,TEAMNAME AS val
	FROM E_TEAM
	WHERE TEAMID NOT IN (1,93) AND ACTIVE=1
	ORDER BY TEAMNAME ASC















GO
