SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[FL_TBL_ALL_AREAS_GETLOOKUP]
/* ===========================================================================
 '   NAME:          FL_TBL_ALL_AREAS_GETLOOKUP
 '   DATE CREATED:   20th May,2009
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Reidmark
 '   PURPOSE:        To get Full table of FL_AREA 
 '	 IN:			 Nothing
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

AS
		SELECT AreaID AS id,Name AS val
		FROM TBL_PDA_AREA		
		ORDER BY AreaType ASC

GO
