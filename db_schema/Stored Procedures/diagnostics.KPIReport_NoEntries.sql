SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [diagnostics].[KPIReport_NoEntries]

AS 

BEGIN 

DECLARE @Month INT = MONTH(DATEADD(m,-1,GETDATE()))
DECLARE @YearStartDate DATETIME
DECLARE @YearEndDate DATETIME

SELECT @YearStartDate = StartDate, @YearEndDate = EndDate FROM diagnostics.CurrentFiscalYear()

-- No Entries
SELECT CAST(COUNT(DISTINCT FaultLogId) AS INT) AS [No Entries]
FROM    dbo.FL_FAULT_nOeNTRY
WHERE   RecordedOn >= @YearStartDate
        AND RecordedOn <= @YearEndDate
        AND MONTH(RecordedOn) = @Month

END 
GO
