USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_LeaveRequestCount]    Script Date: 05/05/2016 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_LeaveRequestCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_LeaveRequestCount AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Raja Aneeq
-- Create date:      21/10/2015
-- Description:      Calculate alert count of LEAVE REQUEST on whiteboard  
-- exec  E_LeaveRequestCount 113
-- =============================================

ALTER PROCEDURE  [dbo].[E_LeaveRequestCount]
	@LineMgr INT 
	
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @lmid int, @totalCount int, @count int
	set @totalCount=0
	set @count=0

	DECLARE LM_cursor CURSOR FOR   
	SELECT Distinct LineManagerId  FROM FN_GetTeamHierarchyForEmployeeSickness(@LineMgr) where LineManagerId != @LineMgr

	OPEN LM_cursor
	
	FETCH NEXT FROM LM_cursor   
	INTO @lmid

	WHILE @@FETCH_STATUS = 0  
	BEGIN 
	if exists(
		select distinct emp.EMPLOYEEID
		from E__EMPLOYEE emp 
        inner join E_JOURNAL jnal on emp.EMPLOYEEID = jnal.EMPLOYEEID
        inner join E_STATUS es on jnal.CURRENTITEMSTATUSID = es.ITEMSTATUSID
        inner join E_NATURE en on jnal.ITEMNATUREID = en.ITEMNATUREID
        inner join E_ABSENCE abs on jnal.JOURNALID = abs.JOURNALID
		AND abs.ABSENCEHISTORYID = (
											SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
											WHERE JOURNALID = JNAL.JOURNALID AND
											emp.EMPLOYEEID = @lmid AND (es.DESCRIPTION = 'Absent' OR es.DESCRIPTION = 'Approved' OR es.DESCRIPTION = 'Completed')
											AND (
												(abs.STARTDATE <= getdate() AND abs.RETURNDATE > getdate())
												OR (en.DESCRIPTION = 'Sickness' AND abs.STARTDATE <= getdate() AND abs.RETURNDATE is null)
											)
											--AND E_ABSENCE.STARTDATE < GETDATE()
										  ) 
        where emp.EMPLOYEEID = @lmid AND (es.DESCRIPTION = 'Absent' OR es.DESCRIPTION = 'Approved' OR es.DESCRIPTION = 'Completed')
        AND (
			(abs.STARTDATE <= getdate() AND abs.RETURNDATE >= getdate()) 
			OR (en.DESCRIPTION = 'Sickness' AND abs.STARTDATE <= getdate() ANd abs.RETURNDATE is null)
			)
		)
		begin 
			Declare @empId int
			DECLARE Employee_cursor CURSOR FOR  
			select Distinct E.EMPLOYEEID
			from E__EMPLOYEE E 
			left join E_JOBDETAILS J on E.EMPLOYEEID = J.EMPLOYEEID
            left join E_TEAM T on J.TEAM = T.TEAMID 
            left join E_GRADE G on J.GRADE = G.GRADEID
            left join E_JOBROLE ET on J.JobRoleId = ET.JobRoleId 
            left join G_ETHNICITY ETH on E.ETHNICITY = ETH.ETHID 
            left join E_DOI DOI on E.EMPLOYEEID = DOI.EmployeeId 
            left join E_DOI_Status DOIs on DOI.Status = DOIs.DOIStatusId 
			where J.LINEMANAGER = @lmid and J.ACTIVE = 1

			OPEN Employee_cursor
	
			FETCH NEXT FROM Employee_cursor   
			INTO @empId

			WHILE @@FETCH_STATUS = 0  
			BEGIN 
				CREATE TABLE #employee(
					STARTDATE          Datetime,
					RETURNDATE  Datetime,
					Status   VARCHAR(50),
					Nature      VARCHAR(50),
					DAYS_ABS        Decimal,
					HOLTYPE        VARCHAR(20),   
					ABSENCEHISTORYID        int,   
					LastActionDate        Datetime,   
					DurationType       VARCHAR(50),   
				);

				insert into #employee (ABSENCEHISTORYID, Nature)
				EXEC [E_GetPendingAbsence] @empId
				
				IF exists( select *  from #employee)
				BEGIN 
					set @count = 1
					set @totalCount = @totalCount + @count 
				END
				
				DROP TABLE #employee
				--set @totalCount = @totalCount + @count 

				FETCH NEXT FROM Employee_cursor   
				INTO @empId
				                  
			END

			CLOSE Employee_cursor;  
			DEALLOCATE Employee_cursor;  
			
        

		end
		
		FETCH NEXT FROM LM_cursor   
		INTO @lmid
	END

	CLOSE LM_cursor;  
	DEALLOCATE LM_cursor;  
  
   
SELECT @count = COUNT(Leave)  From (
	SELECT 
	SUM(AHOL.DURATION) as Leave
	FROM E__EMPLOYEE E   
	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID   
	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM   
	LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID   
	LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND  
	JNAL.CURRENTITEMSTATUSID = 	5 AND  
	JNAL.ITEMNATUREID = 2   
	LEFT JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.STARTDATE >=  '1 '+ 'JAN' + ' ' + CAST(DATEPART (YYYY, GETDATE()) AS VARCHAR)  AND  
	A.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JNAL.JOURNALID)  
	LEFT JOIN (SELECT MAX(JOURNALID) JOURNALID ,EMPLOYEEID 
	FROM E_JOURNAL EJ 
	LEFT JOIN E_NATURE N ON EJ.ITEMNATUREID = N.ITEMNATUREID AND APPROVALREQUIRED = 1  WHERE CURRENTITEMSTATUSID = 3 GROUP BY EMPLOYEEID) JHOL ON J.EMPLOYEEID = JHOL.EMPLOYEEID  
	LEFT JOIN E_ABSENCE AHOL ON JHOL.JOURNALID = AHOL.JOURNALID
	 AND  AHOL.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JHOL.JOURNALID)  
	LEFT JOIN dbo.E_JOBROLE JR ON JR.JobRoleId = J.JobRoleId  
	WHERE J.LINEMANAGER = @LineMgr AND J.ACTIVE = 1 
	GROUP BY J.EMPLOYEEID, HOLIDAYENTITLEMENTDAYS, E.FIRSTNAME, E.LASTNAME,  JR.JobeRoleDescription,
	G.[DESCRIPTION],  
	T.TEAMNAME, E.EMPLOYEEID, J.APPRAISALDATE  
	HAVING  (SUM(AHOL.DURATION)) > 0

)  totalResult

set @totalCount = @totalCount + @count    

select @totalCount
as LeavesCount




END
