SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE PROCEDURE dbo.TO_HOUSE_MOVE_AddRequest
/* ===========================================================================
 '   NAME:           TO_HOUSE_MOVE_AddRequest
 '   DATE CREATED:   12 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add move house request record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 move house record in TO_HOUSE_MOVE
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '   IN:             @MoveToTown
 '   IN:             @StreetName
 '   IN:             @NoOfBedrooms
 '   IN:             @OccupantsNoGreater18
 '   IN:             @OccupantsNoBelow18
 '
 '   OUT:            @TransferID
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(4000),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	
	@LocalAuthorityID NVARCHAR(50),
	@DevelopmentID NVARCHAR(50),
	@NoOfBedrooms INT,
	@OccupantsNoGreater18 INT,
	@OccupantsNoBelow18 INT,
	@TransferID INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_HOUSE_MOVE(
		LocalAuthorityID,
		DevelopmentID,
		NoOfBedRooms,
		OccupantsNoGreater18,
		OccupantsNoBelow18,
		EnquiryLogID

		)
		VALUES(
		@LocalAuthorityID,
		@DevelopmentID,
		@NoOfBedrooms,
		@OccupantsNoGreater18,
		@OccupantsNoBelow18,
		@EnquiryLogID
		)
		
		SELECT @TransferID=TransferID
	FROM TO_HOUSE_MOVE
	WHERE TransferID = @@IDENTITY

				IF @TransferID<0
					BEGIN
						SET @TransferID=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @TransferID=-1
			ROLLBACK TRAN
			END
GO
