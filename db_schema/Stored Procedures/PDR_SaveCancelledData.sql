USE [RSLBHALive]

-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <3/11/2015>
-- Description:	<Save Cancelled Faults>
-- =============================================

IF OBJECT_ID('dbo.PDR_SaveCancelledData') IS NULL 
 EXEC('CREATE PROCEDURE dbo.PDR_SaveCancelledData AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[PDR_SaveCancelledData]
	@FaultLogId int,
	@orderId int	
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @statusId int
	SELECT	@statusId =  faultStatusId 
	FROM	FL_Fault_STATUS
	WHERE	Description ='Cancelled'

	UPDATE	FL_FAULT_LOG 
	SET		statusId = @statusId
	WHERE	faultLogId =@FaultLogID

	UPDATE F_PURCHASEORDER 
	SET POSTATUS = @statusId 
	WHERE OrderID = @orderId
	
END
