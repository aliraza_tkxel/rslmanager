USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyDetailByPropertyId]    Script Date: 10/13/2015 19:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property Detail By PropertyId for Template
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Detail By PropertyId for Template
    v1.1		Oct-13-2015		Raja Aneeq			Add a  new column 'TenureType'
    Execution Command:
    
    Exec PDR_GetPropertyDetailByPropertyId P10040000A
  =================================================================================*/

IF OBJECT_ID('dbo.[PDR_GetPropertyDetailByPropertyId]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[PDR_GetPropertyDetailByPropertyId] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[PDR_GetPropertyDetailByPropertyId]
(@PropertyId varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT P.PROPERTYID,PATCH,DEVELOPMENTID,P.BLOCKID,HOUSENUMBER,FLATNUMBER,ADDRESS1,ADDRESS2,ADDRESS3,TOWNCITY,COUNTY,POSTCODE,STATUS,SUBSTATUS,AVDATE,PROPERTYTYPE, ASSETTYPE,STOCKTYPE,
PROPERTYLEVEL,DATEBUILT,VIEWINGDATE,LETTINGDATE,RIGHTTOBUY,UNITDESC,PRACTICALCOMPLETIONDATE, HOUSINGOFFICER, DEFECTSPERIODEND,DATETIMESTAMP,SAP,OWNERSHIP,MINPURCHASE,PURCHASELEVEL,
DWELLINGTYPE, NROSHASSETTYPEMAIN, NROSHASSETTYPESUB, FUELTYPE, PropertyPicId, P.SCHEMEID,PROPERTYVALUE,IsTemplate ,TemplateName,PhaseId,LeaseStart,LeaseEnd,FloodingRisk,DateAcquired,
viewingcommencement,Valuationdate,GroundRent,LandLord,RentReview,TitleNumber,DeedLocation,TENURETYPE, PV.ValueID,PV.ValueDetail
FROM P__PROPERTY P 
LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P.PROPERTYID
AND A.ITEMPARAMID =
	(
		SELECT ItemParamID FROM PA_ITEM_PARAMETER
		INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
		INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
		WHERE ParameterName = 'Heating Fuel' AND ItemName = 'Heating' and PA_ITEM.IsActive=1
	)
LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID

Where P.PROPERTYID = @PropertyId
END
