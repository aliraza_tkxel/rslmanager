SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_TERMINATION_SelectTerminationDetail]

/* ===========================================================================
 '   NAME:           TO_TERMINATION_SelectTermination
 '   DATE CREATED:   11 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select one TO_TERMINATION record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
            TO_TERMINATION.MovingOutDate
                        
	FROM
	       TO_ENQUIRY_LOG INNER JOIN TO_TERMINATION 
	       ON TO_ENQUIRY_LOG.EnquiryLogID = TO_TERMINATION.EnquiryLogID
	       	       
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)	     
	     


GO
