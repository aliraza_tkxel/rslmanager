SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_getDefectImages @propertyDefectId = 1
-- Author:		<Salman Nazir>
-- Create date: <16/11/2012>
-- Description:	<Get all images related to defact>
-- WebPage: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_getDefectImages]
	@propertyDefectId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
	WHERE PropertyDefectId = @propertyDefectId
END
GO
