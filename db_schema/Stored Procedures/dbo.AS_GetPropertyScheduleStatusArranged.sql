USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetApplianceType]    Script Date: 18-Jan-17 2:26:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetApplianceType
-- Author:		<Noor Muhammad>
-- Create date: <02/11/2012>
-- Description:	<Get all appliance types>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
IF OBJECT_ID('dbo.[AS_GetPropertyScheduleStatusArranged]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetPropertyScheduleStatusArranged] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].AS_GetPropertyScheduleStatusArranged
(@pId varchar(100),
@appointmentType varchar(100),
@isScheduled int out,
@isTimeExceeded int out,
@ScheduleStatus int out )  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @count int
	if @appointmentType = 'Property'
	begin
		set @count=(SELECT  count(*)
		FROM AS_JOURNAL
		WHERE AS_JOURNAL.PropertyId=@pId
		AND AS_JOURNAL.ISCURRENT=1
		and (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE 'Arranged')))

		if(@count=0)
		begin
			SELECT @ScheduleStatus = 0, @isScheduled=1, @isTimeExceeded = 1
		end
		else
		begin
		   SELECT @ScheduleStatus = AS_JOURNAL.isLock, @isScheduled=AS_JOURNAL.isScheduled, @isTimeExceeded = CASE WHEN AS_JOURNAL.LockTime  IS NULL THEN 0 ELSE (CASE WHEN DATEDIFF(minute,AS_JOURNAL.LockTime, GETDATE()) >= 5 THEN 1 ELSE 0 END) END 
			FROM AS_JOURNAL
			WHERE AS_JOURNAL.PropertyId=@pId
			AND AS_JOURNAL.ISCURRENT=1
			and (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE 'Arranged'))
		end
	end
	else if @appointmentType = 'Scheme'
	begin
		set @count =(SELECT  count(*)
		FROM AS_JOURNAL
			WHERE AS_JOURNAL.SchemeID=@pId
		AND AS_JOURNAL.ISCURRENT=1
		and (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE 'Arranged')))

		if(@count=0)
		begin
			SELECT @ScheduleStatus = 0, @isScheduled=1, @isTimeExceeded = 1
		end
		else
		begin
		   SELECT @ScheduleStatus = AS_JOURNAL.isLock, @isScheduled=AS_JOURNAL.isScheduled, @isTimeExceeded = CASE WHEN AS_JOURNAL.LockTime  IS NULL THEN 0 ELSE (CASE WHEN DATEDIFF(minute,AS_JOURNAL.LockTime, GETDATE()) >= 5 THEN 1 ELSE 0 END) END 
			FROM AS_JOURNAL
			WHERE AS_JOURNAL.SchemeID=@pId
			AND AS_JOURNAL.ISCURRENT=1
			and (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE 'Arranged'))
		end
	end
	else if @appointmentType = 'Block'
	begin
		set @count =(SELECT  count(*)
		FROM AS_JOURNAL
			WHERE AS_JOURNAL.BlockId=@pId
		AND AS_JOURNAL.ISCURRENT=1
		and (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE 'Arranged')))

		if(@count=0)
		begin
			SELECT @ScheduleStatus = 0, @isScheduled=1, @isTimeExceeded = 1
		end
		else
		begin
		   SELECT @ScheduleStatus = AS_JOURNAL.isLock, @isScheduled=AS_JOURNAL.isScheduled, @isTimeExceeded = CASE WHEN AS_JOURNAL.LockTime  IS NULL THEN 0 ELSE (CASE WHEN DATEDIFF(minute,AS_JOURNAL.LockTime, GETDATE()) >= 5 THEN 1 ELSE 0 END) END 
			FROM AS_JOURNAL
			WHERE AS_JOURNAL.BlockId=@pId
			AND AS_JOURNAL.ISCURRENT=1
			and (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE 'Arranged'))
		end
	end
    

END
