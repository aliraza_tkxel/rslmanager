SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- YEARLY_DDUPDATE @CUTOFFDATE = '2 MARCH 2008', @REASSESMENTDATE = '1 APRIL 2008'
CREATE PROCEDURE [dbo].[YEARLY_DDUPDATE] (@CUTOFFDATE SMALLDATETIME,@REASSESMENTDATE SMALLDATETIME ) AS

DECLARE @DDSCEHDULEID INT
DECLARE @REGULARPAYMENT MONEY
DECLARE @COUNTER INT

SET @COUNTER = 0

SET NOCOUNT ON

DECLARE DDSCHEDULE CURSOR 
	KEYSET
	FOR 
	SELECT 	D.DDSCHEDULEID, TR.RENT + TR.SERVICES + TR.COUNCILTAX + TR.WATERRATES + 
		TR.INELIGSERV + TR.SUPPORTEDSERVICES + ISNULL(NEWGARAGECHARGE,0) 
	FROM P_TARGETRENT TR
				INNER JOIN P_FINANCIAL F ON F.PROPERTYID = TR.PROPERTYID
					AND F.DATERENTSET <= @CUTOFFDATE
				INNER JOIN C_TENANCY TN ON TN.PROPERTYID = TR.PROPERTYID
					AND TN.STARTDATE <= @CUTOFFDATE 
					AND TN.ENDDATE IS NULL
				INNER JOIN C_CUSTOMERTENANCY C ON C.TENANCYID = TN.TENANCYID
				INNER JOIN F_DDSCHEDULE D ON D.CUSTOMERID = C.CUSTOMERID AND SUSPEND = 0
			WHERE ACTIVE = 1 AND REASSESMENTDATE = @REASSESMENTDATE
				AND	TR.OLD_RENT + TR.OLD_SERVICES + TR.OLD_COUNCILTAX + TR.OLD_WATERRATES + 
				TR.OLD_INELIGSERV + TR.OLD_SUPPORTEDSERVICES + ISNULL(OLD_ADLASSETTOTAL,0) = TR.DDREGULARPAYMENT
	

	OPEN DDSCHEDULE
	FETCH FIRST FROM DDSCHEDULE
	   INTO @DDSCEHDULEID,@REGULARPAYMENT
	WHILE @@FETCH_STATUS = 0
	BEGIN

		UPDATE F_DDSCHEDULE SET REGULARPAYMENT = @REGULARPAYMENT, LASTTRANSACTIONTYPE = 4 WHERE DDSCHEDULEID = @DDSCEHDULEID
		
		SET @COUNTER = @COUNTER + 1

	    -- GET NEXT
	 FETCH NEXT FROM DDSCHEDULE
	 INTO @DDSCEHDULEID,@REGULARPAYMENT
	
	END
CLOSE DDSCHEDULE
DEALLOCATE DDSCHEDULE


PRINT 'THE TOTAL NUMBER OFF DD UPDATE IS ' + CAST(@COUNTER AS NVARCHAR)
GO
