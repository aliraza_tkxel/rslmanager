
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*	===============================================================
TO EXECUTE THIS PROCEDURE, USE THE FOLLOWING STATEMENTS

BEGIN TRAN
DECLARE @CUSTOMERID INT
DECLARE @ERRMESSAGE VARCHAR(8000)
EXEC TO_CUSTOMER_REGISTRATION 'DONNA','ABBS',872539,'A@A.COM','NR1 4FD','REIDMARK','19740213',@CUSTOMERID=@CUSTOMERID OUTPUT, @ERRMESSAGE=@ERRMESSAGE OUTPUT
PRINT @CUSTOMERID
PRINT @ERRMESSAGE
ROLLBACK

'===============================================================*/



CREATE PROCEDURE [dbo].[TO_Customer_Registration]
	/*	===============================================================
	'   NAME:           TO_Customer_Registrations
	'   DATE ALTERD:    23 MAY 2008
	'   ALTERD BY:      Naveed Iqbal
	'   ALTERD FOR:     Broadland Housing
	'   PURPOSE:        To verify the Customer data for registration process and store given password
	'   IN:             @CustomerFirstName 
	'   IN:             @CustomerLastName 
	'   IN:             @CustomerTenancyId 
	'   IN:             @CustomerEmail
	'   IN:             @CustomerPostcode 
	'   IN:             @Password 
	'   OUT: 		    @CustomerId,@ErrMessage     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    31/05/2013
	'   MODIFIED BY:    Nataliya Alexander
	'   REASON MODIFICATION: Removed update to the dob. Removed checking tenancy id on both 
	'                      : tenancy id and customer first name. Updated error message to remove specific details
	'                      : and add a more generic message. Use date of birth as another element to verify customer
	'                      : identity.
	'===============================================================*/

	(
		@CustomerFirstName NVARCHAR(50),
		@CustomerLastName NVARCHAR(50),
		@CustomerTenancyId INT,
		@CustomerEmail NVARCHAR(50),
		@CustomerPostcode NVARCHAR(20),
		@Password NVARCHAR(50),
		@dob smalldatetime,
		@CustomerId INT OUTPUT,
		@ErrMessage VARCHAR(8000) OUTPUT
	)
	
AS
DECLARE 
@TempEmail			NVARCHAR(50),
@TempPostCode		NVARCHAR(20),
@TempFirstName		NVARCHAR(50),
@TempLastName		NVARCHAR(50),
@TempDob			DATETIME,
@TempErrorMessage	VARCHAR(8000)


SET @TempErrorMessage = '<br />Sorry we couldn''t complete your registration for Tenants Online. Please check your details and try again. Alternatively, if you''re sure the details you entered are correct, please <a class="link_item" href="http://www.broadlandhousing.org/site/contact_us.php">contact us</a>.<br />'

SELECT @CustomerId=CT.CUSTOMERID 
FROM C__CUSTOMER C
INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID=C.CUSTOMERID AND CT.ENDDATE IS NULL AND CT.TENANCYID=@CustomerTenancyId

IF(@CustomerId IS NULL) 
	BEGIN
		SET @CustomerId=-1
		SET @ErrMessage = @TempErrorMessage
		RETURN
	END
ELSE 
BEGIN

	SELECT @TempFirstName=FirstName,@TempLastName=LastName,@TempDob=DOB FROM C__CUSTOMER WHERE CUSTOMERID=@CustomerId

	SELECT  @TempEmail=ISNULL(EMail,''),@TempPostCode=PostCode FROM C_ADDRESS WHERE CUSTOMERID=@CustomerId AND ISDEFAULT=1 

	IF (@TempFirstName<>@CustomerFirstName)
	BEGIN
			SET @CustomerId=-1
			SET @ErrMessage = @TempErrorMessage
	END

	IF (@TempLastName<>@CustomerLastName)
	BEGIN
			SET @CustomerId=-1
			SET @ErrMessage = @TempErrorMessage
	END

	IF (@TempEmail<>@CustomerEmail)
	BEGIN
			SET @CustomerId=-1
			SET @ErrMessage = @TempErrorMessage
	END

	IF (REPLACE(@TempPostCode, ' ', '')<>REPLACE(@CustomerPostcode, ' ', ''))
	BEGIN
			SET @CustomerId=-1
			SET @ErrMessage = @TempErrorMessage
	END

	IF (CAST(@TempDob AS DATE) <> CAST(@dob AS DATE))
	BEGIN
			SET @CustomerId=-1
			SET @ErrMessage = @TempErrorMessage
	END

	IF (@CustomerId <> -1)
	BEGIN
		DECLARE @LoginCustomerId INT
		SELECT @LoginCustomerId=CUSTOMERID 
		FROM TO_LOGIN 
		WHERE CUSTOMERID=@CustomerId

		IF(@LoginCustomerId IS NOT NULL)
		BEGIN
			SET @CustomerId=-2
			SET @ErrMessage = '<br />The email address "' + @TempEmail + '" has already been registered.<br /><br /><a class="link_item" href="password_assistance.aspx">Forgotten your password?</a><br />'
		END
		ELSE
			BEGIN
				INSERT INTO TO_LOGIN(
							CUSTOMERID,
							PASSWORD,
							ACTIVE
						)
				VALUES(
							@CustomerId,
							@Password,
							1
						)
			END
	END
END

SET QUOTED_IDENTIFIER ON 



GO
