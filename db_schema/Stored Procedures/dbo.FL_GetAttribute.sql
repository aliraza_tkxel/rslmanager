USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[FL_GetAttribute]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[FL_GetAttribute] AS SET NOCOUNT ON;') 
GO
/* =================================================================================      
    Page Description: Get Attribute for Dop down list over scheme/Block Assign to contractor PopUp   
   
    Author: Ali Raza  
    Creation Date: Feb-1-2015
	Updated Date: July-27-2016  
  
    Change History:  
  
    Version      Date             By                      Description  
    =======     ============    ========           ===========================  
    v1.0         Feb-1-2015     Ali Raza           Get Attribute for Dop down list over scheme/Block Assign to contractor PopUp  
	v1.1         July-27-2016   Junaid Nadeem      Get Attribute for Dop down list over scheme/Block Assign to contractor PopUp  
      
    Execution Command:  
      
    Exec FL_GetAttribute 6  
  =================================================================================*/  
Alter PROCEDURE [dbo].[FL_GetAttribute]  
 -- Add the parameters for the stored procedure here  
 @areaId int   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
  
WITH CTE AS
(
 SELECT   PA_ITEM.ItemID , PA_ITEM.ItemName,PA_ITEM.AreaID,ItemSorder ,ParentItemId ,AreaName 
 FROM PA_AREA   
 INNER JOIN PA_ITEM ON PA_ITEM.AreaID = PA_AREA.AreaID   
 WHERE PA_AREA.IsActive = 1 AND ParentItemId IS NULL AND PA_ITEM.IsActive = 1 AND ShowListView = 0 and PA_AREA.AreaID=@areaId
  
  UNION ALL
  
  SELECT  PA_ITEM.ItemID,PA_ITEM.ItemName, PA_ITEM.AreaID,PA_ITEM.ItemSorder,PA_ITEM.ParentItemId,PA_AREA.AreaName  FROM PA_ITEM 
  INNER JOIN CTE ON PA_ITEM.ParentItemId = CTE.ItemID
  INNER JOIN PA_AREA ON PA_ITEM.AreaID = PA_AREA.AreaID 
 WHERE PA_ITEM.IsActive = 1 AND PA_AREA.IsActive = 1 AND ShowListView = 0 and PA_AREA.AreaID=@areaId
)

SELECT CTE.ItemID,
CASE WHEN CTE.ParentItemId IS NULL then P.ItemName
ELSE  P.ItemName END AS ItemName
 
 FROM PA_ITEM P
INNER JOIN CTE ON P.ItemID = CTE.ItemID
Left OUTER JOIN PA_ITEM parent on CTE.ParentItemId = parent.ItemID
WHERE P.IsActive = 1 AND P.ShowListView = 0 

AND P.ItemID NOT IN  (
SELECT DISTINCT I.ItemID FROM PA_ITEM I
Cross APPLY (SELECT ItemId FROM PA_ITEM pitem WHERE I.ItemID = pitem.ParentItemId AND I.ItemName NOT IN ('Kitchen','Bathroom') )parent

 )
 ORDER BY CTE.AreaID,CTE.ItemID,CTE.ItemSorder ASC
  
END  