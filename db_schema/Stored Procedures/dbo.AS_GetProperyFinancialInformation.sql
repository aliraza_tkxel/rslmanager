
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC	[dbo].[AS_GetProperyFinancialInformation] 'BHA0000727'
-- Author:		<Aqib Javed>
-- Create date: <18-Apr-2014>
-- Description:	<This procedure 'll get the Rent Frequency,Funding Authority, Charge and Rent Type information >
-- Web Page: FinancialTab.ascx
CREATE PROCEDURE [dbo].[AS_GetProperyFinancialInformation] 
@propertyId varchar(50)		
AS
BEGIN
	Select FID, [DESCRIPTION] from P_RENTFREQUENCY order by [DESCRIPTION]
	Select FUNDINGAUTHORITYID, [DESCRIPTION] FROM P_FUNDINGAUTHORITY order by [DESCRIPTION]
	Select CHARGEID, [DESCRIPTION] from P_CHARGE order by [DESCRIPTION]	
	--Add Yield Column for Ticket #6727 
	Select RENTFREQUENCY,FUNDINGAUTHORITY,NOMINATINGBODY,INSURANCEVALUE,CHARGEVALUE,OMVST,EUV,OMV,CHARGE,ISNULL(YIELD,0.000)as YIELD from P_Financial where PropertyId=@propertyId
END
GO
