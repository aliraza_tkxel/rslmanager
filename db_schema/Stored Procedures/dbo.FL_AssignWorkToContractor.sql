USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[FL_AssignWorkToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[FL_AssignWorkToContractor] AS SET NOCOUNT ON;') 
GO  
-- Stored Procedure  
  
-- =============================================  
-- Author:  Aamir Waheed  
-- Create date: 12/06/2014  
-- Description: Assign Work to Contractor, it can be either planned work or misc. work or conditional work.  
-- =============================================  
Alter PROCEDURE [dbo].[FL_AssignWorkToContractor]   
 -- Add the parameters for the stored procedure here  
 @contactId INT,  
 @contractorId INT,  
 @AreaId INT,  
 @AttributeId INT,  
 @userId int,  
 @Estimate SMALLMONEY,  
 @EstimateRef NVARCHAR(200) = '',    
 @POStatus INT,  
 @schemeId INT,  
 @blockId INT,  
 @journalId INT ,  
 @ContractorWorksDetail AS FL_AssingToContractorWorksRequired READONLY,  
 @isSaved BIT = 0 OUTPUT,  
 @POId int OUTPUT,
 @journalIdOut INT OUTPUT  
  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  
  
  
/* Working of this stored Procedure  
** 1- Get Status Id of "Assigned To Contractor" from Planned_Status. In case a Status of "Assigned To Contractor"   
**    is not present add a new one and get its Id.  
**  
** 2- Get Status History Id for "Assigned To Contractor" PLANNED_StatusHistory, for the case a Status of "Assigned To Contractor"  
**  
** 3- Set Planned_Journal Status to "Assigned To Contractor"'s Status Id, if Journal Id/PMO is -1 (miscellaneous appointment case)  
**    then insert a new record in Planned_journal  
**  
** 4- Insert a new record in F_PURCHASEORDER and get the identity value as Order Id.  
**  
** 5- Insert a new record in PLANNED_CONTRACTOR_WORK using the given input data and OrderId  
**    and get Identity Value as PlannedContractorId.  
**  
** Loop (Insert Purchase Items and Works Required Items.  
**  
**   6- Insert a Purchase Item in F_PURCHASEITEM from given an constant data  
**      and get Identity Value PURCHASEORDERITEMID  
**  
**   7- Insert a new work required from given data and also insert PURCHASEORDERITEMID  
**  
** End Loop  
*/  
  
IF (@AreaId <= 0)  
BEGIN  
 SET @AreaId = NULL  
END  
  
--=================================================  
-- Get CustomerId to get Tenant Detail(s) and Risk  
--=================================================  
  
DECLARE @customerId INT = NULL  
  
BEGIN TRANSACTION  
BEGIN TRY  
  
-- =====================================================  
-- General Purpose Variable  
-- =====================================================  
  
-- To save same time stamp in all records   
DECLARE @CurrentDateTime AS datetime2 = GETDATE()  
  
-- =====================================================  
-- General Purpose Variable  
-- =====================================================  
    
DECLARE @DevelopmentId INT = NULL    
  
SELECT @DevelopmentId = S.DEVELOPMENTID    
FROM P_SCHEME S    
WHERE S.SCHEMEID = @schemeId  
  
--================================================================================  
--Get Status Id for Status Title "Assigned To Contractor"  
--In case (for first time) it does not exists Insert it and get Status Id.  
  
-- Variables to get Status Id and Status History Id  
DECLARE @newStatusHistoryId int = NULL  
DECLARE @newStatusId int = NULL  
-- =====================================================  
-- Insert new Purchase Order  
-- =====================================================  
  
DECLARE @Active bit = 1  
, @POTYPE int = (SELECT  
 POTYPEID  
FROM F_POTYPE  
WHERE POTYPENAME = 'Repair') -- 2 = 'Repair'  
  
-- To get Identity Value of Purchase Order.  
, @purchaseOrderId int  
  
INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,  
POTYPE, POSTATUS, GASSERVICINGYESNO, POTIMESTAMP, BLOCKID, DEVELOPMENTID)  
 VALUES (UPPER('Scheme/Block Reactive Repair Work Order'), @CurrentDateTime, 'This purchase order was created for Scheme/Block Reactive Repair.', @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0, @CurrentDateTime, @blockId, @DevelopmentId)  
  
SET @purchaseOrderId = SCOPE_IDENTITY()  

-- =====================================================  
-- Old table(s)  
-- Insert new P_WORKORDER  
-- =====================================================  
DECLARE @Title nvarchar(50) = 'Assigned To Contractor'  
DECLARE @WOSTATUS INT = 6-- 'IN PROGRESS  
If (@POStatus = 0) SET @WOSTATUS = 12 --Pending  
DECLARE @BIRTH_NATURE INT = 60 --'PLANNED MAINTENANCE'  
DECLARE @BIRTH_ENTITY INT = NULL --'PLANNED MAINTENANCE PROGRAMME CAN BE CREATED FOR MULTIPLE PATCHES AND SCHEMES'  
DECLARE @BIRTH_MODULE INT = 5 --'PLANNED MAINTENANCE'  
  
INSERT INTO P_WORKORDER (ORDERID, TITLE, WOSTATUS, CREATIONDATE, BIRTH_MODULE, BIRTH_ENTITY,  
       BIRTH_NATURE, DEVELOPMENTID, BLOCKID, GASSERVICINGYESNO, SchemeId)  
  VALUES(@purchaseOrderId, @TITLE, @WOSTATUS, @CurrentDateTime, @BIRTH_MODULE, @BIRTH_ENTITY  
    , @BIRTH_NATURE, @DevelopmentId, @blockId, 0, @schemeId)  
  
DECLARE @WOID INT = SCOPE_IDENTITY()  
  
-- =====================================================  
-- Insert new PLANNED_CONTRACTOR_WORK  
-- =====================================================  
  
DECLARE @faultContractorId int  
  
INSERT INTO FL_CONTRACTOR_WORK (JournalId, ContractorId, AssignedDate, AssignedBy  
, Estimate, EstimateRef, AreaId, ItemId, ContactId, PurchaseORDERID)  
 VALUES (@journalId,@ContractorId,@CurrentDateTime,@userId,@Estimate,@EstimateRef, @AreaId, @AttributeId, @contactId, @PurchaseOrderId)  
  
SET @faultContractorId = SCOPE_IDENTITY()  
  
  
-- =====================================================  
-- Declare a cursor to enter works requied,  
--  loop through record and instert in table  
-- =====================================================  
  
DECLARE worksRequiredCursor CURSOR FOR SELECT  
 *  
FROM @ContractorWorksDetail  
OPEN worksRequiredCursor  
  
-- Declare Variable to use with cursor  
DECLARE @WorksRequired nvarchar(4000),  
@NetCost smallmoney,  
@VatType int,  
@VAT smallmoney,  
@GROSS smallmoney,  
@PIStatus int,  
@ExpenditureId int  
  
-- Variable used within loop  
DECLARE @PurchaseItemTITLE nvarchar(20) = 'Scheme/Block Reactive Repair' -- Title for Purchase Items, specially to inset in F_PurchaseItem  
  
-- =====================================================  
-- Loop (Start) through records and insert works required  
-- =====================================================    
  -- Fetch record for First loop iteration.  
  FETCH NEXT FROM worksRequiredCursor INTO @WorksRequired, @NetCost, @VatType, @VAT,  
  @GROSS, @PIStatus, @ExpenditureId  
  WHILE @@FETCH_STATUS = 0 BEGIN  
  

   -- =====================================================  
  -- Old table(s)  
  -- INSERT VALUE IN C_JOURNAL FOR EACH PROPERTY SELECTED ON WORKORDER  
  -- =====================================================  
  
  DECLARE @ITEMID INT = 1 --'PROPERTY'  
  DECLARE @STATUS INT = 2 --'ASSIGNED  
  If (@POStatus = 0) SET @STATUS = 12 --Pending  
  
  
  INSERT INTO C_JOURNAL ( CUSTOMERID, ITEMID,ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE)  
  VALUES(@CustomerId, @ITEMID, @BIRTH_NATURE, @STATUS, @CurrentDateTime,@TITLE)  
  
  DECLARE @CJOURNALID INT = SCOPE_IDENTITY()  

   -- =====================================================  
  -- Old table(s)  
  --INSERT VALUE IN C_REPAIR FOR EACH PROPERTY SELECTED ON WORKORDER  
  -- =====================================================  
  
  DECLARE @ITEMACTIONID INT = 2 --'ASSIGNED TO CONTRACTOR'  
  
  If (@POStatus = 0) SET @ITEMACTIONID = 12 --Pending  
  
  
  --ITEMDETAILID WILL BE NULL BECAUSE THE COST OF WORKORDER DEPENDS ON PROGRAMME OF PLANNED MAINTENANCE   
  --AND THE SCOPEID WILL ALSO BE NULL BECAUSE IT IS NOT A GAS SERVICING CONTRACT  
  
  INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE,LASTACTIONUSER, ITEMDETAILID,  
         CONTRACTORID, TITLE, NOTES,SCOPEID)   
  VALUES(@CJOURNALID, @STATUS, @ITEMACTIONID, @CurrentDateTime, @userId,NULL,@CONTRACTORID,'FOR Scheme/Block',@TITLE,NULL)  
  
  DECLARE @REPAIRHISTORYID INT = SCOPE_IDENTITY()  





  -- =====================================================  
  --Insert Values in F_PURCHASEITEM for each work required and get is identity value.  
  -- =====================================================  
  
  INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,  
  NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)  
   VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @WorksRequired,   
   @CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)  
  
  DECLARE @ORDERITEMID int = SCOPE_IDENTITY()  
  

   -- =====================================================  
  --INSERT VALUE IN P_WOTOREPAIR for each work required  
  -- =====================================================  
  
  INSERT INTO P_WOTOREPAIR(WOID, JOURNALID, ORDERITEMID)  
  VALUES(@WOID,@CJOURNALID,@ORDERITEMID) 


  -- =====================================================  
  -- Insert values in FL_CONTRACTOR_WORK_DETAIL for each work required  
  -- =====================================================  
  
  INSERT INTO FL_CONTRACTOR_WORK_DETAIL(FaultContractorId, WorkRequired, NetCost  
   , VatId, Vat, Gross, ExpenditureId, PURCHASEORDERITEMID)  
  VALUES(@faultContractorId, @WorksRequired, @NetCost, @VatType, @VAT, @GROSS  
    , @ExpenditureId,@ORDERITEMID)  
  
-- Fetch record for next loop iteration.  
FETCH NEXT FROM worksRequiredCursor INTO @WorksRequired, @NetCost, @VatType, @VAT,  
@GROSS, @PIStatus, @ExpenditureId  
END  
  
-- =====================================================  
-- Loop (End) through records and insert works required  
-- =====================================================  
  
-- =====================================================  
-- If PO is queue set work order status to 12 (queued) as set in Portfolio Work Order  
-- =====================================================  
  
IF @POStatus = 0  
 BEGIN  
  UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = @WOID  
 END  
ELSE  
 BEGIN  
 -- - - -  - - - - - - -  -  
 -- AUTO ACCEPT REPAIRS  
 -- - ---- - - - -  - - - -  
 -- we check to see if the org has an auto accept functionality =  1  
 -- if so then we auto accept the repair  
  EXEC C_REPAIR_AUTO_ACCEPT @ORDERID = @PurchaseOrderId, @SUPPLIERID =  @ContractorId  
END  
  
  
CLOSE worksRequiredCursor  
DEALLOCATE worksRequiredCursor  
  
-- =====================================================  
--INSERT VALUE IN PM_WORKORDER_CONTRACT TABLE TO CONNECT IT TO THE PM PROPERTIES -- Old RSL PLANNED MAINTANANCE module  
-- =====================================================  
--IF @PROPLISTID IS NOT NULL  
--BEGIN  
-- INSERT INTO PM_WORKORDER_CONTRACT (WORKORDERID, PROPERTYLISTID)  
-- VALUES(@WOID,@PROPLISTID)  
--END  
  
END TRY  
BEGIN CATCH   
 IF @@TRANCOUNT > 0  
 BEGIN       
  ROLLBACK TRANSACTION;     
  SET @isSaved = 0          
 END  
 DECLARE @ErrorMessage NVARCHAR(4000);  
 DECLARE @ErrorSeverity INT;  
 DECLARE @ErrorState INT;  
  
 SELECT @ErrorMessage = ERROR_MESSAGE(),  
 @ErrorSeverity = ERROR_SEVERITY(),  
 @ErrorState = ERROR_STATE();  
  
 -- Use RAISERROR inside the CATCH block to return   
 -- error information about the original error that   
 -- caused execution to jump to the CATCH block.  
 RAISERROR (@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState -- State.  
   );  
END CATCH;  
  
IF @@TRANCOUNT > 0  
 BEGIN    
  COMMIT TRANSACTION;    
  SET @isSaved = 1  
 END  
  
SET @journalIdOut = @journalId  
SET @POId = @purchaseOrderId
END  