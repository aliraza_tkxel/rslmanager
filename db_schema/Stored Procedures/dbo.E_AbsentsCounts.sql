USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_AbsentsCounts]    Script Date: 27/11/2018 13:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Raja Aneeq
-- Create date:      21/10/2015
-- Description:      Calculate alert count of Absentees on whiteboard 
-- exec  E_AbsentsCounts 760
-- =============================================

IF OBJECT_ID('dbo.E_AbsentsCounts') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_AbsentsCounts AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_AbsentsCounts]
	@LineMgr INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 SELECT COUNT(E.EMPLOYEEID) AS AbsentsCount
 FROM 	E__EMPLOYEE E 	
 INNER JOIN E_JOURNAL J ON E.EMPLOYEEID = J.EMPLOYEEID   
 INNER JOIN E_JOBDETAILS JD ON  E.EMPLOYEEID = JD.EMPLOYEEID
 AND J.ITEMNATUREID = 1 AND CURRENTITEMSTATUSID = 1 
 WHERE 	JD.Linemanager = @LineMgr And JD.ACTIVE = 1
	
END
