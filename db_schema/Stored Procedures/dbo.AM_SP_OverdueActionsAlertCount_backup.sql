SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AM_SP_OverdueActionsAlertCount_backup]
	@caseOwnedById int=0
AS
	BEGIN

		SELECT COUNT(*) 
			FROM  AM_CaseHistory INNER JOIN
				  AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
				 -- AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
				  AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
				--  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
				 -- C_CUSTOMERTENANCY ON AM_Case.TenancyId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
				 -- C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
				  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId
   
----			WHERE AM_Case.IsActive = 1 and (AM_Case.CaseOfficer = case when @caseOwnedById= -1 then AM_Case.CaseOfficer else @caseOwnedById end)
----				  and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_Case.ActionRecordeddate ) = 'true'

 WHERE AM_CaseHistory.IsActive = 1 
	and (AM_CaseHistory.CaseOfficer = (case when -1 = @caseOwnedById THEN AM_CaseHistory.CaseOfficer ELSE @caseOwnedById  END))
	and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = 'true' 
	and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId)

END




GO
