-- =============================================    
--EXEC AS_SaveItemNotes    
--  @propertyId = N'A010060001',    
--  @itemId = 1,    
--  @note = N'New',    
--  @createdBy = 1    
-- Author:  <Author,,Ali Raza>    
-- Create date: <Create Date,,10/30/2013>    
-- Description: <Description,,Save the Note for property and Item>    
-- WebPage: PropertyRecord.aspx => Attributes Tab    
-- =============================================    
IF OBJECT_ID('dbo.AS_SaveItemNotes') IS NULL 
EXEC('CREATE PROCEDURE dbo.AS_SaveItemNotes AS SET NOCOUNT ON;') 
GO  
ALTER PROCEDURE [dbo].[AS_SaveItemNotes] (    
@propertyId varchar(500),    
@schemeId int=null,     
@blockId int=null,     
@itemId int,    
@note varchar(500),  
@showInScheduling bit=1,  
@showOnApp bit=0,    
@createdBy int,
@heatingMappingId int = null     
)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
 INSERT INTO PA_PROPERTY_ITEM_NOTES     
 ([PROPERTYID],[ItemId],[Notes] ,[CreatedOn],[CreatedBy],SchemeId,BlockId,ShowInScheduling,ShowOnApp,HeatingMappingId)    
 VALUES    
 (@propertyId,@itemId,@note ,GETDATE(),@createdBy,@schemeId,@blockId,@showInScheduling,@showOnApp,@heatingMappingId)   
 
 INSERT INTO [dbo].[PA_PROPERTY_ITEM_NOTES_HISTORY](
	[SID],
	[PROPERTYID],
	[ItemId],
	[Notes],
	[CreatedOn],
	[CreatedBy],
	[SchemeId],
	[BlockId],
	[ShowInScheduling],
	[ShowOnApp],
	[HeatingMappingId],
	[NotesStatus]
	)
	VALUES
	(@@identity,
	@propertyId,
	@itemId,
	@note,
	GETDATE(),
	@createdBy,
	@schemeId,
	@blockId,
	@showInScheduling,
	@showOnApp,
	@heatingMappingId,
	'Added' )
 
END 