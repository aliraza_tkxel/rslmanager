SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[I_JOBS_INSERT]
(
	@TeamId bigint,
	@Description nvarchar(2500),
	@TypeId bigint,
	@OtherType nvarchar(50),
	@Title nvarchar(200),
	@Ref nvarchar(30),
	@LocationId bigint,
	@Duration nvarchar(30),
	@StartDate nvarchar(30),
	@ClosingDate datetime,
	@Salary nvarchar(30),
	@ContactName nvarchar(30),
	@ApplicationId int
)
AS
	SET NOCOUNT OFF;

DECLARE @CLOSINGDATE_5PM DATETIME

-- Ensure closing date is set to 5pm
SET @CLOSINGDATE_5PM = DATEADD(hh,17, DATEADD(dd, 0, DATEDIFF(dd,0,@ClosingDate)))

INSERT INTO [I_JOB] ([TeamId], [Description], [TypeId], [OtherType], [Title], [Ref], [LocationId], [Duration], 
			[StartDate], [DatePosted], [ClosingDate], [Salary], [ContactName], [ApplicationId], [Active]) 

VALUES (@TeamId, @Description, @TypeId, @OtherType, @Title, @Ref, @LocationId, @Duration, @StartDate, 
		GETDATE(), @CLOSINGDATE_5PM, @Salary, @ContactName, @ApplicationId, 1);
	

SELECT     JobId, TeamId, Description, TypeId, Title, Ref, LocationId, Duration, StartDate, ClosingDate, Salary, ContactName, ApplicationId, OtherType
FROM         I_JOB
WHERE     (JobId = SCOPE_IDENTITY())
GO
