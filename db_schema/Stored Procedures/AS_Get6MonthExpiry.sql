USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:<Adeel Ahmed>
-- Create date: <17-06-2010>
-- Description:	<getting the customer info for letter>
-- =============================================
IF OBJECT_ID('dbo.AS_Get6MonthExpiry') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_Get6MonthExpiry AS SET NOCOUNT ON;') 
GO


-- =============================================
-- Author:		Hussain Ali
-- Create date: 29/11/2012
-- Description:	This SP returns the certificate exipration count in the next six months. 
-- Webpage: dashboard.aspx
-- Exec [dbo].[AS_Get6MonthExpiry]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- =============================================
ALTER PROCEDURE [dbo].[AS_Get6MonthExpiry] 
	-- Parameters for the stored procedure here
	@PatchId		int,
	@DevelopmentId	int,
	@FuelType varchar(8000)
	
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
SET NOCOUNT ON;
	
		-- Declaring the variables to be used in this query
			DECLARE @selectClause	varchar(8000),
					@fromClause		varchar(8000),
					@whereClause	varchar(8000),
					@groupClause	varchar(8000),
					@orderClause	varchar(8000),
					@query			varchar(8000)	     
		if @FuelType = 'Gas'
			Begin	
			-- Initalizing the variables to be used in this query
			SET @selectClause	= 'SELECT 
										COUNT(P__PROPERTY.PROPERTYID) AS monValue, CONVERT(VARCHAR(3), DATEADD(YEAR,1,P_LGSR.ISSUEDATE), 0) AS monName, DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS yearName'
	
			SET @fromClause		= 'FROM 
										P__PROPERTY
										INNER JOIN P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
										Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID'
	
			SET @whereClause	= 'WHERE'
	
			SET @groupClause = 'GROUP BY CONVERT(VARCHAR(3), DATEADD(YEAR,1,P_LGSR.ISSUEDATE), 0), 
													DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)),
													DATEPART(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE))'
 	
 			SET @orderClause = 'ORDER BY DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)), DATEPART(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) ASC'
 		
			-- Adding white spaces between the elements
			SET @selectClause	= @selectClause + CHAR(10)
			SET @fromClause		= @fromClause	+ CHAR(10)
			SET @whereClause	= @whereClause  + CHAR(10)
			SET @groupClause	= @groupClause  + CHAR(10)
		
			-- Filling in the where classes
	
			SET @whereClause = @whereClause + '1=1 
												AND P__PROPERTY.STATUS NOT IN (9,5,6) 
												AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
												AND PA_Parameter_value.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
												AND DATEADD(YEAR,1,P_LGSR.ISSUEDATE) >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
												AND DATEADD(YEAR,1,P_LGSR.ISSUEDATE) < DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 6, 0)' + CHAR(10)
	
			if (@PatchId <> -1 OR @DevelopmentId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'AND' + CHAR(10)
 			END
	
			if (@PatchId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 			END
 	
 			if (@PatchId <> -1 AND @DevelopmentId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'AND' + CHAR(10)
 			END

			if (@DevelopmentId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 			END
		End
		------------------------------- for oil -----------------------------------------------------------
		if @FuelType = 'Oil'
			Begin	
			SET @selectClause	= 'SELECT 
										COUNT(P__PROPERTY.PROPERTYID) AS monValue, CONVERT(VARCHAR(3), DATEADD(YEAR,1,P_LGSR.ISSUEDATE), 0) AS monName, DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS yearName'
	
			SET @fromClause		= 'From P__PROPERTY
										INNER JOIN P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
										Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID'
	
			SET @whereClause	= 'WHERE'
	
			SET @groupClause = 'GROUP BY CONVERT(VARCHAR(3), DATEADD(YEAR,1,P_LGSR.ISSUEDATE), 0), 
													DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)),
													DATEPART(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE))'
 	
 			SET @orderClause = 'ORDER BY DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)), DATEPART(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) ASC'
 		
			-- Adding white spaces between the elements
			SET @selectClause	= @selectClause + CHAR(10)
			SET @fromClause		= @fromClause	+ CHAR(10)
			SET @whereClause	= @whereClause  + CHAR(10)
			SET @groupClause	= @groupClause  + CHAR(10)
		
			-- Filling in the where classes
	
			SET @whereClause = @whereClause + '1=1 
												AND P__PROPERTY.STATUS NOT IN (9,5,6) 
												AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
												AND PA_Parameter_value.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''oil'')
												AND DATEADD(YEAR,1,P_LGSR.ISSUEDATE) >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
												AND DATEADD(YEAR,1,P_LGSR.ISSUEDATE) < DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 6, 0)' + CHAR(10)
	
			if (@PatchId <> -1 OR @DevelopmentId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'AND' + CHAR(10)
 			END
	
			if (@PatchId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 			END
 	
 			if (@PatchId <> -1 AND @DevelopmentId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'AND' + CHAR(10)
 			END

			if (@DevelopmentId <> -1)
			BEGIN
				SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 			END
		End
		------------------------------- alternative servicing -----------------------------------------------------------
		if @FuelType = 'Alternative Servicing'
			Begin
				SET @selectClause	= 'select distinct PA_PARAMETER_VALUE.ValueID,  PA_PARAMETER_VALUE.ValueDetail as monName, ISNULL(TBL_ALT.total,0.1) as monValue'
	
				SET @fromClause		= 'from PA_PARAMETER_VALUE Inner JOIN PA_PARAMETER on PA_PARAMETER_VALUE.ParameterID = PA_PARAMETER.ParameterID
										left Join PA_HeatingMapping on PA_HeatingMapping.HeatingType = PA_PARAMETER_VALUE.ValueID
										left join P__PROPERTY on P__PROPERTY.PROPERTYID = PA_HeatingMapping.PropertyId
										Left join (select COUNT(P__PROPERTY.PROPERTYID) AS total, PA_PARAMETER_VALUE.ValueDetail,PA_PARAMETER_VALUE.ValueId
										from P_LGSR, P__PROPERTY, PA_HeatingMapping,PA_PARAMETER_VALUE ,PA_PARAMETER 
										where P_LGSR.PROPERTYID = P__PROPERTY.PROPERTYID
										and PA_PARAMETER.parameterId = PA_PARAMETER_VALUE.ParameterID
										and P__PROPERTY.PROPERTYID = PA_HeatingMapping.PropertyId and PA_HeatingMapping.HeatingType  = PA_PARAMETER_VALUE.ValueID
										and PA_PARAMETER_VALUE.IsAlterNativeHeating = 1 and PA_PARAMETER_VALUE.ParameterID = 65
										AND DATEADD(YEAR,1,P_LGSR.ISSUEDATE) >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
										AND DATEADD(YEAR,1,P_LGSR.ISSUEDATE) < DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 6, 0)
										GROUP by PA_PARAMETER_VALUE.ValueDetail ,PA_PARAMETER_VALUE.ValueId ) as TBL_ALT on TBL_ALT.ValueID = PA_PARAMETER_VALUE.ValueID'
	
				SET @whereClause	= 'WHERE'
	
				SET @groupClause = ' '
 	
 				SET @orderClause = ' '
 		
				-- Adding white spaces between the elements
				SET @selectClause	= @selectClause + CHAR(10)
				SET @fromClause		= @fromClause	+ CHAR(10)
				SET @whereClause	= @whereClause  + CHAR(10)
				SET @groupClause	= @groupClause  + CHAR(10)
		
				-- Filling in the where classes
	
				SET @whereClause = @whereClause + 'PA_PARAMETER_VALUE.IsAlterNativeHeating=1 and ParameterName = ''heating fuel'' ' + CHAR(10)
	
				if (@PatchId <> -1 OR @DevelopmentId <> -1)
				BEGIN
					SET @whereClause = @whereClause + 'AND' + CHAR(10)
 				END
	
				if (@PatchId <> -1)
				BEGIN
					SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 				END
 	
 				if (@PatchId <> -1 AND @DevelopmentId <> -1)
				BEGIN
					SET @whereClause = @whereClause + 'AND' + CHAR(10)
 				END

				if (@DevelopmentId <> -1)
				BEGIN
					SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 				END
			end
		



 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause + @groupClause  + @orderClause
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
        
END
