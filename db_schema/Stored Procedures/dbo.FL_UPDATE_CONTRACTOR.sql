SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO










CREATE PROCEDURE [dbo].[FL_UPDATE_CONTRACTOR]
	/*	===============================================================
	'   NAME:           FL_UPDATE_CONTRACTOR
	'   DATE CREATED:   4TH March,2008
	'   CREATED BY:     Usman Sharif
	'   CREATED FOR:    Tenants Online
	'   PURPOSE:        To update the fault contractor
	'   IN:             @FaultLogId
	'   IN:             @ORGID
	'   OUT:	    Nothing
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@FaultLogId INT,
		@ORGID INT,
		@notes nvarchar(500),
		@email bit 
		
	)
AS	
	Declare @FaultStatusId int
	Declare @FaultStatusDescription varchar (50)	
	DECLARE @AssginedToContractorStatusId INT
	Declare @faultBasketId int
	Declare @IAmHappy2 bit
	
	BEGIN TRAN
		
	--Get and set the repair assigned to contractor status
	SET @AssginedToContractorStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Assigned To Contractor')
	
	
	
	Select @faultBasketId  = FL_FAULT_LOG.FaultBasketID, @IAmHappy2 = IamHappy  FROM FL_FAULT_LOG INNER JOIN FL_FAULT_BASKET ON 
	FL_FAULT_LOG.FaultBasketID = FL_FAULT_BASKET.FaultBasketID
	WHERE FL_FAULT_LOG.FaultLogID = @FaultLogId

	
	Begin 
	Update FL_FAULT_BASKET 
	SET 
	PreferredContactDetails = @notes,
   	IamHappy = @email
	Where FaultBasketID = @faultBasketId
	
	End

               
		SELECT @FaultStatusDescription = FL_FAULT_STATUS.Description FROM FL_FAULT_LOG INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID  WHERE FaultLogId = @FaultLogId                                
		
		IF @FaultStatusDescription = 'Assigned To Contractor'
		BEGIN
			UPDATE Fl_Fault_Log
			SET 	
			ORGID = @ORGID
			WHERE FaultLogId = @FaultLogId
		END
		ELSE IF @FaultStatusDescription = 'Repair Reported'
		BEGIN
			UPDATE Fl_Fault_Log
			SET 	
			ORGID = @ORGID, StatusID = @AssginedToContractorStatusId
			WHERE FaultLogId = @FaultLogId
		END

		
	COMMIT TRAN



GO
