
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetPropertiesInprogressCount] 

/* ===========================================================================
 '   NAME:           PLANNED_GetPropertiesInprogressCount
-- EXEC	[dbo].[PLANNED_GetPropertiesInprogressCount]	
-- Author:		<Ahmed Mehmood>
-- Create date: <10/09/2013>
-- Description:	<Get Properties Inprogress Count>
-- Web Page: Dashboard.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		

		@componentId int = -1

	)
		
AS
	
	DECLARE
	 @SelectClause varchar(8000),
	 @FromClause   varchar(8000),
	 @WhereClause  varchar(8000),
	 @SelectClauseCount varchar(8000),
	 @FromClauseCount   varchar(8000),
	 @WhereClauseCount  varchar(8000),	        
	 @mainSelectQuery varchar(5500),  
	 @FinalSelectQuery varchar(5500),     
	 @SearchCriteria varchar(8000)

         
    SET @SearchCriteria = '	PLANNED_STATUS.TITLE=''Arranged'' 
							AND PLANNED_SUBSTATUS.TITLE IN (''InProgress'',''In Progress'') 
							AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS IN (''InProgress'',''In Progress'') AND'
        
    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND'  
    

	SET @SelectClause = 'SELECT DISTINCT PLANNED_JOURNAL.PROPERTYID as PID '                                            		
	SET @FromClause = CHAR(10) + '	FROM	PLANNED_APPOINTMENTS
											INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JOURNALID =PLANNED_JOURNAL.JOURNALID 
											INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID																						 
											INNER JOIN PLANNED_SUBSTATUS ON PLANNED_APPOINTMENTS.JOURNALSUBSTATUS = PLANNED_SUBSTATUS.SUBSTATUSID '
	
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 )'        
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
	
	SET @SelectClauseCount ='SELECT COUNT(ARRANGED_PROPERTIES.PID) as TotalCount '
	SET @FromClauseCount ='FROM ('+@mainSelectQuery+') ARRANGED_PROPERTIES'
	
	SET @FinalSelectQuery = @SelectClauseCount + @FromClauseCount
	
	print (@FinalSelectQuery)
	EXEC (@FinalSelectQuery)
				















GO
