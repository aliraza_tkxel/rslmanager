SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_SaveReferral @journalId=1,
--@itemStatusId=1,
--@itemActionId=1,
--@isTypeArears=1,@isTypeDamage=1,
--@isTypeASB=1,@stageId=1,@safetyIssue=1,@conviction =1,
-- @subStanceMisUse=1,@selfHarm=1,@notes='test notes',@isTenantAware =1,@neglectNotes ='test neglect notes',
-- @otherNotes = 'test other notes',@miscNotes ='test misc notes'
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/20/2014>
-- Description:	<Description,,Save the Referral data>
-- WebPage: CustomerDetail.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_SaveReferral]
(
@UserId int,
@propertyId varchar(50),
@tenancyId int,
@customerId int,
@itemId int,
@title varchar(50),
@itemStatusId int,
@ItemNatureId int,
@itemActionId int,
@isTypeArears bit,@isTypeDamage bit,
@isTypeASB bit,@stageId int,@safetyIssue int,@conviction int,
 @subStanceMisUse int,@selfHarm int,@notes varchar(500),@isTenantAware int,@neglectNotes varchar(1000),
 @otherNotes varchar(1000),@miscNotes varchar(1000),
 @JournalId int=0 output
)
AS
BEGIN
Declare @parameterDef varchar(500)
Declare @selectCount varchar(500)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO C_JOURNAL([CUSTOMERID],[TENANCYID],[PROPERTYID],[ITEMID],[ITEMNATUREID],[CURRENTITEMSTATUSID],[LETTERACTION],[CREATIONDATE],[TITLE],[LASTACTIONDATE]
      ,[NEXTACTIONDATE])
      Values(@customerId,@tenancyId,@propertyId,@itemId, @ItemNatureId,@itemStatusId,NULL,GETDATE(),@title,NULL,NULL)
      Set @JournalId =  @@IDENTITY  
    -- Insert statements for procedure here
	Insert INTO C_REFERRAL
(JOURNALID,ITEMSTATUSID,ITEMACTIONID,isTypeArrears,isTypeDamage,isTypeASB,STAGEID,SAFETYISSUES,CONVICTION,SUBSTANCEMISUSE
 ,SELFHARM,NOTES,ISTENANTAWARE,NEGLECTNOTES,OTHERNOTES,MISCNOTES,REASONNOTES,LASTACTIONDATE,ASSIGNTO,LASTACTIONUSER)
 Values
 (@journalId,@itemStatusId,17,@isTypeArears,@isTypeDamage,@isTypeASB,@stageId,@safetyIssue,@conviction,
 @subStanceMisUse,@selfHarm,@notes,@isTenantAware,@neglectNotes,@otherNotes,@miscNotes,NULL,GETDATE(),NULL,@UserId)

Return
END
GO
