USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_INSERT_NL_JOURNALENTRYDEBITLINE]    Script Date: 10/24/2014 17:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC 
-- Author:  
-- Create date: 
-- Last Modified: 
-- Description: 
-- Webpage : 
  
-- =============================================  

IF OBJECT_ID('dbo.F_INSERT_NL_JOURNALENTRYDEBITLINE') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.F_INSERT_NL_JOURNALENTRYDEBITLINE AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[F_INSERT_NL_JOURNALENTRYDEBITLINE]            
(            
@TXNID INT,             
@ACCOUNTID INT,            
@TIMEMODIFIED DATETIME =GETDATE,             
@EDITSEQUENCE INT,             
@TXNDATE SMALLDATETIME =GETDATE,             
@AMOUNT MONEY,             
@DESCRIPTION VARCHAR(500),             
@MEMO VARCHAR(1000),      
@LINEID int output ,  
@TRACKERID  int = NULL,
@COSTCENTREID INT = NULL,     
@HEADID INT = NULL,         
@EXPENDITUREID INT = NULL,
@COMPANYID INT=NULL,
@INCSC BIT=NULL,
@PROPERTYAPPORTIONMENT MONEY=null,
@File_Url nvarchar(max) = null
)            
            
AS            
            
BEGIN            
 BEGIN TRAN            
 INSERT INTO             
  NL_JOURNALENTRYDEBITLINE( TXNID, ACCOUNTID, TIMECREATED, TIMEMODIFIED,             
   EDITSEQUENCE, TXNDATE, AMOUNT, DESCRIPTION, MEMO, COSTCENTREID, HEADID, EXPENDITUREID, TRACKERID, COMPANYID,INCSC,PROPERTYAPPORTIONMENT,File_Url)             
  VALUES( @TXNID, @ACCOUNTID, GETDATE(), @TIMEMODIFIED, @EDITSEQUENCE, @TXNDATE,@AMOUNT , @DESCRIPTION, @MEMO, @COSTCENTREID, @HEADID, @EXPENDITUREID, @TRACKERID, @COMPANYID,@INCSC,@PROPERTYAPPORTIONMENT,@File_Url)             
         
 set @LINEID= SCOPE_IDENTITY()        
        
 IF @@ERROR <> 0             
 BEGIN             
  ROLLBACK TRAN            
 --  RAISERROR ('ERROR WHILE INSERTING IN TO NL_JOURNALENTRYDEBITLINE', 19, 1)            
  RETURN          
 END            
            
 COMMIT            
END    
    
    
  


GO
