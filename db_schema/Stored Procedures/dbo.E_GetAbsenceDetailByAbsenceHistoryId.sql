USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.E_GetAbsenceDetailByAbsenceHistoryId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_GetAbsenceDetailByAbsenceHistoryId AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_GetAbsenceDetailByAbsenceHistoryId] 
	-- Add the parameters for the stored procedure here
	@absenceHistoryId INT
	
AS
BEGIN


	SELECT	A.STARTDATE as StartDate
			,a.RETURNDATE as ReturnDate
			, COALESCE(cast(round(a.DURATION,2) as decimal(36,2)), cast(round(a.DURATION_HRS,2) as decimal(36,2))) as Duration 
			,Case when j.HolidayIndicator=2 and j.PARTFULLTIME=2 then 'hrs' else 'days' end as Unit 
			,ES.DESCRIPTION AS LeaveStatus
			,J.EMPLOYEEID  as EmpId
			,JNAL.ITEMNATUREID as ItemNatureId
			, en.DESCRIPTION as NatureDescription
			, E.FIRSTNAME as FirstName 
			, E.LASTNAME as LastName
			, A.ABSENCEHISTORYID as AbsenceHistoryId
			, A.HolType
			, JNAL.TITLE as Title
			, (select top 1 NOTES from E_ABSENCE where JOURNALID = JNAL.JOURNALID) as SubmitNote
			, case when (select count(E_ABSENCE.ABSENCEHISTORYID) from E_ABSENCE where JOURNALID = JNAL.JOURNALID) > 1 then A.NOTES ELSE '' END as Notes
			, A.REASON as Reason
	FROM 	E__EMPLOYEE E  
			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
			LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID
			LEFT JOIN E_STATUS	ES ON JNAL.CURRENTITEMSTATUSID = ES.ITEMSTATUSID																									
			INNER JOIN E_NATURE en on JNAL.ITEMNATUREID = en.ITEMNATUREID
			INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID
	WHERE 	A.ABSENCEHISTORYID = @absenceHistoryId

END
