USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertiesByDevPhaseBlock]    Script Date: 27-Feb-17 10:39:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
  -- Exec [PDR_GetPropertiesByDevPhaseBlock] 20, -1, 164
-- =============================================

IF OBJECT_ID('dbo.PDR_GetPropertiesByDevPhaseBlock') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetPropertiesByDevPhaseBlock AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetPropertiesByDevPhaseBlock]
@developmentId int = -1,
@phaseId int = -1,
@blockid int = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @whereClause NVarchar(1000) = '';
	DECLARE @propertyQuery NVarchar(1000) = '';

	IF(@developmentId > 0 AND @phaseId = -1)
		BEGIN
			Set @whereClause = @whereClause + ' WHERE PP.DevelopmentId = ' + CONVERT(NVARCHAR, @developmentId) + 
							' AND PP.PhaseId IN  (	Select B.PHASEID from P_PHASE B Where B.DEVELOPMENTID = ' + 
							CONVERT(NVARCHAR, @developmentId) +') AND PP.blockid = ' + CONVERT(NVARCHAR, @blockid)
		END
	ELSE 
		BEGIN
		Set @whereClause = @whereClause + ' WHERE PP.DevelopmentId = ' + CONVERT(NVARCHAR, @developmentId) + 
							' AND PP.PhaseId = ' + CONVERT(NVARCHAR, @phaseId) + 
							' AND PP.blockid = ' + CONVERT(NVARCHAR, @blockid)
		END

	-- Insert statements for procedure here
	Set @propertyQuery =  'SELECT PP.PROPERTYID,(ISNULL(PP.HOUSENUMBER,'' '') +'' ''+ ISNULL(PP.ADDRESS1,'' '')) PropertyAddress FROM P_Block PB INNER JOIN P__PROPERTY PP
							on PB.blockid = PP.blockid '+ @whereClause
	
	PRINT @propertyQuery    
	EXEC (@propertyQuery) 
END