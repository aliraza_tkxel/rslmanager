
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_OverdueStagesAlertCount]  
 @caseOwnedBy int=0  
AS  
 BEGIN  
  declare @RegionSuburbClause varchar(8000)  
  declare @query varchar(8000)  
   
  IF(@caseOwnedBy <= 0 )  
  BEGIN  
   SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'  
  END   
  ELSE  
  BEGIN  
   SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ ' AND IsActive=''true'')   
           OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedBy )+ 'AND IsActive=''true''))'  
  END  
    
  SET @query=  
     ' SELECT COUNT(*) as recordCount   
       FROM(  
         SELECT Count(*) as TotalRecords  
         FROM AM_Case   
        INNER JOIN AM_Action ON AM_Case.ActionId = AM_Action.ActionId   
        INNER JOIN AM_Status ON AM_Case.StatusId = AM_Status.StatusId   
        INNER JOIN AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID   
        INNER JOIN C_TENANCY ON AM_Case.TenancyId  = C_TENANCY.TENANCYID   
        INNER JOIN AM_LookupCode ON AM_Status.NextStatusAlertFrequencyLookupCodeId = AM_LookupCode.LookupCodeId  
        INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
        LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
		INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
        INNER JOIN C_ADDRESS on customer.customerId=C_ADDRESS.CUSTOMERID  
       WHERE AM_Case.IsActive = 1   
        AND AM_CASE.IsPaymentPlan<>1  
       AND  ' + @RegionSuburbClause + '  
       AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)  
       AND AM_Case.CaseId IN ( SELECT AM_CaseHistory.CaseId     
             FROM AM_CaseHistory   
              INNER JOIN AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId   
              INNER JOIN AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId   
              INNER JOIN AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId   
              INNER JOIN E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID   
              INNER JOIN C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID   
              INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID   
              INNER JOIN AM_LookupCode ON AM_Status.NextStatusAlertFrequencyLookupCodeId = AM_LookupCode.LookupCodeId  
                                                     WHERE AM_CaseHistory.IsActive = 1   
                                                     and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Status.NextStatusAlert, AM_LookupCode.CodeName, AM_CaseHistory.StatusReview ) = ''true''   
                                                     and AM_CaseHistory.CaseHistoryId = (Select CaseHistoryId From(
	  SELECT CaseHistoryId, row_number() over ( order by CaseHistoryId desc) as row   
        FROM AM_CaseHistory as ach where ach.CaseId =AM_CaseHistory.CaseId and ach.IsActionIgnored = ''false'' )as temp where row=2 )
                                                 )  
       GROUP BY AM_Case.TenancyId  
      ) as TEMP '  
   
 PRINT @query;  
 EXEC(@query);  
END  
  
GO
