SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Rogers
-- Create date: 9th September 2014
-- Description:	SP to get config value
-- =============================================
CREATE PROCEDURE [dbo].[F_PAYTHRU_GET_CONFIG]
	-- Add the parameters for the stored procedure here
	@SettingName VARCHAR(255) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT  *
    FROM    dbo.F_PAYTHRU_CONFIG
    WHERE   SettingName = @SettingName
    
END
GO
