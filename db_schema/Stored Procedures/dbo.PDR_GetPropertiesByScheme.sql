USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertiesByScheme]    Script Date: 12/20/2017 7:18:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Properties By SchemeID for dropdown
 
    Author: Muhammad Shahzaib	
    Creation Date:  19-12-2017 

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
   
    
    Execution Command:
    
    Exec PDR_GetPropertiesByScheme  2
  =================================================================================*/

IF OBJECT_ID('dbo.PDR_GetPropertiesByScheme') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetPropertiesByScheme AS SET NOCOUNT ON;') 
GO 


ALTER PROCEDURE [dbo].[PDR_GetPropertiesByScheme]
	@SchemeId INT,
	@blockId INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @mainQuery NVARCHAR(MAX),
	@searchCriteria NVARCHAR(MAX),
	@orderByClause NVARCHAR(MAX)
	
	SET @searchCriteria = '1=1 '
	
	IF(@SchemeId <> -1 )
	    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P__PROPERTY.SCHEMEID = ' + CONVERT(NVARCHAR(10), @SchemeId)
	IF(@SchemeId = -1 )
	    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P__PROPERTY.BLOCKID = ' + CONVERT(NVARCHAR(10), @blockId)
    
    SET @orderByClause = 'ORDER BY PROPERTYNAME ASC '

    SET @mainQuery = 'SELECT	PROPERTYID,ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as PROPERTYNAME  FROM	P__PROPERTY WHERE ' + @searchCriteria + @orderByClause
    
    EXEC (@mainQuery)
END
