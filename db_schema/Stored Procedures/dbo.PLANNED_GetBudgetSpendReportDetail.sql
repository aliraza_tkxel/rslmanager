SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
--EXEC	@return_value = [dbo].[PLANNED_GetBudgetSpendReportDetail]
--		@replacementYear = N'1934',
--		@schemeId = -1,
--		@status = '',
--		@componentId = 12	
-- Author:		<Ahmed Mehmood>
-- Create date: <2/12/2014>
-- Description:	<Get Budget Spend Report Detail>
-- Web Page: BudgetSpendReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetBudgetSpendReportDetail]
( 
	-- Add the parameters for the stored procedure here

		@replacementYear varchar(20),
		@schemeId int,
		@status varchar(50),
		@componentId int
)
AS
BEGIN
DECLARE
	 
		@SelectClause varchar(6000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(500),	
        @mainSelectQuery varchar(6000)       
        ,@sortColumn varchar(500)
        ,@sortOrder varchar (5)
        ,@searchCriteria varchar(1500)
        ,@YearStartDate Datetime
        ,@YearEndDate DateTime

				Select @YearStartDate = CONVERT(DATETIME, @replacementYear+'0401') ,
		       @YearEndDate = CONVERT(DATETIME, CONVERT(NVARCHAR,CONVERT(INT,@replacementYear)+1)+'0331 23:59:59')
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		-- SET @searchCriteria = ' 1 = 1 AND YEAR(PID.DueDate) = ' + CONVERT(NVARCHAR(10), @replacementYear) 
		SET @searchCriteria = ' 1 = 1 AND Convert( DateTime, Convert( Char, PID.DueDate, 103 ), 103 ) BETWEEN  
						  Convert( DateTime, ''' + Ltrim( RTrim( CONVERT(Char, @YearStartDate, 103) ) ) + ''', 103 )  AND 
						  Convert( DateTime, ''' + Ltrim( RTrim( CONVERT(Char, @YearEndDate, 103) ) ) + ''', 103 ) '
						+ ' AND PC.COMPONENTID = '+ CONVERT(NVARCHAR(10), @componentId)
		
		IF @schemeId != -1 
		BEGIN
			SET @searchCriteria = @searchCriteria + ' AND P.DEVELOPMENTID = ' + CONVERT(NVARCHAR(10), @schemeId) 		
		END
			
		IF @status != 'Forecast' 
		BEGIN
			IF @status = 'To be Arranged'
			BEGIN
				SET @searchCriteria = @searchCriteria + ' AND (PS.TITLE = ''To be Arranged'' OR PS.TITLE = ''Arranged'' OR PS.TITLE = ''Completed'') '
			END
			ELSE
			BEGIN 
				SET @searchCriteria = @searchCriteria + ' AND PS.TITLE = ''' + CONVERT(NVARCHAR(50), @status) +''''
			END
			
		END	 	
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause		
		
		SET @selectClause = 'SELECT	P.PROPERTYID 
							, ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'' '') AS Address
							,PC.LABOURCOST + PC.MATERIALCOST AS Cost
							,CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int) AS HouseNumber
							, PID.DueDate As DueDate'
		
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		
		IF @status = 'Forecast' 
		BEGIN
			SET @fromClause =	  CHAR(10) +' FROM	PA_PROPERTY_ITEM_DATES AS PID
													INNER JOIN P__PROPERTY AS P ON PID.PROPERTYID = P.PROPERTYID
													INNER JOIN PLANNED_COMPONENT PC ON PID.PLANNED_COMPONENTID = PC.COMPONENTID  '
		END
		ELSE
		BEGIN 
			SET @fromClause =	  CHAR(10) +' FROM	PLANNED_JOURNAL AS PJ
													INNER JOIN PLANNED_STATUS AS PS ON PJ.STATUSID = PS.STATUSID
													INNER JOIN P__PROPERTY AS P ON PJ.PROPERTYID = P.PROPERTYID
													INNER JOIN PLANNED_COMPONENT PC ON PJ.COMPONENTID = PC.COMPONENTID
													INNER JOIN PA_PROPERTY_ITEM_DATES AS PID  ON PID.PLANNED_COMPONENTID = PC.COMPONENTID '	
		END
			-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
				
	
		SET @sortColumn = CHAR(10)+ ' HouseNumber'  		
		SET @sortOrder = CHAR(10)+ 'ASC'
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		PRINT @mainSelectQuery
		EXEC (@mainSelectQuery)
																										
		-- End - Execute the Query 
						
END

GO
