USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetWarrantyList]    Script Date: 16-Oct-17 3:04:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/12/2014>
-- Description:	<Description,,Get WARRANTIES LIST ON warranty page>
-- 
-- =============================================

IF OBJECT_ID('dbo.PDR_GetWarrantyList') IS NULL 
EXEC('CREATE PROCEDURE dbo.PDR_GetWarrantyList AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetWarrantyList]
	-- Add the parameters for the stored procedure here
		@ID varchar(100),
		@requestType varchar(100),
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'WARRANTYTYPE', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 

		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
		@AllValueConst nvarchar(100),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		-- -2 represents the 'All' value in the dropdown list as it was required by client 
		SET @AllValueConst = '-2'


		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		if (@requestType = 'Block')
			BEGIN							
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND(W.Area=PA.AreaID OR W.Area IS NULL OR W.Area = '''+@AllValueConst+''') AND (W.Item=PI.ItemID OR W.Item IS NULL OR W.Item = '''+@AllValueConst+''' ) AND  W.BLOCKID  = '''+@ID+''''
			END
		ELSE IF(@requestType = 'Scheme')
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND(W.Area=PA.AreaID OR W.Area IS NULL OR W.Area = '''+@AllValueConst+''' ) AND (W.Item=PI.ItemID OR W.Item IS NULL OR W.Item = '''+@AllValueConst+''' ) AND  W.SCHEMEID  = '''+@ID+''''
			END
		ELSE
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  W.PROPERTYID ='''+@ID+''''

			END	
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select distinct top ('+convert(varchar(10),@limit)+')
		WARRANTYID,
		convert(varchar(10), EXPIRYDATE, 103) EXPIRYDATE,
		ISNULL(AI.DESCRIPTION,'' '') AS [For]
		,ISNULL(O.NAME,'' '') AS CONTRACTOR
		,ISNULL(WT.DESCRIPTION,'' '') as WARRANTYTYPE
		, ISNULL(PL.LOCATIONNAME, '' '') AS  LOCATIONNAME
		,CASE WHEN W.AREA = -2 THEN ''All'' 
			  ELSE ISNULL(PA.AREANAME, '' '') 
	     END AS  AREANAME
		,CASE WHEN W.ITEM = -2 THEN ''All'' 
			  ELSE ISNULL(PI.ITEMNAME, '' '') 
	     END AS  ITEMNAME
		
		'


		--============================From Clause============================================

		if (@requestType = 'Block' OR @requestType = 'Scheme')
		BEGIN
			SET @fromClause = ' from PDR_WARRANTY W
						INNER JOIN P_WARRANTYTYPE WT on WT.WARRANTYTYPEID = W.WARRANTYTYPE 
						INNER JOIN S_ORGANISATION O on O.ORGID = W.CONTRACTOR
						LEFT JOIN R_AREAITEM AI ON AI.AREAITEMID = W.AREAITEM
						LEFT JOIN (	SELECT LocationID, LocationName 
									FROM PA_LOCATION
									UNION ALL
									SELECT AreaID AS LocationID, AreaName AS LocationName 
									FROM PA_AREA
									WHERE IsForSchemeBlock = 1) PL ON PL.LocationID = W.CATEGORY
						left join( SELECT AreaID, AreaName
									FROM PA_AREA
									WHERE  IsActive=1 
									AND IsForSchemeBlock=0
									UNION ALL
									SELECT ItemID AS AreaID, ItemName AS AreaName
									FROM PA_ITEM
									WHERE AreaID IN (SELECT AreaID 
													FROM PA_AREA
													WHERE IsForSchemeBlock = 1 )
									AND ParentItemId IS NULL 
									AND IsActive=1 
									AND IsForSchemeBlock=1 
									and ItemName not like ''Electrics''
									) PA on pa.AreaID=w.Area
						left join  ( SELECT ItemID, ItemName
									FROM PA_ITEM
									WHERE  IsActive=1 
									AND ParentItemId IS NULL 
									AND IsActive=1
									UNION ALL
									SELECT ItemID, ItemName
									FROM PA_ITEM
									WHERE  IsActive=1 
									) PI on pi.ItemID=w.Item '
		END
		ELSE
		BEGIN
			SET @fromClause = ' from PDR_WARRANTY W
						INNER JOIN P_WARRANTYTYPE WT on WT.WARRANTYTYPEID = W.WARRANTYTYPE 
						INNER JOIN S_ORGANISATION O on O.ORGID = W.CONTRACTOR
						LEFT JOIN R_AREAITEM AI ON AI.AREAITEMID = W.AREAITEM
						LEFT JOIN PA_LOCATION PL ON PL.LocationID = W.CATEGORY
						LEFT JOIN PA_AREA PA ON PA.AreaID = W.AREA
						LEFT JOIN PA_ITEM PI ON PI.ItemID = W.ITEM'
		END

		--============================Order Clause==========================================
		IF(@sortColumn = 'WARRANTYTYPE')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' WARRANTYTYPE' 	

		END

		IF(@sortColumn = 'EXPIRY')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'EXPIRYDATE' 	

		END
		IF(@sortColumn = 'CONTRACTOR')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'CONTRACTOR' 	

		END
		IF(@sortColumn = 'For')
		BEGIN
			SET @sortColumn = CHAR(10)+ '[For]' 	

		END	
		IF(@sortColumn = 'LOCATIONNAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'LOCATIONNAME' 	

		END	
		IF(@sortColumn = 'AREANAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'AREANAME' 	

		END	
		IF(@sortColumn = 'ITEMNAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ITEMNAME' 	

		END	

		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		--=================================	Where Clause ================================

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		--print(@whereClause)
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================	

END
