USE [RSLBHALive]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_WARNINGS  OFF
GO

IF OBJECT_ID('dbo.[V_getSupplierDataForAlert]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[V_getSupplierDataForAlert] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[V_getSupplierDataForAlert](
 @poID int = 0
)
AS
BEGIN

select top 1 org.NAME as OrgName, ISNULL(con.FIRSTNAME, '') + ' ' + ISNULL(con.LASTNAME, '') as ContactName,  
				  ISNULL(E_CONTACT.WORKMOBILE, 'N/A') AS Mobile,   ISNULL(E_CONTACT.WORKDD, 'N/A') WORKDD
				  from PDR_CONTRACTOR_WORK  
				  inner join S_ORGANISATION org on org.ORGID = PDR_CONTRACTOR_WORK.ContractorId  
				  LEFT join E__EMPLOYEE con on con.EMPLOYEEID = PDR_CONTRACTOR_WORK.ContactId  
				  LEFT join E_CONTACT on E_CONTACT.EMPLOYEEID = con.EMPLOYEEID  
				  where PurchaseOrderId =   @poID

END