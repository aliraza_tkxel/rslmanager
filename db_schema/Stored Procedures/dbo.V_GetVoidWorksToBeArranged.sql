Use RSLBHALive
GO
-- =============================================  
/* =================================================================================      
    Page Description: Get Void Works To Be Arranged List   
    Author: Ali Raza  
    Creation Date: May-20-2015  
  
    Change History:  
  
    Version      Date             By                      Description  
    =======     ============    ========           ===========================  
    v1.0         May-20-2015      Ali Raza         Get Void Works To Be Arranged List   
    v2.0         Nov-10-2015      Raja Aneeq        Add a patch filter   
  =================================================================================*/  
--  DECLARE @totalCount int  
--EXEC  [dbo].[V_GetVoidWorksToBeArranged]  
--  @searchText = NULL,  
--  @pageSize = 100,  
--  @pageNumber = 1,  
--  @totalCount = @totalCount OUTPUT  
--SELECT @totalCount as N'@totalCount'  
-- =============================================  
IF OBJECT_ID('dbo.V_GetVoidWorksToBeArranged') IS NULL 
	EXEC('CREATE PROCEDURE dbo.V_GetVoidWorksToBeArranged AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[V_GetVoidWorksToBeArranged]  
-- Add the parameters for the stored procedure here  
  @searchText VARCHAR(200)='',  
  @patch int = 0,  
 --Parameters which would help in sorting and paging  
  @pageSize int = 30,  
  @pageNumber int = 1,  
  @sortColumn varchar(500) = 'JournalId',   
  @sortOrder varchar (5) = 'DESC',  
  @getOnlyCount bit=0,  
  @totalCount int = 0 output   
AS  
BEGIN  
 DECLARE   
   
  @SelectClause varchar(3000),  
        @fromClause   varchar(7000),  
        @whereClause  varchar(4000),           
        @orderClause  varchar(4000),   
        @mainSelectQuery varchar(7000),          
        @rowNumberQuery varchar(7000),  
        @finalQuery varchar(7000),  
        -- used to add in conditions in WhereClause based on search criteria provided  
        @searchCriteria varchar(2000),  
        @filterCriteria varchar(200)='',  
        --variables for paging  
        @offset int,  
  @limit int,  
  @MSATTypeId int,  
  @ToBeArrangedStatusId int  
  --Paging Formula  
  SET @offset = 1+(@pageNumber-1) * @pageSize  
  SET @limit = (@offset + @pageSize)-1  
    
    
  --=====================Search Criteria===============================  
  SET @searchCriteria = 'V_Works.Works > 0 AND (V_Schedule.Arranged < V_Works.Works  OR V_Schedule.Arranged is NULL)And (V_AppointmentRecordedData.IsWorksRequired=1) AND PDR_JOURNAL.STATUSID NOT IN (select STATUSID FROM PDR_STATUS WHERE TITLE = ''Cancelled'')'  
    
  IF(@searchText != '' OR @searchText != NULL)  
  BEGIN        
   SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''') + ISNULL('' ''+P__PROPERTY.ADDRESS1, '''')  + ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''  
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_BLOCK.BLOCKNAME LIKE ''%' + @searchText + '%'''  
   SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_SCHEME.SCHEMENAME LIKE ''%' + @searchText + '%'')'  
  END   
    
  --================================= Filter Criteria================================  
  IF(@patch > 0  )  
  BEGIN  
  SET @filterCriteria = @filterCriteria + 'AND PDR_DEVELOPMENT.PATCHID = ' + CONVERT (VARCHAR,@patch)   
  END  
    
  --=======================Select Clause=============================================  
  SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')  
       ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref,  
        ISNULL(P__PROPERTY.HouseNumber, '''') + ISNULL('' ''+P__PROPERTY.ADDRESS1, '''')  + ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address   
   , P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY, ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,  
  ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,  
 PDR_JOURNAL.JOURNALID   
  ,CONVERT(nvarchar(50),T.TerminationDate, 103) as Termination   
  ,ISNULL(V_Works.Works,0) AS Works ,ISNULL(Arranged ,0)  as Arranged,C_TENANCY.TENANCYID as TenancyId  '  
    
    
  --============================From Clause============================================  
  SET @fromClause = CHAR(10) +'FROM V_AppointmentRecordedData  
  INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID= V_AppointmentRecordedData.InspectionJournalId  
  LEFT JOIN (Select COUNT(RequiredWorksId)as Works,InspectionJournalId from V_RequiredWorks WHERE (IsCanceled IS NULL OR IsCanceled=0) AND (V_RequiredWorks.ISMAJORWORKSREQUIRED is null or V_RequiredWorks.ISMAJORWORKSREQUIRED =0)  
  GROUP BY InspectionJournalId) AS V_Works ON   
  V_Works.InspectionJournalId=PDR_JOURNAL.JOURNALID  
  LEFT JOIN (Select COUNT(RequiredWorksId)as Arranged,InspectionJournalId from V_RequiredWorks WHERE IsScheduled=1  AND (IsCanceled IS NULL OR IsCanceled=0) AND (V_RequiredWorks.ISMAJORWORKSREQUIRED is null or V_RequiredWorks.ISMAJORWORKSREQUIRED =0)
   GROUP BY InspectionJournalId) AS V_Schedule  
  ON V_Schedule.InspectionJournalId = PDR_JOURNAL.JOURNALID  
  INNER JOIN (Select COUNT(RequiredWorksId)as BRS,InspectionJournalId from V_RequiredWorks WHERE (V_RequiredWorks.IsTenantWorks=0 OR V_RequiredWorks.IsTenantWorks IS NUll) AND (IsScheduled=0 OR IsScheduled IS NULL )  AND (IsCanceled IS NULL OR IsCanceled=0) AND (V_RequiredWorks.ISMAJORWORKSREQUIRED is null or V_RequiredWorks.ISMAJORWORKSREQUIRED =0)
   GROUP BY InspectionJournalId) AS V_BRS 
   ON V_BRS.InspectionJournalId = PDR_JOURNAL.JOURNALID 
  INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId  
  INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId    
  INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID 
	Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P__PROPERTY.PropertyID
	AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
	GROUP BY PROPERTYID) as CJournal
CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
  INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DevelopmentId = PDR_DEVELOPMENT.DevelopmentId  
  LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = P__PROPERTY.SchemeId   
  LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID =P__PROPERTY.BLOCKID   
  INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId= C_TENANCY.TENANCYID  
  INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID  
  INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID  
  '  
         
  --============================Order Clause==========================================  
  IF(@sortColumn = 'Ref')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID'    
     
  END  
    
  IF(@sortColumn = 'Address')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ 'Address'    
     
  END  
  IF(@sortColumn = 'Scheme')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ 'Scheme'    
     
  END  
    
  IF(@sortColumn = 'Termination')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ 'Termination'    
     
  END  
    
  IF(@sortColumn = 'Block')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ 'Block'    
     
  END  
  IF(@sortColumn = 'Works')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ 'Works'    
     
  END  
  IF(@sortColumn = 'Arranged')  
  BEGIN  
   SET @sortColumn = CHAR(10)+ 'Arranged'    
     
  END  
      
  SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder  
    
  --================================= Where Clause ================================  
    
  SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria  
    
  --===============================Main Query ====================================  
  Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause   
    
  --=============================== Row Number Query =============================  
  Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row   
        FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'  
    
  --============================== Final Query ===================================  
  Set @finalQuery  =' SELECT *  
       FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result   
       WHERE  
       Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)      
    
  --============================ Exec Final Query =================================  
 IF(@getOnlyCount=0)  
  BEGIN  
   print(@finalQuery)  
   EXEC (@finalQuery)  
  END  
    
  --========================================================================================  
  -- Begin building Count Query   
    
  Declare @selectCount nvarchar(4000),   
  @parameterDef NVARCHAR(500)  
    
  SET @parameterDef = '@totalCount int OUTPUT';  
  SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause  
    
  --print @selectCount  
  EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;  
      
  -- End building the Count Query  
  --========================================================================================   
END  