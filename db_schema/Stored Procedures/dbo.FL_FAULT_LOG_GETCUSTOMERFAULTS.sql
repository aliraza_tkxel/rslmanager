SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE    PROCEDURE dbo.FL_FAULT_LOG_GETCUSTOMERFAULTS
/* ===========================================================================
 '   NAME:           FL_FAULT_LOG_GETCUSTOMERFAULTS
 '   DATE CREATED:   20 Oct. 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve customer faults against specified CustomerID and Response Status
 '   IN:             @customerID
 '   IN:             @ResponseStatus
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
@CustomerID INT,
@FaultStatus VARCHAR(50)
)
AS

DECLARE
@WhereClause VARCHAR(8000),
@SelectClause VARCHAR(8000),
@OrderClause VARCHAR(8000)


SET @OrderClause=' Order By F.SubmitDate DESC, F.FaultLogID DESC'

SET @SelectClause = 'Select F.FaultLogID,F.CustomerId,F.FaultID,F.FaultBasketID,F.SubmitDate,FD.ElementID,F.ContractorID,F.ResponseTime,FS.Status,FD.Description,FD.PriorityID,'+ CHAR(10) + 
' case FP.Days'+ CHAR(10) +
' when 1 then ''Day(s)'' when 0 then ''Hour(s)'' End As PType' + CHAR(10) +
' FROM ' + CHAR(10) +
' FL_FAULT_LOG AS F '+ CHAR(10) +
' INNER JOIN FL_FAULT_STATUS AS FS'+ CHAR(10) + 
' ON F.StatusID = FS.StatusId'+ CHAR(10) + 
' INNER JOIN FL_FAULT AS FD'+ CHAR(10) + 
' ON F.FaultID = FD.FaultID'+ CHAR(10) + 
' INNER JOIN FL_FAULT_PRIORITY AS FP'+ CHAR(10) + 
' ON FD.PriorityID = FP.PriorityID'+ CHAR(10) + 
' Where F.CustomerId ='+ CHAR(10) + Convert(varchar, @CustomerID)
/*print(@SelectClause + @OrderClause)
EXEC(@SelectClause + @OrderClause)*/

IF @FaultStatus = '0' 
	EXEC(@SelectClause + @OrderClause)
ELSE
	BEGIN
			SET @SelectClause= @SelectClause + 'and F.StatusID='+ CHAR(10) + Convert(varchar, @FaultStatus)
			EXEC(@SelectClause + @OrderClause)
	END













GO
