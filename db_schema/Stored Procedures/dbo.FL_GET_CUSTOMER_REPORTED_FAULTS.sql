
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[FL_GET_CUSTOMER_REPORTED_FAULTS]
/* ===========================================================================
 '   NAME:           FL_GET_CUSTOMER_REPORTED_FAULTS
 '   DATE CREATED:   20 Oct, 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve customer faults against specified CustomerID
 
 '   IN:             Customer ID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
	@CustomerID INT
)
AS

/*, SO.NAME AS Contractor*/

	SELECT FL.FaultLogID, FL.CustomerId, FL.FaultID, FL.SubmitDate AS OrderDate, FP.PriorityName, DueDate, FA.Description, FA.PriorityID, FE.ElementName, FR.AreaName, FC.LocationName, JobSheetNumber AS JSNumber, FL.Quantity, FL.ProblemDays, FL.RecuringProblem, FL.CommunalProblem, FL.Notes, FB.PreferredContactDetails,
	CASE SO.NAME WHEN NULL THEN 'N/A' ELSE SO.NAME END AS Contractor,
	(CONVERT(VARCHAR, FP.ResponseTime) + ' ' + CASE FP.Days WHEN 1 THEN  'Day(s)'  WHEN 0 THEN 'Hour(s)' END) AS FResponseTime,
	FP.ResponseTime As OnlyResponseTime,
	CASE FP.Days WHEN 1 THEN  'Day' WHEN 0 THEN 'Hour' END AS OnlyDayHour,
	CASE FL.Recharge WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' ELSE 'No' END AS Recharge,
	CASE FA.Recharge WHEN 1 THEN CONVERT(VARCHAR,FA.Gross) WHEN 0 THEN 'N/A' END AS FRecharge
	, (	SELECT PARL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL
			FROM ( 
			SELECT 
								ISNULL(MAX(PAL.RISKLEVELID),0) AS MAX_RISKLEVELID
					  FROM      P__PROPERTY PP
								INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL PPARL ON PP.PROPERTYID = PPARL.PROPERTYID
								INNER JOIN P_PROPERTY_ASBESTOS_RISK PPAR ON PPAR.PROPASBLEVELID = PPARL.PROPASBLEVELID
								INNER JOIN P_ASBRISKLEVEL PAL ON PAL.ASBRISKLEVELID = PPARL.ASBRISKLEVELID
								WHERE PP.PROPERTYID = FJL.PROPERTYID
					) SUB ,
					(SELECT ASBRISKLEVELDESCRIPTION, RISKLEVELID FROM P_ASBRISKLEVEL UNION SELECT 'None', 0) AS PARL
			WHERE   SUB.MAX_RISKLEVELID = PARL.RISKLEVELID) AS AsbestosRisk		
	FROM FL_FAULT_LOG AS FL 		
	INNER JOIN FL_FAULT AS FA ON FL.FaultID = FA.FaultID
	INNER JOIN FL_ELEMENT AS FE ON FA.ElementID = FE.ElementID
	INNER JOIN FL_AREA AS FR ON FE.AreaID = FR.AreaID
	INNER JOIN FL_LOCATION AS FC ON FR.LocationID = FC.LocationID		
	INNER JOIN FL_FAULT_PRIORITY AS FP ON FA.PriorityID = FP.PriorityID
	INNER JOIN FL_FAULT_BASKET AS FB ON FL.FaultBasketID = FB.FaultBasketID
	LEFT JOIN S_ORGANISATION AS SO ON FL.ORGID = SO.ORGID
	LEFT JOIN FL_FAULT_JOURNAL FJL ON FL.FaultLogId = FJL.FaultLogID
	WHERE FL.FaultBasketID = @CustomerID
	ORDER BY FL.FaultLogID DESC
GO
