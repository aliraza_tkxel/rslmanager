USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyFuelTypes]    Script Date: 22/06/2018 15:09:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.AS_GetPropertyFuelTypes') IS NULL
 EXEC('CREATE PROCEDURE dbo.AS_GetPropertyFuelTypes AS SET NOCOUNT ON;')
GO


ALTER PROCEDURE [dbo].[AS_GetPropertyFuelTypes] 
( 
	-- Add the parameters for the stored procedure here
	@propertyId varchar(20)
)
AS
BEGIN
	select PV.ValueDetail as ValueDetail from PA_HeatingMapping HM
	INNER Join PA_PARAMETER_VALUE PV on PV.ValueID = Hm.HeatingType
	where PropertyId = @propertyId
END