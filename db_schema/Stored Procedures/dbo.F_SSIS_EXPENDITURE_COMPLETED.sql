-- Stored Procedure

ALTER PROCEDURE [dbo].[F_SSIS_EXPENDITURE_COMPLETED]
AS 

--+---------------------------------------------------------------------------
--+ Calculate the start and the end of the current financial year
--+---------------------------------------------------------------------------

    DECLARE @YearStart DATETIME
    DECLARE @YearEnd DATETIME

    SET @YearStart = [dbo].[F_GET_FINANCIAL_YEAR_START_DATE]  (GETDATE())
    SET @YearEnd = DATEADD(YY, 1, @YearStart)

--+---------------------------------------------------------------------------
--+ Gather and return data
--+---------------------------------------------------------------------------

    SELECT  ISNULL(CAST(PO.ORDERID AS VARCHAR), '') AS [Order No] ,
            ISNULL(CONVERT (VARCHAR, PIDATE, 103), '') AS Ordered ,
            ISNULL(E.DESCRIPTION, '') AS Expenditure ,
            ISNULL(PI.ITEMNAME, '') AS Item ,
            ISNULL(CONVERT (VARCHAR, PI.GROSSCOST, 1), '') AS Cost ,
            ISNULL(CASE PITYPE
                     WHEN 5 THEN 'Paid Tenant Reimbursement'
                     WHEN 4 THEN 'Paid Staff Expense'
                     WHEN 3 THEN 'Paid Petty Cash'
                     ELSE PS.POSTATUSNAME
                   END, '') AS [PO Status] ,
            ISNULL(CS.DESCRIPTION, '') AS [Status] ,
            ISNULL(O.NAME, '') AS Supplier ,
            ISNULL(P.HOUSENUMBER, '') + ' ' + ISNULL(P.ADDRESS1, '') + ' '
            + ISNULL(P.ADDRESS2, '') + ' ' + ISNULL(P.TOWNCITY, '') + ' '
            + ISNULL(P.POSTCODE, '') AS [Property Address] ,
            ISNULL(DEV.SCHEMECODE, '') AS [Scheme Code] ,
            AC.ACCOUNTNUMBER [NL Account]
    FROM    F_PURCHASEITEM PI
            LEFT JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID
            LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID
            LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID
            LEFT JOIN P_SCHEME DEV ON DEV.SCHEMEID = P.SCHEMEID
            LEFT JOIN dbo.P_WOTOREPAIR WOR ON WOR.ORDERITEMID = PI.ORDERITEMID
            LEFT JOIN dbo.C_JOURNAL CJ ON CJ.JOURNALID = WOR.JOURNALID
            LEFT JOIN dbo.C_STATUS CS ON CS.ITEMSTATUSID = CJ.CURRENTITEMSTATUSID
            LEFT JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID
            LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID
            INNER JOIN F_EXPENDITURE E ON E.EXPENDITUREID = PI.EXPENDITUREID
            LEFT JOIN NL_ACCOUNT AC ON E.QBDEBITCODE = AC.ACCOUNTNUMBER
            INNER JOIN F_HEAD H ON H.HEADID = E.HEADID
            INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = H.COSTCENTREID
            LEFT JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = PO.USERID
    WHERE   PI.PITYPE <> 6
            AND PI.ACTIVE = 1
            AND ( ( CC.COSTCENTREID = 23 -- Communal Furnishings/Equipment
                    AND H.HEADID = 139 -- Communal Furnishings/Equipment

                  )
                  OR ( CC.COSTCENTREID = 22 -- Tenants Furnishings/Equipment
                       AND H.HEADID = 138 -- Tenants Furnishings/Equipment

                     )
                  OR ( CC.COSTCENTREID = 21 -- Commercial Properties
                       AND H.HEADID = 94 -- Commercial Properties

                     )
                  OR ( CC.COSTCENTREID = 13 -- Services
                       AND H.HEADID = 510 -- Repairs

                     )
                  OR ( CC.COSTCENTREID = 13 -- Services
                       AND H.HEADID = 43 -- Other Communal Costs

                     )
                  OR ( CC.COSTCENTREID = 4 -- Admin Premises
                       AND H.HEADID = 54 -- General Repair and Maintenance

                     )
                  OR ( CC.COSTCENTREID = 11 -- Estate Maintenance
                       AND H.HEADID = 535 -- Revenue Repair and Improvement

                     )
                  OR ( CC.COSTCENTREID = 11 -- Estate Maintenance
                       AND H.HEADID = 24 -- Reactive Repairs

                     )
                  OR ( CC.COSTCENTREID = 11 -- Estate Maintenance
                       AND H.HEADID = 11 -- Major Repairs And Improvements

                     )
                  OR ( CC.COSTCENTREID = 11 -- Estate Maintenance
                       AND H.HEADID = 25 -- Cyclical Repairs

                     )
                  OR ( CC.COSTCENTREID = 11 -- Estate Maintenance
                       AND H.HEADID = 13 -- Aids And Adaptations

                     )
                )
            AND PIDATE >= @YearStart
            AND PIDATE < @YearEnd
            --AND PS.POSTATUSID NOT IN ( 1, 3, 4, 0, 6, 16 )
            AND 
            (
				(PS.POSTATUSID NOT IN ( 1, 3, 4, 0, 6, 16 )) -- Complete
				OR
				(
					PS.POSTATUSID IN ( 1, 3, 4, 0, 6, 16 ) -- Ongoing 
					AND
					CJ.CURRENTITEMSTATUSID = 10 -- Approved
				)
			)

    ORDER BY PO.ORDERID DESC



GO