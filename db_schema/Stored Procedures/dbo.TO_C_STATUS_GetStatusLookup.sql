SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.TO_C_STATUS_GetStatusLookup
/* ===========================================================================
 '   NAME:           TO_C_STATUS_GetStatusLookup
 '   DATE CREATED:   20 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get nature C_STATUS table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT ITEMSTATUSID AS id,DESCRIPTION AS val
	FROM C_STATUS
	WHERE ITEMSTATUSID BETWEEN 26 AND 29
	ORDER BY DESCRIPTION ASC



GO
