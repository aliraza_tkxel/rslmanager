USE [RSLBHALive]

-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <30/10/2015>
-- Description:	<Save work completion record>
-- =============================================

IF OBJECT_ID('dbo.PDR_SaveCompletionRecord') IS NULL 
 EXEC('CREATE PROCEDURE dbo.PDR_SaveCompletionRecord AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[PDR_SaveCompletionRecord]
@FaultLogID int,
@FaultRepairIDList varchar(8000),
@Time VARCHAR(50),
@Date datetime,
@UserID Int,
@FollowOnStatus bit,
@FollowOnNotes nvarchar(1000),
@RepairNotes nvarchar(1000),
@RESULT  INT = 1  OUTPUT 
AS
BEGIN

DECLARE @statusId int
select @statusId =  faultStatusId from FL_Fault_status where Description ='Complete'


	--BEGIN TRAN
DECLARE @FaultRepairId INT,@Success int	
CREATE TABLE #tempFaultRepairId(FaultRepairId INT )
INSERT INTO #tempFaultRepairId (FaultRepairId)
SELECT COLUMN1
FROM dbo.SPLIT_STRING(@FaultRepairIDList, ',')

DECLARE @ItemToInsertCursor CURSOR
--Initialize cursor
SET @ItemToInsertCursor = CURSOR FAST_FORWARD FOR SELECT
	FaultRepairId
FROM #tempFaultRepairId;
   --Open cursor
   OPEN @ItemToInsertCursor
---fetch row from cursor
   FETCH NEXT FROM @ItemToInsertCursor INTO @FaultRepairId
 
   
    ---Iterate cursor to get record row by row
WHILE @@FETCH_STATUS = 0
BEGIN
Set @Success = 0	
INSERT INTO FL_CO_FAULTLOG_TO_REPAIR (FaultLogID,FaultRepairListID, InspectionDate, InspectionTime, UserId,Notes )
VALUES (@FaultLogID, @FaultRepairId, @Date,@Time,@UserID,@RepairNotes)
	
	
FETCH NEXT FROM @ItemToInsertCursor INTO @FaultRepairId
  
END
--close & deallocate cursor		
CLOSE @ItemToInsertCursor

DEALLOCATE @ItemToInsertCursor
IF (@FollowOnStatus = 1)
BEGIN

INSERT INTO FL_FAULT_FOLLOWON (FaultLogID,RecordedOn,FollowOnNotes,IsFollowOnScheduled) values(@FaultLogID,GETDATE(),@FollowOnNotes,0) 	
END

UPDATE	FL_FAULT_LOG 
SET		statusId = @statusId
WHERE	faultLogId =@FaultLogID

IF OBJECT_ID('tempdb..#tempFaultRepairId') IS NOT NULL
DROP TABLE #tempFaultRepairId
 Set @Success = 1
	
	Select @RESULT= @Success
	
END




