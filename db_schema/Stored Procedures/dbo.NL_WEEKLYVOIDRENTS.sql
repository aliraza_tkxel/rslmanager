SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[NL_WEEKLYVOIDRENTS]( 

	@THEDATE SMALLDATETIME
)
 AS

DECLARE @TRANSACTIONDATE SMALLDATETIME
DECLARE @RENT MONEY
DECLARE @SERVICES MONEY
DECLARE @COUNCILTAX MONEY
DECLARE @WATERRATES MONEY         
DECLARE @INELIGSERV MONEY
DECLARE @SUPPORTEDSERVICES MONEY
DECLARE @GARAGE MONEY
DECLARE @TOTALRENT MONEY

DECLARE @ACC_RENT INT
DECLARE @VACC_RENT INT
DECLARE @TRANSACTIONTYPE INT
DECLARE @REVERSEDATE INT

DECLARE @DESC VARCHAR(1000)
DECLARE @TXNID INT
DECLARE @SEQUENCE INT

SET @TRANSACTIONTYPE = 10 -- VOID RENTS
SET @ACC_RENT = 		DBO.GET_ACCOUNT_LIST_ID('1620') -- RENT
SET @VACC_RENT = 		DBO.GET_ACCOUNT_LIST_ID('2085') -- RENT


--WE INCLUDE STATUS 2(LET) BECUASE THERE ARE SOME BAD PROPERTIES SETUP.
DECLARE VOIDRENT CURSOR FAST_FORWARD FOR     
	SELECT CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103), SUM(ISNULL(RENT,0)), SUM(ISNULL(SERVICES,0)), SUM(ISNULL(COUNCILTAX,0)), SUM(ISNULL(WATERRATES,0)), SUM(ISNULL(INELIGSERV,0)), 
		SUM(ISNULL(SUPPORTEDSERVICES,0)), SUM(ISNULL(GARAGE,0)), SUM(ISNULL(TOTALRENT,0))
	FROM F_RENTJOURNAL_WEEKLYVOIDS
	WHERE STATUS IN (1,2) AND CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @THEDATE
	GROUP BY CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103)
OPEN VOIDRENT
FETCH NEXT FROM VOIDRENT
	INTO @TRANSACTIONDATE, @RENT, @SERVICES, @COUNCILTAX, @WATERRATES, @INELIGSERV, @SUPPORTEDSERVICES, @GARAGE, @TOTALRENT
WHILE @@FETCH_STATUS = 0
BEGIN
	
	SET @REVERSEDATE = CAST((CAST(YEAR(@TRANSACTIONDATE) AS VARCHAR) + REPLICATE('0', 2-LEN(CAST(MONTH(@TRANSACTIONDATE) AS VARCHAR))) + CAST(MONTH(@TRANSACTIONDATE) AS VARCHAR) + REPLICATE('0', 2-LEN(CAST(DAY(@TRANSACTIONDATE) AS VARCHAR))) + CAST(DAY(@TRANSACTIONDATE) AS VARCHAR)) AS INT)
	INSERT INTO NL_JOURNALENTRY (TRANSACTIONTYPE, TRANSACTIONID, TXNDATE)
	VALUES 	(@TRANSACTIONTYPE, @REVERSEDATE, @TRANSACTIONDATE)
	
	SET @TXNID = SCOPE_IDENTITY()
	SET @DESC = 'Weekly Void Rent for ''' + CONVERT(VARCHAR, @TRANSACTIONDATE, 103) + ''''

	SET @SEQUENCE = 1 -- VOID MASTER RENT
	INSERT INTO NL_JOURNALENTRYDEBITLINE (TXNID, ACCOUNTID, EDITSEQUENCE, TXNDATE, AMOUNT, DESCRIPTION)
	VALUES (@TXNID, @VACC_RENT, @SEQUENCE, @TRANSACTIONDATE, @TOTALRENT, @DESC)	

	SET @SEQUENCE = @SEQUENCE + 1 --RENT
	INSERT INTO NL_JOURNALENTRYCREDITLINE (TXNID, ACCOUNTID, EDITSEQUENCE, TXNDATE, AMOUNT, DESCRIPTION)
	VALUES (@TXNID, @ACC_RENT, @SEQUENCE, @TRANSACTIONDATE, ISNULL(@RENT,0)+ ISNULL(@SERVICES,0)+ ISNULL(@COUNCILTAX,0)+ISNULL(@WATERRATES,0)+ISNULL(@INELIGSERV,0)+ISNULL(@SUPPORTEDSERVICES,0)+ISNULL(@GARAGE,0),@DESC)	

	FETCH NEXT FROM VOIDRENT
		INTO @TRANSACTIONDATE, @RENT, @SERVICES, @COUNCILTAX, @WATERRATES, @INELIGSERV, @SUPPORTEDSERVICES, @GARAGE, @TOTALRENT
END
CLOSE VOIDRENT
DEALLOCATE VOIDRENT
GO
