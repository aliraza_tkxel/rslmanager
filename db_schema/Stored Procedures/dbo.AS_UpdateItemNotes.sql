-- =============================================    
--EXEC AS_UpdateItemNotes    
--  @propertyId = N'A010060001',    
--  @itemId = 1,    
--  @note = N'New',    
-- Create date: <Create Date,,10/10/2018>    
-- Description: <Description,,Update the Note for property and Item>    
-- WebPage: PropertyRecord.aspx => Attributes Tab    
-- =============================================    
IF OBJECT_ID('dbo.AS_UpdateItemNotes') IS NULL 
EXEC('CREATE PROCEDURE dbo.AS_UpdateItemNotes AS SET NOCOUNT ON;') 
GO  
ALTER PROCEDURE [dbo].[AS_UpdateItemNotes] (    
@notesId int,    
@note varchar(500),
@showInScheduling bit=1,  
@showOnApp bit=0     )    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    

	SET NOCOUNT ON; 
 
	Declare @propertyId nvarchar(50), @itemId Int, @schemeId int, @blockId int, @createdBy int, @heatingMappingId int

	SELECT @propertyId=PROPERTYID, @itemId=ItemId, @schemeId=SchemeId, @blockId=BlockId,
	 @createdBy=CreatedBy, @heatingMappingId=HeatingMappingId
	FROM PA_PROPERTY_ITEM_NOTES
	WHERE SID = @notesId

	UPDATE PA_PROPERTY_ITEM_NOTES
	SET Notes = @note, ShowInScheduling = @showInScheduling, ShowOnApp = @showOnApp
	WHERE SID = @notesId
 
	INSERT INTO [dbo].[PA_PROPERTY_ITEM_NOTES_HISTORY](
	[SID],
	[PROPERTYID],
	[ItemId],
	[Notes],
	[CreatedOn],
	[CreatedBy],
	[SchemeId],
	[BlockId],
	[ShowInScheduling],
	[ShowOnApp],
	[HeatingMappingId],
	[NotesStatus]
	)
	VALUES
	(@notesId,
	@propertyId,
	@itemId,
	@note,
	GETDATE(),
	@createdBy,
	@schemeId,
	@blockId,
	@showInScheduling,
	@showOnApp,
	@heatingMappingId,
	'Updated' )
 
END 