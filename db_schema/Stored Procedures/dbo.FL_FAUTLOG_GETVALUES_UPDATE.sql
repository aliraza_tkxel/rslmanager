SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















CREATE PROCEDURE dbo.FL_FAUTLOG_GETVALUES_UPDATE
/* ===========================================================================
 '   NAME:          FL_FAUTLOG_GETVALUES_UPDATE
 '   DATE CREATED:   27th Feb 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a faultLog against a particular faultLogID from FL_FAULT and TempFault Table table which will be used to update 
 '		     a fault values
 '   IN:            @FaultlogId
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@FaultLogId as Int
AS
		SELECT  FL_FAULT.FAULTID,FL_FAULT.NetCost, FL_FAULT.Recharge, FL_FAULT_PRIORITY.ResponseTime , Case FL_FAULT_PRIORITY.Days WHEN 1 Then 'Days' when 0 then 'Hours' end as responseDays
	 
	FROM  FL_FAULT   INNER JOIN FL_FAULT_PRIORITY
		ON 
		FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
				
WHERE     	(FL_FAULT.FaultID = (select FaultID from FL_FAULT_LOG where FaultLogId=@FaultLogId))










GO
