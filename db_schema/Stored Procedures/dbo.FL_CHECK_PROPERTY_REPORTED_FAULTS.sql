SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[FL_CHECK_PROPERTY_REPORTED_FAULTS]
	/* ===========================================================================
 '   NAME:           FL_CHECK_PROPERTY_REPORTED_FAULTS
 '   DATE CREATED:   7th July, 2009
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get already reported faults against property
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 
 @CustomerId   int	=  NULL,
 @PropertyId varchar(100) = NULL,
 @TenancyId   int	=  NULL

AS
	SELECT FL_AREA.AreaName, FL_ELEMENT.ElementName, FL_FAULT.Description as FaultName, FL_FAULT_STATUS.Description AS StatusName, FL_FAULT_LOG.SubmitDate AS Date
	From FL_FAULT_LOG 
	INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID	
	INNER JOIN FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID	
	INNER JOIN FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID							
	INNER JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerId=C__CUSTOMER.CUSTOMERID	
	INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.CUSTOMERID=C__CUSTOMER.CUSTOMERID AND C_CUSTOMERTENANCY.ENDDATE IS NULL	
	INNER JOIN C_TENANCY ON C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL	
	INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID= C_TENANCY.PROPERTYID	
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
	WHERE 
	-- C__CUSTOMER.CustomerID =@CustomerId 
	--AND P__PROPERTY.PropertyID = @PropertyId 
	C_TENANCY.TENANCYID =  @TenancyId
	AND FL_FAULT_STATUS.Description IN ('Repair Reported', 'Assigned To Contractor', 'Appointment To Be Arranged', 'Appointment Arranged', 'Appointment Rearranged', 'Appointment Cancelled', 'Post Inspection')



/****** Object:  StoredProcedure [dbo].[FL_GET_FAULT_REPAIR_LIST]    Script Date: 08/25/2009 14:03:14 ******/
SET ANSI_NULLS ON

GO
