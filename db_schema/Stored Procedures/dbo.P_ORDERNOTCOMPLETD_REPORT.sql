USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_ORDERNOTCOMPLETD_REPORT]    Script Date: 23/01/2018 16:12:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
/*
DECLARE @TT VARCHAR(20)
SET @TT = '1 APRIL 2005'
SELECT DATEADD(YY, 1, @TT)*/

-- EXEC  P_ORDERNOTCOMPLETD_REPORT 1,8,-1,-1
-- EXEC  P_OREDRNOTCOMPLETD_REPORT_OLD 1,6,-1,-1
-- AMENDED BY PAUL TO INCLUDE SUPPLIER AND STATUS FILTER 9 MAY 2005
-- ONLY INCLUDE 
--	UNRECONCILED PURCHASEITEMS WITH AN ORDER DATE IN ACRRUAL FINANCIAL YEAR OR
-- 	RECONCILED ITEMS WITH A TAXDATE IN THE NEXT FINANCIAL YEAR
--EXEC P_ORDERNOTCOMPLETD_REPORT 1,9,-1,2
--***********************************************************************************

ALTER PROC [dbo].[P_ORDERNOTCOMPLETD_REPORT](@TASK INT, @YRANGE AS INT, @SUPPLIERID AS INT, @CSTATUS AS INT,@POTYPEID AS INT, @COMPANYID INT NULL)      
AS 
	SET NOCOUNT ON      
	DECLARE @FY AS INT  
	DECLARE @GETCURRENT_STARTDATE SMALLDATETIME
	DECLARE @GETCURRENT_ENDDATE  SMALLDATETIME

	SELECT @FY=YRANGE, @GETCURRENT_STARTDATE=YSTART, @GETCURRENT_ENDDATE=YEND FROM F_FISCALYEARS WHERE YRANGE=@YRANGE  

 
	IF @SUPPLIERID = -1 
		SET @SUPPLIERID = NULL

	IF @CSTATUS = -1  
		SET @CSTATUS = NULL

	IF @POTYPEID = -1  
		SET @POTYPEID = NULL

	DECLARE  @TEMP_YEAccruals TABLE(	
		ORDERID INT , 
		ITEMNAME VARCHAR(200), 
		ORDERITEMID INT,
		ITEMCOST MONEY, 
		NAME VARCHAR(100),
		DESCRIPTION VARCHAR(100),
		ORDERCOST MONEY,
		POTYPE VARCHAR(60),
		PIDATE SMALLDATETIME,
		TAXDATE SMALLDATETIME,
		COMPLETIONDATE SMALLDATETIME
	)
	--CREATE CLUSTERED INDEX IX_temp ON #TEMP_YEAccruals (ORDERID ASC, POTYPE ASC, NAME ASC)
declare @MINSUPPLIER    int,
        @MINITEMSTATUSID    int

declare @MAXSUPPLIER    int,
        @MAXITEMSTATUSID    int

SELECT @MINSUPPLIER = MIN(ORGID), @MAXSUPPLIER = MAX(ORGID) FROM S_ORGANISATION
SELECT @MINITEMSTATUSID = MIN(ITEMSTATUSID), @MAXITEMSTATUSID = MAX(ITEMSTATUSID) FROM C_STATUS


	INSERT INTO @TEMP_YEAccruals  
	SELECT PO.ORDERID, PI.ITEMNAME, PI.ORDERITEMID, PI.GROSSCOST, O.NAME,       
	     	ISNULL(S.DESCRIPTION,'Not Applicable') AS DESCRIPTION,
		-1 AS ORDERCOST, PT.POTYPENAME AS POTYPE,
	     	CONVERT(VARCHAR, PI.PIDATE, 103) AS PIDATE,
	     	CONVERT(VARCHAR, INV.TAXDATE, 103) AS TAXDATE,
		COMP.COMPLETIONDATE
	FROM    F_PURCHASEORDER PO
		INNER JOIN (SELECT [NAME], ORGID FROM S_ORGANISATION) O ON PO.SUPPLIERID = O.ORGID 
		INNER JOIN F_PURCHASEITEM PI ON (PO.ORDERID = PI.ORDERID)
		INNER JOIN (SELECT EXPENDITUREID, QBDEBITCODE FROM F_EXPENDITURE) EXP ON PI.EXPENDITUREID=EXP.EXPENDITUREID  
		INNER JOIN NL_ACCOUNT NL ON  EXP.QBDEBITCODE=NL.ACCOUNTNUMBER   
		LEFT JOIN P_WOTOREPAIR W ON PI.ORDERITEMID = W.ORDERITEMID
		--LEFT JOIN (SELECT JOURNALID FROM C_JOURNAL) J ON W.JOURNALID = J.JOURNALID
		--LEFT JOIN (SELECT REPAIRHISTORYID, JOURNALID, ITEMSTATUSID FROM C_REPAIR) R ON (W.JOURNALID = R.JOURNALID 
		--												AND R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = R.JOURNALID AND LASTACTIONDATE <= @GETCURRENT_ENDDATE))
		--												AND R.ITEMSTATUSID BETWEEN  isnull(@CSTATUS, @MINITEMSTATUSID) AND  isnull(@CSTATUS, @MAXITEMSTATUSID)
		LEFT JOIN (
					SELECT I.REPAIRHISTORYID, I.JOURNALID, I.ITEMSTATUSID FROM C_REPAIR I
					WHERE I.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = I.JOURNALID AND LASTACTIONDATE <= @GETCURRENT_ENDDATE)
				  )	 R ON R.JOURNALID =W.JOURNALID 
		LEFT JOIN C_REPAIRCOMPLETION COMP ON COMP.JOURNALID = R.JOURNALID
							AND COMP.C_COMPLETIONID = (SELECT MAX(C_COMPLETIONID) FROM C_REPAIRCOMPLETION WHERE JOURNALID = R.JOURNALID)		
							AND COMP.COMPLETIONDATE < @GETCURRENT_ENDDATE
		LEFT JOIN C_STATUS S ON S.ITEMSTATUSID = R.ITEMSTATUSID
		LEFT JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = PI.ORDERITEMID
		LEFT JOIN (SELECT TAXDATE, INVOICEID, SUPPLIERID FROM F_INVOICE) INV ON INV.INVOICEID = FOTI.INVOICEID
		LEFT JOIN F_POTYPE PT ON PT.POTYPEID = PO.POTYPE
	WHERE    
		PI.ACTIVE = 1 AND  (PI.PITYPE = 1 OR PI.PITYPE = 2) AND PI.PIDATE >= @GETCURRENT_STARTDATE  AND PI.PIDATE <= @GETCURRENT_ENDDATE
		AND PI.ORDERITEMID NOT IN (SELECT POORDERITEMITD FROM NL_YEARENDACCRAL_DTL)
		AND	PO.SUPPLIERID BETWEEN isnull(@SUPPLIERID, @MINSUPPLIER) AND  isnull(@SUPPLIERID, @MAXSUPPLIER)
		AND	(
			  (PI.PISTATUS in (0, 1, 2, 3, 4, 5, 6, 7, 8, 17, 18)) OR (INV.TAXDATE >= DATEADD(YY, 1, @GETCURRENT_STARTDATE) AND PI.PISTATUS >= 9)
			)
		AND (R.ITEMSTATUSID= COALESCE(@CSTATUS, R.ITEMSTATUSID) OR (R.ITEMSTATUSID IS NULL) )
		AND (PO.POTYPE=COALESCE(@POTYPEID,PO.POTYPE))
		AND ISNULL(COMPANYID,1) = @COMPANYID
	GROUP 	BY PO.ORDERID, PI.ITEMNAME, PI.ORDERITEMID, PI.GROSSCOST, 
		O.NAME, S.DESCRIPTION, PI.PISTATUS,
		PT.POTYPENAME, INV.TAXDATE, PI.PIDATE,COMP.COMPLETIONDATE
	ORDER 	BY PO.ORDERID

	INSERT INTO @TEMP_YEAccruals (ORDERID, ORDERITEMID, ORDERCOST, POTYPE, NAME)     
	SELECT 	ORDERID, -1 AS ORDERITEMID, ISNULL(SUM(ITEMCOST),0),  POTYPE, NAME
	FROM  @TEMP_YEAccruals   
	GROUP BY ORDERID, POTYPE, NAME


IF @TASK=1-- REPORT      
BEGIN     
	SELECT PIDATE, TAXDATE, ORDERID,ITEMNAME, POTYPE, ORDERITEMID, NAME AS SUPPLIER, DESCRIPTION AS STATUS, 
	CASE WHEN ITEMCOST IS NULL THEN '' ELSE CAST(ITEMCOST AS VARCHAR) END AS ITEMCOST,
	ORDERCOST,
	CASE WHEN ORDERCOST=-1 THEN '<INPUT TYPE="CHECKBOX" NAME="CHKORDERID" VALUE="'+ CAST(ORDERITEMID AS VARCHAR) + '"
	>'  ELSE ''  END CHKBOX,
	CASE WHEN ORDERITEMID=-1 THEN '<B>PO </B>'+ RIGHT('000000000'+ CAST(ORDERID AS VARCHAR),7) ELSE '' END AS PO,
	CASE WHEN  DESCRIPTION= 'In Progress' AND COMPLETIONDATE IS NOT NULL THEN 1 ELSE 0 END AS COMPLETED_IN_NEXT_FISCAL_YEAR
	FROM @TEMP_YEAccruals 
	ORDER BY ORDERID, ORDERITEMID ASC    
END
IF @TASK=2-- SUM      
BEGIN      
	SELECT ISNULL(SUM(ISNULL(ITEMCOST,0)),0) AS COST, ISNULL(COUNT(*),0) AS THECOUNT FROM @TEMP_YEAccruals 
END      
       



