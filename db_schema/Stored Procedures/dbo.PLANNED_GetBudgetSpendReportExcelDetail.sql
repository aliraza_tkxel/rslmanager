SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
--EXEC	@return_value = [dbo].[PLANNED_GetBudgetSpendReportExcelDetail]
--		@replacementYear = N'1934',
--		@schemeId = -1,
--		@searchText = ''
-- Author:		<Ahmed Mehmood>
-- Create date: <3/12/2014>
-- Description:	<Get Budget Spend Report Excel Detail>
-- Web Page: BudgetSpendReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetBudgetSpendReportExcelDetail]
( 
	-- Add the parameters for the stored procedure here

		@replacementYear varchar(10),
		@schemeId int,
		@searchText varchar(100)
)
AS
BEGIN
DECLARE
	 
		@SelectClause varchar(6000),
        @forecastFromClause   varchar(1500),
        @statusFromClause varchar(2000),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(500),	
        @mainSelectQuery varchar(6000)       
        ,@sortColumn varchar(500)
        ,@sortOrder varchar (5)
        ,@searchCriteria varchar(1500)

		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 AND YEAR(PID.DueDate) = ' + CONVERT(NVARCHAR(10), @replacementYear) 							
		
		IF @schemeId != -1 
		BEGIN
			SET @searchCriteria = @searchCriteria + ' AND P.DEVELOPMENTID = ' + CONVERT(NVARCHAR(10), @schemeId) 		
		END
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PC.COMPONENTNAME LIKE ''%' + @searchText + '%'''
		END	
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT	DISTINCT P.PROPERTYID 
							, ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'' '') AS Address
							,PC.COMPONENTID								
							,CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int) AS HouseNumber'
		
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		
	
		SET @forecastFromClause =	  CHAR(10) +'  FROM	PA_PROPERTY_ITEM_DATES AS PID
													INNER JOIN P__PROPERTY AS P ON PID.PROPERTYID = P.PROPERTYID
													INNER JOIN PLANNED_COMPONENT PC ON PID.PLANNED_COMPONENTID = PC.COMPONENTID  '
													
		SET	@statusFromClause = '	FROM		PLANNED_JOURNAL AS PJ
												INNER JOIN PLANNED_STATUS AS PS ON PJ.STATUSID = PS.STATUSID
												INNER JOIN P__PROPERTY AS P ON PJ.PROPERTYID = P.PROPERTYID
												INNER JOIN PLANNED_COMPONENT PC ON PJ.COMPONENTID = PC.COMPONENTID
												INNER JOIN (SELECT PLANNED_JOURNAL_HISTORY.JOURNALID,MAX(PLANNED_JOURNAL_HISTORY.CREATIONDATE ) DueDate
															FROM PLANNED_JOURNAL_HISTORY  
															GROUP BY PLANNED_JOURNAL_HISTORY.JOURNALID ) AS PID  ON PJ.JOURNALID = PID.JOURNALID   '

			-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
				
	
		SET @sortColumn = CHAR(10)+ ' HouseNumber'  		
		SET @sortOrder = CHAR(10)+ 'ASC'
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		
		
		--========================================================================================
		--							FORECAST DATA
		--========================================================================================
		
		Set @mainSelectQuery = @selectClause + @forecastFromClause + @whereClause + @orderClause 
		PRINT @mainSelectQuery
		EXEC (@mainSelectQuery)	
		
		--========================================================================================
		--							APPROVED DATA
		--========================================================================================
		Set @mainSelectQuery = @selectClause + @statusFromClause + @whereClause + ' AND (PS.TITLE = ''To be Arranged'' OR PS.TITLE = ''Arranged'' OR PS.TITLE = ''Completed'' ) ' + @orderClause 
		PRINT @mainSelectQuery
		EXEC (@mainSelectQuery)	
		
		--========================================================================================
		--							ARRANGED DATA
		--========================================================================================
		Set @mainSelectQuery = @selectClause + @statusFromClause + @whereClause + ' AND ( PS.TITLE = ''Arranged'' ) ' + @orderClause 
		PRINT @mainSelectQuery
		EXEC (@mainSelectQuery)	
		
		--========================================================================================
		--							COMPLETED DATA
		--========================================================================================
		
		Set @mainSelectQuery = @selectClause + @statusFromClause + @whereClause + ' AND ( PS.TITLE = ''Completed'' ) ' + @orderClause 
		PRINT @mainSelectQuery
		EXEC (@mainSelectQuery)	
					
																										
		-- End - Execute the Query 
		
		DECLARE @componentQuery varchar(1000)

		SET @componentQuery = 'SELECT	PC.COMPONENTID AS COMPONENTID
				,PC.COMPONENTNAME AS COMPONENTNAME
				,PC.LABOURCOST + PC.MATERIALCOST AS COST 				
		FROM	PLANNED_COMPONENT PC '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN 
			SET @componentQuery = @componentQuery + CHAR(10) +' WHERE PC.COMPONENTNAME LIKE ''%' + @searchText  + '%'''
		END	
		
		SET @componentQuery = @componentQuery + CHAR(10) +'ORDER BY PC.SORDER ASC'
		
		PRINT @componentQuery
		EXEC (@componentQuery)	
						
END



GO
