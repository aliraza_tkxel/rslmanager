SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[E__EMPLOYEE_DELETE]
(
	@Original_EMPLOYEEID int
)
AS
	SET NOCOUNT OFF;
DELETE FROM [E__EMPLOYEE] WHERE (([EMPLOYEEID] = @Original_EMPLOYEEID))
GO
