SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_UpdateInspectionType @employeeId = @employeeid , @inspectionTypeId = @inspectionTypeId
-- Author:<Salman Nazir>
-- Create date: <10/02/2012>
-- Description:	<Update the InspectionType values against user>
-- WEb Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdateInspectionType](
	-- Add the parameters for the stored procedure here
	@employeeId int,
	@inspectionTypeId int
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update AS_USER_INSPECTIONTYPE SET InspectionTypeID=@inspectionTypeId 
	Where EmployeeId =@employeeId
END
GO
