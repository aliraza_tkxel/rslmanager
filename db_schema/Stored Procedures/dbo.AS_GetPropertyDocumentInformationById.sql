USE [RSLBHALive]
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_GetPropertyDocumentInformationById'))
  DROP PROCEDURE AS_GetPropertyDocumentInformationById
GO

--=============================================
-- EXEC AS_GetPropertyDocumentInformationById @documentId = 7415
-- Author:		<Aamir Waheed>
-- Create date: <14-Dec-2013>
-- Description:	<Delete Property Document By Document Id>
-- WebPage: PropertyRecord.aspx > Document Tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPropertyDocumentInformationById]
	@documentId int	
AS
BEGIN

	-- Get Document Complete Information to show document on client side.
	SELECT	DocumentId, PropertyId, ISNULL(DocumentName,'N/A') AS DocumentName, DocumentPath, DocumentType, isNull(Keywords,'') Keywords, CreatedDate, DocumentSize, DocumentFormat ,DocumentTypeId,DocumentSubTypeId
	FROM	P_Documents
	WHERE DocumentId = @documentId

END

