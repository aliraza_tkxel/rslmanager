SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO















CREATE PROCEDURE dbo.FL_CO_USER_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_CO_USER_GETLOOKUP
 '   DATE CREATED:   31 DEC 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get User Id from E__Employee table which will be shown as lookup value in presentation pages
 '   IN:            @TeamId,@OrgId
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@TeamId as Int,
@OrgId as int
AS
	SELECT     E__EMPLOYEE.EMPLOYEEID as id, E__EMPLOYEE.FIRSTNAME as val
FROM         E__EMPLOYEE INNER JOIN
                      E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID INNER JOIN
                      E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID
WHERE     (E_TEAM.TEAMID =@TeamId and  E__EMPLOYEE.ORGID=@OrgId )
	ORDER BY FIRSTNAME ASC












GO
