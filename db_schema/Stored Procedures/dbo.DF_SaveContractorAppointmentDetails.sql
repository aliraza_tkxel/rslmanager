USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_SaveContractorAppointmentDetails]    Script Date: 5/16/2018 5:21:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================
-- EXEC [dbo].[DF_SaveContractorAppointmentDetails]

	--@ORGID =1270,
	--@DefectId =1487,
	--@UserId =760
-- Author:		<Noor Muhammad>
-- Create date: <29/09/2015>
-- Description:	<Saves all appointment information of defect>
-- History:          29/09/2015 Noor : Created the stored procedure
--                   29/09/2015 Name : Description of Work
-- =====================================================
IF OBJECT_ID('dbo.DF_SaveContractorAppointmentDetails') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_SaveContractorAppointmentDetails AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[DF_SaveContractorAppointmentDetails]
( 		
	@applianceId int,
	@defectId int,
	@propertyId NVARCHAR(100),
	@blockId int null,
	@schemeId int null,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@contactId int, 
	@contractorId int,
	@poStatus int,
	@ContractorWorksDetail AS DF_AssingToContractorWorksRequired READONLY,		
	@isSavedOut INT OUTPUT,
	@journalIdOut int = 0 output,
	@poStatusIdOut int = 0 OUTPUT
	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	BEGIN TRY
		-------- Declaring variables -------------------------------------------------------
		------------------------------------------------------------------------------------
		Declare @tenancyId int,
		@msatTypeId int,
		@appointmentStatusId int,
		@msatId int,
		@defectNotes nvarchar(200),
		@customerId int

		-------- Populating variables ------------------------------------------------------
		------------------------------------------------------------------------------------
		if @contractorId= -1
			set @contractorId=NULL
		
		-- Get Tenancy Detail
		if @propertyId is not null 
		begin
			SELECT	@tenancyId = CT.TenancyId, @customerId = CT.CUSTOMERID
			FROM	C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON 
					( T.TENANCYID = CT.TENANCYID ) AND 
					( T.ENDDATE >= CONVERT(date, GETDATE() ) OR 
					 T.ENDDATE IS NULL ) AND
					( CT.ENDDATE >= CONVERT(date, GETDATE() )	OR 
					 CT.ENDDATE IS NULL )
			WHERE	( T.PROPERTYID = @PropertyId ) AND 
					( CT.CUSTOMERTENANCYID = (	SELECT max( CUSTOMERTENANCYID ) 
												FROM C_CUSTOMERTENANCY C_CT 
												WHERE C_CT.TENANCYID = CT.TENANCYID ) )
		end
		ELSE
		BEGIN
			SELECT	@tenancyId = null
		END

		
		--SELECT MSAT TYPE ID
		SELECT @msatTypeId = msatTypeId
		From PDR_MSATType WHERE PDR_MsatType.MsatTypeName like 'Appliance Defect%'
		
		--Select Status Id
		SELECT @appointmentStatusId = PDR_Status.StatusId
		FROM PDR_Status WHERE Title = 'Assigned To Contractor'
		
		--Get Defect  Notes
		SELECT @defectNotes = DefectNotes FROM P_PROPERTY_APPLIANCE_DEFECTS WHERE PropertyDefectId = @defectId

		-------- Insertion into Tables -----------------------------------------------------
		------------------------------------------------------------------------------------
		--Insert into PDR_MSAT
		INSERT INTO [PDR_MSAT]([PropertyId] ,[MSATTypeId],[TenancyId], [CustomerId], [SchemeId], [BlockId])
		 VALUES(@propertyId ,@msatTypeId,@tenancyId, @customerId, @schemeId, @blockId)
	           
		SET @msatId = SCOPE_IDENTITY()           

		--Insert into PDR_Journal
		INSERT INTO [PDR_JOURNAL]([MSATID],[STATUSID],[CREATIONDATE],[CREATEDBY])
		 VALUES(@msatId,@appointmentStatusId,getdate(),@userId)
		
		SET @journalIdOut  = SCOPE_IDENTITY()    
		   
		--update Into [P_PROPERTY_APPLIANCE_DEFECTS]
		UPDATE [P_PROPERTY_APPLIANCE_DEFECTS]
		SET 
		   [ApplianceDefectAppointmentJournalId] = @journalIdOut     
		  ,[ModifiedBy] = @userId
		  ,[ModifiedDate] = getdate()
		WHERE PropertyDefectId = @defectId
		
		DECLARE @isAssignToContractorSaved bit = 0 		
		-- Execute the stored procedure "Assign Defect Work To Contractor"
		EXEC DF_AssignWorkToContractor 
						@contactId,
						@contractorId,
						@userId,
						@Estimate,
						@EstimateRef,
						@poStatus,
						@propertyId,
						@blockId,
						@schemeId,
						@journalIdOut,
						@contractorWorksDetail,
						@defectNotes,
						@customerId,
						@isAssignToContractorSaved OUTPUT,
						@poStatusIdOut OUTPUT;
									
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isSavedOut = 0        
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
					@ErrorSeverity, -- Severity.
					@ErrorState -- State.
				);
	END CATCH;

	IF @@TRANCOUNT > 0
		BEGIN  
			COMMIT TRANSACTION;  
			SET @isSavedOut = 1
		END
    
END
