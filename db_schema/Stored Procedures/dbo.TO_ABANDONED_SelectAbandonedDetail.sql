SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE  PROCEDURE [dbo].[TO_ABANDONED_SelectAbandonedDetail]

/* ===========================================================================
 '   NAME:           TO_ABANDONED_SelectAbandonedDetail
 '   DATE CREATED:   11 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select one TO_ABANDONED record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT     Abandoned.Location AS LocationDescription, Abandoned.LookUpValueID, EnquiryLog.Description AS OtherInfo
FROM         TO_ABANDONED Abandoned INNER JOIN
                      TO_ENQUIRY_LOG EnquiryLog ON Abandoned.EnquiryLogID = EnquiryLog.EnquiryLogID
WHERE     (EnquiryLog.EnquiryLogID = @enqLogID)


GO
