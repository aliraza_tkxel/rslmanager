SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  procedure [dbo].[sp_get_table_identity]
@tablename varchar(100)
as 

SELECT IDENT_CURRENT(@tablename) as NEXTID



GO
