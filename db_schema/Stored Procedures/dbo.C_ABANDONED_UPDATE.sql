SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--BEGIN TRAN
--EXEC C_ABANDONED_UPDATE 126,13,343,FW,tre,3 
--ROLLBACK



CREATE       PROC [dbo].[C_ABANDONED_UPDATE](
/* ===========================================================================
 '   NAME:           C_ABANDONED_UPDATE
 '   DATE CREATED:  28 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
Creates a new record by updating record in C_ABANDONED and 
 '		  creates entry in   C_JOURNAL tables		 

 '   IN:             @HISTORYID
 '   IN:             @CURRENTITEMSTATUSID
 '   IN:             @LASTACTIONUSER
 '   IN:             @NOTES
 '   IN:             @LOCATION
 '   IN:             @LOOKUPVALUEID
 '
 '   OUT:            	    Nothing    
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@HISTORYID INT , 
@CURRENTITEMSTATUSID INT,
@LASTACTIONUSER INT ,
@NOTES VARCHAR(4000), 
@LOCATION VARCHAR(4000),
@LookUpValueID INT
				
)
AS
BEGIN 

DECLARE @JOURNALID INT
DECLARE @ABANDONEDHISTORYID INT

SET NOCOUNT ON
	
	SELECT 
		@JOURNALID = G.JOURNALID
	FROM C_ABANDONED G
	WHERE G.ABANDONEDHISTORYID = @HISTORYID
	
	BEGIN TRAN

	UPDATE C_JOURNAL SET 	CURRENTITEMSTATUSID = @CURRENTITEMSTATUSID				
	WHERE JOURNALID = @JOURNALID
	
	-- If updation fails, goto HANDLE_ERROR block
	--IF @@ERROR <> 0 GOTO HANDLE_ERROR

	INSERT INTO C_ABANDONED (JOURNALID, LookUpValueID , Location , OtherInfo, LASTACTIONDATE,LASTACTIONUSER,ITEMSTATUSID )
	VALUES (@JOURNALID, @LookUpValueID, @LOCATION, @NOTES,  GETDATE(), @LastActionUser,@CURRENTITEMSTATUSID)


	SELECT 	@ABANDONEDHISTORYID = SCOPE_IDENTITY()
	
    -- If updation fails, goto HANDLE_ERROR block
	--IF @@ERROR <> 0 GOTO HANDLE_ERROR

	SELECT @JOURNALID AS JOURNALID , @ABANDONEDHISTORYID AS SERVICEID
    COMMIT TRAN


	

	-- Control comes here if error occured
--	HANDLE_ERROR:

--	  ROLLBACK TRAN
--
--	  --RETURN -1

END



GO
