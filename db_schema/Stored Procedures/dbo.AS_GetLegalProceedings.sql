USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetLegalProceedings]    Script Date: 20-Jul-18 2:43:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.AS_GetLegalProceedings') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetLegalProceedings AS SET NOCOUNT ON;') 
GO
-- =============================================
-- EXEC	[dbo].[AS_GetLegalProceedings]
--		@StatusTitle = N'Legal Proceedings',
--		@PatchId = 18,
--		@DevelopmentId = 1 
-- Author:		<Hussain Ali>
-- Modified By:		<Noor Muhammad>
-- Create date:	<31/10/2012>
-- Modified date: <12/11/2012>
-- Description:	<This stored procedure gets the 'Legal Proceedings'>
-- Useage: <dashboard.aspx>

-- =============================================
ALTER PROCEDURE [dbo].[AS_GetLegalProceedings]
	-- Add the parameters for the stored procedure here
	@PatchId		int,
	@DevelopmentId	int,
	-- parameters for sorting and paging		
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Address',
	@sortOrder varchar (5) = 'ASC',
	@totalCount int=0 output,
	@FuelType varchar (8000)

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @offset int
	declare @limit int

	set @offset = 1+(@pageNumber-1) * @pageSize
	set @limit = (@offset + @pageSize)-1	

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	nvarchar(max),
			@fromClause		nvarchar(max),

			@selectCountClause	nvarchar(max),
			@selectCountClauseForGas	nvarchar(max),

			@SelectClauseScheme nvarchar(max),
			@fromClauseScheme nvarchar(max),
			@whereClauseScheme	nvarchar(max),
			@SelectClauseBlock nvarchar(max),
			@fromClauseBlock nvarchar(max),
			@whereClauseBlock	nvarchar(max),
			@mainGasUnionQuery nvarchar(max),

			@orderClause  nvarchar(max),
			@whereClause	nvarchar(max),		
			@mainSelectQuery nvarchar(max), 
			@rowNumberQuery nvarchar(max),
			@finalQuery nvarchar(max)  
 ---------------------------------------- FOR GAS ------------------------------------------------------------
if @FuelType = 'Gas'
begin

	SET @selectClause	= 'SELECT 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								PV.ValueDetail As FUEL
								'
	
	SET @fromClause		= 'FROM AS_JOURNAL
								INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID 
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
								
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Gas'' )'


	SET @SelectClauseScheme = ' SELECT
								ISNULL(P.SCHEMENAME,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								PV.ValueDetail As FUEL
								'

	SET @fromClauseScheme= '	FROM AS_JOURNAL
								INNER JOIN P_Scheme P ON AS_JOURNAL.SchemeId = P.SchemeId
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P.SchemeId = PHM.SchemeId
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId '

	SET @whereClauseScheme = '  Where 1=1 
										AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Gas'' )'


	SET @SelectClauseBlock = 'SELECT
								ISNULL(P.BlockName,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								PV.ValueDetail As FUEL
								'
	SET @fromClauseBlock= '	FROM AS_JOURNAL
								INNER JOIN P_Block P ON AS_JOURNAL.BlockId = P.BlockId
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P.BlockId = PHM.BlockId
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId '

	SET @whereClauseBlock = '  Where 1=1 
										AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Gas'' )'

end
---------------------------------------- FOR Oil ------------------------------------------------------------

if @FuelType = 'Oil'
begin
	SET @selectClause	= 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+') 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort
								PV.ValueDetail As FUEL '
	
	SET @fromClause		= 'FROM 
								AS_JOURNAL
								INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID 
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
								
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''oil'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Oil'' )'
end
---------------------------------------- FOR Alternative servicing ------------------------------------------------------------	
if @FuelType = 'Alternative Servicing'
begin
	SET @selectClause	= 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+') 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								FuelTypes.FUELS AS FUEL'
	
	SET @fromClause		= 'FROM 
								AS_JOURNAL
								INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID 

								INNER JOIN 
										(SELECT	P__PROPERTY.PROPERTYID, FUELS  = STUFF(
													(SELECT '', '' + PV.ValueDetail
														FROM PA_HeatingMapping HM
														Inner JOIN PA_PARAMETER_VALUE PV on HM.HeatingType = PV.ValueId
														WHERE HM.IsActive = 1 AND PV.IsAlterNativeHeating = 1 AND P__PROPERTY.PropertyId = HM.PropertyId AND PV.IsActive = 1
														FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''NVARCHAR(max)''), 1, 2, '''')
										FROM	P__PROPERTY 
										GROUP BY P__PROPERTY.PROPERTYID) As FuelTypes 
										on P__PROPERTY.PROPERTYID = FuelTypes.PropertyId

								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
								
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PA_Parameter_value.IsAlterNativeHeating = 1
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Alternative Servicing'' )'
end

	SET @selectCountClause = ' SELECT DISTINCT
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort
								'

			
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + ' P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
		SET @whereClauseBlock = @whereClauseBlock + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @PatchId) + CHAR(10)
		SET @whereClauseScheme = @whereClauseScheme + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 		--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
	SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
	END		


	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
	--========================================================================================
	-- Begin building the main select Query

	Declare @unionQuery varchar(100)
	SET @unionQuery = char(10) + ' UNION ALL ' + CHAR(10)
	if @fuelType = 'Gas'
	Begin
	

		Set @mainGasUnionQuery = @selectClause +@fromClause + @whereClause + @unionQuery +
								@SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery +
								@SelectClauseBlock + @fromClauseBlock + @whereClauseBlock
		Set @mainSelectQuery = ' SELECT TOP ('+convert(VARCHAR(10),@limit)+') * from ( ' + @mainGasUnionQuery + ' ) as TopResult  ' + 
						@orderClause + '  '
	END 
	else
	Begin
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause
	End

	--print(@mainSelectQuery)
	-- End building the main select Query
	--========================================================================================			
	
	
	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	--print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	Declare @countClauseEnd nvarchar(max)
	SET @countClauseEnd = ') AS CertifictaeExpired'

	Declare @selectCount nvarchar(max),
	@parameterDef nvarchar(max)

	SET @parameterDef = '@totalCount int OUTPUT';

	if(@fuelType = 'Gas')
	Begin
		SET @selectCount= 'SELECT @totalCount =  count(*) From ( ' + @selectClause + @fromClause + @whereClause + @unionQuery +
								@SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery +
								@SelectClauseBlock + @fromClauseBlock + @whereClauseBlock + @countClauseEnd
	End
	else
	Begin
	SET @selectCount= 'SELECT @totalCount =  count(*) From ( ' + @selectCountClause + @fromClause + @whereClause + @countClauseEnd
	End

	print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		
	-- End building the Count Query
	--========================================================================================	


END



