USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_TerriorReportExportToExcel]    Script Date: 02/05/2015 22:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Terrior Report data on Terrior Report Page
 
    Author: Ali Raza
    Creation Date:  03/12/2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         05/24/2014     Fakhar uz zaman           Export to Excel button in Terrior Report.
    
    Execution Command:

--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_TerriorReportExportToExcel]
--		@SCHEMEID = NULL,
--		@SEARCHTEXT = N'26 Herringswell Road',
--		@pageSize = 50,
--		@pageNumber = 1,
--		@sortColumn = N'ADDRESS',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'*/

IF OBJECT_ID('dbo.PDR_TerriorReportExportToExcel') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_TerriorReportExportToExcel AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_TerriorReportExportToExcel]
	-- Add the parameters for the stored procedure here
		@SCHEMEID INT = null,
		@SEARCHTEXT VARCHAR(200)= null,
		@isSold bit = 0,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ADDRESS', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause nvarchar(max),        
        @fromClause   nvarchar(max),        
        @whereClause  nvarchar(max),        
        @orderClause  nvarchar(max),        
        @mainSelectQuery nvarchar(max),        
        @rowNumberQuery nvarchar(max),
        @finalQuery nvarchar(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		IF @isSold = 0	--isSold checkbox
			BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (P.STATUS IS NULL OR PROP_STATUS.DESCRIPTION NOT IN (''Sold'',''Transfer'',''Other Losses'',''Demolished'')) 
			'	
			END
					
		IF @SCHEMEID != -1
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( P.SCHEMEID  = ' +convert(varchar(10),@SCHEMEID)  + ')'	
		END
		IF(@SEARCHTEXT != '' OR @SEARCHTEXT != NULL)
		BEGIN	
            SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ISNULL(P.HouseNumber, '''') + '' '' + ISNULL(P.ADDRESS1, '''') + '' '' + ISNULL(P.ADDRESS2, '''') + '' '' + ISNULL(P.ADDRESS3, '''')  LIKE ''%' + @searchText + '%'''
			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P.TOWNCITY LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P.POSTCODE LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		PSch.SCHEMECODE As SCHEMECODE 
		, PSch.SCHEMENAME As SCHEMENAME
		, P.PROPERTYID AS [PROPERTYID]
		,(ISNULL(HOUSENUMBER,'''')+'' ''+ISNULL(P.ADDRESS1,'' '')) as ADDRESS
		, P.TOWNCITY AS TOWNCITY 
		, P.POSTCODE AS POSTCODE
		,ISNULL(PROP_TYPE.DESCRIPTION,''NULL'') AS PROPTYPE
		,CASE
			WHEN P.NROSHASSETTYPEMAIN IS NULL THEN ''N/A''
			ELSE ANM.Description
		END as NROSHAssetType
		,P.DATEBUILT as DateBuilt
		,ISNULL(TENURE_TYPE.TENURETYPE, '''') AS TenureType
		,CASE	WHEN TENURE_TYPE.TENURETYPE =''Leasehold'' AND OWNERSHIP.Description <> ''Outright Owner''
		THEN ISNULL(OWNERSHIP.Description,''NULL'') ELSE  ''-'' END AS LeaseLength
		,ISNULL(CHARGE.DESCRIPTION,''NULL'') As ChargedTo
		,CASE
			WHEN FINANCIALS.RENT IS NULL THEN ''NULL''
			ELSE ''£''+convert(varchar(10), FINANCIALS.RENT)
		 END as RENT
		,CAST(ROUND(IsNull(FINANCIALS.YIELD, 0.00), 3) AS MONEY) as YIELD
		,CASE
			WHEN FINANCIALS.SERVICES IS NULL THEN ''NULL''
			ELSE ''£''+convert(varchar(10), ISNULL(FINANCIALS.SERVICES,0))
		 END as SERVICECHARGE
		 ,CASE
			WHEN FINANCIALS.EUV IS NULL THEN ''NULL''
			ELSE ''£''+convert(varchar(10), ISNULL(FINANCIALS.EUV,0))
		 END as EUV
		 ,CASE 
			WHEN FINANCIALS.OMVST IS NULL THEN ''NULL''
			ELSE ''£''+convert(varchar(10), ISNULL(FINANCIALS.OMVST,0))
		 END as OMVST
		 ,CASE 
			WHEN P.RIGHTTOBUY IS NULL THEN ''NULL''
			WHEN P.RIGHTTOBUY = 1 THEN ''Yes''
			ELSE ''No''
		 END AS RIGHTTOBUY
		,CASE 
			WHEN P.PURCHASELEVEL IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,P.PURCHASELEVEL)
		 END AS PURCHASELEVEL --%SOLD
		, P.TitleNumber AS TitleNumber
		, P.DeedLocation AS DeedLocation
		
		,CASE 
			WHEN S106Date.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,S106Date.DOCUMENTDATE,103)
		 END AS S106Date
		,CASE
			WHEN ElectricalCertDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,ElectricalCertDate.DOCUMENTDATE,103)
		 END AS ElectricalCertDate
		,CASE
			WHEN FireAlamCertDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,FireAlamCertDate.DOCUMENTDATE,103)
		 END AS FireAlamCertDate
		,CASE
			WHEN DryLiningCertDate.DOCUMENTDATE IS NULL THEN ''NULL'' 
			ELSE CONVERT(VARCHAR,DryLiningCertDate.DOCUMENTDATE,103)
		 END AS DryLiningCertDate
		,CASE
			WHEN DampProofCertDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,DampProofCertDate.DOCUMENTDATE,103)
		 END AS DampProofCertDate
		,CASE
			WHEN AsbestosCertDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,AsbestosCertDate.DOCUMENTDATE,103)
		 END AS AsbestosCertDate
		,CASE 
			WHEN EPCDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,EPCDate.DOCUMENTDATE,103)
		 END AS EPCDate
		,CASE
			WHEN HealthSafetyDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,HealthSafetyDate.DOCUMENTDATE, 103)
		 END AS HealthSafetyDate
		,CASE 
			WHEN NHBCDate.DOCUMENTDATE IS NULL THEN ''NULL''
			ELSE CONVERT(VARCHAR,NHBCDate.DOCUMENTDATE,103)
		 END AS NHBCDate
		,ISNULL(PROPERTYCONDITIONRATING.DecentHomes,0) AS DecentHomes
		,ISNULL(S.DESCRIPTION,'' '') as [STATUS]
		,gl.DESCRIPTION as LocalAuthority
		,ISNULL(CONVERT(NVARCHAR,PS.SurveyDate,103),''N/A'') AS SurveyDate
		, CASE 
			WHEN FLOODRISK.PROPERTYID IS NULL THEN ''No''
			ELSE ''Yes''
		 END AS FloodRisk
		 , CASE 
			WHEN NHBCCOVER.PROPERTYID IS NULL THEN ''No''
			ELSE ''Yes''
		 END AS NHBCWarrenty
		 , CASE 
			WHEN NHBCCOVER.PROPERTYID IS NULL THEN ''N/A''
			ELSE ISNULL(NHBCCOVER.EXPIRYDATE,''N/A'')
		 END AS NHBCExpiration
		 ,CASE WHEN ( ISNULL(SCHEMEREST.RESTRICTIONCOUNT,0) > 0 OR ISNULL(BLOCKREST.RESTRICTIONCOUNT,0) > 0) THEN ''Yes'' ELSE ''No'' END as SchemeRestriction
		 ,CASE WHEN  ISNULL(PROPERTYREST.RESTRICTIONCOUNT,0) > 0 THEN ''Yes'' ELSE ''No'' END as PropertyRestriction
		 
  '  
		
		--============================From Clause============================================
		SET @fromClause = 'FROM P__PROPERTY P 
							INNER JOIN P_FINANCIAL F on F.PROPERTYID = P.PROPERTYID
							INNER JOIN  P_SCHEME PSch on PSch.SchemeID = P.SchemeID
							INNER JOIN P_STATUS S on S.STATUSID = P.STATUS
							INNER JOIN P_ASSETTYPE AT on AT.ASSETTYPEID = P.ASSETTYPE
							OUTER APPLY (SELECT MAX(S.SurveyDate) [SurveyDate] FROM PS_Survey S WHERE S.PROPERTYID = P.PROPERTYID) PS
							LEFT JOIN P_ASSETTYPE_NROSH_MAIN AS ANM ON P.NROSHASSETTYPEMAIN = ANM.Sid 
							INNER JOIN P_PROPERTYTYPE PROP_TYPE on PROP_TYPE.PROPERTYTYPEID = P.PROPERTYTYPE
							LEFT JOIN P_TENURESELECTION TENURE_TYPE ON P.TENURETYPE = TENURE_TYPE.TENURETYPEID
							INNER JOIN P_STATUS PROP_STATUS ON PROP_STATUS.STATUSID = P.STATUS
							INNER JOIN P_FINANCIAL FINANCIALS ON FINANCIALS.PROPERTYID = P.PROPERTYID
							LEFT JOIN P_CHARGE CHARGE ON FINANCIALS.CHARGE = CHARGE.CHARGEID
							LEFT JOIN P_OWNERSHIP OWNERSHIP ON OWNERSHIP.OwnershipId = P.OWNERSHIP
							INNER JOIN PDR_DEVELOPMENT pd on pd.DEVELOPMENTID = P.DEVELOPMENTID
							INNER JOIN G_LOCALAUTHORITY gl on gl.LOCALAUTHORITYID = LOCALAUTHORITY
							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''s106 Agreement''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) S106Date ON P.PROPERTYID = S106Date.PROPERTYID
										
							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''Electrical''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) ElectricalCertDate ON P.PROPERTYID = ElectricalCertDate.PROPERTYID

							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''Fire Alarm Systems''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) FireAlamCertDate ON P.PROPERTYID = FireAlamCertDate.PROPERTYID										

							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''Dry Lining Certificate''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) DryLiningCertDate ON P.PROPERTYID = DryLiningCertDate.PROPERTYID										
							
							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''Damp Proof Course Certificate''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) DampProofCertDate ON P.PROPERTYID = DampProofCertDate.PROPERTYID	

							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''Asbestos''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) AsbestosCertDate ON P.PROPERTYID = AsbestosCertDate.PROPERTYID	

							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''EPC''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) EPCDate ON P.PROPERTYID = EPCDate.PROPERTYID										

							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''Health & Safety''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) HealthSafetyDate ON P.PROPERTYID = HealthSafetyDate.PROPERTYID																				

							LEFT JOIN (Select DOCS.DocumentTypeId,PROPERTY.PROPERTYID, MAX(DOCS.DocumentDate) as DocumentDate 
										from P__PROPERTY PROPERTY INNER JOIN P_Documents DOCS ON PROPERTY.PROPERTYID = DOCS.PropertyId
										INNER JOIN P_DOCUMENTS_TYPE DOCTYPE ON DOCTYPE.DOCUMENTTYPEID = DOCS.DOCUMENTTYPEID
										where DOCTYPE.TITLE = ''NHBC''
										Group By DOCS.DocumentTypeId,PROPERTY.PROPERTYID) NHBCDate ON P.PROPERTYID = NHBCDate.PROPERTYID
										
							LEFT JOIN (SELECT	PPA.PROPERTYID PropertyId, COUNT(*) as DecentHomes
										FROM	PA_PROPERTY_ATTRIBUTES PPA
												INNER JOIN PA_ITEM_PARAMETER PIP ON PPA.ITEMPARAMID = PIP.ItemParamID
												INNER JOIN PA_PARAMETER PP ON PIP.PARAMETERID = PP.PARAMETERID
												INNER JOIN PA_PARAMETER_VALUE PPV ON PPA.VALUEID = PPV.ValueID
										WHERE	ParameterName like ''%condition%''
												AND PPV.ValueDetail IN (''unsatisfactory'',''potentially unsatisfactory'')
												AND PPA.PROPERTYID IS NOT NULL
										GROUP BY PPA.PROPERTYID) AS PROPERTYCONDITIONRATING ON P.PROPERTYID = PROPERTYCONDITIONRATING.PROPERTYID

							LEFT JOIN (	SELECT	PROPERTYID 
										FROM	P__PROPERTY 
										WHERE	floodingrisk = 1) AS FLOODRISK ON P.PROPERTYID = FLOODRISK.PROPERTYID
							
							LEFT JOIN (	SELECT	PROPERTYID, CONVERT(VARCHAR,MAX(EXPIRYDATE),103) EXPIRYDATE
										FROM	PDR_WARRANTY PW
												INNER JOIN P_WARRANTYTYPE PWT ON PW.WARRANTYTYPE = PWT.WARRANTYTYPEID
										WHERE	PWT.DESCRIPTION =  ''NHBC Cover'' and PROPERTYID IS NOT NULL
										GROUP BY PROPERTYID) AS NHBCCOVER ON P.PROPERTYID = NHBCCOVER.PROPERTYID

							LEFT JOIN (	SELECT	 SCHEMEID, COUNT(*) RESTRICTIONCOUNT
										FROM	PDR_Restriction
										GROUP BY SCHEMEID
										) SCHEMEREST ON P.SCHEMEID = SCHEMEREST.SCHEMEID
							
							LEFT JOIN (	SELECT	 BLOCKID, COUNT(*) RESTRICTIONCOUNT
										FROM	PDR_Restriction
										GROUP BY BLOCKID
										) BLOCKREST ON P.BLOCKID = BLOCKREST.BLOCKID

							LEFT JOIN (	SELECT	 PROPERTYID, COUNT(*) RESTRICTIONCOUNT
										FROM	PDR_Restriction
										GROUP BY PROPERTYID
										) PROPERTYREST ON P.PROPERTYID = PROPERTYREST.PROPERTYID
																				
							'

		--============================Order Clause==========================================
		IF(@sortColumn = 'ADDRESS')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ADDRESS' 	
			
		END
		
		IF(@sortColumn = 'TownCITY')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TOWNCITY' 	
			
		END
		IF(@sortColumn = 'POSTCODE')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'POSTCODE' 	
			
		END
		IF(@sortColumn = 'OMV')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'OMV' 	
			
		END
		IF(@sortColumn = 'EUV')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'EUV' 	
			
		END
		IF(@sortColumn = 'RENT')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'RENT' 	
			
		END
		IF(@sortColumn = 'YIELD')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'YIELD' 	
			
		END
		IF(@sortColumn = '[STATUS]')
		BEGIN
			SET @sortColumn = CHAR(10)+ '[STATUS]' 	
			
		END	
		--changed by saud to sort data accordingly [SCHEMECODE], [PROPERTYID]
		--SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		SET @orderClause = 'Order By [SCHEMECODE], [PROPERTYID]'

		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		
		--changed by saud to sort data accordingly [SCHEMECODE], [PROPERTYID]
		--Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +' ASC ' + CHAR(10)+') as row	
		
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		PRINT(@mainSelectQuery)
		--print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(max), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
									
END
