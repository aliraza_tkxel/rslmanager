
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_getReportActions
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,>
-- Description:	<Description,,get actions for Report dropdown>
-- WebPage :  TenancySupportReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_getReportActions]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select ITEMACTIONID,DESCRIPTION as ActionName from C_ACTION Where ITEMID = 2
	AND DESCRIPTION IN ('Referred','Rejected','Assigned','Note','Closed')
END
GO
