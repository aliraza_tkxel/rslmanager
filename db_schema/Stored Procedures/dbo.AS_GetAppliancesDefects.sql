SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetAppliancesDefects
--@propertyId = A010060001,
--@applianceId = 1
-- Author:		<Salman Nazir>
-- Create date: <06/11/2012>
-- Description:	<Get Defects against Property Id and Appliance Id>
-- Web Page: PropertyRecord.aspx => Attributes Tab 
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetAppliancesDefects](
@propertyId Varchar(100),
@applianceId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DefectDate,P_PROPERTY_APPLIANCE_DEFECTS.CategoryId,IsDefectIdentified,IsActionTaken,IsWarningIssued,P_DEFECTS_CATEGORY.Description
	FROM P_PROPERTY_APPLIANCE_DEFECTS INNER JOIN
	P_DEFECTS_Category on P_PROPERTY_APPLIANCE_DEFECTS.CategoryId = P_DEFECTS_CATEGORY.CATEGORYID
	Where PropertyId = @propertyId AND ApplianceId = @applianceId
END
GO
