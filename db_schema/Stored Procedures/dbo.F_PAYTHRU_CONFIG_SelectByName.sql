SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[F_PAYTHRU_CONFIG_SelectByName]
    @SettingName VARCHAR(MAX)
AS 
	SET NOCOUNT ON 

    SELECT  
			ConfigId,
			SettingName,
			SettingValue
    FROM    [dbo].[F_PAYTHRU_CONFIG]
    WHERE   ( SettingName = @SettingName ) 





GO
