-- =============================================      
-- Exec AS_SubItemByItemId @areaId=1      
-- Author: <Ali Raza>      
-- Create date: <17/04/2014>       
-- Web Page: PropertyRecord.aspx      
-- =============================================    
IF OBJECT_ID('dbo.AS_SubItemByItemId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_SubItemByItemId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
  
ALTER PROCEDURE [dbo].[AS_SubItemByItemId](      
@itemId int      
)      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
 SELECT DISTINCT item.* ,ISNULL( Si.SubItem ,0) as  SubItem
 From PA_ITEM item 
 Outer APPLY (Select Count(I.ItemId)as SubItem  From PA_ITEM I Where I.ParentItemId= item.ItemID and I.IsActive = 1 Group By I.ItemId  ) as SI    
 Where ParentItemId= @itemId and IsActive = 1  
 Order BY ItemSOrder ASC      
END 