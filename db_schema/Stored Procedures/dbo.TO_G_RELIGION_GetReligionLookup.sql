SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_G_RELIGION_GetReligionLookup
/* ===========================================================================
 '   NAME:           TO_G_RELIGION_GetReligionLookup
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get religion records from G_RELIGION table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT RELIGIONID AS id,DESCRIPTION AS val
	FROM G_RELIGION
	ORDER BY DESCRIPTION ASC

GO
