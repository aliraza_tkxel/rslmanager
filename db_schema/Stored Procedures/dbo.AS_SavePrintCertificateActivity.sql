SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* =============================================
EXEC	[AS_SavePrintCertificateActivity]
		@propertyId = N'A010060001',
		@createdBy = 615,
		@createDate = '2013-09-01'
		
-- Author:		<Author,,Aamir Waheed>
-- Create date: <Create Date,,01/09/2013>
-- Description:	<Description,,Save Activity against property when a CP12 is printed >
-- Web Page: PrintCertificates.aspx
-- =============================================*/
IF OBJECT_ID('dbo.[AS_SavePrintCertificateActivity]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_SavePrintCertificateActivity] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_SavePrintCertificateActivity]
	@propertyId varchar(50),
	@createdBy int,	
	@createDate date,
	@reqType as Varchar(200)
AS
BEGIN
	
	SET NOCOUNT ON;			
   	--Update the Journal
   	DECLARE @inspectionTypeId int,
   			@statusId int,
			@actionId int,
			@notes nvarchar(1000) = NULL,		
			@isLetterAttached bit = 0,
			@isDocumentAttached bit = 0,
			@journalHistoryID INT	
			
	IF(@reqType = 'Scheme')
	BEGIN
		SELECT TOP 1 @inspectionTypeId = INSPECTIONTYPEID, @statusId = STATUSID FROM AS_JOURNALHISTORY
		WHERE SchemeId = @propertyId
		ORDER BY JOURNALHISTORYID DESC
	END

	IF(@reqType = 'Block')
	BEGIN
		SELECT TOP 1 @inspectionTypeId = INSPECTIONTYPEID, @statusId = STATUSID FROM AS_JOURNALHISTORY
		WHERE BlockId = @propertyId
		ORDER BY JOURNALHISTORYID DESC
	END

	IF(@reqType = 'Property')
	BEGIN
		SELECT TOP 1 @inspectionTypeId = INSPECTIONTYPEID, @statusId = STATUSID FROM AS_JOURNALHISTORY
		WHERE PROPERTYID = @propertyId
		ORDER BY JOURNALHISTORYID DESC
	END
	
	IF EXISTS (SELECT ActionId FROM AS_Action WHERE Title = 'Certificate Printed' AND StatusId = @statusId)
	BEGIN
		SELECT @actionId = ActionId FROM AS_Action WHERE Title = 'Certificate Printed' AND StatusId = @statusId
	END
	ELSE
	BEGIN
		DECLARE @StatusName NVARCHAR(100) 
		SELECT @StatusName = AS_STATUS.Title FROM AS_Status WHERE StatusId = @statusId
		DECLARE @ErrorMsg NVARCHAR(2048) =  'The action "Certificate Printed" do not exists for Status "' + @StatusName 
								+ '", kindy add an action as "Certificate Printed" for status "' + @StatusName  + '", in Resources area.'
								+ ' The record entry for "Certificate Printed" is not added to property activities. '
		RAISERROR (@ErrorMsg, 11,1);
		RETURN
	END
	
	EXEC [dbo].[AS_SavePropertyActivity] @propertyID, @createdBy, @inspectionTypeId, @createDate, @statusId, @actionId, NULL, 0, 0, @journalHistoryID
	
END
GO
