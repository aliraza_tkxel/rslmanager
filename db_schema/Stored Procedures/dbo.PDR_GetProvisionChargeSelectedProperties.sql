USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionChargeSelectedProperties]    Script Date: 10/29/2018 12:40:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_GetProvisionChargeSelectedProperties]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetProvisionChargeSelectedProperties] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE  [dbo].[PDR_GetProvisionChargeSelectedProperties]
( 
	@schemeId int,
	@blockId int,
	@itemId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if((@schemeId IS NOT NULL AND @schemeId != 0) AND (@blockId IS NOT NULL AND @blockId != 0))
	BEGIN
		select propertyId as PropertyId
		from PDR_ProvisionChargeProperties
		where (schemeid=@schemeId) and (blockid=@blockId) and (IsActive = '1')
		and isincluded = 1 and itemId=@itemId
	END
	ELSE IF((@schemeId IS NOT NULL AND @schemeId != 0) AND (@blockId IS NULL OR @blockId = 0))
	BEGIN
		select propertyId as PropertyId
		from PDR_ProvisionChargeProperties
		where (schemeid=@schemeId) and (blockid IS NULL OR blockid = 0) and (IsActive = '1')
		and isincluded = 1 and itemId=@itemId
	END
	ELSE IF(@schemeId is null OR @schemeId = 0)
	BEGIN
		select propertyId as PropertyId
		from PDR_ProvisionChargeProperties
		where (blockid=@blockId) AND (SCHEMEID IS NULL OR SCHEMEID = 0) and (IsActive = '1')
		and isincluded = 1 and itemId=@itemId
	END

END

GO


