SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC	[dbo].[RD_GetCountRecall]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'No Entries'>
-- Webpage: dashboard.aspx

-- =============================================
Create PROCEDURE [dbo].[RD_GetCountRecall]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declaring the variables to be used in this query
SELECT  count(DISTINCT FL_FAULT_RECALLS.OriginalFaultLogId)  FROM
							(SELECT OriginalFaultLogId, COUNT(OriginalFaultLogId) AS number FROM FL_FAULT_RECALL GROUP BY OriginalFaultLogId) AS FL_FAULT_RECALLS
							INNER JOIN FL_FAULT_LOG ON FL_FAULT_RECALLS.OriginalFaultLogId = FL_FAULT_LOG.FaultLogID
							INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
							INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
							INNER JOIN FL_AREA ON FL_FAULT.AREAID = FL_AREA.AreaID

							INNER JOIN FL_FAULT_JOURNAL on FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID
							LEFT JOIN FL_FAULTJOURNAL_TO_CJOURNAL on FL_FAULT_JOURNAL.JournalID = FL_FAULTJOURNAL_TO_CJOURNAL.FJOURNALID
							LEFT JOIN (SELECT * FROM C_REPAIR WHERE C_REPAIR.ItemStatusId=11 and C_REPAIR.ItemActionId=6) C_REPAIRS on FL_FAULTJOURNAL_TO_CJOURNAL.CJOURNALID = C_REPAIRS.JOURNALID

							LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId
							LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
							LEFT JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID
		
END




GO
