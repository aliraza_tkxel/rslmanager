SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE diagnostics.RemoveFollowOnFromReport ( @FaultLogId INT )
AS 
    UPDATE  dbo.FL_FAULT_FOLLOWON
    SET     isFollowonScheduled = 1
    WHERE   FaultLogId = @FaultLogId

GO
