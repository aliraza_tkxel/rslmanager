USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FLS_SaveTenancyTermination]    Script Date: 19-Feb-18 11:13:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.FLS_SaveTenancyTermination') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_SaveTenancyTermination AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[FLS_SaveTenancyTermination]
	@PropertyId varchar(100) = NULL,
	@CustomerId int = NULL,
	@TenancyId int = NULL,
	@TerminationDate DateTime,
	@UpdatedBy int,
	@Inspection int=1,
	@JournalID int = 0 out
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from

SET NOCOUNT ON;

DECLARE @MSATTypeId INT,
		@MSATId INT,
		@ToBeArrangedStatusId INT
---Declare Cursor variable            
DECLARE @ItemToInsertCursor CURSOR

SELECT
	@ToBeArrangedStatusId = PDR_STATUS.STATUSID
FROM PDR_STATUS
WHERE TITLE = 'To be Arranged'

IF (@Inspection = 1)
	BEGIN
SELECT
	@MSATTypeId = MSATTypeId
FROM PDR_MSATType
WHERE MSATTypeName = 'Void Inspection'
END

IF Not EXISTS(SELECT MSATID from PDR_MSAT where PropertyId=@PropertyId AND CustomerId=@CustomerId AND TenancyId=@TenancyId)
 BEGIN
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId, TerminationDate)
			VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId, @TerminationDate)

		SELECT
			@MSATId = SCOPE_IDENTITY()

		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
			VALUES (@MSATId, @ToBeArrangedStatusId, GETDATE(), @UpdatedBy)

		SET @JournalID = SCOPE_IDENTITY()
	END
END
