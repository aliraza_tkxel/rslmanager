SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_ENQUIRY_LOG_GetCustomersEqnuiriesCount
/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GetCustomersEqnuiriesCount
 '   DATE CREATED:   03 JULY 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Get the TO_ENQUIRY_LOG records count against the specified customer
 '   IN:             @CustomerID
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CustomerID INT
	)
	
AS

SELECT COUNT(EnquiryLogID) as CustomerEnquiries
FROM TO_ENQUIRY_LOG 
WHERE CustomerID=@CustomerID




GO
