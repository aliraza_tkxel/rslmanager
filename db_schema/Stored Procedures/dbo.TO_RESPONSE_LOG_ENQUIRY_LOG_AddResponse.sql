SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE        PROCEDURE [dbo].[TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse]

/* ===========================================================================
 '   NAME:           TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse
 '   DATE ALTERD:   04 JULY 2008
 '   ALTERD BY:     Munawar Nadeem
 '   ALTERD FOR:    Broadland Housing
 '   PURPOSE:        ALTERs a new record in TO_RESPONSE_LOG and update TO_ENQUIRY_LOG table
 '   IN:             @enqLogID, @closeFlag
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
		@enqLogID INT, 

		@closeFlag BIT,
		@readFlag BIT,
		@serviceId int	= null
	)
	
	AS

BEGIN 

	--DECLARE @READFLAG BIT
	DECLARE @ITEMSTATUSID INT

	--SET @READFLAG = 0 -- UNREAD 
	SET @ITEMSTATUSID = 27 -- ACTIONED from C_STATUS

	SET NOCOUNT ON
	
	BEGIN TRAN


	-- Inserting record in TO_RESPONSE_LOG
	
	IF @readFlag = 0
	BEGIN
		INSERT INTO TO_RESPONSE_LOG

			(EnquiryLogID, ReadFlag, ServiceId)
		VALUES 	( @enqLogID,    @READFLAG, @serviceId)

	END

	-- if insertion fails, go to HandleError block
	IF @@ERROR <> 0  GOTO HANDLE_ERROR

	-- Updating TO_ENQUIRY_LOG
	UPDATE TO_ENQUIRY_LOG

	   SET	
		CloseFlag=@closeFlag, ItemStatusID=@ITEMSTATUSID
	   WHERE
		EnquiryLogID = @enqLogID

	-- if updation fails, go to HandleError block
	IF @@ERROR <> 0  GOTO HANDLE_ERROR


	COMMIT TRAN	

	RETURN 1

	

-- control comes here if some error occured
HANDLE_ERROR:

ROLLBACK TRAN

RETURN -1

END







GO
