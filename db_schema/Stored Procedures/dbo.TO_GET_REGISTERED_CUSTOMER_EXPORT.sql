SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





create PROCEDURE [dbo].[TO_GET_REGISTERED_CUSTOMER_EXPORT]
	/*	===============================================================
	'   NAME:           TO_Customer_Registrations
	'   DATE ALTERD:   04 Nov 2008
	'   ALTERD BY:     Adnan Mirza
	'   ALTERD FOR:    Broadland Housing
	'   PURPOSE:        Produce list of registered TO customers
	'   IN:             @Month
	'   IN:             @Year
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

	(
	
	@MONTH INT = NULL,
	@YEAR INT = NULL
	)
	
AS

BEGIN
	SET NOCOUNT ON;
	
	SELECT C.CUSTOMERID,C.FIRSTNAME + ' ' + C.LASTNAME AS [NAME],ADDRESS1 + ' ' +  ADDRESS2 [ADRESS],POSTCODE,COUNTY,REGISTERDATE 
	FROM TO_LOGIN L
	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID= L .CUSTOMERID
	INNER JOIN C_ADDRESS ADDR ON ADDR.CUSTOMERID=C.CUSTOMERID AND ADDR.ISDEFAULT=1
	AND (DATEPART(MM,REGISTERDATE) = @MONTH OR @MONTH IS NULL) 
	AND (DATEPART(YY,REGISTERDATE) = @YEAR OR @YEAR IS NULL) 
	ORDER BY REGISTERDATE

END




GO
