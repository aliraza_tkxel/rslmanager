SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[F_PAYTHRU_ITEM_TYPE_SelectByDescription] 
    @Description VARCHAR(50)
AS 
	SET NOCOUNT ON 

    SELECT  [ItemTypeId] ,
            [Description]
    FROM    [dbo].[F_PAYTHRU_ITEM_TYPE]
    WHERE   ( [Description] = @Description ) 


GO
