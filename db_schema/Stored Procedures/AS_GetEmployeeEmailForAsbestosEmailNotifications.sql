USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyAddress]    Script Date: 10/01/2018 15:09:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.AS_GetEmployeeEmailForAsbestosEmailNotifications') IS NULL
 EXEC('CREATE PROCEDURE dbo.AS_GetEmployeeEmailForAsbestosEmailNotifications AS SET NOCOUNT ON;')
GO


ALTER PROCEDURE [dbo].[AS_GetEmployeeEmailForAsbestosEmailNotifications] 
( 
	-- Add the parameters for the stored procedure here
		@propertyId varchar(20),
		@requestType varchar(20) = 'property'
)
AS
BEGIN
	if(@requestType = 'property')
	BEGIN
		SELECT			dbo.E__EMPLOYEE.EMPLOYEEID, dbo.E__EMPLOYEE.FIRSTNAME +' '+ dbo.E__EMPLOYEE.LASTNAME AS FULLNAME, 
						dbo.E_JOBROLE.JobeRoleDescription, E_CONTACT.WORKEMAIL, 
						ISNULL(P__PROPERTY.HouseNumber,'') +', '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') +', '+ ISNULL(P__PROPERTY.TOWNCITY ,'')+' '+ ISNULL(P__PROPERTY.POSTCODE ,'') As PROPERTYADDRESS
		FROM            dbo.E__EMPLOYEE INNER JOIN
						dbo.E_JOBROLETEAM ON dbo.E__EMPLOYEE.JobRoleTeamId = dbo.E_JOBROLETEAM.JobRoleTeamId INNER JOIN
						dbo.E_JOBROLE ON dbo.E_JOBROLETEAM.JobRoleId = dbo.E_JOBROLE.JobRoleId INNER JOIN
						dbo.E_CONTACT ON dbo.E__EMPLOYEE.EMPLOYEEID = dbo.E_CONTACT.EMPLOYEEID Left join
						P__PROPERTY on P__PROPERTY.PROPERTYID = @propertyId

		WHERE			JobeRoleDescription = 'Executive Property Director' OR
						JobeRoleDescription = 'Head of property Operations' OR 
						JobeRoleDescription = 'Interim Head of Asset Management'
	END

	if(@requestType = 'block')
	BEGIN
		SELECT			dbo.E__EMPLOYEE.EMPLOYEEID, dbo.E__EMPLOYEE.FIRSTNAME +' '+ dbo.E__EMPLOYEE.LASTNAME AS FULLNAME, 
						dbo.E_JOBROLE.JobeRoleDescription, E_CONTACT.WORKEMAIL, 
						ISNULL(P_BLOCK.BLOCKNAME,'') +', '+ ISNULL(P_BLOCK.ADDRESS1,'') +' '+ ISNULL(P_BLOCK.ADDRESS2,'') +' '+ ISNULL(P_BLOCK.ADDRESS3,'') +', '+ ISNULL(P_BLOCK.TOWNCITY ,'')+' '+ ISNULL(P_BLOCK.POSTCODE ,'') As PROPERTYADDRESS
		FROM            dbo.E__EMPLOYEE INNER JOIN
						dbo.E_JOBROLETEAM ON dbo.E__EMPLOYEE.JobRoleTeamId = dbo.E_JOBROLETEAM.JobRoleTeamId INNER JOIN
						dbo.E_JOBROLE ON dbo.E_JOBROLETEAM.JobRoleId = dbo.E_JOBROLE.JobRoleId INNER JOIN
						dbo.E_CONTACT ON dbo.E__EMPLOYEE.EMPLOYEEID = dbo.E_CONTACT.EMPLOYEEID Left join
						P_BLOCK on P_BLOCK.BLOCKID = @propertyId
		WHERE			JobeRoleDescription = 'Executive Property Director' OR
						JobeRoleDescription = 'Head of property Operations' OR 
						JobeRoleDescription = 'Interim Head of Asset Management'
	END

	if(@requestType = 'scheme')
	BEGIN
		SELECT			dbo.E__EMPLOYEE.EMPLOYEEID, dbo.E__EMPLOYEE.FIRSTNAME +' '+ dbo.E__EMPLOYEE.LASTNAME AS FULLNAME, 
						dbo.E_JOBROLE.JobeRoleDescription, E_CONTACT.WORKEMAIL, 
						ISNULL(P_SCHEME.SCHEMENAME,'') +' '+ ISNULL(PDR_DEVELOPMENT.POSTCODE ,'') As PROPERTYADDRESS
		FROM            dbo.E__EMPLOYEE INNER JOIN
						dbo.E_JOBROLETEAM ON dbo.E__EMPLOYEE.JobRoleTeamId = dbo.E_JOBROLETEAM.JobRoleTeamId INNER JOIN
						dbo.E_JOBROLE ON dbo.E_JOBROLETEAM.JobRoleId = dbo.E_JOBROLE.JobRoleId INNER JOIN
						dbo.E_CONTACT ON dbo.E__EMPLOYEE.EMPLOYEEID = dbo.E_CONTACT.EMPLOYEEID Left join
						P_SCHEME ON P_SCHEME.SCHEMEID = @propertyId
						INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
		WHERE			JobeRoleDescription = 'Executive Property Director' OR
						JobeRoleDescription = 'Head of property Operations' OR 
						JobeRoleDescription = 'Interim Head of Asset Management'
	END
END