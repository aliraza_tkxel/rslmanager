SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_RemoveAlertsAccessRightsForTeam
-- Author:<Ahmed Mehmood>
-- Create date: <Create Date,,26/12/2012>
-- Description:	<Remove alerts access rights for team>
-- Web Page: Alerts.ascx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_RemoveAlertsAccessRightsForTeam](
@teamJobRoleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM E_TEAMALERTS 
	WHERE E_TEAMALERTS.JobRoleTeamId = @teamJobRoleId
END
GO
