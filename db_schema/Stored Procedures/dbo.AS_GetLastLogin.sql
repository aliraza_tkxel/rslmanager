SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Hussain Ali
-- Create date: 07/12/2012
-- Description:	Gets the last login for the required user
-- Useage: dashboard.aspx
-- Exec [dbo].[AS_GetLastLogin]
--			@employeeId = 0
-- =============================================

CREATE PROCEDURE [dbo].[AS_GetLastLogin] 
	-- Add the parameters for the stored procedure here
	@employeeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT Top 2
		AC_LOGIN_HISTORY.FirstName + ' ' + AC_LOGIN_HISTORY.LastName as name,
		CONVERT(varchar(20), AC_LOGIN_HISTORY.LoginDate, 113) as time
	FROM 
		AC_LOGIN_HISTORY
		INNER JOIN AC_LOGINS on AC_Logins.LOGIN = AC_LOGIN_HISTORY.UserName 
	WHERE 
		AC_LOGINS.EMPLOYEEID = @employeeId
	ORDER BY 
		LoginDate desc
END

GO
