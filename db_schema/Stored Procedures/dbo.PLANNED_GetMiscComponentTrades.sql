USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetMiscComponentTrades]    Script Date: 11/11/2016 10:52:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =============================================
--EXEC	[dbo].[PLANNED_GetMiscComponentTrades]	@journalId = 399
-- Author:		<Author,Altamish Arif>
-- Create date: <Create Date,,20 Nov,2013>
-- Last Updated By: Altamish Arif
-- Last Updated Date: 20th Nov 2013
-- Description:	<Description,,This stored procedure fetch trades & its status against the component >
-- WebPage: AvailableAppointments.aspx
-- ============================================= */
IF OBJECT_ID('dbo.[PLANNED_GetMiscComponentTrades]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetMiscComponentTrades] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetMiscComponentTrades] 
	-- Add the parameters for the stored procedure here
	@journalId as int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT J.JOURNALID 
,T.TradeId							AS ComponentTradeId
,T.Description						AS Trade
,MW.Duration						AS Duration	
,S.STATUSID							AS StatusId
,S.TITLE							AS Status
,CASE WHEN J.PROPERTYID is not null then  J.PROPERTYID
	when J.BlockId > 0 THEN  CONVERT(nvarchar(10), PB.BLOCKID)
	when J.SchemeId > 0 THEN CONVERT(nvarchar(10), PS.SCHEMEID)
	end as PropertyId  
,CASE WHEN J.PROPERTYID is not null then  ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')
 + ' ' + ISNULL(P.ADDRESS2,'')
	when J.BlockId > 0 THEN  ISNULL(PB.BLOCKNAME,'') + ' ' + ISNULL(PB.ADDRESS1,'')
 + ' ' + ISNULL(PB.ADDRESS2,'')
	when J.SchemeId > 0 THEN ISNULL(PS.SCHEMENAME,'')
	end as PropertyAddress  
,PAT.Planned_Appointment_Type
,CASE WHEN J.PROPERTYID is not null then  'Property'
	when J.BlockId > 0 THEN  'Block'
	when J.SchemeId > 0 THEN 'Scheme'
	end as Type  		
FROM
PLANNED_JOURNAL J
LEFT JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID
LEFT JOIN P_SCHEME PS ON PS.SCHEMEID = J.SchemeId
LEFT JOIN P_BLOCK PB ON PB.BLOCKID = J.BlockId
INNER JOIN PLANNED_MISC_TRADE MW ON J.JOURNALID = MW.JournalId
INNER JOIN G_TRADE T ON T.TradeId = MW.TRADEID
INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID
INNER JOIN Planned_Appointment_Type PAT ON PAT.Planned_Appointment_TypeId=J.APPOINTMENTTYPEID
WHERE J.JOURNALID = @journalId
AND MW.TradeId NOT IN
	(
		SELECT
			MISCTRADEID
		FROM
			PLANNED_REMOVED_SCHEDULING_TRADES
		WHERE
			JOURNALID = @journalId
	)
END
