SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC PLANNED_GetActionByStatusId @statusId = 0
-- Author:		<Noor Muhamad>
-- Create date: <12/12/2013>
-- Description:	<Get actions of "Inspection Arranged">
-- Web Page: ReplacementList.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_InspectionArrangedActions]
		

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT PLANNED_Action.ActionId,PLANNED_Action.Title
  FROM PLANNED_Action
  WHERE StatusId = (SELECT StatusId FROM PLANNED_STATUS WHERE PLANNED_STATUS.TITLE ='Inspection Arranged')

END
GO
