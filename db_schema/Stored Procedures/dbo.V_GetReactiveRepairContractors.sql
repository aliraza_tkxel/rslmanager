USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetReactiveRepairContractors]    Script Date: 08/01/2016 18:05:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 25/08/2015
-- Description:	To get contractors having at lease one Reactive Repair contract, as drop down values for assgin work to contractor.
-- =============================================

IF OBJECT_ID('dbo.V_GetReactiveRepairContractors') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.V_GetReactiveRepairContractors AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[V_GetReactiveRepairContractors] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID 
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID 
	WHERE AoW.DESCRIPTION = 'Voids' 	--Replaced 'Reactive Repair' with 'Voids'		
			--AND (S.CREATIONDATE < GETDATE()
			--	AND S.RENEWALDATE > GETDATE())
	
END
