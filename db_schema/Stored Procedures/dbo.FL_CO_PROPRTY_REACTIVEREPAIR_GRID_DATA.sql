SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[FL_CO_PROPRTY_REACTIVEREPAIR_GRID_DATA]
(
   @propertyId varchar (30)
)

/* ===========================================================================
 '   NAME:           FL_CO_PROPRTY_REACTIVEREPAIR_GRID_DATA
 '   DATE CREATED:   7th July, 2009
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To Populate Property Reactive Repair Grid
 '   IN:             @PropertyId
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS

	SELECT 
	CONVERT(varchar, FL_FAULT_JOURNAL.CreationDate, 103) as LastActionDate , FL_FAULT_STATUS.DESCRIPTION as FaultStatus,
	CONVERT(varchar,  FL_FAULT_LOG.DueDate, 103) as DueDate, FL_FAULT_LOG.FaultLogID, FL_FAULT.FaultID, FL_FAULT_LOG.StatusID, 
	FL_FAULT.[Description] AS FaultDescription,FL_FAULT.NetCost,FL_FAULT.PreInspection, Convert(varchar, FL_FAULT_LOG.SubmitDate, 103) as SubmitDate,
	C_ITEM.ItemID, C_ITEM.[Description] AS ItemDescription, C_NATURE.[Description] as NatureDescription, C_NATURE.ITEMNATUREID 
	FROM
	FL_FAULT_STATUS 
	INNER JOIN FL_FAULT_JOURNAL ON FL_FAULT_STATUS.FaultStatusId = FL_FAULT_JOURNAL.FaultStatusID 
	INNER JOIN C_ITEM ON FL_FAULT_JOURNAL.ItemId = C_ITEM.ItemId 
	INNER JOIN C_NATURE ON C_ITEM.ItemId = C_NATURE.ItemId
	INNER JOIN FL_FAULT_LOG ON FL_FAULT_JOURNAL.FaultLogId = FL_FAULT_LOG.FaultLogId
	INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultId = FL_FAULT.FaultId
	INNER JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID 
	INNER JOIN C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID --AND C_CUSTOMERTENANCY.ENDDATE IS NULL 
	INNER JOIN C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID --AND C_TENANCY.ENDDATE IS NULL  
	INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
	WHERE ( C_ITEM.ItemID =1 AND P__PROPERTY.PROPERTYID =  CONVERT(varchar, @propertyId)  AND C_NATURE.DESCRIPTION = 'REACTIVE REPAIR' )
	ORDER BY FL_FAULT_LOG.FaultLogId DESC

    

RETURN




GO
