SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[C_SSIS_STARSURVEY_PROCESS]

AS

SET NOCOUNT ON

--+-------------------------------------------------------------
--+ Calculate the total number of records needed for this run. 
--+ This should always be 25% of total live tenants
--+------------------------------------------------------------

DECLARE @REQUIREDCOUNT INT 
SET @REQUIREDCOUNT = (SELECT CAST(((SELECT COUNT(*) FROM dbo.vw_PROPERTY_CURRENT_TENANTS_LIST) * 0.25) AS INT))

DECLARE @STARSURVEY TABLE
(
	ID INT IDENTITY(1,1),
	CUSTOMERID INT,
	TITLE VARCHAR(30),
	FIRSTNAME VARCHAR(50),
	LASTNAME VARCHAR(50),
	HOUSENUMBER VARCHAR(50),
	ADDRESS1 VARCHAR(100),
	ADDRESS2 VARCHAR(100),
	TOWNCITY VARCHAR(80),
	COUNTY VARCHAR(50),
	POSTCODE VARCHAR(10),
	TENANCYID INT, 
	PROPERTYID VARCHAR(20),
	[DESCRIPTION] VARCHAR(50),
	PROCESSED BIT DEFAULT(0)
)
DECLARE @EXCLUDEDRUNS TABLE
(
	STARSURVEYRUNID INT	
)
INSERT  INTO @EXCLUDEDRUNS
        ( STARSURVEYRUNID
        )
        SELECT TOP ( 3 )
                STARSURVEYRUNID
        FROM    dbo.C_STARSURVEYRUN
        ORDER BY STARSURVEYRUNID DESC

;WITH CTE_PREVIOUS_RUN_TENANTS (CUSTOMERID)
AS
(
SELECT DISTINCT CUSTOMERID FROM dbo.C_JOURNAL jr
INNER JOIN C_STARSURVEY ss ON jr.JOURNALID = ss.JOURNALID
INNER JOIN @EXCLUDEDRUNS e ON e.STARSURVEYRUNID = ss.STARSURVEYRUNID
)
,CTE_CURRENT_TENANTS ( CUSTOMERID, TITLE, FIRSTNAME, LASTNAME, HOUSENUMBER, ADDRESS1, ADDRESS2, TOWNCITY, COUNTY, POSTCODE, TENANCYID, PROPERTYID, [DESCRIPTION] )
          AS ( SELECT   ISNULL(CAST(c.CUSTOMERID AS VARCHAR), '') AS CUSTOMERID ,
                        ISNULL(ttl.DESCRIPTION, '') AS TITLE ,
                        ISNULL(c.FIRSTNAME, '') AS FIRSTNAME ,
                        ISNULL(c.LASTNAME, '') AS LASTNAME ,
                        ISNULL(p.HOUSENUMBER, '') AS HOUSENUMBER ,
                        ISNULL(p.ADDRESS1, '') AS ADDRESS1 ,
                        ISNULL(p.ADDRESS2, '') AS ADDRESS2 ,
                        ISNULL(p.TOWNCITY, '') AS TOWNCITY ,
                        ISNULL(p.COUNTY, '') AS COUNTY ,
                        ISNULL(p.POSTCODE, '') AS POSTCODE ,
                        ISNULL(CAST(t.TENANCYID AS VARCHAR), '') AS TENANCYID ,
                        ISNULL(CAST(p.PROPERTYID AS VARCHAR), '') AS PROPERTYID ,
                        ISNULL(at.DESCRIPTION, '') AS DESCRIPTION
               FROM     C__CUSTOMER C
                        INNER JOIN dbo.C_CUSTOMERTENANCY ct ON ct.CUSTOMERID = c.CUSTOMERID
                                                              AND ct.ENDDATE IS NULL
                        INNER JOIN dbo.C_TENANCY t ON t.TENANCYID = ct.TENANCYID
                                                      AND t.ENDDATE IS NULL
                        INNER JOIN dbo.P__PROPERTY p ON p.PROPERTYID = t.PROPERTYID
                        INNER JOIN dbo.P_ASSETTYPE at ON at.ASSETTYPEID = p.ASSETTYPE
                        LEFT JOIN dbo.G_TITLE ttl ON ttl.TITLEID = c.TITLE
                        LEFT JOIN CTE_PREVIOUS_RUN_TENANTS pr ON pr.CUSTOMERID = c.CUSTOMERID
			   WHERE 
						pr.CUSTOMERID IS NULL -- does not exist in the previous set of runs
             )
	INSERT INTO @STARSURVEY
	        ( CUSTOMERID ,
	          TITLE ,
	          FIRSTNAME ,
	          LASTNAME ,
	          HOUSENUMBER ,
	          ADDRESS1 ,
	          ADDRESS2 ,
	          TOWNCITY ,
	          COUNTY ,
	          POSTCODE ,
	          TENANCYID ,
	          PROPERTYID ,
	          DESCRIPTION
	        )
    SELECT  TOP (@REQUIREDCOUNT)
			CUSTOMERID ,
            TITLE ,
            FIRSTNAME ,
            LASTNAME ,
            HOUSENUMBER ,
            ADDRESS1 ,
            ADDRESS2 ,
            TOWNCITY ,
            COUNTY ,
            POSTCODE ,
            TENANCYID ,
            PROPERTYID ,
            [DESCRIPTION]
    FROM    CTE_CURRENT_TENANTS
    ORDER BY NEWID()
  
--+-----------------------------------------
--+ Set up a new run
--+-----------------------------------------

DECLARE @FOUNDCOUNT INT 
SET @FOUNDCOUNT = (SELECT COUNT(*) FROM @STARSURVEY)

DECLARE @STARSURVEYRUNID INT
INSERT INTO dbo.C_STARSURVEYRUN ( RUNDATE, REQUIREDCOUNT, FOUNDCOUNT ) SELECT GETDATE(), @REQUIREDCOUNT, @FOUNDCOUNT

SET @STARSURVEYRUNID = SCOPE_IDENTITY()

DECLARE @ID INT = 1
DECLARE @PROPERTYID VARCHAR(20)
DECLARE @CUSTOMERID INT
DECLARE @TENANCYID INT

--+-----------------------------------------
--+ Start run and process records
--+-----------------------------------------

WHILE EXISTS(SELECT * FROM @STARSURVEY WHERE ID = @ID)
BEGIN
	
	SET @PROPERTYID = NULL
	SET @CUSTOMERID = NULL
	SET @TENANCYID = NULL

	SELECT 
		@PROPERTYID = PROPERTYID, 
		@CUSTOMERID = CUSTOMERID, 
		@TENANCYID = TENANCYID 
	FROM 
		@STARSURVEY
	WHERE
		ID = @ID
	
	BEGIN TRANSACTION

	BEGIN TRY
		
		EXEC C_STARSURVEY_INSERT @PROPERTYID, @CUSTOMERID, @TENANCYID, @STARSURVEYRUNID
		UPDATE @STARSURVEY SET PROCESSED = 1 WHERE ID = @ID
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		BEGIN
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION
		END
	END CATCH

	SET @ID = @ID + 1

END

--+-----------------------------------------
--+ Finalise run
--+-----------------------------------------

DECLARE @PROCESSEDCOUNT INT 
SET @PROCESSEDCOUNT = (SELECT COUNT(*) FROM @STARSURVEY WHERE PROCESSED = 1)

UPDATE dbo.C_STARSURVEYRUN SET PROCESSEDCOUNT = @PROCESSEDCOUNT WHERE STARSURVEYRUNID = @STARSURVEYRUNID


GO
