SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[I_NEWS_SELECT]
AS
	SET NOCOUNT ON;
SELECT     NewsID, NewsCategory, Headline, ImagePath, DateCreated, NewsContent, Rank, Active, HomePageImage, IsHomePage, IsTenantOnline
FROM         I_NEWS
ORDER BY NEWSID DESC




GO
