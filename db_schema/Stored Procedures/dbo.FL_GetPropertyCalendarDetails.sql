USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetPropertyCalendarDetails]    Script Date: 01/26/2017 11:59:12 ******/



IF OBJECT_ID('dbo.[FL_GetPropertyCalendarDetails]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[FL_GetPropertyCalendarDetails] AS SET NOCOUNT ON;')
GO
ALTER PROCEDURE [dbo].[FL_GetPropertyCalendarDetails] @schemeId int = -1  
, @appointmentType nvarchar(50) = ''  
, @patchId int = -1  
, @tradeId int = -1  
, @operativeId int = -1  
, @jsn nvarchar(50) = ''  
, @tenant nvarchar(200) = ''  
, @address nvarchar(200) = ''  
, @startDate datetime  
, @endDate datetime  
AS  
BEGIN  
  DECLARE @currentDateTime AS datetime2 = GETDATE()  
  DECLARE @today AS date = @currentDateTime  
  
  /* ===============================================================  
  * Get operatives for selected trades  
  * =============================================================== */  
  
  --SET  
  --@startDate = DATEADD(DAY, DATEDIFF(DAY, 0, @startDate), '00:00:00')  
  --SET  
  --@endDate = DATEADD(DAY, DATEDIFF(DAY, 0, @endDate), '23:59:59')  
  DECLARE @Operatives AS TABLE (  
    EmployeeID int,  
    Name varchar(200)  
  )  
If(OBJECT_ID('tempdb..#TempAppointment') Is Not Null)
Begin
    Drop Table #TempAppointment
End
CREATE TABLE  #TempAppointment
	(
		js nvarchar(50)COLLATE DATABASE_DEFAULT, 
		title NVarchar(100) COLLATE DATABASE_DEFAULT
	)
   Insert into #TempAppointment(js, title) VALUES('JSD','Appliance Defect')
   Insert into #TempAppointment(js, title) VALUES('JSN','Cyclic Maintenance')
   Insert into #TempAppointment(js, title) VALUES('JSN','M&E Servicing')
   Insert into #TempAppointment(js, title) VALUES('All','Cyclic Maintenance')
   Insert into #TempAppointment(js, title) VALUES('All','M&E Servicing')
   Insert into #TempAppointment(js, title) VALUES('All','Appliance Defect')
   
  INSERT INTO @Operatives (EmployeeID, Name)  
    SELECT DISTINCT  
      U.EmployeeId AS EMPLOYEEID,  
      E.FIRSTNAME + ' ' + E.LASTNAME AS Name  
    FROM AS_USER AS U  
  INNER JOIN (Select UserTypeID from AS_USERTYPE where Description IN ('Gas Engineer','Repair Operative','Manager','Frontline')) AS_USERTYPE ON U.UserTypeID = AS_USERTYPE.UserTypeID  
        INNER JOIN E__EMPLOYEE AS E ON U.EmployeeId = E.EMPLOYEEID  
        LEFT JOIN E_TRADE AS T ON E.EMPLOYEEID = T.EmpId AND T.TradeId = @tradeId          
        INNER JOIN E_JOBDETAILS ON U.EmployeeId = E_JOBDETAILS.EMPLOYEEID   
    WHERE (@tradeId = -1 OR T.TradeId IS NOT NULL)  
     AND (@operativeId = -1 OR U.EmployeeId = @operativeId)    
      AND U.IsActive=1 AND (E_JOBDETAILS.ACTIVE = 1)  
  
  
  /* ===============================================================  
  * Get The list of operatives for calendar.  
  * =============================================================== */  
  
  SELECT  
    *  
  FROM @operatives  
  
  /* ===============================================================  
  * Get Leaves Information for selected operatives  
  * =============================================================== */  
SELECT  distinct 
 J.EMPLOYEEID          AS EMPLOYEEID  
 ,COALESCE(N.DESCRIPTION, A.REASON, E_ABSENCEREASON.DESCRIPTION, 'N/A')  
 + CASE  
  WHEN DATEPART(HOUR, A.STARTDATE) > 0  
   AND DATEPART(HOUR, A.RETURNDATE) > 0  
   THEN '<br /> ' + CONVERT(NVARCHAR(5), A.STARTDATE, 108) + ' - '  
    + CONVERT(NVARCHAR(5), A.RETURNDATE, 108)  
  ELSE ''  
 END             AS REASON  
 ,CONVERT(DATE, A.STARTDATE)       AS StartDate 
,CASE WHEN (J.ITEMNATUREID = 1 AND A.ITEMSTATUSID = 2 AND A.HOLTYPE NOT IN ('M','F-M','A-M') AND A.RETURNDATE IS NOT NULL AND DATEDIFF(day,A.STARTDATE,A.RETURNDATE)>0) THEN 
	CONVERT(DATE, DATEADD(DD,-1,A.RETURNDATE))	
	ELSE
	CONVERT(DATE, A.RETURNDATE)  	
  END AS EndDate    
 ,J.EMPLOYEEID          AS OperativeId  
 ,A.HolType  
 ,A.duration  
 ,CASE  
  WHEN (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A  
   .STARTDATE)))  
   THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')  
  WHEN HolType = '' OR HolType is null  
   THEN '08:00 AM'  
  WHEN HolType = 'M-M'  
   THEN '08:00 AM'  
  WHEN HolType = 'M'  
   THEN '08:00 AM'  
  WHEN HolType = 'A'  
   THEN '01:00 PM'  
  WHEN HolType = 'F'  
   THEN '00:00 AM'  
  WHEN HolType = 'F-F'  
   THEN '00:00 AM'  
  WHEN HolType = 'F-M'  
   THEN '00:00 AM'  
  WHEN HolType = 'A-F'  
   THEN '01:00 PM'  
  WHEN HolType = 'A-A'
   THEN '08:00 AM'
 END             AS StartTime 
 ,CASE
		WHEN (J.ITEMNATUREID = 1 AND A.ITEMSTATUSID = 2 AND A.HOLTYPE NOT IN ('M','F-M','A-M') AND A.RETURNDATE IS NOT NULL AND DATEDIFF(day,A.STARTDATE,A.RETURNDATE)>0) THEN
			CASE 
				WHEN (DATEADD(DD,-1,A.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,A.RETURNDATE))))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, DATEADD(DD,-1,A.RETURNDATE), 0), 8)), 'AM', ' AM'), 'PM', ' PM')
				WHEN HolType = '' or HOLTYPE is null 
				THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
				WHEN HolType = 'M-M'  
				THEN '12:00 PM' 
				WHEN HolType = 'M'
				THEN '12:00 PM'
				WHEN HolType = 'A'
				THEN '05:00 PM'
				WHEN HolType = 'F'
				THEN '11:59 PM'
				WHEN HolType = 'F-F'
				THEN '11:59 PM'
				WHEN HolType = 'F-M'
				THEN '12:00 PM'
				WHEN HolType = 'A-F'
				THEN '11:59 PM'
				WHEN HolType = 'A-M'  
				THEN '12:00 PM'	
				WHEN HolType = 'A-A'
				THEN '12:00 PM'
			END 
		ELSE
			CASE
				WHEN
				(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE)))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
				WHEN HolType = '' or HOLTYPE is null 
				THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
				WHEN HolType = 'M-M'  
				THEN '12:00 PM' 
				WHEN HolType = 'M'
				THEN '12:00 PM'
				WHEN HolType = 'A'
				THEN '05:00 PM'
				WHEN HolType = 'F'
				THEN '11:59 PM'
				WHEN HolType = 'F-F'
				THEN '11:59 PM'
				WHEN HolType = 'F-M'
				THEN '12:00 PM'
				WHEN HolType = 'A-F'
				THEN '11:59 PM'
				WHEN HolType = 'A-M'  
				THEN '12:00 PM'	
				WHEN HolType = 'A-A'
				THEN '12:00 PM'
			END	
	END			AS EndTime  
 ,CASE  
  WHEN  
   (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.STARTDATE)))  
   THEN DATEDIFF(mi, '1970-01-01', A.STARTDATE)  
  WHEN HolType = '' OR HolType is null  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))  
  WHEN HolType = 'M-M'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))  
  WHEN HolType = 'M'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))  
  WHEN HolType = 'A'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))  
  WHEN HolType = 'F'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))  
  WHEN HolType = 'F-F'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))  
  WHEN HolType = 'F-M'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))  
  WHEN HolType = 'A-F'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))  
  WHEN HolType = 'A-A'
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
 END             AS StartTimeInMin  
  ,CASE
   WHEN (J.ITEMNATUREID = 1 AND A.ITEMSTATUSID = 2 AND A.HOLTYPE NOT IN ('M','F-M','A-M') AND A.RETURNDATE IS NOT NULL AND DATEDIFF(day,A.STARTDATE,A.RETURNDATE)>0) THEN
		CASE
		WHEN (DATEADD(DD,-1,A.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,A.RETURNDATE)))) THEN 
		DATEDIFF(mi, '1970-01-01', DATEADD(DD,-1,A.RETURNDATE))
		WHEN HolType = '' or HOLTYPE is null 
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00', 103))
		WHEN HolType = 'M-M'  
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103)) 
		WHEN HolType = 'M'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '05:00 PM', 103))
		WHEN HolType = 'F'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'F-F'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'F-M'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A-F'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '11:59 PM', 103))	
		WHEN HolType = 'A-M'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A-A'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		END 	
	ELSE
		CASE
		WHEN (A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE)))THEN 
		DATEDIFF(mi, '1970-01-01', A.RETURNDATE)
		WHEN HolType = '' or HOLTYPE is null 
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00', 103))
		WHEN HolType = 'M-M'  
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103)) 
		WHEN HolType = 'M'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
		WHEN HolType = 'F'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'F-F'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'F-M'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A-F'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'A-M'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A-A'
		THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))	
		END	
	END	AS EndTimeInMin  
FROM  
 E_JOURNAL J  
  -- This join is added to just bring Max History Id of every Journal    
  CROSS APPLY  
  (  
   SELECT  
    MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryId  
   FROM  
    E_ABSENCE  
   WHERE  
    JOURNALID = J.JOURNALID  
  ) AS MaxAbsence  
  INNER JOIN E_ABSENCE A ON MaxAbsence.MaxAbsenceHistoryId = A.ABSENCEHISTORYID  
  INNER JOIN E_STATUS ON A.ITEMSTATUSID = E_STATUS.ITEMSTATUSID  
  LEFT JOIN E_ABSENCEREASON ON A.REASONID = E_ABSENCEREASON.SID  
  INNER JOIN E_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID  
  INNER JOIN @Operatives O ON O.EMPLOYEEID = J.EMPLOYEEID  
WHERE  
 (  
 -- To filter for planned i.e annual leaves etc.   
 (E_STATUS.ITEMSTATUSID in (3,5)  
 AND J.ITEMNATUREID >= 2 and J.ITEMNATUREID != (select ITEMNATUREID from E_NATURE where DESCRIPTION = 'BRS TOIL Recorded')) 
 OR  
 -- To filter for sickness leaves. where approval is not needed    
 (J.ITEMNATUREID = 1  
 AND E_STATUS.ITEMSTATUSID <> 20)  
 )
 AND  
 ((A.STARTDATE >= @startDate AND A.RETURNDATE <= @endDate)
 OR (A.RETURNDATE >= @startDate AND A.RETURNDATE <= @endDate AND A.STARTDATE < @startDate)
 OR (A.STARTDATE <= @startDate AND A.RETURNDATE >=@endDate)
 OR (A.STARTDATE >= @startDate AND A.RETURNDATE >= @endDate)
 OR (A.STARTDATE <= @endDate AND A.RETURNDATE IS NULL))
 AND A.STARTDATE <= @endDate


-- Get Bank Holidays   
UNION ALL SELECT DISTINCT  
 O.EMPLOYEEID                                  AS EMPLOYEEID  
 ,'Bank Holiday'                                  AS REASON  
 ,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)                          AS StartDate  
 ,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)                          AS EndDate  
 ,O.EmployeeId                                  AS OperativeId  
 ,'F'                                    AS HolType  
 ,1                                     AS duration  
 ,'00:00 AM'                                   AS StartTime  
 ,'11:59 PM'                                   AS EndTime  
 ,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))      AS StartTimeInMin  
 ,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103)) AS EndTimeInMin  
FROM  
 G_BANKHOLIDAYS bh  
  CROSS JOIN @Operatives O  
WHERE  
 bh.BHDATE BETWEEN @startDate AND @endDate  
 AND bh.BHA = 1   
 
   -- ==========================================================================================      
    -- Training  
    -- ========================================================================================== 
	UNION ALL  
    SELECT  distinct 
 EET.EMPLOYEEID          AS EMPLOYEEID  
 ,'Training: ' + COALESCE(EET.Course, 'N/A')  + '<br /> Location: ' + EET.Location 
 + '<br /> ' + Convert(nvarchar,CONVERT(DATE, EET.STARTDATE))  + ' - '  
    + Convert(nvarchar,CONVERT(DATE, EET.ENDDATE))  AS REASON 
 ,CAST(EET.StartDate AS datetime)       AS StartDate  
 ,CAST(EET.StartDate AS datetime) AS EndDate  
 ,EET.EMPLOYEEID          AS OperativeId 
 ,''    AS HolType  
 ,EET.TotalNumberOfDays  AS duration 
 ,''      AS StartTime  
 ,'' AS EndTime
 ,''      AS StartTimeInMin  
 ,'' AS EndTimeInMin    

FROM  
E_EmployeeTrainings EET    
  INNER JOIN @Operatives O ON O.EMPLOYEEID = EET.EMPLOYEEID  
  WHERE EET.Active=1 AND EET.Status in (1,2)
ORDER BY StartDate ;  
  
  /* ===============================================================  
  * Get Appointments For Selected Operatives and Trades  
  * =============================================================== */  
  
  WITH CTE_Appointments (JobsheetNumber, AppointmentDate, OperativeID, [Time], NAME, MOBILE, CustAddress, POSTCODE, FaultStatus, AppointmentID, EndTime, StartTimeInSec, EndTimeInSec, TimeToOrderAppointments, TenancyId, IsCalendarAppointment, AppointmentType, AppointmentEndDate,AppointmentNotes,InspectionType,IsVoid,ddlAppointmentType)  
  AS (  
  
    -- ==========================================================================================      
    -- FAULT APPOINTMENTS  
    -- ==========================================================================================   
  
    SELECT DISTINCT  

	FL_FAULT_LOG.JobSheetNumber as JobsheetNumber,
      CONVERT(varchar(20), APPOINTMENTDATE, 103) AS AppointmentDate,  
      FL_CO_APPOINTMENT.OperativeID AS OperativeID,  
		
      FL_CO_APPOINTMENT.[Time] AS [Time],  
      'Test Name' AS Name,  
      'Test Mobile' AS MOBILE,  
      CASE  
        WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.TOWNCITY, '') + ', '  
          + ISNULL(P__PROPERTY.POSTCODE, '')  
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(', ' + P_BLOCK.TOWNCITY, '')  
          + ISNULL(', ' + P_BLOCK.POSTCODE, '')  
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
          + ISNULL(', ' + PDR_DEVELOPMENT.TOWN, '')  
          + ISNULL(', ' + PDR_DEVELOPMENT.COUNTY, '')  
          + ISNULL(', ' + PDR_DEVELOPMENT.PostCode, '')  
      END AS CustAddress,  
      'Test PostCode' AS POSTCODE,  
      FL_FAULT_STATUS.[Description] AS FaultStatus,  
      FL_CO_APPOINTMENT.AppointmentID AS AppointmentID,  
      FL_CO_APPOINTMENT.EndTime AS EndTime,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), appointmentdate, 103)  
      + ' ' + [Time], 103)) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), appointmentdate, 103)  
      + ' ' + EndTime, 103)) AS EndTimeInSec,  
      CONVERT(time, [TIME]) AS TimeToOrderAppointments,  
      C_TENANCY.TENANCYID AS TenancyId,  
      FL_CO_APPOINTMENT.isCalendarAppointment AS IsCalendarAppointment,  
      CASE  
        WHEN FL_FAULT_LOG.PROPERTYID IS NULL THEN 'SbFault'  
        ELSE 'Fault'  
      END AS AppointmentType,  
      CONVERT(varchar(20), COALESCE(AppointmentEndDate, AppointmentDate), 103) AS AppointmentEndDate  
      ,ISNULL(FL_CO_APPOINTMENT.Notes,'')  AS  AppointmentNotes  
      ,'Reactive' AS InspectionType  
   ,-1 as IsVoid   
  , 'Reactive' as ddlAppointmentType         
    FROM FL_CO_APPOINTMENT  
        INNER JOIN @Operatives AS O ON FL_CO_APPOINTMENT.OperativeID = O.EMPLOYEEID  
        INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId  
        INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID  
        INNER JOIN FL_FAULT_TRADE AS FT ON FL_FAULT_LOG.FaultTradeID = FT.FaultTradeId  
            --AND @tradeId IN (FT.TradeId, -1, '')
			OR @tradeId <> FT.TradeId  
        INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID  
        LEFT JOIN C__CUSTOMER C ON FL_FAULT_LOG.CustomerId = C.CUSTOMERID  
        LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID  
            AND @schemeId IN (P__PROPERTY.SCHEMEID, -1)  
            
        LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID  
            AND (C_TENANCY.ENDDATE IS NULL  
            OR C_TENANCY.ENDDATE >= @today)  
        LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
            AND @schemeId IN (P_SCHEME.SCHEMEID, -1)  
        LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID  
        LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID=PDR_DEVELOPMENT.DEVELOPMENTID AND  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)  
    WHERE FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate, 103)  
      AND  CONVERT(date, @endDate, 103)  
      AND FL_FAULT_STATUS.[Description] != 'Cancelled'  
      AND (FL_FAULT_LOG.PROPERTYID <> ''  
      OR FL_FAULT_LOG.SCHEMEID IS NOT NULL  
      OR FL_FAULT_LOG.BlockId > 0)  
      AND  
         (CASE  
           WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
             + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
             + ISNULL(P__PROPERTY.TOWNCITY, '') + ', '  
             + ISNULL(P__PROPERTY.POSTCODE, '')  
           WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
             + ISNULL(', ' + P_BLOCK.TOWNCITY, '')  
             + ISNULL(', ' + P_BLOCK.POSTCODE, '')  
           WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
             + ISNULL(', ' + PDR_DEVELOPMENT.TOWN, '')  
             + ISNULL(', ' + PDR_DEVELOPMENT.COUNTY, '')  
             + ISNULL(', ' + PDR_DEVELOPMENT.PostCode, '')  
         END LIKE '%' + @address + '%' OR @address='' )
      AND (C.FIRSTNAME + ISNULL(' ' + C.MIDDLENAME, '') + ISNULL(' ' + C.LASTNAME, '') LIKE '%' + @tenant + '%'   OR @tenant ='')
       AND (@jsn = 'All'  OR @jsn = 'JS') 
  
  
    -- ==========================================================================================      
    -- PLANNED APPOINTMENTS  
    -- ==========================================================================================   
    UNION ALL  
    SELECT DISTINCT 
	'JSN' +RIGHT('0000'+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),4)
	  as JobsheetNumber, 
      CONVERT(varchar(20), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103) AS AppointmentDate,  
      PLANNED_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
      PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME AS Time,  
      'Test Name' AS Name,  
      'Test Mobile' AS MOBILE,  
      CASE  
        WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.TOWNCITY, '') + ', '  
          + ISNULL(P__PROPERTY.POSTCODE, '')  
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(', ' + P_BLOCK.TOWNCITY, '')  
          + ISNULL(', ' + P_BLOCK.POSTCODE, '')  
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
          + ISNULL(', ' + PDR_DEVELOPMENT.TOWN, '')  
          + ISNULL(', ' + PDR_DEVELOPMENT.COUNTY, '')  
          + ISNULL(', ' + PDR_DEVELOPMENT.PostCode, '')  
      END AS CustAddress,   
      'Test PostCode' AS POSTCODE,  
     Case   
		WHEN COALESCE(PLANNED_SUBSTATUS.TITLE,PLANNED_APPOINTMENTS.APPOINTMENTSTATUS) = 'NotStarted' 
			OR COALESCE(PLANNED_SUBSTATUS.TITLE,PLANNED_APPOINTMENTS.APPOINTMENTSTATUS) = 'Accepted' THEN 
			'Appointment Arranged'  
		Else
			COALESCE(PLANNED_SUBSTATUS.TITLE,PLANNED_APPOINTMENTS.APPOINTMENTSTATUS)
		End as FaultStatus  ,  

      PLANNED_APPOINTMENTS.AppointmentID AS AppointmentID,  
      PLANNED_APPOINTMENTS.APPOINTMENTENDTIME AS EndTime,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)  
      + ' '  
      + PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME, 103)) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)  
      + ' '  
      + PLANNED_APPOINTMENTS.APPOINTMENTENDTIME, 103)) AS EndTimeInSec,  
      CONVERT(time, PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME) AS TimeToOrderAppointments,  
      '1' AS TenancyId,  
      0 AS IsCalendarAppointment,  
      ISNULL(PAT.Planned_Appointment_Type, 'Planned') AS AppointmentType,  
      CONVERT(varchar(20), PLANNED_APPOINTMENTS.APPOINTMENTENDDATE, 103) AS AppointmentEndDate  
      ,ISNULL(PLANNED_APPOINTMENTS.APPOINTMENTNOTES,'') AS  AppointmentNotes  
      ,'Planned' AS InspectionType   
   ,-1 as IsVoid  ,
  CASE     
  WHEN Planned_Appointment_Type = 'Adaptation' THEN   'Adaptations'  
  WHEN Planned_Appointment_Type = 'Misc' THEN   'Miscellaneous'  
  WHEN Planned_Appointment_Type = 'Condition' THEN   'Condition'  
  WHEN Planned_Appointment_Type = 'Planned' THEN   'Planned'  
  ELSE   
   'N/A'  


   END AS ddlAppointmentType  
  

     
    FROM PLANNED_APPOINTMENTS  
        INNER JOIN @Operatives AS O ON PLANNED_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID  
        LEFT JOIN Planned_Appointment_Type AS PAT ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = PAT.Planned_Appointment_TypeId  
        INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID  
        LEFT JOIN PLANNED_SUBSTATUS ON PLANNED_APPOINTMENTS.JOURNALSUBSTATUS = PLANNED_SUBSTATUS.SUBSTATUSID  
        LEFT JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID  
            AND @schemeId IN (P__PROPERTY.SCHEMEID, -1)                
            --AND ISNULL(P__PROPERTY.HouseNumber, '')  
            --+ ISNULL(' ' + P__PROPERTY.ADDRESS1, '')  
            --+ISNULL(', ' + P__PROPERTY.TOWNCITY, '')  
            --+ ISNULL(', ' + P__PROPERTY.POSTCODE, '') LIKE '%' + @address + '%'  
        --INNER JOIN PDR_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         --AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)       
        LEFT JOIN PLANNED_COMPONENT_TRADE AS PCT ON PLANNED_APPOINTMENTS.COMPTRADEID = PCT.COMPTRADEID  
        LEFT JOIN PLANNED_MISC_TRADE AS PMT ON PLANNED_APPOINTMENTS.APPOINTMENTID = PMT.AppointmentId
		LEFT JOIN P_SCHEME ON PLANNED_JOURNAL.SCHEMEID = P_SCHEME.SCHEMEID  
            AND @schemeId IN (P_SCHEME.SCHEMEID, -1)
        LEFT JOIN P_BLOCK ON PLANNED_JOURNAL.BlockId = P_BLOCK.BLOCKID  
		LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID=PDR_DEVELOPMENT.DEVELOPMENTID AND  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)      
        LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
        LEFT JOIN C_CUSTOMERTENANCY CT ON C_TENANCY.TENANCYID = CT.TENANCYID  
        LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID  
    WHERE ((PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate, 103)  
      AND  CONVERT(date, @endDate, 103)  )  
      OR (PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(date, @startDate, 103)  
      AND  CONVERT(date, @endDate, 103)   ))  
      AND APPOINTMENTSTATUS <> 'Cancelled'
	  AND (PLANNED_JOURNAL.PROPERTYID <> ''  
      OR PLANNED_JOURNAL.SCHEMEID IS NOT NULL  
      OR PLANNED_JOURNAL.BlockId > 0)  
      AND  
         (CASE  
           WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
             + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
             + ISNULL(P__PROPERTY.TOWNCITY, '') + ', '  
             + ISNULL(P__PROPERTY.POSTCODE, '')  
           WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
             + ISNULL(', ' + P_BLOCK.TOWNCITY, '')  
             + ISNULL(', ' + P_BLOCK.POSTCODE, '')  
           WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
             + ISNULL(', ' + PDR_DEVELOPMENT.TOWN, '')  
             + ISNULL(', ' + PDR_DEVELOPMENT.COUNTY, '')  
             + ISNULL(', ' + PDR_DEVELOPMENT.PostCode, '')  
         END LIKE '%' + @address + '%' OR @address='' )  
      --AND ISPENDING = 0  
     -- AND @tradeId IN (PCT.TRADEID, PMT.TradeId, 0, -1,'')
	 -- OR @tradeId NOT IN (PCT.TRADEID, PMT.TradeId)  
      AND (C.FIRSTNAME + ISNULL(' ' + C.MIDDLENAME, '') + ISNULL(' ' + C.LASTNAME, '') LIKE '%' + @tenant + '%'   OR @tenant ='')
      AND (@jsn = 'All'  OR @jsn='JSN' ) 
    
  
  
    -- ==========================================================================================      
    -- PDR APPOINTMENTS  
    -- ==========================================================================================     
  
    ---Get PDR appointment e.g M&E Servicing, Maintenance Servicing  
    UNION ALL  
    SELECT DISTINCT 
    Case WHEN PDR_MSATType.MSATTypeName ='Appliance Defect'
    THEN 
    'JSD' + RIGHT('00000'+ CONVERT(VARCHAR,defect.PropertyDefectId),5)
	
	ELSE
	'JSN' + Convert( NVarchar, PDR_APPOINTMENTS.APPOINTMENTID  ) 
	END
	  as JobsheetNumber,
      CONVERT(varchar(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) AS AppointmentDate,  
      PDR_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
      PDR_APPOINTMENTS.APPOINTMENTSTARTTIME AS [Time],  
      'Test Name' AS Name,  
      'Test Mobile' AS MOBILE,  
      CASE  
        WHEN PDR_MSAT.propertyid IS NOT NULL THEN ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '')  
          + ISNULL(', ' + P__PROPERTY.TOWNCITY, '')  
          + ISNULL(' ' + P__PROPERTY.POSTCODE, '')  
        WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
          + ISNULL(' ' + P_SCHEME.SCHEMECODE, '')  
        WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN ISNULL(P_BLOCK.BLOCKNAME, '') + ' '  
          + ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(', ' + P_BLOCK.TOWNCITY, '')  
          + ISNULL(' ' + P_BLOCK.POSTCODE, '')  
      END AS CustAddress,  
      CASE  
        WHEN PDR_MSAT.propertyid IS NOT NULL THEN ISNULL(P__PROPERTY.POSTCODE, '')  
        WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN ISNULL(P_SCHEME.SCHEMECODE, '')  
        WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN ISNULL(P_BLOCK.POSTCODE, '')  
      END AS POSTCODE,  
      CASE  
        WHEN RTRIM(S.TITLE) = 'Arranged' THEN 'Appointment Arranged'  
        WHEN RTRIM(S.TITLE) = 'Completed' THEN 'Complete'
		WHEN RTRIM(S.TITLE) = 'InProgress' OR RTRIM(S.TITLE) = 'In Progress' THEN 'In Progress'  
        ELSE RTRIM(Coalesce(S.TITLE,PDR_APPOINTMENTS.APPOINTMENTSTATUS,'N/A'))
      END AS FaultStatus,  
      PDR_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
      PDR_APPOINTMENTS.APPOINTMENTENDTIME AS EndTime,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)  
      + ' '  
      + PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 103)) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)  
      + ' '  
      + PDR_APPOINTMENTS.APPOINTMENTENDTIME, 103)) AS EndTimeInSec,  
      CONVERT(time, PDR_APPOINTMENTS.APPOINTMENTSTARTTIME) AS TimeToOrderAppointments,  
      C_TENANCY.TENANCYID AS TenancyId,  
      0 AS IsCalendarAppointment,  
      PDR_MSATType.MSATTypeName AS AppointmentType,  
      CONVERT(varchar(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103) AS AppointmentEndDate  
   ,ISNULL(PDR_APPOINTMENTS.APPOINTMENTNOTES,'') AS  AppointmentNotes  
      ,'Pdr' AS InspectionType   
   ,-1 as IsVoid        
  , 'PDR' as ddlAppointmentType     
    FROM PDR_APPOINTMENTS  
        INNER JOIN @Operatives AS O ON PDR_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID  
        INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID          
        INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID  
        LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID  
            AND @schemeId IN (P_SCHEME.SCHEMEID,  -1)  
        LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID  
        LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID  
            AND @schemeId IN (P__PROPERTY.SCHEMEID,  -1) 
        INNER JOIN PDR_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)     
        INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId  
            AND MSATTypeName NOT LIKE '%Void%'  
        LEFT JOIN p_status ON p__property.[status] = p_status.statusid  
		
		LEFT JOIN P_PROPERTY_APPLIANCE_DEFECTS defect on PDR_JOURNAL.JOURNALID=defect.ApplianceDefectAppointmentJournalId
		LEFT JOIN PDR_STATUS S on defect.DefectJobSheetStatus = S.STATUSID
  
        LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid  
            AND (dbo.c_tenancy.enddate IS NULL  
            OR dbo.c_tenancy.enddate >= @today)  
        LEFT JOIN c_customer_names_grouped CG ON CG.i = c_tenancy.tenancyid  
            AND CG.id IN (SELECT  
                MAX(id) ID  
              FROM c_customer_names_grouped  
              GROUP BY i)  
    WHERE PDR_APPOINTMENTS.appointmentstatus != 'Cancelled'  
      AND --(
      (pdr_appointments.appointmentstartdate BETWEEN CONVERT(date, @startDate)AND  CONVERT(date, @endDate)   ) 
      AND ( 1 = Case 
					WHEN PDR_MSATType.MSATTypeName ='Appliance Defect' THEN
						CASE 
							WHEN defect.ApplianceDefectAppointmentJournalId IS NULL THEN
								0
							ELSE 
								1
						END
					ELSE
						1
				END 
)
      AND  
         CASE  
           WHEN PDR_MSAT.propertyid IS NOT NULL THEN ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '  
             + ISNULL(P__PROPERTY.ADDRESS1, '')  
             + ISNULL(', ' + P__PROPERTY.TOWNCITY, '')  
             + ISNULL(' ' + P__PROPERTY.POSTCODE, '')  
           WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
             + ISNULL(' ' + P_SCHEME.SCHEMECODE, '')  
           WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN ISNULL(P_BLOCK.BLOCKNAME, '') + ' '  
             + ISNULL(P_BLOCK.ADDRESS1, '')  
             + ISNULL(', ' + P_BLOCK.TOWNCITY, '')  
             + ISNULL(' ' + P_BLOCK.POSTCODE, '')  
         END LIKE '%' + @address + '%'  
      AND (CG.LIST LIKE '%' + @tenant + '%'  OR @tenant ='')     
     -- AND (@tradeId = -1 ) 
      --OR PDR_APPOINTMENTS.TRADEID = @tradeId)
	  --OR (PDR_APPOINTMENTS.TRADEID <> @tradeId)  
   --AND (@jsn = 'All'  OR  @jsn='JSN'OR @jsn='JSD') 
   AND PDR_MSATType.MSATTypeName IN (
 Select title from #TempAppointment where #TempAppointment.js = @jsn COLLATE DATABASE_DEFAULT)
   
  
    -- ==========================================================================================      
    -- Void Appointments  
    -- ==========================================================================================   
    UNION ALL  
  
    SELECT DISTINCT  
	Case When V_RequiredWorks.RequiredWorksId IS NULL Then 		
		ISNULL(( 'JSV' + Convert( NVarchar, PDR_JOURNAL.JournalID )),'N/A' )
		ELSE 
		ISNULL(( 'JSV' + Convert( NVarchar, V_RequiredWorks.RequiredWorksId  )),'N/A' )
		END
		 AS JobsheetNumber,
      CONVERT(varchar(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) AS AppointmentDate,  
      PDR_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
      PDR_APPOINTMENTS.APPOINTMENTSTARTTIME AS [Time],  
      'JSV Name' AS Name,  
      'Test Mobile' AS MOBILE,  
      ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
      + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
      + ISNULL(P__PROPERTY.TOWNCITY, '') + ', '  
      + ISNULL(P__PROPERTY.POSTCODE, '') AS CustAddress,  
      ISNULL(P__PROPERTY.POSTCODE, '') AS POSTCODE,  
     Case when MSATTypeName ='Void Works' Then
	 
		  CASE  
			WHEN PDR_STATUS.TITLE = 'Arranged'  THEN 'Appointment Arranged'  
			WHEN PDR_STATUS.TITLE = 'Completed' THEN 'Complete' 
			WHEN PDR_STATUS.TITLE = 'In Progress' THEN 'In Progress'  
			ELSE PDR_STATUS.TITLE  
		  END 
	  ELSE
		 CASE  
			WHEN PDR_APPOINTMENTS.APPOINTMENTSTATUS = 'NotStarted' OR PDR_APPOINTMENTS.APPOINTMENTSTATUS = 'Accepted' THEN 'Appointment Arranged'  
			WHEN PDR_APPOINTMENTS.APPOINTMENTSTATUS = 'Complete' THEN 'Complete' 
			WHEN PDR_APPOINTMENTS.APPOINTMENTSTATUS = 'InProgress' THEN 'In Progress'  
			ELSE PDR_APPOINTMENTS.APPOINTMENTSTATUS 
		  END 
	  END
	  
	  AS FaultStatus,   
      PDR_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
      PDR_APPOINTMENTS.APPOINTMENTENDTIME AS EndTime,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)  
      + ' '  
      + PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 103)) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',  
      CONVERT(datetime, CONVERT(varchar(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)  
      + ' '  
      + PDR_APPOINTMENTS.APPOINTMENTENDTIME, 103)) AS EndTimeInSec,  
      CONVERT(time, PDR_APPOINTMENTS.APPOINTMENTSTARTTIME) AS TimeToOrderAppointments,  
      C_TENANCY.TENANCYID AS TenancyId,  
      0 AS IsCalendarAppointment,  
      PDR_MSATType.MSATTypeName AS AppointmentType,  
      CONVERT(varchar(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103) AS AppointmentEndDate  
 ,ISNULL(PDR_APPOINTMENTS.APPOINTMENTNOTES,'') AS  AppointmentNotes  
    ,'Pdr' AS InspectionType   
   ,-1 as IsVoid   
  , 'Voids' as ddlAppointmentType        
    FROM PDR_APPOINTMENTS  
        INNER JOIN @operatives AS O ON PDR_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID  
        INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
		Left JOIN V_RequiredWorks ON PDR_JOURNAL.JOURNALID =  V_RequiredWorks.WorksJournalId  
        
        INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID  
        INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID  
            AND @schemeId IN (P__PROPERTY.SCHEMEID,-1)  
            
        INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId  
            AND MSATTypeName LIKE '%Void%'  
		INNER JOIN pdr_status ON pdr_status.STATUSID = Case when MSATTypeName ='Void Works' then V_RequiredWorks.StatusId
		else PDR_JOURNAL.STATUSID end AND PDR_STATUS.TITLE <> 'Cancelled'
        LEFT JOIN p_status ON p__property.[status] = p_status.statusid  
		 
        INNER JOIN PDR_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)     
  
        INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId = C_TENANCY.TENANCYID  
        LEFT JOIN c_customer_names_grouped CG ON CG.i = c_tenancy.tenancyid  
            AND CG.id IN (SELECT  
                MAX(id) ID  
              FROM c_customer_names_grouped  
              GROUP BY i)  
    WHERE PDR_STATUS.TITLE != 'Cancelled'  
      AND (PDR_APPOINTMENTS.APPOINTMENTSTARTDATE BETWEEN CONVERT(SMALLDATETIME, @startDate)AND CONVERT(SMALLDATETIME, @endDate))
      AND (ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '  
      + ISNULL(P__PROPERTY.ADDRESS1, '')  
      + ISNULL(', ' + P__PROPERTY.TOWNCITY, '')  
      + ISNULL(' ' + P__PROPERTY.POSTCODE, '')) LIKE '%' + @address + '%'  
      AND (CG.LIST LIKE '%' + @tenant + '%'  OR @tenant ='')
    AND (@jsn = 'All'  OR @jsn='JSV') 
    

    -- ==========================================================================================      
    -- Stock Appointments  
    -- ==========================================================================================    
    UNION ALL  
    SELECT DISTINCT 
	 '' as JobsheetNumber, 
      CONVERT(varchar(20), SA.AppointStartDateTime, 103) AS AppointmentDate,  
      AL.EMPLOYEEID AS OperativeID,  
      CONVERT(varchar(5), SA.AppointStartDateTime, 108) AS [Time],  
      'Test Name' AS Name,  
      'Test Mobile' AS MOBILE,  
      SA.AppointLocation AS CustAddress,  
      P.POSTCODE AS POSTCODE,  
      SA.AppointProgStatus AS FaultStatus,  
      SA.AppointId AS AppointmentID,  
      CONVERT(varchar(5), SA.AppointEndDateTime, 108) AS EndTime,  
      DATEDIFF(s, '1970-01-01', SA.AppointStartDateTime) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01', SA.AppointEndDateTime) AS EndTimeInSec,  
      CONVERT(time, SA.AppointStartDateTime) AS TimeToOrderAppointments,  
      T.TENANCYID AS TenancyId,  
      0 AS IsCalendarAppointment,  
      'Stock' AS AppointmentType,  
      CONVERT(varchar(20), SA.AppointEndDateTime, 103) AS AppointmentEndDate  
      ,ISNULL(SA.AppointNotes,'') AS  AppointmentNotes  
      ,'Stock' AS InspectionType   
   ,-1 as IsVoid        
  , 'Stock' as ddlAppointmentType     
    FROM PS_Appointment SA  
        INNER JOIN AC_LOGINS AS AL ON AL.LOGIN = SA.SurveyourUserName  
        INNER JOIN @Operatives AS O ON AL.EMPLOYEEID = O.EMPLOYEEID  
        INNER JOIN PS_Appointment2Survey AS SA2S ON SA.AppointId = SA2S.AppointId  
        INNER JOIN PS_Survey AS PS ON PS.SurveyId = SA2S.SurveyId  
        INNER JOIN P__PROPERTY AS P ON PS.PROPERTYID = P.PROPERTYID  AND @schemeId IN (P.SCHEMEID,-1)         
        INNER JOIN PDR_DEVELOPMENT ON  P.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)      
        LEFT JOIN C_TENANCY AS T ON PS.PROPERTYID = T.PROPERTYID  
            AND (T.ENDDATE IS NULL  
            OR T.ENDDATE >= @Today)  
        LEFT JOIN c_customer_names_grouped CG ON CG.i = T.tenancyid  
            AND CG.id IN (SELECT  
                MAX(id) ID  
              FROM c_customer_names_grouped  
              GROUP BY i)  
    WHERE SA.AppointProgStatus != 'Cancelled'  
    
      AND (SA.AppointStartDateTime BETWEEN CONVERT(date, @startDate, 103)AND  CONVERT(date, @endDate, 103)
      OR SA.AppointEndDateTime BETWEEN CONVERT(date, @startDate, 103)AND  CONVERT(date, @endDate, 103)
        
      )  
      AND ISNULL(P.HouseNumber, '')  
      + ISNULL(' ' + P.ADDRESS1, '')  
      + ISNULL(', ' + P.TOWNCITY, '')  
      + ISNULL(', ' + P.POSTCODE, '') LIKE '%' + @address + '%'  
      AND CG.LIST LIKE '%' + @tenant + '%'  
      AND (@jsn = 'All')  
  
  
    -- ==========================================================================================      
    -- APPLIANCE APPOINTMENTS  
    -- ==========================================================================================    
  
  
    UNION ALL  
    SELECT DISTINCT  
	'JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER )
	 as JobsheetnNumber,
      CONVERT(varchar(20), APPOINTMENTDATE, 103) AS AppointmentDate,  
      AS_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
      CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) AS [Time],  
      ISNULL(CG.LIST, 'N/A') AS Name,  
      '' AS MOBILE,  
      ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '  
      + ISNULL(P__PROPERTY.ADDRESS1, '') + ' '  
      + ISNULL(P__PROPERTY.ADDRESS2, '') + ' '  
      + ISNULL(P__PROPERTY.ADDRESS3, '')  
      + ISNULL(P__PROPERTY.POSTCODE, '') AS CustAddress,  
      P__PROPERTY.POSTCODE AS POSTCODE,
      CASE  
        WHEN AS_APPOINTMENTS.APPOINTMENTSTATUS = 'NotStarted' OR AS_APPOINTMENTS.APPOINTMENTSTATUS = 'Accepted' THEN 'Appointment Arranged'  
        WHEN AS_APPOINTMENTS.APPOINTMENTSTATUS = 'Finished' THEN 'Complete'  
        ELSE AS_APPOINTMENTS.APPOINTMENTSTATUS  
      END AS FaultStatus,   
     -- P_STATUS.[DESCRIPTION] AS FaultStatus,  
      AS_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
      CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTENDTIME, 108) AS EndTime,  
      DATEDIFF(s, '1970-01-01',APPOINTMENTDATE+' '+ AS_APPOINTMENTS.APPOINTMENTSTARTTIME) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',APPOINTMENTDATE +' '+ AS_APPOINTMENTS.APPOINTMENTENDTIME) AS EndTimeInSec,  
      CONVERT(time, AS_APPOINTMENTS.APPOINTMENTSTARTTIME) AS TimeToOrderAppointments,  
      C_TENANCY.TENANCYID AS TenancyId,  
      0 AS IsCalendarAppointment,  
      'GAS' AS AppointmentType,  
      CONVERT(varchar(20), APPOINTMENTDATE, 103) AS AppointmentEndDate  
 ,ISNULL( AS_APPOINTMENTS.NOTES,'' ) AS AppointmentNotes  
     ,'Appliance' AS InspectionType  
  ,ISNULL(AS_APPOINTMENTS.IsVoid,-1) AS IsVoid       
  , 'Gas' as ddlAppointmentType  
    FROM AS_APPOINTMENTS  
     INNER JOIN @Operatives AS O ON AS_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID 
        INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID  
        INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID  
        INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID   AND @schemeId IN (P__PROPERTY.SCHEMEID,-1)  
       
        INNER JOIN PDR_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
         AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)     
       -- INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID  
        LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID  
            AND (DBO.C_TENANCY.ENDDATE IS NULL  
            OR DBO.C_TENANCY.ENDDATE > GETDATE()  
            )  
        LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID  
            AND CG.ID IN (SELECT  
                MAX(ID) ID  
              FROM C_CUSTOMER_NAMES_GROUPED  
              GROUP BY I)  
    WHERE
		AS_APPOINTMENTS.APPOINTMENTSTATUS != 'Cancelled' AND
	  AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate)AND  CONVERT(date, @endDate)
       AND (@jsn = 'All' OR @jsn='JSG' )

	UNION ALL  
    SELECT DISTINCT  
	'JSG' + Convert( NVarchar, (AS_APPOINTMENTS.JSGNUMBER) )
	 as JobsheetnNumber,
      CONVERT(varchar(20), (APPOINTMENTDATE), 103) AS AppointmentDate,  
      (AS_APPOINTMENTS.ASSIGNEDTO) AS OperativeID,  
      CONVERT(varchar(5), (AS_APPOINTMENTS.APPOINTMENTSTARTTIME), 108) AS [Time],  
       'N/A' AS Name,  
      '' AS MOBILE,  
      ISNULL((P_SCHEME.SCHEMENAME), '')  AS CustAddress,  
      'N/A' AS POSTCODE,
      CASE  
        WHEN (AS_APPOINTMENTS.APPOINTMENTSTATUS) = 'NotStarted' OR (AS_APPOINTMENTS.APPOINTMENTSTATUS) = 'Accepted' THEN 'Appointment Arranged'  
        WHEN (AS_APPOINTMENTS.APPOINTMENTSTATUS) = 'Finished' THEN 'Complete'  
        ELSE (AS_APPOINTMENTS.APPOINTMENTSTATUS)  
      END AS FaultStatus,   
     -- P_STATUS.[DESCRIPTION] AS FaultStatus,  
      AS_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
      CONVERT(varchar(5), (AS_APPOINTMENTS.APPOINTMENTENDTIME), 108) AS EndTime,  
      DATEDIFF(s, '1970-01-01',(APPOINTMENTDATE)+' '+ (AS_APPOINTMENTS.APPOINTMENTSTARTTIME)) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',(APPOINTMENTDATE) +' '+ (AS_APPOINTMENTS.APPOINTMENTENDTIME)) AS EndTimeInSec,  
      CONVERT(time, (AS_APPOINTMENTS.APPOINTMENTSTARTTIME)) AS TimeToOrderAppointments,  
      -1 AS TenancyId,  
      0 AS IsCalendarAppointment,  
      'GASSB' AS AppointmentType,  
      CONVERT(varchar(20), (APPOINTMENTDATE), 103) AS AppointmentEndDate  
 ,ISNULL( (AS_APPOINTMENTS.NOTES),'' ) AS AppointmentNotes  
     ,'Appliance' AS InspectionType  
  ,ISNULL((AS_APPOINTMENTS.IsVoid),-1) AS IsVoid       
  , 'Gas' as ddlAppointmentType    
    FROM AS_APPOINTMENTS  
    INNER JOIN @Operatives AS O ON AS_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID 
    INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID  
    INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID
    INNER JOIN P_SCHEME ON AS_JOURNAL.SchemeID = P_SCHEME.SCHEMEID   AND @schemeId IN (P_SCHEME.SCHEMEID,-1)  
    INNER JOIN PDR_DEVELOPMENT ON  P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
        AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)     
    WHERE 
	AS_APPOINTMENTS.APPOINTMENTSTATUS != 'Cancelled' AND
	  AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate)AND  CONVERT(date, @endDate)
       AND (@jsn = 'All' OR @jsn='JSG' )

	UNION ALL  
SELECT DISTINCT  
	'JSG' + Convert( NVarchar, (AS_APPOINTMENTS.JSGNUMBER) )
	 as JobsheetnNumber,
      CONVERT(varchar(20), (APPOINTMENTDATE), 103) AS AppointmentDate,  
      (AS_APPOINTMENTS.ASSIGNEDTO) AS OperativeID,  
      CONVERT(varchar(5), (AS_APPOINTMENTS.APPOINTMENTSTARTTIME), 108) AS [Time],  
       'N/A' AS Name,  
      '' AS MOBILE,  
      ISNULL((P_BLOCK.BLOCKNAME), '') + ' '  
      + ISNULL((P_BLOCK.ADDRESS1), '') + ' '  
      + ISNULL((P_BLOCK.ADDRESS2), '') + ' '  
      + ISNULL((P_BLOCK.ADDRESS3), '')  
      + ISNULL((P_BLOCK.POSTCODE), '') AS CustAddress,    
      (P_BLOCK.POSTCODE) AS POSTCODE,
      CASE  
        WHEN (AS_APPOINTMENTS.APPOINTMENTSTATUS) = 'NotStarted' OR (AS_APPOINTMENTS.APPOINTMENTSTATUS) = 'Accepted' THEN 'Appointment Arranged'  
        WHEN (AS_APPOINTMENTS.APPOINTMENTSTATUS) = 'Finished' THEN 'Complete'  
        ELSE (AS_APPOINTMENTS.APPOINTMENTSTATUS)  
      END AS FaultStatus,   
     -- P_STATUS.[DESCRIPTION] AS FaultStatus,  
      AS_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
      CONVERT(varchar(5), (AS_APPOINTMENTS.APPOINTMENTENDTIME), 108) AS EndTime,  
      DATEDIFF(s, '1970-01-01',(APPOINTMENTDATE)+' '+ (AS_APPOINTMENTS.APPOINTMENTSTARTTIME)) AS StartTimeInSec,  
      DATEDIFF(s, '1970-01-01',(APPOINTMENTDATE) +' '+ (AS_APPOINTMENTS.APPOINTMENTENDTIME)) AS EndTimeInSec,  
      CONVERT(time, (AS_APPOINTMENTS.APPOINTMENTSTARTTIME)) AS TimeToOrderAppointments,  
      -1 AS TenancyId,  
      0 AS IsCalendarAppointment,  
      'GASSB' AS AppointmentType,  
      CONVERT(varchar(20), (APPOINTMENTDATE), 103) AS AppointmentEndDate  
 ,ISNULL( (AS_APPOINTMENTS.NOTES),'' ) AS AppointmentNotes  
     ,'Appliance' AS InspectionType  
  ,ISNULL((AS_APPOINTMENTS.IsVoid),-1) AS IsVoid       
  , 'Gas' as ddlAppointmentType  
    FROM AS_APPOINTMENTS  
    INNER JOIN @Operatives AS O ON AS_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID 
    INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID  
    INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID
    --INNER JOIN P_SCHEME ON AS_JOURNAL.SchemeID = P_SCHEME.SCHEMEID   AND @schemeId IN (P_SCHEME.SCHEMEID,-1) 
    INNER JOIN P_BLOCK ON AS_JOURNAL.BlockID = P_BLOCK.BLOCKID   AND @schemeId IN (P_BLOCK.SCHEMEID,-1) 
    INNER JOIN PDR_DEVELOPMENT ON  P_BLOCK.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
        AND @patchId IN (PDR_DEVELOPMENT.PATCHID, -1)     
    WHERE 
	AS_APPOINTMENTS.APPOINTMENTSTATUS != 'Cancelled' AND
	  AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate)AND  CONVERT(date, @endDate)
       AND (@jsn = 'All' OR @jsn='JSG' )
              
       ) 


  /* ===============================================================  
  * Populate all appointments  
  * =============================================================== */  
 SELECT  
	JobsheetNumber,
    AppointmentDate,  
    OperativeID,  
    [Time],  
    NAME,  
    MOBILE,  
    CustAddress,  
    POSTCODE,  
    FaultStatus,  
    AppointmentID,  
    EndTime,  
    StartTimeInSec,  
    EndTimeInSec,  
    TimeToOrderAppointments, TenancyId,  
    IsCalendarAppointment,  
    AppointmentType,
    AppointmentEndDate  
    ,AppointmentNotes  
    ,InspectionType  
 ,IsVoid 
 ,ddlAppointmentType 
  FROM CTE_Appointments  
  WHERE (ddlAppointmentType =  @appointmentType OR @appointmentType='All')
  
  ORDER BY APPOINTMENTDATE  
  , TimeToOrderAppointments ASC  

  
  /* ===============================================================  
  * Get operatives working hours core and out of office  
  * =============================================================== */  
  -- Get Core Working Hours    
  SELECT DISTINCT  
    E_WEEKDAYS.NAME AS WeekDayName,  
    E_WEEKDAYS.DayId AS DayId,  
    ECWH.EMPLOYEEID AS EmployeeId,  
    STARTTIME AS StartTime,  
    ENDTIME AS EndTime,  
    'CoreWorkingHours' AS HoursType  
  FROM E_CORE_WORKING_HOURS ECWH  
      INNER JOIN E_WEEKDAYS ON ECWH.DAYID = E_WEEKDAYS.DAYID  
      INNER JOIN @Operatives AS O ON ECWH.EMPLOYEEID = O.EMPLOYEEID  
  
  WHERE LEN(STARTTIME) > 0  
    AND LEN(ENDTIME) > 0  
  
  -- Get Out of office hours  
  UNION ALL  
  SELECT  
    E_WEEKDAYS.NAME AS WeekDayName,  
    E_WEEKDAYS.DayId AS DayId,  
    EOOH.EMPLOYEEID AS EmployeeId,  
    STARTTIME AS StartTime,  
    ENDTIME AS EndTime,  
    'OutOfOfficeHours' AS HoursType  
  FROM E_OUT_OF_HOURS EOOH  
      INNER JOIN E_WEEKDAYS ON DATEPART(WEEKDAY, EOOH.STARTDATE) = E_WEEKDAYS.DAYID  
      INNER JOIN @Operatives AS O ON EOOH.EMPLOYEEID = O.EMPLOYEEID  
  WHERE (EOOH.STARTDATE BETWEEN @startDate AND @endDate  
    OR EOOH.ENDDATE BETWEEN @startDate AND @endDate  
    OR (EOOH.STARTDATE <= @startDate  
    AND EOOH.ENDDATE >= @endDate))   
END
