SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,20 Oct 2014>
-- Description:	<Description,,Get Bankn Holidays details to skip these holidays from Intelligent Scheduling>
-- =============================================
Create PROCEDURE [dbo].[PLANNED_GetBankHolidays]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select Convert(CHAR(10), BHDATE, 101) as BHDATE from G_BANKHOLIDAYS
END
GO
