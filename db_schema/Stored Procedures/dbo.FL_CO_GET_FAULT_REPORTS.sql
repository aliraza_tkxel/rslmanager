SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE dbo.FL_CO_GET_FAULT_REPORTS
/* ===========================================================================
 '   NAME:           FL_CO_GET_FAULT_REPORTS
 '   DATE CREATED:   16TH Feb 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist faults related to contractor and specific to status
 '   IN:             orgId, faultstatus					 
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria	   
		@orgId		int = NULL,
		@faultStatus int= null,
		
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int=50 ,
		@offSet   int=0 ,
		
		-- column name on which sorting is performed
		@sortColumn varchar(50)='F.FaultLogID',
		@sortOrder varchar (5)='DESC'

	)
	
		
AS
DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)
	        
--========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
    SET @SearchCriteria = '' 
        
    IF @faultStatus <> 0
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'AND F.StatusID= '+ CONVERT (varchar, (@faultStatus))
 
	SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'AND F.ORGID= '+ CONVERT (varchar, (@orgId))                             	

    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
						CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, (@noOfRows)) + CHAR(10) + CHAR(9) +
						CHAR(10) + CHAR(9) + ' F.FaultLogID,F.CustomerId,F.FaultID,F.FaultBasketID,F.SubmitDate,' +
						CHAR(10) + CHAR(9) + 'FD.ElementID,F.ORGID, F.DueDate,FP.ResponseTime,FS.DESCRIPTION As Status,' +											
						CHAR(10) + CHAR(9) + 'FD.Description,FD.PriorityID,' +
						CHAR(10) + CHAR(9) + 'CONVERT(VARCHAR, FP.ResponseTime), CASE FP.Days WHEN 1 THEN  CONVERT(VARCHAR, FP.ResponseTime)+''  ''+ ''Day(s)''  WHEN 0 THEN CONVERT(VARCHAR, FP.ResponseTime) +''  ''+ ''Hour(s)'' END AS FResponseTime,' +
						CHAR(10) + CHAR(9) + 'CASE FD.Recharge WHEN 1 THEN CONVERT(VARCHAR,FD.Gross) WHEN 0 THEN ''To be Confirmed'' END AS FRecharge' 

	
	 --========================================================================================    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM FL_FAULT_LOG AS F '+
					  CHAR(10) + CHAR(10)+ 'INNER JOIN FL_FAULT_STATUS AS FS ON F.StatusID = FS.FaultStatusID ' +                      
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT AS FD ON F.FaultID = FD.FaultID' +                      
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PRIORITY AS FP ON FD.PriorityID = FP.PriorityID'                       
                      


	 --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FaultLogID' 
		BEGIN 
			SET @sortColumn = @sortColumn				
			SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
		END
	
	 --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE F.FaultLogID NOT IN' +
                        
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        + CHAR(10) + CHAR(9) +'F.FaultLogID ' +                          
                        CHAR(10) + CHAR(9) + @FromClause +                         
                        CHAR(10) + CHAR(9) + 'WHERE 1=1 '+ @SearchCriteria +                         
                        CHAR(10) + CHAR(9) + ') ' + 
                                                
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria                                                                                                                                        
                       
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause)
EXEC (@SelectClause + @FromClause + @WhereClause)







GO
