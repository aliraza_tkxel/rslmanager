SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO




CREATE           PROC [dbo].[C_HOUSE_MOVE_UPDATE] (
/* ===========================================================================
 '   NAME:           C_HOUSE_MOVE_UPDATE
 '   DATE CREATED:   30 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   Purpose:	     To add a response (new record) in C_HOUSE_MOVE_UPDATE table
 '		     also update C_JOURNAL table		   
 '   IN:	     @HISTORYID, @CURRENTITEMSTATUSID
 '		     @LASTACTIONUSER, @MOVINGBECAUSE, @LOCALAUTHORITYID, @DEVELOPMENTID
 '		     @NOOFBEDROOMS, @OCCUPANTSABOVE18, @OCCUPANTSBELOW18  
 '   OUT:            Nothing    
 '   RETURN:         JournalID or -1
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

	@HISTORYID INT , 
	@CURRENTITEMSTATUSID INT,
	
	/* input parameters used to insert in C_HOUSE_MOVE */		
	@LASTACTIONUSER INT,	
	@MOVINGBECAUSE VARCHAR(255),
	@LOCALAUTHORITYID INT,
	@DEVELOPMENTID INT,
	@NOOFBEDROOMS INT,
	@OCCUPANTSABOVE18 INT,
	@OCCUPANTSBELOW18 INT

)			

AS

BEGIN 

        DECLARE @JOURNALID INT
        SET NOCOUNT ON
	
	/* selecting journalid, used to update status in C_Journal table */
	SELECT 
		@JOURNALID = H.JOURNALID
	FROM
	 	C_HOUSE_MOVE H
	WHERE
		H.HOUSEMOVEHISTORYID = @HISTORYID
	
	BEGIN TRAN

	/* updating status in C_JOURNAL table */
	UPDATE 
		C_JOURNAL
	SET    
		CURRENTITEMSTATUSID = @CURRENTITEMSTATUSID				
	WHERE  
		JOURNALID = @JOURNALID


	-- if updation fails, goto HANDLE_ERROR block
	IF @@ERROR <> 0 GOTO HANDLE_ERROR

	/*adding response to C_HOUSE_MOVE table */	
	INSERT INTO C_HOUSE_MOVE 

		(JOURNALID,  ITEMSTATUSID,         LASTACTIONDATE,  LASTACTIONUSER,  MOVINGBECAUSE,  LOCALAUTHORITYID,  DEVELOPMENTID,  NOOFBEDROOMS,  OCCUPANTSABOVE18,  OCCUPANTSBELOW18) 
	VALUES 	(@JOURNALID, @CURRENTITEMSTATUSID, GETDATE(),       @LASTACTIONUSER, @MOVINGBECAUSE, @LOCALAUTHORITYID, @DEVELOPMENTID, @NOOFBEDROOMS, @OCCUPANTSABOVE18, @OCCUPANTSBELOW18)


	-- if insertion fails, goto HANDLE_ERROR block
	IF @@ERROR <> 0 GOTO HANDLE_ERROR

	COMMIT TRAN

	RETURN @JOURNALID


-- Control comes here if error occured
HANDLE_ERROR:

  ROLLBACK TRAN

  RETURN -1

END






GO
