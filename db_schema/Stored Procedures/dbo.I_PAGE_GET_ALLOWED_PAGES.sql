
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[I_PAGE_GET_ALLOWED_PAGES] @EMPID INT
AS 
    WITH    CTE_PAGE_TEAM ( PageId, TeamId, EmployeeId )
              AS ( SELECT DISTINCT
                            TJ.PageId ,
                            jrt.TeamId ,
                            e.EMPLOYEEID
                   FROM     DBO.I_PAGE_TEAMJOBROLE tj
                            INNER JOIN dbo.E_JOBROLETEAM jrt ON tj.TeamJobRoleId = jrt.JobRoleTeamId
                            INNER JOIN dbo.E__EMPLOYEE e ON e.JobRoleTeamId = jrt.JobRoleTeamId
                   WHERE    jrt.isDeleted = 0
                            AND jrt.IsActive = 1
                 )
        SELECT  FILENAME
        FROM    CTE_PAGE_TEAM PE
                INNER JOIN I_PAGE P ON P.PAGEID = PE.PAGEID
        WHERE   PE.EMPLOYEEID = @EMPID

GO
