USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[ASB_GetOpenCases]    Script Date: 09-Jan-17 1:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[AS_GetSelectedTypeAppointmentsArranged]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetSelectedTypeAppointmentsArranged] AS SET NOCOUNT ON;') 
GO

Alter procedure [dbo].[AS_GetSelectedTypeAppointmentsArranged]
AS

Begin

SELECT	[StatusId] as id , [Title] as title 
FROM	AS_Status 
WHERE	Title LIKE 'Appointment to be arranged' OR Title LIKE 'No Entry' OR Title LIKE 'Aborted' OR TITLE LIKE 'Cancelled'
ORDER BY [Title];

End