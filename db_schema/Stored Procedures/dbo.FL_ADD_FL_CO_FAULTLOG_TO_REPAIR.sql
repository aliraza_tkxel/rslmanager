
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[FL_ADD_FL_CO_FAULTLOG_TO_REPAIR](
/* ===========================================================================
 '   NAME:         [FL_ADD_FL_CO_FAULTLOG_TO_REPAIR]
 '   DATE CREATED:  9 Apr 2013
 '   CREATED BY:    Zeeshan Malik
 '   CREATED FOR:   Fault Locator Iphone application
 '   PURPOSE:       Add records in Fault_Log_To_Repair '   
 '   
 '   IN			@appointmentId INT
 '   IN			@FaultRepairIDList int, // comma seperated list of repair ids
 '   IN			@FaultLogID int,
 '   IN			RepairNotes VARCHAR(500), // Line added by Abdul Wahhab - 14/06/2013
 '   OUT:			  @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:         1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@FaultLogID int,
@FaultRepairIDList varchar(8000),
@UserID Int,
@RepairNotes VARCHAR(500), -- Line added by Abdul Wahhab - 14/06/2013
@RESULT  INT = 1  OUTPUT 
)
AS
DECLARE	@QTY INT,
		@RECHARGE INT,
		@NetCost FLOAT,
		@VatType INT,
		@rVAT FLOAT,
		@GrossCost FLOAT,
		@FaultDescription NVARCHAR(100),
		@RepairListID int,
		@Success int			
BEGIN    

	SET NOCOUNT ON
	BEGIN TRAN
	
	DECLARE c1 CURSOR READ_ONLY

	FOR

	SELECT COLUMN1 FROM dbo.SPLIT_STRING(@FaultRepairIDList,',') 

	OPEN c1

	/*
	Start Looping
	*/

	FETCH NEXT FROM c1
	INTO @RepairListID
					
	WHILE @@FETCH_STATUS = 0
	BEGIN
			
	Set @Success = 0
	--PRINT @FaultLogId
	
	INSERT INTO FL_CO_FAULTLOG_TO_REPAIR
	(
		FaultLogID,	
		FaultRepairListID,
		Notes -- Line added by Abdul Wahhab - 14/06/2013		
	)
    values 
    (
		@FaultLogID,
		@RepairListID,
		@RepairNotes -- Line added by Abdul Wahhab - 14/06/2013
	)
	
	
FETCH NEXT FROM c1
	INTO @RepairListID

END	

CLOSE C1
DEALLOCATE C1
	
	Set @Success = 1
	IF @Success = 0
	Begin
		Rollback Transaction
		SET @RESULT=-1
		Print 'Unable to update the record'
	End
	ELSE
	Begin					
		SET @RESULT=1
		COMMIT TRAN
	END	
END    -- END main BEGIN









GO
