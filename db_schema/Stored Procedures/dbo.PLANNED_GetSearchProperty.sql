USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetSearchProperty]    Script Date: 18-Oct-16 11:55:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PLANNED_GetSearchProperty]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetSearchProperty] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetSearchProperty] 

/* ===========================================================================
 ' NAME: PLANNED_GetSearchProperty
-- EXEC	 [dbo].[PLANNED_GetSearchProperty]
--		
-- Author:		<Ahmed Mehmood>
-- Create date: <12/11/2013>
-- Description:	< Returns the search results for property >
-- Web Page: MiscWorks.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@searchText varchar (MAX) = ''
	)
	
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),		                
	        @SearchCriteria varchar(8000),
	        @mainSelectQuery varchar(8000)
	        

    --========================================================================================
	--							PROPERTY SEARCH
    --========================================================================================
         
    SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + '( SearchProperty.Address LIKE ''%'+ LTRIM(@searchText) +'%'' '
	SET @searchCriteria = @searchCriteria + CHAR(10) +' OR SearchProperty.SCHEMENAME  LIKE ''%' + @searchText + '%'''
	SET @searchCriteria = @searchCriteria + CHAR(10) +' OR SearchProperty.BLOCKNAME  LIKE ''%' + @searchText + '%'') AND'
							
    -- Building Main Query for Property Search
                             
    SET @SelectClause = 'SELECT TOP(34)' +                      
    CHAR(10) + 'SearchProperty.PropertyId as id ,SearchProperty.Address as value, ''Property'' as Type'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '( SELECT B.BLOCKNAME as BLOCKNAME, S.SCHEMENAME as SCHEMENAME,	P__PROPERTY.PROPERTYID as PropertyId,ISNULL(P__PROPERTY.FLATNUMBER,'''')+'' ''+ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
									FROM	P__PROPERTY
									Left JOIN P_BLOCK B ON  P__PROPERTY.BLOCKID = B.BLOCKID
									LEFT JOIN P_SCHEME S On P__PROPERTY.SCHEMEID = S.SCHEMEID  ) SearchProperty'  	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	    

	SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + '( SearchScheme.Address LIKE ''%'+ LTRIM(@searchText) +'%'' ) AND'
							
    -- Building Main Query for Scheme Search
                             
    SET @SelectClause = 'SELECT TOP(33)' +                      
    CHAR(10) + 'SearchScheme.SCHEMEID as id ,SearchScheme.Address as value, ''Scheme'' as Type'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '( SELECT S.SCHEMEID AS SCHEMEID, ISNULL(S.SCHEMENAME,'''') Address  
									FROM P_SCHEME S  ) SearchScheme'  	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	 

	SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + '( SearchBlock.Address LIKE ''%'+ LTRIM(@searchText) +'%'' ) AND'
							
    -- Building Main Query for Block Search
                             
    SET @SelectClause = 'SELECT TOP(33)' +                      
    CHAR(10) + 'SearchBlock.BLOCKID as id ,SearchBlock.Address as value, ''Block'' as Type'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '( SELECT B.BLOCKID as BLOCKID, ISNULL(b.BLOCKNAME,'''') +'' ''+ ISNULL(b.ADDRESS1,'''') +'' ''+ ISNULL(b.ADDRESS2,'''') +'' ''+ ISNULL(b.ADDRESS3,'''') as Address
									FROM	P_BLOCK b  ) SearchBlock'  	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	 
