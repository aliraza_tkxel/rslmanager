SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[AM_SP_GetReviewListCount_old]
			@caseOfficerId	int,
			@statusId	int,
			@overdue	bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if(@overdue = 0)
		BEGIN
		select count(*) as RecordCount from (
		SELECT COUNT(*) as RecordCount
			FROM         AM_Case INNER JOIN
								  AM_Action ON AM_Case.ActionId = AM_Action.ActionId INNER JOIN
								  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
								  AM_Resource ON AM_Case.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								  AM_Customer_Rent_Parameters ON AM_Case.TenancyId=dbo.AM_Customer_Rent_Parameters.TenancyId INNER JOIN
								  --C_CUSTOMERTENANCY ON AM_Case.TenancyId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								  --C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

			WHERE AM_Case.IsActive = 1 
				  and (AM_Case.StatusId = case when @statusId=-1 then AM_Case.StatusId else @statusId end)	
				  and (AM_Case.CaseOfficer = case when @caseOfficerId = -1 then AM_Case.CaseOfficer else @caseOfficerId end)
				  --and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_Case.ActionRecordeddate ) = 'false'
				  and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', dbo.AM_Case.ActionReviewDate ) = 'false' 
		group by AM_Case.TenancyId) as temp
		END
		else
		BEGIN
			select count(*) as RecordCount from (			
			SELECT COUNT(*) as RecordCount
			FROM         AM_CaseHistory INNER JOIN
								  AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
								  AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
								  AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								  AM_Customer_Rent_Parameters ON dbo.AM_CaseHistory.TennantId=dbo.AM_Customer_Rent_Parameters.TenancyId INNER JOIN
								  --C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								  --C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

			WHERE AM_CaseHistory.IsActive = 1 
				  and (AM_CaseHistory.StatusId = case when @statusId=-1 then AM_CaseHistory.StatusId else @statusId end) 
				  and (AM_CaseHistory.CaseOfficer = case when @caseOfficerId = -1 then AM_CaseHistory.CaseOfficer else @caseOfficerId end)
				  --and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = 'true' 
				  and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_CaseHistory.ActionReviewDate ) = 'true' 
			      --and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Second_Last_Case_History_Id(AM_CaseHistory.CaseId)
			      and AM_CaseHistory.CaseHistoryId = (SELECT TOP 1 A.CaseHistoryId FROM dbo.AM_CaseHistory A WHERE A.CaseId=dbo.AM_CaseHistory.CaseId and A.IsActionIgnored = 'false')
			group by AM_CaseHistory.TennantId ) as temp
		END
END
--exec [AM_SP_GetReviewListCount] -1,-1,0






GO
