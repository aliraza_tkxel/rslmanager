USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[HB_ChangeHBSign]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[HB_ChangeHBSign] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[HB_ChangeHBSign] 
	-- Add the parameters for the stored procedure here
	@HBId INT	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRANSACTION
BEGIN TRY

if(@HBId>0)
BEGIN
UPDATE F_HBACTUALSCHEDULE 
SET HB=-1*(F_HBACTUALSCHEDULE.HB) 
WHERE F_HBACTUALSCHEDULE.HBID= @HBId 
	AND F_HBACTUALSCHEDULE.SENT IS NULL 
	AND F_HBACTUALSCHEDULE.VALIDATED IS NULL 
	AND F_HBACTUALSCHEDULE.JOURNALID IS NULL
END

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;    
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
	END

END
