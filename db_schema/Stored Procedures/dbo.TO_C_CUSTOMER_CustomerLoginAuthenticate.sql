 
alter PROCEDURE [dbo].[TO_C_CUSTOMER_CustomerLoginAuthenticate]
	
	/*	===============================================================
	'   NAME:           TO_C_Customer_Login_Authenticate_Customer
	'   DATE CREATED:   15 MAY 2008
	'   CREATED BY:     Munawar Nadeem
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To verify Customer that He/She is already registered  
	'   SELECT:         CustomerID
	'   IN:             @userName, @Password, @Tenancyid
	'   OUT: 	        Nothing    
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

    @userName varchar (50) = null,
    @password varchar (50) ,
	@tenancyid int = null

AS

	 SET NOCOUNT ON 
	 

IF @tenancyid IS NOT NULL
 BEGIN
		SELECT 
			   Customer.CUSTOMERID

		 FROM  C__CUSTOMER Customer 
				INNER JOIN C_ADDRESS CustomerAddress  ON Customer.CUSTOMERID = CustomerAddress.CUSTOMERID 
				INNER JOIN C_CUSTOMERTENANCY  ct ON ct.CUSTOMERID = Customer.CUSTOMERID
				INNER JOIN  TO_LOGIN Login ON Customer.CUSTOMERID = Login.CustomerID 
		 WHERE (ct.TENANCYID = @tenancyid)  AND (Login.Password = @password)
			   AND (CustomerAddress.ISDEFAULT = 1) 
			   AND (Login.Active = 1)
 END
ELSE
 BEGIN
		 SELECT 
	       Customer.CUSTOMERID

		 FROM
			   C__CUSTOMER Customer INNER JOIN C_ADDRESS CustomerAddress
			   ON Customer.CUSTOMERID = CustomerAddress.CUSTOMERID 

			   INNER JOIN  TO_LOGIN Login 
			   ON Customer.CUSTOMERID = Login.CustomerID

		 WHERE

			   (CustomerAddress.Email = @userName)  AND (Login.Password = @password)
			   AND (CustomerAddress.ISDEFAULT = 1) 
			   AND (Login.Active = 1)

 END
	
 






