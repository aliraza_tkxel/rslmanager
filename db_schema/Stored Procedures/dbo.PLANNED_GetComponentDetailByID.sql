Use [RSLBHALive]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PLANNED_GetComponentDetailByID]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[PLANNED_GetComponentDetailByID] AS SET NOCOUNT ON;')
GO

-- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,10/9/2013>
-- Description:	<Description,Get Component Detail by componentID>
-- EXEC PLANNED_GetComponentDetailByID 20
-- Modified by: Saud Ahmed
-- Description: add new column "COMPTRADEID"
-- Modified Date: 03/04/2017
-- =============================================
Alter PROCEDURE [dbo].[PLANNED_GetComponentDetailByID] (@ComponentID int)

	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	COMPONENTID,COMPONENTNAME,ISACCOUNTING,
			CYCLE,FREQUENCY,MATERIALCOST,LABOURCOST			
			,ISNULL(ANNUALBUDGETCOST,0) AS ANNUALBUDGETCOST
	FROM	PLANNED_COMPONENT  
	WHERE	COMPONENTID = @componentID
	
	SELECT	PCT.TRADEID, DURATION,G_TRADE.[Description] TRADE , PCT.SORDER, PCT.COMPTRADEID
	FROM	PLANNED_COMPONENT_TRADE PCT
			INNER JOIN G_TRADE  ON G_TRADE.TradeId = PCT.TRADEID 
	WHERE	PCT.COMPONENTID =@componentID
	ORDER BY PCT.SORDER
 
END

GO
