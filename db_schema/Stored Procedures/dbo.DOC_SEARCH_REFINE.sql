SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[DOC_SEARCH_REFINE]
(	
	@EMPID int,
	@TEAMCODE VARCHAR(100),
	@FOLDERNAME NVARCHAR(100) = NULL,
	@DOCTITLE NVARCHAR(100) = NULL,
	@KEYWORDS NVARCHAR(100) = NULL,	
	@CREATIONDATEFROM SMALLDATETIME = NULL,	
	@CREATIONDATETO SMALLDATETIME = NULL,
	@EXPIRYDATEFROM SMALLDATETIME = NULL,	
	@EXPIRYDATETO SMALLDATETIME = NULL
)
AS
DECLARE @SQL NVARCHAR(4000), @PARAMLIST  NVARCHAR(4000)

                                
SELECT @SQL = 'SELECT D.*, DBO.PATH_BY_DOCID(D.DOCUMENTID) AS FULLPATH

			   FROM DOC_DOCUMENT AS D
	   
			   INNER JOIN DOC_FOLDER AS F ON F.FOLDERID = D.FOLDERID

			    WHERE
				(
					(D.DOCFOR LIKE ''%' + @TEAMCODE + '%'') OR
					( (D.CREATEDBY = '+ CONVERT(VARCHAR,@EMPID) + ') AND (D.DOCUMENTID = (SELECT MAX(DOCUMENTID) AS Expr1
											  FROM   DOC_DOCUMENT AS DOC_DOCUMENT_1 )
					 )
			    ))'
 

IF @CREATIONDATEFROM IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND D.DATECREATED >= @CREATIONDATEFROM '

IF @CREATIONDATETO IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND D.DATECREATED < DATEADD(day, 1, @CREATIONDATETO) '

IF @EXPIRYDATEFROM IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND D.DATEEXPIRES >= @EXPIRYDATEFROM '  

IF @EXPIRYDATETO IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND D.DATEEXPIRES < DATEADD(day, 1, @EXPIRYDATETO) '  

    
IF @FOLDERNAME IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND DBO.PATHONLY_BY_DOCID(D.DOCUMENTID) LIKE ''%' + @FOLDERNAME + '%'''

IF @DOCTITLE IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND D.DOCNAME LIKE ''%' + @DOCTITLE + '%''' 

IF @KEYWORDS IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND D.KEYWORDS LIKE ''%' + @KEYWORDS + '%'''


SELECT @SQL = @SQL + ' ORDER BY D.DOCUMENTID'

SELECT @PARAMLIST =	'@FOLDERNAME NVARCHAR(100) ,
					@DOCTITLE NVARCHAR(100),
					@KEYWORDS NVARCHAR(100),	
					@CREATIONDATEFROM SMALLDATETIME,	
					@CREATIONDATETO SMALLDATETIME,
					@EXPIRYDATEFROM SMALLDATETIME,	
					@EXPIRYDATETO SMALLDATETIME'


SET NOCOUNT OFF 

PRINT @SQL

EXEC SP_EXECUTESQL @SQL, @PARAMLIST,
@FOLDERNAME,
@DOCTITLE,
@KEYWORDS,	
@CREATIONDATEFROM,	
@CREATIONDATETO,
@EXPIRYDATEFROM,
@EXPIRYDATETO

GO
