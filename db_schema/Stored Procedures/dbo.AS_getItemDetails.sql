
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_getItemDetails @itemId =1,@parameterId =1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/13/2012>
-- Description:	<Description,,get the details of the Tree leaf Node>
-- WebPage: PropertyRecord.aspx=>Attributes tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_getItemDetails](
@parameterId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT *
  FROM PA_PARAMETER_VALUE
  WHERE PA_PARAMETER_VALUE.ParameterID =@parameterId  and PA_PARAMETER_VALUE.IsActive = 1
END
GO
