SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- AUTHOR:		<ADNAN MIRZA>
-- CREATE DATE: <10 NOV 2010>
-- DESCRIPTION:	<FOR INFORMATION IFRAME AND POP UP>
-- =============================================
CREATE PROCEDURE [dbo].[RISK_CAT_SUBCAT]
	
	@CUSTOMERID AS INT
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @RISKHISTORYID INTEGER	
	
 SELECT @RISKHISTORYID = RISKHISTORYID
 FROM C_JOURNAL J 
 INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID
 WHERE CUSTOMERID = @CUSTOMERID AND ITEMNATUREID=63 
 AND CR.RISKHISTORYID = (SELECT MAX(RISKHISTORYID) FROM C_RISK IN_CR WHERE IN_CR.JOURNALID=J.JOURNALID)                 
 AND CR.ITEMSTATUSID <> 14  	


 SELECT cr.RiskHistoryId, CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC 
 FROM C_CUSTOMER_RISK CR  
 INNER JOIN C_RISK_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID = CR.SUBCATEGORYID
 INNER JOIN C_RISK_CATEGORY CAT ON CAT.CATEGORYID = SUBCAT.CATEGORYID 
 WHERE CR.RISKHISTORYID= @RISKHISTORYID

END

GO
