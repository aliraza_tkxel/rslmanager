SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[F_PAYTHRU_EMPLOYEE_SelectByPk] 
    @EMPLOYEEID INT
AS 
	SET NOCOUNT ON 

	SELECT [EMPLOYEEID], [FIRSTNAME], [LASTNAME]
	FROM   [dbo].[E__EMPLOYEE] 
	WHERE  ([EMPLOYEEID] = @EMPLOYEEID) 


GO
