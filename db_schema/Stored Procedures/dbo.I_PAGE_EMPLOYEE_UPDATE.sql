SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_PAGE_EMPLOYEE_UPDATE]
(
	@PageID int,
	@EmployeeID int,
	@Original_PageEmployeeID int,
	@PageEmployeeID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_PAGE_EMPLOYEE] SET [PageID] = @PageID, [EmployeeID] = @EmployeeID WHERE (([PageEmployeeID] = @Original_PageEmployeeID));
	
SELECT PageEmployeeID, PageID, EmployeeID FROM I_PAGE_EMPLOYEE WHERE (PageEmployeeID = @PageEmployeeID)
GO
