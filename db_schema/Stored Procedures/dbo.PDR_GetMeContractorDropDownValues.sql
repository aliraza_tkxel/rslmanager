USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_PropertyActivities]    Script Date: 05-Sep-17 3:23:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PDR_GetMeContractorDropDownValues') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetMeContractorDropDownValues AS SET NOCOUNT ON;') 
GO

-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 27/01/2015
-- Description:	To get contractors having at lease one planned contract, as drop down values for assgin work to contractor.
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetMeContractorDropDownValues] 
	-- Add the parameters for the stored procedure here	 
	@msat varchar(100)
	,@schemeId int
	,@blockId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID 
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID 	
	LEFT JOIN P_SCHEME on SS.SchemeId = P_SCHEME.SCHEMEID 
	LEFT JOIN P_BLOCK B on SS.BlockId = B.BLOCKID OR B.SchemeId = SS.SchemeId
	WHERE AoW.DESCRIPTION = @msat 
			AND (SS.BLOCKID = @blockId OR SS.SCHEMEID = @schemeId)OR (B.BLOCKID = @blockId)
			AND (S.CREATIONDATE <= GETDATE()
				AND S.RENEWALDATE > GETDATE())
	
END
