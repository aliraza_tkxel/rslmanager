USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FLS_GetAvailableOperativesForVoidInspection]    Script Date: 13-Feb-18 3:15:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO

IF OBJECT_ID('dbo.FLS_GetAvailableOperativesForVoidInspection') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_GetAvailableOperativesForVoidInspection AS SET NOCOUNT ON;') 
GO

 ALTER PROCEDURE [dbo].[FLS_GetAvailableOperativesForVoidInspection]
	-- Add the parameters for the stored procedure here
	@msattype as varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if 1 = 2
	Begin
		select 0 as EmployeeId , '' as FullName 		
		WHERE
		1 = 2 
	End

	DECLARE @AvailableOperatives TABLE (
		EmployeeId int
		,FullName nvarchar(100)								
	)

    -- Insert statements for procedure here
	INSERT INTO @AvailableOperatives(EmployeeId,FullName)

	SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName + ' '+ E__EMPLOYEE.LastName as FullName	

	FROM  E__EMPLOYEE 
	INNER JOIN E_TRADE ON E_TRADE.EmpId = E__EMPLOYEE.EmployeeId
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID 
	WHERE P_INSPECTIONTYPE.Description  = @msattype 	
	AND E_JOBDETAILS.Active=1

	AND E__EMPLOYEE.EmployeeId NOT IN (
	SELECT EMPLOYEEID 
	FROM E_JOURNAL INNER JOIN E_ABSENCE ON 
		E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID AND E_ABSENCE.ABSENCEHISTORYID IN ( 
		SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID ) 
	WHERE ITEMNATUREID = 1
		AND E_ABSENCE.RETURNDATE IS NULL AND E_ABSENCE.ITEMSTATUSID = 1
	UNION ALL	
			SELECT DISTINCT
				E_JOURNAL.EMPLOYEEID
			FROM E_JOURNAL
			INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
				AND E_ABSENCE.ABSENCEHISTORYID IN (
													SELECT 
														MAX(ABSENCEHISTORYID) 
														FROM E_ABSENCE 
														GROUP BY JOURNALID
													)
			INNER JOIN E_STATUS ON  E_ABSENCE.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
			INNER JOIN E_NATURE ON  E_JOURNAL.ITEMNATUREID = E_NATURE.ITEMNATUREID
			INNER JOIN E_JOBDETAILS ej ON E_JOURNAL.EMPLOYEEID = ej.EMPLOYEEID
			LEFT JOIN E_HOLIDAYRULE hl ON ej.HOLIDAYRULE = hl.EID
			WHERE E_ABSENCE.startdate <= getdate() and 
					E_ABSENCE.RETURNDATE >= getdate() and
				E_NATURE.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,48,43,52,53,54)
				AND E_ABSENCE.ITEMSTATUSID = 3 and
				ej.ISBRS=1)

	SELECT Distinct EmployeeId,FullName FROM  @AvailableOperatives
END
