
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--  EXEC FL_SaveFaultTemporaryReArrange
--  @faultId = 531,
--  @customerId = 5,
--	@propertyId ="A010000018",
--	@quantity =1,
--	@problemDays =1,
--	@recuringProblem =1,
--	@recharge =1,
--	@notes ="This is demo values",
--	@tempFaultId =0,
--  @isRecall = 0,
--  @originalFaultLogId = 0,
--	@faultTradeId = 0
-- Author:		Behroz Sikander
-- Create date: 18/03/2013
-- Description:	This stored procedure inserts the data in FL_TEMP_FAULT
-- WebPage: ReArrangingCurrentFault.aspx
-- =============================================
	CREATE PROCEDURE [dbo].[FL_SaveFaultTemporaryReArrange] 
	-- Add the parameters for the stored procedure here
	(
	@faultId INT,
	@customerId INT,
	@propertyId varchar(100),
	@quantity INT,
	@problemDays INT,
	@recuringProblem INT,	
	@recharge INT,
	@notes nvarchar(4000),
	@tempFaultId INT OUTPUT,
	@isRecall BIT,
	@originalFaultLogId INT,
	@faultTradeId INT,
	@isRearrange BIT
  )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select * from FL_TEMP_FAULT
    -- Insert statements for procedure here
	INSERT INTO FL_TEMP_FAULT(FaultID,CustomerId,Quantity,ProblemDays,RecuringProblem,Recharge,Notes,ItemStatusID,PROPERTYID,ISRECALL,FaultTradeId,OriginalFaultLogId,IsRearrange)		
		VALUES(@faultId,@customerId,@quantity,@problemDays,@recuringProblem,@recharge,@notes,1,@propertyId,@isRecall,@faultTradeId,@originalFaultLogId,@isRearrange)
		
	SET @tempFaultId = @@IDENTITY
END
GO
