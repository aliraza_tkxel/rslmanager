SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[sp_InsertTaskDetails]
@strTitle nvarchar (200),
@strAssignTo int,
@strDevSystem int,
@strType int,
@strPriority int,
@strArea nvarchar (1000),
@strContent nvarchar (4000),
@statusID int,
@completionDate smalldatetime,
@UserID int,
@OrgID int,
@CreatedBy int,
@IsInternal int

AS

SET NOCOUNT ON; INSERT INTO H_Request_JOURNAL (REQUEST_TITLE, ASSIGNED_TO, SYSTEM_ID, REQUEST_TYPE_ID, PRIORITY_ID, AREA, CONTENT, STATUS_ID, COMPLETION_DATE, USER_ID, ORG_ID, CREATED_BY, IS_INTERNAL) 
values (@strTitle,@strAssignTo,@strDevSystem,@strType,@strPriority,@strArea,@strContent,@statusID,@completionDate,@UserID,@OrgID, @CreatedBy, @IsInternal) 
SELECT @@IDENTITY AS THENEWJOBID; 
SET NOCOUNT OFF


GO
