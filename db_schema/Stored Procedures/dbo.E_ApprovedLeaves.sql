USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_ApprovedLeaves]    Script Date: 04/03/2019 17:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_ApprovedLeaves') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_ApprovedLeaves AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE  [dbo].[E_ApprovedLeaves]
	-- Add the parameters for the stored procedure here
	@empId INT	
AS
BEGIN

DECLARE @BHSTART SMALLDATETIME			
DECLARE @BHEND SMALLDATETIME	

SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
FROM	EMPLOYEE_ANNUAL_START_END_DATE_ByYear(@empId, getDate())
	
	
SELECT e.STARTDATE,e.RETURNDATE,s.DESCRIPTION as Status, n.DESCRIPTION as Nature, cast(duration as decimal (16, 2)) as DAYS_ABS,cast(duration_hrs as decimal(16,2)) as HRS_ABS, 
e.HOLTYPE as HolType, e.ABSENCEHISTORYID, e.LASTACTIONDATE as LastActionDate,e.DURATION_TYPE AS DurationType
FROM E_JOURNAL j
INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
INNER JOIN E_ABSENCE e on j.JOURNALID = e.JOURNALID
AND e.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
						   WHERE JOURNALID = j.JOURNALID) 
WHERE j.EMPLOYEEID = @empId AND s.DESCRIPTION = 'Approved' and  
j.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,43,52,53,54)
and e.STARTDATE >= @BHSTART and e.STARTDATE <= @BHEND 
ORDER by e.STARTDATE DESC

	
END
