SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[I_STATIONERY_ORDERITEM_REPORT_FILTERED]
(	
	@DATEREQUESTEDFROM DATETIME = NULL,
	@DATEREQUESTEDTO DATETIME = NULL,
	@ITEMSTATUS int = NULL,
	@TEAM int = NULL,
	@EMPLOYEE int = NULL,
	@LESSTHAN smallmoney = NULL,
	@GREATERTHAN smallmoney = NULL
)
AS
DECLARE @SQL NVARCHAR(4000), @PARAMLIST  NVARCHAR(4000)

SET NOCOUNT ON

SELECT @SQL = 'SELECT     SO.RequestedDate, EMP.FULLNAME AS RequestedFor, EMP.TEAMNAME AS RequestedForTeam, SO.Description AS OrderTitle, 
            ISNULL(GO.DESCRIPTION,SPACE(1)) AS OfficeLocation, SI.ItemNo AS CatNo, SI.ItemDesc, SI.Quantity, 
			SI.Cost, (SI.Quantity * SI.Cost) AS Total, ST.OrderStatus

			FROM         I_STATIONERY_ORDER_ITEM AS SI 

			INNER JOIN I_STATIONERY_ORDER AS SO ON SI.OrderId = SO.OrderId
			INNER JOIN I_STATIONERY_ORDER_ITEM_STATUS AS ST ON ST.OrderStatusId = SI.StatusId
			INNER JOIN VW_EMPLOYEES_INTRANET AS EMP ON  SO.RequestedForEmpId = EMP.EMPLOYEEID
			LEFT OUTER JOIN G_OFFICE AS GO ON GO.OFFICEID = EMP.OFFICELOCATION
			WHERE ( 1=1 ) '


IF @DATEREQUESTEDFROM IS NOT NULL
   SELECT @SQL = @SQL + ' AND SO.REQUESTEDDATE >= @DATEREQUESTEDFROM '

IF @DATEREQUESTEDTO IS NOT NULL
   SELECT @SQL = @SQL + ' AND SO.REQUESTEDDATE < DATEADD(day, 1, @DATEREQUESTEDTO) '	 

IF @ITEMSTATUS IS NOT NULL
   SELECT @SQL = @SQL + ' AND SI.StatusId = @ITEMSTATUS '

IF @TEAM IS NOT NULL
   SELECT @SQL = @SQL + ' AND EMP.TEAMID = @TEAM '

IF @EMPLOYEE IS NOT NULL
   SELECT @SQL = @SQL + ' AND EMP.EMPLOYEEID = @EMPLOYEE '

IF @LESSTHAN IS NOT NULL
   SELECT @SQL = @SQL + ' AND (SI.Quantity * SI.Cost < @LESSTHAN )'

IF @GREATERTHAN IS NOT NULL
   SELECT @SQL = @SQL + ' AND (SI.Quantity * SI.Cost > @GREATERTHAN)'

SELECT @SQL = @SQL + ' ORDER BY SO.RequestedDate DESC'

SELECT @PARAMLIST =		'@DATEREQUESTEDFROM DATETIME,
						 @DATEREQUESTEDTO DATETIME,
						 @ITEMSTATUS INT,
						 @TEAM INT,
						 @EMPLOYEE INT,
						 @LESSTHAN SMALLMONEY,
						 @GREATERTHAN SMALLMONEY'
SET NOCOUNT OFF

PRINT @SQL

EXEC SP_EXECUTESQL @SQL, @PARAMLIST,
@DATEREQUESTEDFROM,
@DATEREQUESTEDTO,
@ITEMSTATUS,
@TEAM,
@EMPLOYEE,
@LESSTHAN,
@GREATERTHAN


GO
