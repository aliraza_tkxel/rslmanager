SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

ALTER PROCEDURE dbo.FL_CO_SEARCHRESULT
	(			
	    @locationId	int	=  NULL,
		@areaId	    int	=  NULL,
		@elementId	int = NULL,
		@priorityId	int = NULL,
		@status		bit = NULL,  
		@orgId		int = NULL,
		@teamId		int  = NULL,
		@patchId	int = NULL,
		@propertyId int = NULL,
		@schemeId int = NULL,
		@stageId int = NULL,
		
		
		
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FL_FAULT_LOG.JobSheetNumber ',
		@sortOrder varchar (5) = 'DESC'
	)
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @status IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT.FaultActive = '+ LTRIM(STR(@status)) + ' AND'  
    
   	IF @patchId IS NOT NULL
	   SET @SearchCriteria =@SearchCriteria + CHAR(10) + CHAR(9) + 
							'E_PATCH.patchId = ' + LTRIM(STR(@patchId)) + ' AND' 
	
	IF @propertyId IS NOT NULL
	  SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'P__PROPERTY.propertyId = ' + LTRIM(STR(@propertyId)) + ' AND'
    
    IF @stageId IS NOT NULL 
      SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'FL_CO_APPOINTMENT_STAGE.AppointmentStageId = ' + LTRIM(STR(@stageId)) + ' AND'
	
	IF @schemeId IS NOT NULL
	  SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'P_SCHEME.SCHEMEID = ' + LTRIM(STR(@schemeId)) + ' AND'                          
    
          
    -- Begin building SELECT clause
    
    
      SET @SelectClause = 'SELECT' +  
                          
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT.JobSheetNumber as [Job Sheet Ref:], FL_FAULT_LOG.Submitdate as Logged,' +
                        CHAR(10) + CHAR(9) + 'C__CUSTOMER.FIRSTNAME as [Name], P__PROPERTY.Address1 as Address, FL_FAULT.[Description],' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.CompletedDate as Completed, E__EMPLOYEE.FirstName as Operative, ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.Gross ' 
                        
                                     
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_LOCATION INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_AREA ON FL_LOCATION.locationId = FL_AREA.locationId INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_AREA.areaId = FL_ELEMENT.areaId INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT ON FL_ELEMENT.ELEMENTID = FL_FAULT.ELEMENTID INNER JOIN' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG ON FL_FAULT.FAULTID = FL_FAULT_LOG.FAULTID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CustomerId INNER JOIN' +
                    CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN'+ 
		      CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID LEFT JOIN'+
                      CHAR(10) + CHAR(9) + 'P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID INNER JOIN'+   
						CHAR(10) + CHAR(9) + 'PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID INNER JOIN'+ 
                      CHAR(10) + CHAR(9) + 'E_PATCH ON PDR_DEVELOPMENT.PatchId = E_PATCH.PatchId INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT.JobSheetNumber INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeId = E__EMPLOYEE.EmployeeId INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.Priorityid = FL_FAULT_PRIORITY.PriorityId'
                      
                
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.JobSheetNumber'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FL_FAULT_LOG.JobSheetNumber '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FL_FAULT_LOG.JobSheetNumber NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FL_FAULT_LOG.JobSheetNumber ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'AND'+ @SearchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
 PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)
 
 --PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)






GO
