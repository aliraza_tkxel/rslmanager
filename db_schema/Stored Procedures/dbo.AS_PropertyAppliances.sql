USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_PropertyAppliances]    Script Date: 01/27/2016 14:34:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
      
version				Author				Creation Date		 Description							Web Page
-------				-------				-------------		 -----------							--------
v1.0				Noor Muhammad		31/10/2012			This stored procedure returns			PropertyRecord.aspx
															the all the appliances against 
															the property
																	
v1.1				Raja Aneeq			25/1/2016			Set SP according to new
															pagination and	Add a						//
															new field Removed Date 
																				

EXEC	[dbo].[AS_PropertyAppliances]
		@propertyId = N'A19045000A',
		@itemId = 73,
		
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'MODEL',
		@sortOrder = N'DESC'


*/

IF OBJECT_ID('dbo.AS_PropertyAppliances') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_PropertyAppliances AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_PropertyAppliances]
	-- Add the parameters for the stored procedure here
		  @propertyId varchar(50)=null,  
		  @itemId int,          
		  @schemeId int=null, 
		  @blockId int=null,          
		  @pageSize int = 30,        
		  @pageNumber int = 1,      
		  @sortColumn varchar(50) = '',      
		  @sortOrder varchar (5) = '',      
		  @totalCount int=0 output      
        
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(3000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(8000),
        
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
	
		--=====================Where Clause===============================
							
		SET @whereClause  = ' WHERE ItemId= '+convert(nvarchar,@itemId)
		IF (@propertyId !=NULL OR @propertyId !='')
		SET @whereClause  = @whereClause + 'AND GS_PROPERTY_APPLIANCE.ISACTIVE = 1 AND (GS_PROPERTY_APPLIANCE.PropertyID = '''+@propertyId+''' OR '''+@propertyId+''' IS NULL)'
		IF (@schemeId !=NULL OR @schemeId !='')
			SET @whereClause  = @whereClause + 'AND (GS_PROPERTY_APPLIANCE.SchemeId = '+convert(nvarchar,@schemeId)+' OR ' +convert(nvarchar,@schemeId)+'  IS NULL )'
		IF (@blockId!=NULL OR @blockId !='')
			SET @whereClause  = @whereClause + 'AND (GS_PROPERTY_APPLIANCE.BlockId = '+convert(nvarchar,@blockId)+'  OR    '+convert(nvarchar,@blockId)+'  IS NULL) '	
		--ELSE IF ((@schemeId !=NULL OR @schemeId !='') AND (@blockId!=NULL OR @blockId !=''))
		--BEGIN
		--	SET @whereClause  = @whereClause +'AND (GS_PROPERTY_APPLIANCE.SchemeId = '+convert(nvarchar,@schemeId)+' OR ' +convert(nvarchar,@schemeId)+'  IS NULL )'
		--	SET @whereClause  = @whereClause + 'AND (GS_PROPERTY_APPLIANCE.BlockId = '+convert(nvarchar,@blockId)+'  OR    '+convert(nvarchar,@blockId)+'  IS NULL) '
		--	END	
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
			  isnull(GS_Appliance_Type.APPLIANCETYPE,''-'') as [Type]      
			 ,isnull(GS_Location.Location,''-'') as Location      
			 ,isnull(GS_MANUFACTURER.MANUFACTURER,''-'') as Make      
			 ,isnull(GS_APPLIANCEModel.Model,''-'') as Model      
			 ,Convert(varchar(10),GS_PROPERTY_APPLIANCE.Datepurchased,103) as DatePurchased
			 , Convert(varchar(10),GS_PROPERTY_APPLIANCE.DateRemoved,103) as DateRemoved     
			 ,GS_PROPERTY_APPLIANCE.PurchaseCost as PurchaseCost      
			 ,GS_PROPERTY_APPLIANCE.PropertyApplianceId as ApplianceId    
			 ,GS_PROPERTY_APPLIANCE.LifeSpan    
			 ,GS_PROPERTY_APPLIANCE.Dimensions               
			 ,GS_PROPERTY_APPLIANCE.Quantity  
			 ,GS_PROPERTY_APPLIANCE.Item  
			 ,GS_PROPERTY_APPLIANCE.LocationID  
			 ,GS_PROPERTY_APPLIANCE.APPLIANCETYPEID as TypeId  
			 ,GS_PROPERTY_APPLIANCE.MANUFACTURERID as MakeId  
			 ,GS_PROPERTY_APPLIANCE.ModelID      as ModelId  
			 ,GS_PROPERTY_APPLIANCE.SerialNumber    
			 ,GS_PROPERTY_APPLIANCE.Notes    '
		
		
		
		
		--============================From Clause============================================
		SET @fromClause = 'FROM GS_PROPERTY_APPLIANCE            
		 Left JOIN GS_Appliance_Type ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_Appliance_Type.APPLIANCETYPEID      
		 Left JOIN GS_MANUFACTURER ON GS_PROPERTY_APPLIANCE.MANUFACTURERID = GS_MANUFACTURER.MANUFACTURERID      
		 left join GS_APPLIANCEModel on  GS_PROPERTY_APPLIANCE.ModelID =   GS_APPLIANCEModel.ModelID  
		 Left Join GS_Location On GS_PROPERTY_APPLIANCE.LocationID = GS_Location.LocationID  '
								
							
		--============================Order Clause==========================================
	IF @sortColumn = '' OR @sortOrder =''
		BEGIN	
		SET @sortColumn ='ApplianceId'
		SET @sortOrder ='DESC'
		END
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		--=================================	Where Clause ================================
		
		 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		PRINT ( @finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
											
END
