SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_SaveTeamJobRole @teamId=1,@jobRole = 'Administrator'
-- Author:		Ahmed Mehmood
-- Create date: <19/12/2013>
-- Last Modified: <19/12/2013>
-- Description:	<Save Team Job Role. >
-- Web Page: JobRoles.aspx

-- =============================================
CREATE PROCEDURE [dbo].[JRA_SaveTeamJobRole]
@teamId int
,@userId int
,@jobRole varchar(50)
,@isSaved bit out
AS
BEGIN

DECLARE @jobRoleId int
,@teamJobroleId int
,@jobRoleActionId int


BEGIN TRANSACTION;
BEGIN TRY

	
	IF EXISTS (	SELECT	1
	FROM	E_JOBROLE
	WHERE	E_JOBROLE.JobeRoleDescription= @jobRole)
	BEGIN
		
		SELECT	@jobRoleId = E_JOBROLE.JobRoleId 
		FROM	E_JOBROLE
		WHERE	E_JOBROLE.JobeRoleDescription= @jobRole
	END
	ELSE
	BEGIN
		INSERT INTO E_JOBROLE
           (JobeRoleDescription)
		VALUES
           (@jobRole)
           
		SELECT @jobRoleId = SCOPE_IDENTITY()
	END
	
           
    INSERT INTO E_JOBROLETEAM
           ([JobRoleId]
           ,[TeamId]
           ,[IsActive]
           ,[isDeleted])
     VALUES
           (@jobRoleId
           ,@teamId
           ,1
           ,0)
           
     SELECT @teamJobroleId = SCOPE_IDENTITY()       
          
    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Job Role Created'
          
    INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (@jobRoleActionId
           ,@teamJobroleId
           ,'Created'
           ,@userId
           ,GETDATE())
           

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END
GO
