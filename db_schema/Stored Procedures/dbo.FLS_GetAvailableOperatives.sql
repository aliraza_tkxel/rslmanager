USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FLS_GetAvailableOperatives]    Script Date: 11/29/2016 17:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON      
GO
/* =============================================
--EXEC	[dbo].[FLS_GetAvailableOperatives]
--		@startDate = N'' 
-- Author:		Rehan Baber
-- Create date: Create Date,,18 Jan,2018
-- Description:	This stored procedure fetch  the employees and their leaves.
-- ============================================= */

IF OBJECT_ID('dbo.FLS_GetAvailableOperatives') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_GetAvailableOperatives AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FLS_GetAvailableOperatives] 
	-- Add the parameters for the stored procedure here
	@startDate as datetime,
	@searchText as nvarchar(max)		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @operativeIdStr NVARCHAR(MAX)
	
	--=================================================================================================================
	------------------------------------------------------ Step 0------------------------------------------------------
	--Create Temporary Table
	IF 1 = 2
  BEGIN
	SELECT Distinct 0 as EmployeeId, '' as FirstName, '' as LastName, '' as FullName, 1 as PatchId, '' as PatchName, '' as Distance, 1 as TradeID, '' as Gender, '' as ImageName, '' as Email
	WHERE
    1 = 2 
  END
	
    DECLARE @AvailableOperatives TABLE (
		EmployeeId int
		,FirstName nvarchar(50)
		,LastName nvarchar(50)
		,FullName nvarchar(100)		
		,PatchId int
		,PatchName nvarchar(20)		
		,Distance nvarchar(10)
		,TradeID INT
		,Gender nvarchar(30)
		,ImageName nvarchar(100)
		,Email nvarchar(50)
						
	)
	DECLARE @AbsentOperatives TABLE (
		EMPLOYEEID int			
	)

	INSERT INTO @AbsentOperatives(EmployeeId)
	SELECT 
		EMPLOYEEID 
	FROM E_JOURNAL
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
	AND E_ABSENCE.ABSENCEHISTORYID IN (
										SELECT 
											MAX(ABSENCEHISTORYID) 
											FROM E_ABSENCE 
											GROUP BY JOURNALID
										)
	WHERE ITEMNATUREID = 1
	AND E_ABSENCE.RETURNDATE IS NULL
	AND E_ABSENCE.ITEMSTATUSID = 1
	UNION ALL	
			SELECT DISTINCT
				E_JOURNAL.EMPLOYEEID
			FROM E_JOURNAL
			INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
				AND E_ABSENCE.ABSENCEHISTORYID IN (
													SELECT 
														MAX(ABSENCEHISTORYID) 
														FROM E_ABSENCE 
														GROUP BY JOURNALID
													)
			INNER JOIN E_STATUS ON  E_ABSENCE.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
			INNER JOIN E_NATURE ON  E_JOURNAL.ITEMNATUREID = E_NATURE.ITEMNATUREID
			INNER JOIN E_JOBDETAILS ej ON E_JOURNAL.EMPLOYEEID = ej.EMPLOYEEID
			LEFT JOIN E_HOLIDAYRULE hl ON ej.HOLIDAYRULE = hl.EID
			WHERE E_ABSENCE.startdate <= @startDate and 
					E_ABSENCE.RETURNDATE >= @startDate and
				E_NATURE.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,48,43,52,53,54)
				AND E_ABSENCE.ITEMSTATUSID = 3 and
				ej.ISBRS=1
	--Trainings
	UNION ALL  
		SELECT  
		EET.EmployeeId
	FROM E_EmployeeTrainings EET    
	INNER JOIN E_JOBDETAILS ej ON EET.EMPLOYEEID = ej.EMPLOYEEID  
	WHERE EET.startdate <= @startDate and 
		  EET.ENDDATE >= @startDate AND EET.Active=1 AND EET.Status in (select StatusId from E_EmployeeTrainingStatus where Title = 'Requested' OR Title = 'Manager Supported' OR Title = 'Exec Supported' OR Title = 'Exec Approved' OR Title = 'HR Approved')
	DECLARE @InsertClause varchar(200)
	DECLARE @SelectClause varchar(800)
	DECLARE @FromClause varchar(800)
	DECLARE @WhereClause varchar(max)		
	DECLARE @MainQuery varchar(max)
	--=================================================================================================================
	------------------------------------------------------ Step 1------------------------------------------------------
	--This query fetches the employees which matches with the following criteria
	--Trade of employee is same as trade of component 
	--User type is M&E Servicing or Cyclic Maintenance		
	--Exclude the sick leaves
	INSERT INTO @AvailableOperatives(EmployeeId,FirstName,LastName,FullName, PatchId, PatchName, Gender, ImageName, Email)
	SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + ' '+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	,E__EMPLOYEE.GENDER
	,E__EMPLOYEE.IMAGEPATH
	, E_CONTACT.WORKEMAIL
	FROM  E__EMPLOYEE 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	INNER JOIN E_JOBROLETEAM ON E__EMPLOYEE.JobRoleTeamId = E_JOBROLETEAM.JobRoleTeamId
	INNER JOIN E_TEAM ON E_JOBROLETEAM.TeamId = E_TEAM.TEAMID
	INNER JOIN E_JOBROLE ON E_JOBROLETEAM.JobRoleId = E_JOBROLE.JobRoleId
	LEFT JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID
	WHERE E_JOBDETAILS.Active=1 
	AND E_TEAM.TEAMNAME = 'Housing Team'
	AND E_TEAM.ACTIVE = 1
	--AND E_JOBROLE.JobeRoleDescription = 'Neighbourhood Officer'
	AND (@searchText = '' or @searchText is null or E__EMPLOYEE.FIRSTNAME like @searchText or E__EMPLOYEE.LASTNAME like @searchText or E__EMPLOYEE.FIRSTNAME + E__EMPLOYEE.LASTNAME like REPLACE(@searchText,' ', ''))
	AND E__EMPLOYEE.EmployeeId NOT IN (
										SELECT EMPLOYEEID FROM @AbsentOperatives
									  )		
	
	SELECT Distinct EmployeeId, FirstName, LastName, FullName, isnull(PatchId,0) as PatchId, PatchName, Distance, isnull(TradeID,0) as TradeID, Gender, ImageName, Email  
	FROM  @AvailableOperatives	
	

END