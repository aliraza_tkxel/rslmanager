SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetFlueType
-- Author:		<Noor Muhammad>
-- Create date: <02/11/2012>
-- Description:	<Get all flue type>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetFlueType]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT FLUETYPEID as id, FLUETYPE as title FROM P_FLUETYPE 
END
GO
