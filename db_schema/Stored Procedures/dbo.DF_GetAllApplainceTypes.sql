USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAllApplainceTypes]    Script Date: 09/09/2015 15:33:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      09/09/2015
-- Description:      Get All Applaince Types
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================

IF OBJECT_ID('dbo.DF_GetAllApplainceTypes') IS NULL 
 EXEC('CREATE PROCEDURE dbo.DF_GetAllApplainceTypes AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[DF_GetAllApplainceTypes]
AS
BEGIN
	SET NOCOUNT ON;


		SELECT VALUEID AS Id, VALUEDETAIL AS Title, 'boiler' as defectType
		FROM	PA_PARAMETER_VALUE PPV
				INNER JOIN PA_PARAMETER PP ON PPV.PARAMETERID = PP.PARAMETERID  
		WHERE	PARAMETERNAME = 'Heating Type' AND VALUEDETAIL <> 'Please Select' 
				AND PPV.ISACTIVE = 1 AND PP.ISACTIVE = 1
				
		UNION ALL

		SELECT	GS_APPLIANCE_TYPE.APPLIANCETYPEID AS Id, GS_APPLIANCE_TYPE.APPLIANCETYPE AS Title , 'appliance' as defectType
		FROM	GS_APPLIANCE_TYPE
		WHERE	APPLIANCETYPE IS NOT NULL


	
END
