SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[P_SSIS_STOCK_SURVEY_DONE_PROPERTIES]
AS 
    SELECT  PROPERTYID AS [Property Ref],
            HOUSENUMBER AS [House Number],
            AddressLine1 AS [Address Line 1],
            AddressLine2 AS [Address Line 2],
            AddressLine3 AS [Address Line 3],
            CityTown AS [City/Town],
            County ,
            Postcode ,
            CAST([Appointment Start Date] AS SMALLDATETIME) AS [Appointment Start Date] ,
            CAST(AppointEndDateTime AS SMALLDATETIME) AS [Appointment End Date],
            [Appointment Progress Status] ,            
            CAST(SurveyDate AS SMALLDATETIME) AS [Survey Date] ,
            SurveyourUserName AS [Surveyor]
    FROM    vwP_SSIS_STOCK_SURVEY_DONE_PROPERTIES
    
GO
