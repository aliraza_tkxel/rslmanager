SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[AM_SP_GetPreviousTenantsList_old]
			@caseOwnedById int=0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@surname		varchar(100),
			@skipIndex	int = 0,
			@pageSize	int = 10,
			@sortBy     varchar(100),
            @sortDirection varchar(10)
AS
BEGIN


declare @orderbyClause varchar(100)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @RegionSuburbClause varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId = ' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'')'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END


SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
SET @query = 
'SELECT TOP ('+convert(varchar(10),@pageSize)+' )
				  AM_Customer_Rent_Parameters.TENANCYID, 
				  AM_Customer_Rent_Parameters.CUSTOMERID,   
				  ISNULL(Convert(varchar(100),(AM_Customer_Rent_Parameters.LastPaymentDate), 103), '''') AS TRANSACTIONDATE, 
				  ISNULL((AM_Customer_Rent_Parameters.LastPayment), 0.0) AS LastCPAY, 
				  ISNULL(AM_Customer_Rent_Parameters.Title,'''') + '' '' + ISNULL(AM_Customer_Rent_Parameters.FIRSTNAME, '''')+'' ''+ ISNULL(AM_Customer_Rent_Parameters.LASTNAME, '''') AS CustomerName, 
                  ISNULL(AM_Customer_Rent_Parameters.RentBalance,0.0) - ISNULL(AM_Customer_Rent_Parameters.EstimatedHBDue,0.0) As OwedToBHA,
				 AM_Customer_Rent_Parameters.CustomerAddress,
                  
					 AM_Customer_Rent_Parameters.RentBalance AS RentBalance,
                     AM_Customer_Rent_Parameters.EstimatedHBDue As EstimatedHBDue,
 				  
					
				   
					(SELECT ISNULL(P_FINANCIAL.Totalrent , 0)
													 FROM C_TENANCY INNER JOIN
													 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
													 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
													 WHERE C_TENANCY.TENANCYID = AM_Customer_Rent_Parameters.TENANCYID) AS TotalRent
				
					FROM         AM_Customer_Rent_Parameters 
								  INNER JOIN C_TENANCY ON AM_Customer_Rent_Parameters.TENANCYID = C_TENANCY.TENANCYID
								  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID				  								  
	
	WHERE '+@RegionSuburbClause+' 
		   AND C_TENANCY.ENDDATE IS NOT NULL
		   AND AM_Customer_Rent_Parameters.LASTNAME = CASE WHEN '''' = '''+ @surname +''' THEN AM_Customer_Rent_Parameters.LASTNAME ELSE '''+ @surname +''' END  
		   and AM_Customer_Rent_Parameters.CUSTOMERID NOT IN ('
		SET @subQuery ='SELECT CUSTOMERID FROM (
						SELECT TOP ('+convert(varchar(10),@skipIndex)+')								
								  AM_Customer_Rent_Parameters.TENANCYID, 
								  AM_Customer_Rent_Parameters.CUSTOMERID,   
								  ISNULL(Convert(varchar(100),(AM_Customer_Rent_Parameters.LastPaymentDate), 103), '''') AS TRANSACTIONDATE, 
								  ISNULL((AM_Customer_Rent_Parameters.LastPayment), 0.0) AS LastCPAY, 
								  ISNULL(AM_Customer_Rent_Parameters.Title,'''') + '' '' + ISNULL(AM_Customer_Rent_Parameters.FIRSTNAME, '''')+'' ''+ ISNULL(AM_Customer_Rent_Parameters.LASTNAME, '''') AS CustomerName, 
                                  ISNULL(AM_Customer_Rent_Parameters.RentBalance,0.0) - ISNULL(AM_Customer_Rent_Parameters.EstimatedHBDue,0.0) As OwedToBHA,
				                  
								AM_Customer_Rent_Parameters.CustomerAddress,                   
									 AM_Customer_Rent_Parameters.RentBalance AS RentBalance,				 				  
									 AM_Customer_Rent_Parameters.EstimatedHBDue As EstimatedHBDue,
								   
									(SELECT ISNULL(P_FINANCIAL.Totalrent , 0)
																	 FROM C_TENANCY INNER JOIN
																	 P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
																	 P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID 
																	 WHERE C_TENANCY.TENANCYID = AM_Customer_Rent_Parameters.TENANCYID) AS TotalRent				
					
					FROM AM_Customer_Rent_Parameters 
								  INNER JOIN C_TENANCY ON AM_Customer_Rent_Parameters.TENANCYID = C_TENANCY.TENANCYID
								  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID				  								  

						WHERE '+@RegionSuburbClause+' 
						   AND C_TENANCY.ENDDATE IS NOT NULL
						   AND AM_Customer_Rent_Parameters.LASTNAME = CASE WHEN '''' = '''+ @surname +''' THEN AM_Customer_Rent_Parameters.LASTNAME ELSE '''+ @surname +''' END '
							+ ' '  + @orderbyClause + ') as Temp)' 

print(@query + @subQuery + @orderbyClause);
exec(@query + @subQuery + @orderbyClause);		   


END












GO
