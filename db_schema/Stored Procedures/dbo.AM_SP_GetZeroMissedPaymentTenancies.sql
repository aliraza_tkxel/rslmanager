SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Noor Muhammad>
-- Create date: <30 Aug 2012>
-- Description:	<This stored procedure should be avoided by using this query in AM_FindOverDueActionCases
--				 This procedure was created to deliver things fast				 	
--				 This function will return the all the distinct tenancies>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetZeroMissedPaymentTenancies] 
	
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Select distinct AM_MissedPaymentsZero.TenantId
	FROM AM_MissedPaymentsZero 
	INNER JOIN AM_Case on AM_MissedPaymentsZero.TenantId = AM_Case.tenancyId
	WHERE IsActive='true'
END

GO
