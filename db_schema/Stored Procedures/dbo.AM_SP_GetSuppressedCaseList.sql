
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetSuppressedCaseList]
	@assignedToId	int = 0,
	@regionId	int = 0,
	@suburbId	int = 0,
	@allAssignedFlag	bit,
	@allRegionFlag	bit,
	@allSuburbFlag	bit,
	@skipIndex	int = 0,
	@pageSize	int = 10,
	@sortBy     varchar(100),
    @sortDirection varchar(10)

AS
BEGIN	

declare @orderbyClause varchar(50)
declare @query varchar(8000)
declare @subQuery varchar(8000)
declare @RegionSuburbClause varchar(8000)

IF(@assignedToId = 0 )
BEGIN

	IF(@regionId = 0 and @suburbId =0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

END
ELSE 
BEGIN

IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ 'AND IsActive=''true'')'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END

SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection
SET @query = 
'select TOP ('+convert(varchar(10),@pageSize)+') Max(AM_Case.CaseId) As CaseId, 
						Max(AM_Case.SuppressedReason) As SuppressedReason, 
						Max(convert(varchar(100), AM_Case.SuppressedDate, 103)) as SuppressedDate, 
						Max(ISNULL(LEFT(E__Employee.FirstName,1), '''') + '''' +ISNULL(LEFT(E__Employee.LastName,1), ''''))  as SuppressedBy, 
						
						  (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
							WHERE TenancyId = AM_Case.TenancyId
							ORDER BY CustomerId ASC) as CustomerName,

					    (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
							WHERE TenancyId = AM_Case.TenancyId
							ORDER BY CustomerId DESC) as CustomerName2,

					    (SELECT Count(DISTINCT CustomerId)
							FROM AM_Customer_Rent_Parameters
							WHERE	TenancyId = AM_Case.TenancyId) as JointTenancyCount,	
                  
						Max(customer.CustomerAddress) AS CustomerAddress, 
						Convert(varchar(8000),ISNULL((SELECT TOP 1 C_ADDRESS.TEL FROM C_Address where CustomerId = Max(customer.CUSTOMERID)),'''')) as Telephone, 
						
						Max(AM_Case.TenancyId) As TenancyId,
						Max(customer.customerId) As customerId
						
						FROM         AM_Case INNER JOIN
									  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN									  
									  C_TENANCY ON customer.TENANCYID = C_TENANCY.TENANCYID INNER JOIN
									  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
									  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
									  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID  
									  INNER JOIN AM_Resource ON AM_Case.SuppressedBy = AM_Resource.ResourceId INNER JOIN
									  E__EMPLOYEE ON AM_Resource.EmployeeId = E__Employee.EmployeeId

						WHERE '+ @RegionSuburbClause +' 
							AND (AM_Case.CaseOfficer = (case when 0 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																								OR AM_Case.CaseManager = (case when 0 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId )+ ' END) ) 
							AND AM_Case.IsSuppressed = 1 
							AND AM_Case.IsActive = 1
							and	AM_Case.TenancyId NOT IN ('

							SET @subQuery ='SELECT TenancyId FROM(
																select TOP ('+convert(varchar(10),@skipIndex)+') Max(AM_Case.CaseId) As CaseId, 
						Max(AM_Case.SuppressedReason) As SuppressedReason, 
						Max(convert(varchar(100), AM_Case.SuppressedDate, 103)) as SuppressedDate, 
						Max(ISNULL(LEFT(E__Employee.FirstName,1), '''') + '''' +ISNULL(LEFT(E__Employee.LastName,1), ''''))  as SuppressedBy, 
						
						  (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
							WHERE TenancyId = AM_Case.TenancyId
							ORDER BY CustomerId ASC) as CustomerName,

					    (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName
							FROM AM_Customer_Rent_Parameters
							WHERE TenancyId = AM_Case.TenancyId
							ORDER BY CustomerId DESC) as CustomerName2,

					    (SELECT Count(DISTINCT CustomerId)
							FROM AM_Customer_Rent_Parameters
							WHERE	TenancyId = AM_Case.TenancyId) as JointTenancyCount,	
                  
						Max(customer.CustomerAddress) AS CustomerAddress, 
						Convert(varchar(8000),ISNULL((SELECT TOP 1 C_ADDRESS.TEL FROM C_Address where CustomerId = Max(customer.CUSTOMERID)),'''')) as Telephone, 
						
						Max(AM_Case.TenancyId) As TenancyId,
						Max(customer.customerId) As customerId
																
																FROM         AM_Case INNER JOIN
																			  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN									  
																			  C_TENANCY ON customer.TENANCYID = C_TENANCY.TENANCYID INNER JOIN
																			  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
																			  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
																			  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID   
																			  INNER JOIN AM_Resource ON AM_Case.SuppressedBy = AM_Resource.ResourceId INNER JOIN
																			  E__EMPLOYEE ON AM_Resource.EmployeeId = E__Employee.EmployeeId

																WHERE '+ @RegionSuburbClause +' 
																	AND (AM_Case.CaseOfficer = (case when 0 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																																		OR AM_Case.CaseManager = (case when 0 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId )+ ' END) ) 
																	AND AM_Case.IsSuppressed = 1 
																	AND AM_Case.IsActive = 1
													GROUP BY AM_Case.TenancyId 
												'  + @orderbyClause + ') as Temp
									) GROUP BY AM_Case.TenancyId 
									'

												
print(@query + @subQuery + @orderbyClause);
exec(@query + @subQuery + @orderbyClause);	


END











GO
