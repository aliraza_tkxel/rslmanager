USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_CancelToBeArrangedAppointment @pmo,@reasonId,@reason,@prevDue,@Due,@isCancelled
-- Author:		Altamish Arif
-- Create date: <10/4/2016>
-- Last Modified: <10/4/2016>
-- Description:	<Description,cancel appointments associated with PMO >
-- Webpage : ViewToBeArrangedPlannedAppointments.aspx

-- =============================================
IF OBJECT_ID('dbo.[PLANNED_CancelToBeArrangedAppointment]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_CancelToBeArrangedAppointment] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_CancelToBeArrangedAppointment]
	 @pmo int
	,@reasonId int
	,@reason varchar(2000)
	,@itemId int
	,@newDue datetime2
	,@prevDue datetime2
	,@userId int
	,@PropertyId varchar(200)
	,@schemeId int
	,@blockId int
	,@type varchar(200)
	,@componentId int
	,@isCancelled int = 0 out
AS
BEGIN

DECLARE @JournalTypeId INT = NULL


BEGIN TRANSACTION;
BEGIN TRY

BEGIN -- Region for insertion in PLANNED_CANCEL_APPOINTMENTS
	-- ========================================================
	--	1- INSERT INTO PLANNED_CANCEL_APPOINTMENTS
	-- ========================================================
	 
	INSERT INTO PLANNED_CANCEL_APPOINTMENTS VALUES ( @pmo,@reasonId,@reason,@userId,GETDATE())
	

END -- Region for insertion in PLANNED_CANCEL_APPOINTMENTS
		
BEGIN -- Region for Update Jounal with 'Cancelled' Status and insert an entry in history.
	-- ========================================================
	--	2- UPDATE PLANNED_JOURNAL
	-- ========================================================
	
	DECLARE @StatusId INT
	SELECT @StatusId = STATUSID
	FROM PLANNED_STATUS WHERE PLANNED_STATUS.TITLE= 'Cancelled'
	
	UPDATE	PLANNED_JOURNAL
	SET		PLANNED_JOURNAL.STATUSID = @StatusId			
	WHERE	PLANNED_JOURNAL.JOURNALID = @pmo
	

	-- ========================================================
	--	3- INSERT INTO PLANNED_JOURNAL_HISTORY
	-- ========================================================
	
	INSERT INTO PLANNED_JOURNAL_HISTORY
           ([JOURNALID]
           ,[PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]
           ,[StatusHistoryId]
           ,[ActionHistoryId]
           ,[IsDocumentAttached]
		   ,[SchemeId]
		   ,[BlockId])
           
	SELECT	PLANNED_JOURNAL.JOURNALID  AS JOURNALID
			,PLANNED_JOURNAL.PROPERTYID AS PROPERTYID
			,PLANNED_JOURNAL.COMPONENTID AS COMPONENTID
			,PLANNED_JOURNAL.STATUSID AS STATUSID
			,PLANNED_JOURNAL.ACTIONID AS ACTIONID
			,GETDATE() AS CREATIONDATE
			,PLANNED_JOURNAL.CREATEDBY AS CREATEDBY
			,@reason AS NOTES
			,0 AS ISLETTERATTACHED
			,NULL AS StatusHistoryId
			,NULL AS ActionHistoryId
			,0 AS IsDocumentAttached
			,PLANNED_JOURNAL.SchemeId AS SchemeId
			,PLANNED_JOURNAL.BlockId AS BlockId
	FROM PLANNED_JOURNAL
	WHERE PLANNED_JOURNAL.JournalId = @pmo
	
	-- Get Journal history Id to change in in planned appointment
	DECLARE @JournalHistoryId BIGINT = SCOPE_IDENTITY()
	
END -- Region for Update Jounal with 'Cancelled' Status and insert an entry in history.	

BEGIN -- Region for Update planned appopintment with 'Cancelled' Status and updated journal history id.
	
	-- ========================================================
	--	4- UPDATE INTO PLANNED_APPOINTMENTS
	-- ========================================================
	-- Update planned appointment status an entry will be inserted in histroy with defined triger.
	UPDATE PLANNED_APPOINTMENTS SET
		JOURNALHISTORYID = @JournalHistoryId,
		APPOINTMENTSTATUS = 'Cancelled'		
	WHERE JournalId = @pmo AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
	
End -- Region for Update planned appopintment with 'Cancelled' Status and updated journal history id.
	
-- ===================================================================================	
	-- 5- Update ReplacementDue in PA_PROPERTY_ITEM_DATES
-- ===================================================================================	

	IF(@ComponentId IS NULL)
		BEGIN
			if(@type = 'Property')
			BEGIN
				UPDATE	PA_PROPERTY_ITEM_DATES 
				SET		DueDate  = @newDue
						,LastDone = @prevDue
						,UPDATEDON = GETDATE()
						,UPDATEDBY = @userId
				WHERE	ItemId = @ItemId 
						AND PROPERTYID = @PropertyId
						AND PLANNED_COMPONENTID IS NULL
			END

			if(@type = 'Scheme')
			BEGIN
				UPDATE	PA_PROPERTY_ITEM_DATES 
				SET		DueDate  = @newDue
						,LastDone = @prevDue
						,UPDATEDON = GETDATE()
						,UPDATEDBY = @userId
				WHERE	ItemId = @ItemId 
						AND SchemeId = @schemeId
						AND PLANNED_COMPONENTID IS NULL
			END

			if(@type = 'Block')
			BEGIN
				UPDATE	PA_PROPERTY_ITEM_DATES 
				SET		DueDate  = @newDue
						,LastDone = @prevDue
						,UPDATEDON = GETDATE()
						,UPDATEDBY = @userId
				WHERE	ItemId = @ItemId 
						AND BlockId = @blockId
						AND PLANNED_COMPONENTID IS NULL
			END

		END
	ELSE
		BEGIN
			if(@type = 'Property')
			BEGIN
				UPDATE	PA_PROPERTY_ITEM_DATES 
				SET		DueDate  = @newDue
						,LastDone = @prevDue
						,UPDATEDON = GETDATE()
						,UPDATEDBY = @userId
				WHERE	PLANNED_COMPONENTID = @ComponentId 
					AND PROPERTYID = @PropertyId		
			END

			if(@type = 'Scheme')
			BEGIN
				UPDATE	PA_PROPERTY_ITEM_DATES 
				SET		DueDate  = @newDue
						,LastDone = @prevDue
						,UPDATEDON = GETDATE()
						,UPDATEDBY = @userId
				WHERE	PLANNED_COMPONENTID = @ComponentId 
						AND SchemeId = @schemeId		
			END

			if(@type = 'Block')
			BEGIN
				UPDATE	PA_PROPERTY_ITEM_DATES 
				SET		DueDate  = @newDue
						,LastDone = @prevDue
						,UPDATEDON = GETDATE()
						,UPDATEDBY = @userId
				WHERE	PLANNED_COMPONENTID = @ComponentId 
						AND BlockId = @blockId		
			END	
		END	
END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isCancelled = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isCancelled = 1
 END

END