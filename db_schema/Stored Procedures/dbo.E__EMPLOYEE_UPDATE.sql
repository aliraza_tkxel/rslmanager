
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[E__EMPLOYEE_UPDATE]
    (
      @CREATIONDATE SMALLDATETIME ,
      @TITLE INT ,
      @FIRSTNAME NVARCHAR(50) ,
      @MIDDLENAME NVARCHAR(50) ,
      @LASTNAME NVARCHAR(100) ,
      @DOB SMALLDATETIME ,
      @GENDER NVARCHAR(20) ,
      @MARITALSTATUS INT ,
      @ETHNICITY INT ,
      @ETHNICITYOTHER NVARCHAR(80) ,
      @RELIGION INT ,
      @RELIGIONOTHER NVARCHAR(80) ,
      @SEXUALORIENTATION INT ,
      @SEXUALORIENTATIONOTHER NVARCHAR(80) ,
      @DISABILITY NVARCHAR(80) ,
      @DISABILITYOTHER NVARCHAR(80) ,
      @BANK NVARCHAR(100) ,
      @SORTCODE NVARCHAR(30) ,
      @ACCOUNTNUMBER NVARCHAR(30) ,
      @ACCOUNTNAME NVARCHAR(100) ,
      @ROLLNUMBER NVARCHAR(30) ,
      @BANK2 NVARCHAR(100) ,
      @SORTCODE2ND NVARCHAR(50) ,
      @ACCOUNTNUMBER2 NVARCHAR(50) ,
      @ACCOUNTNAME2 NVARCHAR(100) ,
      @ROLLNUMBER2 NVARCHAR(50) ,
      @ORGID INT ,
      @IMAGEPATH NCHAR(250) ,
      @ROLEPATH NCHAR(250) ,
      @PROFILE NVARCHAR(500) ,
      @LASTLOGGEDIN SMALLDATETIME ,
      @Original_EMPLOYEEID INT ,
      @EMPLOYEEID INT
    )
AS 
    SET NOCOUNT OFF;
    UPDATE  [E__EMPLOYEE]
    SET     [CREATIONDATE] = @CREATIONDATE ,
            [TITLE] = @TITLE ,
            [FIRSTNAME] = @FIRSTNAME ,
            [MIDDLENAME] = @MIDDLENAME ,
            [LASTNAME] = @LASTNAME ,
            [DOB] = @DOB ,
            [GENDER] = @GENDER ,
            [MARITALSTATUS] = @MARITALSTATUS ,
            [ETHNICITY] = @ETHNICITY ,
            [ETHNICITYOTHER] = @ETHNICITYOTHER ,
            [RELIGION] = @RELIGION ,
            [RELIGIONOTHER] = @RELIGIONOTHER ,
            [SEXUALORIENTATION] = @SEXUALORIENTATION ,
            [SEXUALORIENTATIONOTHER] = @SEXUALORIENTATIONOTHER ,
            [DISABILITY] = @DISABILITY ,
            [DISABILITYOTHER] = @DISABILITYOTHER ,
            [BANK] = @BANK ,
            [SORTCODE] = @SORTCODE ,
            [ACCOUNTNUMBER] = @ACCOUNTNUMBER ,
            [ACCOUNTNAME] = @ACCOUNTNAME ,
            [ROLLNUMBER] = @ROLLNUMBER ,
            [BANK2] = @BANK2 ,
            [SORTCODE2ND] = @SORTCODE2ND ,
            [ACCOUNTNUMBER2] = @ACCOUNTNUMBER2 ,
            [ACCOUNTNAME2] = @ACCOUNTNAME2 ,
            [ROLLNUMBER2] = @ROLLNUMBER2 ,
            [ORGID] = @ORGID ,
            [IMAGEPATH] = @IMAGEPATH ,
            [ROLEPATH] = @ROLEPATH ,
            [PROFILE] = @PROFILE
    WHERE   ( ([EMPLOYEEID] = @Original_EMPLOYEEID) );
	
    SELECT  e.EMPLOYEEID ,
            e.CREATIONDATE ,
            e.TITLE ,
            e.FIRSTNAME ,
            e.MIDDLENAME ,
            e.LASTNAME ,
            e.DOB ,
            e.GENDER ,
            e.MARITALSTATUS ,
            e.ETHNICITY ,
            e.ETHNICITYOTHER ,
            e.RELIGION ,
            e.RELIGIONOTHER ,
            e.SEXUALORIENTATION ,
            e.SEXUALORIENTATIONOTHER ,
            e.DISABILITY ,
            e.DISABILITYOTHER ,
            e.BANK ,
            e.SORTCODE ,
            e.ACCOUNTNUMBER ,
            e.ACCOUNTNAME ,
            e.ROLLNUMBER ,
            e.BANK2 ,
            e.SORTCODE2ND ,
            e.ACCOUNTNUMBER2 ,
            e.ACCOUNTNAME2 ,
            e.ROLLNUMBER2 ,
            e.ORGID ,
            e.IMAGEPATH ,
            e.ROLEPATH ,
            e.PROFILE ,
            l.LASTLOGGEDIN
    FROM    E__EMPLOYEE e
            LEFT JOIN dbo.AC_LOGINS l ON l.EMPLOYEEID = e.EMPLOYEEID
    WHERE   ( e.EMPLOYEEID = @EMPLOYEEID )

GO
