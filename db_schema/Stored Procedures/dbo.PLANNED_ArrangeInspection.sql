
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,10/31/2013>
-- Description:	<Description,Update/Insert PLANNED_JOURNAL,PLANNED_INSPECTION_APPOINTMENTS
--Last modified Date:10/31/2013
---======================================================================

--EXEC	@return_value = dbo.PLANNED_ArrangeInspection
--		@USERID = 142,
--		@SelectedReplacementItems = @ReplacementItem

-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_ArrangeInspection]
	-- Add the parameters for the stored procedure here
@PropertyId varchar(20)
,@ComponentId int
,@statusId int
,@actionId int
,@userId int
,@isLetterAttached bit
,@isDocumentAttached bit
,@tenancyId int
,@appointmentDate Date
,@appointmentStartTime varchar(10)
,@appointmentEndTime varchar(10)
,@operativeId int

,@inspectionId int
,@isSaved bit out
,@journalHistoryIdFinal int = -1 out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY          
		DECLARE 
		@InspectionArrangeId int
		,@ToBeArrangeId int

		,@JournalId int
		,@JournalHistoryId int
		,@CreationDate smalldatetime
		,@CreatedBy int 

		SELECT  @InspectionArrangeId = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='Inspection Arranged'
		SELECT  @ToBeArrangeId = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='To be Arranged'


		if(@actionId=-1)
			set @actionId = NULL

		IF @inspectionId = -1
		BEGIN
			PRINT 'ADD NEW INSPECTION'

			IF EXISTS (	SELECT	1
						FROM	PLANNED_JOURNAL 
						WHERE	PLANNED_JOURNAL.PROPERTYID = @PropertyId
						AND PLANNED_JOURNAL.COMPONENTID =@ComponentId
						AND PLANNED_JOURNAL.STATUSID = @ToBeArrangeId )
			BEGIN	 
				PRINT 'TO BE ARRANGED'
		
				-- If record is found then change the status to 'Inspection Arranged'
 
				DECLARE journalCursor CURSOR 
				FOR  
			
					SELECT	JOURNALID,CREATIONDATE,CREATEDBY
					FROM	PLANNED_JOURNAL 
					WHERE	PLANNED_JOURNAL.PROPERTYID = @PropertyId
							AND PLANNED_JOURNAL.COMPONENTID =@ComponentId
							AND  PLANNED_JOURNAL.STATUSID = @ToBeArrangeId 

					OPEN journalCursor   
					FETCH NEXT FROM journalCursor INTO @JournalId,@CreationDate,@CreatedBy 

						WHILE @@FETCH_STATUS = 0   
						BEGIN	
							
							PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@JournalId )
						--	Update PLANNED_JOURNAL by setting status to ‘Inspection Arranged’							
							UPDATE	PLANNED_JOURNAL
							SET		PLANNED_JOURNAL.STATUSID = @InspectionArrangeId,
									PLANNED_JOURNAL.ACTIONID = @actionId,
									PLANNED_JOURNAL.CREATEDBY = @userId
									
							WHERE	PLANNED_JOURNAL.PROPERTYID = @PropertyId
									AND PLANNED_JOURNAL.COMPONENTID =@ComponentId
									AND PLANNED_JOURNAL.STATUSID = @ToBeArrangeId  

						--  Insert into PLANNED_JOURNAL_HISTORY
							INSERT PLANNED_JOURNAL_HISTORY
							([JOURNALID]
						   ,[PROPERTYID]
						   ,[COMPONENTID]
						   ,[STATUSID]
						   ,[ACTIONID]
						   ,[CREATIONDATE]
						   ,[CREATEDBY]
						   ,[NOTES]
						   ,[ISLETTERATTACHED]
						   ,[StatusHistoryId]
						   ,[ActionHistoryId]
						   ,[IsDocumentAttached]) VALUES
							(@JournalId,@PropertyId,@ComponentId,@InspectionArrangeId,@actionId,@CreationDate,@userId,NULL,@isLetterAttached,NULL,NULL,@isDocumentAttached)

							SELECT @JournalHistoryId = SCOPE_IDENTITY()
							PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)
							
						--	Insert into PLANNED_INSPECTION_APPOINTMENTS					
							INSERT INTO PLANNED_INSPECTION_APPOINTMENTS
						   (TENANCYID,JournalId,JOURNALHISTORYID,APPOINTMENTDATE,APPOINTMENTTIME,APPOINTMENTENDTIME,ACTIONID,ASSIGNEDTO,CREATEDBY,LOGGEDDATE,APPOINTMENTNOTES,TRADEID)
							VALUES
						   (@tenancyId,@JournalId,@JournalHistoryId,@appointmentDate,@appointmentStartTime,@appointmentEndTime,@actionId,@operativeId,@userId,GETDATE(),NULL,NULL)
						
							SELECT @inspectionId = SCOPE_IDENTITY()			
							PRINT 'INSPECTIONID = '+ CONVERT(VARCHAR,@inspectionId)
							
							FETCH NEXT FROM journalCursor INTO @JournalId,@CreationDate,@CreatedBy   
						END   

				CLOSE journalCursor   
				DEALLOCATE journalCursor
			END 	
			ELSE --EXISTS (	SELECT	1
			BEGIN		
				PRINT 'TO BE SCHEDULE'			
				-- Insert into PLANNED_JOURNAL with Status of 'Inspection Arranged'	
				INSERT PLANNED_JOURNAL
				 ([PROPERTYID]
				   ,[COMPONENTID]
				   ,[STATUSID]
				   ,[ACTIONID]
				   ,[CREATIONDATE]
				   ,[CREATEDBY])
				VALUES  (@PropertyId,@ComponentId,@InspectionArrangeId,@actionId,GETDATE(),@userId)
				
				SELECT @JournalId = SCOPE_IDENTITY()		
				PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@JournalId )
			
				-- Insert into PLANNED_JOURNAL_HISTORY
				INSERT PLANNED_JOURNAL_HISTORY
				([JOURNALID]
				   ,[PROPERTYID]
				   ,[COMPONENTID]
				   ,[STATUSID]
				   ,[ACTIONID]
				   ,[CREATIONDATE]
				   ,[CREATEDBY]
				   ,[NOTES]
				   ,[ISLETTERATTACHED]
				   ,[StatusHistoryId]
				   ,[ActionHistoryId]
				   ,[IsDocumentAttached]) VALUES
					(@JournalId,@PropertyId,@ComponentId,@InspectionArrangeId,@actionId,GETDATE(),@userId,NULL,@isLetterAttached,NULL,NULL,@isDocumentAttached)
					
				SELECT @JournalHistoryId = SCOPE_IDENTITY()
				PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)
				
				-- Insert into PLANNED_INSPECTION_APPOINTMENTS
				INSERT INTO PLANNED_INSPECTION_APPOINTMENTS
				   (TENANCYID,JournalId,JOURNALHISTORYID,APPOINTMENTDATE,APPOINTMENTTIME,APPOINTMENTENDTIME,ACTIONID,ASSIGNEDTO,CREATEDBY,LOGGEDDATE,APPOINTMENTNOTES,TRADEID)
					VALUES
				   (@tenancyId,@JournalId,@JournalHistoryId,@appointmentDate,@appointmentStartTime,@appointmentEndTime,@actionId,@operativeId,@userId,GETDATE(),NULL,NULL)
				
				SELECT @inspectionId = SCOPE_IDENTITY()
				PRINT 'INSPECTIONID = '+ CONVERT(VARCHAR,@inspectionId)		
			END 		
		END
		ELSE --@inspectionId != -1
		BEGIN	
			PRINT 'UPDATE EXISTING INSPECTION'
			
			-- SELECT JOURNALID 
			SELECT	@JournalId= PLANNED_INSPECTION_APPOINTMENTS.JournalId
			FROM	PLANNED_INSPECTION_APPOINTMENTS 
			WHERE	PLANNED_INSPECTION_APPOINTMENTS.InspectionID = @inspectionId
		
			PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@JournalId )
			
			-- INSERT INTO JOURNAL HISTORY
			INSERT PLANNED_JOURNAL_HISTORY
			([JOURNALID]
			   ,[PROPERTYID]
			   ,[COMPONENTID]
			   ,[STATUSID]
			   ,[ACTIONID]
			   ,[CREATIONDATE]
			   ,[CREATEDBY]
			   ,[NOTES]
			   ,[ISLETTERATTACHED]
			   ,[StatusHistoryId]
			   ,[ActionHistoryId]
			   ,[IsDocumentAttached]) VALUES
				(@JournalId,@PropertyId,@ComponentId,@InspectionArrangeId,@actionId,GETDATE(),@userId,NULL,@isLetterAttached,NULL,NULL,@isDocumentAttached)
				
			SELECT @JournalHistoryId = SCOPE_IDENTITY()
			PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)
			
			
		   -- UPDATE INTO PLANNED_INSPECTION_APPOINTMENTS 
			UPDATE PLANNED_INSPECTION_APPOINTMENTS
			SET TENANCYID = @tenancyId
			,JournalId = @JournalId
			,JOURNALHISTORYID = @JournalHistoryId
			,APPOINTMENTDATE = @appointmentDate
			,APPOINTMENTTIME = @appointmentStartTime
			,APPOINTMENTENDTIME = @appointmentEndTime
			,ACTIONID = @actionId
			,ASSIGNEDTO = @operativeId
			,CREATEDBY = @userId
			,LOGGEDDATE = GETDATE()
			,APPOINTMENTNOTES = NULL
			,TRADEID = null
			WHERE PLANNED_INSPECTION_APPOINTMENTS.InspectionID = @inspectionId
	          
			PRINT 'INSPECTIONID = '+ CONVERT(VARCHAR,@inspectionId) 
	           
			-- UPDATE PLANNED_JOURNAL
			UPDATE	PLANNED_JOURNAL
			SET		PLANNED_JOURNAL.STATUSID = @InspectionArrangeId,
					PLANNED_JOURNAL.ACTIONID = @actionId
			WHERE	PLANNED_JOURNAL.JOURNALID = @JournalId
		END
		SET @journalHistoryIdFinal = @JournalHistoryId
	END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;		
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END 
	
END
GO
