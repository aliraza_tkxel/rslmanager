USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[AS_GetEmpTrades]    Script Date: 11/05/2018 11:53:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
 --EXEC AS_GetEmpTrades
 --@empId = 423,		

-- ============================================= */
IF OBJECT_ID('dbo.AS_GetEmpTrades') IS NULL -- Check if SP Exists
	EXEC ('CREATE PROCEDURE dbo.AS_GetEmpTrades AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_GetEmpTrades] (
	--Parameters which would help in sorting and paging
	@empId INT
	)
AS
BEGIN
	SELECT [DESCRIPTION] AS TRADE
	FROM E_TRADE et
	INNER JOIN G_TRADE gt ON et.TradeId = gt.TradeId
	WHERE et.EmpId = @empId
	ORDER BY et.EmpTradeId
END