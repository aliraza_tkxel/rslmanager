SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[AM_SP_GetPreviousTenantsCount_old] 
			@caseOwnedById int=0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@surname		varchar(100),
			@skipIndex	int = 0,
			@pageSize	int = 10
AS
BEGIN

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

END
ELSE 
BEGIN

IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'')'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END

SET @query =
					'SELECT COUNT(*) as recordCount
				
					FROM         AM_Customer_Rent_Parameters 
								  INNER JOIN C_TENANCY ON AM_Customer_Rent_Parameters.TENANCYID = C_TENANCY.TENANCYID
								  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID				  
								  --INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
		
					WHERE '+@RegionSuburbClause+' 
	--					   AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId 
	--																	FROM AM_Case 
	--																	WHERE AM_Case.IsActive= ''true'' ) 		   
	--					   AND AM_FirstDetecionList.IsDefaulter = ''false''
						   AND C_TENANCY.ENDDATE IS NOT NULL
						   AND AM_Customer_Rent_Parameters.LASTNAME = CASE WHEN '''' = '''+ @surname +''' THEN AM_Customer_Rent_Parameters.LASTNAME ELSE '''+ @surname +''' END  '

PRINT(@query);
EXEC(@query);

END









GO
