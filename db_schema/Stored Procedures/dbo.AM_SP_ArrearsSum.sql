SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_ArrearsSum]

AS
BEGIN
	SELECT     sum(P_FINANCIAL.TOTALRENT)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_Case.TenancyId)) as RentBalance
FROM         AM_Case INNER JOIN
                      C_TENANCY ON AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
                      P_FINANCIAL ON C_TENANCY.PROPERTYID = P_FINANCIAL.PROPERTYID
WHERE AM_Case.IsActive = 1
END





GO
