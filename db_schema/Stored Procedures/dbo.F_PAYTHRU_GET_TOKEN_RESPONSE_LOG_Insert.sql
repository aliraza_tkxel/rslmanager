SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[F_PAYTHRU_GET_TOKEN_RESPONSE_LOG_Insert] 
    @TransactionId uniqueidentifier,
    @Payload xml,
    @RedemptionUrl varchar(MAX),
    @DateReceived datetime,
    @DateCreated datetime,
    @CreatedBy varchar(256)
AS 
	SET NOCOUNT ON 
	
    INSERT  INTO [dbo].[F_PAYTHRU_GET_TOKEN_RESPONSE_LOG]
            ( [TransactionId] ,
              [Payload] ,
              [RedemptionUrl] ,
              [DateReceived] ,
              [DateCreated] ,
              [CreatedBy]
            )
            SELECT  @TransactionId ,
                    @Payload ,
                    @RedemptionUrl ,
                    @DateReceived ,
                    @DateCreated ,
                    @CreatedBy

	SELECT SCOPE_IDENTITY() AS GetTokenResponseId


GO
