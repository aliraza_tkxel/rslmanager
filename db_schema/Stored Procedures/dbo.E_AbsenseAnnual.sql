USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_AbsenseAnnual]    Script Date: 6/6/2017 3:39:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Rehman
-- Create date: June 2, 2017
-- =============================================

IF OBJECT_ID('dbo.E_AbsenseAnnual') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_AbsenseAnnual AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_AbsenseAnnual] 
	-- Add the parameters for the stored procedure here
	@empId INT,
	@fiscalYear SMALLDATETIME = null
	
AS
BEGIN

DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME	

--SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
--	FROM	EMPLOYEE_ANNUAL_START_END_DATE(@empId)

if(@fiscalYear is null)
begin
	set @fiscalYear = getDate()
end

SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE_ByYear(@empId, @fiscalYear)
	
	
SELECT e.STARTDATE,e.RETURNDATE,s.DESCRIPTION as Status, n.DESCRIPTION as Nature, cast(duration as decimal (16, 2)) as DAYS_ABS, e.HOLTYPE as HolType, e.ABSENCEHISTORYID, e.LASTACTIONDATE as LastActionDate
, e.DURATION_TYPE AS DurationType
FROM E_JOURNAL j
inner join E_ABSENCE e on j.JOURNALID = e.JOURNALID
 AND e.ABSENCEHISTORYID = (
									SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
									WHERE JOURNALID = j.JOURNALID
								  ) 

INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
WHERE j.EMPLOYEEID = @empId and j.ITEMNATUREID = 2 and e.STARTDATE >= @BHSTART and (e.STARTDATE <= @BHEND or @fiscalYear is null)
ORDER by e.STARTDATE DESC

	
END
