USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocksByScheme]    Script Date: 10/30/2017 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_GetBlocksByScheme]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetBlocksByScheme] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetBlocksByScheme]
	@schemeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 0 AS BLOCKID, 'All' AS BLOCKNAME

	UNION ALL

	SELECT BLOCKID, BLOCKNAME
	FROM P_BLOCK
	WHERE SchemeId = @schemeId
	ORDER BY BLOCKNAME
	
	
	
END
