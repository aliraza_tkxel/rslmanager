IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_GetBudgetHolderByOrderId'))
  DROP PROCEDURE AS_GetBudgetHolderByOrderId
  GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 1/3/2016
-- Description:	Get budget holder email id of fuel servicing 
-- exec AS_GetBudgetHolderByOrderId 265749
-- =============================================
Create PROCEDURE [dbo].[AS_GetBudgetHolderByOrderId]
	-- Add the parameters for the stored procedure here
	@orderId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 

   SELECT PO.OrderId, emp.EMPLOYEEID,emp.OperativeName,emp.Email ,emp.MinLIMIT as limit                    
FROM F_PURCHASEORDER PO
INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS
LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID Cross APPLY
  (SELECT 
   Distinct lim.EMPLOYEEID,lim.OperativeName,lim.Email,lim.MinLIMIT 
   FROM F_PURCHASEITEM PI
   INNER JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID CROSS APPLY
     (SELECT MinLIMIT, ELInner.EMPLOYEEID,CONVERT(varchar(50),E.FIRSTNAME)+' '+ CONVERT(varchar(50),E.LASTNAME)   As OperativeName,
     
       C.WORKEMAIL  as Email
      FROM F_EMPLOYEELIMITS ELInner
      INNER JOIN E__EMPLOYEE E ON ELInner.EMPLOYEEID = E.EMPLOYEEID
      INNER JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID 
       CROSS APPLY
        (SELECT MIN(LIMIT) MinLIMIT
         FROM F_EMPLOYEELIMITS
         WHERE LIMIT > PI.GROSSCOST
         AND EXPENDITUREID = ELInner.EXPENDITUREID) MinLim
      WHERE LIMIT = MinLIMIT
      AND ELInner.EXPENDITUREID = 28) lim
      OUTER APPLY
     (SELECT 1 AS IsAbsent
      FROM E_JOURNAL J CROSS APPLY
        (SELECT MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryID
         FROM E_ABSENCE
         WHERE JOURNALID = J.JOURNALID) MA
      INNER JOIN E_ABSENCE A ON MA.MaxAbsenceHistoryID = A.ABSENCEHISTORYID
      AND J.CURRENTITEMSTATUSID IN (1, 5)
      WHERE (getdate() BETWEEN A.STARTDATE AND ISNULL(A.RETURNDATE,getdate()))
        AND J.EMPLOYEEID = lim.EMPLOYEEID) [Absent]
            
  WHERE PI.ACTIVE = 1
     AND (PI.PISTATUS = 0 OR PI.PISTATUS = 1)
     AND ORDERID = PO.ORDERID
      AND ((EL.LIMIT >= MinLIMIT)
          OR (IsAbsent = 1
              AND EL.LIMIT > MinLIMIT))) emp
WHERE PO.ORDERID = @orderId
END
