
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =====================================================
-- EXEC [dbo].[FL_SaveSubcontractorAppointmentDetails]
	--@CustomerID = 2242,
	--@FaultId =741,
	--@ORGID =1270,
	--@Quantity = 1,
	--@ProblemDays =2,
	--@RecuringProblem =false,
	--@CommunalProblem =false,
	--@Notes ='Window is broken from 2 sides',
	--@PropertyId ='A010000098',
	--@Recharge= false,
	--@TempFaultID =1487,
	--@UserId =null
-- Author:		<Ahmed Mehmood>
-- Create date: <12/3/2013>
-- Description:	<Saves all appointment information>
-- Web Page: JobSheetSummarySubcontractor.aspx
-- =====================================================
CREATE PROCEDURE [dbo].[FL_SaveSubcontractorAppointmentDetails]
( 
	@CustomerID int,
	@FaultId int,
	@ORGID int,
	@Quantity int,
	@ProblemDays int,
	@RecuringProblem bit,
	@CommunalProblem bit,
	@Notes varchar(4000),
	@PropertyId nvarchar(20),
	@FaultTradeId int,
	@Recharge bit,
	@TempFaultID int,
	@UserId int,
	@result varchar(50) OUTPUT,
	@isRecall BIT,
	@OriginalFaultLogId int
)
AS
BEGIN
	-------- Declaring variables -------------------------------------------------------
	------------------------------------------------------------------------------------
		
	Declare @SelectStmt as nvarchar(3000)
	Declare @Sql as nvarchar(3200)
	Declare @StatusID as int
	Declare @IsReported as bit
	Declare @SubmitDate as DateTime
	Declare @FaultLogID as int
	Declare @JournalID as int
	Declare @JSNumberPrefix as varchar(10)
	Declare @ResponseTime as int
	Declare @Days as int
	Declare @ItemID as int
	Declare @ItemNatureID as int
	Declare @TenancyID as int
	Declare @DueDate as DateTime
	Declare @ItemActionId as int
	Declare @JobSheetNumber as varchar(50)

	-------- Populating variables ------------------------------------------------------
	------------------------------------------------------------------------------------
	
	if @ORGID= -1
		set @ORGID=NULL
		
	SET @JSNumberPrefix  = 'JS'
	SET @SubmitDate = GETDATE()
	
	SELECT @ResponseTime = ResponseTime, @Days = Days
	FROM FL_FAULT_PRIORITY 
	INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PriorityID = FL_FAULT.PriorityID 
	WHERE FaultID = @FaultId
	
	If @Days = 0
	SET @DueDate = DateAdd(HH,@ResponseTime,@SubmitDate)
	ELSE
	SET @DueDate = DateAdd(DD,@ResponseTime,@SubmitDate)
	
	SELECT @TenancyId = TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = @CustomerID AND ENDDATE != NULL
	SET @ItemId = (SELECT  ItemID FROM C_ITEM WHERE DESCRIPTION = 'Property')
	SET @ItemNatureId = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION = 'Reactive Repair')
	SET @IsReported = 1
	SET @StatusID = 2
	SET @ItemActionId = 2
	
	if @OriginalFaultLogId = -1
		set @OriginalFaultLogId = NULL
	

	-------- Inserting into Tables -----------------------------------------------------
	------------------------------------------------------------------------------------
	
	-- FL_FAULT_LOG TABLE
	
	INSERT INTO FL_FAULT_LOG
	(FaultID, CustomerID, Quantity , ProblemDays, RecuringProblem, CommunalProblem, Notes,
	ORGID, Recharge, PROPERTYID, StatusID, IsReported, SubmitDate, DueDate,FaultTradeId)
	VALUES
	(@FaultID, @CustomerID, @Quantity , @ProblemDays, @RecuringProblem, @CommunalProblem,
	@Notes, @ORGID, @Recharge, @PropertyId, @StatusID, @IsReported, @SubmitDate, @DueDate,@FaultTradeId)	
	
	SET @FaultLogID = @@IDENTITY
	
	-- UPDATE JOBSHEETNUMBER
	
	UPDATE FL_FAULT_LOG 
	SET JobSheetNumber = (@JSNumberPrefix + CONVERT(VARCHAR,@FaultLogID)) 
	WHERE FaultLogId = @FaultLogID
	
	SET @JobSheetNumber= (@JSNumberPrefix + CONVERT(VARCHAR,@FaultLogID)) 
	
	-- JOURNAL TABLE
	
	INSERT INTO FL_FAULT_JOURNAL
	(FaultLogID, CustomerID, PropertyID, TenancyID, ItemID, ItemNatureID, FaultStatusID, CreationDate, LastActionDate)
	VALUES
	(@FaultLogID, @CustomerID, @PropertyId, @TenancyID, @ItemID, @ItemNatureID, @StatusID, @SubmitDate, @SubmitDate)
		
	SET @JournalID = @@IDENTITY
	
	-- FL_FAULT_LOG_HISTORY
	
	-- 'Repair Reported'
	set @StatusID = 1
	INSERT INTO FL_FAULT_LOG_HISTORY
	(JournalID, FaultLogID, FaultStatusID, Notes, ORGID, ItemActionID, PROPERTYID, LastActionDate, LastActionUserID)
	VALUES
	(@JournalID, @FaultLogID, @StatusID, @Notes, @ORGID, @ItemActionId, @PropertyId, @SubmitDate,@UserId)
	
	-- 'Assign to Subcontractor'
	set @StatusID = 2
	INSERT INTO FL_FAULT_LOG_HISTORY
	(JournalID, FaultLogID, FaultStatusID, Notes, ORGID, ItemActionID, PROPERTYID, LastActionDate,LastActionUserID)
	VALUES
	(@JournalID, @FaultLogID, @StatusID, @Notes, @ORGID, @ItemActionId, @PropertyId, @SubmitDate,@UserId)
	
	-- UPDATE FL_TEMP_FAULT
	
	UPDATE FL_TEMP_FAULT
	SET isAppointmentConfirmed = 1, FaultLogId = @FaultLogID
	WHERE TempFaultID = @TempFaultID	
	
	If @isRecall = 1
	BEGIN
		INSERT INTO FL_FAULT_RECALL VALUES (@FaultLogID,@OriginalFaultLogId)
	END
	
	set @result=@JobSheetNumber
    
END

GO
