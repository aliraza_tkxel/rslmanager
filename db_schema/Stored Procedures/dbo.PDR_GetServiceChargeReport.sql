USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetServiceChargeReport]    Script Date: 23/10/2018 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PDR_GetServiceChargeReport') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeReport AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeReport]
		@filterText VARCHAR(200),
	-- Add the parameters for the stored procedure here
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ITEMID', 
		@sortOrder varchar (5) = 'DESC',
		@fiscalYear int,
		@totalCount int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
	
		@SelectClause NVARCHAR(MAX),
        @fromClause   NVARCHAR(MAX),
        @whereClause  NVARCHAR(MAX),	        
        @orderClause  NVARCHAR(MAX),	
        @mainSelectQuery NVARCHAR(MAX),        
        @rowNumberQuery NVARCHAR(MAX),
        @finalQuery NVARCHAR(MAX),
		@YStart varchar(100),
		@YEnd varchar(100),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria NVARCHAR(MAX),
		
		--variables for paging
        @offset int,
		@limit int
		
		select @YStart=YStart,@YEnd=YEnd From F_FISCALYEARS where YRange=@fiscalYear
		
		SET @searchCriteria = '(m.SchemeId > 0 OR m.BlockId > 0 ) '
		IF(@filterText != '' OR @filterText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND '+  @filterText
		END	
	
	  
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		SET @SelectClause = 'Select DISTINCT top ('+convert(nvarchar(10),@limit)+')
							 ISNULL(S.SCHEMENAME,''-'') as SCHEMENAME
							,ISNULL(B.BLOCKNAME,''-'') as BLOCKNAME
							,ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0) as InPropCount
							,ISNULL(I.ItemName,''-'') as ITEMNAME
							,''�'' + ISNULL(CONVERT(varchar, CAST(M.AnnualApportionment AS money), 1), ''0.00'') as BUDGET -- ANNUALAPPORTIONMENT

							,case								
							when  (M.INCSC is not null AND M.INCSC=1) then ''�'' + ISNULL(CONVERT(varchar,M.PropertyApportionment,1),''0.00'')
							else ''�'' +ISNULL(CONVERT(varchar,ISNULL(CAST(M.AnnualApportionment AS money), 0)/NULLIF((ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0)),0),1), ''0.00'') end as APPORTIONMENTBUDGET

							,case 
							when B.BLOCKNAME is null then ''�'' + ISNULL(CONVERT(varchar, CAST(scPOInfo.ActualTotal AS money), 1), ''0.00'')  
							when S.SCHEMENAME is null then ''�'' + ISNULL(CONVERT(varchar, CAST(blPOInfo.ActualTotal AS money), 1), ''0.00'') 							 
							END as ACTUALTOTAL	
																	
							,case 
							when B.BLOCKNAME is null then CONVERT(varchar,''�'' +ISNULL(CONVERT(varchar,ISNULL(CAST(scPOInfo.ActualTotal AS money), 0)/NULLIF((ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0)),0),1), ''0.00'') )
							when S.SCHEMENAME is null then CONVERT(varchar, ''�'' +ISNULL(CONVERT(varchar,ISNULL(CAST(blPOInfo.ActualTotal AS money), 0)/NULLIF((ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0)),0),1), ''0.00''))
							else CONVERT(varchar,''�0.00'')
							 END as APPORTIONMENTACTUAL
							,ISNULL(p.PropertyCount,'' '') as PROPERTYCOUNT
							,I.ItemID as ITEMID
							,(SELECT 
                                COUNT(*) AS Count
							    FROM   P__PROPERTY P
		                        where P.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ServiceChargeProperties P1 
								where P1.IsIncluded=1 AND P1.IsActive=1 AND P1.SCHEMEID=m.SchemeId AND P1.ItemID=I.ItemID
								GROUP BY P1.BLOCKID, P1.ItemId, P1.PROPERTYID)	and P.SCHEMEID=m.SchemeId
								) + 
								(SELECT 
                                COUNT(*) AS Count
							    FROM   P__PROPERTY P
		                        where P.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ServiceChargeProperties P1 
								where P1.IsIncluded=1 AND P1.IsActive=1 AND P1.BLOCKID=m.BLOCKID AND P1.ItemID=I.ItemID
								GROUP BY P1.BLOCKID, P1.ItemId, P1.PROPERTYID)	and P.BLOCKID=m.BLOCKID
								)
								 as ExPropCount
							,ISNULL(M.SCHEMEID,0) AS SCHEMEID
							,ISNULL(M.BLOCKID,0) AS BLOCKID							

							,CASE 
								WHEN B.BLOCKNAME IS NULL THEN  ISNULL( ''PO '' + CONVERT(VARCHAR,scPOInfo.PORef) , ''-'')  
								WHEN S.SCHEMENAME IS NULL THEN ISNULL( ''PO '' + CONVERT(VARCHAR,blPOInfo.PORef) , ''-'')					 
							END AS DETAILS	

							,case
							when B.BLOCKNAME is null then scPOInfo.PODate
							when S.SCHEMENAME is null then blPOInfo.PODate end AS DateCreated
							
							,CASE 
								WHEN B.BLOCKNAME IS NULL THEN  scPOInfo.PORef 
								WHEN S.SCHEMENAME IS NULL THEN blPOInfo.PORef					 
							END AS Ref	
							,CONVERT(BIT, 0) AS IsServiceChargePO 
							'
				
		SET @fromClause = CHAR(10) +' FROM PA_ITEM I 
									Left JOIN PDR_MSAT m ON m.ItemId = I.ItemID 	  									
									LEFT JOIN P_SCHEME S ON S.SCHEMEID = m.SchemeId
									LEFT JOIN P_BLOCK B ON B.BLOCKID= m.BlockId
									LEFT JOIN (SELECT COUNT(prop.BlockID) AS PropertyCount, prop.BLOCKID FROM P__PROPERTY prop GROUP BY prop.BLOCKID) P ON B.BLOCKID = P.BLOCKID
										LEFT JOIN (SELECT sum(FI.GROSSCOST) as ActualTotal,SI.SchemeId,ISC.ItemID ,ISC.ItemName,FP.PODate, Max(SI.PORef) PORef
											  FROM CM_ServiceItems SI
											  INNER JOIN P_SCHEME S ON SI.SchemeId=S.SCHEMEID 
											  INNER JOIN PA_ITEM ISC ON SI.ItemId = ISC.ItemID
											  INNER JOIN F_PURCHASEORDER FP on SI.PORef = FP.ORDERID
											  INNER JOIN F_PURCHASEITEM  FI on FP.ORDERID=FI.ORDERID
											  Where FP.PODATE between CONVERT(Datetime, ''' + @YStart +''', 120) and CONVERT(Datetime, ''' + @YEnd +''', 120)
											  group by SI.SCHEMEID,ISC.ItemID,ISC.ItemName,FP.PODATE
									) scPOInfo on (scPOInfo.SCHEMEID=m.SchemeId) and m.ItemId=scPOInfo.ItemID
									
									LEFT JOIN (SELECT sum(FI.GROSSCOST) as ActualTotal,SI.BLOCKID,IBL.ItemID ,IBL.ItemName ,FP.PODate, Max(SI.PORef) PORef
												  FROM CM_ServiceItems SI
												  INNER JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID
												  INNER JOIN PA_ITEM IBL ON SI.ItemId = IBL.ItemID
												  INNER JOIN F_PURCHASEORDER FP on SI.PORef = FP.ORDERID
												  INNER JOIN F_PURCHASEITEM  FI on FP.ORDERID=FI.ORDERID
												  Where FP.PODATE between CONVERT(Datetime, ''' + @YStart +''', 120) and CONVERT(Datetime, ''' + @YEnd +''', 120)
												  group by SI.BLOCKID,IBL.ItemID,IBL.ItemName,FP.PODATE) blPOInfo on (blPOInfo.BLOCKID= m.BlockId) and m.ItemId=blPOInfo.ItemID
									
									LEFT JOIN (SELECT DISTINCT COUNT(*) AS Count, SCHEMEID, itemid
												FROM PDR_ServiceChargeProperties
												WHERE isincluded=0 and IsActive=1
												GROUP BY SCHEMEID, itemid) exProp on (exProp.SCHEMEID=m.SchemeId) and m.ItemId=exProp.itemid
									LEFT JOIN (SELECT COUNT(*) AS Count, BLOCKID, itemid
												FROM PDR_ServiceChargeProperties
												WHERE isincluded=0 and IsActive=1
												and schemeid is null
												GROUP BY BLOCKID, itemid) exProp1 on (exProp1.BLOCKID=m.BLOCKID) and m.ItemId=exProp1.itemid 
									LEFT JOIN (SELECT DISTINCT COUNT(*) AS Count, SCHEMEID, itemid
												FROM PDR_ServiceChargeProperties
												WHERE isincluded=1 and IsActive=1
												GROUP BY SCHEMEID, itemid) inProp on (inProp.SCHEMEID=m.SchemeId) and m.ItemId=inProp.itemid
									LEFT JOIN (SELECT COUNT(*) AS Count, BLOCKID, itemid
												FROM PDR_ServiceChargeProperties
												WHERE isincluded=1 and IsActive=1
												and schemeid is null
												GROUP BY BLOCKID, itemid) inProp1 on (inProp1.BLOCKID=m.BLOCKID) and m.ItemId=inProp1.itemid '
								   
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		SET @whereClause =	CHAR(10) + 'WHERE 1=1 AND' + CHAR(10) + @searchCriteria 
		



		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		
		EXEC (@finalQuery)
		--print(@finalQuery)
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount NVARCHAR(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
