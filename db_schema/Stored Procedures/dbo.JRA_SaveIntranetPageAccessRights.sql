
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit
		
--DECLARE @pageIdList AS TEAMJOBROLE_INTRANET_PAGE_ACCESSRIGHTS;
--INSERT INTO @pageIdList(JobRoleTeamId,PageId)
--VALUES (1,1)

--EXEC	@return_value = [dbo].[JRA_SaveIntranetPageAccessRights]
--		@teamJobRoleId = 21,
--		@userId =615,
--		@pageIdList = @pageIdList,
--		@isSaved = @isSaved OUTPUT

--SELECT	@isSaved as N'@isSaved'

--SELECT	'Return Value' = @return_value
-- Author:		Ahmed Mehmood
-- Create date: <3/1/2014>
-- Last Modified: <3/1/2014>
-- Description:	<Save Intranet Page Access Rights. >
-- Web Page: JobRoles.aspx

-- =============================================
CREATE PROCEDURE [dbo].[JRA_SaveIntranetPageAccessRights]
@teamJobRoleId int
,@userId int
,@pageIdList as TEAMJOBROLE_INTRANET_PAGE_ACCESSRIGHTS readonly
,@isSaved bit out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

--====================================================
--			AC_PAGES_ACCESS INSERTION
--====================================================

	-- Create temprary tables: #ins and #del
	SELECT * INTO #ins FROM I_PAGE_TEAMJOBROLE WHERE 1 = 0
	SELECT * INTO #del FROM I_PAGE_TEAMJOBROLE WHERE 1 = 0
	
	-- Create temprary tables: #ins and #del
    DELETE FROM I_PAGE_TEAMJOBROLE
    OUTPUT DELETED.PageId,DELETED.TeamJobRoleId INTO #del
	WHERE I_PAGE_TEAMJOBROLE.TeamJobRoleId = @teamJobRoleId     
	AND PageId NOT IN (SELECT PageId FROM @pageIdList)

	-- Insert any new record, and insert in to temp table #ins
    INSERT INTO I_PAGE_TEAMJOBROLE(PageId,TeamJobRoleId)
    OUTPUT INSERTED.PageId,INSERTED.TeamJobRoleId INTO #ins
	SELECT PageId,JobRoleTeamId 
	FROM @pageIdList
	WHERE PageId NOT IN (SELECT PageId FROM I_PAGE_TEAMJOBROLE WHERE TeamJobRoleId = @teamJobRoleId)

------------------------------------------------------
-- Insertion in JobRoleAuditHistory Table
------------------------------------------------------
    Declare @jobRoleActionId int

    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Access Rights Amended - Intranet'
    
    DECLARE @pageId INT = NULL	
		
	-- Note deleted records in history
	-- Select first record
	SELECT @pageId = MIN(PageId) FROM #del	
	WHILE @pageId IS NOT NULL
	BEGIN
		INSERT INTO E_JOBROLEAUDITHISTORY
			   ([JobRoleActionId]
			   ,[JobRoleTeamId]
			   ,[Detail]
			   ,[CreatedBy]
			   ,[CreatedDate])
		 VALUES
			   (@jobRoleActionId
			   ,@teamJobRoleId
			   ,(SELECT ISNULL(M.MenuTitle+'>','') + P.Name  
					FROM I_PAGE P LEFT OUTER JOIN I_MENU M ON P.MenuID = M.MenuID
					WHERE P.PageID = @pageId ) + ', removed'
			   ,@userId
			   ,GETDATE())

		-- Select next record
		SELECT @pageId = MIN(PageId) FROM #del WHERE PageId > @pageId
	END
	
	-- Note inserted records in history
	-- Select first record
	SET @pageId = NULL
	SELECT @pageId = MIN(PageId) FROM #ins	
	WHILE @pageId IS NOT NULL
	BEGIN
		INSERT INTO E_JOBROLEAUDITHISTORY
			   ([JobRoleActionId]
			   ,[JobRoleTeamId]
			   ,[Detail]
			   ,[CreatedBy]
			   ,[CreatedDate])
		 VALUES
			   (@jobRoleActionId
			   ,@teamJobRoleId
			   ,(SELECT ISNULL(M.MenuTitle+'>','') + P.Name  
					FROM I_PAGE P LEFT OUTER JOIN I_MENU M ON P.MenuID = M.MenuID
					WHERE P.PageID = @pageId ) + ', added'
			   ,@userId
			   ,GETDATE())

		-- Select next record
		SELECT @pageId = MIN(PageId) FROM #ins WHERE PageId > @pageId
	END
	
	--Remove the temp tables
	IF OBJECT_ID('tempdb..#ins') IS NOT NULL DROP TABLE #ins
	IF OBJECT_ID('tempdb..#del') IS NOT NULL DROP TABLE #del    
---------------------------------------------------------------

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END
GO
