USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[GET_VALIDATION_PERIOD_AUTOUPDATE]    Script Date: 27/07/2018 00:24:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GET_VALIDATION_PERIOD_AUTOUPDATE] (@LOCKDOWNID INT, @CompanyId INT = 1)

AS
BEGIN
 
DECLARE @ExtendedDate SMALLDATETIME  
DECLARE @NewYear int 
DECLARE @NewMonth INT
 
SELECT @ExtendedDate = CAST(CAST(ExtendedYear AS VARCHAR(4)) +  RIGHT('0' + CAST(ExtendedMonth AS VARCHAR(2)), 2) + REPLACE(RIGHT('0' + CAST(ExtendedDays AS VARCHAR(2)), 2) ,'00','01') AS SMALLDATETIME) 
FROM AC_FINANCIALLOCKDOWN WHERE LOCKDOWNID = @LOCKDOWNID AND CompanyId = @CompanyId
 
--print @ExtendedDate
--print DateDiff(month,dateAdd(MONTH, 1, @ExtendedDate),GETDATE()) 
--print DATEPART(DAY,@ExtendedDate)

if (DateDiff(month,dateAdd(MONTH, 1, @ExtendedDate),GETDATE()) >= 1 or ( DateDiff(month,dateAdd(MONTH, 1, @ExtendedDate),GETDATE()) = 0 and DATEPART(DAY,@ExtendedDate) < DATEPART(DAY,GETDATE())))
 BEGIN 
	IF (DATEPART(DAY,GETDATE()) < DATEPART(DAY,@ExtendedDate) and DATEPART(DAY,@ExtendedDate) < 23)
		BEGIN
			SET @NewYear = YEAR(DATEADD(MONTH,-1,Getdate()))
			SET @NewMonth = MONTH(DATEADD(MONTH,-1,Getdate()))	 
			UPDATE AC_FINANCIALLOCKDOWN SET ExtendedYear = @NewYear , ExtendedMonth = @NewMonth WHERE LOCKDOWNID = @LOCKDOWNID and companyid = @CompanyId
		end
	ELSE
		BEGIN
			SET @NewYear = YEAR(Getdate())
			SET @NewMonth = MONTH(Getdate())
			UPDATE AC_FINANCIALLOCKDOWN SET ExtendedYear = @NewYear , ExtendedMonth = @NewMonth WHERE LOCKDOWNID = @LOCKDOWNID and companyid = @CompanyId
		end
 END

 END

 