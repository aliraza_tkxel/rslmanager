USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetChecksToBeArranged]    Script Date: 06/03/2015 14:36:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get First Inspections To Be Arranged List 
    Author: Ali Raza
    Creation Date: May-15-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         May-15-2015      Ali Raza         Get First Inspections To Be Arranged List 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetGasElectricChecksToBeArranged]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
IF OBJECT_ID('dbo.[V_GetGasElectricChecksToBeArranged]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_GetGasElectricChecksToBeArranged] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[V_GetGasElectricChecksToBeArranged]
-- Add the parameters for the stored procedure here
		@patch int = 0,
		@searchText VARCHAR(200)='',
		@checksRequired int = 1,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
         @filterCriteria varchar(200)='',
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT  @ArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To Be Arranged'
		SET @checksRequiredType='Gas/Electric Check'
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		IF (@checksRequired = 1 AND @getOnlyCount=0)
			BEGIN
				--SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (rec.IsGasCheckRequired=1 OR N.IsGasCheck=1)'
				SET @checksRequiredType='Gas Check'
				SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Gas Check'
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)
			END
		ELSE IF (@checksRequired = 2 AND @getOnlyCount=0)
			BEGIN
				--SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (rec.IsElectricCheckRequired=1 OR N.IsElectricCheck=1)'
				SET @checksRequiredType='Electric Check'
				SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Electric Check'
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)
			END
			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ArrangedStatusId)+''
		IF(@getOnlyCount=1)
			BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  PDR_MSATType.MSATTypeName IN (  ''Void Electric Check'', ''Void Gas Check '')'
			END
			
	--=================================	Filter Criteria================================
		IF(@patch > 0 )
		BEGIN
		SET @filterCriteria = @filterCriteria + 'AND D.PATCHID = ' + CONVERT (VARCHAR,@patch) 
		END
	--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address
		,	P.TOWNCITY,P.COUNTY,	ISNULL(P.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When C.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(+'' ''+C.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),TerminationDate, 103) as Termination
		,Case When PDR_MSAT.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) Else	CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) END as Relet
		,PDR_MSAT.TenancyId	,'''+@checksRequiredType+''' AS Type
		, TerminationDate,PDR_MSAT.ReletDate as reletDate
		'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'From  PDR_JOURNAL 
			INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
			INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
			INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
			INNER JOIN	P__PROPERTY P ON PDR_MSAT.PropertyId = P.PROPERTYID	
			INNER JOIN PDR_DEVELOPMENT D ON P.DevelopmentId = D.DevelopmentId
			INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
			INNER JOIN C__CUSTOMER C ON PDR_MSAT.CustomerId = C.CUSTOMERID
			INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
			LEFT JOIN G_TITLE ON C.TITLE=G_TITLE.TITLEID
			
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TerminationDate' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'reletDate' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		print(@searchCriteria)
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			
			EXEC (@finalQuery)
		END
		print(@finalQuery)
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT PDR_JOURNAL.JOURNALID) ' + @fromClause + @whereClause
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
