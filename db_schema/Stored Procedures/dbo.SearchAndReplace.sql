SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC dbo.SearchAndReplace 
(
     @FindString    NVARCHAR(100)
    ,@ReplaceString NVARCHAR(100)
)
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @TextPointer VARBINARY(16) 
    DECLARE @DeleteLength INT 
    DECLARE @OffSet INT 

    SELECT @TextPointer = TEXTPTR([TEMPLETTER])
      FROM [C_STANDARDLETTERS]

    SET @DeleteLength = LEN(@FindString) 
    SET @OffSet = 0
    SET @FindString = '%' + @FindString + '%'

    WHILE (SELECT COUNT(*)
             FROM [C_STANDARDLETTERS]
            WHERE PATINDEX(@FindString, [TEMPLETTER]) <> 0) > 0
    BEGIN 
        SELECT @OffSet = PATINDEX(@FindString, [TEMPLETTER]) - 1
          FROM [C_STANDARDLETTERS]
         WHERE PATINDEX(@FindString, [TEMPLETTER]) <> 0

        UPDATETEXT [C_STANDARDLETTERS].[TEMPLETTER]
            @TextPointer
            @OffSet
            @DeleteLength
            @ReplaceString
    END

    SET NOCOUNT OFF
END
GO
