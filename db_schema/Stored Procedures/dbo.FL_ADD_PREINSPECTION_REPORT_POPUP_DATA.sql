SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE dbo.FL_ADD_PREINSPECTION_REPORT_POPUP_DATA
	/* ===========================================================================
 '   NAME:           FL_ADD_PREINSPECTION_REPORT_POPUP_DATA
 '   DATE CREATED:   10-04-2009
 '   CREATED BY:     Tahir GUL 
 '   CREATED FOR:    Tenants Online Reported Faults
 '   PURPOSE:        To save PreInspection Report Popup Data 
 '   IN:             @userId
 '   IN:             @orgId
 '   IN:             @notes
 '   IN:             @inspectionDate
 '   IN:             @dueDate
 '   IN:             @netCost
 '   IN:             @approved
 '   IN:             @faultLogId
 '
 '   OUT:            @result
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
  (
  
	@userId INT,
	@orgId INT = null,
	@recharge INT,
	@notes nvarchar(300),
	@inspectionDate DATETIME,
	@dueDate DATETIME = null,
	@netCost float,
	@approved INT,	
	@faultLogId INT,
	@actionUserId INT,
	@result INT OUTPUT	
  ) 
AS

DECLARE 
		@JournalId int,
        @ScopeId int,
		@IsApproved bit,
		@itemId int,
		@faultStatus int,
		@AssginedToContractorStatusId INT,
		@CancelledStatusId INT,
		@HistoryId INT
BEGIN 	
BEGIN TRAN
	--Get and set the repair assigned to contractor status
	SET @AssginedToContractorStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Assigned To Contractor')


	--Get and set the works completed status
	SET @CancelledStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Cancelled Fault')		
	

	
	UPDATE FL_FAULT_PREINSPECTIONINFO
	SET 
		USERID =@userId ,
		INSPECTIONDATE = @inspectionDate,
		NETCOST = @netCost,
		DUEDATE =@dueDate ,
		NOTES = @notes,
		ORGID = @orgId,
		Approved =@approved, 
		Recharge = @recharge
		WHERE faultlogid = @faultLogId
	
	

	SELECT @result=PREINSPECTONID
	FROM FL_FAULT_PREINSPECTIONINFO
	WHERE PREINSPECTONID = @@IDENTITY

	IF(@orgId IS NOT NULL)
	BEGIN
		UPDATE FL_FAULT_LOG
		SET 	
		ORGID = @orgId	    	
		WHERE FaultLogID = @faultLogId
	END

	IF(@approved = 0)
	BEGIN
		set @faultStatus=@CancelledStatusId
		UPDATE FL_FAULT_LOG
			SET 
			StatusId = @CancelledStatusId
			WHERE faultLogId = @faultLogid
		UPDATE FL_FAULT_JOURNAL
			SET 
			FaultStatusId = @CancelledStatusId
			WHERE faultLogId = @faultLogid

	END
	ELSE
	BEGIN
		set @faultStatus=@AssginedToContractorStatusId
		UPDATE FL_FAULT_LOG
			SET 
			StatusId = @AssginedToContractorStatusId,
			USERID =@userId 
			WHERE faultLogId = @faultLogid

		UPDATE FL_FAULT_JOURNAL
			SET 
			FaultStatusId = @AssginedToContractorStatusId
			WHERE faultLogId = @faultLogid
	END


	SELECT @ScopeID=SCOPEID FROM S_SCOPE WHERE ORGID=@OrgId AND AREAOFWORK= (SELECT AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair'))
	
	UPDATE FL_FAULT_LOG
	SET 	
	UserId = @actionUserId,
	DueDate = @dueDate	    	
	WHERE FaultLogID = @faultLogId

--SELECT  @IsApproved= APPROVED, @Notes = Notes  FROM FL_FAULT_PREINSPECTIONINFO WHERE faultlogid = @FaultLogId 

--IF @IsApproved = 1 

--Select @faultStatus = StatusId FROM FL_FAULT_LOG 
--WHERE faultLogId = @faultLogid

	Select @JournalId = JournalId, @itemId = ItemId FROM FL_FAULT_JOURNAL 
	WHERE faultLogid = @faultLogid


	INSERT INTO FL_FAULT_LOG_HISTORY(JournalId,FaultStatusId,ItemActionId, LastActionDate, LastActionUserID, FaultLogId,OrgId,ScopeId,Title,Notes)
	VALUES(@JournalId,  @faultStatus , @itemId, GETDATE(), Null, @FaultLogId, @orgId,@SCOPEID, '', @Notes )
	
	--Get the latest History ID
	SELECT @HistoryId = FaultLogHistoryId 
	FROM FL_FAULT_LOG_HISTORY
	WHERE FaultLogHistoryId  = @@Identity
	
	IF @HistoryId <0 
	Begin
		Rollback Transaction
		SET @Result=-1
		Print 'Unable to update the record'
	End
	ELSE
	Begin					
		SET @Result=1
		COMMIT TRAN
	END
END
GO
