SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_E_BENTYPE_GetBenifitLookup
/* ===========================================================================
 '   NAME:           TO_E_BENTYPE_GetBenifitLookup
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get benifit types records from E_BENTYPE table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT BenefitTypeID AS id,BenefitTypeDesc AS val
	FROM E_BENTYPE
	ORDER BY BenefitTypeDesc ASC

GO
