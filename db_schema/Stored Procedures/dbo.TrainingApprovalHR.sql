USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[TrainingApprovalHR]    Script Date: 13/06/2018 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.TrainingApprovalHR') IS NULL 
	EXEC('CREATE PROCEDURE dbo.TrainingApprovalHR AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Saud Ahmed
-- Create date:      13/06/2018
-- Description:      Calculate alert count of Training Approval HR (submitted Training that cost is > 1000 and professional Qual. and require Approval from HR.) on whiteboard  
-- exec  TrainingApprovalHR 423
-- =============================================

ALTER PROCEDURE  [dbo].[TrainingApprovalHR]
	@USERID INT 
AS
BEGIN
	
	SET NOCOUNT ON;
   
SELECT PendingInterest as LeavesCount From (
	SELECT count(*) AS PendingInterest
	from E_EmployeeTrainings EET 
		join E_EmployeeTrainingStatus ES on EET.Status = ES.StatusId
        join E__EMPLOYEE E on EET.EmployeeId = E.EMPLOYEEID
        join E_JOBDETAILS J on E.EMPLOYEEID = J.EMPLOYEEID
        left join E_TEAM T on J.TEAM = T.TEAMID 
        left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
	where ES.Title = 'Submitted'
		and (EET.ProfessionalQualification = 1 OR EET.TotalCost > 1000) 
		and EET.Active = 1
		and J.ACTIVE = 1
)  totalResult




END
