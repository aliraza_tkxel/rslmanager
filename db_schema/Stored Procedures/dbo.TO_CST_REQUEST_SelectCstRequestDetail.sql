SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE  PROCEDURE [dbo].[TO_CST_REQUEST_SelectCstRequestDetail]

/* ===========================================================================
 '   NAME:          TO_CST_REQUEST_SelectCstRequestDetail
 '   DATE CREATED:  July 2, 2008
 '   CREATED BY:    Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select single TO_PARKING_SPACE record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
		TO_ENQUIRY_LOG.Description, 
            	TO_CST_REQUEST.RequestNatureId AS CATEGORYID
                        
	FROM
		TO_ENQUIRY_LOG INNER JOIN TO_CST_REQUEST
		ON TO_ENQUIRY_LOG.EnquiryLogID =TO_CST_REQUEST.EnquiryLogID	       	       
			
	WHERE
	
		TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID
	     	

GO
