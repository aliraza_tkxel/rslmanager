SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_KEYMESSAGES_INSERT]
(
	@KeyMessageTitle varchar(50),
	@KeyMessageText varchar(150)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [I_KEYMESSAGES] ([KeyMessageTitle], [KeyMessageText]) VALUES (@KeyMessageTitle, @KeyMessageText);
	
SELECT KeyMessageID, KeyMessageTitle, KeyMessageText FROM I_KEYMESSAGES WHERE (KeyMessageID = SCOPE_IDENTITY())
GO
