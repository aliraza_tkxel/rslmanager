SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO













CREATE PROCEDURE dbo.FL_GETPATCHLOOKUP
/* ===========================================================================
 '   NAME:          FL_GETPATCHLOOKUP
 '   DATE CREATED:  23 Dec. 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get patch  E_PATCH table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT PATCHID AS id,LOCATION AS val
	FROM E_PATCH
	ORDER BY LOCATION ASC










GO
