/* =================================================================================    
    Page Description:    Get the detail of tree leaf node
 
    Author: Ali Raza
    Creation Date: jan-2-2015  

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         jan-2-2015      Ali Raza           Get the detail of tree leaf node
    
    Execution Command:
    
    Exec PDR_GetItemDetail
    @itemId=28,
    @schemeId = 1
  =================================================================================*/
  IF OBJECT_ID('dbo.PDR_GetItemDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetItemDetail AS SET NOCOUNT ON;') 
GO
      
      
      
ALTER PROCEDURE [dbo].[PDR_GetItemDetail]            
 -- Add the parameters for the stored procedure here            
 --@areaId int,              
 @itemId int,  
 @schemeId int=null, 
 @blockId int=null,
 @ChildAttributeMappingId int =null           
              
AS            
BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from            
 -- interfering with SELECT statements.            
 SET NOCOUNT ON;            
--=================================================================            
  --get Parameters            
 select ItemParamID,PA_ITEM.ItemID,PA_PARAMETER.ParameterID,ParameterName,DataType,ControlType,IsDate,ParameterSorder,         
 (Select Top 1 convert(varchar(10),UPDATEDON,103 )as UPDATEDON  FROM PA_PROPERTY_ATTRIBUTES        
INNER JOIN PA_ITEM_PARAMETER on PA_ITEM_PARAMETER.ItemParamID = PA_PROPERTY_ATTRIBUTES.ItemParamId         
WHERE (PA_ITEM_PARAMETER.SchemeId = @schemeId OR @schemeId IS NULL)AND (PA_ITEM_PARAMETER.BlockId = @blockId OR @blockId IS NULL)AND ItemId = @itemId order by UPDATEDON DESC ) as LastInspected ,      
PA_PARAMETER.ShowInApp as  ShowInApp      
  from PA_ITEM_PARAMETER             
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID             
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId             
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID             
 where PA_ITEM.ItemID=@itemId and PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1     
-- and PA_ITEM_PARAMETER.ParameterValueId=(Select ValueID from PA_PARAMETER_VALUE where ValueDetail='Mains Gas' And ParameterID=65)                 
 ORDER BY ParameterSorder  --AND PA_AREA.AreaID =@areaId            
             
--================================================================           
---get parameter values            
--================================================================            
 SELECT ValueID, ParameterID, ValueDetail, Sorder           
 FROM PA_PARAMETER_VALUE where ParameterID IN (           
 select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER             
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID             
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId             
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID             
 where PA_ITEM.ItemID=@itemId and PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 ) and PA_PARAMETER_VALUE.IsActive = 1            
 AND PA_PARAMETER_VALUE.IsAlterNativeHeating is NULL
 Order BY SOrder --AND PA_AREA.AreaID =@areaId)             
             
 --=================================================================            
 ---get pre inserted values             
 --=================================================================            
SELECT PROPERTYID,ATTRIBUTEID,ITEMPARAMID,PARAMETERVALUE,VALUEID,UPDATEDON,UPDATEDBY ,IsCheckBoxSelected,HeatingMappingId FROM PA_PROPERTY_ATTRIBUTES            
  WHERE (SchemeId = @schemeId OR @schemeId IS NULL)AND (BlockId = @blockId OR @blockId IS NULL) AND ITEMPARAMID IN  ( select ItemParamID from PA_ITEM_PARAMETER             
 INNER JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID             
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId             
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID             
where PA_ITEM.ItemID=@itemId and PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 )
ORDER BY UPDATEDON DESC --AND PA_AREA.AreaID =@areaId)              
             
 --=================================================================            
  ---get Last Replaced Dates            
  --=================================================================            
  SELECT [SID],PROPERTYID,ItemID,convert(varchar(20),LastDone,103)AS LastDone,convert(varchar(20),DueDate,103)AS DueDate,PA_PARAMETER.ParameterId,ParameterName,PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID as ComponentId,HeatingMappingId  
   from PA_PROPERTY_ITEM_DATES        
    
   left JOIN PA_PARAMETER On PA_PARAMETER.ParameterID = PA_PROPERTY_ITEM_DATES.ParameterId            
   where (SchemeId = @schemeId OR @schemeId IS NULL) AND (BlockId = @blockId OR @blockId IS NULL) AND ItemId =@itemId
   ORDER BY UPDATEDON DESC      
--=================================================================            
  ---get Maintenance,Servicing and testing         
SELECT 
MSATId,PDR_MSAT.ItemId,PDR_MSAT.CycleTypeId, IsRequired, convert(varchar(20),LastDate,103)AS LastDate,Cycle,convert(varchar(20),NextDate,103)AS NextDate,AnnualApportionment,CycleType, PDR_MSAT.MSATTypeId,  MSATTypeName, PDR_MSAT.CycleCost

from PDR_MSAT
left JOIN PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId And PDR_CycleType.IsActive = 1
INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId and PDR_MSATType.IsActive = 1  
--INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId
INNER JOIN PA_ITEM ON PA_ITEM.ItemID= PDR_MSAT.ItemId
where (PDR_MSAT.SchemeId = @schemeId OR @schemeId IS NULL)AND (PDR_MSAT.BlockId = @blockId OR @blockId IS NULL) AND PA_ITEM.ItemID =@itemId 
And ( (PDR_MSAT.ChildAttributeMappingId=@ChildAttributeMappingId AND @ChildAttributeMappingId > 0)   OR PDR_MSAT.ChildAttributeMappingId IS NULL)

 --=================================================================            
 ---Get planned_component mapping        
 SELECT PLANNED_COMPONENT_ITEM.COMPONENTID, PLANNED_COMPONENT_ITEM.ITEMID,PLANNED_COMPONENT_ITEM.PARAMETERID,PLANNED_COMPONENT_ITEM.VALUEID,        
CASE WHEN PLANNED_COMPONENT.FREQUENCY  = 'yrs' THEN        
   convert(INT,PLANNED_COMPONENT.CYCLE )* 12 ELSE        
   convert(INT,PLANNED_COMPONENT.CYCLE ) END AS CYCLE,        
   CASE WHEN PLANNED_COMPONENT.FREQUENCY  = 'yrs' THEN        
   convert(varchar(10),PLANNED_COMPONENT.CYCLE )+' Years'  ELSE        
   convert(varchar(10),PLANNED_COMPONENT.CYCLE )+' Months' END AS LIFECYCLE,        
   PLANNED_COMPONENT.ISACCOUNTING,         
PLANNED_COMPONENT_ITEM.SubParameter,PLANNED_COMPONENT_ITEM.SubValue        
   FROM PLANNED_COMPONENT_ITEM         
INNER JOIN PLANNED_COMPONENT ON PLANNED_COMPONENT.COMPONENTID = PLANNED_COMPONENT_ITEM.COMPONENTID         
INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PLANNED_COMPONENT_ITEM.ITEMID         
WHERE PA_ITEM.ItemID =@itemId AND PLANNED_COMPONENT_ITEM.isActive=1        
--================================================================= 
 --=================================================================            
 ---Get Cyclical service Item detail      
 Select ContractorId,Convert(NVARCHAR(10),ContractCommencement,103)as Commencement,ContractPeriod,ContractPeriodType,Cycle,CycleType,CycleValue,PORef,CM_ServiceItems.StatusId,CM_ServiceItems.BFSCarryingOutWork,VAT,TotalValue,
 '<a href="../../../../../Suppliers/SRM.asp?CompanyID='+CONVERT(varchar(10),Contractor.ORGID)+'" target="_blank" >'+Contractor.ContractorName + '</a> (Start:'+Convert(NVARCHAR(10),Contractor.STARTDATE,103)+' End:'+Convert(NVARCHAR(10),
 Contractor.RENEWALDATE,103)+')'as CurrentContractor, CustomCycleFrequency,CustomCycleOccurance,CustomCycleType
  from CM_ServiceItems
 Outer APPLY(
 SELECT DISTINCT  O.ORGID,S.STARTDATE,S.RENEWALDATE,NAME AS ContractorName
 FROM S_ORGANISATION O  
 INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID  
 INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID   
 INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID   
 left JOIN (
SELECT Top(1)ch.ContractorId,ch.ServiceItemId,ch.ItemId
  FROM CM_ServiceItems_History ch where 
  ch.ItemId=@ItemId AND (ch.SchemeId= @schemeId Or ch.SchemeId is null) And (ch.BlockId=@blockId or (ch.BlockId is null And @blockId is null)) 
)h on O.ORGID = h.ContractorId
WHERE  AoW.DESCRIPTION = 'Cyclical Services' AND O.ORGID=CM_ServiceItems.ContractorId
 AND (SS.BLOCKID = @blockId OR SS.SCHEMEID = @schemeId)
 AND h.ItemId=@ItemId
 )Contractor
 
 where CM_ServiceItems.ItemId=@ItemId AND (CM_ServiceItems.SchemeId= @schemeId Or CM_ServiceItems.SchemeId is null) And (CM_ServiceItems.BlockId=@blockId or (CM_ServiceItems.BlockId is null And @blockId is null)) And 
  ( (ChildAttributeMappingId=@ChildAttributeMappingId AND @ChildAttributeMappingId > 0)   OR ChildAttributeMappingId IS NULL)

 --=================================================================            
       
END 