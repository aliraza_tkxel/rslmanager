USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetAppointmentTypes]    Script Date: 10/05/2016 17:53:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <3/10/2016>
-- Description:	<Get all type of appointments>
-- =============================================
IF OBJECT_ID('dbo.P_GetAppointmentTypes') IS NULL 
	EXEC('CREATE PROCEDURE dbo.P_GetAppointmentTypes AS SET NOCOUNT ON;') 
GO  
ALTER PROCEDURE [dbo].[P_GetAppointmentTypes]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *FROM P_AppointmentType
END
