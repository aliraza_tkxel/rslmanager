
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC FL_GetNewlyCreatedAppointmentInfo @appointmentId=34732
-- Author:		Aqib Javed
-- Create date: <13/03/2013>
-- Description:	<Provided information of Scheduled Appointments of Engineers on Weekly basis>
-- Webpage : SchedulingCalendar.aspx

-- =============================================
CREATE PROCEDURE [dbo].[FL_GetNewlyCreatedAppointmentInfo]
@appointmentId int	
AS
BEGIN
	Select distinct 
	FL_FAULT_LOG.FaultLogID,
	FL_FAULT.FaultID,
	FL_FAULT_LOG.JobSheetNumber,
	FL_AREA.AreaName,
	FL_FAULT.[Description],
	FL_FAULT.Duration,
	FL_FAULT_LOG.ProblemDays,
	FL_FAULT_LOG.RecuringProblem ,
	FL_FAULT_LOG.CommunalProblem,
	DATENAME(D,FL_FAULT_LOG.DueDate)+ ' ' +  CONVERT(varchar(3), FL_FAULT_LOG.DueDate, 100) as DueDate,
	FL_FAULT_LOG.Notes,
	FL_FAULT.IsGasSafe,
	FL_FAULT.IsOftec,
	FL_FAULT_LOG.Quantity,
	FL_FAULT_LOG.Recharge,
	G_TRADE.[Description]  As TradeName,
	'' As OrgId,
	'' As IsAppointmentConfirmed,
	'' As tempFaultId,
	FL_FAULT_TRADE.FaultTradeId,
	(CASE WHEN FL_Fault_Priority.Days = 1 THEN
	CONVERT(nvarchar(50), FL_Fault_Priority.ResponseTime) + ' days' ELSE
	CONVERT(nvarchar(50), FL_Fault_Priority.ResponseTime) + ' hours' END) as Response   
	from FL_CO_APPOINTMENT 
	INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId 
	INNER JOIN	FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
	INNER JOIN	FL_FAULT ON FL_FAULT_LOG.FaultId = FL_FAULT.FaultID 
	INNER JOIN	FL_AREA ON FL_FAULT.AREAID = FL_AREA.AreaID 
	INNER JOIN FL_Fault_Trade ON FL_FAULT_LOG.FaultTradeId  = FL_Fault_Trade.FaultTradeId 
	INNER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
	INNER JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
Where FL_FAULT_APPOINTMENT.AppointmentId =@appointmentId
      
END
GO
