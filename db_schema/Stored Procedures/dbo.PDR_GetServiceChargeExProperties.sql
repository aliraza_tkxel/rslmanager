USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetServiceChargeExProperties') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeExProperties AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_GetServiceChargeExProperties] 
	@schemeId	 INT=NULL, 
	@blockId	 INT=null,
	@itemid		 INT,
	@childItemId INT = NULL
AS
BEGIN
	
	IF(@blockId > 0)
	BEGIN
		 SELECT ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
		 FROM   P__PROPERTY P
		 WHERE	P.BLOCKID = @blockId 
				AND P.PROPERTYID NOT IN (	SELECT	DISTINCT P1.PROPERTYID AS PROPERTYID
											FROM	dbo.PDR_ServiceChargeProperties P1 
											WHERE	P1.IsIncluded=1 
												AND P1.IsActive=1 
												AND P1.BLOCKID= @blockId 
												AND P1.ItemID=@itemid
												AND ((@childItemId IS NULL AND 1=1) OR (@childItemId IS NOT NULL AND P1.ChildAttributeMappingId = @childItemId)))
		ORDER BY CAST(SUBSTRING(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,''), 1,CASE	WHEN PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) > 0 THEN 
															PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) - 1 
														ELSE LEN(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) 
														END
														) AS INT) ASC, ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') ASC
	END
	ELSE
	BEGIN
		SELECT ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
		FROM   P__PROPERTY P
		WHERE  P.SCHEMEID = @schemeId 
			   AND  P.PROPERTYID NOT IN (SELECT DISTINCT P1.PROPERTYID AS PROPERTYID
										 FROM	dbo.PDR_ServiceChargeProperties P1 
										 WHERE  P1.IsIncluded=1 
												AND P1.IsActive=1 
												AND P1.SCHEMEID=@schemeId 
												AND P1.ItemID=@itemid
												AND ((@childItemId IS NULL AND 1=1) OR (@childItemId IS NOT NULL AND P1.ChildAttributeMappingId = @childItemId)))
		ORDER BY CAST(SUBSTRING(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,''), 1,CASE	WHEN PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) > 0 THEN 
															PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) - 1 
														ELSE LEN(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) 
														END
														) AS INT) ASC, ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') ASC
	END

	

END
