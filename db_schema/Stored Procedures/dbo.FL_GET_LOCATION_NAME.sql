SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_GET_LOCATION_NAME
/* ===========================================================================
 '   NAME:           FL_GET_LOCATION_NAME
 '   DATE CREATED:   14th Oct 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the location name against id
 '   IN:             @LocationID
 '
 '   OUT:            @LocationName
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
	@LocationID INT	
	)	
	
AS 	

SELECT LocationName 
FROM FL_LOCATION 
WHERE LocationId = @LocationID














GO
