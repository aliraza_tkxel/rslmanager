SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetReviewList_old]
			@caseOfficerId	int,
			@statusId	int,
			@overdue	bit,
			@skipIndex	int = 0,
			@pageSize	int = 10
AS
BEGIN
		declare @overdueset varchar(50)
		if(@overdue = 0)
		BEGIN
			set @overdueset = 'false'
			SELECT TOP(@pageSize) 
						Max(AM_Case.TenancyId) as TenantId,
						Max(AM_Status.Title) as Status,
				        Max(dbo.AM_FN_Get_Next_Action(AM_Status.StatusId, AM_Action.ActionId)) AS NextAction, 
						--@nextActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(AM_Status.StatusId, AM_Action.ActionId),
						Max(isnull(E__EMPLOYEE.FIRSTNAME, '') + ' ' + isnull(E__EMPLOYEE.LASTNAME, '')) as Owned, 
						(SELECT TOP 1 ISNULL(Title,'') +  ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, ' ') as CName
																	FROM AM_Customer_Rent_Parameters
																	WHERE TenancyId = AM_Case.TENANCYID
																	ORDER BY CustomerId ASC) as CustomerName,

																(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
																	FROM AM_Customer_Rent_Parameters
																	WHERE TenancyId = AM_Case.TENANCYID
																	ORDER BY CustomerId DESC) as CustomerName2,

																(SELECT Count(DISTINCT CustomerId)
																	FROM AM_Customer_Rent_Parameters
																	WHERE	TenancyId = AM_Case.TENANCYID) as JointTenancyCount, 				
						Max(convert(varchar(100), AM_Case.ActionRecordeddate, 103)) as ActionRecordedDate, 
						Max(dbo.AM_FN_GetCaseHistoryId(AM_Case.Caseid)) as CaseHistoryid,Max(convert(varchar(50), @overdueset)) as IsOverdue,
						Max(AM_Case.Caseid) as CaseId, 
						Max(AM_Customer_Rent_Parameters.CUSTOMERID) as CustomerId, 
						dbo.AM_FN_IS_NEXT_STATUS(AM_Status.StatusId, AM_Action.ActionId) as IsNextStatus, 
						Max(convert(varchar(100), AM_Case.StatusRecordedDate, 103)) as StatusRecordedDate,
						convert(varchar(50),( SELECT innerAction.RecommendedFollowupPeriod 
												FROM AM_Action as innerAction
												Where innerAction.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(AM_Status.StatusId, AM_Action.ActionId))) + ';' + 
						(SELECT AM_LookupCode.CodeName 
						 FROM AM_LookupCode INNER JOIN
							 AM_Action as innerAction2 ON AM_LookupCode.LookupCodeId = innerAction2.RecommendedFollowupPeriodFrequencyLookup
						 WHERE innerAction2.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(AM_Status.StatusId, AM_Action.ActionId))as nextActionAlert,
						Max(Convert(varchar(100), AM_Case.ActionReviewDate , 103)) as ActionReviewDate

			FROM         AM_Case INNER JOIN
								  AM_Action ON AM_Case.ActionId = AM_Action.ActionId INNER JOIN
								  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
								  AM_Resource ON AM_Case.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								  AM_Customer_Rent_Parameters ON dbo.AM_Case.TenancyId=dbo.AM_Customer_Rent_Parameters.TenancyId INNER JOIN
								  --C_CUSTOMERTENANCY ON AM_Case.TenancyId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								  --C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

			WHERE AM_Case.IsActive = 1 
				  and (AM_Case.StatusId = case when @statusId=-1 then AM_Case.StatusId else @statusId end)	
				  and (AM_Case.CaseOfficer = case when @caseOfficerId = -1 then AM_Case.CaseOfficer else @caseOfficerId end)
				  --and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_Case.ActionRecordeddate ) = 'false'
				  and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_Case.ActionReviewDate ) = 'false' 
				  and AM_Case.CaseId NOT IN(
											SELECT DISTINCT TOP(@skipIndex) AM_Case.Caseid
											FROM         AM_Case INNER JOIN
																  AM_Action ON AM_Case.ActionId = AM_Action.ActionId INNER JOIN
																  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
																  AM_Resource ON AM_Case.CaseOfficer = AM_Resource.ResourceId INNER JOIN
																  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
																  AM_Customer_Rent_Parameters ON dbo.AM_Case.TenancyId=dbo.AM_Customer_Rent_Parameters.TenancyId INNER JOIN
																  --C_CUSTOMERTENANCY ON AM_Case.TenancyId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
																  --C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
																  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

											WHERE AM_Case.IsActive = 1 
												  and (AM_Case.StatusId = case when @statusId=-1 then AM_Case.StatusId else @statusId end)	
												  and (AM_Case.CaseOfficer = case when @caseOfficerId = -1 then AM_Case.CaseOfficer else @caseOfficerId end)
												  --and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_Case.ActionRecordeddate ) = 'false'
												  and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_Case.ActionReviewDate ) = 'false' 
											order by AM_Case.Caseid
											)
			group by AM_Case.TenancyId, AM_Status.StatusId, AM_Action.ActionId	
		END
		else
		BEGIN			
			
			set @overdueset = 'true'
			SELECT  TOP (@pageSize) Max(AM_Status.Title)  as Status, dbo.AM_FN_Get_Next_Action(AM_Status.StatusId, AM_Action.ActionId) AS NextAction, 
									Max(isnull(E__EMPLOYEE.FIRSTNAME, '') + ' ' + isnull(E__EMPLOYEE.LASTNAME, '')) as Owned, 
									(SELECT TOP 1 ISNULL(Title,'') +  ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, ' ') as CName
																	FROM AM_Customer_Rent_Parameters
																	WHERE TenancyId = AM_CaseHistory.TennantId
																	ORDER BY CustomerId ASC) as CustomerName,

																(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
																	FROM AM_Customer_Rent_Parameters
																	WHERE TenancyId = AM_CaseHistory.TennantId
																	ORDER BY CustomerId DESC) as CustomerName2,

																(SELECT Count(DISTINCT CustomerId)
																	FROM AM_Customer_Rent_Parameters
																	WHERE	TenancyId = AM_CaseHistory.TennantId) as JointTenancyCount, 
									Max(convert(varchar(100), AM_CaseHistory.ActionRecordeddate, 103)) as ActionRecordedDate, 
									Max(AM_CaseHistory.CaseHistoryId) as CaseHistoryId, Max(AM_CaseHistory.TennantId) as TenantId, @overdueset as IsOverdue, 
									Max(AM_CaseHistory.Caseid) as CaseId, 
									Max(AM_Customer_Rent_Parameters.CUSTOMERID) as CustomerId,
									dbo.AM_FN_IS_NEXT_STATUS(AM_Status.StatusId, AM_Action.ActionId) as IsNextStatus, 
									Max(convert(varchar(100), AM_CaseHistory.StatusRecordedDate, 103)) as StatusRecordedDate,
									(convert(varchar(50), ( SELECT innerAction.RecommendedFollowupPeriod 
															FROM AM_Action as innerAction
															Where ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(AM_Status.StatusId, AM_Action.ActionId))) + ';' + 
									(SELECT AM_LookupCode.CodeName 
									 FROM AM_LookupCode INNER JOIN
										  AM_Action as innerAction2 ON AM_LookupCode.LookupCodeId = innerAction2.RecommendedFollowupPeriodFrequencyLookup
									 WHERE innerAction2.ActionId = dbo.AM_FN_GET_NEXT_ACTION_ID(AM_Status.StatusId, AM_Action.ActionId)))as nextActionAlert, AM_Action.ActionId as curentactionId,
									Max(Convert(varchar(100), AM_CaseHistory.ActionReviewDate , 103)) as ActionReviewDate

			FROM         AM_CaseHistory INNER JOIN
								  AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
								  AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
								  AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
								  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
								  AM_Customer_Rent_Parameters ON dbo.AM_CaseHistory.TennantId=dbo.AM_Customer_Rent_Parameters.TenancyId INNER JOIN
								  --C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
								  --C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
								  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

			WHERE AM_CaseHistory.IsActive = 1 
					and (AM_CaseHistory.StatusId = case when @statusId=-1 then AM_CaseHistory.StatusId else @statusId end) 
					and (AM_CaseHistory.CaseOfficer = case when @caseOfficerId = -1 then AM_CaseHistory.CaseOfficer else @caseOfficerId end)
					--and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = 'true' 
					and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_CaseHistory.ActionReviewDate ) = 'true' 
					--and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId)
					and AM_CaseHistory.CaseHistoryId = (SELECT TOP 1 A.CaseHistoryId FROM dbo.AM_CaseHistory A WHERE A.CaseId=dbo.AM_CaseHistory.CaseId and A.IsActionIgnored = 'false')
					and AM_Customer_Rent_Parameters.CUSTOMERID NOT IN(
												SELECT TOP(@skipIndex) AM_Customer_Rent_Parameters.CUSTOMERID 
												FROM         AM_CaseHistory INNER JOIN
																	  AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId INNER JOIN
																	  AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
																	  AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId INNER JOIN
																	  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
																	  AM_Customer_Rent_Parameters ON dbo.AM_CaseHistory.TennantId=dbo.AM_Customer_Rent_Parameters.TenancyId INNER JOIN
																	  --C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID INNER JOIN
																	  --C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID INNER JOIN
																	  AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId

												WHERE AM_CaseHistory.IsActive = 1 
													  and (AM_CaseHistory.StatusId = case when @statusId=-1 then AM_CaseHistory.StatusId else @statusId end) 
													  and (AM_CaseHistory.CaseOfficer = case when @caseOfficerId = -1 then AM_CaseHistory.CaseOfficer else @caseOfficerId end)
													  --and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(AM_Action.RecommendedFollowupPeriod, AM_LookupCode.CodeName, AM_CaseHistory.ActionRecordeddate ) = 'true'
													  and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, 'Days', AM_CaseHistory.ActionReviewDate ) = 'true' 
													   --and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId)
													  and AM_CaseHistory.CaseHistoryId = (SELECT TOP 1 A.CaseHistoryId FROM dbo.AM_CaseHistory A WHERE A.CaseId=dbo.AM_CaseHistory.CaseId and A.IsActionIgnored = 'false')
												order by AM_CaseHistory.CaseId )
			group by AM_CaseHistory.TennantId, AM_Status.StatusId, AM_Action.ActionId
			
	  END
END

--exec AM_SP_GetReviewList -1, -1, 1, 0 , 1400















GO
