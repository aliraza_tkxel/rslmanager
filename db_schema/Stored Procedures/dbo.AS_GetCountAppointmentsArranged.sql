USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetCountAppointmentsArranged]    Script Date: 28-Jul-17 1:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[AS_GetCountAppointmentsArranged]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetCountAppointmentsArranged] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetCountAppointmentsArranged]
	@PatchId		int,
	@DevelopmentId	int
AS
BEGIN
	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT 
								COUNT(P__PROPERTY.PROPERTYID) AS Number'
	
	SET @fromClause		= 'FROM AS_APPOINTMENTS 
										INNER JOIN	AS_JOURNAL on AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID 
										INNER JOIN	AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId 
										INNER JOIN E__EMPLOYEE on AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
										INNER JOIN P__PROPERTY on AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
										--INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
										LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
										LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
										INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID 
										LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
										LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = C_TENANCY.TENANCYID
										AND CT.CUSTOMERTENANCYID = (SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					)
										LEFT JOIN C_ADDRESS CA ON CA.CUSTOMERID = CT.CUSTOMERID  AND CA.IsDefault=1
										LEFT JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = CT.CUSTOMERID
										LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P__PROPERTY.PROPERTYID
										AND A.ITEMPARAMID =  (SELECT ItemParamID FROM PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
													WHERE ParameterName = ''Heating Fuel'' AND ItemName = ''Heating'')
										LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
										INNER JOIN AS_EmailStatus on AS_APPOINTMENTS.EmailStatusId=AS_EmailStatus.EmailStatusId
										INNER JOIN AS_SmsStatus on AS_APPOINTMENTS.SmsStatusId=AS_SmsStatus.SmsStatusId
										INNER JOIN AS_PushNoticificationStatus on AS_APPOINTMENTS.PushNoticificationId=AS_PushNoticificationStatus.PushNoticificationId'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND P__PROPERTY.FUELTYPE = 1
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_APPOINTMENTS.APPOINTMENTSTATUS NOT IN (''InProgress'')
										AND (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE ''Arranged''))																						
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
									
	IF (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
		
	IF (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	IF (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause
	
	-- Printing the query for debugging
	PRINT @query 
	
	-- Executing the query
    EXEC (@query)
    
END
