
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetSuppressedCaseListCount]
		@assignedToId	int = 0,
	@regionId	int = 0,
	@suburbId	int = 0,
	@allAssignedFlag	bit,
	@allRegionFlag	bit,
	@allSuburbFlag	bit
AS
BEGIN

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

IF(@assignedToId = 0 )
BEGIN

	IF(@regionId = 0 and @suburbId =0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

END
ELSE 
BEGIN

IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
														FROM AM_ResourcePatchDevelopment 
														WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ 'AND IsActive=''true'')'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END
END

SET @query =
			'SELECT COUNT(*) as recordCount FROM(SELECT COUNT(*) as RecordCount									
					FROM         AM_Case INNER JOIN
								  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN									  
								  C_TENANCY ON customer.TENANCYID = C_TENANCY.TENANCYID INNER JOIN
								  P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
								  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
								  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
								  INNER JOIN AM_Resource ON AM_Case.SuppressedBy = AM_Resource.ResourceId INNER JOIN
								  E__EMPLOYEE ON AM_Resource.EmployeeId = E__Employee.EmployeeId

					WHERE '+ @RegionSuburbClause +' 
						AND (AM_Case.CaseOfficer = (case when 0 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@assignedToId )+ ' END)
																							OR AM_Case.CaseManager = (case when 0 = ' +  convert(varchar(10),@assignedToId )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@assignedToId )+ ' END) ) 
						AND AM_Case.IsSuppressed = 1 
						AND AM_Case.IsActive = 1 GROUP BY AM_Case.TenancyId ) as TEMP'


--PRINT(@query);
EXEC(@query);

END







GO
