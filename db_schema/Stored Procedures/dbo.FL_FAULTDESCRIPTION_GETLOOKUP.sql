SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_FAULTDESCRIPTION_GETLOOKUP 
	/* ===========================================================================
 '   NAME:          FL_FAULTDESCRIPTION_GETLOOKUP
 '   DATE CREATED:   28 OCT 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Description FL_FAULT table which will be shown as lookup value in presentation pages
 '   IN:            @ElementID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@ElementID as Int
AS
	SELECT FaultID AS id,Description AS val
	FROM FL_FAULT
	WHERE ElementID=@ElementID
	ORDER BY FaultID DESC













GO
