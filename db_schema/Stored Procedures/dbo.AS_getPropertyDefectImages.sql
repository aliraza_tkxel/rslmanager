
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
-- EXEC AS_getPropertyDefectImages @propertyDefectId = 1  
-- Author:  <Salman Nazir>  
-- Create date: <22/11/2012>  
-- Description: <Get the property Defect Images>  
-- WebPage: PropertyRecord.aspx => Attributes Tab => Appliances Tab  
-- =============================================  
CREATE PROCEDURE [dbo].[AS_getPropertyDefectImages](  
@propertyId varchar(20)  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 --SELECT *  
 --FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES  
 --WHERE PropertyDefectId = @propertyDefectId 
 
 select * from P_PROPERTY_APPLIANCE_DEFECTS_IMAGES where PropertyDefectId IN (
Select  PropertyDefectId  from P_PROPERTY_APPLIANCE_DEFECTS  where PropertyId=@propertyId 
) 
END  
GO
