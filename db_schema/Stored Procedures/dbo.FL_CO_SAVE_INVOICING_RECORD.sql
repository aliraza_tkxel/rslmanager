
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[FL_CO_SAVE_INVOICING_RECORD](
/* ===========================================================================
 '   NAME:         [FL_CO_SAVE_INVOICING_RECORD]
 '   DATE CREATED:  2 Jan 2009
 '   CREATED BY:    Waseem Hassan
 '   Chagned by :   Noor Muhammad, Adnan Mirza
 '   CREATED FOR:    RSL
 '   Save record for invoicing 
 '   
 '   IN:	   @repairId INT,
 '   IN:       @completionDate DateTime ,
 '   IN:       @completedBy INT,
 '   IN        @orgId INT,
 '   IN       @appointmentId INT
 '   IN:	   @time nvarchar,
 '   IN:	   @repairDetail nvarchar,
 '   IN:	   @net float,
 '   IN:	   @vatId INT,
 '   IN:	   @vat float,
 '   IN:	   @gross float,
'
 '   OUT:			  @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:         1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@FaultLogId INT,
@completionDate DateTime ,
@completedBy INT,
@orgId INT,
@appointmentId INT,
@time nvarchar(20),
@RESULT  INT = 1  OUTPUT 
)
AS

DECLARE	@UserId INT,
		@TeamId  INT,
		@OrderId  INT,
		@NatureTitle  NVARCHAR(1000),
		@CustomerId INT,
		@PropertyId nvarchar(20),
		@TenancyId int,
		@ItemnatureId INT,
		@ItemId INT,
		@ItemStatusId INT,
		@JournalId INT,
		@JobSheetNumber nvarchar(100),
		@WorkorderId INT,
		@ExpiryDate DateTime,
		@PostInspection bit,
		@ScopeID int,
		@ORDERITEMID int,
		@WOTOREPAIRID int,
		@FaultRepairListId int,
		@CJournalId int,
		@repairDetail nvarchar(1000),
		@net float,
		@vatId INT,
		@vat float,
		@gross float,		
		@PostInspectionStatusId INT,
		@WorksCompletedStatusId INT,
		@completionDateTIME DATETIME
			

			
BEGIN    

SET NOCOUNT ON
BEGIN TRAN

	IF @orgId = 0 OR @orgId IS NULL 
		BEGIN
			-- ASSIGN IT TO 3C IF ORGID IS NULL OR 0
			SET @orgId = 1270
		END
	SELECT
	     @UserId = E__EMPLOYEE.EMPLOYEEID,@TeamId = E_TEAM.TEAMID,
		 @JobSheetNumber = FL_CO_APPOINTMENT.JobSheetNumber,
		 @ExpiryDate = FL_FAULT_LOG.DueDate,
		 @CustomerId = C__CUSTOMER.CUSTOMERID,
		 @PropertyId = TENANCY.PROPERTYID,
		 @TenancyId = TENANCY.TENANCYID,
		 @NatureTitle = C_NATURE.DESCRIPTION,
		 @ItemnatureId = C_NATURE.ITEMNATUREID,
		 @ItemId = C_NATURE.ITEMID,
		 @ItemStatusId = FL_FAULT_STATUS.FaultStatusID
		 
		FROM   FL_CO_APPOINTMENT 
		INNER JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID
	    INNER JOIN E_JOBDETAILS ON  E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
		INNER JOIN E_TEAM ON  E_JOBDETAILS.TEAM = E_TEAM.TEAMID
		INNER JOIN FL_FAULT_LOG ON  FL_CO_APPOINTMENT.JobSheetNumber = FL_FAULT_LOG.JobSheetNumber
		INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		INNER JOIN C__CUSTOMER ON  FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND CT.ENDDATE IS NULL
		INNER JOIN C_TENANCY TENANCY ON TENANCY.TENANCYID = CT.TENANCYID AND TENANCY.ENDDATE IS NULL	
		--INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C__CUSTOMER.CUSTOMERID
		--INNER JOIN C_TENANCY TENANCY ON TENANCY.TENANCYID = CT.TENANCYID	
		INNER JOIN FL_FAULT_STATUS ON  FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
		INNER JOIN FL_FAULT_JOURNAL ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID
		iNNER JOIN C_NATURE ON  FL_FAULT_JOURNAL.ITEMID = C_NATURE.ITEMID
		
	WHERE 
		 E__EMPLOYEE.ORGID = @ORGID  AND FL_CO_APPOINTMENT.AppointmentID = @appointmentId AND C_NATURE.ITEMNATUREID = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION='Reactive Repair') --AND C_STATUS.ITEMSTATUSID = 32



--Get and set the repair assigned to contractor status
SET @PostInspectionStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Post Inspection')

--Get and set the works completed status
SET @WorksCompletedStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Works Completed')


SET @completionDateTIME = @completionDate + @time

/* ------------ FINANCE PART IS NO LONGER REQUIERD ON COMPLETION OF JOB - ADNAN -------------
--Insertion Into F_PURCHASEORDER
INSERT INTO F_PURCHASEORDER 
(PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE, POTYPE, POSTATUS) 
VALUES 
( 'WORK ORDER',  @completionDate, 'This purchase order was created automatically from the Repairs process.', @UserId, @orgId, 1, 2, 5)

SELECT @OrderId = ORDERID 
FROM F_PURCHASEORDER 
WHERE ORDERID = @@Identity


--Insertion Into P_WORKORDER
INSERT INTO P_WORKORDER 
(ORDERID, TITLE, CUSTOMERID, PROPERTYID, TENANCYID, WOSTATUS, BIRTH_NATURE, BIRTH_ENTITY, BIRTH_MODULE) 
VALUES( @OrderId , @NatureTitle, @CustomerId , @PropertyId ,@TenancyId , 2,@ItemnatureId , 1, 1)

--Get the latest Work order Id
SELECT @WorkorderId = WOID 
FROM P_WORKORDER 
WHERE WOID = @@Identity
*/

SET @OrderId=100 -- DUMMY ORDERID

SELECT @JournalId = JOURNALID 
FROM FL_FAULT_JOURNAL 
WHERE FaultLogID = @FaultLogId  
	
	
/*
Declare the cursor
*/
DECLARE c1 CURSOR READ_ONLY

FOR

SELECT FaultLogID,FaultRepairListID FROM FL_CO_FAULTLOG_TO_REPAIR WHERE FaultLogID = @FaultLogId  

OPEN c1

/*
Start Looping
*/

FETCH NEXT FROM c1
INTO @FaultLogId, @FaultRepairListId
				
WHILE @@FETCH_STATUS = 0
BEGIN

--PRINT @FaultLogId
--Get the latest CJournal ID
	SELECT @repairDetail= Description, @net=NetCost, @vatId=VatRateID, @vat=Vat, @gross= Gross 
	FROM FL_FAULT_REPAIR_LIST 
	WHERE FaultRepairListID =@FaultRepairListId
	
	--Insertion into C_Journal
	INSERT INTO C_JOURNAL 
	(CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) 
	VALUES 
	( @CustomerId, @TenancyId,@PropertyId,@ItemId  , @ItemnatureId, 11, @repairDetail)

	--Get the latest CJournal ID
	SELECT @CJournalId = JOURNALID 
	FROM C_JOURNAL 
	WHERE JOURNALID = @@Identity

	--Insertion into C_Repair
	INSERT INTO C_REPAIR
	(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, ScopeID, Title, Notes)
	VALUES
	(@CJournalId, 11, 6, @completionDateTIME, NULL, @FaultLogId, @OrgId, @ScopeID, NULL, NULL)

-- Insertion into table which will be used in Invoicing
	INSERT INTO FL_FAULTJOURNAL_TO_CJOURNAL (FJOURNALID,CJOURNALID) VALUES (@JournalId,@CJournalId)

	/* ------------ FINANCE PART IS NO LONGER REQUIERD ON COMPLETION OF JOB - ADNAN -------------
	--Insertion into F_PURCAHSEITEM
	INSERT INTO F_PURCHASEITEM 
	(ORDERID, ITEMNAME, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE,PISTATUS,EXPENDITUREID) 
	VALUES 
	(@OrderId, @repairDetail,@completionDate, @ExpiryDate, @net, @vatId, @vat, @gross,@UserId, 1, 2 , 5,24)
	
	--Get the latest ORDER ITEM ID
	SELECT @ORDERITEMID = ORDERITEMID 
	FROM F_PURCHASEITEM 
	WHERE ORDERITEMID = @@Identity
	
	--Insertion into P_WOTOREPAIR	
	INSERT INTO P_WOTOREPAIR 
	(WOID, JOURNALID, ORDERITEMID) 
	VALUES 
	(@WorkorderId, @CJournalId,@ORDERITEMID)
	
	--Get the latest WO TO REPAIR ID
	SELECT @WOTOREPAIRID = WOTOREPAIRID 
	FROM P_WOTOREPAIR 
	WHERE WOTOREPAIRID = @@Identity
	
	*/
	
FETCH NEXT FROM c1
	INTO @FaultLogId, @FaultRepairListId

END	

CLOSE C1
DEALLOCATE C1
		

----------------------Recharge Process-------------------------------------------
		DECLARE @QTY INT
		DECLARE @RECHARGE INT
		DECLARE @NetCost FLOAT
		DECLARE @VatType INT
		DECLARE @rVAT FLOAT
		DECLARE @GrossCost FLOAT
		DECLARE @FaultDescription NVARCHAR(100)
		DECLARE @SaleID INT
		DECLARE @SALEITEMID INT
		DECLARE @RentJournal_id BIGINT

		SELECT @QTY=QUANTITY,@RECHARGE=FL.RECHARGE,@NetCost=NETCOST,@VatType=VATRATEID,@rVAT=VAT,@GrossCost=GROSS,@FaultDescription=F.DESCRIPTION
		FROM dbo.FL_FAULT_LOG FL
		INNER JOIN dbo.FL_FAULT F ON F.FaultID=FL.FaultID
		WHERE FL.FaultLogID = @FaultLogId

		IF @RECHARGE=1
			BEGIN
					INSERT INTO C_RECHARGEABLE (JOURNALID) VALUES (@CJournalId)
					INSERT INTO F_SALESINVOICE (SONAME, SODATE, SONOTES, USERID, TENANCYID, ACTIVE, SOTYPE, SOSTATUS) VALUES
					('TENANT RECHARGE', @completionDateTIME, 'Automated Tenant Recharge for: ' + @FaultDescription, @UserID,@TenancyId, 1, 1, 1)
					
					SELECT @SaleID = SaleID FROM F_SALESINVOICE WHERE SaleID = SCOPE_IDENTITY()
					
					INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID )
					VALUES (@TenancyId, @completionDateTIME, 5, NULL, @GrossCost, 5)
					SELECT @RentJournal_id = SCOPE_IDENTITY()
					
					INSERT INTO F_SALESINVOICEITEM (SALEID, SALESCATID, ITEMNAME, ITEMDESC, SIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, SITYPE, SISTATUS, RENTJOURNALID) 
					VALUES (@SaleID, 1, @FaultDescription, @FaultDescription, @completionDateTIME,@NetCost,@VatType,@rVAT,@GrossCost, @UserID, 1, 1, 1, @RentJournal_id)
					SELECT @SALEITEMID = SCOPE_IDENTITY()
					
					--INSERT INTO F_SALEITEM_TO_PURCHASEITEM (SALEITEMID, ORDERITEMID) VALUES (@SALEITEMID, @ORDERITEMID)
					INSERT INTO F_SALEITEM_TO_PURCHASEITEM (SALEITEMID) VALUES (@SALEITEMID)

					EXEC NL_SALESORDER @SaleID

			END 

----------------------End of Recharge Process-------------------------------------------
	
		
	-- Get Value of ScopeID
	SELECT @ScopeID=SCOPEID 
	FROM S_SCOPE 
	WHERE ORGID=@OrgId 
	AND AREAOFWORK= (SELECT AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair'))

	
	SET @PostInspection = (SELECT  DISTINCT(FL_FAULT_REPAIR_LIST.PostInspection)
	FROM  FL_CO_FAULTLOG_TO_REPAIR 
	INNER JOIN FL_FAULT_REPAIR_LIST ON FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID = FL_FAULT_REPAIR_LIST.FaultRepairListID
	WHERE  (FL_FAULT_REPAIR_LIST.PostInspection = 1) AND (FL_CO_FAULTLOG_TO_REPAIR.FaultLogID = @FaultLogId))
 
	IF @PostInspection =1	
	Begin 
		
		--An Entry into FL_FAULT_LOG_HISTORY for works completed status										
		INSERT INTO FL_FAULT_LOG_HISTORY
		(JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
		VALUES
		(@JOURNALID, @WorksCompletedStatusId, NULL,GETDATE(), NULL, @FaultLogId, @OrgId, @ScopeID, NULL, NULL)
		
		--An Entry into FL_FAULT_LOG_HISTORY for Post Inspection status										
		INSERT INTO FL_FAULT_LOG_HISTORY
		(JournalID,FaultStatusID,ItemActionID,LastActionDate,LastActionUserID,FaultLogID,ORGID,ScopeID,Title,Notes)
		VALUES
		(@JOURNALID, @PostInspectionStatusId, NULL, GETDATE(), NULL, @FaultLogId, @OrgId, @ScopeID, NULL, NULL)
					
		--Update Fault Status in Fault Journal
		UPDATE FL_FAULT_JOURNAL  
		SET FaultStatusID = @PostInspectionStatusId 
		WHERE FaultLogID =  @FaultLogId
							
		--Update Fault Status in Fault Log
		UPDATE FL_FAULT_LOG  
		SET StatusID = @PostInspectionStatusId  
		WHERE FaultLogID =  @FaultLogId
				
    END
    
    ELSE  --If post inspection is false then its status will be works completed			
    BEGIN
		--An Entry into FL_FAULT_LOG_HISTORY for works completed status										
		INSERT INTO FL_FAULT_LOG_HISTORY
		(JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
		VALUES
		(@JOURNALID, @WorksCompletedStatusId, NULL, GETDATE(), NULL, @FaultLogId, @OrgId, @ScopeID, NULL, NULL)
					
		--Update Fault Status in Fault Journal
		UPDATE FL_FAULT_JOURNAL  
		SET FaultStatusID = @WorksCompletedStatusId, LastActionDate= GETDATE() 
		WHERE FaultLogID =  @FaultLogId
							
		--Update Fault Status in Fault Log
		UPDATE FL_FAULT_LOG  
		SET StatusID = @WorksCompletedStatusId  
		WHERE FaultLogID =  @FaultLogId
    END
    
    --IF @OrderId <0 OR @WorkorderId<0 
	IF @orderid<0
	Begin
		Rollback Transaction
		SET @Result=-1
		Print 'Unable to update the record'
	End
	ELSE
	Begin					
		SET @Result=1
		COMMIT TRAN
	END
END    








GO
