USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetStandardLetterById]    Script Date: 07/05/2018 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[AS_GetStandardLetterById]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetStandardLetterById] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetStandardLetterById]
	-- Add the parameters for the stored procedure here
	@StandardLetterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		AS_StandardLetters.StandardLetterId,
		AS_StandardLetters.StatusId,
		AS_StandardLetters.ActionId,
		AS_StandardLetters.Title,
		AS_StandardLetters.Code,
		AS_StandardLetters.Body,
		AS_StandardLetters.IsAlternativeServicing
	FROM
		AS_StandardLetters
	WHERE
		AS_StandardLetters.StandardLetterId=@StandardLetterID
			
END
