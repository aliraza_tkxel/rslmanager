SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE dbo.FL_GET_CUSTOMERINFO
/* ===========================================================================
 '   NAME:          FL_GET_CUSTOMERINFO
 '   DATE CREATED:   20 NOV 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get CUSTOMER tenancy and property id to be used in journal
 '   IN:            @CUSTOMERID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@CUSTOMERID as Int
AS
	SELECT     C_TENANCY.TENANCYID AS TENANCY, PROPERTYID AS PROPERTY
FROM         C_TENANCY
	INNER JOIN C_CUSTOMERTENANCY ON C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
	INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID=C__CUSTOMER.CUSTOMERID
WHERE     (C__CUSTOMER.CUSTOMERID = @CUSTOMERID)













GO
