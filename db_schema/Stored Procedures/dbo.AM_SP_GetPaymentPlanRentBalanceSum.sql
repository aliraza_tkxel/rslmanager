
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetPaymentPlanRentBalanceSum]	
			@assignedToId	int = 0,
			@regionId	int = 0,
			@suburbId	int = 0,
			@allAssignedFlag	bit,
			@allRegionFlag	bit,
			@allSuburbFlag	bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--    	SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance
--		FROM       AM_PaymentPlan INNER JOIN
--  				   C_TENANCY ON AM_PaymentPlan.TennantId = C_TENANCY.TENANCYID INNER JOIN
--				   P_FINANCIAL ON C_TENANCY.PROPERTYID = P_FINANCIAL.PROPERTYID
--		WHERE AM_PaymentPlan.IsCreated = 1
		
	if(@allAssignedFlag = 0)
	BEGIN
		if(@allRegionFlag = 0)
		BEGIN
			if(@allSuburbFlag = 0)
			BEGIN
				SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance

				FROM         AM_PaymentPlan INNER JOIN
							 C_CUSTOMERTENANCY on AM_PaymentPlan.TennantId = C_CUSTOMERTENANCY.TenancyId INNER JOIN
							 C__CUSTOMER ON C_CUSTOMERTENANCY.CustomerId = C__CUSTOMER.CustomerId INNER JOIN
							 G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId INNER JOIN
							 C_Tenancy ON C_CUSTOMERTENANCY.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId 
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
				Where		 AM_PaymentPlan.IsCreated = 'true' and PDR_DEVELOPMENT.Patchid = @regionId and P__PROPERTY.SCHEMEID = @suburbId
							and AM_PaymentPlan.CreatedBy = @assignedToId 
			END
			else
			BEGIN
				SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance

				FROM         AM_PaymentPlan INNER JOIN
							 C_CUSTOMERTENANCY on AM_PaymentPlan.TennantId = C_CUSTOMERTENANCY.TenancyId INNER JOIN
							 C__CUSTOMER ON C_CUSTOMERTENANCY.CustomerId = C__CUSTOMER.CustomerId INNER JOIN
							 G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId INNER JOIN
							 C_Tenancy ON C_CUSTOMERTENANCY.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId 
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
				Where		 AM_PaymentPlan.IsCreated = 'true' and PDR_DEVELOPMENT.Patchid = @regionId and AM_PaymentPlan.CreatedBy = @assignedToId
			END
		END
		else
		BEGIN
				SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance

				FROM         AM_PaymentPlan INNER JOIN
							 C_CUSTOMERTENANCY on AM_PaymentPlan.TennantId = C_CUSTOMERTENANCY.TenancyId INNER JOIN
							 C__CUSTOMER ON C_CUSTOMERTENANCY.CustomerId = C__CUSTOMER.CustomerId INNER JOIN
							 G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId INNER JOIN
							 C_Tenancy ON C_CUSTOMERTENANCY.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId 
							  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
				Where		 AM_PaymentPlan.IsCreated = 'true' and AM_PaymentPlan.CreatedBy = @assignedToId
		END
	END
	else
	BEGIN
		if(@allRegionFlag = 0)
		BEGIN
			if(@allSuburbFlag = 0)
			BEGIN
				SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance

				FROM         AM_PaymentPlan INNER JOIN
							 C_CUSTOMERTENANCY on AM_PaymentPlan.TennantId = C_CUSTOMERTENANCY.TenancyId INNER JOIN
							 C__CUSTOMER ON C_CUSTOMERTENANCY.CustomerId = C__CUSTOMER.CustomerId INNER JOIN
							 G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId INNER JOIN
							 C_Tenancy ON C_CUSTOMERTENANCY.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
				Where		 AM_PaymentPlan.IsCreated = 'true' and PDR_DEVELOPMENT.Patchid = @regionId and P__PROPERTY.SCHEMEID = @suburbId							
			END
			else
			BEGIN
				SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance

				FROM         AM_PaymentPlan INNER JOIN
							 C_CUSTOMERTENANCY on AM_PaymentPlan.TennantId = C_CUSTOMERTENANCY.TenancyId INNER JOIN
							 C__CUSTOMER ON C_CUSTOMERTENANCY.CustomerId = C__CUSTOMER.CustomerId INNER JOIN
							 G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId INNER JOIN
							 C_Tenancy ON C_CUSTOMERTENANCY.TenancyId = C_Tenancy.TenancyId INNER JOIN
							 P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId 
							 LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
							 INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 
				Where		 AM_PaymentPlan.IsCreated = 'true' and PDR_DEVELOPMENT.Patchid = @regionId							 
			END
		END
		else
		BEGIN
			SELECT     sum(AM_PaymentPlan.WeeklyRentAmount)as TotalRent, sum(dbo.FN_GET_TENANT_BALANCE(AM_PaymentPlan.TennantId)) as RentBalance

			FROM         AM_PaymentPlan INNER JOIN
						 C_CUSTOMERTENANCY on AM_PaymentPlan.TennantId = C_CUSTOMERTENANCY.TenancyId INNER JOIN
						 C__CUSTOMER ON C_CUSTOMERTENANCY.CustomerId = C__CUSTOMER.CustomerId INNER JOIN
						 G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId 
			Where		 AM_PaymentPlan.IsCreated = 'true' 
		END		
	END	
END





GO
