USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[AS_GetIssuedCertificateReport]    Script Date: 2/21/2017 3:25:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <22/08/2013>
-- Description:	<This stored procedure fetches the Issued Certificate Report>
-- Web Page:    <IssuedCertificates.aspx>
-- EXEC	@return_value = [dbo].[AS_GetIssuedCertificateReport]
--		@fromDate = N'2013/07/01',
--		@toDate = N'2013/07/31',
--		@searchText = N'Adnan',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Ref',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT   
-- =============================================
IF OBJECT_ID('dbo.AS_GetIssuedCertificateReport') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetIssuedCertificateReport AS SET NOCOUNT ON;') 
GO
 
ALTER PROCEDURE [dbo].[AS_GetIssuedCertificateReport] 
  -- Add the parameters for the stored procedure here  
  @fromDate   DATETIME, 
  @toDate     DATETIME, 
  @searchText NVARCHAR(200), 
  @heatingTypeId INT = -1,

  --Parameters which would help in sorting and paging  
  @pageSize   INT = 1000, 
  @pageNumber INT = 1, 
  @sortColumn VARCHAR(50) = 'Ref', 
  @sortOrder  VARCHAR (5) = 'DESC', 
  @totalCount INT = 0 output 
AS 
  BEGIN 
      -- Insert statements for procedure here  
      DECLARE @Certificate             INT, 
              @CertificateScheme       INT, 
              @CertificateBlock        INT, 
              @Defects                 INT, 
              @DefectID                INT, 
              @JournalID               INT, 
              @PropertyID              NVARCHAR(20), 
              @SchemeId                NVARCHAR(20), 
              @BlockId                 NVARCHAR(20), 
              @SingleWarning           BIT, 
              @Warning                 NVARCHAR(5), 
              -- Declared for the paging part  
              @SelectClause            NVARCHAR(max), 
              @SelectClauseCount       NVARCHAR(max), 
              @fromClause              NVARCHAR(max), 
              @SelectClauseScheme      NVARCHAR(max), 
              @SelectClauseSchemeCount NVARCHAR(max), 
              @fromClauseScheme        NVARCHAR(max), 
              @SelectClauseBlock       NVARCHAR(max), 
              @SelectClauseBlockCount  NVARCHAR(max), 
              @fromClauseBlock         NVARCHAR(max), 
              @unionQuery              NVARCHAR(max), 
              @orderClause             NVARCHAR(max), 
              @mainSelectQuery         NVARCHAR(max), 
              @rowNumberQuery          NVARCHAR(max), 
              @finalQuery              NVARCHAR(max), 
	      @whereProperty		NVARCHAR(max),
	      @whereScheme		NVARCHAR(max),
	      @whereBlock		NVARCHAR(max),
              -- used to add in conditions in WhereClause based on search criteria provided  
              @searchCriteria          NVARCHAR(1500), 
              --variables for paging  
              @offset                  INT, 
              @limit                   INT 

      SET @Warning = 'No' 
      --Paging Formula  
      SET @offset = ( @pageNumber - 1 ) * @pageSize 
      SET @limit = ( @offset + @pageSize ) - 1 
      -- SET NOCOUNT ON added to prevent extra result sets from  
      -- interfering with SELECT statements.  
      SET nocount ON; 

      --Property  
      SELECT Distinct p__property.propertyid                       AS Ref, 
             p__property.housenumber + ' ' 
             + p__property.address1 + ', ' 
             + p__property.towncity                       AS 'Address', 
             p__property.housenumber                      AS HOUSENUMBER, 
             p__property.address1                         AS ADDRESS1, 
             p__property.towncity                         AS TOWNCITY, 
             ''                                           AS SchemeName, 
             ''                                           AS BlockName, 
             p_lgsr.lgsrid                                AS 'Certificate', 
             CASE 
               WHEN p_lgsr.cp12passed = 0 THEN 'No' 
               ELSE 'Yes' 
             END                                          AS 'Passed', 
             p_lgsr.cp12number                            AS CP12NUMBER, 
             CONVERT(NVARCHAR(10), p_lgsr.issuedate, 103) AS Issued, 
             --'CONVERT(nvarchar(5),P_LGSR.ISSUEDATE, 108) + ' ' + CONVERT(nvarchar(10),P_LGSR.ISSUEDATE, 103) AS Issued,
             p_lgsr.issuedate                             AS ISSUEDATE, 
             e__employee.firstname + ' ' 
             + e__employee.lastname                       AS 'By', 
             e__employee.firstname                        AS FIRSTNAME, 
             e__employee.lastname                         AS LASTNAME, 
             as_journalhistory.journalid                  AS JournalID, 
             CASE 
               WHEN DefectWarningInfo.defectwarningcount > 0 THEN 'Yes' 
               ELSE 'NO' 
             END                                          AS WarningNote, 
             Isnull(DefectsCountInfo.defectscount, 0)     AS Defects, 
             'Property'                                   AS ReqType,
			hm.IsActive									As IsActive,
			pv.ValueID										AS FuelId,
			pv.ValueDetail									AS FuelType,
			CASE 
               WHEN p_lgsr.CP12DOCUMENT is NULL THEN 0
               ELSE 1 
             END                                          AS IsDocumentExist
      INTO   #tempissuedcertificatereport 
      FROM   (SELECT journalid, 
                     propertyid, 
                     schemeid, 
                     blockid 
              FROM   as_journalhistory 
                     INNER JOIN as_status 
                             ON as_journalhistory.statusid = as_status.statusid 
              WHERE  as_status.title = 'Certificate issued') AS_JOURNALHISTORY 
             LEFT JOIN p_lgsr 
                     ON as_journalhistory.propertyid = p_lgsr.propertyid 
                        AND as_journalhistory.journalid = p_lgsr.journalid 
             INNER JOIN p__property 
                     ON as_journalhistory.propertyid = p__property.propertyid 
             INNER JOIN e__employee 
                     ON p_lgsr.cp12issuedby = e__employee.employeeid 
             LEFT JOIN (SELECT propertyid, 
                               journalid, 
                               Count(propertydefectid) AS DefectWarningCount 
                        FROM   p_property_appliance_defects 
                        WHERE  iswarningissued = 1 
                        GROUP  BY propertyid, 
                                  journalid) AS DefectWarningInfo 
                    ON p__property.propertyid = DefectWarningInfo.propertyid 
                       AND as_journalhistory.journalid = 
                           DefectWarningInfo.journalid 
             LEFT JOIN (SELECT propertyid, 
                               journalid, 
                               Count(propertydefectid) AS DefectsCount 
                        FROM   p_property_appliance_defects 
                        WHERE  defectdate >= @fromDate 
                               AND defectdate <= @toDate 
                        GROUP  BY propertyid, 
                                  journalid) AS DefectsCountInfo 
                    ON p__property.propertyid = DefectsCountInfo.propertyid 
                       AND as_journalhistory.journalid = 
                           DefectsCountInfo.journalid 

			 LEFT JOIN PA_HeatingMapping hm on hm.HeatingMappingId = P_LGSR.HeatingMappingId
			 LEFT JOIN PA_PARAMETER_VALUE pv on pv.ValueID = hm.HeatingType
      

      --Scheme  
      SELECT Distinct  CONVERT(VARCHAR, p_scheme.schemeid)          AS Ref, 
             ''                                           AS 'Address', 
             ''                                           AS HOUSENUMBER, 
             p_scheme.schemename                          AS ADDRESS1, 
             ''                                           AS TOWNCITY, 
             p_scheme.schemename                          AS SchemeName, 
             ''                                           AS BlockName, 
             p_lgsr.lgsrid                                AS 'Certificate', 
             CASE 
               WHEN p_lgsr.cp12passed = 0 THEN 'No' 
               ELSE 'Yes' 
             END                                          AS 'Passed', 
             p_lgsr.cp12number                            AS CP12NUMBER, 
             CONVERT(NVARCHAR(10), p_lgsr.issuedate, 103) AS Issued, 
             --'CONVERT(nvarchar(5),P_LGSR.ISSUEDATE, 108) + ' ' + CONVERT(nvarchar(10),P_LGSR.ISSUEDATE, 103) AS Issued,
             p_lgsr.issuedate                             AS ISSUEDATE, 
             e__employee.firstname + ' ' 
             + e__employee.lastname                       AS 'By', 
             e__employee.firstname                        AS FIRSTNAME, 
             e__employee.lastname                         AS LASTNAME, 
             as_journalhistory.journalid                  AS JournalID, 
             CASE 
               WHEN DefectWarningInfo.defectwarningcount > 0 THEN 'Yes' 
               ELSE 'NO' 
             END                                          AS WarningNote, 
             Isnull(DefectsCountInfo.defectscount, 0)     AS Defects, 
             'Scheme'                                     AS ReqType,
			hm.IsActive										As IsActive,
			pv.ValueID										AS FuelId,
			pv.ValueDetail									AS FuelType,
			CASE 
               WHEN p_lgsr.CP12DOCUMENT is NULL THEN 0
               ELSE 1 
             END                                          AS IsDocumentExist
      INTO   #tempissuedcertificatereportscheme 
      FROM   (SELECT journalid, 
                     propertyid, 
                     schemeid, 
                     blockid 
              FROM   as_journalhistory 
                     INNER JOIN as_status 
                             ON as_journalhistory.statusid = as_status.statusid 
              WHERE  as_status.title = 'Certificate issued') AS_JOURNALHISTORY 
             LEFT JOIN p_lgsr 
                     ON as_journalhistory.schemeid = p_lgsr.schemeid 
                        AND as_journalhistory.journalid = p_lgsr.journalid 
             INNER JOIN p_scheme 
                     ON as_journalhistory.schemeid = p_scheme.schemeid 
             INNER JOIN e__employee 
                     ON p_lgsr.cp12issuedby = e__employee.employeeid 
             LEFT JOIN (SELECT schemeid, 
                               journalid, 
                               Count(propertydefectid) AS DefectWarningCount 
                        FROM   p_property_appliance_defects 
                        WHERE  iswarningissued = 1 
                        GROUP  BY schemeid, 
                                  journalid) AS DefectWarningInfo 
                    ON p_scheme.schemeid = DefectWarningInfo.schemeid 
                       AND as_journalhistory.journalid = 
                           DefectWarningInfo.journalid 
             LEFT JOIN (SELECT schemeid, 
                               journalid, 
                               Count(propertydefectid) AS DefectsCount 
                        FROM   p_property_appliance_defects 
                        WHERE  defectdate >= @fromDate 
                               AND defectdate <= @toDate 
                        GROUP  BY schemeid, 
                                  journalid) AS DefectsCountInfo 
                    ON p_scheme.schemeid = DefectsCountInfo.schemeid 
                       AND as_journalhistory.journalid = 
                           DefectsCountInfo.journalid

			LEFT JOIN PA_HeatingMapping hm on hm.HeatingMappingId = P_LGSR.HeatingMappingId
			LEFT JOIN PA_PARAMETER_VALUE pv on pv.ValueID = hm.HeatingType 
      

      --Block  
      SELECT Distinct  CONVERT(VARCHAR, p_block.blockid)            AS Ref, 
             ''                                           AS 'Address', 
             p_block.blockname                            AS HOUSENUMBER, 
             p_block.address1                             AS ADDRESS1, 
             p_block.towncity                             AS TOWNCITY, 
             ''                                           AS SchemeName, 
             p_block.blockname + ' ' + p_block.address1 + ', ' 
             + p_block.towncity                           AS BlockName, 
             p_lgsr.lgsrid                                AS 'Certificate', 
             CASE 
               WHEN p_lgsr.cp12passed = 0 THEN 'No' 
               ELSE 'Yes' 
             END                                          AS 'Passed', 
             p_lgsr.cp12number                            AS CP12NUMBER, 
             CONVERT(NVARCHAR(10), p_lgsr.issuedate, 103) AS Issued, 
             --'CONVERT(nvarchar(5),P_LGSR.ISSUEDATE, 108) + ' ' + CONVERT(nvarchar(10),P_LGSR.ISSUEDATE, 103) AS Issued,
             p_lgsr.issuedate                             AS ISSUEDATE, 
             e__employee.firstname + ' ' 
             + e__employee.lastname                       AS 'By', 
             e__employee.firstname                        AS FIRSTNAME, 
             e__employee.lastname                         AS LASTNAME, 
             as_journalhistory.journalid                  AS JournalID, 
             CASE 
               WHEN DefectWarningInfo.defectwarningcount > 0 THEN 'Yes' 
               ELSE 'NO' 
             END                                          AS WarningNote, 
             Isnull(DefectsCountInfo.defectscount, 0)     AS Defects, 
             'Block'                                      AS ReqType,
			hm.IsActive									As IsActive,
			pv.ValueID										AS FuelId,
			pv.ValueDetail									AS FuelType,
			CASE 
               WHEN p_lgsr.CP12DOCUMENT is NULL THEN 0
               ELSE 1 
             END                                          AS IsDocumentExist
      INTO   #tempissuedcertificatereportblock 
      FROM   (SELECT journalid, 
                     propertyid, 
                     schemeid, 
                     blockid 
              FROM   as_journalhistory 
                     INNER JOIN as_status 
                             ON as_journalhistory.statusid = as_status.statusid 
              WHERE  as_status.title = 'Certificate issued') AS_JOURNALHISTORY 
             LEFT JOIN p_lgsr 
                     ON as_journalhistory.blockid = p_lgsr.blockid 
                        AND as_journalhistory.journalid = p_lgsr.journalid 
             INNER JOIN p_block 
                     ON as_journalhistory.blockid = p_block.blockid 
             INNER JOIN e__employee 
                     ON p_lgsr.cp12issuedby = e__employee.employeeid 
             LEFT JOIN (SELECT blockid, 
                               journalid, 
                               Count(propertydefectid) AS DefectWarningCount 
                        FROM   p_property_appliance_defects 
                        WHERE  iswarningissued = 1 
                        GROUP  BY blockid, 
                                  journalid) AS DefectWarningInfo 
                    ON p_block.blockid = DefectWarningInfo.blockid 
                       AND as_journalhistory.journalid = 
                           DefectWarningInfo.journalid 
             LEFT JOIN (SELECT blockid, 
                               journalid, 
                               Count(propertydefectid) AS DefectsCount 
                        FROM   p_property_appliance_defects 
                        WHERE  defectdate >= @fromDate 
                               AND defectdate <= @toDate 
                        GROUP  BY blockid, 
                                  journalid) AS DefectsCountInfo 
                    ON p_block.blockid = DefectsCountInfo.blockid 
                       AND as_journalhistory.journalid = 
                           DefectsCountInfo.journalid 

			 LEFT JOIN PA_HeatingMapping hm on hm.HeatingMappingId = p_lgsr.HeatingMappingId
			 LEFT JOIN PA_PARAMETER_VALUE pv on pv.ValueID = hm.HeatingType
			 			
	SET @whereScheme = 
		' WHERE IsActive = 1 AND ( ( FIRSTNAME + '' '' 
            + LASTNAME ) LIKE ''%' + @searchText + '%'' 
        OR ( ADDRESS1 ) LIKE ''%' + @searchText + '%'' ) 
        AND ISSUEDATE >= '''+CONVERT(VARCHAR,@fromDate)+''' 
        AND ISSUEDATE <= '''+CONVERT(VARCHAR,@ToDate)+'''' 
				   
	SET @whereProperty = 
		' WHERE IsActive = 1 AND ( ( FIRSTNAME + '' '' 
            + LASTNAME ) LIKE ''%' + @searchText + '%'' 
        OR ( HOUSENUMBER + '' '' + ADDRESS1 + '', '' 
            + TOWNCITY ) LIKE ''%' + @searchText + '%'' ) 
        AND ISSUEDATE >= '''+CONVERT(VARCHAR,@fromDate)+''' 
        AND ISSUEDATE <= '''+CONVERT(VARCHAR,@ToDate)+''''

	SET @whereBlock = 
		' WHERE IsActive = 1 AND ( FIRSTNAME + '' '' 
		+ LASTNAME LIKE ''%' + @searchText + '%'' 
		OR HOUSENUMBER + '' '' + ADDRESS1 + '', '' 
			+ TOWNCITY LIKE ''%' + @searchText + '%'' ) 
		AND ISSUEDATE >= '''+CONVERT(VARCHAR,@fromDate)+''' 
        AND ISSUEDATE <= '''+CONVERT(VARCHAR,@ToDate)+''''

	IF(@heatingTypeId <> -1)
	BEGIN
		set @whereProperty = 
		@whereProperty + '
			AND FuelId = ' + CONVERT(varchar, (@heatingTypeId)) + ''
			 
			Set @whereScheme = @whereScheme + '
			AND FuelId = ' + CONVERT(varchar, (@heatingTypeId)) + ''
			 
			set @whereBlock = @whereBlock + '
			AND FuelId = ' + CONVERT(varchar, (@heatingTypeId)) + '' 
	END

      --========================================================================================          
      -- Begin building SELECT clause  
      -- Insert statements for procedure here  
      SET @selectClause = 'SELECT *' 
      SET @SelectClauseScheme = 'SELECT *' 
      SET @SelectClauseBlock = 'SELECT *' 
      -- End building SELECT clause  
      --========================================================================================               
      --========================================================================================      
      -- Begin building FROM clause  
      SET @fromClause = Char(10) 
                        + 'FROM #TempIssuedCertificateReport'  + @whereProperty

						print (@fromClause)
      SET @fromClauseScheme = Char(10) 
                              + 'FROM #TempIssuedCertificateReportScheme' + @whereScheme
      SET @fromClauseBlock = Char(10) 
                             + 'FROM #TempIssuedCertificateReportBlock' + @whereBlock
      SET @unionQuery = Char(10) + ' UNION ALL ' + Char(10) 

	  

      -- End building From clause  
      --========================================================================================                               
      --========================================================================================      
      -- Begin building OrderBy clause      
      -- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
      IF( @sortColumn = 'Address' ) 
        BEGIN 
            SET @sortColumn = Char(10) + 'HOUSENUMBER,ADDRESS1,TOWNCITY' 
        END 

      IF( @sortColumn = 'By' ) 
        BEGIN 
            SET @sortColumn = Char(10) + 'FIRSTNAME,LASTNAME' 
        END 

      IF( @sortColumn = 'Issued' ) 
        BEGIN 
            SET @sortColumn = Char(10) + 'ISSUEDATE' 
        END 

      SET @orderClause = Char(10) + ' Order By ' + @sortColumn + Char(10) 
                         + @sortOrder 
      -- End building OrderBy clause  
      --========================================================================================                                                                      
      --========================================================================================
      SET @mainSelectQuery = 'SELECT * FROM ( ' + @selectClause 
                             + @fromClause + @unionQuery 
                             + @SelectClauseScheme + @fromClauseScheme 
                             + @unionQuery + @SelectClauseBlock 
                             + @fromClauseBlock + ' ) tmpTable ' 
                             + @orderClause + ' OFFSET ' 
                             + CONVERT(NVARCHAR, @offset) 
                             + ' ROWS FETCH NEXT ' 
                             + CONVERT(NVARCHAR, @pageSize) + ' ROWS ONLY ' 

     -- PRINT @mainSelectQuery 

      EXEC (@mainSelectQuery) 

      -- End - Execute the Query   
      --========================================================================================                  
      --========================================================================================
      -- Begin building Count Query   
      DECLARE @selectCount  NVARCHAR(max), 
              @parameterDef NVARCHAR(max) 

      SET @parameterDef = '@totalCount int OUTPUT'; 
      SET @selectCount = 'SELECT @totalCount =  ( select count(*) FROM (' 
                         + @selectClause + @fromClause + @unionQuery 
                         + @SelectClauseScheme + @fromClauseScheme 
                         + @unionQuery + @SelectClauseBlock 
                         + @fromClauseBlock + ') as Records ) ' 

      --print @selectCount  
      EXECUTE Sp_executesql 
        @selectCount, 
        @parameterDef, 
        @totalCount output; 
  -- End building the Count Query  
  --========================================================================================    
  END 
GO

