USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_PopulateServiceChargePropertyList') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_PopulateServiceChargePropertyList AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_PopulateServiceChargePropertyList] 
	@schemeId		 INT=null, 
	@blockId		 INT=null,  
	@itemId		 INT=null,  
	@ItemDetail    AS PDR_ServiceChargePropertyList readonly ,
	@attributeChildId INT=NULL
AS
BEGIN

	DECLARE   
            @propertyId	NVARCHAR(MAX),
			@serviceListId INT=0

	IF ((@blockId IS NOT NULL AND @blockId > 0) AND (@schemeId IS NULL OR @schemeId <= 0))
	BEGIN

		DELETE 
		FROM	PDR_ServiceChargeProperties
		WHERE	BlockId=@blockId 
				AND	itemId=@itemId 
				AND (ChildAttributeMappingId = @attributeChildId OR ChildAttributeMappingId IS NULL)

		INSERT	INTO PDR_ServiceChargeProperties(BlockId, PropertyId, IsIncluded, itemId, ChildAttributeMappingId,ISActive)
		SELECT	@blockId ,PropertyId,IsIncluded,@itemId,@attributeChildId,1 
		FROM	@ItemDetail

	END
	ELSE
		BEGIN
		
		DELETE 
		FROM	PDR_ServiceChargeProperties
		WHERE	SchemeId=@schemeId 
				AND	itemId=@itemId 
				AND (ChildAttributeMappingId = @attributeChildId OR ChildAttributeMappingId IS NULL)

		INSERT	INTO PDR_ServiceChargeProperties(SchemeId, PropertyId, IsIncluded, itemId, ChildAttributeMappingId,ISActive)
		SELECT	@schemeId ,PropertyId,IsIncluded,@itemId,@attributeChildId,1 
		FROM	@ItemDetail

		END

END	

