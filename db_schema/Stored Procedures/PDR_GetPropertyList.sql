USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyList]    Script Date: 10/06/2015 18:56:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description: Get propertyList on PropertyList.aspx
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date                   By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza         Get propertyList on PropertyList.aspx 
    v1.1		Oct 6 2015		Raja Aneeq			Add four new columns 'MV-T','EUV-SH','MV-VP','Tenure' 
													and remove 'propertyValue' column				
  =================================================================================*/
  
IF OBJECT_ID('dbo.[PDR_GetPropertyList]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[PDR_GetPropertyList] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[PDR_GetPropertyList]
	-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),	
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'PropertyId', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
	
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ISNULL(P.HouseNumber, '''') + '' '' + ISNULL(P.ADDRESS1, '''') + '' '' + ISNULL(P.ADDRESS2, '''') + '' '' + ISNULL(P.ADDRESS3, '''')  LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR S.SCHEMENAME  LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.BLOCKNAME  LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P.PROPERTYID LIKE ''%' + @searchText + '%'') '
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		P.PROPERTYID AS PropertyId,
		ISNULL(P.HouseNumber, '''') + '' '' + ISNULL(P.ADDRESS1, '''') + '' '' + ISNULL(P.ADDRESS2, '''') + '' '' + ISNULL(P.ADDRESS3, '''')AS Address,
		ISNULL(S.SchemeName,''-'') AS Scheme,
		ISNULL(B.BLOCKNAME,''-'') AS Block ,
		''�'' + convert(varchar(10), ISNULL(F.OMV,0)) as MVVP,
		''�'' + convert(varchar(10), ISNULL(F.EUV,0))as EUVSH,   
		''�'' + convert(varchar(10), ISNULL(F.OMVST,0)) as MVT ,
		ISNULL(CONVERT(nvarchar(50),DateDiff(DAY,P.LeaseStart,P.LeaseEnd))+  '' Days'',''N/A'') AS Tenure '
		
		
		
		
		--============================From Clause============================================
		SET @fromClause = 'FROM P__PROPERTY P
							Left JOIN P_BLOCK B ON  P.BLOCKID = B.BLOCKID
							LEFT JOIN P_SCHEME S On P.SCHEMEID = S.SCHEMEID 
							Left JOIN P_FINANCIAL F ON P.PropertyId = F.PropertyId'
							
							
		--============================Order Clause==========================================
	IF @sortColumn = '' OR @sortOrder =''
		BEGIN	
		SET @sortColumn ='PropertyId'
		SET @sortOrder ='DESC'
		END
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		PRINT (@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
											
END
