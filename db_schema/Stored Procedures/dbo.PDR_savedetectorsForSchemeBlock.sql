USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[Df_savedetectors]    Script Date: 17/04/2017 13:16:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_savedetectorsForSchemeBlock') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_savedetectorsForSchemeBlock AS SET NOCOUNT ON;') 
GO

Alter PROCEDURE [dbo].[PDR_savedetectorsForSchemeBlock] 
								 (@SchemeId          int=null,
								  @BlockId			int= null,
                                  @detectorType        NVARCHAR(10), 
                                  @Location            NVARCHAR(400), 
                                  @Manufacturer        NVARCHAR(200), 
                                  @SerialNumber        NVARCHAR(200), 
                                  @PowerSource         INT = NULL, 
                                  @InstalledDate       DATETIME=NULL, 
                                  @InstalledBy         INT = NULL, 
                                  @IsLandlordsDetector BIT = NULL, 
                                  @TestedDate          DATETIME = NULL, 
                                  @BatteryReplaced     DATETIME = NULL, 
                                  @Passed              BIT = NULL, 
                                  @Notes               NVARCHAR(2000),
								  @smokeDetectorType   NVARCHAR(100),
								  @noOfSmokeDetector   NVARCHAR(100), 
                                  @saveStatus          BIT output) 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from 
      -- interfering with SELECT statements. 
      SET nocount ON; 

      BEGIN TRANSACTION 

      BEGIN try 
          DECLARE @DetectorTypeId INT 

          SELECT @DetectorTypeId = detectortypeid 
          FROM   as_detectortype 
          WHERE  as_detectortype.detectortype LIKE '%' + @detectorType + '%' 

          INSERT INTO p_detector 
                      (PropertyId, 
                       DetectorTypeId, 
                       Location, 
                       Manufacturer, 
                       SerialNumber, 
                       PowerSource, 
                       InstalledDate, 
                       InstalledBy, 
                       IsLandlordsDetector, 
                       TestedDate, 
                       BatteryReplaced, 
                       Passed, 
                       Notes,
					   SmokeDetectorType,
					   NumberOfSmokeDetectors,
					   SchemeId,
					   BlockId) 
          VALUES      ('', 
                       @DetectorTypeId, 
                       @Location, 
                       @Manufacturer, 
                       @SerialNumber, 
                       @PowerSource, 
                       @InstalledDate, 
                       @InstalledBy, 
                       @IsLandlordsDetector, 
                       @TestedDate, 
                       @BatteryReplaced, 
                       @Passed, 
                       @Notes,
					   @smokeDetectorType,
					   @noOfSmokeDetector,
					   @SchemeId,
					   @BlockId) 
      END try 

      BEGIN catch 
          IF @@TRANCOUNT > 0 
            BEGIN 
                ROLLBACK TRANSACTION; 

                SET @saveStatus = 0 
            END 

          DECLARE @ErrorMessage NVARCHAR(4000); 
          DECLARE @ErrorSeverity INT; 
          DECLARE @ErrorState INT; 

          SELECT @ErrorMessage = Error_message(), 
                 @ErrorSeverity = Error_severity(), 
                 @ErrorState = Error_state(); 

          -- Use RAISERROR inside the CATCH block to return  
          -- error information about the original error that  
          -- caused execution to jump to the CATCH block. 
          RAISERROR (@ErrorMessage,-- Message text. 
                     @ErrorSeverity,-- Severity. 
                     @ErrorState -- State. 
          ); 
      END catch; 

      IF @@TRANCOUNT > 0 
        BEGIN 
            COMMIT TRANSACTION; 

            SET @saveStatus = 1 
        END 
  END 
  
  