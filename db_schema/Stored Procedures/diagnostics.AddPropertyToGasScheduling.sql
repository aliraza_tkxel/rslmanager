SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [diagnostics].[AddPropertyToGasScheduling]
	@PropertyId VARCHAR(20),
	@EmployeeId INT
AS

BEGIN

	DECLARE @JournalId INT
	DECLARE @Count INT

	SET @Count = (SELECT COUNT(*) FROM [dbo].[AS_JOURNAL] AS aj	WHERE AJ.[PROPERTYID] = @PropertyId) + 
				 (SELECT COUNT(*) FROM [dbo].[AS_JOURNALHISTORY] AS aj WHERE aj.[PROPERTYID] =	@PropertyId) + 
				 (SELECT COUNT(*) FROM dbo.P_LGSR WHERE PROPERTYID = @PropertyId)
				 
	SELECT * FROM [dbo].[AS_JOURNAL] AS aj	WHERE AJ.[PROPERTYID]	= @PropertyId
	SELECT * FROM [dbo].[AS_JOURNALHISTORY] AS aj WHERE aj.[PROPERTYID] =	@PropertyId
	SELECT * FROM dbo.P_LGSR WHERE PROPERTYID = @PropertyId
				 
	IF @Count > 0 
	BEGIN
		RAISERROR('Records already exists.', 16, 1)
		RETURN	
	END

	INSERT INTO [dbo].[AS_JOURNAL]
			( [PROPERTYID] ,
			  [STATUSID] ,
			  [ACTIONID] ,
			  [INSPECTIONTYPEID] ,
			  [CREATIONDATE] ,
			  [CREATEDBY] ,
			  [ISCURRENT]
			)
	VALUES  ( @PropertyId , -- PROPERTYID - nvarchar(20)
			  1 , -- STATUSID - int - to be arranged
			  NULL , -- ACTIONID - int
			  1 , -- INSPECTIONTYPEID - smallint
			  GETDATE() , -- CREATIONDATE - smalldatetime
			  @EmployeeId , -- CREATEDBY - int
			  1  -- ISCURRENT - bit
			)
	        
	SET @JournalId = SCOPE_IDENTITY()

	INSERT INTO [dbo].[AS_JOURNALHISTORY]
			( [JOURNALID] ,
			  [PROPERTYID] ,
			  [STATUSID] ,
			  [ACTIONID] ,
			  [INSPECTIONTYPEID] ,
			  [CREATIONDATE] ,
			  [CREATEDBY] ,
			  [NOTES] ,
			  [ISLETTERATTACHED] ,
			  [StatusHistoryId] ,
			  [ActionHistoryId] ,
			  [IsDocumentAttached]
			)
	VALUES  ( @JournalId , -- JOURNALID - int
			  @PropertyId , -- PROPERTYID - nvarchar(20)
			  1 , -- STATUSID - int
			  NULL , -- ACTIONID - int
			  1 , -- INSPECTIONTYPEID - smallint
			  GETDATE() , -- CREATIONDATE - smalldatetime
			  @EmployeeId , -- CREATEDBY - int
			  N'' , -- NOTES - nvarchar(1000)
			  NULL , -- ISLETTERATTACHED - bit
			  NULL , -- StatusHistoryId - int
			  NULL , -- ActionHistoryId - int
			  0  -- IsDocumentAttached - bit
			)

	IF NOT EXISTS (SELECT * FROM dbo.P_LGSR WHERE PROPERTYID = @PropertyId) 
	BEGIN 

		INSERT INTO dbo.P_LGSR
				( PROPERTYID ,
				  ISSUEDATE ,
				  CP12NUMBER ,
				  CP12ISSUEDBY ,
				  DOCUMENTTYPE ,
				  NOTES ,
				  DTIMESTAMP ,
				  CP12DOCUMENT ,
				  DOCTITLE ,
				  CP12UPLOADEDBY ,
				  CP12UPLOADEDON ,
				  DOCFILE ,
				  BSAVE ,
				  BUPLOAD ,
				  RECEVIEDONBEHALF ,
				  RECEVIEDDATE ,
				  TENANTHANDED ,
				  INSPECTIONCARRIED ,
				  JOURNALID ,
				  ISPRINTED ,
				  IsAppointmentCompleted
				)
		VALUES  ( @PropertyId , -- PROPERTYID - nvarchar(20)
				  '01/01/2014' , -- ISSUEDATE - smalldatetime
				  N'' , -- CP12NUMBER - nvarchar(20)
				  0 , -- CP12ISSUEDBY - int
				  '' , -- DOCUMENTTYPE - char(3)
				  '' , -- NOTES - varchar(200)
				  NULL , -- DTIMESTAMP - smalldatetime
				  NULL , -- CP12DOCUMENT - image
				  N'' , -- DOCTITLE - nvarchar(200)
				  0 , -- CP12UPLOADEDBY - int
				  NULL , -- CP12UPLOADEDON - smalldatetime
				  N'' , -- DOCFILE - nvarchar(50)
				  0 , -- BSAVE - smallint
				  0 , -- BUPLOAD - smallint
				  '' , -- RECEVIEDONBEHALF - varchar(20)
				  NULL , -- RECEVIEDDATE - smalldatetime
				  NULL , -- TENANTHANDED - bit
				  NULL , -- INSPECTIONCARRIED - bit
				  NULL , -- JOURNALID - int
				  0 , -- ISPRINTED - bit
				  0  -- IsAppointmentCompleted - bit
				)


	END

	SELECT * FROM [dbo].[AS_JOURNAL] AS aj	WHERE AJ.[PROPERTYID]	= @PropertyId
	SELECT * FROM [dbo].[AS_JOURNALHISTORY] AS aj WHERE aj.[PROPERTYID] =	@PropertyId
	SELECT * FROM dbo.P_LGSR WHERE PROPERTYID = @PropertyId

	END


GO
