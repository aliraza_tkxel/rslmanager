SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[I_STATIONERY_ORDER_REPORTBY_LINEMANAGERID]
(
	@LMID int,
	@DATEREQUESTEDFROM DATETIME = NULL,
	@DATEREQUESTEDTO DATETIME = NULL,
	@ORDERSTATUS int = NULL,
	@TEAM int = NULL,
	@EMPLOYEE int = NULL,
	@LESSTHAN smallmoney = NULL,
	@GREATERTHAN smallmoney = NULL
)
AS
DECLARE @SQL_OUTER NVARCHAR(4000)
DECLARE @SQL NVARCHAR(4000), @PARAMLIST  NVARCHAR(4000)
                     
SET NOCOUNT ON 
                  
SELECT @SQL = 'SELECT SO.*, OS.OrderStatus, EMPBY.FULLNAME AS RequestedByEmpFullName, EMPBY.TEAMNAME AS RequestedByTeamName, 
						EMPFOR.FULLNAME AS RequestedForEmpFullName, EMPFOR.TEAMNAME AS RequestedForTeamName, EMPFOR.TEAMID AS RequestedForTeamId

						FROM I_STATIONERY_ORDER AS SO

						INNER JOIN VW_EMPLOYEES_INTRANET AS EMPBY ON SO.RequestedByEmpId = EMPBY.EMPLOYEEID
						INNER JOIN VW_EMPLOYEES_INTRANET AS EMPFOR ON SO.RequestedForEmpId = EMPFOR.EMPLOYEEID
						INNER JOIN I_STATIONERY_ORDER_STATUS AS OS ON SO.StatusID = OS.OrderStatusId						
						
						'

SELECT @SQL_OUTER = 'SELECT SO.*, SI.TotalCost
						FROM
						(' + @SQL + '
						) SO
						LEFT OUTER JOIN
						(
							SELECT SI.OrderId, SUM(SI.Cost * SI.Quantity) as TotalCost
							FROM I_STATIONERY_ORDER_ITEM AS SI
							WHERE SI.StatusId <> 4
							GROUP BY SI.OrderId
						) SI ON SO.OrderID = SI.OrderID 
						INNER JOIN VW_EMPLOYEES_INTRANET AS LM ON SO.RequestedByEmpId = LM.EMPLOYEEID

						WHERE (LM.LINEMANAGER = @LMID) AND (SO.StatusId <> 1)
						 ' 


IF @DATEREQUESTEDFROM IS NOT NULL                                            
   SELECT @SQL_OUTER = @SQL_OUTER + ' AND SO.REQUESTEDDATE >= @DATEREQUESTEDFROM '                                                                     

IF @DATEREQUESTEDTO IS NOT NULL     
	SELECT @SQL_OUTER = @SQL_OUTER + ' AND SO.REQUESTEDDATE < DATEADD(day, 1, @DATEREQUESTEDTO) '                                      
   /*SELECT @SQL_OUTER = @SQL_OUTER + ' AND SO.REQUESTEDDATE <= @DATEREQUESTEDTO ' */
    
IF @ORDERSTATUS IS NOT NULL                                            
   SELECT @SQL_OUTER = @SQL_OUTER + ' AND SO.STATUSID = @ORDERSTATUS ' 

IF @TEAM IS NOT NULL                                            
   SELECT @SQL_OUTER = @SQL_OUTER + ' AND RequestedForTeamId = @TEAM '   

IF @EMPLOYEE IS NOT NULL                                            
   SELECT @SQL_OUTER = @SQL_OUTER + ' AND RequestedForEmpId = @EMPLOYEE '   

IF @LESSTHAN IS NOT NULL                                            
   SELECT @SQL_OUTER = @SQL_OUTER + ' AND TotalCost < @LESSTHAN '   

IF @GREATERTHAN IS NOT NULL                                            
   SELECT @SQL_OUTER = @SQL_OUTER + ' AND TotalCost > @GREATERTHAN '   

SELECT @SQL_OUTER = @SQL_OUTER + 'ORDER BY SO.REQUESTEDDATE DESC'

SELECT @PARAMLIST =		'@LMID INT,
						 @DATEREQUESTEDFROM DATETIME,
						 @DATEREQUESTEDTO DATETIME,
						 @ORDERSTATUS INT,
						 @TEAM INT,
						 @EMPLOYEE INT,
						 @LESSTHAN SMALLMONEY,
						 @GREATERTHAN SMALLMONEY'
SET NOCOUNT OFF 

PRINT @SQL_OUTER

EXEC SP_EXECUTESQL @SQL_OUTER, @PARAMLIST,
@LMID,
@DATEREQUESTEDFROM,
@DATEREQUESTEDTO,
@ORDERSTATUS,
@TEAM,
@EMPLOYEE,
@LESSTHAN,
@GREATERTHAN


GO
