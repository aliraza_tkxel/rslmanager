SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_Get_Estimated_Due_HB_Payment] 
	@tenancyId	int
AS
BEGIN

DECLARE @HB_ANT	MONEY
DECLARE @HB_ADV	MONEY
DECLARE @result	int
DECLARE @SQL_CLAUSE VARCHAR(300)
DECLARE @SQL_STR VARCHAR(8000)
DECLARE @THEDATE SMALLDATETIME
DECLARE @REQUIREDDATE SMALLDATETIME
DECLARE @CURRENTDATESTRING VARCHAR (40)
DECLARE @COMPAREDATE SMALLDATETIME
DECLARE @NEXTMONTHDATE SMALLDATETIME
DECLARE @RESPECTIVEENDDATE SMALLDATETIME
DECLARE @YEAREND SMALLDATETIME
DECLARE @YEARSTART SMALLDATETIME
DECLARE @HB_DR MONEY
DECLARE @TENANCYSTART SMALLDATETIME
DECLARE @LASTPAYMENTENDDATE SMALLDATETIME
DECLARE @ADJ_MONTH INT
DECLARE @NON_REQUIRED_DAYS INT
DECLARE @A_12TH_OF_A_YEAR FLOAT
DECLARE @TOTAL_HB_OWED_ON_DATE MONEY
DECLARE @HB_OWED MONEY
DECLARE @ADJ_YEARSTART SMALLDATETIME
DECLARE @ADJ_YEAREND SMALLDATETIME
DECLARE @MASTERAMOUNT MONEY
DECLARE @CUSTOMERID INT
DECLARE @FULLNAME VARCHAR(200)
--DECLARE @TENANCYID varchar(50)

SET @MASTERAMOUNT = 0
SET @THEDATE = GETDATE()
--SET @TENANCYID = 10065

-- START A CURSOR TO LOOP THROUGH THE CUSTOMERS IN EACH TENANCY
--	: WE DO THIOS BECAUSE EACH CUSTOMER MIGHT HAVE THERE OWN HB SET UP
-- 	: WE TRY TO NOT ADD IN AN HB VALUE WHICH HAS NOT BEEN ENDED ON AN ENDED TENANCY WHOS CUSTOMER HAVE SET UP A NEW TENANCY AND HB SCHEDULE

  SET @HB_ANT	= 0
  SET @HB_ADV	= 0


DECLARE CUSTOMERS_HB CURSOR 
	  	    	FAST_FORWARD 
		    	FOR 
				SELECT 	HBI.CUSTOMERID ,T.TENANCYID, HBI.initialSTARTDATE , ISNULL(HBA.ENDDATE,DATEADD(D,-1,HBI.INITIALSTARTDATE)),
					HB_DR = 
					CASE 
						WHEN HBA.HBID IS NOT NULL THEN ((ABS(HBA.HB)/ (DATEDIFF(D,HBA.STARTDATE,HBA.ENDDATE)+1)))
						WHEN HBA.HBID IS NULL THEN ((ABS(HBI.INITIALPAYMENT)/ (DATEDIFF(D,HBI.INITIALSTARTDATE,HBI.INITIALENDDATE)+1)))
					END					
				FROM C_TENANCY T
					INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID
					INNER JOIN F_HBINFORMATION HBI ON HBI.TENANCYID = T.TENANCYID
					LEFT JOIN F_HBACTUALSCHEDULE HBA ON HBA.HBID = HBI.HBID
									AND HBA.HBROW = (SELECT MAX(HBROW) FROM F_HBACTUALSCHEDULE WHERE HBID = HBA.HBID AND VALIDATED = 1)
					LEFT JOIN F_HBACTUALSCHEDULE HBA2 ON HBA2.HBID = HBI.HBID
									AND HBA2.HBROW = (SELECT min(HBROW) FROM F_HBACTUALSCHEDULE WHERE HBID = HBA2.HBID AND VALIDATED is null)
					
				WHERE  HBI.ACTUALENDDATE IS NULL AND T.TENANCYID = @TENANCYID
					AND (((T.ENDDATE IS NULL OR T.ENDDATE  >= @THEDATE ) AND (T.STARTDATE  < @THEDATE)))
		
	OPEN CUSTOMERS_HB
	FETCH 	NEXT FROM CUSTOMERS_HB INTO @CUSTOMERID,@TENANCYID, @TENANCYSTART,@LASTPAYMENTENDDATE, @HB_DR
	  WHILE 	@@FETCH_STATUS = 0
	     BEGIN
				set @MASTERAMOUNT = 0
				
				SET @REQUIREDDATE =  '1 ' + DATENAME(M, GETDATE()) + ' ' + CAST(DATEPART( YYYY, GETDATE()) AS VARCHAR)
				
				SET @CURRENTDATESTRING = '1 ' + DATENAME(M, @TENANCYSTART)
				
				SET @COMPAREDATE = @CURRENTDATESTRING + ' ' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)
				
				SET @NEXTMONTHDATE = DATEADD(M,1,@REQUIREDDATE)
				
				IF (@REQUIREDDATE) >= (@COMPAREDATE)
				 BEGIN
					SET @YEARSTART = @COMPAREDATE					
					SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1 , @YEARSTART)	
					SET @RESPECTIVEENDDATE = DATEADD(D, -1 , @RESPECTIVEENDDATE)
					SET @YEAREND = @RESPECTIVEENDDATE					
				
				 END
				ELSE
				 BEGIN
					SET @YEARSTART = @CURRENTDATESTRING + CAST((DATEPART( YYYY, GETDATE())-1) AS VARCHAR)
					SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1 , @YEARSTART)
					SET @RESPECTIVEENDDATE = DATEADD(D, -1 , @RESPECTIVEENDDATE)				
					SET @YEAREND = @RESPECTIVEENDDATE
				
				 END
				
				SET @ADJ_MONTH = DATEDIFF(M, @YEARSTART, GETDATE())+1
				
				IF (DATEADD(D,1,@LASTPAYMENTENDDATE)) < @YEARSTART
				 BEGIN
					
					SET @NON_REQUIRED_DAYS = 0
					SET @A_12TH_OF_A_YEAR = 0.0
					SET @TOTAL_HB_OWED_ON_DATE = 0
					SET @A_12TH_OF_A_YEAR = (DATEDIFF(D, @YEARSTART, @YEAREND)+1)
					set @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12							
					SET @HB_OWED = ((@ADJ_MONTH * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR
					
					SET @ADJ_YEARSTART = DATEADD(YYYY, -1, @YEARSTART)
					SET @ADJ_YEAREND = DATEADD(YYYY, -1, @YEAREND)
					
					--loop whilst we still have whole years to the LAST_PAYMENT_END
						WHILE (@LASTPAYMENTENDDATE < @ADJ_YEARSTART)
						 BEGIN
							SET @HB_OWED = @HB_OWED + (@HB_DR * (DATEDIFF(D, @ADJ_YEARSTART, @ADJ_YEAREND)+1))			
							SET @ADJ_YEARSTART = DATEADD(YYYY, -1, @ADJ_YEARSTART)
							SET @ADJ_YEAREND = DATEADD(YYYY, -1, @ADJ_YEAREND)

						  END		
						
						SET @NON_REQUIRED_DAYS = DATEDIFF(D, @ADJ_YEARSTART, @LASTPAYMENTENDDATE)
						
						SET @A_12TH_OF_A_YEAR = DATEDIFF(D, @ADJ_YEARSTART, @ADJ_YEAREND)+1
						SET @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR /12
						
						SET @TOTAL_HB_OWED_ON_DATE = @HB_OWED + (((12 * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR)
				 		
						SET @MASTERAMOUNT = @MASTERAMOUNT +  ISNULL(@TOTAL_HB_OWED_ON_DATE,0)
											
						
						SET @TOTAL_HB_OWED_ON_DATE = 0
						SET @HB_OWED = 0.00
				 END
				ELSE
				 BEGIN
					SET @A_12TH_OF_A_YEAR = 0.0
					SET @NON_REQUIRED_DAYS = DATEDIFF(D, @YEARSTART, @LASTPAYMENTENDDATE)+1
					
					SET @A_12TH_OF_A_YEAR = (DATEDIFF(D, @YEARSTART, @YEAREND)+1)
					set @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12
					SET @TOTAL_HB_OWED_ON_DATE = (((@ADJ_MONTH * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR)
					SET @MASTERAMOUNT = @MASTERAMOUNT + ISNULL(@TOTAL_HB_OWED_ON_DATE,0)
					
					SET @TOTAL_HB_OWED_ON_DATE = 0
										
				 END
				
				-- IF ANY HB HAS BEEN PAID FOR THE FUTURE THEN WE NEED TO ADD THIS TO THE GROSS COST
				-- SO THEN WE DONT ADD IT TO THE ANTICPATED HB
				IF ISNULL(@MASTERAMOUNT,0) < 0
				 BEGIN
					SET @HB_ANT	= ISNULL(@HB_ANT,0)
					SET @HB_ADV	= ISNULL(@HB_ADV,0) + ISNULL(abs(@MASTERAMOUNT),0)
					--UPDATE #TBL_ESTHB SET ADVHB = ISNULL(ADVHB,0) + ISNULL(abs(@MASTERAMOUNT),0), ANTHB = ISNULL(ANTHB,0) WHERE TENANCYID = @TENANCYID
				 END
				ELSE
				 BEGIN		
					SET @HB_ANT	= ISNULL(@HB_ANT,0) + ISNULL(ABS(@MASTERAMOUNT),0)
					SET @HB_ADV	= ISNULL(@HB_ADV,0)
					--UPDATE #TBL_ESTHB SET ANTHB = ISNULL(ANTHB,0) + ISNULL(ABS(@MASTERAMOUNT),0), ADVHB = ISNULL(ADVHB,0) WHERE TENANCYID = @TENANCYID
				 END 

			set @MASTERAMOUNT = 0
			FETCH NEXT FROM CUSTOMERS_HB INTO  @CUSTOMERID,@TENANCYID,@TENANCYSTART,@LASTPAYMENTENDDATE, @HB_DR
		END
	--END
	CLOSE CUSTOMERS_HB
	DEALLOCATE CUSTOMERS_HB

SELECT ISNULL(@HB_ANT, 0) as ESTHB

END





GO
