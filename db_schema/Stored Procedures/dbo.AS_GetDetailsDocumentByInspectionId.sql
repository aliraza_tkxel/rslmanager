SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Abdullah Saeed
-- Create date: August 15,2014
-- Description:	Property Inspection Document
-- EXEC AS_GetDetailsDocumentByInspectionId '1'
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetDetailsDocumentByInspectionId]
	@Document int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PropertyId,InspectionDocument,SurveyId
	 from PA_Property_Inspection_Record
	 where InspectionId=@document
END
GO
