USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:<Adeel Ahmed>
-- Create date: <17-06-2010>
-- Description:	<getting the customer info for letter>
-- =============================================
IF OBJECT_ID('dbo.AM_SP_GetCustomerInfoForLetter') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AM_SP_GetCustomerInfoForLetter AS SET NOCOUNT ON;') 
GO

Alter PROCEDURE [dbo].[AM_SP_GetCustomerInfoForLetter]
	@tenantId	int,
	@customerId	int, @addressId int = -1
AS
BEGIN
	Select (SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE AM_Customer_Rent_Parameters.TenancyId = @tenantId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,

				(SELECT TOP 1 ISNULL(Title,'') + ' ' + LEFT(ISNULL(FIRSTNAME, ''), 1)+' '+ ISNULL(LASTNAME, '') as CName
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE AM_Customer_Rent_Parameters.TenancyId = @tenantId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,

				(SELECT Count(Distinct AM_Customer_Rent_Parameters.CustomerId)
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE AM_Customer_Rent_Parameters.TenancyId = @tenantId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						) as JointTenancyCount,
				Case When @addressId != -1 then
					C_Address.HOUSENUMBER
				else
					P__PROPERTY.HOUSENUMBER
				end as HOUSENUMBER,

				Case When @addressId != -1 then
					C_Address.ADDRESS1
				else
					P__PROPERTY.ADDRESS1
				end as ADDRESS1,

				Case When @addressId != -1 then
					C_Address.ADDRESS2
				else
					P__PROPERTY.ADDRESS2
				end as ADDRESS2,

				Case When @addressId != -1 then
					C_Address.ADDRESS3
				else
					P__PROPERTY.ADDRESS3
				end as ADDRESS3,

				Case When @addressId != -1 then
					C_Address.TOWNCITY
				else
					P__PROPERTY.TOWNCITY
				end as TOWNCITY,

				Case When @addressId != -1 then
					null
				else
					P__PROPERTY.COUNTY
				end as COUNTY,

				Case When @addressId != -1 then
					C_Address.POSTCODE
				else
					P__PROPERTY.POSTCODE
				end as POSTCODE
	FROM C__CUSTOMER
	INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID
	INNER JOIN G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleID 
	INNER JOIN C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
	INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
	Left JOIN C_Address ON C_Address.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_Address.ADDRESSID = @addressId
	WHERE 
	C_CUSTOMERTENANCY.TENANCYID = @tenantId and C__CUSTOMER.CUSTOMERID = @customerId
END


--exec [AM_SP_GetCustomerInfoForLetter] 12238,60,2