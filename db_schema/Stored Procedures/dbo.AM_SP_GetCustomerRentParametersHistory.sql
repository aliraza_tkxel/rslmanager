SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AM_SP_GetCustomerRentParametersHistory] 
	
AS
BEGIN

	DELETE FROM AM_Customer_Rent_Parameters_History	

	INSERT INTO AM_Customer_Rent_Parameters_History (TenancyId, CustomerId, Title, FirstName, LastName, CustomerAddress, RentBalance, NextHB, LastPayment, LastPaymentDate, SalesLedgerBalance, EstimatedHBDue,TimeCreated)
	SELECT TenancyId, CustomerId, Title, FirstName, LastName, CustomerAddress, RentBalance, NextHB, LastPayment, LastPaymentDate, SalesLedgerBalance, EstimatedHBDue,GETDATE()
	FROM AM_Customer_Rent_Parameters
END

GO
