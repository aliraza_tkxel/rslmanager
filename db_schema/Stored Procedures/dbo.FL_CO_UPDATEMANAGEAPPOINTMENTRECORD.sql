SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO







CREATE    PROC [dbo].[FL_CO_UPDATEMANAGEAPPOINTMENTRECORD](
/* ===========================================================================
 '   NAME:          FL_CO_UPDATEMANAGEAPPOINTMENTRECORD
 '   DATE CREATED:  29 Dec, 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:   RSL
 '   update appointment in  FL_CO_APPOINTMENT TABLE
 '
 '   IN:	       @appointmentId INT , 
 '   IN:           @stageId INT ,
 '   IN:           @appointmentDate DATETIME,
 '   IN:	       @appointmentTime VARCHAR,
 '   IN:	       @lastActioned DATETIME ,
 '   IN:	       @operativeId INT,
 '   IN:	       @appointmentNotes VARCHAR,
 '   IN:	       @letterId INT,
 '
 '   OUT:          @RESULT    
 '   RETURN:       Nothing    
 '   VERSION:      1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@appointmentId INT,
 @stageId INT,
@appointmentDate NVARCHAR(20) ,
@appointmentTime VARCHAR(12),
@lastActioned NVARCHAR(30) ,
 @operativeId INT,
@appointmentNotes VARCHAR(5000),
@letterId INT,
@RESULT  INT OUTPUT
)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN
DECLARE 
@FaultLogId int,
@JournalId int,
@OrgId int,
@ScopeId int,
@StatusId int,
@ApptDesc nvarchar(50)



UPDATE FL_CO_APPOINTMENT SET 
	AppointmentDate =CONVERT(VARCHAR,@appointmentDate,103), OperativeID=@operativeId, LetterID=@letterId,AppointmentStageID=@stageId, Notes=@appointmentNotes,Time=@appointmentTime
	WHERE AppointmentID= @appointmentId
--Get Appointment Description
if(@stageId=1)
begin
set @ApptDesc='Appointment To Be Arranged'
end
else
if(@stageId=2)
begin
set @ApptDesc='Appointment Arranged'
end
else 
if (@stageId=3)
begin
set @ApptDesc='Appointment Arranged'
end
else
if (@stageId=4)
begin
set @ApptDesc='Appointment Cancelled'
end
--Get Status Id
Set @StatusId=(SELECT FaultStatusID FROM FL_FAULT_STATUS WHERE (Description = @ApptDesc))
--Get FaultLog Id
set @FaultLogId=(select FaultLogId from FL_Fault_Log where JobSheetNumber=(select JobSheetNumber from FL_CO_APPOINTMENT WHERE AppointmentID=@appointmentId))
--Get Journal Id
Set @JournalId=(select JournalId from FL_Fault_Journal where FaultLogId=@FaultLogId)
--Get OrgId
set @OrgId=(select OrgId from Fl_Fault_Log where FaultLogId=@FaultLogId)
--Get Scope Id
set @ScopeId=(select ScopeId from S_Scope where ORGID=@OrgId and AreaOfWork=(select AREAOFWORKID from S_AREAOFWORK where DESCRIPTION='Reactive Repair'))


--Update FL_FAULT_LOG Table
update FL_FAULT_LOG set StatusID=@StatusId where FaultLogId=@FaultLogId

--Update FL_FAULT_JOURNAL Table
update FL_FAULT_JOURNAL set FaultStatusID=@StatusId where JournalId=@JournalId

--Insert into FL_FAULT_LOG_HISTORY
INSERT INTO FL_FAULT_LOG_HISTORY
                      (JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
VALUES     (@JournalId,@StatusId,null,GetDate(),null,@FaultLogId,@OrgId,@ScopeId,'','')

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	

SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN

GO
