USE [RSLBHALive]
GO
-- =============================================
-- =============================================
-- Author:		Ali Raza
-- Create date: July 5 2, 2017
-- =============================================

IF OBJECT_ID('dbo.E_GetMyTeamList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_GetMyTeamList AS SET NOCOUNT ON;') 
GO

-- =============================================
ALTER PROCEDURE [dbo].[E_GetMyTeamList] 
	-- Add the parameters for the stored procedure here
	@empId INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
DECLARE @BHSTART SMALLDATETIME,
        @BHEND SMALLDATETIME,
		@JobRole INT

SELECT @BHSTART = ystart,
       @BHEND = yend
FROM f_fiscalyears
ORDER BY yrange ASC

SELECT @JobRole = J.JobRoleId
FROM E_JOBDETAILS J
WHERE J.EMPLOYEEID = @empId

SELECT T.TEAMID,
       T.TEAMNAME,
			  (SELECT Isnull(Cast(Count(*) AS NVARCHAR), '0')
					FROM E__EMPLOYEE E
					LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID
					Inner Join E_JOBROLE JR on j.JobRoleId = jr.JobRoleId
					WHERE J.TEAM = T.TEAMID AND J.ACTIVE = 1 
					AND E.EMPLOYEEID <> @empId
					AND (LINEMANAGER=@empId OR @JobRole = 4)
				) AS TEAMNUM,
			  (SELECT Isnull(Cast(Sum(holidayentitlementdays)AS NVARCHAR), '0')
			   FROM e_jobdetails
			   Inner Join E_JOBROLE on e_jobdetails.JobRoleId = E_JOBROLE.JobRoleId
			   WHERE team = T.teamid
				 AND active = 1
				 AND (LINEMANAGER =@empId or T.DIRECTOR=@empId OR @JobRole = 4)
				 ) AS LEAVE,

			  (SELECT Isnull(Cast(Sum(salary)AS NVARCHAR), '0')
			   FROM e_jobdetails
			   Inner Join E_JOBROLE on e_jobdetails.JobRoleId = E_JOBROLE.JobRoleId
			   WHERE team = T.teamid
				 AND active = 1
				 AND (LINEMANAGER =@empId or T.DIRECTOR=@empId OR @JobRole = 4 )
				 ) AS SALARY,

				Isnull(SickLeaveInfo.SicknessAbsense,0) AS SICKNESSABSENCE,

				Isnull(SickLeaveInfo.SicknessSalary,0) AS SicknessSalary
FROM e_team T

 OUTER Apply
  (SELECT SUM(calculatedSalary) AS SicknessSalary,SUM(SICKNESSABSENCE) AS SicknessAbsense,TEAMID
   FROM
     (SELECT ((Isnull(jd.salary, 0)/52)/5)* Isnull(Sum(A.duration), 0) AS calculatedSalary, Isnull(Sum(A.duration), 0) AS SICKNESSABSENCE, Isnull(jd.salary, 0) AS SALARY,T.TEAMID
      FROM e__employee E
      INNER JOIN e_journal J ON J.employeeid = E.employeeid
      INNER JOIN e_absence A ON A.journalid = J.journalid
      AND A.startdate BETWEEN @BHSTART AND @BHEND
      AND A.absencehistoryid =
        (SELECT Max(absencehistoryid)
         FROM e_absence
         WHERE journalid = A.journalid)
      LEFT JOIN e_jobdetails JD ON JD.employeeid = E.employeeid
      LEFT JOIN e_team IT ON IT.teamid = JD.team
      WHERE itemnatureid = 1
        AND A.itemstatusid NOT IN (20)
        AND duration IS NOT NULL
        AND T.TEAMID = IT.TEAMID
        AND (JD.LINEMANAGER= @empId or IT.DIRECTOR=@empId OR @JobRole = 4)
      GROUP BY IT.teamid, jd.Salary) AS RESULT
   GROUP BY Result.TEAMID)SickLeaveInfo


WHERE T.teamid <> 1
  AND T.active = 1 AND
    (SELECT Isnull(Cast(Count(*) AS NVARCHAR), '0')
     FROM e_jobdetails
	 LEFT JOIN E_TEAM  ON TEAMID = e_jobdetails.TEAM
     WHERE team = E_TEAM.teamid
       AND e_jobdetails.active = 1
       AND (LINEMANAGER =@empId or E_TEAM.DIRECTOR=@empId OR @JobRole = 4)
	   ) >0
  AND T.TEAMID IN (SELECT DISTINCT T.TEAMID
		FROM E__EMPLOYEE E
		INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID
		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM
		WHERE (LINEMANAGER = @empId or DIRECTOR = @empId OR @JobRole = 4) 
		AND T.ACTIVE=1 and jd.active=1
		)
ORDER BY T.TEAMNAME
END
GO
