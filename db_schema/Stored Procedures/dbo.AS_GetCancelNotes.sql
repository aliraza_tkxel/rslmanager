USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[CancelFaultPurchaseOrder]    Script Date: 10/7/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.AS_GetCancelNotes') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCancelNotes AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Rehan Baber
-- Create date: 10/7/2017
-- Description:	Cancels a purchase order on cancelling the respective Assign To Contractor Job Sheet
-- EXEC CancelFaultPurchaseOrder 

-- =============================================
ALTER PROCEDURE [dbo].[AS_GetCancelNotes]
@JournalId int,
@notes nvarchar(max) OUTPUT	
AS
BEGIN

	select top 1 @notes=Notes 
	FROM AS_Cancelled
	where journalid=@JournalId
	order by 1 desc

	select isnull(@notes,'') as notes					
END
