SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[FL_CANCEL_REPORT_GRID]
	(
		@userId int,
		@faultLogID int,
		@reason nvarchar(500),
		@result int output
	)
	
	/* ===========================================================================
 '   NAME:           FL_CANCEL_REPORT_GRID
 '   DATE CREATED:   13 APRIL 2009
 '   CREATED BY:     Tahir Gul 
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist cancelled fault based on search criteria provided
 '   IN:             @faultLogID, @reason
 '   OUT:            @result
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
AS
	SET NOCOUNT ON 

	Declare
	 @cancelID INT,
	@JournalId int,
        	@ScopeId int,
	@IsApproved bit,
	@itemId int,
	@faultStatus int,
	@faultBasketId INT,
	@orgId INT
	
	Update FL_FAULT_PREINSPECTIONINFO
	SET 
	Approved = 0,
	NOTES = @reason
	WHERE faultlogid = @faultLogID
	
	-----------------------
	
	Select @faultBasketId  = FL_FAULT_LOG.FaultBasketID  
	FROM FL_FAULT_LOG INNER JOIN FL_FAULT_BASKET ON FL_FAULT_LOG.FaultBasketID = FL_FAULT_BASKET.FaultBasketID
	WHERE FL_FAULT_LOG.FaultLogID = @FaultLogId
	
	Update FL_FAULT_BASKET SET CancelReason = @reason Where FaultBasketID = @faultBasketId
	
	---------------------------

	Set @cancelID =  (Select FaultStatusID FROM FL_FAULT_STATUS WHERE Description = 'Cancelled Fault')

	Update FL_FAULT_LOG
	SET
	 StatusID = @cancelID, 
	 UserId = @userId
	Where faultlogid = @faultLogID

	
	UPDATE FL_FAULT_JOURNAL
	SET 
	FaultStatusId = @cancelID
	WHERE faultlogid = @faultLogID
	--End 
	--End 

SELECT @ScopeID=SCOPEID FROM S_SCOPE WHERE ORGID=@OrgId AND AREAOFWORK= (SELECT AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair'))

Select @JournalId = JournalId, @itemId = ItemId FROM FL_FAULT_JOURNAL 
WHERE faultLogid = @faultLogid


INSERT INTO FL_FAULT_LOG_HISTORY(JournalId,FaultStatusId,ItemActionId, LastActionDate, LastActionUserID, FaultLogId,OrgId,ScopeId,Title,Notes)
VALUES(@JournalId,  @cancelID , @itemId, GETDATE(), Null, @FaultLogId, @orgId,@SCOPEID, '', @reason )

IF @@ERROR <> 0 
	set @result = -1 
	Else
	set @result = 1


GO
