
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_UpdateUserData @employeeId = @employeeId ,@userTypeId = 1, @modifiedBy = 5
-- Author:		<Salman Nazir>
-- Create date: <10/02/2012>
-- Description:	<Save user Data after editing >
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdateUserData](
	@employeeId int,
	@userTypeId int,
	@modifiedBy int,
	@SignaturePath varchar(250)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE AS_USER  
	SET UserTypeID =@userTypeId 
		,IsActive=1,ModifiedDate=GETDATE()
		,ModifiedBy = @modifiedBy 
	Where EmployeeId = @employeeId 
	
	Update E__EMPLOYEE 
	Set SIGNATUREPATH = @SignaturePath
	Where EmployeeId = @employeeId
	
END
GO
