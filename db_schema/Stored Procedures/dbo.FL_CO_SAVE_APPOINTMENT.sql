SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE     PROC [dbo].[FL_CO_SAVE_APPOINTMENT](
/* ===========================================================================
 '   NAME:         [FL_CO_SAVE_APPOINTMENT]
 '   DATE CREATED:  30 December 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   Saves an Appointment with Time and Date in FL_CO_APPOINTMENT table
 '   
 '   IN:	   @JSNUMBER,
 '   IN:           @APPOINTMENTDATE
 '   IN:           @OPERATIVEID
 '   IN:	   @LETTERID
 '   IN:	   @APPOINTMENTSTAGEID
 '   IN:	   @LASTACTIONDATE
 '   IN:	   @NOTES
 '   IN:	   @APPOINTMENTTIME

 '   OUT:			  @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:         1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@JSNUMBER NVARCHAR(20),
@APPOINTMENTDATE NVARCHAR(20) ,
@OPERATIVEID INT,
@LETTERID INT,
@APPOINTMENTSTAGEID INT ,
@LASTACTIONDATE NVARCHAR(20),
@NOTES NVARCHAR(500),
@APPOINTMENTTIME AS NVARCHAR(50),
@RESULT  INT OUTPUT
)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN

DECLARE @AppointmentArrangedStatusId INT
DECLARE @AppointmentStageDescription VARCHAR(20)
DECLARE @FaultLogId INT
DECLARE @JournalId INT
DECLARE @OrgId INT
DECLARE @ScopeId INT

--Get the description of Stage 
SET @AppointmentStageDescription = (SELECT StageName FROM FL_CO_APPOINTMENT_STAGE WHERE AppointmentStageID = @APPOINTMENTSTAGEID)

--Get & Set the Appointment Arranged
SET @AppointmentArrangedStatusId = (SELECT FAULTSTATUSID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Appointment Arranged')

--Get Fault Log ID
Set @FaultLogId=(select FaultLogId from FL_FAULT_LOG where JobSheetNumber=@JSNUMBER)

--Get Journal ID
Set @JournalId=(select JournalId from FL_Fault_Journal where FaultLogId=@FaultLogId)

--Get ORGID
Set @OrgId=(select ORGID from FL_Fault_Log where FaultLogId=@FaultLogId)


--Get ScopeId
set @ScopeId=(SELECT SCOPEID FROM S_SCOPE WHERE (ORGID = @OrgId) AND (AREAOFWORK = 62))
INSERT INTO FL_CO_APPOINTMENT
                      (JobSheetNumber, AppointmentDate, OperativeID, LetterID, AppointmentStageID, LastActionDate, Notes, [Time])
VALUES     (@JSNUMBER,@APPOINTMENTDATE,@OPERATIVEID,@LETTERID,@APPOINTMENTSTAGEID,CONVERT(DateTime,@LASTACTIONDATE,103),@NOTES,@APPOINTMENTTIME)

update FL_FAULT_LOG SET StatusID=@AppointmentArrangedStatusId where JobSheetNumber=@JSNUMBER
update FL_FAULT_JOURNAL SET FaultStatusID=@AppointmentArrangedStatusId where FaultLogId=@FaultLogId
INSERT INTO FL_FAULT_LOG_HISTORY
                      (JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
VALUES     (@JournalId,@AppointmentArrangedStatusId,null,GetDate(),null,@FaultLogId,@OrgId,@ScopeId,'','')


-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	

SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN



GO
