SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_ABANDON_ENQUIRYLOG_REPORTABANDON]

/* ===========================================================================
 '   NAME:           TO_ABANDON_ENQUIRYLOG_REPORTABANDON
 '   DATE CREATED:   13 JUNE 2008
 '   CREATED BY:     Zahid Ali
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display Enquiry Detail i.e. ABANDON Part of C_Address fields 

are also selected
                     as subset as these need to be displayed on View enquiry pop up.  
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
            TO_LOOKUP_VALUE.Name As NameText,
            TO_ABANDONED.LOCATION,
			C_ADDRESS.HOUSENUMBER, 
			C_ADDRESS.ADDRESS1, 
			C_ADDRESS.ADDRESS2, 
			C_ADDRESS.ADDRESS3, 
			C_ADDRESS.TOWNCITY, 
            C_ADDRESS.POSTCODE, 
            C_ADDRESS.COUNTY, 
            C_ADDRESS.TEL 
            
	FROM
	       TO_ENQUIRY_LOG INNER JOIN TO_ABANDONED 
	       ON TO_ENQUIRY_LOG.EnquiryLogID = TO_ABANDONED.EnquiryLogID
	       INNER JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID 
	       INNER JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	       INNER JOIN TO_LOOKUP_VALUE ON TO_ABANDONED.LOOKUPVALUEID = TO_LOOKUP_VALUE.LOOKUPVALUEID
	       
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)
	     AND (C_ADDRESS.ISDEFAULT = 1) 


GO
