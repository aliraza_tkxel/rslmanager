SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

-- =============================================
--Exec FL_GetEngineersLeaveInformation
--@startDate='08/12/2012',
--@returnDate ='10/12/2012',
--@employeeIds ='34,70'
-- Author:		<Aqib Javed>
-- Create date: <23/01/2013>
-- Description:	<This store procedure shall provide the engineer's leaves information based on appointments>
-- Webpage : SchedulingCalendar.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetEngineersLeaveInformationOld] 
@startDate datetime,
@returnDate datetime,
@employeeIds nvarchar(250)
AS
BEGIN
DECLARE 
        @SelectClause varchar(1000),
        @fromClause   varchar(1000),
        @whereClause varchar(1000),
        @orderClause varchar(500)
	--Getting Employee Leaves Information
	SET  @SelectClause='Select DISTINCT 
								E__EMPLOYEE.EMPLOYEEID AS EMPLOYEEID
								,COALESCE(E_ABSENCE.REASON,E_ABSENCEREASON.DESCRIPTION) AS REASON
								,E_ABSENCE.STARTDATE AS StartDate
								,COALESCE(E_ABSENCE.RETURNDATE,GETDATE()) AS EndDate'

	SET @fromClause=' FROM E_JOURNAL 
								 INNER JOIN E__EMPLOYEE ON E_JOURNAL.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID
								 -- This join is added to just bring Max History Id of every Journal
								 INNER JOIN (SELECT MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryId, JOURNALID 
											FROM E_ABSENCE GROUP BY JOURNALID) AS MaxAbsence ON MaxAbsence.JOURNALID = E_JOURNAL.JOURNALID
								 INNER JOIN E_ABSENCE ON MaxAbsence.MaxAbsenceHistoryId = E_ABSENCE.ABSENCEHISTORYID 
								 INNER JOIN E_STATUS ON E_ABSENCE.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
								 LEFT JOIN E_ABSENCEREASON ON E_ABSENCE.REASONID = E_ABSENCEREASON.SID '

	SET @whereClause=' Where 1 = 1
								 AND (
										-- To filter for planned i.e annual leaves etc. where approval is needed
										( E_STATUS.ITEMSTATUSID = 5
											AND itemnatureid >= 2)
										OR
										-- To filter for sickness leaves. where approval is not needed
										( ITEMNATUREID = 1
											AND E_STATUS.ITEMSTATUSID <> 20 )
									 ) 
	AND ( (E_ABSENCE.STARTDATE>='''+CONVERT(varchar, (@startDate))+''' AND COALESCE(E_ABSENCE.RETURNDATE,GETDATE())<='''+CONVERT(varchar, (@returnDate))+''')' 
		+ ' OR (E_ABSENCE.STARTDATE<='''+CONVERT(varchar, (@returnDate))+''' AND COALESCE(E_ABSENCE.RETURNDATE,GETDATE())>='''+CONVERT(varchar, (@startDate))+''')'
		+ ' OR (E_ABSENCE.STARTDATE<='''+CONVERT(varchar, (@startDate))+''' AND COALESCE(E_ABSENCE.RETURNDATE,GETDATE())>='''+CONVERT(varchar, (@returnDate))+''') )'
	+' AND E__EMPLOYEE.EMPLOYEEID IN('+@employeeIds+')' 

	SET @orderClause=' ORDER BY StartDate'
	print @selectClause 
	print @fromClause
	print @whereClause 
	print @orderClause

	EXEC (@selectClause + @fromClause + @whereClause+@orderClause ) 

END


GO
