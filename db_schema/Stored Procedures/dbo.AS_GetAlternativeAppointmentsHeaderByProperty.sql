USE [RSLBHALive]
GO 
/****** Object:  StoredProcedure [dbo].[AS_GetAlternativeAppointmentsByProperty]    Script Date: 07/12/2018 11:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
 --EXEC AS_GetAlternativeAppointmentsHeaderByProperty
	--		@PropertyId = 'P100280005'
		
-- Author:		<Shehriyar Zafar>
-- Modified By: <Shehriyar, July 12, 2018>
-- Create date: <15 Sep 2012>
-- Description:	<This stored procedure returns the alternative appointments against property id>
-- Parameters:	
		--@PropertyId
-- ============================================= */


IF OBJECT_ID('dbo.AS_GetAlternativeAppointmentsHeaderByProperty') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetAlternativeAppointmentsHeaderByProperty AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetAlternativeAppointmentsHeaderByProperty]
( 
		--These parameters will be used for search
		@PropertyId Nvarchar(50) = ''
)
AS
BEGIN

	Declare @RenewablesTradeId INT

	Select @RenewablesTradeId=TradeId from G_TRADE
	WHERE Description = 'Renewable Qualified'

		SELECT 	DISTINCT						
				Case When ISNULL(PV.ValueDetail, '-') = 'Please Select' Then '-'
				ELSE ISNULL(PV.ValueDetail, '-') end AS Fuel,
				ISNULL(APP.Duration,1) as Duration,
				'Renewable Qualified' AS Trade,
				Case AS_Status.Title WHEN 'No Entry' then 'No Entry ('+ convert(varchar(10),
					(SELECT Count(JOURNALHISTORYID)
					FROM AS_JournalHistory 
					Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
					AND AS_JournalHistory.StatusId=3 AND 
					YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
					))+')' 
				WHEN 'Aborted' then 'Aborted ('+ convert(varchar(10),
					(SELECT Count(JOURNALHISTORYID)
					FROM AS_JournalHistory 
					Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
					AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE 'Aborted') AND 
					YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
					))+')' 
				else 
					AS_Status.Title end AS Status,
					PV.ValueID as HeatingTypeId,
					@RenewablesTradeId as TradeId

	FROM P__PROPERTY 
				INNER JOIN PA_HeatingMapping HM on hm.PropertyId = P__PROPERTY.PROPERTYID
				INNER JOIN PA_PARAMETER_VALUE PV on PV.ValueID = Hm.HeatingType
				INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HM.HeatingMappingId AND HM.IsActive = 1
				INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID
				LEFT JOIN AS_APPOINTMENTS APP ON APP.JOURNALID = dbo.AS_JOURNAL.JOURNALID								
				INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
				LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
				LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
				INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
				LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
				INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
																			
	WHERE 
		
				1=1 
				AND (AS_JOURNAL.STATUSID  IN (select StatusId from AS_Status WHERE Title LIKE 'Appointment to be arranged' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted' OR TITLE LIKE 'Cancelled'))		
				AND AS_JOURNAL.IsCurrent = 1
				AND dbo.P__PROPERTY.STATUS IN (SELECT STATUSID FROM P_STATUS WHERE DESCRIPTION IN('Let','Available to rent','Unavailable')) 
				AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
				AND PV.IsAlterNativeHeating = 1
				AND HM.IsActive = 1
				AND ServicingTypeId = (Select ServicingTypeID FROM P_ServicingType WHERE Description = 'Alternative Servicing')
				AND dbo.P__PROPERTY.PROPERTYID = @PropertyId						
END


