USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[TS_GetTimeSheetAnalysisReport]    Script Date: 2/13/2017 10:21:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.[TS_GetTimeSheetAnalysisReport]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[TS_GetTimeSheetAnalysisReport] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[TS_GetTimeSheetAnalysisReport]
	-- Add the parameters for the stored procedure here
	@fromDate		DATETIME
	,@toDate		DATETIME
	,@searchText	NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
PRINT 'CREATE TABLE #TimeSheetTempTable'
	-- Insert statements for procedure here

	DECLARE	@OperativeId AS INT
			,@AvailableHours AS FLOAT
			,@RepairJobs AS INT
			,@RepairHours FLOAT
			,@FaultMatrialHours DECIMAL(9,2) = 0
			,@AbsentHoursSick FLOAT
			,@AbsentHoursOther FLOAT
			,@FuelServicingHours FLOAT
			,@FuelServicingJobs INT
			,@PlannedHours FLOAT
			,@PlannedJobs INT
			,@VoidHours FLOAT
			,@VoidJobs INT
			,@PlannedMatrialHours DECIMAL(9,2) = 0
			,@MaterialHours DECIMAL(9,2)
			,@TotalWorkedHours FLOAT
			,@AbsentHoursTotal FLOAT
			
			
	--=========================================================================
	-- CALCULATE WEEKDAY WISE COUNT BETWEEN 2 DATES
	--=========================================================================	
	
	CREATE TABLE #DaysOfWeekTempTable (
		day_number INT
		,day_name NVARCHAR(20)
		,days_count INT		
	)
	
	SET DATEFIRST 1

	;WITH Days_Of_The_Week AS (
		SELECT 1 AS day_number, 'Monday' AS day_name UNION ALL
		SELECT 2 AS day_number, 'Tuesday' AS day_name UNION ALL
		SELECT 3 AS day_number, 'Wednesday' AS day_name UNION ALL
		SELECT 4 AS day_number, 'Thursday' AS day_name UNION ALL
		SELECT 5 AS day_number, 'Friday' AS day_name UNION ALL
		SELECT 6 AS day_number, 'Saturday' AS day_name UNION ALL
		SELECT 7 AS day_number, 'Sunday' AS day_name
	)
	INSERT INTO #DaysOfWeekTempTable (day_number, day_name, days_count)
	SELECT
		day_number, day_name,
		1 + DATEDIFF(wk, @fromDate, @toDate) -
			CASE WHEN DATEPART(weekday, @fromDate) > day_number THEN 1 ELSE 0 END -
			CASE WHEN DATEPART(weekday, @toDate)   < day_number THEN 1 ELSE 0 END AS days_count
	FROM
		Days_Of_The_Week



	--=========================================================================
	-- Create Temp Table and fetch employee detail/operative for calculation
	--=========================================================================

	CREATE TABLE #TimeSheetTempTable (
		operative NVARCHAR(100)
		,availableHours FLOAT DEFAULT 0.0
		,operativeId INT
		,repairsHours FLOAT DEFAULT 0.0
		,repairJobs INT DEFAULT 0
		,fuelServicingHours FLOAT DEFAULT 0.0
		,fuelServicingJobs INT DEFAULT 0
		,plannedHours FLOAT DEFAULT 0.0
		,plannedJobs INT DEFAULT 0
		,voidHours FLOAT DEFAULT 0.0
		,voidJobs INT DEFAULT 0
		,materialsHours FLOAT DEFAULT 0.0
		,absentHoursSick FLOAT DEFAULT 0.0
		,absentHoursOther FLOAT DEFAULT 0.0
		,totalWorkedHours FLOAT DEFAULT 0.0
		,absentHoursTotal FLOAT DEFAULT 0.0
	)

	INSERT INTO #TimeSheetTempTable (operativeId, operative)
	SELECT
			E__EMPLOYEE.EMPLOYEEID	AS operativeId
			,E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME	AS operative

	FROM	AS_USER 
			INNER JOIN	AS_USERTYPE ON AS_USER.UserTypeID = AS_USERTYPE.UserTypeID 
			INNER JOIN	E__EMPLOYEE ON AS_USER.EmployeeId = E__EMPLOYEE.EMPLOYEEID 
			INNER JOIN	E_JOBDETAILS ON AS_USER.EmployeeId = E_JOBDETAILS.EMPLOYEEID 
	WHERE	AS_USER.IsActive=1 
			AND E_JOBDETAILS.ACTIVE = 1
			AND (E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME LIKE '%' + @searchText + '%')
	ORDER BY E__EMPLOYEE.FIRSTNAME, E__EMPLOYEE.LASTNAME
		
	
	--=========================================================================
	-- CALCULATE WEEKDAY WISE COUNT BETWEEN 2 DATES
	--=========================================================================	
		
	SELECT
		@OperativeId = MIN(operativeId) 
	FROM
		#TimeSheetTempTable
	
	WHILE @OperativeId IS NOT NULL
	BEGIN
	
		DECLARE @extraTime as float =0.0
		DECLARE @coreWorkingTime float = 0.0
				
		--==============================
		-- CALCULATE EXTENDED HOURS
		--==============================
		
		SELECT	@extraTime = ISNULL (SUM(DATEDIFF(MINUTE, STARTTIME ,ENDTIME)/60.0),0.0) 
		FROM	E_OUT_OF_HOURS  
		WHERE	EmployeeId = @OperativeId 
				AND (STARTDATE >= @fromDate  and ENDDATE <= @toDate)
		
		--==============================
		-- CALCULATE CORE HOURS
		--==============================
		
		SELECT	@coreWorkingTime = CONVERT(DECIMAL(10,2),SUM (#DaysOfWeekTempTable.days_count * 
												CASE 
													WHEN ECWH.EMPLOYEEID IS NULL AND (day_name = 'Saturday' OR day_name = 'Sunday') THEN
														0.0
													WHEN ECWH.EMPLOYEEID IS NULL THEN
														8.5
													ELSE
														DATEDIFF(MINUTE,ECWH.STARTTIME ,ECWH.ENDTIME)/60.0
													END))
															 
		FROM	#DaysOfWeekTempTable
				LEFT JOIN E_CORE_WORKING_HOURS ECWH ON #DaysOfWeekTempTable.day_number = ECWH.DAYID 
				AND ECWH.EMPLOYEEID = @OperativeId

		SET @AvailableHours = ISNULL(@coreWorkingTime,0.0) + @extraTime

		--==============================
		-- CALCULATE REPAIR HOURS & JOBS
		--==============================
		
		SELECT
			@repairHours = CONVERT(DECIMAL(10,2),SUM(DATEDIFF(mi, FL_FAULT_JOBTIMESHEET.StartTime, FL_FAULT_JOBTIMESHEET.EndTime)) / 60.0)
			,@repairJobs = COUNT(DISTINCT FL_FAULT_JOBTIMESHEET.FaultLogId)
		FROM
			FL_FAULT_JOBTIMESHEET
				JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_JOBTIMESHEET.APPOINTMENTID
		WHERE
			(FL_CO_APPOINTMENT.OperativeID = @operativeID)
			AND (FL_FAULT_JOBTIMESHEET.StartTime >= @fromDate)
			AND (FL_FAULT_JOBTIMESHEET.EndTime <= @toDate)

		IF	@repairHours IS NULL
			SET @repairHours = 0

		IF	@repairJobs IS NULL
			SET @repairJobs = 0
			
		--=======================================================================================================
		-- CALCULATE FUEL SERVICING HOURS & JOBS
		-- NOTE : Currently no table exist to track appointment actual start and end time that is why appointment 
		-- start and end time in AS_APPOINTMENTS has been used in ticket # 9542. It is legacy issue
		--=======================================================================================================

		SELECT
			@FuelServicingHours = CONVERT(DECIMAL(10,2),SUM(DATEDIFF(MINUTE, ASJ.StartTime, asj.EndTime)) / 60.0)
			,@FuelServicingJobs = COUNT(DISTINCT AA.APPOINTMENTID)
		FROM
			AS_APPOINTMENTS AS AA
			INNER JOIN AS_JOBTIMESHEET ASJ on ASJ.AppointmentId = AA.APPOINTMENTID
		WHERE
			AA.ASSIGNEDTO = @OperativeId
			AND AA.appointmentstatus NOT IN ('NotStarted','No Entry')
			AND CONVERT(DATE, AA.APPOINTMENTDATE) BETWEEN @fromDate AND @toDate

		IF	@FuelServicingHours IS NULL
			SET @FuelServicingHours = 0

		IF	@FuelServicingJobs IS NULL
			SET @FuelServicingJobs = 0
			
			
		--=========================================================================
		-- CALCULATE PLANNED HOURS & JOBS
		--=========================================================================

		SELECT
			@PlannedHours = CONVERT(DECIMAL(10,2),SUM(DATEDIFF(MINUTE, PJ.StartTime,PJ.EndTime)) / 60.0)
			,@PlannedJobs = COUNT(DISTINCT PA.APPOINTMENTID)
		FROM
			PLANNED_APPOINTMENTS AS PA
				INNER JOIN PLANNED_JOBTIMESHEET AS PJ ON PA.APPOINTMENTID = PJ.APPOINTMENTID
		WHERE
			PA.assignedto = @OperativeId
			AND CONVERT(DATE, PJ.StartTime) >= @fromDate
			AND CONVERT(DATE,PJ.EndTime) <= @toDate

		IF	@PlannedHours IS NULL
			SET @PlannedHours = 0

		IF	@PlannedJobs IS NULL
			SET @PlannedJobs = 0
		
		--=======================================================================================================
		-- CALCULATE VOID HOURS & JOBS
		-- NOTE : Currently no table exist to track appointment actual start and end time that is why appointment 
		-- start and end time in PDR_APPOINTMENTS has been used in ticket # 9542. It is legacy issue
		--=======================================================================================================			
				
		--SELECT	@VoidHours = CONVERT(DECIMAL(10,2),SUM(V_RequiredWorks.Duration))
		--		,@VoidJobs = COUNT(DISTINCT V_RequiredWorks.RequiredWorksId)
		--FROM	V_RequiredWorks
		--		INNER JOIN PDR_APPOINTMENTS ON V_RequiredWorks.WorksJournalId = PDR_APPOINTMENTS.JOURNALID 
		--WHERE	PDR_APPOINTMENTS.Assignedto = @OperativeId
		--		AND V_RequiredWorks.Duration IS NOT NULL
		--		AND PDR_APPOINTMENTS.appointmentstatus NOT IN ('NotStarted','No Entry')
		--		AND CONVERT(DATE, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE) >= @fromDate
		--		AND CONVERT(DATE, PDR_APPOINTMENTS.APPOINTMENTENDDATE) <= @toDate

		SELECT	@VoidHours = CONVERT(DECIMAL(10,2),SUM(DATEDIFF(MINUTE, js.StartTime,js.EndTime)) / 60.0)
				,@VoidJobs = COUNT(DISTINCT PDR_APPOINTMENTS.APPOINTMENTID)
		FROM	PDR_APPOINTMENTS
				--INNER JOIN PDR_APPOINTMENTS ON V_RequiredWorks.WorksJournalId = PDR_APPOINTMENTS.JOURNALID 
				INNER JOIN V_JOBTIMESHEET js on js.AppointmentId = PDR_APPOINTMENTS.APPOINTMENTID
				INNER JOIN PDR_JOURNAL j ON PDR_APPOINTMENTS.JOURNALID = j.JOURNALID 				
				INNER JOIN PDR_MSAT M ON j.MSATID=M.MSATId
				INNER JOIN PDR_MSATType MT ON M.MSATTypeId = MT.MSATTypeId  AND js.MSATType= MT.MSATTypeId
		WHERE	PDR_APPOINTMENTS.Assignedto = @OperativeId
				--AND V_RequiredWorks.Duration IS NOT NULL
				AND PDR_APPOINTMENTS.appointmentstatus NOT IN ('NotStarted','No Entry')
				AND CONVERT(DATE, js.StartTime) >= @fromDate
				AND CONVERT(DATE, js.EndTime) <= @toDate
				AND MT.MSATTypeName LIKE '%Void%' 

		IF	@VoidHours IS NULL
			SET @VoidHours = 0

		IF	@VoidJobs IS NULL
			SET @VoidJobs = 0

		--=========================================================================
		-- Getting absent hour sick and absent hour other for a single operative
		--=========================================================================

		SELECT @AbsentHoursOther = ISNULL(SUM(AbsenceHoursOthers),0)

		FROM	(SELECT
		AbsenceDate,  
		--CASE WHEN Sum(AbsenceHoursOthers) > 8 THEN
		--						 8
		--						ELSE
		--						Sum(AbsenceHoursOthers)
		--						END 
		MAX(AbsenceHoursOthers)	AS AbsenceHoursOthers
								
		FROM
			E_ABSENCEHOURS
		WHERE
			EmployeeID = @OperativeId
			AND AbsenceDate >= @fromDate
			AND AbsenceDate <= @toDate
		GROUP BY AbsenceDate) EmployeeAbsenceHours


		SELECT
			@AbsentHoursSick = ISNULL(SUM(AbsenceHoursSick),0)			
		FROM
			E_ABSENCEHOURS
		WHERE
			EmployeeID = @OperativeId
			AND AbsenceDate >= @fromDate
			AND AbsenceDate <= @toDate

		--=========================================================================
		-- Geting Fault/Reactive Matrial Hours
		--=========================================================================
		SELECT
			@FaultMatrialHours = ROUND(SUM(DATEDIFF(MI, FFJ1.EndTime, FFJ2.StartTime)) / 60.0, 2)
		FROM
			FL_CO_APPOINTMENT AS FCA
				INNER JOIN FL_FAULT_APPOINTMENT AS FFA ON FCA.AppointmentID = FFA.AppointmentId
				INNER JOIN
					(
						SELECT
							ROW_NUMBER() OVER (PARTITION BY FFJ.FaultLogId ORDER BY FFJ.FaultLogId DESC)	AS [Row]
							,FFJ.TimeSheetID
							,FFJ.FaultLogId
							,FFJ.StartTime
							,FFJ.EndTime
							,FFJ.APPOINTMENTID
							,FP.Reason
						FROM
							FL_FAULT_JOBTIMESHEET AS FFJ
							LEFT JOIN FL_FAULT_PAUSED FP ON FP.PauseID = FFJ.PauseID
						WHERE
							CONVERT(DATE, FFJ.StartTime) >= @fromDate
							AND (CONVERT(DATE, FFJ.EndTime) <= @toDate OR FFJ.EndTime IS NULL)
					) AS FFJ1 ON FFJ1.APPOINTMENTID = FFA.AppointmentId
				LEFT JOIN
					(
						SELECT
							ROW_NUMBER() OVER (PARTITION BY FFJ.FaultLogId ORDER BY FFJ.FaultLogId DESC)	AS [Row]
							,FFJ.TimeSheetID
							,FFJ.FaultLogId
							,FFJ.StartTime
							,FFJ.EndTime
							,FFJ.APPOINTMENTID							
						FROM
							FL_FAULT_JOBTIMESHEET AS FFJ							
						WHERE
							CONVERT(DATE, FFJ.StartTime) >= @fromDate
							AND (CONVERT(DATE, FFJ.EndTime) <= @toDate OR FFJ.EndTime IS NULL)
					) AS FFJ2 ON FFJ2.[Row] = FFJ1.[Row] + 1
					AND
					FFJ2.FaultLogId = FFJ1.FaultLogId
		WHERE
			FFJ1.[Row] IS NOT NULL
			AND FFJ1.EndTime <= FFJ2.StartTime
			AND FFJ1.Reason LIKE 'Parts Required'
			--AND CONVERT(DATE, FFJ.EndTime) = CONVERT(DATE, FFJ1.StartTime) --This is avoid matrial hour calculation when day is changed.
			AND FCA.OperativeID = @OperativeId

		IF	@FaultMatrialHours IS NULL
			SET @FaultMatrialHours = 0

		--=========================================================================
		-- Geting Planned Matrial Hours
		--=========================================================================

		SELECT
			@PlannedMatrialHours = ROUND(SUM(ISNULL(DATEDIFF(MI, PJ1.EndTime, PJ2.StartTime), 0)) / 60.0, 2)
		FROM
			PLANNED_APPOINTMENTS AS PA
				INNER JOIN
					(
						SELECT
							ROW_NUMBER() OVER (PARTITION BY PJ.APPOINTMENTID ORDER BY PJ.APPOINTMENTID DESC)	AS [Row]
							,PJ.TimeSheetID
							,PJ.APPOINTMENTID
							,PJ.StartTime
							,PJ.EndTime
							,PP.Reason							
						FROM
							PLANNED_JOBTIMESHEET AS PJ
							LEFT JOIN PLANNED_PAUSED PP ON PP.PauseID = PJ.PauseID
						WHERE
							CONVERT(DATE, PJ.StartTime) >= @fromDate
							AND (CONVERT(DATE, PJ.EndTime) <= @toDate OR PJ.EndTime IS NULL)
					) AS PJ1 ON PJ1.APPOINTMENTID = PA.AppointmentId
				LEFT JOIN
					(
						SELECT
							ROW_NUMBER() OVER (PARTITION BY PJ.APPOINTMENTID ORDER BY PJ.APPOINTMENTID DESC)	AS [Row]
							,PJ.TimeSheetID
							,PJ.APPOINTMENTID
							,PJ.StartTime
							,PJ.EndTime							
						FROM
							PLANNED_JOBTIMESHEET AS PJ							
						WHERE
							CONVERT(DATE, PJ.StartTime) >= @fromDate
							AND (CONVERT(DATE, PJ.EndTime) <= @toDate OR PJ.EndTime IS NULL)
					) AS PJ2 ON PJ2.[Row] = PJ1.[Row] + 1
					AND
					PJ2.APPOINTMENTID = PJ1.APPOINTMENTID
		WHERE
			PJ1.[Row] IS NOT NULL
			AND PJ1.EndTime <= PJ2.StartTime
			AND PJ1.Reason LIKE 'Parts Required'
			--AND CONVERT(DATE, FFJ.EndTime) = CONVERT(DATE, FFJ1.StartTime) --This is avoid matrial hour calculation when day is changed.
			AND PA.ASSIGNEDTO = @OperativeId

		IF	@PlannedMatrialHours IS NULL
			SET @PlannedMatrialHours = 0

		--=========================================================================
		-- Getting Total Matrial hours
		--=========================================================================

		SET @MaterialHours = @FaultMatrialHours + @PlannedMatrialHours

		--=========================================================================
		-- Getting Total Worked hours
		--=========================================================================

		SET @TotalWorkedHours = @RepairHours + @FuelServicingHours + @PlannedHours + @VoidHours + @MaterialHours

		--=========================================================================
		-- Getting Total Absent Hours
		--=========================================================================

		SET @AbsentHoursTotal = @AbsentHoursSick + @AbsentHoursOther

		--=========================================================================
		-- Update the values in timeSheetTemoTable for current operative
		--=========================================================================
		
		
		PRINT '================='
		PRINT 'EMPLOYEE = ' + CONVERT(NVARCHAR(100),@OperativeId)
		PRINT  'CORE = ' + CONVERT(NVARCHAR(100),@coreWorkingTime)
		PRINT  'EXTRA = ' + CONVERT(NVARCHAR(100),@extraTime)
		PRINT  'Fault Material hours = ' + CONVERT(NVARCHAR(100),@FaultMatrialHours)
		PRINT  'Planned Material hours = ' + CONVERT(NVARCHAR(100),@PlannedMatrialHours)
		PRINT  'Void hours = ' + CONVERT(NVARCHAR(100),@VoidHours)		
		PRINT '================='
		

		UPDATE
			#TimeSheetTempTable
		SET
			availableHours = @AvailableHours
			,repairsHours = @RepairHours
			,repairJobs = @repairJobs
			,fuelServicingHours = @FuelServicingHours
			,fuelServicingJobs = @FuelServicingJobs
			,plannedHours = @PlannedHours
			,plannedJobs = @PlannedJobs
			,voidHours = @VoidHours
			,voidJobs = @VoidJobs
			,materialsHours = @MaterialHours
			,absentHoursOther = @AbsentHoursOther
			,absentHoursSick = @AbsentHoursSick
			,totalWorkedHours = @TotalWorkedHours
			,absentHoursTotal = @AbsentHoursTotal
		WHERE
			operativeId = @OperativeId

		SELECT
			@OperativeId = MIN(operativeId)
		FROM
			#TimeSheetTempTable
		WHERE
			operativeId > @OperativeId

	END

	SELECT
		*
		,ROUND((totalWorkedHours - availableHours), 2)	AS [difference]
		,(totalWorkedHours + absentHoursTotal)			AS totalHours
		,(CASE
			WHEN availableHours = 0
				THEN 0
			ELSE ROUND((totalWorkedHours / availableHours) * 100, 2)
		END
		)												AS percentage
	FROM
		#TimeSheetTempTable

	DROP TABLE #DaysOfWeekTempTable
	DROP TABLE #TimeSheetTempTable

END

GO

