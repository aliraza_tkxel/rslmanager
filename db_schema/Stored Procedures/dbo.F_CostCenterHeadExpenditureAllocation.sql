USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePhotograph]    Script Date: 12/01/2016 06:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[F_CostCenterHeadExpenditureAllocation]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[F_CostCenterHeadExpenditureAllocation] AS SET NOCOUNT ON;') 
GO 

ALTER PROCEDURE [dbo].[F_CostCenterHeadExpenditureAllocation] 
AS  
BEGIN  

	BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @RunningDate DATETIME SET @RunningDate = getDate()

	DECLARE @YStartDate DATETIME DECLARE @YEndDate DATETIME

	SET @YStartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, @RunningDate)) - 4)%12), @RunningDate ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, @RunningDate)) - 4)%12),@RunningDate ))+1 ) ) 
	SET @YEndDate = DATEADD(dd,-1,DATEADD(mm,12,@YStartDate ))

	DECLARE @YRange int=0,@IsInsertRequired bit=0

	IF EXISTS(Select 1 from F_FISCALYEARS  where YStart = @YStartDate)
		BEGIN
			Select @YRange=YRange from F_FISCALYEARS  where YStart = @YStartDate		
			SET @IsInsertRequired = 0
		END
	ELSE
		BEGIN
		SET @YRange = (Select MAX( YRange) from F_FISCALYEARS) + 1
			INSERT INTO F_FISCALYEARS(YRange,YStart,YEnd)
			Values(@YRange,@YStartDate,@YEndDate)
		SET @IsInsertRequired = 1
		END	

	IF @IsInsertRequired = 1
		BEGIN

		---====== INSERT INTO F_COSTCENTRE_ALLOCATION =================================
		INSERT INTO F_COSTCENTRE_ALLOCATION(COSTCENTREID,FISCALYEAR,MODIFIED,MODIFIEDBY,COSTCENTREALLOCATION,ACTIVE)
		SELECT COSTCENTREID,@YRange,GETDATE(),423,COSTCENTREALLOCATION,1 FROM F_COSTCENTRE_ALLOCATION WHERE FISCALYEAR=@YRange-1 and ACTIVE=1

		---====== INSERT INTO F_HEAD_ALLOCATION =================================
		INSERT INTO F_HEAD_ALLOCATION(FISCALYEAR,HEADID,HEADALLOCATION,MODIFIEDBY,MODIFIED,ACTIVE)
		SELECT @YRange,HEADID,HEADALLOCATION,423,GetDate(),1 FROM F_HEAD_ALLOCATION WHERE FISCALYEAR=@YRange-1 and ACTIVE=1


		INSERT INTO F_EXPENDITURE_ALLOCATION(EXPENDITUREID,FISCALYEAR,EXPENDITUREALLOCATION,MODIFIED,MODIFIEDBY,ACTIVE)
		SELECT EXPENDITUREID,@YRange,EXPENDITUREALLOCATION,GetDate(),423,1 FROM F_EXPENDITURE_ALLOCATION WHERE FISCALYEAR=@YRange-1 and ACTIVE=1

		END
	
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
	END TRY

	BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState );
	Print (@ErrorMessage)
	END CATCH

END



