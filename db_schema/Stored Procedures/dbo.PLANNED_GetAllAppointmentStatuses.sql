
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC PLANNED_GetAllAppointmentStatuses
-- Author:		<Ahmed Mehmood>
-- Create date: <23/10/2013>
-- Description:	<Returns All Appointment statuses>
-- Webpage: ReplacementList.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetAllAppointmentStatuses] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN


SELECT	PLANNED_STATUS.STATUSID as StatusId, PLANNED_STATUS.TITLE as StatusTitle				
FROM	PLANNED_STATUS 

END
GO
