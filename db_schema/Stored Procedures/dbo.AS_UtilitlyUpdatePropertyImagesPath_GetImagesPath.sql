
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,Ali Raza>
-- Create date: <Create Date,30/11/2013>
-- Description:	<Description,Get property Images Data>
-- =============================================
CREATE PROCEDURE [dbo].[AS_UtilitlyUpdatePropertyImagesPath_GetImagesPath]
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here
SELECT
	SID,PROPERTYID,ImagePath,ImageName,'gas' [Type]
	FROM PA_PROPERTY_ITEM_IMAGES  where ImagePath <> 'C:\inetpub\sysdata\bha.rslmanager\PropertyImages\'
UNION ALL
	SELECT ImagesId [SID],PROPERTYID,ImagePath,ImageName,'stock' [Type]
	FROM PS_Survey_Item_Images
	INNER JOIN PS_Survey ON PS_Survey.SurveyId = PS_Survey_Item_Images.SurveyId
	where ImagePath <> 'C:\inetpub\sysdata\bha.rslmanager\PropertyImages\'
UNION ALL 
	SELECT PropertyDefectImageId [SID],PROPERTYID,ImagePath,ImageTitle ImageName,'defect'		[Type]
	FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
	INNER JOIN P_PROPERTY_APPLIANCE_DEFECTS ON P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId = P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.PropertyDefectId
Where ImagePath  <> 'C:\inetpub\sysdata\bha.rslmanager\PropertyImages\'	
  
END
GO
