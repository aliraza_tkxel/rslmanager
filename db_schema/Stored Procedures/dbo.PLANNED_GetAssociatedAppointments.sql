USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetAssociatedAppointments]    Script Date: 4/10/2016 3:14:33 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:		Altamish Arif
-- Create date: 3/10/2016
-- Description:	Get reason related to cancel condition.
-- =============================================

IF OBJECT_ID('dbo.[PLANNED_GetAssociatedAppointments]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetAssociatedAppointments] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetAssociatedAppointments]
@journalId INT = -1
AS
BEGIN
	SELECT APPOINTMENTID
	FROM PLANNED_APPOINTMENTS
	WHERE JournalId = @journalId
	AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
END