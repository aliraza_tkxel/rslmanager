SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[I_NEWS_INSERT]
(
	@NewsCategory int,
	@Headline varchar(250),
	@ImagePath varchar(250),
	@DateCreated smalldatetime,
	@NewsContent text,
	@Rank int,
	@Active bit,
	@HomePageImage varchar(250),
	@IsHomePage bit,
	@IsTenantOnline bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [I_NEWS] ([NewsCategory], [Headline], [ImagePath], [DateCreated], [NewsContent], [Rank], [Active], [HomePageImage], [IsHomePage],[IsTenantOnline]) VALUES (@NewsCategory, @Headline, @ImagePath, @DateCreated, @NewsContent, @Rank, @Active, @HomePageImage, @IsHomePage,@IsTenantOnline);
	
SELECT NewsID, NewsCategory, Headline, ImagePath, DateCreated, NewsContent, Rank, Active, HomePageImage, IsHomePage,IsTenantOnline FROM I_NEWS WHERE (NewsID = SCOPE_IDENTITY())



GO
