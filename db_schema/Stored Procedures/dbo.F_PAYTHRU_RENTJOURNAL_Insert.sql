SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[F_PAYTHRU_RENTJOURNAL_Insert] 
    @TENANCYID int,
    @TRANSACTIONDATE smalldatetime,
    @ITEMTYPE int,
    @PAYMENTTYPE int,
    @PAYMENTSTARTDATE smalldatetime,
    @PAYMENTENDDATE smalldatetime,
    @AMOUNT money,
    @ISDEBIT int,
    @STATUSID int,
    @ACCOUNTTIMESTAMP datetime,
    @QBSENT INT
AS 
	SET NOCOUNT ON 
	
    INSERT INTO [dbo].[F_RENTJOURNAL]
            ( [TENANCYID] ,
              [TRANSACTIONDATE] ,
              [ITEMTYPE] ,
              [PAYMENTTYPE] ,
              [PAYMENTSTARTDATE] ,
              [PAYMENTENDDATE] ,
              [AMOUNT] ,
              [ISDEBIT] ,
              [STATUSID] ,
              [ACCOUNTTIMESTAMP] ,
              [QBSENT]
            )
            VALUES( 
					@TENANCYID ,
                    @TRANSACTIONDATE ,
                    @ITEMTYPE ,
                    @PAYMENTTYPE ,
                    @PAYMENTSTARTDATE ,
                    @PAYMENTENDDATE ,
                    @AMOUNT ,
                    @ISDEBIT ,
                    @STATUSID ,
                    @ACCOUNTTIMESTAMP ,
                    @QBSENT
			)

	SELECT SCOPE_IDENTITY() AS JOURNALID




GO
