USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetBlockDetail]    Script Date: 1/10/2017 7:01:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

IF OBJECT_ID('dbo.[PDR_GetAllContractors]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetAllContractors] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetAllContractors]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT O.ORGID AS [id],
	NAME AS [description]
	FROM S_ORGANISATION O
	WHERE O.ORGACTIVE = 1 order by Name
END
