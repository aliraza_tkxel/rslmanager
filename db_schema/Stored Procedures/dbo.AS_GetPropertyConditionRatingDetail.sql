SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC	[dbo].[AS_GetPropertyConditionRatingDetail]
--		@propertyId = N'A010060001'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,, 5 August, 2014>
-- Description:	<Description,,This procedure 'll get the Property Condition Rating Detail >
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPropertyConditionRatingDetail] 
	@propertyId as varchar(20)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
	SELECT	CASE WHEN PCI.PARAMETERID IS NOT NULL THEN
				PL.LocationName +' => '+ PA.AreaName +' => '+ I.ItemName
				ELSE		
				PL.LocationName +' => '+ PA.AreaName
		END AS Location
		,PC.COMPONENTNAME AS Component
		,ISNULL(COMPONENT_CONDITION.ValueDetail,'-') AS Condition
		,ISNULL(CONVERT(VARCHAR,YEAR(COMPONENT_DATES.LastDone)),'-') AS LastReplaced
		,ISNULL(CONVERT(VARCHAR,YEAR(COMPONENT_DATES.DueDate)),'-') AS DueDate
		,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), '-') AS [By]
	FROM	PLANNED_COMPONENT PC
		INNER JOIN PLANNED_COMPONENT_ITEM PCI ON PC.COMPONENTID = PCI.COMPONENTID
		INNER JOIN PA_ITEM I ON PCI.ITEMID = I.ItemID 
		LEFT JOIN PA_PARAMETER PP ON PCI.PARAMETERID = PP.ParameterID
		INNER JOIN PA_AREA PA ON I.AreaID = PA.AreaID 
		INNER JOIN PA_LOCATION PL ON PA.LocationId= PL.LocationID
		LEFT JOIN (	SELECT	ComponentId,ValueDetail, PPA.UPDATEDBY AS UPDATEDBY 
					FROM	PLANNED_CONDITIONWORKS PCW
							INNER JOIN PA_PROPERTY_ATTRIBUTES PPA ON PCW.AttributeId = PPA.ATTRIBUTEID 
							INNER JOIN PA_PARAMETER_VALUE PPV ON  PPA.VALUEID = PPV.ValueID
					WHERE	PPA.PROPERTYID= @propertyId) COMPONENT_CONDITION ON PC.COMPONENTID = COMPONENT_CONDITION.ComponentId 
		--LEFT JOIN (	SELECT	PID.PLANNED_COMPONENTID, LastDone , DueDate 
		--			FROM	PA_PROPERTY_ITEM_DATES PID
		--			WHERE	PLANNED_COMPONENTID IS NOT NULL
		--					AND PROPERTYID = @propertyId) COMPONENT_DATES ON PC.COMPONENTID = COMPONENT_DATES.PLANNED_COMPONENTID 
		LEFT JOIN E__EMPLOYEE E ON COMPONENT_CONDITION.UPDATEDBY = E.EMPLOYEEID
		
		LEFT JOIN ( SELECT	PLANNED_COMPONENTID,MAX(SID) AS SUB_SID
						FROM	PA_PROPERTY_ITEM_DATES
						WHERE   PROPERTYID = @propertyId
						GROUP BY PLANNED_COMPONENTID )  AS PID_SUB ON PC.COMPONENTID = PID_SUB.PLANNED_COMPONENTID
		LEFT JOIN	PA_PROPERTY_ITEM_DATES COMPONENT_DATES ON PID_SUB.SUB_SID = COMPONENT_DATES.SID
	
	UNION ALL
	
		SELECT	PL.LocationName +' => '+ PA.AreaName AS Location
				,I.ItemName AS Component
				,ISNULL(PPA.ValueDetail,'-') AS Condition
				,ISNULL(CONVERT(VARCHAR,YEAR(ITEM_LAST_DONE.LastDone)),'-') AS LastReplaced
				,ISNULL(CONVERT(VARCHAR,YEAR(ITEM_DUE_DATE.DueDate)),'-') AS DueDate
				,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), '-') AS [By]
						 
		FROM	PA_ITEM_PARAMETER AS PIP
				INNER JOIN	PA_PARAMETER AS PP ON PIP.ParameterId = PP.ParameterID 
				INNER JOIN	PA_ITEM AS I ON PIP.ItemId = I.ItemID
				INNER JOIN	PA_AREA AS PA ON I.AreaID = PA.AreaID 
				INNER JOIN	PA_LOCATION AS PL ON PA.LocationId = PL.LocationID	
				LEFT JOIN	(	SELECT	PIP.ItemId,ValueDetail, PPA.UPDATEDBY AS UPDATEDBY 
								FROM	PA_PROPERTY_ATTRIBUTES PPA
										INNER JOIN PA_ITEM_PARAMETER PIP ON PIP.ItemParamID = PPA.ITEMPARAMID
										INNER JOIN PA_PARAMETER PP ON PIP.ParameterId = PP.ParameterID  
										INNER JOIN PA_PARAMETER_VALUE PPV ON  PPA.VALUEID = PPV.ValueID
								WHERE	PPA.PROPERTYID = @propertyId
										AND PP.ParameterName = 'Condition Rating'
								) AS PPA ON PIP.ItemId = PPA.ItemId
				LEFT JOIN	(	SELECT	ItemId , LastDone 
								FROM	PA_PROPERTY_ITEM_DATES PID
								WHERE	PROPERTYID = @propertyId
										AND (ParameterId IS NULL OR ParameterId = 0  OR ParameterId IN (SELECT DISTINCT ParameterID FROM pa_parameter where parametername like '%last%'))
								) ITEM_LAST_DONE ON PIP.ItemId = ITEM_LAST_DONE.ItemId
				LEFT JOIN	(	SELECT	ItemId , DueDate
								FROM	PA_PROPERTY_ITEM_DATES PID
								WHERE	PROPERTYID = @propertyId
										AND (ParameterId IS NULL OR ParameterId = 0  OR ParameterId IN (SELECT DISTINCT ParameterID FROM pa_parameter where parametername like '%due%'))
								) ITEM_DUE_DATE ON PIP.ItemId = ITEM_DUE_DATE.ItemId    
				LEFT JOIN E__EMPLOYEE E ON PPA.UPDATEDBY = E.EMPLOYEEID
								
		WHERE	PP.ParameterName = 'Condition Rating'
				AND PIP.IsActive =1
				AND PP.IsActive = 1
				AND I.IsActive =1
				AND PIP.ItemId NOT IN ( SELECT  DISTINCT ITEMID 
										FROM	PLANNED_COMPONENT_ITEM)
		
	
		
		
END
GO
