USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetCndtWorksApprovedCount]    Script Date: 27/04/2017 12:13:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PLANNED_GetCndtWorksApprovedCount]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetCndtWorksApprovedCount] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetCndtWorksApprovedCount] 
/* ===========================================================================
-- NAME:		PLANNED_GetCndtWorksApprovedCount
-- EXEC			[dbo].[PLANNED_GetCndtWorksApprovedCount] -1
-- Author:		<Muhammad Awais>
-- Create date: <03/02/2015>
-- Description:	<Get Replacement Due Current Year Count>
-- Web Page:	Dashboard.aspx
'==============================================================================*/
(
	@componentId int = -1
)
AS
Begin
	DECLARE @sqlCommand varchar(1000)

	SET @sqlCommand = char(10) + 'Select COUNT([PCW].ConditionWorksId) AS TOTALCOUNT
						From PLANNED_CONDITIONWORKS [PCW] 
						INNER JOIN	PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID
						INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID
						INNER JOIN PA_PARAMETER [PARAM] ON [PARAM].ParameterID = IP.ParameterId AND PARAM.ParameterName LIKE ''%Condition Rating%'' AND ([ATT].PARAMETERVALUE = ''Unsatisfactory'' OR [ATT].PARAMETERVALUE = ''Potentially Unsatisfactory'' OR [ATT].PARAMETERVALUE = ''Satisfactory'')
						INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = [ATT].VALUEID AND (PV.ValueDetail = ''Unsatisfactory'' OR PV.ValueDetail = ''Potentially Unsatisfactory'' OR PV.ValueDetail = ''Satisfactory'')
						INNER JOIN PLANNED_JOURNAL [PJ] on [PJ].JournalId = [PCW].JournalId
						INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId  and [PLA].Title = ''Approved'' '
						
						IF @componentId != -1
						BEGIN
							SET @sqlCommand = @sqlCommand + CHAR(10) +' AND [PCW].ComponentId = ' + CONVERT(nvarchar(10), ''+@componentId+'')
						END
	EXEC (@sqlCommand)
END 


