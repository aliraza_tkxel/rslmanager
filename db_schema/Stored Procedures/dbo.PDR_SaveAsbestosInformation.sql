USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveAsbestosInformation]    Script Date: 10/18/2017 17:10:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[PDR_SaveAsbestosInformation]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_SaveAsbestosInformation] AS SET NOCOUNT ON;') 
GO  
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PDR_SaveAsbestosInformation]
@asbestosLevelId int,  
 @asbestosElementId int,  
 @riskId varchar(50),  
 @addedDate date,  
 @removedDate varchar(50),  
 @notes varchar(250),  
 @Id int,  
 @userId int,
  @riskLevel int=null,
 @UrgentActionRequired bit,
 @RequestType varchar(100)  
AS  
BEGIN 

if @removedDate IS NULL OR @removedDate = ''
	Begin
	IF (@RequestType = 'Block')
			BEGIN
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,BlockId,DateAdded,UserID,Notes,RiskLevelId,IsUrgentActionRequired)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@userId,@Notes,@riskLevel,@UrgentActionRequired)  
			END
	ELSE
			BEGIN
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,SchemeId,DateAdded,UserID,Notes,RiskLevelId,IsUrgentActionRequired)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@userId,@Notes,@riskLevel,@UrgentActionRequired)  
			END            
	END
Else
	BEGIN
		IF (@RequestType = 'Block')
			BEGIN	
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,BlockId,DateAdded,DateRemoved,UserID,Notes,RiskLevelId,IsUrgentActionRequired)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@RemovedDate,@userId,@Notes,@riskLevel,@UrgentActionRequired)  
			END
		ELSE
			BEGIN
				Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,SchemeId,DateAdded,DateRemoved,UserID,Notes,RiskLevelId,IsUrgentActionRequired)  
							Values(@asbestosLevelId,@asbestosElementId,@Id,@AddedDate,@RemovedDate,@userId,@Notes,@riskLevel,@UrgentActionRequired)  	
			END	            
	END
	INSERT INTO P_PROPERTY_ASBESTOS_RISK(PROPASBLEVELID,ASBRISKID)  
	VALUES(@@IDENTITY,@riskId)
END	

