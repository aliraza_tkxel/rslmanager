USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetWarrantyData]    Script Date: 16-Oct-17 2:37:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

/* =================================================================================    
    Page Description:  Get Warranty Data

    Author: Salman Nazir
    Creation Date: Dec-25-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-25-2014      Salman Nazir           Get Warranty Data

    Execution Command:

    Exec PDR_GetWarrantyData 1
  =================================================================================*/

IF OBJECT_ID('dbo.PDR_GetWarrantyData') IS NULL 
EXEC('CREATE PROCEDURE dbo.PDR_GetWarrantyData AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetWarrantyData] 
@warrantyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select WARRANTYID,convert(varchar(10), EXPIRYDATE, 103) EXPIRYDATE,CONTRACTOR,NOTES,AREAITEM,WARRANTYTYPE, ISNULL(Category,-1) AS Category, ISNULL(Area,-1) AS Area, ISNULL(Item,-1) AS Item
	from PDR_WARRANTY 
	Where WarrantyId = @warrantyId
END
