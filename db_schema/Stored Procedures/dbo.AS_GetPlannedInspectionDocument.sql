SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	[dbo].[AS_GetPlannedInspectionDocument] @JournalHistoryId = 15
-- Author:		<Ahmed Mehmood>
-- Create date: <October, 17 2014>
-- Description:	<This Stored Proceedure gets Planned Inspection Documents>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPlannedInspectionDocument]

	@JournalHistoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here     
    SELECT	DocumentId, DocumentName, DocumentPath   
	FROM	PLANNED_Documents 
	WHERE	JournalHistoryId  = @JournalHistoryId
	
END
GO
