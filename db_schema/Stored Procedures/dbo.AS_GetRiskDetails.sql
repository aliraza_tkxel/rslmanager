USE [RSLBHALive ]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_GetRiskDetails'))
  DROP PROCEDURE AS_GetRiskDetails

 GO
-- =============================================
-- Author:			Raja Aneeq
-- Create date:		8/2/2016
-- Description:		Display Risk associated with a customer
-- Webpage:			RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx
-- Exec AS_GetRiskDetails 2111
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetRiskDetails]
	@customerId int
AS
BEGIN
	
	
	
	SET NOCOUNT ON;

   SELECT RISKHISTORYID,CATDESC,SUBCATDESC FROM dbo.RISK_CATS_SUBCATS (@customerId)
END
