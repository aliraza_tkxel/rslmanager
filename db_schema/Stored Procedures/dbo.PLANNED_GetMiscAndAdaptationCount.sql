USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetMiscAndAdaptationCount]    Script Date: 27/04/2017 11:29:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PLANNED_GetMiscAndAdaptationCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_GetMiscAndAdaptationCount AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetMiscAndAdaptationCount] 
/* ===========================================================================
--NAME:				PLANNED_GetMiscAndAdaptationCount
--EXEC				[dbo].[PLANNED_GetMiscAndAdaptationCount]	
-- Author:			<Muhammad Awais>
-- Create date:		<10/Mar/2015>
-- Description:		<Get Adaptation Not Scheduled Count>
-- Web Page:		PLANNED -> Dashboard.aspx
'==============================================================================*/
(
	@componentId int = -1,
	@adaptationStatus varchar(100) = 'To Be Arranged',
	@appointmentType varchar(100) = 'Adaptation'
)
AS
	DECLARE @SearchCriteria varchar(500) = ''

	IF @componentId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' WHERE J.ComponentId = ' + CONVERT(nvarchar(10), @componentId) 	 	
		END
	 IF @adaptationStatus = 'Arranged'
	 begin
		 --arranged count
		SELECT COUNT(J.JOURNALID) AS TOTALCOUNT
		FROM PLANNED_JOURNAL J     
		  INNER JOIN PLANNED_APPOINTMENTS ON J.JOURNALID = PLANNED_APPOINTMENTS.JournalId AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS = 'NotStarted'     AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS <> 'Cancelled' 
		  INNER JOIN PLANNED_APPOINTMENT_TYPE ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = PLANNED_APPOINTMENT_TYPE.Planned_Appointment_TypeId    AND PLANNED_APPOINTMENT_TYPE.Planned_Appointment_Type = @appointmentType 
		  INNER JOIN PLANNED_STATUS ON J.STATUSID = PLANNED_STATUS.STATUSID     AND PLANNED_STATUS.TITLE=@adaptationStatus
		+ @searchCriteria
	 end

	 else
	 begin
		 --to be arranged count
		 SELECT COUNT(J.JOURNALID) AS TOTALCOUNT
		 FROM PLANNED_JOURNAL J  
			INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID AND S.TITLE = @adaptationStatus 
			INNER JOIN PLANNED_APPOINTMENT_TYPE PAT ON PAT.Planned_Appointment_TypeId = J.APPOINTMENTTYPEID AND PAT.Planned_Appointment_Type = @appointmentType
		+ @searchCriteria
	 end
	



