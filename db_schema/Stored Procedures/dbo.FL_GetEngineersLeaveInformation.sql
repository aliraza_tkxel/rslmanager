
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
--Exec FL_GetEngineersLeaveInformation
--@startDate='08/12/2012',
--@returnDate ='10/12/2012',
--@employeeIds ='34,70'
-- Author:		<Aqib Javed>
-- Create date: <23/01/2013>
-- Description:	<This store procedure shall provide the engineer's leaves information based on appointments>
-- Webpage : SchedulingCalendar.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetEngineersLeaveInformation]
	@startDate		DATETIME
	,@returnDate	DATETIME
	,@employeeIds	NVARCHAR(250)
AS
BEGIN
	--Getting Employee Leaves Information
	SELECT DISTINCT
		J.EMPLOYEEID										AS EMPLOYEEID
		,COALESCE(N.DESCRIPTION, A.REASON, E_ABSENCEREASON.DESCRIPTION, 'N/A')
		+ CASE
			WHEN DATEPART(HOUR, A.STARTDATE) > 0
				AND DATEPART(HOUR, A.RETURNDATE) > 0
				THEN '<br /> ' + CONVERT(NVARCHAR(5), A.STARTDATE, 108) + ' - '
					+ CONVERT(NVARCHAR(5), A.RETURNDATE, 108)
			ELSE ''
		END													AS REASON
		,CONVERT(DATE, A.STARTDATE)							AS StartDate
		,CONVERT(DATE, COALESCE(A.RETURNDATE, GETDATE()))	AS EndDate
		,J.EMPLOYEEID								AS OperativeId
		,A.HolType
		,A.duration
		,CASE
			WHEN (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
				.STARTDATE)))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
			WHEN HolType = ''
				THEN '08:00 AM'
			WHEN HolType = 'M-M'
				THEN '08:00 AM'
			WHEN HolType = 'M'
				THEN '08:00 AM'
			WHEN HolType = 'A'
				THEN '01:00 PM'
			WHEN HolType = 'F'
				THEN '00:00 AM'
			WHEN HolType = 'F-F'
				THEN '00:00 AM'
			WHEN HolType = 'F-M'
				THEN '00:00 AM'
			WHEN HolType = 'A-F'
				THEN '01:00 PM'
		END													AS StartTime
		,CASE
			WHEN
				(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
				.RETURNDATE)))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
			WHEN HolType = ''
				THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
			WHEN HolType = 'M-M'
				THEN '12:00 PM'
			WHEN HolType = 'M'
				THEN '12:00 PM'
			WHEN HolType = 'A'
				THEN '05:00 PM'
			WHEN HolType = 'F'
				THEN '11:59 PM'
			WHEN HolType = 'F-F'
				THEN '11:59 PM'
			WHEN HolType = 'F-M'
				THEN '12:00 PM'
			WHEN HolType = 'A-F'
				THEN '11:59 PM'
		END													AS EndTime
		,CASE
			WHEN
				(A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.STARTDATE)))
				THEN DATEDIFF(mi, '1970-01-01', A.STARTDATE)
			WHEN HolType = ''
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'M-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'A'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))
			WHEN HolType = 'F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
			WHEN HolType = 'F-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
			WHEN HolType = 'F-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
			WHEN HolType = 'A-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))
		END													AS StartTimeInMin
		,CASE
			WHEN
				(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE)))
				THEN DATEDIFF(mi, '1970-01-01', A.RETURNDATE)
			WHEN HolType = ''
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'M-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'A'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
			WHEN HolType = 'F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
			WHEN HolType = 'F-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
			WHEN HolType = 'F-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'A-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		END													AS EndTimeInMin

	FROM
		E_JOURNAL J
			-- This join is added to just bring Max History Id of every Journal
			INNER JOIN
				(
					SELECT
						MAX(ABSENCEHISTORYID)	AS MaxAbsenceHistoryId
						,JOURNALID
					FROM
						E_ABSENCE
					GROUP BY
						JOURNALID
				) AS MaxAbsence ON MaxAbsence.JOURNALID = J.JOURNALID
			INNER JOIN E_ABSENCE A ON MaxAbsence.MaxAbsenceHistoryId = A.ABSENCEHISTORYID
			INNER JOIN E_STATUS ON A.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
			LEFT JOIN E_ABSENCEREASON ON A.REASONID = E_ABSENCEREASON.SID
			INNER JOIN E_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID
			INNER JOIN dbo.SPLIT_STRING(@employeeIds, ',') Employees ON Employees.COLUMN1 = J.EMPLOYEEID
	WHERE
		(
		-- To filter for planned i.e annual leaves etc. where approval is needed
		(E_STATUS.ITEMSTATUSID = 5
				AND J.ITEMNATUREID >= 2)
			OR
		-- To filter for sickness leaves. where approval is not needed
		(J.ITEMNATUREID = 1
				AND E_STATUS.ITEMSTATUSID <> 20)
		)
		AND ((CONVERT(DATE,A.STARTDATE) >= CONVERT(DATE,@startDate)
				AND CONVERT(DATE,COALESCE(A.RETURNDATE, GETDATE())) <= CONVERT(DATE,@returnDate))
			OR (CONVERT(DATE,A.STARTDATE) <= CONVERT(DATE,@returnDate)
				AND CONVERT(DATE,COALESCE(A.RETURNDATE, GETDATE())) >= CONVERT(DATE,@startDate))
			OR (CONVERT(DATE,A.STARTDATE) <= CONVERT(DATE,@startDate)
				AND CONVERT(DATE,COALESCE(A.RETURNDATE, GETDATE())) >= CONVERT(DATE,@returnDate)))

	ORDER BY StartDate
END
GO
