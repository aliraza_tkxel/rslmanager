
-- =============================================
--EXEC dbo.FL_GetPropertyDetail @jobSheetNumber ='JS81809'
-- Author:		<Junaid Nadeem>
-- Create date: <5/10/2016>
-- Description:	<Returns Property Details>
-- WebPage: ReArrangingCurrentFault.aspx
-- =============================================
IF OBJECT_ID('dbo.FL_GetPropertyDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetPropertyDetail AS SET NOCOUNT ON;') 
GO

ALTER  PROCEDURE [dbo].[FL_GetPropertyDetail] 
	-- Add the parameters for the stored procedure here
	(
	@jobSheetNumber varchar(50)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
-- Property Address  Info
	
	SELECT ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address     
	,P__PROPERTY.TownCity as TownCity      
	,P__PROPERTY.County as County      
	,P__PROPERTY.PostCode as PostCode      
	,E_PATCH.PatchId as PatchId  
	,E_PATCH.Location as PatchName  
	FROM P__PROPERTY      
	LEFT JOIN P_SCHEME ON  P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
	INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID 
	WHERE PROPERTYID =	(
						SELECT  FL_FAULT_LOG.PROPERTYID propertyID
						FROM	FL_FAULT_LOG
						WHERE	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
						)  
				
END


