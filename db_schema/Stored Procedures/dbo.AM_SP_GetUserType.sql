SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adeel Ahmed>
-- Create date: <30/04/2010>
-- Description:	<Finding the user type by user id>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetUserType]
	-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


    -- Insert statements for procedure here
	SELECT AM_LookupCode.CodeName from AM_LookupCode

	INNER JOIN AM_Resource on AM_Resource.LookupCodeId = AM_LookupCode.LookupCodeId 
	WHERE AM_Resource.EmployeeId = @userId
END





GO
