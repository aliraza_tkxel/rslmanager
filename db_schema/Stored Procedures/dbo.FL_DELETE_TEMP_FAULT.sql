SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_DELETE_TEMP_FAULT
	/* ===========================================================================
 '   NAME:           FL_DELETE_TEMP_FAULT
 '   DATE CREATED:   23nd Oct 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the faults
 '   IN:             @TempFaultID
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
	@TempFaultID INT,
	@CustomerID INT,
	@CountRecords INT OUTPUT	
	)	
	
AS 	

Declare @Trecords int 

DELETE FROM FL_TEMP_FAULT WHERE TempFaultID = @TempFaultID

If (@@RowCount = 0)
	BEGIN
		SET @CountRecords =-1		
		RETURN
	END

SELECT @Trecords = Count(*) FROM FL_TEMP_FAULT WHERE CustomerID = @CustomerID 

					
SET @CountRecords = @Trecords














GO
