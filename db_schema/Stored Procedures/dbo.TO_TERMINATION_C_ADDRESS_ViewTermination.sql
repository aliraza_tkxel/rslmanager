SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_TERMINATION_C_ADDRESS_ViewTermination]

/* ===========================================================================
 '   NAME:           TO_TERMINATION_C_ADDRESS_VIEWTERMINATION
 '   DATE CREATED:   06 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display Enquiry Detail i.e. Termination. Part of C_Address fields are also selected
                     as subset as these need to be displayed on View enquiry pop up.  
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
            TO_TERMINATION.MovingOutDate,   
            
			C_ADDRESS.HOUSENUMBER, 
			C_ADDRESS.ADDRESS1, 
			C_ADDRESS.ADDRESS2, 
			C_ADDRESS.ADDRESS3, 
			C_ADDRESS.TOWNCITY, 
            C_ADDRESS.POSTCODE, 
            C_ADDRESS.COUNTY, 
            C_ADDRESS.TEL 
            
            
	FROM
	       TO_ENQUIRY_LOG INNER JOIN TO_TERMINATION 
	       ON TO_ENQUIRY_LOG.EnquiryLogID = TO_TERMINATION.EnquiryLogID
	      INNER JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID 
	       INNER JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	       
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)
	     AND (C_ADDRESS.ISDEFAULT = 1) 
	     

GO
