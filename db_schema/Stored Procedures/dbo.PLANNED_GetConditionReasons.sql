USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetConditionReasons]    Script Date: 10/3/2016 3:14:33 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 14/05/2014
-- Description:	Get reason related to condition.
-- =============================================

IF OBJECT_ID('dbo.[PLANNED_GetConditionReasons]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetConditionReasons] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetConditionReasons]

AS
	SELECT	ReasonId ,Reason 
	FROM	PLANNED_REASON 
	WHERE PLANNED_REASON.CANCELLEDREASON = 0
	ORDER BY ReasonId  ASC

