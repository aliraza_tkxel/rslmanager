SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC RD_GetCompletedFaultDetail
-- @jobSheetNumber ='JS6'
-- Author:		<Ahmed Mehmood>
-- Create date: <16/8/2013>
-- Description:	<Returns Completed Fault Details>
-- Webpage:ReportsArea.aspx
-- =============================================
Create PROCEDURE [dbo].[RD_GetCompletedFaultDetail] 
	-- Add the parameters for the stored procedure here
	(
	@jobSheetNumber varchar(50)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from

	SET NOCOUNT ON;

	-- Job Sheet Detail Customer Information
	EXEC FL_GetJobSheetAndCustomerDetail @jobSheetNumber
	
	-- Repair Detail			
	Select	convert(date, FL_CO_APPOINTMENT.RepairCompletionDateTime,103) as CompletionDate,
			Ltrim(Rtrim(FL_FAULT_REPAIR_LIST.Description)) as Description  			  
	FROM	FL_CO_FAULTLOG_TO_REPAIR 
			INNER JOIN FL_FAULT_REPAIR_LIST on FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID=FL_FAULT_REPAIR_LIST.FaultRepairListID
			INNER JOIN FL_FAULT_LOG ON FL_FAULT_LOG.FaultLogId = FL_CO_FAULTLOG_TO_REPAIR.FaultLogId
			INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FAULTLOGID = FL_FAULT_APPOINTMENT.FAULTLOGID 
			INNER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.APPOINTMENTID = FL_CO_APPOINTMENT.APPOINTMENTID			
	Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
	
	-- Follow on Work List
	SELECT	FL_FAULT_FOLLOWON.FollowOnNotes
	FROM	FL_FAULT_LOG 
		INNER JOIN FL_FAULT_FOLLOWON on FL_FAULT_LOG.FaultLogID = FL_FAULT_FOLLOWON.FaultLogId
	WHERE	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
       				
END
GO
