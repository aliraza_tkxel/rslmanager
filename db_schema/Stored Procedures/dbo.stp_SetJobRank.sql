SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[stp_SetJobRank]

@rank int,
@JobIDarray int

AS

UPDATE H_REQUEST_JOURNAL set rank = @rank
where job_id = @JobIDarray

GO
