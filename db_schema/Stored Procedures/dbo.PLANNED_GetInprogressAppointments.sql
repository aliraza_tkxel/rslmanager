
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetInprogressAppointments] 

/* ===========================================================================
 '   NAME:           PLANNED_GetInprogressAppointments
-- EXEC	[dbo].[PLANNED_GetInprogressAppointments]	
-- Author:		<Ahmed Mehmood>
-- Create date: <10/10/2013>
-- Description:	<Get Properties Inprogress Appointments>
-- Web Page: Dashboard.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@componentId int = -1,
		@operativeId int = -1
	)
		
AS
	
	DECLARE
	 @SelectClause varchar(8000),
	 @FromClause   varchar(8000),
	 @WhereClause  varchar(8000),        
	 @mainSelectQuery varchar(5500),      
	 @SearchCriteria varchar(8000)

         
    SET @SearchCriteria = '	PLANNED_STATUS.TITLE=''Arranged'' 
							AND PLANNED_SUBSTATUS.TITLE IN (''InProgress'',''In Progress'') 
							AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS IN (''InProgress'',''In Progress'') AND'
        
    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND'  
    
    IF NOT @operativeId = -1
	   SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_APPOINTMENTS.ASSIGNEDTO = '+ CONVERT(VARCHAR(10),@operativeId) + ' AND'  

	SET @SelectClause =	'	SELECT	RIGHT(''00000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),6) as JSN 
									,P__PROPERTY.PROPERTYID as PROPERTYID
									,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as ADDRESS
									,P__PROPERTY.POSTCODE as POSTCODE
									,E__EMPLOYEE.FIRSTNAME +'' ''+ E__EMPLOYEE.LASTNAME as OPERATIVE
									,ISNULL(PLANNED_COMPONENT.COMPONENTNAME,''Miscellaneous'') as COMPONENTNAME
									,CONVERT(char(5),CAST(PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME as datetime),108) as STARTTIME
									,P__PROPERTY.TOWNCITY as TOWNCITY
									,P__PROPERTY.COUNTY as COUNTY
									,CONVERT(VARCHAR ,ISNULL(PLANNED_APPOINTMENTS.DURATION,PLANNED_MISC_TRADE.Duration ))+'' hour(s) '' as DURATION
									,ISNULL(CONVERT(VARCHAR(3),datename(weekday,PLANNED_APPOINTMENTS.APPOINTMENTDATE)) +'' ''+
									CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 106),''N/A'') as APPOINTMENTDATE'                                            		
	SET @FromClause =	'	FROM	PLANNED_APPOINTMENTS
									INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
									INNER JOIN P__PROPERTY ON  PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
									INNER JOIN E__EMPLOYEE ON PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
									LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID																						 
									INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
									LEFT JOIN PLANNED_COMPONENT_TRADE	ON PLANNED_APPOINTMENTS.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID
									INNER JOIN PLANNED_SUBSTATUS ON PLANNED_APPOINTMENTS.JOURNALSUBSTATUS = PLANNED_SUBSTATUS.SUBSTATUSID
									LEFT JOIN PLANNED_MISC_TRADE ON PLANNED_APPOINTMENTS.APPOINTMENTID = PLANNED_MISC_TRADE.AppointmentId  		'
	
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 )'        
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
	
	print (@mainSelectQuery)
	EXEC (@mainSelectQuery)
GO
