SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
















CREATE PROCEDURE dbo.FL_CONTRACTOR_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_CONTRACTOR_GETLOOKUP
 '   DATE CREATED:   19 NOV 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Contractor which will be shown as lookup value in presentation pages
 '   IN:            no
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT     S_ORGANISATION.ORGID AS id, S_ORGANISATION.NAME AS val
FROM         S_ORGANISATION INNER JOIN
                      S_SCOPE ON S_ORGANISATION.ORGID = S_SCOPE.ORGID INNER JOIN
                      S_AREAOFWORK ON S_SCOPE.AREAOFWORK = S_AREAOFWORK.AREAOFWORKID
WHERE     (S_AREAOFWORK.AREAOFWORKID = 8)
	ORDER BY S_ORGANISATION.NAME ASC













GO
