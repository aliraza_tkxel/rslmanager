SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_AddCustomerResponse]
/* ===========================================================================
'   NAME:           TO_ENQUIRY_LOG_AddCustomerResponse
'   DATE CREATED:   19 SEPTEMBER 2008
'   CREATED BY:     WASEEM HASSAN
'   CREATED FOR:    Broadland Housing
'   PURPOSE:        To add clearing arrears record, it adds rocord in TO_ENQUIRY_LOG table
'   IN:             @EnquiryID
'   VARCHAR:        @Notes
'   DATETIME:       @ResponseDate
'   IN:             @Status
'  
'
'   OUT:            @CustomerResponseId
'   RETURN:         Nothing    
'   VERSION:        1.0          
'   COMMENTS:      
'   MODIFIED ON:    
'   MODIFIED BY:    
'   REASON MODIFICATION:
'==============================================================================*/
       (
       @EnquiryID INT,
       @Notes VARCHAR(4000),
       @ResponseDate SMALLDATETIME,
       @CustomerResponseId INT OUTPUT
       )

AS
       INSERT INTO TO_CUSTOMER_RESPONSE(
       EnquiryLogId,
       Notes,
       ResponseDate
       )

VALUES(
       @EnquiryID,
       @Notes,
       getdate()--@ResponseDate
)

SELECT @CustomerResponseId=CustResponseId
FROM TO_CUSTOMER_RESPONSE
WHERE CustResponseId = SCOPE_IDENTITY()

IF @CustomerResponseId<0
       BEGIN
               SET @CustomerResponseId=-1                                        
       END
ELSE
       BEGIN
               UPDATE TO_ENQUIRY_LOG SET ItemStatusID = 29
               WHERE EnquiryLogID = @EnquiryID
       END






GO
