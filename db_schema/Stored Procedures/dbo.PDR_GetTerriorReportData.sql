USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetTerriorReportData]    Script Date: 02-Jul-16 11:14:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Get Terrior Report data on Terrior Report Page
 
    Author: Ali Raza
    Creation Date:  03/12/2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         03/12/2014     Ali Raza           Get Terrior Report data on Terrior Report Page
    
    Execution Command:

--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_GetTerriorReportData]
--		@SCHEMEID = NULL,
--		@SEARCHTEXT = N'26 Herringswell Road',
--		@pageSize = 50,
--		@pageNumber = 1,
--		@sortColumn = N'ADDRESS',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'*/

IF OBJECT_ID('dbo.PDR_GetTerriorReportData') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetTerriorReportData AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_GetTerriorReportData]
	-- Add the parameters for the stored procedure here
		@SCHEMEID INT = null,
		@SEARCHTEXT VARCHAR(200)= null,
		@isSold bit = 0,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ADDRESS', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		IF @isSold = 0	--isSold checkbox
			BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (P.STATUS IS NULL OR PROP_STATUS.DESCRIPTION NOT IN (''Sold'',''Transfer'',''Other Losses'',''Demolished'')) 
			'	
			END
					
		IF @SCHEMEID != -1
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( P.SCHEMEID  = ' +convert(varchar(10),@SCHEMEID)  + ')'	
		END
		IF(@SEARCHTEXT != '' OR @SEARCHTEXT != NULL)
		BEGIN	
            SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ISNULL(P.HouseNumber, '''') + '' '' + ISNULL(P.ADDRESS1, '''') + '' '' + ISNULL(P.ADDRESS2, '''') + '' '' + ISNULL(P.ADDRESS3, '''')  LIKE ''%' + @searchText + '%'''
			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR TOWNCITY LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR POSTCODE LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		(ISNULL(HOUSENUMBER,'' '')+'' ''+ISNULL(ADDRESS1,'' '')) as ADDRESS,
		 ISNULL(TOWNCITY,'' '') as TownCITY,
  ISNULL(POSTCODE,'' '') as POSTCODE,''£''+convert(varchar(10), ISNULL(F.OMV,0)) as OMV,''£''+convert(varchar(10), ISNULL(F.EUV,0))as EUV,  
  ''£''+convert(varchar(10), ISNULL(F.RENT,0))as RENT,convert(varchar(10), ISNULL(F.YIELD,0))+''%''as YIELD,  
  ''£''+convert(varchar(10), ISNULL((f.RENT*12),0)) as AnnualRent, ISNULL(S.DESCRIPTION,'' '') as [STATUS],ISNULL(AT.DESCRIPTION, '' '') as [Type]  
	,ISNULL(ANM.Description, ''N/A'') as [NROSHASSETTYPEMAIN]
	,P.PROPERTYID AS [PROPERTYID]
	,ISNULL(CONVERT(NVARCHAR,PS.SurveyDate,103),''N/A'') AS [SurveyDate]
	,PROP_TYPE.DESCRIPTION AS [PROPTYPE]
	,PROP_STATUS.DESCRIPTION AS [PROPSTATUS]
	,ISNULL(maxDoc.CategoryType,''N/A'') As EpcRating
	,ISNULL(CHARGE.DESCRIPTION,''N/A'') As Charge
	,ISNULL(PROPERTYCONDITIONRATING.DecentHomes,0) AS DecentHomes
	,CASE WHEN ( ISNULL(SCHEMEREST.RESTRICTIONCOUNT,0) > 0 OR ISNULL(BLOCKREST.RESTRICTIONCOUNT,0) > 0) THEN ''Yes'' ELSE ''No'' END as SchemeRestriction
	,CASE WHEN  ISNULL(PROPERTYREST.RESTRICTIONCOUNT,0) > 0 THEN ''Yes'' ELSE ''No'' END as PropertyRestriction

  '  
		
		--============================From Clause============================================
		SET @fromClause = 'FROM P__PROPERTY P 
							INNER JOIN P_FINANCIAL F on F.PROPERTYID = P.PROPERTYID
							--LEFT JOIN AS_JOURNAL J ON J.PROPERTYID=P.PROPERTYID AND J.ISCURRENT=1
							INNER JOIN P_STATUS S on S.STATUSID = P.STATUS
							INNER JOIN P_ASSETTYPE AT on AT.ASSETTYPEID = P.ASSETTYPE
							OUTER APPLY (SELECT MAX(S.SurveyDate) [SurveyDate] FROM PS_Survey S WHERE S.PROPERTYID = P.PROPERTYID) PS
							LEFT JOIN P_ASSETTYPE_NROSH_MAIN AS ANM ON P.NROSHASSETTYPEMAIN = ANM.Sid 
							INNER JOIN P_PROPERTYTYPE PROP_TYPE on PROP_TYPE.PROPERTYTYPEID = P.PROPERTYTYPE
							INNER JOIN P_STATUS PROP_STATUS ON PROP_STATUS.STATUSID = P.STATUS
							INNER JOIN P_FINANCIAL FINANCIALS ON FINANCIALS.PROPERTYID = P.PROPERTYID
							LEFT JOIN P_CHARGE CHARGE ON FINANCIALS.CHARGE = CHARGE.CHARGEID
							LEFT JOIN (
								Select PropertyDocs.PropertyId,EpcCategory.CategoryType from 
								(	Select Documents.PropertyId,Documents.EpcRating from 
									(	
										Select PropertyId,MAX(DocumentId)as DocumentId from 
										P_Documents Docs 
										Group By Docs.PropertyId
										
									)	PropertyDocs inner join P_Documents Documents on Documents.DocumentId= PropertyDocs.DocumentId
									
								) PropertyDocs Left Join P_EpcCategory EpcCategory on EpcCategory.CategoryTypeId = PropertyDocs.EpcRating
							
							) AS maxDoc ON maxDoc.PropertyId = P.PROPERTYID

							LEFT JOIN (SELECT	PPA.PROPERTYID PropertyId, COUNT(*) as DecentHomes
										FROM	PA_PROPERTY_ATTRIBUTES PPA
												INNER JOIN PA_ITEM_PARAMETER PIP ON PPA.ITEMPARAMID = PIP.ItemParamID
												INNER JOIN PA_PARAMETER PP ON PIP.PARAMETERID = PP.PARAMETERID
												INNER JOIN PA_PARAMETER_VALUE PPV ON PPA.VALUEID = PPV.ValueID
										WHERE	ParameterName like ''%condition%''
												AND PPV.ValueDetail IN (''unsatisfactory'',''potentially unsatisfactory'')
												AND PPA.PROPERTYID IS NOT NULL
										GROUP BY PPA.PROPERTYID) AS PROPERTYCONDITIONRATING ON P.PROPERTYID = PROPERTYCONDITIONRATING.PROPERTYID
							
							LEFT JOIN (	SELECT	 SCHEMEID, COUNT(*) RESTRICTIONCOUNT
										FROM	PDR_Restriction
										GROUP BY SCHEMEID
										) SCHEMEREST ON P.SCHEMEID = SCHEMEREST.SCHEMEID
							
							LEFT JOIN (	SELECT	 BLOCKID, COUNT(*) RESTRICTIONCOUNT
										FROM	PDR_Restriction
										GROUP BY BLOCKID
										) BLOCKREST ON P.BLOCKID = BLOCKREST.BLOCKID

							LEFT JOIN (	SELECT	 PROPERTYID, COUNT(*) RESTRICTIONCOUNT
										FROM	PDR_Restriction
										GROUP BY PROPERTYID
										) PROPERTYREST ON P.PROPERTYID = PROPERTYREST.PROPERTYID
							
							'



  --============================Order Clause==========================================
  IF (@sortColumn = 'ADDRESS')
  BEGIN
    SET @sortColumn = CHAR(10) + 'ADDRESS'

  END

  IF (@sortColumn = 'TownCITY')
  BEGIN
    SET @sortColumn = CHAR(10) + 'TOWNCITY'

  END

  IF (@sortColumn = 'Charge')
  BEGIN
    SET @sortColumn = CHAR(10) + 'Charge'

  END

  IF (@sortColumn = 'POSTCODE')
  BEGIN
    SET @sortColumn = CHAR(10) + 'POSTCODE'

  END
  IF (@sortColumn = 'EpcRating')
  BEGIN
    SET @sortColumn = CHAR(10) + 'EpcRating'

  END
  IF (@sortColumn = 'TYPE')
  BEGIN
    SET @sortColumn = CHAR(10) + '[PROPTYPE]'

  END

  IF (@sortColumn = 'OMV')
  BEGIN
    SET @sortColumn = CHAR(10) + 'OMV'

  END
  IF (@sortColumn = 'EUV')
  BEGIN
    SET @sortColumn = CHAR(10) + 'EUV'

  END
  IF (@sortColumn = 'RENT')
  BEGIN
    SET @sortColumn = CHAR(10) + 'RENT'

  END
  IF (@sortColumn = 'YIELD')
  BEGIN
    SET @sortColumn = CHAR(10) + 'YIELD'

  END
  IF (@sortColumn = 'AnnualRent')
  BEGIN
    SET @sortColumn = CHAR(10) + 'AnnualRent'

  END
  IF (@sortColumn = 'STATUS')
  BEGIN
    SET @sortColumn = CHAR(10) + '[STATUS]'

  END
  IF (@sortColumn = 'SurveyDate')
  BEGIN
    SET @sortColumn = CHAR(10) + '[SurveyDate]'

  END
  IF (@sortColumn = 'NROSHASSETTYPEMAIN')
  BEGIN
    SET @sortColumn = CHAR(10) + '[NROSHASSETTYPEMAIN]'

  END

  SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

  --=================================	Where Clause ================================

  SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria

  --===============================Main Query ====================================
  SET @mainSelectQuery = @selectClause + @fromClause + @whereClause + @orderClause

  --=============================== Row Number Query =============================
  SET @rowNumberQuery = '  SELECT *, row_number() over (order by ' + CHAR(10) + @sortColumn + CHAR(10) + @sortOrder + CHAR(10) + ') as row	
								FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'

  --============================== Final Query ===================================
  SET @finalQuery = ' SELECT *
							FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
			
END
