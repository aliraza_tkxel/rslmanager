USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_TrainingApprovalMyStaff]    Script Date: 13/06/2018 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_PayPointReviewCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_PayPointReviewCount AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Saud Ahmed
-- Create date:      13/06/2018
-- Description:      Calculate alert count of Training Approval HR (submitted Training that cost is > 1000 and professional Qual. and require Approval from HR.) on whiteboard  
-- exec  E_TrainingApprovalMyStaff 423
-- =============================================

ALTER PROCEDURE  [dbo].[E_PayPointReviewCount]
	@LineMgr INT 
AS
BEGIN
	
	SET NOCOUNT ON;
   
SELECT PendingPayPoint as PayPointCount From (
	SELECT count(*) AS PendingPayPoint
	from E__EMPLOYEE E 
		join E_PaypointSubmission PS on E.EMPLOYEEID = PS.employeeId
		join E_PayPointStatus PSS on PS.paypointStatusId = PSS.paypointStatusId
		left join E_JOBDETAILS J on E.EMPLOYEEID = J.EMPLOYEEID
		left join E_TEAM T on J.TEAM = T.TEAMID
		left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
		left join E_GRADE G on J.GRADE = G.GRADEID
		left join E_JOBROLE ET on J.JobRoleId = ET.JobRoleId
		left join G_ETHNICITY ETH on E.ETHNICITY = ETH.ETHID
		left join E_DOI DOI on E.EMPLOYEEID = DOI.EmployeeId
		left join E_DOI_Status DOIs on DOI.Status = DOIs.DOIStatusId
		CROSS APPLY (SELECT TOP 1 YRange FROM F_FISCALYEARS ff ORDER BY ff.YRange DESC) AS yRange
    where PS.fiscalYearId = yRange.YRange and PSS.Description = 'Submitted' and
		T.DIRECTOR = @LineMgr and J.ACTIVE = 1 
)  totalResult




END
