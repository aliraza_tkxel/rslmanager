USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.PDR_SaveExcludedProperties') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_SaveExcludedProperties AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_SaveExcludedProperties]
	
	@schemeId INT
	,@orderItemId INT
	,@excludedProperties AS PDR_EXCLUDEDPROPERTY READONLY
	,@isSaved BIT = 0 OUTPUT
	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION
	BEGIN TRY
	

	DELETE	FSCEP
	FROM	F_ServiceChargeExProperties FSCEP
			INNER JOIN P__PROPERTY PP ON FSCEP.PROPERTYID = PP.PROPERTYID
	WHERE	PurchaseItemId = @orderItemId AND PP.SCHEMEID = @schemeId

	
	INSERT	INTO [dbo].[F_ServiceChargeExProperties] ([PropertyId],[PurchaseItemId])
    SELECT  propertyId ,@orderItemId
	FROM	@excludedProperties;


	END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

END
	

