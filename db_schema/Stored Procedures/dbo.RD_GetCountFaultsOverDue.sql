
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC  [dbo].[RD_GetCountFaultsOverDue]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetCountFaultsOverDue]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	
	SET NOCOUNT ON;
	
SELECT COUNT( DISTINCT FL_FAULT_LOG.FaultLogID)
FROM (SELECT DISTINCT FL_FAULT_LOG.FaultLogID ,FL_FAULT_LOG.SubmitDate,CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as Priority ,CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			DATEADD(day,FL_FAULT_PRIORITY.ResponseTime,FL_FAULT_LOG.SubmitDate) ELSE
			DATEADD(hour,FL_FAULT_PRIORITY.ResponseTime,FL_FAULT_LOG.SubmitDate) END as Deadline
				FROM FL_FAULT_LOG 
				INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID 
				INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
				--17	Complete
				--11	Cancelled Fault
				--13	Cancelled
				where StatusID NOT IN (11,13,17)	) IncompleteFaults
	INNER JOIN FL_FAULT_LOG on IncompleteFaults.FaultLogID = FL_FAULT_LOG.FaultLogId
	INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
	INNER JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
	INNER JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
	INNER JOIN FL_FAULT_TRADE on FL_FAULT.FaultID = FL_FAULT_TRADE.FaultId
	INNER JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
	INNER JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID=FL_FAULT_LOG.StatusID
	INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.FaultLogId=FL_FAULT_LOG.FaultLogID
	INNER JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID=FL_FAULT_APPOINTMENT.AppointmentId
WHERE
  GETDATE() > IncompleteFaults.Deadline
	
END




GO
