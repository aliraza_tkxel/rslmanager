SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetRentList]
		@skipIndex		int,
		@takeIndex		int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP(@takeIndex)     
			AM_Customer_Rent_Parameters.CustomerId, 
			AM_Customer_Rent_Parameters.TenancyId, 
			ISNULL(AM_Customer_Rent_Parameters.RentBalance, 0.0) - ISNULL(AM_Customer_Rent_Parameters.EstimatedHBDue, 0.0) AS RentBalance,
			P_FINANCIAL.TOTALRENT as rentAmount

	FROM        C_TENANCY INNER JOIN 
				AM_Customer_Rent_Parameters ON C_Tenancy.TenancyId = AM_Customer_Rent_Parameters.TenancyId INNER JOIN       
				P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
				P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID				

	WHERE (C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE>GETDATE())
	--AND dbo.C_TENANCY.TENANCYID NOT IN(131954,971493,972228,972958,972958,974099,974099,974689,974689,974767,974767)
	and AM_Customer_Rent_Parameters.CustomerId NOT IN(														
											SELECT TOP(@skipIndex)     
														AM_Customer_Rent_Parameters.CustomerId

											FROM        C_TENANCY INNER JOIN 
														AM_Customer_Rent_Parameters ON AM_Customer_Rent_Parameters.TenancyId = C_Tenancy.TenancyId INNER JOIN       
														P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
														P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID				

											WHERE (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE>GETDATE())
											--AND dbo.C_TENANCY.TENANCYID NOT IN(131954,971493,972228,972958,972958,974099,974099,974689,974689,974767,974767)
											order by AM_Customer_Rent_Parameters.TenancyId ASC
										)
	order by AM_Customer_Rent_Parameters.TenancyId ASC

END

--exec AM_SP_GetRentList 0, 7000












GO
