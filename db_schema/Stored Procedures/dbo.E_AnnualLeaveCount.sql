USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_AnnualLeaveCount]    Script Date: 05/05/2016 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_AnnualLeaveCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_AnnualLeaveCount AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Raja Aneeq
-- Create date:      21/10/2015
-- Description:      Calculate alert count of LEAVE REQUEST on whiteboard  
-- exec  E_AnnualLeaveCount 113
-- =============================================

ALTER PROCEDURE  [dbo].[E_AnnualLeaveCount]
	@empId INT 
AS
BEGIN
	SELECT MAX(A.AbsenceHistoryID) AS AbsenceHistoryId, en.DESCRIPTION as Nature
      FROM E_JOURNAL J CROSS APPLY
        (SELECT MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryID
         FROM E_ABSENCE
         WHERE JOURNALID = J.JOURNALID) MA
      INNER JOIN E_ABSENCE A ON MA.MaxAbsenceHistoryID = A.ABSENCEHISTORYID --and 
	  join E_STATUS es on J.CURRENTITEMSTATUSID = es.ITEMSTATUSID
	  join E_NATURE en on J.ITEMNATUREID = en.ITEMNATUREID
       WHERE J.EMPLOYEEID = @empId AND (es.DESCRIPTION = 'Absent' OR es.DESCRIPTION = 'Approved' OR es.DESCRIPTION = 'Completed')
	   AND (
    (A.STARTDATE <= CONVERT(date, getdate()) AND A.RETURNDATE >= CONVERT(date, getdate()))
    OR (en.DESCRIPTION = 'Sickness' AND A.STARTDATE <= CONVERT(date, getdate()) AND A.RETURNDATE is null) )
	  group by en.DESCRIPTION

END
