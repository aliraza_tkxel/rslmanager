
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC [dbo].[RD_GetCountFOLLOWON]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetCountFollowOn]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT   count(FollowOnID)  FROM FL_FAULT_FOLLOWON    
	JOIN FL_FAULT_LOG on FL_FAULT_FOLLOWON.FaultLogId = FL_FAULT_LOG.FaultLogID    
	JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
	WHERE isFollowonScheduled = 0
		
END

GO
