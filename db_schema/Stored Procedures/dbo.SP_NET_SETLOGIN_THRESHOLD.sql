
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================

--<Author : Aqib Javed>
--<Created Date : 25-Nov-2013>
--<Description : This store procedure will keep track of threshold and lock the account after 5 attempts >
--<Web page: rsl_master_site\BHAIntranet\Login.aspx>
-- =============================================

CREATE proc [dbo].[SP_NET_SETLOGIN_THRESHOLD]
@username varchar(50)
as
DECLARE 
@threshold tinyint,
@islocked bit,
@firstname varchar (50),
@lastname varchar (50)

--Getting Threshold Count
Select @threshold=AC_LOGINS.THRESHOLD,@islocked=ISLOCKED,@firstname=E__EMPLOYEE.FIRSTNAME ,@lastname=E__EMPLOYEE.LASTNAME from AC_LOGINS 
LEFT JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = AC_LOGINS.EMPLOYEEID
where [LOGIN] =@username

--Increasing the Threshold Count
if(@threshold is null) 
 BEGIN
    set @threshold=1
 END   
else if( @threshold<5 and @islocked=0)
 BEGIN 
 	set @threshold=@threshold +1
 END 	
 --Applying Lock
 if(@threshold=5)
	set @islocked=1
 else 
    set @islocked=0 		
--Tracking Account Failed Attempts
if(@threshold<=5 )
BEGIN
	INSERT INTO AC_LOGIN_HISTORY (FIRSTNAME, LASTNAME, USERNAME,ISFAILED,ISLOCKED) 
	VALUES (@firstname, @lastname, @username,1,@islocked)
    --Updating the Threshold Count & Lock Account
	update AC_LOGINS set THRESHOLD = @threshold,ISLOCKED=@islocked 
	where [LOGIN] = @username AND (ISLOCKED=0 OR ISLOCKED IS NULL) 	
	--Return Values
END	

Select @threshold as Threshold, @islocked as Locked	
GO
