USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyRentType]    Script Date: 02/08/2018 16:37:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.AS_GetPropertyRentType') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetPropertyRentType AS SET NOCOUNT ON;') 
GO
-- =============================================  
--EXEC [dbo].[AS_GetPropertyRentType]  
-- @propertyId='BHA0000018'  
-- Author:  <Aqib Javed>  
-- Create date: <18-Apr-2014>  
-- Description: <This procedure 'll get the Rent Type and Associate Financial information >  
-- Web Page: FinancialTab.ascx  
ALTER PROCEDURE [dbo].[AS_GetPropertyRentType]   
 @propertyId varchar(100)    
AS  
BEGIN  
 Select TENANCYTYPEID, [DESCRIPTION] from C_TENANCYTYPE order by TENANCYTYPEID  
 Select * from P_FINANCIAL where propertyId=@propertyId  
	Select FID, [DESCRIPTION] from P_RENTFREQUENCY order by [DESCRIPTION]
----------get dataset for Annual Rent Tab

 SELECT TOP 1 DATERENTSET,UPLOADDATE,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,REASSESMENTDATE, DATEADD(DD,-1,DATEADD(YY,1,REASSESMENTDATE)) AS RENTEFFECTIVE, 
		 (RENT+SERVICES+COUNCILTAX+WATERRATES+INELIGSERV+SUPPORTEDSERVICES) AS TOTALRENT 
		 FROM P_TARGETRENT T 
			INNER JOIN P_RENTINCREASE_FILES F ON F.FILEID = T.FILEID 
		 WHERE PROPERTYID = @propertyId AND REASSESMENTDATE > GETDATE() ORDER BY TARGETRENTID DESC	
	
END