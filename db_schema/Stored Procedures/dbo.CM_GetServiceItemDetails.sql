USE [RSLBHALive]
GO


IF OBJECT_ID('dbo.[CM_GetServiceItemDetails]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[CM_GetServiceItemDetails] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[CM_GetServiceItemDetails]
-- Add the parameters for the stored procedure here
		@ServiceItemIds VARCHAR(100)='' 
		
AS
BEGIN


SELECT Result.ServiceItemId,
       Result.SchemeBlock,
       Result.Commencement,
       Result.Cycle,
       CEILING((Result.EndDateInDays-Result.StartDateInDays)/Result.CycleDays) AS Cycles,
       Result.CycleValue,
       ((Result.EndDateInDays-Result.StartDateInDays)/Result.CycleDays)* Result.CycleValue AS Value,
      Convert(nvarchar(20),RESULT.EndDate,103)as EndDate,RESULT.CycleDays,  RESULT.CycleType as cycleType, RESULT.cycleNo as cycleperiod
		

FROM
  ( SELECT C.ServiceItemId,
           CASE
               WHEN B.BLOCKID IS NULL THEN S.SCHEMENAME
               ELSE B.BLOCKNAME
           END AS SchemeBlock,
           Convert(nvarchar(20),C.ContractCommencement,103) AS Commencement,
           C.ContractPeriod,
           CPT.CycleType AS ContractPeriodType,
           'Every '+cast(C.Cycle AS NVARCHAR)+' '+ CT.CycleType AS CYCLE,
           C.CycleValue,
           CASE
               WHEN CPT.CycleType='Week(s)' THEN datediff(d, '1970-01-01',DATEADD(WEEK, C.ContractPeriod, C.ContractCommencement))
               WHEN CPT.CycleType='Month(s)' THEN datediff(d, '1970-01-01', DATEADD(MONTH, C.ContractPeriod, C.ContractCommencement))
               WHEN CPT.CycleType='Year(s)' THEN datediff(d, '1970-01-01', DATEADD(YEAR, C.ContractPeriod, C.ContractCommencement))
               ELSE datediff(d, '1970-01-01',DATEADD(DAY, C.ContractPeriod, C.ContractCommencement))
           END AS EndDateInDays,
           datediff(d, '1970-01-01',ContractCommencement) AS StartDateInDays,
            CASE
               WHEN CPT.CycleType='Week(s)' THEN DATEADD(WEEK, C.ContractPeriod, C.ContractCommencement)
               WHEN CPT.CycleType='Month(s)' THEN DATEADD(MONTH, C.ContractPeriod, C.ContractCommencement)
               WHEN CPT.CycleType='Year(s)' THEN DATEADD(YEAR, C.ContractPeriod, C.ContractCommencement)
               ELSE DATEADD(DAY, C.ContractPeriod, C.ContractCommencement)
           END AS EndDate
           
           ,
           CASE
               WHEN CT.CycleType = 'Week(s)' THEN C.Cycle * 7
               WHEN CT.CycleType = 'Month(s)' THEN C.Cycle * 30
               WHEN CT.CycleType = 'Year(s)' THEN C.Cycle * 52* 7
               ELSE C.Cycle
           END AS CycleDays
		   ,
		   CT.CycleType as CycleType,
		   C.Cycle as cycleNo 
		   
   FROM CM_ServiceItems C
   INNER JOIN PDR_CycleType CPT ON C.ContractPeriodType=CPT.CycleTypeId
   INNER JOIN PDR_CycleType CT ON C.CycleType=CT.CycleTypeId
   LEFT JOIN P_BLOCK B ON C.BlockId=B.BLOCKID
   LEFT JOIN P_SCHEME S ON C.SchemeId=S.SCHEMEID
   OR B.SchemeId=S.SCHEMEID
   LEFT JOIN PDR_DEVELOPMENT D ON S.DEVELOPMENTID= D.DEVELOPMENTID
   INNER JOIN PA_ITEM I ON C.ItemId = I.ItemID
  Where C.ServiceItemId IN (Select COLUMN1 from SPLIT_STRING(@ServiceItemIds,','))
   
   ) AS RESULT

END