SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[TO_G_NATIONALITY_GetLookup]
/* ===========================================================================
 '   NAME:           TO_G_NATIONALITY_GetLookup
 '   DATE CREATED:   16 MAY 2013
 '   CREATED BY:     Nataliya Alexander
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get NATIONALITY records from G_NATIONALITY table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT NATID AS id,DESCRIPTION AS val
	FROM G_CUSTOMERNATIONALITY
	ORDER BY DESCRIPTION ASC




GO
