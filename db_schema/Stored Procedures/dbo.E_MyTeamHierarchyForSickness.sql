USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_MyTeamHierarchyForSickness]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.E_MyTeamHierarchyForSickness') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_MyTeamHierarchyForSickness AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_MyTeamHierarchyForSickness] 
	-- Add the parameters for the stored procedure here
	@empId INT,
	@Status VARCHAR(MAX)
	
AS
BEGIN

	DECLARE @MyStaff TABLE(
		EmployeeId INT,
		EmployeeFName VARCHAR(MAX), 
		EmployeeLName VARCHAR(MAX), 
		LineManagerId INT
	)

	DECLARE @MyStaffLeavesData TABLE(
		StartDate smalldatetime null,
		EmployeeId INT,
		EmployeeFName VARCHAR(MAX), 
		EmployeeLName VARCHAR(MAX), 
		LineManagerId INT,
		ReturnDate smalldatetime null,
		Duration float null,
		Unit varchar(255),
		StatusId int null,
		LeaveStatus varchar(255),
		ItemNatureId int null,
		NatureDescription varchar(255),
		AbsenceHistoryId int null,
		Reason varchar(255),
		ReasonId int null,
		AnticipatedReturnDate smalldatetime null,
		holyType varchar(max),
		notes varchar(max)
	)
	DECLARE @staffId INT
	DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME
	
	INSERT INTO @MyStaff (EmployeeId, EmployeeFName, EmployeeLName,LineManagerId)
	(SELECT * from dbo.FN_GetTeamHierarchyForEmployeeSickness(@empId))
	
	DECLARE myStaffCursor CURSOR  FOR
	SELECT EmployeeId from @MyStaff
	
	OPEN myStaffCursor
	FETCH NEXT FROM myStaffCursor INTO @staffId
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		SELECT @BHSTART = STARTDATE, @BHEND = ENDDATE
		FROM EMPLOYEE_ANNUAL_START_END_DATE(@staffId)
		
		INSERT INTO @MyStaffLeavesData (StartDate,EmployeeId,EmployeeFName,EmployeeLName,LineManagerId,ReturnDate,Duration,Unit,LeaveStatus,ItemNatureId,NatureDescription,AbsenceHistoryId,Reason,ReasonId,AnticipatedReturnDate,StatusId,holyType,notes)
		
		SELECT A.STARTDATE, J.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, J.LINEMANAGER, a.RETURNDATE 
		,A.DURATION as duration,A.DURATION_TYPE as unit,es.DESCRIPTION,jnal.ITEMNATUREID, en.DESCRIPTION
		,a.ABSENCEHISTORYID, ar.DESCRIPTION, a.REASONID, a.AnticipatedReturnDate, a.ITEMSTATUSID,a.HOLTYPE,a.notes
		FROM E__EMPLOYEE E  
							LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
							LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID   
							inner JOIN E_STATUS es on JNAL.CURRENTITEMSTATUSID = es.ITEMSTATUSID
							inner JOIN E_NATURE en on JNAL.ITEMNATUREID = en.ITEMNATUREID
							INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = (
											SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
											WHERE JOURNALID = JNAL.JOURNALID AND A.STARTDATE BETWEEN @BHSTART AND @BHEND
										  ) 
							INNER JOIN E_ABSENCEREASON ar on A.REASONID = ar.SID
					WHERE 	E.EMPLOYEEID =  @staffId
					and es.DESCRIPTION = @Status
					and JNAL.ITEMNATUREID  IN (SELECT ITEMNATUREID from E_NATURE 
											where DESCRIPTION  in ('Sickness') )
		
		
		FETCH NEXT FROM myStaffCursor INTO @staffId
	END
	CLOSE myStaffCursor
	DEALLOCATE myStaffCursor
	
	INSERT INTO @MyStaffLeavesData (EmployeeId,EmployeeFName,EmployeeLName,LineManagerId)
	SELECT EmployeeId,EmployeeFName,EmployeeLName,LineManagerId FROM @MyStaff 
	WHERE EMPLOYEEID NOT IN (SELECT EmployeeId FROM @MyStaffLeavesData)
	
	SELECT StartDate,EmployeeId,EmployeeFName,EmployeeLName,LineManagerId,ReturnDate,Duration,Unit,LeaveStatus,ItemNatureId,NatureDescription,AbsenceHistoryId,Reason,ReasonId,AnticipatedReturnDate,StatusId,holyType,notes
	FROM @MyStaffLeavesData
	
END
	
	
	
	
	
	
	