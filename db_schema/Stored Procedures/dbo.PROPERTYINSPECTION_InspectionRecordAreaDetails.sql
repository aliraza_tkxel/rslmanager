USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PROPERTYINSPECTION_InspectionRecordAreaDetails]    Script Date: 06/07/2016 11:02:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Abdullah Saeed      
-- Create date: August 4,2014      
-- Description: Area Details      
-- Exec PROPERTYINSPECTION_InspectionRecordAreaDetails 'A240110002'      
-- =============================================      

IF OBJECT_ID('dbo.PROPERTYINSPECTION_InspectionRecordAreaDetails') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PROPERTYINSPECTION_InspectionRecordAreaDetails AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[PROPERTYINSPECTION_InspectionRecordAreaDetails]      
 -- Add the parameters for the stored procedure here      
(      
	@propertyID varchar(100),
	@surveyId int = -1      
)      
       
       
AS      
BEGIN      
       
 SET NOCOUNT ON;      
   
--==========================================================================
-- (1) LOCATION
--==========================================================================
SELECT DISTINCT  PA_LOCATION.LocationID,PA_LOCATION.LocationName  
FROM PA_ITEM PI  
 INNER JOIN PA_AREA PA ON PA.AreaID = PI.AreaID  
 INNER JOIN PA_LOCATION ON PA_LOCATION.LocationID =PA.LocationId
 INNER JOIN PA_ITEM on  PA_ITEM.AreaID = PA.AreaID 
 INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID 
 INNER JOIN PA_PARAMETER on PA_PARAMETER.ParameterID = PA_ITEM_PARAMETER.ParameterId
 INNER JOIN PA_PROPERTY_ATTRIBUTES ON  PA_PROPERTY_ATTRIBUTES.ITEMPARAMID= PA_ITEM_PARAMETER.ItemParamID
WHERE PA_PROPERTY_ATTRIBUTES.PROPERTYID= @propertyID AND PA_PROPERTY_ATTRIBUTES.IsUpdated = 1 AND PI.ParentItemId is Null and PI.IsActive =1  And PI.ShowInApp = 1 And PI.ShowListView = 0 AND PA.ShowInApp = 1  
      
--==========================================================================
-- (2) AREA
--==========================================================================      
 SELECT DISTINCT PA.AreaID, PA.LocationId, PA.AreaName 
 FROM PA_ITEM PI  
 INNER JOIN PA_AREA PA ON PA.AreaID = PI.AreaID  
 INNER JOIN PA_ITEM on  PA_ITEM.AreaID = PA.AreaID 
 INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID 
 INNER JOIN PA_PARAMETER on PA_PARAMETER.ParameterID = PA_ITEM_PARAMETER.ParameterId
 INNER JOIN PA_PROPERTY_ATTRIBUTES ON  PA_PROPERTY_ATTRIBUTES.ITEMPARAMID= PA_ITEM_PARAMETER.ItemParamID
 WHERE PA_PROPERTY_ATTRIBUTES.PROPERTYID= @propertyID AND PA_PROPERTY_ATTRIBUTES.IsUpdated = 1 
 AND PI.ParentItemId is Null and PI.IsActive =1  
 And PI.ShowInApp = 1 And PI.ShowListView = 0 AND PA.ShowInApp = 1  

--==========================================================================
-- (3) ITEM
--==========================================================================      
  SELECT DISTINCT PA_ITEM.ItemID,AreaID,ItemName 
  FROM PA_ITEM 
	INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM.ItemID= PA_ITEM_PARAMETER.ItemId
	INNER JOIN PA_PROPERTY_ATTRIBUTES A ON PA_ITEM_PARAMETER.ItemParamID = A.ITEMPARAMID AND A.PROPERTYID=@propertyID 
  WHERE ParentItemId IS NULL AND PA_ITEM.IsActive=1 And ShowInApp = 1 And ShowListView = 0 AND PA_ITEM_PARAMETER.IsActive=1 AND A.IsUpdated =1
	
 UNION ALL
   
  SELECT DISTINCT PA_ITEM.ItemID,AreaID,ItemName 
  FROM	PA_ITEM
  WHERE	IsActive =1  And ShowInApp = 1 And ShowListView = 0
   AND ItemID IN (Select ParentItemId from PA_ITEM Where PA_ITEM.ParentItemId IS NOT NULL)

--==========================================================================
-- (4) SUB-ITEM
--==========================================================================         
 SELECT ItemID as SubItemID ,AreaID ,ItemName as SubItemName, ParentItemId  
 FROM	PA_ITEM       
 WHERE  ParentItemId is not Null and IsActive =1 And ItemName Not like '%Aids & Adaptations%' And ShowInApp = 1 And ShowListView = 0   
       
   
--==========================================================================
-- (5) TABLE UPDATE STATUS
--==========================================================================    
 SELECT ItemId , PROPERTYID,ATTRIBUTEID,PA_ITEM_PARAMETER.ITEMPARAMID 
 FROM	PA_PROPERTY_ATTRIBUTES        
		INNER JOIN PA_ITEM_PARAMETER ON  PA_PROPERTY_ATTRIBUTES.ITEMPARAMID= PA_ITEM_PARAMETER.ItemParamID           
 WHERE PROPERTYID=  @propertyID and IsUpdated =1       
      
	   
--==========================================================================
-- (6) PARAMETERS
--==========================================================================     

--============================
-- 6a.  PA_PROPERTY_ATTRIBUTES 
--============================
  
 SELECT  DISTINCT PA_ITEM_PARAMETER.ItemParamID,PA_ITEM.ItemID,PA_ITEM.ItemName ,PA_PARAMETER.ParameterID,ParameterName
		, ControlType ,ParameterSorder,ParameterValueId, PPA.HeatingMappingId  HeatingId ,  HeatingType as SelectedHeatingType
		, PHM.IsActive
 FROM	 PA_ITEM_PARAMETER             
		 INNER JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID             
		 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId             
		 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID          
		 INNER JOIN PA_PROPERTY_ATTRIBUTES PPA ON PA_ITEM_PARAMETER.ItemParamID=PPA.ITEMPARAMID And PPA.PROPERTYID=@propertyID    
		 LEFT JOIN  PA_HeatingMapping PHM on PPA.HeatingMappingId = PHM.HeatingMappingId      
 WHERE  PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 
		AND PA_PARAMETER.ShowInApp = 1 AND PA_ITEM.ShowInApp=1  
		AND PA_ITEM.ShowListView = 0 AND PA_AREA.ShowInApp=1          
      
 UNION ALL    
   
 --===========================
 -- 6b. PA_PROPERTY_ITEM_DATES
 --===========================
 SELECT DISTINCT PA_ITEM_PARAMETER.ItemParamID,PA_ITEM.ItemID,PA_ITEM.ItemName,PA_PARAMETER.ParameterID,ParameterName,ControlType
 ,ParameterSorder,ParameterValueId, PPA.HeatingMappingId  HeatingId ,  HeatingType  as SelectedHeatingType , PHM.IsActive
 FROM PA_ITEM_PARAMETER    
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID    
	INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId    
	INNER JOIN PA_AREA ON PA_AREA.AreaID = PA_ITEM.AreaID    
	INNER JOIN PA_PROPERTY_ITEM_DATES PPA  ON PA_ITEM.ItemID = PPA.ItemId  AND PPA.PROPERTYID = @propertyID   
	LEFT JOIN  PA_HeatingMapping PHM on PPA.HeatingMappingId = PHM.HeatingMappingId 
 WHERE PA_ITEM_PARAMETER.isActive = 1 AND PA_ITEM.isActive = 1 AND PA_PARAMETER.isActive = 1    
	And PA_PARAMETER.ControlType='Date' And PA_PARAMETER.ShowInApp = 1 AND PA_ITEM.ShowInApp=1  
	AND PA_ITEM.ShowListView = 0 AND PA_AREA.ShowInApp=1           
 ORDER BY ParameterSorder ASC    

--==========================================================================
-- (7) PARAMETERS VALUES
--==========================================================================          
 SELECT ValueID, ParameterID, ValueDetail, Sorder           
 FROM	PA_PARAMETER_VALUE 
 WHERE	ParameterID IN (           
				 SELECT PA_PARAMETER.ParameterID 
				 FROM	PA_ITEM_PARAMETER             
						INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID        
						INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId             
						INNER JOIN PA_AREA ON PA_AREA.AreaID = PA_ITEM.AreaID             
				 WHERE  PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1   
						 And PA_PARAMETER.ShowInApp = 1 AND PA_ITEM.ShowInApp=1  
						 AND PA_ITEM.ShowListView =0 AND PA_AREA.ShowInApp=1) 
	AND PA_PARAMETER_VALUE.IsActive = 1            
 ORDER BY SOrder             
   
--==========================================================================
-- (8) PROPERTY PRE-INSERTED VALUES
--==========================================================================  
         
--============================
-- 8a.  PA_PROPERTY_ATTRIBUTES 
--============================	   
SELECT PROPERTYID,ATTRIBUTEID,ITEMPARAMID,PARAMETERVALUE,VALUEID,UPDATEDON,UPDATEDBY ,IsCheckBoxSelected , HeatingMappingId as HeatingId
FROM PA_PROPERTY_ATTRIBUTES            
WHERE PROPERTYID=  @propertyId 
	 AND ITEMPARAMID IN  (	SELECT ItemParamID 
							FROM PA_ITEM_PARAMETER             
								INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID             
								INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId             
								INNER JOIN PA_AREA ON PA_AREA.AreaID = PA_ITEM.AreaID             
							WHERE	PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1  
									And PA_PARAMETER.ShowInApp = 1 AND PA_ITEM.ShowInApp=1  
									AND PA_ITEM.ShowListView =0 AND PA_AREA.ShowInApp=1)          
             
--===========================
-- 8b. PA_PROPERTY_ITEM_DATES
--=========================== 
SELECT [SID],PROPERTYID,ItemId,LastDone,DueDate,PA_PARAMETER.ParameterId,ISNULL(ParameterName,'NA')as  ParameterName , HeatingMappingId as HeatingId
FROM	 PA_PROPERTY_ITEM_DATES            
		LEFT JOIN PA_PARAMETER On PA_PARAMETER.ParameterID = PA_PROPERTY_ITEM_DATES.ParameterId            
WHERE PROPERTYID=@propertyId       
         
      
--==========================================================================
-- (9) INSPECTION NOTES
--========================================================================== 
         
SELECT	DISTINCT PROP_ATTRIB.PROPERTYID ,ITEM_NOTES.ItemId,ITEM_NOTES.Notes,AREA.LocationID,ITEM.AreaID,LOCATION.LocationName,AREA.AreaName,ITEM.ItemName       
FROM	PS_Survey_Item_Notes ITEM_NOTES 
		INNER JOIN PA_ITEM ITEM ON ITEM_NOTES.ItemId = ITEM.ItemID
		INNER JOIN PA_AREA AREA ON AREA.AreaID = ITEM.AreaID
		INNER JOIN PA_LOCATION LOCATION ON LOCATION.LocationID = AREA.LocationId
		INNER JOIN PA_ITEM_PARAMETER ITEM_PARAM ON ITEM_PARAM.ItemId = ITEM.ItemID
		INNER JOIN PA_PROPERTY_ATTRIBUTES PROP_ATTRIB ON PROP_ATTRIB.ITEMPARAMID = ITEM_PARAM.ItemParamID
WHERE  PROP_ATTRIB.PROPERTYID = @propertyID AND ITEM_NOTES.SurveyId=@surveyId AND PROP_ATTRIB.IsUpdated = 1
         
   
--==========================================================================
-- (10) PROPERTY IMAGES
--==========================================================================   
SELECT	DISTINCT	PROP_ATTRIB.PROPERTYID,ITEM.ItemID,'../../../PropertyImages/'+@propertyID+'/Images/'+SURVEY_IMAGES.ImageName as ImageName
		,SURVEY_IMAGES.ImagePath,LOCATION.LocationID,AREA.AreaID,  
		LOCATION.LocationName,AREA.AreaName,ITEM.ItemName      
FROM	PS_Survey_Item_Images SURVEY_IMAGES       
		INNER JOIN PA_ITEM ITEM on ITEM.ItemID = SURVEY_IMAGES.ItemId      
		INNER JOIN PA_AREA AREA on AREA.AreaID = ITEM.AreaID      
		INNER JOIN PA_LOCATION LOCATION on LOCATION.LocationID = AREA.LocationId      
		INNER JOIN PA_ITEM_PARAMETER ITEM_PARAM ON ITEM_PARAM.ItemId = ITEM.ItemID
		INNER JOIN PA_PROPERTY_ATTRIBUTES PROP_ATTRIB ON PROP_ATTRIB.ITEMPARAMID = ITEM_PARAM.ItemParamID
WHERE	PROP_ATTRIB.PROPERTYID=@propertyID AND SURVEY_IMAGES.SurveyId = @surveyId AND PROP_ATTRIB.IsUpdated= 1       
       
--==========================================================================
-- (11) HEATING INFO
--==========================================================================
SELECT PropertyHeatings.HeatingId, 'Heating Type ' + convert(nvarchar(20), (ROW_NUMBER() OVER (ORDER BY PropertyHeatings.HeatingId ASC))) as HeatingName
FROM	(SELECT	DISTINCT COALESCE(PSP.HeatingMappingId,PSID.HeatingMappingId) as HeatingId
		FROM	PS_Survey PS
				LEFT JOIN 	PS_Survey_Parameters PSP ON PS.SURVEYID = PSP.SURVEYID
				LEFT JOIN PS_Survey_Item_Dates PSID ON PS.SURVEYID = PSID.SURVEYID
		WHERE	PS.SURVEYID  = @surveyId 
				AND PS.PROPERTYID = @propertyID  
				AND (PSP.HeatingMappingId IS NOT NULL OR PSID.HeatingMappingId IS NOT NULL)		
		) PropertyHeatings

 
END   
