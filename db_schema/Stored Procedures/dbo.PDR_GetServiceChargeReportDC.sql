USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetServiceChargeReportDC]    Script Date: 26/10/2018 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetServiceChargeReportDC') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeReportDC AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeReportDC]
		@filterText VARCHAR(200),
	-- Add the parameters for the stored procedure here
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ITEMID', 
		@sortOrder varchar (5) = 'DESC',
		@fiscalYear int,
		@totalCount int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
	
		@SelectClause varchar(3000),
		@CTEClause varchar (max),
        @fromClause   varchar(max),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
		@YStart varchar(100),
		@YEnd varchar(100),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
		
		--variables for paging
        @offset int,
		@limit int
		
		select @YStart=YStart,@YEnd=YEnd From F_FISCALYEARS where YRange=@fiscalYear
		
		SET @searchCriteria = '(S.SchemeId > 0 OR B.BlockId > 0 ) '
		IF(@filterText != '' OR @filterText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND '+  @filterText
		END	
	
	  
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		SET @CTEClause='With TXNData As ( Select CL.INCSC,JE.SCHEMEID,JE.TIMECREATED,CL.TXNId, 
			ISNULL(JE.REFNUMBER,''-'') AS DETAILS,
			''�'' + ISNULL(CONVERT(varchar,CL.PropertyApportionment,1),''0.00'') as APPORTIONMENTBUDGET,
			''�'' + ISNULL(CONVERT(varchar,CL.Amount,1),''0.00'') as ACTUALTOTAL
			From NL_JOURNALENTRY JE 									
			LEFT JOIN NL_JOURNALENTRYCREDITLINE CL on CL.TXNID = JE.TXNID 
			
			UNION ALL
			SELECT DL.INCSC, JE.SCHEMEID,JE.TIMECREATED,DL.TXNId,  
			ISNULL(JE.REFNUMBER,''-'') AS DETAILS,
			''�'' + ISNULL(CONVERT(varchar,DL.PropertyApportionment,1),''0.00'') as APPORTIONMENTBUDGET,
			''�'' + ISNULL(CONVERT(varchar,DL.Amount,1),''0'') as ACTUALTOTAL
			From NL_JOURNALENTRY JE 									
			LEFT JOIN NL_JOURNALENTRYDEBITLINE DL ON DL.TXNID = JE.TXNID
			 )'		
		SET @SelectClause = 'Select DISTINCT top ('+convert(nvarchar(10),@limit)+')
							 ISNULL(S.SCHEMENAME,''-'') as SCHEMENAME
							,ISNULL(B.BLOCKNAME,''-'') as BLOCKNAME
							,0 as InPropCount
							,''-'' as ITEMNAME
							,''�0'' as BUDGET 

							,case
							when  (TD.INCSC is not null AND TD.INCSC=1 ) then TD.APPORTIONMENTBUDGET
							when (select count(*) from TXNData where TXNData.TXNId=TD.TXNId and TXNData.INCSC=1 ) >0 then TD.APPORTIONMENTBUDGET								
							else ''�0'' end as APPORTIONMENTBUDGET

							,case 
							when (TD.INCSC is not null AND TD.INCSC=1 ) then  TD.ACTUALTOTAL
							when (select count(*) from TXNData where TXNData.TXNId=TD.TXNId and TXNData.INCSC=1 ) >0 then TD.ACTUALTOTAL
							else ''�0'' END as ACTUALTOTAL	
																	
							,CONVERT(varchar,''�0'') as APPORTIONMENTACTUAL
							,ISNULL(p.PropertyCount,'' '') as PROPERTYCOUNT
							,0 as ITEMID
							,(SELECT 
                                COUNT(*) AS Count
							    FROM   P__PROPERTY P
		                        where P.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ServiceChargeProperties P1 
								where P1.IsIncluded=1 AND P1.IsActive=1 AND P1.SCHEMEID=S.SchemeId 
								GROUP BY P1.BLOCKID, P1.PROPERTYID)	and P.SCHEMEID=S.SchemeId) + 
							(SELECT 
                                COUNT(*) AS Count
							    FROM   P__PROPERTY P
		                        where P.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ServiceChargeProperties P1 
								where P1.IsIncluded=1 AND P1.IsActive=1 AND P1.BLOCKID=B.BLOCKID 
								GROUP BY P1.BLOCKID, P1.PROPERTYID)	and P.BLOCKID=B.BLOCKID)
								AS ExPropCount
							,ISNULL(S.SCHEMEID,0) AS SCHEMEID
							,ISNULL(B.BLOCKID,0) AS BLOCKID
							,TD.DETAILS  AS DETAILS
							,TD.TIMECREATED AS DateCreated
							,TD.TXNId AS Ref
							,CONVERT(BIT, 0) AS IsServiceChargePO 
							'
				
		SET @fromClause = CHAR(10) +' FROM P_SCHEME S 
									LEFT JOIN P_BLOCK B ON B.SCHEMEID= S.SCHEMEID
									INNER JOIN TXNData TD ON TD.SCHEMEID = S.SCHEMEID  AND ' + @searchCriteria + '
									LEFT JOIN (SELECT COUNT(prop.BlockID) AS PropertyCount, prop.BLOCKID FROM P__PROPERTY prop GROUP BY prop.BLOCKID) P ON B.BLOCKID = P.BLOCKID'
								   
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		SET @whereClause =	CHAR(10) + 'WHERE 1=1 AND' + CHAR(10) + @searchCriteria 
		



		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =@CTEClause + ' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		
		EXEC (@finalQuery)
		--print(@finalQuery)
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= @CTEClause + 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
