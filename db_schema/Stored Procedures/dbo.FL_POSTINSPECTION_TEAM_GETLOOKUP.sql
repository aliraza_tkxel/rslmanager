SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[FL_POSTINSPECTION_TEAM_GETLOOKUP]

/* ===========================================================================
 '   NAME:          FL_POSTINSPECTION_TEAM_GETLOOKUP
 '   DATE CREATED:   20TH FEBRUARY, 2009
 '   CREATED BY:       TAHIR GUL
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get team from E_Team table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT TEAMID AS id,TEAMNAME AS val
	FROM E_TEAM
	WHERE TEAMID IN (116,117,104)
	ORDER BY TEAMNAME ASC



GO
