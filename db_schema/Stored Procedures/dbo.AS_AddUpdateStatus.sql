
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC	[AS_AddUpdateStatus]
--		@Title = N'Hello',
--		@Ranking = 2,
--		@CreatedBy = 1,
--		@InspectionTypeId = 3,
--		@modifiedBy = 1
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AS_AddUpdateStatus](
	-- Add the parameters for the stored procedure here
	@Title varchar(1000),
	@Ranking int,
	@CreatedBy int,
	@InspectionTypeId int,
	@modifiedBy int
	)
AS
BEGIN
Declare @oldStatusId int
Declare @countRanking int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		
		
   SELECT @oldStatusId = StatusId from AS_Status WHERE AS_Status.Ranking = @Ranking
   SELECT @countRanking = COUNT(*) FROM AS_Status
	
	if @oldStatusId > 0
		BEGIN 
			update AS_Status SET Ranking = @countRanking WHERE StatusId = @oldStatusId
			declare @oldStatusTitle varchar(100)
			declare @oldStatusInspectionTypeId int
			SELECT @oldStatusTitle = Title,@oldStatusInspectionTypeId = InspectionTypeId FROM AS_Status WHERE StatusId = @oldStatusId
			INSERT INTO AS_StatusHistory(StatusId,Title,Ranking,CreatedBy,InspectionTypeID,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
			VALUES (@oldStatusId,@oldStatusTitle,@countRanking,@CreatedBy,@oldStatusInspectionTypeId,@modifiedBy,GETDATE(),GETDATE(),1)
			
			INSERT INTO AS_Status (Title,Ranking,CreatedBy,InspectionTypeId,IsEditable,CreatedDate)
			VALUES (@Title,@Ranking,@CreatedBy,@InspectionTypeId,1,GETDATE())
			INSERT INTO AS_StatusHistory(StatusId,Title,Ranking,CreatedBy,InspectionTypeID,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
			VALUES (@@IDENTITY,@Title,@Ranking,@CreatedBy,@InspectionTypeId,@modifiedBy,GETDATE(),GETDATE(),1)
		END
	ELSE
		BEGIN
			INSERT INTO AS_Status (Title,Ranking,CreatedBy,InspectionTypeId,IsEditable,CreatedDate)
			VALUES (@Title,@Ranking,@CreatedBy,@InspectionTypeId,1,GETDATE())
			
			INSERT INTO AS_StatusHistory(StatusId,Title,Ranking,CreatedBy,InspectionTypeID,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
			VALUES (@@IDENTITY,@Title,@Ranking,@CreatedBy,@InspectionTypeId,@modifiedBy,GETDATE(),GETDATE(),1)
		END
	--if (exists (SELECT * from AS_Status Where Ranking = @rankTobeUpdated)AND @rankTobeUpdated > 0)
	--	begin
	--	   update AS_STATUS SET  Ranking = @NewRanking , ModifiedBy = @modifiedBy
	--	   where StatusId = @StatusIdTOBeUpdated
	--	   insert AS_STATUS (Title,Ranking,CreatedBy,InspectionTypeId,IsEditable,CreatedDate)
	--	   values (@newTitle,@rankTobeUpdated,@CreatedBy,@InspectionTypeId,1,GETDATE())
	--	end
	--	Else
	--	Begin
	--	insert AS_STATUS (Title,Ranking,CreatedBy,InspectionTypeId,IsEditable,CreatedDate)
	--	   values (@newTitle,@NewRanking,@CreatedBy,@InspectionTypeId,1,GETDATE())
	--	End
END
GO
