
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 07/05/2014
-- Description:	Get the customerId and customerName as tenants of the given property.
-- EXEC PLANNED_GetContactDropDownValuesbyPropertyId @propertyId='D130050005'
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetContactDropDownValuesbyPropertyId] 
	-- Add the parameters for the stored procedure here
	@propertyId NVARCHAR(20) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT C.CUSTOMERID AS [id],
			C.FIRSTNAME + ' ' + C.LASTNAME AS [description]
	FROM C_TENANCY T
	INNER JOIN C_CUSTOMERTENANCY CT ON T.TENANCYID = CT.TENANCYID AND (T.ENDDATE >= CONVERT(DATE,GETDATE()) OR T.ENDDATE IS NULL)
	INNER JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID AND (CT.ENDDATE >= CONVERT(DATE,GETDATE()) OR CT.ENDDATE IS NULL)
	WHERE T.PROPERTYID = @propertyId

END
GO
