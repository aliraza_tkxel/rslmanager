SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_JOB_TYPE_UPDATE]
(
	@JobType nvarchar(30),
	@Original_JobTypeId bigint,
	@JobTypeId bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [I_JOB_TYPE] SET [JobType] = @JobType WHERE (([JobTypeId] = @Original_JobTypeId));
	
SELECT JobTypeId, JobType FROM I_JOB_TYPE WHERE (JobTypeId = @JobTypeId)
GO
