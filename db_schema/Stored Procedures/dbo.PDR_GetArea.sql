USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetArea]    Script Date: 28-Feb-17 11:20:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_GetArea]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetArea] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetArea]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT  Distinct PA_LOCATION.LocationID, PA_AREA.AreaID AS [id], 
			CASE WHEN PA_LOCATION.LocationName is Null then
			PA_AREA.AreaName
			--+' > '+PA_Item.ItemName
			ELSE
			PA_LOCATION.LocationName+' > '+PA_AREA.AreaName
			end
			AS [description] 
	FROM	PA_AREA 
			Left JOIN PA_LOCATION ON PA_AREA.LocationId = PA_LOCATION.LocationID
			--Inner JOIN PA_Item ON PA_AREA.AreaID = PA_Item.AreaID
	WHERE	PA_AREA.IsActive = 1  
	--AND PA_Item.IsActive = 1  
	ORDER BY PA_LOCATION.LocationID  ASC
END