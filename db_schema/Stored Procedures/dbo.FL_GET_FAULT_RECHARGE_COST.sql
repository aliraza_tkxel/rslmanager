SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC dbo.FL_GET_FAULT_RECHARGE_COST
/* ===========================================================================
 '   NAME:           FL_GET_FAULT_RECHARGE_COST
 '   DATE CREATED:   19th Oct 2009
 '   CREATED BY:     Adnan Mirza
 '   CREATED FOR:    Fault Locator
 '   PURPOSE:        To get the fault recharge cost
 '   IN:             @QuestionId
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@FaultId INT	
	)	
	
AS 	

SELECT NETCOST FROM FL_FAULT WHERE FAULTID= @FaultId 








GO
