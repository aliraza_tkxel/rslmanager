USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get available Properties for Report 
    Author: Ali Raza
    Creation Date: June-28-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         June-28-2015      Ali Raza         Get available Properties for Report 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetAvailableProperties]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
IF OBJECT_ID('dbo.V_GetAvailableProperties') IS NULL 
	EXEC('CREATE PROCEDURE dbo.V_GetAvailableProperties AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[V_GetAvailableProperties]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Ref', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' ( ( P.STATUS = 1 AND (P.SUBSTATUS <> 21 OR P.SUBSTATUS IS NULL) ) OR 
		(P.Status=2 AND P.SUBSTATUS = 22 ) )'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ( P.PROPERTYID LIKE ''%' + @searchText + '%'' ) OR ( ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''')  LIKE ''%' + @searchText + '%''))'
		END	
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select distinct top ('+convert(varchar(10),@limit)+') 
							  P.PROPERTYID As Ref,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address, P.TOWNCITY, P.POSTCODE  
,ISNULL(S.SCHEMENAME,''-'') as Scheme,ISNULL(B.BLOCKNAME,''-'') AS Block,ISNULL(Convert(Varchar(50), T.TerminationDate,103),''-'') as Termination,
convert(DATETIME, T.TerminationDate,103) as TerminationDate,
ISNULL(Convert(Varchar(50),T.ReletDate,103),''-'') as Relet,ISNULL(ST.DESCRIPTION,''-'') AS Status
,ISNULL(SUB.DESCRIPTION,''-'') as SubStatus, ISNULL(Convert(Varchar(50),TENANCY.STARTDATE,103),''-'') as ActualRelet,
 TENANCY.STARTDATE  as ActualRelet_col, 
 ISNULL(Convert(Varchar(50),VoidWorks.WorksCompletionDate,103),''-'') as WorksCompletionDate,
 VoidWorks.WorksCompletionDate as WorksCompletionDate_col '  
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM P__PROPERTY P  

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P.PropertyID
AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
 GROUP BY PROPERTYID) as CJournal
CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination
Cross Apply (SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
		Case When T.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
		ELSE Convert(Varchar(50), T.ReletDate,103)END as ReletDate,J.JOURNALID
		FROM P__PROPERTY P
    INNER JOIN C_JOURNAL J ON CJournal.journalID=J.JOURNALID
	INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
	INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
	
 ) As  T-- ON P.PROPERTYID=T.PROPERTYID
 
 LEFT JOIN ( SELECT CT.STARTDATE, T.PROPERTYID 
FROM C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON T.TENANCYID = CT.TENANCYID  
WHERE CT.ENDDATE is NULL AND ( CUSTOMERTENANCYID = ( SELECT max( CUSTOMERTENANCYID ) FROM C_CUSTOMERTENANCY C_CT WHERE C_CT.TENANCYID = CT.TENANCYID ) )
) AS TENANCY ON  TENANCY.PROPERTYID = P.PROPERTYID   

LEFT JOIN 
(	select M.PropertyId, max(V.JobsheetCompletionDate) as WorksCompletionDate
	From PDR_JOURNAL J    
	INNER JOIN PDR_MSAT M   ON J.MSATID = M.MSATId   
	INNER JOIN V_RequiredWorks V   ON J.JOURNALID = V.WorksJournalId 
	where V.JobsheetCompletionDate is not null AND
	M.PropertyId NOT IN 
		(select DISTINCT PDR_MSAT.PropertyId From  PDR_JOURNAL     
		INNER JOIN PDR_MSAT   ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId   
		INNER JOIN V_RequiredWorks ON PDR_JOURNAL.JOURNALID = V_RequiredWorks.WorksJournalId
		where V_RequiredWorks.JobsheetCompletionDate is Null) 
	group by  M.PropertyId
) AS VoidWorks ON  VoidWorks.PROPERTYID = P.PROPERTYID      '  
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Ref' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TerminationDate' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END

		IF(@sortColumn = 'ActualRelet')  
		BEGIN  
			SET @sortColumn = CHAR(10)+ 'ActualRelet_col'    
		END  

		IF(@sortColumn = 'WorksCompletionDate')  
		BEGIN  
			SET @sortColumn = CHAR(10)+ 'WorksCompletionDate_col'    
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT P.PropertyID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
