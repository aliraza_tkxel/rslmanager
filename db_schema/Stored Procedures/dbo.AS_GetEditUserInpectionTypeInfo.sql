SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetEditUserInpectionTypeInfo @employeeId = @employeeId
-- Author:	<Salman Nazir>
-- Create date: <10/1/2012>
-- Description:	<On Edit User page get the user InspectionType info>
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetEditUserInpectionTypeInfo](
@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT AS_USER_INSPECTIONTYPE.InspectionTypeID, P_INSPECTIONTYPE.Description
		From AS_USER_INSPECTIONTYPE INNER JOIN
		P_INSPECTIONTYPE on AS_USER_INSPECTIONTYPE.InspectionTypeID = P_INSPECTIONTYPE.InspectionTypeID
		Where AS_USER_INSPECTIONTYPE.EmployeeId = @employeeId
END
GO
