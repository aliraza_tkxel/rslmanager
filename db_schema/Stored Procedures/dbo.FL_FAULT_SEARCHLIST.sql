SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE [dbo].[FL_FAULT_SEARCHLIST] 

/* ===========================================================================
 '   NAME:           FL_FAULT_SEARCHLIST
 '   DATE CREATED:   20th OCTOBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist faults
 '   IN:                      @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	 
	 
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FaultID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
          
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
                           CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.FaultID, FL_LOCATION.LocationName,' +
                        CHAR(10) + CHAR(9) + 'FL_AREA.AreaName, FL_ELEMENT.ElementName,' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.Description, FL_FAULT_PRIORITY.PriorityName AS Priority, ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY.ResponseTime, FL_FAULT.NetCost, ' +
	           CHAR(10) + CHAR(9) + 'Case  FL_FAULT_PRIORITY.Days'+
	           CHAR(10) +CHAR(9) +' When 0 then ''Hour(s)'' when 1 then ''Day(s)''  end as Type ,' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.Vat, FL_FAULT.Gross,Case FL_FAULT.FaultActive when 0 then ''Inactive'' when 1 then ''Active'' end as FaultActive'   
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_LOCATION ON FL_AREA.LocationID = FL_LOCATION.LocationID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID'
                    
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FaultID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FaultID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause = CHAR(10)+  CHAR(10) + 'WHERE (' +
                        CHAR(10) + CHAR (9) + @sortColumn + ' NOT IN' + 
                        CHAR(10) + CHAR (9) + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' ' + @sortColumn +
                        + @FromClause + @OrderClause + ')'+
                        CHAR(10) + CHAR (9) + 'AND 1 = 1  )'                 
     
                        
    -- End building WHERE clause
    --========================================================================================
        
	
--PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)













GO
