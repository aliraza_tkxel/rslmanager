SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_DELETECUSTOMERRESPONSE]

/* ===============================================================
' NAME: TO_ENQUIRY_LOG_DELETECUSTOMERRESPONSE
' DATE CREATED: 24 Sep. 2008
' CREATED BY: Waseem Hassan
' CREATED FOR: Broadland Housing
' PURPOSE: To delete equirylog entry of customer response
' UPDATE: TO_CUSTOMER_RESPONSE.CustResponseId
' IN: @custResponseId 
' OUT: 
' RETURN: Nothing 
' VERSION: 1.0 
' COMMENTS: 
' MODIFIED ON: 
' MODIFIED BY: 
' REASON MODIFICATION: 
'==============================================================*/
@custResponseId int



AS


/* ' update Archived and set it to 1 */


UPDATE 
TO_CUSTOMER_RESPONSE

SET
TO_CUSTOMER_RESPONSE.Archived = '1'

WHERE 
( TO_CUSTOMER_RESPONSE.CustResponseId= @custResponseId )
GO
