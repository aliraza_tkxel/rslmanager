SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Muhammad Abdul Wahhab
-- Create date: 03-08-2013
-- Description:	Get the available hours of an operative with in specific dates
--DECLARE	@return_value int,
--		@totalTime float

--EXEC	@return_value = [dbo].[TS_GetAvailableHours]
--		@operativeID = 615,
--		@fromDate = N'01/01/2013',
--		@toDate = N'07/08/2013',
--		@totalTime = @totalTime OUTPUT

--SELECT	@totalTime as N'@totalTim
-- =============================================
CREATE PROCEDURE [dbo].[TS_GetAvailableHours] 
	-- Add the parameters for the stored procedure here
	@operativeID int,
	@fromDate datetime,
	@toDate datetime,
	@totalTime float output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET DATEFIRST 1;

    -- Insert statements for procedure here
	DECLARE @loopDate datetime
	DECLARE @dailyTime float
	DECLARE @day int	

	SET @loopDate = @fromDate
	SET @dailyTime = 0
	SET @totalTime = 0
	
	-- Looping between the specified dates	

	WHILE @loopDate <= @toDate
	BEGIN
	
		-- Getting day of the week

		SET @day = DATEPART(DW,@loopDate)
	
		-- print convert(nvarchar(50),@day)
			
		-- If day is Monday then fetching value
		IF @day = 1
		BEGIN
			SELECT @dailyTime = Mon
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END
		
		-- If day is Tuesday then fetching value
		IF @day = 2
		BEGIN
			SELECT @dailyTime = Tue
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END
		
		-- If day is Wednesday then fetching value
		IF @day = 3
		BEGIN
			SELECT @dailyTime = Wed
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END
		
		-- If day is Thursday then fetching value
		IF @day = 4
		BEGIN
			SELECT @dailyTime = Thu
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END
		
		-- If day is Friday then fetching value
		IF @day = 5
		BEGIN
			SELECT @dailyTime = Fri
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END
		
		-- If day is Saturday then fetching value
		IF @day = 6
		BEGIN
			SELECT @dailyTime = Sat
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END
		
		-- If day is Sunday then fetching value
		IF @day = 7
		BEGIN
			SELECT @dailyTime = Sun
			FROM E_WorkingHours
			WHERE EmployeeId = @operativeID
		END	
	
		--print convert(nvarchar(50),@loopdate) + ' ' + convert(nvarchar(50),@dailyTime)
		
		-- Summing up the time and increamenting date by one day for the loop
	
		SET @totalTime = @totalTime + @dailyTime
	
		SET @dailyTime = 0
	
		SET @loopDate = DATEADD(day,1,@loopDate)
	
	END

	-- Return the result of the function
	RETURN @totalTime
	
END

GO
