
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[TO_C_ARREARS_ManageArrears]

 /*=============================================================================
    Important Note:- The Query is taken from ASP pages of RSLManager application. 
    Required changes are made in consultation with Adnan(Reidmark)
 ===============================================================================*/


 /* ===========================================================================
 '   NAME:           TO_C_ARREARS_MANAGEARREARS
 '   DATE CREATED:   29 MAY 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Selects latest comments entered by CRM for csutomer and custoemr's
 '                   current arrears position
 '   IN:             @customerID INT 
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    12/06/2013 
 '   MODIFIED BY:    Nataliya Alexander
 '   REASON MODIFICATION: Updated to read from the new arrears module
 '==============================================================================*/
 
 
  @tenancyId INT
  
AS

    SELECT  
			ISNULL(S.Title, 'N/A') AS [ACTION]
    FROM    
			AM_CASE CS
            INNER JOIN dbo.AM_Status S ON CS.StatusId = S.StatusId
    WHERE   
			CS.IsActive = 1
			AND
			CS.TenancyId = @tenancyId
GO
