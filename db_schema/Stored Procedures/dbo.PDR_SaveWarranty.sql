USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveWarranty]    Script Date: 16-Oct-17 11:06:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,05/12/2014>
-- Description:	<Description,,Save Warranty for the Block>
-- =============================================
IF OBJECT_ID('dbo.PDR_SaveWarranty') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_SaveWarranty AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_SaveWarranty] 
	   @ID varchar(100) 
      ,@WARRANTYTYPE int
      ,@CONTRACTOR int 
      ,@EXPIRYDATE smalldatetime
      ,@NOTES varchar(500)
      ,@requestType nvarchar (50)
      ,@WarrantyId int
	  ,@Category int
	  ,@Area int
	  ,@Item int
     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF (@WarrantyId > 0)
		BEGIN 
			Update PDR_WARRANTY Set WARRANTYTYPE= @WARRANTYTYPE,CONTRACTOR= @CONTRACTOR,EXPIRYDATE= @EXPIRYDATE,NOTES = @NOTES,Category=@Category,Area= @Area, Item=@Item WHERE WARRANTYID = @WarrantyId
		END
    ELSE
		BEGIN
			If (@requestType = 'Block')
				BEGIN
			    
					INSERT INTO PDR_WARRANTY(BLOCKID,WARRANTYTYPE,CONTRACTOR,EXPIRYDATE,NOTES,Category,Area,Item)
					VALUES (@ID,@WARRANTYTYPE,@CONTRACTOR,@EXPIRYDATE,@NOTES,@Category,@Area,@Item)
				END
			ELSE IF(@requestType = 'Scheme')
				BEGIN 
					INSERT INTO PDR_WARRANTY(SCHEMEID,WARRANTYTYPE,CONTRACTOR,EXPIRYDATE,NOTES,Category,Area,Item)
					VALUES (@ID,@WARRANTYTYPE,@CONTRACTOR,@EXPIRYDATE,@NOTES,@Category,@Area,@Item)
				END
			ELSE 
				BEGIN 
					INSERT INTO PDR_WARRANTY(PROPERTYID,WARRANTYTYPE,CONTRACTOR,EXPIRYDATE,NOTES,Category,Area,Item)
					VALUES (@ID,@WARRANTYTYPE,@CONTRACTOR,@EXPIRYDATE,@NOTES,@Category,@Area,@Item)
				END
		END		
END
