USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[TS_GetLeavesForEmployee]    Script Date: 2/9/2017 3:19:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.[TS_GetLeavesForEmployee]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[TS_GetLeavesForEmployee] AS SET NOCOUNT ON;') 
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[TS_GetLeavesForEmployee] 
	-- Add the parameters for the stored procedure here
	@fromDate		DATETIME
	,@toDate		DATETIME
	,@employeeId	int
	,@isSick		int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE	@natureId AS INT
	declare @sickId int 
	select @sickId = ITEMNATUREID from E_NATURE where DESCRIPTION = 'Sickness'
	PRINT @sickId
	if(@isSick = 0)
	Begin
		SELECT DISTINCT convert(NVARCHAR, E_ABSENCE.STARTDATE,103) as StarDate ,Convert(NVARCHAR, E_ABSENCE.RETURNDATE,103)as EndDate,E_NATURE.DESCRIPTION as LeaveType
		from E_ABSENCEHOURS
		INNER JOIN E_JOURNAL ON E_ABSENCEHOURS.EmployeeID = E_JOURNAL.EMPLOYEEID
		INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID and E_ABSENCEHOURS.AbsenceDate = STARTDATE
		INNER JOIN E_NATURE ON E_JOURNAL.ITEMNATUREID = E_NATURE.ITEMNATUREID
		where E_ABSENCEHOURS.EmployeeID=@employeeId and
		E_NATURE.ITEMNATUREID!=1  AND 
		E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) 
		FROM E_ABSENCE  GROUP BY JOURNALID )
		AND E_ABSENCE.STARTDATE  BETWEEN  @fromDate and @toDate
	End

	ELSE
	BEGIN
		SELECT DISTINCT convert(NVARCHAR, E_ABSENCE.STARTDATE,103) as StarDate ,Convert(NVARCHAR, E_ABSENCE.RETURNDATE,103)as EndDate,E_NATURE.DESCRIPTION as LeaveType
		from E_ABSENCEHOURS
		INNER JOIN E_JOURNAL ON E_ABSENCEHOURS.EmployeeID = E_JOURNAL.EMPLOYEEID
		INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID and E_ABSENCEHOURS.AbsenceDate = STARTDATE
		INNER JOIN E_NATURE ON E_JOURNAL.ITEMNATUREID = E_NATURE.ITEMNATUREID
		where E_ABSENCEHOURS.EmployeeID=@employeeId and
		E_NATURE.ITEMNATUREID = 1  AND 
		E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) 
		FROM E_ABSENCE  GROUP BY JOURNALID )
		AND E_ABSENCE.STARTDATE  BETWEEN  @fromDate and @toDate
	END
	 
	
END

GO

