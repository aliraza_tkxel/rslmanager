USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectsArrangedList]    Script Date: 06/08/2016 11:05:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.DF_GetDefectsArrangedList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetDefectsArrangedList AS SET NOCOUNT ON;') 
GO
/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  10/09/2015
	Description: Get Defect Arranged List to show in defect scheduling.
    Change History:    
    
Execution Command:
----------------------------------------------------

DECLARE	@totalCount int

EXEC	[dbo].[DF_GetDefectsArrangedList] @sortColumn = 'Address',
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_GetDefectsArrangedList](
		
		@searchText varchar(5000)='',		
		@schemeId int = -1,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'AppointmentDateSort',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(max),
		@SelectClauseCount varchar(max),
        @fromClause   varchar(max),
        @whereClause  varchar(max),	    
		@SelectClauseScheme varchar(max),
		@SelectClauseSchemeCount varchar(max),
        @fromClauseScheme   varchar(max),
        @whereClauseScheme  varchar(max),	
		@SelectClauseBlock varchar(max),
		@SelectClauseBlockCount varchar(max),
        @fromClauseBlock   varchar(max),
        @whereClauseBlock  varchar(max),	    
        @orderClause  varchar(max),	
        @mainSelectQuery varchar(max),        
        @rowNumberQuery varchar(max),
        @finalQuery varchar(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        @searchCriteriaScheme varchar(5000),
		@searchCriteriaBlock varchar(5000),
        @unionQuery varchar(1000),
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 
								AND (AD.detectortypeid IS NULL OR AD.detectortypeid = 0)
								AND ISNULL(M.isPending,1) = 0
								AND (((AD.ApplianceId IS NOT NULL AND AD.ApplianceId <> 0 ) AND PA.ISACTIVE =1) OR (AD.ApplianceId IS NULL or AD.ApplianceId = 0 ))   
								AND DS.TITLE IN (''Arranged'', ''No Entry'', ''Paused'') '

		SET @searchCriteriaScheme = @searchCriteria

		SET @searchCriteriaBlock = @searchCriteria
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ((ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND (ISNULL(P.SCHEMENAME, '''') LIKE ''%' + @searchText + '%'')'
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND ((ISNULL(P.BLOCKNAME, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'') '
			SET @searchCriteria = @searchCriteria + CHAR(10) + '	OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + '	OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + '	OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
		END
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
				
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AD
											INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId											  
											INNER JOIN PDR_STATUS DS ON AD.DefectJobSheetStatus = DS.STATUSID
											LEFT JOIN GS_PROPERTY_APPLIANCE PA ON AD.ApplianceId = PA.PROPERTYAPPLIANCEID
											LEFT JOIN GS_APPLIANCE_TYPE AT ON PA.APPLIANCETYPEID = AT.APPLIANCETYPEID
											LEFT JOIN PA_PARAMETER_VALUE PPV ON AD.BoilerTypeId =PPV.ValueID
											INNER JOIN P__PROPERTY P ON AD.PropertyId = P.PROPERTYID
											INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
											INNER JOIN PDR_MSAT M ON PJ.MSATID = M.MSATId
											INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
											INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID											
									'

		SET @fromClauseScheme = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AD
											INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId											  
											INNER JOIN PDR_STATUS DS ON AD.DefectJobSheetStatus = DS.STATUSID
											LEFT JOIN GS_PROPERTY_APPLIANCE PA ON AD.ApplianceId = PA.PROPERTYAPPLIANCEID
											LEFT JOIN GS_APPLIANCE_TYPE AT ON PA.APPLIANCETYPEID = AT.APPLIANCETYPEID
											LEFT JOIN PA_PARAMETER_VALUE PPV ON AD.BoilerTypeId =PPV.ValueID
											INNER JOIN P_SCHEME P ON AD.SchemeId = P.SchemeId
											INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
											INNER JOIN PDR_MSAT M ON PJ.MSATID = M.MSATId
											INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
											INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID											
									'

		SET @fromClauseBlock = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AD
											INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId											  
											INNER JOIN PDR_STATUS DS ON AD.DefectJobSheetStatus = DS.STATUSID
											LEFT JOIN GS_PROPERTY_APPLIANCE PA ON AD.ApplianceId = PA.PROPERTYAPPLIANCEID
											LEFT JOIN GS_APPLIANCE_TYPE AT ON PA.APPLIANCETYPEID = AT.APPLIANCETYPEID
											LEFT JOIN PA_PARAMETER_VALUE PPV ON AD.BoilerTypeId =PPV.ValueID
											INNER JOIN P_BLOCK P ON AD.BLOCKID = P.BLOCKID
											INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
											INNER JOIN PDR_MSAT M ON PJ.MSATID = M.MSATId
											INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
											INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID								
		
									'
		--=======================Select Clause=============================================
		SET @SelectClause = '
									 RIGHT(''00000'' + CONVERT(NVARCHAR, AD.PropertyDefectId), 5)				AS JSD
									,P.HOUSENUMBER + '' '' + P.ADDRESS1 + '', '' + P.TOWNCITY					AS Address
									,P.POSTCODE																	AS Postcode
									,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END	AS Appliance
									,DC.Description																AS Defect
									,CASE
										DS.TITLE
										WHEN ''No Entry''
											THEN RTRIM(DS.TITLE) + ''('' + CONVERT(NVARCHAR,
												(
													SELECT
														COUNT(ADH.IDENTITYCOL)
													FROM
														P_PROPERTY_APPLIANCE_DEFECTS_HISTORY ADH
													WHERE
														ADH.PropertyDefectId = AD.PropertyDefectId
														AND ADH.DefectJobSheetStatus = DS.STATUSID
												)
												) + '')''
										ELSE DS.TITLE
									END																			AS DefectStatus
									,E.FIRSTNAME + '' '' + E.LASTNAME											AS Engineer
									,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108)
									+ '' - '' + CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTENDTIME), 108)	AS AppointmentTimes
									,LEFT(DATENAME(WEEKDAY, APPOINTMENTSTARTDATE), 3)
									+ '' '' + CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 106)					AS AppointmentDate
									,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME							AS AppointmentDateSort
									,AD.PropertyDefectId														AS DefectId
									,A.APPOINTMENTID															AS AppointmentId
									,P.HouseNumber																AS HouseNumber									
									,P.ADDRESS1																	AS PrimaryAddress	
									, ''''	AS Scheme
									, ''''	AS Block	
									,''Scheme'' AS AppointmentType
									,CAST(SUBSTRING(P.HouseNumber, 1,CASE when patindex(''%[^0-9]%'',P.HouseNumber) > 0 THEN PATINDEX(''%[^0-9]%'',P.HouseNumber) - 1 ELSE LEN(P.HouseNumber) END) AS INT) as SortAddress																										
								
							'	
		SET @SelectClauseCount = '  SELECT  ' + @SelectClause
		SET @SelectClause = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClause
				
		SET @SelectClauseScheme = ' 
									 RIGHT(''00000'' + CONVERT(NVARCHAR, AD.PropertyDefectId), 5)				AS JSD
									,''''					AS Address
									,''''															AS Postcode
									,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END	AS Appliance
									,DC.Description																AS Defect
									,CASE
										DS.TITLE
										WHEN ''No Entry''
											THEN RTRIM(DS.TITLE) + ''('' + CONVERT(NVARCHAR,
												(
													SELECT
														COUNT(ADH.IDENTITYCOL)
													FROM
														P_PROPERTY_APPLIANCE_DEFECTS_HISTORY ADH
													WHERE
														ADH.PropertyDefectId = AD.PropertyDefectId
														AND ADH.DefectJobSheetStatus = DS.STATUSID
												)
												) + '')''
										ELSE DS.TITLE
									END																			AS DefectStatus
									,E.FIRSTNAME + '' '' + E.LASTNAME											AS Engineer
									,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108)
									+ '' - '' + CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTENDTIME), 108)	AS AppointmentTimes
									,LEFT(DATENAME(WEEKDAY, APPOINTMENTSTARTDATE), 3)
									+ '' '' + CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 106)					AS AppointmentDate
									,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME							AS AppointmentDateSort
									,AD.PropertyDefectId														AS DefectId
									,A.APPOINTMENTID															AS AppointmentId
									,P.SCHEMENAME																AS HouseNumber									
									,P.SCHEMENAME																AS PrimaryAddress	
									, P.SCHEMENAME	AS Scheme
									, ''''	AS Block	
									,''Scheme'' AS AppointmentType
									,CAST(SUBSTRING(P.SCHEMENAME, 1,CASE when patindex(''%[^0-9]%'',P.SCHEMENAME) > 0 THEN PATINDEX(''%[^0-9]%'',P.SCHEMENAME) - 1 ELSE LEN(P.SCHEMENAME) END) AS INT) as SortAddress																										
							'	
		SET @SelectClauseSchemeCount = '  SELECT  ' + @SelectClauseScheme
		SET @SelectClauseScheme = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseScheme
							
		SET @SelectClauseBlock = ' 
									RIGHT(''00000'' + CONVERT(NVARCHAR, AD.PropertyDefectId), 5)				AS JSD
									,''''					AS Address
									,P.POSTCODE														AS Postcode
									,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END	AS Appliance
									,DC.Description																AS Defect
									,CASE
										DS.TITLE
										WHEN ''No Entry''
											THEN RTRIM(DS.TITLE) + ''('' + CONVERT(NVARCHAR,
												(
													SELECT
														COUNT(ADH.IDENTITYCOL)
													FROM
														P_PROPERTY_APPLIANCE_DEFECTS_HISTORY ADH
													WHERE
														ADH.PropertyDefectId = AD.PropertyDefectId
														AND ADH.DefectJobSheetStatus = DS.STATUSID
												)
												) + '')''
										ELSE DS.TITLE
									END																			AS DefectStatus
									,E.FIRSTNAME + '' '' + E.LASTNAME											AS Engineer
									,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108)
									+ '' - '' + CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTENDTIME), 108)	AS AppointmentTimes
									,LEFT(DATENAME(WEEKDAY, APPOINTMENTSTARTDATE), 3)
									+ '' '' + CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 106)					AS AppointmentDate
									,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME							AS AppointmentDateSort
									,AD.PropertyDefectId														AS DefectId
									,A.APPOINTMENTID															AS AppointmentId
									,P.BLOCKNAME																AS HouseNumber									
									,P.BLOCKNAME																AS PrimaryAddress	
									, ''''	AS Scheme
									, P.BLOCKNAME + '' '' + P.ADDRESS1 + '', '' + P.TOWNCITY	AS Block	
									,''Block'' AS AppointmentType
									,CAST(SUBSTRING(P.BLOCKNAME, 1,CASE when patindex(''%[^0-9]%'',P.BLOCKNAME) > 0 THEN PATINDEX(''%[^0-9]%'',P.BLOCKNAME) - 1 ELSE LEN(P.BLOCKNAME) END) AS INT) as SortAddress								
							'	
		SET @SelectClauseBlockCount = '  SELECT  ' + @SelectClauseBlock
		SET @SelectClauseBlock = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseBlock									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		SET @whereClauseScheme =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaScheme 		
		SET @whereClauseBlock =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaBlock 		

		SET @unionQuery = CHAR(10) + ' UNION ALL ' + CHAR(10)

		IF(@getOnlyCount=0)
		BEGIN
			--============================Order Clause==========================================
			
			IF (@sortColumn = 'Address') BEGIN
				SET @sortColumn = CHAR(10) + ' SortAddress '
			END
							
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @unionQuery
									+  @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery 
									+  @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock 
									+ @orderClause 
			PRINT @selectClause +@fromClause + @whereClause
			PRINT @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme
			PRINT @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+ CHAR(10) + @sortColumn + CHAR(10) + @sortOrder +CHAR(10) + ') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			-- PRINT @rowNumberQuery
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			PRINT @finalQuery
			--============================ Exec Final Query =================================
	
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(max), 
		@parameterDef NVARCHAR(500)
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@SelectClauseCount+@fromClause+@whereClause+@unionQuery+@SelectClauseSchemeCount+@fromClauseScheme+
													@whereClauseScheme+@unionQuery+@SelectClauseBlockCount+@fromClauseBlock+@whereClauseBlock+') as Records ) '
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================

END
