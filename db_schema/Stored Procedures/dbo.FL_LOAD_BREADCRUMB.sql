SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_LOAD_BREADCRUMB
/* ===========================================================================
 '   NAME:           FL_LOAD_BREADCRUMB
 '   DATE CREATED:   6th Nov 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the bread crumb
 '   IN:             @CurPageId
 '   IN:             @CurPageIdType
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
	@CurPageId INT,	
	@CurPageIdType NVARCHAR(500)
	)	

AS 	


print @CurPageIdType
	

IF @CurPageIdType = 'Element'
	
	BEGIN		
	
		SELECT  lo.LocationID, lo.LocationName, ar.AreaID, ar.AreaName, el.ElementID, el.ElementName
		FROM FL_LOCATION lo, FL_AREA ar, FL_ELEMENT el
		WHERE el.AreaID = ar.AreaID
		AND ar.LocationID = lo.LocationID
		AND el.ElementID = @CurPageId 
	END	
ELSE IF @CurPageIdType = 'Fault'
	BEGIN
		SELECT  lo.LocationID, lo.LocationName, ar.AreaID, ar.AreaName, el.ElementID, el.ElementName,fa.Description, fa.FaultID
		FROM FL_LOCATION lo, FL_AREA ar, FL_ELEMENT el, FL_FAULT fa
		WHERE fa.ElementID = el.ElementID
		AND el.AreaID = ar.AreaID
		AND ar.LocationID = lo.LocationID
		AND fa.FaultID = @CurPageId
	END















GO
