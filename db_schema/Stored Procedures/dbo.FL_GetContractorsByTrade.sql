
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 -- =============================================
-- EXEC FL_GetContractorsByTrade
--  @tradeType = 'Plumber'
-- Author:		<Ahmed Mehmood>
-- Create date: <23/01/2013>
-- Description:	<Get Contractors list against Trade name>
-- Web Page: FaultBasket.aspx
-- =============================================
 
CREATE PROCEDURE [dbo].[FL_GetContractorsByTrade] 

 (	
	@tradeType varchar(100)
  )
AS
	SELECT S_ORGANISATION.ORGID orgid , S_ORGANISATION.NAME name
	FROM (SELECT DISTINCT S_SCOPE.ORGID orgid
		  FROM S_SCOPE
		  WHERE AREAOFWORK=8)AS CONTRACTORS 
		  INNER JOIN S_ORGANISATION ON S_ORGANISATION.ORGID=CONTRACTORS.orgid 
			 --INNER JOIN G_TRADE on G_TRADE.TradeId= S_ORGANISATION.TRADE
			 --WHERE G_TRADE.Description = @tradeType 
	ORDER BY S_ORGANISATION.NAME ASC
GO
