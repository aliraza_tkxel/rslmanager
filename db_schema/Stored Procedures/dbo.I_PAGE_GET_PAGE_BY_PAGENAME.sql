SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[I_PAGE_GET_PAGE_BY_PAGENAME]
@PAGENAME VARCHAR(250),
@PAGEID INT OUTPUT
AS

SET @PAGEID = (SELECT PAGEID FROM I_PAGE WHERE FILENAME = @PAGENAME)

GO
