SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[TO_PARKING_SPACE_SelectParkingSpaceDetail]

/* ===========================================================================
 '   NAME:           TO_PARKING_SPACE_SelectParkingSpaceDetail
 '   DATE CREATED:   20 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select single TO_PARKING_SPACE record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
		TO_ENQUIRY_LOG.Description, 
            	TO_PARKING_SPACE.LookUpValueID AS CATEGORYID
                        
	FROM
		TO_ENQUIRY_LOG INNER JOIN TO_PARKING_SPACE 
		ON TO_ENQUIRY_LOG.EnquiryLogID = TO_PARKING_SPACE.EnquiryLogID	       	       
			
	WHERE
	
		TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID

	     
	     	



GO
