USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_LeavesRequested]    Script Date: 22/12/2017 1:56:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec E_BirthdayPersonalLeavesRequested 1227 
IF OBJECT_ID('dbo.[E_BirthdayPersonalLeavesRequested]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[E_BirthdayPersonalLeavesRequested] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_BirthdayPersonalLeavesRequested] 
	-- Add the parameters for the stored procedure here
	@empId INT,
	@leaveType nvarchar(250),
	@fiscalYear SMALLDATETIME = null
	
AS
BEGIN

DECLARE @BHSTART SMALLDATETIME			
DECLARE @BHEND SMALLDATETIME	

if(@fiscalYear is null)
begin
	SELECT TOP 1 @BHSTART=YStart,@BHEND=YEnd
	  FROM F_FISCALYEARS
	  order by YRange desc
END
ELSE
BEGIN
	SELECT @BHSTART=YStart,@BHEND=YEnd
	FROM F_FISCALYEARS
	WHERE @fiscalYear BETWEEN YStart AND YEnd
 	order by YRange DESC
END


SELECT	A.STARTDATE,a.RETURNDATE,
cast(a.DURATION as decimal (16, 2)) as duration 
,isnull(a.DURATION_TYPE, Case when j.PARTFULLTIME=2 then 'hrs' else 'days' end) as unit 

,J.EMPLOYEEID,JNAL.ITEMNATUREID, en.DESCRIPTION
, A.ABSENCEHISTORYID as AbsenceHistoryId
			FROM 	E__EMPLOYEE E  
					LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
					LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID  AND JNAL.CURRENTITEMSTATUSID = (SELECT ITEMSTATUSID from E_STATUS WHERE DESCRIPTION = 'Pending')
					--AND JNAL.ITEMNATUREID = 1  
					inner JOIN E_NATURE en on JNAL.ITEMNATUREID = en.ITEMNATUREID
					INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = (
									SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
									WHERE JOURNALID = JNAL.JOURNALID AND A.STARTDATE BETWEEN @BHSTART AND @BHEND 
								  ) 
			WHERE 	E.EMPLOYEEID =  @empId --and a.STARTDATE != NULL
			and JNAL.ITEMNATUREID  IN (SELECT ITEMNATUREID from E_NATURE 
									where DESCRIPTION=@leaveType)
									--where DESCRIPTION  in ('Personal Day', 'Birthday Leave') )

END
