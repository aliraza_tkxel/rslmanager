-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 04/01/2015
-- Description:	To get contractors having at lease one Repirs contract, as drop down values for assgin work to contractor.
-- =============================================

IF OBJECT_ID('dbo.FL_GetRepairsContractorDropDownValues') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.FL_GetRepairsContractorDropDownValues AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE dbo.FL_GetRepairsContractorDropDownValues 
	-- Add the parameters for the stored procedure here	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID AND RTRIM(LTRIM(AoW.DESCRIPTION)) LIKE '%Reactive Repair%'
		AND O.ORGACTIVE =1
	ORDER BY NAME ASC
	
	
END
GO
