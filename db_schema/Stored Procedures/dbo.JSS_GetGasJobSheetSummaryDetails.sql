USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetJobSheetSummaryDetails]    Script Date: 04/10/2015 19:06:34 ******/

--[dbo].[JSS_GetGasJobSheetSummaryDetails] 'JSG15108'
IF OBJECT_ID('dbo.[JSS_GetGasJobSheetSummaryDetails]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[JSS_GetGasJobSheetSummaryDetails] AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[JSS_GetGasJobSheetSummaryDetails]
	-- Add the parameters for the stored procedure here
	@JobSheetNumber nvarchar(20)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT DISTINCT  
	 Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER )
	 as JobsheetNumber,
	 AS_JOURNAL.PROPERTYID as PropertyId,
      CONVERT(varchar(20), APPOINTMENTDATE, 103) AS StartDate, 
        CONVERT(varchar(20), APPOINTMENTDATE, 103) AS EndDate,  
      AS_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
      ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, ' ')				AS Operative,
      CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) AS [StartTime],
      AS_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
      CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTENDTIME, 108) AS EndTime,      
      'GAS' AS AppointmentType,
	  ISNULL( AS_APPOINTMENTS.NOTES,'' ) AS CustomerNotes ,
	 AS_APPOINTMENTS.APPOINTMENTSTATUS as Status,
      'Appliance' AS InspectionType  ,
      ISNULL(AS_APPOINTMENTS.IsVoid,-1) AS IsVoid ,      
      'Gas' as ddlAppointmentType,
      ISNULL(NULLIF(ISNULL('Flat No:' + P__PROPERTY.FLATNUMBER + ',', '') +
	  ISNULL(P__PROPERTY.HOUSENUMBER , '') + 
	  ISNULL(', ' + P__PROPERTY.ADDRESS1, '')+
	  ISNULL(', ' + P__PROPERTY.ADDRESS2, '') + ISNULL(' ' + P__PROPERTY.ADDRESS3, '') 
	    , ''), 'N/A') AS [Address],
	   ISNULL(P__PROPERTY.TOWNCITY, '') TownCity, ISNULL(P__PROPERTY.COUNTY, '') COUNTY,
	  ISNULL(P__PROPERTY.POSTCODE,'') POSTCODE,
	   ISNULL(CUST.FIRSTNAME + ' ' + CUST.LASTNAME, 'NA') AS ClientName, 
         ISNULL(C_ADD.TEL,'N/A') AS ClientTel,
         ISNULL(C_ADD.MOBILE, 'N/A') AS ClientMobile,
         ISNULL(C_ADD.EMAIL, 'N/A') AS ClientEmail,
          ISNULL(ps.SCHEMENAME, 'N/A') AS SCHEMENAME ,
		  ISNULL(Convert(nvarchar(20), 
		  CAST (DATEADD(YEAR,1,pl.ISSUEDATE) AS DATE),103),'NA') AS CP12Expiry,
		   ISNULL(CONVERT(nvarchar(20),pl.ISSUEDATE,103),'NA')as CP12Issued
    FROM AS_APPOINTMENTS  
     INNER JOIN E__EMPLOYEE AS E ON AS_APPOINTMENTS.ASSIGNEDTO = E.EMPLOYEEID 
        INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID
		INNER JOIN AS_JournalHeatingMapping JHM ON JHM.JournalId = AS_JOURNAL.JOURNALID
        INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID  
        INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID  
		INNER JOIN PA_HeatingMapping PHM ON JHM.HeatingMappingId = PHM.HeatingMappingId        
		LEFT JOIN P_LGSR pl ON pl.HeatingMappingId = PHM.HeatingMappingId
		LEFT JOIN P_SCHEME ps ON (AS_JOURNAL.SchemeId = ps.SCHEMEID OR ps.SCHEMEID = P__PROPERTY.SchemeId)
        LEFT JOIN ( SELECT CT.CUSTOMERID, T.PROPERTYID 
    FROM C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON ( T.TENANCYID = CT.TENANCYID ) AND 
  ( T.ENDDATE >= CONVERT(date, GETDATE() ) OR 
    T.ENDDATE IS NULL ) AND
  ( CT.ENDDATE >= CONVERT(date, GETDATE() )	OR 
    CT.ENDDATE IS NULL )
   WHERE ( CUSTOMERTENANCYID = ( SELECT max( CUSTOMERTENANCYID ) FROM C_CUSTOMERTENANCY C_CT WHERE C_CT.TENANCYID = CT.TENANCYID ) )
) TENANCY ON ( TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID ) 
         LEFT JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = TENANCY.CUSTOMERID			
         LEFT JOIN C_ADDRESS C_ADD ON ( CUST.CUSTOMERID = C_ADD.CUSTOMERID ) AND ( ISNULL( C_ADD.ISDEFAULT, 0 ) = 1 )
         
        WHERE  
		(AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = 'Alternative Servicing') 
		OR AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = 'Gas')
		OR AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = 'Oil'))
		 AND
      ('JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber)
      
  ---////////////////////////////////////////////////////////////////////
  ---//////////////////////Get Asbestos risk ///////////////////////////
  ---//////////////////////////////////////////////////////////////////    
  Select P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
         P_Asbestos.RISKDESCRIPTION Description,P_PROPERTY_ASBESTOS_RISKLEVEL.DateAdded
    From AS_JOURNAL AJ
		INNER JOIN AS_APPOINTMENTS ON AJ.JOURNALID= AS_APPOINTMENTS.JournalId
         INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID = AJ.PROPERTYID And P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved IS NULL
         INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
         INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID	  
   Where ( 'JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber) 
   and  (P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved is null or CONVERT(DATE,P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved) > convert(date, getdate()) )
AND P_ASBESTOS.other = 0
 
END