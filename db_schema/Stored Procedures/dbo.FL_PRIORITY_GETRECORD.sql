SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_PRIORITY_GETRECORD
/* ===========================================================================
 '   NAME:          FL_PRIORITY_GETRECORD
 '   DATE CREATED:   3 NOV 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a priority against a particular priorityID from FL_FAULT_PRIORITY table which will be used to update 
 '		     a fault values
 '   IN:            @priorityID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@priorityID as Int
AS
	SELECT     PriorityID, PriorityName, ResponseTime, Status, Days
FROM         FL_FAULT_PRIORITY
WHERE     (PRIORITYID= @priorityID)













GO
