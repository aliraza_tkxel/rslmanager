SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[TO_ENQUIRY_LOG_GetEnquiryID]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GetEnquiryID
 '   DATE CREATED:   06 JULY 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get equirylogid against given journalID  
 '   IN:             @journalID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@journalID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.EnquiryLogID
            
            
	FROM
	       TO_ENQUIRY_LOG 
	       
	WHERE
	
	      JournalID = @journalID




GO
