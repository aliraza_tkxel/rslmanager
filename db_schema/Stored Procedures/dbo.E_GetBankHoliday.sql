USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_GetBankHoliday]    Script Date: 15/11/2018 16:13:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.E_GetBankHoliday') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_GetBankHoliday AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_GetBankHoliday]  
  -- Add the parameters for the stored procedure here 
  @empId INT, 
  @fiscalYear SMALLDATETIME = null 
AS 
BEGIN 
DECLARE @BHSTART SMALLDATETIME 
DECLARE @BHEND SMALLDATETIME 
DECLARE @WorkingHoursCount INT
SELECT @BHSTART = startdate, 
       @BHEND = enddate 
FROM   Employee_annual_start_end_date(@empId) 

IF( @fiscalYear IS NOT NULL ) 
  BEGIN 
      SELECT @BHSTART = startdate, 
             @BHEND = enddate 
      FROM   Employee_annual_start_end_date_byyear(@empId, @fiscalYear) 
  END 

SELECT @WorkingHoursCount = Count(*) 
FROM   e_workinghours 
WHERE  employeeid = @empId 

SELECT bh.bhdate      AS STARTDATE, 
       bh.bhdate      AS RETURNDATE, 
       'Bank Holiday' AS Status, 
       'Bank Holiday' AS Nature, 
CASE WHEN (@WORKINGHOURSCOUNT<>0) THEN
       CASE WHEN ( jDetail.holidayindicator = 2 ) THEN 
             CASE WHEN ( ( Datepart(week, bh.bhdate) - Datepart(week, Dateadd(mm, 
                    Datediff(mm, 0, bh.bhdate), 0)) + 1 ) % @WorkingHoursCount ) > 0  THEN 
             (SELECT CASE 
                       WHEN Datepart(dw, bh.bhdate) = 1 THEN mon 
                       WHEN Datepart(dw, bh.bhdate) = 2 THEN tue 
                       WHEN Datepart(dw, bh.bhdate) = 3 THEN wed 
                       WHEN Datepart(dw, bh.bhdate) = 4 THEN thu 
                       WHEN Datepart(dw, bh.bhdate) = 5 THEN fri 
                       WHEN Datepart(dw, bh.bhdate) = 6 THEN sat 
                       WHEN Datepart(dw, bh.bhdate) = 7 THEN sun 
                       ELSE '' 
                     END 
              FROM   (SELECT Row_number () 
                               OVER ( 
                                 ORDER BY wid) AS RowNum, 
                             * 
                      FROM   e_workinghours 
                      WHERE  employeeid = @empId) sub 
              WHERE  rownum = ( Datepart(week, bh.bhdate) - 
                                Datepart(week, Dateadd(mm, 
                     Datediff(mm, 0, bh.bhdate), 0)) + 
                     1 ) % @WorkingHoursCount) 
             ELSE (SELECT CASE 
                            WHEN Datepart(dw, bh.bhdate) = 1 THEN mon 
                            WHEN Datepart(dw, bh.bhdate) = 2 THEN tue 
                            WHEN Datepart(dw, bh.bhdate) = 3 THEN wed 
                            WHEN Datepart(dw, bh.bhdate) = 4 THEN thu 
                            WHEN Datepart(dw, bh.bhdate) = 5 THEN fri 
                            WHEN Datepart(dw, bh.bhdate) = 6 THEN sat 
                            WHEN Datepart(dw, bh.bhdate) = 7 THEN sun 
                            ELSE '' 
                          END 
                   FROM   (SELECT Row_number () 
                                    OVER ( 
                                      ORDER BY wid) AS RowNum, 
                                  * 
                           FROM   e_workinghours 
                           WHERE  employeeid = @empId) sub 
                   WHERE  rownum = @WorkingHoursCount) 
             END 
         ELSE 
            CASE WHEN ( CASE WHEN ( ( Datepart(week, bh.bhdate) - Datepart(week, Dateadd(mm, Datediff(mm, 0, bh.bhdate), 0)) + 1 ) % @WorkingHoursCount ) > 0 THEN 
                      (SELECT CASE 
                                WHEN Datepart(dw, bh.bhdate) = 1 THEN mon 
                                WHEN Datepart(dw, bh.bhdate) = 2 THEN tue 
                                WHEN Datepart(dw, bh.bhdate) = 3 THEN wed 
                                WHEN Datepart(dw, bh.bhdate) = 4 THEN thu 
                                WHEN Datepart(dw, bh.bhdate) = 5 THEN fri 
                                WHEN Datepart(dw, bh.bhdate) = 6 THEN sat 
                                WHEN Datepart(dw, bh.bhdate) = 7 THEN sun 
                                ELSE '' 
                              END 
                       FROM   (SELECT Row_number () 
                                        OVER ( 
                                          ORDER BY wid) AS RowNum, 
                                      * 
                               FROM   e_workinghours 
                               WHERE  employeeid = @empId) sub 
                       WHERE  rownum = ( Datepart(week, bh.bhdate) - 
                                         Datepart(week, Dateadd(mm, 
                              Datediff(mm, 0, bh.bhdate), 0)) + 
                              1 ) % @WorkingHoursCount) 
                      ELSE (SELECT CASE 
                                     WHEN Datepart(dw, bh.bhdate) = 1 THEN mon 
                                     WHEN Datepart(dw, bh.bhdate) = 2 THEN tue 
                                     WHEN Datepart(dw, bh.bhdate) = 3 THEN wed 
                                     WHEN Datepart(dw, bh.bhdate) = 4 THEN thu 
                                     WHEN Datepart(dw, bh.bhdate) = 5 THEN fri 
                                     WHEN Datepart(dw, bh.bhdate) = 6 THEN sat 
                                     WHEN Datepart(dw, bh.bhdate) = 7 THEN sun 
                                     ELSE '' 
                                   END 
                            FROM   (SELECT Row_number () 
                                             OVER ( 
                                               ORDER BY wid) AS RowNum, 
                                           * 
                                    FROM   e_workinghours 
                                    WHERE  employeeid = @empId) sub 
                            WHERE  rownum = @WorkingHoursCount) 
                     END ) > 0 THEN '1' 
             ELSE '0' 
			 END 
       END
ELSE
'0'
END AS DAYS_ABS,             
       'F'            AS HolType, 
       0              AS ABSENCEHISTORYID, 
       bh.bhdate      AS LastActionDate, 
       CASE 
         WHEN ( jDetail.holidayindicator = 2 ) THEN 'Hours' 
         ELSE 'Days' 
       END            AS DurationType 
FROM   g_bankholidays bh 
       CROSS apply e__employee emp 
       INNER JOIN e_jobdetails jDetail 
               ON emp.employeeid = jDetail.employeeid 
WHERE  jDetail.employeeid = @empId 
       AND bh.bhdate >= @BHSTART 
       AND bh.bhdate <= @BHEND 
       AND ( bh.bha = 1 
              OR bh.meridianeast = 1 ) 
ORDER  BY bh.bhdate DESC 
END
