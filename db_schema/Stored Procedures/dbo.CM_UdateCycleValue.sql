USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePhotograph]    Script Date: 12/01/2016 06:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[CM_UdateCycleValue]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CM_UdateCycleValue] AS SET NOCOUNT ON;') 
GO 
ALTER PROCEDURE [dbo].CM_UdateCycleValue (    
@cycleValue money,    
@serviceItemId int  
)    
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  

	Update CM_ServiceItems set CycleValue=@cycleValue where ServiceItemId=@serviceItemId
END  

