SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[AM_spGetTenantBalance]
	@tenantId	int
AS
BEGIN
	
	Select dbo.FN_GET_TENANT_BALANCE(@tenantId)

END





GO
