SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Rogers
-- Create date: 9th October 2014
-- Description:	Stored Procedure to Show Holiday Absences 
-- =============================================
CREATE PROCEDURE [diagnostics].[SR_getEmployeeHolidayDetail] 
	-- Add the parameters for the stored procedure here
	@employeeId INT,
	@financeDate smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT FIRSTNAME, LASTNAME FROM dbo.E__EMPLOYEE WHERE EMPLOYEEID = @employeeId

DECLARE @startdate AS SMALLDATETIME
SET @startdate = (SELECT  CAST(
		CAST(DAY(STARTDATE) AS VARCHAR) + '/'
        + CAST(MONTH(STARTDATE) AS VARCHAR) + '/' 
        + CAST(YEAR(GETDATE()) AS VARCHAR) 
        AS smalldatetime) 
        AS [sdate]
FROM    e_jobdetails
WHERE   EMPLOYEEID = @employeeId)
IF (@startdate > GETDATE()) SET @startdate = DATEADD(yyyy,-1,@startdate)
SELECT @startdate; 

DECLARE @abs TABLE (
	journalid INT,
	absenceId INT
)

WITH cte_journal (journalid, absenceId) 
AS
(
		SELECT  JOURNALID ,
        MAX(ABSENCEHISTORYID) AS absenceID
		FROM    dbo.E_ABSENCE
		WHERE   JOURNALID IN ( SELECT   JOURNALID
                       FROM     dbo.E_JOURNAL
                       WHERE    EMPLOYEEID = @employeeId
                                AND ITEMNATUREID = 2
                                AND ITEMID = 1 )
GROUP BY JOURNALID
)
INSERT INTO @abs
SELECT journalid, absenceId
FROM cte_journal

SELECT  SUM(DURATION) AS TotalDuration
FROM    dbo.E_ABSENCE a
        INNER JOIN @abs cj ON ( a.ABSENCEHISTORYID = cj.absenceId )
WHERE   a.ITEMSTATUSID = 5
        AND ( a.STARTDATE >= @startdate
              OR ( a.STARTDATE < @startdate
                   AND a.RETURNDATE >= @startdate
                 )
            )
        AND a.STARTDATE < @financeDate

SELECT  CASE WHEN startdate < @startdate THEN '*' 
		WHEN (startdate < @startdate AND returndate > @startdate) THEN '*'
		WHEN (startdate < @financeDate AND returndate > @financeDate) THEN '*'
		ELSE '' END AS Issue,  *
FROM    dbo.E_ABSENCE a
        INNER JOIN @abs cj ON ( a.ABSENCEHISTORYID = cj.absenceId )
WHERE   a.ITEMSTATUSID = 5
        AND ( a.STARTDATE >= @startdate
              OR ( a.STARTDATE < @startdate
                   AND a.RETURNDATE >= @startdate
                 )
            )
        AND a.STARTDATE < @financeDate
        ORDER BY STARTDATE ASC

SELECT  *
FROM    dbo.E_JOURNAL
WHERE   EMPLOYEEID = @employeeid
        AND CURRENTITEMSTATUSID = 5
        AND ITEMNATUREID = 2
        AND ITEMID = 1 
        ORDER BY JOURNALID DESC
        
END
GO
