SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC dbo.FL_TEMP_FAULT_BASKET

/* ===========================================================================
 '   NAME:           FL_TEMP_FAULT_BASKET
 '   DATE CREATED:   22nd Oct 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the faults from temp fault table
 '   IN:             @CustomerId
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(	
	@CustomerID int	    								
)
	
		
AS
	    	        
    -- Begin building SELECT clause
    SELECT FL_TEMP_FAULT.TempFaultID,  FL_TEMP_FAULT.Quantity, FL_FAULT.Description, FL_ELEMENT.ElementName,FL_AREA.AreaName, FL_LOCATION.LocationName,
    (CONVERT(VARCHAR, FL_FAULT_PRIORITY.ResponseTime) + ' ' + CASE FL_FAULT_PRIORITY.Days WHEN 1 THEN  'Day(s)'  WHEN 0 THEN 'Hour(s)' END) AS FResponseTime,FL_FAULT.PreInspection,FL_TEMP_FAULT.ORGID,
    CASE FL_TEMP_FAULT.Recharge WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' ELSE 'No' END AS Recharge
	FROM FL_TEMP_FAULT 
	INNER JOIN FL_FAULT ON FL_TEMP_FAULT.FaultID = FL_FAULT.FaultID
	INNER JOIN FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID 
	INNER JOIN FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID
	INNER JOIN FL_LOCATION ON FL_LOCATION.LocationID = FL_AREA.LocationID
	INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
    WHERE FL_TEMP_FAULT.CustomerID = @CustomerID
    Order By TempFaultID  DESC  
                                     
           
RETURN	


GO
