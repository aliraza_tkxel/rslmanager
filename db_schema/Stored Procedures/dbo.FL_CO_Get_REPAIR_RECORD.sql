SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_CO_Get_REPAIR_RECORD 
/* ===========================================================================
 '   NAME:           FL_CO_Get_REPAIR_RECORD
 '   DATE CREATED:   02 Jan., 2009
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    RSL Work Completion
 '   PURPOSE:        To get repair record from FL_FAULT_REPAIR_LIST
 
 '   IN:             @repairDetail

 '
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
	@repairDetail  NVARCHAR(1000)
  )
AS
		
SELECT FaultRepairListID,NetCost ,VatRateID,Vat,Gross 
FROM FL_FAULT_REPAIR_LIST
WHERE Description =@repairDetail












GO
