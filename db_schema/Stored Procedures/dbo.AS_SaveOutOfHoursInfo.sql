SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,12/05/2014>
-- Description:	<Description, Save Out Of Hours Information>
--Last modified Date:12/05/2014
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveOutOfHoursInfo]
	-- Add the parameters for the stored procedure here
	@employeeId int
	,@editedBy int
	,@newOutOfHoursDt as AS_OutOfHoursInfo readonly
	,@deletedOutOfHoursDt as AS_DeletedOutOfHoursIds readonly
	,@isSaved int = 0 out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	
	
	BEGIN TRANSACTION;
	BEGIN TRY
	
	--=====================================
	-- DELETE OLD ENTRIES
	--=====================================
	
	create table #tmp_DeletedOutOfHours
	(
		OutOfHoursId [int] NOT NULL
	)
	
	INSERT INTO #tmp_DeletedOutOfHours
    SELECT * FROM @deletedOutOfHoursDt
	
	
	DELETE
	FROM	E_OUT_OF_HOURS 
	WHERE	E_OUT_OF_HOURS.OUTOFHOURSID IN (SELECT #tmp_DeletedOutOfHours.OutOfHoursId
											FROM #tmp_DeletedOutOfHours)
	
	DROP TABLE #tmp_DeletedOutOfHours
	
	
	--=====================================
	-- ADD NEW ENTRIES
	--=====================================
	
	create table #tmp_OutOfHoursInfo
	(
	[OutOfHoursId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[StartTime] [nvarchar](10) NULL,
	[EndTime] [nvarchar](10) NULL,
	[GeneralStartDate] [nvarchar](100) NULL,
	[GeneralEndDate] [nvarchar](100) NULL
	)
	
	INSERT INTO #tmp_OutOfHoursInfo
    SELECT * FROM @newOutOfHoursDt
    
    INSERT INTO [E_OUT_OF_HOURS]
    ([EMPLOYEEID],[TYPEID],[STARTDATE],[ENDDATE],[STARTTIME],[ENDTIME],[UPDATEDDATE],[UPDATEDBY])
	SELECT	@employeeId
			, #tmp_OutOfHoursInfo.TypeId 
			, #tmp_OutOfHoursInfo.StartDate 
			, #tmp_OutOfHoursInfo.EndDate 
			, #tmp_OutOfHoursInfo.StartTime 
			, #tmp_OutOfHoursInfo.EndTime 
			, GETDATE()
			,@editedBy
	FROM #tmp_OutOfHoursInfo
    
    DROP TABLE #tmp_OutOfHoursInfo
    
        
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isSaved = 0		
		END
		
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		 @ErrorSeverity, -- Severity.
		 @ErrorState -- State.
		 );
	END CATCH;

	IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
	SET @isSaved = 1
	END

END
GO
