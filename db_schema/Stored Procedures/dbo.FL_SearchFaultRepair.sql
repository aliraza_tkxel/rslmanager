USE [RSLBHALive ]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 21/03/2016
-- Description:	Search Repair List
-- Exec FL_SearchFaultRepair ''
-- =============================================

IF OBJECT_ID('dbo.FL_SearchFaultRepair') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_SearchFaultRepair AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FL_SearchFaultRepair] 
( 	
	@searchText nvarchar(1000) = ''
)
AS
BEGIN

SET NOCOUNT ON;

	SELECT	FaultRepairListID as id
			, [Description] as value
	FROM	FL_FAULT_REPAIR_LIST  
	WHERE	RepairActive = 1 AND [Description] LIKE +'%'+@searchText+'%'
	ORDER BY FL_FAULT_REPAIR_LIST.Description ASC
					
END