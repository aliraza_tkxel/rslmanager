
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adeel Ahmed>
-- ALTER date: <30/04/2010>
-- Description:	<Counting First Detections>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_CountFirstDetections]
	-- Add the parameters for the stored procedure here
	@caseOfficerId INT,
	@regionId INT,
	@suburbId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
	if(@caseOfficerId > 0  and @regionId <= 0 and @suburbId <= 0)
	BEGIN
		SELECT AM_Case.CaseId 
		from AM_Case INNER JOIN
			AM_Status on AM_Case.StatusId = AM_Status.StatusId
		where AM_Case.CaseOfficer = @caseOfficerId and AM_Status.Title = 'First Detection' and AM_CASE.IsActive=1

	END
	else if(@caseOfficerId > 0  and @regionId > 0 and @suburbId <= 0)
	BEGIN
	
		SELECT AM_Case.CaseId 
		FROM AM_Case 
			INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
			INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
			INNER JOIN PDR_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID 
			LEFT JOIN  P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.schemeid
			INNER JOIN AM_Status on AM_Case.StatusId = AM_Status.StatusId
		WHERE AM_Case.CaseOfficer = @caseOfficerId and PDR_DEVELOPMENT.PATCHID = @regionId and AM_Status.Title = 'First Detection' and AM_CASE.IsActive=1
	END
	else if(@caseOfficerId > 0  and @regionId > 0 and @suburbId > 0)
	BEGIN
		SELECT AM_Case.CaseId  
		from AM_Case 
			INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID 
			INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID 
			INNER JOIN PDR_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID 
			LEFT JOIN  P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.schemeid
			INNER JOIN AM_Status on AM_Case.StatusId = AM_Status.StatusId
		WHERE AM_Case.CaseOfficer = @caseOfficerId and PDR_DEVELOPMENT.PATCHID = @regionId and P__PROPERTY.SCHEMEID = @suburbId and AM_Status.Title = 'First Detection' and AM_CASE.IsActive=1


	END
END





GO
