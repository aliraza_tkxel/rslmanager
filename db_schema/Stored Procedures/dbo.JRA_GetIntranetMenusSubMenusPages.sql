SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_GetIntranetMenusSubMenusPages @teamJobRoleId = 616
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Get intranet menus>
-- Web Page: JobRoles.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetIntranetMenusSubMenusPages](
@teamJobRoleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    --============================================================
    --					MAIN MENUS
    --============================================================
    
    SELECT	I_MENU.MenuID AS MenuId, I_MENU.MenuTitle AS MenuName
    FROM	I_MENU 
    WHERE	I_MENU.ACTIVE = 1 and ParentID IS NULL
    
     --============================================================
    --					SUB MENUS
    --============================================================
    
    SELECT	I_MENU.MenuID AS SubMenuId, I_MENU.MenuTitle as SubMenuName , I_MENU.ParentID as ParentId
    FROM	I_MENU
    WHERE	I_MENU.Active = 1 AND  ParentID IS NOT NULL
    
     --============================================================
    --					MENU PAGES
    --============================================================
    
    SELECT	I_PAGE.PageID AS PageId,I_PAGE.Title as PageName,I_PAGE.MenuID AS MenuId
    FROM	I_PAGE 
    WHERE	I_PAGE.Active =1
    
	
END
GO
