USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_DeclarationOfInterestCount]    Script Date: 02/03/2018 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_DeclarationOfInterestCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_DeclarationOfInterestCount AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Saud Ahmed
-- Create date:      02/03/2018
-- Description:      Calculate alert count of Declaration Of Interest on whiteboard  
-- exec  E_DeclarationOfInterestCount 423
-- =============================================

ALTER PROCEDURE  [dbo].[E_DeclarationOfInterestCount]
	@LineMgr INT 
	
AS
BEGIN
	
	SET NOCOUNT ON;
   
SELECT PendingInterest as LeavesCount From (
	SELECT count(*) AS PendingInterest
	from E_DOI doi
        join E__EMPLOYEE emp on doi.EmployeeId = emp.EMPLOYEEID
        join E_JOBDETAILS jd on emp.EMPLOYEEID = jd.EMPLOYEEID
        join E__EMPLOYEE lm on jd.LINEMANAGER = lm.EMPLOYEEID
        left join E_DOI_Status s on doi.Status = s.DOIStatusId
		CROSS APPLY (SELECT TOP 1 YRange FROM F_FISCALYEARS ff ORDER BY ff.YRange DESC) AS yRange
	WHERE doi.FiscalYear = yRange.YRange 
		AND s.Description = 'Submitted' AND lm.EMPLOYEEID = @LineMgr
)  totalResult




END
