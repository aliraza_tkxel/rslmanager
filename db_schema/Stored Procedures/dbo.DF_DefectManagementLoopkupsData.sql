
/* =================================================================================    
    Page Description: DefectAvailableAppointments.aspx
 
    Author: Aamir Waheed
    Creation Date:  26/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         02/09/2015     Aamir Waheed       Created:DF_GetDefectManagementLoopkupsData, to get lookup values for defect management user control
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[DF_GetDefectManagementLoopkupsData]
						@PropertyId = 'BHA0000118'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

IF OBJECT_ID('dbo.DF_GetDefectManagementLoopkupsData') IS NULL
 EXEC('CREATE PROCEDURE dbo.DF_GetDefectManagementLoopkupsData AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[DF_GetDefectManagementLoopkupsData]
	@PropertyId NVARCHAR(20)
AS
BEGIN

-- 1- Get Defect Categories
SELECT
	CategoryId
	,[Description]	AS CategoryName
FROM
	P_DEFECTS_CATEGORY

-- 2- Get Property Appliances by PropertyId
SELECT
	A.PROPERTYAPPLIANCEID
	,AT.APPLIANCETYPE	AS APPLIANCE
FROM
	GS_PROPERTY_APPLIANCE A
	INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
	INNER JOIN PA_ITEM ON A.ItemId = PA_ITEM.ItemID
WHERE
	A.PROPERTYID = @PropertyId 
	AND A.ISACTIVE = 1
	AND ItemName = 'Appliances'

-- 3- Get Employees List (for Parts order by)
SELECT
	E.EMPLOYEEID
	,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')	AS EmployeeName
FROM
	E__EMPLOYEE E
		INNER JOIN E_JOBDETAILS JD ON E.EMPLOYEEID = JD.EMPLOYEEID
			AND
			JD.ACTIVE = 1

-- 4- Get Defect Priorities List.
SELECT
	P.PriorityID
	,P.PriorityName
FROM
	P_DEFECTS_PRIORITY P

-- 5- Get Trades List
SELECT
	T.TradeId
	,T.[Description]	AS Trade
FROM
	G_TRADE T

END