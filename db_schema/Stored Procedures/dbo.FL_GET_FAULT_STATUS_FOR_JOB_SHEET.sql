SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE PROCEDURE dbo.FL_GET_FAULT_STATUS_FOR_JOB_SHEET
	/* ===========================================================================
 '   NAME:           FL_GET_FAULT_STATUS_FOR_JOB_SHEET
 '   DATE CREATED:   31st Dec 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get fault status from FL_FAULT_STATUS table which will be shown as lookup value in contractor portal pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT FaultStatusID AS id,DESCRIPTION AS val
	FROM FL_FAULT_STATUS where FaultStatusID >=2










GO
