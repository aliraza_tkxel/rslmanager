SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[F_PAYTHRU_GET_TOKEN_RESPONSE_LOG_SelectByTransactionId]
    (
      @TransactionId UNIQUEIDENTIFIER
    )
AS 
    SELECT  [GetTokenResponseId] ,
            [TransactionId] ,
            [Payload] ,
            [RedemptionUrl] ,
            [DateReceived] ,
            [DateCreated] ,
            [CreatedBy] ,
            [Version]
    FROM    [dbo].[F_PAYTHRU_GET_TOKEN_RESPONSE_LOG]
    WHERE   TransactionId = @TransactionId

GO
