
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetPropertyAppliances @propertyId = A010060001
-- Author:		<Salman Nazir>
-- Create date: <06/11/2012>
-- Description:	<Get Appliances against Property Id >
-- Web Page: PropertyRecord.aspx => Add Defect PopUp
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPropertyAppliances](
@propertyId Varchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct( GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID),APPLIANCETYPE
	FROM GS_PROPERTY_APPLIANCE INNER JOIN
	GS_APPLIANCE_TYPE on GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_APPLIANCE_TYPE.APPLIANCETYPEID
	Where PROPERTYID = @propertyId
END
GO
