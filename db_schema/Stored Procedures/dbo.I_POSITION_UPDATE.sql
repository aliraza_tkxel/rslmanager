SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_POSITION_UPDATE]
(
	@WebSiteID int,
	@Position varchar(50),
	@Original_PositionID int,
	@PositionID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_POSITION] SET [WebSiteID] = @WebSiteID, [Position] = @Position WHERE (([PositionID] = @Original_PositionID));
	
SELECT PositionID, WebSiteID, Position FROM I_POSITION WHERE (PositionID = @PositionID)
GO
