SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_MeterLocations 
-- Author:	<Salman Nazir>
-- Create date: <30/10/2012>
-- Description:	<Get all meter locations for deropdown on Attribute Tab >
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_MeterLocations]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM PA_PARAMETER_VALUE 
	WHERE ParameterID IN 
						(SELECT ParameterID 
						 FROM PA_PARAMETER 
						 WHERE ParameterName='Meter Location')
END
GO
