SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO













CREATE PROCEDURE dbo.FL_GETSCHEMELOOKUP
/* ===========================================================================
 '   NAME:          FL_GETSCHEMELOOKUP
 '   DATE CREATED:  23 Dec. 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get scheme G_SCHEME table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT DEVELOPMENTID AS id,SCHEMENAME AS val
	FROM P_DEVELOPMENT
	ORDER BY SCHEMENAME ASC










GO
