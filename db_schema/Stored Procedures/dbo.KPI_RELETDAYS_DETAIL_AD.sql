SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- SEE GROUP SP FOR NOTES

--EXEC KPI_RELETDAYS_DETAIL_AD '20080401','20090331',1,1,1

CREATE            PROCEDURE [dbo].[KPI_RELETDAYS_DETAIL_AD] (
	@PS DATETIME, 
	@PE DATETIME,
	@DMONTH INT = -1,
	@ASSETTYPE1 INT, @ASSETTYPE2 INT
	 )
AS
--DECLARE @PS DATETIME
--DECLARE @PE DATETIME
--SET @PS = '1 APR 2004'
--SET @PE = '31 MAR 2005'
DECLARE @PROPERTYID NVARCHAR(20)
DECLARE @THECOUNT INT
DECLARE @STARTDATE DATETIME
DECLARE @IN_ENDDATE DATETIME
DECLARE @CURRENTEND DATETIME
DECLARE @PROPERTY_CURSOR_ROWCOUNT INT
DECLARE @LOOPCNT INT
DECLARE @TOTALDAYS FLOAT
DECLARE @TOTALVACS FLOAT
DECLARE @PREV_PROP NVARCHAR(20)
DECLARE @ADDRESS NVARCHAR(200)
DECLARE @IN_ADDRESS NVARCHAR(200)
DECLARE @IN_PROP NVARCHAR(200)
DECLARE @TENANCYID INT
DECLARE @DAYS INT
DECLARE @JOURNALID INT
DECLARE @LAST_END_DATE SMALLDATETIME

SET NOCOUNT ON
SET @LOOPCNT = 0
SET @TOTALVACS = 0
SET @TOTALDAYS = 0

CREATE TABLE #TEMP (
	THEID INT IDENTITY (1,1) NOT NULL,
	DESCRIPTION VARCHAR(1000) NULL,
	TERM SMALLDATETIME NULL,
	RELET SMALLDATETIME NULL,
	DURATION INT NULL,
	PROPERTYID VARCHAR(25),
	JOURNALID INT
) ON [PRIMARY]

DECLARE STARTER CURSOR 
	FAST_FORWARD
	FOR SELECT T.STARTDATE, HOUSENUMBER + ' ' +  P.ADDRESS1 AS ADDRESS, P.PROPERTYID, TENANCYID
		FROM C_TENANCY T INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID 
		WHERE T.STARTDATE >= @PS AND T.STARTDATE <= @PE AND 
			(ASSETTYPE = @ASSETTYPE1 OR ASSETTYPE = @ASSETTYPE2) AND T.MUTUALEXCHANGESTART <> 1
			AND P.PROPERTYID='A830180000'
		ORDER BY T.STARTDATE
	OPEN STARTER
	FETCH NEXT FROM STARTER
	   INTO @STARTDATE, @ADDRESS, @PROPERTYID, @TENANCYID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		--
		SELECT TOP 1 @IN_PROP = T.PROPERTYID, @IN_ADDRESS = HOUSENUMBER + ' ' +  P.ADDRESS1, 
			@IN_ENDDATE = T.ENDDATE 
		FROM C_TENANCY T INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID
		WHERE T.PROPERTYID = @PROPERTYID AND T.MUTUALEXCHANGEEND <> 1 AND
			T.ENDDATE <= @PE AND TENANCYID < @TENANCYID 
		ORDER BY T.ENDDATE DESC
	
		IF @IN_PROP IS NOT NULL
		BEGIN

			SET @LAST_END_DATE = @IN_ENDDATE
			
			SELECT TOP 1
				@JOURNALID = CASE 
				WHEN @IN_ENDDATE > RP.LASTACTIONDATE THEN NULL
				ELSE J.JOURNALID END,
			
				@LAST_END_DATE = CASE 
				WHEN @IN_ENDDATE > RP.LASTACTIONDATE THEN @IN_ENDDATE
				ELSE RP.LASTACTIONDATE END 
			FROM 	C_JOURNAL J
				INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR 
					WHERE ITEMACTIONID IN (9) GROUP BY JOURNALID) R ON R.JOURNALID = J.JOURNALID
				INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID 
				INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = RP.ITEMDETAILID 
								AND KPI11_ACTIVE = 1 -- THIS WAS ADDED BY JIM (NOTE A)
			WHERE  --(MAINTENANCECODE LIKE 'M%' OR ITEMNATUREID = 21) AND  -- THIS WAS COMMENTED OUT BY JIM (NOTE A)
				 PROPERTYID = @IN_PROP --AND RP.LASTACTIONDATE <= @STARTDATE
				ORDER BY RP.LASTACTIONDATE DESC
			PRINT @IN_ENDDATE
			PRINT @LAST_END_DATE
			PRINT @STARTDATE
			print @JOURNALID
			--SELECT @LAST_END_DATE = RETURNDATE, @JOURNALID = JOURNALID FROM CALC_ENDDATE(@IN_PROP, @IN_ENDDATE , @STARTDATE)
			SET @DAYS = DATEDIFF(D, @LAST_END_DATE, @STARTDATE) 
			--SET @DAYS = DATEDIFF(D, @IN_ENDDATE, @STARTDATE) - DBO.CALC_UNAVAILABLE_DAYS (@IN_PROP, @IN_ENDDATE , @STARTDATE)
			INSERT INTO #TEMP VALUES (@IN_ADDRESS, @IN_ENDDATE, @STARTDATE, @DAYS, @IN_PROP, @JOURNALID)
		END

		SET @IN_PROP = NULL
		SET @IN_ADDRESS = NULL
		SET @IN_ENDDATE = NULL
		SET @LAST_END_DATE = NULL
		SET @JOURNALID = NULL

	-- GET NEXT
	FETCH NEXT FROM STARTER
	INTO @STARTDATE, @ADDRESS, @PROPERTYID, @TENANCYID
	END
CLOSE STARTER
DEALLOCATE STARTER

IF @DMONTH = -1
	SELECT * FROM #TEMP ORDER BY RELET
ELSE
	SELECT * FROM #TEMP WHERE  MONTH(RELET) = @DMONTH
DROP TABLE #TEMP


GO
