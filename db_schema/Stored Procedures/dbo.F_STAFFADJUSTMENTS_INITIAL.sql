SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--       EXEC F_STAFFADJUSTMENTS_INITIAL

CREATE    PROCEDURE [dbo].[F_STAFFADJUSTMENTS_INITIAL]
AS

DECLARE @TENANCYID INT
DECLARE @AMOUNT	MONEY

DECLARE STAFFEDHOUSING CURSOR FAST_FORWARD FOR 
SELECT 	T.TENANCYID, SUM(AMOUNT)
FROM 	P__PROPERTY P 
	INNER JOIN C_TENANCY T ON P.PROPERTYID = T.PROPERTYID
	INNER JOIN F_RENTJOURNAL J ON J.TENANCYID = T.TENANCYID
WHERE 	ASSETTYPE = 3 AND ((T.ENDDATE < '01 APR 2005' AND T.ENDDATE > '31 MAR 2004')  OR T.ENDDATE IS NULL)
	AND J.TRANSACTIONDATE > '31 MAR 2004' AND J.TRANSACTIONDATE < '1 APR 2005'
	AND J.ITEMTYPE = 1 AND PAYMENTTYPE IS NULL
GROUP	BY T.TENANCYID
OPEN STAFFEDHOUSING
FETCH NEXT FROM STAFFEDHOUSING
	INTO @TENANCYID, @AMOUNT
WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT 'RUNING ROUTINE FOR ' + CAST (@TENANCYID AS VARCHAR)
	INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID )
	 VALUES (@TENANCYID, '1 apr 2005', 1, 39, -@AMOUNT, 2)

	FETCH NEXT FROM STAFFEDHOUSING
	INTO @TENANCYID, @AMOUNT
END
CLOSE STAFFEDHOUSING
DEALLOCATE STAFFEDHOUSING



GO
