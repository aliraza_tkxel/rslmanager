SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_ELEMENT_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_ELEMENT_GETLOOKUP
 '   DATE CREATED:   16 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Location FL_AREA table which will be shown as lookup value in presentation pages
 '   IN:            @AreaID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@AreaID as Int
AS
	SELECT ElementID AS id,ElementName AS val
	FROM FL_ELEMENT
	WHERE AreaID=@AreaID
	ORDER BY ElementName ASC













GO
