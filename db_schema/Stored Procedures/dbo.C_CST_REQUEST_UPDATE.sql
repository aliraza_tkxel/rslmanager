SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE     PROC [dbo].[C_CST_REQUEST_UPDATE](
/* ===========================================================================
 '   NAME:           C_CST_REQUEST_UPDATE
 '   DATE CREATED:  28 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
Creates a new record by updating record in C_ABANDONED and 
 '		  creates entry in   C_JOURNAL tables		 
 '   IN:             @HISTORYID
 '   IN:             @CURRENTITEMSTATUSID
 '   IN:             @LASTACTIONUSER
 '   IN:             @NOTES
 '   IN:             @REQUESTNATUREID
 '
 '   OUT:            Nothing    
 '   RETURN:         journalid OR -1
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@HISTORYID INT , 
@CURRENTITEMSTATUSID INT,
@LASTACTIONUSER INT ,
@NOTES VARCHAR(4000), 
@REQUESTNATUREID INT
)
AS
BEGIN 

DECLARE @JOURNALID INT
DECLARE @CSTREQUESTEHISTORYID INT

SET NOCOUNT ON
	
	SELECT 
		@JOURNALID = G.JOURNALID
	FROM C_CST_REQUEST G
	WHERE G.CSTREQUESTEHISTORYID  = @HISTORYID
	
	BEGIN TRAN
	
	UPDATE C_JOURNAL SET 	CURRENTITEMSTATUSID = @CURRENTITEMSTATUSID 
	WHERE JOURNALID = @JOURNALID

	-- If updation fails, goto HANDLE_ERROR block
	--IF @@ERROR <> 0 GOTO HANDLE_ERROR
	
	INSERT INTO C_CST_REQUEST (JOURNALID,RequestNatureID , Description, LASTACTIONDATE,LASTACTIONUSER,ITEMSTATUSID )
	VALUES (@JOURNALID, @REQUESTNATUREID, @NOTES,  GETDATE(), @LastActionUser,@CURRENTITEMSTATUSID)

	SELECT 	@CSTREQUESTEHISTORYID = SCOPE_IDENTITY()

	-- If updation fails, goto HANDLE_ERROR block
	--IF @@ERROR <> 0 GOTO HANDLE_ERROR


	SELECT @JOURNALID AS JOURNALID , @CSTREQUESTEHISTORYID AS SERVICEID
	COMMIT TRAN
	
	--RETURN @JOURNALID


	-- Control comes here if error occured
--	HANDLE_ERROR:
--
--	  ROLLBACK TRAN
--
--	  RETURN -1

END





GO
