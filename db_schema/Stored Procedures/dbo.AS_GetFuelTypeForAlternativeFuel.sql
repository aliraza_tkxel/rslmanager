USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetFuelTypeForAlternativeFuel]    Script Date: 07/05/2018 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetFuelType
-- Author:	<Noor Muhammad>
-- Create date: <02/11/2012>
-- Description:	<Get all fuel types from AS_GetFuelType table>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================

IF OBJECT_ID('dbo.[AS_GetFuelTypeForAlternativeFuel]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetFuelTypeForAlternativeFuel] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetFuelTypeForAlternativeFuel]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT FUELTYPEID as id, FUELTYPE as title
	--FROM P_FUELTYPE 
	--order by FUELTYPE asc
	
	SELECT ValueID as id,ValueDetail as title  FROM  PA_PARAMETER_VALUE
	INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.ParameterID = PA_PARAMETER.ParameterID
	INNER JOIN PA_ITEM_PARAMETER ON PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
	INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
	WHERE ParameterName = 'Heating Fuel' AND ItemName = 'Heating' And PA_PARAMETER_VALUE.IsActive = 1 And PA_PARAMETER_VALUE.IsAlterNativeHeating = 1
	Order By Sorder ASC
	
	
END
