USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetHealthAndSafetyTabInformation]    Script Date: 12/21/2016 12:12:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.[AS_GetHealthAndSafetyTabInformation]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetHealthAndSafetyTabInformation] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[AS_GetHealthAndSafetyTabInformation]  
 @propertyId varchar(50),
 @pageSize int = 30,  
@pageNumber int = 1, 
 @totalCount int = 0 output  
   
AS  
BEGIN  
  
 DECLARE @SelectClause varchar(2000),  
        @fromClause   varchar(3000),  
        @whereClause  varchar(3000),           
        @orderClause  varchar(200),   
        @mainSelectQuery varchar(MAX),          
        @rowNumberQuery varchar(MAX),  
        @finalQuery varchar(MAX),  
        @sortColumn varchar(100) = 'ASBESTOSID',  
		@sortOrder varchar (5) = 'DESC',  
        -- used to add in conditions in WhereClause based on search criteria provided  
        @searchCriteria varchar(1500) ,
        @offset int,  
		@limit int  
  --Paging Formula  
  SET @offset = 1 + (@pageNumber - 1) * @pageSize  
  SET @limit = (@offset + @pageSize) - 1     
  
  --========================================================================================           
  -- Begin building SELECT clause  
  -- Insert statements for procedure here  
  
  SET @selectClause = 'SELECT   
					 PR.PROPASBLEVELID AS ASBESTOSID,  
                       PL.ASBRISKLEVELID AS RiskLevelId,  
                       PL.ASBRISKLEVELDESCRIPTION AS RiskLevel,  
                       P.RISKDESCRIPTION AS ElementDescription,  
					   isNull(Convert(nvarchar(10),PR.DateAdded,103) ,''-'')  as DateAdded,
					   isNull(Convert(nvarchar(10),PR.DateRemoved,103) , ''-'') as DateRemoved,
                       PRI.ASBRISKID AS Risk,ISNULL(PAL.Description,''-'') as Level   
                       ,ISNULL(LEFT(E.FIRSTNAME, 1) + '' '' + LEFT(E.LASTNAME,1) ,''N/A'') AS UploadedBy 
					   ,ISNULL(E.FIRSTNAME + '' '' + E.LASTNAME	,''N/A'') AS UploadedByEmp 
					   '  

  
  -- End building SELECT clause  
  --========================================================================================          
  
  
  --========================================================================================      
  -- Begin building FROM clause  
  SET @fromClause = CHAR(10) + 'from P_ASBRISK PRI, P_PROPERTY_ASBESTOS_RISKLEVEL PR  
                                LEFT JOIN P_PROPERTY_ASBESTOS_RISK PAR ON PR.PROPASBLEVELID= PAR.PROPASBLEVELID   
                                INNER JOIN P_ASBRISKLEVEL PL ON PR.ASBRISKLEVELID = PL.ASBRISKLEVELID  
                                INNER JOIN P_ASBESTOS P ON PR.ASBESTOSID = P.ASBESTOSID   
                                LEFT JOIN dbo.E__EMPLOYEE E ON PR.UserId = E.EMPLOYEEID
                                LEFT JOIN P_AsbestosRiskLevel PAL ON PR.RiskLevelId = PAL.AsbestosLevelId
                                '  

  -- End building From clause  
  --========================================================================================  
    
  --========================================================================================  
  -- Begin building SearchCriteria clause  
  -- These conditions will be added into where clause based on search criteria provided  
  
  SET @searchCriteria = ' 1=1 '  
  SET @searchCriteria += 'AND PAR.ASBRISKID = PRI.ASBRISKID  AND P.Other = 0'  
  SET @searchCriteria += ' And PropertyId =''' + @propertyId +''''   
  
  -- End building SearchCriteria clause     
  --========================================================================================  
  
  --========================================================================================  
  -- Begin building WHERE clause  
  
  -- This Where clause contains subquery to exclude already displayed records       
   
  SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria  
  
  -- End building WHERE clause  
  --========================================================================================  
  
  --========================================================================================      
  -- Begin building OrderBy clause    
  
  -- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias  
  
  SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder  
  
  -- End building OrderBy clause  
  --========================================================================================  
  
  --========================================================================================  
  -- Begin building the main select Query  
  
  SET @mainSelectQuery = @selectClause + @fromClause + @whereClause --+ @orderClause  
   
  -- End building the main select Query  
  --========================================================================================                                     
  
  --========================================================================================  
  -- Begin building the row number query  
  
  SET @rowNumberQuery = '  SELECT *, row_number() over (order by ' + CHAR(10) + @sortColumn + CHAR(10) + @sortOrder + CHAR(10) + ') as row   
          FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'  
  
  -- End building the row number query  
  --========================================================================================  
  
  --========================================================================================  
  -- Begin building the final query   
  
 SET @finalQuery = ' SELECT *  
       FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result   
       WHERE  
       Result.row between' + CHAR(10) + CONVERT(varchar(10), @offset) + CHAR(10) + 'and' + CHAR(10) + CONVERT(varchar(10), @limit)  
        
      
    
  -- End building the final query  
  --========================================================================================           
   
  --========================================================================================  
  -- Begin - Execute the Query   
  print(@finalQuery)  
  EXEC (@finalQuery)  
  --EXEC(@mainSelectQuery)  
                           
  -- End - Execute the Query   
  --========================================================================================           
    
  --========================================================================================  
  -- Begin building Count Query   
    
  Declare @selectCount nvarchar(2000),   
  @parameterDef NVARCHAR(500)  
  
  SET @parameterDef = '@totalCount int OUTPUT';  
  SET @selectCount = 'SELECT  @totalCount = COUNT(PL.ASBRISKLEVELID) ' + @fromClause + @whereClause  
  
  --print @selectCount  
  EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;  
      
  -- End building the Count Query  
  --========================================================================================  
  
END