SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO











CREATE PROCEDURE dbo.FL_GETOPERATIVELOOKUP
/* ===========================================================================
 '   NAME:          FL_GETOPERATIVELOOKUP
 '   DATE CREATED:  29 Dec. 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get operative E__EMPLOYEE table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION
 '==============================================================================*/
@ORGID as Int
AS
	SELECT EMPLOYEEID AS id,FIRSTNAME AS val
	FROM E__EMPLOYEE
	WHERE  ORGID = @ORGID
	ORDER BY FIRSTNAME ASC










GO
