/****** Object:  StoredProcedure [dbo].[TO_MOB_ABANDONED_AddAbandonRequestWithImage]    Script Date: 05/30/2013 15:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TO_MOB_ABANDONED_AddAbandonRequestWithImage]
/* ===========================================================================
 '   NAME:           TO_MOB_ABANDONED_AddAbandonRequestWithImage
 '   DATE CREATED:   30 MAY 2013
 '   CREATED BY:     Jon Swain
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Same as [TO_MOB_ABANDONED_AddAbandonRequest] but adds an image too
 
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '   IN:             @Location
 '   IN:             @LookUpValueID
 '
 '   OUT:            @AbandonedId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(4000),	
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	@Location NVARCHAR(4000),
	@LookUpValueID INT,
	@GuidFilePath NVARCHAR(4000),
	@AbandonedId INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,	
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_ABANDONED(
		Location,
		EnquiryLogID,
		LookUpValueID,
		GuidFilePath
		)
		
		VALUES(
		@Location,
		@EnquiryLogID,
		@LookUpValueID,
		@GuidFilePath
		)
		
		SELECT @AbandonedId=AbandonedId
	FROM TO_ABANDONED
	WHERE AbandonedId = @@IDENTITY

				IF @AbandonedId<0
					BEGIN
						SET @AbandonedId=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @AbandonedId=-1
			ROLLBACK TRAN
			END
	

GO


