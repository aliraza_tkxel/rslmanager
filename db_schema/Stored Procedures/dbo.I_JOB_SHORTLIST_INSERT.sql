SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_JOB_SHORTLIST_INSERT]
(
	@EmpId int,
	@JobId bigint
)
AS
	SET NOCOUNT OFF;
	
DECLARE @Count int
SELECT @Count = ( SELECT COUNT(*) FROM I_JOB_SHORTLIST WHERE (I_JOB_SHORTLIST.EmpId = @EmpId) AND (I_JOB_SHORTLIST.JobId = @JobId) )

IF @Count = 0
BEGIN
    -- Only insert the item into the shortlist if the item doesn't already exist
	INSERT INTO [I_JOB_SHORTLIST] ([EmpId], [JobId]) VALUES (@EmpId, @JobId);
	SELECT ShortListId, EmpId, JobId FROM I_JOB_SHORTLIST WHERE (ShortListId = SCOPE_IDENTITY())
END
GO
