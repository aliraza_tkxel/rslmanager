USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetRepairList]    Script Date: 01/12/2016 17:02:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Ali Raza
-- Create date: 14/12/2015
-- Description:	get Reactive Repair List
-- Exec FL_GetRepairList ''
-- =============================================

IF OBJECT_ID('dbo.FL_GetRepairList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetRepairList AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FL_GetRepairList] 
( 

	-- Add the parameters for the stored procedure here
		
		@searchedText nvarchar(1000) = '',		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn nvarchar(500) = 'RepairDescription',
		@sortOrder nvarchar (5) = 'ASC',
		@isFullList bit = 0,
		@totalCount int = 0 output
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @SelectClause nvarchar(2000),
		@fromClause   nvarchar(1500),		
		@unionClause nvarchar(100),
		@whereClause  nvarchar(1500),		      
		@orderClause  nvarchar(1000),	
		@mainSelectQuery nvarchar(MAX),        
		@rowNumberQuery nvarchar(MAX),
		@finalQuery nvarchar(MAX),
		@total INT,
		@totalPaused INT,
		-- used to add in conditions in WhereClause based on search criteria provided
		@searchCriteria nvarchar(1500),
		 @searchCriteriaPaused nvarchar(1500),

		--variables for paging
		@offset int,
		@limit int

	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1

	--========================================================================================
	-- Begin building SearchCriteria clause for 
	-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteria = '1=1'

	IF(@searchedText <> '' AND  @searchedText IS NOT NULL)
	BEGIN					
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND FL.Description LIKE ''%' + @searchedText + '%'''
	END	
	
	
	--========================================================================================	        
	-- Begin building SELECT clause
	-- SELECT statements for procedure here
	DECLARE @dataLimitation varchar(10)
	IF (@isFullList = 1) SET @dataLimitation = '' ELSE SET @dataLimitation = 'top (' + CONVERT(VARCHAR(10), @limit) + ')'

	SET @SelectClause = 'SELECT	DISTINCT '+@dataLimitation +' 
	FL.FaultRepairListID,FL.[Description] as RepairDescription
	,FL.NetCost
	,FL.VatRateID
	,FL.Vat
	,FL.Gross
	,Fl.PostInspection
	,FL.StockConditionItem
	,FL.RepairActive
	,ISNULL(Fault.AssiciatedFault,0) as AssiciatedFault
    ,Convert(nvarchar(50), case  when  RepairActive =1 then 
									''Yes'' 
									else 
									''No''  end )as IsActive'

	-- End building SELECT clause
	--======================================================================================== 							

	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause =	  CHAR(10) +'FROM	FL_FAULT_REPAIR_LIST AS FL 
											OUTER APPLY (	SELECT ISNULL( Count(DISTINCT FaultId),0) as AssiciatedFault 
															FROM	FL_FAULT_ASSOCIATED_REPAIR 
															WHERE	RepairId =FL.FaultRepairListID   
															GROUP BY RepairId ) as Fault'
	-- End building From clause
	--======================================================================================== 														  

		
	-- End building From clause
	--======================================================================================== 														  
	
	IF (@sortColumn = 'Status') BEGIN
		SET @sortColumn = CHAR(10) + 'IsActive'
	END

	--========================================================================================    
	-- Begin building OrderBy clause		

	
	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

	-- End building OrderBy clause
	--========================================================================================								

	--========================================================================================
	-- Begin building WHERE clause

	-- This Where clause contains subquery to exclude already displayed records			  

	SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria

	-- End building WHERE clause
	--========================================================================================
	
	--========================================================================================

	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery = @SelectClause +@fromClause+@whereClause   + @orderClause 

	-- End building the main select Query
	--========================================================================================																																			

	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(nvarchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(nvarchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	
	IF (@isFullList = 1) BEGIN
		EXEC (@mainSelectQuery)
	END ELSE BEGIN
		EXEC (@finalQuery)
	END
																								
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	-- Declaring the variables to be used in In Progress Count Query
	Declare @selectCount nvarchar(4000),
			@parameterDefCount nvarchar(500)

	SET @parameterDefCount = '@total int OUTPUT';
	SET @selectCount = 'SELECT  @total = COUNT(FL.FaultRepairListID) ' + @fromClause + @whereClause

	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDefCount, @totalCount OUTPUT;

	

	-- End building the Count Query
	--========================================================================================							
END