SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE diagnostics.CompletePlannedAppointment
(
	@AppointmentId INT
)
AS 

-- Complete Planned Appointment

DECLARE @AppointmentStart DATETIME 
DECLARE @AppointmentEnd DATETIME 
DECLARE @JournalId INT 
DECLARE @PropertyId VARCHAR(50) 
DECLARE @ComponentId INT 
DECLARE @OperativeId INT

SET @AppointmentStart = (SELECT DateAdd(minute, DATEPART(minute, CAST(APPOINTMENTSTARTTIME AS DATETIME)), DateAdd(hour, DATEPART(hour, CAST(APPOINTMENTSTARTTIME AS DATETIME)), CAST(CAST(APPOINTMENTDATE AS DATE) AS DATETIME))) FROM dbo.PLANNED_APPOINTMENTS WHERE APPOINTMENTID = @AppointmentId) 
SET @AppointmentEnd = (SELECT DateAdd(minute, DATEPART(minute, CAST(APPOINTMENTENDTIME AS DATETIME)), DateAdd(hour, DATEPART(hour, CAST(APPOINTMENTENDTIME AS DATETIME)), CAST(CAST(APPOINTMENTENDDATE AS DATE) AS DATETIME))) FROM dbo.PLANNED_APPOINTMENTS WHERE APPOINTMENTID = @AppointmentId)
SET @JournalId = (SELECT JournalId FROM dbo.PLANNED_APPOINTMENTS WHERE APPOINTMENTID = @AppointmentId)
SET @PropertyId = (SELECT PROPERTYID FROM dbo.PLANNED_JOURNAL WHERE JOURNALID = @JournalId)
SET @ComponentId = (SELECT ComponentId FROM dbo.PLANNED_JOURNAL WHERE JOURNALID = @JournalId)
SET @OperativeId = (SELECT AssignedTo FROM dbo.PLANNED_APPOINTMENTS WHERE APPOINTMENTID = @AppointmentId)

SELECT @AppointmentStart AS [AppointmentStart], @AppointmentEnd AS [AppointmentEnd], @JournalId AS JournalId, @PropertyId AS PropertyId, @ComponentId AS ComponentId, @OperativeId AS OperativeId

SELECT * FROM dbo.PLANNED_APPOINTMENTS WHERE APPOINTMENTID = @AppointmentId

UPDATE  [dbo].[PLANNED_APPOINTMENTS]
SET     [JOURNALSUBSTATUS] = 4 ,
        [APPOINTMENTSTATUS] = 'Complete'
WHERE   APPOINTMENTID = @AppointmentId

SELECT * FROM dbo.PLANNED_APPOINTMENTS WHERE APPOINTMENTID = @AppointmentId

SELECT * FROM dbo.PLANNED_JOBTIMESHEET WHERE APPOINTMENTID = @AppointmentId

IF EXISTS ( SELECT  *
            FROM    [PLANNED_JOBTIMESHEET]
            WHERE   APPOINTMENTID = @AppointmentId ) 
    BEGIN 

        UPDATE  [PLANNED_JOBTIMESHEET]
        SET     [StartTime] = @AppointmentStart ,
                [EndTime] = @AppointmentEnd
        WHERE   [APPOINTMENTID] = @AppointmentId

    END
ELSE 
    BEGIN

        INSERT  [dbo].[PLANNED_JOBTIMESHEET]
                ( [StartTime] ,
                  [EndTime] ,
                  [APPOINTMENTID]
                )
        VALUES  ( @AppointmentStart ,
                  @AppointmentEnd ,
                  @AppointmentId
                )
    END 
        
SELECT * FROM dbo.PLANNED_JOBTIMESHEET WHERE APPOINTMENTID = @AppointmentId

SELECT  PID.*
FROM    dbo.PA_PROPERTY_ITEM_DATES PID
        INNER JOIN dbo.PLANNED_JOURNAL pa ON pid.PROPERTYID = pa.PROPERTYID
                                             AND pid.PLANNED_COMPONENTID = pa.COMPONENTID
        INNER JOIN dbo.PLANNED_APPOINTMENTS paa ON pa.JOURNALID = paa.JournalId
        INNER JOIN dbo.PLANNED_COMPONENT pc ON pc.COMPONENTID = pa.COMPONENTID
WHERE   paa.APPOINTMENTID = @AppointmentId


UPDATE  PID
SET     pid.LastDone = CAST(@AppointmentEnd AS DATE) ,
        pid.DueDate = DATEADD(year, pc.Cycle, CAST(@AppointmentEnd AS DATE))
FROM    dbo.PA_PROPERTY_ITEM_DATES PID
        INNER JOIN dbo.PLANNED_JOURNAL pa ON pid.PROPERTYID = pa.PROPERTYID
                                             AND pid.PLANNED_COMPONENTID = pa.COMPONENTID
        INNER JOIN dbo.PLANNED_APPOINTMENTS paa ON pa.JOURNALID = paa.JournalId
        INNER JOIN dbo.PLANNED_COMPONENT pc ON pc.COMPONENTID = pa.COMPONENTID
WHERE   paa.APPOINTMENTID = @AppointmentId


SELECT  PID.*
FROM    dbo.PA_PROPERTY_ITEM_DATES PID
        INNER JOIN dbo.PLANNED_JOURNAL pa ON pid.PROPERTYID = pa.PROPERTYID
                                             AND pid.PLANNED_COMPONENTID = pa.COMPONENTID
        INNER JOIN dbo.PLANNED_APPOINTMENTS paa ON pa.JOURNALID = paa.JournalId
        INNER JOIN dbo.PLANNED_COMPONENT pc ON pc.COMPONENTID = pa.COMPONENTID
WHERE   paa.APPOINTMENTID = @AppointmentId


SELECT  *
FROM    dbo.PLANNED_APPOINTMENTS
WHERE   JournalId = @JournalId

IF NOT EXISTS ( SELECT  *
                FROM    dbo.PLANNED_APPOINTMENTS
                WHERE   APPOINTMENTSTATUS <> 'Complete'
                        AND JournalId = @JournalId ) 
    BEGIN 


        SELECT  *
        FROM    dbo.PLANNED_JOURNAL WHERE [JOURNALID] = @JournalId
        SELECT  *
        FROM    dbo.PLANNED_JOURNAL_HISTORY WHERE [JOURNALID] = @JournalId

        UPDATE  [dbo].[PLANNED_JOURNAL]
        SET     [STATUSID] = 4
        WHERE   ( [JOURNALID] = @JournalId )

		IF NOT EXISTS(SELECT * FROM dbo.PLANNED_JOURNAL_HISTORY WHERE JOURNALID = @JOURNALID AND STATUSID = 4)
		BEGIN 
		
			INSERT  [dbo].[PLANNED_JOURNAL_HISTORY]
					( [JOURNALID] ,
					  [PROPERTYID] ,
					  [COMPONENTID] ,
					  [STATUSID] ,
					  [ACTIONID] ,
					  [CREATIONDATE] ,
					  [CREATEDBY] ,
					  [NOTES] ,
					  [ISLETTERATTACHED] ,
					  [StatusHistoryId] ,
					  [ActionHistoryId] ,
					  [IsDocumentAttached]
					)
			VALUES  ( @JournalId ,
					  @PropertyId ,
					  @ComponentId ,
					  4 ,
					  NULL ,
					  @AppointmentEnd ,
					  @OperativeId ,
					  NULL ,
					  NULL ,
					  NULL ,
					  NULL ,
					  NULL
					)
		END
		
        SELECT  *
        FROM    dbo.PLANNED_JOURNAL WHERE [JOURNALID] = @JournalId
        SELECT  *
        FROM    dbo.PLANNED_JOURNAL_HISTORY WHERE [JOURNALID] = @JournalId


    END 

GO
