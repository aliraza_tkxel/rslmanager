SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[I_JOBS_SEARCH]
(	
	@JobTitle nvarchar(30) = NULL,
	@LocationId int = NULL,
	@PostedIn int = NULL,
	@Active int
)
AS

DECLARE @SQL NVARCHAR(4000), @PARAMLIST  NVARCHAR(4000)
                     
SET NOCOUNT ON 
                  
SELECT @SQL = 'SELECT J.*,I_JOB_TYPE.JobType, G_OFFICE.DESCRIPTION  as Location,E_TEAM.TeamName
				FROM  I_JOBS_CURRENT AS J
				INNER JOIN I_JOB_TYPE on J.TypeId = I_JOB_TYPE.JobTypeId
				INNER JOIN G_OFFICE on J.LocationId = G_OFFICE.OfficeId
				INNER JOIN E_TEAM on J.TeamId = E_TEAM.TeamId WHERE (J.Active = @Active) AND 
				(J.EXPIRED = 0 )'

IF @JobTitle IS NOT NULL                                            
   SELECT @SQL = @SQL + ' AND (J.Title LIKE ''%' + @JobTitle + '%'')'
    

IF @LocationId IS NOT NULL 
	SELECT @SQL = @SQL + ' AND (J.LocationId = @LocationId)'

if @PostedIn = 1 
begin
	SELECT @SQL = @SQL + 'AND (DATEDIFF(day, J.DatePosted, GETDATE())) <= 7'
end
else if @PostedIn = 2
begin
	SELECT @SQL = @SQL + 'AND (DATEDIFF(day, J.DatePosted, GETDATE())) <= 2'
end
else if @PostedIn = 3
begin	
	SELECT @SQL = @SQL + 'AND (DATEDIFF(day, J.DatePosted, GETDATE())) < 1'
end
else if @PostedIn = 4
begin
	SELECT @SQL = @SQL + 'AND (DATEDIFF(hour, J.DatePosted, GETDATE())) <= 4'
end
else if @PostedIn = 5
begin
	SELECT @SQL = @SQL + 'AND (DATEDIFF(hour, J.DatePosted, GETDATE())) <= 2'
end

SELECT @SQL = @SQL + ' ORDER BY J.DatePosted DESC ' 

SELECT @PARAMLIST =	'@JobTitle nvarchar(30),
					 @LocationId int,
					 @PostedIn int,
					 @Active int'
					 
SET NOCOUNT OFF 

PRINT '*********** Final SQL *************'
PRINT @SQL
PRINT '***********************************'

EXEC SP_EXECUTESQL @SQL, @PARAMLIST,
@JobTitle,
@LocationId,
@PostedIn,
@Active




















GO
