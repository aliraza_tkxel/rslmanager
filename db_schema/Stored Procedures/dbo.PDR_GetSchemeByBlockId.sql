USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlockNameBySchemeId]    Script Date: 12/12/2018 12:14:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    
    Execution Command:
    
    Exec dbo.PDR_GetSchemeByBlockId 2
  =================================================================================*/
 IF OBJECT_ID('dbo.PDR_GetSchemeByBlockId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetSchemeByBlockId AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_GetSchemeByBlockId]
( @blockId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ISNULL(Sch.SCHEMEID,0) As SchemeId, 
	isNull(Sch.SCHEMENAME,'-') As SchemeName
	FROM P_BLOCK Bl
	LEFT JOIN P_SCHEME Sch ON Sch.SCHEMEID = Bl.SchemeId
	WHERE Bl.BLOCKID = @blockId
END
