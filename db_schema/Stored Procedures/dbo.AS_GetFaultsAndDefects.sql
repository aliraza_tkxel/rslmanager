USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================              
 EXEC AS_GetFaultsAndDefects @propertyId ='BHA0001816'              
  ,@actionType = 5              
             
-- ============================================= */      


IF OBJECT_ID('dbo.AS_GetFaultsAndDefects') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetFaultsAndDefects AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetFaultsAndDefects]  
(               
  @propertyId varchar(50),              
  @actionType int = -1                 
  -- column name on which sorting is performed              
         
                
)              
AS              
BEGIN      
              
              
 DECLARE @PlannedSelectClause NVARCHAR(MAX),              
   @PlannedFromClause NVARCHAR(MAX),              
   @PlannedWhereClause NVARCHAR(MAX),              
   
   @PlannedSelectClause_AC NVARCHAR(MAX),              
   @PlannedFromClause_AC NVARCHAR(MAX),              
   @PlannedWhereClause_AC NVARCHAR(MAX),

   @PdrSelectClause NVARCHAR(MAX),            
   @PdrFromClause NVARCHAR(MAX),            
   @PdrWhereClause NVARCHAR(MAX),            
            
                 
   @ConditionSelectClause NVARCHAR(MAX),              
   @ConditionFromClause NVARCHAR(MAX),              
   @ConditionWhereClause NVARCHAR(MAX),              
                 
   @unionClause NVARCHAR(MAX),              
   @mainSelectQuery Nvarchar(MAX),   
   
   @StockConditionSelectClause NVARCHAR(MAX),            
   @StockConditionFromClause NVARCHAR(MAX),            
   @StockConditionWhereClause NVARCHAR(MAX),           
               
   @ApplianceSelectClause NVARCHAR(MAX),              
   @ApplianceFromClause NVARCHAR(MAX),              
   @ApplianceWhereClause NVARCHAR(MAX),              
    
   @ApplianceDefectSelectClause NVARCHAR(MAX),          
   @ApplianceDefectFromClause NVARCHAR(MAX),          
   @ApplianceDefectWhereClause NVARCHAR(MAX),
   @ApplianceDefectOrderClause NVARCHAR(max),
   
                  
   @orderClause  NVARCHAR(max),              
   @FaultSelectClause NVARCHAR(MAX),            
   @FaultFromClause NVARCHAR(MAX),            
   @FaultWhereClause NVARCHAR(MAX)  ,  
     
     
   @PaintPackSelectClause NVARCHAR(MAX),            
   @PaintPackFromClause NVARCHAR(MAX),            
   @PaintPackWhereClause NVARCHAR(MAX),            
  
   @VoidWorksSelectClause NVARCHAR(MAX),            
   @VoidWorksfromClause NVARCHAR(MAX),            
   @VoidWorksWhereClause NVARCHAR(MAX),            
   @VoidWorksOrderClause NVARCHAR(MAX),

   @VoidInspectionsSelectClause NVARCHAR(MAX),            
   @VoidInspectionsfromClause NVARCHAR(MAX),            
   @VoidInspectionsWhereClause NVARCHAR(MAX),            
   @VoidInspectionsOrderClause NVARCHAR(MAX),  
   
   @sortColumn varchar(50) = 'CREATIONDATE',              
   @sortOrder varchar (5) = 'DESC',          
   @Inspection varchar(100)  
    
--===========================================================================================              
--       INSPECTION DESCRIPTION  
--===========================================================================================      
    
  SELECT @Inspection = P.Description FROM P_INSPECTIONTYPE P WHERE P.InspectionTypeID = @actionType  
    
--===========================================================================================            
--       CONDITION ACTIVITIES              
--===========================================================================================              
      
SET @ConditionSelectClause = '            
  SELECT  DISTINCT  [PCW].ConditionWorksHistoryId AS JournalHistoryId            
  ,ISNULL(COALESCE(PC.COMPONENTNAME,[I].ItemName) +'' - ''+ PV.ValueDetail,''N/A'') AS Status
  , CASE  WHEN [PLA].Title =''Rejected'' THEN    
  [PLA].Title + '' - '' + [PCWR].[RejectionNotes]    
	 ELSE    
	  ISNULL([PLA].Title,''N/A'')     
	 END AS Action  
  ,''Condition Works'' AS InspectionType              
  ,CONVERT(varchar(10), [PCW].CreatedDate , 103) AS CreateDate            
  ,SUBSTRING(E.FIRSTNAME,1,1) + '' '' +E.LASTNAME AS Name             
  ,[PCW].CreatedDate AS CREATIONDATE  
    
  ,ISNULL((''JSN'' + CONVERT(VARCHAR,A.APPOINTMENTID) ),''N/A'') AS REF     
  ,ISNULL(Convert( NVarchar, A.APPOINTMENTDATE, 103 ),''N/A'') As AppointmentDate     
  ,ISNULL((OP.FIRSTNAME + '' '' + OP.LASTNAME),''N/A'')  As OPERATIVENAME    
  ,ISNULL(T.Description,''N/A'') As  OPERATIVETRADE 
    
  ,[PCW].ConditionWorksId As JournalId
  ,ISNULL(A.APPOINTMENTNOTES,'''') AS  AppointmentNotes 
  ,''Condition'' AS InspectionTypeDescription
  ,ISNULL(A.APPOINTMENTID,-1) AS APPOINTMENTID
  ,CW.WorksRequired AS [Description]  
  ,0 AS IsDocumentAttached
  ,0 AS CP12DocumentID
  ' + CHAR(10)      
      
SET @ConditionFromClause = ' FROM PLANNED_CONDITIONWORKS_HISTORY  [PCW]             
  INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId             
  INNER JOIN PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID             
  INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID              
  INNER JOIN PA_ITEM [I] ON [I].ItemId = [IP].ItemId             
  INNER JOIN PA_PARAMETER_VALUE PV ON [PCW].VALUEID = PV.ValueID                
    
  INNER JOIN PLANNED_CONDITIONWORKS CW ON CW.ConditionWorksId = PCW.ConditionWorksId    
  INNER JOIN PLANNED_JOURNAL J ON CW.JournalId = J.JOURNALID    
  INNER JOIN PLANNED_APPOINTMENTS A ON A.JournalId = J.JOURNALID    
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = A.ASSIGNEDTO    
  LEFT JOIN PLANNED_MISC_TRADE MT ON MT.AppointmentId = A.APPOINTMENTID    
  LEFT JOIN G_TRADE T ON T.TradeId = MT.TradeId     
    
  LEFT JOIN E__EMPLOYEE E ON [PCW].CreatedBy = E.EMPLOYEEID     
  LEFT JOIN PA_PROPERTY_ITEM_DATES [PID] ON [PID].ItemId = [IP].ItemID AND [PID].PROPERTYID = [ATT].PROPERTYID AND ([PID].ParameterId = [IP].ParameterId OR [PID].ParameterId IS NULL)            
  LEFT JOIN PLANNED_COMPONENT [PC] ON [PC].COMPONENTID = [PCW].COMPONENTID    
  LEFT JOIN PLANNED_CONDITIONWORKS_REJECTED [PCWR] ON [PCWR].ConditionWorksId = [PCW].ConditionWorksId '    
        
+ CHAR(10)      
      
      
SET @ConditionWhereClause = 'WHERE [ATT].PROPERTYID = ''' + @propertyId + ''''      
+ CHAR(10)      
      
 -- PRINT '--===================================================================='  
 -- PRINT @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause  
--===========================================================================================              
--       PLANNED MAINTENANCE ACTIVITIES              
--===========================================================================================              
     
SET @PlannedSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId              
   ,    COALESCE(PBS.TITLE, AH.APPOINTMENTSTATUS) AS Status                   
   ,ISNULL(C.COMPONENTNAME ,''N/A'' ) AS Action            
  ,AT.Planned_Appointment_Type AS InspectionType              
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate              
  ,ISNULL(E.FIRSTNAME,'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name              
                
  ,AH.LOGGEDDATE AS CREATIONDATE    
  ,ISNULL(( ''JSN'' + Convert( NVarchar, A.APPOINTMENTID )),''N/A'' ) AS REF     
  ,ISNULL((Convert( NVarchar, A.APPOINTMENTDATE, 103 )),''N/A'') As AppointmentDate     
  ,ISNULL((SUBSTRING(OP.FIRSTNAME,1,1) +'' ''+ OP.lastname),''N/A'') As OPERATIVENAME    
  ,ISNULL(T.Description,''N/A'') As OPERATIVETRADE   
  ,PJH.JOURNALID As JournalId
  ,ISNULL(A.APPOINTMENTNOTES,'''') AS  AppointmentNotes
  ,AT.Planned_Appointment_Type AS InspectionTypeDescription  
  ,ISNULL(A.APPOINTMENTID,-1) AS APPOINTMENTID 
  ,C.ComponentName AS [Description]
  ,0 AS IsDocumentAttached
  ,0 AS CP12DocumentID
  ' + CHAR(10)      
      
SET @PlannedFromClause = ' FROM PLANNED_JOURNAL_HISTORY PJH        
  LEFT JOIN PLANNED_COMPONENT C ON PJH.COMPONENTID = C.COMPONENTID               
  INNER JOIN PLANNED_STATUS S ON  PJH.STATUSID = S.STATUSID   
             
  LEFT JOIN PLANNED_Action ACT ON  PJH.ACTIONID = ACT.ActionId               
  LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID        
  INNER JOIN PLANNED_APPOINTMENTS_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID        
  INNER JOIN PLANNED_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID    
  LEFT JOIN PLANNED_SUBSTATUS PBS ON  AH.JOURNALSUBSTATUS = PBS.SUBSTATUSID   
  LEFT JOIN Planned_Appointment_Type AT ON AT.Planned_Appointment_TypeId = A.Planned_Appointment_TypeId     
  LEFT JOIN PLANNED_INSPECTION_APPOINTMENTS PIA ON PIA.JournalId = PJH.JOURNALID    
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = PIA.ASSIGNEDTO OR OP.EMPLOYEEID = AH.ASSIGNEDTO      
  LEFT JOIN PLANNED_COMPONENT_TRADE CT ON CT.COMPTRADEID = A.COMPTRADEID    
  LEFT JOIN PLANNED_MISC_TRADE MT ON MT.AppointmentId = A.APPOINTMENTID    
  LEFT JOIN G_TRADE T ON T.TRADEID = CT.TRADEID OR T.TradeId = MT.TradeId '    
+ CHAR(10)      
      
SET @PlannedWhereClause = 'WHERE PJH.PROPERTYID = ''' + @propertyId + ''''      
+ CHAR(10)      
      --PRINT '--===================================================================='  
  --PRINT @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause  
    
--===========================================================================================              
--       PLANNED MAINTENANCE ACTIVITIES - Assigned to Contractor             
--===========================================================================================              
     
SET @PlannedSelectClause_AC = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId 
	,ISNULL(S.TITLE,''N/A'') AS Status              
  ,ISNULL(ACT.TITLE,''N/A'' ) AS Action              
  ,''Planned - ''+ COALESCE(C.COMPONENTNAME,''N/A'') AS InspectionType              
  ,Convert(varchar(10),PJH.CREATIONDATE,103) as CreateDate              
  ,ISNULL(E.FIRSTNAME,'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name                        
   ,PJH.CREATIONDATE AS CREATIONDATE
   ,ISNULL(( ''PMO'' + Convert( NVarchar, PCW.JOURNALID )),''N/A'' ) AS REF     
  ,ISNULL((Convert( NVarchar, PCW.ASSIGNEDDATE, 103 )),''N/A'') As AppointmentDate
  ,ISNULL(SO.NAME,''N/A'') As OPERATIVENAME                    
  ,''N/A'' As OPERATIVETRADE   
  ,PJH.JOURNALID As JournalId
   ,''N/A'' AS  AppointmentNotes
  ,''Assigned To Contractor'' AS InspectionTypeDescription  
  ,PCW.PLANNEDCONTRACTORID AS APPOINTMENTID
  ,C.ComponentName AS [Description]
  ,ISNULL(IsDocumentAttached, 0) AS IsDocumentAttached  
  ,0 as CP12DocumentID
  ' + CHAR(10)      
      
SET @PlannedFromClause_AC = ' FROM PLANNED_JOURNAL_HISTORY PJH        
  LEFT JOIN PLANNED_COMPONENT C ON PJH.COMPONENTID = C.COMPONENTID               
  INNER JOIN PLANNED_STATUS S ON  PJH.STATUSID = S.STATUSID                
  LEFT JOIN PLANNED_Action ACT ON  PJH.ACTIONID = ACT.ActionId               
  LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID  
  INNER JOIN PLANNED_CONTRACTOR_WORK PCW ON  PJH.JOURNALID = PCW.JOURNALID         
  INNER JOIN S_ORGANISATION SO on PCW.ContractorId = SO.ORGID'    
+ CHAR(10)      
      
SET @PlannedWhereClause_AC = 'WHERE PJH.PROPERTYID = ''' + @propertyId + ''''      
+ CHAR(10)      
   --   PRINT '--===================================================================='  
 -- PRINT @PlannedSelectClause_AC + @PlannedFromClause_AC + @PlannedWhereClause_AC

--===========================================================================================            
--       PDR ACTIVITIES            
--===========================================================================================            
  
SET @PdrSelectClause = 'SELECT  PJH.JOURNALHISTORYID as JournalHistoryId            
  ,ISNULL(AH.APPOINTMENTSTATUS, ''N/A'')  AS Status            
   ,''N/A''  AS Action          
  ,MSATTYPE.MSATTypeName +'' - ''+ ISNULL(PL.LocationName +'' > ''+ PA.AreaName + '' > '' + PT.ItemName,''N/A'') AS InspectionType            
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate            
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name            
             
  ,AH.LOGGEDDATE AS CREATIONDATE     
  ,ISNULL(( ''JSN'' + Convert( NVarchar, AH.APPOINTMENTID ) ),''N/A'') AS REF     
  ,ISNULL(Convert( NVarchar, AH.APPOINTMENTSTARTDATE, 103 ),''N/A'') As AppointmentDate     
  ,ISNULL((SUBSTRING( OP.FIRSTNAME,1,1) +'' ''+ OP.lastname),''N/A'') As OPERATIVENAME    
  ,ISNULL(T.Description,''N/A'') As OPERATIVETRADE  
  ,PJH.JOURNALID As JournalId  
  ,ISNULL(AH.APPOINTMENTNOTES,'''') AS  AppointmentNotes  
  ,MSATTYPE.MSATTypeName AS InspectionTypeDescription 
  ,ISNULL(AH.APPOINTMENTID,-1) AS APPOINTMENTID
  ,MSATTYPE.MSATTypeName AS [Description]
  ,0 AS IsDocumentAttached
  ,0 AS CP12DocumentID
   
   ' + CHAR(10)    
    
SET @PdrFromClause = ' FROM PDR_JOURNAL_HISTORY PJH  
 INNER JOIN PDR_MSAT MSAT ON PJH.MSATID = MSAT.MSATId  
 INNER JOIN PDR_MSATType MSATTYPE ON MSAT.MSATTypeId = MSATTYPE.MSATTypeId   
 INNER JOIN PDR_STATUS S ON  PJH.STATUSID = S.STATUSID    
 INNER JOIN PA_ITEM PT ON MSAT.ItemId = PT.ItemID  
 INNER JOIN PA_AREA PA ON  PT.AreaID = PA.AreaID  
 INNER JOIN PA_LOCATION PL ON PA.LocationId = PL.LocationID          
 LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID  
 left JOIN PDR_APPOINTMENT_HISTORY AH ON PJH.JOURNALID = AH.JOURNALID        
 --INNER JOIN PDR_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID    
 LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AH.ASSIGNEDTO   
 LEFT JOIN G_TRADE T ON AH.TRADEID = T.TRADEID   
     ' + CHAR(10)    
    
    
SET @PdrWhereClause = 'WHERE MSAT.PROPERTYID = ''' + @propertyId + ''''    
  
+ CHAR(10)    
  
IF @actionType != -1  
BEGIN  
 SET @PdrWhereClause = @PdrWhereClause+ ' AND  MSATTYPE.MSATTypeName = '''+ @Inspection + ''''    
END  
      
   -- PRINT '--===================================================================='  
    --PRINT @PdrSelectClause + @PdrFromClause + @PdrWhereClause  
--===========================================================================================              
--       APPLIANCES ACTIVITIES              
--===========================================================================================         
      
SET @ApplianceSelectClause = 'SELECT     DISTINCT          
      AS_JOURNALHISTORY.JOURNALHISTORYID as JournalHistoryId              
      ,CASE WHEN AS_APPOINTMENTSHISTORY.APPOINTMENTSTATUS = ''Finished'' THEN ''Certificate issued''
            WHEN AS_APPOINTMENTSHISTORY.APPOINTMENTSTATUS = ''InProgress'' THEN ''In Progress''  
			WHEN AS_APPOINTMENTSHISTORY.APPOINTMENTSTATUS = ''NotStarted'' THEN ''Arranged''
			 ELSE   AS_APPOINTMENTSHISTORY.APPOINTMENTSTATUS      
		END AS Status                
      ,ISNULL(as_action.title, ''N/A'') as Action             
      ,ISNULL(P_INSPECTIONTYPE.description, ''N/A'') AS InspectionType              
      ,Convert(varchar(10),AS_JOURNALHISTORY.creationdate,103) as CreateDate              
      ,ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name              
                  
      ,AS_APPOINTMENTSHISTORY.LOGGEDDATE AS CREATIONDATE    
      , ISNULL((''JSG'' + convert(NVARCHAR, COALESCE(AS_APPOINTMENTS.JSGNUMBER,AH.JSGNUMBER) )),''N/A'') AS REF     
      ,ISNULL(( Convert( NVarchar, COALESCE(AS_APPOINTMENTSHISTORY.APPOINTMENTDATE,AH.APPOINTMENTDATE), 103 ) + '' '' + COALESCE(AS_APPOINTMENTSHISTORY.APPOINTMENTSTARTTIME,AH.APPOINTMENTSTARTTIME) + ''-'' + COALESCE(AS_APPOINTMENTSHISTORY.APPOINTMENTENDTIME,AH.APPOINTMENTENDTIME) ),''N/A'') As AppointmentDate     
      ,ISNULL((SUBSTRING( COALESCE(OP.FIRSTNAME,OPP.FIRSTNAME),1,1) + '' '' +  COALESCE(OP.LASTNAME,OPP.LASTNAME) ),''N/A'') As OPERATIVENAME     
      ,''N/A'' As OPERATIVETRADE   
      ,AS_JOURNAL.JOURNALID As JournalId
      ,ISNULL(AS_APPOINTMENTS.Notes,'''') As AppointmentNotes 
      ,''GAS'' AS InspectionTypeDescription 
      ,ISNULL(AS_APPOINTMENTSHISTORY.APPOINTMENTID,-1) AS APPOINTMENTID    
      ,as_action.title AS [Description]
	  ,0 AS IsDocumentAttached
	  ,0 AS CP12DocumentID
	  
	  '
       + CHAR(10)      
      
SET @ApplianceFromClause = ' FROM AS_Journal
INNER JOIN AS_JOURNALHISTORY  ON AS_JOURNAL.JOURNALID=AS_JOURNALHISTORY.JOURNALID
INNER JOIN P_INSPECTIONTYPE ON AS_JOURNALHISTORY.INSPECTIONTYPEID = P_INSPECTIONTYPE.INSPECTIONTYPEID  
INNER JOIN as_status ON as_status.statusid = AS_JOURNALHISTORY.statusid  
LEFT JOIN AS_Action ON AS_Action.actionid = AS_JOURNALHISTORY.actionid  
INNER JOIN e__employee ON e__employee.employeeid = AS_JOURNALHISTORY.createdby  
LEFT JOIN P_LGSR_HISTORY  ON P_LGSR_HISTORY.JOURNALID = AS_Journal.JOURNALID  AND P_LGSR_HISTORY.CP12DOCUMENT IS NOT NULL
LEFT JOIN AS_APPOINTMENTSHISTORY ON AS_JOURNALHISTORY.JOURNALhistoryID = AS_APPOINTMENTSHISTORY.JOURNALhistoryID  
LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTSHISTORY.APPOINTMENTID = AS_APPOINTMENTS.APPOINTMENTID
LEFT JOIN E__EMPLOYEE OP ON AS_APPOINTMENTSHISTORY.ASSIGNEDTO = OP.EMPLOYEEID  
LEFT JOIN (	SELECT  JOURNALID,MAX(APPOINTMENTHISTORYID) AS APPOINTMENTHISTORYID
			FROM	AS_APPOINTMENTSHISTORY
			GROUP BY JOURNALID
			) AS AS_HISTORY ON AS_HISTORY.JOURNALID = AS_JOURNALHISTORY.JOURNALID
LEFT JOIN AS_APPOINTMENTSHISTORY AS AH ON AS_HISTORY.APPOINTMENTHISTORYID = AH.APPOINTMENTHISTORYID
LEFT JOIN E__EMPLOYEE OPP ON AH.ASSIGNEDTO = OPP.EMPLOYEEID 
'  + CHAR(10)      
      
      
SET @ApplianceWhereClause = 'WHERE (' + CONVERT(NVARCHAR, @actionType) + '= -1 ' + ' OR as_journalhistory.inspectiontypeid = ' + CONVERT(NVARCHAR, @actionType) + ')              
       AND AS_JOURNALHISTORY.PROPERTYID= ''' + @propertyId + ''' AND as_status.title NOT IN (''Appointment to be arranged'') AND AS_APPOINTMENTSHISTORY.APPOINTMENTSTATUS is not null'      
+ CHAR(10)      
      
  --PRINT '--===================================================================='  
  --PRINT @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause  
    
--===========================================================================================            
--       APPLIANCES DEFECT ACTIVITIES            
--===========================================================================================       
       
   SET @ApplianceDefectSelectClause = 'SELECT DISTINCT
		P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectHistoryId as JournalHistoryId 
		     ,CASE WHEN PDR_STATUS.AppTitle IS NOT NULL THEN 
			PDR_STATUS.AppTitle 
	  ELSE 
			''N/A'' 
	  END as Status      	
		 ,CASE WHEN PDR_STATUS.Title = ''Completed'' THEN 
			''Defect: Completed'' 
		ELSE 
			ISNULL(''Defect Category :''+P_DEFECTS_CATEGORY.Description, ''N/A'') 
		END AS  Action                      
	  	            
      ,''Appliance Defect''  AS InspectionType              
      ,Convert(varchar(10),P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectDate,103) as CreateDate              
       ,ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name   	   	                            
	  ,COALESCE(P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.[ModifiedDate],LOGGEDDATE) AS CREATIONDATE 	    
	  , ISNULL(''JSD'' + RIGHT(''00000''+ CONVERT(nVARCHAR,P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PropertyDefectId),5),''N/A'') AS REF               
      ,ISNULL(( Convert( NVarchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103 ) + '' '' + PDR_APPOINTMENTS.APPOINTMENTSTARTTIME + ''-'' + PDR_APPOINTMENTS.APPOINTMENTENDTIME ),''N/A'') As AppointmentDate     
      ,ISNULL((SUBSTRING( OP.FIRSTNAME,1,1) + '' '' + OP.LASTNAME),''N/A'') As OPERATIVENAME      
      ,isNull(G_TRADE.Description,''N/A'') As OPERATIVETRADE
      ,P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PropertyDefectId As JournalId 
      ,ISNULL(PDR_APPOINTMENTS.APPOINTMENTNOTES,'''') AS  AppointmentNotes 
      ,''Defects'' AS InspectionTypeDescription  
      ,ISNULL(PDR_APPOINTMENTS.APPOINTMENTID,-1) AS APPOINTMENTID
      ,P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectNotes AS [Description]
	  ,0 AS IsDocumentAttached
	  ,0 AS CP12DocumentID
   
   ' + CHAR(10)    
    
SET @ApplianceDefectFromClause = CHAR(10)+' FROM P_PROPERTY_APPLIANCE_DEFECTS_HISTORY 
	  INNER JOIN PDR_STATUS ON P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectJobSheetStatus = PDR_STATUS.STATUSID
	  LEFT JOIN GS_PROPERTY_APPLIANCE GPA ON P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.APPLIANCEID = GPA.PROPERTYAPPLIANCEID
	  INNER JOIN P_DEFECTS_CATEGORY ON P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.CategoryId = P_DEFECTS_CATEGORY.CategoryId
	  INNER JOIN AS_JOURNAL ON AS_JOURNAL.JOURNALID =P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.JournalId	
	  INNER JOIN P_INSPECTIONTYPE ON AS_JOURNAL.INSPECTIONTYPEID = P_INSPECTIONTYPE.INSPECTIONTYPEID 
	  INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.CreatedBy	  
	  INNER JOIN PDR_APPOINTMENTS ON P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.ApplianceDefectAppointmentJournalId = PDR_APPOINTMENTS.JOURNALID
	  LEFT JOIN G_TRADE ON P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.TradeId = G_TRADE.TradeId
	  LEFT JOIN E__EMPLOYEE OP ON PDR_APPOINTMENTS.ASSIGNEDTO = OP.EMPLOYEEID
 '+ CHAR(10)    
    
    SET @ApplianceDefectOrderClause = CHAR(10)+ 'Order By P_PROPERTY_APPLIANCE_DEFECTS.DefectDate DESC'
    
SET @ApplianceDefectWhereClause = 'WHERE (' + CONVERT(NVARCHAR, @actionType) + '= -1 ' + ' OR P_INSPECTIONTYPE.INSPECTIONTYPEID = ' + CONVERT(NVARCHAR, @actionType) + ')  
	 AND (((P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.ApplianceId <> 0 ) AND GPA.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.ApplianceId = 0 ))   	          
       AND P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PROPERTYID= ''' + @propertyId + ''''
       + CHAR(10)    
                    
    
  --PRINT '--===================================================================='
   --PRINT @ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause
          
--===========================================================================================            
--       FAULT APPOINTMENTS            
--===========================================================================================            
      
SET @FaultSelectClause = 'SELECT DISTINCT FL_FAULT_LOG_HISTORY.FaultLogHistoryID as JournalHistoryId              
        ,ISNULL(FL_FAULT_STATUS.[Description],''N/A'' ) AS Status              
         ,FL_AREA.AreaName +'' > ''+ FL_FAULT.DESCRIPTION   AS Action         
        ,''Fault Repair'' AS InspectionType              
        ,Convert(varchar(10),FL_FAULT_LOG.SubmitDate ,103) as CreateDate              
        ,ISNULL(E__EMPLOYEE.FIRSTNAME,'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'' '')  AS Name              
                   
        ,FL_FAULT_LOG.SubmitDate AS CREATIONDATE     
        ,ISNULL(FL_FAULT_LOG.JobSheetNumber,''N/A'')  AS REF     
        ,ISNULL((Convert( NVarchar, FL_CO_APPOINTMENT.AppointmentDate, 103 )),''N/A'') As AppointmentDate     
        ,ISNULL((SUBSTRING(E__EMPLOYEE.FIRSTNAME,1,1) +'' '' + E__EMPLOYEE.lastname ),''N/A'') As OPERATIVENAME    
        ,ISNULL(T.Description,''N/A'') As OPERATIVETRADE   
         ,FL_FAULT_LOG_HISTORY.JournalID As JournalId
         ,ISNULL(FL_CO_APPOINTMENT.Notes,'''')  AS  AppointmentNotes
         ,CASE  WHEN FL_FAULT_LOG.PROPERTYID IS NULL THEN ''SbFault''  
			ELSE ''Fault''  END  AS InspectionTypeDescription
         ,ISNULL(FL_CO_APPOINTMENT.AppointmentID,-1) AS APPOINTMENTID    
        ,FL_AREA.AreaName +'' > ''+ FL_FAULT.DESCRIPTION  AS [Description]
		,0 AS IsDocumentAttached
		,0 AS CP12DocumentID
		
		' + CHAR(10)      
    
SET @FaultFromClause = 'from FL_FAULT_LOG_HISTORY    
      Inner Join FL_FAULT_LOG On FL_FAULT_LOG.FaultLogID = FL_FAULT_LOG_HISTORY.FaultLogID  
      INNER JOIN FL_FAULT ON FL_FAULT_LOG.FAULTID=FL_FAULT.FAULTID 
	  INNER JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID            
      LEFT join FL_FAULT_APPOINTMENT on FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID             
      INNER JOIN FL_FAULT_STATUS on FL_FAULT_LOG_HISTORY.FaultStatusID = FL_FAULT_STATUS.FaultStatusID            
      INNER JOIN E__EMPLOYEE On EMPLOYEEID = UserId       
      INNER JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentID    
      LEFT JOIN E__EMPLOYEE OP On OP.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID    
      LEFT JOIN FL_FAULT_TRADE ON FL_FAULT_TRADE.FaultTradeID = FL_FAULT_LOG.FaultTradeID    
      LEFT JOIN G_TRADE T ON FL_FAULT_TRADE.TradeId = T.TradeId    
      ' + CHAR(10)      
    
SET @FaultWhereClause = 'where  FL_FAULT_STATUS.FaultStatusID <>  13 AND FL_FAULT_LOG.PROPERTYID =''' + @propertyId + ''''      
+ CHAR(10)      
    
   -- PRINT '--===================================================================='  
   -- PRINT @FaultSelectClause + @FaultFromClause + @FaultWhereClause  
    

 -- PRINT '--===================================================================='  
  --PRINT @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause  


  --===========================================================================================              
--       STOCK CONDITION    
--===========================================================================================              
SET @StockConditionSelectClause = 'SELECT             
      0 as JournalHistoryId,            
      ''Inspection Completed'' As Status,            
      ''Stock Inspection'' as Action,            
      ''Stock Condition'' As InspectionType,            
      CONVERT(CHAR(10), PA_Property_Inspection_Record.CreatedDate, 103) As ''CreateDate'',            
      ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name  ,             	             
      PA_Property_Inspection_Record.CreatedDate as CREATIONDATE     
      ,''N/A'' AS REF     
      ,ISNULL((Convert( char, PA_Property_Inspection_Record.inspectionDate, 103 )),''N/A'') As AppointmentDate     
      ,ISNULL(( SUBSTRING(OP.FIRSTNAME,1,1) +'' '' + OP.lastname ),''N/A'') As OPERATIVENAME    
       ,''N/A'' As OPERATIVETRADE   
       ,InspectionId As JournalId   
       ,ISNULL(PS_Appointment.AppointNotes,'''') AS AppointmentNotes   
       ,''Stock Condition'' As InspectionTypeDescription   
       ,ISNULL(PS_Appointment.AppointId,-1) AS APPOINTMENTID 
      , AppointTitle AS [Description]           
      ,CASE 
		WHEN PA_Property_Inspection_Record.InspectionDocument IS NOT NULL Then 
			1 
		Else 
			0 
		END AS IsDocumentAttached,                        
      PA_Property_Inspection_Record.InspectionId  as CP12DocumentID
	  
	  ' + CHAR(10)      
      
SET @StockConditionFromClause = ' from PA_Property_Inspection_Record ' + CHAR(10) +      
   'LEFT JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = PA_Property_Inspection_Record.createdBy     
    LEFT JOIN PS_Appointment2Survey ON PS_Appointment2Survey.SurveyId = PA_Property_Inspection_Record.SurveyId    
    LEFT JOIN PS_Appointment ON PS_Appointment2Survey.AppointId = PS_Appointment.AppointId   
	LEFT JOIN AC_LOGINS ON  PS_Appointment.SurveyourUserName = AC_LOGINS.login 
    LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AC_LOGINS.EMPLOYEEID    
   '      
    
SET @StockConditionWhereClause = 'where PA_Property_Inspection_Record.PropertyId=''' + @propertyId + '''' + CHAR(10)   
  
 -- PRINT '--===================================================================='  
  --PRINT @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause 
  
----===========================================================================================            
       --Void Inspections Electric ACTIVITIES            
--===========================================================================================        
   
SET @VoidInspectionsSelectClause='SELECT  PDR_JOURNAL_HISTORY.JOURNALHISTORYID as JournalHistoryId,
	
	Case When PDR_MSATType.MSATTypeName = ''Void Works'' Then
		RequiredWorksStatus.Title
	Else 
		ISNULL(AH.APPOINTMENTSTATUS, ''N/A'')  
	End AS Status,


		COALESCE(V_RequiredWorks.workType, V_RequiredWorks.WorkDescription,''N/A'') AS Action,
		PDR_MSATType.MSATTypeName   AS InspectionType,
		Convert(varchar(10),PDR_JOURNAL_HISTORY.CREATIONDATE ,103) as CreateDate ,
		ISNULL(substring(EC.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(EC.lastname,'''')  AS Name ,
		AH.Loggeddate AS CREATIONDATE,
		Case When V_RequiredWorks.RequiredWorksId IS NULL Then 		
		ISNULL(( ''JSV'' + Convert( NVarchar, PDR_JOURNAL_HISTORY.JournalID )),''N/A'' )
		ELSE 
		ISNULL(( ''JSV'' + Convert( NVarchar, V_RequiredWorks.RequiredWorksId  )),''N/A'' )
		END
		 AS REF     
		  ,ISNULL(Convert( NVarchar, A.APPOINTMENTSTARTDATE, 103 ),''N/A'') As AppointmentDate     
		  ,ISNULL((SUBSTRING( OP.FIRSTNAME,1,1) +'' ''+OP.lastname),''N/A'') As OPERATIVENAME    
		  ,ISNULL(T.Description,''N/A'') As OPERATIVETRADE  
		  ,Case When V_RequiredWorks.RequiredWorksId IS NULL Then		  
		  PDR_JOURNAL_HISTORY.JOURNALID
		  ELSE
		  V_RequiredWorks.RequiredWorksId
		  END
		   As JournalId
		  ,ISNULL(A.APPOINTMENTNOTES,'''') AS  AppointmentNotes  
		  ,PDR_MSATType.MSATTypeName  AS InspectionTypeDescription
		  ,ISNULL(A.APPOINTMENTID,-1) AS APPOINTMENTID  
		   ,V_RequiredWorks.WorkDescription AS [Description]
		,0 AS IsDocumentAttached
		,0 AS CP12DocumentID
		   ' 

		--PDR_JOURNAL_HISTORY.JOURNALID'             
SET   @VoidInspectionsfromClause=' FROM PDR_JOURNAL_HISTORY  
		--INNER JOIN PDR_JOURNAL ON PDR_JOURNAL_HISTORY.JOURNALID= PDR_JOURNAL.JOURNALID  
		Left JOIN V_RequiredWorks ON PDR_JOURNAL_HISTORY.JOURNALID =  V_RequiredWorks.WorksJournalId
		LEFT JOIN PDR_STATUS RequiredWorksStatus ON RequiredWorksStatus.STATUSID=V_RequiredWorks.STATUSID
		INNER JOIN PDR_APPOINTMENT_HISTORY AH ON PDR_JOURNAL_HISTORY.JOURNALHISTORYID = AH.JOURNALHISTORYID      
		INNER JOIN PDR_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID    
		LEFT JOIN (	SELECT	COUNT(RequiredWorksId)as Works,InspectionJournalId 
					FROM	V_RequiredWorks 
					WHERE	(IsCanceled IS NULL OR IsCanceled=0) 
							AND (V_RequiredWorks.ISMAJORWORKSREQUIRED is null 
							OR V_RequiredWorks.ISMAJORWORKSREQUIRED =0)  
					GROUP BY InspectionJournalId) AS V_Works ON V_Works.InspectionJournalId=PDR_JOURNAL_HISTORY.JOURNALID  
		LEFT JOIN (	Select	COUNT(RequiredWorksId)as Arranged,InspectionJournalId 
					from	V_RequiredWorks 
					WHERE	IsScheduled=1  
							AND (IsCanceled IS NULL OR IsCanceled=0) 
							AND (V_RequiredWorks.ISMAJORWORKSREQUIRED is null 
							OR V_RequiredWorks.ISMAJORWORKSREQUIRED =0)
					GROUP BY InspectionJournalId ) AS V_Schedule ON V_Schedule.InspectionJournalId = PDR_JOURNAL_HISTORY.JOURNALID  
		INNER JOIN PDR_MSAT ON PDR_JOURNAL_HISTORY.MSATID = PDR_MSAT.MSATId  
		left JOIN E__EMPLOYEE ON AH.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
		LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AH.ASSIGNEDTO  
		INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId    
		INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID 
		INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DevelopmentId = PDR_DEVELOPMENT.DevelopmentId  
		LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = P__PROPERTY.SchemeId   
		LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID =P__PROPERTY.BLOCKID   
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId= C_TENANCY.TENANCYID  
		LEFT JOIN G_TRADE T ON AH.TRADEID = T.TRADEID
		LEFT JOIN E__EMPLOYEE EC ON AH.CREATEDBY=EC.EMPLOYEEID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID  
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and 
C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID 
		
		INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID=PDR_JOURNAL_HISTORY.STATUSID' + CHAR(10) 
		
		        
 SET  @VoidInspectionsWhereClause='WHERE PDR_MSAT.PROPERTYID = ''' + @propertyId + ''' and PDR_MSATType.MSATTypeName!= ''Void Works'''  + CHAR(10)             

--==================================== VOID WORKS  ================================================          

SET @VoidWorksSelectClause = ' SELECT  
  V_RequiredWorkshistory.RequiredWorksHistoryId as JournalHistoryId,
  ISNULL(PDR_STATUS.TITLE, ''N/A'')  AS Status,
  COALESCE(V_RequiredWorkshistory.workType, V_RequiredWorkshistory.WorkDescription,''N/A'') AS Action,
  PDR_MSATType.MSATTypeName   AS InspectionType,
  Convert(varchar(10),V_RequiredWorkshistory.CreatedDate ,103) as CreateDate ,
  ISNULL(substring(EC.FIRSTNAME,1,1),'' '') +'' ''+ ISNULL(EC.lastname,'' '')  AS Name,        
  COALESCE( V_RequiredWorkshistory.ModifiedDate,V_RequiredWorkshistory.CreatedDate) AS CREATIONDATE,
  ISNULL(( ''JSV'' + Convert( NVarchar, V_RequiredWorks.RequiredWorksId  )),''N/A'' ) AS REF     
  ,ISNULL(Convert( NVarchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103 ),''N/A'') As AppointmentDate     
  ,ISNULL((SUBSTRING( OP.FIRSTNAME,1,1) +'' ''+OP.lastname),''N/A'') As OPERATIVENAME    
  ,ISNULL(T.Description,''N/A'') As OPERATIVETRADE  
  ,V_RequiredWorks.RequiredWorksId As JournalId
  ,ISNULL(PDR_APPOINTMENTS.APPOINTMENTNOTES,'''') AS  AppointmentNotes  
  ,PDR_MSATType.MSATTypeName  AS InspectionTypeDescription
  ,ISNULL(PDR_APPOINTMENTS.APPOINTMENTID,-1) AS APPOINTMENTID  
  ,V_RequiredWorks.WorkDescription AS [Description]
  ,0 AS IsDocumentAttached
  ,0 AS CP12DocumentID '

  Set @VoidWorksfromClause = 'FROM V_RequiredWorkshistory
  INNER JOIN V_RequiredWorks ON V_RequiredWorkshistory.RequiredWorksId = V_RequiredWorks.RequiredWorksId
  INNER JOIN PDR_STATUS ON V_RequiredWorkshistory.StatusId = PDR_STATUS.STATUSID
  INNER JOIN PDR_JOURNAL ON V_RequiredWorkshistory.WorksJournalId = PDR_JOURNAL.JOURNALID
  INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
  INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTypeId = PDR_MSATTYPE.MSATTypeId
  INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
  LEFT JOIN E__EMPLOYEE EC ON PDR_APPOINTMENTS.CREATEDBY=EC.EMPLOYEEID
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO  
  LEFT JOIN G_TRADE T ON PDR_APPOINTMENTS.TRADEID = T.TRADEID'

  set @VoidWorksWhereClause = ' WHERE PDR_MSAT.PROPERTYID = ''' + @propertyId + ''' and PDR_MSATType.MSATTypeName = ''Void Works'' '



   
--==============================================================================================          
    
--Set union Clause            
SET @unionClause = CHAR(10) + CHAR(9) + 'UNION ALL' + CHAR(10) + CHAR(9)    
--========================================================================================              
--Set ORDER Clause      
SET @OrderClause = 'order by ' + @sortColumn + ' ' + @sortOrder + CHAR(10)   
IF @sortColumn = 'CREATIONDATE'  
 Begin  
  SET @OrderClause = @OrderClause + ', JournalHistoryId ' + @sortOrder + CHAR(10)   
 END  
--========================================================================================   
     
/* @actionType    
 -1 => All (Default)    
 1 => Appliance Servicing    
 2 => Reactive    
 3 => Stock    
 4 => Void    
 5 => Planned    
 6 => Condition Works   
 7 => M&E Servicing  
 8 => Cyclic Maintenance  
 9 => PAT Testing   
*/    
    
IF (@actionType = 1)     
  SET @mainSelectQuery = @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause 
						+@unionClause
						+@ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause
						+@OrderClause      
ELSE IF (@actionType = 2)    
 SET @mainSelectQuery = @FaultSelectClause + @FaultFromClause + @FaultWhereClause + @OrderClause    
ELSE IF (@actionType = 3)    
 SET @mainSelectQuery = @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause + @OrderClause   

ELSE IF (@actionType = 4)    
 SET @mainSelectQuery = @VoidInspectionsSelectClause + @VoidInspectionsfromClause + @VoidInspectionsWhereClause + @OrderClause + @unionClause +  @VoidWorksSelectClause + @VoidWorksfromClause + @VoidWorksWhereClause   
ELSE IF (@actionType = 5)    
 SET @mainSelectQuery = @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause +  @unionClause+ @PlannedSelectClause_AC + @PlannedFromClause_AC + @PlannedWhereClause_AC  + @OrderClause  
ELSE IF (@actionType = 6)    
 SET @mainSelectQuery = @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause + @OrderClause    
ELSE IF (@actionType = 7 or @actionType = 8 or @actionType = 9)    
 SET @mainSelectQuery = @PdrSelectClause + @PdrFromClause + @PdrWhereClause + @OrderClause   
ELSE 
SET @mainSelectQuery = 
 @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause    
 + @unionClause
 + @PlannedSelectClause_AC + @PlannedFromClause_AC + @PlannedWhereClause_AC
 + @unionClause 
 + @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause  
 + @unionClause
 + @ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause   
 + @unionClause    
 + @FaultSelectClause + @FaultFromClause + @FaultWhereClause    
 + @unionClause    
 + @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause    
 + @unionClause    
 + @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause    
 + @unionClause    
 + @PdrSelectClause + @PdrFromClause + @PdrWhereClause   
 + @unionClause    
 
 + @VoidInspectionsSelectClause + @VoidInspectionsfromClause + @VoidInspectionsWhereClause
 + @unionClause    
 + @VoidWorksSelectClause + @VoidWorksfromClause + @VoidWorksWhereClause  

 + @OrderClause    
   
--   PRINT '===================================================================='   
 PRINT @VoidWorksSelectClause + @VoidWorksfromClause + @VoidWorksWhereClause  
 EXEC (@mainSelectQuery)    
    
END

