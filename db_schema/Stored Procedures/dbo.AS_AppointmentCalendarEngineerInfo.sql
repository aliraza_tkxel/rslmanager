
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_AppointmentCalendarEngineerInfo
-- Author:		<Aqib Javed>
-- Create date: <23/11/2012>
-- Description:	<Description,Display the list of available Engineers>
-- Webpage : WeeklyCalendar.aspx,MonthlyCalendar.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_AppointmentCalendarEngineerInfo] 
AS
BEGIN
--Select DISTINCT E__EMPLOYEE.EMPLOYEEID ,E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME as Name from AS_APPOINTMENTS INNER JOIN E__EMPLOYEE ON AS_APPOINTMENTS.ASSIGNEDTO =E__EMPLOYEE.EMPLOYEEID 

Select DISTINCT E.EMPLOYEEID ,E.FIRSTNAME+' '+E.LASTNAME as Name,U.IsActive 
FROM dbo.AS_USER U
INNER JOIN dbo.E__EMPLOYEE E ON E.EMPLOYEEID=U.EmployeeId
INNER JOIN dbo.E_JOBDETAILS J ON J.EMPLOYEEID=E.EMPLOYEEID AND (J.ACTIVE=1 OR (J.ACTIVE=0 AND J.ENDDATE BETWEEN J.ENDDATE AND DATEADD(D,7,J.ENDDATE)))
LEFT JOIN AS_APPOINTMENTS AP ON AP.ASSIGNEDTO=E.EMPLOYEEID
WHERE U.UserTypeID = 3 AND U.IsActive  = 1

END
GO
