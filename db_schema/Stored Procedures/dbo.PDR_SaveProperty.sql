USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveProperty]    Script Date: 12/6/2016 2:29:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  ---=================================================================================
IF OBJECT_ID('dbo.[PDR_SaveProperty]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_SaveProperty] AS SET NOCOUNT ON;') 
GO 
ALTER PROCEDURE [dbo].[PDR_SaveProperty]

@DevelopmentId int,
@BlockId int,
@PhaseId int,
@heatingFuelValueId int,
@HouseNumber nvarchar(50),
@Address1 varchar(500),
@Address2 varchar(500),
@Address3 varchar(50),
@TownCity varchar(50),
@County varchar(50),
@PostCode nvarchar(10),
@Status int,
@SubStatus int,
@PropertyType int,
@AssetType int,
@StockType int,
@TenureType int, --new field
@PropertyLevel int,
@DateBuilt datetime= null,
@RightToBuy int,
@DefectsPeriodEnd smalldatetime= null,
@OwnerShip int,
@MinPurchase float,
@PurchaseLevel float,
@DwellingType int,
@NROSHAssetTypeMain int,
@NROSHAssetTypeSub int,
@IsTemplate bit,
@TemplateName nvarchar(200),
@LeaseStart smalldatetime= null,
@LeaseEnd smalldatetime= null,
@FloodingRisk bit,
@DateAcquired smalldatetime= null,
@viewingcommencement smalldatetime= null,
@GroundRent nvarchar(200) ,
@LandLord nvarchar(200) ,
@RentReview smalldatetime= null,
@ExistingPropertyID nvarchar(200)= null,
@TitleNumber nvarchar(200)= null,
@DeedLocation varchar(200)= null,
@updatedBy int,
@actualCompletion smalldatetime= null,
@handoverintoManagement smalldatetime= null,
@PropertyId nvarchar(200)= null output 	
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY 

	if (@PhaseId <> -1)
	begin
		UPDATE P_PHASE SET 
		ActualCompletion=@actualCompletion ,HandoverintoManagement=@handoverintoManagement 
		Where PHASEID  = @PhaseId
	end

	if (@PhaseId = -1)
	begin
		set @PhaseId =null
	end

	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	IF @ExistingPropertyID = '' OR @ExistingPropertyID IS NULL 
	BEGIN
			IF Not Exists(select top 1 * from P__Property Where(HOUSENUMBER = @HouseNumber OR FLATNUMBER=@HouseNumber)
		And ADDRESS1= @Address1 And TOWNCITY = @TownCity) 
			BEGIN

				Declare @PID nvarchar(20)

				SELECT @PID = convert(varchar(20),'BHA'+Convert(varchar(20),RIGHT('00000' + RTRIM(P_IDENTITY+1), 7))) FROM P_PROPERTY_IDENTITY 
				SELECT @PID
				SET NOCOUNT ON;

				INSERT INTO P__PROPERTY(PROPERTYID,DEVELOPMENTID ,BLOCKID ,HOUSENUMBER  ,ADDRESS1 ,ADDRESS2 ,ADDRESS3 ,TOWNCITY ,COUNTY ,POSTCODE ,STATUS ,SUBSTATUS ,
				PROPERTYTYPE ,ASSETTYPE ,STOCKTYPE ,PROPERTYLEVEL ,DATEBUILT   ,RIGHTTOBUY ,DEFECTSPERIODEND ,OWNERSHIP ,MINPURCHASE ,PURCHASELEVEL ,
				DWELLINGTYPE ,NROSHASSETTYPEMAIN ,NROSHASSETTYPESUB,IsTemplate ,TemplateName,PhaseId,LeaseStart,LeaseEnd,FloodingRisk,DateAcquired,
				viewingcommencement,GroundRent,LandLord,RentReview,TitleNumber,DeedLocation,TenureType)

				VALUES(@PID,@DevelopmentId ,@BlockId ,@HouseNumber  ,@Address1 ,@Address2 ,@Address3 ,@TownCity ,@County ,@PostCode ,@Status ,@SubStatus ,@PropertyType ,
				@AssetType ,@StockType ,@PropertyLevel ,@DateBuilt  ,@RightToBuy ,@DefectsPeriodEnd ,@OwnerShip ,@MinPurchase ,@PurchaseLevel ,
				@DwellingType ,@NROSHAssetTypeMain ,@NROSHAssetTypeSub ,@IsTemplate ,@TemplateName,@PhaseId,@LeaseStart,@LeaseEnd,@FloodingRisk,@DateAcquired,
				@viewingcommencement,@GroundRent,@LandLord,@RentReview,@TitleNumber,@DeedLocation,@TenureType)
				SET @PropertyId = @PID

				--- Update P_PROPERTY_IDENTITY table
				UPDATE P_PROPERTY_IDENTITY SET P_IDENTITY = (SELECT P_IDENTITY + 1 FROM P_PROPERTY_IDENTITY)
		   END
		   ELSE
			   BEGIN
			   SET @PropertyId='Exist'
			   END
	END
ELSE
	BEGIN
	UPDATE P__PROPERTY SET 
	DEVELOPMENTID=@DevelopmentId ,BLOCKID=@BlockId ,HOUSENUMBER=@HouseNumber  ,ADDRESS1=@Address1 ,ADDRESS2 =@Address2,ADDRESS3=@Address3 ,TOWNCITY=@TownCity ,COUNTY=@County ,POSTCODE=@PostCode ,STATUS=@Status ,SUBSTATUS=@SubStatus ,
	PROPERTYTYPE=@PropertyType ,ASSETTYPE=@AssetType ,STOCKTYPE=@StockType ,PROPERTYLEVEL=@PropertyLevel ,DATEBUILT=@DateBuilt,RIGHTTOBUY=@RightToBuy ,DEFECTSPERIODEND=@DefectsPeriodEnd ,OWNERSHIP=@OwnerShip ,MINPURCHASE=@MinPurchase ,PURCHASELEVEL=@PurchaseLevel ,
	DWELLINGTYPE=@DwellingType ,NROSHASSETTYPEMAIN=@NROSHAssetTypeMain ,NROSHASSETTYPESUB=@NROSHAssetTypeSub ,IsTemplate=@IsTemplate ,TemplateName=@TemplateName,PhaseId=@PhaseId,LeaseStart=@LeaseStart,LeaseEnd=@LeaseEnd,FloodingRisk=@FloodingRisk,DateAcquired=@DateAcquired,
	viewingcommencement=@viewingcommencement,GroundRent=@GroundRent,LandLord=@LandLord,RentReview=@RentReview,TitleNumber = @TitleNumber,DeedLocation = @DeedLocation, TenureType = @TenureType
	Where PROPERTYID = @ExistingPropertyID
	
	SET @PropertyId=@ExistingPropertyID
	END	
	
	IF @heatingFuelValueId > 0 AND @PropertyId <> 'Exist'
	BEGIN
	----Declare variables
	DECLARE @itemParamId int, @valueDetail nvarchar(200) 
	
	SELECT @itemParamId = ItemParamID FROM PA_ITEM_PARAMETER
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
	INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
	WHERE ParameterName = 'Heating Fuel' AND ItemName = 'Heating' and PA_ITEM.IsActive=1	
	
	SELECT @valueDetail=ValueDetail from PA_PARAMETER_VALUE where ValueID=@heatingFuelValueId
	
	IF Not EXISTS(SELECT * FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID=@PropertyId AND ItemParamID =@itemParamId) 
		BEGIN
			INSERT INTO PA_PROPERTY_ATTRIBUTES(PROPERTYID,ITEMPARAMID, PARAMETERVALUE, VALUEID, UPDATEDON, UPDATEDBY, IsCheckBoxSelected, IsUpdated)	
			VALUES(@PropertyId,@itemParamId,@valueDetail,@heatingFuelValueId,GETDATE(),@updatedBy,0,1)
		END
	ELSE
		BEGIN 
			UPDATE PA_PROPERTY_ATTRIBUTES Set VALUEID = @heatingFuelValueId,PARAMETERVALUE = @valueDetail 
			WHERE PROPERTYID=@PropertyId AND ItemParamID =@itemParamId		 
		END	
	END
	
	--Ticket # 10777
	IF Not EXISTS(SELECT * FROM AS_JOURNAL WHERE PROPERTYID = @PropertyId AND ISCURRENT = 1)
		BEGIN
			DECLARE @TEMP_STATUSID int, @TEMP_ACTIONID int,@TEMP_INSPECTIONTYPEID int
			
			SELECT TOP 1 @TEMP_STATUSID = STATUSID FROM AS_Status WHERE TITLE = 'Appointment to be arranged'
			SELECT TOP 1 @TEMP_ACTIONID = ACTIONID FROM AS_Action WHERE Title = 'Appointment to be arranged '
			SELECT TOP 1 @TEMP_INSPECTIONTYPEID = INSPECTIONTYPEID FROM P_INSPECTIONTYPE WHERE DESCRIPTION = 'Appliance Servicing'
						
			INSERT INTO AS_JOURNAL(PROPERTYID,STATUSID,ACTIONID,INSPECTIONTYPEID,CREATIONDATE,CREATEDBY,ISCURRENT)
			VALUES (@PROPERTYID,@TEMP_STATUSID,@TEMP_ACTIONID,@TEMP_INSPECTIONTYPEID,GETDATE(),@updatedBy,1)
		END
	
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @PropertyId='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;
		
	END 
END
