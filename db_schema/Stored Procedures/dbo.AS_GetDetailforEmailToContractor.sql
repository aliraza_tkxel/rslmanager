USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetDetailforEmailToContractor]    Script Date: 12/22/2016 12:34:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 18/4/2016
-- Description:	To get detail to send email to contractor, details are: contractor details, property details, tenant details, tenant risk details
-- Web Page:	RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx

/*

EXEC	[dbo].[AS_GetDetailforEmailToContractor]
		@journalId = 6545,
		@propertyId = N'''''',
		@empolyeeId = 943,
		@ContractorId = 1270
*/
-- =============================================

IF OBJECT_ID('dbo.[AS_GetDetailforEmailToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetDetailforEmailToContractor] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[AS_GetDetailforEmailToContractor]
	@journalId INT
	,@propertyId NVARCHAR(20)
	,@empolyeeId INT
	,@ContractorId INT
	,@contractorsContactIdParam INT
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @contractorsContactIdParam

--=================================================
--Get Property Detail(s)
--=================================================
SELECT
	ISNULL(P.PROPERTYID,'') PROPERTYID,
	ISNULL(HOUSENUMBER,'') HOUSENUMBER,
	ISNULL(FLATNUMBER,'') FLATNUMBER,
	ISNULL(ADDRESS1,'') ADDRESS1,
	ISNULL(ADDRESS2,'') ADDRESS2,
	ISNULL(ADDRESS3,'') ADDRESS3,
	ISNULL(TOWNCITY,'') TOWNCITY,
	ISNULL(COUNTY,'') COUNTY,
	ISNULL(POSTCODE,'') POSTCODE,
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, '') + ISNULL(' ' + TOWNCITY, '')
	+ ISNULL(', ' + COUNTY, '') + ISNULL(', ' + POSTCODE, ''), ''), 'N/A') AS [FullAddress],
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress],
	 CASE WHEN EXISTS(Select ASBESTOSID from P_PROPERTY_ASBESTOS_RISKLEVEL ARL WHERE ARL.PROPERTYID = P.PROPERTYID ) Then 
	 1 
	 ELSE
	 0
	 END AS AsbestosExists
FROM P__PROPERTY P
WHERE P.PROPERTYID = @propertyId

--=================================================
-- Get CustomerId to get Tenant Detail(s) and Risk
--=================================================

CREATE TABLE #Customers(CUSTOMERID INT)

INSERT INTO #Customers(CUSTOMERID)
SELECT CT.CUSTOMERID
FROM C_TENANCY T
INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID 
	AND (CT.ENDDATE IS NULL OR CT.ENDDATE > CAST(GETDATE() AS DATE))
	AND (T.ENDDATE IS NULL OR T.ENDDATE > CAST(GETDATE() AS DATE))
WHERE T.PROPERTYID = @propertyId
ORDER BY CT.CUSTOMERTENANCYID ASC

--=================================================
--Get Customer/Tenant Detail(s)
--=================================================

SELECT TOP 1
	ISNULL(C.CUSTOMERID,'') AS [CUSTOMERID],
	ISNULL(FIRSTNAME, '') AS [FIRSTNAME],
	ISNULL(MIDDLENAME, '') AS [MIDDLENAME],
	ISNULL(LASTNAME, '') AS [LASTNAME],
	ISNULL(TEL, '') AS [TEL],
	ISNULL(T.DESCRIPTION, '') [TITLE],
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '') + ISNULL(' ' + C.MIDDLENAME, '')
	+ ISNULL(' ' + C.LASTNAME, '') + ISNULL(', Tel:' + A.TEL, ''), ''), 'N/A') AS ContactDetail,
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '')
	+ ISNULL(' ' + C.LASTNAME, ''), ''), 'N/A') [FullName]
FROM #Customers TC
INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = TC.CUSTOMERID
INNER JOIN G_TITLE T
	ON C.TITLE = T.TITLEID
INNER JOIN C_ADDRESS A
	ON C.CUSTOMERID = A.CUSTOMERID
	AND A.ISDEFAULT = 1
	
--=================================================
-- Create temp tables to get Customer/Tenant Risk and Vulnerability Detail(s)
--=================================================

CREATE TABLE #RISKS (CATDESC NVARCHAR(60), SUBCATDESC NVARCHAR(60))
CREATE TABLE #VULNERABILITY (CATDESC NVARCHAR(50), SUBCATDESC NVARCHAR(140))

DECLARE @CustomerId INT = -1

SELECT @CustomerId = MIN(CUSTOMERID) FROM #Customers WHERE CUSTOMERID > @CustomerId
WHILE (@CustomerId IS NOT NULL)
BEGIN

--=================================================
--Fill Customer/Tenant Risk Detail(s) in temp tables
--=================================================
INSERT INTO #RISKS
SELECT	
	CATDESC,
	SUBCATDESC
FROM RISK_CATS_SUBCATS(@CustomerId)

--=================================================
--Fill Customer/Tenant Vulnerability Detail(s)  in temp table
--=================================================
DECLARE @VULNERABILITYHISTORYID int = 0

SELECT
	@VULNERABILITYHISTORYID = VULNERABILITYHISTORYID
FROM C_JOURNAL J
INNER JOIN C_VULNERABILITY CV
	ON CV.JOURNALID = J.JOURNALID
WHERE CUSTOMERID = @customerId
AND ITEMNATUREID = 61
AND CV.ITEMSTATUSID <> 14
AND CV.VULNERABILITYHISTORYID = (SELECT
	MAX(VULNERABILITYHISTORYID)
FROM C_VULNERABILITY IN_CV
WHERE IN_CV.JOURNALID = J.JOURNALID)
  
INSERT INTO #VULNERABILITY
EXECUTE VULNERABILITY_CAT_SUBCAT @VULNERABILITYHISTORYID  


SELECT @CustomerId = MIN(CUSTOMERID) FROM #Customers WHERE CUSTOMERID > @CustomerId
END

--=================================================
--Get Customer/Tenant Risk Detail(s)
--=================================================
SELECT DISTINCT * FROM #RISKS

--=================================================
--Get Customer/Tenant Vulnerability Detail(s)
--=================================================
SELECT DISTINCT * FROM #VULNERABILITY

--================================================
-- Get Ordered Person Details 
--================================================

--SELECT ISNULL( C.WORKDD,'N/A') AS DDial,ISNULL(C.WORKEMAIL, 'N/A') AS EMAIL  ,
--ACW.PurchaseOrder AS OrderId, 
--(ISNULL(E.FIRSTNAME,' ') + ISNULL(' ' + E.LASTNAME,' ')) AS OrderedBy
--FROM AS_CONTRACTOR_WORK ACW
--INNER JOIN E_CONTACT C ON C.EMPLOYEEID = ACW.AssignedBy
--INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID =   ACW.AssignedBy
--Where ACW.ContractorId = @ContractorId
SELECT 
ISNULL(ISNULL(EMPLOYEE.FIRSTNAME,'')+ ISNULL(EMPLOYEE.MIDDLENAME,'')+ ISNULL(EMPLOYEE.LASTNAME,''),'N/A') AS OrderedBy,
ISNULL(CONTACT.WORKDD,'N/A') AS DDial,
ISNULL(CONTACT.WORKEMAIL,'N/A') AS Email
FROM	E_CONTACT CONTACT INNER JOIN E__EMPLOYEE EMPLOYEE ON CONTACT.EMPLOYEEID = EMPLOYEE.EMPLOYEEID
WHERE CONTACT.EMPLOYEEID = @empolyeeId


--================================================
-- Get Property Absestos Detail.
--================================================

 SELECT RL.ASBRISKLEVELDESCRIPTION,A.RISKDESCRIPTION
 FROM P__PROPERTY P
 INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ARL ON P.PROPERTYID = ARL.PROPERTYID  
 INNER JOIN P_PROPERTY_ASBESTOS_RISK AR ON ARL.PROPASBLEVELID = AR.PROPASBLEVELID  
 INNER JOIN P_ASBESTOS A ON ARL.ASBESTOSID = A.ASBESTOSID
 INNER JOIN P_ASBRISKLEVEL RL ON ARL.ASBRISKLEVELID = RL.ASBRISKLEVELID  
 WHERE P.PROPERTYID = @propertyId  and (ARL.DateRemoved is null or CONVERT(DATE,ARL.DateRemoved) > convert(date, getdate()) )

--=================================================
--Remove temp tables
--=================================================

DROP TABLE #RISKS
DROP TABLE #VULNERABILITY
DROP TABLE #Customers

END
