SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE  PROCEDURE [dbo].[TO_ENQUIRY_LOG_SelectArrearDetail]

/* ===========================================================================
 '   NAME:          TO_ENQUIRY_LOG_SelectArrearDetail
 '   DATE CREATED:    1 JULY 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select one TO_ENQUIRY_LOG record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT     EnquiryLog.Description AS NOTES
FROM                TO_ENQUIRY_LOG EnquiryLog 
WHERE     (EnquiryLog.EnquiryLogID = @enqLogID)
GO
