SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_CUSTOMER_RESPONSE_CREATEJOURNAL]
	/*	===============================================================
	'   NAME:           TO_CUSTOMER_RESPONSE_CREATEJOURNAL
	'   DATE CREATED:   20 SEP 2008
	'   CREATED BY:     Naveed Iqbal
	'   CREATED FOR:    Enquiry Management
	'   PURPOSE:        To update customer response details
	'   IN:             @journalId ,@responseId ,@nature ,@responseNotes 	
	'   OUT: 	        @result
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	
	(
	@enquiryLogId int,
	@journalId int,
	@responseId int,
	@nature int,
	@responseNotes nvarchar(4000),
	@result int OUTPUT	
	)
AS

	DECLARE @itemStatusId int,
			@itemActionId int,
			@lastActionDate smallDateTime ,
			@lastActionUser int,
			@terminationDate smallDateTime,	
			@reason nvarchar(2000),
			@tTimeStamp smallDateTime,
			@team int,
			@assignTo int,
			@escalationLevel int,
			@category int,
			@escalationDate smallDateTime,
			@resolutionReached int,
			@contactType int,
			@movingBecause varchar(255),
			@localAuthorityId int,
			@developmentId int,
			@noofBedRooms int,
			@occupantsAbove18 int,
			@occupantsBelow18 int,
			@lookupValueId int,
			@additionalInfo varchar(255),
			@location varchar(255),
			@otherInfo varchar(255),
			@courtDate smallDateTime,
			@possessionDate smallDateTime,
			@requestNatureId int,
			@Description varchar(255)			
	SET @lastActionDate= CONVERT(DATETIME, GETDATE(), 103)
	SET @lastActionUser = NULL
		
if @nature = 50/*In case of Termination(TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@terminationDate=TERMINATIONDATE,
	@reason=REASON,
	@tTimeStamp=TTIMESTAMP 
	FROM C_TERMINATION
	WHERE TERMINATIONHISTORYID = (
	SELECT MAX (TERMINATIONHISTORYID) 
	FROM C_TERMINATION 
	WHERE JOURNALID = @journalId
	)
	
	BEGIN TRAN

	INSERT INTO C_TERMINATION(JOURNALID, NOTES, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, 
TERMINATIONDATE, REASON, TTIMESTAMP )
	VALUES(@journalId, @responseNotes, @itemStatusId, @itemActionId, @lastActionDate, @lastActionUser, 
@terminationDate, @reason, @tTimeStamp)	

	SELECT @result = TERMINATIONHISTORYID
	FROM C_TERMINATION
	WHERE TERMINATIONHISTORYID = SCOPE_IDENTITY()

	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END	
END	

ELSE IF @nature = 51/*In case of Service Complaints (TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@team = TEAM,
	@assignTo = ASSIGNTO,
	@escalationLevel = ESCALATIONLEVEL,
	@category = CATEGORY,
	@escalationDate = ESCALATIONDATE,
	@resolutionReached = RESOLUTIONREACHED		
	FROM C_SERVICECOMPLAINT
	WHERE COMPLAINTHISTORYID = (
	SELECT MAX (COMPLAINTHISTORYID) 
	FROM C_SERVICECOMPLAINT 
	WHERE JOURNALID = @journalId
	)
	
	BEGIN TRAN

	INSERT INTO C_SERVICECOMPLAINT
(JOURNALID,NOTES,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,TEAM,ASSIGNTO,ESCALATIONLEVEL,CATEGORY,ESCALATIONDATE,RESOLUTIONREACHED )
	VALUES
(@journalId,@responseNotes,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@team,@assignTo,@escalationLevel,@category,@escalationDate,@resolutionReached)	

	SELECT @result = COMPLAINTHISTORYID
	FROM C_SERVICECOMPLAINT
	WHERE COMPLAINTHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
	
END	

ELSE IF @nature = 52/*In case of Garage/Car Parking (TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@lookupValueId = LOOKUPVALUEID,
	@additionalInfo = ADDITIONALINFO		
	FROM C_PARKING_SPACE
	WHERE PARKINGSPACEHISTORYID = (
	SELECT MAX (PARKINGSPACEHISTORYID) 
	FROM C_PARKING_SPACE 
	WHERE JOURNALID = @journalId
	)
	
	BEGIN TRAN


	INSERT INTO C_PARKING_SPACE
    (JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,LOOKUPVALUEID,ADDITIONALINFO )
	VALUES(@journalId,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@lookupValueId,@responseNotes)	

	SELECT @result = PARKINGSPACEHISTORYID
	FROM C_PARKING_SPACE
	WHERE PARKINGSPACEHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
	
END	

ELSE IF @nature = 53/*In case of ASB - Complainant (TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@assignTo = ASSIGNTO,
	@contactType = CONTACTTYPE,
	@category = CATEGORY
	FROM C_ASB
	WHERE ASBHISTORYID = (
	SELECT MAX (ASBHISTORYID) 
	FROM C_ASB 
	WHERE JOURNALID = @journalId
	)
	
	BEGIN TRAN

	INSERT INTO C_ASB(JOURNALID,NOTES,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,ASSIGNTO,CATEGORY 
,CONTACTTYPE)
	VALUES
(@journalId,@responseNotes,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@assignTo,@category,@contactType)	

	SELECT @result = ASBHISTORYID
	FROM C_ASB
	WHERE ASBHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
	
END	

ELSE IF @nature = 54/*In case of Abandoned Property/Vehicle (TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@lookupValueId = LOOKUPVALUEID,
	@location = LOCATION,
	@otherInfo = OTHERINFO
	FROM C_ABANDONED
	WHERE AbandonedHistoryID = (
	SELECT MAX (AbandonedHistoryID) 
	FROM C_ABANDONED 
	WHERE JOURNALID = @journalId
	)
	
	BEGIN TRAN

	SELECT * FROM C_ABANDONED

	INSERT INTO C_ABANDONED
(JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,LOOKUPVALUEID,LOCATION,OTHERINFO)
	VALUES(@journalId,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@lookupValueId,@location,@responseNotes)	

	SELECT @result = AbandonedHistoryID
	FROM C_ABANDONED
	WHERE AbandonedHistoryID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
END	

ELSE IF @nature = 55/*In case of House Move (TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@movingBecause=MOVINGBECAUSE,
	@localAuthorityId=LOCALAUTHORITYID,
	@developmentId=DEVELOPMENTID,
	@noofBedRooms=NOOFBEDROOMS,
	@occupantsAbove18=OCCUPANTSABOVE18,
	@occupantsBelow18=OCCUPANTSBELOW18
	FROM C_HOUSE_MOVE
	WHERE HOUSEMOVEHISTORYID = (
	SELECT MAX (HOUSEMOVEHISTORYID) 
	FROM C_HOUSE_MOVE 
	WHERE JOURNALID = @journalId
	)
	
	BEGIN TRAN

	INSERT INTO C_HOUSE_MOVE
(JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,MOVINGBECAUSE,LOCALAUTHORITYID,DEVELOPMENTID,NOOFBEDROOMS,OCCUPANTSABOVE18,OCCUPANTSBELOW18)
	VALUES
(@journalId,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@movingBecause,@localAuthorityId,@developmentId,@noofBedRooms,@occupantsAbove18,@occupantsBelow18)	

	SELECT @result = HOUSEMOVEHISTORYID
	FROM C_HOUSE_MOVE
	WHERE HOUSEMOVEHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END	
END	

ELSE IF @nature = 56/*In case of Arrears*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@assignTo = ASSIGNTO,
	@courtDate = COURTDATE,
	@possessionDate = POSSESSIONDATE
	FROM C_ARREARS
	WHERE ARREARSHISTORYID = (
	SELECT MAX (ARREARSHISTORYID) 
	FROM C_ARREARS 
	WHERE JOURNALID = @journalId
	)		
		
	BEGIN TRAN
	
	INSERT INTO C_ARREARS
(JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,NOTES,ASSIGNTO,COURTDATE,POSSESSIONDATE)
	VALUES(@journalId,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@responseNotes,@assignTo,@courtDate,@possessionDate)	

	SELECT @result = ARREARSHISTORYID
	FROM C_ARREARS
	WHERE ARREARSHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
END	

ELSE IF @nature = 57/*In case of Rent*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@assignTo = ASSIGNTO,
	@contactType = CONTACTTYPE
	FROM C_GENERAL
	WHERE GENERALHISTORYID = (
	SELECT MAX (GENERALHISTORYID) 
	FROM C_GENERAL 
	WHERE JOURNALID = @journalId
	)		
		
	BEGIN TRAN
	
	INSERT INTO C_GENERAL
(JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,NOTES,ASSIGNTO,CONTACTTYPE)
	VALUES(@journalId,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@responseNotes,@assignTo,@contactType)	

	SELECT @result = GENERALHISTORYID
	FROM C_GENERAL
	WHERE GENERALHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
END	

ELSE IF @nature = 58/*In case of CST Request (TO)*/
BEGIN

	SELECT 	@itemStatusId=ITEMSTATUSID,
	@itemActionId=ITEMACTIONID,
	@requestNatureId = REQUESTNATUREID,
	@Description = DESCRIPTION	
	FROM C_CST_REQUEST
	WHERE CSTREQUESTEHISTORYID = (
	SELECT MAX (CSTREQUESTEHISTORYID) 
	FROM C_CST_REQUEST 
	WHERE JOURNALID = @journalId
	)		
		
	BEGIN TRAN
	
	INSERT INTO C_CST_REQUEST
(JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,REQUESTNATUREID,DESCRIPTION)
	VALUES(@journalId,@itemStatusId,@itemActionId,@lastActionDate,@lastActionUser,@requestNatureId,@responseNotes)	

	SELECT @result = CSTREQUESTEHISTORYID
	FROM C_CST_REQUEST
	WHERE CSTREQUESTEHISTORYID = SCOPE_IDENTITY()
	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	END
END

IF @result > 0
BEGIN
	INSERT INTO TO_RESPONSE_LOG(EnquiryLogID, ReadFlag, ServiceId)
	VALUES(@enquiryLogId, 1, @result)
	
	SELECT @result = ResponseLogID
	FROM TO_RESPONSE_LOG
	WHERE ResponseLogID = SCOPE_IDENTITY()

	BEGIN
	IF @result < 0
		BEGIN
			SET @result = -1
			ROLLBACK TRAN
		END
	ELSE
		COMMIT TRAN
	END	
END

GO
