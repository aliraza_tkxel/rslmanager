USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_RechargesToSalesInvoice]    Script Date: 09-Nov-16 3:07:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Junaid Nadeem
-- Create date: <Create Date,01 December 2016,>
-- Description:	<Create sales Invoice for void work recharges>
-- =============================================


IF OBJECT_ID('dbo.F_RechargesToSalesInvoice') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_RechargesToSalesInvoice AS SET NOCOUNT ON;') 
GO

 ALTER PROCEDURE dbo.F_RechargesToSalesInvoice
    @SOTYPE INT = 1, 
    @TENANCYID INT , 
    @SODATE SMALLDATETIME,
    @USERID INT,
	@ACTIVE Bit = 1,
	@SOSTATUS INT = 1,
	@SONOTES VARCHAR(100),
    @SONAME VARCHAR(100)

AS 
BEGIN 
   
INSERT INTO F_SALESINVOICE (SONAME, SODATE, SONOTES, USERID, TENANCYID, ACTIVE, SOTYPE, SOSTATUS)
VALUES (@SONAME, @SODATE, @SONOTES, @UserID, @TENANCYID, @ACTIVE, @SOTYPE, @SOSTATUS)

END 