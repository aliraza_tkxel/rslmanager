SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_CO_SAVE_REPAIR_AGAINST_FAULT]
	/*	===============================================================
	'   NAME:           FL_CO_SAVE_REPAIR_AGAINST_FAULT
	'   DATE CREATED:   02Jan,2009
	'   CREATED BY:     Noor Muhammad
	'   CREATED FOR:    RSL
	'   PURPOSE:        To save the repair ids against fault log record
	'   IN:             @RepairId
	'   IN:             @FaultLogId
	'   IN:             @Qty
	'
	'   OUT:           Nothing
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@RepairId INT,
		@FaultLogId INT,
		@Qty INT,
		@Result INT OUTPUT 
		
	)
AS
	Declare @FaultLogToRepairID INT 
	
	BEGIN
		
		
		SET NOCOUNT ON
		Begin Transaction 
		
		INSERT INTO FL_CO_FAULTLOG_TO_REPAIR 
			(FaultLogID, FaultRepairListID,Quantity) 
		VALUES(@FaultLogId,@RepairId,@Qty) 
		
		SELECT @FaultLogToRepairID=FaultLogToRepairID
		FROM FL_CO_FAULTLOG_TO_REPAIR
		WHERE FaultLogToRepairID = @@IDENTITY
		
		IF @FaultLogToRepairID <0
		Begin
			Rollback Transaction
			SET @Result=-1
			Print 'Unable to update the record'
		End
		ELSE
		Begin					
			SET @Result=1
			COMMIT TRAN
		END			

	END








GO
