SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetDefectCategory
-- Author:		<Salman Nazir>
-- Create date: <06/11/2012>
-- Description:	<Get Defects from P_DEFECTS_CATEGORY>
-- Web Page: PropertyRecord.aspx => Add Defect Tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetDefectCategory]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   	SELECT * 
	FROM P_DEFECTS_CATEGORY
END
GO
