SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,01/07/2014>
-- Description:	<Description,,Get the pages of employee.>
-- Web Page: JobRoleAccess.Master
-- =============================================
CREATE PROCEDURE [dbo].[JRA_CheckEmployeePageAccess](
@employeeId int
,@pageName nvarchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
AC_PAGES.DESCRIPTION AS PageName
,AC_MODULES.DIRECTORY +'/'+AC_PAGES.PAGE AS PageNamePath
,AC_PAGES.PAGEID as PageId
,AC_PAGES.MENUID 
FROM E__EMPLOYEE
INNER JOIN AC_PAGES_ACCESS on AC_PAGES_ACCESS.JobRoleTeamId = E__EMPLOYEE.JobRoleTeamId
INNER JOIN AC_PAGES on AC_PAGES.PAGEID = AC_PAGES_ACCESS.PageId
INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID 
INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID 
Where E__EMPLOYEE.EMPLOYEEID = @employeeId AND AC_PAGES.ACTIVE = 1 AND AC_PAGES.DESCRIPTION = @pageName


END
GO
