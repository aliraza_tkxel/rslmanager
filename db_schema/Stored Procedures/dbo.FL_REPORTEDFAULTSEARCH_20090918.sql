SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






create PROCEDURE [dbo].[FL_REPORTEDFAULTSEARCH_20090918] 

/* ===========================================================================
 '   NAME:           FL_REPORTEDFAULTSEARCH
 '   DATE CREATED:   24 Dec 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist reported faults based on search criteria provided
 '   IN:            @locationId, @areaId, @elementId, @priorityId, @status,
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	
	(
	    -- These Parameters are passed as Search Criteria
	    @firstName	varchar(50)	=  NULL,
		@lastName	varchar(50)	=  NULL,
		
		@tenancyID	float = NULL,
		@nature		int = NULL,
		@status		int = NULL,    
		
		@text		varchar(4000)    = NULL,
		
		@date		nvarchar(200)	=  NULL,
	
		
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @firstName IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'C__CUSTOMER.FIRSTNAME LIKE ''' + '%' +  @firstName + '%' + '''' +  '  AND'  
    
    
     IF @lastName IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'C__CUSTOMER.LASTNAME LIKE ''' + '%'  +@lastName + '%' + ''''  + ' AND'  
    
    
    IF @tenancyID IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_JOURNAL.TENANCYID = '+ LTRIM(STR(@tenancyID)) + ' AND'  
    
    IF @nature IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'C_NATURE.ITEMNATUREID = '+ LTRIM(STR(@nature)) + ' AND'  
                             
    IF @status IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@status)) + ' AND'  
                             
    IF @text IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_FAULT.Description LIKE ''%'+ @text + '%'''+ ' AND'  
    
    IF @date IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
		 'CONVERT(varchar, FL_FAULT_LOG.SubmitDate, 103)  = ''' +  CONVERT(varchar,@date, 103) + '''  AND'  
		-- 'FL_FAULT_LOG.SubmitDate = ' + CONVERT(varchar, @date, 103)  + ' AND' 
           
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT' + 
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID,CONVERT(varchar, FL_FAULT_LOG.DueDate, 103) as DueDate, FL_FAULT_LOG.CustomerID,FL_FAULT_LOG.FaultID,FL_FAULT_LOG.FaultBasketID, CONVERT(varchar, FL_FAULT_LOG.SubmitDate, 103) as SubmitDate,FL_FAULT_LOG.ORGID,FL_FAULT_LOG.StatusID,FL_FAULT_LOG.Quantity, ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.Description,FL_FAULT.Recharge,FL_FAULT.NetCost,FL_FAULT.PreInspection,' +
                        CHAR(10) + CHAR(9) + 'C__CUSTOMER.CUSTOMERID,ISNULL(C__CUSTOMER.FIRSTNAME,'''') AS FIRSTNAME, C__CUSTOMER.LASTNAME, '+
                        CHAR(10) + CHAR(9) + 'FL_ELEMENT.ElementName, '+ 
                        CHAR(10) + CHAR(9) + 'FL_AREA.AreaName, ' +  
                        CHAR(10) + CHAR(9) + '(CONVERT(VARCHAR, FL_FAULT_PRIORITY.ResponseTime)+' + ' ' + 'CASE FL_FAULT_PRIORITY.Days WHEN 1 THEN  '+'''Day(s)'''+ ' WHEN 0 THEN '+'''Hour(s)'''+ 'END) as TimeFrame , ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.DESCRIPTION as FaultStatus, '+ 
                        CHAR(10) + CHAR(9) + '(CASE WHEN S_ORGANISATION.NAME is NULL then '+'''TBA'''+'WHEN S_ORGANISATION.NAME IS NOT NULL then S_ORGANISATION.NAME END ) as NAME,' + 
	           CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.JobSheetNumber as JS_Number, Convert(varchar, FL_FAULT_PREINSPECTIONINFO.InspectionDate, 103) as AppointmentDate ' 

--, 
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    
     SET @FromClause =CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG INNER JOIN C__CUSTOMER ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID ' +
	        CHAR(10) + CHAR(9) + 'LEFT JOIN FL_FAULT_PREINSPECTIONINFO ' + 
	        CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultLogID = FL_FAULT_PREINSPECTIONINFO.faultlogid ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_STATUS ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_JOURNAL ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FAULTLOGID ' +
                     -- CHAR(10) + CHAR(9) + 'INNER JOIN  ' +
                      --CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG_HISTORY.JOURNALID = FL_FAULT_JOURNAL.JOURNALID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C_NATURE ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_JOURNAL.ITEMNATUREID = C_NATURE.ITEMNATUREID ' +
                      CHAR(10) + CHAR(9) + 'LEFT JOIN S_ORGANISATION ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_ELEMENT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.ElementID = FL_ELEMENT.ElementID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PRIORITY ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_AREA ' +
                      CHAR(10) + CHAR(9) + 'ON FL_ELEMENT.AreaID = FL_AREA.AreaID '
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.FaultLogID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FL_FAULT_LOG.FaultLogID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FL_FAULT_LOG.FaultLogID NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FL_FAULT_LOG.FaultLogID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'AND'+ @SearchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	

    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)

  PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)




GO
