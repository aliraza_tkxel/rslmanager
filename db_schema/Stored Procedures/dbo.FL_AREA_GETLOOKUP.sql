SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_AREA_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_AREA_GETLOOKUP
 '   DATE CREATED:   16 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Location FL_AREA table which will be shown as lookup value in presentation pages
 '   IN:            @LocationID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@LocationID as Int
AS
	SELECT AreaID AS id,AreaName AS val
	FROM FL_AREA
	WHERE LocationID=@LocationID
	ORDER BY AreaName ASC













GO
