SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  procedure [dbo].[STP_PRINT_SUMMARY]

@JOBID int

AS

SELECT RC.COMMENTS, dbo.FinalCompletionDate(WD.StartDate,WD.Days) as CompletionDay, wd.EstCompDate, DS.DESCRIPTION AS SYSTEM, RJ.JOB_ID, RJ.REQUEST_TITLE, 
RJ.COMPLETION_DATE, RJ.DATE_ENTERED, RJ.AREA, RJ.CONTENT, 
CONVERT(VARCHAR, RJ.DATE_ENTERED, 103) + ' ' + LEFT(CONVERT(VARCHAR, RJ.DATE_ENTERED, 108), 5) AS FORMATTEDDATE, 
RT.DESCRIPTION AS TYPEDESC, PT.IMAGE AS PRIORITYIMAGE, PT.DESCRIPTION AS PRIORITYDESC, ST.DESCRIPTION AS STATUSDESC, 
ST.IMAGE AS STATUSIMAGE, LG.FIRSTNAME + ' ' + LG.LASTNAME AS FullName, dbo.H_SUPPORT_UNITS.DEBIT, F.Quoted, 
cs.cs_description 
FROM dbo.H_REQUEST_JOURNAL RJ 
INNER JOIN dbo.H_REQUEST_TYPE RT ON RJ.REQUEST_TYPE_ID = RT.REQUEST_TYPE_ID 
INNER JOIN dbo.H_PRIORITY PT ON RJ.PRIORITY_ID = PT.PRIORITY_ID 
INNER JOIN dbo.H_SYSTEM DS ON DS.SYSTEM_ID = RJ.SYSTEM_ID 
INNER JOIN dbo.H_STATUS ST ON ST.STATUS_ID = RJ.STATUS_ID 
INNER JOIN H_STATUSLINK SL ON rj.STATUS_ID = SL.D_STATUSID 
INNER JOIN C_STATUS CS ON CS.CS_ID = SL.C_STATUSID 
LEFT JOIN H_REQUEST_COMMENTS RC ON RC.JOB_ID = RJ.JOB_ID
LEFT JOIN H_WORKDATES WD ON RJ.JOB_ID = WD.JOBID 
LEFT OUTER JOIN dbo.H_SUPPORT_UNITS ON RJ.JOB_ID = dbo.H_SUPPORT_UNITS.JOB_ID 
LEFT OUTER JOIN dbo.H_LOGIN LG ON LG.USER_ID = RJ.USER_ID 
LEFT JOIN H_FINANCE F ON F.JOB_ID = RJ.JOB_ID 
WHERE RJ.JOB_ID = @JOBID


GO
