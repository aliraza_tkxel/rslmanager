SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_AddDefectImage @propertyDefectId = 1,
		--@imagePath = N'C;\Appliances\Documents',
		--@imageName = N'i.jpg'
-- Author:		<Salman Nazir>
-- Create date: <16/11/2012>
-- Description:	<Get all defect images>
-- WebPage: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_AddDefectImage]
@propertyDefectId int,
@imagePath varchar(1000),
@imageName varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
	([PropertyDefectId],[ImageTitle],[ImagePath])
	VALUES
	(@propertyDefectId,@imageName,@imagePath)
END
GO
