SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[sp_getuserlist]
@org_id int
AS 

SELECT 
Active = 
CASE H_LOGIN.active
WHEN 1 THEN 'Active'
ELSE 'Inactive'
END,
dbo.H_LOGIN.[User_ID],dbo.H_LOGIN.User_Login,
dbo.H_LOGIN.FirstName + ' ' + dbo.H_LOGIN.LastName as fullname, dbo.H_LOGIN.Active,
dbo.H_ORGANISATION.orgname
FROM dbo.H_LOGIN INNER JOIN
dbo.H_ORGANISATION ON dbo.H_LOGIN.ORG_ID = dbo.H_ORGANISATION.ORG_ID
WHERE     (dbo.H_ORGANISATION.ORG_ID = @org_id)


GO
