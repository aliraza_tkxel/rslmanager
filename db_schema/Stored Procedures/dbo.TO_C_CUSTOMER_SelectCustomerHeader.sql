SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[TO_C_CUSTOMER_SelectCustomerHeader]

	/*	===============================================================
	'   NAME:           TO_C_CUSTOMER_SelectCustomerHeader
	'   DATE ALTERD:   23 MAY 2008
	'   ALTERD BY:     Munawar Nadeem
	'   ALTERD FOR:    Broadland Housing
	'   PURPOSE:        Used to Fill CustomerHeaderBO, eventually stored in session  
	'   SELECT:         Title, FirstName, MiddleName, LastName, Tenancy
	'   IN:             @customerID
	'   OUT: 	        Nothing    
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

    @customerID varchar (50)
    

AS

	 SET NOCOUNT ON 
	 
	 SELECT
		     G_TITLE.DESCRIPTION, 
			 C__CUSTOMER.FIRSTNAME, 
			 C__CUSTOMER.MIDDLENAME, 
			 C__CUSTOMER.LASTNAME, 
             C_CUSTOMERTENANCY.TENANCYID
             
	FROM         
			 C__CUSTOMER LEFT JOIN  G_TITLE 
			 ON C__CUSTOMER.TITLE = G_TITLE.TITLEID 
			 
			 INNER JOIN C_CUSTOMERTENANCY 
			 ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
			 
	WHERE     
			( C__CUSTOMER.CUSTOMERID = @customerID
			AND
			C_CUSTOMERTENANCY.ENDDATE IS NULL ) 




GO
