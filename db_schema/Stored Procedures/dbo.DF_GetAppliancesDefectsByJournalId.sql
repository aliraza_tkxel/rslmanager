USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAppliancesDefectsByJournalId]    Script Date: 10/01/2015 17:30:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  05/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         05/08/2015     Aamir Waheed       Created:DF_GetAppliancesDefectsByJournalId
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[DF_GetAppliancesDefectsByJournalId]
						@journalId = 10341,
						@defectCategoryId = -1

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

IF OBJECT_ID('dbo.DF_GetAppliancesDefectsByJournalId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetAppliancesDefectsByJournalId AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[DF_GetAppliancesDefectsByJournalId](
		
		@journalId INT,
		@defectCategoryId INT = -1
)
AS
BEGIN

Declare @assingnedToContractorStatusId int 
SET @assingnedToContractorStatusId = (SELECT PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Assigned To Contractor')
SELECT
	D.PropertyDefectId									AS DefectId
	,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
	,DC.Description										AS DefectCategory
	,ISNULL(DP.PriorityName, 'N/A')						AS DefectPriority
	,ISNULL(T.Description, 'N/A')						AS DefectTrade
	,ISNULL(D.IsTwoPersonsJob, 0)						AS isTwoPersonsJob
	,ISNULL(D.IsDisconnected, 0)						AS isApplianceDisconnected
	,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108) + ' '
	+ CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 103)	AS AppointmentDate
	,D.PartsDue											AS DefectPartsDue
	,E.EMPLOYEEID										AS EMPLOYEEID
	,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')			AS EMPLOYEENAME
	,ISNULL(D.Duration, 0)								AS Duration
	,T.TradeId											AS TradeId
	,CASE
		WHEN D.ApplianceDefectAppointmentJournalId IS NULL
			THEN 0
		ELSE 1
	END													AS isAppointmentCreated
	,CASE
		WHEN J.STATUSID = @assingnedToContractorStatusId
			THEN 1
		ELSE 0
	END													As isAssignedToContractor
	,CASE
		WHEN D.PROPERTYID IS NOT NULL
			THEN 'Property'
		WHEN D.SchemeId IS NOT NULL
			THEN 'Scheme'
		ELSE 'Block'

	END													As AppointmentType
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AS D
		LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
		LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
		LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
		INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
		LEFT JOIN P_DEFECTS_PRIORITY AS DP ON D.Priority = DP.PriorityID
		LEFT JOIN G_TRADE AS T ON D.TradeId = T.TradeId
		LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END
		LEFT JOIN PDR_JOURNAL J ON D.ApplianceDefectAppointmentJournalId = J.JOURNALID 
		LEFT JOIN PDR_MSAT M ON J.MSATID = M.MSATId AND M.isPending = 1 
		LEFT JOIN PDR_APPOINTMENTS A ON J.JOURNALID = A.JOURNALID
WHERE
	D.JournalId = @journalId
	AND (D.detectortypeid IS NULL OR D.detectortypeid = 0)
	AND (((D.ApplianceId IS NOT NULL AND D.ApplianceId <> 0 ) AND APP.ISACTIVE =1) OR (D.ApplianceId IS NULL or D.ApplianceId = 0 ))   
	AND (@defectCategoryId = -1 OR D.CategoryId = @defectCategoryId)
	AND D.DefectDate >= CONVERT( DATETIME, '7 Jan 2016', 106 ) 
	AND DC.Description IN ('RIDDOR', 'Immediately Dangerous', 'At Risk', 'Not to Current Standards','Other')
	AND D.DefectJobSheetStatus IN (SELECT STATUSID FROM PDR_STATUS WHERE TITLE IN ('Approved', 'Arranged'))
END