IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_GetFuelServicingContractors'))
  DROP PROCEDURE AS_GetFuelServicingContractors
  GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 10/2/2016
-- Description:	Get fuel servicing contractors list
-- Exec AS_GetFuelServicingContractors
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetFuelServicingContractors]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT DISTINCT org.orgid as id, org.name as description FROM S_ORGANISATION org 
	INNER JOIN S_Scope scope ON scope.orgid = org.orgid
	INNER JOIN S_AREAOFWORK area ON area.areaofworkid=scope.areaofwork 
	WHERE area.areaofworkid=5
	
END
