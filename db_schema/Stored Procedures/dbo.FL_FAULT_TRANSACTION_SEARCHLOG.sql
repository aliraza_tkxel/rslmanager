SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[FL_FAULT_TRANSACTION_SEARCHLOG] 

/* ===========================================================================
 '   NAME:           FL_FAULT_TRANSACTION_SEARCHLOG
 '   DATE CREATED:   29 OCTOBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist fault logs based on search criteria provided
 '   IN:            @locationId, @areaId, @elementId, @teamId, @userId
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	        @locationId	int	=  NULL,
		@areaId	        int	=  NULL,
		
		@elementId	int = NULL,
		@teamId		int = NULL,
		@userId		int = NULL,    
	
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FAULTTRANSLOGID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @teamId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' E_TEAM.TEAMID = '+ LTRIM(STR(@teamId)) + ' AND'  
                             
    IF @userId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E__EMPLOYEE.EMPLOYEEID = '+ LTRIM(STR(@userId)) + ' AND'  
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + ' E.FAULTTRANSLOGID,E.TRANDATE,' +
                        CHAR(10) + CHAR(9) + 'E__EMPLOYEE.FIRSTNAME + '' '' + E__EMPLOYEE.LASTNAME ENAME ,Case E.FAULTACTION ' +
                        CHAR(10) + CHAR(9) + 'when 0 then ''New Fault'' when 1 then ''Amend Fault'' end as FAULTACTIONTYPE,' +
                        CHAR(10) + CHAR(9) + 'FL_LOCATION.LocationName, FL_AREA.AreaName, FL_ELEMENT.ElementName, FL_FAULT.Description'	                 
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT_TRANSACTION_LOG as E INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'E__EMPLOYEE ON E.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT ON E.FAULTID = FL_FAULT.FaultID INNER JOIN'+
	         CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID INNER JOIN'+
	         CHAR(10) + CHAR(9) + 'FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'FL_LOCATION ON FL_LOCATION.LOCATIONID = FL_AREA.LOCATIONID'
		 
                    
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FAULTTRANSLOGID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FAULTTRANSLOGID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FAULTTRANSLOGID NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FAULTTRANSLOGID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'AND'+ @SearchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)














GO
