SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_MANDATORY_READ_SELECT]
AS
	SET NOCOUNT ON;
SELECT     DOCEMPID, EMPLOYEEID, DOCUMENTID
FROM         I_MANDATORY_READ
GO
