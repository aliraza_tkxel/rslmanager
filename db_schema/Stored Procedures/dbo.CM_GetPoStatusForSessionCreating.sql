USE [RSLBHALive]
IF OBJECT_ID('dbo.[CM_GetPoStatusForSessionCreating]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[CM_GetPoStatusForSessionCreating] AS SET NOCOUNT ON;') -- Create dummy/empty SP

 GO
-- =============================================
-- Author:		<Ali Raza>
-- Create date: <06/09/2017>
-- Description:	<Get PO status for opening PO link>
-- Web Page:	<Email to schedular>
-- EXEC CM_GetPoStatusForSessionCreating  265606
-- =============================================
ALTER PROCEDURE [dbo].[CM_GetPoStatusForSessionCreating]
	@orderId int
AS
BEGIN

--Select S.Title as PoStatus from CM_ServiceItems I
--INNER JOIN CM_Status S on I.StatusId = S.StatusId
--Left JOIN CM_ContractorWork cw ON I.ServiceItemId = cw.ServiceItemId
--where (I.PORef = @orderId OR cw.PurchaseOrderId=@orderId)
Select S.Title as PoStatus from CM_ContractorWork cw
Cross APPLY(Select top 1 *  from CM_ServiceItems_History I where I.PORef = cw.PurchaseOrderId Order by I.ServiceItemHistoryId DESC ) I
INNER JOIN CM_Status S on I.StatusId = S.StatusId
Where cw.PurchaseOrderId=@orderId


	
END
