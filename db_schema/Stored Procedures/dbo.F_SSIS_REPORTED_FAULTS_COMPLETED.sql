
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[F_SSIS_REPORTED_FAULTS_COMPLETED]
AS

--+---------------------------------------------------------------------------
--+ Calculate the start and the end of the current financial year
--+---------------------------------------------------------------------------

    DECLARE @YearStart DATETIME
    DECLARE @YearEnd DATETIME
    
    SET @YearStart = [dbo].[F_GET_FINANCIAL_YEAR_START_DATE]  (GETDATE())
    SET @YearEnd = DATEADD(YY, 1, @YearStart)

--+---------------------------------------------------------------------------
--+ Gather and return data
--+---------------------------------------------------------------------------
    
    SELECT  FL_FAULT_LOG.JobSheetNumber AS [JS Number] ,
            ISNULL(CONVERT (VARCHAR, FL_FAULT_LOG.SubmitDate, 103), '') AS [Submit Date] ,
            ISNULL(CONVERT (VARCHAR, FL_FAULT_PREINSPECTIONINFO.InspectionDate, 103),
                   '') AS [Appointment Date] ,
            C__CUSTOMER.LASTNAME AS [Customer Name] ,
            ISNULL(NULLIF(ISNULL(P__PROPERTY.HOUSENUMBER, '') + ISNULL(' '
                                                              + P__PROPERTY.ADDRESS1,
                                                              '') + ISNULL(' '
                                                              + P__PROPERTY.ADDRESS2,
                                                              '') + ISNULL(' '
                                                              + P__PROPERTY.ADDRESS3,
                                                              '')
                          + ISNULL(', ' + P__PROPERTY.TOWNCITY, '')
                          + ISNULL(', ' + P__PROPERTY.COUNTY, '')
                          + ISNULL(', ' + P__PROPERTY.POSTCODE, ''), ''),
                   'N/A') AS [Property Address] ,
            ISNULL(FL_AREA.AreaName, '') AS [Area Name] ,
            ISNULL(FL_ELEMENT.ElementName, '') AS [Element Name] ,
            FL_FAULT.Description AS [Fault Description] ,
            CASE FL_FAULT_LOG.Recharge
              WHEN 1 THEN 'Yes'
              WHEN 0 THEN 'No'
              ELSE 'No'
            END Recharge ,
            FL_FAULT_LOG.Quantity ,
            ( CONVERT (VARCHAR, FL_FAULT_PRIORITY.ResponseTime)
              + CASE FL_FAULT_PRIORITY.Days
                  WHEN 1 THEN 'Day(s)'
                  WHEN 0 THEN 'Hour(s)'
                END ) AS Priority ,
            FL_FAULT_STATUS.DESCRIPTION AS [Fault Status] ,
            ( CASE WHEN S_ORGANISATION.NAME IS NULL THEN 'TBA'
                   WHEN S_ORGANISATION.NAME IS NOT NULL
                   THEN S_ORGANISATION.NAME
              END ) AS Contractor
    FROM    FL_FAULT_LOG
            INNER JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID
            LEFT JOIN FL_FAULT_PREINSPECTIONINFO ON FL_FAULT_LOG.FaultLogID = FL_FAULT_PREINSPECTIONINFO.faultlogid
            INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
            INNER JOIN FL_FAULT_JOURNAL ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FAULTLOGID
            INNER JOIN C_NATURE ON FL_FAULT_JOURNAL.ITEMNATUREID = C_NATURE.ITEMNATUREID
            LEFT JOIN S_ORGANISATION ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID
            INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
            LEFT JOIN FL_ELEMENT ON FL_FAULT.ElementID = FL_ELEMENT.ElementID
            INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
            LEFT JOIN FL_AREA ON FL_ELEMENT.AreaID = FL_AREA.AreaID
            LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
    WHERE   FL_FAULT_LOG.SubmitDate >= @YearStart
            AND FL_FAULT_LOG.SubmitDate < @YearEnd
            AND FL_FAULT_STATUS.FaultStatusID IN ( 17, 10, 7 ) -- Completed
ORDER BY    FL_FAULT_LOG.FaultLogID DESC




GO
