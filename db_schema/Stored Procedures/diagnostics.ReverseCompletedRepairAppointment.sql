SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [diagnostics].[ReverseCompletedRepairAppointment]

@AppointmentId INT, 
@FaultLogId INT


AS 


UPDATE FL_FAULT_LOG SET StatusID = 4, CompletedDate = NULL WHERE FaultLogID = @FaultLogId
UPDATE FL_FAULT_JOURNAL SET	FaultStatusID = 4, LastActionDate = getdate() WHERE FaultLogID = @FaultLogId AND FaultStatusID = 17
DELETE FL_FAULT_LOG_HISTORY WHERE FaultLogID = @FaultLogId AND FaultStatusID = 17
UPDATE FL_CO_APPOINTMENT SET	AppointmentStatus = 'NotStarted', LastActionDate = GETDATE(), RepairCompletionDateTime = NULL WHERE AppointmentID = @AppointmentId

GO
