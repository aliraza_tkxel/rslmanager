USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AlternativeAppointmentsArranged]    Script Date: 31/07/2018 11:36:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_AlternativeAppointmentsArranged') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_AlternativeAppointmentsArranged AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_AlternativeAppointmentsArranged] 
	(
		-- Add the parameters for the stored procedure here
		@check56Days bit=0,			
		@searchedText varchar(8000)='',
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'AppointmentSortDate',
		@sortOrder varchar (5) = 'ASC',
		@patchId		int=-1,
		@schemeId	    int=-1,
		@DevelopmentId	int = -1,
		@fuelType		int = -1,
		@totalCount int=0 output
	)
AS
BEGIN
	DECLARE @SelectClause varchar(max),
        @fromClause   varchar(max),
        @whereClause  varchar(max),	        
        @orderClause  varchar(1000),	
        @mainSelectQuery varchar(max),
		@mainSelectQueryScheme varchar(max), 
		@resultingQuery varchar(max), 
		@rowNumberQuery varchar(max),
        @finalQuery varchar(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(max),
		@selectClaseForCountProperty varchar(max),
		@RecordLimit INT,      
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		SET @RecordLimit = 30
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SELECT  @searchCriteria = ' 1=1 '
		
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND ISNULL(P__PROPERTY.HOUSENUMBER + '' '','''') 
					+ P__PROPERTY.ADDRESS1 
					+ ISNULL('' '' + P__PROPERTY.ADDRESS2,'''') 
					+ ISNULL('' '' + P__PROPERTY.ADDRESS3,'''') LIKE ''%' + @searchedText + '%'''
		END 
				
		IF(@check56Days = 1)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <=56 
																AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0'
		END	
		IF (@patchId <> -1)
		BEGIN
			SET @searchCriteria = @searchCriteria + 'AND PDR_DEVELOPMENT.PATCHID = ' + CONVERT(varchar, @patchId) + CHAR(10)
 		END		 		
		IF (@schemeId <> -1)
		BEGIN
			SET @searchCriteria = @searchCriteria + 'AND P__PROPERTY.SCHEMEID = ' + CONVERT(varchar, @schemeId) + CHAR(10)
		END		
		If (@developmentId <>-1) 
		Begin
			SET @searchCriteria = @searchCriteria + ' AND P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, (@developmentId)) + CHAR(10)	
		End

		If (@fuelType <>-1) 
		Begin
			SET @searchCriteria = @searchCriteria +' AND PV.ValueId = ' + CONVERT(varchar, (@fuelType)) + CHAR(10)	
		End 		
		
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (AS_JOURNAL.STATUSID IN (select StatusId from AS_Status WHERE Title LIKE ''Arranged''))																						
															AND AS_JOURNAL.IsCurrent = 1
															AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9) -- Garage, Car port, car space
															AND PV.IsAlterNativeHeating = 1
															AND AS_JOURNAL.ServicingTypeId = (Select ServicingTypeID FROM P_ServicingType WHERE Description = ''Alternative Servicing'')'
				
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause			
		
		SET @SelectClause = 'SELECT Distinct
							AS_APPOINTMENTS.JSGNUMBER AS JSGNUMBER
							,CONVERT(varchar,AS_APPOINTMENTS.APPOINTMENTDATE,103)+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME+''-''+ AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENT
							,AS_APPOINTMENTS.ASSIGNEDTO
							,AS_APPOINTMENTS.LOGGEDDATE as LOGGEDDATE,
							LEFT(E__EMPLOYEE.FIRSTNAME, 1)+LEFT(E__EMPLOYEE.LASTNAME,1) AS ENGINEER,
							ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''')+ ISNULL('' , ''+P__PROPERTY.TOWNCITY,'''')  AS ADDRESS ,  
							P__PROPERTY.HouseNumber as HouseNumber,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							P__PROPERTY.ADDRESS3 as ADDRESS3,							
							P__PROPERTY.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,
							--P_FUELTYPE.FUELTYPE AS FUEL,
							FuelTypes.FUELS AS FUEL,
							P__PROPERTY.PROPERTYID,
							C_TENANCY.TENANCYID,
							AS_APPOINTMENTS.APPOINTMENTID,
							AS_APPOINTMENTS.JournalId,							
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3 
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
								
							ISNULL(CA.MOBILE,''N/A'') as Mobile,
							CUST.FIRSTNAME  + '' '' + CUST.LASTNAME AS NAME,
							
							ISNULL(P_STATUS.DESCRIPTION, ''N/A'') AS PropertyStatus,
							E_PATCH.PatchId as PatchId,
							E_PATCH.Location as PatchName,
							ISNULL(CA.Tel,''N/A'') as Telephone,
							AS_APPOINTMENTS.APPOINTMENTDATE as AppointmentSortDate,
							P_LGSR.ISSUEDATE as LGSRDate
							,AS_APPOINTMENTS.NOTES as Notes
							,CT.CUSTOMERID as CustomerId,
							AS_EmailStatus.EmailStatusId as EmailStatusId,
							AS_EmailStatus.StatusImagePath as EmailImagePath,
							AS_APPOINTMENTS.EmailDescription as EmailDescription,
							AS_EmailStatus.StatusDescription as EmailStatusDescription,
							AS_SmsStatus.SmsStatusId as SmsStatusId,
							AS_SmsStatus.StatusImagePath as SmsImagePath,
							AS_APPOINTMENTS.SmsDescription as SmsDescription,
							AS_SmsStatus.StatusDescription as SmsStatusDescription,
							AS_APPOINTMENTS.JournalHistoryId as JournalHistoryId,
							AS_PushNoticificationStatus.PushNoticificationId as PushNoticificationStatusId,
							AS_PushNoticificationStatus.StatusImagePath as PushNoticificationImagePath,
							AS_APPOINTMENTS.PushNoticificationDescription as PushNoticificationDescription,
							AS_PushNoticificationStatus.StatusDescription as PushNoticificationStatusDescription,
							AS_APPOINTMENTS.APPOINTMENTDATE as ApDate
							, CA.EMAIL, P__PROPERTY.TOWNCITY, P__PROPERTY.COUNTY
							, ''Property'' as AppointmentType
							,ISNULL(AS_APPOINTMENTS.Duration,1) as Duration'

		SET @selectClaseForCountProperty = 'SELECT Distinct
							AS_APPOINTMENTS.JSGNUMBER AS JSGNUMBER
							,CONVERT(varchar,AS_APPOINTMENTS.APPOINTMENTDATE,103)+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME+''-''+ AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENT
							,AS_APPOINTMENTS.ASSIGNEDTO
							,AS_APPOINTMENTS.LOGGEDDATE as LOGGEDDATE,
							LEFT(E__EMPLOYEE.FIRSTNAME, 1)+LEFT(E__EMPLOYEE.LASTNAME,1) AS ENGINEER,
							ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''')+ ISNULL('' , ''+P__PROPERTY.TOWNCITY,'''')  AS ADDRESS ,  
							P__PROPERTY.HouseNumber as HouseNumber,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							P__PROPERTY.ADDRESS3 as ADDRESS3,							
							P__PROPERTY.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,
							--P_FUELTYPE.FUELTYPE AS FUEL,
							FuelTypes.FUELS AS FUEL,
							P__PROPERTY.PROPERTYID,
							C_TENANCY.TENANCYID,
							AS_APPOINTMENTS.APPOINTMENTID,
							AS_APPOINTMENTS.JournalId,							
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3 
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							WHEN ''Aborted'' then ''Aborted (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE ''Aborted'') 
								-- AND YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
								
							ISNULL(CA.MOBILE,''N/A'') as Mobile,
							CUST.FIRSTNAME  + '' '' + CUST.LASTNAME AS NAME,
							
							ISNULL(P_STATUS.DESCRIPTION, ''N/A'') AS PropertyStatus,
							E_PATCH.PatchId as PatchId,
							E_PATCH.Location as PatchName,
							ISNULL(CA.Tel,''N/A'') as Telephone,
							AS_APPOINTMENTS.APPOINTMENTDATE as AppointmentSortDate,
							P_LGSR.ISSUEDATE as LGSRDate
							,AS_APPOINTMENTS.NOTES as Notes
							,CT.CUSTOMERID as CustomerId,
							AS_EmailStatus.EmailStatusId as EmailStatusId,
							AS_EmailStatus.StatusImagePath as EmailImagePath,
							AS_APPOINTMENTS.EmailDescription as EmailDescription,
							AS_EmailStatus.StatusDescription as EmailStatusDescription,
							AS_SmsStatus.SmsStatusId as SmsStatusId,
							AS_SmsStatus.StatusImagePath as SmsImagePath,
							AS_APPOINTMENTS.SmsDescription as SmsDescription,
							AS_SmsStatus.StatusDescription as SmsStatusDescription,
							AS_APPOINTMENTS.JournalHistoryId as JournalHistoryId,
							AS_PushNoticificationStatus.PushNoticificationId as PushNoticificationStatusId,
							AS_PushNoticificationStatus.StatusImagePath as PushNoticificationImagePath,
							AS_APPOINTMENTS.PushNoticificationDescription as PushNoticificationDescription,
							AS_PushNoticificationStatus.StatusDescription as PushNoticificationStatusDescription,
							AS_APPOINTMENTS.APPOINTMENTDATE as ApDate
							, CA.EMAIL, P__PROPERTY.TOWNCITY, P__PROPERTY.COUNTY
							, ''Property'' as AppointmentType
							,ISNULL(AS_APPOINTMENTS.Duration,1) as Duration'
		
			-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause				
		
		SET @fromClause = CHAR(10) + ' FROM AS_APPOINTMENTS 
										INNER JOIN	AS_JOURNAL on AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID 
										INNER JOIN AS_JournalHeatingMapping ON AS_JournalHeatingMapping.JournalId = AS_JOURNAL.JOURNALID
										INNER JOIN	AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId 
										INNER JOIN E__EMPLOYEE on AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
										INNER JOIN P__PROPERTY on AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
										--INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 

										INNER JOIN 
										(SELECT	P__PROPERTY.PROPERTYID, FUELS  = STUFF(
													(SELECT '', '' + PV.ValueDetail
														FROM PA_HeatingMapping HM
														Inner JOIN PA_PARAMETER_VALUE PV on HM.HeatingType = PV.ValueId
														WHERE HM.IsActive = 1 AND PV.IsAlterNativeHeating = 1 AND P__PROPERTY.PropertyId = HM.PropertyId AND PV.IsActive = 1
														FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''NVARCHAR(max)''), 1, 2, '''')
										FROM	P__PROPERTY 
										GROUP BY P__PROPERTY.PROPERTYID) As FuelTypes 
										on P__PROPERTY.PROPERTYID = FuelTypes.PropertyId

										LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
										LEFT JOIN P_LGSR on P_LGSR.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
										INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
										INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID 
										LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
										LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = C_TENANCY.TENANCYID
										AND CT.CUSTOMERTENANCYID = (SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					)
										LEFT JOIN C_ADDRESS CA ON CA.CUSTOMERID = CT.CUSTOMERID  AND CA.IsDefault=1
										LEFT JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = CT.CUSTOMERID
										LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
										AND A.ITEMPARAMID =  (SELECT ItemParamID FROM PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
													WHERE ParameterName = ''Heating Fuel'' AND ItemName = ''Heating'')
										LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
										INNER JOIN AS_EmailStatus on AS_APPOINTMENTS.EmailStatusId=AS_EmailStatus.EmailStatusId
										INNER JOIN AS_SmsStatus on AS_APPOINTMENTS.SmsStatusId=AS_SmsStatus.SmsStatusId
										INNER JOIN AS_PushNoticificationStatus on AS_APPOINTMENTS.PushNoticificationId=AS_PushNoticificationStatus.PushNoticificationId
										'

		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
		END

			
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
										 
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause + @fromClause + @whereClause

		SET @resultingQuery = @mainSelectQuery
		print(@mainSelectQuery)
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@resultingQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT TOP (' + CONVERT(NVARCHAR(10),@RecordLimit) + ') *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		--print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(max), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@selectClaseForCountProperty+@fromClause+@whereClause+') as Records ) '
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							
END
