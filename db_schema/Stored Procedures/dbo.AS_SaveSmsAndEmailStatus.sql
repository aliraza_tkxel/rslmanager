USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveSmsAndEmailStatus]    Script Date: 27/01/2017 15:01:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_SaveSmsAndEmailStatus') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_SaveSmsAndEmailStatus AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_SaveSmsAndEmailStatus] 
	@journalHistoryId int,
	@smsStatus nvarchar(max),
	@smsDescription nvarchar(max),
	@emailStatus nvarchar(max),
	@emailDescription nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;			
	IF EXISTS (Select 1 from [AS_APPOINTMENTS] where [JOURNALHISTORYID] = @journalHistoryId)	
	BEGIN
		if @smsStatus='' or @emailStatus=''
		BEGIN
   		--Update the Journal
			IF @smsStatus=''
			BEGIN
				Update [dbo].[AS_APPOINTMENTS] SET
					[EmailStatusId]=(select [EmailStatusId] from AS_EmailStatus where StatusDescription=@emailStatus)
				   ,[EmailDescription] = @emailDescription
				   WHERE [JOURNALHISTORYID] = @journalHistoryId  
			END
			ELSE
			BEGIN
				Update [dbo].[AS_APPOINTMENTS] SET				
					   [SmsStatusId]= (select [SmsStatusId] from AS_SmsStatus where StatusDescription=@smsStatus)
					   ,[SmsDescription]=@smsDescription
					   WHERE [JOURNALHISTORYID] = @journalHistoryId  
			END
			
		End
		ELSE
		BEGIN
			Update [dbo].[AS_APPOINTMENTS] SET
					[EmailStatusId]=(select [EmailStatusId] from AS_EmailStatus where StatusDescription=@emailStatus)
				   ,[EmailDescription] = @emailDescription
				   ,[SmsStatusId]= (select [SmsStatusId] from AS_SmsStatus where StatusDescription=@smsStatus)
				   ,[SmsDescription]=@smsDescription
				   WHERE [JOURNALHISTORYID] = @journalHistoryId  
		END
	END
   
END

