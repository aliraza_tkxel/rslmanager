USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetSchedulingPropertyStatus]    Script Date: 18-Jan-17 2:26:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetApplianceType
-- Author:		<Noor Muhammad>
-- Create date: <02/11/2012>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
IF OBJECT_ID('dbo.[AS_GetSchedulingPropertyStatus]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetSchedulingPropertyStatus] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].AS_GetSchedulingPropertyStatus
@journalId INT,
@appointmentType varchar(100),
@isScheduled int out,
@isTimeExceeded int out,
@ScheduleStatus int out 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @count int
	if @appointmentType = 'Property'
	begin
		set @count=(SELECT  count(*)
		FROM AS_JOURNAL
		WHERE JOURNALID = @journalId
		AND AS_JOURNAL.ISCURRENT=1	
		AND (STATUSID IN (SELECT STATUSID FROM AS_STATUS WHERE TITLE LIKE 'Appointment to be arranged' OR TITLE LIKE 'Cancelled' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted')))

		if(@count=0)
		begin
			SELECT @ScheduleStatus = 0, @isScheduled=1, @isTimeExceeded = 1
		end
		else
		begin
		   SELECT @ScheduleStatus = AS_JOURNAL.isLock, @isScheduled=AS_JOURNAL.isScheduled, @isTimeExceeded = CASE WHEN AS_JOURNAL.LockTime  IS NULL THEN 0 ELSE (CASE WHEN DATEDIFF(minute,AS_JOURNAL.LockTime, GETDATE()) >= 5 THEN 1 ELSE 0 END) END 
			FROM AS_JOURNAL
			WHERE JOURNALID = @journalId
			AND AS_JOURNAL.ISCURRENT=1
			and (STATUSID IN (SELECT STATUSID FROM AS_STATUS WHERE TITLE LIKE 'Appointment to be arranged' OR TITLE LIKE 'Cancelled' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted'))
		end
	end
	else if @appointmentType = 'Scheme'
	begin
		set @count=(SELECT  count(*)
		FROM AS_JOURNAL
		WHERE JOURNALID = @journalId
		AND AS_JOURNAL.ISCURRENT=1	
		AND (STATUSID IN (SELECT STATUSID FROM AS_STATUS WHERE TITLE LIKE 'Appointment to be arranged' OR TITLE LIKE 'Cancelled' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted')))

		if(@count=0)
		begin
			SELECT @ScheduleStatus = 0, @isScheduled=1, @isTimeExceeded = 1
		end
		else
		begin
		   SELECT @ScheduleStatus = AS_JOURNAL.isLock, @isScheduled=AS_JOURNAL.isScheduled, @isTimeExceeded = CASE WHEN AS_JOURNAL.LockTime  IS NULL THEN 0 ELSE (CASE WHEN DATEDIFF(minute,AS_JOURNAL.LockTime, GETDATE()) >= 5 THEN 1 ELSE 0 END) END 
			FROM AS_JOURNAL
			WHERE JOURNALID = @journalId
			AND AS_JOURNAL.ISCURRENT=1
			and (STATUSID IN (SELECT STATUSID FROM AS_STATUS WHERE TITLE LIKE 'Appointment to be arranged' OR TITLE LIKE 'Cancelled' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted'))
		end
	end
	else if @appointmentType = 'Block'
	begin
		set @count=(SELECT  count(*)
		FROM AS_JOURNAL
		WHERE JOURNALID = @journalId
		AND AS_JOURNAL.ISCURRENT=1	
		AND (STATUSID IN (SELECT STATUSID FROM AS_STATUS WHERE TITLE LIKE 'Appointment to be arranged' OR TITLE LIKE 'Cancelled' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted')))

		if(@count=0)
		begin
			SELECT @ScheduleStatus = 0, @isScheduled=1, @isTimeExceeded = 1
		end
		else
		begin
		   SELECT @ScheduleStatus = AS_JOURNAL.isLock, @isScheduled=AS_JOURNAL.isScheduled, @isTimeExceeded = CASE WHEN AS_JOURNAL.LockTime  IS NULL THEN 0 ELSE (CASE WHEN DATEDIFF(minute,AS_JOURNAL.LockTime, GETDATE()) >= 5 THEN 1 ELSE 0 END) END 
			FROM AS_JOURNAL
			WHERE JOURNALID = @journalId
			AND AS_JOURNAL.ISCURRENT=1
			and (STATUSID IN (SELECT STATUSID FROM AS_STATUS WHERE TITLE LIKE 'Appointment to be arranged' OR TITLE LIKE 'Cancelled' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted'))
		end
	end

END