USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[JSS_GetSbJobSheetDetails]    Script Date: 10/23/2017 18:03:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =============================================
 * EXEC JSS_GetSbJobSheetDetails @jobSheetNumber ='JS66414'
 * Author:  Ali Raza	
 * Create date: 28/01/2015
 * Description: Returns Job Sheet Details, that is consist of following details/Tables:
 *				1- Job Sheet(Fault Log) and Appointment details.
 *				2- Customer Info/Deatil
 *				3- Repair List and Repair Notes
 *				4- Job Sheet(Fault Log) Activities
 *				5- Asbestos Info/Detail(s)
 *
 * Webpage:JobSheetSummary.aspx
 * ============================================= */
IF OBJECT_ID('dbo.[JSS_GetSbJobSheetDetails]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[JSS_GetSbJobSheetDetails] AS SET NOCOUNT ON;') 
GO 
 
ALTER PROCEDURE [dbo].[JSS_GetSbJobSheetDetails]
 -- Add the parameters for the stored procedure here  
 (  
 @jobSheetNumber varchar(50)  
 )  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  

SET NOCOUNT ON;

-- =========================================
-- 1- Fault Log And Appointment Details
-- =========================================

SELECT DISTINCT
	FL.JobSheetNumber JSN,
	ISNULL(O.NAME, 'N/A') ContractorName,
	ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, '-') AS OperativeName,
	CASE
		WHEN FP.Days = 1 THEN CONVERT(varchar(5), FP.ResponseTime) + ' days'
		ELSE CONVERT(varchar(5), FP.ResponseTime) + ' hours'
	END AS [Priority],
	CONVERT(varchar, FL.DueDate, 106) + ' ' + CONVERT(varchar(5), FL.DueDate, 108) AS CompletionDue,
	CONVERT(varchar, FL.SubmitDate, 106) + ' ' + CONVERT(varchar(5), FL.SubmitDate, 108) AS ReportedDate,
	ISNULL(AREA.AreaName, 'N/A') Location,
	F.Description [Description],
	ISNULL(FL.Notes, 'N/A') FaultNotes,
	ISNULL(CONVERT(varchar(20), A.AppointmentDate, 106) + ' ' + A.Time + '-' + A.EndTime, '') AS AppointmentTimeDate,
	ISNULL(A.Notes, 'N/A') AppointmentNotes,
	ISNULL(A.Time + 'N/A' + DATENAME(WEEKDAY, A.AppointmentDate), 'N/A') Time,
	ISNULL(T.[Description], 'N/A') AS Trade,
	ISNULL(FON.FollowOnNotes, ' ') AS FollowOnNotes,
	ISNULL(CONVERT(NVARCHAR(17),A.RepairCompletionDateTime,113),'-') [CompletionDateTime],
	FL.FollowOnFaultLogId AS FollowOnFaultLogId,
	FL.PROPERTYID AS [PROPERTYID],
	FL.SchemeId,
	FL.BlockId	

FROM
/* Fault LOG Detail */
FL_FAULT_LOG FL
/* Fault Detail */
INNER JOIN FL_FAULT F
	ON FL.FaultID = F.FaultID
LEFT OUTER JOIN FL_AREA AREA
	ON AREA.AreaID = FL.AREAID
LEFT OUTER JOIN FL_FAULT_TRADE FT
	ON FL.FaultTradeId = FT.FaultTradeId
LEFT OUTER JOIN FL_FAULT_PRIORITY FP
	ON F.PriorityID = FP.PriorityID
LEFT OUTER JOIN G_TRADE T
	ON T.TradeId = FT.TradeId
/* Appointment Detail */
LEFT OUTER JOIN FL_FAULT_APPOINTMENT FA
	ON FL.FaultLogID = FA.FaultLogId
LEFT OUTER JOIN FL_CO_APPOINTMENT A
	ON FA.AppointmentId = A.AppointmentID
/* Operative Detail(s) - Associated with Appointment */
LEFT OUTER JOIN E__EMPLOYEE E
	ON E.EMPLOYEEID = A.OperativeID
/* Contractor Detail */
LEFT OUTER JOIN S_ORGANISATION O
	ON O.ORGID = FL.ORGID
/* Follow on Detail */
LEFT OUTER JOIN FL_FAULT_FOLLOWON FON
	ON FON.FaultLogID = FL.FaultLogId

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

-- =========================================
-- 2- Customer Info/Deatil
-- =========================================
Select ISNULL(P_SCHEME.SCHEMENAME,' ') as SchemeName, 
Case 
	When FL_FAULT_LOG.BlockId <> NULL THEN P_BLOCK.BLOCKNAME
	ELSE 'Non Selected' END as [BlockName],
	
Case 
	When FL_FAULT_LOG.BlockId <> NULL THEN ISNULL(P_BLOCK.ADDRESS1,' ')+' '+ISNULL(P_BLOCK.ADDRESS2,' ')+' '+ISNULL(P_BLOCK.ADDRESS3,' ') 
	ELSE ISNULL(PDR_DEVELOPMENT.ADDRESS1,' ')+' '+ISNULL(PDR_DEVELOPMENT.ADDRESS2,' ') END as [Address],
Case	
	When FL_FAULT_LOG.BlockId <> NULL THEN ISNULL(P_BLOCK.TOWNCITY,'N/A')
	ELSE ISNULL(PDR_DEVELOPMENT.TOWN,'N/A ') END as Town,
	
Case	
	When FL_FAULT_LOG.BlockId <> NULL THEN ISNULL(P_BLOCK.COUNTY,'N/A')
	ELSE ISNULL(PDR_DEVELOPMENT.COUNTY,'N/A ') END as County,
Case	
	When FL_FAULT_LOG.BlockId <> NULL THEN ISNULL(P_BLOCK.POSTCODE,'N/A')
	ELSE ISNULL(PDR_DEVELOPMENT.PostCode,'N/A ') END as PostCode		
		
	

 from FL_FAULT_LOG
 
 INNER JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
 Left JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID  
 LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID	
Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL_FAULT_LOG.FaultLogID) = @jobSheetNumber

-- =========================================
-- 3- Repair List and Repair Notes
-- =========================================

SELECT
	CONVERT(date, FLTR.InspectionDate, 103) AS CompletionDate,
	LTRIM(RTRIM(FRL.Description)) AS Description,
	FLTR.Notes [RepairNotes]
FROM FL_FAULT_LOG FL
INNER JOIN FL_CO_FAULTLOG_TO_REPAIR FLTR
	ON FL.FaultLogId = FLTR.FaultLogId
INNER JOIN FL_FAULT_REPAIR_LIST FRL
	ON FLTR.FaultRepairListID = FRL.FaultRepairListID

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

-- =========================================
-- 4- Job Sheet Activities
-- =========================================
SELECT
	FLH.LastActionDate AS [Date],
	FS.Description AS [Activity],
	ISNULL(LEFT(E.FIRSTNAME, 1) + LEFT(E.LASTNAME, 1), '-') AS [ActivityUserInitials] -- Activity User can be Scheduler, Operative etc.

FROM
/* Fault LOG Detail(s) */
FL_FAULT_LOG FL
/* Fault LOG History Detail(s) to get Activity/Status Change */
INNER JOIN FL_FAULT_LOG_HISTORY FLH
	ON FLH.FaultLogID = FL.FaultLogID
/* To get Status Name/Description for the status mentioned in fault log */
LEFT OUTER JOIN FL_FAULT_STATUS FS
	ON FS.FaultStatusID = FLH.FaultStatusID
/* Operative/Scheduler Detail(s) - Associated with Fault Log Activities */
LEFT OUTER JOIN E__EMPLOYEE E
	ON E.EMPLOYEEID = FLH.LastActionUserID

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber


-- =========================================
-- 5- Asbestos Info/Detail(s)
-- =========================================
SELECT
	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
	P_Asbestos.RISKDESCRIPTION Description

FROM FL_FAULT_LOG FL
INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL sRISKLEVEL
	ON FL.SchemeId = sRISKLEVEL.SchemeId
LEFT JOIN P_PROPERTY_ASBESTOS_RISKLEVEL bRISKLEVEL
	ON FL.BlockId = bRISKLEVEL.BlockId	
INNER JOIN P_PROPERTY_ASBESTOS_RISK
	ON P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = sRISKLEVEL.PROPASBLEVELID
INNER JOIN P_ASBESTOS
	ON sRISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber
and  (sRISKLEVEL.DateRemoved is null or CONVERT(DATE,sRISKLEVEL.DateRemoved) > convert(date, getdate()) )
AND P_ASBESTOS.other = 0
-- TODO: Remove After Testing with followon notes in Fault Log Query
-- =========================================
-- 6- Follow on Work Detail(s)
-- =========================================

SELECT
	FON.FollowOnNotes
FROM
/* Fault LOG Detail(s)*/
FL_FAULT_LOG FL
/* Follow on Work Detail(s) */
INNER JOIN FL_FAULT_FOLLOWON FON
	ON FL.FaultLogID = FON.FaultLogId
WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

END