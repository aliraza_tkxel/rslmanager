USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetServiceChargeReport]    Script Date: 23/10/2018 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetServiceChargeSchemeAssociatedBlocks') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeSchemeAssociatedBlocks AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeSchemeAssociatedBlocks]
		@filterSchemeId INT,
		@orderItemId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	
	DECLARE   @minSchemeId INT, @SchemeId INT, @BlockId INT, @PropertyId NVARCHAR(40)


	SELECT	@minSchemeId = MIN(schemeid) 
	FROM	F_PurchaseItemSCInfo 
	WHERE	OrderItemId = @orderItemId 
			AND IsActive = 1

	 IF @minSchemeId = -1
		BEGIN

		SELECT	P_BLOCK.BlockId, BlockName AS Address
		FROM	(SELECT	DISTINCT blockid
				FROM	P__PROPERTY
				WHERE	PROPERTYID NOT IN (	SELECT	propertyid 
											FROM	F_ServiceChargeExProperties 
											WHERE	PurchaseItemId = @orderItemId )
						AND SCHEMEID = @filterSchemeId) IncludedBlocks
				INNER JOIN P_BLOCK ON IncludedBlocks.blockid = P_BLOCK.blockid
		ORDER BY CAST(SUBSTRING(P_BLOCK.BLOCKNAME, 1,CASE	WHEN PATINDEX('%[^0-9]%',P_BLOCK.BLOCKNAME) > 0 THEN 
												PATINDEX('%[^0-9]%',P_BLOCK.BLOCKNAME) - 1 
											ELSE LEN(P_BLOCK.BLOCKNAME) 
											END
											) AS INT) ASC, P_BLOCK.BLOCKNAME ASC
				

		END
	ELSE
		BEGIN

			--============================================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES WHEN SELECTED SCHEMES 
			--============================================================

			CREATE TABLE #PropertyInc
			(
				propertyId NVARCHAR(40) COLLATE SQL_Latin1_General_CP1_CI_AS
			)


			DECLARE PropertyCount_Cursor CURSOR FOR
			SELECT	SchemeId, BlockId, PropertyId
			FROM	F_PurchaseItemSCInfo
			WHERE   OrderItemId = @orderitemid AND IsActive = 1

			OPEN PropertyCount_Cursor
	

			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId

			WHILE @@FETCH_STATUS = 0
			BEGIN		

				IF @PropertyId != '-1'
				BEGIN

					INSERT INTO #PropertyInc (propertyId)
					VALUES (@PropertyId)
			
				END
				ELSE IF @BlockId != -1 AND @BlockId IS NOT NULL
				BEGIN

					INSERT INTO #PropertyInc (propertyId)
					SELECT PROPERTYID
					FROM   P__PROPERTY 
					WHERE  BLOCKID = @BlockId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

				END
				ELSE IF @SchemeId != -1
				BEGIN

					INSERT INTO #PropertyInc (propertyId)
					SELECT PROPERTYID
					FROM   P__PROPERTY 
					WHERE  SCHEMEID = @SchemeId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

				END

				
			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId
			END

			CLOSE  PropertyCount_Cursor
			DEALLOCATE PropertyCount_Cursor

			
			SELECT	 SCHEMEBLOCK.BLOCKID as BlockId, BlockName AS Address
			FROM	(SELECT DISTINCT BLOCKID
					 FROM	#PropertyInc
							INNER JOIN P__PROPERTY PP ON  #PropertyInc.PROPERTYID = PP.PROPERTYID
					 WHERE PP.SCHEMEID = @filterSchemeId 
					) SCHEMEBLOCK
					INNER JOIN P_BLOCK ON SCHEMEBLOCK.BLOCKID = P_BLOCK.BLOCKID			
			ORDER BY CAST(SUBSTRING(P_BLOCK.BLOCKNAME, 1,CASE	WHEN PATINDEX('%[^0-9]%',P_BLOCK.BLOCKNAME) > 0 THEN 
												PATINDEX('%[^0-9]%',P_BLOCK.BLOCKNAME) - 1 
											ELSE LEN(P_BLOCK.BLOCKNAME) 
											END
											) AS INT) ASC, P_BLOCK.BLOCKNAME ASC
			
			DROP TABLE #PropertyInc

		END


END
