USE [RSLBHALive]
GO


IF OBJECT_ID('dbo.[CM_GetCancelledOrder]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[CM_GetCancelledOrder] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[CM_GetCancelledOrder]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',		
		@offset int = 1,
		@limit  int = 100,		 
		@sortColumn varchar(500) = 'OrderId', 
		@sortOrder varchar (5) = 'DESC',		
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        @filterCriteria varchar(200)='',
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
         
		@ToBeArrangedStatusId  int
		--Paging Formula
		 
		
	 
		SELECT  @ToBeArrangedStatusId =  StatusId FROM CM_Status WHERE TITLE = 'To Be Allocated'
		--=====================Search Criteria===============================
		SET @searchCriteria = '1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
			BEGIN						
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( S.SCHEMENAME LIKE ''%' + @searchText + '%'''			
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.BLOCKNAME LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR I.ItemName LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.POSTCODE LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR O.NAME LIKE ''%' + @searchText + '%'')'
				
				
			END	
		 -- IF(@schemeId > 0)
		 --BEGIN
			-- SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  SI.SchemeId = ' + convert(varchar(10),@schemeId)
			-- SET @searchCriteria = @searchCriteria + CHAR(10) +' OR  B.SchemeId = ' + convert(varchar(10),@schemeId)  	  	
		 --END
		--IF(@blockId > 0)
		-- BEGIN
		--	 SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  SI.BlockId = ' + convert(varchar(10),@blockId)  	
		-- END
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT  top ('+convert(varchar(10),@limit)+')
		 cw.PurchaseOrderId as OrderId,
		CONVERT(NVARCHAR(20),C.CancelOn,103)as CancelDate,
		''PO ''+cast(cw.PurchaseOrderId as NVARCHAR(10)) as PONumber,
		ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS CancelledBy,
		C.Reason, O.NAME as Contractor,
		ISNULL(S.SCHEMENAME,''-'') AS Scheme, 
		ISNULL(B.BLOCKNAME,''-'') AS Block,I.ItemName As  Attribute
		'

		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM  CM_ContractorWork cw
		INNER JOIN CM_Cancelled C on cw.CMContractorId = c.CMContractorId
		Cross APPLY(Select top 1 * from CM_ServiceItems_History where CM_ServiceItems_History.PORef = cw.PurchaseOrderId Order BY CM_ServiceItems_History.ServiceItemHistoryId DESC )SI
		LEFT JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID
		LEFT JOIN   P_SCHEME S ON SI.SchemeId=S.SCHEMEID Or B.SchemeId=S.SCHEMEID
		INNER JOIN PA_ITEM I ON SI.ItemId = I.ItemID
		INNER JOIN S_ORGANISATION O ON SI.ContractorId=O.ORGID
		INNER JOIN E__EMPLOYEE E on C.CancelBy = E.EMPLOYEEID'

		--============================Order Clause==========================================
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		 
		--IF(@getOnlyCount=0)
		--BEGIN			
			EXEC (@finalQuery)
		--END
		--print(@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
