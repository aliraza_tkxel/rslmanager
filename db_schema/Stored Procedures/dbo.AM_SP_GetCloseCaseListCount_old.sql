SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[AM_SP_GetCloseCaseListCount_old] 
		@caseOwnedBy int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,				
		@allRegionFlag	bit,
		@allCaseOwnerFlag	bit,
		@allSuburbFlag	bit,
		@statusTitle  varchar(100),
        @surname varchar(50)
AS
BEGIN
	declare @RegionSuburbClause varchar(8000)
	declare @query varchar(8000)


	IF(@regionId = 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'
	END
	ELSE IF(@regionId > 0 and @suburbId = 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
	END

SET @query =
	'SELECT COUNT(*) as recordCount FROM(SELECT COUNT(*) as recordCount

		  FROM   AM_Action INNER JOIN
					  AM_Case ON AM_Action.ActionId = AM_Case.ActionId INNER JOIN
					  AM_Status ON AM_Case.StatusId = AM_Status.StatusId INNER JOIN
					  AM_Customer_Rent_Parameters customer ON AM_Case.TenancyId = customer.TENANCYID INNER JOIN											  
					  C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID INNER JOIN
					  P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID

			Where (AM_Case.CaseOfficer = (case when 0 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseOfficer ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END)
								OR AM_Case.CaseManager = (case when 0 = ' +  convert(varchar(10),@caseOwnedBy )+ ' THEN AM_Case.CaseManager ELSE ' +  convert(varchar(10),@caseOwnedBy )+ ' END) ) 
						  AND  '+ @RegionSuburbClause +'
						  AND (customer.LASTNAME = case when '''' = '''+ @surname +''' then customer.LASTNAME else '''+ @surname +''' end) 
						  AND AM_Case.IsActive = 0
						  AND (AM_Status.Title = case when '''' = '''+ @statusTitle  +''' then AM_Status.Title else '''+ @statusTitle  +''' end)
                          GROUP BY AM_Case.TenancyId) as TEMP'

PRINT(@query);
EXEC(@query);


END







GO
