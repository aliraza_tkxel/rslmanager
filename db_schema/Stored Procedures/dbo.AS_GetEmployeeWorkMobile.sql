USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyEmployeeEmail]    Script Date: 14/04/2017 09:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- EXEC AS_GetEmployeeWorkMobile
		--@@employeeId=1226
		
-- Author:		Saud Ahmed
-- Create date: 14/04/2017
-- Description:	<This stored procedure returns the Operative Work Mobile>
-- [AS_GetEmployeeWorkMobile] 1226	

-- =============================================

IF OBJECT_ID('dbo.AS_GetEmployeeWorkMobile') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetEmployeeWorkMobile AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetEmployeeWorkMobile]
( 
	@employeeId nvarchar(50) = ''
)
AS
BEGIN

SELECT	ISNULL(WORKDD,'N/A' ) WORKDD
FROM	E_CONTACT 
WHERE	EMPLOYEEID = @employeeId

END

