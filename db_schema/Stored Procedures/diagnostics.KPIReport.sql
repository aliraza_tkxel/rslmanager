SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [diagnostics].[KPIReport]

AS 

BEGIN 

DECLARE @Month INT = MONTH(DATEADD(m,-1,GETDATE()))
DECLARE @YearStartDate DATETIME
DECLARE @YearEndDate DATETIME

SELECT @YearStartDate = StartDate, @YearEndDate = EndDate FROM diagnostics.CurrentFiscalYear()

-- Emergency
SELECT  DATENAME(month, CompletedDate) AS [Month] ,
        ISNULL(SupplierName, '') AS NAME ,
        OPERATIVE ,
        JobSheetNumber ,
        DEVELOPMENTNAME AS [Scheme] ,
        PROPERTYID AS [Property] ,
        [FaultDescription] AS [Repair/Fault] ,
        SUBMITDATE AS LoggedDate ,
        DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                        AppointmentDate)) AS AppointmentEndDateTime ,
        DATEDIFF(mi, SUBMITDATE,
                 DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                         DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                                 AppointmentDate))) - ( 86400 / 60 ) AS [Logged vs AppointmentEndDate] ,
        AppointmentDate AS AppointmentDate ,
        StartTime ,
        EndTime ,
        CompletedDate AS CompletionDate ,
        DATEDIFF(Day, SUBMITDATE, CompletedDate) AS [Days] ,
        PATCH ,
        STOCKTYPE
FROM    dbo.FL_FAULT_LIST_COMPLETED_DETAILS
WHERE   CompletedDate >= @YearStartDate
        AND CompletedDate <= @YearEndDate
        AND MONTH(CompletedDate) = @Month
        AND PriorityId IN ( 3, 4 ) -- Emergency
ORDER BY MONTH(CompletedDate)

-- Routine
SELECT  DATENAME(month, CompletedDate) AS [Month] ,
        ISNULL(SupplierName, '') AS NAME ,
        OPERATIVE ,
        JobSheetNumber ,
        DEVELOPMENTNAME AS [Scheme] ,
        PROPERTYID AS [Property] ,
        [FaultDescription] AS [Repair/Fault] ,
        SUBMITDATE AS LoggedDate ,
        DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                        AppointmentDate)) AS AppointmentEndDateTime ,
        DATEDIFF(mi, SUBMITDATE,
                 DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                         DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                                 AppointmentDate))) - 40320 AS [Logged vs AppointmentEndDate] ,
        AppointmentDate AS AppointmentDate ,
        StartTime ,
        EndTime ,
        CompletedDate AS CompletionDate ,
        DATEDIFF(Day, SUBMITDATE, CompletedDate) AS [Days] ,
        PATCH ,
        STOCKTYPE
FROM    FL_FAULT_LIST_COMPLETED_DETAILS
WHERE   CompletedDate >= @YearStartDate
        AND CompletedDate <= @YearEndDate
        AND MONTH(CompletedDate) = @Month
        AND PriorityId IN ( 1 ) -- Routine
ORDER BY MONTH(CompletedDate)

-- No Entries
SELECT  COUNT(DISTINCT FaultLogId)
FROM    dbo.FL_FAULT_nOeNTRY
WHERE   RecordedOn >= @YearStartDate
        AND RecordedOn <= @YearEndDate
        AND MONTH(RecordedOn) = @Month

END 
GO
