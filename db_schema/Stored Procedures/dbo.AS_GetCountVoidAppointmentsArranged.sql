USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_VoidAppointmentsArranged]    Script Date: 03/14/2016 11:45:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =============================================
EXEC	[dbo].[AS_GetCountVoidAppointmentsArranged]
		@FuelType = N'',
		@PatchId = -1,
		@DevelopmentId = -1

-- ============================================= */


IF OBJECT_ID('dbo.AS_GetCountVoidAppointmentsArranged') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCountVoidAppointmentsArranged AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetCountVoidAppointmentsArranged] 
(
	@FuelType varchar(25) = '',	
	@PatchId int = -1,
	@DevelopmentId	int = -1
)
AS
BEGIN

		--THESE PARAMETERS WILL BE USED FOR SEARCH
		DECLARE @SEARCHEDTEXT nVARCHAR(MAX),		
		
		--PARAMETERS WHICH WOULD HELP IN SORTING AND PAGING
		@PAGESIZE INT = 30,
		@PAGENUMBER INT = 1,
		@SORTCOLUMN VARCHAR(50) = 'JOURNALID',
		@SORTORDER VARCHAR (5) = 'DESC'
		
		
	DECLARE @SelectClause nvarchar(MAX),
        @fromClause   nvarchar(MAX),
        @whereClause  nvarchar(MAX),	        
        @orderClause  nvarchar(MAX),	
        @mainSelectQuery nvarchar(MAX),        
        @rowNumberQuery nvarchar(MAX),
        @finalQuery nvarchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria nvarchar(MAX),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- BEGIN BUILDING SEARCHCRITERIA CLAUSE
		-- THESE CONDITIONS WILL BE ADDED INTO WHERE CLAUSE BASED ON SEARCH CRITERIA PROVIDED
		
		SET @SEARCHCRITERIA = ' 1=1 '

		IF(@FuelType <> '' AND @FuelType IS NOT NULL AND @FuelType <> 'Please Select')
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND T.PARAMETERVALUE = ''' + @FuelType	+ ''''+CHAR(10)
		END
		
		IF (@PatchId > 0 )	--NOT EQUALS TO 0 OR 1			
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ( PATCH.PATCHID = ' + CONVERT(VARCHAR(10),@PatchId) + ' ) '
		END
		
		IF (@DevelopmentId > 0)
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND S.SCHEMEID = ' + CONVERT(VARCHAR(10),@DevelopmentId) + CHAR(10)
		END
			
		--THESE CONDITIONS W�LL BE USED IN EVERY CASE
		
		SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ( ( P.STATUS = 1 AND (P.SUBSTATUS <> 21 OR P.SUBSTATUS IS NULL) ) OR 
		(P.Status=2 AND P.SUBSTATUS = 22 ) ) --And AS_APPOINTMENTS.IsVoid =1
		AND AS_APPOINTMENTS.APPOINTMENTSTATUS NOT IN (''InProgress'')
		AND P.PROPERTYTYPE NOT IN (SELECT PROPERTYTYPEID FROM P_PROPERTYTYPE WHERE DESCRIPTION IN (''Garage'',''Car Port'',''Car Space''))
		
		)TABS
		'
	
		-- END BUILDING SEARCHCRITERIA CLAUSE   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause			
		
		SET @SelectClause = 'SELECT COUNT(TABS.PROPERTYID) FROM (SELECT TOP (' + CONVERT(NVARCHAR(10),@limit) + ') 
							AS_APPOINTMENTS.JSGNUMBER AS JSGNUMBER
							,CONVERT(varchar,AS_APPOINTMENTS.APPOINTMENTDATE,103)+'' ''+AS_APPOINTMENTS.APPOINTMENTSTARTTIME+''-''+ AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENT
							,AS_APPOINTMENTS.ASSIGNEDTO
							,AS_APPOINTMENTS.LOGGEDDATE as LOGGEDDATE,
							LEFT(E__EMPLOYEE.FIRSTNAME, 1)+LEFT(E__EMPLOYEE.LASTNAME,1) AS ENGINEER,
							ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'''') +'' ''+ ISNULL(P.ADDRESS3,'''')+ ISNULL('' , ''+P.TOWNCITY,'''')  AS ADDRESS ,  
							P.HouseNumber as HouseNumber,
							P.ADDRESS1 as ADDRESS1,
							P.ADDRESS2 as ADDRESS2,
							P.ADDRESS3 as ADDRESS3,							
							P.POSTCODE as POSTCODE,
							CONVERT(varchar,DATEADD(YEAR,1,CP12.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,CP12.ISSUEDATE)) AS DAYS,
							P_FUELTYPE.FUELTYPE AS FUEL,
							--ISNULL(PV.ValueDetail, ''-'')AS FUEL,
							P.PROPERTYID,
							T.TENANCYID,
							AS_APPOINTMENTS.APPOINTMENTID,
							AS_APPOINTMENTS.JournalId,							
							Case AS_Status.Title WHEN ''No Entry'' then ''No Entry (''+ convert(varchar(10),
								(SELECT Count(JOURNALHISTORYID)
								FROM AS_JournalHistory 
								Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
								AND AS_JournalHistory.StatusId=3 AND 
								YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
								))+'')'' 
							else 
								AS_Status.Title end AS StatusTitle,
								
							ISNULL(T.MOBILE,''N/A'') as Mobile,
							T.NAME AS NAME,
							
							ISNULL(ST.DESCRIPTION, ''N/A'') AS PropertyStatus,
							PATCH.PatchId as PatchId,
							PATCH.Location as PatchName,
							T.TELEPHONE as Telephone,
							AS_APPOINTMENTS.APPOINTMENTDATE as AppointmentSortDate,
							CP12.ISSUEDATE as LGSRDate
							,AS_APPOINTMENTS.NOTES as Notes
							,T.CUSTOMERID as CustomerId
							,T.TerminationDate as TERMINATION
							'
				
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause				
		
		SET @fromClause = CHAR(10) + ' FROM P__PROPERTY P  

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
LEFT JOIN E_PATCH PATCH ON P.PATCH = PATCH.PATCHID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
LEFT JOIN P_LGSR CP12 ON P.PROPERTYID = CP12.PROPERTYID
LEFT JOIN P_FUELTYPE ON P.FUELTYPE = P_FUELTYPE.FUELTYPEID

Cross Apply(
			Select Max(j.JOURNALID) as journalID from C_JOURNAL j 
			where j.PropertyId=P.PropertyID AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
			GROUP BY PROPERTYID
			) as CJournal
			
CROSS APPLY (
			Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination
			
			Cross Apply (
				SELECT P.PROPERTYID,
				Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
				Case 
					When M.ReletDate IS NULL Then 
						CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
					ELSE
						Convert(Varchar(50), M.ReletDate,103)
				END as ReletDate,
				J.JOURNALID,
				CT.TENANCYID,
				ISNULL(A.TEL,''N/A'') AS TELEPHONE,
				C.FIRSTNAME  + '' '' + C.LASTNAME AS NAME,
				C.CUSTOMERID,
				A.MOBILE,
				PROP_ATTRIB.PARAMETERVALUE
				
				
			FROM P__PROPERTY P
			LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES PROP_ATTRIB ON PROP_ATTRIB.PROPERTYID = P.PROPERTYID	AND
										PROP_ATTRIB.ITEMPARAMID =
										(
											SELECT
												ItemParamID
											FROM
												PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
											WHERE
												ParameterName = ''Heating Fuel''
												AND ItemName = ''Heating''
												AND PARAMETERVALUE = ''Mains Gas''
										)
			INNER JOIN C_JOURNAL J ON CJournal.journalID=J.JOURNALID
			INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID
			INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
			INNER JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1
			INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
			INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
			INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
	
			 ) As  T -- ON P.PROPERTYID=T.PROPERTYID

INNER JOIN (
	SELECT P2.PROPERTYID,J2.JOURNALID, J2.STATUSID,APP.APPOINTMENTID FROM P__PROPERTY P2 
	INNER JOIN AS_JOURNAL J2 ON P2.PROPERTYID = J2.PROPERTYID
	INNER JOIN AS_APPOINTMENTS APP ON APP.JournalId = J2.JOURNALID
	WHERE J2.ISCURRENT = 1 --AND J2.STATUSID = 2
	GROUP BY P2.PROPERTYID,J2.JOURNALID,J2.STATUSID,APP.APPOINTMENTID
) AS AS_JOURNAL ON AS_JOURNAL.PROPERTYID = P.PROPERTYID

INNER JOIN AS_STATUS ON AS_STATUS.STATUSID = AS_JOURNAL.STATUSID
INNER JOIN AS_APPOINTMENTS ON AS_JOURNAL.APPOINTMENTID = AS_APPOINTMENTS.APPOINTMENTID
INNER JOIN E__EMPLOYEE ON AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
										'
		
		-- End building From clause
		--======================================================================================== 																  
				
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause
		PRINT (@mainSelectQuery)
		EXEC (@mainSelectQuery)
		
		-- End building the main select Query
		--========================================================================================																																			

										
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
								
END
