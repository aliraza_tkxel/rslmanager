-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Adil>
-- Create date: <Create Date,,>
-- Description:	<Returns the types of Provisions>
-- =============================================

IF OBJECT_ID('dbo.[PDR_GetProvisionCategories]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetProvisionCategories] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PDR_GetProvisionCategories]
	
AS
BEGIN

	SELECT * from PDR_ProvisionCategories
	ORDER BY CategoryName ASC
END
GO
