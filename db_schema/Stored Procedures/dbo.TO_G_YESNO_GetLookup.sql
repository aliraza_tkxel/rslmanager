SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TO_G_YESNO_GetLookup]
/* ===========================================================================
 '   NAME:           TO_G_YESNO_GetLookup
 '   DATE CREATED:   09 May 2013
 '   CREATED BY:     Nataliya Alexander
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get communication records from G_YESNO table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
	[YESNOID] AS id ,
    [DESCRIPTION] AS val
FROM   
	G_YESNO
ORDER BY 
	[DESCRIPTION] ASC


GO
