USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[CM_AssignCyclicalServicesToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CM_AssignCyclicalServicesToContractor] AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[CM_AssignCyclicalServicesToContractor]    Script Date: 	29/08/2017 14:21:37 ******/

-- =============================================
-- Author:		Ali Raza
-- Create date:	29/08/2017
-- Description:	Assign Cyclical Services to  Contractor
-- WebPage:		PropertyDataRestructure/Views/CyclicalServices/CyclicalServices.aspx
-- =============================================


ALTER  PROCEDURE [dbo].[CM_AssignCyclicalServicesToContractor] (
	-- Add the parameters for the stored procedure here
	
	@ContractorWorks AS CM_ContractorWorkType READONLY,
	@ContractorWorkDetail AS CM_ContractorWorkDetail READONLY,
	@isSaved BIT = 0 OUTPUT
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
BEGIN TRY
	
	
	
-- =====================================================
-- Contractor Works cursor variables
-- =====================================================	

	DECLARE	@ServiceItemId int,@ContractorId int,@ContactId int,@ExpenditureId int,@ContractNetValue money,@VatId int,@Vat money,@ContractGrossValue money,
		@SendApprovalLink bit,@Commence datetime,@AssignedBy int,@AssignedDate datetime,@POStatus int,@RequiredWorks nvarchar(1000) 
	
DECLARE ContractorWorks_cursor CURSOR FOR  
SELECT  * from @ContractorWorks 

OPEN ContractorWorks_cursor   
FETCH NEXT FROM ContractorWorks_cursor INTO @ServiceItemId ,@ContractorId ,@ContactId ,@ExpenditureId ,@ContractNetValue ,@VatId ,@Vat ,@ContractGrossValue,
		@SendApprovalLink ,@Commence ,@AssignedBy ,@AssignedDate ,@POStatus,@RequiredWorks   

WHILE @@FETCH_STATUS = 0   
BEGIN  
-- =====================================================
-- General Purpose Variable
-- =====================================================
	DECLARE @CurrentDateTime AS datetime2 = GETDATE()
	DECLARE @BlockId AS INT
	DECLARE @DevelopmentId AS INT
	DECLARE @SchemeId AS INT , @ItemName NVARCHAR(500)

---======================================================================================================= 
     SELECT @BlockId= SI.BlockId,@SchemeId=SI.SchemeId, @ItemName=I.ItemName,@DevelopmentId=D.DEVELOPMENTID FROM CM_ServiceItems SI
     LEFT JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID
	 LEFT JOIN P_SCHEME S ON SI.SchemeId=S.SCHEMEID Or B.SchemeId=S.SCHEMEID
	 Left JOIN PDR_DEVELOPMENT D on S.DEVELOPMENTID= D.DEVELOPMENTID
	 INNER JOIN PA_ITEM I ON SI.ItemId = I.ItemID
     WHERE ServiceItemId = @ServiceItemId
 -- =====================================================
-- Insert new Purchase Order
-- =====================================================

	DECLARE @Active bit = 1, 
	@POTYPE int = (SELECT POTYPEID FROM F_POTYPE WHERE POTYPENAME = 'Repair') -- 2 = 'Repair'
	-- To get Identity Value of Purchase Order.
	, @purchaseOrderId int
	
	INSERT INTO [RSLBHALive].[dbo].[F_PURCHASEORDER] (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
	POTYPE, POSTATUS, POTIMESTAMP, BLOCKID, DEVELOPMENTID)
	VALUES (UPPER('Cyclical Works Order'), @CurrentDateTime, 'This purchase order was created for the '+ @ItemName +' attribute.', 
	@AssignedBy, @ContractorId, @Active, @POTYPE,@POStatus, @CurrentDateTime, @BlockId, @DevelopmentId)

	SET @purchaseOrderId = SCOPE_IDENTITY()
	
-- =====================================================
	-- Old table(s)
	-- Insert new P_WORKORDER
	-- =====================================================
	--DECLARE @Title nvarchar(50) = 'Cyclical Works Order/Assigned To Contractor'
	--DECLARE @WOSTATUS INT = 6-- 'IN PROGRESS
	--If (@POStatus = 0) SET @WOSTATUS = 12 --Pending
	--DECLARE @BIRTH_NATURE INT = 0 --'Automated Cyclical Item'
	--DECLARE @BIRTH_ENTITY INT = CASE WHEN @BlockId >0  THEN 2 ELSE 3 END
	--DECLARE @BIRTH_MODULE INT = 3 --'Automated Cyclical Item'
 


	--INSERT INTO P_WORKORDER (ORDERID, TITLE, WOSTATUS, CREATIONDATE, BIRTH_MODULE, BIRTH_ENTITY,
	--							BIRTH_NATURE, DEVELOPMENTID, BLOCKID,  SchemeId)
	--VALUES(@purchaseOrderId, @Title, @WOSTATUS, @CurrentDateTime, @BIRTH_MODULE, @BIRTH_ENTITY
	--, @BIRTH_NATURE, @DevelopmentId, @BlockId, @SchemeId)

	--DECLARE @WOID INT = SCOPE_IDENTITY()	
 --=============================================================================================================================================
 DECLARE @CMContractorId int
 INSERT INTO [RSLBHALive].[dbo].[CM_ContractorWork]
           ([ServiceItemId],[ContractorId],[ContactId],[ExpenditureId],[ContractNetValue],[VatId],[Vat],[ContractGrossValue],[SendApprovalLink],[Commence],[AssignedBy]
           ,[AssignedDate],[RequiredWorks],[PurchaseOrderId])
 VALUES(@ServiceItemId,@ContractorId,@ContactId,@ExpenditureId,@ContractNetValue,@VatId,@Vat,@ContractGrossValue,@SendApprovalLink,@Commence,@AssignedBy
           ,@AssignedDate,@RequiredWorks,@purchaseOrderId)          
               
 SET @CMContractorId = SCOPE_IDENTITY();
     
-- =====================================================
-- update data in CM_ServiceItems
-- =====================================================
	DECLARE @newstatus INT 	
	SELECT @newstatus = StatusId  FROM CM_Status where Title = 'Assigned'
	UPDATE CM_ServiceItems SET StatusId = @newstatus,PORef=@purchaseOrderId Where ServiceItemId=@ServiceItemId
     
 -- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================
Declare @ServicesItemId int,@CycleDate smalldatetime,@NetCost money,@VatType int,@VATCost money,@GrossCost money,@WorksRequired NVARCHAR(500)

DECLARE ContractorWorksDetail_cursor CURSOR FOR SELECT * FROM @ContractorWorkDetail Where ServiceItemId=@ServiceItemId
OPEN ContractorWorksDetail_cursor
    
 FETCH NEXT FROM ContractorWorksDetail_cursor INTO   
   @ServicesItemId ,@CycleDate,@NetCost ,@VatType ,@VATCost,@GrossCost ,@WorksRequired  
     
WHILE @@FETCH_STATUS = 0   
BEGIN      
     
-- =====================================================
--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
-- =====================================================

INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
VALUES (@purchaseOrderId, @ExpenditureId, @ItemName, @WorksRequired,@CurrentDateTime,  @NetCost, @VatType, @VATCost, @GrossCost, @AssignedBy, @ACTIVE, @POTYPE, @POStatus)

DECLARE @ORDERITEMID int = SCOPE_IDENTITY()
 
 -- =====================================================
--Insert Values in CM_ContractorWorkDetail for each work required 
-- =====================================================    
INSERT INTO [RSLBHALive].[dbo].[CM_ContractorWorkDetail]
           ([CMContractorId],[CycleDate],[NetCost],[VatId],[VAT],[GrossCost],[PurchaseOrderItemId])     
 VALUES(@CMContractorId,@CycleDate,@NetCost,@VatType,@VATCost,@GrossCost,@ORDERITEMID)    
     
     
     
  FETCH NEXT FROM ContractorWorksDetail_cursor INTO   
   @ServicesItemId ,@CycleDate,@NetCost ,@VatType ,@VATCost,@GrossCost ,@RequiredWorks  
       
END   
 -- =====================================================
--Close And Deallocate Inner cursor
-- ===================================================== 
CLOSE ContractorWorksDetail_cursor   
DEALLOCATE ContractorWorksDetail_cursor   
    
---======================================================================================================= 

FETCH NEXT FROM ContractorWorks_cursor INTO @ServiceItemId ,@ContractorId ,@ContactId ,@ExpenditureId ,@ContractNetValue ,@VatId ,@Vat ,@ContractGrossValue,
		@SendApprovalLink ,@Commence ,@AssignedBy ,@AssignedDate ,@POStatus,@RequiredWorks    
END   
 -- =====================================================
--Close And Deallocate parent cursor
-- ===================================================== 
CLOSE ContractorWorks_cursor   
DEALLOCATE ContractorWorks_cursor

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;



END