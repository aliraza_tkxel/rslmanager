USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Exec [dbo].[AS_GetCountMIData]  
--  @PatchId = 18,  
--  @DevelopmentId = 1  
-- Author:  Hussain Ali  
-- Create date: 28/11/2012  
-- Description: This SP returns the MI data for the dashboard  
-- Webpage: dashboard.aspx  
  
-- =============================================  

IF OBJECT_ID('dbo.AS_GetCountMIData') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCountMIData AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[AS_GetCountMIData] ---1, -1  
 @PatchId  int,  
 @DevelopmentId int,
 @FuelType Varchar(8000)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
   
 -- Declaring the variables to be used in this query  
 DECLARE @propertiesQuery varchar(8000),  
   @boilerQuery  varchar(8000),  
   @noCertificates  varchar(8000),  
   @dueIn8To12Weeks varchar(8000),  
   @whereClause  varchar(8000),  
   @query    varchar(8000)           
  
 -- Initializing the variables  
 SET @whereClause  = CHAR(10)  
 SET @propertiesQuery = CHAR(10)  
 SET @boilerQuery  = CHAR(10)  
 SET @noCertificates  = CHAR(10)  
 SET @dueIn8To12Weeks = CHAR(10)  
   
 -- Setting the where clause to cater for the patch and development id  
 if (@PatchId <> -1 OR @DevelopmentId <> -1)  
 BEGIN  
  SET @whereClause = @whereClause + 'AND' + CHAR(10)  
  END  
   
 if (@PatchId <> -1)  
 BEGIN  
  SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)  
  END  
    
  if (@PatchId <> -1 AND @DevelopmentId <> -1)  
 BEGIN  
  SET @whereClause = @whereClause + 'AND' + CHAR(10)  
  END  
  
 if (@DevelopmentId <> -1)  
 BEGIN  
  SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)  
  END  
  

if @FuelType = 'Gas'
begin
 -- Initializing the sub query to fetch the count of all properties  
 SET @propertiesQuery = 'SELECT COUNT(P__PROPERTY.PROPERTYID) AS Properties  
       FROM P__PROPERTY  
        INNER JOIN AS_JOURNAL J ON J.PROPERTYID=P__PROPERTY.PROPERTYID AND J.ISCURRENT=1  
        LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P__PROPERTY.PROPERTYID  
                  AND A.ITEMPARAMID =  (SELECT ItemParamID FROM PA_ITEM_PARAMETER  
                     INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID  
                     INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID  
                     WHERE ParameterName = ''Heating Fuel'' AND ItemName = ''Heating'')  
       WHERE P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND ASSETTYPE <> 10   
        AND A.VALUEID  = 172' + CHAR(10)+ @whereClause  
        -- AND P__PROPERTY.FUELTYPE = 1  -- #10661: filter on new Heating Property Attribute, not Old Portfolio location (Attribute Value 172: Mains Gas)  
   
 -- Initializing the sub query to fetch the count of all boilers  
 SET @boilerQuery = 'SELECT 0'   
         
 -- Initializing the sub query to fetch the count of all properties with no certificates  
 SET @noCertificates = 'SELECT   
        COUNT(P__PROPERTY.[PROPERTYID])  
         FROM dbo.P__PROPERTY  
        LEFT JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) P_LGSR ON P_LGSR.PROPERTYID=P__PROPERTY.PROPERTYID 
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID 
         WHERE   
        P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND  
        P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND PA_Parameter_value.ValueID = 172
        AND  
        ((P_LGSR.CP12NUMBER IS NULL AND P_LGSR.ISSUEDATE IS NULL) OR   
        (P_LGSR.CP12NUMBER IS NOT NULL AND P_LGSR.ISSUEDATE IS NULL))' + CHAR(10)+ @whereClause         
                 
 -- Initializing the sub query to fetch the count of all properties with certificates due to expire in 8 to 12 weeks  
 SET @dueIn8To12Weeks = 'SELECT   
        COUNT(P__PROPERTY.[PROPERTYID])  
          FROM dbo.P__PROPERTY  
        INNER JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) P_LGSR ON P_LGSR.PROPERTYID=P__PROPERTY.PROPERTYID
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID 
          WHERE   
        P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND  
        P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND PA_Parameter_value.ValueID = 172
        AND  
        DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>56 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<=84' + CHAR(10)+ @whereClause   
end  

if @FuelType = 'Oil'
begin
	 -- Initializing the sub query to fetch the count of all properties  
 SET @propertiesQuery = 'SELECT COUNT(P__PROPERTY.PROPERTYID) AS Properties  
       FROM P__PROPERTY  
        INNER JOIN AS_JOURNAL J ON J.PROPERTYID=P__PROPERTY.PROPERTYID AND J.ISCURRENT=1  
        LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P__PROPERTY.PROPERTYID  
                  AND A.ITEMPARAMID =  (SELECT ItemParamID FROM PA_ITEM_PARAMETER  
                     INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID  
                     INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID  
                     WHERE ParameterName = ''Heating Fuel'' AND ItemName = ''Heating'')  
       WHERE P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND ASSETTYPE <> 10   
        AND A.VALUEID  = 174' + CHAR(10)+ @whereClause  
        -- AND P__PROPERTY.FUELTYPE = 1  -- #10661: filter on new Heating Property Attribute, not Old Portfolio location (Attribute Value 172: Mains Gas)  
   
 -- Initializing the sub query to fetch the count of all boilers  
 SET @boilerQuery = 'SELECT 0'   
         
 -- Initializing the sub query to fetch the count of all properties with no certificates  
 SET @noCertificates = 'SELECT   
        COUNT(P__PROPERTY.[PROPERTYID])  
         FROM dbo.P__PROPERTY  
        LEFT JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) P_LGSR ON P_LGSR.PROPERTYID=P__PROPERTY.PROPERTYID  
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID 
         WHERE   
        P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND  
        P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
           
       AND PA_Parameter_value.ValueID = 174
        AND  
        ((P_LGSR.CP12NUMBER IS NULL AND P_LGSR.ISSUEDATE IS NULL) OR   
        (P_LGSR.CP12NUMBER IS NOT NULL AND P_LGSR.ISSUEDATE IS NULL))' + CHAR(10)+ @whereClause         
                 
 -- Initializing the sub query to fetch the count of all properties with certificates due to expire in 8 to 12 weeks  
 SET @dueIn8To12Weeks = 'SELECT   
        COUNT(P__PROPERTY.[PROPERTYID])  
          FROM dbo.P__PROPERTY  
        INNER JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) P_LGSR ON P_LGSR.PROPERTYID=P__PROPERTY.PROPERTYID 
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID  
          WHERE   
        P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND  
        P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND PA_Parameter_value.ValueID = 174
        AND  
        DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>56 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<=84' + CHAR(10)+ @whereClause   
end


if @FuelType = 'Alternative Servicing'
begin
		 -- Initializing the sub query to fetch the count of all properties  
 SET @propertiesQuery = 'SELECT COUNT(P__PROPERTY.PROPERTYID) AS Properties  
       FROM P__PROPERTY  
        INNER JOIN AS_JOURNAL J ON J.PROPERTYID=P__PROPERTY.PROPERTYID AND J.ISCURRENT=1  
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID  

       WHERE P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND PA_Parameter_value.IsAlterNativeHeating  = 1' + CHAR(10)+ @whereClause  
        -- AND P__PROPERTY.FUELTYPE = 1  -- #10661: filter on new Heating Property Attribute, not Old Portfolio location (Attribute Value 172: Mains Gas)  
   
 -- Initializing the sub query to fetch the count of all boilers  
 SET @boilerQuery = 'SELECT 0'   
         
 -- Initializing the sub query to fetch the count of all properties with no certificates  
 SET @noCertificates = 'SELECT   
        COUNT(P__PROPERTY.[PROPERTYID])  
         FROM dbo.P__PROPERTY  
        LEFT JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) P_LGSR ON P_LGSR.PROPERTYID=P__PROPERTY.PROPERTYID  
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID 
         WHERE   
        P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND  
        P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
           
       AND PA_Parameter_value.IsAlterNativeHeating  = 1
        AND  
        ((P_LGSR.CP12NUMBER IS NULL AND P_LGSR.ISSUEDATE IS NULL) OR   
        (P_LGSR.CP12NUMBER IS NOT NULL AND P_LGSR.ISSUEDATE IS NULL))' + CHAR(10)+ @whereClause         
                 
 -- Initializing the sub query to fetch the count of all properties with certificates due to expire in 8 to 12 weeks  
 SET @dueIn8To12Weeks = 'SELECT   
        COUNT(P__PROPERTY.[PROPERTYID])  
          FROM dbo.P__PROPERTY  
        INNER JOIN (SELECT PROPERTYID,CP12NUMBER,ISSUEDATE FROM P_LGSR) P_LGSR ON P_LGSR.PROPERTYID=P__PROPERTY.PROPERTYID 
		Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
		Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID  
          WHERE   
        P__PROPERTY.STATUS NOT IN (9,5,6)   
        AND  
        P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)  
        AND PA_Parameter_value.IsAlterNativeHeating  = 1
        AND  
        DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>56 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<=84' + CHAR(10)+ @whereClause  
end
 -- Building the Query   
 SET @query = 'Select (' + @propertiesQuery + ') as Properties, (' + @boilerQuery + ') as Boilers, (' + @noCertificates + ') as NoCertificates, (' + @dueIn8To12Weeks + ') as DueIn8To12Weeks'  
   
 -- Printing the query for debugging  
 print @query   
   
 -- Executing the query  
    EXEC (@query)  
END  
  
  