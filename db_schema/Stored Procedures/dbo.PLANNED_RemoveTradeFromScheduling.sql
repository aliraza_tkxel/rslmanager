SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@isSaved bit
--EXEC	[dbo].[PLANNED_RemoveTradeFromScheduling]
--		@pmo = 54,
--		@compTradeId = 23,
--		@isRemoved = @isRemoved OUTPUT
--		SELECT	@isSaved as N'@isSaved'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,27 August,2014>
-- Description:	<Description,,This procedure 'll delete the pending appointments>
-- =============================================
CREATE  PROCEDURE [dbo].[PLANNED_RemoveTradeFromScheduling] 
	@pmo int
	,@compTradeId int
	,@duration float
	,@isRemoved bit OUT
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY	
		
		INSERT INTO [PLANNED_REMOVED_SCHEDULING_TRADES]([COMPTRADEID],[JOURNALID],[DURATION])
		VALUES	(@compTradeId,@pmo,@duration)
														
		set @isRemoved = 1
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isRemoved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isRemoved = 1
	END 
    
END
GO
