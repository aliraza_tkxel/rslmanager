SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE diagnostics.RemoveRebate
(
	@TENANCYID INT
)
AS 
BEGIN 

	SET NOCOUNT ON 
	
	DECLARE @JOURNALID INT
	DECLARE @JOURNALCOUNT INT

	SELECT @JOURNALCOUNT = COUNT(*) FROM F_RENTJOURNAL WHERE TENANCYID=@TENANCYID AND STATUSID=13 AND PAYMENTTYPE= 12 AND ITEMTYPE = 1

	IF @JOURNALCOUNT = 1 
	BEGIN 
		SET @JOURNALID = (SELECT [JOURNALID] FROM F_RENTJOURNAL WHERE TENANCYID = @TENANCYID AND STATUSID = 13 AND PAYMENTTYPE = 12 AND ITEMTYPE = 1)
		SELECT @JOURNALID AS [JOURNALID]
	END
	ELSE
	BEGIN
		RAISERROR('Expected Rent Journal Count: 1. Actual Rent Journal Count: %d.', 16, 1, @JOURNALID)	
		RETURN 
	END 

	SET NOCOUNT OFF

	PRINT 'Removing credit line and debit line entries.'

	DELETE FROM NL_JOURNALENTRYCREDITLINE WHERE TXNID =(SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONID = @JOURNALID)--TXNID=579497
	DELETE FROM NL_JOURNALENTRYDEBITLINE WHERE TXNID =(SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONID = @JOURNALID)--TXNID=579497

	PRINT 'Removing nominal ledger entry.'

	DELETE FROM NL_JOURNALENTRY WHERE TRANSACTIONID = @JOURNALID

	PRINT 'Removing F_RENTJOURNAL_IR entry.'

	DELETE FROM F_RENTJOURNAL_IR WHERE JOURNALID = @JOURNALID

	PRINT 'Removing RGE record from C_TERMINATIONSUM if it has been inserted.'

	DELETE FROM C_TERMINATIONSUM WHERE JOURNALID =@JOURNALID

	PRINT 'Removing records from F_RENTJOURNAL.'

	DELETE FROM F_RENTJOURNAL WHERE TENANCYID =@TENANCYID AND STATUSID=13 AND PAYMENTTYPE=12 AND ITEMTYPE=1 AND JOURNALID= @JOURNALID

END

GO
