SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE   procedure [dbo].[sp_getunitsummary]
@orgid int 
as
SELECT 
MIN(dbo.H_SUPPORT_UNITS.[TIMESTAMP]) AS StartDate, 
MAX(dbo.H_SUPPORT_UNITS.[TIMESTAMP]) AS EndDate, 
Sum(H_SUPPORT_UNITS.debit) as unitsused, 
Sum(H_SUPPORT_UNITS.credit) as totalunits, 
sum(H_SUPPORT_UNITS.credit)- Sum(H_SUPPORT_UNITS.debit)as totalleft 
from H_SUPPORT_UNITS
WHERE ORG_ID =@orgid and 
((pending = 1 and credit <> 0) or (debit <> 0)) 




GO
