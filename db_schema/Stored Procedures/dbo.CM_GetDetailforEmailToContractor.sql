USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[CM_GetDetailforEmailToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CM_GetDetailforEmailToContractor] AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[CM_GetDetailforEmailToContractor]    Script Date: 	29/08/2017 14:21:37 ******/

-- =============================================
-- Author:		Ali Raza
-- Create date:	29/08/2017
-- Description:	Assign Cyclical Services to  Contractor
-- WebPage:		PropertyDataRestructure/Views/CyclicalServices/CyclicalServices.aspx
-- =============================================
ALTER PROCEDURE [dbo].[CM_GetDetailforEmailToContractor]
	 @serviceItemId INT
	,@empolyeeId INT			
AS
BEGIN
--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email], ISNULL(C.WORKDD, '') AS DD
FROM E__EMPLOYEE E
INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @empolyeeId
--=================================================
--Get PO Detail(s)
--=================================================

Select cw.PurchaseOrderId, ISNULL(SCH.SCHEMENAME,'-') as SchemeName,ISNULL(B.BLOCKNAME,'-') AS BlockName,I.ItemName AS ServiceRequired,
'Every '+cast(s.Cycle AS NVARCHAR)+' '+ CT.CycleType AS CYCLE,CONVERT(NVARCHAR(20),s.ContractCommencement,103)AS Commencement,'�'+Cast(CONVERT(DECIMAL(10,2),s.CycleValue) AS NVARCHAR) AS CycleValue,
'�'+Cast(CONVERT(DECIMAL(10,2),cw.ContractNetValue) AS NVARCHAR) AS NetCost,'�'+Cast(CONVERT(DECIMAL(10,2),cw.Vat) AS NVARCHAR) AS vat,'�'+Cast(CONVERT(DECIMAL(10,2),cw.ContractGrossValue) AS NVARCHAR) AS GrossCost,
ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '')  AS OrderedBy,ISNULL(C.WORKDD, '') AS DD,ISNULL(C.WORKEMAIL, '') AS [Email]
FROM CM_ServiceItems s
INNER JOIN CM_ContractorWork cw ON s.ServiceItemId = cw.ServiceItemId and s.PORef = cw.PurchaseOrderId
INNER JOIN PA_ITEM I ON s.ItemId = I.ItemID
LEFT JOIN P_BLOCK B ON s.BlockId=B.BLOCKID
LEFT JOIN P_SCHEME SCH ON s.SchemeId=SCH.SCHEMEID OR B.SchemeId=SCH.SCHEMEID 
INNER JOIN PDR_CycleType CT ON s.CycleType=CT.CycleTypeId
INNER JOIN E__EMPLOYEE E ON cw.AssignedBy = E.EMPLOYEEID
INNER JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID
 
Where  s.ServiceItemId=@serviceItemId
--=================================================
--Get PI Detail(s)
--=================================================

Select ROW_NUMBER() OVER(ORDER BY cwd.PurchaseOrderItemId ASC)as [row],CONVERT(NVARCHAR(20),cwd.CycleDate,103) as CycleDate 
FROM CM_ServiceItems s
INNER JOIN CM_ContractorWork cw ON s.ServiceItemId = cw.ServiceItemId and s.PORef = cw.PurchaseOrderId
INNER JOIN CM_ContractorWorkDetail cwd ON cw.CMContractorId = cwd.CMContractorId
Where  s.ServiceItemId=@serviceItemId




END