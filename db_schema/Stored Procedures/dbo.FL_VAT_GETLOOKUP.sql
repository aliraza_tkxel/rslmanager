SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_VAT_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_VAT_GETLOOKUP
 '   DATE CREATED:   27th OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Vat Rates table which will be shown as lookup value in presentation pages
 '   IN:            No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT VATID AS id,VATNAME AS val
	FROM F_VAT
	ORDER BY VATID ASC













GO
