USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_AbsenseSickness]    Script Date: 6/6/2017 4:33:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Rehman
-- Create date: June 2, 2017
-- =============================================

IF OBJECT_ID('dbo.E_AbsenseSickness') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_AbsenseSickness AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_AbsenseSickness] 
	-- Add the parameters for the stored procedure here
	@empId INT,
	@fiscalYear SMALLDATETIME = null	
	
AS
BEGIN

DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME	
	DECLARE @DAYS_ABS int
																										  
SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE(@empId)
	
if(@fiscalYear is not null)
begin
	SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE_ByYear(@empId, @fiscalYear)
end
	
SELECT e.STARTDATE,e.RETURNDATE,ar.DESCRIPTION as Reason,s.DESCRIPTION as Status, n.DESCRIPTION as Nature,e.DURATION_TYPE as DURATION_TYPE, 
Case 
when e.returndate is null then dbo.EMP_SICKNESS_DURATION(j.EMPLOYEEID, e.STARTDATE, CAST(CAST(GETDATE() AS DATE) AS DATETIME), e.JOURNALID)  
else dbo.EMP_SICKNESS_DURATION(j.EMPLOYEEID, e.STARTDATE, e.RETURNDATE, e.JOURNALID)  end as DAYS_ABS, 
e.HOLTYPE as HolType,e.AnticipatedReturnDate as AnticipatedReturnDate, ar.SID as ReasonId, n.ITEMNATUREID as NatureId, s.ITEMSTATUSID as StatusId,e.ABSENCEHISTORYID as AbsenceHistoryId, e.NOTES as Notes
FROM E_JOURNAL j
inner join E_ABSENCE e on j.JOURNALID = e.JOURNALID
 AND e.ABSENCEHISTORYID = (
									SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
									WHERE JOURNALID = j.JOURNALID
								  ) 
INNER JOIN E_ABSENCEREASON ar on e.REASONID = ar.SID
INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
WHERE j.EMPLOYEEID = @empId and j.ITEMNATUREID = 1 and e.STARTDATE >= @BHSTART and (e.STARTDATE <= @BHEND or @fiscalYear is null)
ORDER by e.STARTDATE DESC

	
END
