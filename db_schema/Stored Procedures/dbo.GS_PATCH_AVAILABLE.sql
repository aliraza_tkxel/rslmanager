
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GS_PATCH_AVAILABLE]

-- EXEC GS_PATCH_AVAILABLE

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT E.PATCHID, E.LOCATION
FROM E_PATCH E 
			    --WHERE PATCHID NOT IN( 
			    --               		  SELECT ISNULL(SS.PATCHID,-1)
			   	--	                  FROM S_SCOPETOPATCHANDSCHEME SS
			    --           			  INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID 
			    --           			  WHERE S.RENEWALDATE >= DATEADD(WW,10,GETDATE()) 
			    --                         AND SS.DEVELOPMENTID IS NULL		
			 			--              ) 

END

GO
