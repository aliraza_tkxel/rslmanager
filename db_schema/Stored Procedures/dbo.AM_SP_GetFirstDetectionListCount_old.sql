SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AM_SP_GetFirstDetectionListCount_old] 
		    @postCode varchar(15),
			@caseOwnedById int=0,
            @regionId	int = 0,
			@suburbId	int = 0,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@surname		varchar(100)
AS
BEGIN

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN

	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = P__PROPERTY.PATCH'
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId )+ ') ' 
	END
ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN

		IF(@regionId = -1 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = '(P__PROPERTY.DEVELOPMENTID IN (SELECT DevelopmentId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR P__PROPERTY.PATCH IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'
		END
		ELSE IF(@regionId > 0 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) 
		END
		ELSE IF(@regionId > 0 and @suburbId > 0)
		BEGIN
			SET @RegionSuburbClause = 'P__PROPERTY.PATCH = ' + convert(varchar(10), @regionId) +' AND (P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') ' 
		END
ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P__PROPERTY.DEVELOPMENTID = ' + convert(varchar(10), @suburbId) 
    END
END

SET @query = ' SELECT COUNT(*) as recordCount FROM(
			SELECT COUNT(*) as recordCount		
			--SELECT P__PROPERTY.DEVELOPMENTID, P__PROPERTY.PATCH
			FROM         AM_FirstDetecionList 
						  --INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
						  --INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID				  
						  --INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
                          --INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
                          --INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
                  INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.TENANCYID = customer.TENANCYID
                  INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
				  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID				  
				  --INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
                  INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
			WHERE '+ @RegionSuburbClause+' AND dbo.AM_FN_CHECK_OWED_TO_BHA(ISNULL(customer.RentBalance, 0.0),ISNULL(customer.EstimatedHBDue, 0.0))=''true'' 
								AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId 
																			FROM AM_Case 
																			WHERE AM_Case.IsActive= ''true'' ) 		   
							   AND AM_FirstDetecionList.IsDefaulter = ''true''
							   AND customer.LASTNAME = CASE WHEN '''' = '''+ @surname +''' THEN customer.LASTNAME ELSE '''+ @surname +''' END  
							   AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
							   AND C_TENANCY.ENDDATE IS NULL
			GROUP By	AM_FirstDetecionList.TENANCYID) as Temp

'
PRINT(@query);
EXEC(@query);



END





















GO
