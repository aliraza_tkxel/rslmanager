
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_GetActions
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,12/24/2013,>
-- Description:	<Description,,for the dropdown list on the Job ROle Audit Report>
-- WebPage: JobRoleAuditReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetActions]
AS
BEGIN
	SELECT * from E_JOBROLEACTIONS
END
GO
