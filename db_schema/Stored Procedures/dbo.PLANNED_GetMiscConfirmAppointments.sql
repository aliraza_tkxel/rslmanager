USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetMiscConfirmAppointments]    Script Date: 11/15/2016 6:20:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--EXEC	[dbo].[PLANNED_GetMiscConfirmAppointments]
--		@propertyId = N'A010060001',
--		@journalId = 1
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,6th Dec,2013>
-- Description:	<Description,,This stored procedure 'll return the pending pending appointments against the property & journal id>
-- =============================================
IF OBJECT_ID('dbo.[PLANNED_GetMiscConfirmAppointments]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetMiscConfirmAppointments] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetMiscConfirmAppointments]
	@propertyId varchar(20),
	@journalId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT 
	PLANNED_APPOINTMENTS.APPOINTMENTID as AppointmentId
	,PLANNED_APPOINTMENTS.JournalId as JournalId
	,PLANNED_APPOINTMENTS.MiscTrade as ComponentTradeId
	,E__EMPLOYEE.EMPLOYEEID AS OperativeId
	,ISNULL(E__EMPLOYEE.FIRSTNAME ,'')+' '+ISNULL(E__EMPLOYEE.LASTNAME ,'')	as OperativeName	
	,convert(datetime,convert(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE,103) + ' ' + PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME,103)as StartDate
	,convert(datetime,convert(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTENDDATE,103) + ' ' + PLANNED_APPOINTMENTS.APPOINTMENTENDTIME,103)as EndDate		
	FROM 
		PLANNED_APPOINTMENTS 
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN E__EMPLOYEE ON PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
	WHERE 
	1=1
	AND PLANNED_APPOINTMENTS.ISPENDING = 1
	AND PLANNED_JOURNAL.JOURNALID = @journalId

END