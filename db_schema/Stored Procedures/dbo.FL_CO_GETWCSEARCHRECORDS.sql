SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE [dbo].[FL_CO_GETWCSEARCHRECORDS] 

/* ===========================================================================
 '   NAME:           FL_CO_GETWCSEARCHRECORDS
 '   DATE CREATED:   01 Jan 2009
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    RSL
 '   PURPOSE:        To get work completion record based on Search
 '   IN:            @locationId, @areaId, @elementId,@teamId,@userId,@patchId,@schemeId, @priorityId, @statusId,@stageId,@due,@postCode,@ORGID,
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	        @locationId	int	=  NULL,
		@areaId	        int	=  NULL,
		
		@elementId	int = NULL,
		@teamId	int = NULL,
		@userId	int = NULL,
		@patchId	int = NULL,
		@schemeId	int = NULL,
		@due              nvarchar(30)=  NULL,
		@postCode 	nvarchar(500)=NULL,
		@priorityId	int = NULL,
		@statusId 	int = NULL,  
		@stageId	int = NULL,  
		@ORGID           int = NULL,
		@JsNumber	INT = NULL,
		
	
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
     SET @SearchCriteria = '' 
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @statusId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@statusId)) + ' AND'  
   IF @patchId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY .PATCH = '+ LTRIM(STR(@patchId)) + ' AND'  
    IF @schemeId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY .DEVELOPMENTID  = '+ LTRIM(STR(@schemeId)) + ' AND'    
    IF @postCode IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'P__PROPERTY.POSTCODE = '''+ CONVERT(varchar,@postCode)+ '''  AND'  
    IF @due IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
        'Convert(varchar(10),FL_FAULT_LOG.DueDate,103) ='''+ Convert(varchar,@due,120) +''' AND'
    IF @ORGID IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_LOG.ORGID = '+ LTRIM(STR(@ORGID)) + ' AND' 
     IF @JsNumber IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_LOG.FAULTLOGID = '+ LTRIM(STR(@JsNumber)) + ' AND' 
   --IF @teamId IS NOT NULL
   --    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
   --                          'E_TEAM.TEAMID = '+ LTRIM(STR(@teamId)) + ' AND'
    IF @userId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'E__EMPLOYEE.EMPLOYEEID = '+ LTRIM(STR(@userId)) + ' AND'
   IF @stageId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_CO_APPOINTMENT_STAGE.AppointmentStageID = '+ LTRIM(STR(@stageId)) + ' AND'
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT' +                      
							CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
							CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID AS FaultLogID, FL_FAULT_LOG.StatusID AS FaultStatusID, FL_FAULT_LOG.JobSheetNumber AS JSNumber,FL_FAULT_LOG.DueDate AS DueDate,' +
							CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.SubmitDate AS Logged,C__CUSTOMER.FIRSTNAME AS Name, C__CUSTOMER.LASTNAME, C__CUSTOMER.CUSTOMERID AS CustomerNo,C__CUSTOMER.TENANCY AS TenancyRef,' +
							CHAR(10) + CHAR(9) + 'P__PROPERTY.HOUSENUMBER + '','' +  P__PROPERTY.ADDRESS1 + '','' +  ISNULL(P__PROPERTY.ADDRESS2,'''') + '','' + P__PROPERTY.TOWNCITY + '','' +    P__PROPERTY.POSTCODE + '', '' + P__PROPERTY.COUNTY AS COMPLETEADDRESS,' +
							CHAR(10) + CHAR(9) + 'C_ADDRESS.ADDRESS1 AS CustomerAddress,C_ADDRESS.TEL AS CustomerTel,' +
							CHAR(10) + CHAR(9) + 'C_ADDRESS.HOUSENUMBER AS CustomerHouseNo, C_ADDRESS.POSTCODE AS CustomerPostCode,' +
							CHAR(10) + CHAR(9) + 'C_ADDRESS.COUNTY AS CustomerCounty, C_ADDRESS.TOWNCITY AS CustomerTownCity,' +
							CHAR(10) + CHAR(9) + 'P__PROPERTY.ADDRESS1 AS Address,' +
							CHAR(10) + CHAR(9) + ' FL_FAULT.Description AS Description,' +
							CHAR(10) + CHAR(9) + ' FL_ELEMENT.ElementName AS ElementName,' +
							CHAR(10) + CHAR(9) + ' FL_AREA.AreaName AS AreaName,' +
							CHAR(10) + CHAR(9) + ' FL_LOCATION.LocationName AS LocationName,' +
							CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT.AppointmentDate AS Appointment,FL_CO_APPOINTMENT.AppointmentID AS AppointmentID,FL_CO_APPOINTMENT.LastActionDate AS LastActioned,FL_CO_APPOINTMENT.Notes AS AppointmentNotes,FL_CO_APPOINTMENT.AppointmentStageID AS AppointmentStageId,FL_CO_APPOINTMENT.OperativeID AS AppointmentOperativeID,FL_CO_APPOINTMENT.LetterID AS AppointmentLetterID,FL_CO_APPOINTMENT.Time AS AppointmentTime,' +
							CHAR(10) + CHAR(9) + 'E__EMPLOYEE.FIRSTNAME AS Operative,' +
							CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE.StageName AS Stage,' +
							CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY.ResponseTime AS ResponseTime,Case  FL_FAULT_PRIORITY.Days'+
							CHAR(10) +CHAR(9) +' When 0 then ''Hour(s)'' when 1 then ''Day(s)''  end as Type, ' +
							CHAR(10) + CHAR(9) + 'G_TITLE.DESCRIPTION as TitleDescription' 
                       
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
  SET @FromClause =CHAR(10) + CHAR(10)+  'FROM ' + 
                      CHAR(10) + CHAR(9) + 'fl_co_appointment inner join' +
                      CHAR(10) + CHAR(9) + 'fl_fault_log on fl_fault_log.jobsheetnumber=fl_co_appointment.jobsheetnumber inner join' +
                      CHAR(10) + CHAR(9) + 'fl_fault on fl_fault_log.faultid=fl_fault.faultid inner join' +
                      CHAR(10) + CHAR(9) + 'c__customer on fl_fault_log.customerid=c__customer.customerid LEFT join' +
	         CHAR(10) + CHAR(9) + 'G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID INNER JOIN ' + 
                      CHAR(10) + CHAR(9) + 'c_address on c__customer.customerid=c_address.customerid AND C_ADDRESS.ISDEFAULT=1 inner join' +
                      CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID and C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN'+ 
                     CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL INNER JOIN'+		
                     --CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN'+ 
                     --CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE on fl_co_appointment.appointmentstageId=FL_CO_APPOINTMENT_STAGE.appointmentstageid inner join' +
                      CHAR(10) + CHAR(9) + 'fl_fault_priority on fl_fault.priorityid=fl_fault_priority.priorityid inner join ' +
                      CHAR(10) + CHAR(9) + 'e__employee on fl_co_appointment.operativeid=e__employee.employeeid INNER JOIN' +
                      CHAR(10) + CHAR(9) + 'fl_element on fl_fault.elementid=fl_element.elementid inner join' +
                      CHAR(10) + CHAR(9) + 'fl_area on fl_element.areaid=fl_area.areaid inner join ' +
                      CHAR(10) + CHAR(9) + 'fl_location on fl_area.locationid=fl_location.locationid INNER JOIN' +
                     -- CHAR(10) + CHAR(9) + 'e_jobdetails on e__employee.employeeid=e_jobdetails.employeeid inner join' +
                      --CHAR(10) + CHAR(9) + 'e_team on e_jobdetails.team=e_team.teamid inner join ' +
                      CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS on fl_fault_log.statusid=FL_FAULT_STATUS.FaultStatusID'
	        
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.FaultLogID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FL_FAULT_LOG.FaultLogID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FL_FAULT_LOG.FaultLogID NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FL_FAULT_LOG.FaultLogID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'WHERE 1=1 AND FL_FAULT_STATUS.DESCRIPTION in (''Appointment Arranged'',''Appointment Rearranged'')   AND'+ @SearchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        
                        
                        CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.DESCRIPTION in (''Appointment Arranged'',''Appointment Rearranged'')  AND 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)



GO
