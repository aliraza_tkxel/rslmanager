USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateCTerminationData]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[V_UpdateCTerminationData]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_UpdateCTerminationData] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[V_UpdateCTerminationData]
-- Add the parameters for the stored procedure here
		@pdrJournalId int = 0,
		@reletDate DateTime null,
		@userId int =0
	
AS
BEGIN
	
	SET NOCOUNT ON;  

	Declare @TerminationHistoryId int, @todayDate DateTime

	SELECT @todayDate= CONVERT(DATE,GETDATE())

	select @TerminationHistoryId = T.TERMINATIONHISTORYID 
                  FROM	PDR_JOURNAL 
                  INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId 
		         INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		 
		         LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID 
		         Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P__PROPERTY.PropertyID 
		        	AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15)  
		        	 GROUP BY PROPERTYID) as CJournal 
		         CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
		         INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
		         where PDR_JOURNAL.JOURNALID =  @pdrJournalId

	if(@TerminationHistoryId is not null and @reletDate is not null and @pdrJournalId is not null)
	BEGIN
		INSERT INTO C_TERMINATION(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, TERMINATIONDATE, REASON, NOTES, TTIMESTAMP, REASONCATEGORYID, AppVersion)
		SELECT JOURNALID, ITEMSTATUSID, ITEMACTIONID, @todayDate, @userId, TERMINATIONDATE, REASON, NOTES, TTIMESTAMP, REASONCATEGORYID, AppVersion FROM C_TERMINATION WHERE TERMINATIONHISTORYID=@TerminationHistoryId
		
		UPDATE C_TERMINATION SET RELETDATE = @reletDate 
		WHERE TERMINATIONHISTORYID = (SELECT TOP 1 TERMINATIONHISTORYID FROM C_TERMINATION ORDER BY 1 DESC)
	END
	                                            
END
