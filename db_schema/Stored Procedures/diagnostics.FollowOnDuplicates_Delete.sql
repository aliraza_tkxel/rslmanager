SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE diagnostics.FollowOnDuplicates_Delete
AS ;
    WITH    CTE_Dup ( TotalRecords, MinFollowOnId, FaultLogId, Notes, isFollowonScheduled)
              AS ( SELECT   COUNT(FollowOnId) AS TotalRecords ,
                            MIN(FollowOnId) AS MinFollowOnId ,
                            FaultLogId ,
                            FollowOnNotes ,
                            isFollowonScheduled
                   FROM     dbo.FL_FAULT_FOLLOWON
                   GROUP BY FaultLogId ,
                            FollowOnNotes ,
                            isFollowonScheduled
                   HAVING   COUNT(FollowOnId) > 1
                 )
        DELETE  t
        FROM    dbo.FL_FAULT_FOLLOWON t
                INNER JOIN CTE_Dup dup ON t.FaultLogId = dup.FaultLogId
                                          AND t.FollowOnId <> dup.MinFollowOnId

GO
