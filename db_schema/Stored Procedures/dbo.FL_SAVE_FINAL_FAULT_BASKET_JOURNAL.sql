SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[FL_SAVE_FINAL_FAULT_BASKET_JOURNAL]
/* ===========================================================================
 '   NAME:           FL_SAVE_FINAL_FAULT_BASKET_JOURNAL
 '   DATE CREATED:   24th NOV, 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Tenants Online Fault Basket
 '   PURPOSE:        To save faults in fault basket, fault log, journal and repair
 
 '   IN:             @CustomerId
 '   IN:             @PreferredContactDetails
 '   IN:             @SubmitDate
 '   IN:             @IamHappy
 '   IN:             @PropertyId
 '   IN:	     @TenancyId
 '
 '   OUT:            @FaultBasketId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (	
	@CustomerId INT,
	@PreferredContactDetails nvarchar(500),
	@SubmitDate datetime,
	@IamHappy INT,
	@StatusId int = 1,
	@IsReported int = 0,
	@PropertyId nvarchar(20),
	@TenancyId int,
	@UserId int,
	@FaultBasketId INT OUTPUT
	
	
  )
AS
	BEGIN TRANSACTION

/*
Inserting the record in Fault Basket Table
*/
	INSERT INTO FL_FAULT_BASKET(
		CustomerID,
		PreferredContactDetails,
		SubmitDate,
		IamHappy		
	)

	VALUES(
		@CustomerId,
		@PreferredContactDetails,
		@SubmitDate,
		@IamHappy				
	)

/*
Select the recent FaultBasketId SubmitDate, StatusID,IsReported,
*/

SELECT @FaultBasketID=FaultBasketId
FROM FL_FAULT_BASKET
WHERE FaultBasketId = @@IDENTITY


/*
Select the record from temptable 
*/
DECLARE @TempFaultID INT
DECLARE @FaultId INT
DECLARE @Quantity INT
DECLARE @Recharge INT
DECLARE @ProblemDays INT
DECLARE @RecuringProblem INT
DECLARE @CommunalProblem INT
DECLARE @Notes nvarchar(500)
DECLARE @ORGID INT
DECLARE @FAULTLOGID INT
DECLARE @JOURNALID INT
DECLARE @ITEMSTATUSID INT
DECLARE @ITEMACTIONID INT
DECLARE @SCOPEID INT
DECLARE @JSNumberPrefix VARCHAR(10)
DECLARE @DUEDATE varchar(30)
DECLARE @RESPONSETIME INT
DECLARE @DAYS bit
DECLARE @IsSelected bit
DECLARE @PreInspectionId INT
DECLARE @IsApproved bit
DECLARE @RepairReportedStatusId INT
DECLARE @AssginedToContractorStatusId INT
DECLARE @WorksCompletedStatusId INT
DECLARE @ItemId INT
DECLARE @ItemNatureId INT




SET @JSNumberPrefix  = 'JS'

--Get and set the repair reported status
SET @RepairReportedStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Repair Reported')

--Get and set the repair assigned to contractor status
SET @AssginedToContractorStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Assigned To Contractor')


--Get and set the works completed status
SET @WorksCompletedStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Works Completed')

--Get and set the Item id 
SET @ItemId = (SELECT  ItemID FROM C_ITEM WHERE DESCRIPTION='Property')

--Get and set the Item nature id 
SET @ItemNatureId = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION='Reactive Repair')

--Get Value of ScopeID
SELECT @ScopeID = SCOPEID FROM S_SCOPE WHERE ORGID=@OrgId AND AREAOFWORK= (SELECT AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair'))


--Declare the cursor

DECLARE c1 CURSOR READ_ONLY

FOR

SELECT TempFaultID,FaultId, Quantity,Recharge,ProblemDays, RecuringProblem, CommunalProblem, Notes,ORGID,ITEMACTIONID,ITEMSTATUSID FROM FL_TEMP_FAULT WHERE CustomerID = @CustomerId  

OPEN c1

--Start Looping
FETCH NEXT FROM c1
INTO @TempFaultID,@FaultId, @Quantity, @Recharge, @ProblemDays, @RecuringProblem, @CommunalProblem, @Notes,@ORGID,@ITEMACTIONID,@ITEMSTATUSID

WHILE @@FETCH_STATUS = 0
BEGIN

--Insert the record in fault log table

SELECT @RESPONSETIME= RESPONSETIME, @DAYS=DAYS 
FROM FL_FAULT_PRIORITY 
INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PRIORITYID=FL_FAULT.PRIORITYID 
WHERE FAULTID=@FaultId

if @DAYS=0
set @DUEDATE=DateAdd(hh,@RESPONSETIME,@SubmitDate)
ELSE
set @DUEDATE=DateAdd(d,@RESPONSETIME,@SubmitDate)

--If contractor has been assgined
If @ORGID IS NOT NULL 
	SET @StatusId = @AssginedToContractorStatusId	

If @IsSelected IS NULL 
	SET @IsSelected = 0

INSERT INTO FL_FAULT_LOG(
		FaultBasketID,
		FaultID,
		CustomerID,
		SubmitDate,
		ORGID,
		StatusID,
		IsReported,
		Quantity,
		ProblemDays,
		RecuringProblem,
		Recharge,
		CommunalProblem,
		Notes,
		DueDate,
		IsSelected,
		UserId
				
	)

	VALUES(
		@FaultBasketId,
		@FaultId,
		@CustomerId,
		@SubmitDate,
		@ORGID,
		@StatusId,
		@IsReported,		
		@Quantity, 
		@ProblemDays, 
		@RecuringProblem, 
		@Recharge,
		@CommunalProblem, 
		@Notes,
		@DUEDATE,
		@IsSelected,
		@USERID
		
	)

-- Get latest fault log id
SELECT @FAULTLOGID=FaultLogId
FROM FL_FAULT_LOG
WHERE FaultLogId = @@IDENTITY

-- Update preinspection table 
UPDATE FL_FAULT_PREINSPECTIONINFO 
SET faultlogid=@faultlogid 
WHERE faultlogid=@TempFaultID

-- Insert the job sheet number
UPDATE FL_FAULT_LOG 
SET JobSheetNumber = (@JSNumberPrefix + CONVERT(VARCHAR,@@IDENTITY)) 
WHERE FaultLogId = @@IDENTITY 


--First Insertion into Journal Table
INSERT INTO FL_FAULT_JOURNAL
(FAULTLOGID, CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, FaultStatusID, LETTERACTION, CREATIONDATE, TITLE, LASTACTIONDATE, NEXTACTIONDATE)
VALUES
(@FAULTLOGID, @CustomerId, @TenancyId, @PropertyId, @ItemId, @ItemNatureId, @ITEMSTATUSID, null, @SubmitDate, null, GETDATE(), null)


--Get latest Journal id
SELECT @JOURNALID=JournalID 
FROM FL_FAULT_JOURNAL 
WHERE JournalID = @@IDENTITY



SELECT @PreInspectionId=PREINSPECTONID, @IsApproved= APPROVED 
FROM FL_FAULT_PREINSPECTIONINFO 
WHERE faultlogid = @FAULTLOGID 



	--Insert Values into FL_FAULT_LOG_HISTORY table
			  									  					  			

IF @ORGID IS NOT NULL AND @IsApproved IS NULL /*IF Contractor has been assgined & Pre-Inspection is false then history will have two entries*/
	BEGIN	
	
	
		--Insertion into fault log history with repair reported status
		INSERT INTO FL_FAULT_LOG_HISTORY
		(JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
		VALUES
		(@JOURNALID, @RepairReportedStatusId, @ITEMACTIONID, GETDATE(), NULL, @FAULTLOGID, @ORGID, @SCOPEID, NULL, @NOTES)
	
		
		--Insertion into fault log history with assigned to contractor status
		INSERT INTO FL_FAULT_LOG_HISTORY(JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
		VALUES
		(@JOURNALID, @AssginedToContractorStatusId, @ITEMACTIONID, GETDATE(), NULL, @FAULTLOGID, @ORGID, @SCOPEID, NULL, @NOTES)	
	
		
		--Insertion into fault journal with assigned to contractor status	
		UPDATE FL_FAULT_JOURNAL 
		SET FaultStatusID= @AssginedToContractorStatusId
		WHERE JournalID = @JOURNALID
		
		
		--Insertion into fault log with assigned to contractor status	
		UPDATE FL_FAULT_LOG
		SET StatusID= @AssginedToContractorStatusId
		WHERE FaultLogID = @FAULTLOGID
	END
ELSE IF @ORGID IS NULL AND @IsApproved IS NULL /*IF Contractor is not assgined & Pre-Inspection is false then history will have one entry*/
	BEGIN	
		
		--Insertion into fault log history with repair reported status
		INSERT INTO FL_FAULT_LOG_HISTORY
		(JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
		VALUES
		(@JOURNALID,@RepairReportedStatusId,@ITEMACTIONID,GETDATE(),NULL,@FAULTLOGID,@ORGID,@SCOPEID,NULL,@NOTES)				
	END
				  

	FETCH NEXT FROM c1
	INTO @TempFaultID,@FaultId, @Quantity,@Recharge, @ProblemDays, @RecuringProblem, @CommunalProblem, @Notes,@ORGID,@ITEMACTIONID,@ITEMSTATUSID

END

CLOSE c1
DEALLOCATE c1 

DELETE 
FROM FL_TEMP_FAULT 
WHERE CustomerId = @CustomerId	   

IF @FaultBasketId<0
	BEGIN
		SET @FaultBasketId=-1
		ROLLBACK TRANSACTION
		RETURN 
	END
	
 SELECT @FaultBasketID=max(FaultBasketId)
FROM FL_FAULT_BASKET

COMMIT TRANSACTION











GO
