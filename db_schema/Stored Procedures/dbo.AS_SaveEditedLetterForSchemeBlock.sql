USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_SaveEditedLetterForSchemeBlock') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_SaveEditedLetterForSchemeBlock AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_SaveEditedLetterForSchemeBlock] 
	-- Add the parameters for the stored procedure here
	@journalHistoryId varchar(max),
	@title varchar(max),
	@body nvarchar(MAX),
	@standardLetterId int,
	@teamId int,
	@resourceId int,
	@signOffId int,		
	@rentBalance float,
	@rentCharge float,
	@todayDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @SplitValues TABLE 
	(
		ID int
	)
	DECLARE @xml xml
	SET @xml = N'<root><r>' + replace(@journalHistoryId,',','</r><r>') + '</r></root>'

	INSERT INTO @SplitValues(ID)
	SELECT r.value('.','int')
	FROM @xml.nodes('//root/r') as records(r)

    INSERT INTO [RSLBHALive].[dbo].[AS_SAVEDLETTERS]
           ([JOURNALHISTORYID]
           ,[LETTERCONTENT]
           ,[LETTERID]
           ,[LETTERTITLE]
           ,[TEAMID]
           ,[FromResourceId]
           ,[SignOffLookupCode]
           ,[RentBalance]
           ,[RentCharge]
           ,[TodayDate]
           ,[CreatedDate]
           ,[ModifiedDate]
           )
	SELECT ID 
		   ,@body 
           ,@standardLetterId
           ,@title 
           ,@teamId
           ,@resourceId
           ,@signOffId 
           ,@rentBalance
           ,@rentCharge
           ,@todayDate
           ,CURRENT_TIMESTAMP 
           ,CURRENT_TIMESTAMP 
	FROM @SplitValues
END
