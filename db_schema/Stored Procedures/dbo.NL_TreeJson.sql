USE [RSLBHALive]
GO
IF OBJECT_ID('dbo.NL_TreeJson') IS NULL 
	EXEC('CREATE PROCEDURE dbo.NL_TreeJson AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[NL_TreeJson]    Script Date: 18/07/2017 20:46:13 ******/ --[NL_TreeJson] '', '', 35
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[NL_TreeJson]
(@theState NVARCHAR(10) null, @company NVARCHAR(10) null, @openedAccountId NVARCHAR(10) null)
AS
BEGIN

	DECLARE @theJson NVARCHAR(max)
	SET @theJson = '['

	SELECT @theState = CASE @theState
		WHEN '' THEN 1
		WHEN 'active' THEN 1
		WHEN 'inactive' THEN 0
		END 

	IF @theState = 1
	 BEGIN
		 SET @theJson = @theJson + '{
			"thecode": "New",
			"title": "Parent Nominal Code",
			"theid": "0",
			"theparentid": "0",
			"children": [
			]
		  },'
	 END
	 PRINT DATEPART(SECOND, GETDATE() )
DECLARE @openedAccountlEVEL1 INT
DECLARE @openedAccountlEVEL2 INT
DECLARE @openedAccountlEVEL3 INT
CREATE TABLE #selectedId(id int)

IF NOT @openedAccountId = '' 
 BEGIN
	 SELECT @openedAccountlEVEL3 = a.ACCOUNTID , @openedAccountlEVEL2 = a2.ACCOUNTID  , @openedAccountlEVEL1 = a3.ACCOUNTID 
		 FROM  dbo.NL_ACCOUNT a
			left JOIN nl_account a2 ON a.PARENTREFLISTID = a2.ACCOUNTID
			left JOIN nl_account a3 ON a2.PARENTREFLISTID = a3.ACCOUNTID
		WHERE a.ACCOUNTID = @openedAccountId OR a2.ACCOUNTID = @openedAccountId

	
	INSERT INTO #selectedId (id) SELECT @openedAccountlEVEL1
	INSERT INTO #selectedId (id) SELECT @openedAccountlEVEL2
	INSERT INTO #selectedId (id) SELECT @openedAccountlEVEL3

 END

 IF @theState = 0
  begin
	SELECT @theJson = @theJson + '{ ' 
			
		   +' "parentid":"'+ ISNULL(CAST(o.PARENTREFLISTID as nvarchar(10)),'') +'"'
		   +',"theid": "'+CAST(o.ACCOUNTID   as nvarchar(10))+ '"'
			+',"thecode": "'+ o.ACCOUNTNUMBER + '"'
			+',"isactive": "' + CAST(o.ISACTIVE AS NVARCHAR(10)) + '"'
		   +',"title": "'+ o.NAME + '"' 
		   +',"expanded": ' + (CASE WHEN s.id IS NOT NULL THEN 'true' ELSE 'false' END)
		   +',"codetype":"'+ a.DESCRIPTION +'"'
		   + ISNULL(dbo.NL_SelectChild(o.ACCOUNTID, o.SUBLEVEL, @theState, @company, @openedAccountlEVEL1,@openedAccountlEVEL2,@openedAccountlEVEL3), '')+'},'
	FROM nl_account AS o
		INNER JOIN dbo.NL_ACCOUNTTYPE a ON a.ACCOUNTTYPEID = o.ACCOUNTTYPE -- AND o.ACCOUNTNUMBER in ('0010', '0200')
		left JOIN (
					SELECT COUNT(1) AS CompanyCount , ac.ACCOUNTID
					FROM nl_account_to_company ac
					WHERE ac.COMPANYID IN (@company)
					GROUP BY ac.ACCOUNTID
					) b ON b.accountid = o.ACCOUNTID 
		 LEFT JOIN #selectedId s ON s.id = o.ACCOUNTID
		 LEFT JOIN ( 
					SELECT COUNT(1) AS acnt, a.ACCOUNTID
					FROM dbo.NL_ACCOUNT a
						INNER JOIN dbo.NL_ACCOUNT_TO_COMPANY C ON C.ACCOUNTID = a.ACCOUNTID AND (C.COMPANYID IN (@company) OR @company = '')
						left JOIN dbo.NL_ACCOUNT a2 ON a2.PARENTREFLISTID = a.ACCOUNTID
						left JOIN dbo.NL_ACCOUNT a3 ON a3.PARENTREFLISTID = a2.ACCOUNTID
					WHERE	@theState = 0  AND a.SUBLEVEL  = 0 AND a.ISACTIVE = 1 
							AND (ISNULL(a2.ISACTIVE,-1) = 0 or ISNULL(a3.ISACTIVE,-1) = 0 )   
					GROUP BY a.ACCOUNTID
					) ap ON ap.ACCOUNTID = o.ACCOUNTID
	WHERE o.SUBLEVEL =0  
			AND (( b.CompanyCount > 0 OR @company = '')) 
			AND (o.ISACTIVE = @theState OR (@theState = 0 AND ap.acnt >= 1))
	ORDER BY o.ACCOUNTNUMBER
  END
 ELSE
  BEGIN
  	SELECT @theJson = @theJson + '{ ' 
			
		   +' "parentid":"'+ ISNULL(CAST(o.PARENTREFLISTID as nvarchar(10)),'') +'"'
		   +',"theid": "'+CAST(o.ACCOUNTID   as nvarchar(10))+ '"'
			+',"thecode": "'+ o.ACCOUNTNUMBER + '"'
		   +',"title": "'+ o.NAME +  '"' 
		   +',"expanded": ' + (CASE WHEN s.id IS NOT NULL THEN 'true' ELSE 'false' END)
		   +',"codetype":"'+ a.DESCRIPTION +'"'
		   + ISNULL(dbo.NL_SelectChild(o.ACCOUNTID, o.SUBLEVEL, @theState, @company, @openedAccountlEVEL1,@openedAccountlEVEL2,@openedAccountlEVEL3), '')+'},'
	FROM nl_account AS o
		INNER JOIN dbo.NL_ACCOUNTTYPE a ON a.ACCOUNTTYPEID = o.ACCOUNTTYPE -- AND o.ACCOUNTNUMBER in ('0010', '0200')
		left JOIN (
					SELECT COUNT(1) AS CompanyCount , ac.ACCOUNTID
					FROM nl_account_to_company ac
					WHERE ac.COMPANYID IN (@company)
					GROUP BY ac.ACCOUNTID
					) b ON b.accountid = o.ACCOUNTID 
		 LEFT JOIN #selectedId s ON s.id = o.ACCOUNTID
	WHERE o.SUBLEVEL =0  
			AND (( b.CompanyCount > 0 OR @company = '')) 
			AND o.ISACTIVE = @theState 
	ORDER BY o.ACCOUNTNUMBER
  END
  



SET  @theJson = @theJson + ']'

SELECT  REPLACE(@theJson, ',]',']') AS theJson

 	 PRINT DATEPART(SECOND, GETDATE() )
END


