SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adnan Mirza>
-- Create date: <30/9/2011>
-- Description:	<Update table P_LGSR from the uploaded excel file>
-- =============================================
CREATE PROCEDURE [dbo].[P_LGSR_UPDATE_DATA] (@FILEID INT)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE P_LGSR SET BSAVE=1,ISSUEDATE=U.ISSUEDATE,CP12NUMBER=U.CP12NUMBER
	FROM P_LGSR_UPLOADFILE_DATA U
	INNER JOIN P_LGSR P ON P.PROPERTYID=U.PROPERTYID
	WHERE FILEID=@FILEID

END
GO
