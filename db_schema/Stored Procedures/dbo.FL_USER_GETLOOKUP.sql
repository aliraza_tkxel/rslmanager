SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_USER_GETLOOKUP]
/* ===========================================================================
 '   NAME:          FL_USER_GETLOOKUP
 '   DATE CREATED:   30 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get User Id from E__Employee table which will be shown as lookup value in presentation pages
 '   IN:            @TeamId
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@TeamId as Int
AS
	
	SELECT E__EMPLOYEE.EMPLOYEEID as id, E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME  as val
	FROM   E__EMPLOYEE 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID AND E_JOBDETAILS.ACTIVE=1
	INNER JOIN E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID
	WHERE     (E_TEAM.TEAMID =@TeamId )
	ORDER BY FIRSTNAME ASC









GO
