USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[I_DASHBOARD_SELECT_BY_EMP]    Script Date: 12/08/2018 21:34:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date, 4/1/2014>
-- Description:	<Description, Return Alerts of Employee>
-- updated : <Saud Ahmed>
-- updated Date : <01/03/2017>
-- change : order by Name
-- =============================================
-- [I_DASHBOARD_SELECT_BY_EMP] 443
alter PROCEDURE [dbo].[I_DASHBOARD_SELECT_BY_EMP] 
(
	@EmpId	int
)
AS  
BEGIN  

--SELECT * FROM dbo.AC_PAGES WHERE pageid = 115
--SELECT * FROM dbo.AC_PAGES_ACCESS
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 
SELECT moduleid,modulename,moduleurl modulelink
FROM  (
SELECT '1' AS moduleid , 'Create PO'  AS modulename, '/finance/CreatePurchaseOrder.asp' AS moduleurl 
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 43) > 0
UNION
SELECT '2' AS moduleid , 'Record Sickness'   AS modulename, '/RSLHRModuleWeb/Bridge/?Module=MyJob&UserId=' + CAST(@EmpId AS NVARCHAR(20)) AS moduleurl

FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 115) > 0
UNION
SELECT '3' AS moduleid , 'Book Leave'   AS modulename, '/RSLHRModuleWeb/Bridge/?Module=MyJob&UserId=' + CAST(@EmpId AS NVARCHAR(20)) AS moduleurl
UNION
SELECT '4' AS moduleid  , '+ Internal Meeting'   AS modulename,  '/RSLHRModuleWeb/Bridge/?Module=MyJob&UserId=' + CAST(@EmpId AS NVARCHAR(20)) AS moduleurl 
UNION
--SELECT '5' AS moduleid  , 'Latest Documents'   AS modulename, '/BHAIntranet/CustomPages/LatestDocuments.aspx' AS moduleurl
--UNION
SELECT '6' AS moduleid , 'Find Supplier'   AS modulename, '/Suppliers/SupplierSearch.asp'AS moduleurl
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 54) > 0
UNION
SELECT '7' AS moduleid , 'Find a Customer'   AS modulename, '/Customer/CustomerSearch.asp'AS moduleurl
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 34) > 0
UNION
SELECT '8' AS moduleid , 'Find a Property'   AS modulename, '/PropertyModule/bridge.aspx'AS moduleurl
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 269) > 0
UNION
SELECT '9' AS moduleid , 'Find a Scheme'   AS modulename, '/PropertyModule/bridge.aspx' AS moduleurl
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 404) > 0
UNION
--SELECT '10' AS moduleid , 'Training Videos'   AS modulename, '#'AS moduleurl 
--UNION
SELECT '11' AS moduleid , 'Invoices for Approval' AS modulename, '/Finance/InvoiceRecievedList.asp'AS moduleurl
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 425) > 0
UNION
SELECT '12' AS moduleid , 'Goods for Approval' AS modulename, '/Finance/GoodsReceived.asp'AS moduleurl
FROM dbo.AC_PAGES_ACCESS WHERE dbo.[I_DASHBOARD_PanelAccessCheck](@EmpId, 100) > 0

) a
 
 
 --SELECT     E_TEAMALERTS.ALERTID, E_ALERTS.AlertName, E_ALERTS.AlertUrl, E_ALERTS.SOrder  
 --FROM	E__EMPLOYEE 
	--	INNER JOIN E_TEAMALERTS ON E__EMPLOYEE.JobRoleTeamId = E_TEAMALERTS.JobRoleTeamId 
	--	INNER JOIN E_ALERTS ON E_TEAMALERTS.AlertId = E_ALERTS.AlertID 
 --WHERE      (E__EMPLOYEE.EMPLOYEEID = @EmpId)  
 --ORDER BY   E_ALERTS.AlertName--E_ALERTS.SOrder
		
 
 
END 

