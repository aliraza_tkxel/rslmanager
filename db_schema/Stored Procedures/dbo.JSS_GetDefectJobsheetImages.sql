USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[JSS_GetDefectJobsheetImages]    Script Date: 3/10/2017 2:34:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abdul Rehman
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

IF OBJECT_ID('dbo.[JSS_GetDefectJobsheetImages]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[JSS_GetDefectJobsheetImages] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[JSS_GetDefectJobsheetImages] 
	-- Add the parameters for the stored procedure here
	@defectId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	PropertyDefectId,
	ImageTitle,
	ImagePath,
	ImageIdentifier,
	isBefore,
	CASE
		WHEN isBefore = 1 THEN 'Before'
		ELSE 'After'
	END AS Status

FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES

WHERE PropertyDefectId = @defectId
END

GO

