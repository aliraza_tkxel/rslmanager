USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetAppointmentDetailByJournalId]    Script Date: 15-Mar-18 10:42:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[V_GetAppointmentDetailByJournalId]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_GetAppointmentDetailByJournalId] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      06/07/2015
-- Description:      Get appointmentInfo by Journal Id
-- History:          06/07/2015 Void: Get appointment Info to send email cancel appointment detail to last Operative.
--                   
-- =============================================

ALTER PROCEDURE [dbo].[V_GetAppointmentDetailByJournalId](
	@journalId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
	SELECT 
		'JSV' + CONVERT(VARCHAR, RIGHT('000000' + CONVERT(VARCHAR, ISNULL(
	CASE
		WHEN V.WorksJournalId IS NULL THEN J.JOURNALID
		ELSE V.WorksJournalId
	END, -1)), 4)) AS Ref,
		ISNULL(P.HouseNumber, '') + ISNULL(' ' + P.ADDRESS1, '') + ISNULL(', ' + P.ADDRESS2, '') AS Address,
		ISNULL(P.TOWNCITY,'') as TOWNCITY,
		ISNULL(P.COUNTY,'') as COUNTY,
		ISNULL(P.POSTCODE,'') as POSTCODE,		
		ISNULL(P_SCHEME.SCHEMENAME,'') as Scheme,		
		ISNULL(P_BLOCK.BLOCKNAME,'') as Block,CONVERT(NVARCHAR(50), T.TerminationDate, 103) AS Termination,
		Case When T.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,T.TerminationDate), 103) Else	CONVERT(nvarchar(50),T.ReletDate, 103) END
		 as Relet,		
		E.EMPLOYEEID as EmployeeId, E.FIRSTNAME +' '+ E.LASTNAME as OperativeName , C.WORKEMAIL as Email,CONVERT(NVARCHAR(50), 
		A.APPOINTMENTSTARTDATE, 103)as StartDate,A.APPOINTMENTSTARTTIME as StartTime,A.APPOINTMENTENDTIME AS EndTime
	FROM
		PDR_JOURNAL J
		INNER JOIN PDR_MSAT ON J.MSATID =  PDR_MSAT.MSATId
		INNER JOIN PDR_APPOINTMENTS A ON J.JOURNALID=A.JOURNALID
		INNER JOIN P__PROPERTY P ON PDR_MSAT.PropertyId = P.PROPERTYID
		Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P.PropertyID
		AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,15) 
		GROUP BY PROPERTYID) as CJournal
		Cross APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
		INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
		INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO=E.EMPLOYEEID    
		LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID 
		LEFT JOIN P_SCHEME ON P.SCHEMEID = P_SCHEME.SCHEMEID 
		LEFT JOIN P_BLOCK ON P.BLOCKID = P_BLOCK.BLOCKID
		LEFT JOIN V_RequiredWorks V ON J.JOURNALID = V.WorksJournalId
	WHERE J.JOURNALID=@journalId	
		
					
END
