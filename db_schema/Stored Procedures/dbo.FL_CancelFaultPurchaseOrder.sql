USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[CancelFaultPurchaseOrder]    Script Date: 10/7/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.FL_CancelFaultPurchaseOrder') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_CancelFaultPurchaseOrder AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Rehan Baber
-- Create date: 10/7/2017
-- Description:	Cancels a purchase order on cancelling the respective Assign To Contractor Job Sheet
-- EXEC CancelFaultPurchaseOrder 

-- =============================================
ALTER PROCEDURE [dbo].[FL_CancelFaultPurchaseOrder]
@FaultLogId int,
@UserId int,
@sendEmail INT OUTPUT	
AS
BEGIN

	DECLARE @PURCHASEORDERID INT

	select @PURCHASEORDERID=FW.PurchaseORDERID from FL_FAULT_JOURNAL fj
	inner join FL_FAULT_LOG fl on fl.FaultLogID=fj.FaultLogID
	inner join FL_CONTRACTOR_WORK fw on fw.JournalId=fj.JournalID
	where fl.FaultLogID=@FaultLogId

	select @sendEmail=isnull(ORDERID,0) from F_PURCHASEORDER
	WHERE ORDERID = @PURCHASEORDERID AND POSTATUS IN (SELECT POSTATUSID 
													  FROM F_POSTATUS
													  WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))

	UPDATE F_PURCHASEORDER 
	SET POSTATUS = (SELECT POSTATUSID
					FROM F_POSTATUS
					WHERE POSTATUSNAME LIKE '%Cancelled%')
	WHERE ORDERID = @PURCHASEORDERID AND POSTATUS IN (SELECT POSTATUSID 
													  FROM F_POSTATUS
													  WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))
	
	
	DECLARE @IDENTIFIER VARCHAR
	SELECT @IDENTIFIER = Replace(Replace(Replace((CONVERT(VARCHAR(10), GETDATE(), 103) + ' '  + convert(VARCHAR(8), GETDATE(), 14)), ':', ''), '/', ''),' ','') + '_' + CAST(@UserId AS VARCHAR)
	
	INSERT INTO F_PURCHASEORDER_LOG
	(IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION, image_url)
	(SELECT @IDENTIFIER, PO.ORDERID, 1, PO.POTYPE, PO.POSTATUS, GETDATE(), @UserId, 'Cancelled', NULL FROM F_PURCHASEORDER PO WHERE ORDERID=@PURCHASEORDERID AND POSTATUS IN (SELECT POSTATUSID 
																																													FROM F_POSTATUS
																																													WHERE POSTATUSNAME IN ('Cancelled')))

	UPDATE F_PURCHASEITEM 
	SET PISTATUS = (SELECT POSTATUSID
					FROM F_POSTATUS
					WHERE POSTATUSNAME LIKE '%Cancelled%')
	WHERE ORDERID = @PURCHASEORDERID AND PISTATUS IN (SELECT POSTATUSID 
													  FROM F_POSTATUS
													  WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))
														


	select @sendEmail as sendEmail					
END
