SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE  PROCEDURE [dbo].[TO_HOUSE_MOVE_SelectHouseMoveDetail]

/* ===========================================================================
 '   NAME:           TO_HOUSE_MOVE_SelectHouseMoveDetail
 '   DATE CREATED:   28 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select single TO_HOUSE_MOVE record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
           		TO_HOUSE_MOVE.LocalAuthorityID,
			TO_HOUSE_MOVE.DevelopmentID, 
			TO_HOUSE_MOVE.NoOfBedrooms,
			TO_HOUSE_MOVE.OccupantsNoGreater18,
			TO_HOUSE_MOVE.OccupantsNoBelow18
                        
	FROM
			TO_ENQUIRY_LOG INNER JOIN TO_HOUSE_MOVE
			ON TO_ENQUIRY_LOG.EnquiryLogID = TO_HOUSE_MOVE.EnquiryLogID	       	       
			
	WHERE
	
	     TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID

GO
