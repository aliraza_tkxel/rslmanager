USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetCancelledReasons]    Script Date: 3/10/2016 3:14:33 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:		Altamish Arif
-- Create date: 3/10/2016
-- Description:	Get reason related to cancel condition.
-- =============================================

IF OBJECT_ID('dbo.[PLANNED_GetCancelReasons]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetCancelReasons] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetCancelReasons]

AS
	SELECT	ReasonId ,Reason 
	FROM	PLANNED_REASON 
	WHERE PLANNED_REASON.CANCELLEDREASON = 1
	ORDER BY ReasonId  ASC

