SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_C_SERVICECOMPLAINT_CATEGORY_GetCategory 
/* ===========================================================================
 '   NAME:           TO_C_SERVICECOMPLAINT_CATEGORY_GetCategory
 '   DATE CREATED:   09 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get complaint category records from C_SERVICECOMPLAINT_CATEGORY table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT CATEGORYID AS id,DESCRIPTION AS val
	FROM C_SERVICECOMPLAINT_CATEGORY
	ORDER BY DESCRIPTION ASC

GO
