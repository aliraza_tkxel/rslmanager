SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 06/05/0214
-- Description:	To get contractors having at lease one planned contract, as drop down values for assgin work to contractor.
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetPlannedContractorDropDownValues] 
	-- Add the parameters for the stored procedure here	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID AND RTRIM(LTRIM(AoW.DESCRIPTION)) LIKE '%Planned%'
	
END
GO
