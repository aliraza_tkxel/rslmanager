USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GET_DOCUMENT_TYPES]    Script Date: 04/12/2016 10:48:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.P_GET_DOCUMENT_TYPES') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.P_GET_DOCUMENT_TYPES AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
-- =============================================

-- Author				Create date			Description						

--Simon Rogers			1st August 2014			Retrieve Document Types
			 	

-- =============================================

ALTER PROCEDURE [dbo].[P_GET_DOCUMENT_TYPES]
	-- Add the parameters for the stored procedure here
    @ShowActiveOnly BIT = 1
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        IF @ShowActiveOnly = 1 
            SELECT  -1 AS DocumentTypeId ,
                    'Please select' AS Title ,
                    1 AS [Active] ,
                    0 AS [Order]
            UNION
            SELECT  DocumentTypeId ,
                    Title ,
                    Active ,
                    [Order]
            FROM    dbo.P_Documents_Type dt
            WHERE   [Active] = 1 AND dt.Title !='Asbestos'
            ORDER BY [Order] ,
                    [Title]
	
        ELSE 
            SELECT  -1 AS DocumentTypeId ,
                    'Please select' AS Title ,
                    1 AS [Active] ,
                    0 AS [order]
            UNION
            SELECT  DocumentTypeId ,
                    Title ,
                    Active ,
                    [Order]
            FROM    dbo.P_Documents_Type dt
            
            ORDER BY [Order] ,
                    [Title]
	
	
    END


