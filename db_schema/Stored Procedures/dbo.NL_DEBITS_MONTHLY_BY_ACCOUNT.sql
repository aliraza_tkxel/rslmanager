USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[NL_CreateCode]    Script Date: 17/07/2017 11:43:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.NL_DEBITS_MONTHLY_BY_ACCOUNT') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.NL_DEBITS_MONTHLY_BY_ACCOUNT AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


alter      PROCEDURE [dbo].[NL_DEBITS_MONTHLY_BY_ACCOUNT]
(
 @LISTID INT,
 @STARTDATE SMALLDATETIME,
 @ENDDATE SMALLDATETIME,
 @Company int
)

AS

SELECT 	MONTH(J.TXNDATE) AS IMONTH, YEAR(J.TXNDATE) AS IYEAR, SUM(D.AMOUNT) AS AMOUNT 
FROM  	NL_JOURNALENTRYDEBITLINE D
	INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = D.ACCOUNTID
	INNER JOIN NL_JOURNALENTRY J ON J.TXNID = D.TXNID
WHERE 	(A.ACCOUNTID = @LISTID OR A.PARENTREFLISTID = @LISTID)
	AND J.TXNDATE >= @STARTDATE AND J.TXNDATE <= @ENDDATE
	AND J.TRANSACTIONTYPE <> 9 	-- OPENING BALANCE
	AND isnull(d.COMPANYID,1) = @Company
GROUP	BY MONTH(J.TXNDATE), YEAR(J.TXNDATE)
ORDER 	BY IYEAR ASC, IMONTH ASC





GO
