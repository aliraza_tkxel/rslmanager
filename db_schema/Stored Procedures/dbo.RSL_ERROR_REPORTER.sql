SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jimmy Millican
-- Create date: 11 june 2007
-- Description:	test for learning net
-- =============================================
CREATE PROCEDURE [dbo].[RSL_ERROR_REPORTER]
	-- Add the parameters for the stored procedure here
	@ERRORID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--- CREDIT NOTES THAT DONT SUM TO THE SUM IN THE NOMINAL
DECLARE @SQL  AS VARCHAR(8000)
SELECT @SQL = SQLCHECK FROM RSL_CHECKS WHERE CHECKID = @ERRORID

execute (@SQL)

	
END



GO
