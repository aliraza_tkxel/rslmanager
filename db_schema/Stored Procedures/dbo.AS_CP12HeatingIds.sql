SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC	[dbo].[AS_CP12JournalIds] @propertyId = N'A010060001'
-- =============================================
-- Author:		<Author,,NoorMuhammad>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

IF OBJECT_ID('dbo.AS_CP12HeatingIds') IS NULL
 EXEC('CREATE PROCEDURE dbo.AS_CP12HeatingIds AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[AS_CP12HeatingIds] 
	@propertyId AS varchar(20),
	@schemeId AS INT,
	@blockId AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    
    

	SELECT	AJ.JournalId, PL.PropertyId, PL.SchemeId, PL.BlockId, Issuedate , PL.HeatingMappingId AS HeatingId
	FROM	P_LGSR PL
			INNER JOIN AS_JOURNAL AJ ON PL.JOURNALID = AJ.JOURNALID
			INNER JOIN AS_STATUS AST on AJ.statusid = AST.statusid
	WHERE	(PL.PROPERTYID = @propertyId OR PL.SCHEMEID = @schemeId OR PL.BLOCKID = @blockId)
			AND AST.title= 'Certificate issued' AND PL.HeatingMappingId IS NOT NULL
	
END

GO
