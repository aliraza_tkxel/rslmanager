USE [RSLBHALive]
GO 
/****** Object:  StoredProcedure [dbo].[AS_GetAlternativeAppointmentsByProperty]    Script Date: 07/12/2018 11:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
 --EXEC AS_GetAlternativeAppointmentsByProperty
	--		@PropertyId = 'P100280005'
		
-- Author:		<Shehriyar Zafar>
-- Modified By: <Shehriyar, July 12, 2018>
-- Create date: <15 Sep 2012>
-- Description:	<This stored procedure returns the alternative appointments against property id>
-- Parameters:	
		--@PropertyId
-- ============================================= */


IF OBJECT_ID('dbo.AS_GetAlternativeAppointmentsByProperty') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetAlternativeAppointmentsByProperty AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetAlternativeAppointmentsByProperty]
( 
		--These parameters will be used for search
		@PropertyId Nvarchar(50) = ''
)
AS
BEGIN
		SELECT ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')+ ISNULL(' , '+P__PROPERTY.TOWNCITY,'')  AS ADDRESS ,  
				P__PROPERTY.HouseNumber as HouseNumber,
				P__PROPERTY.ADDRESS1 as ADDRESS1,
				P__PROPERTY.ADDRESS2 as ADDRESS2,
				P__PROPERTY.ADDRESS3 as ADDRESS3,	
				P__PROPERTY.POSTCODE as POSTCODE,
				CONVERT(varchar,DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103)AS EXPIRYDATE,
				DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS DAYS,							
				Case When ISNULL(PV.ValueDetail, '-') = 'Please Select' Then '-'
				ELSE ISNULL(PV.ValueDetail, '-') end AS FUEL,
				P__PROPERTY.PROPERTYID,
				C_TENANCY.TENANCYID,
				AS_JOURNAL.JOURNALID,
				Case AS_Status.Title WHEN 'No Entry' then 'No Entry ('+ convert(varchar(10),
					(SELECT Count(JOURNALHISTORYID)
					FROM AS_JournalHistory 
					Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
					AND AS_JournalHistory.StatusId=3 AND 
					YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
					))+')' 
				WHEN 'Aborted' then 'Aborted ('+ convert(varchar(10),
					(SELECT Count(JOURNALHISTORYID)
					FROM AS_JournalHistory 
					Where AS_JournalHistory.JOURNALID = AS_JOURNAL.JOURNALID 
					AND AS_JournalHistory.StatusId IN (select StatusId from AS_Status WHERE Title LIKE 'Aborted') AND 
					YEAR(AS_JournalHistory.CREATIONDATE) = YEAR(GETDATE())
								
					))+')' 
				else 
					AS_Status.Title end AS StatusTitle,
				ISNULL(P_STATUS.DESCRIPTION, 'N/A') AS PropertyStatus,
				E_PATCH.PatchId as PatchId,
				E_PATCH.Location as PatchName,
				ISNULL(CA.Tel,'N/A') as Telephone,
				CUST.FIRSTNAME  + ' ' + CUST.LASTNAME AS NAME,
				CONVERT(varchar,P_LGSR.ISSUEDATE,103)AS ISSUEDATE,
				P_LGSR.ISSUEDATE as LGSRDate,
				CT.CUSTOMERID as CustomerId,
				ISNULL(APP.APPOINTMENTID, 0) AS APPOINTMENTID
				,riskCounter.risk, CA.EMAIL, P__PROPERTY.TOWNCITY, P__PROPERTY.COUNTY
				,1 as BoilersCount
				,'Property' as AppointmentType
				,ISNULL(APP.Duration,1) as Duration

	FROM P__PROPERTY 
				CROSS APPLY (SELECT COUNT(*) AS BOILERSCOUNT, PROPERTYID AS PROPERTYID, MAX(PA_HeatingMapping.HeatingMappingId) AS HeatingMappingId
							FROM PA_HeatingMapping
							WHERE PROPERTYID = P__PROPERTY.PROPERTYID and IsActive = 1
							GROUP BY PROPERTYID) AS HeatingMapping

				INNER JOIN 
				(SELECT	P__PROPERTY.PROPERTYID, FUELS  = STUFF(
							(SELECT ', ' + PV.ValueDetail
								FROM PA_HeatingMapping HM
								Inner JOIN PA_PARAMETER_VALUE PV on HM.HeatingType = PV.ValueId
								WHERE HM.IsActive = 1 AND PV.IsAlterNativeHeating = 1 AND P__PROPERTY.PropertyId = HM.PropertyId AND PV.IsActive = 1
								FOR XML PATH('')), 1, 2, '')
				FROM	P__PROPERTY 
				GROUP BY P__PROPERTY.PROPERTYID) As FuelTypes 
				on P__PROPERTY.PROPERTYID = FuelTypes.PropertyId

				INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HeatingMapping.HeatingMappingId
				INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID
				LEFT JOIN AS_APPOINTMENTS APP ON APP.JOURNALID = dbo.AS_JOURNAL.JOURNALID								
				INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
				--INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
				LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
				LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
				INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
				LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
				INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
										
										
				OUTER Apply(SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
								Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
								ELSE Convert(Varchar(50), M.ReletDate,103)END as ReletDate,J.JOURNALID
							FROM P__PROPERTY P
					INNER JOIN C_JOURNAL J ON C_TENANCY.TENANCYID =J.TENANCYID
					INNER JOIN C_TERMINATION T ON J.JOURNALID = T.JOURNALID 
					INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
					INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
					INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
					INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
					WHERE ((DATEADD(YEAR,1,P_LGSR.ISSUEDATE) BETWEEN T.TTIMESTAMP AND T.TerminationDate)
							OR (DATEADD(YEAR,1,P_LGSR.ISSUEDATE) > T.TerminationDate))
					) As  T
										
				Left JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = C_TENANCY.TENANCYID
				AND CT.CUSTOMERTENANCYID = (SELECT	MIN(CUSTOMERTENANCYID)
															FROM	C_CUSTOMERTENANCY 
															WHERE	TENANCYID=C_TENANCY.TENANCYID )

				Left JOIN C_ADDRESS CA ON CA.CUSTOMERID = CT.CUSTOMERID And CA.ISDEFAULT=1
				Left JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = CT.CUSTOMERID
				LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P__PROPERTY.PROPERTYID
				AND A.ITEMPARAMID = (	SELECT	ItemParamID
										FROM	PA_ITEM_PARAMETER
												INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
												INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
										WHERE	ParameterName = 'Heating Fuel' AND ItemName = 'Heating')
				LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
				Outer Apply (SELECT count( RISKHISTORYID) as risk
				FROM C_JOURNAL J 
				INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID
				WHERE CUSTOMERID = CUST.CUSTOMERID AND ITEMNATUREID=63 
				AND CR.RISKHISTORYID = (SELECT MAX(RISKHISTORYID) FROM C_RISK IN_CR WHERE IN_CR.JOURNALID=J.JOURNALID)                 
				AND CR.ITEMSTATUSID <> 14) riskCounter 
																			
	WHERE 
				1=1 
				AND (AS_JOURNAL.STATUSID  IN (select StatusId from AS_Status WHERE Title LIKE 'Appointment to be arranged' OR Title LIKE 'No Entry' OR Title LIKE 'Legal Proceedings' OR Title LIKE 'Aborted' OR TITLE LIKE 'Cancelled'))		
				AND AS_JOURNAL.IsCurrent = 1
				AND dbo.P__PROPERTY.STATUS IN (SELECT STATUSID FROM P_STATUS WHERE DESCRIPTION IN('Let','Available to rent','Unavailable')) 
				AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
				AND PV.IsAlterNativeHeating = 1
				AND dbo.P__PROPERTY.PROPERTYID = @PropertyId						
END


