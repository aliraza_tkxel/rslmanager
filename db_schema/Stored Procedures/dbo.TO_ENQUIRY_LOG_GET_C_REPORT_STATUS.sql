SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_GET_C_REPORT_STATUS]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GET_CUSTOMERID
 '   DATE CREATED:   17 JUNE 2008
 '   CREATED BY:     Zahid Ali
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get nature id  and its description for populating nature dropdown on
 '					 customer enquiry page
 '   IN:             @itemID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	AS
	
	SELECT  
			C_STATUS.ITEMSTATUSID As id,
			C_STATUS.DESCRIPTION As val
            
            
	FROM
	       C_STATUS
	       
	ORDER BY C_STATUS.DESCRIPTION ASC 


GO
