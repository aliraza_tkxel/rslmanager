USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetRemovedMiscTradesFromScheduling]    Script Date: 11/18/2016 2:52:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[PLANNED_GetRemovedMiscTradesFromScheduling]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetRemovedMiscTradesFromScheduling] AS SET NOCOUNT ON;') 
GO 
ALTER  PROCEDURE [dbo].[PLANNED_GetRemovedMiscTradesFromScheduling] 
	@pmo int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT DISTINCT
		G_TRADE.TradeId as ComponentId
		,G_TRADE.Description as Component
		,COALESCE ( PLANNED_REMOVED_SCHEDULING_TRADES.DURATION,PLANNED_MISC_TRADE.DURATION ) as Duration
		,G_TRADE.TradeId as ComponentTradeId
		,G_TRADE.TradeId  as TradeId
		,G_TRADE.Description  as Trade
		,0 as StatusId
		,'Removed from scheduling' as Status
		,PAT.Planned_Appointment_Type
	FROM 
		PLANNED_REMOVED_SCHEDULING_TRADES  
		INNER JOIN PLANNED_MISC_TRADE ON PLANNED_MISC_TRADE.TRADEID = PLANNED_REMOVED_SCHEDULING_TRADES.MISCTRADEID 
		INNER JOIN G_TRADE ON G_TRADE.TradeId = PLANNED_REMOVED_SCHEDULING_TRADES.MISCTRADEID 
		INNER JOIN PLANNED_JOURNAL J ON J.JOURNALID = PLANNED_REMOVED_SCHEDULING_TRADES.JOURNALID
		INNER JOIN Planned_Appointment_Type PAT ON PAT.Planned_Appointment_TypeId=J.APPOINTMENTTYPEID
	WHERE 
		PLANNED_REMOVED_SCHEDULING_TRADES.JOURNALID = @pmo
    
END
