USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.PDR_GetSchemeBlockLocations') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetSchemeBlockLocations AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_GetSchemeBlockLocations] 
AS
BEGIN

	SELECT LocationID, LocationName FROM PA_LOCATION

	UNION ALL

	SELECT AreaID AS LocationID, AreaName AS LocationName 
	FROM PA_AREA
	WHERE IsForSchemeBlock = 1

END