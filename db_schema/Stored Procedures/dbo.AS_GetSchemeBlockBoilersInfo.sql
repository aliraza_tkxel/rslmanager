USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AppointmentEngineerInfo]    Script Date: 11/28/2012 02:01:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_AppointmentEngineerInfo
-- Author:		<Aqib Javed>
-- Create date: <20/10/2012>
-- Description: <This Store procedure shall provide the engineerings list that are scheduled for GAS inspection.>
-- Webpage : FuelScheduling.aspx
-- =============================================
IF OBJECT_ID('dbo.[AS_GetSchemeBlockBoilersInfo]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetSchemeBlockBoilersInfo] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetSchemeBlockBoilersInfo] 
(
	@Id int,				
	@AppointmentType varchar(50)=''	
)	
AS
BEGIN
	
	if @AppointmentType = 'Scheme' 
	begin
		select 'Boiler '+ Convert(varchar,ROW_NUMBER() OVER(ORDER BY HM.HeatingMappingId ASC)) AS BoilerName, PV.ValueDetail AS HeatingFuel,
			PVT.ValueDetail AS BoilerType, PVMAN.ValueDetail as Manufacturer,
			'Expiry ' + ISNULL(RIGHT('00' + CAST(DATEPART(DAY, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS VARCHAR(2)), 2) + ' ' +
						DATENAME(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) + ' ' +
						CAST(DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE))  AS VARCHAR(4)), 'N/A') AS EXPIRYDATE
		FROM P_SCHEME 
		INNER JOIN PA_HeatingMapping HM ON HM.SchemeID = P_SCHEME.SCHEMEID
		INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HM.HeatingMappingId
		INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.JOURNALID=JHM.JOURNALID
		LEFT JOIN AS_APPOINTMENTS APP ON APP.JournalId = AS_JOURNAL.JOURNALID								
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		LEFT JOIN (SELECT P_LGSR.ISSUEDATE,HeatingMappingId from P_LGSR)as P_LGSR on HM.HeatingMappingId = P_LGSR.HeatingMappingId
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.SchemeId = P_SCHEME.SCHEMEID AND A.HeatingMappingId = HM.HeatingMappingId
		AND A.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Heating Fuel' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES HT ON HT.SchemeId = P_SCHEME.SCHEMEID AND HT.HeatingMappingId = HM.HeatingMappingId
		AND HT.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Heating Type' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVT ON HT.VALUEID = PVT.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES MANUF ON MANUF.SchemeId = P_SCHEME.SCHEMEID AND MANUF.HeatingMappingId = HM.HeatingMappingId
		AND MANUF.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Manufacturer' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVMAN ON MANUF.VALUEID = PVMAN.ValueID
		where AS_JOURNAL.JOURNALID=@Id AND PV.ValueDetail = 'Mains Gas' AND HM.IsActive = 1
		ORDER BY HM.HeatingMappingId
	end
	else if @AppointmentType = 'Block' 
	begin
		select 'Boiler '+ Convert(varchar,ROW_NUMBER() OVER(ORDER BY HM.HeatingMappingId ASC)) AS BoilerName, PV.ValueDetail AS HeatingFuel,
			PVT.ValueDetail AS BoilerType, PVMAN.ValueDetail as Manufacturer,
			'Expiry ' + ISNULL(RIGHT('00' + CAST(DATEPART(DAY, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS VARCHAR(2)), 2) + ' ' +
						DATENAME(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) + ' ' +
						CAST(DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE))  AS VARCHAR(4)), 'N/A') AS EXPIRYDATE
		FROM P_BLOCK 
		INNER JOIN PA_HeatingMapping HM ON HM.BlockID = P_BLOCK.BLOCKID
		INNER JOIN AS_JournalHeatingMapping JHM ON JHM.HeatingMappingId = HM.HeatingMappingId
		INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.JOURNALID=JHM.JOURNALID
		LEFT JOIN AS_APPOINTMENTS APP ON APP.JournalId = AS_JOURNAL.JOURNALID						
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		LEFT JOIN (SELECT P_LGSR.ISSUEDATE,HeatingMappingId from P_LGSR)as P_LGSR on HM.HeatingMappingId = P_LGSR.HeatingMappingId
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.BlockId = P_BLOCK.BLOCKID AND A.HeatingMappingId = HM.HeatingMappingId
		AND A.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Heating Fuel' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES HT ON HT.BlockId = P_BLOCK.BLOCKID AND HT.HeatingMappingId = HM.HeatingMappingId
		AND HT.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Heating Type' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVT ON HT.VALUEID = PVT.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES MANUF ON MANUF.BlockId = P_BLOCK.BLOCKID AND MANUF.HeatingMappingId = HM.HeatingMappingId
		AND MANUF.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Manufacturer' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVMAN ON MANUF.VALUEID = PVMAN.ValueID
		where AS_JOURNAL.JOURNALID=@Id AND PV.ValueDetail = 'Mains Gas' AND HM.IsActive = 1
		ORDER BY HM.HeatingMappingId
	end
	
END
