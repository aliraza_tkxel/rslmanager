SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[TO_COMPLAINT_SelectComplaintDetail]

/* ===========================================================================
 '   NAME:           TO_COMPLAINT_SelectComplaintDetail
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select single TO_COMPLAINT record based on EnquiryLogID provided                     
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
            		TO_COMPLAINT.CategoryID
                        
	FROM
			TO_ENQUIRY_LOG INNER JOIN TO_COMPLAINT 
			ON TO_ENQUIRY_LOG.EnquiryLogID = TO_COMPLAINT.EnquiryLogID	       	       
			
	WHERE
	
	     TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID
	     
	     


GO
