SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE   PROCEDURE [dbo].[TO_ENQUIRY_LOG_UpdateJournalID]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_UpdateJournalID
 '   DATE CREATED:   7 JULY 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To update journalID, ItemStatusID for the passed enquiryID into TO_ENQUIRY_LOG
 '   IN:             @journalID, @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
 
  @enqLogID INT,
  @journalID INT
 )
 
 AS

DECLARE @ItemStatusID INT
SET @ItemStatusID = 27 -- ACTIONED in C_STATUS table

 -- Updating TO_ENQUIRY_LOG
 
 UPDATE

  TO_ENQUIRY_LOG
                                
 SET
  JOURNALID = @journalID,
  ITEMSTATUSID = @ItemStatusID
   
 WHERE
 
  ENQUIRYLOGID = @enqLogID      
     


GO
