USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetCoreAndOutOfHoursInfo]    Script Date: 05/26/2014 11:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetCoreAndOutOfHoursInfo
-- Author:		<Ahmed Mehmood>
-- Create date: <23/09/2012>
-- Description:	<This Stored Procedure Get Core And Out Of Hours Info >
-- Web Page: Resources.aspx
-- =============================================
IF OBJECT_ID('dbo.AS_GetCoreAndOutOfHoursInfo') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetCoreAndOutOfHoursInfo AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_GetCoreAndOutOfHoursInfo](
@employeeId int
)
AS
BEGIN

	--====================
	-- CORE WORKING HOURS
	--====================

	SELECT	E_WEEKDAYS.DAYID AS DayId
			,E_WEEKDAYS.NAME AS Name
			,E_CORE_WORKING_HOURS.EMPLOYEEID AS EmployeeId
			,E_CORE_WORKING_HOURS.STARTTIME AS StartTime
			,E_CORE_WORKING_HOURS.ENDTIME AS EndTime
	FROM	E_WEEKDAYS
			LEFT JOIN  (SELECT	*
						FROM	E_CORE_WORKING_HOURS
						WHERE	EMPLOYEEID = @employeeId
						) AS E_CORE_WORKING_HOURS
						ON E_WEEKDAYS.DAYID = E_CORE_WORKING_HOURS.DAYID
	ORDER BY SORDER ASC
	
	--====================
	-- CORE WORKING HOURS
	--====================

	SELECT	E_WEEKDAYS.DAYID AS DayId
			,E_WEEKDAYS.NAME AS Name
			,E_CORE_WORKING_HOURS.EMPLOYEEID AS EmployeeId
			,E_CORE_WORKING_HOURS.STARTTIME AS StartTime
			,E_CORE_WORKING_HOURS.ENDTIME AS EndTime
	FROM	E_CORE_WORKING_HOURS
			INNER JOIN E_WEEKDAYS ON  E_CORE_WORKING_HOURS.DAYID = E_WEEKDAYS.DAYID 
	WHERE	EMPLOYEEID = @employeeId

	--====================
	-- OUT OF HOURS 
	--====================

	SELECT	OUTOFHOURSID AS OutOfHoursId
			,E_OUT_OF_HOURS.TYPEID	AS TypeId
			,TYPE	AS Type
			,STARTDATE AS StartDate
			,ENDDATE  AS EndDate
			,STARTTIME AS StartTime
			,ENDTIME  AS EndTime
			,CAST(CAST(STARTDATE AS DATE) AS DATETIME) + CAST(STARTTIME AS DATETIME) AS GeneralStartDate
			,CAST(CAST(ENDDATE AS DATE) AS DATETIME) + CAST(ENDTIME AS DATETIME) AS GeneralEndDate
			
	FROM	E_OUT_OF_HOURS
			INNER JOIN E_OUT_OF_HOURS_TYPE ON E_OUT_OF_HOURS.TYPEID = E_OUT_OF_HOURS_TYPE.TYPEID
	WHERE	EMPLOYEEID = @employeeId AND  CONVERT(Datetime,STARTDATE ) > CONVERT(Datetime,DATEADD(day,-45,getDate())) 
	ORDER BY GeneralStartDate DESC
	
	
	--====================
	-- OUT OF HOURS TYPE
	--====================

	SELECT	TYPEID	AS TypeId
			,TYPE	AS Type			
	FROM	 E_OUT_OF_HOURS_TYPE



END
