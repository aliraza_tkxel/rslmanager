USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].S_GetOrganizationData    Script Date: 6/6/2017 4:33:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Moiz
-- Create date: June 2, 2017
-- =============================================

IF OBJECT_ID('dbo.S_GetOrganizationData') IS NULL 
	EXEC('CREATE PROCEDURE dbo.S_GetOrganizationData AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[S_GetOrganizationData] 
	-- Add the parameters for the stored procedure here
	@organizationId INT
	
AS
Begin
	select NAME,ADDRESS1,ADDRESS2, ADDRESS3,TOWNCITY,POSTCODE,COUNTY from 
	S_ORGANISATION where ORGID = @organizationId
end
