SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TC_GETPROGRAMME]
@TYPEID INT
AS 
SELECT PROGRAMMEID,PROGRAMMETITLE FROM TC_PROGRAMME
WHERE PROGRAMMETYPEID=@TYPEID


GO
