SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_COMPLAINT_C_ADDRESS_TRANSFER]

/* ===========================================================================
 '   NAME:           TO_COMPLAINT_C_ADDRESS_TRANSFER
 '   DATE CREATED:   13 JUNE 2008
 '   CREATED BY:     Zahid Ali
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display Enquiry Detail i.e. TRANSFER Part of C_Address fields 

are also selected
                     as subset as these need to be displayed on View enquiry pop up.  
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description, 
            TO_LOOKUP_VALUE.Name As NameText,
            G_LOCALAUTHORITY.DESCRIPTION as LocalAuthority,
            P_DEVELOPMENT.DEVELOPMENTNAME as Development,
            TO_TRANSFER_EXCHANGE.NOOFBEDROOMS,
            TO_TRANSFER_EXCHANGE.OCCUPANTSNOGREATER18,
            TO_TRANSFER_EXCHANGE.OCCUPANTSNOBELOW18,
			C_ADDRESS.HOUSENUMBER, 
			C_ADDRESS.ADDRESS1, 
			C_ADDRESS.ADDRESS2, 
			C_ADDRESS.ADDRESS3, 
			C_ADDRESS.TOWNCITY, 
            C_ADDRESS.POSTCODE, 
            C_ADDRESS.COUNTY, 
            C_ADDRESS.TEL 
            
	FROM
	       TO_ENQUIRY_LOG LEFT JOIN TO_TRANSFER_EXCHANGE 
	       ON TO_ENQUIRY_LOG.EnquiryLogID = TO_TRANSFER_EXCHANGE.EnquiryLogID
	       LEFT JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID 
	       LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	       LEFT JOIN G_LOCALAUTHORITY ON TO_TRANSFER_EXCHANGE.LocalAuthorityID = G_LOCALAUTHORITY.LOCALAUTHORITYID
	       LEFT JOIN P_DEVELOPMENT ON TO_TRANSFER_EXCHANGE.DevelopmentID = P_DEVELOPMENT.DEVELOPMENTID
	       LEFT JOIN TO_LOOKUP_VALUE ON TO_TRANSFER_EXCHANGE.LOOKUPVALUEID = TO_LOOKUP_VALUE.LOOKUPVALUEID
	       
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)
	     AND (C_ADDRESS.ISDEFAULT = 1) 
	     

GO
