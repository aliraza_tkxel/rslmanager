USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetTenureSelectionType]    Script Date: 21/03/2019 11:46:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
   

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-6-2015      Raja Aneeq           Get Tenure Type for dropdown
    
    Execution Command:
    
    Exec PDR_GetTenureSelectionType
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetTenureSelectionType') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetTenureSelectionType AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[PDR_GetTenureSelectionType]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT TENURETYPEID,TENURETYPE FROM P_TENURESELECTION order by TENURETYPE ASC
	
END
