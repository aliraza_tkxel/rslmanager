SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--		DECLARE	@return_value int,
--		@propertyAddress varchar(100)

--		EXEC	@return_value = [dbo].[P_GetPropertyAddress]
--				@propertyId = N'A010060001',
--				@propertyAddress = @propertyAddress OUTPUT

--		SELECT	@propertyAddress as N'@propertyAddress'

--		SELECT	'Return Value' = @return_value

-- Author:		<Ahmed Mehmood>
-- Create date: <14/9/2013>
-- Description:	<This procedure selects the property address against property id>
-- Web Page: PropertyRecordSummary.aspx
-- =============================================
CREATE PROCEDURE [dbo].[P_GetPropertyAddress](
@propertyId varchar(20),
@propertyAddress varchar(100) out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT @propertyAddress = ISNULL(P__PROPERTY.HouseNumber,'') +' , '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') +' , '+ ISNULL(P__PROPERTY.TOWNCITY ,'')+' '+ ISNULL(P__PROPERTY.POSTCODE ,'')
		From P__PROPERTY 
		Where P__PROPERTY.PROPERTYID = @propertyId
END
GO
