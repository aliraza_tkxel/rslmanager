SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_SaveUserInspectionType @employeeId = 161 @inspectionTypeId = 2 
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,29/09/2012>
-- Description:	<Description,,get User Inspection Types for showing in DropDown>
-- WEB Page:  Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveUserInspectionType]
	-- Add the parameters for the stored procedure here
	@employeeId int,
	@inspectionTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO AS_USER_INSPECTIONTYPE (EmployeeId,InspectionTypeID) VALUES 
										(@employeeId,@inspectionTypeId)
END
GO
