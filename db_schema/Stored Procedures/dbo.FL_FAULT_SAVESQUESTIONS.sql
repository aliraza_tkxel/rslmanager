SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_FAULT_SAVESQUESTIONS 
	/* ===========================================================================
 '   NAME:           FL_FAULT_SAVESQUESTIONS
 '   DATE CREATED:   31 Oct. 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add the satisfactory question in FL_SATISFACTION_ANSWER table
 '   IN:             @QUESTIONID
 '   IN:             @ANSWER
 '   IN:             @FAULTLOGID 
 '
 '   OUT:            @SQID
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@QUESTIONID INT,
	@ANSWER NVARCHAR(4000),
	@FAULTLOGID INT,
	@SAID INT=0 OUTPUT
	)
	
AS
		
	BEGIN TRAN
		 IF (SELECT count(*) FROM FL_SATISFACTION_ANSWER WHERE FaultLogID = @FAULTLOGID AND SQuestionID = @QUESTIONID) = 0
		 BEGIN
			INSERT INTO FL_SATISFACTION_ANSWER(
			FaultLogID,
			SQuestionID,
			SAnswer
			)
			VALUES(
			@FAULTLOGID,
			@QUESTIONID,
			@ANSWER
			)
			
		
			SELECT @SAID=SelectiveAnswerID
			FROM FL_SATISFACTION_ANSWER
			WHERE SelectiveAnswerID = @@IDENTITY		
		END
						
IF @SAID<=0
	BEGIN		
									
		SET @SAID =-1
		ROLLBACK TRAN				
		RETURN
	END	
	
SET @SAID = 1
COMMIT TRANSACTION
	
  
		













GO
