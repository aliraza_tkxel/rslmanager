SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_AddCategory @categoryTitle = 'NewCategory'
-- Author:		<Salman Nazir>
-- Create date: <06/11/2012>
-- Description:	<Add new category into the database >
-- Web Page: PropertyRecord.aspx => Add Category PopUp
-- =============================================
CREATE PROCEDURE [dbo].[AS_AddCategory] (
@categoryTitle varchar(500)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO P_DEFECTS_CATEGORY
	(Description)
	Values
	(@categoryTitle)
END
GO
