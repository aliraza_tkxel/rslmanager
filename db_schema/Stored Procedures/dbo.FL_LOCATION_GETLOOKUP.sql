SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_LOCATION_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_LOCATION_GETLOOKUP
 '   DATE CREATED:   16 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Location FL_LOCATION table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT LocationID AS id,LocationName AS val
	FROM FL_LOCATION
	ORDER BY LocationName ASC













GO
