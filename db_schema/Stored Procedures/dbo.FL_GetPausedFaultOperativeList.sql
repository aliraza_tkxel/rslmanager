SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
--EXEC	[dbo].[FL_GetPausedFaultOperativeList]	
-- Author:		<Ahmed Mehmood>
-- Create date: <26/7/2013>
-- Description:	<Get operative list who paused the fault.>
-- Web Page: ReportsArea.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetPausedFaultOperativeList] 
	
AS
BEGIN

	SELECT DISTINCT E__EMPLOYEE.EMPLOYEEID as OperativeId, E__EMPLOYEE.FIRSTNAME as firstname,
	E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME as Operative
	FROM	FL_FAULT_PAUSED 
			INNER JOIN E__EMPLOYEE ON FL_FAULT_PAUSED.PausedBy= E__EMPLOYEE.EMPLOYEEID 
	ORDER BY E__EMPLOYEE.FIRSTNAME
	    
END


GO
