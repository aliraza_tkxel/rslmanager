USE [RSLBHALive]
GO
IF OBJECT_ID('dbo.NL_DeleteCode') IS NULL 
	EXEC('CREATE PROCEDURE dbo.NL_DeleteCode AS SET NOCOUNT ON;') 
GO


alter  PROCEDURE [dbo].[NL_DeleteCode]
( 
           @AccountId INT 
		    )
as
BEGIN

		DELETE FROM dbo.NL_ACCOUNT_TO_COMPANY WHERE ACCOUNTID = @AccountId
		DELETE FROM dbo.NL_ACCOUNT WHERE ACCOUNTID = @AccountId
		     

END

