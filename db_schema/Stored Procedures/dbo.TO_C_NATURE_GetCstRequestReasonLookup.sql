SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_C_NATURE_GetCstRequestReasonLookup
/* ===========================================================================
 '   NAME:           TO_C_NATURE_GetCstRequestReasonLookup
 '   DATE CREATED:   20 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get nature C_NATURE table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT ITEMNATUREID AS id,DESCRIPTION AS val
	FROM C_NATURE
	WHERE ITEMID=2 and
	ITEMNATUREID IN(5, 10, 8, 26, 28, 7, 4, 9, 27, 6, 59)
	ORDER BY DESCRIPTION ASC

GO
