USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveSmsAndEmailStatus]    Script Date: 27/01/2017 15:01:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_SavePushNoticificationStatus') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_SavePushNoticificationStatus AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_SavePushNoticificationStatus] 
	@journalHistoryId int,
	@pushnoticificationStatus nvarchar(max),
	@pushnoticificationDescription nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;			
	IF EXISTS (Select 1 from [AS_APPOINTMENTS] where [JOURNALHISTORYID] = @journalHistoryId)	
	BEGIN	
			Update [dbo].[AS_APPOINTMENTS] SET
					[PushNoticificationId]=(select [PushNoticificationId] from AS_PushNoticificationStatus where StatusDescription=@pushnoticificationStatus)
				   ,[PushNoticificationDescription] = @pushnoticificationDescription
				   WHERE [JOURNALHISTORYID] = @journalHistoryId  
	END
   
END

