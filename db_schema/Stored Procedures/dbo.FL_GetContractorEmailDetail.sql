USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetContractorEmailDetail]    Script Date: 13-Jul-17 11:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.FL_GetContractorEmailDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetContractorEmailDetail AS SET NOCOUNT ON;') 
GO

-- =============================================  
 --EXEC FL_GetContractorEmailDetail  
 -- @FaultLogId = 5,  
 --@propertyId ='A010000018',@isRecall=0
-- Author:  <Ahmed Mehmood>  
-- Create date: <30/01/2013>  
-- Description: <Get Fault Basket Info FL_TEMP_FAULT table>  
-- Web Page: FaultBasket.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[FL_GetContractorEmailDetail]  
 (  
  @FaultLogId  VARCHAR(100)
 ,@propertyId NVARCHAR(20)
 ,@BlockId INT	    
 )  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @contractorId INT

select @contractorId=FW.ContactId from FL_FAULT_JOURNAL fj
	inner join FL_FAULT_LOG fl on fl.FaultLogID=fj.FaultLogID
	inner join FL_CONTRACTOR_WORK fw on fw.JournalId=fj.JournalID
	where fl.FaultLogID=@FaultLogId

--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
	INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @contractorId

--=================================================
--Get Property Detail(s)
--=================================================
SELECT
	ISNULL(P.PROPERTYID,'') PROPERTYID,
	ISNULL(HOUSENUMBER,'') HOUSENUMBER,
	ISNULL(FLATNUMBER,'') FLATNUMBER,
	ISNULL(ADDRESS1,'') ADDRESS1,
	ISNULL(ADDRESS2,'') ADDRESS2,
	ISNULL(ADDRESS3,'') ADDRESS3,
	ISNULL(TOWNCITY,'') TOWNCITY,
	ISNULL(COUNTY,'') COUNTY,
	ISNULL(POSTCODE,'') POSTCODE,
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, '') + ISNULL(' ' + TOWNCITY, '')
	+ ISNULL(', ' + COUNTY, '') + ISNULL(', ' + POSTCODE, ''), ''), 'N/A') AS [FullAddress],
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress]
FROM FL_FAULT_LOG fl
left join P__PROPERTY P on P.PROPERTYID=fl.PROPERTYID
WHERE fl.FaultLogID = @FaultLogId


--=================================================
--Get Block Detail(s)
--=================================================
SELECT
	ISNULL(P.BLOCKID,0) as BlockId, ISNULL(P.BLOCKNAME,'') AS BlockName
FROM FL_FAULT_LOG fl
left join P__PROPERTY PR on PR.PROPERTYID=fl.PROPERTYID
LEFT JOIN P_BLOCK P on  P.BLOCKID	=
	Case WHEN FL.BlockId is NULL THEN PR.BLOCKID 
		ELSE 		FL.BlockId 	END
WHERE fl.FaultLogID = @FaultLogId
 
--=================================================
--Get PO and Work Required Details
--=================================================

select fwd.FaultWorkDetailId as WorkDetailId,
 fwd.WorkRequired as WorkRequired,
 fw.PurchaseORDERID as POID,
 CONVERT(DECIMAL(10,2),FWD.NetCost) AS NetCost,
 CONVERT(DECIMAL(10,2),FWD.vat) AS VAT,
 CONVERT(DECIMAL(10,2),FWD.Gross) AS Gross
from FL_FAULT_JOURNAL fj
inner join FL_FAULT_LOG fl on fl.FaultLogID=fj.FaultLogID
inner join FL_CONTRACTOR_WORK fw on fw.JournalId=fj.JournalID
inner join FL_CONTRACTOR_WORK_DETAIL fwd on fwd.FaultContractorId=fw.FaultContractorId
where fl.FaultLogID=@FaultLogId

END