USE [RSLBHALive]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
-- Author:		Aamir Waheed
-- Create date: 05 August 2013
-- Description:	To calculate absence hour for previous month mean 1st date of previous month to last date of previous month. or for dates providedd as parameters.
-- EXEC	[dbo].[TS_CalculateAbsenceHours]
-- EXEC	[dbo].[TS_CalculateAbsenceHours] @jobStartDate = '01/06/2016', @jobEndDate = '31/06/2016'
-- Few notes about stored procedure execution
-- 1- If strored procedure is executed without giving any dates,
--	 it will run for dates, First date of previous month to today.
-- 2- If either on of start date or end is set it will set the other 60 days away.
-- 3- In case both dates are provided, the stored procedure will be executed for specified days.
-- ============================================= */

IF OBJECT_ID('dbo.[TS_CalculateAbsenceHours]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[TS_CalculateAbsenceHours] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[TS_CalculateAbsenceHours]

	-- Add the parameters for the stored procedure here
	@jobStartDate datetime = NULL,
	@jobEndDate datetime = NULL
AS
BEGIN
	
	IF @jobStartDate IS NOT NULL AND @jobEndDate IS NOT NULL AND DATEDIFF(DAY, @jobStartDate, @jobEndDate) < 0
	BEGIN
		-- Check for invalid date mean start date is after end date.
		-- In case end date is not less that srat date no further processing will be done.
		SELECT 'Kindly Provide an end date that is less than start date' AS [Invalid Dates]
		PRINT 'Kindly Provide an end Date that is less than start date'
		RETURN
	END
	ELSE IF @jobStartDate IS NULL AND @jobEndDate IS NULL
	BEGIN
		--Set start date to first day of previous month
		SET @jobStartDate = DATEADD(mm, DATEDIFF(m,0,GETDATE())-2,0)
		--Set end date to today with time part set to 23:59:59
		SET @jobEndDate = DATEADD(SECOND, -1, CONVERT(DATETIME,CONVERT(DATE,GETDATE()+1)))
	END
	ELSE IF @jobStartDate IS NOT NULL AND @jobEndDate IS NULL
	BEGIN
		--In case start date is set and end date is not set
		--Set end date 60 days from start date.
		SET @jobEndDate = DATEADD(DAY,60,@jobStartDate)
	END
	ELSE IF @jobEndDate IS NOT NULL AND @jobStartDate IS NULL
	BEGIN
		--In case end date is set and start date is not set
		--Set start date -90 days from end date.
		SET @jobStartDate = DATEADD(DAY, -90, @jobEndDate)
	END
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    PRINT 'Calcuting absence hours:'
			+ CHAR(10) 
			+ CHAR(10) + 'From: ' + CONVERT(NVARCHAR, @jobStartDate) 
			+ CHAR(10) + 'To: ' + CONVERT(NVARCHAR,@jobEndDate)

IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
    DROP TABLE #TempTable

--===============================================================================================

--===============================================================================================
-- Filter and Insert Records into temp table for leave(s) hours calculation

SELECT	MAX(E_ABSENCE.ABSENCEHISTORYID) AS ABSENCEHISTORYID
		,E_JOURNAL.EMPLOYEEID AS EMPLOYEEID		
		,E_ABSENCE.STARTDATE AS StartDate
		,E_ABSENCE.RETURNDATE AS EndDate		
		,E_JOURNAL.ITEMNATUREID AS ITEMNATUREID
		,CASE WHEN LEN(HOLTYPE)>= 1 THEN LEFT(HOLTYPE,1) END StartDateHoliDayType
		,CASE WHEN LEN(HOLTYPE)>= 1 THEN RIGHT(HOLTYPE,1) END EndDateHoliDayType

INTO #TempTable
FROM E_JOURNAL
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID=E_ABSENCE.JOURNALID 
	INNER JOIN E_ITEM ON E_JOURNAL.ITEMID = E_ITEM.ITEMID AND E_JOURNAL.ITEMID = 1
	INNER JOIN E_STATUS ON E_JOURNAL.CURRENTITEMSTATUSID = E_STATUS.ITEMSTATUSID	
	
WHERE 1 = 1
	AND((E_ABSENCE.STARTDATE>= @jobStartDate AND E_ABSENCE.RETURNDATE<=@jobEndDate) 
				OR (E_ABSENCE.STARTDATE<=@jobEndDate AND E_ABSENCE.RETURNDATE>=@jobStartDate)
				OR (E_ABSENCE.STARTDATE<=@jobStartDate AND E_ABSENCE.RETURNDATE>=@jobEndDate )
				OR (E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.STARTDATE BETWEEN @jobStartDate AND @jobEndDate ))
	AND( 
			(	
				--Filter for sick leaves
				E_JOURNAL.ITEMNATUREID = 1
				--AND E_JOURNAL.CURRENTITEMSTATUSID = 5	-- Approval is not required for sick leaves				
			)
		OR -- Filter for Other type of Leaves
			(	
				-- Filter for Other type of Leaves
				E_JOURNAL.ITEMNATUREID >= 2
				AND	E_JOURNAL.CURRENTITEMSTATUSID = 5	
			)
		)	
		
GROUP BY	E_JOURNAL.EMPLOYEEID
			,E_ABSENCE.STARTDATE
			,E_ABSENCE.RETURNDATE			
			,E_ABSENCE.HOLTYPE
			,E_JOURNAL.ITEMNATUREID

-- Filter and Insert Records into temp table for leave(s) hours calculation
--===============================================================================================

--===============================================================================================

-- Variables to store one record from temptable using select statement
DECLARE @absenceHistoryID INT
		,@employeeID INT
		,@startDate DATETIME
		,@endDate DATETIME
		,@itemNatureID INT
		,@startDateHoliDayType CHAR(1) = 'N'
		,@endDateHolidayType CHAR(1) = 'N'
		
-- Varialbes to store one record that will be added to E_ABSENCEHOURS after calculations
DECLARE	@absenceDate DATETIME,
		@absenceHoursSick FLOAT = 0.0,
		@absenceHoursOthers FLOAT = 0.0
		
-- Variables used only for calculation
DECLARE @counter INT = 0,
		@leaveDay DATETIME,
		@leaveDayHours FLOAT,
		@outOfHours FLOAT,			
		@weekDay INT

--------------------------------------------------------------------------------------------------
-- Iterate through the recoreds in Temp Table Calculate Leaves hour and Insert Into E_AbsenceHours

SET @absenceHistoryID = (SELECT MIN(ABSENCEHISTORYID) FROM #TempTable)

WHILE @absenceHistoryID IS NOT NULL
BEGIN	

	-- Select one record from temp table for calculation
	SELECT	@employeeID = EMPLOYEEID			
			,@startDate = StartDate
			,@endDate = EndDate
			,@itemNatureID = ITEMNATUREID
			,@startDateHoliDayType = StartDateHoliDayType
			,@endDateHolidayType = EndDateHoliDayType
	FROM #TempTable WHERE ABSENCEHISTORYID = @absenceHistoryID
	
	--==================================================================

	--==================================================================
	
	
--------------------------------------------------------------------------------------------------
	
	--Apply These Rules
	--Scenrio Leave Start Date is less than start of month/date under considration	
	IF @startDate IS NULL OR @startDate < @jobStartDate
	BEGIN
		SET @startDate = @jobStartDate
		IF @startDate = @endDate
			SET @startDateHoliDayType = @endDateHolidayType		
		ELSE
			SET @endDateHolidayType = 'F'		
	END
	
	--Scenrio Leave End Date is greater than end of month/date under considration	
	IF @endDate IS NULL OR @endDate > @jobEndDate
	BEGIN
		IF @endDate IS NULL 
		BEGIN
			SET @endDate = @startDate
		END
		ELSE
		BEGIN
			SET @endDate = @jobEndDate
		END
	
		IF @endDate = @startDate
			SET @endDateHolidayType = @startDateHoliDayType
		ELSE
			SET @endDateHolidayType = 'F'		
	END
	
--------------------------------------------------------------------------------------------------
	
	--===============
		-- Set first week of day to monday and store existing week of the day in memory
		DECLARE @DateFirst INT = @@DATEFIRST
		SET DATEFIRST 1
	--===============
	
	SET @counter = DATEDIFF(DD, @startDate, @endDate)
	
	WHILE @counter >= 0
	BEGIN
		
		-----------------------------------------------------------------------		
		-- Calculate Working hours of current leave day
		SET @leaveDay = DATEADD(DD,@counter, @startDate)		
		SET @weekDay = DATEPART(WEEKDAY, @leaveDay)

		--==================================
		-- Calculating Core working hours
		--==================================
		IF EXISTS (	SELECT	1
					FROM	E_CORE_WORKING_HOURS 
					WHERE	EMPLOYEEID = @employeeID )
					BEGIN
						SELECT	@leaveDayHours = CONVERT(DECIMAL(10,2),DATEDIFF(MINUTE, StartTime, EndTime)/ 60.0)
						FROM	E_CORE_WORKING_HOURS 
						WHERE	EMPLOYEEID = @employeeID 
						AND DAYID = @weekDay
						
					END
					ELSE
					BEGIN
						SELECT @leaveDayHours = CASE @weekDay 
									WHEN 6 THEN 0		
									WHEN 7 THEN 0						
									ELSE 8 
								END
					END
			
		--==================================
		-- Calculating Extended Working Hours
		--==================================
		SELECT  @outOfHours = ISNULL(SUM(CONVERT(DECIMAL(10,2),DATEDIFF(MINUTE,StartTime , EndTime)/ 60.0)),0 )
		FROM	E_OUT_OF_HOURS
		WHERE	CONVERT(VARCHAR(10),STARTDATE,103) = CONVERT(VARCHAR(10),@startDate,103) 
				AND EMPLOYEEID = @employeeID
		
		SET	@leaveDayHours = @leaveDayHours + @outOfHours
		
		-----------------------------------------------------------------------		
		-- Calculate sick absence hours and other absence hours
		SET @absenceHoursSick = 0.0
		SET	@absenceHoursOthers = 0.0
	
		IF @itemNatureID IS NOT NULL AND  @itemNatureID = 1 AND @leaveDayHours > 0
		BEGIN
			IF (@leaveDay = @startDate AND (@startDateHoliDayType IN('A','M')))
					OR (@leaveDay = @endDate AND (@endDateHolidayType IN('A','M')))
				SET @AbsenceHoursSick = 0.5 * @leaveDayHours
			ELSE
				SET @AbsenceHoursSick = @leaveDayHours

		END
		ELSE IF @itemNatureID IS NOT NULL AND  @itemNatureID > 1 AND @leaveDayHours > 0
		BEGIN
			IF (@leaveDay = @startDate AND (@startDateHoliDayType IN('A','M')))
					OR (@leaveDay = @endDate AND (@endDateHolidayType IN('A','M')))
				SET @AbsenceHoursOthers = 0.5 * @leaveDayHours
			ELSE
				SET @AbsenceHoursOthers = @leaveDayHours
	
		END
		
		-----------------------------------------------------------------------		
		-- Add or update the one record in table E_ABSENCEHOURS
		
		IF @leaveDayHours > 0 AND (@AbsenceHoursSick > 0 OR @AbsenceHoursOthers > 0)
		BEGIN			
			-- Add new record in table E_ABSENCEHOURS
			IF NOT EXISTS (SELECT AbsenceHourID FROM E_ABSENCEHOURS 
							WHERE EmployeeID = @employeeID AND AbsenceDate = CAST(@leaveDay AS DATE))
			BEGIN				
				INSERT INTO E_ABSENCEHOURS (EmployeeID, AbsenceDate, AbsenceHoursSick, AbsenceHoursOthers)
				VALUES (@employeeID, @leaveDay, @AbsenceHoursSick, @AbsenceHoursOthers)			
			END
			-- Update and existing record in table E_ABSENCEHOURS
			ELSE IF EXISTS (SELECT AbsenceHourID FROM E_ABSENCEHOURS 
							WHERE EmployeeID = @employeeID AND AbsenceDate = CAST(@leaveDay AS DATE))
			BEGIN
				Update E_ABSENCEHOURS
					SET AbsenceHoursSick = @AbsenceHoursSick,
						AbsenceHoursOthers = @AbsenceHoursOthers
				WHERE EmployeeID = @employeeID AND AbsenceDate = @leaveDay
			END		
		END
				
		-----------------------------------------------------------------------
	
		SET @counter = @counter - 1
	END
	
	--===============
		-- restore existing week of the day from memory
		SET DATEFIRST @DateFirst
	--===============
	
--------------------------------------------------------------------------------------------------
	
SET @absenceHistoryID = (SELECT MIN(ABSENCEHISTORYID) FROM #TempTable WHERE ABSENCEHISTORYID > @absenceHistoryID)
END
-- Iterate through the recoreds in Temp Table Calculate Leaves hour and Insert Into E_AbsenceHours
--===============================================================================================

--===============================================================================================

-- Delete temp table that was created during process in tempdb

IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
    DROP TABLE #TempTable
    
--===============================================================================================

END
