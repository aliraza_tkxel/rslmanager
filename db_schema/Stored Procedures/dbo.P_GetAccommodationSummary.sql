SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[P_GetAccommodationSummary] 

/* ===========================================================================
 ' NAME: P_GetAccommodationSummary
-- EXEC	 [dbo].[P_GetAccommodationSummary]
-- @PropertyId = 'A720040007'	
-- Author:		<Ahmed Mehmood>
-- Create date: <11/09/2013>
-- Description:	< Returns the property summary info >
-- Web Page: PropertyRecordSummary.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@PropertyId varchar (MAX) = ''
	)
	
AS
BEGIN
SET NOCOUNT ON;

SELECT  FLOORAREA,
		MAXPEOPLE,
		BEDROOMS
FROM	P_ATTRIBUTES A
		INNER JOIN P__PROPERTY P ON P.PROPERTYID= A.PROPERTYID
WHERE	P.PROPERTYID=@PropertyId
	
END












GO
