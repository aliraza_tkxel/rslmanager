USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetJobSheetSummaryByJSN]    Script Date: 11/28/2016 5:55:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.FL_GetJobSheetSummaryByJSN') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetJobSheetSummaryByJSN AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FL_GetJobSheetSummaryByJSN]
/* ===========================================================================
--	EXEC FL_GetJobSheetSummaryByJSN
		@JSN = 'JS3085'
--  Author:			Aamir Waheed
--  DATE CREATED:	14 March 2013
--  Description:	To Get Job Sheet Summary Detail By Job Sheet Number
--  Webpage:		View/Reports/ReportArea.aspx 
 '==============================================================================*/
	@JSN nvarchar(20) = NULL  
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT	TOP 1			
			FL_FAULT_LOG.FaultLogID AS FaultLogID,
			FL_FAULT_LOG.JobSheetNumber AS JSN,
			FL_FAULT_STATUS.Description AS FaultStatus,
			S_ORGANISATION.NAME AS Contractor,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1
				THEN CONVERT(varchar(5), FL_FAULT_PRIORITY.ResponseTime) + ' days' 
				ELSE CONVERT(varchar(5), FL_FAULT_PRIORITY.ResponseTime) + ' hours' END AS priority,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1
				THEN CONVERT(VARCHAR, DATEADD(DAY, FL_FAULT_PRIORITY.ResponseTime, FL_FAULT_LOG.SubmitDate), 106)
					 + ' ' + CONVERT(VARCHAR(5), DATEADD(DAY, FL_FAULT_PRIORITY.ResponseTime, GETDATE()), 108) 
				ELSE CONVERT(VARCHAR, DATEADD(Hour, FL_FAULT_PRIORITY.ResponseTime, FL_FAULT_LOG.SubmitDate), 106)
					 + ' ' + CONVERT(VARCHAR(5), DATEADD(Hour, FL_FAULT_PRIORITY.ResponseTime, GETDATE()), 108) END AS completionDue, 
			CONVERT(VARCHAR, FL_FAULT_LOG.SubmitDate, 106)
				 + ' ' + CONVERT(VARCHAR(5), FL_FAULT_LOG.SubmitDate, 108) AS orderDate,
			isnull(FL_AREA.AreaName,'') AS Location,
			FL_FAULT.Description AS Description,
			ISNULL(STUFF((
							SELECT
								', ' + [G_TRADE].[Description] 
							FROM
								FL_FAULT_TRADE
								JOIN G_TRADE
								ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
							WHERE
								FL_FAULT_TRADE.FaultId = FL_FAULT.FaultID
							FOR XML PATH('')
						)
						,1,1,''), 'No Trade Specified') As Trade,
			FL_FAULT_LOG.Notes AS Notes,
			REPLACE(ISNULL(C__CUSTOMER.FIRSTNAME, '') 
				+ ' ' + ISNULL(C__CUSTOMER.MIDDLENAME, '')
				+ ' ' + ISNULL(C__CUSTOMER.LASTNAME, ''), '  ', ' ') AS ClientName,	
				CASE WHEN 	FL_FAULT_LOG.PROPERTYID IS NULL THEN
					ISNULL(NULLIF(REPLACE(RTRIM(ISNULL(P_BLOCK.ADDRESS1, '') 
				+ ' ' + ISNULL(P_BLOCK.ADDRESS2, '') + 
				' ' + ISNULL(P_BLOCK.ADDRESS3,'')), '  ', ''),''),'N/A')
				ELSE
			ISNULL(NULLIF(REPLACE(RTRIM(ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') 
				+ ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + 
				' ' + ISNULL(P__PROPERTY.ADDRESS3,'')), '  ', ''),''),'N/A')
				END AS ClientStreetAddress,
				CASE WHEN 	FL_FAULT_LOG.PROPERTYID IS NULL THEN
				ISNULL(P_BLOCK.TOWNCITY,'N/A') 
				ELSE
				ISNULL(P__PROPERTY.TOWNCITY,'N/A') 
				END AS ClientCity,
			CASE WHEN 	FL_FAULT_LOG.PROPERTYID IS NULL THEN
				ISNULL(P_BLOCK.POSTCODE,'N/A') 
				ELSE
				ISNULL(P__PROPERTY.POSTCODE,'N/A') 
				END AS ClientPostCode,
			CASE WHEN 	FL_FAULT_LOG.PROPERTYID IS NULL THEN
				ISNULL(P_BLOCK.COUNTY,'N/A') 
				ELSE
				ISNULL(P__PROPERTY.COUNTY,'N/A') 
				END AS ClientRegion,
			ISNULL(C_ADDRESS.TEL,'N/A') AS ClientTel,
			ISNULL(C_ADDRESS.MOBILE, 'N/A') AS ClientMobile,
			ISNULL(C_ADDRESS.EMAIL, 'N/A') AS ClientEmail,
			C__CUSTOMER.CUSTOMERID AS CustomerId,
   			ISNULL(TblRepairNotes.Notes,' ') as RepairNotes,
   			 P__PROPERTY.propertyid As PropertyId         
	FROM    FL_FAULT_LOG
			INNER JOIN S_ORGANISATION ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID
			INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
			INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
			LEFT JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID
			LEFT JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID	
			INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
			LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
			LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
			left join P_SCHEME ON FL_FAULT_LOG.SchemeId = P_SCHEME.SCHEMEID
			left join P_BLOCK ON P_SCHEME.SchemeId = P_BLOCK.SCHEMEID
   LEFT JOIN (Select * from FL_FAULT_LOG_HISTORY Where FaultStatusID = 17 ) as TblRepairNotes on TblRepairNotes.FaultLogID = FL_FAULT_LOG.FaultLogID  

	WHERE	(
				FL_FAULT_LOG.JobSheetNumber = @JSN
				AND C_ADDRESS.ISDEFAULT = 1
				 )
 ORDER BY C_ADDRESS.ISDEFAULT,TblRepairNotes.LastActionDate DESC  

	Select	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
			P_Asbestos.RISKDESCRIPTION Description
				
	From	FL_FAULT_LOG 
			INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID = FL_FAULT_LOG.PROPERTYID
			INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
			INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID	  
		
	Where	FL_FAULT_LOG.JobSheetNumber = @JSN

END
