SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_CST_REQUEST_ENQUIRYLOG_GETCSTREQUEST]

/* ===========================================================================
 '   NAME:          TO_CST_REQUEST_ENQUIRYLOG_GETCSTREQUEST
 '   DATE CREATED:  1 JULY 2008
 '   CREATED BY:    Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display Enquiry Detail i.e. CLEAR ARREARS Part of C_Address fields 

are also selected
                     as subset as these need to be displayed on View enquiry pop up.  
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT 
		    	C_NATURE.DESCRIPTION as NatureDescription,
			TO_ENQUIRY_LOG.Description, 
          			C_ADDRESS.HOUSENUMBER, 
			C_ADDRESS.ADDRESS1, 
			C_ADDRESS.ADDRESS2, 
			C_ADDRESS.ADDRESS3, 
			C_ADDRESS.TOWNCITY, 
            C_ADDRESS.POSTCODE, 
            C_ADDRESS.COUNTY, 
            C_ADDRESS.TEL 
            
	FROM TO_ENQUIRY_LOG
	       INNER JOIN TO_CST_REQUEST CST ON  CST.EnquiryLogID=TO_ENQUIRY_LOG.EnquiryLogID
	       LEFT JOIN C_NATURE ON CST.RequestNatureId  = C_NATURE.ITEMNATUREID
	       LEFT JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID 
	       LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID
	       
 
	       WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)
	     AND (C_ADDRESS.ISDEFAULT = 1) 
	     

GO
