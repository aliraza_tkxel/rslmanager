SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CASHBOOK_DEPOSIT_PAGEBALANCE] (
@ACCOUNT INT,
@TOP INT,
@STARTDATE SMALLDATETIME,
@ENDDATE SMALLDATETIME
) 

AS

DECLARE @SQLSTRING VARCHAR(8000)

SET @SQLSTRING = '
SELECT ISNULL(SUM(DEBIT),0)-ISNULL(SUM(CREDIT),0) AS BALANCE FROM (
	SELECT TOP ' + CAST(@TOP AS VARCHAR) + ' CREDIT, DEBIT FROM (
		SELECT 	ISNULL(BR.GROUPINGID,-1) AS RECCED, 
			10 AS THETYPE, 
			B.BLTID AS THEID, 
			1 AS ITEMCOUNT, 
			CONVERT(VARCHAR,B.TRANSFERDATE, 103) AS BOOKDATE, 
			''Balance Transfer'' AS DESCRIPTION, 
			B.TRANSFERDATE AS TRANSACTIONDATE,
			CASE WHEN B.ACCOUNTTO = ' + CAST(@ACCOUNT AS VARCHAR) + ' THEN B.AMOUNT ELSE 0 END AS DEBIT,
			CASE WHEN B.ACCOUNTFRM = ' + CAST(@ACCOUNT AS VARCHAR) + ' THEN B.AMOUNT ELSE 0 END AS CREDIT 
		FROM 	NL_BALANCETRANSFER B
			LEFT JOIN F_BANK_RECONCILIATION_OTHERACCOUNTS BR ON BR.RECCODE = B.BLTID AND BR.RECTYPE = 4 AND BR.ACCOUNTID = ' + CAST(@ACCOUNT AS VARCHAR) + '
		WHERE	(B.ACCOUNTTO = ' + CAST(@ACCOUNT AS VARCHAR) + ' OR B.ACCOUNTFRM = ' + CAST(@ACCOUNT AS VARCHAR) + ')
		UNION ALL
		SELECT 	ISNULL(BR.GROUPINGID,-1) AS RECCED, 
			13 AS THETYPE, 
			H.ID AS THEID, 
			COUNT(*) AS ITEMCOUNT, 
			CONVERT(VARCHAR,H.MISDATE, 103) AS BOOKDATE, 
			''Misc. Income/Payment: MIS'' + RIGHT(''0000000'' + CAST(H.ID AS VARCHAR),7) AS DESCRIPTION, 
			H.MISDATE AS TRANSACTIONDATE,
			CASE WHEN SUM(NETAMOUNT) >= 0 THEN SUM(NETAMOUNT) ELSE 0 END AS DEBIT, 
			CASE WHEN SUM(NETAMOUNT) < 0 THEN -SUM(NETAMOUNT) ELSE 0 END AS CREDIT
		FROM 	NL_MISCELLANEOUS_HDR H
			INNER JOIN NL_MISCELLANEOUS_DTL D ON H.ID = D.HDRID
			LEFT JOIN F_BANK_RECONCILIATION_OTHERACCOUNTS BR ON BR.RECCODE = D.ID AND BR.RECTYPE = 6 AND BR.ACCOUNTID = ' + CAST(@ACCOUNT AS VARCHAR) + '	
		WHERE BANK = ' + CAST(@ACCOUNT AS VARCHAR) + '
		GROUP 	BY H.ID, H.MISDATE, BR.GROUPINGID
	) INNERVIEW
	WHERE TRANSACTIONDATE >= ''' + CAST(@STARTDATE AS VARCHAR)+ ''' AND TRANSACTIONDATE <= ''' + CAST(@ENDDATE AS VARCHAR) + '''
	ORDER BY TRANSACTIONDATE, THETYPE, THEID, RECCED
) OUTERVIEW
'

EXECUTE (@SQLSTRING)
GO
