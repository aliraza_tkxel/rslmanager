SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE PROCEDURE dbo.FL_GETSTAGELOOKUP
/* ===========================================================================
 '   NAME:          FL_GETSCHEMELOOKUP
 '   DATE CREATED:  27 Dec. 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get scheme FL_CO_APPOINTMENT_STAGE table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT AppointmentStageID AS id,StageName AS val
	FROM FL_CO_APPOINTMENT_STAGE	
	WHERE StageName NOT IN('To Be Arranged','Arranged','Cancelled')
	ORDER BY StageName ASC





GO
