SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetParametersByAreaId @itemId =1
-- Author:		<Salman Nazir>
-- Create date: <13/11/2012>
-- Description:	<Get the details of the Tree leaf node>
-- WebPage: PropertyRecord.aspx=>Attributes tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetParametersByAreaId] (
@areaId int,
@itemId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT ItemId,PA_PARAMETER.*
  FROM PA_ITEM_PARAMETER INNER JOIN
  PA_PARAMETER on PA_PARAMETER.ParameterID = PA_ITEM_PARAMETER.ParameterId 
  WHERE  PA_ITEM_PARAMETER.ItemId = @areaId 
END
GO
