USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePropertyDocumentInformation]    Script Date: 12/14/2015 18:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- ============================================= 
/* 
EXEC AS_SavePropertyDocumentInformation   
'A010000038' ,'RFC Compliance Documents Updates 1.82d73b5e.pdf',
'eqw','2609KB','.pdf', 24,60,
760,'01/01/2016','02/12/2015',-1,'1524',''

						--------------
							History
						--------------
						
Version:		Author:			Create date:	Description:			WebPage:  

v1.0			Aqib Javed 		19-Dec-2013		Save Property 			PropertyRecord.aspx	-- > Document Tab  		
												Document information
	
v1.1			Raja Aneeq		30/11/2015		Add four new fields. 
												uploaded by,document date,Expiry date,CP12Number
	
-- =============================================  


*/

IF OBJECT_ID('dbo.[AS_SavePropertyDocumentInformation]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_SavePropertyDocumentInformation] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_SavePropertyDocumentInformation]  
@propertyId varchar(50)=null,  
@documentCategory varchar(15) = null,
@documentName  varchar(150),  
@keyword varchar(255),  
@documentSize varchar(50),  
@documentFormat varchar(50),  
@documentTypeId int,  
@documentSubTypeId int , 
@uploadedBy int,
@expiryDate datetime,
@documentDate datetime,
@EpcRating int, 
@CP12Number nvarchar(20)='',
@cp12Doc as image ,
@schemeId int=null,
@blockId int = null,
@documentPath nvarchar(500)='' 

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

 IF @EpcRating > -1 
 BEGIN
 
	 INSERT INTO P_Documents  
	 (PropertyId,Category,DocumentName,Keywords,DocumentSize,DocumentFormat,DocumentTypeId,DocumentSubtypeId,SchemeId,BlockId,UploadedBy ,ExpiryDate ,DocumentDate,EpcRating,CP12Number,DocumentPath)  
	 VALUES  
	 (@propertyId,@documentCategory,@documentName ,@keyword,@documentSize,@documentFormat,@documentTypeId,@documentSubTypeId,@schemeId,@blockId,@uploadedBy,@expiryDate,@documentDate,@EpcRating,Null,@documentPath)
END
ELSE IF (@EpcRating <=-1) AND (@CP12Number ='' OR @CP12Number =Null)
BEGIN

INSERT INTO P_Documents  
	 (PropertyId,Category,DocumentName,Keywords,DocumentSize,DocumentFormat,DocumentTypeId,DocumentSubtypeId,SchemeId,BlockId,UploadedBy ,ExpiryDate ,DocumentDate,EpcRating,CP12Number,DocumentPath)  
	 VALUES  
	 (@propertyId,@documentCategory,@documentName ,@keyword,@documentSize,@documentFormat,@documentTypeId,@documentSubTypeId,@schemeId,@blockId,@uploadedBy,@expiryDate,@documentDate,Null,Null,@documentPath)  

END

ELSE IF  @CP12Number !='' OR @CP12Number !=Null
BEGIN

-- inserting values in p_documents table
 INSERT INTO P_Documents  
(PropertyId,Category,DocumentName,Keywords,DocumentSize,DocumentFormat,DocumentTypeId,DocumentSubtypeId,SchemeId,BlockId,UploadedBy ,ExpiryDate ,DocumentDate,EpcRating,CP12Number,DocumentPath)  
 VALUES  (@propertyId,@documentCategory,@documentName ,@keyword,@documentSize,@documentFormat,@documentTypeId,@documentSubTypeId,@schemeId,@blockId,@uploadedBy,@expiryDate,@documentDate,@EpcRating,@CP12Number,@documentPath)  
 
 -- updating dsts in P_LGSR table
 
 UPDATE P_LGSR SET  CP12UPLOADEDBY =@uploadedBy, DOCUMENTTYPE = @documentFormat, CP12NUMBER = @CP12Number,
 CP12DOCUMENT = @cp12Doc, ISSUEDATE = @documentDate, CP12Renewal =@expiryDate 
 WHERE PropertyId = @propertyId
 
 -- updating data in P_LGSR_HISTORY table
 
 UPDATE P_LGSR_HISTORY SET CP12DOCUMENT = @cp12Doc,  ISSUEDATE = @documentDate, CP12NUMBER = @CP12Number
  WHERE  LGSRHISTORYID=(SELECT MAX(LGSRHISTORYID) FROM P_LGSR_HISTORY WHERE PROPERTYID=@propertyId)


END
 




END
