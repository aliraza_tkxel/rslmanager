
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC	[dbo].[RD_GetEmergencyfaults]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'No Entries'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetEmergencyFaults]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
		
	DECLARE @StartDate DATETIME
	DECLARE @EndDate DATETIME
	
	SET @StartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ) )
	SET @EndDate = DATEADD(SS,-1,DATEADD(mm,12,@StartDate))

SELECT COUNT(*) 
FROM 
		(	SELECT FL_FAULT_LOG.FaultLogId FaultLogId,CONVERT(VARCHAR(10),FL_FAULT_LOG.SubmitDate, 120) Submitted
			FROM FL_FAULT_LOG  ) EmergencyFaults
		INNER JOIN FL_FAULT_LOG on EmergencyFaults.FaultLogId = FL_FAULT_LOG.FaultLogId
		LEFT OUTER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogId = FL_FAULT_APPOINTMENT.FaultLogId 
		INNER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN FL_FAULT ON  FL_FAULT_LOG.FaultID= FL_FAULT.FaultID 
		INNER JOIN FL_AREA ON FL_FAULT.AREAID = FL_AREA.AreaID
		INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
		WHERE FL_FAULT.PriorityID=3	AND (FL_FAULT_LOG.SubmitDate BETWEEN @StartDate AND @EndDate)	



END




GO
