SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

-- =============================================
-- Author:		Aamir Waheed
-- Create date: 14/05/2014
-- Description:	Get the VatRate and Vat Name as tenants of the given property.
-- =============================================

CREATE PROCEDURE [dbo].[PLANNED_GetVatDropDownValues]

AS
	SELECT VATID AS [id] -- To set as value field in dropdown and use in calculations
		,VATNAME AS [description] -- To show as text field in dropdown values.
		,CONVERT(DECIMAL,ISNULL(VATRATE,0)) AS [vatRate]
	FROM F_VAT
	ORDER BY VATID ASC
GO
