SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[FL_CANCELLED_FAULTS_ROWCOUNT] 

/* ===========================================================================
 '   NAME:           FL_CANCELLED_FAULTS_ROWCOUNT
 '   DATE CREATED:   10 APRIL 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To count cancelled fault based on search criteria provided
 '   IN:             @firstName, @lastName, @tenancyId, @status, @text,@date,
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	
	(
	    -- These Parameters are passed as Search Criteria
	        @firstName	varchar(50)	=  NULL,
		@lastName	varchar(50)	=  NULL,
		@tenancyID	float = NULL,
		@status		int = NULL,    
		@text		varchar(4000)    = NULL,
		@date		nvarchar(200)	=  NULL,
	
		
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @firstName IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'C__CUSTOMER.FIRSTNAME LIKE ''' + '%' +  @firstName + '%' + '''' +  '  AND'  
    
    
     IF @lastName IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'C__CUSTOMER.LASTNAME LIKE ''' + '%'  +@lastName + '%' + ''''  + ' AND'  
    
    
    IF @tenancyID IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_JOURNAL.TENANCYID = '+ LTRIM(STR(@tenancyID)) + ' AND'  
                               
    IF @status IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@status)) + ' AND'  
                             
    IF @text IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_FAULT.Description LIKE ''%'+ @text + '%'''+ ' AND'  
    
    IF @date IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
		 'CONVERT(varchar, FL_FAULT_LOG.SubmitDate, 103)  = ''' +  CONVERT(varchar,@date, 103) + '''  AND'  
		-- 'FL_FAULT_LOG.SubmitDate = ' + CONVERT(varchar, @date, 103)  + ' AND' 
           
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
       -- Begin building SELECT clause
           SET @SelectClause = 'SELECT' +                      
                       CHAR(10) + CHAR(9) + 'COUNT(*) As numOfRows ' 
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    
     SET @FromClause =CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG INNER JOIN C__CUSTOMER ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID ' +
		Char(10) + Char(9) + 'LEFT JOIN E__EMPLOYEE' + 
		Char(10) + Char(9) + 'ON FL_FAULT_LOG.UserId = E__EMPLOYEE.EMPLOYEEID' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_STATUS ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_JOURNAL ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FAULTLOGID ' +
                     -- CHAR(10) + CHAR(9) + 'INNER JOIN  ' +
                      --CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG_HISTORY.JOURNALID = FL_FAULT_JOURNAL.JOURNALID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C_NATURE ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_JOURNAL.ITEMNATUREID = C_NATURE.ITEMNATUREID ' +
                      CHAR(10) + CHAR(9) + 'LEFT JOIN S_ORGANISATION ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_ELEMENT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.ElementID = FL_ELEMENT.ElementID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PRIORITY ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_AREA ' +
                      CHAR(10) + CHAR(9) + 'ON FL_ELEMENT.AreaID = FL_AREA.AreaID '+
		      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PREINSPECTIONINFO'+
		      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FAULTLOGID=FL_FAULT_PREINSPECTIONINFO.FAULTLOGID'
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.FaultLogID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FL_FAULT_LOG.FaultLogID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE'+
	 -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        char(10) + CHAR(9) +'FL_FAULT_PREINSPECTIONINFO.APPROVED=0 AND '+
                        
                        CHAR(10) + CHAR(9) + ' 1=1 '
                        
    -- End building WHERE clause
    --========================================================================================
        
	

    
 EXEC (@SelectClause + @FromClause + @WhereClause )

  PRINT (@SelectClause + @FromClause + @WhereClause )

GO
