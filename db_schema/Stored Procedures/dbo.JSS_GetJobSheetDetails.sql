USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[JSS_GetJobSheetDetails]    Script Date: 12/26/2016 5:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.[JSS_GetJobSheetDetails]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[JSS_GetJobSheetDetails] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[JSS_GetJobSheetDetails]
 -- Add the parameters for the stored procedure here  
 (  
 @jobSheetNumber varchar(50)  
 )  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  

SET NOCOUNT ON;

-- =========================================
-- 1- Fault Log And Appointment Details
-- =========================================

SELECT DISTINCT
	FL.JobSheetNumber JSN,
	ISNULL(O.NAME, 'N/A') ContractorName,
	ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, '-') AS OperativeName,
	CASE
		WHEN FP.Days = 1 THEN CONVERT(varchar(5), FP.ResponseTime) + ' days'
		ELSE CONVERT(varchar(5), FP.ResponseTime) + ' hours'
	END AS [Priority],
	CONVERT(varchar, FL.DueDate, 106) + ' ' + CONVERT(varchar(5), FL.DueDate, 108) AS CompletionDue,
	CONVERT(varchar, FL.SubmitDate, 106) + ' ' + CONVERT(varchar(5), FL.SubmitDate, 108) AS ReportedDate,
	ISNULL(AREA.AreaName, 'N/A') Location,
	F.Description [Description],
	ISNULL(FL.Notes, 'N/A') FaultNotes,
	ISNULL(CONVERT(varchar(20), A.AppointmentDate, 106) + ' ' + A.Time + '-' + A.EndTime, '-') AS AppointmentTimeDate,
	ISNULL(A.Notes, 'N/A') AppointmentNotes,
	ISNULL(A.Time + 'N/A' + DATENAME(WEEKDAY, A.AppointmentDate), 'N/A') Time,
	ISNULL(T.[Description], 'N/A') AS Trade,
	ISNULL(FON.FollowOnNotes, ' ') AS FollowOnNotes,
	ISNULL(CONVERT(NVARCHAR(17),A.RepairCompletionDateTime,113),'-') [CompletionDateTime],
	FL.FollowOnFaultLogId AS FollowOnFaultLogId,
	FL.PROPERTYID AS [PROPERTYID]	

FROM
/* Fault LOG Detail */
FL_FAULT_LOG FL
/* Fault Detail */
INNER JOIN FL_FAULT F ON FL.FaultID = F.FaultID
LEFT OUTER JOIN FL_AREA AREA ON AREA.AreaID = FL.AREAID
LEFT OUTER JOIN FL_FAULT_TRADE FT ON FL.FaultTradeId = FT.FaultTradeId
LEFT OUTER JOIN FL_FAULT_PRIORITY FP ON F.PriorityID = FP.PriorityID
LEFT OUTER JOIN G_TRADE T ON T.TradeId = FT.TradeId
/* Appointment Detail */
LEFT OUTER JOIN FL_FAULT_APPOINTMENT FA ON FL.FaultLogID = FA.FaultLogId
LEFT OUTER JOIN FL_CO_APPOINTMENT A ON FA.AppointmentId = A.AppointmentID
/* Operative Detail(s) - Associated with Appointment */
LEFT OUTER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = A.OperativeID
/* Contractor Detail */
LEFT OUTER JOIN S_ORGANISATION O ON O.ORGID = FL.ORGID
/* Follow on Detail */
LEFT OUTER JOIN FL_FAULT_FOLLOWON FON ON FON.FaultLogID = FL.FaultLogId

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

-- =========================================
-- 2- Customer Info/Deatil
-- =========================================
SELECT
	ISNULL(C.FirstName, '') + ISNULL(' ' + C.MiddleName, '') + ISNULL(' ' + C.LastName, '') AS NAME,
	ISNULL(P.HouseNumber, '') + ISNULL(' ' + P.ADDRESS1, '') + ISNULL(' ' + P.ADDRESS2, '') + ISNULL(' ' + P.ADDRESS3, '') AS Address,
	ISNULL(P.TownCity, 'Town/City N/A') AS TOWNCITY,
	ISNULL(CONVERT(nvarchar(15), P.PostCode), 'Post Code N/A') AS POSTCODE,
	ISNULL(P.County, 'County N/A') AS COUNTY,
	ISNULL(CA.Tel, 'N/A') AS TEL,
	ISNULL(CA.MOBILE, 'N/A') AS MOBILE,
	ISNULL(CA.Email, 'N/A') AS EMAIL,
	FL.CustomerId customerID,
	FL.PROPERTYID propertyID

FROM
/* Fault LOG Detail(s) */
FL_FAULT_LOG FL
/* Property's Detail(s) */
LEFT OUTER JOIN P__PROPERTY P ON FL.PROPERTYID = P.PROPERTYID
/* Customer's Detail(s) */
LEFT OUTER JOIN C__CUSTOMER C ON FL.CustomerId = C.CUSTOMERID
/* Customer's Address (Current Address => condition IsDefault = 1 ) */
LEFT OUTER JOIN C_ADDRESS CA ON FL.CustomerId = CA.CUSTOMERID AND CA.IsDefault = 1

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

-- =========================================
-- 3- Repair List and Repair Notes
-- =========================================

SELECT	
	ISNULL(CONVERT(varchar, FL.CompletedDate, 106) + ' ' + CONVERT(varchar(5), FL.CompletedDate, 108), '-') AS CompletionDate,
	ISNULL(LTRIM(RTRIM(FRL.Description)),'') AS Description,
	ISNULL(FLTR.Notes,'') [RepairNotes]
FROM FL_FAULT_LOG FL
INNER JOIN FL_CO_FAULTLOG_TO_REPAIR FLTR
	ON FL.FaultLogId = FLTR.FaultLogId
LEFT JOIN FL_FAULT_REPAIR_LIST FRL
	ON FLTR.FaultRepairListID = FRL.FaultRepairListID

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

-- =========================================
-- 4- Job Sheet Activities
-- =========================================
SELECT
ISNULL(format(

	CASE 
		WHEN FLH.LastActionDate is null and FS.Description = 'Cancelled' THEN 
			FLC.RecordedOn
		ELSE
			 FLH.LastActionDate 
		END

,'d MMM yyyy HH:mm'),'NA') AS [Date],
	--Convert(nvarcher(10),FLH.LastActionDate,103) AS [Date],
	FS.Description AS [Activity],
	--****** Selection to be added for fetching the, along with the user id who cancelled and the Reason for cancellation.
	--FC.Notes AS [Reason],
	ISNULL(FLC.Notes, '-') AS [Reason],

	CASE 
		WHEN LEFT(E.FIRSTNAME, 1) + LEFT(E.LASTNAME, 1) IS NULL THEN 
			'N/A' 
			ELSE
			 LEFT(E.FIRSTNAME, 1) + LEFT(E.LASTNAME, 1) 
		END AS [ActivityUserInitials] 
--	ISNULL( , 'N/A' ) AS [ActivityUserInitials] -- Activity User can be Scheduler, Operative etc.

FROM

--***** The inner joins to be applied on the basis of FaultLogId in FAULT_CANCELLED TABLE, and FaultLogId in FAULT_LOG_HISTORY

/* Fault LOG Detail(s) */
FL_FAULT_LOG FL
/* Fault LOG History Detail(s) to get Activity/Status Change */
INNER JOIN FL_FAULT_LOG_HISTORY FLH
	ON FLH.FaultLogID = FL.FaultLogID
/* To get Status Name/Description for the status mentioned in fault log */
LEFT OUTER JOIN FL_FAULT_STATUS FS
	ON FS.FaultStatusID = FLH.FaultStatusID
/* Operative/Scheduler Detail(s) - Associated with Fault Log Activities */
LEFT OUTER JOIN E__EMPLOYEE E
	ON E.EMPLOYEEID = FLH.LastActionUserID
	/*TO GET REASON FOR THE CANCELLATION OF APPOINTMENT*/
	LEFT OUTER JOIN fl_fault_cancelled FLC on 
	FLC.FaultLogID = FLH.FaultLogID

WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

ORDER BY CASE 
		WHEN FLH.LastActionDate is null and FS.Description = 'Cancelled' THEN 
			FLC.RecordedOn
		ELSE
			 FLH.LastActionDate 
		END DESC

-- =========================================
-- 5- Asbestos Info/Detail(s)
-- =========================================
SELECT
	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
	P_Asbestos.RISKDESCRIPTION Description

FROM FL_FAULT_LOG FL
INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL
	ON FL.PROPERTYID = P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID
INNER JOIN P_PROPERTY_ASBESTOS_RISK
	ON P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
INNER JOIN P_ASBESTOS
	ON P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID

WHERE (FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber) and  (P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved is null or CONVERT(DATE,P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved) > convert(date, getdate()) )
AND P_ASBESTOS.other = 0

-- TODO: Remove After Testing with followon notes in Fault Log Query
-- =========================================
-- 6- Follow on Work Detail(s)
-- =========================================

SELECT
	FON.FollowOnNotes
FROM
/* Fault LOG Detail(s)*/
FL_FAULT_LOG FL
/* Follow on Work Detail(s) */
INNER JOIN FL_FAULT_FOLLOWON FON
	ON FL.FaultLogID = FON.FaultLogId
WHERE FL.JobSheetNumber = @jobSheetNumber OR CONVERT(NVARCHAR,FL.FaultLogID) = @jobSheetNumber

END