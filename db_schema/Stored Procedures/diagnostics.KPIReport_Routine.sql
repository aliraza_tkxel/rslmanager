SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [diagnostics].[KPIReport_Routine]

AS 

BEGIN 

DECLARE @Month INT = MONTH(DATEADD(m,-1,GETDATE()))
DECLARE @YearStartDate DATETIME
DECLARE @YearEndDate DATETIME

SELECT @YearStartDate = StartDate, @YearEndDate = EndDate FROM diagnostics.CurrentFiscalYear()

-- Routine
SELECT  DATENAME(month, CompletedDate) AS [Month] ,
        ISNULL(SupplierName, '') AS NAME ,
        OPERATIVE ,
        JobSheetNumber ,
        DEVELOPMENTNAME AS [Scheme] ,
        PROPERTYID AS [Property] ,
        CAST ([FaultDescription] AS VARCHAR(255)) AS [Repair/Fault] ,
        SUBMITDATE AS LoggedDate ,
        DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                        AppointmentDate)) AS AppointmentEndDateTime ,
       CAST( DATEDIFF(mi, SUBMITDATE,
                 DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                         DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                                 AppointmentDate))) - 40320 AS INT) AS [Logged vs AppointmentEndDate] ,
        AppointmentDate AS AppointmentDate ,
        StartTime ,
        EndTime ,
        CompletedDate AS CompletionDate ,
        CAST(DATEDIFF(Day, SUBMITDATE, CompletedDate) AS INT) AS [Days] ,
        PATCH ,
        STOCKTYPE
FROM    FL_FAULT_LIST_COMPLETED_DETAILS
WHERE   CompletedDate >= @YearStartDate
        AND CompletedDate <= @YearEndDate
        AND MONTH(CompletedDate) = @Month
        AND PriorityId IN ( 1 ) -- Routine
ORDER BY MONTH(CompletedDate)



END 
GO
