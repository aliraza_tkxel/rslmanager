SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_PRICINGCONTROL_GETDATA 
	/* ===========================================================================
 '   NAME:          FL_PRICINGCONTROL_GETDATA
 '   DATE CREATED:   21 OCT 2008
 '   CREATED BY:     Waseem Hassan		
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get data from FL_FAULT table which will be shown in textboxes (net,vatrate,vat,gross) in presentation pages
 '   IN:            @FaultID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@FaultID as Int
AS
	SELECT FR.FaultID,FR.NetCost,FR.Vat,FR.Gross,FR.VatRateID,FV.VATID,FV.VATRATE,FV.VATNAME
	FROM FL_Fault As FR
	INNER JOIN F_VAT As FV
	ON FR.VatRateID = FV.VATID
	WHERE FaultID=@FaultID
	














GO
