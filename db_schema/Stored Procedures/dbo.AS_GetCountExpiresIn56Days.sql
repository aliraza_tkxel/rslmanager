
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('dbo.AS_GetCountExpiresIn56Days') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCountExpiresIn56Days AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Exec [dbo].[AS_GetCountExpiresIn56Days]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- Author:		Hussain Ali
-- Create date: 31/10/2012
-- Description:	This stored procedure returns the count of all "Expires in 56 days in" properties
-- Usage: Dashboard
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetCountExpiresIn56Days]
	@PatchId		int,
	@DevelopmentId	int,
	@FuelType varchar (8000)
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
if @FuelType = 'Gas'
begin

	SET @selectClause	= 'SELECT 
								COUNT(P__PROPERTY.PROPERTYID) AS Number'
	
	SET @fromClause		= 'FROM 
								P__PROPERTY
								INNER JOIN AS_JOURNAL ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID
								LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND P__PROPERTY.FUELTYPE = 1
										AND AS_JOURNAL.IsCurrent = 1
										AND (AS_JOURNAL.STATUSID = 1 OR AS_JOURNAL.STATUSID = 2 OR AS_JOURNAL.STATUSID = 3 OR AS_JOURNAL.STATUSID = 4)
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
end	
--------------------------------------- for Oil ---------------------------------------------------------------
if @FuelType = 'Oil'
begin
SET @selectClause	= 'SELECT 
								COUNT(P__PROPERTY.PROPERTYID) AS Number'
	
	SET @fromClause		= 'FROM 
								P__PROPERTY
								INNER JOIN AS_JOURNAL ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID
								LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND P__PROPERTY.FUELTYPE = 1
										AND AS_JOURNAL.IsCurrent = 1
										AND (AS_JOURNAL.STATUSID = 1 OR AS_JOURNAL.STATUSID = 2 OR AS_JOURNAL.STATUSID = 3 OR AS_JOURNAL.STATUSID = 4)
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
end
--------------------------------------- for Alternative ---------------------------------------------------------------
if @FuelType = 'Alternative Servicing'
begin 
	SET @selectClause	= 'SELECT 
								COUNT(P__PROPERTY.PROPERTYID) AS Number'
	
	SET @fromClause		= 'FROM 
								P__PROPERTY
								INNER JOIN AS_JOURNAL ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID
								LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PA_Parameter_value.IsAlternativeHeating = 1
										AND AS_JOURNAL.IsCurrent = 1
										AND (AS_JOURNAL.STATUSID = 1 OR AS_JOURNAL.STATUSID = 2 OR AS_JOURNAL.STATUSID = 3 OR AS_JOURNAL.STATUSID = 4)
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
end
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
		
END


GO
