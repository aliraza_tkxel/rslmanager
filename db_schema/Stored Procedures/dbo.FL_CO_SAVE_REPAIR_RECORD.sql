SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[FL_CO_SAVE_REPAIR_RECORD]
	/*	===============================================================
	'   NAME:           FL_CO_SAVE_REPAIR_RECORD
	'   DATE CREATED:   02Jan,2009
	'   CREATED BY:     Waseem Hassan
	'   CREATED FOR:    RSL
	'   PURPOSE:        To update the repair 
	'   IN:             @repairDetail
	'   IN:             @net
	'   IN:             @vatType
	'   IN:             @vat
	'   IN:             @total
	'
	'   OUT:           @result
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@repairDetail NVARCHAR(1000),
		@net FLOAT,
		@vatType INT,	
		@vat FLOAT,	
		@total FLOAT,
		@result INT OUTPUT
	)
AS
	BEGIN
		
		SET NOCOUNT ON
		BEGIN TRAN
		
		INSERT INTO FL_FAULT_REPAIR_LIST 
			(DESCRIPTION, NETCOST, VAT,GROSS, VATRATEID) 
		VALUES(@repairDetail, round(@net,2), round(@vat,2),round(@total,2), @vatType) 
		
		-- If insertion fails, goto HANDLE_ERROR block
		IF @@ERROR <> 0 GOTO HANDLE_ERROR
		
		COMMIT TRAN	
		
		DECLARE @MFAULTID INT
		SELECT @MFAULTID = MAX(FaultRepairListID) FROM FL_FAULT_REPAIR_LIST
		SET @result=1
		RETURN
	
	END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @result=-1
RETURN













GO
