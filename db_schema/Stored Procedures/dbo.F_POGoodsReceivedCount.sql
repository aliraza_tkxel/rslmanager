USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_POGoodsReceivedCount]    Script Date: 4/11/2017 7:08:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[F_POGoodsReceivedCount]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[F_POGoodsReceivedCount] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[F_POGoodsReceivedCount]   
   
 @USERID int  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 DECLARE @POSTATUSID INT   
 DECLARE @WOSTATUSID INT   
 DECLARE @FINALPOSTATUSNAME NVARCHAR(100)   
 DECLARE @FINALWOSTATUSNAME NVARCHAR(100)   
Select @POSTATUSID = PS.POSTATUSID, @FINALPOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'   
Select @WOSTATUSID = PS.POSTATUSID, @FINALWOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'  

	SELECT COUNT (DISTINCT  PO.ORDERID ) GoodsReceived                                                                                                                                              
              FROM F_PURCHASEORDER PO  
               INNER JOIN (SELECT ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PIStatus IN (1,3) GROUP BY ORDERID) PI1 ON PI1.ORDERID = PO.ORDERID  
               CROSS APPLY (SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) PI  
               INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID   
               LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID  
               LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  
               LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID  
               WHERE PO.ACTIVE = 1   
               AND PO.USERID= @USERID and  po.POSTATUS not in (16)  
  
END  