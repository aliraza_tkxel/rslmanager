USE [RSLBHALive]
GO
 --Object  StoredProcedure [dbo].[E_UpdateOftecGasSafe]    Script Date 7262017 23238 PM 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.E_UpdateOftecGasSafe') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_UpdateOftecGasSafe AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_UpdateOftecGasSafe]
(
	@userId		INT,
	@empId		INT,
	@ofTec		INT,
	@GSRENo	varchar(20) = NULL,
	@gasSafe		INT
) AS

Begin
-- STEP 1
-- Update the record, so the correct timestampuserid goes to Audit table
UPDATE E_JOBDETAILS SET IsOftec = @ofTec, LASTACTIONTIME = GETDATE(), GSRENo = @GSRENo, IsGasSafe = @gasSafe, LASTACTIONUSER = @userId
WHERE EMPLOYEEID = @empId

END

