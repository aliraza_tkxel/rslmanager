
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC PLANNED_GetAllComponents
-- Author:		<Ahmed Mehmood>
-- Create date: <8/10/2013>
-- Description:	<Returns All Components>
-- Webpage:Dashboard.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetAllComponents] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN


SELECT	PLANNED_COMPONENT.COMPONENTID as ComponentId, PLANNED_COMPONENT.COMPONENTNAME as ComponentName				
FROM	PLANNED_COMPONENT
ORDER BY SORDER ASC

END
GO
