SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	[dbo].[AS_GetCP12DocumentByLGSRID]
--		@LGSRID = 15
-- Author:		<Aamir Waheed>
-- Create date: <April, 17 2013>
-- Description:	<This Stored Proceedure gets CP12Document against a LGSR ID>
-- Web Page: PropertyRecord.aspx.aspx
-- =============================================
IF OBJECT_ID('dbo.AS_GetCP12DocumentByLGSRID') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCP12DocumentByLGSRID AS SET NOCOUNT ON;') 
GO
 
ALTER PROCEDURE [dbo].[AS_GetCP12DocumentByLGSRID] 
	@LGSRID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here 

	SELECT PROPERTYID, BlockId, SchemeId, CP12NUMBER, DOCUMENTTYPE, CP12DOCUMENT 
	FROM P_LGSR
	WHERE LGSRID = @LGSRID
END
GO
