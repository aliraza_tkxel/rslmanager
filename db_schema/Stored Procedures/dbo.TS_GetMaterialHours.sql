SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Muhammad Abdul Wahhab
-- Create date: 30/07/2013
-- Description:	Get Material Hours for Time Sheet with in specific dates
--		DECLARE	@return_value int,
--		@MaterialHours float

--		EXEC @return_value = [dbo].[TS_GetMaterialHours]
--		@operativeID = 615,
--		@fromDate = N'01/01/2013',
--		@toDate = N'07/08/2013',		
--		@MaterialHours = @MaterialHours OUTPUT
--		SELECT	@MaterialHours as N'@MaterialHours'
-- =============================================
CREATE PROCEDURE [dbo].[TS_GetMaterialHours] 
	-- Add the parameters for the stored procedure here
	@operativeId int,
	@fromDate DateTime,
	@toDate DateTime,
	@MaterialHours float output 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    DECLARE @FaultLogId int
	DECLARE @originalstartdatetime datetime
	DECLARE @startdate datetime = null
	DECLARE @enddate datetime = null
	DECLARE @hours float = 0
	
	SET @MaterialHours = 0
	
	-- Setting all FaultLogIds against an operative in temp table	
    
	SELECT FL_FAULT_APPOINTMENT.FaultLogId AS FaultLogId
	INTO #TempFaultRecords
	FROM FL_FAULT_APPOINTMENT
	JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.APPOINTMENTID
	WHERE FL_CO_APPOINTMENT.OperativeID = @operativeId
	
	SELECT @FaultLogId = MIN(FaultLogId)
	FROM #TempFaultRecords

	WHILE @FaultLogId IS NOT NULL
	BEGIN
	-- Do something with the row data  
		
		-- Getting all job sheets against a fault in temp table
		SELECT *
		INTO #TempJobSheet
		FROM FL_FAULT_JOBTIMESHEET
		WHERE FL_FAULT_JOBTIMESHEET.FaultLogId = @FaultLogId AND FL_FAULT_JOBTIMESHEET.StartTime >= @fromDate
		AND FL_FAULT_JOBTIMESHEET.EndTime <= @toDate  
   
		--SELECT * FROM #TempJobSheet
		
		-- Fetching last job sheet against a fault
   
		SELECT @originalstartdatetime = MAX(StartTime)
		FROM #TempJobSheet
   
		WHILE @originalstartdatetime IS NOT NULL
		BEGIN
   
			SELECT @endDate = EndTime
			FROM #TempJobSheet
			WHERE StartTime = @originalstartdatetime      
       
			--PRINT 'start date: ' + convert(varchar(50),ISNULL(@startdate,'1/1/1970'))+ '  end date: ' + convert(varchar(50),ISNULL(@enddate,'1/1/1970'))+ '  faultlogid: ' + convert(varchar(50),@faultlogid)+ '  hours: ' + convert(varchar(50),@hours)      
			
			-- This checks if there is startdate from previous record and enddate from current record
			
			IF @enddate IS NOT NULL AND @startdate IS NOT NULL
			BEGIN
				SET @hours = DATEDIFF(MI,@enddate,@startdate)
				--PRINT 'start date1: ' + convert(varchar(50),@startdate)+ '  end date: ' + convert(varchar(50),@enddate)+ '  faultlogid: ' + convert(varchar(50),@faultlogid)+ '  hours: ' + convert(varchar(50),@hours)        
       
				IF @hours > 0
					SET @MaterialHours = @MaterialHours + @hours
			END
       
			SET @startdate = @originalstartdatetime       
   
			SELECT @originalstartdatetime = MAX(StartTime)
			FROM #TempJobSheet
			WHERE StartTime < @originalstartdatetime      
		END
		
		-- Setting start date to null for next collection of job sheets for new faultLogId
   
		SET @startdate = null
   
		drop table #TempJobSheet
		-- Get next row id
		SELECT @FaultLogId = MIN(FaultLogId)
		FROM #TempFaultRecords
		WHERE FaultLogId > @FaultLogId
	END

	drop table #TempFaultRecords

	--print CONVERT(nvarchar(20),@totalHours)

	SET @MaterialHours = ROUND(@MaterialHours/60,2)

	--print CONVERT(nvarchar(20),@totalHours)	

	RETURN @MaterialHours

END

GO
