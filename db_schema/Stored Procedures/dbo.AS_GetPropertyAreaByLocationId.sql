USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.AS_GetPropertyAreaByLocationId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetPropertyAreaByLocationId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetPropertyAreaByLocationId](
@locationId int
)
AS
BEGIN

	SELECT AreaID, AreaName
	FROM PA_AREA
	WHERE LocationId=@locationId AND IsActive=1

END