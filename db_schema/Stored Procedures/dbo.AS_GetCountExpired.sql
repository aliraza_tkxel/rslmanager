USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetCountExpired]    Script Date: 10/7/2016 11:09:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[AS_GetCountExpired]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetCountExpired] AS SET NOCOUNT ON;') 
GO
			
ALTER PROCEDURE [dbo].[AS_GetCountExpired]
	@PatchId		int,
	@DevelopmentId	int,
	@FuelType varchar (8000)
AS
BEGIN
		-- Declaring the variables to be used in this query
	DECLARE @selectClause	nvarchar(max),
			@fromClause		nvarchar(max),
			@whereClause	nvarchar(max),
			
			@fromClauseScheme	nvarchar(max),
			@whereClauseScheme	nvarchar(max),
			@fromClauseBlock	nvarchar(max),
			@whereClauseBlock	nvarchar(max),

			@query			nvarchar(max)	 
			
			       
			
	-- Initalizing the variables to be used in this query
if @FuelType = 'Gas'
begin
		SET @selectClause	= 'SELECT 
								COUNT(*) AS Number'
	SET @fromClause		= 'From (SELECT
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
	From 
	AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID
	INNER JOIN P__PROPERTY P__PROPERTY on P__PROPERTY.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_LGSR ON P_LGSR.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P__PROPERTY.ASSETTYPE
	'
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + '1=1 
						AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
						AND P__PROPERTY.STATUS NOT IN (9,5,6,7) 
						AND PV.ValueDetail=''Mains Gas''
						AND PHM.IsActive = 1
						AND AT.description not in (''Shared Ownership'')
						AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
						AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')'

	SET @fromClauseScheme = ' SELECT
								ISNULL(P.SCHEMENAME,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								FROM dbo.P_SCHEME P 
	INNER JOIN PA_HeatingMapping PHM ON P.SchemeID = PHM.SchemeID
	LEFT JOIN P_LGSR P_LGSR ON P_LGSR.HeatingMappingId=PHM.HeatingMappingId
	LEFT JOIN AS_JOURNAL J ON J.SchemeId=P.SCHEMEID AND J.ISCURRENT=1
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	'
	SET @whereClauseScheme = ' WHERE 1=1 
						AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
						AND PV.ValueDetail=''Mains Gas''
						AND PHM.IsActive = 1
						AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')'

							
	SET @fromClauseBlock = 'SELECT
								ISNULL(P.BlockName,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
								FROM dbo.P_BLOCK P 
	INNER JOIN PA_HeatingMapping PHM ON P.BlockId = PHM.BlockId
	LEFT JOIN P_LGSR P_LGSR ON P_LGSR.HeatingMappingId=PHM.HeatingMappingId
	LEFT JOIN P_SCHEME PD ON PD.SCHEMEID=P.SCHEMEID 
	LEFT JOIN AS_JOURNAL J ON J.BlockId=P.BlockId AND J.ISCURRENT=1
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	'

	SET @whereClauseBlock = ' WHERE 1=1 
						AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
						AND PV.ValueDetail=''Mains Gas''
						AND PHM.IsActive = 1
						AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')' 

end
--------------------------------------- for Oil ---------------------------------------------------------------
if @FuelType = 'Oil'
begin
	SET @selectClause	= 'SELECT 
								COUNT(*) AS Number'
	SET @fromClause		= 'From (SELECT 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
	From AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID
	INNER JOIN P__PROPERTY P__PROPERTY on P__PROPERTY.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_LGSR ON P_LGSR.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P__PROPERTY.ASSETTYPE
	'
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
		SET @whereClause = @whereClause + '1=1 
								AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
								AND P__PROPERTY.STATUS NOT IN (9,5,6,7) 
								AND PV.ValueDetail=''Oil''
								AND PHM.IsActive = 1
								AND AT.description not in (''Shared Ownership'')
								AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
								AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Oil'')
								'
end
--------------------------------------- for Alternative servicing ---------------------------------------------------------------
if @FuelType = 'Alternative Servicing'
begin
	SET @selectClause	= 'SELECT 
			COUNT(*) AS Number'
	SET @fromClause		= 'From (SELECT 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(ST.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								AS_APPOINTMENTS.APPOINTMENTDATE,
								PV.ValueDetail As FUEL
	From AS_JOURNAL J
	INNER JOIN AS_JournalHeatingMapping JM on JM.JournalId = J.JOURNALID

	INNER JOIN P__PROPERTY P__PROPERTY on P__PROPERTY.PROPERTYID = J.PROPERTYID
	INNER JOIN PA_HeatingMapping PHM ON JM.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN P_FINANCIAL F on F.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_LGSR ON P_LGSR.HeatingMappingId = PHM.HeatingMappingId
	INNER JOIN PA_PARAMETER_VALUE PV ON PHM.HeatingType = PV.ValueID
	LEFT JOIN dbo.AS_Status ST ON ST.StatusId=J.STATUSID 
	LEFT JOIN AS_APPOINTMENTS ON AS_APPOINTMENTS.JOURNALID = J.JOURNALID
	LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
	LEFT JOIN dbo.P_ASSETTYPE AT ON AT.ASSETTYPEID = P__PROPERTY.ASSETTYPE
	'
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
		SET @whereClause = @whereClause + '1=1 
								AND (DATEDIFF(D,GETDATE(),DATEADD(YY,1,P_LGSR.ISSUEDATE)) <= 0)
								AND P__PROPERTY.STATUS NOT IN (9,5,6,7) 
								AND AT.description not in (''Shared Ownership'')
								AND PT.DESCRIPTION not in (''Garage'', ''Car Space'', ''Car Port'')
								AND PHM.IsActive = 1
								AND J.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Alternative Servicing'')'
end

	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
		SET @whereClauseScheme = @whereClauseScheme + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
		SET @whereClauseBlock = @whereClauseBlock + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @unionQuery varchar(100)
	SET @unionQuery = char(10) + ' UNION ALL ' + CHAR(10)
	-- Building the Query	
	if @fuelType = 'Gas'
	Begin
		SET @whereClauseBlock = @whereClauseBlock + ') AS CertifictaeExpired'
		SET @query = @selectClause + @fromClause + @whereClause + @unionQuery +
					@fromClauseScheme + @whereClauseScheme + @unionQuery +
					@fromClauseBlock + @whereClauseBlock
	End
	Else
	Begin
		SET @whereClause = @whereClause + ') AS CertifictaeExpired'
		SET @query = @selectClause + @fromClause + @whereClause
	End
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
   
END
