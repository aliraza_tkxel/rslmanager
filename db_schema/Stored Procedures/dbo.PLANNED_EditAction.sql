
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	[PLANNED_EditAction]
		--@statusId  = 1,
		--@actionId = 5,
		--@title = N'New',
		--@ranking = 2,
		--@modifiedBy = 1,
		--@isSaved = 0
-- Author:		<Noor Muhammad>
-- Create date: <04/11/2013>
-- Description:	<Edit Action's value from table>
-- WebPage: Status.aspx => Edit Action
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_EditAction]
	(
	@statusId  int,
	@actionId int,
	@title varchar(500),
	@ranking int,
	@modifiedBy int,
	@isSaved bit out
	)
AS
BEGIN
Declare @checkOldRanking int
Declare @maxRanking int
Declare @actionTableId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
    --Select the action id with the same ranking as input parameter
    SELECT @actionTableId = PLANNED_Action.ActionId 
    FROM PLANNED_Action
    WHERE Ranking = @ranking AND StatusId = @statusId 
	
	--get old ranking of action id (which is in input parameter)        
    SELECT @checkOldRanking = PLANNED_Action.Ranking FROM PLANNED_Action WHERE ActionId = @actionId   
    SELECT @maxRanking = MAX(Ranking) FROM PLANNED_Action WHERE StatusId = @statusId 
    
    BEGIN TRANSACTION 
    BEGIN TRY 
		--If input actionid and action id (with same ranking and status) both are equal then
		if @actionTableId > 0 AND @actionTableId = @actionId
			BEGIN		
				--update the values
				Update PLANNED_Action SET Title = @title, Ranking = @ranking, ModifiedBy = @modifiedBy
				WHERE ActionId = @actionId AND StatusId = @statusId 
				
				--ineser the record in history
				INSERT INTO PLANNED_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				Values(@actionId,@statusId ,@title,@ranking,1,@modifiedBy,GETDATE(),GETDATE())		
			END
		ELSE
			BEGIN					
				--update the rank of action which should be updated,with same rank as input parameter
				UPDATE PLANNED_Action SET Ranking = @checkOldRanking WHERE ActionId = @actionTableId
				Declare @actionTitle varchar(500)
				Declare @actionStatusId int			
				Declare @isEditable smallint
				
				--insert the change in history
				SELECT @actionTitle=Title,@actionStatusId=StatusId,@isEditable=IsEditable FROM PLANNED_Action WHERE ActionId = @actionTableId
				INSERT INTO PLANNED_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				Values(@actionTableId,@actionStatusId,@actionTitle,@maxRanking+1,@isEditable,@modifiedBy,GETDATE(),GETDATE())
				
				--update the action which comes in input paramter													
				Update PLANNED_Action SET Title = @title, Ranking = @ranking, ModifiedBy = @modifiedBy
				WHERE ActionId = @actionId AND StatusId = @statusId 
				
				--insert the change in history
				INSERT INTO PLANNED_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				Values(@actionId,@statusId ,@title,@ranking,@isEditable,@modifiedBy,GETDATE(),GETDATE())									
			END
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END  
	EXEC PLANNED_AllActions				
END
GO
