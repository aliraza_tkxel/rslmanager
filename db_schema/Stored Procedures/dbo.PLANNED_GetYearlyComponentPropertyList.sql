USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
 /*Author:		<Author,Ahmed Mehmood>
		DECLARE	@return_value int,
		@totalCount int

		EXEC	[dbo].[PLANNED_GetYearlyComponentPropertyList]
		@componentId = 40,
		@operator = 0,
		@year = N'2013',
		@schemeId = -1,
		@searchText = N'C03',
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'Address',
		@sortOrder = N'desc',
		@isFullList = 1,
		@totalCount = @totalCount OUTPUT
*/
-- Create date: <Create Date,17/3/2014>
-- Description: Returns yearly component property list.
--Last modified Date:17/3/2014
-- =============================================

IF OBJECT_ID('dbo.[PLANNED_GetYearlyComponentPropertyList]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetYearlyComponentPropertyList] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetYearlyComponentPropertyList]
	-- Add the parameters for the stored procedure here
		@componentId int
		,@operator int
		,@year NVARCHAR(10)
		,@schemeId int
		,@isFullList bit
		,@dueDateDt as PLANNED_PROPERTY_REPLACEMENT_DUEDATES readonly
		,@editedComponentsDt as PLANNED_EDITED_COMPONENTS readonly

	-- Parameters which would help in sorting and paging
		,@pageSize int = 30
		,@pageNumber int = 1
		,@sortColumn NVARCHAR(500) = 'Address'
		,@sortOrder NVARCHAR (5) = 'DESC'

AS
BEGIN
	DECLARE 

		@SelectClause VARCHAR(MAX),
        @fromClause   VARCHAR(MAX),
        @whereClause  VARCHAR(MAX),	        
        @orderClause  VARCHAR(MAX),	
        @mainSelectQuery VARCHAR(MAX),        
        @rowNumberQuery VARCHAR(MAX),
        @finalQuery VARCHAR(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria VARCHAR(MAX),
		@propertyCyclicDueDateSearchCriteria VARCHAR(MAX),
		@propertyDueDateSelect VARCHAR(MAX),
		
        --variables for paging
        @offset int,
		@limit int
		,@dataLimitation NVARCHAR(10)

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided

		SET @searchCriteria = ' '	  

		set @propertyDueDateSelect = ' '

		-- YEAR OPERATOR 
		-- -1 => "PREVIOUS YEARS"
		-- 0 => "CURRENT YEAR"
		-- 1 => "UPCOMING YEARS"

		IF @operator = -1
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + '  YEAR(TMP_DUEDATE) < '	+ CONVERT(NVARCHAR(10), @year) 
				SET @propertyDueDateSelect = @propertyDueDateSelect + ' Year(TMP_DUEDATE) '
			END
		ELSE 
			BEGIN	 	
				SET @searchCriteria = @searchCriteria + CHAR(10) + '  YEAR(TMP_DUEDATE) <= ' + CONVERT(NVARCHAR(10), @year) + '
																											  AND (( ' + CONVERT(NVARCHAR(10), @year) + ' - YEAR(TMP_DUEDATE))
																											  %
																											   CASE
																												 WHEN TMP_FREQUENCY = ''mnths'' THEN CEILING(CAST(TMP_CYCLE AS decimal) / 12)
																												ELSE TMP_CYCLE
																											   END = 0) '	
	 		SET @propertyDueDateSelect = @propertyDueDateSelect + '''' + CONVERT(NVARCHAR(MAX), @year) + ''''
	 		
	 		END
				


		-- End building SearchCriteria clause   
		--========================================================================================


		--=========================================================
		-- CONVERT VARIABLE TABLES TO #TEMP TABLES FOR MANIPULATION
		--=========================================================

		CREATE TABLE 
		#TMP_EDITED_COMPONENTS
		(TMP_COMPONENTID smallint
		,TMP_COMPONENTNAME NVARCHAR(100)
		,TMP_CYCLE INT
		,TMP_FREQUENCY NVARCHAR(10)
		,TMP_MATERIALCOST FLOAT
		,TMP_LABOURCOST FLOAT
		,TMP_COSTFLAG bit
		,TMP_CYCLEFLAG bit)

		INSERT	INTO #TMP_EDITED_COMPONENTS (TMP_COMPONENTID, TMP_COMPONENTNAME,TMP_CYCLE,TMP_FREQUENCY,TMP_MATERIALCOST,TMP_LABOURCOST,TMP_COSTFLAG,TMP_CYCLEFLAG) 
		SELECT	COMPONENTID, COMPONENTNAME, CYCLE,FREQUENCY,MATERIALCOST,LABOURCOST,COSTFLAG,CYCLEFLAG
		FROM	@editedComponentsDt 

		CREATE TABLE 
		#TMP_UPDATED_PROPERTY_DUES
		( TMP_SID int
		, TMP_DUEDATE VARCHAR(10) )

		INSERT	INTO #TMP_UPDATED_PROPERTY_DUES (TMP_SID, TMP_DUEDATE) 
		SELECT	SID, DUEDATE 
		FROM	@dueDateDt 
		
		CREATE TABLE
		#TMP_PROPERTY_COMPONENT_DUEDATE (
		  TMP_SID INT,
		  TMP_PROPERTYID NVARCHAR(40),
		  TMP_PLANNED_COMPONENTID SMALLINT,
		  TMP_DUEDATE DATETIME,
		  TMP_COST FLOAT,
		  TMP_CYCLE INT,
		  TMP_FREQUENCY NVARCHAR(20),
		  
		  TMP_SCHEME NVARCHAR(500),
		  TMP_ADDRESS NVARCHAR(200),
		  TMP_POSTCODE NVARCHAR(10),
		  TMP_COMPONENT NVARCHAR(100),
		  TMP_LASTDONE DATETIME,
		  HouseNumber NVARCHAR(50),
		  TMP_PRIMARYADDRESS NVARCHAR(100)
		)
		
		
;WITH ATTR_PARAM_CTE (PROPERTYID, ITEMID,PARAMETERID, VALUEID)
AS

(
    SELECT
        PPA_PARAM.PROPERTYID,
        PIP_PARAM.ITEMID,
        PIP_PARAM.PARAMETERID,
        PPA_PARAM.VALUEID
	FROM	PA_PROPERTY_ATTRIBUTES PPA_PARAM
          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
)		
INSERT INTO #TMP_PROPERTY_COMPONENT_DUEDATE (TMP_SID, TMP_PROPERTYID,TMP_PLANNED_COMPONENTID, TMP_DUEDATE, TMP_COST, TMP_CYCLE, TMP_FREQUENCY, TMP_SCHEME,TMP_ADDRESS ,TMP_POSTCODE,TMP_COMPONENT, TMP_LASTDONE,HouseNumber ,TMP_PRIMARYADDRESS )
SELECT 
  PA_PROPERTY_ITEM_DATES.SID,
  PA_PROPERTY_ITEM_DATES.PROPERTYID,
  PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID, 
  CASE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL AND
      #TMP_UPDATED_PROPERTY_DUES.TMP_SID IS NOT NULL THEN #TMP_UPDATED_PROPERTY_DUES.TMP_DUEDATE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL THEN CASE
        WHEN #TMP_EDITED_COMPONENTS.TMP_CYCLEFLAG = 1 THEN CASE
            WHEN #TMP_EDITED_COMPONENTS.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, #TMP_EDITED_COMPONENTS.TMP_CYCLE, PA_PROPERTY_ITEM_DATES.LastDone)
            ELSE DATEADD(MM, #TMP_EDITED_COMPONENTS.TMP_CYCLE, PA_PROPERTY_ITEM_DATES.LastDone)
          END
        ELSE PA_PROPERTY_ITEM_DATES.DueDate
      END
    WHEN #TMP_UPDATED_PROPERTY_DUES.TMP_SID IS NOT NULL THEN #TMP_UPDATED_PROPERTY_DUES.TMP_DUEDATE
    ELSE PA_PROPERTY_ITEM_DATES.DueDate
  END AS DueDate,
  CASE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL THEN #TMP_EDITED_COMPONENTS.TMP_LABOURCOST + #TMP_EDITED_COMPONENTS.TMP_MATERIALCOST
    ELSE PC.LABOURCOST + PC.MATERIALCOST
  END AS Cost
  ,CASE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL THEN 
		#TMP_EDITED_COMPONENTS.TMP_CYCLE
    ELSE 
		PC.CYCLE
   END AS CYCLE
  ,CASE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL THEN 
		#TMP_EDITED_COMPONENTS.TMP_FREQUENCY
    ELSE 
		PC.FREQUENCY
   END AS FREQUENCY
   ,ISNULL(SCHEMENAME,'') AS SCHEME
   ,ISNULL(P__PROPERTY.HouseNumber, '') + ISNULL(' ' + P__PROPERTY.FLATNUMBER, '') + ISNULL(' ' + P__PROPERTY.ADDRESS1, '') + ISNULL(' ' + P__PROPERTY.ADDRESS2, '') + ISNULL(' ' + P__PROPERTY.ADDRESS3, '') AS Address
   ,ISNULL(POSTCODE,'') AS POSTCODE   
   ,ISNULL(PC.COMPONENTNAME,'') AS COMPONENTNAME
   , LASTDONE
   ,COALESCE(HOUSENUMBER, FLATNUMBER) AS HouseNumber
   ,ADDRESS1 
FROM PA_PROPERTY_ITEM_DATES
	INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PCI.COMPONENTID
	INNER JOIN PLANNED_COMPONENT PC ON PC.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID     
	INNER JOIN P__PROPERTY ON PA_PROPERTY_ITEM_DATES.PROPERTYID = P__PROPERTY.PROPERTYID
    INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
    INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.StatusID
    LEFT JOIN #TMP_EDITED_COMPONENTS ON #TMP_EDITED_COMPONENTS.TMP_COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES ON PA_PROPERTY_ITEM_DATES.SID = #TMP_UPDATED_PROPERTY_DUES.TMP_SID

    LEFT JOIN (SELECT DISTINCT
        PPA_NUM.PROPERTYID,
        PIP_NUM.ItemId,
        PPA_NUM.PARAMETERVALUE
      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
        AND PA_PROPERTY_ITEM_DATES.PROPERTYID = PNP.PROPERTYID

    LEFT JOIN ATTR_PARAM_CTE P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
        AND P_PARAM.ParameterId = PCI.PARAMETERID
        AND PA_PROPERTY_ITEM_DATES.PROPERTYID = P_PARAM.PROPERTYID

    LEFT JOIN ATTR_PARAM_CTE P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
        AND P_SUBPARAM.ParameterId = PCI.SubParameter
        AND PA_PROPERTY_ITEM_DATES.PROPERTYID = P_SUBPARAM.PROPERTYID


WHERE 
   (@schemeId = -1 OR P_SCHEME.SCHEMEID = @schemeId)
  AND PC.COMPONENTID  = @componentId
  AND
 P_STATUS.[DESCRIPTION] NOT IN ('to be Demolished',
  'sold',
  'demolished')
  AND CASE
    WHEN PC.COMPONENTNAME LIKE '%windows%' THEN CASE
        WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR
          PNP.PARAMETERVALUE IS NULL THEN 1
        ELSE 0
      END
    WHEN PCI.SUBVALUE IS NOT NULL THEN CASE
        WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND
          P_PARAM.VALUEID = PCI.VALUEID THEN 1
        ELSE 0
      END
    WHEN PCI.VALUEID IS NOT NULL AND
      PCI.SUBVALUE IS NULL THEN CASE
        WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 1
        ELSE 0
      END
    ELSE 1    
  END = 1
  AND DATEPART(YEAR, CASE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL AND
      #TMP_UPDATED_PROPERTY_DUES.TMP_SID IS NOT NULL THEN #TMP_UPDATED_PROPERTY_DUES.TMP_DUEDATE
    WHEN #TMP_EDITED_COMPONENTS.TMP_COMPONENTID IS NOT NULL THEN CASE
        WHEN #TMP_EDITED_COMPONENTS.TMP_CYCLEFLAG = 1 THEN CASE
            WHEN #TMP_EDITED_COMPONENTS.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, #TMP_EDITED_COMPONENTS.TMP_CYCLE, PA_PROPERTY_ITEM_DATES.LastDone)
            ELSE DATEADD(MM, #TMP_EDITED_COMPONENTS.TMP_CYCLE, PA_PROPERTY_ITEM_DATES.LastDone)
          END
        ELSE PA_PROPERTY_ITEM_DATES.DueDate
      END
    WHEN #TMP_UPDATED_PROPERTY_DUES.TMP_SID IS NOT NULL THEN #TMP_UPDATED_PROPERTY_DUES.TMP_DUEDATE
    ELSE PA_PROPERTY_ITEM_DATES.DueDate
  END) <= CONVERT(INT, @year) 

		--=========================================================


		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here

		IF (@isFullList = 1) SET @dataLimitation = '' ELSE SET @dataLimitation = 'top (' + CONVERT(NVARCHAR(10), @limit) + ')'


		SET @selectClause = N' SELECT ' + @dataLimitation + '
		
		  PID.TMP_PROPERTYID AS Ref 
		,ISNULL(PID.TMP_SCHEME,'''') AS Scheme	
		,ISNULL(PID.TMP_ADDRESS,'''') AS Address
		,ISNULL(PID.TMP_POSTCODE,'''') AS Postcode
		,ISNULL(PID.TMP_COMPONENT,'''') AS Component
		,ISNULL(Year(PID.TMP_LASTDONE),'''') AS Replaced
		,ISNULL('+@propertyDueDateSelect+','''') AS Due   
		,ISNULL(dbo.PLANNED_fnDecimalToCurrency(PID.TMP_COST),'''') AS Cost 

		-- For Sorting
		,ISNULL(PID.TMP_SID,'''') as DueSID
		,ISNULL(PID.HouseNumber,'''') as HouseNumber
		,ISNULL(PID.TMP_PRIMARYADDRESS,'''') as PrimaryAddress
		
		'
		--PRINT '======================================================================='
		--PRINT  'SELECT'
		--PRINT '======================================================================='
		--PRINT @selectClause


		-- End building SELECT clause
		--======================================================================================== 							


		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause = N''
		SET @fromClause = @fromClause +	N' FROM	#TMP_PROPERTY_COMPONENT_DUEDATE PID	'
		
		--PRINT '======================================================================='
		--PRINT  'FROM'
		--PRINT '======================================================================='
		--PRINT @fromClause
	
		
		-- End building From clause
		--======================================================================================== 														  


		--========================================================================================    
		-- Begin building OrderBy clause		

		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int) ' 				
		END


		SET @orderClause =  ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		-- End building OrderBy clause
		--========================================================================================								

		--========================================================================================
		-- Begin building WHERE clause

		-- This Where clause contains subquery to exclude already displayed records			  

		SET @whereClause =	' WHERE' + CHAR(10) + @searchCriteria 

		--PRINT '======================================================================='
		--PRINT  'WHERE'
		--PRINT '======================================================================='
		--PRINT @whereClause

		-- End building WHERE clause
		--========================================================================================

		--========================================================================================
		-- Begin building the main select Query

		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		PRINT '======================================================================='
		PRINT  'MAIN SELECT QUERY'
		PRINT '======================================================================='
		PRINT @mainSelectQuery

		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query

		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
								
		--PRINT '======================================================================='
		--PRINT  'ROW NUMBER QUERY'
		--PRINT '======================================================================='
		--PRINT @rowNumberQuery

		-- End building the row number query
		--========================================================================================

		--========================================================================================
		-- Begin building the final query 

		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(nvarchar(10), @offset) + CHAR(10)+ ' and ' + CHAR(10)+ convert(varchar(10),@limit)	
							
							
		PRINT '======================================================================='
		PRINT  'FINAL QUERY'
		PRINT '======================================================================='
		PRINT @finalQuery			

		-- End building the final query
		--========================================================================================									

		--========================================================================================
		-- Begin - Execute the Query 
		--print(@finalQuery)
		IF (@isFullList = 1) 
			BEGIN
				EXEC (@mainSelectQuery)
			END 
		ELSE 
			BEGIN
				EXEC (@finalQuery)
			END
																											
		-- End - Execute the Query 
		--========================================================================================									

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectTotalAmount NVARCHAR(MAX)
		SET @selectTotalAmount = 'SELECT dbo.PLANNED_fnDecimalToCurrency(SUM(PID.TMP_COST)) as TotalCost ' + @fromClause + @whereClause

		--print(@selectTotalAmount)
		EXEC (@selectTotalAmount)


		Declare @selectCount NVARCHAR(MAX), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT count(*) as TotalCount ' + @fromClause + @whereClause

		--print(@selectCount)
		EXEC (@selectCount)

		DROP TABLE #TMP_EDITED_COMPONENTS
		DROP TABLE #TMP_UPDATED_PROPERTY_DUES

		-- End building the Count Query
		--========================================================================================	


END

