USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_SP_GetPOForNotifyUpperLimitOperative]    Script Date: 7/19/2017 11:57:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.E_PayPointEmailNotification') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_PayPointEmailNotification AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_PayPointEmailNotification]
AS
BEGIN

	

DECLARE @EMPLOYEEID  INT = 0,
	@EMPLOYEENAME  nvarchar(100) = '',
	@PAYPOINTREVIEWDATE  smalldatetime,
	@LINEMANAGERID  INT = 0,
	@LINEMANAGERNAME  nvarchar(100) = '',
	@LINEMANAGEREMAIL nvarchar (100) = '',
	@DIRECTORID  INT = 0,
	@DIRECTORNAME  nvarchar(100) = '',
	@FISCALYEARID int = 0,
	@PAYPOINTSTATUS int = 0

	select top 1 @FISCALYEARID = YRange from F_FISCALYEARS order by YRange desc
	select @PAYPOINTSTATUS = PayPointStatusId from E_PayPointStatus where Description = 'Waiting Submission'

	DECLARE Notification_CURSOR 
	CURSOR FOR 
	select  E.EMPLOYEEID as EMPLOYEEID, E.FIRSTNAME +' '+ e.LASTNAME as EMPLOYEENAME, CONVERT(date, jd.PAYPOINTREVIEWDATE) as PAYPOINTREVIEWDATE,
			LM.EMPLOYEEID as LINEMANAGERID, LM.FIRSTNAME +' '+ LM.LASTNAME as LINEMANAGERNAME, LMC.WORKEMAIL as LINEMANAGEREMAIL,
			D.EMPLOYEEID as DIRECTORID, D.FIRSTNAME +' '+ D.LASTNAME as DIRECTORNAME
	from E__EMPLOYEE E
		INNER JOIN E_JOBDETAILS jd on E.EMPLOYEEID = jd.EMPLOYEEID
		inner join E__EMPLOYEE LM on jd.LINEMANAGER = LM.EMPLOYEEID
		Inner Join E_CONTACT LMC on LM.EMPLOYEEID = LMC.EMPLOYEEID
		inner join E_JOBROLETEAM JRT on e.JobRoleTeamId = JRT.JobRoleTeamId
		Inner Join E_Team T on JRT.TeamId = T.TeamId
		inner join E__EMPLOYEE D on T.DIRECTOR = D.EMPLOYEEID
	where jd.ACTIVE=1 and CONVERT(date, jd.PAYPOINTREVIEWDATE) = DateAdd(month, 1, CONVERT(date, GETDATE()))
	
	OPEN Notification_CURSOR

	FETCH NEXT FROM Notification_CURSOR	
	into @EMPLOYEEID, @EMPLOYEENAME, @PAYPOINTREVIEWDATE, @LINEMANAGERID, @LINEMANAGERNAME, @LINEMANAGEREMAIL, @DIRECTORID, @DIRECTORNAME

	WHILE @@FETCH_STATUS = 0 
    BEGIN 
			
		declare @emailBody nvarchar(max) = ''
		-- compose an email 
	 	
		set @emailBody = '<!DOCTYPE html><html><head><title>Queued Purchase Order</title></head> ' +
						 '<body> ' +
                         '				<style type="text/css"> ' +
                         '                   .topAllignedCell{vertical-align: top;} ' +
                         '                  .bottomAllignedCell{vertical-align: bottom;} ' +
                         '               </style> ' +
						 '				 <font size="3" face="arial"> '+
                         '			     <span>Dear '+ @LINEMANAGERNAME+'</span> ' +
                         '               <p>'+ @EMPLOYEENAME+' is eligible for a Pay Point Review on the '+cast( ISNULL(CONVERT(VARCHAR(10), @PAYPOINTREVIEWDATE, 103),0) as NVARCHAR)+'. If you wish to submit an application to '+ @DIRECTORNAME+' please go to the employee record for '+ @EMPLOYEENAME+' and select the Pay Point in the Personal Details. A new screen will appear and you can then complete the required information.</p> ' +
                         '               <p>The application will be submitted to '+ @DIRECTORNAME+' and if they approve the application it will be considered by the Exec team. </p> ' +
						 '				 <p>Further notifications will be issued on the progress of the submission.  </p> ' +			
						 '				 <p>Kind regards ' +
						 '					<br /> ' +
						 '					HR Team ' +
						 '				 </p> ' +
					
						 '<table><tbody><tr><td><img src="cid:broadlandImage" alt="Broadland Housing Group" /></td> ' +
                         '                           <td class="bottomAllignedCell"><font size="3" face="arial"> ' +
                         '                               Broadland Housing Group<br /> ' +
                         '                               NCFC, The Jarrold Stand<br /> ' +
                         '                               Carrow Road, Norwich, NR1 1HU<br /> ' +
                         '                           </font></td> ' +
                         '                       </tr></tbody></table></font></body></html>'


		 --send email 

		EXEC msdb.dbo.sp_send_dbmail  

		@profile_name = 'EMailProfilePO',  
		@recipients = @LINEMANAGEREMAIL,  
		@body = @emailBody,  
		@subject = 'Pay Point Application',
		@body_format = 'HTML' ;  

		insert into E_PaypointSubmission (employeeId, fiscalYearId, paypointStatusId) values (@EMPLOYEEID, @FISCALYEARID, @PAYPOINTSTATUS) 

		FETCH NEXT FROM Notification_CURSOR INTO @EMPLOYEEID, @EMPLOYEENAME, @PAYPOINTREVIEWDATE, @LINEMANAGERID, @LINEMANAGERNAME, @LINEMANAGEREMAIL, @DIRECTORID, @DIRECTORNAME

			 
    END 
CLOSE Notification_CURSOR  
 DEALLOCATE Notification_CURSOR 

END	

	
