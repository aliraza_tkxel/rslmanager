SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE PROCEDURE dbo.TO_ABANDONED_AddAbandonRequest
/* ===========================================================================
 '   NAME:           TO_ABANDONED_AddAbandonRequest
 '   DATE CREATED:   04 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add abandoned Property/Vehicle record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 abandon record in TO_ABANDONED table
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '   IN:             @Location
 '   IN:             @LookUpValueID
 '
 '   OUT:            @AbandonedId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(4000),	
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	@Location NVARCHAR(4000),
	@LookUpValueID INT,
	@AbandonedId INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,	
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_ABANDONED(
		Location,
		EnquiryLogID,
		LookUpValueID
		)
		
		VALUES(
		@Location,
		@EnquiryLogID,
		@LookUpValueID
		)
		
		SELECT @AbandonedId=AbandonedId
	FROM TO_ABANDONED
	WHERE AbandonedId = @@IDENTITY

				IF @AbandonedId<0
					BEGIN
						SET @AbandonedId=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @AbandonedId=-1
			ROLLBACK TRAN
			END
	
	
	
	
	
	
	
	
	
	




GO
