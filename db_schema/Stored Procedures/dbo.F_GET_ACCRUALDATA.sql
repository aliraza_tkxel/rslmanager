SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE       PROC [dbo].[F_GET_ACCRUALDATA]   
 (@ACCRUALID AS INT)  
AS  
 BEGIN  
  SELECT     NL_ACCRUALS.ACCRUALID, NL_ACCRUALS.NOMINALACC, NL_ACCRUALS.PREPAYACC, NL_ACCRUALS.[VALUE],   
  NL_ACCRUALS.NO_MONTHS, NL_ACCRUALS.MONTH_AMT, NL_ACCRUALS.DETAILS, CONVERT(varchar(12), NL_ACCRUALS.STARTDATE,   
  103) AS STARTDATE, NL_ACCOUNT.[DESC] as NLDESC, isnull(NL_ACCRUALS.EXPENDITUREID,0) as EXPENDITUREID,isNull(NL_ACCRUALS.TRANSACTIONID,0) as TRANSACTIONID  
  FROM         NL_ACCRUALS INNER JOIN  
  NL_ACCOUNT ON NL_ACCRUALS.NOMINALACC = NL_ACCOUNT.ACCOUNTID  
  WHERE     (NL_ACCRUALS.ACCRUALID = @ACCRUALID)  
 END 
GO
