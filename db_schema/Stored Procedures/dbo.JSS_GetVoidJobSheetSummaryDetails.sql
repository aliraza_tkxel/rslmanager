USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[JSS_GetVoidJobSheetSummaryDetails]    Script Date: 02/01/2017 17:47:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[JSS_GetVoidJobSheetSummaryDetails]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[JSS_GetVoidJobSheetSummaryDetails] AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[JSS_GetVoidJobSheetSummaryDetails]
	-- Add the parameters for the stored procedure here
	@JobSheetNumber nvarchar(20),@appointmentType nvarchar(20)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
DECLARE @propertyId VARCHAR(50),@journalId int, @customerId int , @tenancyId int
-- Insert statements for procedure here
--=============================================================
-- Appointment Info Details
--============================================================
IF (@appointmentType = 'Void Works')
BEGIN

	Select @journalId=RequiredWorksId,@propertyId=PropertyId, @customerId=CustomerId,@tenancyId=TenancyId from PDR_JOURNAL	
	INNER JOIN V_RequiredWorks ON PDR_JOURNAL.JOURNALID = V_RequiredWorks.WorksJournalId
	INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
	Where ('JSV' + CONVERT(NVARCHAR, V_RequiredWorks.RequiredWorksId)) = @JobSheetNumber
	PRINT(@journalId);PRINT(@propertyId);
END
ELSE	
BEGIN 
	PRINT('Other then Void');
		Select @journalId=JOURNALID,@propertyId=PropertyId, @customerId=CustomerId,@tenancyId=TenancyId 
		from PDR_JOURNAL	
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
		Where ('JSV' + CONVERT(NVARCHAR, PDR_JOURNAL.JournalID)) = @JobSheetNumber
	END


SELECT
	CASE
		WHEN V_RequiredWorks.RequiredWorksId IS NULL THEN ISNULL(('JSV' + CONVERT(NVARCHAR, PDR_JOURNAL.JournalID)), 'N/A')
		ELSE ISNULL(('JSV' + CONVERT(NVARCHAR, V_RequiredWorks.RequiredWorksId)), 'N/A')
	END
	AS JobsheetNumber,
	PDR_JOURNAL.JOURNALID,
	CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) AS StartDate,
	CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103) AS EndDate,
	ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, ' ') AS Operative,
	PDR_APPOINTMENTS.APPOINTMENTSTARTTIME AS [StartTime],
	PDR_APPOINTMENTS.APPOINTMENTENDTIME AS EndTime,

	Case when @appointmentType = 'Void Works' then
		ISNULL(RequiredWorksStatus.Title, 'N/A')
		Else 
		COALESCE(PDR_APPOINTMENTS.AppointmentStatus, PDR_STATUS.TITLE,'N/A')
	End AS Status,

	PDR_APPOINTMENTS.APPOINTMENTID AS AppointmentID,

	V_RequiredWorks.WorkDescription,
	PDR_MSATType.MSATTypeName AS AppointmentType,
	PDR_APPOINTMENTS.DURATION,
	ISNULL(PDR_APPOINTMENTS.APPOINTMENTNOTES, '') AS JobsheetNotes,
	ISNULL(PDR_APPOINTMENTS.CUSTOMERNOTES, '') AS CustomerNotes,
	PDR_MSAT.PropertyId,
	FL_Area.AreaName as Location
FROM PDR_APPOINTMENTS
INNER JOIN E__EMPLOYEE AS E
	ON PDR_APPOINTMENTS.ASSIGNEDTO = E.EMPLOYEEID
INNER JOIN PDR_JOURNAL
	ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
LEFT JOIN V_RequiredWorks
	ON PDR_JOURNAL.JOURNALID = V_RequiredWorks.WorksJournalId
LEFT JOIN PDR_STATUS RequiredWorksStatus 
	ON RequiredWorksStatus.STATUSID=V_RequiredWorks.STATUSID
INNER JOIN pdr_status
	ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
	AND PDR_STATUS.TITLE <> 'Cancelled'
INNER JOIN PDR_MSAT
	ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
INNER JOIN PDR_MSATType
	ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
	AND MSATTypeName LIKE '%Void%'
LEFT JOIN FL_Area ON V_RequiredWorks.faultAreaID = FL_Area.AreaID	
Where (((@appointmentType='Void Works') AND (V_RequiredWorks.RequiredWorksId = @journalId ) )
OR ((@appointmentType!='Void Works') AND (PDR_JOURNAL.JOURNALID=@journalId ) ))



--==================================================================
-- Property & Customer Info
--==================================================================
Select 
		ISNULL(C_TENANCY.TENANCYID,-1) as TenancyId,		
		ISNULL (G_TITLE.DESCRIPTION,'')+ ISNULL(' ' + C__CUSTOMER.FIRSTNAME ,'') + ISNULL(' ' + C__CUSTOMER.LASTNAME ,'') as TenantName,
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')+ISNULL(', '+P__PROPERTY.TOWNCITY ,'') +' '+ISNULL(P__PROPERTY.POSTCODE  ,'') as Address,
		ISNULL(P__PROPERTY.HOUSENUMBER, '') AS HOUSENUMBER,		
		ISNULL(P__PROPERTY.ADDRESS1,'') as ADDRESS1,
		ISNULL(P__PROPERTY.ADDRESS2,'') as ADDRESS2,
		ISNULL(P__PROPERTY.TOWNCITY,'') as TOWNCITY,
		ISNULL(P__PROPERTY.COUNTY,'') as COUNTY,
		ISNULL(P__PROPERTY.POSTCODE,'') as POSTCODE,
		ISNULL(P__PROPERTY.PROPERTYID,'') as PROPERTYID,
		ISNULL (G_TITLE.DESCRIPTION,'') + ISNULL(' ' + SUBSTRING(C__CUSTOMER.FIRSTNAME,1,1) ,'') + ISNULL(' '+ C__CUSTOMER.LASTNAME ,'') as TenantNameSalutation,
		ISNULL(C_ADDRESS.MOBILE ,'') as Mobile,
		ISNULL(C_ADDRESS.TEL ,'') as Telephone,
		ISNULL(P_SCHEME.SCHEMENAME,'') as SchemeName,
		ISNULL(C_ADDRESS.EMAIL,'') as Email,
		ISNULL(C__CUSTOMER.CUSTOMERID,-1) as CustomerId

	From		
		P__PROPERTY 
		LEFT JOIN C_TENANCY ON C_TENANCY.TENANCYID = @tenancyId
		LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = @customerId
		LEFT JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_ADDRESS.ISDEFAULT = 1  
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
		INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 

	WHERE P__PROPERTY.PROPERTYID = @propertyId

END