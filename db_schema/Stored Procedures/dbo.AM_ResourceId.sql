SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_ResourceId]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT     AM_Case.InitiatedBy

				FROM       AM_ResourcePatchDevelopment INNER JOIN
						   AM_Resource ON AM_ResourcePatchDevelopment.ResourceId = AM_Resource.ResourceId INNER JOIN
						   AM_Case ON AM_Resource.ResourceId = AM_Case.InitiatedBy 

				Where	AM_ResourcePatchDevelopment.PatchId = 12
END





GO
