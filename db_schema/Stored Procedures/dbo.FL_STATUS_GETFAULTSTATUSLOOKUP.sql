SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_STATUS_GETFAULTSTATUSLOOKUP
	/* ===========================================================================
 '   NAME:           FL_STATUS_GETFAULTSTATUSLOOKUP
 '   DATE CREATED:   21 Oct. 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get status of FL_FAULT_STATUS table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT FaultStatusId AS id,Description AS val
	FROM FL_FAULT_STATUS
	ORDER BY Description ASC






GO
