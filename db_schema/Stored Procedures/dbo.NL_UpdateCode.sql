USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[NL_UpdateCode]    Script Date: 28/04/2018 22:23:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER  PROCEDURE [dbo].[NL_UpdateCode]
(		   
		   @AccountId INT,
		   @Name NVARCHAR(100),
           @AccountNumber NVARCHAR(10),
           @accounttype INT,
           @taxcode int  ,       
		   @desc NVARCHAR(100),
		   @AllocateExpenditures BIT,
           @isBankable INT,
		   @CompanyBankDefault NVARCHAR(10),
		   @CompanyPurchaseControlDefault NVARCHAR(10),
           @Company NVARCHAR(10),
		   @isActive int )
as
BEGIN
	
	 
		UPDATE dbo.NL_ACCOUNT SET	 
		    TIMEMODIFIED =  GETDATE(),
		    NAME = @Name,
		    FULLNAME = @Name,  
		    ACCOUNTTYPE = @accounttype,
		    ACCOUNTNUMBER = @AccountNumber,
		    [DESC] = @Name,
		    TAXCODE = @taxcode,
		    ISBANKABLE = @isBankable,
			AllocateExpenditures = @AllocateExpenditures,
			ISACTIVE = @isActive
			WHERE ACCOUNTID = @AccountId


		DELETE FROM NL_ACCOUNT_TO_COMPANY WHERE ACCOUNTID = @AccountId
		while len(@Company) > 0
		begin
		  --print left(@S, charindex(',', @S+',')-1) 
		  INSERT INTO dbo.NL_ACCOUNT_TO_COMPANY ( ACCOUNTID, COMPANYID  )
		  VALUES (   @AccountId,  left(@Company, charindex(',', @Company+',')-1)   )

		  set @Company = stuff(@Company, 1, charindex(',', @Company+','), '')
		end


		DELETE FROM NL_ACCOUNT_TO_COMPANY_BANK_DEFAULT WHERE ACCOUNTID = @AccountId
		while len(@CompanyBankDefault) > 0
		BEGIN
        
		  DELETE FROM NL_ACCOUNT_TO_COMPANY_BANK_DEFAULT WHERE COMPANYID = left(@CompanyBankDefault, charindex(',', @CompanyBankDefault+',')-1)
		  --print left(@S, charindex(',', @S+',')-1) 
		  INSERT INTO dbo.NL_ACCOUNT_TO_COMPANY_BANK_DEFAULT ( ACCOUNTID, COMPANYID, createdDate)
		  VALUES (   @AccountId,  left(@CompanyBankDefault, charindex(',', @CompanyBankDefault+',')-1), GETDATE())

		  UPDATE dbo.RSL_DEFAULTS SET DEFAULTVALUE = @AccountNumber WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTNUMBER' + @CompanyBankDefault

		  set @CompanyBankDefault = stuff(@CompanyBankDefault, 1, charindex(',', @CompanyBankDefault+','), '')
		END
        

		-- default purchase control account - 1 per company
		DELETE FROM NL_ACCOUNT_TO_COMPANY_PURCHASECONTROL_DEFAULT WHERE ACCOUNTID = @AccountId
		while len(@CompanyPurchaseControlDefault) > 0
		BEGIN
        
		  DELETE FROM NL_ACCOUNT_TO_COMPANY_PURCHASECONTROL_DEFAULT WHERE COMPANYID = left(@CompanyPurchaseControlDefault, charindex(',', @CompanyPurchaseControlDefault+',')-1)
		  --print left(@S, charindex(',', @S+',')-1) 
		  INSERT INTO dbo.NL_ACCOUNT_TO_COMPANY_PURCHASECONTROL_DEFAULT ( ACCOUNTID, COMPANYID , createdDate)
		  VALUES (   @AccountId,  left(@CompanyPurchaseControlDefault, charindex(',', @CompanyPurchaseControlDefault+',')-1), GETDATE())

		  UPDATE dbo.RSL_DEFAULTS SET DEFAULTVALUE = @AccountNumber WHERE DEFAULTNAME = 'PURCHASECONTROLACCOUNTNUMBER' + @CompanyPurchaseControlDefault

		  set @CompanyPurchaseControlDefault = stuff(@CompanyPurchaseControlDefault, 1, charindex(',', @CompanyPurchaseControlDefault+','), '')
		end

END

