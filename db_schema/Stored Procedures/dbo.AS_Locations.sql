USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_Locations]    Script Date: 08-Mar-17 11:55:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_Locations
-- Author:		<Salman Nazir>
-- Create date: <25/10/2012>
-- Description:	<Get All locations for Attributes Tree>
-- Web Page: PropertyRecord.aspx
-- =============================================
IF OBJECT_ID('dbo.AS_Locations') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_Locations AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_Locations]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LocationID as LocationID,LocationName as LocationName, 'Location'as Location From PA_LOCATION
	--UNION ALL
	--	Select AreaID as LocationID, AreaName as LocationName, 'Area' as Location  from PA_AREA 
	--	where AreaName in ('Services') AND isActive = 1
	--UNION ALL
	--	SELECT itemID as LocationID,itemName as LocationName, 'Item'as Location    	From PA_item
	--	where  itemName in ('Electrics','Meters') AND isActive = 1
END
