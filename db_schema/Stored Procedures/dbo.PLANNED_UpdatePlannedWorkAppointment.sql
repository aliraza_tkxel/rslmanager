-- =============================================  
--DECLARE @return_value int,  
--  @isSaved int  
  
--EXEC @return_value = [dbo].[PLANNED_UpdatePlannedWorkAppointment]  
--  @propertyId = N'A500090007',  
--  @userId = 615,  
--  @journalId = 2,  
--  @componentId = 1,  
--  @customerNotes = N'jjljl',  
--  @appointmentNotes = N'uuu',  
--  @tenancyId = NULL,  
--  @startDate = '12/12/2013',  
--  @endDate = '12/12/2013',  
--  @startTime = N'09:00 AM',  
--  @endTime = N'13:00 PM',  
--  @operativeId = 615,  
--  @appointmentId = 21,  
--  @isSaved = @isSaved OUTPUT  
  
--SELECT @isSaved as N'@isSaved'  
  
--SELECT 'Return Value' = @return_value  
  
-- Author:  <Author,Noor Muhammad>  
-- Create date: <Create Date,11/12/2013>  
-- Description: <Description,Update/Insert PLANNED_JOURNAL,PLANNED_APPOINTMENTS  
--Last modified Date:11/12/2013  
---======================================================================  
IF OBJECT_ID('dbo.[PLANNED_UpdatePlannedWorkAppointment]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_UpdatePlannedWorkAppointment] AS SET NOCOUNT ON;') 
GO  
-- =============================================  
Alter   PROCEDURE [dbo].[PLANNED_UpdatePlannedWorkAppointment]  
 -- Add the parameters for the stored procedure here  
@propertyId varchar(20)  
,@userId int  
,@journalId int  
,@componentId int  
,@customerNotes varchar(1000)  
,@appointmentNotes varchar(1000)  
,@tenancyId int  
,@startDate date  
,@endDate date  
,@duration float  
,@startTime varchar(10)  
,@endTime varchar(10)  
,@operativeId int  
,@appointmentId int
,@schemeId int
,@blockId int  
,@isSaved int = 0 out  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
           
 DECLARE   
  
 @ArrangedId int  
 ,@JournalHistoryId int    
  
 SELECT  @ArrangedId = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='Arranged'  
  
  
 BEGIN TRANSACTION;  
 BEGIN TRY  
  
  IF @componentId = -1
  BEGIN
	Set @componentId=NULL;
  END

  if(@blockId is not null)
  BEGIN 
	SELECT @schemeId=SchemeId from P_BLOCK where BLOCKID = @blockId
  end 
  else if(@schemeId is not null)
  BEGIN
	SET @blockId = 0
  END
  -- ====================================================================================================  
  --          INSERTION (PLANNED_JOURNAL_HISTORY)  
  -- ====================================================================================================  
  -- =============================================  
  -- get status history id of "Arranged"  
  -- =============================================  
  Declare @statusHistoryId int  
  SELECT @statusHistoryId= MAX(StatusHistoryId) FROM PLANNED_StatusHistory WHERE StatusId =@ArrangedId  
  
  INSERT PLANNED_JOURNAL_HISTORY  
  ([JOURNALID]  
  ,[PROPERTYID]  
  ,[COMPONENTID]  
  ,[STATUSID]  
  ,[ACTIONID]  
  ,[CREATIONDATE]  
  ,[CREATEDBY]  
  ,[NOTES]  
  ,[ISLETTERATTACHED]  
  ,[StatusHistoryId]  
  ,[ActionHistoryId]  
  ,[IsDocumentAttached]
  ,[SchemeId]
  ,[BlockId]) VALUES  
  (@journalId,@propertyId,@componentId,@ArrangedId,null,GETDATE(),@userId,NULL,0,@statusHistoryId,NULL,0, @schemeId, @blockId)   
  
  SELECT @JournalHistoryId = SCOPE_IDENTITY()  
  --PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
  
  
  -- ====================================================================================================  
  --          INSERTION (PLANNED_APPOINTMENTS)  
  -- ====================================================================================================  
  
  
  UPDATE PLANNED_APPOINTMENTS  
  SET [JOURNALHISTORYID]=@JournalHistoryId  
  ,[APPOINTMENTDATE] = @startDate  
  ,[APPOINTMENTENDDATE] = @endDate  
  ,[APPOINTMENTSTARTTIME] = @startTime   
  ,[APPOINTMENTENDTIME] = @endTime  
  ,[ASSIGNEDTO] = @operativeId    
  ,[LOGGEDDATE] = Getdate()  
  ,[APPOINTMENTNOTES] = @appointmentNotes  
  ,[CUSTOMERNOTES] = @customerNotes  
  ,[ISPENDING] = 1  
  ,[DURATION] = @duration          
  ,[APPOINTMENTSTATUS] = 'NotStarted'          
  WHERE APPOINTMENTID = @appointmentId  
  
  -- =============================================  
  --insert into planned_appointments_History using the following trigger   
  --PLANNED_AFTER_INSERT_PLANNED_APPAOINTMENTS  
  -- =============================================  
        
        
 END TRY  
 BEGIN CATCH   
  IF @@TRANCOUNT > 0  
  BEGIN       
   ROLLBACK TRANSACTION;     
   SET @isSaved = 0    
  END  
    
  DECLARE @ErrorMessage NVARCHAR(4000);  
  DECLARE @ErrorSeverity INT;  
  DECLARE @ErrorState INT;  
  
  SELECT @ErrorMessage = ERROR_MESSAGE(),  
  @ErrorSeverity = ERROR_SEVERITY(),  
  @ErrorState = ERROR_STATE();  
  
  -- Use RAISERROR inside the CATCH block to return   
  -- error information about the original error that   
  -- caused execution to jump to the CATCH block.  
  RAISERROR (@ErrorMessage, -- Message text.  
   @ErrorSeverity, -- Severity.  
   @ErrorState -- State.  
   );  
 END CATCH;  
  
 IF @@TRANCOUNT > 0  
 BEGIN    
  COMMIT TRANSACTION;    
 SET @isSaved = 1  
 END  
  
  
END  