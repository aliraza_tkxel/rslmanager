USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetApplianceLocations]    Script Date: 18-Jan-17 2:22:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- EXEC AS_GetApplianceLocations    
-- Author:  <Author,,Noor Muhammad>    
-- Create date: <Create Date,,05/11/2012>    
-- Description: <Description,,Get all in house locations of property>    
-- Web Page: PropertyRecrod.aspx    
-- Control Page: Appliance.ascx    
-- =============================================    
IF OBJECT_ID('dbo.[AS_GetApplianceLocations]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetApplianceLocations] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetApplianceLocations]    
(@prefix varchar(100) )  
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
 SELECT a.LocationID as id , a.Location as title     
 From GS_LOCATION a
 INNER JOIN
 (SELECT Location, MIN(LocationID) as Id 
   FROM GS_LOCATION
   GROUP BY Location) AS b
   ON a.Location=b.Location
   AND a.LocationID=b.Id
  where a.LOCATION like'%'+@prefix+'%'  order by LocationID ASC     
END 
GO
