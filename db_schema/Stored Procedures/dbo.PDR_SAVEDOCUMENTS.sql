USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SAVEDOCUMENTS]    Script Date: 03/24/2016 07:15:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/*
 Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        02/12/2014		Salman Nazir			Save Documents on SaveDevelopment.aspx
    v1.1		 30/11/2015		Raja Aneeq				Add two new field named DocumentDate,UploadedDate
    
*/
--GO
-- =============================================

IF OBJECT_ID('dbo.PDR_SAVEDOCUMENTS') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_SAVEDOCUMENTS AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_SAVEDOCUMENTS]
@category varchar(50),
@title varchar(50),
@documentPath varchar(500),
@expires smalldatetime,
@fileType varchar(50),
@createdBy int,
@DocumentTypeId int,
@DocumentDate smalldatetime,
@UploadedDate smalldatetime,
@documentName varchar(100),
@DocumentId int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO PDR_DOCUMENTS (Expires,DocumentPath,AppointmentTypeId,FileType,CreatedBy,DocumentTypeId,Title ,DocumentDate,UploadedDate,DocumentName,Category)
	VALUES (@expires,@documentPath,0,@fileType,@createdBy,@DocumentTypeId,@title,@DocumentDate,@UploadedDate,@documentName,@category)
	
	SELECT @DocumentId = SCOPE_IDENTITY()
	Return @DocumentId 
END
GO
