SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[NL_LEDGERDETAILS_ITEM_BALANCE]
    (
      @TXNID INT ,
      @PERCENTAGE INT
    )
AS 
    BEGIN

        DECLARE @TRANSACTIONDATE SMALLDATETIME
        DECLARE @TRANSACTIONTYPE INT
        SELECT  @TRANSACTIONDATE = TXNDATE ,
                @TRANSACTIONTYPE = TRANSACTIONTYPE
        FROM    DBO.NL_JOURNALENTRY
        WHERE   TXNID = @TXNID


                                IF @TRANSACTIONTYPE = 10
                                BEGIN 
                                                
                                                SELECT  ISNULL(SUM(0) - SUM(CREDIT), 0) AS BALANCE
                                                FROM    ( SELECT TOP ( @PERCENTAGE )
                                                                                                                                FRJ.TRANSACTIONDATE ,
                                                                                                                                TOTALRENT AS DEBIT ,
                                                                                                                                TOTALRENT AS CREDIT
                                                                                  FROM      DBO.F_RENTJOURNAL_VOIDS FRJ
                                                                                                                                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
                                                                                  WHERE     CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                                                                                                                                AND FRJ.STATUS = 1
                                                                                ) dd
                                END
                                ELSE
                                BEGIN 
                                
                                                SELECT  ISNULL(SUM(0) - SUM(CREDIT), 0) AS BALANCE
                                                FROM    ( SELECT TOP ( @PERCENTAGE )
                                                                                                                                FRJ.ACCOUNTTIMESTAMP ,
                                                                                                                                AMOUNT AS DEBIT ,
                                                                                                                                AMOUNT AS CREDIT
                                                                                  FROM      DBO.F_RENTJOURNAL FRJ
                                                                                                                                INNER JOIN F_ITEMTYPE FIT ON FRJ.ITEMTYPE = FIT.ITEMTYPEID
                                                                                                                                INNER JOIN NL_TRANSACTIONTYPE NLTT ON @TRANSACTIONTYPE = NLTT.TRANSACTIONTYPEID
                                                                                  WHERE     CONVERT(SMALLDATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 103), 103) = @TRANSACTIONDATE
                                                                                                                                AND FIT.ITEMTYPEID = 1
                                                                                                                                AND PAYMENTTYPE IS NULL
                                                                                                                                --AND USERID IS NULL
                                                                                ) dd
                                END
    END

GO
