USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectsToBeApprovedListCount]    Script Date: 28-Nov-17 3:02:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.DF_GetDefectsToBeApprovedListCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetDefectsToBeApprovedListCount AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[DF_GetDefectsToBeApprovedListCount]
AS
BEGIN
SET NOCOUNT ON;

 SELECT count(*)
								FROM(
  SELECT *, row_number() over (order by 
JournalId
DESC
) as row	
									FROM (
SELECT 
									 D.JournalId									AS JournalId,
									d.PropertyDefectId as DefectId
									,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
									,DC.Description										AS DefectCategory
									,E.EMPLOYEEID										AS EmployeeId
									,E.FIRSTNAME + ISNULL(' '+E.LASTNAME, '')			AS EmployeeName
									,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), 'NA') + ISNULL(' ' + CONVERT(NVARCHAR, D.DefectDate,103), '')  AS RecordedBy
									,P.HOUSENUMBER + ISNULL(' ' + P.ADDRESS1, '') + ISNULL(', ' + P.TOWNCITY, '')	AS Address
									,P.POSTCODE AS POSTCODE,
									P.PROPERTYID as PropertyId	
									,P.HouseNumber																AS HouseNumber									
									,P.ADDRESS1																	AS Address1	
									, ''	AS Scheme
									, ''	AS Block
									,CAST(SUBSTRING(HouseNumber, 1,CASE when patindex('%[^0-9]%',HouseNumber) > 0 THEN PATINDEX('%[^0-9]%',HouseNumber) - 1 ELSE LEN(HouseNumber) END) AS INT) as SortAddress														
							
 FROM
										P_PROPERTY_APPLIANCE_DEFECTS AS D
										LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
										LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
										INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
										LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
										INNER JOIN P__PROPERTY P ON P.PROPERTYID = d.PropertyId										
									
 WHERE 
 1 = 1 
								AND (D.detectortypeid IS NULL OR D.detectortypeid = 0)
								AND (((D.ApplianceId IS NOT NULL AND D.ApplianceId <> 0 ) AND APP.ISACTIVE =1) OR (D.ApplianceId IS NULL or D.ApplianceId = 0 ))   
								AND D.DefectDate >= CONVERT( DATETIME, '7 Jan 2016', 106 ) 
								AND DC.Description IN ('RIDDOR', 'Immediately Dangerous', 'At Risk', 'Not to Current Standards','Other')
								and D.DefectJobSheetStatus = (SELECT STATUSID FROM PDR_STATUS WHERE TITLE LIKE 'To Be Approved')
 UNION ALL 
SELECT 
								D.JournalId									AS JournalId,
								d.PropertyDefectId as DefectId
								,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
								,DC.Description										AS DefectCategory
								,E.EMPLOYEEID										AS EmployeeId
								,E.FIRSTNAME + ISNULL(' '+E.LASTNAME, '')			AS EmployeeName
								,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), 'NA') + ISNULL(' ' + CONVERT(NVARCHAR, D.DefectDate,103), '')  AS RecordedBy
								, '' 	AS Address
								,'' AS POSTCODE,
								Convert(varchar,P.SCHEMEID) as PropertyId	
								,P.SCHEMENAME																AS HouseNumber									
								,P.SCHEMENAME													AS Address1		
								,  ISNULL(P.SCHEMENAME, '')	AS Scheme
								, ''	AS Block	
								, CAST(SUBSTRING(P.SCHEMENAME, 1,CASE when patindex('%[^0-9]%',P.SCHEMENAME) > 0 THEN PATINDEX('%[^0-9]%',P.SCHEMENAME) - 1 ELSE LEN(P.SCHEMENAME) END) AS INT) as SortAddress									
						
 FROM
										P_PROPERTY_APPLIANCE_DEFECTS AS D
										LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
										LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
										INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
										LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
										INNER JOIN P_SCHEME P ON P.SCHEMEID = d.SchemeId								
									
 WHERE 
 1 = 1 
								AND (D.detectortypeid IS NULL OR D.detectortypeid = 0)
								AND (((D.ApplianceId IS NOT NULL AND D.ApplianceId <> 0 ) AND APP.ISACTIVE =1) OR (D.ApplianceId IS NULL or D.ApplianceId = 0 ))   
								AND D.DefectDate >= CONVERT( DATETIME, '7 Jan 2016', 106 ) 
								AND DC.Description IN ('RIDDOR', 'Immediately Dangerous', 'At Risk', 'Not to Current Standards','Other')
								and D.DefectJobSheetStatus = (SELECT STATUSID FROM PDR_STATUS WHERE TITLE LIKE 'To Be Approved')
 UNION ALL 
SELECT
									D.JournalId									AS JournalId,
								d.PropertyDefectId as DefectId
								,CASE WHEN AT.APPLIANCETYPE IS NULL THEN PPV.ValueDetail ELSE AT.APPLIANCETYPE END AS Appliance
								,DC.Description										AS DefectCategory
								,E.EMPLOYEEID										AS EmployeeId
								,E.FIRSTNAME + ISNULL(' '+E.LASTNAME, '')			AS EmployeeName
								,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), 'NA') + ISNULL(' ' + CONVERT(NVARCHAR, D.DefectDate,103), '')  AS RecordedBy
								,''	AS Address
								,P.POSTCODE AS POSTCODE,
								CONVERT(VARCHAR,P.BLOCKID) as PropertyId	
								,P.BLOCKNAME																AS HouseNumber									
								,P.ADDRESS1																	AS Address1		
								, ''	AS Scheme
								, ISNULL(P.BLOCKNAME, '') + ISNULL(', ' + P.TOWNCITY, '')	AS Block
								, CAST(SUBSTRING(P.BLOCKNAME, 1,CASE when patindex('%[^0-9]%',P.BLOCKNAME) > 0 THEN PATINDEX('%[^0-9]%',P.BLOCKNAME) - 1 ELSE LEN(P.BLOCKNAME) END) AS INT)  as SortAddress														
						
 FROM
										P_PROPERTY_APPLIANCE_DEFECTS AS D
										LEFT JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
										LEFT JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
										LEFT JOIN PA_PARAMETER_VALUE PPV ON PPV.ValueID=D.BoilerTypeId
										INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
										LEFT JOIN E__EMPLOYEE AS E ON E.EMPLOYEEID = Case When D.CreatedBy > 0 then D.CreatedBy Else ModifiedBy END	
										INNER JOIN P_BLOCK P ON P.BLOCKID = d.BlockId									
									
 WHERE 
 1 = 1 
								AND (D.detectortypeid IS NULL OR D.detectortypeid = 0)
								AND (((D.ApplianceId IS NOT NULL AND D.ApplianceId <> 0 ) AND APP.ISACTIVE =1) OR (D.ApplianceId IS NULL or D.ApplianceId = 0 ))   
								AND D.DefectDate >= CONVERT( DATETIME, '7 Jan 2016', 106 ) 
								AND DC.Description IN ('RIDDOR', 'Immediately Dangerous', 'At Risk', 'Not to Current Standards','Other')
								and D.DefectJobSheetStatus = (SELECT STATUSID FROM PDR_STATUS WHERE TITLE LIKE 'To Be Approved')

)AS Records
) AS Result 
		-- End building the Count Query
		--========================================================================================


END