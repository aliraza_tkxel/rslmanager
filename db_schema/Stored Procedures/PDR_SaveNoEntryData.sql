USE [RSLBHALive]

-- =============================================
-- Author:		<Raja Aneeq
-- Create date: 11/3/2015>
-- Description:	<Save No entry data>
-- =============================================

IF OBJECT_ID('dbo.PDR_SaveNoEntryData') IS NULL 
 EXEC('CREATE PROCEDURE dbo.PDR_SaveNoEntryData AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[PDR_SaveNoEntryData]
	@FaultLogId int,
	@recordedDateTime smalldatetime
AS
BEGIN

	DECLARE @statusId int
	SELECT	@statusId =  faultStatusId 
	FROM	FL_Fault_STATUS
	WHERE	Description ='No Entry'
	SET NOCOUNT ON;

--	INSERT data to No entry table 
    INSERT INTO FL_FAULT_NOENTRY(FAULTLOGID,RECORDEDON,NOTES,ISNOENTRYSCHEDULED)
    VALUES(@FaultLogId,@recordedDateTime,'',0)
    
--	UPDATE appropriate entry to Fault log table
	UPDATE	FL_FAULT_LOG 
	SET		statusId = @statusId
	WHERE	faultLogId =@FaultLogID
	
END
