USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetServiceChargeReport]    Script Date: 23/10/2018 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetIncExcPropertiesOfServiceCharge') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetIncExcPropertiesOfServiceCharge AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetIncExcPropertiesOfServiceCharge]
		@filterSchemeId INT,		
		@orderItemId INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @itemIncPropertyCount INT , @minSchemeId INT, @blockProperties INT, @schemeProperties INT
		, @SchemeId INT, @BlockId INT, @PropertyId NVARCHAR(40), @BlockScheme INT


	SELECT	@minSchemeId = MIN(schemeid) 
	FROM	F_PurchaseItemSCInfo 
	WHERE	OrderItemId = @orderItemId 
			AND IsActive = 1

	 IF @minSchemeId = -1
		BEGIN
			
			--===============================================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES WHEN ALL SCHEMES SELECTED
			--===============================================================

			SELECT	P__PROPERTY.propertyId
					, ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2,'') AS propertyAddress
					, CASE WHEN PropExc.PROPERTYID IS NOT NULL THEN 1
					  ELSE 0
					  END  AS isExcluded
			FROM	P__PROPERTY
					LEFT JOIN (SELECT	propertyid 
							   FROM		F_ServiceChargeExProperties 
							   WHERE	PurchaseItemId = @orderItemId) PropExc ON  P__PROPERTY.propertyId = PropExc.propertyId

			WHERE  P__PROPERTY.SCHEMEID = @filterSchemeId			
			ORDER BY CAST(SUBSTRING(ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, ''), 1,CASE	WHEN PATINDEX('%[^0-9]%',ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '')) > 0 THEN 
												PATINDEX('%[^0-9]%',ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '')) - 1 
											ELSE LEN(ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '')) 
											END
											) AS INT) ASC, ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '') ASC

		END
	ELSE
		BEGIN

			--============================================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES WHEN SELECTED SCHEMES 
			--============================================================

			CREATE TABLE #PropertyInc
			(
				propertyId NVARCHAR(40)
			)


			DECLARE PropertyCount_Cursor CURSOR FOR
			SELECT	SchemeId, BlockId, PropertyId
			FROM	F_PurchaseItemSCInfo
			WHERE   OrderItemId = @orderitemid AND IsActive = 1 AND SchemeId = @filterSchemeId

			OPEN PropertyCount_Cursor
	

			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId

			WHILE @@FETCH_STATUS = 0
			BEGIN

				SET @itemIncPropertyCount = 0	

				IF @PropertyId != '-1'
				BEGIN

					IF NOT EXISTS(  SELECT  1
									FROM	F_ServiceChargeExProperties 
									WHERE	PurchaseItemId = @orderitemid 
											AND PropertyId = @PropertyId)
					BEGIN
						INSERT INTO #PropertyInc (propertyId)
						VALUES (@PropertyId)
					END

				END
				ELSE IF @BlockId != -1 AND @BlockId IS NOT NULL
				BEGIN

					INSERT INTO #PropertyInc (propertyId)
					SELECT PROPERTYID
					FROM   P__PROPERTY 
					WHERE  BLOCKID = @BlockId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

				END
				ELSE IF @SchemeId != -1
				BEGIN

					INSERT INTO #PropertyInc (propertyId)
					SELECT PROPERTYID
					FROM   P__PROPERTY 
					WHERE  SCHEMEID = @SchemeId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)
				END
			
				
			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId
			END

			CLOSE  PropertyCount_Cursor
			DEALLOCATE PropertyCount_Cursor

			
			SELECT *
			FROM (

			SELECT	P__PROPERTY.propertyId
					, ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2,'') AS propertyAddress
					, 0 AS isExcluded
			FROM	#PropertyInc
					INNER JOIN P__PROPERTY ON #PropertyInc.PROPERTYID = P__PROPERTY.PROPERTYID COLLATE SQL_Latin1_General_CP1_CI_AS

			UNION ALL

			SELECT	P__PROPERTY.propertyId
					, ISNULL(HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2,'') AS propertyAddress
					, 1 AS isExcluded
			FROM	F_ServiceChargeExProperties 
					INNER JOIN P__PROPERTY ON F_ServiceChargeExProperties.PROPERTYID = P__PROPERTY.PROPERTYID
			WHERE	PurchaseItemId = @orderitemid AND P__PROPERTY.schemeid = @filterSchemeId) TotalProperties

			ORDER BY CAST(SUBSTRING(TotalProperties.propertyAddress, 1,CASE	WHEN PATINDEX('%[^0-9]%',TotalProperties.propertyAddress) > 0 THEN 
												PATINDEX('%[^0-9]%',TotalProperties.propertyAddress) - 1 
											ELSE LEN(TotalProperties.propertyAddress) 
											END
											) AS INT) ASC, TotalProperties.propertyAddress ASC

			DROP TABLE #PropertyInc

		END


END
