SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_PAGE_TEAM_UPDATE]
(
	@PageID int,
	@TeamID int,
	@Original_PageTeamID int,
	@PageTeamID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_PAGE_TEAM] SET [PageID] = @PageID, [TeamID] = @TeamID WHERE (([PageTeamID] = @Original_PageTeamID));
	
SELECT PageTeamID, PageID, TeamID FROM I_PAGE_TEAM WHERE (PageTeamID = @PageTeamID)
GO
