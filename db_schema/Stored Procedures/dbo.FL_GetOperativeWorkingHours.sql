USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetOperativeWorkingHours]    Script Date: 11/15/2016 15:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Ali Raza>    
-- Create date: <Create Date,22/05/2014>    
-- Description: <Description,,>    
-- =============================================    
IF OBJECT_ID('dbo.[FL_GetOperativeWorkingHours]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[FL_GetOperativeWorkingHours] AS SET NOCOUNT ON;') 
GO 
ALTER  PROCEDURE [dbo].[FL_GetOperativeWorkingHours]   
 -- Add the parameters for the stored procedure here    
    @operativeId AS INT,
    @defaultStartTime AS VARCHAR(10),
    @defaultEndTime AS VARCHAR(10)    
AS     
    BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
        SET NOCOUNT ON;

        
  ------------------------------------------------------
  -- Step 1     
  ------------------------------------------------------   
     
   --This query select the week days of employee with start time and end time    
        SELECT  	@operativeId AS OperativeId
					, E_WEEKDAYS.NAME  AS Name
					, COALESCE(STARTTIME,
						CASE WHEN NAME = 'Saturday' OR NAME = 'Sunday' THEN
							''
						ELSE
							@defaultStartTime
						END
					) AS StartTime
					,COALESCE(ENDTIME,CASE WHEN NAME = 'Saturday' OR NAME = 'Sunday' THEN
							''
						ELSE
							@defaultEndTime
						END) AS EndTime
					,'NA' AS StartDate     
					,'NA' AS EndDate
					,Case when CREATEDDATE IS NULL then DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), dateadd(day,-60,GETDATE()), 103))) 
					ELSE DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CREATEDDATE)) END as CreationDate
		FROM		E_WEEKDAYS 
					LEFT JOIN E_CORE_WORKING_HOURS on E_WEEKDAYS.DAYID = E_CORE_WORKING_HOURS.DAYID AND EMPLOYEEID =@operativeId

           
        UNION ALL    
        
  ------------------------------------------------------
  -- Step 2
  ------------------------------------------------------  
      
   --This query select the extended hour and oncall hour     
        SELECT  EMPLOYEEID AS OperativeId ,    
                CONVERT(VARCHAR(20), DATENAME(dw, STARTDATE)) AS Name ,    
                STARTTIME AS StartTime ,    
                ENDTIME AS EndTime ,    
                CONVERT(VARCHAR(10), STARTDATE, 103) AS StartDate ,    
                CONVERT(VARCHAR(10), ENDDATE, 103) AS EndDate ,
                 DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, UPDATEDDATE)) as CreationDate   
        FROM    E_OUT_OF_HOURS    
                INNER JOIN E_OUT_OF_HOURS_TYPE ON E_OUT_OF_HOURS.TYPEID = E_OUT_OF_HOURS_TYPE.TYPEID    
        WHERE   EMPLOYEEID = @operativeId and CONVERT(date, STARTDATE, 103) >= CONVERT(date, GETDATE() , 103)    
     
    END    