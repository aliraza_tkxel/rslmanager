USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[ASB_GetOpenCases]    Script Date: 09-Jan-17 1:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[AS_GetCountAppointmentsToBeArranged]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetCountAppointmentsToBeArranged] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetCountAppointmentsToBeArranged]
	@PatchId		int,
	@DevelopmentId	int
AS
BEGIN
	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT 
								COUNT(*) AS Number'
	
	SET @fromClause		= 'FROM P__PROPERTY 
										INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID							
										INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
										--INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
										LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
										LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										INNER JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
										LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
										INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
										INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID
										
										
										OUTER Apply(SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
														Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
														ELSE Convert(Varchar(50), M.ReletDate,103)END as ReletDate,J.JOURNALID
													FROM P__PROPERTY P
											INNER JOIN C_JOURNAL J ON C_TENANCY.TENANCYID =J.TENANCYID
											INNER JOIN C_TERMINATION T ON J.JOURNALID = T.JOURNALID 
											INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
											INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
											INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
											INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
											WHERE ((DATEADD(YEAR,1,P_LGSR.ISSUEDATE) BETWEEN T.TTIMESTAMP AND T.TerminationDate)
													OR (DATEADD(YEAR,1,P_LGSR.ISSUEDATE) > T.TerminationDate))
										 ) As  T
										

										Left JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = C_TENANCY.TENANCYID
										AND CT.CUSTOMERTENANCYID = (SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					)

										Left JOIN C_ADDRESS CA ON CA.CUSTOMERID = CT.CUSTOMERID And CA.ISDEFAULT=1
										Left JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = CT.CUSTOMERID
										LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P__PROPERTY.PROPERTYID
										AND
										A.ITEMPARAMID =
										(
											SELECT
												ItemParamID
											FROM
												PA_ITEM_PARAMETER
													INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
													INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
											WHERE
												ParameterName = ''Heating Fuel''
												AND ItemName = ''Heating''
										)
										LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
										Outer Apply (SELECT count( RISKHISTORYID) as risk
										FROM C_JOURNAL J 
										INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID
										WHERE CUSTOMERID = CUST.CUSTOMERID AND ITEMNATUREID=63 
										AND CR.RISKHISTORYID = (SELECT MAX(RISKHISTORYID) FROM C_RISK IN_CR WHERE IN_CR.JOURNALID=J.JOURNALID)                 
										AND CR.ITEMSTATUSID <> 14) riskCounter '
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + '1=1
										AND P__PROPERTY.FUELTYPE = 1
                                        AND AS_STATUS.Title  in (''Appointment to be arranged'' , ''No Entry'',''Legal Proceedings'', ''Aborted'', ''Cancelled'')
										AND AS_JOURNAL.IsCurrent = 1
										AND PV.ValueDetail = ''Mains Gas'' 
										AND dbo.P__PROPERTY.STATUS IN(SELECT STATUSID FROM P_STATUS WHERE DESCRIPTION IN(''Let'',''Available to rent'',''Unavailable'')) 
										AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND A.PARAMETERVALUE = ''Mains Gas''
										--AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'

	IF (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
		
	IF (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	IF (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	IF (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
  	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause
	
	-- Printing the query for debugging
	PRINT @query 
	
	-- Executing the query
    EXEC (@query)
 
END




