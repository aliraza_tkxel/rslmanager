USE [RSLBHALive ]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_GetJobSheetSummaryByJournalId'))
  DROP PROCEDURE AS_GetJobSheetSummaryByJournalId
  GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 22/2/2016
-- Description:	Get JObsheet details by journalId
-- webpage:	
-- EXEC AS_GetJobSheetSummaryByJournalId 6550	
-- =============================================
Create PROCEDURE [dbo].[AS_GetJobSheetSummaryByJournalId]
	-- Add the parameters for the stored procedure here
	 @journalId INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT DISTINCT
  -- P__PROPERTY.PROPERTYID,
		 CONVERT(NVARCHAR ,AS_JOURNAL.JOURNALID)  AS Ref,	
		acw.PurchaseOrder AS PoNumber,		
		org.NAME AS Contractor,
		(ee.FIRSTNAME + ' '+ ee.LASTNAME) AS Contact,	
		acwd.WorkRequired,
		ee1.FIRSTNAME + ' ' + ee1.LASTNAME AS AssignedBy,
		CAST (acw.AssignedDate AS DATE) AS AssignedDate,
		CAST (DATEADD(YEAR,1,pl.ISSUEDATE) AS DATE) AS CP12Expiry,
		AS_Status.Title AS Status,
		ex.DESCRIPTION AS Budget,
		ISNULL(NULLIF(acw.EstimateRef, ''), 0.00) AS Estimate,
		acwd.NetCost,
		acwd.Vat,
		acwd.Gross,
		ps.SCHEMENAME,		
		ISNULL(NULLIF(ISNULL('Flat No:' + P__PROPERTY.FLATNUMBER + ',', '') +
		 ISNULL(P__PROPERTY.HOUSENUMBER , '') + 
		ISNULL(', ' + P__PROPERTY.ADDRESS1, '')+
		ISNULL(', ' + P__PROPERTY.ADDRESS2, '') + ISNULL(' ' + P__PROPERTY.ADDRESS3, '') 
		+ ISNULL(', ' + P__PROPERTY.TOWNCITY, '') , ''), 'N/A') AS [Address],
		ISNULL(P__PROPERTY.POSTCODE,'') POSTCODE,
		 CUST.CUSTOMERID AS CustomerId,
         REPLACE(ISNULL(CUST.FIRSTNAME, '') + ' ' + ISNULL(CUST.MIDDLENAME, '') + ' ' + ISNULL(CUST.LASTNAME, ''), '  ', ' ') AS ClientName, 
         ISNULL(C_ADD.TEL,'N/A') AS ClientTel,
         ISNULL(C_ADD.MOBILE, 'N/A') AS ClientMobile,
         ISNULL(C_ADD.EMAIL, 'N/A') AS ClientEmail,
         acwd.PurchaseOrderItemId
	
 FROM P__PROPERTY  
		INNER JOIN dbo.AS_JOURNAL  ON dbo.AS_JOURNAL.PROPERTYID=dbo.P__PROPERTY.PROPERTYID
		CROSS APPLY (SELECT TOP 1 * FROM AS_JournalHeatingMapping WHERE AS_JournalHeatingMapping.JournalId=AS_JOURNAL.JournalId) PA_HeatingMapping
		INNER JOIN AS_CONTRACTOR_WORK acw ON acw.journalId = 	AS_JOURNAL.JOURNALID
		INNER JOIN F_PURCHASEITEM fp ON fp.ORDERID = acw.PurchaseOrder
		INNER JOIN AS_CONTRACTOR_WORK_DETAIL acwd ON acwd.PurchaseOrderItemId = fp.ORDERITEMID			
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN S_ORGANISATION org ON org.ORGID = acw.ContractorId
		INNER JOIN E__EMPLOYEE ee ON ee.EMPLOYEEID = acw.ContactId
		INNER JOIN E__EMPLOYEE ee1 ON ee1.EMPLOYEEID = acw.AssignedBy
		INNER JOIN P_LGSR pl ON pl.HeatingMappingId = PA_HeatingMapping.HeatingMappingId
		INNER JOIN F_EXPENDITURE ex ON ex.EXPENDITUREID = acwd.ExpenditureId
		INNER JOIN P_SCHEME ps ON ps.SCHEMEID = P__PROPERTY.SCHEMEID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.HeatingMappingId = PA_HeatingMapping.HeatingMappingId
		AND
		A.ITEMPARAMID =
		(
			SELECT
				ItemParamID
			FROM
				PA_ITEM_PARAMETER
					INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
					INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
			WHERE
				ParameterName = 'Heating Fuel'
				AND ItemName = 'Heating'
		)
		LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
		LEFT JOIN ( SELECT CT.CUSTOMERID, T.PROPERTYID 
    FROM C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON ( T.TENANCYID = CT.TENANCYID ) AND 
  ( T.ENDDATE >= CONVERT(date, GETDATE() ) OR 
    T.ENDDATE IS NULL ) AND
  ( CT.ENDDATE >= CONVERT(date, GETDATE() )	OR 
    CT.ENDDATE IS NULL )
   WHERE ( CUSTOMERTENANCYID = ( SELECT max( CUSTOMERTENANCYID ) FROM C_CUSTOMERTENANCY C_CT WHERE C_CT.TENANCYID = CT.TENANCYID ) )
) TENANCY ON ( TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID ) 
         LEFT JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID = TENANCY.CUSTOMERID			
         LEFT JOIN C_ADDRESS C_ADD ON ( CUST.CUSTOMERID = C_ADD.CUSTOMERID ) AND ( ISNULL( C_ADD.ISDEFAULT, 0 ) = 1 )
   WHERE ( AS_JOURNAL.JOURNALID = @journalId )
				
AND AS_JOURNAL.STATUSID = 6  
AND  1=1 
AND AS_JOURNAL.STATUSID = 6 
AND AS_JOURNAL.IsCurrent = 1
AND PV.ValueDetail = 'Mains Gas'
AND dbo.P__PROPERTY.STATUS NOT IN (9,5,6) 
AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
Order By Address
DESC

  Select P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
         P_Asbestos.RISKDESCRIPTION Description
    From AS_JOURNAL AJ
         INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID = AJ.PROPERTYID
         INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
         INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID	  
   Where ( AJ.JOURNALID = @journalId )

END
