SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_G_TITLE_GetTitleLookup
/* ===========================================================================
 '   NAME:           TO_G_TITLE_GetTitleLookup
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get title records from G_TITLE table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT TITLEID AS id,DESCRIPTION AS val
	FROM G_TITLE
	ORDER BY DESCRIPTION ASC

GO
