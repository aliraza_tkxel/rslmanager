SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO


CREATE   proc [dbo].[F_INSERT_NEW_SCHEDULE]
	(
	 @Scheduledate datetime,
	 @LocAuthority int,
	 @ScheduledTot money
	)
as 
begin 

	INSERT INTO [F_SCHEDULE]
		( 
		 [Scheduledate], 
		 [LocAuthority], 
		 [ScheduledTot])
	VALUES( @Scheduledate, @LocAuthority, @ScheduledTot)
end




GO
