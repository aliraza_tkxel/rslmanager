USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPlannedComponentsDetail]    Script Date: 04/04/2016 14:42:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_GetPlannedComponentsDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetPlannedComponentsDetail AS SET NOCOUNT ON;') 
GO
-- =============================================
--EXEC	[dbo].[AS_GetPlannedComponentsDetail]
--		@propertyId = N'A010060001'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,, 4 August, 2014>
-- Description:	<Description,,This procedure 'll get the Planned Components Detail >
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetPlannedComponentsDetail] 
	@propertyId as varchar(20)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT	PC.COMPONENTNAME AS Component
			,ISNULL(CONVERT(VARCHAR(10),YEAR(PID.LastDone)),'-') AS LastReplaced
			,ISNULL(CONVERT(VARCHAR(10),YEAR(PID.DueDate)),'-') AS Due
	FROM	PLANNED_COMPONENT AS PC
			LEFT JOIN ( SELECT	PLANNED_COMPONENTID,MAX(SID) AS SUB_SID
						FROM	PA_PROPERTY_ITEM_DATES
						INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PCI.COMPONENTID AND PA_PROPERTY_ITEM_DATES.ItemId = PCI.ITEMID
						WHERE   PROPERTYID = @propertyId
						GROUP BY PLANNED_COMPONENTID )  AS PID_SUB ON PC.COMPONENTID = PID_SUB.PLANNED_COMPONENTID
			LEFT JOIN	PA_PROPERTY_ITEM_DATES PID ON PID_SUB.SUB_SID = PID.SID
			
	ORDER BY  PC.SOrder  asc
	
	
END
