USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_UpdatePropertyCustomerEmail]    Script Date: 14/04/2017 09:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
		
-- Author:		Saud Ahmed
-- Create date: 14/04/2017
-- Description:	<This stored procedure returns the update the appointment record when user resend email from property activities>

-- =============================================

IF OBJECT_ID('dbo.AS_UpdatePropertyCustomerEmail') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_UpdatePropertyCustomerEmail AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_UpdatePropertyCustomerEmail]
( 
		@customerEmail nvarchar(50) = '',
		@appointmentId nvarchar(50) = '',
		@emailStatus varchar(50) = ''
		
)
AS
BEGIN
update AS_APPOINTMENTS 
	set [CustomerEmail] = @customerEmail, 
	[CustomerEmailStatus]= (select [EmailStatusId] from AS_EmailStatus where StatusDescription=@emailStatus)
where APPOINTMENTID = @appointmentId

END

