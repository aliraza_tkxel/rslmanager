USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[CM_GetServicesAllocated]    Script Date: 10/4/2018 8:59:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.CM_GetServicesAllocated') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.CM_GetServicesAllocated AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[CM_GetServicesAllocated]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='', 
		@offset int = 1,
		@limit  int = 100,		 
		@sortColumn varchar(500) = 'ServiceItemId', 
		@sortOrder varchar (5) = 'DESC',		
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        @filterCriteria varchar(200)='',
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
         
		@ArrangedStatusId int
		--Paging Formula
		 
		
	 
		SELECT  @ArrangedStatusId =  StatusId FROM CM_Status WHERE TITLE = 'To Be Allocated'
		--=====================Search Criteria===============================
		SET @searchCriteria = '1=1 AND SI.StatusId not in (SELECT StatusId FROM CM_Status WHERE TITLE IN(''To Be Allocated'',''Rejected'',''Cancelled''))' 
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( S.SCHEMENAME LIKE ''%' + @searchText + '%'''			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.BLOCKNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR I.ItemName LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.POSTCODE LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR D.POSTCODE LIKE ''%' + @searchText + '%'')'
			
		END	
		  
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select   top ('+convert(varchar(10),@limit)+')
		 ServiceItemId, ISNULL(S.SCHEMENAME,''-'') AS SchemeName,CONVERT(varchar(10), SI.ContractCommencement,103) as Commencement,
		ISNULL(B.BLOCKNAME,''-'') AS BlockName,
		Case WHEN B.BLOCKNAME is not null then B.POSTCODE ELSE D.PostCode END as Postcode,		
		I.ItemName As  ServiceRequired, O.NAME as Contractor,SI.ContractorId,
		CS.Title as [Status],CS.StatusId,
		Case When po.POSTATUS = 0 then ''-'' else CONVERT(VARCHAR(10), SI.PORef) END as PORef,CONVERT(varchar(10),Contractor.STARTDATE,103)+'' - ''+CONVERT(varchar(10), Contractor.RENEWALDATE,103)as ContractStart'

		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM  CM_ServiceItems SI
		LEFT JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID
		LEFT JOIN   P_SCHEME S ON SI.SchemeId=S.SCHEMEID Or B.SchemeId=S.SCHEMEID
		Left JOIN PDR_DEVELOPMENT D on S.DEVELOPMENTID= D.DEVELOPMENTID
		INNER JOIN PDR_CycleType PT ON SI.ContractPeriodType= PT.CycleTypeId
		INNER JOIN PA_ITEM I ON SI.ItemId = I.ItemID
		INNER JOIN S_ORGANISATION O ON SI.ContractorId=O.ORGID
		INNER JOIN CM_Status CS on SI.StatusId= CS.StatusID	
		outer APPLY (SELECT * from F_PURCHASEORDER where F_PURCHASEORDER.ORDERID = SI.PORef) as po
		CROSS APPLY(
		SELECT DISTINCT O.ORGID AS [id],
					NAME AS [description], S.STARTDATE,S.RENEWALDATE
			FROM S_ORGANISATION O
			INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
			INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID 
			INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID 	
			LEFT JOIN P_SCHEME on SS.SchemeId = P_SCHEME.SCHEMEID 
			LEFT JOIN P_BLOCK B on SS.BlockId = B.BLOCKID OR B.SchemeId = SS.SchemeId
			LEFT JOIN E_PATCH e on ss.PATCHID=e.PATCHID 
			WHERE AoW.DESCRIPTION = ''Cyclical Services'' AND O.ORGID=SI.ContractorId
					AND ((SS.BLOCKID = SI.BlockId OR SS.SchemeId = SI.SchemeId)OR (B.BLOCKID = SI.BlockId)or e.LOCATION = ''All'')
					AND (S.CREATIONDATE <= GETDATE()
						AND S.RENEWALDATE > GETDATE()))Contractor'

		--============================Order Clause==========================================
		IF(@sortColumn = 'SchemeName')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' SchemeName' 	
			
		END
		
		IF(@sortColumn = 'BlockName')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'BlockName' 	
			
		END
		IF(@sortColumn = 'Postcode')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Postcode' 	
			
		END
		
	 
		
		IF(@sortColumn = 'ServiceRequired')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ServiceRequired' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		 
			print(@finalQuery)
			EXEC (@finalQuery)
	 
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
