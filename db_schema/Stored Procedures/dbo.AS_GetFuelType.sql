USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetFuelType]    Script Date: 04/21/2014 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetFuelType
-- Author:	<Noor Muhammad>
-- Create date: <02/11/2012>
-- Description:	<Get all fuel types from AS_GetFuelType table>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================

IF OBJECT_ID('dbo.[AS_GetFuelType]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetFuelType] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetFuelType]
@heatingTypeId INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF(@heatingTypeId = 0)
	BEGIN
		SELECT ValueID as Id,ValueDetail as Title  FROM  PA_PARAMETER_VALUE
		INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.ParameterID = PA_PARAMETER.ParameterID
		INNER JOIN PA_ITEM_PARAMETER ON PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
		INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
		WHERE ParameterName = 'Heating Fuel' AND ItemName = 'Heating' And PA_PARAMETER_VALUE.IsActive = 1
		Order By Sorder ASC
	END
	
	IF(@heatingTypeId = -1)
	BEGIN
		SELECT ValueID as Id, ValueDetail as Title FROM PA_PARAMETER_VALUE
		WHERE ValueDetail IN ('Biomass','Electric','Mains Electric','Air Source','Wet Electric','MVHR & Heat Recovery')
		And ParameterID = (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = 'Heating Fuel') and IsActive = 1
	END

	IF(@heatingTypeId <> -1)
	BEGIN
		SELECT ValueID as Id,ValueDetail as Title  FROM  PA_PARAMETER_VALUE
		INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.ParameterID = PA_PARAMETER.ParameterID
		INNER JOIN PA_ITEM_PARAMETER ON PA_PARAMETER.ParameterID= PA_ITEM_PARAMETER.ParameterId
		INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
		WHERE ParameterName = 'Heating Fuel' AND ItemName = 'Heating' And PA_PARAMETER_VALUE.IsActive = 1
		AND PA_PARAMETER_VALUE.ValueID = @heatingTypeId
		Order By Sorder ASC
	END

	
END
