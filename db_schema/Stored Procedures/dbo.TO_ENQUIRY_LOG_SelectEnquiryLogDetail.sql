SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_SelectEnquiryLogDetail]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_SelectEnquiryLogDetail
 '   DATE CREATED:   12 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display a single record against EnquiryLogID
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(	    		
		@enqLogID int				
	)
			
AS
  SELECT  
		 TO_ENQUIRY_LOG.TenancyID, 
		 TO_ENQUIRY_LOG.CustomerID,
		 TO_ENQUIRY_LOG.ItemNatureID,
		 C_NATURE.ITEMID,
		C_NATURE.DESCRIPTION
		 
  FROM     

		TO_ENQUIRY_LOG INNER JOIN C_NATURE 
		ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID
		
  WHERE   
	  (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID) 

GO
