SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	[dbo].[AS_DeleteStandardLetterTemplate]
--		@LetterId = 1 
-- Author:		Hussain Ali
-- Create date: 10/16/2012
-- Description:	This SP would delete the letter based on the Standard Letter ID
-- Useage: ViewLetters.aspx
-- =============================================
Create PROCEDURE [dbo].[AS_DeleteStandardLetterTemplate]
	-- Add the parameters for the stored procedure here
	@LetterId			int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    UPDATE 
		AS_StandardLetters   
	SET 
		IsActive = 0
	Where 
		StandardLetterId = @LetterId 
   
END
GO
