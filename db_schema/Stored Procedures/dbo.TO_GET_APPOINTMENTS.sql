SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Rogers
-- Create date: 3rd February 2015
-- Description:	SP to return call current and future appointments excluding those completed
-- =============================================
CREATE PROCEDURE [dbo].[TO_GET_APPOINTMENTS] 
	-- Add the parameters for the stored procedure here
    @tenancyId INT = 0
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @morning AS VARCHAR(50) = 'Between 8am - 1pm'
        DECLARE @afternoon AS VARCHAR(50) = 'Between 12noon - 5pm'

        SELECT  'Reactive Repair' AS [Type] ,
                ct.TenancyId ,
				--fl.CustomerId ,
                CAST(a.AppointmentDate AS DATE) AS [AppointmentDate] ,
                CASE WHEN CAST([time] AS TIME) < '13:00:00' THEN ( SELECT
                                                              @morning
                                                              )
                     ELSE ( SELECT  @afternoon
                          )
                END AS [AppointmentSlot]
        FROM    dbo.FL_CO_APPOINTMENT a
                LEFT JOIN ( SELECT  AppointmentId ,
                                    MAX(FaultLogId) AS FaultLogId
                            FROM    FL_FAULT_APPOINTMENT
                            GROUP BY AppointmentId
                          ) fa ON ( fa.AppointmentId = a.AppointmentID )
                LEFT JOIN FL_FAULT_LOG fl ON ( fa.FaultLogId = fl.FaultLogID )
                LEFT JOIN dbo.vw_PROPERTY_CURRENT_TENANTS_LIST ct ON ( fl.CustomerId = ct.CUSTOMERID )
        WHERE   1 = 1
                AND a.AppointmentStatus <> ''
                AND a.AppointmentStatus != 'Complete'
                AND a.AppointmentDate >= CONVERT(DATE, GETDATE())
                AND ct.TenancyId = @tenancyId
        UNION ALL
        SELECT  'Gas Servicing' AS [Type] ,
                ct.TenancyId ,
                CAST(a.AppointmentDate AS DATE) AS [AppointmentDate] ,
                CASE WHEN CAST(APPOINTMENTSTARTTIME AS TIME) < '13:00:00'
                     THEN ( SELECT  @morning
                          )
                     ELSE ( SELECT  @afternoon
                          )
                END AS [AppointmentSlot]
        FROM    AS_APPOINTMENTS a
                LEFT JOIN dbo.AS_JOURNAL j ON ( a.JournalId = j.JOURNALID )
                LEFT JOIN ( SELECT  PropertyId ,
                                    TenancyId ,
                                    MAX(CUSTOMERID) AS CUSTOMERID
                            FROM    dbo.vw_PROPERTY_CURRENT_TENANTS_LIST
                            GROUP BY PropertyId ,
                                    TenancyId
                          ) ct ON ( j.PROPERTYID = ct.PropertyId )
        WHERE   1 = 1
                AND APPOINTMENTSTATUS != 'Finished'
                AND a.AppointmentDate >= CONVERT(DATE, GETDATE())
                AND ct.TenancyId = @tenancyId
        UNION ALL
        SELECT  'Planned Works' AS [Type] ,
                ct.TenancyId ,
                CAST(a.AppointmentDate AS DATE) AS [AppointmentDate] ,
                CASE WHEN CAST(APPOINTMENTSTARTTIME AS TIME) < '13:00:00'
                     THEN ( SELECT  @morning
                          )
                     ELSE ( SELECT  @afternoon
                          )
                END AS [AppointmentSlot]
        FROM    PLANNED_APPOINTMENTS a
                LEFT JOIN ( SELECT  TenancyId ,
                                    MAX(CUSTOMERID) AS CUSTOMERID
                            FROM    dbo.vw_PROPERTY_CURRENT_TENANTS_LIST
                            GROUP BY TenancyId
                          ) ct ON ( a.TENANCYID = ct.TenancyId )
        WHERE   1 = 1
                AND APPOINTMENTSTATUS != 'Finished'
                AND a.AppointmentDate >= CONVERT(DATE, GETDATE())
                AND a.TENANCYID = @tenancyId
        UNION ALL
        SELECT  'Stock Survey' AS [Type] ,
                [as].TenancyId ,
                CAST(a.AppointStartDateTime AS DATE) AS [AppointmentDate] ,
                CASE WHEN CAST(AppointStartDateTime AS TIME) < '13:00:00'
                     THEN ( SELECT  @morning
                          )
                     ELSE ( SELECT  @afternoon
                          )
                END AS [AppointmentSlot]
        FROM    dbo.PS_Appointment a
                LEFT JOIN dbo.PS_Property2Appointment [as] ON ( a.AppointId = [as].AppointId )
        WHERE   1 = 1
                AND AppointStartDateTime >= CONVERT(DATE, GETDATE())
                AND AppointProgStatus != 'Complete'
                AND TenancyId = @tenancyId
        ORDER BY APPOINTMENTDATE

    END
GO
