SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE [dbo].[sp_getorganisations]
AS 

select org_id,orgname,address1,tel,town,'' BLANK,
Active = 
      CASE active
         WHEN 1 THEN 'Active'
         ELSE 'Inactive'
      END

 from h_organisation



GO
