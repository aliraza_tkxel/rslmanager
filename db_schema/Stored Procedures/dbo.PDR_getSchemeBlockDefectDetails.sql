USE [RSLBHALive]
GO
IF OBJECT_ID('dbo.PDR_getSchemeBlockDefectDetails') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_getSchemeBlockDefectDetails AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[PDR_getSchemeBlockDefectDetails](  
@propertyDefectId int  
)  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;

-- Insert statements for procedure here  
SELECT
	PropertyDefectId								AS PropertyDefectId
	,Convert(varchar, ISNULL(AD.BlockId, AD.SchemeId))				AS Id
	,CASE WHEN AD.BlockId IS NULL THEN 'scheme' else 'block' end				AS RequestType
	,AD.CategoryId									AS CategoryId
	,DC.Description									AS DefectCategory
	,DefectDate										AS DefectDate
	,JournalId										AS JournalId
	,ISNULL(IsDefectIdentified, 0)					AS IsDefectIdentified
	,ISNULL(DefectNotes, '')						AS DefectNotes
	,ISNULL(IsActionTaken, 0)						AS IsRemedialActionTaken
	,ISNULL(ActionNotes, '')						AS RemedialActionNotes
	,ISNULL(IsWarningIssued, 0)						AS IsWarningIssued
	,ApplianceId									AS ApplianceId
	,ISNULL(AD.SerialNumber, '')					AS SerialNumber
	,ISNULL(AD.GasCouncilNumber, '')				AS GcNumber
	,ISNULL(IsWarningFixed, 0)						AS IsWarningFixed
	,AD.IsDisconnected								AS IsApplianceDisconnected
	,AD.IsPartsrequired								AS IsPartsRequired
	,AD.IsPartsOrdered								AS IsPartsOrdered
	,AD.PartsOrderedBy								AS PartsOrderedBy
	,ISNULL(POB.FIRSTNAME + ' ' + POB.LASTNAME, '')	AS PartsOrderedByName
	,AD.PartsDue									AS PartsDueDate
	,ISNULL(AD.PartsDescription,'')					AS PartsDescription
	,ISNULL(AD.PartsLocation,'')					AS PartsLocation
	,ISNULL(AD.IsTwoPersonsJob, 0)					AS IsTwoPersonsJob
	,ISNULL(AD.ReasonFor2ndPerson,'')				AS ReasonForSecondPerson
	,AD.Duration									AS EstimatedDuration
	,AD.Priority									AS PriorityId
	,ISNULL(DP.PriorityName,'N/A')					AS PriorityName
	,AD.TradeId										AS TradeId
	,ISNULL(T.Description,'N/A')					AS Trade
	,ISNULL(PhotoNotes,'')							AS PhotoNotes
	,ISNULL(AT.APPLIANCETYPE,'')					AS APPLIANCE
	,ISNULL(PV.ValueDetail,'')						AS BOILER
	,ISNULL(AD.HeatingMappingId, 0) as HeatingMappingId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		LEFT JOIN GS_PROPERTY_APPLIANCE A ON AD.ApplianceId = A.PROPERTYAPPLIANCEID
		LEFT JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		LEFT JOIN PA_PARAMETER_VALUE PV ON PV.ValueID=AD.BoilerTypeId
		LEFT JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
		LEFT JOIN E__EMPLOYEE POB ON AD.PartsOrderedBy = POB.EMPLOYEEID
		LEFT JOIN G_TRADE T ON AD.TradeId = T.TradeId
		LEFT JOIN P_DEFECTS_PRIORITY DP ON AD.Priority = DP.PriorityID
WHERE
	PropertyDefectId = @propertyDefectId
END
