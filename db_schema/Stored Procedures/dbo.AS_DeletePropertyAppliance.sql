USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,,09/07/2016>
-- Description:	<Description,Delete appliance against property >
-- Modified : <Aamir Waheed, 09/07/2016>
-- =============================================

IF OBJECT_ID('dbo.AS_DeletePropertyAppliance') IS NULL
 EXEC('CREATE PROCEDURE dbo.AS_DeletePropertyAppliance AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[AS_DeletePropertyAppliance]
	@applianceId int=0,
	@isDeleted bit=0 out
AS
BEGIN	     
	-- SET NOCOUNT ON added to prevent extra result sets from	
	SET NOCOUNT ON;			

BEGIN TRANSACTION
BEGIN TRY

	--============================================
	-- Delete/Deactivate appliance
	--============================================ 
	UPDATE	GS_PROPERTY_APPLIANCE
	SET		IsActive = 0
	WHERE	PROPERTYAPPLIANCEID = @applianceId

	

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
		SET		@isDeleted = 1;
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;
		SET		@isDeleted = 0;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

END
