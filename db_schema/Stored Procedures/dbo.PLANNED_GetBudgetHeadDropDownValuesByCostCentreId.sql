USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetBudgetHeadDropDownValuesByCostCentreId]    Script Date: 04/28/2016 11:54:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 30/04/2014
-- Description:	To get Budget Head by the Cost Centre Id, this will be used for assign to contractor in Planned Maintenance Module.
-- Web Page:	AssignToContractor.ascx(User Control)
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetBudgetHeadDropDownValuesByCostCentreId] 
	-- Add the parameters for the stored procedure here
	@costCentreId INT

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @YRange int
SELECT
	@YRange = YRange
FROM F_FiscalYears
WHERE YStart <= CONVERT(date, GETDATE())
AND YEnd >= CONVERT(date, GETDATE())

-- Get Budget Heads by Cost Centre Id provided as Input Parameter.
SELECT DISTINCT
	HE.HEADID [id],
	HE.DESCRIPTION [description]
FROM F_HEAD HE
INNER JOIN F_HEAD_ALLOCATION HEA
	ON HEA.HEADID = HE.HEADID
	AND HEA.ACTIVE = 1
	AND (HEA.FISCALYEAR = @YRange
	OR HEA.FISCALYEAR IS NULL)
	AND EXISTS (SELECT
		HEADID
	FROM F_EXPENDITURE EX
	INNER JOIN F_EXPENDITURE_ALLOCATION EXA
		ON EXA.EXPENDITUREID = EX.EXPENDITUREID
		AND (EXA.FISCALYEAR = @YRange
		OR EXA.FISCALYEAR IS NULL)
		AND EXA.ACTIVE = 1)
WHERE HE.CostCentreID = @CostCentreId
ORDER BY HE.DESCRIPTION ASC
END
