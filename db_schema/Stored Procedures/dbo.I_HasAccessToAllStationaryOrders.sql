SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ===================================================================
-- Author:		Simon Rogers	
-- Create date: 30th September 2014
-- Description:	Returns employee IDs for employees who should see
--              all orders
-- ===================================================================

CREATE PROCEDURE [dbo].[I_HasAccessToAllStationaryOrders]
(@EmployeeId INT)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @HASACCESS BIT = 0
	
	IF EXISTS(SELECT * FROM dbo.RSL_DEFAULTS WHERE   DEFAULTNAME = 'ACCESS_TO_STATIONARY_ORDERS' AND DEFAULTVALUE = CAST(@EmployeeId AS VARCHAR))
	BEGIN
		SET @HASACCESS = 1
	END
	
	
	SELECT @HASACCESS
	
END
GO
