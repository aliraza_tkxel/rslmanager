USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FLS_GetAvailableOperativesAppointedForVoidInspection]    Script Date: 13-Feb-18 3:09:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON 
GO  

IF OBJECT_ID('dbo.FLS_GetAvailableOperativesAppointedForVoidInspection') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_GetAvailableOperativesAppointedForVoidInspection AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FLS_GetAvailableOperativesAppointedForVoidInspection] 
	-- Add the parameters for the stored procedure here
	@msattype as varchar(max),
	@startDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF 1 = 2
	BEGIN
		SELECT DISTINCT
		CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME)																										AS AppointmentStartDate
		,CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME)																										AS AppointmentEndDate
		,0																																					AS OperativeId
		,'00:00 AM'																																			AS StartTime
		,'11:59 PM'																																			AS EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInSec
		,''																																					AS PostCode
		,''																																					AS Address
		,''																																					AS TownCity
		,''																																					AS County
		WHERE
		1 = 2 
	END

    -- Insert statements for procedure here
	-- Insert statements for procedure here
	DECLARE @AvailableOperatives TABLE(
		EmployeeId int,
		FullName nvarchar(100)
		)

		INSERT INTO @AvailableOperatives exec dbo.FLS_GetAvailableOperativesForVoidInspection @msattype

		--This query selects the appointments of employees i.e the employess which we get in step1
	--Fault Appointments
	--Gas Appointments
	--Planned Appointments
	--M&E Servicing or Cyclic Maintenance Appointments
	-----------------------------------------------------------------------------------------------------------------------
	-- Fault Appointments
	SELECT AppointmentDate as AppointmentStartDate
	,AppointmentDate as AppointmentEndDate
	,OperativeId as OperativeId
	,Time as StartTime
	,EndTime as EndTime
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + Time,103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,p__property.postcode as PostCode
	,CASE  WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS2, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS3, '')  
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(P_BLOCK.ADDRESS2, '') + ', '  
          + ISNULL(P_BLOCK.ADDRESS3, '')  
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
          
      END	AS Address
		,CASE  WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.TownCity, '')
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.TownCity, '')  
          
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ''
          
      END as TownCity																																			
		,CASE  WHEN FL_FAULT_LOG.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.County, '')
        WHEN FL_FAULT_LOG.BLOCKID > 0 THEN ISNULL(P_BLOCK.County, '')  
          
        WHEN FL_FAULT_LOG.SCHEMEID > 0 THEN ''
          
      END  as County
	,'Fault Appointment'  as AppointmentType
	, DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LastActionDate)) AS CreationDate
	FROM FL_CO_Appointment 
	inner join fl_fault_appointment on FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
	inner join fl_fault_log on fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
	LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID  
			LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
			LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID 		
	WHERE 
	1=1
	AND appointmentdate >= CONVERT(DATE,@startDate)
	AND (FL_FAULT_STATUS.Description <> 'Cancelled' AND FL_FAULT_STATUS.Description <> 'Complete')
	AND operativeid in (SELECT employeeid FROM  @AvailableOperatives)	
	-----------------------------------------------------------------------------------------------------------------------
	--Appliance Appointments	
	UNION ALL 	
	SELECT 
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address
		,P__PROPERTY.TownCity as TownCity
		,P__PROPERTY.County as County
		,'Gas Appointment' as AppointmentType 
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME,  LOGGEDDATE)) AS CreationDate 
	FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	WHERE AS_JOURNAL.IsCurrent = 1		
		AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= Convert(date,@startDate,103)
		AND (AS_Status.Title <> 'Cancelled'
		AND APPOINTMENTSTATUS <> 'Complete')		
		AND AS_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  @AvailableOperatives)
	UNION ALL
	SELECT
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,'' 																																			AS PostCode
		,ISNULL(P_SCHEME.SCHEMENAME, '') AS Address
		,''																																		AS TownCity
		,''																																			AS County
		,'Gas Appointment'																																				AS AppointmentType
		 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM
		AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P_SCHEME ON AS_JOURNAL.SchemeId = P_SCHEME.SCHEMEID
	WHERE
		AS_JOURNAL.IsCurrent = 1
		 AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= Convert(date,@startDate,103)
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
		AND AS_APPOINTMENTS.ASSIGNEDTO IN
		(
			SELECT
				employeeid
			FROM
				@AvailableOperatives
		)
	UNION ALL
	SELECT
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,P_BLOCK.POSTCODE																																			AS PostCode
		,ISNULL(P_BLOCK.BLOCKNAME, '') + ' ' + ISNULL(P_BLOCK.ADDRESS1, '') + ' ' + ISNULL(P_BLOCK.ADDRESS2, '') + ' ' + ISNULL(P_BLOCK.ADDRESS3, '')	AS Address
		,P_BLOCK.TownCity																																			AS TownCity
		,P_BLOCK.County																																				AS County
		,'Gas Appointment'																																				AS AppointmentType
		 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM
		AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P_BLOCK ON AS_JOURNAL.BLOCKID = P_BLOCK.BLOCKID
	WHERE
		AS_JOURNAL.IsCurrent = 1
		 AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= Convert(date,@startDate,103)
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
		AND AS_APPOINTMENTS.ASSIGNEDTO IN
		(
			SELECT
				employeeid
			FROM
				@AvailableOperatives
		) 		
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		
		,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.POSTCODE, ' ')
		when PLANNED_JOURNAL.BlockId > 0 then ISNULL(P_BLOCK.POSTCODE,' ')     
		when PLANNED_JOURNAL.SchemeId > 0 then ' '
  end                          AS PostCode  
 ,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS2, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS3, '')  
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(P_BLOCK.ADDRESS2, '') + ', '  
          + ISNULL(P_BLOCK.ADDRESS3, '')  
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  END AS Address

,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.TownCity, '')
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.TownCity, '')  
          
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ''
          
      END																																	AS TownCity																																			
		,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.County, ' ')
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.County, ' ')  
          
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ' '
          
      END AS County
		,'Planned Appointment' as AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME,LOGGEDDATE)) AS CreationDate
	FROM 
		PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN PLANNED_STATUS ON PLANNED_STATUS.STATUSID = PLANNED_JOURNAL.STATUSID
		left JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
		left join P_SCHEME on  PLANNED_JOURNAL.SchemeId = P_SCHEME.SCHEMEID
		left join P_BLOCK on PLANNED_JOURNAL.BlockId = P_BLOCK.BLOCKID 
		
	WHERE
		((Convert(date,APPOINTMENTDATE,103) >= Convert(date,@startDate,103)
		OR Convert(date,APPOINTMENTENDDATE,103) >= Convert(date,@startDate,103)))
		--AND (Convert(date,APPOINTMENTDATE,103) >= Convert(date,@startDate,103)
		--OR Convert(date,APPOINTMENTEndDATE,103) <= Convert(date,@startDate,103)))

		AND (PLANNED_STATUS.TITLE <> 'Cancelled'
		AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled'))	
		AND PLANNED_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  @AvailableOperatives)

	--Trainings
	UNION ALL 
	SELECT 
		EET.STARTDATE as AppointmentStartDate
		,EET.ENDDATE as AppointmentEndDate
		,EET.EMPLOYEEID  as OperativeId
		,'00:00 AM' as StartTime
		,'11:59 PM' as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), EET.STARTDATE,103) + ' ' + '00:00 AM' ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), EET.ENDDATE,103) + ' ' + '11:59 PM',103)) as EndTimeInSec
		,ISNULL(EET.POSTCODE, ' ')  AS PostCode  
		,ISNULL(EET.LOCATION, '')  AS Address
		,ISNULL(EET.VENUE, '') AS TownCity																																			
		,'' AS County
		,'Training' as AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME,EET.CREATEDDATE)) AS CreationDate
	FROM 
	    E_EmployeeTrainings EET	
	WHERE
		((Convert(date,EET.STARTDATE,103) <= Convert(date,@startDate,103)
		AND Convert(date,EET.ENDDATE,103) >= Convert(date,@startDate,103)))
		AND EET.Active=1 AND EET.Status in (select StatusId from E_EmployeeTrainingStatus where Title = 'Requested' OR Title = 'Manager Supported' OR Title = 'Exec Supported' OR Title = 'Exec Approved' OR Title = 'HR Approved')	
		AND EET.EMPLOYEEID in (SELECT employeeid FROM  @AvailableOperatives)	
	-----------------------------------------------------------------------------------------------------------------------
	--M&E Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTSTARTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTSTARTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,P__PROPERTY.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,ISNULL(P__PROPERTY.TownCity,'') as TownCity    
		,ISNULL(P__PROPERTY.County,'') as County
		,PDR_MSATType.MSATTypeName +' Appointment' as AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM 
		PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId 
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN PDR_MSATType on PDR_MSAT.MSATTypeId=PDR_MSATType.MSATTypeId
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
	WHERE 
		((Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)
		OR Convert(date,APPOINTMENTENDDATE,103) >= Convert(date,@startDate,103)))
		--AND (Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)
		--OR Convert(date,APPOINTMENTEndDATE,103) <= Convert(date,@startDate,103)))


		AND (PDR_STATUS.TITLE <> 'Cancelled'
		AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled'))		
		AND PDR_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  @AvailableOperatives)
END
