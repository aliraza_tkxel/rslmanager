USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetJobSheetSummaryDetails]    Script Date: 04/10/2015 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[JSS_GetPlannedJobSheetSummaryDetails]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[JSS_GetPlannedJobSheetSummaryDetails] AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[JSS_GetPlannedJobSheetSummaryDetails]
	-- Add the parameters for the stored procedure here
	@JobSheetNumber nvarchar(20)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
DECLARE @propertyId VARCHAR(50)
DECLARE @schemeId int
DECLARE @blockId int 
-- Insert statements for procedure here
--=============================================================
-- Appointment Info Details
--============================================================
SELECT DISTINCT
	'JSN' +RIGHT('0000'+ CONVERT(VARCHAR,PA.APPOINTMENTID),4)	AS JSN
	,ISNULL(PAH.APPOINTMENTDATE, ' ')							AS StartDate
	,ISNULL(PAH.APPOINTMENTENDDATE, ' ')						AS EndDate
	,COALESCE(SS.TITLE, PAH.APPOINTMENTSTATUS, ' ')				AS InterimStatus
	,PA.JournalId												AS PMO
	,CASE WHEN PJH.PROPERTYID is not null then  PJH.PROPERTYID
	when PJH.BlockId > 0 THEN  convert(nvarchar(max),PJH.BlockId)
	when PJH.SchemeId > 0 THEN convert(nvarchar(max),PJH.SchemeId)
	end as PropertyId 
   ,CASE WHEN PJH.PROPERTYID is not null then  'Property'
	when PJH.BlockId > 0 THEN  'Block'
	when PJH.SchemeId > 0 THEN 'Scheme'
	end as Type  
	,PA.JOURNALHISTORYID										AS JOURNALHISTORYID
	,PAH.COMPTRADEID											AS COMPTRADEID
	,ISNULL(PC.COMPONENTNAME, ' ')								AS Component
	,COALESCE(PLANNED_TRADE.Description, MISC_TRADE.Description,' ')									AS Trade
	,PAt.Planned_Appointment_Type								AS AppointmentType
	,COALESCE(PA.Duration,PMT.DURATION, ' ')									AS Duration
	,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, ' ')				AS Operative
	,PAH.isMiscAppointment										AS isMiscAppointment
	,ISNULL(PAH.CUSTOMERNOTES, ' ')								AS CustomerNotes
	,Case When PAH.APPOINTMENTSTATUS = 'Complete' Then PAH.CompletionNotes
	Else 	COALESCE(JSNotes.JobSheetNotes, PAH.APPOINTMENTNOTES, ' ') END	AS JobSheetNotes
	,ISNULL(PAH.APPOINTMENTSTARTTIME, ' ')						AS StartTime
	,ISNULL(PAH.APPOINTMENTENDTIME, ' ')						AS EndTime
	,ROW_NUMBER() OVER (ORDER BY PA.APPOINTMENTID)				AS Row
FROM
	PLANNED_JOURNAL PJH
	INNER JOIN PLANNED_APPOINTMENTS PA 
		ON PJH.JOURNALID = PA.JournalId
	INNER JOIN PLANNED_APPOINTMENTS_HISTORY PAH
		ON PAH.APPOINTMENTID = PA.APPOINTMENTID
	INNER JOIN
	(
		SELECT
			MAX(APPOINTMENTHISTORYID)	AS [MAXAPPOINTMENTHISTORYID]
			,APPOINTMENTID
		FROM
			PLANNED_APPOINTMENTS_HISTORY
		GROUP BY
			APPOINTMENTID
	) MPAH
		ON MPAH.MAXAPPOINTMENTHISTORYID = PAH.APPOINTMENTHISTORYID
	LEFT JOIN PLANNED_COMPONENT_TRADE PCT
		ON PCT.COMPTRADEID = PAH.COMPTRADEID
	LEFT JOIN PLANNED_COMPONENT PC
		ON PC.COMPONENTID = PCT.COMPONENTID
	LEFT JOIN G_TRADE PLANNED_TRADE
		ON PLANNED_TRADE.TradeId = PCT.TRADEID
	LEFT JOIN PLANNED_MISC_TRADE PMT
		ON PMT.AppointmentId = PA.APPOINTMENTID
	LEFT JOIN G_TRADE MISC_TRADE
		ON MISC_TRADE.TradeId = PMT.TRADEID
		
	LEFT JOIN Planned_Appointment_Type PAT
		ON PAT.Planned_Appointment_TypeId = PA.Planned_Appointment_TypeId
	LEFT JOIN PLANNED_SUBSTATUS SS
		ON SS.SUBSTATUSID = PAH.JOURNALSUBSTATUS
	LEFT JOIN E__EMPLOYEE E
		ON E.EMPLOYEEID = PAH.ASSIGNEDTO
	LEFT JOIN
	(
		SELECT
			*
		FROM
			(
				SELECT
					APPOINTMENTHISTORYID				AS APPOINTMENTHISTORYID
					,APPOINTMENTID						AS APPOINTMENTID
					,APPOINTMENTNOTES					AS JobSheetNotes
					,ROW_NUMBER() OVER
					(PARTITION BY APPOINTMENTID
					ORDER BY APPOINTMENTHISTORYID DESC)	AS RN
				FROM
					PLANNED_APPOINTMENTS_HISTORY
			) Results
		WHERE
			RN = 2
	) JSNotes
		ON JSNotes.APPOINTMENTID = PAH.APPOINTMENTID

WHERE
	@JobSheetNumber = 'JSN' +RIGHT('0000'+ CONVERT(VARCHAR,PA.APPOINTMENTID),4)


SELECT
	@propertyId = j.PROPERTYID, @schemeId=j.SchemeId, @blockId=j.BlockId
FROM PLANNED_APPOINTMENTS Ap
	INNER JOIN PLANNED_JOURNAL j ON ap.JournalId = j.JOURNALID
WHERE
	@JobSheetNumber = 'JSN' +RIGHT('0000'+ CONVERT(VARCHAR,Ap.APPOINTMENTID),4)
--==================================================================
-- Property & Customer Info
--==================================================================
if(@propertyId is not null)
BEGIN
	EXEC PLANNED_GetPropertyDetail @propertyId  
END  
IF(@blockId > 0)
BEGIN
	EXEC PLANNED_GetBlockDetail @blockId  
END
ELSE IF(@schemeId > 0) 
BEGIN
	EXEC PLANNED_GetSchemeDetail @schemeId  
END 

END