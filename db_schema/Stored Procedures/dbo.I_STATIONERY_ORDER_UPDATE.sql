SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_STATIONERY_ORDER_UPDATE]
(
	@DeliveryLocation nvarchar(300),
	@Description nvarchar(50),
	@OrderId int,
	@RequestedForEmpId int
)
AS
SET NOCOUNT OFF;
UPDATE    I_STATIONERY_ORDER
SET       DeliveryLocation = @DeliveryLocation, Description = @Description, RequestedForEmpId = @RequestedForEmpId
WHERE     (OrderId = @OrderId)

GO
