SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.FL_GET_FAULT_STATUS_FOR_FAULT_LOG
	/* ===========================================================================
 '   NAME:           FL_GET_FAULT_STATUS_FOR_FAULT_LOG
 '   DATE CREATED:   19 Nov 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get fault status from FL_FAULT_STATUS table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT FaultStatusID AS id,Description AS val
	FROM FL_FAULT_STATUS WHERE Description <> 'Repair Reported'
GO
