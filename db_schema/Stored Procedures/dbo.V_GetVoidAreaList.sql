USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Junaid Nadeem
-- Create date:      19/09/2016
-- Description:		 got Area list to populate dropdown list 
-- =============================================
IF OBJECT_ID('dbo.V_GetVoidAreaList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.V_GetVoidAreaList AS SET NOCOUNT ON;') 
GO  

Alter PROCEDURE [dbo].[V_GetVoidAreaList] 
	
AS
BEGIN
	Select AreaId, AreaName from FL_Area
END
