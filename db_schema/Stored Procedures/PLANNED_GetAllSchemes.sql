USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC PLANNED_GetAllSchemes
-- Author:		<Ahmed Mehmood>
-- Create date: <21/10/2013>
-- Description:	<Returns All Schemes>
-- Webpage: ReplacementList.aspx
-- =============================================


IF OBJECT_ID('dbo.[PLANNED_GetAllSchemes]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetAllSchemes] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PLANNED_GetAllSchemes] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN



SELECT DISTINCT
	P_SCHEME.SCHEMEID AS DevelopmentId,
	SCHEMENAME AS SchemeName
FROM P_SCHEME
INNER JOIN P__Property
	ON P_SCHEME.SCHEMEID = P__Property.SchemeId
WHERE P__PROPERTY.STATUS NOT IN (SELECT
	STATUSID
FROM P_STATUS
WHERE [DESCRIPTION] IN (
'transfer',
'sold'))
ORDER BY SchemeName ASC

END