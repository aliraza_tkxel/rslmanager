SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO













CREATE PROCEDURE dbo.FL_USERTBA_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_USERTBA_GETLOOKUP
 '   DATE CREATED:   2 JAN 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get User Id from E__Employee table which will be shown as lookup value in presentation pages
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT     E__EMPLOYEE.EMPLOYEEID as id, E__EMPLOYEE.FIRSTNAME+" "+E__EMPLOYEE.LASTNAME as val
FROM         E__EMPLOYEE INNER JOIN
                      E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID INNER JOIN
                      E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID
	ORDER BY FIRSTNAME ASC










GO
