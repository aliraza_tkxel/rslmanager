USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_GET_FISCALYEAR]    Script Date: 24/01/2018 8:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Umair Naeem
-- Create date: 07 July 2010
-- Description:	This procedure gets the 
--				list of Fiscal Year
-- =============================================
--
-- EXEC [F_GET_FISCALYEAR] @YRANGE=11
--

IF OBJECT_ID('dbo.F_GET_FISCALYEAR') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_GET_FISCALYEAR AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[F_GET_FISCALYEAR]
	-- Add the parameters for the stored procedure here
	@YRANGE INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT YRANGE,YSTART,YEND,CONVERT(VARCHAR,YStart,103)+' - '+CONVERT(VARCHAR,YEnd,103) AS PERIOD, CONVERT(VARCHAR,YEAR(YStart))+'/'+ SUBSTRING(CONVERT(VARCHAR,YEAR(YEnd)),3,2) AS YEARSHORT
	FROM dbo.F_FISCALYEARS WHERE YRange=COALESCE(@YRANGE, YRange)
	Order By YRANGE DESC
	
	SET NOCOUNT OFF;
END 





















