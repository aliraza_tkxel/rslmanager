USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_PopulateProvisionChargePropertyList]    Script Date: 10/29/2018 12:42:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_PopulateProvisionChargePropertyList]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_PopulateProvisionChargePropertyList] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_PopulateProvisionChargePropertyList] 
( 
	@schemeId		 INT=null, 
	@blockId		 INT=null,  
	@itemId			INT=null,  
	@ItemDetail		AS PDR_ServiceChargePropertyList readonly
)
AS
BEGIN

	DECLARE @isIncluded	INT,   
            @propertyId	NVARCHAR(MAX),
			@serviceListId INT=0

	Delete FROM PDR_ProvisionChargeProperties
	WHERE 
	itemId = @itemId	 
	INSERT INTO PDR_ProvisionChargeProperties(SchemeId, BlockId, PropertyId, IsIncluded, itemId,ISActive)
	Select @schemeId,@blockId ,PropertyId,IsIncluded,@itemId,1 from @ItemDetail
	
END
