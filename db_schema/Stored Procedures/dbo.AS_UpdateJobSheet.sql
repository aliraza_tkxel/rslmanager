USE [RSLBHALive ]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'AS_UpdateJobSheet'))
  DROP PROCEDURE AS_UpdateJobSheet
  

GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 22/2/2016
-- Description:	Update JobSheet details
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdateJobSheet]
	-- Add the parameters for the stored procedure here
	@journalId INT,
	@workRequire NVARCHAR(MAX),
	@OrderId INT,
	@userID INT,
	@purchaseOrderItemId INT,
	@Result INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @PROPERTYID NVARCHAR(MAX)
		SELECT @PROPERTYID = AS_JOURNAL.propertyId
		  FROM AS_JOURNAL 
		 WHERE AS_JOURNAL.JOURNALID = @journalId
		 
		DECLARE @statusId INT 
		  SELECT @statusId=statusId FROM AS_Status WHERE Title = 'Assign to Contractor'
		 
		UPDATE AS_CONTRACTOR_WORK_DETAIL SET WorkRequired = @workRequire WHERE PurchaseOrderItemId= @purchaseOrderItemId
		UPDATE F_PURCHASEITEM SET ITEMDESC = @workRequire WHERE ORDERITEMID= @purchaseOrderItemId
		SET @RESULT = 1
			
END
