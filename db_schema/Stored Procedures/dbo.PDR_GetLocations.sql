-- =============================================
-- Exec AS_Locations
-- Author:		Ali Raza
-- Create date: <02/10/2015>
-- Description:	<Get All locations for Attributes Tree>
-- =============================================
IF OBJECT_ID('dbo.PDR_GetLocations') IS NULL 
EXEC('CREATE PROCEDURE dbo.PDR_GetLocations AS SET NOCOUNT ON;') 
GO
Alter PROCEDURE [dbo].[PDR_GetLocations]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
		Select AreaID as LocationID, AreaName as LocationName, 'Area' as Location  from PA_AREA 
		where IsForSchemeBlock = 1 AND isActive = 1
	UNION ALL
		SELECT LocationID as LocationID,LocationName as LocationName, 'Location'as Location    	From PA_LOCATION 
	UNION ALL
		SELECT itemID as LocationID,itemName as LocationName, 'Item'as Location    	From PA_item
		where itemName in ('Electrics','Meters') AND isActive = 1 and  IsForSchemeBlock = 1
END
