USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveAsbestosInformation]    Script Date: 10/17/2017 15:03:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[AS_SaveAsbestosInformation]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_SaveAsbestosInformation] AS SET NOCOUNT ON;') 
GO  
-- Exec AS_SaveAsbestosInformation   
-- Author:  <Aqib Javed>  
-- Create date: <25-4-2014>  
-- Description: <This store procedure will save the information of Asbestos and Risk>  
-- Web Page: AsbestosTab.ascx  
-- =============================================  
ALTER  PROCEDURE [dbo].[AS_SaveAsbestosInformation]  
 @asbestosLevelId int,  
 @asbestosElementId int,  
 @riskId varchar(50),  
 @addedDate date,  
 @removedDate varchar(50),  
 @notes varchar(250),  
 @propertyId varchar(100),
 @riskLevel int=null,
 @UrgentActionRequired bit,  
 @userId int  
AS  
BEGIN 

if @removedDate IS NULL OR @removedDate = ''
Begin
Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,PROPERTYID,DateAdded,UserID,Notes,RiskLevelId,IsUrgentActionRequired)  
            Values(@asbestosLevelId,@asbestosElementId,@propertyId,@AddedDate,@userId,@Notes,@riskLevel,@UrgentActionRequired)  
END
Else
BEGIN
Insert into P_PROPERTY_ASBESTOS_RISKLEVEL(ASBRISKLEVELID,ASBESTOSID,PROPERTYID,DateAdded,DateRemoved,UserID,Notes,RiskLevelId,IsUrgentActionRequired)  
            Values(@asbestosLevelId,@asbestosElementId,@propertyId,@AddedDate,@RemovedDate,@userId,@Notes,@riskLevel,@UrgentActionRequired)  
            
END
INSERT INTO P_PROPERTY_ASBESTOS_RISK(PROPASBLEVELID,ASBRISKID)  
VALUES(@@IDENTITY,@riskId) 

          
END 