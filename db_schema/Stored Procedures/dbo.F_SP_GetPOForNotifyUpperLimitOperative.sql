USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_SP_GetPOForNotifyUpperLimitOperative]    Script Date: 09/03/2017 12:10:50 ******/
-- exec [F_SP_GetPOForNotifyUpperLimitOperative] -- orderid = 294919
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:<Saud Ahmed>
-- Create date: <09-03-2017>
-- Description:	<get all PO which status is 0 (Queued) and pending for being ordered after 2 days of PO created>
-- =============================================
IF OBJECT_ID('dbo.F_SP_GetPOForNotifyUpperLimitOperative') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_SP_GetPOForNotifyUpperLimitOperative AS SET NOCOUNT ON;') 
GO
Alter PROCEDURE [dbo].[F_SP_GetPOForNotifyUpperLimitOperative]
AS
BEGIN
	DECLARE @Date AS DATE = GETDATE() 
	DECLARE @SendToEmployee as varchar(50)= '',
		@SendToEmail as varchar(20)= '',
		@PONumber as varchar(20)= '',
		@Supplier as varchar(50)= '',
		@ITEMNAME as varchar(100)= '',
		@ITEMDESC as varchar(500)= '',
		@GROSSCOST as varchar(50)= '',
		@Status as varchar(20)= '',
		@RaisedBy as varchar(50)= '',
		@SendToEmpId as int 

	create table #temp_SendEmail
	(
		SendToEmployee varchar(100),
		SendToEmail varchar(50) null,
		PONumber varchar(20) null,
		Supplier varchar(100) null,
		ITEMNAME varchar(100) null,
		ITEMDESC varchar(max) null,
		GROSSCOST varchar(100) null,
		Status varchar(25) null,
		RaisedBy varchar(50) null,
		SendToEmpId int null,
		ORDERITEMID int null
	)

	DECLARE EmpId_CURSOR 
	 
	CURSOR FOR 
	-- start select
	SELECT  Distinct EL.EMPLOYEEID
	FROM F_PURCHASEITEM PI 
		INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID
		INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID 
		INNER JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = EX.EXPENDITUREID
	WHERE PO.ACTIVE = 1 AND PO.POSTATUS = 0  
	
	-- end select
	OPEN EmpId_CURSOR 

	FETCH NEXT FROM EmpId_CURSOR 
	INTO @SendToEmpId

    -- Check @@FETCH_STATUS to see if there are any more rows to fetch. 
    WHILE @@FETCH_STATUS = 0 
    BEGIN 

	INSERT INTO #temp_SendEmail (SendToEmployee, SendToEmail, PONumber, Supplier, ITEMNAME, ITEMDESC, GROSSCOST, Status, RaisedBy, SendToEmpId, ORDERITEMID)
		-- start select
	SELECT  E.FIRSTNAME +' '+ E.LASTNAME as SendToEmployee, EC.WORKEMAIL as SendToEmail,
	 'PO'+ CONVERT(varchar(10),PO.ORDERID) as PONumber,
	 S.NAME as Supplier,
	 PI.ITEMNAME , 
	 PI.ITEMDESC, PI.GROSSCOST, 'Queued' as Status,
	 RE.FIRSTNAME+' '+ RE.LASTNAME as RaisedBy, E.EMPLOYEEID, PI.ORDERITEMID
	FROM F_PURCHASEITEM PI 
		INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID
		INNER JOIN E__EMPLOYEE RE ON RE.EMPLOYEEID = PO.USERID
		INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID 
		JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID AND EL.EMPLOYEEID = @SendToEmpId and 
			( 
				el.LIMIT > = PI.GROSSCOST AND 
				( 
					El.LIMIT >= PI.GROSSCOST And
					(select isnull(Max(LIMIT), 0) from F_EMPLOYEELIMITS where  
						EXPENDITUREID =  PI.EXPENDITUREID and LIMIT <  
							(select LIMIT from F_EMPLOYEELIMITS where  
								EXPENDITUREID =  PI.EXPENDITUREID and EMPLOYEEID = @SendToEmpId)) < Pi.GROSSCOST 
				)  
				OR 
				( El.LIMIT >= PI.GROSSCOST AND  Pi.GROSSCOST > 
					    (select Isnull(( 
						    select LIMIT  from 
						        F_EMPLOYEELIMITS where  
							        EXPENDITUREID =   PI.EXPENDITUREID and LIMIT <=  
								        (select LIMIT from F_EMPLOYEELIMITS where  
									        EXPENDITUREID =   PI.EXPENDITUREID and EMPLOYEEID = @SendToEmpId) 
						        group by LIMIT  
						        order by Limit desc  
						        OFFSET (datediff(DD, PI.PIDATE, getDate())/3 +1) ROWS FETCH NEXT (1) ROWS ONLY), 0) as LIMIT) -- assign upper limit budget holder after (3) days
				)
				ANd PI.ORDERITEMID NOt IN (
				select PII.ORDERITEMID from F_PURCHASEITEM PII where 
					el.LIMIT > = PI.GROSSCOST AND
					PII.GROSSCOST >= 
						(select Max(LIMIT) from F_EMPLOYEELIMITS where 
							EXPENDITUREID = PII.EXPENDITUREID and LIMIT <
								(select LIMIT from F_EMPLOYEELIMITS where 
									EXPENDITUREID = PII.EXPENDITUREID and EMPLOYEEID = @SendToEmpId )) 
				)
			) 
		Inner Join E__EMPLOYEE E ON E.EMPLOYEEID = EL.EMPLOYEEID
		Inner JOIN E_CONTACT EC ON EC.EMPLOYEEID = E.EMPLOYEEID
		INNER JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID
	WHERE PI.ACTIVE = 1 AND PISTATUS = 0 AND PI.ACTIVE = 1 AND PISTATUS = 0 
	ORDER BY ORDERITEMID ASC
	-- end select
		FETCH NEXT FROM EmpId_CURSOR INTO @SendToEmpId
    END 

	CLOSE EmpId_CURSOR  
	DEALLOCATE EmpId_CURSOR 

 
	DECLARE QueuedPO_CURSOR 
	 
	CURSOR FOR 
	-- start select
	select Distinct SE.SendToEmployee, SE.SendToEmail, SE.PONumber, SE.Supplier,
	 SE.ITEMNAME, SE.ITEMDESC, SE.GROSSCOST, SE.Status, SE.RaisedBy 
	FROM #temp_SendEmail SE
	LEFT JOIN  F_PO_SendEmailNotification SEN ON SEN.ORDERITEMID = SE.ORDERITEMID and SEN.SendToEmpId = SE.SendToEmpId
	WHERE SEN.SendToEmpId is null ANd SEN.ORDERITEMID IS NULL 
	-- end select
	OPEN QueuedPO_CURSOR 

	FETCH NEXT FROM QueuedPO_CURSOR 
	INTO @SendToEmployee, @SendToEmail, @PONumber, @Supplier, @ITEMNAME, @ITEMDESC,
		@GROSSCOST, @Status, @RaisedBy

    -- Check @@FETCH_STATUS to see if there are any more rows to fetch. 
    WHILE @@FETCH_STATUS = 0 
    BEGIN 
		declare @emailBody nvarchar(max) = ''
		-- compose an email 

	 	set @emailBody = '<!DOCTYPE html><html><head><title>Queued Purchase Order</title></head>' +
						 '<body>'+
                         '               <style type="text/css">'+
                         '                   .topAllignedCell{vertical-align: top;} '+
                         '                   .bottomAllignedCell{vertical-align: bottom;} '+
                         '               </style> '+
                         '              <span>Dear'+ @SendToEmployee+'</span> '+
                         '               <p>A Purchase Order has been queued and requires your approval, the details are as follows:</p> '+
                         '               <table><tbody> '+
                         '                       <tr><td>PO Number:</td><td>'+@PONumber+'</td></tr> '+
                         '                        <tr><td>Supplier Name:</td><td>'+@Supplier+'</td></tr> '+
                         '                       <tr><td>Item Name:</td><td>'+@ITEMNAME+'</td></tr> '+
                         '                       <tr><td class="topAllignedCell">Notes:</td><td>'+@ITEMDESC+'</td></tr> '+
                         '                       <tr><td>&nbsp;</td><td>&nbsp;</td></tr> '+
                         '                       <tr><td>Gross(�):</td><td>'+@GROSSCOST+'</td></tr> '+
                         '                       <tr><td>Status:</td><td>'+@Status+'</td></tr> '+
                         '                       <tr><td>Raised by:</td><td>'+@RaisedBy+'</td></tr> '+
                         '               </tbody></table><br /><br /> '+
                         '               <table><tbody><tr><td><img src="cid:broadlandImage" alt="Broadland Housing Group" /></td> '+
                         '                           <td class="bottomAllignedCell"> '+
                         '                               Broadland Housing Group<br /> '+
                         '                               NCFC, The Jarrold Stand<br /> '+
                         '                               Carrow Road, Norwich, NR1 1HU<br /> '+
                         '                           </td> '+
                         '                       </tr></tbody></table></body></html>'



		-- send email 
		EXEC msdb.dbo.sp_send_dbmail  

		@profile_name = 'EMailProfilePO',  
		@recipients = @SendToEmail,  
		@body = @emailBody,  
		@body_format = 'HTML',
		@subject = 'Queued Purchase Order' ;  	

		FETCH NEXT FROM QueuedPO_CURSOR INTO @SendToEmployee,@SendToEmail, @PONumber, @Supplier, @ITEMNAME, @ITEMDESC,
			@GROSSCOST, @Status, @RaisedBy
    END 
 CLOSE QueuedPO_CURSOR  
 DEALLOCATE QueuedPO_CURSOR 

 Insert Into F_PO_SendEmailNotification (SendToEmployee, SendToEmail, PONumber, Supplier, ITEMNAME, ITEMDESC, GROSSCOST, Status, RaisedBy, SendToEmpId, ORDERITEMID )

 select Distinct SE.SendToEmployee, SE.SendToEmail, SE.PONumber, SE.Supplier,
	 SE.ITEMNAME, SE.ITEMDESC, SE.GROSSCOST, SE.Status, SE.RaisedBy, SE.SendToEmpId, SE.ORDERITEMID
	FROM #temp_SendEmail SE
	LEFT JOIN  F_PO_SendEmailNotification SEN ON SEN.ORDERITEMID = SE.ORDERITEMID and SEN.SendToEmpId = SE.SendToEmpId
	WHERE SEN.SendToEmpId is null ANd SEN.ORDERITEMID IS NULL 


 DROP TABLE #temp_SendEmail

END






