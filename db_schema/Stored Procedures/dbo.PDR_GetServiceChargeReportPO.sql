USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetServiceChargeReport]    Script Date: 23/10/2018 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetServiceChargeReportPO') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetServiceChargeReportPO AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeReportPO]
		@filterSchemeId int,
		@filterBlockId int,
	-- Add the parameters for the stored procedure here
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ITEMID', 
		@sortOrder varchar (5) = 'ASC',
		@fiscalYear int,
		@totalCount int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	DECLARE 
	
		@SelectClause NVARCHAR(MAX),
        @fromClause   NVARCHAR(MAX),
        @whereClause  NVARCHAR(MAX),	        
        @orderClause  NVARCHAR(MAX),	
        @mainSelectQuery NVARCHAR(MAX),        
        @rowNumberQuery NVARCHAR(MAX),
        @finalQuery NVARCHAR(MAX),
		@YStart NVARCHAR(100),
		@YEnd NVARCHAR(100),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria  Nvarchar(MAX),
		
		--variables for paging
        @offset INT,
		@limit INT
		
		SELECT	@YStart=YStart,@YEnd=YEnd 
		FROM	F_FISCALYEARS 
		WHERE	YRange = @fiscalYear
		
		SET @searchCriteria = ' FPO.IsServiceChargePO = 1 AND  FPO.PODATE BETWEEN CONVERT(DATETIME, ''' + @YStart +''' , 120) AND CONVERT(DATETIME, ''' + @YEnd +''' , 120)
				AND #ItemPropertyInfo.isItemIncluded = 1 '
  
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1


		--=========================================================================================================

		DECLARE @allPropertiesCount INT , @itemIncPropertyCount INT , @itemExcPropertyCount INT
		, @itemSchemeCount INT, @minSchemeId INT, @blockProperties INT, @schemeProperties INT, @orderItemId INT
		, @SchemeId INT, @BlockId INT, @PropertyId NVARCHAR(40), @BlockScheme INT, @isItemIncluded BIT

		SELECT	@allPropertiesCount = COUNT(*) 
		FROM	P__PROPERTY 
		WHERE	SCHEMEID != -1 OR SCHEMEID IS NOT NULL

		CREATE TABLE #ItemPropertyInfo
		(
			orderItemId INT,
			propertyIncluded INT, 
			propertyExcluded INT,
			schemeCount INT,
			isItemIncluded BIT
		)

		DECLARE PurchaseItem_Cursor CURSOR FOR
		SELECT	ORDERITEMID
		FROM	F_PURCHASEORDER FPO
				INNER JOIN F_PURCHASEITEM FPI ON FPO.ORDERID = FPI.ORDERID
		WHERE	FPO.IsServiceChargePO = 1 
				AND  FPO.PODATE BETWEEN CONVERT(DATETIME, @YStart , 120) and CONVERT(DATETIME, @YEnd , 120)

		OPEN PurchaseItem_Cursor

		FETCH NEXT FROM PurchaseItem_Cursor
		INTO @orderItemId

		WHILE @@FETCH_STATUS = 0
		BEGIN

			SET @itemIncPropertyCount = 0
			SET @itemExcPropertyCount = 0
			SET @itemSchemeCount = 0
			SET @isItemIncluded = 0
			
			--===========================================
			-- CALCULATE TOTAL EXCLUDED PROPERTIES
			--===========================================
			SELECT	@itemExcPropertyCount = COUNT(PROPERTYID) 
			FROM	F_ServiceChargeExProperties 
			WHERE	PurchaseItemId = @orderitemid

			--===========================================
			-- CALCULATE TOTAL INCLUDED PROPERTIES
			--===========================================
	
			DECLARE PropertyCount_Cursor CURSOR FOR
			SELECT	SchemeId, BlockId, PropertyId
			FROM	F_PurchaseItemSCInfo
			WHERE   OrderItemId = @orderitemid AND IsActive = 1

			OPEN PropertyCount_Cursor
	

			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId

			WHILE @@FETCH_STATUS = 0
			BEGIN

				IF @PropertyId != '-1'
				BEGIN

					IF NOT EXISTS(  SELECT  1
									FROM	F_ServiceChargeExProperties 
									WHERE	PurchaseItemId = @orderitemid 
											AND PropertyId = @PropertyId)
					BEGIN
						SET @itemIncPropertyCount = @itemIncPropertyCount + 1
					END
			
				END
				ELSE IF @BlockId != -1 AND @BlockId IS NOT NULL
				BEGIN
					 SET @blockProperties = 0
					  SET @BlockScheme = 0

					 SELECT @blockProperties = COUNT(PROPERTYID) , @BlockScheme = MAX(SchemeId) 
					 FROM	P__PROPERTY 
					 WHERE  BLOCKID = @BlockId AND PropertyId NOT IN (SELECT PROPERTYID 
																	  FROM	F_ServiceChargeExProperties 
																	  WHERE	PurchaseItemId = @orderitemid)

					 SET @itemIncPropertyCount = @itemIncPropertyCount + @blockProperties
				END
				ELSE IF @SchemeId != -1
				BEGIN
					 SET @schemeProperties = 0

					 SELECT @schemeProperties = COUNT(PROPERTYID) 
					 FROM	P__PROPERTY 
					 WHERE  SCHEMEID = @SchemeId AND PropertyId NOT IN (SELECT  PROPERTYID 
																		FROM	F_ServiceChargeExProperties 
																		WHERE	PurchaseItemId = @orderitemid)

					 SET @itemIncPropertyCount = @itemIncPropertyCount + @schemeProperties
				END
				ELSE IF @SchemeId = -1
				BEGIN
						SELECT	@allPropertiesCount = COUNT(propertyId)
						FROM	P__PROPERTY 
						WHERE	SCHEMEID != -1 OR SCHEMEID IS NOT NULL 
								AND PropertyId NOT IN (	SELECT	PROPERTYID 
														FROM	F_ServiceChargeExProperties 
														WHERE	PurchaseItemId = @orderitemid)
			
						SET @itemIncPropertyCount = @itemIncPropertyCount + @allPropertiesCount

				END


			FETCH NEXT FROM PropertyCount_Cursor
			INTO @SchemeId, @BlockId, @PropertyId
			END

			CLOSE  PropertyCount_Cursor
			DEALLOCATE PropertyCount_Cursor


			--===========================================
			-- CALCULATE TOTAL NUMBER OF SCHEMES 
			--===========================================

			 SELECT	@minSchemeId = MIN(schemeid) 
			 FROM	F_PurchaseItemSCInfo 
			 WHERE	OrderItemId = @orderItemId 
					AND IsActive = 1

			 IF @minSchemeId = -1
				BEGIN
					
					SELECT	@itemSchemeCount = COUNT(*)
					FROM	P_SCHEME

					SET @isItemIncluded = 1

				END
			ELSE
				BEGIN

					SELECT	@itemSchemeCount = COUNT(DISTINCT SCHEMEID)
					FROM	F_PurchaseItemSCInfo
					WHERE	OrderItemId = @orderItemId AND IsActive = 1



					IF @filterSchemeId IS NOT NULL AND @filterSchemeId > 0
					BEGIN

						IF EXISTS (	SELECT	1
									FROM	F_PurchaseItemSCInfo
									WHERE	OrderItemId = @orderItemId AND IsActive = 1 AND SCHEMEID = @filterSchemeId)
						BEGIN
							SET @isItemIncluded = 1
						END
						ELSE
						BEGIN
							SET @isItemIncluded = 0
						END

					END
					ELSE
					BEGIN
						SET @isItemIncluded = 1
					END



					IF @filterBlockId IS NOT NULL AND @filterBlockId > 0
					BEGIN


						SELECT	@BlockScheme = SCHEMEID
						FROM	P_BLOCK
						WHERE	BLOCKID = @filterBlockId  


						IF EXISTS (	SELECT	1
									FROM	F_PurchaseItemSCInfo
									WHERE	OrderItemId = @orderItemId 
											AND IsActive = 1 
											AND (SCHEMEID = @BlockScheme AND (BLOCKID IS NULL OR BLOCKID = -1 OR BLOCKID = @filterBlockId)))
						BEGIN
							SET @isItemIncluded = 1
						END
						ELSE
						BEGIN
							SET @isItemIncluded = 0
						END

					END
					

				END

			INSERT INTO #ItemPropertyInfo (orderItemId, propertyIncluded, propertyExcluded, schemeCount,isItemIncluded)
			VALUES (@orderitemid, @itemIncPropertyCount, @itemExcPropertyCount, @itemSchemeCount,@isItemIncluded)


		FETCH NEXT FROM PurchaseItem_Cursor
		INTO @orderItemId

		END

		CLOSE PurchaseItem_Cursor
		DEALLOCATE PurchaseItem_Cursor


		--=========================================================================================================

		SET @SelectClause = 'SELECT TOP ('+CONVERT(NVARCHAR(10),@limit)+')
							 ''Multiple Schemes Selected'' AS SCHEMENAME
							, ''-'' AS BLOCKNAME
							, #ItemPropertyInfo.propertyIncluded AS InPropCount
							, FPI.ITEMNAME AS ITEMNAME
							, ''-'' AS BUDGET
							, ''-'' AS APPORTIONMENTBUDGET
							, CASE WHEN FPS.POSTATUSNAME = ''Reconciled'' THEN 
								''�''+ CONVERT(varchar,CONVERT(NUMERIC(38, 2), FPI.GROSSCOST/(#ItemPropertyInfo.schemeCount*1.0))) 
							  ELSE
									''-''
							  END AS ACTUALTOTAL			
							, CASE WHEN FPS.POSTATUSNAME = ''Reconciled'' THEN 
									''�''+ CONVERT(varchar,CONVERT(NUMERIC(38, 2), (FPI.GROSSCOST/(#ItemPropertyInfo.schemeCount*1.0))/(#ItemPropertyInfo.propertyIncluded*1.0)))
							  ELSE
									''-''
							  END AS APPORTIONMENTACTUAL								  														  	
							, ISNULL(#ItemPropertyInfo.propertyIncluded,0) as PROPERTYCOUNT
							, FPI.ORDERITEMID as ITEMID
							, #ItemPropertyInfo.propertyExcluded AS ExPropCount
							,NULL AS SCHEMEID
							,NULL AS BLOCKID
							,''PO ''+CONVERT(VARCHAR,FPO.ORDERID)  AS DETAILS
							,FPO.PODATE AS DateCreated
							, FPO.ORDERID AS Ref
							,FPO.IsServiceChargePO 
							'
				
		SET @fromClause = CHAR(10) +' FROM	F_PURCHASEITEM FPI
					INNER JOIN F_PURCHASEORDER FPO ON FPI.ORDERID = FPO.ORDERID
					INNER JOIN F_POSTATUS FPS ON FPO.POSTATUS = FPS.POSTATUSID
					INNER JOIN #ItemPropertyInfo ON FPI.ORDERITEMID = #ItemPropertyInfo.orderItemId 
					'
								   
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		SET @whereClause =	CHAR(10) + 'WHERE 1=1 AND ' + CHAR(10) + @searchCriteria 
		



		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		
		EXEC (@finalQuery)
		--print(@finalQuery)
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	

		DROP TABLE #ItemPropertyInfo
END
