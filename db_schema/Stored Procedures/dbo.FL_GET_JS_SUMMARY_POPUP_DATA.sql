
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FL_GET_JS_SUMMARY_POPUP_DATA]  
/* ===========================================================================
 '   NAME:           FL_GET_JS_SUMMARY_POPUP_DATA
 '   DATE CREATED:   17 feb 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To retrieve customer faults against specified JsNumber
 
 '   IN:             JSNumber
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
	@JSNumber varchar(10)
)
AS

/*, SO.NAME AS Contractor*/

	SELECT FL.FaultLogID, G_Title.[Description] as TitleDescription, FL.DueDate, PROP.HOUSENUMBER, PROP.POSTCODE, FL.CustomerId,CS.FIRSTNAME, CS.LASTNAME, CS.MIDDLENAME, PROP.ADDRESS1,PROP.TOWNCITY,PROP.COUNTY,AD.TEL,AD.MOBILE,AD.EMAIL, FL.FaultID, FL.SubmitDate AS OrderDate, FP.PriorityName, FA.Description, FA.PriorityID, FE.ElementName, FR.AreaName, FC.LocationName, JobSheetNumber AS JSNumber, FL.Quantity,  FL.ProblemDays ,CASE  FL.RecuringProblem WHEN 0 THEN 'This is not a recurring problem' ELSE 'This is a recurring problem' end   AS RecurringProblem,CASE  FL.CommunalProblem WHEN 0 THEN 'This is not in communal area' ELSE 'This is in communal area' end AS CommunalProblem, FL.Notes, FB.PreferredContactDetails,
	
	CASE SO.NAME WHEN NULL THEN 'N/A' ELSE SO.NAME END AS Contractor,
	
	(CONVERT(VARCHAR, FP.ResponseTime) + ' ' + CASE FP.Days WHEN 1 THEN  'Day(s)'  WHEN 0 THEN 'Hour(s)' END) AS FResponseTime,
	
	FP.ResponseTime As OnlyResponseTime,
	
	CASE FP.Days WHEN 1 THEN  'Day' WHEN 0 THEN 'Hour' END AS OnlyDayHour,
	
	CASE FA.Recharge WHEN 1 THEN CONVERT(VARCHAR,FA.Gross) WHEN 0 THEN 'N/A' END AS FRecharge
	, (	SELECT PARL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL
			FROM ( 
			SELECT 
								ISNULL(MAX(PAL.RISKLEVELID),0) AS MAX_RISKLEVELID
					  FROM      P__PROPERTY PP
								INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL PPARL ON PP.PROPERTYID = PPARL.PROPERTYID
								INNER JOIN P_PROPERTY_ASBESTOS_RISK PPAR ON PPAR.PROPASBLEVELID = PPARL.PROPASBLEVELID
								INNER JOIN P_ASBRISKLEVEL PAL ON PAL.ASBRISKLEVELID = PPARL.ASBRISKLEVELID
								WHERE PP.PROPERTYID = FJL.PROPERTYID
					) SUB ,
					(SELECT ASBRISKLEVELDESCRIPTION, RISKLEVELID FROM P_ASBRISKLEVEL UNION SELECT 'None', 0) AS PARL
			WHERE   SUB.MAX_RISKLEVELID = PARL.RISKLEVELID) AS AsbestosRisk		
		
	FROM FL_FAULT_LOG AS FL 	
	
	INNER JOIN FL_FAULT AS FA ON FL.FaultID = FA.FaultID
	
	INNER JOIN FL_ELEMENT AS FE ON FA.ElementID = FE.ElementID
	
	INNER JOIN FL_AREA AS FR ON FE.AreaID = FR.AreaID
	
	INNER JOIN FL_LOCATION AS FC ON FR.LocationID = FC.LocationID		
	
	INNER JOIN FL_FAULT_PRIORITY AS FP ON FA.PriorityID = FP.PriorityID
	
	INNER JOIN FL_FAULT_BASKET AS FB ON FL.FaultBasketID = FB.FaultBasketID
	
	INNER JOIN C__CUSTOMER AS CS ON FL.CustomerId=CS.CUSTOMERID
	
	INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID=CS.CUSTOMERID AND CT.ENDDATE IS NULL
	
	INNER JOIN C_TENANCY T ON T.TENANCYID=CT.TENANCYID AND T.ENDDATE IS NULL
	
	INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID= T.PROPERTYID		

	LEFT JOIN G_TITLE ON CS.TITLE = G_TITLE.TITLEID	
	
	INNER JOIN C_ADDRESS AS AD ON CS.CUSTOMERID=AD.CUSTOMERID
	
	LEFT JOIN S_ORGANISATION AS SO ON FL.ORGID = SO.ORGID
	
	LEFT JOIN FL_FAULT_JOURNAL FJL ON FL.FaultLogId = FJL.FaultLogID
	
	WHERE (FL.JobSheetNumber = @JSNumber
	AND AD.ADDRESSTYPE=3	)

	ORDER BY FL.FaultLogID DESC
GO
