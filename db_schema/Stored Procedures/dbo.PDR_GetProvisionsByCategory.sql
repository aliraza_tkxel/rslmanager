USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetProvisions]    Script Date: 12/11/2018 12:57:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* =================================================================================    
    Author: Shaheen
    Date: Nov-18-2015

    Execution Command: Exec [PDR_GetProvisions]
  =================================================================================*/
IF OBJECT_ID('dbo.[PDR_GetProvisionsByCategory]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetProvisionsByCategory] AS SET NOCOUNT ON;') 
GO 

ALTER PROCEDURE [dbo].[PDR_GetProvisionsByCategory]
( 
 @categoryId INT=NULL,
 @schemeId INT=NULL,
 @blockId INT=NULL
 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--=================================================================            
--Get All Provisions 
--=================================================================            
	if @schemeId != -1
	BEGIN
		SELECT * FROM PDR_Provisions WHERE PDR_Provisions.IsActive = 1 
			AND PDR_Provisions.ProvisionCategoryId=@categoryId 
			AND PDR_Provisions.SchemeId = @schemeId 
	END
	if @blockId != -1
	BEGIN
		SELECT * FROM PDR_Provisions WHERE PDR_Provisions.IsActive = 1 
			AND PDR_Provisions.ProvisionCategoryId=@categoryId 
			AND PDR_Provisions.SchemeId = @schemeId 
	END
	
END





GO


