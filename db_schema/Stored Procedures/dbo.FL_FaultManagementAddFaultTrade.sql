SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_FaultManagementAddFaultTrade]
/* ===========================================================================
 --	EXEC FL_FaultManagementAddFaultTrade
		@FaultID = 1,
		@TradeID  = 1,
		@RESULT OUTPUT		
--  Author:			Aamir Waheed
--  DATE CREATED:	1 March 2013
--  Description:	To Add/Create a new fault Trade Relation with given FaultID and TradeID
--  Webpage:		View/Reports/ReportArea.aspx (For Add/Amend Fault Modal Popup)
 '==============================================================================*/

@FaultID INT,
@TradeID INT,

-- Output Parameters 
@RESULT  INT OUT

AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN


INSERT INTO FL_FAULT_TRADE
	(TradeId, FaultId) 	
	
VALUES 	(@TradeID, @FaultID) 

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	

SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:
   ROLLBACK TRAN  
  SET @RESULT=-1
RETURN













GO
