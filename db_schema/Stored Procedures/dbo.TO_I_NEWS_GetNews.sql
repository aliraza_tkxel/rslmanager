SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TO_I_NEWS_GetNews]
	/*	===============================================================
	'   NAME:           TO_I_NEWS_GetNews
	'   DATE CREATED:   18 MAY 2008
	'   CREATED BY:     Naveed Iqbal
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To retrieve the broadland news
	'   IN:             No
	'   OUT: 		    No     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	
AS

SELECT NewsID,NewsCategory,Headline,ImagePath,DateCreated,NewsContent,Rank,Active,IsHomePage,HomePageImage

FROM I_NEWS
WHERE IsTenantOnline=1
ORDER BY DateCreated DESC



GO
