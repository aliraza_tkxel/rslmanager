USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_AddTenantRecharges]    Script Date: 02-Dec-16 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Junaid Nadeem
-- Create date:      2/12/2016
-- Description:      Add tenant recharges               
-- =============================================
--EXEC	V_AddTenantRecharges
		--@worksRequiredId = 8,
		--@checkedValue = 0,
		--@tenantNeglect = 0


IF OBJECT_ID('dbo.V_AddTenantRecharges') IS NULL 
	EXEC('CREATE PROCEDURE dbo.V_AddTenantRecharges AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[V_AddTenantRecharges](
	@worksRequiredId int,
	@checkedValue bit,
	@tenantNeglect money,
	@isSaved int = 0 out 
	)
	AS
BEGIN

BEGIN TRANSACTION;    
BEGIN TRY   
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @TENANCYID int,
	@ITEMTYPE int,
	@ISDEBIT int,
	@STATUSID int,
	@PAYMENTSTARTDATE smalldatetime,
	@PAYMENTENDDATE smalldatetime

	SELECT @TENANCYID=PM.TenancyId, @PAYMENTSTARTDATE=PM.TerminationDate, @PAYMENTENDDATE=PM.ReletDate
	FROM V_RequiredWorks RW JOIN pdr_journal PJ ON RW.inspectionjournalid = PJ.journalid
	JOIN pdr_msat PM ON PJ.msatid = PM.msatid WHERE RW.RequiredWorksId = @worksRequiredId

	SELECT @ITEMTYPE=itemTypeId FROM F_ITEMTYPE WHERE DESCRIPTION = 'Recharges'

	IF(@checkedValue = 0)
		BEGIN 
			SELECT @STATUSID=transactionStatusId  FROM F_TRANSACTIONSTATUS WHERE DESCRIPTION = 'Due'
			set @ISDEBIT = 1
		END
	ELSE
		BEGIN
			SELECT @STATUSID=transactionStatusId  FROM F_TRANSACTIONSTATUS WHERE DESCRIPTION = 'Paid'
			set @tenantNeglect = @tenantNeglect *( -1.0 )
			set @ISDEBIT = 0
		END


	-- Insertion into F_RENTJOURNAL    
	INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, STATUSID, AMOUNT, ISDEBIT, PAYMENTSTARTDATE, PAYMENTENDDATE)
	VALUES (@TENANCYID, getdate(), @ITEMTYPE, @STATUSID, @tenantNeglect, @ISDEBIT, @PAYMENTSTARTDATE, @PAYMENTENDDATE)
   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END