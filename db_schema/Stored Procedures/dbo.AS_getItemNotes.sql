USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_getItemNotes]    Script Date: 09/19/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_getItemNotes 'BHA0000987' ,1  
-- Author:  <Ali Raza>  
-- Create date: <10/30/2013>  
-- Description: <Get the property Notes against Item Id>  
-- WebPage: PropertRecord.aspx => Attributes Tab  
-- =============================================  
IF OBJECT_ID('dbo.[AS_getItemNotes]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_getItemNotes] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_getItemNotes] (  
@proerptyId Varchar(100),  
@itemId int,
@heatingMappingId int = 0
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
   
	If(@heatingMappingId = 0)
	Begin
		SELECT SID,PROPERTYID,ItemId,Notes, 
		CONVERT(varchar,CreatedOn,103 )   CreatedOn,
		(e.FIRSTNAME + ' ' + e.LASTNAME)  CreatedBy,
		ShowInScheduling,
		ShowOnApp,ISACTIVE
		FROM PA_PROPERTY_ITEM_NOTES
		LEFT JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy 
		WHERE PROPERTYID = @proerptyId AND ItemId = @itemId 
	END 
	ELSE
	BEGIN
		SELECT SID,PROPERTYID,ItemId,Notes, 
		CONVERT(varchar,CreatedOn,103 )   CreatedOn,
		(e.FIRSTNAME + ' ' + e.LASTNAME)  CreatedBy,
		ShowInScheduling,
		ShowOnApp
		FROM PA_PROPERTY_ITEM_NOTES
		LEFT JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy 
		WHERE PROPERTYID = @proerptyId AND ItemId = @itemId AND HeatingMappingId = @heatingMappingId
	END
END  