-- =============================================    
--EXEC AS_DeleteItemNotes    
--  @notesId = 1721
-- Create date: <Create Date,,10/10/2018>    
-- Description: <Description,,Update the Note for property and Item>    
-- WebPage: PropertyRecord.aspx => Attributes Tab    
-- =============================================    
IF OBJECT_ID('dbo.AS_DeleteItemNotes') IS NULL 
EXEC('CREATE PROCEDURE dbo.AS_DeleteItemNotes AS SET NOCOUNT ON;') 
GO  
ALTER PROCEDURE [dbo].[AS_DeleteItemNotes] (    
@notesId int)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    

	SET NOCOUNT ON; 
 
	UPDATE PA_PROPERTY_ITEM_NOTES  SET IsActive = 0 WHERE SID = @notesId;

	Declare @note varchar(500),@propertyId nvarchar(50), @itemId Int, @schemeId int, @blockId int, @createdBy int, @heatingMappingId int,@showInScheduling bit,  
	@showOnApp bit

	SELECT @propertyId=PROPERTYID, @itemId=ItemId, @schemeId=SchemeId, @blockId=BlockId,
	@createdBy=CreatedBy, @heatingMappingId=HeatingMappingId,@note=Notes,@showOnApp=showOnApp,@showInScheduling=showInScheduling
	FROM PA_PROPERTY_ITEM_NOTES
	WHERE SID = @notesId

 	INSERT INTO [dbo].[PA_PROPERTY_ITEM_NOTES_HISTORY](
	[SID],
	[PROPERTYID],
	[ItemId],
	[Notes],
	[CreatedOn],
	[CreatedBy],
	[SchemeId],
	[BlockId],
	[ShowInScheduling],
	[ShowOnApp],
	[HeatingMappingId],
	[NotesStatus],
	[IsActive]
	)
	VALUES
	(@notesId,
	@propertyId,
	@itemId,
	@note,
	GETDATE(),
	@createdBy,
	@schemeId,
	@blockId,
	@showInScheduling,
	@showOnApp,
	@heatingMappingId,
	'Deleted',0 )

	--DELETE FROM PA_PROPERTY_ITEM_NOTES_HISTORY
	--WHERE SID = @notesId

	--DELETE FROM PA_PROPERTY_ITEM_NOTES
	--WHERE SID = @notesId
END 