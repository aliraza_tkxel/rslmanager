USE [RSLBHALive ]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'FL_DeleteFollowOnWork'))
  DROP PROCEDURE FL_DeleteFollowOnWork
  
 GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 18/1/2016
-- Description:	Delete FollowOn work details
-- =============================================
CREATE PROCEDURE [dbo].[FL_DeleteFollowOnWork]
	-- Add the parameters for the stored procedure here
	@followOnWorkId int,
	@faultLogId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Delete data from FL_Fault_FollowOn
	DELETE FROM FL_Fault_FollowOn WHERE followonId = @followOnWorkId
	--update data of FL_Fault_Log
	UPDATE FL_Fault_Log set FollowOnFaultLogId = null where FaultLogId=@faultLogId
END
