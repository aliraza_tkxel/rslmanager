USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyStatus]    Script Date: 20/03/2019 5:56:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description: Get Property Status for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Status for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyStatus
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetPropertyStatus') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyStatus AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[PDR_GetPropertyStatus]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select S.STATUSID,S.DESCRIPTION from P_STATUS S Order by S.DESCRIPTION ASC
	
END
