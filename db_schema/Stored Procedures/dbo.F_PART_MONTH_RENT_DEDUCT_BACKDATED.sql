SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[F_PART_MONTH_RENT_DEDUCT_BACKDATED] (

	@ENDDATE SMALLDATETIME,
	@MONTHEND SMALLDATETIME,
	@PROPERTYID NVARCHAR(50),
	@TENANCYID INT,
	@REMOVALTYPE INT,  -- 1 IS RENT PROPERTY, 2 -- IS A SINGULAR ADDITIONAL ASSET
	@TERMINATIONJOURNALID INT = -1,
	@RUNDATE SMALLDATETIME = NULL
)

AS

DECLARE @YEAR_START SMALLDATETIME
DECLARE @TOTALRENT MONEY
DECLARE @END_MONTH FLOAT
DECLARE @DAYS_IN_MONTH FLOAT
DECLARE @DAYS_IN_YEAR FLOAT
DECLARE @END_YEAR FLOAT
DECLARE @END_DAY FLOAT
DECLARE @RENT_DAYS FLOAT
DECLARE @TOTALRENT_PAYABLE MONEY
DECLARE @MONTH_DIFFERENCE FLOAT
DECLARE @DAILYRATE FLOAT
DECLARE @YEAR_END SMALLDATETIME
DECLARE @DATE SMALLDATETIME
DECLARE @JOURNALID INT

DECLARE @RENT MONEY
DECLARE @SERVICES MONEY
DECLARE @COUNCILTAX MONEY
DECLARE @WATERRATES MONEY
DECLARE @INELIGSERV MONEY
DECLARE @SUPPORTEDSERVICES MONEY
DECLARE @GARAGE MONEY

SET @TOTALRENT = 0
SET @RENT = 0
SET @SERVICES = 0
SET @COUNCILTAX = 0
SET @WATERRATES = 0
SET @INELIGSERV = 0
SET @SUPPORTEDSERVICES = 0
SET @GARAGE = 0

DECLARE @RENT_PART MONEY
DECLARE @SERVICES_PART MONEY
DECLARE @COUNCILTAX_PART MONEY
DECLARE @WATERRATES_PART MONEY
DECLARE @INELIGSERV_PART MONEY
DECLARE @SUPPORTEDSERVICES_PART MONEY
DECLARE @GARAGE_PART MONEY

DECLARE @RENTTYPE INT
DECLARE @PROPERTYTYPE INT

DECLARE @PORPORTION FLOAT

	SET @DATE = ISNULL(@RUNDATE,CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103))
	SET @END_DAY = DAY(@ENDDATE)
	SET @END_MONTH = MONTH(@ENDDATE)
	SET @END_YEAR = YEAR(@ENDDATE)
	
	-- GET TOTAL RENT OF MAIN ASSET WHICH IS TO BE ENDED
	SELECT @TOTALRENT = ISNULL(F.TOTALRENT,0), 
		@RENT = ISNULL(F.RENT,0),
		@SERVICES = ISNULL(F.SERVICES,0),
		@COUNCILTAX = ISNULL(F.COUNCILTAX,0),
		@WATERRATES = ISNULL(F.WATERRATES,0),
		@INELIGSERV = ISNULL(F.INELIGSERV,0),
		@SUPPORTEDSERVICES = ISNULL(F.SUPPORTEDSERVICES,0),
		@GARAGE = ISNULL(F.GARAGE,0),
		@RENTTYPE = F.RENTTYPE,
		@PROPERTYTYPE = P.PROPERTYTYPE
	FROM P__PROPERTY P 
		INNER JOIN P_FINANCIAL F ON P.PROPERTYID = F.PROPERTYID
	WHERE F.PROPERTYID = @PROPERTYID
	
	--IF THE TYPE IS A TENANCY END THEN ADD ALL ADDITIONALASSETS ASLO.
	IF @REMOVALTYPE = 1
	BEGIN
		SELECT @TOTALRENT = @TOTALRENT + ISNULL(SUM(F.TOTALRENT),0), 
			@RENT = @RENT + ISNULL(SUM(F.RENT),0),
			@SERVICES = @SERVICES + ISNULL(SUM(F.SERVICES),0),
			@COUNCILTAX = @COUNCILTAX + ISNULL(SUM(F.COUNCILTAX),0),
			@WATERRATES = @WATERRATES + ISNULL(SUM(F.WATERRATES),0),
			@INELIGSERV = @INELIGSERV + ISNULL(SUM(F.INELIGSERV),0),
			@SUPPORTEDSERVICES = @SUPPORTEDSERVICES + ISNULL(SUM(F.SUPPORTEDSERVICES),0),
			@GARAGE = @GARAGE + ISNULL(SUM(F.GARAGE),0)
		FROM C_ADDITIONALASSET AD
			      INNER JOIN P__PROPERTY P ON P.PROPERTYID = AD.PROPERTYID
			      INNER JOIN P_FINANCIAL F ON P.PROPERTYID = F.PROPERTYID
		WHERE TENANCYID = @TENANCYID AND AD.ENDDATE IS NULL
	END

	SET @MONTH_DIFFERENCE = DATEDIFF(mm, @ENDDATE, ISNULL(@RUNDATE,GETDATE()))

	-- CALCULATE DAYS IN MONTH
	IF @END_MONTH IN (4,6,9,11) 
	   SET @DAYS_IN_MONTH = 30
        	ELSE IF @END_MONTH = 2
	   IF @END_YEAR IN (2004, 2008, 2012, 2016, 2020, 2024)
	     SET @DAYS_IN_MONTH = 29
	   ELSE
	     SET @DAYS_IN_MONTH = 28
	ELSE 
	   SET @DAYS_IN_MONTH = 31
	
	-- GET NUMBER OF DAYS IN CURRENT YEAR
	SET @YEAR_START = CAST(('1' + ' ' + DATENAME(MM, @ENDDATE) + ' ' + DATENAME(YYYY, @ENDDATE)) AS SMALLDATETIME)
	SET @YEAR_END = DATEADD(M, 12, @YEAR_START)
	SET @DAYS_IN_YEAR = DATEDIFF(dd, @YEAR_START, @YEAR_END)

	-- GET DAILY RATE
	SET @DAYS_IN_MONTH = @DAYS_IN_YEAR/12
 	SET @DAILYRATE = @TOTALRENT * (12/@DAYS_IN_YEAR)

	SET @TOTALRENT_PAYABLE = ROUND( @DAILYRATE * (@DAYS_IN_MONTH - @END_DAY),2)
	--NEGATE THE FIGURE HERE
	SET @TOTALRENT_PAYABLE = -(@TOTALRENT_PAYABLE + (@TOTALRENT * @MONTH_DIFFERENCE))

	-- MAKE JOURNAL ENTRY
	INSERT INTO F_RENTJOURNAL 
	(	TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, PAYMENTSTARTDATE,
		PAYMENTENDDATE, AMOUNT, STATUSID )
	VALUES (@TENANCYID, @ENDDATE, 1, 12, @ENDDATE, @MONTHEND, @TOTALRENT_PAYABLE, 13)
	
	SET @JOURNALID = SCOPE_IDENTITY()
	PRINT @JOURNALID
	
	-- FIND THE PORPORTION OF THE RENT AND APPLY IT TO THE OTHER BITS
	IF @TOTALRENT = 0 
	BEGIN
		SET @PORPORTION = 0
	END 
	ELSE
	BEGIN
		SET @PORPORTION = @TOTALRENT_PAYABLE / @TOTALRENT
	END
	PRINT @PORPORTION
	SET @SERVICES_PART = ROUND(@SERVICES * @PORPORTION,2)
	SET @COUNCILTAX_PART = ROUND(@COUNCILTAX * @PORPORTION,2)
	SET @WATERRATES_PART = ROUND(@WATERRATES * @PORPORTION,2)
	SET @INELIGSERV_PART = ROUND(@INELIGSERV * @PORPORTION,2)
	SET @SUPPORTEDSERVICES_PART = ROUND(@SUPPORTEDSERVICES * @PORPORTION,2)

	--THIS PART GETS RID OF ANY SLIGHT ROUNDING ERRORS DUE TO THE PORPORTION FIGURE.
	--DEPENDING ON WHAT TYPE OF PROPERTY/ASSET IS BEING REMOVED WE CORRECT A 
	--DIFFERENT PART OF THE BREAK UP.
	IF @RENT = 0
	BEGIN
		SET @RENT_PART = ROUND(@RENT * @PORPORTION,2)
		-- USE THE GARAGE PART TO GET RID OF ANY ROUNDING ERRORS.
		SET @GARAGE_PART = @TOTALRENT_PAYABLE -(@SERVICES_PART + @COUNCILTAX_PART + @WATERRATES_PART + @INELIGSERV_PART + @SUPPORTEDSERVICES_PART + @RENT_PART)
	END
	ELSE
	BEGIN
		SET @GARAGE_PART = ROUND(@GARAGE * @PORPORTION,2)
		-- USE THE MAIN RENT PART TO GET RID OF ANY ROUNDING ERRORS.
		SET @RENT_PART = @TOTALRENT_PAYABLE -(@SERVICES_PART + @COUNCILTAX_PART + @WATERRATES_PART + @INELIGSERV_PART + @SUPPORTEDSERVICES_PART + @GARAGE_PART)
	END
	
	--FINALLY INSERT INTO THE TABLE F_RENTJOURNAL_IR (INITIALS AND REBATES)
	INSERT INTO F_RENTJOURNAL_IR (
		JOURNALID, TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT,
		RENT, SERVICES, COUNCILTAX, WATERRATES, INELIGSERV, SUPPORTEDSERVICES, GARAGE, TOTALRENT,
		PROPERTYID, RENTTYPE, PROPERTYTYPE, 
		RENT_PART, SERVICES_PART, COUNCILTAX_PART, WATERRATES_PART, INELIGSERV_PART, SUPPORTEDSERVICES_PART, GARAGE_PART 
		)
	VALUES (
		@JOURNALID, @TENANCYID, @endDATE, 1, 12, @TOTALRENT_PAYABLE, 
		@RENT, @SERVICES, @COUNCILTAX, @WATERRATES, @INELIGSERV, @SUPPORTEDSERVICES, @GARAGE, @TOTALRENT,
		@PROPERTYID, @RENTTYPE, @PROPERTYTYPE,
		@RENT_PART, @SERVICES_PART, @COUNCILTAX_PART, @WATERRATES_PART, @INELIGSERV_PART, @SUPPORTEDSERVICES_PART, @GARAGE_PART
		)

	EXEC NL_INITIAL_AND_REBATES

	-- STORE RENT JOURNAL ENTRY INCASE OF FUTURE REVERSAL
	IF @TERMINATIONJOURNALID <> -1
	BEGIN
	   INSERT INTO C_TERMINATIONSUM (JOURNALID, ADJUSTMENT)
	   VALUES (@TERMINATIONJOURNALID, @TOTALRENT_PAYABLE)
	END




GO
