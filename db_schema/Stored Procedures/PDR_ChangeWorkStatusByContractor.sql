USE [RSLBHALive]
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'PDR_ChangeWorkStatusByContractor'))
  DROP PROCEDURE PDR_ChangeWorkStatusByContractor

 GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <28/10/2015>
-- Description:	<update PO Status after accepting or rejecting work by contractor >
-- EXEC PDR_ChangeWorkStatusByContractor 265825,0
-- =============================================
CREATE PROCEDURE [dbo].[PDR_ChangeWorkStatusByContractor] 
	@orderId int ,
	@isAccept bit
	AS
BEGIN
	
	SET NOCOUNT ON;
	Declare @StatusId int
	
IF  (@isAccept = 1) 
	BEGIN
		SELECT  @StatusId = FLS.FaultStatusId FROM FL_FAULT_STATUS FLS WHERE Description = 'Accepcted by Contractor'  
	End 
  
ELSE 
  BEGIN
	SELECT  @StatusId = FLS.FaultStatusId FROM FL_FAULT_STATUS FLS WHERE Description = 'Rejected by Contractor'  
  End
  
  
	DECLARE @FaultLogID int
	DECLARE @faultId int 
	DECLARE @areaId int 
	
	select @FaultLogID = FL.FaultLogId 
	from FL_FAULT_JOURNAL J
	INNER JOIN FL_FAULT_LOG FL ON  J.FaultLogId = FL.FaultLogId
	INNER JOIN  FL_CONTRACTOR_WORK  CW   ON CW.JournalId  =J.JournalId
	WHERE CW.PURCHASEORDERID = @orderId
  
	SELECT @faultId = faultId FROM FL_FAULT_LOG ffl WHERE ffl.FaultLogID=FaultLogID
	select @areaId = areaId From FL_fault where faultid =@faultId

	update FL_FAULT_LOG set statusId = @statusId,AreaId = @areaId
	where faultLogId =@FaultLogID

  

  
  
  --END IF
END






