USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_MyTeamLeaveOfAbsenseRequested]    Script Date: 04/27/2018 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Rehman
-- Create date: June 2, 2017
-- =============================================

IF OBJECT_ID('dbo.E_MyTeamLeaveOfAbsenseRequested') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_MyTeamLeaveOfAbsenseRequested AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_MyTeamLeaveOfAbsenseRequested] 
	-- Add the parameters for the stored procedure here
	@empId INT
	
AS
BEGIN

	DECLARE @MyStaffEmpIds TABLE(
		EmpId int
	)

	DECLARE @MyStaffLeavesData TABLE(
		StartDate smalldatetime null,
		ReturnDate smalldatetime null,
		Reason varchar(max),
		Status varchar(255),
		EmpId int null,
		ItemNatureId int null,
		Nature varchar(255),
		DAYS_ABS int,
		HolType varchar(255),
		FirstName varchar(255),
		LastName varchar(255),
		AbsenceHistoryId int,
		Duration float null,
		Unit varchar(255)
	)

	DECLARE @staffId INT
	DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME		

	INSERT INTO @MyStaffEmpIds (EmpId)
	(SELECT E.EMPLOYEEID 
	FROM	E__EMPLOYEE E   
			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
			WHERE	 	J.LINEMANAGER =  @empId   AND J.ACTIVE = 1)

	DECLARE myStaffCursor CURSOR  FOR
	SELECT EmpId from @MyStaffEmpIds

	OPEN myStaffCursor

	FETCH NEXT FROM myStaffCursor INTO @staffId
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE 
		FROM	EMPLOYEE_ANNUAL_START_END_DATE(@staffId)

		INSERT INTO @MyStaffLeavesData (StartDate, ReturnDate, Reason, Status, EmpId, ItemNatureId, Nature, DAYS_ABS, HolType, FirstName, LastName, AbsenceHistoryId, Duration,Unit)
		SELECT e.STARTDATE,e.RETURNDATE,ar.DESCRIPTION as Reason,s.DESCRIPTION as Status,j.EMPLOYEEID,n.ITEMNATUREID, n.DESCRIPTION, 
		Case when e.returndate is null 
			then dbo.EMP_SICKNESS_DURATION(Emp.EMPLOYEEID, e.STARTDATE, getdate(), e.JOURNALID) 
			ELSE dbo.EMP_SICKNESS_DURATION(Emp.EMPLOYEEID, e.STARTDATE, e.RETURNDATE, e.JOURNALID) 
		end as DAYS_ABS
		, e.HOLTYPE as HolType,
			emp.FIRSTNAME, emp.LASTNAME, e.ABSENCEHISTORYID, 
		Case when Jd.HolidayIndicator=2 and Jd.PARTFULLTIME=2 then e.DURATION_HRS else e.DURATION end as duration,e.DURATION_TYPE as Unit
		FROM E__EMPLOYEE Emp  
		LEFT JOIN E_JOBDETAILS Jd ON Emp.EMPLOYEEID = Jd.EMPLOYEEID  
		LEFT JOIN E_JOURNAL j ON Jd.EMPLOYEEID = j.EMPLOYEEID  AND j.CURRENTITEMSTATUSID = (SELECT ITEMSTATUSID from E_STATUS WHERE DESCRIPTION = 'Pending')
		inner join E_ABSENCE e on j.JOURNALID = e.JOURNALID
		 AND e.ABSENCEHISTORYID = (
											SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
											WHERE JOURNALID = j.JOURNALID
										  ) 
		LEFT JOIN E_ABSENCEREASON ar on e.REASONID = ar.SID
		INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
		INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
		WHERE j.EMPLOYEEID = @staffId and j.ITEMNATUREID not IN (SELECT ITEMNATUREID from E_NATURE 
		where DESCRIPTION  in ('Sickness','Annual Leave','BRS TOIL Recorded','BRS Time Off in Lieu','BHG TOIL','Birthday Leave','Personal Day') ) and e.STARTDATE >= @BHSTART and  e.STARTDATE <= @BHEND
		ORDER by e.STARTDATE DESC
		
	FETCH NEXT FROM myStaffCursor INTO @staffId
	END

	CLOSE myStaffCursor
	DEALLOCATE myStaffCursor

	SELECT StartDate, ReturnDate, Reason, Status, EmpId, ItemNatureId, Nature, DAYS_ABS, HolType, FirstName, LastName, AbsenceHistoryId, Duration, Unit
	FROM @MyStaffLeavesData

END

