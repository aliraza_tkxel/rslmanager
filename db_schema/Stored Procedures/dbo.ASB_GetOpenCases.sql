USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[ASB_GetOpenCases]    Script Date: 09-Jan-17 1:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[ASB_GetOpenCases]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[ASB_GetOpenCases] AS SET NOCOUNT ON;') 
GO

ALTER  PROCEDURE [dbo].[ASB_GetOpenCases] (
					@USERID INT
					) AS
DECLARE @STRSQL VARCHAR(8000)
DECLARE @SEARCHSTRING VARCHAR(500)=''
DECLARE @SHOWALLSTRING VARCHAR(200)

BEGIN

IF @USERID IS NOT NULL
	SET @SEARCHSTRING = 'WHERE asb.CaseOfficer = ' + CAST(@USERID AS VARCHAR) + ' OR asb.CaseOfficerTwo = ' + CAST(@USERID AS VARCHAR)

SET @STRSQL =	'SELECT	ISNULL(COUNT(*),0) AS NUM
		FROM 	ASB_Case asb
		INNER JOIN E__EMPLOYEE cone on asb.CaseOfficer=cone.EMPLOYEEID
		LEFT JOIN E__EMPLOYEE Ctwo on asb.CaseOfficerTwo=Ctwo.EMPLOYEEID
		' + @SEARCHSTRING
EXECUTE (@STRSQL)
END




