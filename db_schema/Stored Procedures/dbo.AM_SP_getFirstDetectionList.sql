USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AM_SP_getFirstDetectionList]    Script Date: 02/19/2016 10:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AM_SP_getFirstDetectionList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AM_SP_getFirstDetectionList AS SET NOCOUNT ON;') 
GO



--exec [AM_SP_getFirstDetectionList] '',-1,-1,-1,1,1,'',1,100,'TENANCYID','ASC','',''  
ALTER PROCEDURE [dbo].[AM_SP_getFirstDetectionList]  
   @postCode varchar(15),  
   @caseOwnedById int=0,  
   @regionId int = 0,  
   @suburbId int = 0,  
   @allRegionFlag bit,  
   @allSuburbFlag bit,  
   @surname  varchar(100),  
   @skipIndex int = 0,  
   @pageSize int = 10,  
            @sortBy     varchar(100),  
            @sortDirection varchar(10),  
            @address1 varchar(200),  
            @tenancyid VARCHAR(15)  
     
AS  
BEGIN  
--  
declare @orderbyClause varchar(50)  
declare @query varchar(8000)  
declare @subQuery varchar(8000)  
declare @RegionSuburbClause varchar(8000),  
@rowNumberQuery varchar(MAX),  
  @finalQuery varchar(MAX),  
  @offset int,  
  @limit int    
    
 SET @offset = 1+(@skipIndex-1) * @pageSize  
  SET @limit = (@offset + @pageSize)-1   
      
IF(@caseOwnedById = -1 )  
BEGIN  
  
 IF(@regionId = -1 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
    ELSE IF(@regionId = -1 and @suburbId > 0)  
    BEGIN  
          SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId)   
    END  
  
END  
ELSE   
BEGIN  
  
IF(@regionId = -1 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=1) OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=1))'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)+ ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=1) OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=1))'  
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
    ELSE IF(@regionId = -1 and @suburbId > 0)  
    BEGIN  
          SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId)   
    END  
END  
  
  
  
SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection  
SET @query =   
'SELECT TOP ('+convert(varchar(10),@limit)+' )  
      AM_FirstDetecionList.TENANCYID,   
      Max(AM_FirstDetecionList.CUSTOMERID) AS CUSTOMERID,  
      Max(ISNULL(Convert(varchar(100),(customer.LastPaymentDate), 103), '''')) AS TRANSACTIONDATE,   
      Max(ISNULL((customer.LastPayment), 0.0)) AS LastCPAY,   
      Max(customer.CustomerAddress) AS CustomerAddress,   
      Max(AM_FirstDetecionList.FirstDetectionDate) AS FirstDetectionDate,   
                  Max(customer.RentBalance) AS RentBalance,  
       Max(customer.EstimatedHBDue) AS EstimatedHBDue,  
       CONVERT(nvarchar,MAX(DD.PAYMENTDATE),103)AS NextDD ,  
       --ISNULL(DATEPART(DD,D.PAYMENTDATE) ,'''') AS NextDD,  
       --CONVERT(DECIMAL,(Max(customer.RentBalance)-Max(ISNULL(customer.LastPayment, 0.0))))AS NextAmount ,  
       Max(ISNULL(DD.REGULARPAYMENT,0))AS NextAmount ,  
    
      Max(ISNUll(ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0),0.0)) as OwedToBHA,  
  
      (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
      FROM AM_Customer_Rent_Parameters  
       INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId  
      WHERE AM_Customer_Rent_Parameters.TenancyId = AM_FirstDetecionList.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())  
      ORDER BY AM_Customer_Rent_Parameters.CustomerId ASC) as CustomerName,  
  
      (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
      FROM AM_Customer_Rent_Parameters  
       INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId  
      WHERE AM_Customer_Rent_Parameters.TenancyId = AM_FirstDetecionList.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())  
      ORDER BY AM_Customer_Rent_Parameters.CustomerId DESC) as CustomerName2,  
  
      (SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)  
      FROM AM_Customer_Rent_Parameters  
       INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId  
      WHERE AM_Customer_Rent_Parameters.TenancyId = AM_FirstDetecionList.TENANCYID   
      AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())) as JointTenancyCount,  
         
      (SELECT ISNULL(P_FINANCIAL.Totalrent , 0)  
              FROM C_TENANCY INNER JOIN  
              P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID   
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID  
              INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P_SCHEME.DEVELOPMENTID   
              INNER JOIN P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID   
              WHERE C_TENANCY.TENANCYID = AM_FirstDetecionList.TENANCYID) AS TotalRent  
       
     --ISNULL((ISNULL(RentBalance, 0) - ISNULL(null, 0)), 0) AS OwedToBHA  
   
 FROM         AM_FirstDetecionList   
      --INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID  
                  INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.TENANCYID = customer.TENANCYID  
                  INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID  
      INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID   
      LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID  
     -- left JOIN F_DDSCHEDULE DD ON DD.TENANCYID  = AM_FirstDetecionList.TENANCYID--change  
      Outer APPLY (Select   
      PAYMENTDATE = (  
      CASE WHEN ISNULL( DD.PAYMENTDATE ,GETDATE() + 1) >=  GETDATE()     
      THEN ISNULL(CONVERT(NVARCHAR, DD.PAYMENTDATE, 103) ,'''')    
      ELSE    
       CASE WHEN CONVERT(DATETIME,CAST(DATEPART(DD,DD.PAYMENTDATE) AS VARCHAR) + '' '' + DATENAME(MM,GETDATE()) + '' '' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)) >= GETDATE()     
       THEN CONVERT(DATETIME,CAST(DATEPART(DD,DD.PAYMENTDATE) AS VARCHAR) + '' '' + DATENAME(MM,GETDATE()) + '' '' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR))   
       ELSE   
       DATEADD(MM,1,CONVERT(DATETIME,CAST(DATEPART(DD,DD.PAYMENTDATE) AS VARCHAR) + '' '' + DATENAME(MM,GETDATE()) + '' '' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)))   
       END   
      END  ),ISNULL(DD.REGULARPAYMENT,0) AS REGULARPAYMENT  
      From F_DDSCHEDULE DD WHERE (DD.SUSPEND is NULL OR DD.SUSPEND =0) AND (DD.TEMPSUSPEND is NULL  OR DD.TEMPSUSPEND=0) AND DD.TENANCYID =  C_TENANCY.TENANCYID   
             
           ) as DD   
        
      INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID      
     -- INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId  
                  INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID  
   
 WHERE '+ @RegionSuburbClause +' AND   
   
 1 = Case when (ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0)) > 0then 1 else 0 END  
   --dbo.AM_FN_CHECK_OWED_TO_BHA(ISNULL(customer.RentBalance, 0.0),ISNULL(customer.EstimatedHBDue, 0.0))=''true''  
             AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId   
              FROM AM_Case   
              WHERE AM_Case.IsActive= 1 )        
     AND AM_FirstDetecionList.IsDefaulter =1  
     AND customer.LASTNAME LIKE '''' + CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN customer.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END + ''%''  
             
           AND P__PROPERTY.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN P__PROPERTY.POSTCODE ELSE '''+@postCode+''' END   
           AND (  
      (P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.HOUSENUMBER + '' 
'' + P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')    
        
    )  
     AND customer.TenancyId='''' + CASE WHEN '''' = '''+ REPLACE(@tenancyid,'''','''''') +''' THEN customer.TenancyId ELSE '''+ REPLACE(@tenancyid,'''','''''') +''' END + ''''  
     AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE>GETDATE())  
     GROUP By AM_FirstDetecionList.TENANCYID  
   '  
    
   
   
   
    
  --=============================== Row Number Query =============================  
  Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortBy+ CHAR(10) +@sortDirection+CHAR(10)+') as row   
        FROM ('+CHAR(10)+@query+CHAR(10)+')AS Records'  
    
  --============================== Final Query ===================================  
  Set @finalQuery  =' SELECT *  
       FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result   
       WHERE  
       Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)      
    
  --============================ Exec Final Query =================================  
     
   
print(@finalQuery);  
  
exec(@finalQuery);       
  
  
END  