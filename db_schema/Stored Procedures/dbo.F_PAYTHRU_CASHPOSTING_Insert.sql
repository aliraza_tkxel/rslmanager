SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[F_PAYTHRU_CASHPOSTING_Insert] 
    @JOURNALID int,
    @TENANCYID int,
    @CUSTOMERID int,
    @CREATIONDATE smalldatetime,
    @CREATEDBY int,
    @OFFICE int,
    @RECEIVEDFROM nvarchar(100),
    @ITEMTYPE int,
    @PAYMENTTYPE int,
    @AMOUNT float,
    @CHQNUMBER nvarchar(30),
    @PAYMENTSLIPNUMBER nvarchar(50),
    @CASHPOSTINGORIGIN INT
AS 
	SET NOCOUNT ON 
	
    INSERT  INTO [dbo].[F_CASHPOSTING]
            ( [JOURNALID] ,
              [TENANCYID] ,
              [CUSTOMERID] ,
              [CREATIONDATE] ,
              [CREATEDBY] ,
              [OFFICE] ,
              [RECEIVEDFROM] ,
              [ITEMTYPE] ,
              [PAYMENTTYPE] ,
              [AMOUNT] ,
              [CHQNUMBER] ,
              [PAYMENTSLIPNUMBER] ,
              [CASHPOSTINGORIGIN]
            )
            VALUES (
				    @JOURNALID ,
                    @TENANCYID ,
                    @CUSTOMERID ,
                    @CREATIONDATE ,
                    @CREATEDBY ,
                    @OFFICE ,
                    @RECEIVEDFROM ,
                    @ITEMTYPE ,
                    @PAYMENTTYPE ,
                    @AMOUNT ,
                    @CHQNUMBER ,
                    @PAYMENTSLIPNUMBER ,
                    @CASHPOSTINGORIGIN
			)

	SELECT SCOPE_IDENTITY() AS CASHPOSTINGID


GO
