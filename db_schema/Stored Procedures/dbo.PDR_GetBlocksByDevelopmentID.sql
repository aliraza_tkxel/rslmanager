USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocksByDevelopmentID]    Script Date: 21/03/2019 11:49:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:   Get Blocks By developmentId 
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Blocks By developmentId 
    
    Execution Command:
    
    Exec PDR_GetBlocksByDevelopmentID
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetBlocksByDevelopmentID') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetBlocksByDevelopmentID AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
  
ALTER PROCEDURE [dbo].[PDR_GetBlocksByDevelopmentID]
( @developmentId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select B.BLOCKID,B.BLOCKNAME from P_BLOCK B
	WHERE B.DEVELOPMENTID =@developmentId
	ORDER BY B.BLOCKNAME ASC
END
