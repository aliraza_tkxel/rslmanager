SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  proc [dbo].[F_POSTERROR]
@paymenttypeid int,
@tenancyid int,
@transactiondate smalldatetime,
@amount money,
@rent money,
@services money,
@council money,
@water money,
@garage money,
@support money,
@ineligible money,
@totalrent money,
@propertyid varchar(50),
@renttype int,
@propertytype int,
@startpaymentdate smalldatetime,
@endpaymentdate smalldatetime
as

DECLARE @intErrorCode INT

BEGIN TRAN



insert into F_RENTJOURNAL_ERRORS_MONTHLY
(PAYMENTTYPEID,TENANCYID,TRANSACTIONDATE,AMOUNT,RENT,SERVICES,COUNCILTAX,
WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,PROPERTYID,RENTTYPE,PROPERTYTYPE,
startpaymentdate,endpaymentdate,direction,ITEMTYPEID)
select 
(select PAYMENTTYPEID from F_PAYMENTTYPE where paymenttypeid in (select f_paymenttypeid from F_MISPOSTITEMS where itemid = @paymenttypeid)),
@tenancyid,@transactiondate,@amount,@rent,@services,@council,
@water,@ineligible,@support,@garage,@totalrent,@propertyid,@renttype,@propertytype,
@startpaymentdate,@endpaymentdate,(select direction from F_MISPOSTITEMS where itemid = @paymenttypeid),
(select itemtype from F_MISPOSTITEMS where itemid = 1)

SELECT @intErrorCode = @@ERROR
IF (@intErrorCode <> 0) GOTO PROBLEM
COMMIT TRAN

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
RETURN 'There was an unexpected error please try again'
ROLLBACK TRAN
END





GO
