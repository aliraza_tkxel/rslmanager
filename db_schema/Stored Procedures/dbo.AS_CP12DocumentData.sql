USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_CP12DocumentData]    Script Date: 11/23/2015 17:30:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC	[dbo].[AS_CP12DocumentData] @journalId = 8184, @propertyId = N'A650750009'
-- =============================================
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

IF OBJECT_ID('dbo.AS_CP12DocumentData') IS NULL
 EXEC('CREATE PROCEDURE dbo.AS_CP12DocumentData AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[AS_CP12DocumentData] 
	
	@journalId as INT,
	@heatingId as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;			
	

	DECLARE @propertyId nvarchar(20)
	DECLARE @schemeId int
	DECLARE @blockId int
	DECLARE @heatingFuel int

	SELECT  @propertyId = PropertyId
			,@schemeId = SchemeID
			,@blockId = BlockID
			,@heatingFuel = HeatingType
	FROM	PA_HeatingMapping
	WHERE   HeatingMappingId = @heatingId

	
-- =================================	
-- (1.A) APPLIANCE DETAIL
-- =================================
DECLARE @itemName nvarchar(100)	
DECLARE @boilerParam nvarchar(100)		
SET @boilerParam = 'Heating Type'

IF @propertyId IS NOT NULL AND @propertyId <> ''
BEGIN
	SET	@itemName = 'Heating'
END
ELSE
BEGIN
	SET	@itemName = 'Boiler Room'
END
			


DECLARE @BOILERTYPEID BIGINT
DECLARE	@PROPERTYBOILERATTRIBUTEID INT

SELECT	@BOILERTYPEID = PPA.VALUEID
		,@PROPERTYBOILERATTRIBUTEID = PPA.ATTRIBUTEID
FROM	dbo.PA_PROPERTY_ATTRIBUTES PPA
		INNER JOIN PA_ITEM_PARAMETER PIP ON PPA.ITEMPARAMID = PIP.ItemParamID
		INNER JOIN PA_PARAMETER PP ON PIP.ParameterId = PP.ParameterID
		INNER JOIN dbo.PA_ITEM PIT ON PIP.ItemId = PIT.ItemID 
WHERE	PP.ParameterName = @boilerParam 
		AND PIT.ItemName = @itemName
		AND PPA.HeatingMappingId = @heatingId


SELECT 
		GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
		,GS_PROPERTY_APPLIANCE.FLUETYPE as FlueType
		,GS_PROPERTY_APPLIANCE.DATEINSTALLED
		,	CASE 
				WHEN GS_PROPERTY_APPLIANCE.ISLANDLORDAPPLIANCE is null THEN 'No'
				WHEN GS_PROPERTY_APPLIANCE.ISLANDLORDAPPLIANCE = 0 THEN 'No' 
				WHEN GS_PROPERTY_APPLIANCE.ISLANDLORDAPPLIANCE = 1 THEN 'Yes'
			END as isLandlordAppliance
		,GS_PROPERTY_APPLIANCE.REPLACEMENTDATE
		,GS_LOCATION.LOCATION as Location	
		,GS_MANUFACTURER.MANUFACTURER as Manufacturer	
		,CASE 
			WHEN GS_ApplianceModel.Model IS NULL THEN 
				'--'
			ELSE 
				GS_ApplianceModel.Model 
		END AS Model
		,GS_APPLIANCE_TYPE.APPLIANCETYPE as ApplianceType
		,GS_APPLIANCE_TYPE.APPLIANCETYPEID
								
FROM	GS_PROPERTY_APPLIANCE
		INNER JOIN GS_LOCATION ON GS_PROPERTY_APPLIANCE.LOCATIONID = (GS_LOCATION.LOCATIONID)
		INNER JOIN GS_MANUFACTURER ON GS_PROPERTY_APPLIANCE.MANUFACTURERID = (GS_MANUFACTURER.MANUFACTURERID)
		LEFT OUTER JOIN GS_ApplianceModel ON GS_PROPERTY_APPLIANCE.MODELID = (GS_ApplianceModel.ModelID)
		Left OUTER JOIN GS_APPLIANCE_TYPE ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = (GS_APPLIANCE_TYPE.APPLIANCETYPEID)
WHERE	GS_PROPERTY_APPLIANCE.IsActive = 1
		AND ((GS_PROPERTY_APPLIANCE.PROPERTYID = @propertyId)
		OR (GS_PROPERTY_APPLIANCE.SCHEMEID = @schemeId) 
		OR (GS_PROPERTY_APPLIANCE.BLOCKID = @blockId) )

	UNION ALL

-- =================================	
-- (1.B) BOILER DETAIL
-- =================================
		
 SELECT 	
		@PROPERTYBOILERATTRIBUTEID AS PROPERTYAPPLIANCEID
		,ISNULL(dbo.GetHeatingAttributeValue(@heatingId,@itemName,'Flue Type'),'--') as FlueType 
		,NULL AS DATEINSTALLED
		,ISNULL(dbo.GetHeatingAttributeValue(@heatingId,@itemName,'Landlord Appliance'),'--') AS isLandlordAppliance 
		,NULL AS REPLACEMENTDATE	
		,ISNULL(dbo.GetHeatingAttributeValue(@heatingId,@itemName,'Location'),'--') AS Location 	
		,ISNULL(dbo.GetHeatingAttributeValue(@heatingId,@itemName,'Manufacturer'),'--') AS Manufacturer 
		,ISNULL(dbo.GetHeatingAttributeValue(@heatingId,@itemName,'Model'),'--') AS Model 		
		,dbo.GetHeatingAttributeValue(@heatingId, @itemName, @boilerParam) AS ApplianceType
		,@BOILERTYPEID AS APPLIANCETYPEID


-- =================================	
-- (2.A) APPLIANCE INSPECTION
-- =================================

SELECT	APPLIANCEINSPECTIONID
		, PROPERTYAPPLIANCEID
		, ISINSPECTED as Inspected
		, COMBUSTIONREADING as CombustionReading
		, ISNULL(OPERATINGPRESSURE,'')  + ISNULL(' ' + OperatingPressureUnit,'') as OperatingPressure
		, SAFETYDEVICEOPERATIONAL as SafteyDeviceOprational
		, SPILLAGETEST as SpillageTest
		, SMOKEPELLET as SmokePellet
		, ADEQUATEVENTILATION as AdequateVentilation
		, FLUEVISUALCONDITION as FlueVisualCondition
		, SATISFACTORYTERMINATION as SatisfactoryTermination
		, FLUEPERFORMANCECHECKS as FluePerformanceChecks
		, APPLIANCESERVICED as ApplianceServiced
		, APPLIANCESAFETOUSE as ApplianceSafeToUse
		, INSPECTIONDATE
		, INSPECTEDBY
		, JOURNALID
FROM	P_APPLIANCE_INSPECTION 		
WHERE	P_APPLIANCE_INSPECTION.JOURNALID = @journalId
  
UNION ALL

-- =================================	
-- (2.B) BOILER INSPECTION
-- =================================
SELECT	BOILERINSPECTIONID APPLIANCEINSPECTIONID
		, @PROPERTYBOILERATTRIBUTEID AS PROPERTYAPPLIANCEID
		, ISINSPECTED as Inspected
		, COMBUSTIONREADING as CombustionReading
		, ISNULL(OPERATINGPRESSURE,'')  + ISNULL(' ' + OperatingPressureUnit,'') as OperatingPressure
		, SAFETYDEVICEOPERATIONAL as SafteyDeviceOprational
		, SPILLAGETEST as SpillageTest
		, SMOKEPELLET as SmokePellet
		, ADEQUATEVENTILATION as AdequateVentilation
		, FLUEVISUALCONDITION as FlueVisualCondition
		, SATISFACTORYTERMINATION as SatisfactoryTermination
		, FLUEPERFORMANCECHECKS as FluePerformanceChecks
		, BOILERSERVICED as ApplianceServiced
		, BOILERSAFETOUSE as ApplianceSafeToUse
		, INSPECTIONDATE
		, INSPECTEDBY
		, JOURNALID
FROM	P_BOILER_INSPECTION  
WHERE	JOURNALID = @journalId and HeatingMappingId = @heatingId


-- =====================================	
-- (3) SMOKE AND CO DETECTOR INFORMATION
-- =====================================

SELECT	DetectorId as detectorId	
		,ISNULL(Location,'N/A') AS location
		,DetectorType AS detectorType
		,CASE WHEN Passed IS NULL THEN 0 ELSE 1 END AS isTested 
		, CASE 
			WHEN IsLandlordsDetector IS NULL THEN 
				'N/A' 
			ELSE 
				CASE WHEN IsLandlordsDetector =0 THEN
					'No' 
					ELSE
					'Yes'
					END
			END AS  isLandlordsDetector

FROM	P_DETECTOR  
		INNER JOIN  AS_DetectorType ON P_DETECTOR.DetectorTypeId = AS_DetectorType.DetectorTypeId
WHERE	PROPERTYID = @propertyId
		OR SCHEMEID = @schemeId
		OR BLOCKID = @blockId


-- ==========================	
-- (4) GET PROPERTY FAULTS
-- ==========================

SELECT CASE 
         WHEN actionnotes IS NOT NULL THEN actionnotes 
         ELSE '--' 
       END     AS ActionNotes, 
       ( CASE 
           WHEN detectortypeid = 1 THEN 1 -- smoke   
           WHEN detectortypeid = 2 THEN 2 -- co2   
           WHEN applianceid IS NULL THEN @PROPERTYBOILERATTRIBUTEID 
           ELSE applianceid 
         END ) AS ApplianceId, 
       categoryid, 
       defectdate, 
       CASE 
         WHEN defectnotes IS NOT NULL THEN defectnotes 
         ELSE '--' 
       END     AS DefectNotes, 
       CASE 
         WHEN isactiontaken IS NULL THEN 0 
         ELSE isactiontaken 
       END     AS IsActionTaken, 
       CASE 
         WHEN isdefectidentified IS NULL THEN 0 
         ELSE isdefectidentified 
       END     AS IsDefectIdentified, 
       iswarningfixed, 
       CASE 
         WHEN iswarningissued IS NULL THEN 0 
         ELSE iswarningissued 
       END     AS IsWarningIssued, 
       journalid, 
       propertydefectid, 
       p_property_appliance_defects.propertyid
	   ,p_property_appliance_defects.SchemeId
	   ,p_property_appliance_defects.BlockId
       ,CASE 
         WHEN warningnoteserialno IS NULL THEN '--' 
         ELSE warningnoteserialno 
       END     AS SerialNumber 
FROM   p_property_appliance_defects 
       LEFT JOIN gs_property_appliance 
              ON p_property_appliance_defects.applianceid = 
                 gs_property_appliance.propertyapplianceid 
WHERE  categoryid != 1 
       AND ( 
			  ( p_property_appliance_defects.applianceid IS NOT NULL AND p_property_appliance_defects.applianceid <> 0 ) 
              OR ( p_property_appliance_defects.HeatingMappingId = @heatingId )
			  OR (DetectorTypeId > 0) 
		   ) 
       AND journalid = @journalId 


-- ==========================	
-- (5) GET INSTALLATION PIPE WORK
-- ==========================
SELECT	PipeWorkID
		PropertyId,
		DateStamp,
		EmergencyControl,
		EquipotentialBonding,
		GasTightnessTest,
		VisualInspection,
		JournalId
FROM	P_InstallationPipeWork
WHERE	JournalId = @journalId 
		AND heatingMappingId = @heatingId


-- =============================
-- (6) GET SIGNATURE INFORMATION
-- =============================

	SELECT e__employee.FirstName +' '+e__employee.LastName as Name, signaturepath 
	FROM dbo.AS_APPOINTMENTS   
	INNER JOIN e__employee on dbo.AS_APPOINTMENTS.ASSIGNEDTO = e__employee.employeeid  
	WHERE AS_APPOINTMENTS.journalid=@journalid  

-- =======================	
-- (7) CUSTOMER INFORMATION
-- =======================

IF EXISTS(	SELECT * 
			FROM vw_PROPERTY_CURRENT_TENANTS_LIST 
			WHERE PROPERTYID = @propertyId)  
BEGIN   

    SELECT TOP ( 1 )  
            C__CUSTOMER.CUSTOMERID ,  
            G_Title.Description AS Title ,  
            C__CUSTOMER.FIRSTNAME AS FirstName ,  
            C__CUSTOMER.MIDDLENAME AS MiddleName ,  
            C__CUSTOMER.LASTNAME AS LastName ,  
            C_Address.Tel AS Telephone ,  
            C_Address.Mobile AS Mobile ,  
            C_Address.Fax AS Fax ,  
            C_Address.Email AS Email ,  
            ISNULL(P__Property.HouseNumber, '--') AS HouseNumber ,  
            ISNULL(P__Property.FLATNUMBER, '--') AS FlatNumber ,  
            ISNULL(P__Property.ADDRESS1, '--') AS Address1 ,  
            ISNULL(P__Property.ADDRESS2, '--') AS Address2 ,  
            ISNULL(P__Property.ADDRESS3, '--') AS Address3 ,  
            ISNULL(P__Property.TOWNCITY, '--') AS TownCity ,  
            ISNULL(P__Property.POSTCODE, '--') AS PostCode ,  
            ISNULL(P__Property.COUNTY, '--') AS County ,  
            C_Tenancy.TENANCYID ,  
            P__Property.PROPERTYID ,
			 'Property' AS InfoType
    FROM    P__PROPERTY  
            INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID  
            INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID  
            INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID  
            INNER JOIN G_Title ON C__CUSTOMER.Title = G_Title.TitleId  
            INNER JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID  
            INNER JOIN ( SELECT P_LGSR.ISSUEDATE ,  
                                PROPERTYID  
                    FROM   P_LGSR  
                       ) AS P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID  
    WHERE   P__Property.propertyid = @propertyId  
	AND C_ADDRESS.ISDEFAULT = 1 
            AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR dbo.C_CUSTOMERTENANCY.ENDDATE > GETDATE())  
            AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID IN (  
               SELECT  CUSTOMERTENANCYID  
               FROM    C_CUSTOMERTENANCY  
               WHERE   TENANCYID = C_TENANCY.TENANCYID AND (C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())  
              )  
END  
ELSE  
BEGIN  

	SELECT   
            0 AS CUSTOMERID,  
            '' Title ,  
            'N/A' AS FirstName ,  
            '' AS MiddleName ,  
            '' AS LastName ,  
            '' AS Telephone ,  
            '' AS Mobile ,  
            '' AS Fax ,  
            '' AS Email ,  
            ISNULL(P__Property.HouseNumber, '--') AS HouseNumber ,  
            ISNULL(P__Property.FLATNUMBER, '--') AS FlatNumber ,  
            ISNULL(P__Property.ADDRESS1, '--') AS Address1 ,  
            ISNULL(P__Property.ADDRESS2, '--') AS Address2 ,  
            ISNULL(P__Property.ADDRESS3, '--') AS Address3 ,  
            ISNULL(P__Property.TOWNCITY, '--') AS TownCity ,  
            ISNULL(P__Property.POSTCODE, '--') AS PostCode ,  
            ISNULL(P__Property.COUNTY, '--') AS County ,  
            -1 AS TENANCYID ,  
            'Property' AS InfoType
    FROM    P__PROPERTY  
	WHERE	P__Property.PropertyId = @propertyId 
	
	UNION ALL
	 
	SELECT   
            0 AS CUSTOMERID,  
            SCHEMENAME Title ,  
            'N/A' AS FirstName ,  
            '' AS MiddleName ,  
            '' AS LastName ,  
            '' AS Telephone ,  
            '' AS Mobile ,  
            '' AS Fax ,  
            '' AS Email ,  
            '' AS HouseNumber ,  
            '' AS FlatNumber ,  
            '' AS Address1 ,  
            '' AS Address2 ,  
            '' AS Address3 ,  
            '' AS TownCity ,  
            '' AS PostCode ,  
            '' AS County ,  
            -1 AS TENANCYID ,  
            'Scheme' AS InfoType
    FROM    P_SCHEME  
	WHERE	P_SCHEME.SCHEMEID = @schemeId

	UNION ALL

	SELECT   
            0 AS CUSTOMERID,  
            BLOCKNAME Title ,  
            'N/A' AS FirstName ,  
            '' AS MiddleName ,  
            '' AS LastName ,  
            '' AS Telephone ,  
            '' AS Mobile ,  
            '' AS Fax ,  
            '' AS Email ,  
            '' AS HouseNumber ,  
            '' AS FlatNumber ,  
            ISNULL(ADDRESS1, '--') AS Address1 ,  
            ISNULL(ADDRESS2, '--') AS Address2 ,  
            ISNULL(ADDRESS3, '--') AS Address3 ,  
            ISNULL(TOWNCITY, '--') AS TownCity ,  
            ISNULL(POSTCODE, '--') AS PostCode ,  
            ISNULL(COUNTY, '--') AS County ,  
            -1 AS TENANCYID ,  
            'Block' AS InfoType
    FROM    P_BLOCK  
	WHERE	P_BLOCK.blockId = @blockId 


END  
    
-- =======================	
-- (8) ISSUE DATE
-- =======================
	SELECT	CONVERT(VARCHAR, issueDate, 103) AS IssueDate 
	FROM	P_LGSR 
	WHERE	HeatingMappingId = @heatingId  
		

-- =======================	
-- (9) DEFECT NOTES
-- =======================
	SELECT  	
		CASE WHEN DefectNotes IS NOT NULL THEN 
			DefectNotes 
		ELSE 
			'--'
	END AS DefectNotes	
	FROM	P_PROPERTY_APPLIANCE_DEFECTS
			LEFT JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
	WHERE CategoryId = (SELECT CategoryId  
						FROM P_DEFECTS_CATEGORY 
						WHERE Description ='General comment')
		AND JournalId = @journalId
		 
	
-- =======================	
-- (10) APPLIANCE INSPECTION ID
-- =======================
	SELECT	COUNT(APPLIANCEINSPECTIONID) as ApplianceTested 
	FROM	P_APPLIANCE_INSPECTION  
	WHERE	JournalId = @journalId

	
-- =================	
-- (11) JSGNUMBER
-- =================
	SELECT	JSGNUMBER 
	FROM	AS_appointments 
	WHERE	JournalId = @journalId  

END
