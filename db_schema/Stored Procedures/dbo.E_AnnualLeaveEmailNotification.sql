USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_SP_GetPOForNotifyUpperLimitOperative]    Script Date: 7/19/2017 11:57:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.E_AnnualLeaveEmailNotification') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_AnnualLeaveEmailNotification AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_AnnualLeaveEmailNotification]
AS
BEGIN


	Declare @T Table (
	Unit nvarchar(100),
	StartDate SMALLDATETIME ,  
	LeavesBooked FLOAT     ,  	           
	LeaveRequested      FLOAT,   
	LeaveTaken         FLOAT,      
	LEAVE_AVAILABLE    FLOAT,      
	ANNUAL_LEAVE_DAYS  FLOAT,      
	CARRY_FWD          FLOAT,      
	BNK_HD            FLOAT,      
	TOTAL_LEAVE       FLOAT,       
	TIME_OFF_IN_LIEU_OWED FLOAT,   
	PART_FULL   INT,
	TOIL FLOAT )

	DECLARE @EMPLOYEEID  INT = 0,
	@EMAIL  nvarchar (100) = '',
	@NAME  nvarchar(100) = '',
	@STARTDATE  smalldatetime ,
	@ENDDATE   smalldatetime  


 
	DECLARE LeaveNotification_CURSOR 
	CURSOR FOR 
	select  e.EMPLOYEEID,e.FIRSTNAME+' '+e.LASTNAME as NAME, ec.WORKEMAIL as EMAIL ,empStart.STARTDATE,empStart.ENDDATE from E__EMPLOYEE e
	cross apply  (select STARTDATE,ENDDATE from EMPLOYEE_ANNUAL_START_END_DATE(e.EMPLOYEEID)) empStart 
	INNER JOIN E_JOBDETAILS jd on e.EMPLOYEEID = jd.EMPLOYEEID
	Inner Join E_CONTACT ec on e.EMPLOYEEID = ec.EMPLOYEEID
	where jd.ACTIVE=1 and jd.ENDDATE IS NULL AND (DATEDIFF(MONTH, GETDATE() , empStart.ENDDATE)<=3 )
	
	OPEN LeaveNotification_CURSOR

	FETCH NEXT FROM LeaveNotification_CURSOR	
	into @EMPLOYEEID, @NAME,@EMAIL ,@STARTDATE, @ENDDATE

	WHILE @@FETCH_STATUS = 0 
    BEGIN 
			
		declare @emailBody nvarchar(max) = ''
		-- compose an email 
		 Insert @T Exec E_LeaveStats @EMPLOYEEID
		
		DECLARE @annual  float = ( SELECT top(1) ANNUAL_LEAVE_DAYS from @T)
		DECLARE @booked  float = ( SELECT top(1) LeavesBooked from @T)
		DECLARE @requested  float = ( SELECT top(1) LeaveRequested from @T)
		DECLARE @available  float = ( SELECT top(1) LEAVE_AVAILABLE from @T)
		DECLARE @carryFwd  float = ( SELECT top(1) ANNUAL_LEAVE_DAYS from @T)
		DECLARE @bankHoliday  float = ( SELECT top(1) BNK_HD from @T)
		DECLARE @total  float = ( SELECT top(1) TOTAL_LEAVE from @T)
		DECLARE @taken  float = ( SELECT top(1) LeaveTaken from @T)

	 	set @emailBody = '<!DOCTYPE html><html><head><title>Queued Purchase Order</title></head>' +
						 '<body>'+
                         '               <style type="text/css">'+
                         '                   .topAllignedCell{vertical-align: top;} '+
                         '                   .bottomAllignedCell{vertical-align: bottom;} '+
                         '               </style> '+
						 '				<div> '+
						 '              <table><tbody><tr><td style="font-family: arial !important; font-size: 12pt !important;">Dear '+ @NAME+' </td></tr>'+
						 '				<tr><td><br/></td></tr>'+
                         '               <tr><td style="font-family: arial !important; font-size: 12pt !important;">Your leave status is as follows:</td></tr><tr><td><br/></td></tr></tbody></table>'+
                         '               <table><tbody> '+
                         '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;">Annual Leave:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+ cast( @annual as NVARCHAR) +'</td></tr> '+                         
                         '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;">Leave Booked:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+cast( ISNULL(@booked,0) as NVARCHAR)+'</td></tr> '+
                         '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;" class="topAllignedCell">Leave Requested:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+cast( ISNULL(@requested,0) as NVARCHAR)+'</td></tr> '+                   
                         '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;">Leave Taken:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+cast( ISNULL(@taken,0) as NVARCHAR)+'</td></tr> '+
                         '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;">Leave Availabe:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+cast( ISNULL(@available,0) as NVARCHAR)+'</td></tr> '+
                         '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;"> Carry Forward:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+cast( ISNULL(@carryFwd,0) as NVARCHAR)+'</td></tr> '+						 
						 '                       <tr><td style="font-family: arial !important; font-size: 12pt !important;"> Total Leave:</td><td style="font-family: arial !important; font-size: 12pt !important;">'+cast( ISNULL(@total,0) as NVARCHAR)+'</td></tr> '+
                         '               </tbody></table><br /><br /> '+
                         '               <table><tbody><tr><td><img src="cid:broadlandImage" alt="Broadland Housing Group" /></td> '+
                         '                           <td style="font-family: arial !important; font-size: 12pt !important;" class="bottomAllignedCell"> '+
                         '                               Broadland Housing Group<br /> '+
                         '                               NCFC, The Jarrold Stand<br /> '+
                         '                               Carrow Road, Norwich, NR1 1HU<br /> '+
                         '                           </td> '+
                         '                       </tr></tbody></table></div></body></html>'



		 --send email 
		EXEC msdb.dbo.sp_send_dbmail  

		@profile_name = 'EMailProfilePO',  
		@recipients = @EMAIL,  
		@body = @emailBody,  
		@subject = 'Leave Notification',
		@body_format = 'HTML' ;  

		FETCH NEXT FROM LeaveNotification_CURSOR INTO @EMPLOYEEID, @NAME,@EMAIL ,@STARTDATE, @ENDDATE

			 
    END 
CLOSE LeaveNotification_CURSOR  
 DEALLOCATE LeaveNotification_CURSOR 

END	

	
