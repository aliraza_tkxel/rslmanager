SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_FAULT_GETCUSTOMERUNREADFAULTSCOUNT
/* ===========================================================================
 '   NAME:           FL_FAULT_GETCUSTOMERUNREADFAULTSCOUNT
 '   DATE CREATED:   22 Oct 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Get the unread fault records count against the specified customer
 '   IN:             @CustomerID,@FaultStatus
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
		@CustomerId INT,
		@FaultStatus VARCHAR(50)
	)
	
AS
	DECLARE
	@SelectClause VARCHAR(8000)
	
	
SET @SelectClause ='SELECT COUNT(FaultLogID) as UnreadFaultCount
FROM FL_FAULT_LOG WHERE CustomerId='+char(10)+Convert(varchar, @CustomerId)+' and IsReported = 0' 


IF @FaultStatus = '0' 
	EXEC(@SelectClause )
ELSE
	BEGIN
			SET @SelectClause= @SelectClause + ' and StatusID='+ CHAR(10) + Convert(varchar, @FaultStatus)
			EXEC(@SelectClause)
	END













GO
