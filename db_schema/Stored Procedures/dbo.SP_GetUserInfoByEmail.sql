SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--<Author : Ahmed Mehmood>
--<Created Date : 24 January 2014>
--<Description : This store procedure returns user information by given email address >
-- =============================================
CREATE PROC [dbo].[SP_GetUserInfoByEmail]
@emailAddress varchar(120)
AS

SELECT  AC_LOGINS.LOGIN , AC_LOGINS.PASSWORD ,E_CONTACT.WORKEMAIL
FROM E_CONTACT INNER JOIN AC_LOGINS ON E_CONTACT.EMPLOYEEID = AC_LOGINS.EMPLOYEEID
WHERE E_CONTACT.WORKEMAIL = @emailAddress
GO
