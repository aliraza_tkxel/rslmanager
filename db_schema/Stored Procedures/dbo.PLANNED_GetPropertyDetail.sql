USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetPropertyDetail]    Script Date: 1/10/2017 7:01:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- EXEC PLANNED_GetPropertyDetail
	--@propertyId = 'A010060001'

-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,30/Oct/2013>
-- Description:	<Description,,PLANNED_GetPropertyDetail >
-- Web Page: ReplacementList.aspx => Get property information 
-- =============================================
IF OBJECT_ID('dbo.[PLANNED_GetPropertyDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetPropertyDetail] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetPropertyDetail](
	@propertyId varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select 
		ISNULL(C_TENANCY.TENANCYID,-1) as TenancyId,		
		ISNULL (G_TITLE.DESCRIPTION,'')+ ISNULL(' ' + C__CUSTOMER.FIRSTNAME ,'') + ISNULL(' ' + C__CUSTOMER.LASTNAME ,'') as TenantName,
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')+ISNULL(', '+P__PROPERTY.TOWNCITY ,'') +' '+ISNULL(P__PROPERTY.POSTCODE  ,'') as Address,
		ISNULL(P__PROPERTY.HOUSENUMBER, '') AS HOUSENUMBER,		
		ISNULL(P__PROPERTY.ADDRESS1,'') as ADDRESS1,
		ISNULL(P__PROPERTY.ADDRESS2,'') as ADDRESS2,
		ISNULL(P__PROPERTY.TOWNCITY,'') as TOWNCITY,
		ISNULL(P__PROPERTY.COUNTY,'') as COUNTY,
		ISNULL(P__PROPERTY.POSTCODE,'') as POSTCODE,
		ISNULL (G_TITLE.DESCRIPTION,'') + ISNULL(' ' + SUBSTRING(C__CUSTOMER.FIRSTNAME,1,1) ,'') + ISNULL(' '+ C__CUSTOMER.LASTNAME ,'') as TenantNameSalutation,
		ISNULL(C_ADDRESS.MOBILE ,'') as Mobile,
		ISNULL(C_ADDRESS.TEL ,'') as Telephone,
		ISNULL(P_SCHEME.SCHEMENAME,'') as SchemeName,
		ISNULL(C_ADDRESS.EMAIL,'') as Email,
		ISNULL(C__CUSTOMER.CUSTOMERID,-1) as CustomerId,
		ISNULL(P_Block.BLOCKNAME, '') + ' ' + ISNULL(P_Block.ADDRESS1, '') + ' ' + ISNULL(P_Block.ADDRESS2, '') + ' ' + ISNULL(P_Block.ADDRESS3, '') AS Block

	From		
		P__PROPERTY 
		LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
		LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
										AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																							AND ENDDATE IS NULL
																					)
		LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
		LEFT JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_ADDRESS.ISDEFAULT = 1  
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
		LEFT JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
		LEFT JOIN P_Block on P__PROPERTY.BLOCKID = P_Block.BLOCKID 

	WHERE P__PROPERTY.PROPERTYID = @propertyId
END
