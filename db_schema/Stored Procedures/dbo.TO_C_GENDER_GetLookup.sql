SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_C_GENDER_GetLookup]
/* 
===============================================================
'   NAME:           TO_C_GENDER_GetLookup
'   DATE CREATED:   07/05/2013
'   CREATED BY:     Nataliya Alexander
'   CREATED FOR:    Broadland Housing
'   PURPOSE:        To retrieve a list of all Gender records
'   IN:             None 
'   OUT: 		    None   
'   RETURN:         Nothing    
'   VERSION:        1.0           
'   COMMENTS:       
'   MODIFIED ON:    
'   MODIFIED BY:    
'   REASON MODIFICATION: 
'===============================================================
*/
AS

WITH    CTE_GENDER ( id, val )
          AS ( SELECT   1 AS id , 
                        'Female' AS val 
               UNION
               SELECT   2 AS id ,
                        'Male' AS val                         
               UNION
               SELECT   3 AS id ,
                        'Prefer not to say' AS val
               UNION
               SELECT   4 AS id ,
                        'Transgender' AS val
             )
    SELECT  id ,
            val
    FROM    CTE_GENDER
    ORDER BY val ASC

GO
