USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetPropertyImages]    Script Date: 03/06/2014 14:07:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC P_GetPropertyImages
--@propertyId = 'BHA0000119'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,, 17 Sep,2013>
-- Description:	<Description,,This stored procedure returns all the images against the property>
-- =============================================

IF OBJECT_ID('dbo.[P_GetPropertyImages]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[P_GetPropertyImages] AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[P_GetPropertyImages] 
	@propertyId as varchar(20)
	,@count int out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 	 	 	 SELECT 
		PA_PROPERTY_ITEM_IMAGES.ImagePath 
		,ImageName
		,'Stock' as Type
		,COnvert(varchar(10),CreatedOn,103) as CreatedOn
		,(ISNULL(E__EMPLOYEE.FirstNAME,'')+' '+ISNULL(E__EMPLOYEE.LastName,'')) as CreatedBy
		,ISNULL(PA_PROPERTY_ITEM_IMAGES.Title,'') Title
	FROM
		PA_PROPERTY_ITEM_IMAGES
		INNER JOIN P__PROPERTY P on PA_PROPERTY_ITEM_IMAGES.SID = P.PropertyPicId
		INNER JOIN E__EMPLOYEE on PA_PROPERTY_ITEM_IMAGES.CreatedBy = E__EMPLOYEE.EmployeeId	
	WHERE 
		PA_PROPERTY_ITEM_IMAGES.PROPERTYID=@propertyId
			
			
			
    SET @count =(	SELECT	COUNT(*) 
					FROM	PA_PROPERTY_ITEM_IMAGES 
					INNER JOIN P__PROPERTY P on PA_PROPERTY_ITEM_IMAGES.SID = P.PropertyPicId
					INNER JOIN E__EMPLOYEE on PA_PROPERTY_ITEM_IMAGES.CreatedBy = E__EMPLOYEE.EmployeeId	
					WHERE 	PA_PROPERTY_ITEM_IMAGES.PROPERTYID=@propertyId	)
END
