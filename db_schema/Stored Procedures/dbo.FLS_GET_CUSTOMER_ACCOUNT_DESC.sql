USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FLS_GET_CUSTOMER_ACCOUNT_DESC]    Script Date: 12/03/2018 14:40:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF OBJECT_ID('dbo.FLS_GET_CUSTOMER_ACCOUNT_DESC') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_GET_CUSTOMER_ACCOUNT_DESC AS SET NOCOUNT ON;') 
GO
--EXEC [FLS_GET_CUSTOMER_ACCOUNT_DESC] @TENANCYID = 2161, @FROMDATE = NULL

ALTER     PROCEDURE [dbo].[FLS_GET_CUSTOMER_ACCOUNT_DESC]
    (
      @TENANCYID INT ,
      @FROMDATE SMALLDATETIME = NULL 
    )
AS 
    BEGIN   
   
        SET NOCOUNT ON  
        DECLARE @CURR INT ,
            @SID INT ,
            @BALANCE MONEY  
  
        CREATE TABLE #TEMP
            (
              CID INT IDENTITY(1, 1) ,
              JOURNALID INT ,
              F_TRANSACTIONDATE SMALLDATETIME ,
              PAYMENTSTARTDATE SMALLDATETIME ,
              PAYMENTENDDATE SMALLDATETIME ,
              ITEMTYPE VARCHAR(50) ,
              PTID INT ,
              PAYMENTTYPE VARCHAR(70) ,
              DEBIT MONEY ,
              CREDIT MONEY ,
              BREAKDOWN MONEY ,
              ISDEBIT INT ,
              STATUS VARCHAR(50) ,
              STATUSID INT ,
              ACCOUNTTIMESTAMP DATETIME
            )  
   
        INSERT  INTO #TEMP
                ( JOURNALID ,
                  F_TRANSACTIONDATE ,
                  PAYMENTSTARTDATE ,
                  PAYMENTENDDATE ,
                  ITEMTYPE ,
                  PTID ,
                  PAYMENTTYPE ,
                  DEBIT ,
                  CREDIT ,
                  BREAKDOWN ,
                  ISDEBIT ,
                  STATUS ,
                  STATUSID ,
                  ACCOUNTTIMESTAMP  
                )
                SELECT  J.JOURNALID ,
                        J.TRANSACTIONDATE AS F_TRANSACTIONDATE ,
                        J.PAYMENTSTARTDATE AS PAYMENTSTARTDATE ,
                        J.PAYMENTENDDATE AS PAYMENTENDDATE ,
                        ISNULL(I.DESCRIPTION, ' ') AS ITEMTYPE ,
                        ISNULL(J.PAYMENTTYPE, -1) AS PTID ,
                        Case When P.DESCRIPTION = 'Payment Card' Then PaymentCard.DESCRIPTION
																 ELSE ISNULL(P.DESCRIPTION, ' ') END AS PAYMENTTYPE,		
                        CASE WHEN ISNULL(J.AMOUNT, 0) >= 0
                             THEN ISNULL(J.AMOUNT, 0)
                        END DEBIT ,
                        CASE WHEN ISNULL(J.AMOUNT, 0) < 0
                             THEN ISNULL(J.AMOUNT, 0)
                        END CREDIT ,
                        0 AS BREAKDOWN ,
                        ISNULL(J.ISDEBIT, 1) AS ISDEBIT ,
                        ISNULL(S.DESCRIPTION, ' ') AS STATUS ,
                        J.STATUSID ,
                        J.ACCOUNTTIMESTAMP
                FROM    F_RENTJOURNAL J WITH ( NOLOCK )
						OUTER APPLY (Select F_PAYMENTCARDCODES.DESCRIPTION,F_PAYMENTCARDCODES.CODEID,F_PAYMENTCARDCODES.FileExtensions from  F_PAYMENTCARDDATA 
						INNER JOIN F_PAYMENTCARDFILES ON F_PAYMENTCARDDATA.FILEID =F_PAYMENTCARDFILES.FILEID
						INNER JOIN F_PAYMENTCARDCODES ON  F_PAYMENTCARDFILES.PAYMENTCODE = F_PAYMENTCARDCODES.CODEID
						Where F_PAYMENTCARDDATA.JOURNALID = J.JOURNALID 
						AND '.' + RIGHT(F_PAYMENTCARDFILES.[FILENAME], Len(F_PAYMENTCARDFILES.[FILENAME]) - Charindex('.', F_PAYMENTCARDFILES.[FILENAME])) = F_PAYMENTCARDCODES.FileExtensions
						)PaymentCard
                        LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID
                        LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID
                        LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID
                WHERE   J.TENANCYID = @TENANCYID
                        AND ( J.STATUSID NOT IN ( 1, 4 )
                              OR J.PAYMENTTYPE IN ( 17, 18 )
                            )
                        AND J.ITEMTYPE IN ( 1, 8, 9, 10 )
                        AND ( J.TRANSACTIONDATE >= @FROMDATE
                              OR @FROMDATE IS NULL
                            )
                ORDER BY J.TRANSACTIONDATE DESC ,
                        J.ACCOUNTTIMESTAMP DESC  
   
   
   
        SET @BALANCE = 0.00
        SELECT  @CURR = MAX(CID)
        FROM    #TEMP  
   
        WHILE @CURR > 0 
            BEGIN   
  
                IF ( SELECT STATUSID
                     FROM   #TEMP
                     WHERE  [CID] = @CURR
                   ) <> 1 
                    BEGIN  
                        SET @BALANCE = ( SELECT -ISNULL(DEBIT, 0)
                                                + ABS(ISNULL(CREDIT, 0))
                                         FROM   #TEMP
                                         WHERE  [CID] = @CURR
                                       ) + @BALANCE
                        UPDATE  #TEMP
                        SET     BREAKDOWN = @BALANCE
                        WHERE   [CID] = @CURR  
                    END  
                ELSE 
                    BEGIN  
                        UPDATE  #TEMP
                        SET     BREAKDOWN = @BALANCE
                        WHERE   [CID] = @CURR  
                    END  
                SET @CURR = @CURR - 1  
            END  
  
        SELECT  Top (7) JOURNALID ,
                CONVERT(VARCHAR, F_TRANSACTIONDATE, 103) AS F_TRANSACTIONDATE_TEXT ,
                CONVERT(VARCHAR, PAYMENTSTARTDATE, 103) AS PAYMENTSTARTDATE ,
                CONVERT(VARCHAR, PAYMENTENDDATE, 103) AS PAYMENTENDDATE ,
                ITEMTYPE ,
                PTID ,
                PAYMENTTYPE ,
                CAST(DEBIT AS VARCHAR) AS DEBIT ,
                CAST(CREDIT AS VARCHAR) AS CREDIT ,
                CAST(BREAKDOWN AS VARCHAR) AS BREAKDOWN ,
                ISDEBIT ,
                STATUS ,
                STATUSID
        FROM    #TEMP
        ORDER BY CID ASC
        DROP TABLE #TEMP  
    END