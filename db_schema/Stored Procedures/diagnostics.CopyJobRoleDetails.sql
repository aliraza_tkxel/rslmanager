SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SELECT  
--		jrt.JobRoleTeamId,
--		jr.* ,
--        t.TEAMNAME
--FROM    dbo.E_JOBROLE jr
--        INNER JOIN dbo.E_JOBROLETEAM jrt ON jr.JobRoleId = jrt.JobRoleId
--        INNER JOIN dbo.E_TEAM t ON jrt.TeamId = t.TEAMID
--WHERE
--		jr.JobeRoleDescription like '%Information%'
		

CREATE PROC diagnostics.CopyJobRoleDetails
    (
      @JobRoleTeamIdFrom INT ,
      @JobRoleTeamIdTo INT ,
      @ActionedByEmployeeId INT 
    )
AS 
    DECLARE @p1 dbo.TEAMJOBROLE_MODULES_ACCESSRIGHTS
    INSERT  INTO @p1
            SELECT  @JobRoleTeamIdTo ,
                    ModuleId
            FROM    dbo.AC_MODULES_ACCESS
            WHERE   JobRoleTeamId = @JobRoleTeamIdFrom


    DECLARE @p2 dbo.TEAMJOBROLE_MENU_ACCESSRIGHTS
    INSERT  INTO @p2
            SELECT  @JobRoleTeamIdTo ,
                    MenuId
            FROM    dbo.AC_MENUS_ACCESS
            WHERE   JobRoleTeamId = @JobRoleTeamIdFrom


    DECLARE @p3 dbo.TEAMJOBROLE_PAGE_ACCESSRIGHTS
    INSERT  INTO @p3
            SELECT  @JobRoleTeamIdTo ,
                    PageId
            FROM    dbo.AC_PAGES_ACCESS
            WHERE   JobRoleTeamId = @JobRoleTeamIdFrom


    DECLARE @p6 BIT
    SET @p6 = NULL
    EXEC JRA_SaveModuleAccessRights @moduleIdList = @p1, @menuIdList = @p2,
        @pageIdList = @p3, @teamJobRoleId = @JobRoleTeamIdTo,
        @userId = @ActionedByEmployeeId, @isSaved = @p6 OUTPUT
    SELECT  @p6

    DECLARE @p7 dbo.TEAMJOBROLE_INTRANET_PAGE_ACCESSRIGHTS
    INSERT  INTO @p7
            SELECT  @JobRoleTeamIdTo ,
                    PageId
            FROM    dbo.I_PAGE_TEAMJOBROLE
            WHERE   TeamJobRoleId = @JobRoleTeamIdFrom


    DECLARE @p8 BIT
    SET @p8 = NULL
    EXEC JRA_SaveIntranetPageAccessRights @pageIdList = @p7,
        @teamJobRoleId = @JobRoleTeamIdTo, @userId = @ActionedByEmployeeId,
        @isSaved = @p8 OUTPUT
    SELECT  @p8


    DECLARE @p9 dbo.TEAMJOBROLE_ALERTS_ACCESSRIGHTS
    INSERT  INTO @p9
            SELECT  @JobRoleTeamIdTo ,
                    AlertId
            FROM    dbo.E_TEAMALERTS
            WHERE   JobRoleTeamId = @JobRoleTeamIdFrom


    EXEC JRA_SaveAlertsAccessRightsForTeam @teamJobRoleId = @JobRoleTeamIdTo,
        @userId = @ActionedByEmployeeId, @alertIdList = @p9

GO
