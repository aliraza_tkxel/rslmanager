SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_PAGE_UPDATE]
(
	@MenuID int,
	@WebsiteID int,
	@Title varchar(50),
	@Name varchar(50),
	@FileName varchar(50),
	@PageContent text,
	@Active bit,
	@IsBespoke bit,
	@PageID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_PAGE] SET [MenuID] = @MenuID, [WebsiteID] = @WebsiteID, [Title] = @Title, [Name] = @Name, [FileName] = @FileName, [PageContent] = @PageContent, [Active] = @Active, [IsBespoke] = @IsBespoke WHERE (([PageID] = @PageID));
	
SELECT PageID, MenuID, WebsiteID, Title, Name, FileName, PageContent, Active, IsBespoke FROM I_PAGE WHERE (PageID = @PageID)
GO
