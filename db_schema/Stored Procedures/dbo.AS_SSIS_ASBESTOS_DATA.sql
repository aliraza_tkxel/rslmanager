SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[AS_SSIS_ASBESTOS_DATA]
AS 
    SELECT  pr.PROPERTYID ,
            ISNULL(pp.HOUSENUMBER, '') AS [House Number] ,
            ISNULL(pp.ADDRESS1, '') AS [Address Line 1] ,
            ISNULL(pp.ADDRESS2, '') AS [Address Line 2] ,
            ISNULL(pp.ADDRESS3, '') AS [Address Line 3] ,
            ISNULL(pp.TOWNCITY, '') AS [Town/City] ,
            ISNULL(pp.COUNTY, '') AS [County] ,
            ISNULL(pp.Postcode, '') AS [Postcode] ,
            S.DESCRIPTION AS [Status] ,
            PT.DESCRIPTION AS [Type] ,
            pl.asbRisklevelDescription AS [Risk Level] ,
            p.RiskDescription AS [Element] ,
            par.ASBRiskId AS [Risk] ,
            ISNULL(CONVERT(VARCHAR, pr.DateAdded, 103), '') AS [Date Added] ,
            ISNULL(CONVERT(VARCHAR, pr.DateRemoved, 103), '') AS [Date Removed] ,
            CAST(ISNULL(pr.Notes, '') AS VARCHAR(255)) AS Notes ,
            ISNULL(E.FIRSTNAME, '') AS [First Name] ,
            ISNULL(E.LASTNAME, '') AS [Last Name]
    FROM    P_PROPERTY_ASBESTOS_RISKLEVEL PR
            INNER JOIN P_ASBRISKLEVEL PL ON PR.ASBRISKLEVELID = PL.ASBRISKLEVELID
            INNER JOIN P_ASBESTOS P ON PR.ASBESTOSID = P.ASBESTOSID
            INNER JOIN dbo.P__PROPERTY PP ON PP.PROPERTYID = PR.PROPERTYID
            LEFT JOIN P_PROPERTY_ASBESTOS_RISK PAR ON PR.PROPASBLEVELID = PAR.PROPASBLEVELID
            LEFT JOIN dbo.E__EMPLOYEE E ON E.EMPLOYEEID = PR.UserID
            LEFT JOIN dbo.P_STATUS S ON S.STATUSID = PP.STATUS
            LEFT JOIN dbo.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = PP.PROPERTYTYPE
    WHERE   PP.STATUS NOT IN ( 9, 5, 6 )
    ORDER BY 
			pp.PropertyId ,
            pl.asbRisklevelDescription ,
            p.RiskDescription
	
GO
