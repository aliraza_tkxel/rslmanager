SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[I_NEWS_GET_BY_NEWSID]
@NEWSID INT
AS
SET NOCOUNT ON;
SELECT     NewsCategory, NewsID, Headline, ImagePath, DateCreated, NewsContent, Rank, Active,HomePageImage, IsHomePage,IsTenantOnline
FROM         I_NEWS WHERE NEWSID = @NEWSID
GO
