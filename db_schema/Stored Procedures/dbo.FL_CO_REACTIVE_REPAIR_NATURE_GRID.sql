SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO




CREATE PROCEDURE dbo.FL_CO_REACTIVE_REPAIR_NATURE_GRID 

/* ===========================================================================
 '   NAME:           FL_REPORTEDFAULTSEARCHLIST
 '   DATE CREATED:   20TH February, 2009
 '   CREATED BY:     Tahir Gul 
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To fetch all the repairs against a particular faultLogID from FL_FAULT-REPAIR_LIST
 '   IN:             @faultLogID int
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
 @faultLogID int
)

AS

SELECT  FL_FAULT_REPAIR_LIST.[Description]
FROM FL_FAULT_LOG 
INNER JOIN FL_CO_FAULTLOG_TO_REPAIR 
ON  FL_FAULT_LOG.FaultLogID = FL_CO_FAULTLOG_TO_REPAIR.FaultLogID
INNER JOIN FL_FAULT_REPAIR_LIST
ON FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID = FL_FAULT_REPAIR_LIST.FaultRepairListID
WHERE FL_FAULT_LOG.FaultLogID = @faultLogID



GO
