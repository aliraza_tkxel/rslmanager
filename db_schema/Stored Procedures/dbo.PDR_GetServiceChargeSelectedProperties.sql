USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyByBlockId]    Script Date: 10/30/2017 19:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PDR_GetServiceChargeSelectedProperties]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetServiceChargeSelectedProperties] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetServiceChargeSelectedProperties]
	@schemeId int,
	@blockId int,
	@itemId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@schemeId is not null AND @blockId IS NOT NULL)
	BEGIN
		select propertyId as PropertyId
		from PDR_ServiceChargeProperties
		where (schemeid=@schemeId) and (blockid=@blockId)
		and isincluded = 1 and itemId=@itemId and IsActive = '1'
	END
	ELSE IF(@schemeId is not null AND @blockId IS NULL)
	BEGIN
		select propertyId as PropertyId
		from PDR_ServiceChargeProperties
		where (schemeid=@schemeId) and (blockid IS NULL)
		and isincluded = 1 and itemId=@itemId and IsActive = '1'
	END
	ELSE IF(@schemeId is null)
	BEGIN
		select propertyId as PropertyId
		from PDR_ServiceChargeProperties
		where (blockid=@blockId) AND SCHEMEID IS NULL
		and isincluded = 1 and itemId=@itemId and IsActive = '1'
	END

END
