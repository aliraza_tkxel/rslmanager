SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_STATIONERY_ORDERPRIORITY_UPDATE]
(
	@Priority varchar(20),
	@Original_StationeryOrderPriorityId smallint,
	@StationeryOrderPriorityId smallint
)
AS
	SET NOCOUNT OFF;
UPDATE [I_STATIONERY_ORDER_PRIORITY] SET [Priority] = @Priority WHERE (([StationeryOrderPriorityId] = @Original_StationeryOrderPriorityId));
	
SELECT Priority, StationeryOrderPriorityId FROM I_STATIONERY_ORDER_PRIORITY WHERE (StationeryOrderPriorityId = @StationeryOrderPriorityId)

GO
