SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* ===========================================================================  
-- EXEC PLANNED_GetStatusLookUp
--  Author:			Muhammad Awais
--  DATE CREATED:	02 February 2015  
--  Description:	To Get PLANNED_STATUS Look Up List for Job Sheet Summary - Update
--  Webpage:		View/Reports/AssignedToContactor.aspx  
 '==============================================================================*/  
CREATE PROCEDURE [dbo].[PLANNED_GetStatusLookUp] 
AS  
BEGIN  
   
 SET NOCOUNT ON;  
  
    SELECT StatusID as id, Title AS val  
      FROM PLANNED_STATUS
     WHERE Title IN ( 'Cancelled', 'Completed', 'Assigned To Contractor' )

END  
GO
