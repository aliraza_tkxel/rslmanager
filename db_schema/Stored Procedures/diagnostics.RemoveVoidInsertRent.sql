SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--EXEC diagnostics.RemoveVoidInsertRent 
--	@TransactionId = <TransactionId, int, > -- e.g.: 20080101 (yyyymm01) -- always the first of the month when the void entry went in
--	@PropertyId = <PropertyId, varchar(25), >
--	@TransactionDate = <TransactionDate, datetime, > -- This is TransactionId as date e.g.: '20080101' and is always the first of the month
--	@TenancyId = <TenancyId, int, > -- TenancyId
--	@LastRentTransactionDate = <LastRentTransactionDate, datetime, > --The first date of the month when the last rent was inserted e.g.: '20071201'

CREATE PROCEDURE [diagnostics].[RemoveVoidInsertRent]
    (
      @TransactionId INT ,
      @PropertyId VARCHAR(25) ,
      @TransactionDate SMALLDATETIME ,
      @TenancyId INT ,
      @LastRentTransactionDate SMALLDATETIME
    )
AS 
    BEGIN 

        DECLARE @JOURNALID INT

        PRINT 'Remove Void and Rent entries from rent journal and nominal ledger.'
	
        DELETE  FROM F_RENTJOURNAL_VOIDS
        WHERE   PROPERTYID = @PROPERTYID
                AND TRANSACTIONDATE = @TransactionDate
        DELETE  FROM NL_JOURNALENTRYCREDITLINE
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TRANSACTIONID )
                                    AND TRANSACTIONTYPE = 7 ) -- 7 = rent
        DELETE  FROM NL_JOURNALENTRYDEBITLINE
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TRANSACTIONID )
                                    AND TRANSACTIONTYPE = 7 ) -- 7 = rent
        DELETE  FROM NL_JOURNALENTRY
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TRANSACTIONID )
                                    AND TRANSACTIONTYPE = 7 )
                                    
        DELETE  FROM NL_JOURNALENTRYCREDITLINE
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TRANSACTIONID )
                                    AND TRANSACTIONTYPE = 10 ) -- 10 = void
        DELETE  FROM NL_JOURNALENTRYDEBITLINE
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TRANSACTIONID )
                                    AND TRANSACTIONTYPE = 10 ) -- 10 = rent
        DELETE  FROM NL_JOURNALENTRY
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TRANSACTIONID )
                                    AND TRANSACTIONTYPE = 10 )


        PRINT 'Insert entry in rent journal.'
	
        INSERT  INTO F_RENTJOURNAL
                ( TENANCYID ,
                  TRANSACTIONDATE ,
                  ITEMTYPE ,
                  PAYMENTTYPE ,
                  PAYMENTSTARTDATE ,
                  PAYMENTENDDATE ,
                  AMOUNT ,
                  STATUSID 
                )
                SELECT  TENANCYID ,
                        @TransactionDate ,
                        ITEMTYPE ,
                        PAYMENTTYPE ,
                        @TransactionDate ,
                        dbo.udfGetLastDayOfMonth(@TransactionDate) ,
                        Amount ,
                        StatusId
                FROM    dbo.F_RENTJOURNAL
                WHERE   TENANCYID = @TenancyId
                        AND ITEMTYPE = 1
                        AND TRANSACTIONDATE = @LastRentTransactionDate

        SET @JOURNALID = SCOPE_IDENTITY()
	
        PRINT 'Insert entry in rent monthly journal.'

        INSERT  INTO F_RENTJOURNAL_MONTHLY
                ( JOURNALID ,
                  TENANCYID ,
                  TRANSACTIONDATE ,
                  AMOUNT ,
                  RENT ,
                  [SERVICES] ,
                  COUNCILTAX ,
                  WATERRATES ,
                  INELIGSERV ,
                  SUPPORTEDSERVICES ,
                  GARAGE ,
                  TOTALRENT ,
                  PROPERTYID ,
                  RENTTYPE ,
                  PROPERTYTYPE
                )
                SELECT  @JOURNALID ,
                        @TENANCYID ,
                        @TransactionDate ,
                        AMOUNT ,
                        RENT ,
                        [SERVICES] ,
                        COUNCILTAX ,
                        WATERRATES ,
                        INELIGSERV ,
                        SUPPORTEDSERVICES ,
                        GARAGE ,
                        TOTALRENT ,
                        PROPERTYID ,
                        RENTTYPE ,
                        PROPERTYTYPE
                FROM    dbo.F_RENTJOURNAL_MONTHLY
                WHERE   TENANCYID = @TENANCYID
                        AND PROPERTYID = @PROPERTYID
                        AND TRANSACTIONDATE = @LastRentTransactionDate

		PRINT 'Run the rent procedure to update nominal ledger.'
		
        EXEC NL_MONTHLYRENT @TransactionDate
        SELECT  SUM(AMOUNT)
        FROM    NL_JOURNALENTRYCREDITLINE
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TransactionId )
                                    AND TRANSACTIONTYPE = 7 )

		PRINT 'Run the void procedure to update nominal ledger.'
		
        EXEC NL_VOIDRENTS @TransactionDate
        SELECT  SUM(AMOUNT)
        FROM    NL_JOURNALENTRYCREDITLINE
        WHERE   TXNID IN ( SELECT   TXNID
                           FROM     NL_JOURNALENTRY
                           WHERE    TRANSACTIONID IN ( @TransactionId )
                                    AND TRANSACTIONTYPE = 10 )

    END


GO
