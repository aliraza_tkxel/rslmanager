SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

ALTER  PROCEDURE [dbo].[FL_CO_INVOICING_RowCount]
	(			
	   	@locationId	int	=  NULL,
		@areaId	int	=  NULL,
		@elementId	int = NULL,
		@priorityId	int = NULL,
		@statusId	int = NULL,  
		@orgId		int = NULL,
		@userId	int = NULL,
   		@postCode	nvarchar(10) = NULL,
		@dueDate         nvarchar(20) =   NULL,
		@patchId	int = NULL,
		@schemeId	 int = NULL,
		@stageId	 int = NULL,
			
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int =0,
				
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FL_FAULT_LOG.JobSheetNumber ',
		@sortOrder varchar (5) = 'DESC'
	)
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = ''
        
    IF @locationId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_LOCATION.LocationID= '+ LTRIM(STR(@locationId)) + ' AND'  
    
    
     IF @areaId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'FL_AREA.AreaID = '+ LTRIM(STR(@areaId)) + ' AND'  
    
    
    IF @elementId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_ELEMENT.ElementID = '+ LTRIM(STR(@elementId)) + ' AND'  
    
    IF @priorityId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_PRIORITY.PriorityID = '+ LTRIM(STR(@priorityId)) + ' AND'  
                             
    IF @statusId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_STATUS.FaultStatusID = '+ LTRIM(STR(@statusId)) + ' AND'  
    
   IF @orgId IS NOT NULL
      SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
		'FL_FAULT_LOG.OrgId =' + LTRIM(STR(@orgId)) + ' AND' 
   IF @userId IS NOT NULL
      SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
		'FL_CO_APPOINTMENT.OperativeId = ' + LTRIM(STR(@userId)) + ' AND' 

  IF @postCode IS NOT NULL
     SET  @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
		'P__PROPERTY.POSTCODE = '''+@postCode  + ''' AND' 

  IF @dueDate IS NOT NULL
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
    'Convert(varchar(10),FL_FAULT_LOG.DueDate,101) ='''+ Convert(varchar,@dueDate,120) +''' AND'

  IF @patchId IS NOT NULL
      SET @SearchCriteria =@SearchCriteria + CHAR(10) + CHAR(9) + 
		'E_PATCH.patchId = ' + LTRIM(STR(@patchId)) + ' AND' 

IF @stageId IS NOT NULL 
      SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
		'FL_CO_APPOINTMENT_STAGE.AppointmentStageId = ' + LTRIM(STR(@stageId)) + ' AND'
	
IF @schemeId IS NOT NULL
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
		'P_SCHEME.SCHEMEID  = ' + LTRIM(STR(@schemeId)) + ' AND'                          
    
        
    -- Begin building SELECT clause
    
    
      SET @SelectClause = 'SELECT' +                      
                       CHAR(10) + CHAR(9) + 'COUNT(*) As numOfRows '           
                        
    -- End building SELECT clause
    --========================================================================================    
       
   -- CHAR(10) + CHAR(10) +
    --========================================================================================    
    -- Begin building FROM clause
    
    SET @FromClause =	CHAR(10) + CHAR(10) +   ' FROM ' + 
						CHAR(10) + CHAR(9) + 'FL_LOCATION  INNER JOIN' +
						CHAR(10) + CHAR(9) + 'FL_AREA ON FL_LOCATION.locationId = FL_AREA.locationId INNER JOIN' +
						CHAR(10) + CHAR(9) + 'FL_ELEMENT ON FL_AREA.areaId = FL_ELEMENT.areaId INNER JOIN' +
						CHAR(10) + CHAR(9) + 'FL_FAULT ON FL_ELEMENT.ELEMENTID = FL_FAULT.ELEMENTID INNER JOIN' + 
                      	CHAR(10) + CHAR(9) + 'FL_FAULT_LOG ON FL_FAULT.FAULTID = FL_FAULT_LOG.FAULTID RIGHT  JOIN' +
						CHAR(10) + CHAR(9) + 'FL_CO_FAULTLOG_TO_REPAIR ON FL_FAULT_LOG.FaultLogID = FL_CO_FAULTLOG_TO_REPAIR.FaultLogID RIGHT JOIN' + 
						CHAR(10) + CHAR(9) + 'FL_FAULT_REPAIR_LIST ON FL_CO_FAULTLOG_TO_REPAIR.FAULTREPAIRLISTID = FL_FAULT_REPAIR_LIST.FAULTREPAIRLISTID INNER JOIN' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS  ON FL_FAULT_LOG.STATUSID = FL_FAULT_STATUS.FaultStatusID INNER JOIN' + 
						CHAR(10) + CHAR(9) + 'C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CustomerId LEFT JOIN' +
						CHAR(10) + CHAR(9) +  'G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID INNER JOIN' +
						CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID  AND C_CUSTOMERTENANCY.ENDDATE IS NULL INNER JOIN'+ 
						CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL INNER JOIN'+		
						--CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID  INNER JOIN'+ 
						--CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN'+
						CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID LEFT JOIN'+
						CHAR(10) + CHAR(9) + 'P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID INNER JOIN'+   
						CHAR(10) + CHAR(9) + 'PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID INNER JOIN'+ 
						CHAR(10) + CHAR(9) + 'E_PATCH ON PDR_DEVELOPMENT.PatchId = E_PATCH.PatchId INNER JOIN' +
                      --CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT.JobSheetNumber INNER JOIN' +
						CHAR(10) + CHAR(9) + '(SELECT MAX(OperativeId) OperativeId,JobSheetNumber  FROM FL_CO_APPOINTMENT GROUP BY JobSheetNumber) FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT.JobSheetNumber  INNER JOIN' +
						CHAR(10) + CHAR(9) + 'E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeId = E__EMPLOYEE.EmployeeId INNER JOIN' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.Priorityid = FL_FAULT_PRIORITY.PriorityId'--  INNER JOIN' +
	             -- CHAR(10) + CHAR(9) + 'INNER JOIN FL_CO_APPOINTMENT_STAGE  ON FL_CO_APPOINTMENT.AppointmentStageId = FL_CO_APPOINTMENT_STAGE.AppointmentStageId'
                      
                
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'FL_FAULT_LOG.JobSheetNumber'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , FL_FAULT_LOG.JobSheetNumber '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    
                       
SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( FL_FAULT_LOG.JobSheetNumber NOT IN' + 

                       
                       	 CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' FL_FAULT_LOG.JobSheetNumber ' +  
                        	CHAR(10) + CHAR(9) + @FromClause + 
		CHAR(10) + CHAR(9) + 'WHERE 1 = 1 AND FL_FAULT_STATUS.Description IN (''Works Completed'',''Post Inspected'') ' +  ' AND'  + @SearchCriteria + 
                       	 CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9)  + ' AND ' + 

		--CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.FaultStatusID IN (4,6)' +
                        
                        -- Search Based Criteria added if supplied by user
		 CHAR(10) + CHAR(10) + @SearchCriteria +

		CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.Description IN (''Works Completed'',''Post Inspected'')' +  ' AND ' + 
                       
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                       
                        
    -- End building WHERE clause
    --========================================================================================
        
	
 PRINT (@SelectClause + @FromClause + @WhereClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause)
 
 --PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)







GO
