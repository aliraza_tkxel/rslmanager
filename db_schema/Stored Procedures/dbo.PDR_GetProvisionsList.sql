USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionsList]    Script Date: 10/25/2018 11:46:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen
-- Create date: 1st December 2015
-- Description:	Provision List for Provision Report
-- =============================================

IF OBJECT_ID('dbo.[PDR_GetProvisionsList]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_GetProvisionsList] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetProvisionsList]
( 
		@filterText VARCHAR(200),
		@categoryId int = 0,
	-- Add the parameters for the stored procedure here
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'PROVISIONID', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
		--@YStart varchar(100),
		--@YEnd varchar(100),

        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
		
		--variables for paging
        @offset int,
		@limit int
		
		
		SET @searchCriteria = ' 1=1 '
		IF(@filterText != '' OR @filterText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND '+  @filterText
		END	
	
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
				
		SET @SelectClause = 'Select top ('+convert(nvarchar(10),@limit)+')
							 ISNULL(S.SCHEMENAME, ''-'') as SCHEMENAME
							,ISNULL(B.BLOCKNAME,''-'') as BLOCKNAME
							,PC.CategoryName AS CATEGORYNAME
							,ISNULL(p.ProvisionDescription,''-'') as PROVISIONDESCRIPTION
							
							,ISNULL(CAST (p.PurchasedDate as DATE),'' '') as PURCHASEDDATE
							,ISNULL(p.LifeSpan,'' '') as LIFESPAN
							,case when p.AnnualApportionmentPC is null then 0 else ISNULL(CAST(p.AnnualApportionmentPC AS DECIMAL(18, 1)),''-'') end  as ANNUALAPPOINTMENT 
							
							,ISNULL(Format(p.ReplacementDue,''MMMM yyyy''),'' '') as REPLACEMENTDUE
							
							,p.ProvisionId as PROVISIONID
							,(select COUNT(*) from PDR_ProvisionChargeProperties where IsIncluded = 1 and (IsActive = ''1'') and ItemId=p.ProvisionId) as IncludedCount
							
							,''�'' + ISNULL(CONVERT(varchar, CAST(p.AnnualApportionmentPC AS money), 1), ''0.00'') as BUDGET 

							,''�'' +ISNULL(CONVERT(varchar,ISNULL(CAST(p.AnnualApportionmentPC AS money), 0)/NULLIF((ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0)),0),1), ''0.00'') as APPORTIONMENTBUDGET
							,case when B.BLOCKNAME is null then ''�'' + ISNULL(CONVERT(varchar, CAST(scPOInfo.ActualTotal AS money), 1), ''0.00'')  when S.SCHEMENAME is null then ''�'' + ISNULL(CONVERT(varchar, CAST(blPOInfo.ActualTotal AS money), 1), ''0.00'') END as ACTUALTOTAL
							,case 
							when B.BLOCKNAME is null then ''�'' +ISNULL(CONVERT(varchar,ISNULL(CAST(scPOInfo.ActualTotal AS money), 0)/NULLIF((ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0)),0),1), ''0.00'') 
							when S.SCHEMENAME is null then ''�'' +ISNULL(CONVERT(varchar,ISNULL(CAST(blPOInfo.ActualTotal AS money), 0)/NULLIF((ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0)),0),1), ''0.00'') END as APPORTIONMENTACTUAL
							,(SELECT 
                                COUNT(*) AS Count
							    FROM   P__PROPERTY PI
		                        where PI.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ProvisionChargeProperties P1 
								where P1.IsIncluded=1 AND P1.SCHEMEID=p.SchemeId AND P1.IsActive=1 AND P1.ItemID=p.ProvisionId
								GROUP BY P1.BLOCKID, P1.ItemId, P1.PROPERTYID)	and PI.SCHEMEID=p.SchemeId
								) + 
								(SELECT 
                                COUNT(*) AS Count
							    FROM   P__PROPERTY PI
		                        where PI.PROPERTYID not in (select MAX(P1.PROPERTYID) AS PROPERTYID
								from dbo.PDR_ProvisionChargeProperties P1 
								where P1.IsIncluded=1 AND P1.IsActive=1 AND P1.BLOCKID=p.BLOCKID AND P1.ItemID=p.ProvisionId
								GROUP BY P1.BLOCKID, P1.ItemId, P1.PROPERTYID)	and PI.BLOCKID=p.BLOCKID
								)
								 as ExPropCount
							,ISNULL(inProp.count, 0) + ISNULL(inProp1.count, 0) as InPropCount
							,ISNULL(P.ProvisionID, 0)  as ITEMID
							,ISNULL(P.SCHEMEID,0) AS SCHEMEID
							,ISNULL(P.BLOCKID,0) AS BLOCKID							
							'
			
		SET @fromClause = CHAR(10) +' FROM PDR_Provisions p
									
								LEFT JOIN PDR_ProvisionCategories PC ON PC.ProvisionCategoryId = P.ProvisionCategoryId
								   LEFT JOIN P_SCHEME S ON S.SCHEMEID = p.SchemeId 
								   LEFT JOIN P_BLOCK B on B.BLOCKID= p.BlockId

								   LEFT JOIN (SELECT COUNT(prop.BlockID) AS PropertyCount, prop.BLOCKID FROM P__PROPERTY prop GROUP BY prop.BLOCKID) PI ON B.BLOCKID = PI.BLOCKID
										LEFT JOIN (SELECT sum(FI.GROSSCOST) as ActualTotal,SI.SchemeId,ISC.ItemID ,ISC.ItemName
											  FROM CM_ServiceItems SI
											  INNER JOIN P_SCHEME S ON SI.SchemeId=S.SCHEMEID 
											  INNER JOIN PA_ITEM ISC ON SI.ItemId = ISC.ItemID
											  INNER JOIN F_PURCHASEORDER FP on SI.PORef = FP.ORDERID
											  INNER JOIN F_PURCHASEITEM  FI on FP.ORDERID=FI.ORDERID
											  Where FP.PODATE between CONVERT(Datetime, ''' + ''', 120) and CONVERT(Datetime, ''' + ''', 120)
											  group by SI.SCHEMEID,ISC.ItemID,ISC.ItemName
									) scPOInfo on (scPOInfo.SCHEMEID=p.SchemeId) and p.ProvisionId=scPOInfo.ItemID
									
									LEFT JOIN (SELECT sum(FI.GROSSCOST) as ActualTotal,SI.BLOCKID,IBL.ItemID ,IBL.ItemName
												  FROM CM_ServiceItems SI
												  INNER JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID
												  INNER JOIN PA_ITEM IBL ON SI.ItemId = IBL.ItemID
												  INNER JOIN F_PURCHASEORDER FP on SI.PORef = FP.ORDERID
												  INNER JOIN F_PURCHASEITEM  FI on FP.ORDERID=FI.ORDERID
												  Where FP.PODATE between CONVERT(Datetime, ''' + ''', 120) and CONVERT(Datetime, ''' + ''', 120)
												  group by SI.BLOCKID,IBL.ItemID,IBL.ItemName) blPOInfo on (blPOInfo.BLOCKID= p.BlockId) and p.ProvisionId=blPOInfo.ItemID
								   
								   LEFT JOIN (SELECT DISTINCT COUNT(*) AS Count, SCHEMEID, itemid
												FROM PDR_ProvisionChargeProperties
												WHERE isincluded=0 and IsActive=1
												GROUP BY SCHEMEID, itemid) exProp on (exProp.SCHEMEID=p.SchemeId) and p.ProvisionId=exProp.itemid
								   LEFT JOIN (SELECT COUNT(*) AS Count, BLOCKID, itemid
												FROM PDR_ProvisionChargeProperties
												WHERE isincluded=0 and IsActive=1
												and schemeid is null
												GROUP BY BLOCKID, itemid) exProp1 on (exProp1.BLOCKID=p.BLOCKID) and p.ProvisionId=exProp1.itemid 
								   LEFT JOIN (SELECT DISTINCT COUNT(*) AS Count, SCHEMEID, itemid
												FROM PDR_ProvisionChargeProperties
												WHERE isincluded=1 and IsActive=1
												GROUP BY SCHEMEID, itemid) inProp on (inProp.SCHEMEID=p.SchemeId) and p.ProvisionId=inProp.itemid
									LEFT JOIN (SELECT COUNT(*) AS Count, BLOCKID, itemid
												FROM PDR_ProvisionChargeProperties
												WHERE isincluded=1 and IsActive=1
												and schemeid is null
												GROUP BY BLOCKID, itemid) inProp1 on (inProp1.BLOCKID=p.BLOCKID) and p.ProvisionId=inProp1.itemid
								   '
								   
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder						   
		
		SET @whereClause =	CHAR(10) + 'WHERE P.ProvisionCharge = 1 AND '

		If(@categoryId != 0)
		Begin
			SET @whereClause =	@whereClause + CHAR(10) + ' P.ProvisionCategoryId = ' + CONVERT(varchar, (@categoryId)) + ' AND '
		End

		SET @whereClause =	@whereClause + CHAR(10) + @searchCriteria 
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END

