USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyEmployeeEmail]    Script Date: 14/04/2017 09:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- EXEC AS_GetPropertyEmployeeEmail
		--@@employeeId=1226
		
-- Author:		Saud Ahmed
-- Create date: 14/04/2017
-- Description:	<This stored procedure returns the Operative Email that appointment was create for>
-- [AS_GetPropertyEmployeeEmail] 1226	

-- =============================================

IF OBJECT_ID('dbo.AS_GetPropertyEmployeeEmail') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_GetPropertyEmployeeEmail AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[AS_GetPropertyEmployeeEmail]
( 
	@employeeId nvarchar(50) = ''
)
AS
BEGIN

select WORKEMAIL from E_CONTACT where EMPLOYEEID = @employeeId

END

