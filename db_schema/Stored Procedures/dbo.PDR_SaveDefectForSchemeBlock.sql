USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveDefect]    Script Date: 4/5/2018 3:22:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PDR_SaveDefectForSchemeBlock') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_SaveDefectForSchemeBlock AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_SaveDefectForSchemeBlock]  
 -- Add the parameters for the stored procedure here
 @propertyDefectId INT 
 ,@id INT
 ,@requestType VARCHAR(20)
 ,@HeatingMappingId int
 ,@CategoryId INT 
 ,@DefectDate SMALLDATETIME = null
 ,@isDefectIdentified BIT=null
 ,@DefectIdentifiedNotes NVARCHAR(200)= null
 ,@isRemedialActionTaken BIT= null
 ,@remedialActionNotes NVARCHAR(200)= null
 ,@isWarningIssued BIT= null
 ,@serialNumber NVARCHAR(20)= null
 ,@GcNumber NVARCHAR(9)= null
 ,@isWarningFixed BIT= null
 ,@isApplianceDisconnected BIT= null
 ,@isPartsRequired BIT= null
 ,@isPartsOrdered BIT= null
 ,@partsOrderedBy INT= null
 ,@partsDueDate DATE= null
 ,@partsDescription NVARCHAR(1000)= null
 ,@partsLocation NVARCHAR(200)= null
 ,@isTwoPersonsJob BIT= null
 ,@reasonForSecondPerson NVARCHAR(1000)= null
 ,@estimatedDuration DECIMAL(9,2)= null
 ,@priorityId INT= null
 ,@tradeId INT= null
 ,@filePath VARCHAR(500)= null 
 ,@photoName VARCHAR(100)= null
 ,@PhotoNotes VARCHAR(1000)= null
 ,@userId INT= null
 
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @Now DATETIME2 = GETDATE()

IF NOT EXISTS
(
	SELECT
		PropertyDefectId
	FROM
		P_PROPERTY_APPLIANCE_DEFECTS
	WHERE PropertyDefectId = @propertyDefectId
) 
BEGIN

DECLARE @JournalId INT
DECLARE @BoilerTypeId INT

select top 1 @BoilerTypeId = PVMAN.ValueID
from PA_HeatingMapping HM
LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES MANUF ON MANUF.HeatingMappingId = HM.HeatingMappingId
		AND MANUF.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Boiler Type' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVMAN ON MANUF.VALUEID = PVMAN.ValueID
		where hm.HeatingMappingId=@HeatingMappingId
-- Insert statements for procedure here  

DECLARE @StatusId INT
SELECT @StatusId = STATUSID from PDR_STATUS where TITLE like 'To Be Approved'

	if @requestType = 'block'
	begin
		SELECT TOP 1
			@JournalId = AS_JOURNAL.JournalId
		FROM
			AS_JOURNAL
			INNER JOIN AS_JournalHeatingMapping ON AS_JournalHeatingMapping.JournalId = AS_JOURNAL.JOURNALID
		WHERE
			AS_JOURNAL.BlockId = @id AND AS_JournalHeatingMapping.HeatingMappingId = @HeatingMappingId
			AND ISCURRENT = 1

		INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS (
				[BlockId]
				,[CategoryId]
				,[JournalId]
				,[HeatingMappingId]
				,[IsDefectIdentified]
				,[DefectNotes]
				,[IsActionTaken]
				,[ActionNotes]
				,[IsWarningIssued]
				,[BoilerTypeId]
				,[SerialNumber]
				,[IsWarningFixed]
				,[DefectDate]
				,[CreatedBy]
				,[PhotoNotes]
				,[GasCouncilNumber]
				,[IsDisconnected]
				,[IsPartsrequired]
				,[IsPartsOrdered]
				,[PartsOrderedBy]
				,[PartsDue]
				,[PartsDescription]
				,[PartsLocation]
				,[IsTwoPersonsJob]
				,[ReasonFor2ndPerson]
				,[Duration]
				,[Priority]
				,[TradeId]
				,CreatedDate
				,DateCreated
				, DefectJobSheetStatus
				)           
		VALUES (
				@id, @categoryId, @JournalId, @HeatingMappingId
				,@isDefectIdentified, @DefectIdentifiedNotes
				,@isRemedialActionTaken, @remedialActionNotes
				,@isWarningIssued
				,@BoilerTypeId
				,@serialNumber
				,@isWarningFixed
				,@DefectDate
				,@userId
				,@PhotoNotes
				,@GcNumber
				,@isApplianceDisconnected
				,@isPartsRequired , @isPartsOrdered
				,@partsOrderedBy ,@partsDueDate
				,@partsDescription ,@partsLocation
				,@isTwoPersonsJob, @reasonForSecondPerson
				,@estimatedDuration ,@priorityId
				,@tradeId
				,@Now
				,@Now
				,@StatusId
			)
			IF  @photoName <> ''
			BEGIN
				INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS_IMAGES (
								ImageTitle
								,ImagePath
								,PropertyDefectId
							)
					VALUES (
							@photoName, @filePath, SCOPE_IDENTITY()
						)
			END			
	END
	ELSE
	BEGIN
		SELECT TOP 1
			@JournalId = AS_JOURNAL.JournalId
		FROM
			AS_JOURNAL
			INNER JOIN AS_JournalHeatingMapping ON AS_JournalHeatingMapping.JournalId = AS_JOURNAL.JOURNALID
		WHERE
			AS_JOURNAL.SchemeId = @id AND AS_JournalHeatingMapping.HeatingMappingId = @HeatingMappingId
			AND ISCURRENT = 1
		
		INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS (
				[SchemeId]
				,[CategoryId]
				,[JournalId]
				,[HeatingMappingId]
				,[IsDefectIdentified]
				,[DefectNotes]
				,[IsActionTaken]
				,[ActionNotes]
				,[IsWarningIssued]
				,[BoilerTypeId]
				,[SerialNumber]
				,[IsWarningFixed]
				,[DefectDate]
				,[CreatedBy]
				,[PhotoNotes]
				,[GasCouncilNumber]
				,[IsDisconnected]
				,[IsPartsrequired]
				,[IsPartsOrdered]
				,[PartsOrderedBy]
				,[PartsDue]
				,[PartsDescription]
				,[PartsLocation]
				,[IsTwoPersonsJob]
				,[ReasonFor2ndPerson]
				,[Duration]
				,[Priority]
				,[TradeId]
				,CreatedDate
				,DateCreated
				, DefectJobSheetStatus
				)           
		VALUES (
				@id, @categoryId, @JournalId, @HeatingMappingId
				,@isDefectIdentified, @DefectIdentifiedNotes
				,@isRemedialActionTaken, @remedialActionNotes
				,@isWarningIssued
				,@BoilerTypeId
				,@serialNumber
				,@isWarningFixed
				,@DefectDate
				,@userId
				,@PhotoNotes
				,@GcNumber
				,@isApplianceDisconnected
				,@isPartsRequired , @isPartsOrdered
				,@partsOrderedBy ,@partsDueDate
				,@partsDescription ,@partsLocation
				,@isTwoPersonsJob, @reasonForSecondPerson
				,@estimatedDuration ,@priorityId
				,@tradeId
				,@Now
				,@Now
				,@StatusId
			)
			IF  @photoName <> ''
			BEGIN
				INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS_IMAGES (
								ImageTitle
								,ImagePath
								,PropertyDefectId
							)
					VALUES (
							@photoName, @filePath, SCOPE_IDENTITY()
						)
			END			
	END
END
ELSE
BEGIN

	UPDATE P_PROPERTY_APPLIANCE_DEFECTS
		SET IsPartsrequired = @isPartsRequired
			,IsPartsOrdered = @isPartsOrdered
			,PartsDue = @partsDueDate
			,PartsDescription = @partsDescription
			,PartsLocation = @partsLocation
			,IsTwoPersonsJob = @isTwoPersonsJob
			,ReasonFor2ndPerson = @reasonForSecondPerson
			,Duration = @estimatedDuration
			,[Priority] = @priorityId
			,TradeId = @tradeId
			,ModifiedBy = @userId
			,ModifiedDate = @Now
			
	WHERE PropertyDefectId = @propertyDefectId
	
END

END