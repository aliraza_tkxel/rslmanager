SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
--Exec FL_GETFAULTREPAIRLIST 'rear'
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,11/Dec/2013>
-- Description:	<Description,Get fault repair list>
-- =============================================
CREATE PROCEDURE [dbo].[FL_GETFAULTREPAIRLIST]
	@searchText nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT FaultRepairListID, [Description] FROM FL_FAULT_REPAIR_LIST  
	where Description like '%'+@searchText+'%'  
END
GO
