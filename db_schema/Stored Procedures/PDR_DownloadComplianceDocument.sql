USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Fakhar uz zaman>
-- Create date: <Create Date,,15/03/2016>
-- Description:	<Returns all development documents on ComplianceDocument.aspx>


-- =============================================

IF OBJECT_ID('dbo.PDR_DownloadComplianceDocument') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_DownloadComplianceDocument AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE dbo.PDR_DownloadComplianceDocument
(
	--These parameters will be used for search
	--@category varchar(15) = 'Compliance', -- (Compliance or General) No need of Category for Development and Scheme
	@documentId int = -1,
	@type varchar(50) = '',
	@typeId varchar(30) = '' output,
	@documentPath varchar(200)='' output,
	@documentName varchar(100)='' output,
	@documentExtension varchar(100)='' output	
	
)
AS
BEGIN
	PRINT 'FA'
	IF(@type = 'Development')
	BEGIN
		Select 
			@documentName = DocumentName, 
			@documentPath = DocumentPath, 
			@documentExtension = FileType, 
			@typeId = (Select TOP(1) DevelopmentId from P_DevelopmentDocuments where DocumentId = @documentId)
		from PDR_DOCUMENTS 
		where DocumentID = @documentId
	END
	ELSE IF (@type = 'Scheme')
	BEGIN
		Select 
			@documentName = DocumentName,
			@documentPath = '', 
			@documentExtension = '',
			@typeId = SchemeId
		from P_Documents where DocumentId = @documentId
	END
	ELSE--Property
	BEGIN
		Select 
		@documentName = DocumentName,
		@documentPath = '', 
		@documentExtension = DocumentFormat ,
		@typeId = PropertyId
		from P_Documents where DocumentId = @documentId
	END
END
