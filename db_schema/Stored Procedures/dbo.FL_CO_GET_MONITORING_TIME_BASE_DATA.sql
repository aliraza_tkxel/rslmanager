SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















CREATE PROCEDURE [dbo].[FL_CO_GET_MONITORING_TIME_BASE_DATA] 

/* ===========================================================================
 '   NAME:           FL_CO_GET_MONITORING_TIME_BASE_DATA
 '   DATE CREATED:   30TH DECEMBER 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist monitoring grid faults based on date criteria provided
 '   IN:             @selectedDateOption ,@orgId				
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	    @selectedDateOption varchar(50) = NULL,
	    @orgId int,
		
		
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FAULTLOGID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
as	
		
DECLARE		@SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
     IF @orgId IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
							'FL_FAULT_LOG.ORGID = '+ LTRIM(STR(@orgId))+ ' AND'
        
    IF @selectedDateOption = 'AllJobs'
       SET @SearchCriteria = '1=1' 
                           
	IF @selectedDateOption = 'OverDue' 	   
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+ 'Convert(varchar(10),FL_FAULT_LOG.DueDate,120) < '''+ Convert(varchar(11),GETDATE(),120)+''''  
       
	IF @selectedDateOption = 'SevenDays' 
	BEGIN
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '(Convert(varchar(10),FL_FAULT_LOG.DueDate,120) BETWEEN Convert(varchar(11),GETDATE(),120) AND Convert(varchar(11),DATEADD(d,7,GETDATE()),120)) '		
		--SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '( Convert(varchar(10),FL_FAULT_LOG.DueDate,103) <= '''+ Convert(varchar,DATEADD(d,7,GETDATE()),103)+''''
		--SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  ' AND Convert(varchar(10),FL_FAULT_LOG.DueDate,103) >= '''+ Convert(varchar,GETDATE(),103)+''')'  
	END
	IF @selectedDateOption = 'OneFourDays'          
	BEGIN
	    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '(Convert(varchar(10),FL_FAULT_LOG.DueDate,120) BETWEEN Convert(varchar(11),GETDATE(),120) AND Convert(varchar(11),DATEADD(d,14,GETDATE()),120)) '		
		--SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '( Convert(varchar(10),FL_FAULT_LOG.DueDate,103) <= '''+ Convert(varchar,DATEADD(d,14,GETDATE()),103)+''''
		--SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  ' AND Convert(varchar(10),FL_FAULT_LOG.DueDate,103) >= '''+ Convert(varchar,GETDATE(),103)+''')'  
	END 
	 IF @selectedDateOption = 'TwoOneDays'          
	BEGIN
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '(Convert(varchar(10),FL_FAULT_LOG.DueDate,120) BETWEEN Convert(varchar(11),GETDATE(),120) AND Convert(varchar(11),DATEADD(d,21,GETDATE()),120)) '		
		--SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '( Convert(varchar(10),FL_FAULT_LOG.DueDate,103) <= '''+ Convert(varchar,DATEADD(d,21,GETDATE()),103)+''''
		--SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  ' AND Convert(varchar(10),FL_FAULT_LOG.DueDate,103) >= '''+ Convert(varchar,GETDATE(),103)+''')'  
	END                                                                                
	                                                       
	IF  @selectedDateOption = 'TwoEightDays'          
	BEGIN
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9)+  '(Convert(varchar(10),FL_FAULT_LOG.DueDate,120) BETWEEN Convert(varchar(11),GETDATE(),120) AND Convert(varchar(11),DATEADD(d,28,GETDATE()),120)) '		
	END                
	/*IF @selectedDateOption = 'DueDate'
	BEGIN
		SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 'DATEDIFF(d, FL_FAULT_LOG.DueDate,  GetDate()) = 0'
	END     */           
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT  ' +                      
						
						CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID AS FaultLogID, FL_FAULT_LOG.JobSheetNumber AS JSNumber,' +
						CHAR(10) + CHAR(9) + 'Convert(varchar, FL_FAULT_LOG.SubmitDate, 103) AS Logged,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY.PriorityName AS Priority,Convert(varchar, FL_FAULT_PRIORITY.ResponseTime) + '' '' + Case  FL_FAULT_PRIORITY.Days When 0 then ''Hour(s)'' when 1 then ''Day(s)'' end AS PriorityTime,' +
						CHAR(10) + CHAR(9) + 'Convert(varchar, FL_FAULT_LOG.DueDate, 103) AS Due, P__PROPERTY.ADDRESS1 AS Address,' +
						CHAR(10) + CHAR(9) + 'P__PROPERTY.HOUSENUMBER + '','' +  P__PROPERTY.ADDRESS1 + '','' +  P__PROPERTY.ADDRESS2 + '','' + P__PROPERTY.TOWNCITY + '','' +    P__PROPERTY.POSTCODE + '', '' + P__PROPERTY.COUNTY AS COMPLETEADDRESS,' +
						CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE.StageName AS Stage,' +
						CHAR(10) + CHAR(9) + 'FL_FAULT.Description,' +
						CHAR(10) + CHAR(9) + 'E__EMPLOYEE.FIRSTNAME +''  ''+ E__EMPLOYEE.MIDDLENAME +''  ''+ E__EMPLOYEE.LASTNAME As Operative,' +
						CHAR(10) + CHAR(9) + 'Case When DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()) < 0 then  ''  ''  When DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()) > 0 then Convert(varchar, ABS(DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()))) + ''  day(s)''  When DATEDIFF(d,  FL_FAULT_LOG.DueDate,  GetDate()) = 0 then '''' end as OverDue '
											
                       
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM'+                     
                      CHAR(10) + CHAR(9) + 'FL_FAULT INNER JOIN' +                      
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID INNER JOIN'+                     
                      CHAR(10) + CHAR(9) + 'C__CUSTOMER ON FL_FAULT_LOG.CustomerID = C__CUSTOMER.CustomerID INNER JOIN'+                     
                      CHAR(10) + CHAR(9) + 'C_CUSTOMERTENANCY ON C__CUSTOMER.CUSTOMERID= C_CUSTOMERTENANCY.CUSTOMERID INNER JOIN'+ 
                      CHAR(10) + CHAR(9) + 'C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID=C_TENANCY.TENANCYID INNER JOIN'+		
                      CHAR(10) + CHAR(9) + 'P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID INNER JOIN'+                                  
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT .JobSheetNumber INNER JOIN'+
                      CHAR(10) + CHAR(9) + 'E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID INNER JOIN'+                        
                      CHAR(10) + CHAR(9) + 'FL_CO_APPOINTMENT_STAGE ON FL_CO_APPOINTMENT.AppointmentStageID = FL_CO_APPOINTMENT_STAGE.AppointmentStageID'
		
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   --IF @sortColumn != 'FaultLOGID'       
	--SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
	--				' , FaultLOGID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ' +
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +  'AND'   +                                                                                                                  
                        
                        CHAR(10) + CHAR(9) + ' 1=1 '
                        
    -- End building WHERE clause
    --========================================================================================
        
	
PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)






GO
