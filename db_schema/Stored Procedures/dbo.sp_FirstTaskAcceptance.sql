SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[sp_FirstTaskAcceptance]

@jobid int,
@userid int

AS

--Update the status to In Analysis and the assigned_to column to the person who is clicking the accept button
update h_request_journal set status_id = 6, assigned_to = @userid WHERE JOB_ID = @jobid

--Insert into the AssignHistory table the ID of the person who is clicking the accept button
INSERT INTO H_ASSIGNHISTORY (ASSIGNEDTO, Job_ID) VALUES (@userid, @jobid)




GO
