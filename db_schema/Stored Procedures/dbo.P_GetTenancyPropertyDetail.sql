USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[P_GetTenancyPropertyDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[P_GetTenancyPropertyDetail] AS SET NOCOUNT ON;') 
GO
/* ===========================================================================
 EXEC	 P_GetTenancyPropertyDetail 'A190540009'
						--------------
							History
						--------------
						
Version:	Author:				Create date:	Description:				WebPage:  

1.0			Ahmed Mehmood 		11/09/2013		Returns the property		PropertyRecordSummary.aspx		
												summary info														
	
1.1			Raja Aneeq			27/11/2014		Add a new column			PropertyRecordSummary.aspx
												'EpcRating' 
EXEC P_GetTenancyPropertyDetail 'A010000038'										
	
 '==============================================================================*/
ALTER PROCEDURE [dbo].[P_GetTenancyPropertyDetail] 


	
	(
	    -- These Parameters are passed as Search Criteria
		@PropertyId varchar (20) = ''
	)
	
AS
BEGIN
SET NOCOUNT ON;
	
SELECT 
		
	DISTINCT	ISNULL(P.PROPERTYID,'N/A') PROPERTYID,
		P.HOUSENUMBER,
		P.ADDRESS1,
		P.ADDRESS2,
		ISNULL(P.POSTCODE,'N/A') POSTCODE,
		P.TOWNCITY,
		ISNULL(P.HOUSENUMBER,'')+' '+ISNULL(P.ADDRESS1,'')+' '+ISNULL(P.ADDRESS2,'')+' , '+ISNULL(P.TOWNCITY,'') ADDRESS,
		ISNULL(PD.DEVELOPMENTNAME,'N/A') DEVELOPMENTNAME,
		ISNULL(PS.SCHEMENAME,'N/A') SCHEMENAME,
		ISNULL(S.DESCRIPTION,'N/A') [STATUS],
		ISNULL(SS.DESCRIPTION,'N/A') [SUBSTATUS],
		ISNULL(PT.DESCRIPTION,'N/A') [PROPERTYTYPE],FT.FUELTYPE [FUELTYPE] ,
        DT.DESCRIPTION [DWELLING TYPE],AT.DESCRIPTION [AssetType] ,DATEBUILT,
		RIGHTTOBUY,
        STOCK.DESCRIPTION [STOCK TYPE],O.DESCRIPTION [OWNERSHIP],
		maxDoc.EpcRating  As EpcRating,
        ISNULL(A_SAPRating.PARAMETERVALUE,'N/A') AS SAPRating,
        ISNULL(CONVERT(NVARCHAR,DATEADD(YEAR,1,CP12.ISSUEDATE),103),'N/A') AS GasCertificateExpiry, 
		ISNULL(CONVERT(NVARCHAR,P.DATEBUILT,103),'N/A') AS BuildDate
        
FROM	DBO.P__PROPERTY P

		LEFT JOIN P_Documents PDoc ON PDoc.PropertyId = P.PropertyId 
		OUTER APPLY (SELECT top 1 ISNULL(Ecat.CategoryType,'N/A') As EpcRating  from P_Documents doc
		left JOIN P_EpcCategory Ecat ON Ecat.CategoryTypeId = doc.EpcRating 
		where PropertyId =P.PropertyId AND doc.DocumentTypeId= 4  ORDER by CreatedDate DESC) AS maxDoc   
		
		LEFT JOIN DBO.P_SCHEME PS ON P.SCHEMEID = PS.SCHEMEID
		INNER JOIN PDR_DEVELOPMENT PD ON P.DEVELOPMENTID = PD.DEVELOPMENTID
		INNER JOIN DBO.P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID=P.PROPERTYTYPE
		INNER JOIN DBO.P_STATUS S ON S.STATUSID=P.STATUS
		Left JOIN DBO.P_FUELTYPE FT ON FT.FUELTYPEID=P.FUELTYPE
		LEFT JOIN DBO.P_SUBSTATUS SS ON SS.SUBSTATUSID=P.SUBSTATUS
		LEFT JOIN P_DWELLINGTYPE DT ON DT.DTYPEID=P.DWELLINGTYPE
		LEFT JOIN DBO.P_ASSETTYPE AT ON AT.ASSETTYPEID=P.ASSETTYPE
		LEFT JOIN DBO.P_STOCKTYPE STOCK ON STOCK.STOCKTYPE=P.STOCKTYPE
		LEFT JOIN DBO.P_OWNERSHIP O ON O.OWNERSHIPID=P.OWNERSHIP
		
		Left outer Join PA_PROPERTY_ATTRIBUTES A_SAPRating ON 
			( A_SAPRating.PROPERTYID = P.PROPERTYID ) AND 
			( A_SAPRating.ITEMPARAMID = ( SELECT ItemParamID 
								  From PA_ITEM_PARAMETER INNER JOIN PA_PARAMETER ON 
										  PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
								 Where ParameterName='SAP Rating' ) )
		LEFT OUTER JOIN P_LGSR CP12 ON P.PROPERTYID = CP12.PROPERTYID
		OUTER APPLY (SELECT PID.* FROM 
						PA_PROPERTY_ITEM_DATES PID
						INNER JOIN PA_ITEM I ON PID.ItemId = I.ItemID AND I.ItemName LIKE 'Structure'
						INNER JOIN PA_PARAMETER [PARAM] ON PID.ParameterId = [PARAM].ParameterID AND [PARAM].ParameterName LIKE 'Build Date'
						WHERE PID.PROPERTYID = P.PROPERTYID
						) A_BuiltDate
   
WHERE P.PROPERTYID=@PropertyId
	
END




