SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_ADD_PRICINGCONTROLDATA 
	/* ===========================================================================
 '   NAME:           FL_FAULT_AMEND
 '   DATE CREATED:   29 OCTOBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   Add the record in  FL_PRICING_CONTROL TABLE
 '
 '   IN:	   @LOCATIONID INT ,
 '	 IN:       @AREAID INT,
 '   IN:       @ELEMENTID INT,
 '   IN:	   @DESCRIPTIONID INT ,
 '   IN:       @NETCOST FLOAT ,
 '   IN:       @Vat FLOAT,
 '   IN:	   @GROSS FLOAT,
 '   IN:	   @VATRATEID INT ,
 '   IN:	   @EFFECTFROM NVARCHAR,
 '   IN        @INFLATIONVALUE,
 
 '
 '   OUT:           @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@LOCATIONID INT ,
@AREAID INT,
@ELEMENTID INT,
@DESCRIPTIONID INT ,
@NETCOST FLOAT ,
@Vat FLOAT,
@GROSS FLOAT,
@VATRATEID INT ,
@EFFECTFROM NVARCHAR(20),
@INFLATIONVALUE FLOAT,
@RESULT  INT OUTPUT

AS
DECLARE
@resNetCost float,
@resVat float

BEGIN 

SET NOCOUNT ON
BEGIN TRAN

IF @LOCATIONID <> -1 AND @AREAID <> -1 AND @ELEMENTID <> -1 AND @DESCRIPTIONID<>-1
	BEGIN
		UPDATE FL_FAULT SET NetCost=Cast((NetCost+NetCost*@INFLATIONVALUE/100)as Decimal(25,2)),Vat=Cast(((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)),Gross=Cast((NetCost+NetCost*@INFLATIONVALUE/100)+((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100) As Decimal(25,2)) ,EffectFrom=CONVERT(DateTime,@EFFECTFROM,103)
		WHERE FaultID IN (SELECT FF.FaultID FROM FL_FAULT AS FF WHERE FF.FaultID=@DESCRIPTIONID)
	END
ELSE IF @LOCATIONID <> -1 AND @AREAID <> -1 AND @ELEMENTID <> -1 
	BEGIN
		UPDATE FL_FAULT SET NetCost=Cast((NetCost+NetCost*@INFLATIONVALUE/100) as Decimal(25,2)),Vat=Cast(((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)),Gross=Cast((NetCost+NetCost*@INFLATIONVALUE/100)+((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)) ,EffectFrom=CONVERT(DateTime,@EFFECTFROM,103)
		WHERE FaultID IN (SELECT FF.FaultID FROM FL_FAULT AS FF WHERE FF.ElementID=@ELEMENTID) 
	END
ELSE IF @LOCATIONID <> -1 AND @AREAID <> -1
	BEGIN
		UPDATE FL_FAULT SET NetCost=Cast((NetCost+NetCost*@INFLATIONVALUE/100)as Decimal(25,2)),Vat=Cast(((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)),Gross=Cast((NetCost+NetCost*@INFLATIONVALUE/100)+((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100) as decimal(25,2)),EffectFrom=CONVERT(DateTime,@EFFECTFROM,103)
		WHERE FaultID IN (SELECT FF.FaultID FROM FL_FAULT AS FF
					   INNER JOIN FL_ELEMENT AS FE 
					   ON FF.ElementID = FE.ElementID AND FE.AreaID=@AREAID)
		
	END
ELSE IF @LOCATIONID <> -1
	BEGIN
		UPDATE FL_FAULT SET NetCost=Cast((NetCost+NetCost*@INFLATIONVALUE/100)as Decimal(25,2)),Vat=Cast(((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100) as Decimal(25,2)),Gross=Cast((NetCost+NetCost*@INFLATIONVALUE/100)+((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)) ,EffectFrom=CONVERT(DateTime,@EFFECTFROM,103)
		WHERE FaultID IN (SELECT FF.FaultID FROM FL_FAULT AS FF
						INNER JOIN FL_ELEMENT AS FE
						ON  FE.ElementID=FF.ElementID	
						INNER JOIN FL_AREA as FA
						ON FE.AreaID=FA.AreaID AND FA.LocationID=@LOCATIONID)		
	END
ELSE
	BEGIN
		UPDATE FL_FAULT SET NetCost=Cast((NetCost+NetCost*@INFLATIONVALUE/100)as Decimal(25,2)),Vat=Cast(((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)),Gross=Cast((NetCost+NetCost*@INFLATIONVALUE/100)+((NetCost+NetCost*@INFLATIONVALUE/100)*(Select VATRATE FROM F_VAT WHERE VATID=VatRateID)/100)as Decimal(25,2)) ,EffectFrom=CONVERT(DateTime,@EFFECTFROM,103)
		WHERE FaultID IN (SELECT FF.FaultID FROM FL_FAULT AS FF) 
				
	END
	



-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	
SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  SET @RESULT=-1
RETURN













GO
