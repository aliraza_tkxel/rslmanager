USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AppointmentsCalendarDetail]    Script Date: 10/24/2014 17:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_AppointmentsCalendarDetail @startDate='2012-12-31',@endDate='2014-01-04'  
-- Author:  Aqib Javed  
-- Create date: <9/21/2012>  
-- Last Modified: <04/01/2013>  
-- Description: <Description,Provided information of Scheduled Appointments of Engineers on Weekly basis>  
-- Webpage : WeeklyCalendar.aspx, MonthlyCalendar.aspx  
  
-- =============================================  

IF OBJECT_ID('dbo.AS_AppointmentsCalendarDetail') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_AppointmentsCalendarDetail AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_AppointmentsCalendarDetail]
    @startDate DATE ,
    @endDate DATE
AS 
    BEGIN  
;
        WITH    CTE_Appointment ( APPOINTMENTSHIFT, APPOINTMENTDATE, NAME, MOBILE, CUSTADDRESS, ASSIGNEDTO, POSTCODE, TITLE, APPOINTMENTSTARTTIME, APPOINTMENTENDTIME, [DESCRIPTION], [TYPE], APPOINTMENTENDDATE )
                  AS (			
                  
                  
-- ==========================================================================================    
-- PDR And Void APPOINTMENTS
-- ==========================================================================================  
                  
                  
                  SELECT   
								DISTINCT                    CASE WHEN DATEPART(HOUR,
                                                   PDR_APPOINTMENTS.APPOINTMENTSTARTTIME) < 12
                                     THEN 'AM'
                                     ELSE 'PM'
                                END AS APPOINTMENTSHIFT ,
                                CONVERT(VARCHAR, APPOINTMENTSTARTDATE, 103) AS APPOINTMENTDATE ,
                                ISNULL(CG.LIST, 'N/A') AS NAME ,
                                '' AS MOBILE 
                                ,CASE
								WHEN PDR_MSAT.propertyid IS NOT NULL THEN
									ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')+ ', ' + ISNULL(P__PROPERTY.postcode, '')
								WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
									ISNULL(P_SCHEME.SCHEMENAME , '') + ISNULL( ', '+P_SCHEME.SCHEMECODE, '')		
								ELSE
									ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
									+ ISNULL(P_BLOCK.ADDRESS1 , '') 
									+ ISNULL(', '+P_BLOCK.ADDRESS2, '') 
									+ ISNULL( ' '+P_BLOCK.ADDRESS3, '')
								    + ISNULL( ', '+P_BLOCK.POSTCODE, '')
								END AS CUSTADDRESS ,
                                PDR_APPOINTMENTS.ASSIGNEDTO 
                                ,COALESCE(p__property.postcode,P_SCHEME.SCHEMECODE,P_BLOCK.POSTCODE) POSTCODE ,
                                PDR_STATUS.TITLE ,
                                PDR_APPOINTMENTS.APPOINTMENTSTARTTIME ,
                                PDR_APPOINTMENTS.APPOINTMENTENDTIME ,
                                P_STATUS.[DESCRIPTION] ,
                                ISNULL(PDR_MSATTYPE.MSATTypeName,'') AS Type ,
                                CONVERT(VARCHAR, APPOINTMENTENDDATE, 103) AS APPOINTMENTENDDATE
                       FROM     PDR_APPOINTMENTS
                                INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
                                INNER JOIN	PDR_Status ON PDR_JOURNAL.StatusId = PDR_Status.StatusId AND PDR_STATUS.TITLE Not In ('Cancelled','To be Arranged')
								INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATID
								INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTYPEID = PDR_MSATTYPE.MSATTYPEID
								LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
								LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
								LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
								LEFT JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
								LEFT JOIN C_TENANCY ON PDR_MSAT.TenancyId = C_TENANCY.TENANCYID
                                --LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                --                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                --                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                --                           )
                                LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                                              AND CG.ID IN (
                                                              SELECT
                                                              MAX(ID) ID
                                                              FROM
                                                              C_CUSTOMER_NAMES_GROUPED
                                                              GROUP BY I )
                       WHERE    		PDR_APPOINTMENTS.APPOINTMENTSTARTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR PDR_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR (PDR_APPOINTMENTS.APPOINTMENTSTARTDATE <= CONVERT(DATE, @startDate, 103) AND PDR_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103))
       
-- ==========================================================================================    
-- PLANNED APPOINTMENTS
-- ==========================================================================================  
                  
                      UNION ALL
                      
								SELECT   
								DISTINCT                    CASE WHEN DATEPART(HOUR,
                                                   PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME) < 12
                                     THEN 'AM'
                                     ELSE 'PM'
                                END AS APPOINTMENTSHIFT ,
                                CONVERT(VARCHAR, APPOINTMENTDATE, 103) AS APPOINTMENTDATE ,
                                ISNULL(CG.LIST, 'N/A') AS NAME ,
                                '' AS MOBILE ,
                                ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS1, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS2, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS3, '') + ', '
                                + ISNULL(P__PROPERTY.POSTCODE, '') AS CUSTADDRESS ,
                                PLANNED_APPOINTMENTS.ASSIGNEDTO ,
                                P__PROPERTY.POSTCODE ,
                                PLANNED_STATUS.TITLE ,
                                PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME ,
                                PLANNED_APPOINTMENTS.APPOINTMENTENDTIME ,
                                P_STATUS.[DESCRIPTION] ,
                                'Planned' AS Type ,
                                CONVERT(VARCHAR, APPOINTMENTENDDATE, 103) AS APPOINTMENTENDDATE
                       FROM     PLANNED_APPOINTMENTS
                                INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JOURNALID = PLANNED_JOURNAL.JOURNALID
                                INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
                                INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
                                INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
                                INNER JOIN PLANNED_COMPONENT_TRADE pcd ON PLANNED_APPOINTMENTS.COMPTRADEID = pcd.COMPTRADEID
                                LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                                           )
                                LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                                              AND CG.ID IN (
                                                              SELECT
                                                              MAX(ID) ID
                                                              FROM
                                                              C_CUSTOMER_NAMES_GROUPED
                                                              GROUP BY I )
                       WHERE    		(PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= CONVERT(DATE, @startDate, 103) AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103)))
										AND APPOINTMENTSTATUS <> 'Cancelled' AND ISPENDING = 0
                     
-- ==========================================================================================    
-- APPLIANCE APPOINTMENTS
-- ==========================================================================================  
                     
                     
                       UNION ALL
                       SELECT DISTINCT
                                AS_APPOINTMENTS.APPOINTMENTSHIFT ,
                                CONVERT(VARCHAR, APPOINTMENTDATE, 103) AS APPOINTMENTDATE ,
                                ISNULL(CG.LIST, 'N/A') AS NAME  
    --,C__CUSTOMER.FIRSTNAME+'  '+C__CUSTOMER.LASTNAME AS Name       
                                ,
                                '' AS MOBILE --C_ADDRESS.MOBILE  
                                ,
                                ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS1, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS2, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS3, '')+ ', '
                                + ISNULL(P__PROPERTY.POSTCODE, '') AS CUSTADDRESS ,
                                AS_APPOINTMENTS.ASSIGNEDTO ,
                                P__PROPERTY.POSTCODE ,
                                AS_STATUS.TITLE ,
                                AS_APPOINTMENTS.APPOINTMENTSTARTTIME ,
                                AS_APPOINTMENTS.APPOINTMENTENDTIME ,
                                P_STATUS.[DESCRIPTION] ,
                                'GAS' AS Type ,
                                CONVERT(VARCHAR, APPOINTMENTDATE, 103) AS APPOINTMENTENDDATE
                       FROM     AS_APPOINTMENTS
                                INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID
                                INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID
                                INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
                                INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
                                LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                                           )
                                LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                                              AND CG.ID IN (
                                                              SELECT
                                                              MAX(ID) ID
                                                              FROM
                                                              C_CUSTOMER_NAMES_GROUPED
                                                              GROUP BY I )
                       WHERE    AS_JOURNAL.ISCURRENT = 1
                                AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate
                                                              AND
                                                              @endDate
                     )
            SELECT  APPOINTMENTSHIFT ,
                    APPOINTMENTDATE ,
                    NAME ,
                    MOBILE ,
                    CUSTADDRESS ,
                    ASSIGNEDTO ,
                    POSTCODE ,
                    TITLE ,
                    APPOINTMENTSTARTTIME ,
                    APPOINTMENTENDTIME ,
                    [DESCRIPTION] ,
                    [TYPE] ,
                    APPOINTMENTENDDATE
            FROM    CTE_Appointment
            ORDER BY APPOINTMENTDATE ASC ,
                    APPOINTMENTSTARTTIME ASC
	
    END

