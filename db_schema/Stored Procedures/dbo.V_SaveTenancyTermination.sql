USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_SaveTenancyTermination]    Script Date: 12-Mar-18 2:25:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Save Tenancy Termination for 1st Inspection
-- History:          28/05/2015 AR : Save Tenancy Termination for 1st Inspection    
-- =============================================
IF OBJECT_ID('dbo.[V_SaveTenancyTermination]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_SaveTenancyTermination] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[V_SaveTenancyTermination]
	@PropertyId varchar(100) = NULL,
	@CustomerId int = NULL,
	@TenancyId int = NULL,
	@TerminationDate DateTime,
	@UpdatedBy int,
	@Inspection int=1
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from

SET NOCOUNT ON;

DECLARE @MSATTypeId INT,
		@MSATId INT,
		@ToBeArrangedStatusId INT
---Declare Cursor variable            
DECLARE @ItemToInsertCursor CURSOR

SELECT
	@ToBeArrangedStatusId = PDR_STATUS.STATUSID
FROM PDR_STATUS
WHERE TITLE = 'To be Arranged'

IF (@Inspection = 1)
	BEGIN
SELECT
	@MSATTypeId = MSATTypeId
FROM PDR_MSATType
WHERE MSATTypeName = 'Void Inspection'
END


INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId, TerminationDate)
			VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId, @TerminationDate)

		SELECT
			@MSATId = SCOPE_IDENTITY()

		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
			VALUES (@MSATId, @ToBeArrangedStatusId, GETDATE(), @UpdatedBy)

		return SCOPE_IDENTITY()
--IF Not EXISTS(SELECT MSATID from PDR_MSAT where PropertyId=@PropertyId AND CustomerId=@CustomerId AND TenancyId=@TenancyId)
-- BEGIN
--		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId, TerminationDate)
--			VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId, @TerminationDate)

--		SELECT
--			@MSATId = SCOPE_IDENTITY()

--		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
--			VALUES (@MSATId, @ToBeArrangedStatusId, GETDATE(), @UpdatedBy)

--		return SCOPE_IDENTITY()
--	END
END
