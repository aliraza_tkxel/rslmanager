USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_DeleteRemovedTrades]    Script Date: 11/17/2016 5:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@isSaved bit
--EXEC	[dbo].[PLANNED_DeleteRemovedTrades]
--		@componentTradeId = 22,
--		@pmo =2,
--		@isDeleted = @isDeleted OUTPUT
--		SELECT	@isDeleted as N'@isSaved'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,27 Dec,2014>
-- Description:	<Description,,This procedure 'll delete removed trades while scheduling>
-- =============================================
IF OBJECT_ID('dbo.[PLANNED_DeleteRemovedTrades]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_DeleteRemovedTrades] AS SET NOCOUNT ON;') 
GO
ALTER  PROCEDURE [dbo].[PLANNED_DeleteRemovedTrades] 
	@componentTradeId int,
	@pmo int,
	@isDeleted bit OUT
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY	
		DELETE FROM PLANNED_REMOVED_SCHEDULING_TRADES WHERE JOURNALID  =@pmo AND (COMPTRADEID = @componentTradeId 
		OR MISCTRADEID = @componentTradeId )
		set @isDeleted = 1
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isDeleted = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isDeleted = 1
	END 
    
END
