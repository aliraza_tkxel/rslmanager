SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[E__EMPLOYEE_SELECT]
AS
	SET NOCOUNT ON;
SELECT     EMPLOYEEID, CREATIONDATE, TITLE, FIRSTNAME, MIDDLENAME, LASTNAME, DOB, GENDER, MARITALSTATUS, ETHNICITY, ETHNICITYOTHER, 
                      RELIGION, RELIGIONOTHER, SEXUALORIENTATION, SEXUALORIENTATIONOTHER, DISABILITY, DISABILITYOTHER, BANK, SORTCODE, PROFILE, 
                      ROLEPATH, IMAGEPATH, ORGID, ROLLNUMBER2, ACCOUNTNAME2, ACCOUNTNUMBER2, SORTCODE2ND, BANK2, ROLLNUMBER, ACCOUNTNAME, 
                      ACCOUNTNUMBER
FROM         E__EMPLOYEE
ORDER BY LASTNAME, FIRSTNAME ASC
GO
