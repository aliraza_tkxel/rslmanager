
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
-- Author:  <Aqib Javed>  
-- Create date: <25-Nov-2013>  
-- Description: <This store procedure will fetch the login history for particular user>  
-- =============================================  
CREATE PROCEDURE [dbo].[SP_LOGIN_AUDIT_REPORT]   
--@userid int,  
@searchString varchar (100),  
@searchYear int,  
@searchMonth int  
AS  
DECLARE   
@SelectClause varchar(500),  
@WhereClause  varchar(500),  
@OrderClause  varchar(200)  
BEGIN  
 Set @SelectClause='Select
 CONVERT(varchar,AC_LOGIN_HISTORY.LoginDate, 103) as LoginHistoryDate,  
 CONVERT(varchar,AC_LOGIN_HISTORY.LoginDate, 108) as [Time],  
 AC_LOGIN_HISTORY.FirstName+ '' ''+ AC_LOGIN_HISTORY.LastName Name, 
 AC_LOGIN_HISTORY.UserName, AC_LOGIN_HISTORY.ISFAILED, AC_LOGIN_HISTORY.ISLOCKED  
 from AC_LOGIN_HISTORY 
  
 '  --INNER JOIN  AC_LOGINS on AC_LOGIN_HISTORY.UserName = AC_LOGINS.LOGIN  
    -- Where Clause Start here   
     Set @WhereClause= ' WHERE 1=1' 
 --Set @WhereClause= ' AND AC_LOGINS.EMPLOYEEID ='+ CONVERT(VARCHAR(10),@userid)   
 if(@searchString!='') and (@searchString is not null)  
   Set @WhereClause=@WhereClause + ' and AC_LOGIN_HISTORY.firstname+'' ''+ AC_LOGIN_HISTORY.lastname  like ''%'+ @searchString+'%''' 
   
   
 if(@searchYear!=0) AND (@searchYear Is not null)  
   Set @WhereClause= @WhereClause +' and YEAR(AC_LOGIN_HISTORY.LoginDate)='+ CONVERT(VARCHAR(10),@searchYear)  
   
 if(@searchMonth!=0) AND (@searchMonth is not null)   
	Set @WhereClause= @WhereClause +' and Month(AC_LOGIN_HISTORY.LoginDate)='+ CONVERT(VARCHAR(10),@searchMonth)     
    
 Set @OrderClause=' ORDER BY AC_LOGIN_HISTORY.LoginDate desc'  
   
 -- Print (@SelectClause+@WhereClause+@OrderClause) 
  Exec (@SelectClause+@WhereClause+@OrderClause)
END  
GO
