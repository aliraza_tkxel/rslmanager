USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_CancelAppointmentsOnTerminationCancellation]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[V_CancelAppointmentsOnTerminationCancellation]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_CancelAppointmentsOnTerminationCancellation] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[V_CancelAppointmentsOnTerminationCancellation]
-- Add the parameters for the stored procedure here
		@pdrJournalId int = 0
	
AS
BEGIN
	
	SET NOCOUNT ON;  
	Declare @CancelledStatus int

	SELECT @CancelledStatus = StatusId
	FROM PDR_STATUS
	WHERE TITLE = 'Cancelled'   
	
	if(@pdrJournalId >= 1 AND @pdrJournalId IS NOT NULL) 
	BEGIN
		Declare @InspectionStatus varchar(50)

		SELECT @InspectionStatus = s.title
		FROM PDR_JOURNAL PJ
		INNER JOIN PDR_STATUS S ON S.STATUSID = PJ.STATUSID
		WHERE PJ.JOURNALID = @pdrJournalId 

		IF @InspectionStatus NOT IN ('Cancelled')
		BEGIN
			UPDATE PDR_JOURNAL set STATUSID = (select statusid from PDR_STATUS where TITLE = 'Cancelled')
			where JOURNALID=@pdrJournalId

			Declare @VoidInspectionMsatId int, @PropertyId varchar(50), @msatId int, @MSATTypeId int

			SELECT @VoidInspectionMsatId = MSATID
			FROM PDR_JOURNAL
			WHERE JOURNALID = @pdrJournalId

			select @PropertyId = PropertyId from PDR_MSAT where MSATId = @VoidInspectionMsatId

			select @msatId = max(msatid),@MSATTypeId = max(PDR_MSAT.MSATTypeId) 
			from PDR_MSAT
			inner join PDR_MSATType on PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
			where PropertyId = @PropertyId and MSATTypeName = 'Post Void Inspection'

			if(@MSATTypeId = (select MSATTypeId from PDR_MSATType where MSATTypeName = 'Post Void Inspection') AND @msatId > @VoidInspectionMsatId)
			BEGIN
				Declare @PVoidInspectionJournalId INT, @PVoidInspectionStatus INT
				SELECT @PVoidInspectionJournalId = JOURNALID, @PVoidInspectionStatus = STATUSID FROM PDR_JOURNAL WHERE MSATID = @msatId

				IF(@PVoidInspectionStatus != (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = 'Cancelled'))
				BEGIN
					UPDATE PDR_JOURNAL SET STATUSID = (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = 'Cancelled') WHERE JOURNALID = @PVoidInspectionJournalId

					UPDATE PDR_APPOINTMENTS SET APPOINTMENTSTATUS = 'Cancelled', JOURNALHISTORYID = (SELECT MAX(JOURNALHISTORYID) FROM PDR_JOURNAL_HISTORY WHERE JOURNALID = @PVoidInspectionJournalId)
					WHERE JOURNALID = @PVoidInspectionJournalId
				END

			end

		END
		if @InspectionStatus = 'Completed'
		BEGIN

			update PDR_JOURNAL SET STATUSID = @CancelledStatus
					 where JOURNALID IN (Select work.WorksJournalId
                     from PDR_JOURNAL j  
					 INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId 
					 INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId 
					 INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID 
					 OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId, 
					 case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]                                                                                      
					  from V_RequiredWorks w  
					  Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID 
					  Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID 
					  LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId 
					  where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work 
					 left join PDR_APPOINTMENTs app on app.JOURNALID = work.WorksJournalId  
					 Where j.JOURNALID = @pdrJournalId AND work.Status NOT IN ('Completed', 'Cancelled'))

			update V_RequiredWorks SET STATUSID = @CancelledStatus, IsCanceled = 1
					 where InspectionJournalId = @pdrJournalId
					 and StatusId not in (SELECT  StatusId
											FROM PDR_STATUS
											WHERE TITLE IN ('Cancelled','Completed'))

			DECLARE @WorkJournaId int, @workJournalHistoryId int

			DECLARE workJournalId_cursor CURSOR FOR   
			Select work.WorksJournalId
            from PDR_JOURNAL j  
			INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId 
			INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId 
			INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID 
			OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId, 
			case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]                                                                                      
			from V_RequiredWorks w  
			Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID 
			Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID 
			LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId 
			where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work 
			left join PDR_APPOINTMENTs app on app.JOURNALID = work.WorksJournalId  
			Where j.JOURNALID = @pdrJournalId AND work.PurchaseOrderId IS NULL AND work.Status IN ('Cancelled')

			OPEN workJournalId_cursor 

			FETCH NEXT FROM workJournalId_cursor   
			INTO @WorkJournaId

			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				
				SELECT @workJournalHistoryId = max(JOURNALHISTORYID)
				FROM PDR_JOURNAL_HISTORY 
				WHERE JOURNALID = @WorkJournaId

				update PDR_APPOINTMENTS SET APPOINTMENTSTATUS = 'Cancelled',
										JOURNALHISTORYID = @workJournalHistoryId
					 where JOURNALID = @WorkJournaId

				FETCH NEXT FROM workJournalId_cursor   
				INTO @WorkJournaId
			END
			CLOSE workJournalId_cursor;  
			DEALLOCATE workJournalId_cursor;  
			
		END
	END                                                                                        
END