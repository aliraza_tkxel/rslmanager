SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC	[dbo].[PLANNED_AllLetters]
--		@StatusId = NULL,
--		@ActionId = NULL,
--		@Title = NULL,
--		@Code = NULL
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,31/10/2013>
-- Description:	<Description,,This stored procedure will return the all letters>
-- Webpage:ViewLetters.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_AllLetters]
	@StatusId	int = -1,
	@ActionId	int = -1,
	@Title		varchar(max) = null,
	@Code		varchar(max) = null
AS
BEGIN

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT 
								PLANNED_StandardLetters.StandardLetterId,
								PLANNED_StandardLetters.Code, 
								PLANNED_Status.Title as StatusTitle, 
								PLANNED_Action.Title as ActionTitle, 
								PLANNED_StandardLetters.Title, 
								PLANNED_StandardLetters.IsActive,
								CONVERT(varchar(10), PLANNED_StandardLetters.ModifiedDate, 103) as ModifiedDate,
								ISNULL(E__EMPLOYEE.FIRSTNAME,'''') + ''' + ' ' + ''' + ISNULL(E__EMPLOYEE.LASTNAME,'''') as Name'
	
	SET @fromClause		= 'FROM 
								PLANNED_StandardLetters INNER JOIN
								PLANNED_Status ON PLANNED_Status.StatusId=PLANNED_StandardLetters.StatusId INNER JOIN 
								PLANNED_Action ON PLANNED_Action.ActionId=PLANNED_StandardLetters.ActionId INNER JOIN
								E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PLANNED_StandardLetters.ModifiedBy'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	if (@StatusId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'PLANNED_StandardLetters.StatusId = ' + CONVERT(varchar, @StatusId) + ' AND' + CHAR(10)
 	END
 	
 	if (@ActionId != -1)
	BEGIN
		SET @whereClause = @whereClause + 'PLANNED_StandardLetters.ActionId = ' + CONVERT(varchar, @ActionId) + ' AND' + CHAR(10)
 	END
 	
 	if (@Title IS NOT NULL)
	BEGIN
		SET @whereClause = @whereClause + 'PLANNED_StandardLetters.Title LIKE ''%' + @Title + '%'' AND' + CHAR(10)
 	END
 	
 	if (@Code IS NOT NULL)
	BEGIN
		SET @whereClause = @whereClause + 'PLANNED_StandardLetters.Code LIKE ''%' + @Code + '%'' AND' + CHAR(10)
 	END
	
	SET @whereClause = @whereClause + 'PLANNED_StandardLetters.IsActive = 1' + CHAR(10)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause
	
	SET @query = @query + @whereClause	
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
END
---- SET NOCOUNT ON added to prevent extra result sets from
	---- interfering with SELECT statements.
	--SET NOCOUNT ON;

 --   -- Insert statements for procedure here
	
	--SELECT PLANNED_StandardLetters.StandardLetterId
	--,PLANNED_StandardLetters.Title 
	--FROM PLANNED_StandardLetters
GO
