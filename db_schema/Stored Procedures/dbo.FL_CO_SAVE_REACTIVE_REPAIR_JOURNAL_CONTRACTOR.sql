SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO





CREATE PROCEDURE dbo.FL_CO_SAVE_REACTIVE_REPAIR_JOURNAL_CONTRACTOR 

/* ===========================================================================
 '   NAME:           FL_CO_SAVE_REACTIVE_REPAIR_JOURNAL_CONTRACTOR 
 '   DATE CREATED:	18th March, 2009
 '   CREATED BY:     	Tahir Gul 
 '   CREATED FOR:    	Broadland Housing
 '   PURPOSE:        	To Update the Contractor in FL_FAULT_LOG TABLE in case the user Replaces the already chosen contractor. 
 '   IN:             @FaultLogId  int, @OrgId int
 '   OUT:            @result
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
(
     	@FaultLogId int, 
	@OrgId int,
	@result int OUTPUT
)
AS
BEGIN TRAN 

BEGIN 
UPDATE FL_FAULT_LOG
SET OrgId = @OrgId
Where FaultLogID = @FaultLogid
END 
 

IF @@ERROR <> 0
BEGIN
GOTO ERRORBLCOK
END 
COMMIT TRAN
SET @result = 1
RETURN 

ERRORBLCOK:

ROLLBACK TRAN 
SET @result = -1




GO
