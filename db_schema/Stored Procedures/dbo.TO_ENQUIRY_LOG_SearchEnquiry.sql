SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_SearchEnquiry] 

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_SEARCHENQUIRY
 '   DATE CREATED:   03 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist enquiries based on search criteria provided
 '   IN:             @firstName, @lastName, @tenancyID, @nature, @status, @text, @date
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	    @firstName	varchar(50)	=  NULL,
		@lastName	varchar(50)	=  NULL,
		
		@tenancyID	int = NULL,
		@nature		int = NULL,
		@status		int = NULL,    
		
		@text		varchar(4000)    = NULL,
		
		@date		smalldatetime	=  NULL,
	
		
		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 50,
		@offSet   int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'EnquiryLogID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(500),
	        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF @firstName IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'C__CUSTOMER.FIRSTNAME like ''%' +@firstName+'%''' + ' AND'  
    
    
     IF @lastName IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'C__CUSTOMER.LASTNAME like ''%' +@lastName+'%''' + ' AND'   
    
    
    IF @tenancyID IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'TO_ENQUIRY_LOG.TenancyID = '+ LTRIM(STR(@tenancyID)) + ' AND'  
    
    IF @nature IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'TO_ENQUIRY_LOG.ItemNatureID = '+ LTRIM(STR(@nature)) + ' AND'  
                             
    IF @status IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'TO_ENQUIRY_LOG.ItemStatusID = '+ LTRIM(STR(@status)) + ' AND'  
                             
    IF @text IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'TO_ENQUIRY_LOG.Description LIKE ''%'+ @text + '%'''+ ' AND'  
    
    IF @date IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'TO_ENQUIRY_LOG.CreationDate = '''+ CONVERT(varchar,@date) + ''''+ ' AND'  
           
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT' +                      
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'TO_ENQUIRY_LOG.EnquiryLogID, TO_ENQUIRY_LOG.CreationDate, ' +
                        CHAR(10) + CHAR(9) + 'TO_ENQUIRY_LOG.TenancyID,    C_NATURE.DESCRIPTION AS Nature, ' +
                        CHAR(10) + CHAR(9) + 'C__CUSTOMER.FIRSTNAME, C__CUSTOMER.LASTNAME, ' +
                        CHAR(10) + CHAR(9) + 'C_ITEM.DESCRIPTION AS Item '
                        
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
    
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'TO_ENQUIRY_LOG INNER JOIN C__CUSTOMER ' +
                      CHAR(10) + CHAR(9) + 'ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C_NATURE ' +
                      CHAR(10) + CHAR(9) + 'ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN C_ITEM ' + 
                      CHAR(10) + CHAR(9) + 'ON C_NATURE.ITEMID = C_ITEM.ITEMID'
    
    -- End building FROM clause
    --========================================================================================                                
    
    --========================================================================================    
    -- Begin building OrderBy clause
    
   IF @sortColumn != 'EnquiryLogID'       
	SET @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + 
					' , EnquiryLogID '
	
	--SET @OrderClause = CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder 
	
	SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' DESC'
    
    -- End building OrderBy clause
    --========================================================================================    


    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( TO_ENQUIRY_LOG.EnquiryLogID NOT IN' + 

                       
                        CHAR(10) + CHAR(9)  + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' TO_ENQUIRY_LOG.EnquiryLogID ' +  
                        CHAR(10) + CHAR(9) + @FromClause + 
                        CHAR(10) + CHAR(9) + 'AND'+ @SearchCriteria + 
                        CHAR(10) + CHAR(9) + '1 = 1 ' + @OrderClause + ')' + CHAR(10) + CHAR(9) + 'AND' + 
                        
                        -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +
                        
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
    --========================================================================================
        
	
--PRINT (@SelectClause + @FromClause + @WhereClause + @OrderClause)
    
 EXEC (@SelectClause + @FromClause + @WhereClause + @OrderClause)



GO
