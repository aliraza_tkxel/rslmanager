USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[SP_NET_CHECKLOGIN]    Script Date: 12/28/2016 14:34:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--<Author : Aqib Javed>
--<Created Date : 23-Nov-2013>
--<Description : This store procedure verify the user login & password and also update threshold to zero after 5 Minutes >
--<Web page: rsl_master_site\BHAIntranet\Login.aspx>
-- =============================================
IF OBJECT_ID('dbo.[SP_NET_CHECKLOGIN]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[SP_NET_CHECKLOGIN] AS SET NOCOUNT ON;') 
GO
ALTER  PROC [dbo].[SP_NET_CHECKLOGIN]
@username varchar(50),
@password varchar(50)
AS
Declare
@lastlogintime datetime,
@differ int,
@nonContractorExpiry smalldatetime

set @nonContractorExpiry = dateadd(yyyy,1, getdate())

SELECT LOGIN,LOGINID,PASSWORD,
EXPIRES = case when t.teamid = 1 then EXPIRES else @nonContractorExpiry end ,FIRSTNAME, LASTNAME, E.EMPLOYEEID, TE.TEAMID,
PASSWORD as Password1, L.ACTIVE, UPPER(ISNULL(T.TEAMNAME, 'UNKNOWN')) AS TEAMNAME, 
ISNULL(TE.TEAMCODE, 'UNK') AS TEAMCODE, E.ORGID, ISLOCKED, THRESHOLD, ehi.Indicator AS HolidayIndicator
FROM AC_LOGINS L  
LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = L.EMPLOYEEID
LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID 
LEFT JOIN E_HOLIDAYENTITLEMENT_INDICATOR ehi ON J.HolidayIndicator = ehi.Sid 
LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID 
LEFT JOIN G_TEAMCODES TE ON T.TEAMID = TE.TEAMID 
WHERE (
L.LOGIN = @username  and 
L.password = @password Collate Latin1_General_CS_AS)   
AND L.ACTIVE = 1
AND (ISLOCKED=0 OR ISLOCKED IS NULL)
----Reset Login Failure Threshold after 5 Minutes
Select Top 1 @lastlogintime=LoginDate from AC_LOGIN_HISTORY where UserName=@username order by LoginId desc
if((SELECT DATEDIFF(minute,@lastlogintime,CURRENT_TIMESTAMP))>5)
  begin
   	update AC_LOGINS set THRESHOLD = 0 where [LOGIN] = @username AND (ISLOCKED=0 OR ISLOCKED IS NULL)
  end 