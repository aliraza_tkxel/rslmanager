USE [RSLBHALive]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_WARNINGS  OFF
GO

IF OBJECT_ID('dbo.[V_GetVWAppointmentByInspectionJournalId]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[V_GetVWAppointmentByInspectionJournalId] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[V_GetVWAppointmentByInspectionJournalId](
 @InspectionJournalID int = 0
)
AS
BEGIN

Select j.JOURNALID,s.TITLE as InspectionStatus,  mt.MSATTypeName, m.PropertyId,m.CustomerId,m.TenancyId,work.WorkDescription, work.VoidWorksNotes, work.[Status],work.IsScheduled, 
                 work.WorksJournalId,work.PurchaseOrderId 
                     from PDR_JOURNAL j  
                 INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId 
                 INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId 
                 INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID 
                 OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId, 
                 case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]                                                                                      
                  from V_RequiredWorks w  
                  Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID 
                  Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID 
                  LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId 
                  where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work 
                 Where j.JOURNALID =   @InspectionJournalID   order by work.PurchaseOrderId 

END