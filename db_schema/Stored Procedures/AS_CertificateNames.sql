USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:<Adeel Ahmed>
-- Create date: <17-06-2010>
-- Description:	<getting the customer info for letter>
-- =============================================
IF OBJECT_ID('dbo.AS_CertificateNames') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_CertificateNames AS SET NOCOUNT ON;') 
GO

-- =============================================
-- Author:		Abdul Moiz
-- Create date: 30/07/2018
-- Description:	this sp returns all the certificate names.
-- Webpage: certificate expiry.aspx
-- Exec [dbo].[AS_CertificateNames]
--		
-- =============================================
ALTER PROCEDURE [dbo].[AS_CertificateNames] 
@heatingTypeId INT = -1
AS
BEGIN
	IF(@heatingTypeId = -1)
	BEGIN
	   SELECT HeatingType AS heatingTypeId, title AS CertificateNames FROM P_LGSR_Certificates 
	   WHERE IsActive = 1
	   END
	ELSE
	BEGIN
		IF EXISTS(SELECT 1 FROM P_LGSR_Certificates 
		WHERE IsActive = 1 AND HeatingType = @heatingTypeId)
		BEGIN
			SELECT HeatingType AS heatingTypeId, title AS CertificateNames FROM P_LGSR_Certificates 
			WHERE IsActive = 1 AND HeatingType = @heatingTypeId
		END
		ELSE
		BEGIN
			SELECT HeatingType AS heatingTypeId, title AS CertificateNames FROM P_LGSR_Certificates 
			WHERE Title = 'N/A' AND IsActive = 1
		END
	END
END

