SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TO_F_RENTJOURNAL_PERIOD_ACCOUNT_BALANCE]

 /*=============================================================================
   Important Note:- The Query is taken from ASP pages of RSLManager application. 
   TkXel had made no  changes other than writing inside StoredProcedure
  ===============================================================================*/


 /* ===========================================================================
 '   NAME:           TO_F_RENTJOURNAL_CALCULATEACCOUNTBALANCE
 '   DATE CREATED:   27 MAY 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Calculates (sum) customer's account/rent balance
 '   IN:             @tenancyID INT 
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 
 
  @tenancyID INT,
  @PAYMENTSTARTDATE SMALLDATETIME = null,
  @PAYMENTENDDATE SMALLDATETIME = null
 
 
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
        
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000)


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 

       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'WHERE J.TENANCYID = ''' +LTRIM(STR(@tenancyID))+'''' + ' AND ( J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18) ) ' 
                                     
    IF @PAYMENTSTARTDATE IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +  
                             'AND J.PAYMENTSTARTDATE BETWEEN ''' +convert(varchar, convert(datetime, @PAYMENTSTARTDATE), 111) +'''' + ' AND ''' + convert(varchar, convert(datetime, @PAYMENTENDDATE), 111)+''''+' AND '
                             

	IF @PAYMENTENDDATE IS NOT NULL
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'J.PAYMENTSTARTDATE BETWEEN ''' +convert(varchar, convert(datetime, @PAYMENTSTARTDATE), 111)+'''' + ' AND ''' + convert(varchar, convert(datetime, @PAYMENTENDDATE), 111)  +''''                      



    --========================================================================================	        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT' +                      
                        CHAR(10) + CHAR(9) + 'ISNULL(SUM(J.AMOUNT),0) AS  AccountBalance '
                        
    -- End building SELECT clause
    --========================================================================================  


    --========================================================================================    
    -- Begin building FROM clause
    
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'F_RENTJOURNAL J LEFT JOIN F_TRANSACTIONSTATUS S ' +
                      CHAR(10) + CHAR(9) + 'ON J.STATUSID = S.TRANSACTIONSTATUSID '

    
    -- End building FROM clause
    --======================================================================================== 
--PRINT (@SelectClause + @FromClause + @SearchCriteria)

EXEC (@SelectClause + @FromClause + @SearchCriteria)


GO
