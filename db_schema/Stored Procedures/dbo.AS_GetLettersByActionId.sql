SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetLettersByStatusId @statusId = 0
-- Author:		<Noor Muhammad>
-- Create date: <16/10/2012>
-- Description:	<Get Letters against Action Id>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetLettersByActionId](
	-- Add the parameters for the stored procedure here
	@actionId int = -1
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	  SELECT StandardLetterId,Title from AS_StandardLetters WHERE ActionId = @actionId

END
GO
