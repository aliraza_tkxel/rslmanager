SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE [dbo].[FL_UPDATE_CUSTOMER_CONTACT_DETAIL]
	/*	===============================================================
	'   NAME:           FL_UPDATE_CUSTOMER_CONTACT_DETAIL
	'   DATE CREATED:   29 Oct,2008
	'   CREATED BY:     Noor Muhammad
	'   CREATED FOR:    Tenants Online
	'   PURPOSE:        To update the Customer information to be displayed on fault submission page
	'   IN:             @CustomerId 
	'   OUT: 		    No     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@CustomerId INT,
		@FirstName varchar (50),
		@MiddleName varchar (50),
		@LastName varchar (50),		
		@Address1 varchar (50),
		@Address2 varchar (50),
		@Address3 varchar (50),
		@HouseNumber varchar (50),
		@TownCity varchar (50),
		@County varchar (50),
		@Telephone varchar (20),
		@TelephoneMobile varchar (20),
		@Email varchar (50),
		@PostCode varchar (10),
		@CustId int OUTPUT		
	)
AS

                              
	UPDATE C__CUSTOMER 
	SET 	
	FIRSTNAME = @FirstName,
	MIDDLENAME = @MiddleName,
	LASTNAME =@LastName	    	
	WHERE CUSTOMERID = @CUSTOMERID 


	UPDATE C_ADDRESS SET 
	ADDRESS1 = @Address1,
	ADDRESS2 = @Address2,
	ADDRESS3 = @Address3,
	HOUSENUMBER = @HouseNumber,
	TOWNCITY = @TownCity,
	POSTCODE = @PostCode,
	COUNTY = @County,
	TEL = @Telephone,
	MOBILE = @TelephoneMobile,
	EMAIL = @Email
	WHERE ISDEFAULT = 1 AND CUSTOMERID = @CUSTOMERID


SET @CUSTID= @CUSTOMERID



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

















GO
