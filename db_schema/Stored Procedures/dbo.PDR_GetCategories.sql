USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetCategories]    Script Date: 18/04/2017 11:43:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetCategories') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetCategories AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[PDR_GetCategories]
			@reportFor	varchar (8000)
AS
BEGIN
	if @reportFor =  '' 
		begin
			select title , categorytypeid from P_category 
		end
	else if @reportFor = 'Development'
		begin
			select title , categorytypeid from P_category  where isdevelopment = 1
		end
	else if @reportFor = 'Scheme'
		begin
			select title , categorytypeid from P_category  where isScheme = 1
		END
	else if @reportFor = 'Property' 
		BEGIN
			select title , categorytypeid from P_category  where isProperty = 1
		END


END

--exec [PDR_GetCustomerInformation] 972014
GO
