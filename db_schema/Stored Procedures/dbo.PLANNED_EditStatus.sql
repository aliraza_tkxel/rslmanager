
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	PLANNED_EditStatus @Title = N'NewStatus',@Ranking = 7,	@ModifiedBy = 1,@StatusId = 2,@isSaved = 0
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,04/11/2013>
-- Description:	<Description,,Edit the values of Status>
-- Web Page: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_EditStatus] (
	-- Add the parameters for the stored procedure here
	@Title varchar(1000),
	@Ranking int,
	@ModifiedBy int,
	@StatusId int,
	@isSaved bit out
	)
AS
BEGIN
Declare @oldStatusId int
Declare @oldRanking int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Begin the transacttion
	BEGIN TRANSACTION
	BEGIN TRY 
		-- Insert statements for procedure here
		SELECT @oldStatusId=StatusId FROM PLANNED_Status Where Ranking = @Ranking
		SELECT @oldRanking=Ranking FROM PLANNED_Status Where StatusId = @StatusId
		--If this condition matches then it means ranking of status has not been changed.
		IF @oldStatusId = @StatusId
			BEGIN
			Print 'here1'
				UPDATE PLANNED_Status SET Title = @Title,Ranking=@Ranking,ModifiedBy=@ModifiedBy,IsEditable=1,ModifiedDate=GETDATE() 
				WHERE StatusId = @StatusId
				
				INSERT INTO PLANNED_StatusHistory(StatusId,Title,Ranking,CreatedBy,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
				VALUES (@StatusId,@Title,@Ranking,@ModifiedBy,@modifiedBy,GETDATE(),GETDATE(),1)
				
			END
		ELSE	
			BEGIN		
				--This Else means that we 'll have to swap the ranking	
				IF @oldStatusId > 0 
					BEGIN					
						--update the rank of status which has the same rank as input of stored procedure.
						UPDATE PLANNED_Status SET Ranking = @oldRanking WHERE StatusId = @oldStatusId
						declare @oldStatusTitle varchar(100)			
						SELECT @oldStatusTitle = Title FROM PLANNED_Status WHERE StatusId = @oldStatusId
						-- record this change in history
						INSERT INTO PLANNED_StatusHistory(StatusId,Title,Ranking,CreatedBy,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
						VALUES (@oldStatusId,@oldStatusTitle,@oldRanking,@ModifiedBy,@modifiedBy,GETDATE(),GETDATE(),1)
					END					
				--now update the status for which this stored procedure was called.										
				UPDATE PLANNED_Status SET Title = @Title,Ranking=@Ranking,ModifiedBy=@ModifiedBy,IsEditable=1,ModifiedDate=GETDATE() 
				WHERE StatusId = @StatusId
				-- also save the change in history
				INSERT INTO PLANNED_StatusHistory(StatusId,Title,Ranking,CreatedBy,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
				VALUES (@StatusId,@Title,@Ranking,@ModifiedBy,@modifiedBy,GETDATE(),GETDATE(),1)			
			END	
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END  		
	
	EXEC PLANNED_AllStauses	
END
GO
