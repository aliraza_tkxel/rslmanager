USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_ToilLeave]    Script Date: 6/6/2017 4:33:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Moiz
-- Create date: April 27, 2018
-- =============================================

IF OBJECT_ID('dbo.E_ToilLeave') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_ToilLeave AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_ToilLeave] 
	-- Add the parameters for the stored procedure here
	@empId INT
	
AS
BEGIN

	DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME	
	DECLARE @DAYS_ABS int

SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE(@empId)
	
SELECT e.STARTDATE,e.RETURNDATE,ar.DESCRIPTION as Reason,s.DESCRIPTION as Status, n.DESCRIPTION as Nature, 
 e.DURATION_HRS as DAYS_ABS
 , e.HOLTYPE as HolType,
s.ITEMSTATUSID as StatusId, n.ITEMNATUREID as NatureId, ar.SID as ReasonId, e.LASTACTIONDATE as LastActionDate,e.ABSENCEHISTORYID as AbsenceHistoryId, e.DURATION_TYPE as Unit
FROM E_JOURNAL j
inner join E_ABSENCE e on j.JOURNALID = e.JOURNALID
 AND e.ABSENCEHISTORYID = (
									SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
									WHERE JOURNALID = j.JOURNALID
								  ) 
LEFT JOIN E_ABSENCEREASON ar on e.REASONID = ar.SID
INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
WHERE j.EMPLOYEEID = @empId and j.ITEMNATUREID =  
	(SELECT ITEMNATUREID from E_NATURE 
		where DESCRIPTION  = 'BRS Time Off in Lieu'  --   ,'Birthday Leave','Personal Day'  it has been removed 
		-- because Birthday and Personal Day Leaves handle in code. 
	) 
and e.STARTDATE >= @BHSTART and  e.STARTDATE <= @BHEND
ORDER by e.STARTDATE DESC

	
	
END