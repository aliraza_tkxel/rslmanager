USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SchemeBlockAssignWorkToContractor]    Script Date: 22/10/2018 16:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 29/10/2015
-- Description:	Scheme Block Assign Work to Contractor.
-- History:     29/10/2015 AR : Save Purchase order for Scheme Block(Raise a PO) Assign Work to Contractor
-- =============================================
IF OBJECT_ID('dbo.[PDR_SchemeBlockAssignWorkToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PDR_SchemeBlockAssignWorkToContractor] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PDR_SchemeBlockAssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@schemeId	INT=null, 
	@blockId	INT=null,
	@incServiceCharge	bit=null, 
	@propertyApportionment	MONEY=null,
	@schemeProperty NVARCHAR(200)=null,
	@attributeTypeId	INT=null,
	@provisionId int = null,

	@provisionCategory NVARCHAR(200)=null,
	@provisionDescription NVARCHAR(50)=null,

	@itemId int = null,  
	@pdrContractorId INT,	
	@ContractorId INT,
	@POName NVARCHAR(200)=null,	
	@ContactId INT,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@POStatus INT,
	@ContractStartDate NVARCHAR(20),
	@ContractEndDate NVARCHAR(20),
	@ContractorWorksDetail AS PDR_AssingToContractorWorksRequired READONLY,
	@MSATTypeId	INT=null,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT,
	@orderIdOut INT OUTPUT,
	@expenditureOut INT OUTPUT
	
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


/* Working of this stored Procedure
** 1- Get Status Id of "Assigned To Contractor" from Planned_Status. In case a Status of "Assigned To Contractor" 
**    is not present add a new one and get its Id.

** 2- Insert a new record in F_PURCHASEORDER and get the identity value as Order Id.
**
** 3- Insert a new record in PDR_CONTRACTOR_WORK using the given input data and OrderId
**    and get Identity Value as PDRContractorId.
**
** Loop (Insert Purchase Items and Works Required Items.
**
**   4- Insert a Purchase Item in F_PURCHASEITEM from given an constant data
**      and get Identity Value PURCHASEORDERITEMID
**
**   5- Insert a new work required from given data and also insert PURCHASEORDERITEMID
**
** End Loop
*/


BEGIN TRANSACTION
BEGIN TRY

-- =====================================================
-- General Purpose Variable
-- =====================================================
DECLARE 
 @MSATId INT
,@journalId INT 
,@MsatType NVARCHAR(200)
-- To save same time stamp in all records 
DECLARE @CurrentDateTime AS datetime2 = GETDATE()
DECLARE @ContractStartDateTime AS smalldatetime = convert(datetime, @ContractStartDate)
DECLARE @ContractEndDateTime AS smalldatetime = convert(datetime, @ContractEndDate)
--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusId int = NULL

-- =====================================================
-- get status history id of "Assigned To Contractor"
-- =====================================================

SELECT	@newStatusId = STATUSID
FROM	PDR_STATUS
WHERE	PDR_STATUS.TITLE = 'Assigned To Contractor'

-- =============================================    
-- Get status id of "Arranged"    and MSATTypeId
-- =============================================     
	 
	
	SET @MsatType= 'Scheme Block PO'
	
-- ====================================================================================================    
--          INSERT PDR_MSAT AND  PDR_JOURNAL    
-- ====================================================================================================    	
	
	
INSERT INTO PDR_MSAT (SchemeId, MSATTypeId, BlockId,ItemId,ProvisionId, ProvisionCategory, ProvisionDescription, attributeTypeId,IsActive,SchemeProperty,INCSC,PropertyApportionment)
	VALUES (@schemeId, @MSATTypeId, @blockId, @itemId,@provisionId, @provisionCategory, @provisionDescription ,@attributeTypeId,1,@schemeProperty,@incServiceCharge,@propertyApportionment)

SELECT
	@MSATId = SCOPE_IDENTITY()

INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
	VALUES (@MSATId, @newStatusId, GETDATE(), @userId)

SELECT @journalId = SCOPE_IDENTITY()
	





IF (@PdrContractorId = -1)
BEGIN

-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @Active bit = 1
, @POTYPE int = (SELECT	POTYPEID
				FROM F_POTYPE
				WHERE POTYPENAME = 'General') -- 2 = 'Repair'

-- To get Identity Value of Purchase Order.
, @purchaseOrderId int
if @POName is null
BEGIN
set @POName=UPPER( 'Scheme Block Purchase Order')
END
INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
							POTYPE, POSTATUS, GASSERVICINGYESNO, SchemeId,BLOCKID,F_PURCHASEORDER.CompanyId)
VALUES (@POName, @CurrentDateTime
, 'This purchase order was created for Scheme Block(Raise a PO).'
, @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0, @schemeId,@blockId,1)

SET @purchaseOrderId = SCOPE_IDENTITY()

-- Changes to get Purchase Order ID as output

SET @orderIdOut = @purchaseOrderId
-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================

	INSERT INTO [PDR_CONTRACTOR_WORK]
           ([JournalId],[ContractorId],[ContactId],[AssignedDate],[AssignedBy],[Estimate],[EstimateRef],[PurchaseOrderId],ContractStartDate,ContractEndDate)
	VALUES
           (@journalId,@ContractorId,@ContactId,@CurrentDateTime,@userId,@Estimate,@EstimateRef,@PurchaseOrderId,@ContractStartDateTime,@ContractEndDateTime)
    SET @PdrContractorId = SCOPE_IDENTITY()    
           
END
ELSE
BEGIN
	UPDATE [PDR_CONTRACTOR_WORK]
	SET [ContractorId] = @ContractorId
	,[ContactId] = @ContactId
	,[AssignedDate] = @CurrentDateTime
	,[AssignedBy] = @userId
	,[Estimate] = @Estimate
	,[EstimateRef] = @EstimateRef
	
	WHERE PDRContractorId = @PdrContractorId
END


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	*
FROM @ContractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE
@WorkDetailId int ,
@ServiceRequired nvarchar(4000),
@NetCost smallmoney,
@VatType int,
@VAT smallmoney,
@GROSS smallmoney,
@PIStatus int,
@ExpenditureId int,
@CostCenterId int,
@BudgetHeadId int

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(20) = @MsatType -- Title for Purchase Items, specially to inset in F_PurchaseItem

		-- =====================================================
		-- Loop (Start) through records and insert works required
		-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId,@ServiceRequired, @NetCost, @VatType, @VAT,
		@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
		WHILE @@FETCH_STATUS = 0 BEGIN
	
		IF (@WorkDetailId = -1)
		BEGIN
			-- =====================================================
			--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
			-- =====================================================

			INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
									NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
			VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @ServiceRequired, 
				@CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

			DECLARE @ORDERITEMID int = SCOPE_IDENTITY()
			SET @expenditureOut = @ExpenditureId
			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================

			INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL]
           ([PDRContractorId],[ServiceRequired],[NetCost],[VatId],[Vat],[Gross],[ExpenditureId],[CostCenterId],[BudgetHeadId],[PURCHASEORDERITEMID])
			VALUES
           (@pdrContractorId,@ServiceRequired,@NetCost,@VatType,@VAT,@GROSS,@ExpenditureId,@CostCenterId,@BudgetHeadId,@ORDERITEMID)
           
		END

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId, @ServiceRequired, @NetCost, @VatType, @VAT,
				@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = @journalId

END