SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[sp_getUserOrg_byUserID]
@user_id int
as
SELECT     dbo.H_LOGIN.*, dbo.H_ORGANISATION.*, dbo.H_LOGIN.USER_ID AS Expr1
FROM         dbo.H_ORGANISATION INNER JOIN
                      dbo.H_LOGIN ON dbo.H_ORGANISATION.ORG_ID = dbo.H_LOGIN.ORG_ID
WHERE     (dbo.H_LOGIN.USER_ID = @user_id) 

GO
