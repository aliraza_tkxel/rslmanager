USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AssignToContractorReport]    Script Date: 03/24/2016 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.I_ALERTS_SELECT_BY_EMP') IS NULL 
	EXEC('CREATE PROCEDURE dbo.I_ALERTS_SELECT_BY_EMP AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date, 4/1/2014>
-- Description:	<Description, Return Alerts of Employee>
-- updated : <Saud Ahmed>
-- updated Date : <01/03/2017>
-- change : order by Name
-- =============================================
Alter PROCEDURE [dbo].[I_ALERTS_SELECT_BY_EMP]	
(
	@EmpId	int
)
AS  
BEGIN  
 DECLARE @UserIdString nvarchar(10)
 SET @UserIdString = CAST(@EmpId AS NVARCHAR(10))
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

 
 -- SELECT     E_TEAMALERTS.ALERTID, E_ALERTS.AlertName, E_ALERTS.AlertUrl, E_ALERTS.SOrder  
 -- FROM	E__EMPLOYEE 
 --		INNER JOIN E_TEAMALERTS ON E__EMPLOYEE.JobRoleTeamId = E_TEAMALERTS.JobRoleTeamId 
 --		INNER JOIN E_ALERTS ON E_TEAMALERTS.AlertId = E_ALERTS.AlertID 
 -- WHERE      (E__EMPLOYEE.EMPLOYEEID = @EmpId)  
 -- ORDER BY   E_ALERTS.AlertName--E_ALERTS.SOrder

SELECT E_TEAMALERTS.ALERTID, E_ALERTS.AlertName,Case When E_ALERTS.AlertName = 'Service Complaints' THEN REPLACE( E_ALERTS.AlertUrl, '{LastName}',@UserIdString) ELSE REPLACE( E_ALERTS.AlertUrl, '{UserId}',@UserIdString) end as AlertUrl, E_ALERTS.SOrder  ,ec.description AS AlertCategory
 FROM	E__EMPLOYEE 
		INNER JOIN E_TEAMALERTS ON E__EMPLOYEE.JobRoleTeamId = E_TEAMALERTS.JobRoleTeamId 
		INNER JOIN E_ALERTS ON E_TEAMALERTS.AlertId = E_ALERTS.AlertID 
		LEFT JOIN e_alertcategory ec ON ec.alertcategoryid = E_ALERTS.categoryid
 WHERE      (E__EMPLOYEE.EMPLOYEEID = @EmpId)  
 ORDER BY   ec.description, E_ALERTS.AlertName--E_ALERTS.SOrder		
 
 
END

GO
