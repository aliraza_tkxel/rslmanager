SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [diagnostics].[CompleteRepairAppointment]
    (
      @AppointmentId INT ,
      @FaultLogID INT ,
      @FollowOnNotes VARCHAR(500) ,
      @RepairNotes VARCHAR(500) ,
      @RepairCompletionDateTime SMALLDATETIME ,
      @IsAppointmentCompleted BIT ,
      @FaultLogCompletedDate DATETIME = NULL ,
      @RESULT INT = 1 OUTPUT
    )
AS 
    DECLARE @UserId INT ,
        @CustomerId INT ,
        @PropertyId NVARCHAR(20) ,
        @TenancyId INT ,
        @ItemnatureId INT ,
        @ItemId INT ,
        @JournalId INT ,
        @CJournalId INT ,
        @repairDetail NVARCHAR(1000) ,
        @CompleteStatusId INT ,
        @OrgID INT ,
        @QTY INT ,
        @RECHARGE INT ,
        @NetCost FLOAT ,
        @VatType INT ,
        @rVAT FLOAT ,
        @GrossCost FLOAT ,
        @FaultDescription NVARCHAR(100) ,
        @SaleID INT ,
        @SALEITEMID INT ,
        @Success INT ,
        @FaultRepairListId INT ,
        @RentJournal_id BIGINT    
       
    BEGIN

        SET NOCOUNT ON
        BEGIN TRAN
        SET @Success = 0
        SELECT  @UserId = E__EMPLOYEE.EMPLOYEEID ,
                @CustomerId = C__CUSTOMER.CUSTOMERID ,
                @PropertyId = TENANCY.PROPERTYID ,
                @TenancyId = TENANCY.TENANCYID ,
                @ItemnatureId = C_NATURE.ITEMNATUREID ,
                @ItemId = C_NATURE.ITEMID ,
                @orgId = FL_FAULT_LOG.ORGID
        FROM    FL_CO_APPOINTMENT
                INNER JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID
                INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
                INNER JOIN E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID
                INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
                INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
                INNER JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID
                INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C__CUSTOMER.CUSTOMERID
                                                   AND CT.ENDDATE IS NULL
                INNER JOIN C_TENANCY TENANCY ON TENANCY.TENANCYID = CT.TENANCYID
                                                AND TENANCY.ENDDATE IS NULL
                INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
                INNER JOIN FL_FAULT_JOURNAL ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID
                INNER JOIN C_NATURE ON FL_FAULT_JOURNAL.ITEMID = C_NATURE.ITEMID
        WHERE   FL_CO_APPOINTMENT.AppointmentID = @AppointmentId
                AND C_NATURE.ITEMNATUREID = ( SELECT    ItemNatureID
                                              FROM      C_NATURE
                                              WHERE     DESCRIPTION = 'Reactive Repair'
                                            )
                AND FL_FAULT_APPOINTMENT.FaultAppointmentId = ( SELECT
                                                              MIN(FaultAppointmentId)
                                                              FROM
                                                              FL_FAULT_APPOINTMENT
                                                              WHERE
                                                              AppointmentID = @AppointmentId
                                                              )

--Get 'Complete' status ID    
        SET @CompleteStatusId = ( SELECT    FaultStatusID
                                  FROM      FL_FAULT_STATUS
                                  WHERE     DESCRIPTION = 'Complete'
                                )


        SELECT  @RECHARGE = FL.RECHARGE ,
                @NetCost = NETCOST ,
                @VatType = VATRATEID ,
                @rVAT = VAT ,
                @GrossCost = GROSS ,
                @FaultDescription = F.DESCRIPTION
        FROM    dbo.FL_FAULT_LOG FL
                INNER JOIN dbo.FL_FAULT F ON F.FaultID = FL.FaultID
        WHERE   FL.FaultLogID = @FaultLogId
        SELECT  @JournalId = JOURNALID
        FROM    FL_FAULT_JOURNAL
        WHERE   FaultLogID = @FaultLogId
        PRINT ( @JournalId )
/*    
Declare the cursor on FaultRepairListID for processing each Fault Repair    
*/
        DECLARE c1 CURSOR READ_ONLY
        FOR
            SELECT  FaultRepairListID
            FROM    FL_CO_FAULTLOG_TO_REPAIR
            WHERE   FaultLogID = @FaultLogId

        OPEN c1

/*    
Start Looping    
*/

        FETCH NEXT FROM c1
INTO @FaultRepairListId

        WHILE @@FETCH_STATUS = 0 
            BEGIN


--Get repair detail    
                SELECT  @repairDetail = Description
                FROM    FL_FAULT_REPAIR_LIST
                WHERE   FaultRepairListID = @FaultRepairListId
                PRINT ( @repairDetail )
--Insertion into C_Journal    
                INSERT  INTO C_JOURNAL
                        ( CUSTOMERID ,
                          TENANCYID ,
                          PROPERTYID ,
                          ITEMID ,
                          ITEMNATUREID ,
                          CURRENTITEMSTATUSID ,
                          TITLE
                        )
                VALUES  ( @CustomerId ,
                          @TenancyId ,
                          @PropertyId ,
                          @ItemId ,
                          @ItemnatureId ,
                          11 ,
                          @repairDetail
                        )

--Get the latest CJournal ID    
                SELECT  @CJournalId = JOURNALID
                FROM    C_JOURNAL
                WHERE   JOURNALID = @@Identity

--Insertion into C_Repair    
                INSERT  INTO C_REPAIR
                        ( JOURNALID ,
                          ITEMSTATUSID ,
                          ITEMACTIONID ,
                          LASTACTIONDATE ,
                          LASTACTIONUSER ,
                          ITEMDETAILID
                        )
                VALUES  ( @CJournalId ,
                          11 ,
                          6 ,
                          @RepairCompletionDateTime ,
                          @UserId ,
                          @FaultLogId
                        )

-- Insertion into FL_FAULTJOURNAL_TO_CJOURNAL    
                INSERT  INTO FL_FAULTJOURNAL_TO_CJOURNAL
                        ( FJOURNALID, CJOURNALID )
                VALUES  ( @JournalId, @CJournalId )

                FETCH NEXT FROM c1 INTO @FaultRepairListId

            END

        CLOSE C1
        DEALLOCATE C1

----------------------Recharge Process-------------------------------------------    

        IF @RECHARGE = 1 
            BEGIN
-- Insertion into C_RECHARGEABLE    
                INSERT  INTO C_RECHARGEABLE
                        ( JOURNALID )
                VALUES  ( @CJournalId )
-- Insertion into F_SALESINVOICE    
                INSERT  INTO F_SALESINVOICE
                        ( SONAME ,
                          SODATE ,
                          SONOTES ,
                          USERID ,
                          TENANCYID ,
                          ACTIVE ,
                          SOTYPE ,
                          SOSTATUS
                        )
                VALUES  ( 'TENANT RECHARGE' ,
                          @RepairCompletionDateTime ,
                          'Automated Tenant Recharge for: '
                          + @FaultDescription ,
                          @UserID ,
                          @TenancyId ,
                          1 ,
                          1 ,
                          1
                        )
-- Get the latest CJournal ID    
                SELECT  @SaleID = SaleID
                FROM    F_SALESINVOICE
                WHERE   SaleID = SCOPE_IDENTITY()
-- Insertion into F_RENTJOURNAL    
                INSERT  INTO F_RENTJOURNAL
                        ( TENANCYID ,
                          TRANSACTIONDATE ,
                          ITEMTYPE ,
                          PAYMENTTYPE ,
                          AMOUNT ,
                          STATUSID
                        )
                VALUES  ( @TenancyId ,
                          @RepairCompletionDateTime ,
                          5 ,
                          NULL ,
                          @GrossCost ,
                          5
                        )
                SELECT  @RentJournal_id = SCOPE_IDENTITY()
-- Insertion into F_SALESINVOICEITEM    
                INSERT  INTO F_SALESINVOICEITEM
                        ( SALEID ,
                          SALESCATID ,
                          ITEMNAME ,
                          ITEMDESC ,
                          SIDATE ,
                          NETCOST ,
                          VATTYPE ,
                          VAT ,
                          GROSSCOST ,
                          USERID ,
                          ACTIVE ,
                          SITYPE ,
                          SISTATUS ,
                          RENTJOURNALID
                        )
                VALUES  ( @SaleID ,
                          1 ,
                          @FaultDescription ,
                          @FaultDescription ,
                          GETDATE() ,
                          @NetCost ,
                          @VatType ,
                          @rVAT ,
                          @GrossCost ,
                          @UserID ,
                          1 ,
                          1 ,
                          1 ,
                          @RentJournal_id
                        )
                SELECT  @SALEITEMID = SCOPE_IDENTITY()
-- Insertion into F_SALEITEM_TO_PURCHASEITEM    
                INSERT  INTO F_SALEITEM_TO_PURCHASEITEM
                        ( SALEITEMID )
                VALUES  ( @SALEITEMID )
-- Execute procedure for sales order    
                EXEC NL_SALESORDER @SaleID

            END -- END RECHARGE    

---------------------End of Recharge Process-------------------------------------------    

----------------------FollowOn Notes -------------------------------------------    
        IF ( ( NOT ( @FollowOnNotes = 'N/A' )
             )
             AND ( LEN(@FollowOnNotes) > 0 )
           ) 
            BEGIN
-- Insertion into FL_FAULT_FOLLOWON
                INSERT  INTO FL_FAULT_FOLLOWON
                        ( FaultLogId ,
                          RecordedOn ,
                          FollowOnNotes
                        )
                VALUES  ( @FaultLogID ,
                          GETDATE() ,
                          @FollowOnNotes
                        )
            END

---------------------End of FollowOn Notes-------------------------------------------    

-- Update Fault Status in FL_FAULT_LOG
        UPDATE  FL_FAULT_LOG
        SET     StatusID = @CompleteStatusId ,
                CompletedDate = ISNULL(@FaultLogCompletedDate, CompletedDate)
        WHERE   FaultLogID = @FaultLogId

-- Update Fault Status in FL_FAULT_JOURNAL    
        UPDATE  FL_FAULT_JOURNAL
        SET     FaultStatusID = @CompleteStatusId ,
                LastActionDate = @RepairCompletionDateTime
        WHERE   FaultLogID = @FaultLogId

-- Insertion into FL_FAULT_LOG_HISTORY            
        INSERT  INTO FL_FAULT_LOG_HISTORY
                ( JournalID ,
                  FaultStatusID ,
                  LastActionDate ,
                  LastActionUserID ,
                  FaultLogID ,
                  PropertyID
                )
        VALUES  ( @JOURNALID ,
                  @CompleteStatusId ,
                  @RepairCompletionDateTime ,
                  @UserId ,
                  @FaultLogId ,
                  @PropertyId
                )

--Update ENDTIME in FL_FAULT_JOBTIMESHEET    
        UPDATE  FL_FAULT_JOBTIMESHEET
        SET     EndTime = @RepairCompletionDateTime
        WHERE   FaultLogId = @FaultLogId
                AND TimeSheetID = ( SELECT  MAX(TimeSheetID)
                                    FROM    FL_FAULT_JOBTIMESHEET
                                    WHERE   FaultLogId = @FaultLogId
                                  )

--Update AppointmentStatus,RepairNotes and LastActionDate in FL_CO_APPOINTMENT    

        IF @IsAppointmentCompleted = 1 
            BEGIN
                UPDATE  FL_CO_APPOINTMENT
                SET     AppointmentStatus = 'Complete' ,
                        RepairNotes = @RepairNotes ,
                        LastActionDate = GETDATE() ,
                        RepairCompletionDateTime = @RepairCompletionDateTime
                WHERE   AppointmentID = @AppointmentId
            END
        ELSE 
            BEGIN
                UPDATE  FL_CO_APPOINTMENT
                SET     RepairNotes = @RepairNotes ,
                        LastActionDate = GETDATE() ,
                        RepairCompletionDateTime = @RepairCompletionDateTime
                WHERE   AppointmentID = @AppointmentId
            END

        SET @Success = 1

        IF @Success = 0 
            BEGIN
                ROLLBACK TRANSACTION
                SET @Result = -1
                PRINT 'Unable to update the record'
            END
        ELSE 
            BEGIN
                SET @Result = 1
                COMMIT TRAN
            END
    END -- END main BEGIN
GO
