-- Stored Procedure

-- =============================================
-- EXEC FL_GetNoEntryDetail @jobSheetNumber ='JS5'
-- Author:		<Ahmed Mehmood>
-- Create date: <15/2/2013>
-- Description:	<Returns No Entry Detail>
-- =============================================
IF OBJECT_ID('dbo.FL_GetNoEntryDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetNoEntryDetail AS SET NOCOUNT ON;') 
GO

ALTER  PROCEDURE [dbo].[FL_GetNoEntryDetail] 
	-- Add the parameters for the stored procedure here
	(
	@jobSheetNumber varchar(50)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
Declare @schemeId INT = 0, @blockId INT = 0 

	SET NOCOUNT ON;
Select @schemeId = SchemeId,@blockId=BlockId from FL_FAULT_LOG WHERE	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber   
	if @schemeId > 0 Or @blockId > 0 
	BEGIN 

	-- Job Sheet Detail Customer Information
	EXEC FL_GetSbJobSheetDetail @jobSheetNumber

	END
	ELSE
	BEGIN 
	-- Job Sheet Detail Customer Information
	EXEC FL_GetJobSheetAndCustomerDetail @jobSheetNumber

	END

	-- No Entry Detail
	SELECT	 CONVERT(VARCHAR(17), FL_FAULT_NOENTRY.RecordedOn, 113) RecordedOn,FL_FAULT_NOENTRY.Notes,FL_FAULT_LOG.SchemeId
	FROM	FL_FAULT_LOG INNER JOIN FL_FAULT_NOENTRY on FL_FAULT_LOG.FaultLogID = FL_FAULT_NOENTRY.FaultLogId
	WHERE	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
	
	-- Property Address  Info
	EXEC dbo.FL_GetPropertyDetail @jobSheetNumber


END
