SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[stp_SelectContactListNames]

@JOBID int

AS

select L.USER_ID, l.firstname + ' ' + L.LASTNAME AS FULLNAME, L.EMAIL 
from h_login l
	inner join TASK_USER TU ON TU.USER_ID = L.USER_ID
where TU.JOB_ID = @JOBID


GO
