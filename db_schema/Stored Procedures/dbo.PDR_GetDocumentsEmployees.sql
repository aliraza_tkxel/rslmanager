USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDocumentsEmployees]    Script Date: 03/17/2016 19:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
/* =================================================================================    
    Page Description: Get All Employees who uploaded At Least one Document

    Author: Fakhar uz zaman
    Creation Date: Mar-17-2016


    Execution Command:

    Exec PDR_GetDocumentsEmployees
  =================================================================================*/


IF OBJECT_ID('dbo.PDR_GetDocumentsEmployees') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetDocumentsEmployees AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

  
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetDocumentsEmployees]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT 
	EMP.EMPLOYEEID AS EmployeeId, (EMP.FIRSTNAME + EMP.LASTNAME) AS EmployeeName
	FROM PDR_DOCUMENTS DOC JOIN E__EMPLOYEE EMP ON DOC.CreatedBy = EMP.EMPLOYEEID
	ORDER BY EmployeeName
END



   
   
   