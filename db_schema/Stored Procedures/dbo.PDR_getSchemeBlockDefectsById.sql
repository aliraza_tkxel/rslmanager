USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_getSchemeBlockDefectsById') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_getSchemeBlockDefectsById AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_getSchemeBlockDefectsById](
@id int
)
AS
BEGIN
	
	SET NOCOUNT ON;

		SELECT Convert(varchar(10),DefectDate,103) AS Date
		, PropertyDefectId
		,IsDefectIdentified
		,ISNULL(IsActionTaken,0) as IsActionTaken
		,Description
		,ISNULL(IsWarningFixed,0) as IsWarningFixed 
		,ISNULL(IsWarningIssued,0) as IsWarningIssued
		,ISNULL(DefectNotes,'') DefectNotes
		,CASE 
			WHEN ApplianceId IS NOT NULL AND ApplianceId <> 0 THEN
				'Appliance'
			WHEN BoilerTypeId IS NOT NULL AND BoilerTypeId <> 0 THEN
				'Boiler'
			ELSE
				'Detector'			
		END AS	DefectType
		FROM	P_PROPERTY_APPLIANCE_DEFECTS	
				INNER JOIN	P_DEFECTS_CATEGORY on P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
				LEFT JOIN	GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
		WHERE	P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId = @id 
				AND (
						(
							(ApplianceId IS NOT NULL AND ApplianceId <> 0 )
							AND GS_PROPERTY_APPLIANCE.ISACTIVE =1
						) 
						OR 
							(ApplianceId IS NULL or ApplianceId = 0 )
					)
			
END
