SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adnan Mirza>
-- Create date: <22 July 2010>
-- Description:	<To link Properties with Tenancy End dates (using 1 row per property)>
-- =============================================
CREATE PROCEDURE [dbo].[NROSH_TENANCYENDDATE] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @PropertyId nvarchar(12)
	Declare @count int

	CREATE TABLE dbo.PropertyEndDate
	   (
		SID INT IDENTITY(1,1),
		PROPERTYID NVARCHAR(12),
		ENDDATE1 NVARCHAR(10),	ENDDATE2 NVARCHAR(10),	ENDDATE3 NVARCHAR(10),	ENDDATE4 NVARCHAR(10),
		ENDDATE5 NVARCHAR(10),	ENDDATE6 NVARCHAR(10),	ENDDATE7 NVARCHAR(10),	ENDDATE8 NVARCHAR(10),
		ENDDATE9 NVARCHAR(10),	ENDDATE10 NVARCHAR(10),	ENDDATE11 NVARCHAR(10),	ENDDATE12 NVARCHAR(10),
		ENDDATE13 NVARCHAR(10),	ENDDATE14 NVARCHAR(10),	ENDDATE15 NVARCHAR(10)
		
	   )


	CREATE TABLE #TenancyStartEndDate
	   (
		SID INT IDENTITY(1,1),
		PROPERTYID NVARCHAR(12),
		STARTDATE SMALLDATETIME,
		ENDDATE SMALLDATETIME
	   )


	DECLARE c1 CURSOR READ_ONLY

	FOR

	 SELECT COUNT(C.PROPERTYID) AS PCOUNT,C.PROPERTYID FROM C_TENANCY C
	   INNER JOIN P__PROPERTY P ON P.PROPERTYID=C.PROPERTYID
	   WHERE P.PROPERTYTYPE NOT IN (6,8,9,16)
	   AND (P.STATUS<>6 AND ISNULL(P.SUBSTATUS,0)<>23)
	   GROUP BY C.PROPERTYID
	   
	OPEN c1

	FETCH NEXT FROM c1
	INTO @count,@PropertyId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		   INSERT INTO #TenancyStartEndDate (PROPERTYID,STARTDATE,ENDDATE)
		   (
				SELECT PROPERTYID,STARTDATE,ENDDATE FROM C_TENANCY WHERE PROPERTYID=@PropertyId
		   )

		insert into dbo.PropertyENDDATE (PROPERTYID,ENDDATE1,ENDDATE2,ENDDATE3,ENDDATE4,ENDDATE5,ENDDATE6,ENDDATE7,ENDDATE8,ENDDATE9,ENDDATE10,ENDDATE11,ENDDATE12,ENDDATE13,ENDDATE14,ENDDATE15)
		   SELECT *
			FROM
			(
				SELECT SID,PROPERTYID,CONVERT(VARCHAR(10), ENDDATE, 120) SD FROM #TenancyStartEndDate
			)		 A
			PIVOT (MAX(SD) FOR SID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15])) AS PVT
						
			
			DELETE FROM #TenancyStartEndDate
			DBCC CHECKIDENT ("#TenancyStartEndDate",RESEED,0)


	FETCH NEXT FROM c1
		INTO @count,@PropertyId

	END	

	CLOSE C1
	DEALLOCATE C1

END
GO
