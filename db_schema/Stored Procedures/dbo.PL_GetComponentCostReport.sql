SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Exec PL_GetComponentCostReport
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,12/03/2014>
-- Description:	<Description,,get the cost component report data.>
-- WebPage: AnnualSpendReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PL_GetComponentCostReport](
@startYear varchar(15),
@selectedScheme int,
@searchText varchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Declare @currentyear int
--SET @currentyear = (select DATEPART(year,getdate()))

    -- Insert statements for procedure here
Select tblCompoName.COMPONENTNAME,'< '+ tblComponent.yr as yr,ISNULL(tblComponent.totalProperties,0) as PropertyCount ,ISNULL(tblComponent.cost,0) as cost
,ISNULL(tblCurrentComponent.totalProperties,0) as PropertyCount,ISNULL(tblCurrentComponent.cost,0) as Cost,@startYear as yr
,ISNULL(tblNextComponent.totalProperties,0) as PropertyCount,ISNULL(tblNextComponent.cost,0) as cost,DATEPART(YEAR, DateAdd(YEAR,1, GETDATE())) as yr
,ISNULL(tbl2016Component.totalProperties,0) as PropertyCount,ISNULL(tbl2016Component.cost,0) as cost,DATEPART(YEAR, DateAdd(YEAR,2, GETDATE())) as yr
,ISNULL(tblnext2016Component.totalProperties,0) as PropertyCount,ISNULL(tblnext2016Component.cost,0) as cost,DATEPART(YEAR, DateAdd(YEAR,3, GETDATE())) as yr
From
	(Select COMPONENTNAME,COMPONENTID from PLANNED_COMPONENT) as tblCompoName
	Left outer Join  
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
	Where DATEPART(year,DueDate) < @startYear AND P_DEVELOPMENT.DEVELOPMENTID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tblComponent
	on tblCompoName.COMPONENTID = tblComponent.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
	Where DATEPART(year,DueDate) = @startYear AND P_DEVELOPMENT.DEVELOPMENTID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) AS tblCurrentComponent 
	on tblComponent.COMPONENTID = tblCurrentComponent.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
	Where DATEPART(year,DueDate) = DATEADD(year,1, @startYear) AND P_DEVELOPMENT.DEVELOPMENTID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tblNextComponent
	on tblNextComponent.COMPONENTID = tblCompoName.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
	Where DATEPART(year,DueDate) = DATEADD(year,2, @startYear) AND P_DEVELOPMENT.DEVELOPMENTID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tbl2016Component
	on tbl2016Component.COMPONENTID = tblCompoName.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_DEVELOPMENT on P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
	Where DATEPART(year,DueDate) = DATEADD(year,3, @startYear) AND P_DEVELOPMENT.DEVELOPMENTID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tblnext2016Component
	on tblnext2016Component.COMPONENTID = tblCompoName.COMPONENTID
Where tblCompoName.COMPONENTNAME like '%'+@searchText+'%'

END
GO
