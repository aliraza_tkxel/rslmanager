USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_POInvoiceReceivedCount]    Script Date: 4/11/2017 7:10:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[F_POInvoiceReceivedCount]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[F_POInvoiceReceivedCount] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[F_POInvoiceReceivedCount]    
     
 @USERID int    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  DECLARE @POSTATUSID INT  
 DECLARE @FINALPOSTATUSNAME NVARCHAR(100)  
 SELECT @POSTATUSID = PS.POSTATUSID, @FINALPOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'invoice approved' 
 SELECT COUNT( DISTINCT  PO.ORDERID) Total
				                                                                                              
                 FROM F_PURCHASEORDER PO   
                 CROSS APPLY (SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) PI  
 	               Left JOIN F_PURCHASEITEM P ON P.ORDERID = PO.ORDERID AND PISTATUS IN (7) 
                   INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PO.POSTATUS   
                   LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  
                   LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID  
                  CROSS APPLY (SELECT TOP 1 image_url AS image_url FROM F_PURCHASEITEM_LOG POL WHERE POL.ORDERID = PO.ORDERID AND image_url IS NOT NULL  
                  ORDER BY TIMESTAMP DESC) Invoice  
                   Cross Apply (SELECT 1 AS ONE from F_PURCHASEITEM FPI  Where FPI.PISTATUS =7 AND FPI.ORDERID = PO.ORDERID   
					AND image_url IS NOT NULL  ) InvoiceUploaded  
                 WHERE PO.ACTIVE = 1 
                   AND (( P.APPROVED_BY IS NULL AND P.USERID =  @USERID ) OR (P.APPROVED_BY=  @USERID))    
                 ORDER BY Total  DESC 
   
    
END 