USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetAttributeList]    Script Date: 28-Jun-16 11:18:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,5/2/2016>
-- Description:	<Description,Get Attribute List for Cyclic Maintenance and M & E Servicing To Be Arrange and apointment arranged List Filter.>
-- =============================================
IF OBJECT_ID('dbo.PDR_GetAttributeList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_GetAttributeList AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_GetAttributeList]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 WITH CTE AS
(
 SELECT   PA_ITEM.ItemID , PA_ITEM.ItemName,PA_ITEM.AreaID,ItemSorder ,ParentItemId ,AreaName 
	FROM PA_AREA   
	INNER JOIN PA_ITEM ON PA_ITEM.AreaID = PA_AREA.AreaID   
	WHERE PA_AREA.IsActive = 1 AND ParentItemId IS NULL AND PA_ITEM.IsActive = 1 AND ShowListView = 0 AND PA_ITEM.IsForSchemeBlock=1
  
  UNION ALL
  
  SELECT  PA_ITEM.ItemID,PA_ITEM.ItemName, PA_ITEM.AreaID,PA_ITEM.ItemSorder,PA_ITEM.ParentItemId,PA_AREA.AreaName  FROM PA_ITEM 
  INNER JOIN CTE ON PA_ITEM.ParentItemId = CTE.ItemID
  INNER JOIN PA_AREA ON PA_ITEM.AreaID = PA_AREA.AreaID 
	WHERE PA_ITEM.IsActive = 1 AND PA_AREA.IsActive = 1 AND ShowListView = 0 AND PA_AREA.IsForSchemeBlock=1 AND PA_ITEM.IsForSchemeBlock=1
)


Select * 
	FROM (
		SELECT DISTINCT P.ItemName AS ItemName, CTE.AreaID AS AreaID, CTE.ItemID AS ItemID, CTE.ItemSorder AS ItemSorder,
		ROW_NUMBER() OVER(PARTITION BY P.ItemName ORDER BY CTE.AreaID,CTE.ItemID,CTE.ItemSorder ASC) AS row_number
		--CASE WHEN CTE.ParentItemId IS NULL then CTE.AreaName +' => ' + P.ItemName
		--ELSE CTE.AreaName +' => ' + parent.ItemName +' => ' + P.ItemName END AS ItemName,
		 FROM PA_ITEM P
		 INNER JOIN CTE ON P.ItemID = CTE.ItemID
		 Left OUTER JOIN PA_ITEM parent on CTE.ParentItemId = parent.ItemID
		 WHERE
		 P.IsActive = 1 AND P.ShowListView = 0 
		 AND P.ItemID NOT IN  (
			 SELECT DISTINCT I.ItemID FROM PA_ITEM I
			 Cross APPLY (SELECT ItemId FROM PA_ITEM pitem WHERE I.ItemID = pitem.ParentItemId AND I.ItemName NOT IN ('Kitchen','Bathroom') )parent)
		--ORDER BY CTE.AreaID,CTE.ItemID,CTE.ItemSorder ASC
	) result
	WHERE row_number = 1
	ORDER BY ItemName ASC

END

Go
