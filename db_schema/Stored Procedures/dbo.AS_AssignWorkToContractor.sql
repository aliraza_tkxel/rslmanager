USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[AS_AssignWorkToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_AssignWorkToContractor] AS SET NOCOUNT ON;') 
GO


ALTER  PROCEDURE [dbo].[AS_AssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@propertyId NVARCHAR(100),
	@blockId NVARCHAR(100),
	@schemeId NVARCHAR(100),
	@contactId INT,
	@contractorId INT,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200) = '', 	
	@POStatus INT,
	@journalId INT ,
	@ContractorWorksDetail AS FL_AssingToContractorWorksRequired READONLY,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT,
	@POStatusId INT OUTPUT,
	@orderId INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	BEGIN TRY
	-- =====================================================
	-- General Purpose Variable
	-- =====================================================
	DECLARE @CurrentDateTime AS datetime2 = GETDATE()
	--DECLARE @BlockId AS INT
	DECLARE @AppointmentType AS varchar(30)
	DECLARE @DevelopmentId AS INT
	--DECLARE @SchemeId AS INT
	
	--SELECT @BlockId= BLOCKID,@SchemeId=SCHEMEID,@DevelopmentId=DEVELOPMENTID FROM P__PROPERTY WHERE PROPERTYID=@propertyId

	
    -- =====================================================
	-- Insert new Purchase Order
	-- =====================================================

	DECLARE @Active bit = 1
	, @POTYPE int = (SELECT
		POTYPEID
	FROM F_POTYPE
	WHERE POTYPENAME = 'Repair') -- 2 = 'Repair'

	-- To get Identity Value of Purchase Order.
	, @purchaseOrderId int

	INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
	POTYPE, POSTATUS, GASSERVICINGYESNO, POTIMESTAMP, BLOCKID, DEVELOPMENTID, SchemeId)
	VALUES (UPPER('WORK ORDER'), @CurrentDateTime, 'This purchase order was created for gas servicing', 
	@userId, @ContractorId, @ACTIVE, @POTYPE,@POSTATUS, 0, @CurrentDateTime, @blockId, @DevelopmentId, @schemeId)

	SET @purchaseOrderId = SCOPE_IDENTITY()


	-- =====================================================
	-- Old table(s)
	-- Insert new P_WORKORDER
	-- =====================================================
	DECLARE @Title nvarchar(50) = 'Assigned To Contractor'
	DECLARE @WOSTATUS INT = 6-- 'IN PROGRESS
	If (@POStatus = 0) SET @WOSTATUS = 12 --Pending
	DECLARE @BIRTH_NATURE INT = 0 --'PLANNED MAINTENANCE'
	DECLARE @BIRTH_ENTITY INT = NULL --'PLANNED MAINTENANCE PROGRAMME CAN BE CREATED FOR MULTIPLE PATCHES AND SCHEMES'
	DECLARE @BIRTH_MODULE INT = 4 --'PLANNED MAINTENANCE'

	INSERT INTO P_WORKORDER (ORDERID, TITLE, WOSTATUS, CREATIONDATE, BIRTH_MODULE, BIRTH_ENTITY,
								BIRTH_NATURE, DEVELOPMENTID, BLOCKID, GASSERVICINGYESNO, SchemeId, PROPERTYID)
	VALUES(@purchaseOrderId, @TITLE, @WOSTATUS, @CurrentDateTime, @BIRTH_MODULE, @BIRTH_ENTITY
	, @BIRTH_NATURE, @DevelopmentId, @blockId, 0, @schemeId,@propertyId)

	DECLARE @WOID INT = SCOPE_IDENTITY()
	
	
	-- =====================================================
	-- update data in AS_JOURNAL
	-- =====================================================
	DECLARE @newstatus INT 
	
	SELECT @newstatus = statusid FROM AS_Status WHERE Title='Assigned to Contractor'
	UPDATE AS_JOURNAL SET STATUSID = @newstatus WHERE JOURNALID=@journalId
	
	
	
	
	-- =====================================================
	-- Insert data in AS_CONTRACTOR_WORK
	-- =====================================================

	DECLARE @applianceServicingContractorId int

	INSERT INTO AS_CONTRACTOR_WORK (JournalId, ContractorId, AssignedDate, AssignedBy
	, EstimateRef, ContactId, PurchaseORDER)
	VALUES (@journalId,@ContractorId,@CurrentDateTime,@userId,@EstimateRef,@contactId, @PurchaseOrderId)

	SET @applianceServicingContractorId = SCOPE_IDENTITY()
	

-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
*FROM @ContractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE @WorksRequired nvarchar(4000),
@NetCost smallmoney,
@VatType int,
@VAT smallmoney,
@GROSS smallmoney,
@PIStatus int,
@ExpenditureId INT =3788

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(100) = 'Gas Servicing Cyclical Repairs' -- Title for Purchase Items, specially to inset in F_PurchaseItem
Select @ExpenditureId=E.EXPENDITUREID from F_EXPENDITURE E
	Inner JOIN F_HEAD H on E.HEADID=H.HEADID
	INNER JOIN F_COSTCENTRE C ON H.COSTCENTREID = C.COSTCENTREID
  WHERE C.DESCRIPTION = 'Property Asset Management' AND H.DESCRIPTION='Servicing' AND E.DESCRIPTION='Gas Servicing'
-- =====================================================
-- Loop (Start) through records and insert works required
-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @WorksRequired, @NetCost, @VatType, @VAT,
		@GROSS, @PIStatus, @ExpenditureId
		WHILE @@FETCH_STATUS = 0 BEGIN


		-- =====================================================  
  -- Old table(s)  
  -- INSERT VALUE IN C_JOURNAL FOR EACH PROPERTY SELECTED ON WORKORDER  
  -- =====================================================  
  
  DECLARE @ITEMID INT = 1 --'PROPERTY'  
  DECLARE @STATUS INT = 2 --'ASSIGNED  
  If (@POStatus = 0) SET @STATUS = 12 --Pending  

  DECLARE @ITEMACTIONID INT = 2 --'ASSIGNED TO CONTRACTOR'  
  
	  If (@POStatus = 0) SET @ITEMACTIONID = 12 --Pending  
  
  if(@propertyId is not null)
  begin 
	INSERT INTO C_JOURNAL (  PROPERTYID, ITEMID,ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE)  
	  VALUES( @PROPERTYID, @ITEMID, @BIRTH_NATURE, @STATUS, @CurrentDateTime,@TITLE)  
  
	  DECLARE @CJOURNALID INT = SCOPE_IDENTITY() 
	  
	  -- =====================================================  
	  -- Old table(s)  
	  --INSERT VALUE IN C_REPAIR FOR EACH PROPERTY SELECTED ON WORKORDER  
	  -- =====================================================  
  
	  DECLARE @PROPERTYADDRESS NVARCHAR(400)  
  
	  SELECT @PROPERTYADDRESS = ISNULL(FLATNUMBER+',','')+ISNULL(HOUSENUMBER+',','')+ISNULL(ADDRESS1+',','')+ISNULL(ADDRESS2+',','')+ISNULL(ADDRESS3+',','')+ISNULL(TOWNCITY+',','')+ISNULL(POSTCODE,'')  
	  FROM P__PROPERTY  
	  WHERE PROPERTYID = @PROPERTYID  
  
	  --ITEMDETAILID WILL BE NULL BECAUSE THE COST OF WORKORDER DEPENDS ON PROGRAMME OF PLANNED MAINTENANCE   
	  --AND THE SCOPEID WILL ALSO BE NULL BECAUSE IT IS NOT A GAS SERVICING CONTRACT  
  
	  INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE,LASTACTIONUSER, ITEMDETAILID,  
			 CONTRACTORID, TITLE, NOTES,SCOPEID)   
	  VALUES(@CJOURNALID, @STATUS, @ITEMACTIONID, @CurrentDateTime, @userId,NULL,@CONTRACTORID,'FOR'+@PROPERTYADDRESS,@TITLE,NULL)  
  
	  DECLARE @REPAIRHISTORYID INT = SCOPE_IDENTITY() 
  end
  

		-- =====================================================
		--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
		-- =====================================================

		INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
		NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
			VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @WorksRequired, 
			@CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

		DECLARE @ORDERITEMID int = SCOPE_IDENTITY()

		 -- =====================================================  
  --INSERT VALUE IN P_WOTOREPAIR for each work required  
  -- =====================================================  
  
  INSERT INTO P_WOTOREPAIR(WOID, JOURNALID, ORDERITEMID)  
  VALUES(@WOID,@CJOURNALID,@ORDERITEMID)  
  

		-- =====================================================
		-- Insert values in AS_CONTRACTOR_WORK_DETAIL for each work required
		-- =====================================================




--@ContractorId
		INSERT INTO AS_CONTRACTOR_WORK_DETAIL(ContractorId, WorkRequired, NetCost
			, VatId, Vat, Gross, ExpenditureId, PURCHASEORDERITEMID)
		VALUES(@applianceServicingContractorId, @WorksRequired, @NetCost, @VatType, @VAT, @GROSS
				, @ExpenditureId,@ORDERITEMID)
				
				
				
				

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorksRequired, @NetCost, @VatType, @VAT,
@GROSS, @PIStatus, @ExpenditureId
END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

IF @POStatus = 0
 BEGIN
  UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = @WOID
 END
ELSE
 BEGIN
	-- - - -  - - - - - - -  -
	-- AUTO ACCEPT REPAIRS
	-- - ---- - - - -  - - - -
	-- we check to see if the org has an auto accept functionality =  1
	-- if so then we auto accept the repair
  EXEC C_REPAIR_AUTO_ACCEPT @ORDERID = @PurchaseOrderId, @SUPPLIERID =  @ContractorId
END


CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor
END TRY
-- =====================================================
--INSERT VALUE IN PM_WORKORDER_CONTRACT TABLE TO CONNECT IT TO THE PM PROPERTIES -- Old RSL PLANNED MAINTANANCE module
-- =====================================================
--IF @PROPLISTID IS NOT NULL
--BEGIN
--	INSERT INTO PM_WORKORDER_CONTRACT (WORKORDERID, PROPERTYLISTID)
--	VALUES(@WOID,@PROPLISTID)
--END


BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = @journalId
SET @POStatusId = @POStatus
SET @orderId= @PurchaseOrderId
END