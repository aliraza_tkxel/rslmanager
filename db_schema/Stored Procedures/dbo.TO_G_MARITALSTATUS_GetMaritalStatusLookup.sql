SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_G_MARITALSTATUS_GetMaritalStatusLookup
/* ===========================================================================
 '   NAME:           TO_G_MARITALSTATUS_GetMaritalStatusLookup
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get marital status records from G_MARITALSTATUS table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT MARITALSTATUSID AS id,DESCRIPTION AS val
	FROM G_MARITALSTATUS
	ORDER BY DESCRIPTION ASC

GO
