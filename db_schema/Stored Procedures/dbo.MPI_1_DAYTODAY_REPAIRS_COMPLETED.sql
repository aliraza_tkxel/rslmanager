SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- 	EXEC MPI_1_DAYTODAY_REPAIRS_COMPLETED '', '', 'JUN', 11

CREATE      PROCEDURE [dbo].[MPI_1_DAYTODAY_REPAIRS_COMPLETED] (
		@DEVELOPMENTID VARCHAR(10), 
		@ASSETTYPE VARCHAR(10), 
		@MONTH VARCHAR(10), 
		@YEAR INT
		)
AS

DECLARE @SQL VARCHAR (4000)
DECLARE @SQL_DEV_CRITERIA VARCHAR (200)
DECLARE @SQL_ASSETTYPE_CRITERIA VARCHAR (200)
DECLARE @SQL_STARTDATE_CRITERIA VARCHAR (20)
DECLARE @SQL_ENDDATE_CRITERIA VARCHAR (20)
DECLARE @FYSTARTDATE SMALLDATETIME 
DECLARE @FYENDDATE SMALLDATETIME
DECLARE @MONTHYEAR INT
DECLARE @LASTDAYOFMONTH INT

SET @SQL_DEV_CRITERIA = ''
SET @SQL_ASSETTYPE_CRITERIA = ''

-- GET START AND ENDDATES OF CHOSEN FISCAL YEAR
SELECT @FYSTARTDATE = YSTART, @FYENDDATE = YEND FROM F_FISCALYEARS WHERE YRANGE = @YEAR

-- FIRST WE SET THE DATE CRITERIA
IF @MONTH = '' 
	BEGIN
	   SET @SQL_STARTDATE_CRITERIA = @FYSTARTDATE
	   SET @SQL_ENDDATE_CRITERIA = @FYENDDATE
	END
IF @MONTH <> '' 
	BEGIN
 	   -- FIRST GET THE CORRECT YEAR TO USE
	   IF @MONTH IN ('JAN', 'FEB', 'MAR')
		SET @MONTHYEAR = YEAR(@FYENDDATE)
	   ELSE
		SET @MONTHYEAR = YEAR(@FYSTARTDATE)
	   -- THEN BUILD DATE STRINGS
	   SET @SQL_STARTDATE_CRITERIA = '1 ' + @MONTH + ' ' + CAST(@MONTHYEAR AS VARCHAR)
  	   SET @LASTDAYOFMONTH = DATEPART(DAY,DATEADD(d, -DAY(DATEADD(m,1,@SQL_STARTDATE_CRITERIA)),DATEADD(m,1,@SQL_STARTDATE_CRITERIA)))
	   SET @SQL_ENDDATE_CRITERIA = CAST(@LASTDAYOFMONTH AS VARCHAR) + ' '  + @MONTH + ' ' + CAST(@MONTHYEAR AS VARCHAR)
	END

-- NOW SET OTHER CRITERIA
IF @DEVELOPMENTID <> ''
	   SET @SQL_DEV_CRITERIA = ' AND DALL.DEVELOPMENTID = ' + @DEVELOPMENTID
IF @ASSETTYPE <> ''
	   SET @SQL_ASSETTYPE_CRITERIA = ' AND P.ASSETTYPE = ' + @ASSETTYPE

-- COUNT THE NUMBER OF REPAIRS IN PROGRESS
SET @SQL = '
SELECT PRIORITY,SUM(A.COMPLETED) AS COMPLETED,SUM(COST) AS COST,SORTORDER  FROM
(
SELECT 	ISNULL(COUNT(*),0) AS COMPLETED, PRI.KPIPRIORITY AS PRIORITY, PRI.SORTORDER, SUM(PI.GROSSCOST) AS COST
FROM 	C_JOURNAL J
	INNER JOIN (
		    SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR 
		    GROUP BY JOURNALID
		   ) R ON R.JOURNALID = J.JOURNALID
	INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID 
	INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = RP.ITEMDETAILID
	LEFT JOIN (SELECT MAX(COMPLETIONDATE) AS COMPLETIONDATE,JOURNALID FROM C_MAXCOMPLETEDREPAIRDATE GROUP BY JOURNALID) RC ON RC.JOURNALID = J.JOURNALID  
	LEFT JOIN (SELECT MAX(LASTACTIONDATE) AS LASTACTIONDATE,JOURNALID FROM C_LASTCOMPLETEDACTION GROUP BY JOURNALID) LC ON LC.JOURNALID = J.JOURNALID    
	LEFT JOIN R_PRIORITY PRI ON PRI.PRIORITYID = REP.PRIORITY
	LEFT JOIN P_WOTOREPAIR WOT ON WOT.JOURNALID = J.JOURNALID
	LEFT JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WOT.ORDERITEMID
	LEFT JOIN P_WORKORDER W ON WOT.WOID = W.WOID
	LEFT JOIN P__PROPERTY P ON P.PROPERTYID = W.PROPERTYID
	LEFT JOIN P_BLOCK B ON B.BLOCKID = W.BLOCKID
	LEFT JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = W.DEVELOPMENTID
	LEFT JOIN P_DEVELOPMENT DALL ON DALL.DEVELOPMENTID = P.DEVELOPMENTID
		OR DALL.DEVELOPMENTID = B.DEVELOPMENTID OR DALL.DEVELOPMENTID = D.DEVELOPMENTID
WHERE 	(REP.R_EXP_ID = 24) AND J.CURRENTITEMSTATUSID IN (10,11,22) ' + @SQL_DEV_CRITERIA + @SQL_ASSETTYPE_CRITERIA + '
	AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) >= ''' + @SQL_STARTDATE_CRITERIA + '''
	AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) <= ''' + @SQL_ENDDATE_CRITERIA + '''
	AND J.ITEMNATUREID <> 62
GROUP 	BY PRI.KPIPRIORITY, PRI.SORTORDER 
UNION
	SELECT ISNULL(COUNT(FL_LOG.FAULTLOGID),0) AS COMPLETED, PRI.PRIORITYNAME AS PRIORITY, PRI.SORTORDER, SUM(I.GROSSCOST) AS COST
	FROM FL_FAULT_LOG FL_LOG  
							INNER JOIN FL_FAULT_JOURNAL FJ ON FJ.FAULTLOGID=FL_LOG.FAULTLOGID  
							INNER JOIN FL_FAULT FL ON FL.FAULTID=FL_LOG.FAULTID  
							INNER JOIN FL_FAULT_PRIORITY PRI ON PRI.PRIORITYID = FL.PRIORITYID 
							INNER JOIN (SELECT MAX(CJOURNALID) AS CJOURNALID,FJOURNALID FROM FL_FAULTJOURNAL_TO_CJOURNAL GROUP BY FJOURNALID) FJ_CJ  ON FJ_CJ.FJOURNALID=FJ.JOURNALID  
							INNER JOIN C_JOURNAL J ON J.JOURNALID = FJ_CJ.CJOURNALID   
							INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID   
							INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID  AND I.PITYPE = 2    
							INNER JOIN P__PROPERTY P ON P.PROPERTYID = FJ.PROPERTYID   
							INNER JOIN P_DEVELOPMENT PD ON PD.DEVELOPMENTID = P.DEVELOPMENTID   
							INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR GROUP BY JOURNALID) R ON R.JOURNALID = J.JOURNALID   
							INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID  AND RP.LASTACTIONDATE >= ''' + @SQL_STARTDATE_CRITERIA + ''' AND RP.LASTACTIONDATE <= ''' + @SQL_ENDDATE_CRITERIA + ''' AND RP.ITEMSTATUSID=11 AND  RP.ITEMACTIONID IN (6,9,10,15)    
							WHERE 1=1  ' + @SQL_ASSETTYPE_CRITERIA + '
	GROUP BY PRI.PRIORITYNAME,PRI.SORTORDER

) 
A
GROUP BY A.PRIORITY,A.SORTORDER '
	

EXECUTE(@SQL)










GO
