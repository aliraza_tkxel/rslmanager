
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC PLANNED_GetLetterByActionId @statusId = 0
-- Author:		<Ahmed Mehmood>
-- Create date: <30/10/2013>
-- Description:	<Get Letters against Action Id>
-- Web Page: ReplacementList.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetLetterByActionId](
	-- Add the parameters for the stored procedure here
	@actionId int = -1	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT PLANNED_StandardLetters.StandardLetterId as LetterId,PLANNED_StandardLetters.Title as Title
  FROM PLANNED_StandardLetters
  WHERE PLANNED_StandardLetters.ActionId = @actionId

END
GO
