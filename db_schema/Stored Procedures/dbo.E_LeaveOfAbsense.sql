USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_AbsenseSickness]    Script Date: 6/6/2017 4:33:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Rehman
-- Create date: June 2, 2017
-- =============================================

IF OBJECT_ID('dbo.E_LeaveOfAbsense') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_LeaveOfAbsense AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_LeaveOfAbsense] 
	-- Add the parameters for the stored procedure here
	@empId INT,
	@fiscalYear SMALLDATETIME = null	
	
AS
BEGIN

DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME	
	DECLARE @DAYS_ABS int

SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE(@empId)


if(@fiscalYear is not null)
begin
	SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE_ByYear(@empId, @fiscalYear)
end
	
SELECT e.STARTDATE,e.RETURNDATE,ar.DESCRIPTION as Reason,s.DESCRIPTION as Status, n.DESCRIPTION as Nature, 
 --Case 
	--WHEN e.returndate is null then
	--	dbo.EMP_SICKNESS_DURATION(j.EMPLOYEEID, e.STARTDATE, getdate(), e.JOURNALID) 
 --   WHEN e.startdate = e.returndate  AND e.duration > 1 AND dbo.Emp_sickness_duration(J.employeeid, e.startdate, e.returndate, e.journalid) !=0 THEN
	--	dbo.EMP_SICKNESS_DURATION(j.EMPLOYEEID, e.STARTDATE, e.RETURNDATE, e.JOURNALID)
	--ELSE 
	--	e.duration
 --end 
 e.duration as DAYS_ABS
 , e.HOLTYPE as HolType,
s.ITEMSTATUSID as StatusId, n.ITEMNATUREID as NatureId, ar.SID as ReasonId, e.LASTACTIONDATE as LastActionDate,e.ABSENCEHISTORYID as AbsenceHistoryId,e.DURATION_TYPE as Unit
FROM E_JOURNAL j
inner join E_ABSENCE e on j.JOURNALID = e.JOURNALID
 AND e.ABSENCEHISTORYID = (
									SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
									WHERE JOURNALID = j.JOURNALID
								  ) 
LEFT JOIN E_ABSENCEREASON ar on e.REASONID = ar.SID
INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
WHERE j.EMPLOYEEID = @empId and j.ITEMNATUREID not IN 
	(SELECT ITEMNATUREID from E_NATURE 
		where DESCRIPTION  in ('Sickness','Annual Leave','BRS TOIL Recorded','BRS Time Off in Lieu','BHG TOIL') --   ,'Birthday Leave','Personal Day'  it has been removed 
		-- because Birthday and Personal Day Leaves handle in code. 
	) 
and e.STARTDATE >= @BHSTART and  e.STARTDATE <= @BHEND
ORDER by e.STARTDATE DESC

	
	
END