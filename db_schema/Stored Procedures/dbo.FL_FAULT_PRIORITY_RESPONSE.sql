SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_FAULT_PRIORITY_RESPONSE
/* ===========================================================================
 '   NAME:          FL_FAULT_PRIORITY_RESPONSE
 '   DATE CREATED:   21 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Priority Response against a particular Priority
 '   IN:             @priorityID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@priorityID as int
AS
	SELECT ResponseTime ,
	Case Days 
	when 1 then 'Day(s)' 
	when 0 then 'hour(s)'
	 end as type
	FROM FL_FAULT_PRIORITY
where PriorityID=@priorityID













GO
