SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetActionRankingByStatusId @statusId = 1
-- Author:		<Salman Nazir>
-- Create date: <20/11/2012>
-- Description:	<Get Action ranking against StatusId>
-- WebPage: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetActionRankingByStatusId](
@statusId int
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ActionId ,StatusId  ,Title, Ranking,IsEditable
	FROM AS_Action
	WHERE StatusId = @statusId
END
GO
