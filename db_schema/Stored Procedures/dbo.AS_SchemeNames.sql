USE [RSLBHALive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_SchemeNames 
   --@patchid int = -1
-- Author:		<Salman Nazir>
-- Create date: <09/06/2012>
-- Description:	<This Stored Proceedure fetch the Scheme Names and shows in the drop down on Add User Page>
-- WebPage: Resources.aspx
-- =============================================
IF OBJECT_ID('dbo.AS_SchemeNames') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_SchemeNames AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_SchemeNames](
@patchid int = -1
)	
AS
BEGIN
		SET NOCOUNT ON;

		SELECT  P_SCHEME.SCHEMENAME ,P_SCHEME.SCHEMEID AS  DEVELOPMENTID, PDR_DEVELOPMENT.PATCHID
		From	P_SCHEME
				INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
		WHERE	(PDR_DEVELOPMENT.PATCHID = @patchid OR @patchid = -1) AND PDR_DEVELOPMENT.PATCHID IS NOT NULL
		ORDER BY CAST(ISNULL(NULLIF(LEFT(LTRIM(P_SCHEME.SCHEMENAME),PATINDEX('%[^0-9]%',LTRIM(P_SCHEME.SCHEMENAME))-1),''), '100000') AS INT)
			,LTRIM(P_SCHEME.SCHEMENAME) ASC

END

GO
