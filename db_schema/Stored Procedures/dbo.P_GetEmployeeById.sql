
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC P_GetEmployeeById 	@employeeId = 615
-- Author:		<Ahmed Mehmood>
-- Create date: <24/06/2013>
-- Description:	<This Stored Proceedure get the Employee by id >
-- Web Page: Bridge.aspx
-- =============================================
CREATE PROCEDURE [dbo].[P_GetEmployeeById](
	@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT E__EMPLOYEE.EMPLOYEEID as EmployeeId, E__EMPLOYEE.FirstName +' ' + E__EMPLOYEE.LastName as FullName,AS_USER.UserId as UserId, AS_USER.IsActive as IsActive, AS_Pages.PageId, AS_Pages.PageName as PageName
	--FROM E__EMPLOYEE 
	--INNER JOIN AS_USER ON E__EMPLOYEE.EMPLOYEEID = AS_USER.EmployeeId
	--INNER JOIN AS_User_Pages ON E__EMPLOYEE.EMPLOYEEID = AS_User_Pages.EmployeeId
	--INNER JOIN AS_Pages ON AS_Pages.PageId = AS_User_Pages.PageId
	--Where E__EMPLOYEE.EMPLOYEEID = @employeeId AND AS_Pages.IsActive = 1 AND AS_User_Pages.IsActive = 1
	
	SELECT E__EMPLOYEE.EMPLOYEEID as EmployeeId, E__EMPLOYEE.FirstName +' ' + E__EMPLOYEE.LastName as FullName, AC_LOGINS.Active as IsActive
	,(Select Description 
		From E__employee 
		INNER JOIN AS_USER ON E__EMPLOYEE.EMPLOYEEID = AS_USER.EMPLOYEEID
		INNER JOIN AS_USERType ON AS_USER.UserTypeId = AS_USERType.UserTypeId
		Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
	) as UserType
	FROM E__EMPLOYEE 	
	INNER JOIN AC_LOGINS on E__EMPLOYEE.EmployeeId = AC_LOGINS.EmployeeId
	Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
END
GO
