SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE [dbo].[sp_getorg_logins]
@user_id int
AS 

SELECT 
(select count(USER_id) from H_ORG_LOGIN_LINKAGE OL
where OL.user_id = @user_id
and OL.org_id = O.org_id ) 
as orgcount,O.*
from
H_ORGANISATION O 



GO
