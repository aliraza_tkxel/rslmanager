SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_GetStage
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/16/2014>
-- Description:	<Description,,get the stages for dropdown>
-- WebPage: CustomerDetail.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_GetStage]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Select * from C_REFERRAL_STAGES
END
GO
