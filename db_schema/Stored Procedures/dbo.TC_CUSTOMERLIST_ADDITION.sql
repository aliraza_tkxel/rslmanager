SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[TC_CUSTOMERLIST_ADDITION] (
	@CUSTOMERID INT,
	@TENANCYID INT,
	@PROGRAMMEID INT,
	@EMPID INT
)
AS
SET NOCOUNT ON

BEGIN TRAN
--DECLARE @CUSTOMERID INT
--DECLARE @TENANCYID INT
--DECLARE @PROGRAMMEID INT
--DECLARE @EMPID INT

--SET @EMPID = 343
--SET @CUSTOMERID = 8914
--SET @TENANCYID = 971997
--SET @PROGRAMMEID = 1

DECLARE @PROPERTYID VARCHAR(20)
DECLARE @CUSTOMERLISTID AS INT
DECLARE @CUSTOMERLISTSTATUSID INT

SELECT @PROPERTYID=PROPERTYID FROM C_TENANCY WHERE TENANCYID = @TENANCYID

INSERT INTO TC_CUSTOMER_LIST 
(PROGRAMMEID, PROPERTYID, CUSTOMERID)
VALUES(@PROGRAMMEID, @PROPERTYID, @CUSTOMERID)

SET @CUSTOMERLISTID=(SELECT @@IDENTITY)	
SET @CUSTOMERLISTSTATUSID=(SELECT TOP 1 CUSTOMERLISTSTATUSID FROM TC_CUSTOMER_LIST_STATUS WHERE CUSTOMERLISTSTATUS='Attached')


INSERT INTO TC_CUSTOMER_LIST_JOURNAL 
(LASTACTIONUSER, LASTACTIONDATE, CUSTOMERLISTID,CUSTOMERLISTSTATUSID)
VALUES(@EMPID, GETDATE(), @CUSTOMERLISTID,@CUSTOMERLISTSTATUSID)

--DECLARE @PSTATUSID INT

--SELECT @PSTATUSID = PROGRAMMESTATUSID FROM TC_PROGRAMME_STATUS
--WHERE PROGRAMMESTATUS = 'In Progress'
--
--UPDATE TC_PROGRAMME
--SET 
--STARTBY = @EMPID,
--STARTDATE = GETDATE(),
--PROGRAMMESTATUSID = @PSTATUSID
--WHERE PROGRAMMEID = @PROGRAMMEID

--SELECT * FROM dbo.TC_CUSTOMER_LIST

--SELECT * FROM dbo.TC_PROGRAMME WHERE PROGRAMMEID = @PROGRAMMEID

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRAN
END
ELSE
BEGIN
	COMMIT TRAN
END
RETURN @@ERROR

GO
