USE [RSLBHALive]
GO
IF OBJECT_ID('dbo.NL_GETNOMINALCODEDETAILS') IS NULL 
	EXEC('CREATE PROCEDURE dbo.NL_GETNOMINALCODEDETAILS AS SET NOCOUNT ON;') 
GO


alter PROCEDURE NL_GETNOMINALCODEDETAILS(@AccountId INT)
AS
BEGIN
		DECLARE @CountOfLinked int
        SELECT @CountOfLinked = SUM(CNT) FROM (
        SELECT COUNT(1) cnt FROM F_SALESCAT WHERE  nominalcode = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM NL_ACCRUALS WHERE  nominalacc = @AccountId
        UNION 
        SELECT COUNT(1) cnt FROM NL_BANKACCOUNT WHERE  nominalaccount = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM NL_PREPAYMENT WHERE  nominalacc = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM PM_PROGRAMME WHERE  nominalcode = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM (SELECT TOP 1 accountid FROM NL_JOURNALENTRYCREDITLINE WHERE accountid = @AccountId ) A
        UNION
        SELECT COUNT(1) cnt FROM (SELECT TOP 1 accountid FROM NL_JOURNALENTRYdebITLINE WHERE accountid = @AccountId ) A
        UNION
        SELECT COUNT(1) cnt FROM NL_BALANCESHEET WHERE accountid = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM NL_OPENINGBALANCES WHERE accountid = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM NL_PROFITANDLOSS WHERE accountid = @AccountId
        UNION
        SELECT COUNT(1) cnt FROM F_BANK_RECONCILIATION_OTHERACCOUNTS WHERE accountid = @AccountId
		UNION
        SELECT COUNT(1) cnt FROM dbo.NL_ACCOUNT WHERE PARENTREFLISTID = @AccountId 
		union
		SELECT COUNT(1) cnt FROM dbo.NL_ACCOUNT l3 INNER JOIN NL_ACCOUNT l2 ON l2.ACCOUNTID = l3.PARENTREFLISTID WHERE l2.PARENTREFLISTID = @AccountId  
        ) B



        select a.AccountNumber, 
                a.Name , 
                a.AccountType, 
				cast(a.AllocateExpenditures AS INT) AllocateExpenditures,
                LTrim(RTrim(a.isBankable)) AS isBankable,
                LTrim(RTrim(a.TaxCode)) AS TaxCode,
                cast(a.isActive  AS INT) isactive,
                @CountOfLinked as CountOfLinkedRecords,
                ISNULL(PA.AccountNumber,'') as ParentAccountNumber,  ISNULL(PA.Name,'') as ParentName,
                ISNULL(PB.AccountNumber,'') as GrandParentAccountNumber,  ISNULL(PB.Name,'') AS GrandParentName
        FROM NL_ACCOUNT a 
            LEFT JOIN NL_ACCOUNT PA ON PA.AccountId = a.PARENTREFLISTID  
            LEFT JOIN NL_ACCOUNT PB ON PB.AccountId = PA.PARENTREFLISTID 
        WHERE a.AccountId = @AccountId
END

GO
