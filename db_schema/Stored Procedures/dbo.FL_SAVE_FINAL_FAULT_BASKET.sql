
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_SAVE_FINAL_FAULT_BASKET 
/* ===========================================================================
 '   NAME:           FL_SAVE_FINAL_FAULT_BASKET
 '   DATE CREATED:   24th Oct, 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online Fault Basket
 '   PURPOSE:        To save faults in fault basket & fault log
 
 '   IN:             @CustomerId
 '   IN:             @PreferredContactDetails
 '   IN:             @SubmitDate
 '   IN:             @IamHappy
 '
 '   OUT:            @FaultBasketId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (	
	@CustomerId INT,
	@PreferredContactDetails nvarchar(500),
	@SubmitDate datetime,
	@IamHappy INT,
	@StatusId int = 1,
	@IsReported int = 0,
	@FaultBasketId INT OUTPUT
  )
AS
	BEGIN TRANSACTION

	
/*
Inserting the record in Fault Basket Table
*/
	INSERT INTO FL_FAULT_BASKET(
		CustomerID,
		PreferredContactDetails,
		SubmitDate,
		IamHappy		
	)

	VALUES(
		@CustomerId,
		@PreferredContactDetails,
		@SubmitDate,
		@IamHappy				
	)

/*
Select the recent FaultBasketId SubmitDate, StatusID,IsReported,
*/

SELECT @FaultBasketID=FaultBasketId
FROM FL_FAULT_BASKET
WHERE FaultBasketId = @@IDENTITY


/*
Select the record from temptable 
*/
DECLARE @FAULTLOGID INT
DECLARE @TempFaultID INT
DECLARE @FaultId INT
DECLARE @Quantity INT
DECLARE @ProblemDays INT
DECLARE @RecuringProblem INT
DECLARE @CommunalProblem INT
DECLARE @Notes nvarchar(500)
DECLARE @JSNumberPrefix VARCHAR(10)
DECLARE @DUEDATE varchar(30)
DECLARE @RESPONSETIME INT
DECLARE @DAYS bit
DECLARE @IsSelected bit
DECLARE @TenancyId INT
DECLARE @PropertyId nvarchar(20)
DECLARE @ItemId INT 
DECLARE @ItemNatureId INT
DECLARE @RepairReportedStatusId INT
DECLARE @JOURNALID INT
 

SET @JSNumberPrefix  = 'JS'
SET @IsSelected = 0

--Get &  Set Tenancy Id 
SELECT @TenancyId= C_CUSTOMERTENANCY.TENANCYID , @PropertyId = C_TENANCY.PROPERTYID
FROM C_CUSTOMERTENANCY
INNER JOIN C_TENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
WHERE CustomerId = @CustomerId 


--Get and set the Item id 
SET @ItemId = (SELECT  ItemID FROM C_ITEM WHERE DESCRIPTION='Property')

--Get and set the Item nature id 
SET @ItemNatureId = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION='Reactive Repair')

--Get and set the repair reported status
SET @RepairReportedStatusId = (SELECT  FaultStatusID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Repair Reported')

/*
Declare the cursor
*/
DECLARE c1 CURSOR READ_ONLY

FOR

SELECT FaultId, Quantity, ProblemDays, RecuringProblem, CommunalProblem, Notes FROM FL_TEMP_FAULT WHERE CustomerID = @CustomerId  

OPEN c1

/*
Start Looping
*/

FETCH NEXT FROM c1
INTO @FaultId, @Quantity, @ProblemDays, @RecuringProblem, @CommunalProblem, @Notes

WHILE @@FETCH_STATUS = 0
BEGIN

SELECT @RESPONSETIME= RESPONSETIME, @DAYS=DAYS FROM FL_FAULT_PRIORITY INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PRIORITYID=FL_FAULT.PRIORITYID WHERE FAULTID=@FaultId
if @DAYS=0
set @DUEDATE=DateAdd(hh,@RESPONSETIME,@SubmitDate)
ELSE
set @DUEDATE=DateAdd(d,@RESPONSETIME,@SubmitDate)

--Insert the record in fault log table
INSERT INTO FL_FAULT_LOG
(FaultBasketID, FaultID, CustomerID, SubmitDate, StatusID, IsReported, Quantity, ProblemDays, RecuringProblem, CommunalProblem, Notes, DueDate, IsSelected)
VALUES
(@FaultBasketId, @FaultId, @CustomerId, @SubmitDate, @StatusId, @IsReported, @Quantity, @ProblemDays, @RecuringProblem, @CommunalProblem, @Notes, @DUEDATE, 0)
	
-- get latest fault log id
SELECT @FAULTLOGID=FaultLogId
FROM FL_FAULT_LOG
WHERE FaultLogId = @@IDENTITY

-- update the job sheet number
UPDATE FL_FAULT_LOG 
SET JobSheetNumber = (@JSNumberPrefix + CONVERT(VARCHAR,@@IDENTITY)) 
WHERE FaultLogId = @@IDENTITY 


--First Insertion into Journal Table
INSERT INTO FL_FAULT_JOURNAL
(FAULTLOGID, CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, FaultStatusID, LETTERACTION, CREATIONDATE, TITLE, LASTACTIONDATE, NEXTACTIONDATE)
VALUES
(@FAULTLOGID, @CustomerId, @TenancyId, @PropertyId, @ItemId, @ItemNatureId, @RepairReportedStatusId, null, @SubmitDate, null, GETDATE(), null)


--Get latest Journal id
SELECT @JOURNALID=JournalID 
FROM FL_FAULT_JOURNAL 
WHERE JournalID = @@IDENTITY


--Insertion Fault Log History With Repair Reported Status
INSERT INTO FL_FAULT_LOG_HISTORY
(JournalID, FaultStatusID, ItemActionID, LastActionDate, LastActionUserID, FaultLogID, ORGID, ScopeID, Title, Notes)
VALUES
(@JOURNALID, @RepairReportedStatusId, NULL, GETDATE(), NULL, @FAULTLOGID, NULL, NULL, NULL, @Notes)	
		
		
	FETCH NEXT FROM c1
	INTO @FaultId, @Quantity, @ProblemDays, @RecuringProblem, @CommunalProblem, @Notes

END

CLOSE c1
DEALLOCATE c1 

/* delete the fault from temporary table*/
DELETE FROM FL_TEMP_FAULT WHERE CustomerId = @CustomerId	   

IF @FaultBasketId<0
	BEGIN
		SET @FaultBasketId=-1
		ROLLBACK TRANSACTION
		RETURN 
	END
	
SET @FaultBasketId = 1
COMMIT TRANSACTION	
	













GO
