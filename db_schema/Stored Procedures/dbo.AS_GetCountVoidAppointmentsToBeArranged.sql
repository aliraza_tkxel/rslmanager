USE [RSLBHALIVE]
GO 
/****** OBJECT:  STOREDPROCEDURE [DBO].[AS_GetCountVoidAppointmentsToBeArranged]    SCRIPT DATE: 01/15/2016 11:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
EXEC	[dbo].[AS_GetCountVoidAppointmentsToBeArranged]
		@FuelType = N'',
		@PatchId = -1,
		@DevelopmentId = -1

-- ============================================= */


IF OBJECT_ID('DBO.AS_GetCountVoidAppointmentsToBeArranged') IS NULL -- CHECK IF SP EXISTS
 EXEC('CREATE PROCEDURE DBO.AS_GetCountVoidAppointmentsToBeArranged AS SET NOCOUNT ON;') -- CREATE DUMMY/EMPTY SP
GO


ALTER PROCEDURE [DBO].[AS_GetCountVoidAppointmentsToBeArranged]
( 
		@FuelType varchar(25) = '',	
		@PatchId int = -1,
		@DevelopmentId	int = -1
		
)
AS
BEGIN
		--THESE PARAMETERS WILL BE USED FOR SEARCH
		DECLARE @SEARCHTEXT VARCHAR(5000),		
		
		--PARAMETERS WHICH WOULD HELP IN SORTING AND PAGING
		@PAGESIZE INT = 30,
		@PAGENUMBER INT = 1,
		@SORTCOLUMN VARCHAR(50) = 'JOURNALID',
		@SORTORDER VARCHAR (5) = 'DESC'
		
		
		DECLARE @SELECTCLAUSE VARCHAR(MAX),
        @FROMCLAUSE   VARCHAR(MAX) = ' ',
        @WHERECLAUSE  VARCHAR(1000) = ' ' ,	        
        @ORDERCLAUSE  VARCHAR(100) = ' ',	
        @MAINSELECTQUERY VARCHAR(8000) = ' ',        
        @ROWNUMBERQUERY VARCHAR(8000) = ' ',
        @FINALQUERY VARCHAR(8000),
        -- USED TO ADD IN CONDITIONS IN WHERECLAUSE BASED ON SEARCH CRITERIA PROVIDED
        @SEARCHCRITERIA VARCHAR(1000) = ' ',
        
        --VARIABLES FOR PAGING
        @OFFSET INT,
		@LIMIT INT
		
		--PAGING FORMULA
		SET @OFFSET = 1+(@PAGENUMBER-1) * @PAGESIZE
		SET @LIMIT = (@OFFSET + @PAGESIZE)-1
        		
		--========================================================================================
		-- BEGIN BUILDING SEARCHCRITERIA CLAUSE
		-- THESE CONDITIONS WILL BE ADDED INTO WHERE CLAUSE BASED ON SEARCH CRITERIA PROVIDED
		
		SET @SEARCHCRITERIA = ' 1=1 '
		
		IF(@FuelType <> '' AND @FuelType IS NOT NULL AND @FuelType <> 'Select Type')
		BEGIN
			IF(@FuelType = 'Gas')
			BEGIN
				SET @FuelType = 'Mains Gas'
			END
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ATTR.PARAMETERVALUE = ''' + @FuelType	+ ''''+CHAR(10)
		END
		
		IF (@PatchId > 0 )	--NOT EQUALS TO 0 OR 1		
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ( PATCH.PATCHID = ' + CONVERT(VARCHAR(10),@PatchId) + ' ) '
		END
		
		IF (@DevelopmentId > 0)
		BEGIN
			SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND S.SCHEMEID = ' + CONVERT(VARCHAR(10),@DevelopmentId) + CHAR(10)
		END
				
		--THESE CONDITIONS WÍLL BE USED IN EVERY CASE
		
		SET @SEARCHCRITERIA = @SEARCHCRITERIA + CHAR(10) + ' AND ( ( P.STATUS = 1 AND (P.SUBSTATUS <> 21 OR P.SUBSTATUS IS NULL) ) OR 
		(P.Status=2 AND P.SUBSTATUS = 22 ) )
		AND P.PROPERTYTYPE NOT IN (SELECT PROPERTYTYPEID FROM P_PROPERTYTYPE WHERE DESCRIPTION IN (''Garage'',''Car Port'',''Car Space''))
		AND (DATEADD(YEAR,1,CP12.ISSUEDATE) BETWEEN T.TerminationDate AND CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103) 
		OR DATEADD(YEAR,1,CP12.ISSUEDATE) > CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103))
		) AS TAB
		'
	
		-- END BUILDING SEARCHCRITERIA CLAUSE   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- BEGIN BUILDING SELECT CLAUSE
		-- INSERT STATEMENTS FOR PROCEDURE HERE
		
		SET @SELECTCLAUSE = 'SELECT COUNT( DISTINCT TAB.Ref) FROM ( 
							SELECT DISTINCT
							P.PROPERTYID As Ref,
							ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address, 
							P.HOUSENUMBER AS HOUSENUMBER,
							P.ADDRESS1 AS ADDRESS1,
							P.ADDRESS2 AS ADDRESS2,
							P.ADDRESS3 AS ADDRESS3,
							P.TOWNCITY, 
							P.COUNTY,
							P.POSTCODE,
							ISNULL(S.SCHEMENAME,''-'') as Scheme,
							ISNULL(B.BLOCKNAME,''-'') AS Block,
							--DEVELOPMENT.DEVELOPMENTNAME AS DEVELOPMENT,
							ISNULL(CONVERT(nvarchar(50),T.TERMINATIONDATE,103),''-'') as Termination,
							T.TerminationDate as TerminationDate,
							CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103) as Relet,
							CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103) as ReletSort,
							ISNULL(ST.DESCRIPTION,''-'') AS Status,
							ISNULL(SUB.DESCRIPTION,''-'') as SubStatus,
							CONVERT(VARCHAR,DATEADD(YEAR,1,CP12.ISSUEDATE),103)AS EXPIRYDATE,
							DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,CP12.ISSUEDATE)) AS DAYS,
							/*CASE 
								WHEN ISNULL(PV.VALUEDETAIL, ''-'') = ''PLEASE SELECT'' THEN 
									''-''
								ELSE 
									ISNULL(PV.VALUEDETAIL, ''-'') 
							END AS FUEL,*/
							P.PROPERTYID,
							J.TENANCYID,
							AS_JOURNAL.JOURNALID,
							--AS_STATUS.TITLE AS STATUSTITLE,
							ISNULL(ST.DESCRIPTION, ''N/A'') AS PROPERTYSTATUS,
							PATCH.PATCHID AS PATCHID,
							PATCH.LOCATION AS PATCHNAME,
							ISNULL(A.TEL,''N/A'')  as TELEPHONE,
							C.FIRSTNAME  + '' '' + C.LASTNAME AS NAME,
							/*CONVERT(VARCHAR,P_LGSR.ISSUEDATE,103)AS ISSUEDATE,
							P_LGSR.ISSUEDATE AS LGSRDATE,*/
							C.CUSTOMERID AS CUSTOMERID '
		
		-- END BUILDING SELECT CLAUSE
		--======================================================================================== 							

		
		--========================================================================================    
		-- BEGIN BUILDING FROM CLAUSE
		
		SET @FROMCLAUSE = CHAR(10) + ' FROM P__PROPERTY P  

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
LEFT JOIN E_PATCH PATCH ON P.PATCH = PATCH.PATCHID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
LEFT JOIN P_LGSR CP12 ON P.PROPERTYID = CP12.PROPERTYID

Cross Apply(
			Select Max(j.JOURNALID) as journalID from C_JOURNAL j 
			where j.PropertyId=P.PropertyID AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
			GROUP BY PROPERTYID
			) as CJournal
Cross APPLY (
Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination
	INNER JOIN C_JOURNAL J ON CJournal.journalID=J.JOURNALID
	INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
Outer Apply (
		SELECT P.PROPERTYID,PROP_ATTRIB.PARAMETERVALUE
	FROM P__PROPERTY P
	LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES PROP_ATTRIB ON PROP_ATTRIB.PROPERTYID = P.PROPERTYID	AND
								PROP_ATTRIB.ITEMPARAMID =
								(
									SELECT
										ItemParamID
									FROM
										PA_ITEM_PARAMETER
											INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
											INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
									WHERE
										ParameterName = ''Heating Fuel''
										AND ItemName = ''Heating''
										AND PARAMETERVALUE = ''Mains Gas''
								)
	INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
	
		) As ATTR -- ON P.PROPERTYID=T.PROPERTYID

			INNER JOIN (
				SELECT P2.PROPERTYID,J2.JOURNALID FROM P__PROPERTY P2 
					INNER JOIN AS_JOURNAL J2 ON P2.PROPERTYID = J2.PROPERTYID
				WHERE J2.ISCURRENT = 1 AND J2.STATUSID = 1
			) AS AS_JOURNAL ON AS_JOURNAL.PROPERTYID = P.PROPERTYID'
		
		-- END BUILDING FROM CLAUSE
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- BEGIN BUILDING ORDERBY CLAUSE						
		IF(@SORTCOLUMN = 'ADDRESS')
		BEGIN
			SET @SORTCOLUMN = CHAR(10)+ 'ADDRESS2, HOUSENUMBER'		
		END
				
		IF (@SORTCOLUMN = 'JOURNALID') 
			BEGIN
				SET @ORDERCLAUSE =  CHAR(10) + ' ORDER BY ' + 'AS_JOURNAL.'+@SORTCOLUMN + CHAR(10) + @SORTORDER
			END
		ELSE
			BEGIN
				SET @ORDERCLAUSE =  CHAR(10) + ' ORDER BY ' +@SORTCOLUMN + CHAR(10) + @SORTORDER
			END
		
		
		-- END BUILDING ORDERBY CLAUSE
		--========================================================================================
		
		--========================================================================================
		-- BEGIN BUILDING WHERE CLAUSE
	    			  				
		SET @WHERECLAUSE =	CHAR(10) + 'WHERE ' + CHAR(10) + @SEARCHCRITERIA 
		
		-- END BUILDING WHERE CLAUSE
		--========================================================================================
		
		--========================================================================================
		
		--========================================================================================
		-- BEGIN BUILDING THE MAIN SELECT QUERY
		
		SET @MAINSELECTQUERY = @SELECTCLAUSE +@FROMCLAUSE + @WHERECLAUSE
		PRINT @MAINSELECTQUERY
		EXEC (@MAINSELECTQUERY) 
		
		-- END BUILDING THE MAIN SELECT QUERY
		--========================================================================================	
END



