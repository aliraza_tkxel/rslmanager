
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
--EXEC FL_GetJobSheetAndCustomerDetail @jobSheetNumber ='JS81809'
--@jobSheetNumber ='JS5'
-- Author:		<Ahmed Mehmood>
-- Create date: <15/2/2013>
-- Description:	<Returns Job Sheet detail and Customer Info>
-- WebPage: ReportsArea.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetJobSheetAndCustomerDetail] 
	-- Add the parameters for the stored procedure here
	(
	@jobSheetNumber varchar(50)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Fault Log Info and Appointment Info
	Select	FL_FAULT_LOG.JobSheetNumber JSN,
			ISNULL(S_ORGANISATION.NAME,'N/A') ContractorName,
			ISNULL(E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME,'-') as OperativeName,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as Priority,
			convert(VARCHAR, FL_FAULT_LOG.DueDate,106)+' '+ convert(VARCHAR(5), FL_FAULT_LOG.DueDate,108)as Completion,
			convert(VARCHAR, FL_FAULT_LOG.SubmitDate,106)+' '+convert(VARCHAR(5), FL_FAULT_LOG.SubmitDate,108)as OrderDate,
			ISNULL(FL_AREA.AreaName,'N/A') Location,
			FL_FAULT.Description Description,
			ISNULL(FL_FAULT_LOG.Notes,'N/A') FaultNotes,
			ISNULL(convert(VARCHAR(20), FL_CO_APPOINTMENT.AppointmentDate,107),'')as AppointmentTimeDate,
			ISNULL(FL_CO_APPOINTMENT.Notes,'N/A') AppointmentNotes,
			ISNULL(FL_CO_APPOINTMENT.Time +'N/A'+datename(weekday,FL_CO_APPOINTMENT.AppointmentDate),'N/A') Time ,
			ISNULL(G_TRADE.[Description],'N/A') as Trade,
			ISNULL(FL_FAULT_FOLLOWON.FollowOnNotes, ' ') as FollowOnNotes,
			ISNULL(TblRepairNotes.Notes,'N/A') RepairNotes,
			FL_FAULT_LOG.PROPERTYID AS [PROPERTYID]
		  
		  
	From	FL_FAULT_LOG
			LEFT OUTER JOIN FL_FAULT_APPOINTMENT on  FL_FAULT_LOG.FaultLogID=FL_FAULT_APPOINTMENT.FaultLogId
			LEFT OUTER JOIN FL_CO_APPOINTMENT on FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
			LEFT OUTER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
			LEFT OUTER JOIN FL_FAULT_PRIORITY  on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
			LEFT OUTER JOIN FL_AREA on FL_AREA.AreaID = FL_FAULT.AREAID 
		    LEFT OUTER JOIN S_ORGANISATION on S_ORGANISATION.ORGID=FL_FAULT_LOG.ORGID 
		    LEFT OUTER JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID
		    LEFT OUTER JOIN FL_FAULT_TRADE on FL_FAULT_LOG.FaultTradeId =FL_FAULT_TRADE.FaultTradeId 
			LEFT OUTER JOIN G_TRADE on G_TRADE.TradeId = FL_FAULT_TRADE.TradeId
			LEFT OUTER JOIN FL_FAULT_FOLLOWON on FL_FAULT_FOLLOWON.FaultLogID = FL_FAULT_LOG.FaultLogId
			LEFT OUTER JOIN (Select * from FL_FAULT_LOG_HISTORY Where FaultStatusID = 17 ) as TblRepairNotes on TblRepairNotes.FaultLogID = FL_FAULT_LOG.FaultLogID
	Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
	Order by TblRepairNotes.LastActionDate DESC
-- Customer Info
	
SELECT	ISNULL(C__CUSTOMER.FirstName,'') + ISNULL(' ' + C__CUSTOMER.MiddleName,'') + ISNULL(' ' + C__CUSTOMER.LastName,'') as NAME
		,ISNULL(P__PROPERTY.HouseNumber,'') + ISNULL(' ' + P__PROPERTY.ADDRESS1,'') +ISNULL(' ' + P__PROPERTY.ADDRESS2,'') + ISNULL(' ' + P__PROPERTY.ADDRESS3,'')  AS Address
		,ISNULL(P__PROPERTY.TownCity, 'Town/City N/A') as TOWNCITY
		,ISNULL(CONVERT(NVARCHAR(15),P__PROPERTY.PostCode), 'Post Code N/A') as POSTCODE     
		,ISNULL(P__PROPERTY.County, 'County N/A') as COUNTY
		,ISNULL(C_ADDRESS.Tel, 'N/A') as TEL
		,ISNULL(C_ADDRESS.MOBILE, 'N/A') as MOBILE
		,ISNULL(C_ADDRESS.Email, 'N/A') as EMAIL
		, FL_FAULT_LOG.CustomerId customerID
		, FL_FAULT_LOG.PROPERTYID propertyID
		
FROM	FL_FAULT_LOG
		LEFT OUTER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID= P__PROPERTY.PROPERTYID 
		LEFT OUTER JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID 
		LEFT OUTER JOIN C_ADDRESS ON FL_FAULT_LOG.CustomerId = 	C_ADDRESS.CUSTOMERID AND C_ADDRESS.IsDefault = 1 
		
WHERE	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber
				
END


GO
