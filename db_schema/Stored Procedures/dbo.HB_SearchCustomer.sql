USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.HB_SearchCustomer') IS NULL 
	EXEC('CREATE PROCEDURE dbo.HB_SearchCustomer AS SET NOCOUNT ON;') 
GO
      

ALTER PROCEDURE [dbo].[HB_SearchCustomer] 
(
	@firstName NVARCHAR (MAX) = ''
	,@lastName NVARCHAR (MAX) = ''
	,@address NVARCHAR (MAX) = ''
	,@postcode NVARCHAR (MAX) = ''
)
			
AS
	
	DECLARE @SelectClause NVARCHAR(MAX),
	        @FromClause   NVARCHAR(MAX),
	        @WhereClause  NVARCHAR(MAX),		                
	        @SearchCriteria NVARCHAR(MAX),
	        @MainSelectQuery NVARCHAR(MAX)



		SET  @searchCriteria = ' 1=1 AND CTY.DESCRIPTION IS NOT NULL '

		IF (@firstName <> '' AND @firstName IS NOT NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria +' AND C.FIRSTNAME LIKE ''%'+ LTRIM(@firstName) +'%'''
 		END	
		
		IF (@lastName <> '' AND @lastName IS NOT NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria +' AND C.LASTNAME LIKE ''%'+ LTRIM(@lastName) +'%'''
 		END		 	
			
		IF (@address <> '' AND @address IS NOT NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria +' AND ( 
															(P.ADDRESS1 LIKE ''%'+ LTRIM(@address) +'%'' 
															OR P.ADDRESS2 LIKE ''%'+ LTRIM(@address) +'%'' 
															OR P.ADDRESS3 LIKE ''%'+ LTRIM(@address) +'%'' 
															OR P.HOUSENUMBER + '' '' + P.ADDRESS1 LIKE  ''%'+ LTRIM(@address) +'%'')'
													+ ' OR '
													+ '		(A.ADDRESS1 LIKE ''%'+ LTRIM(@address) +'%'' 
															OR A.ADDRESS2 LIKE ''%'+ LTRIM(@address) +'%'' 
															OR A.ADDRESS3 LIKE ''%'+ LTRIM(@address) +'%'' 
															OR A.HOUSENUMBER + '' '' + A.ADDRESS1 LIKE  ''%'+ LTRIM(@address) +'%'')) '
 		END	

		IF (@postcode <> '' AND @postcode IS NOT NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria +' AND ( A.POSTCODE LIKE ''%'+ LTRIM(@postcode) +'%'' OR P.POSTCODE LIKE ''%'+ LTRIM(@postcode) +'%'' ) '
 		END	

                                                                                                                                                                                                                      
    -- Building Main Query for Property Search
                             
    SET @SelectClause = ' SELECT TOP(300)' +                      
    CHAR(10) + ' C.CustomerId 
            	, ISNULL(G.DESCRIPTION+'' '','''') + ISNULL(C.FIRSTNAME,'''') + '' '' + ISNULL(C.LASTNAME,'''') AS CustomerName  
            	, CASE WHEN DATEDIFF(DAY, CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103),CT.ENDDATE) <= 0 THEN 
								''ft'' 
								ELSE 
								''t'' 
							END as Tenant , 
				ISNULL(P.HOUSENUMBER + '' '' + P.ADDRESS1, A.HOUSENUMBER + '' '' + A.ADDRESS1) AS FullAddress
				,(	SELECT COUNT(CUSTOMERTENANCYID)  
				 	FROM C_CUSTOMERTENANCY  
			    	WHERE ENDDATE IS NULL AND TENANCYID = T.TENANCYID) AS TenancyCount 
				,C.CustomerType  
				,CTY.DESCRIPTION CustomerTypeDesc
				,ISNULL(CT.TenancyId,0)  TenancyId'                     
                           
    SET @FromClause = '	FROM C__CUSTOMER C  
				LEFT JOIN C_CUSTOMERTENANCY CT ON C.CUSTOMERID = CT.CUSTOMERID  
				LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID  
				LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID  
				LEFT JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1  
				LEFT JOIN G_TITLE G ON C.TITLE = G.TITLEID
				LEFT JOIN C_CUSTOMERTYPE CTY ON C.CUSTOMERTYPE =  CTY.CUSTOMERTYPEID  '	                      
      
    SET @WhereClause =  ' WHERE ' + @searchCriteria  
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	    












