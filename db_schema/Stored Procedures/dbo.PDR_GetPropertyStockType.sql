USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyStockType]    Script Date: 20/03/2019 5:52:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property StockType for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property StockType for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyStockType
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetPropertyStockType') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyStockType AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
  
ALTER PROCEDURE [dbo].[PDR_GetPropertyStockType]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT STOCKTYPE, DESCRIPTION FROM P_STOCKTYPE order by DESCRIPTION ASC
	
END
