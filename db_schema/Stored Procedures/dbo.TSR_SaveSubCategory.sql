SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC TSR_SaveSubCategory @journalId = 236,@subCategoryId = 1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/20/2014>
-- Description:	<Description,,Save the Subcategory against the referral data>
-- WebPage: CustomerDetail.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_SaveSubCategory]
(
@journalId int,
@subCategoryId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 INSERT INTO C_REFERRAL_CUSTOMER_HELPSUBCATEGORY
 (JOURNALID,SUBCATEGORYID) Values
 (@journalId,@subCategoryId)
END
GO
