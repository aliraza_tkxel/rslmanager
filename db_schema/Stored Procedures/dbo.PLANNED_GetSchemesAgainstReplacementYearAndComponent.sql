USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetSchemesAgainstReplacementYearAndComponent]    Script Date: 02/19/2016 10:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PLANNED_GetSchemesAgainstReplacementYearAndComponent') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_GetSchemesAgainstReplacementYearAndComponent AS SET NOCOUNT ON;') 
GO


-- Stored Procedure  
  
-- =============================================  
--EXEC [PLANNED_GetSchemesAgainstReplacementYearAndComponent]  
--  @replacementYear = -1,  
--  @componentId = -1  
-- Author:  <Noor Muhammad>  
-- Create date: <13/1/2014>  
-- Description: <Returns Schemes Against Selected Year And Component>  
-- Webpage: ReplacementList.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[PLANNED_GetSchemesAgainstReplacementYearAndComponent]   
  
 @replacementYear int = -1,  
 @componentId int = -1  
AS  
BEGIN  
  
 DECLARE  
 @selectClause varchar(8000),  
 @fromClause   varchar(8000),  
 @whereClause  varchar(8000),           
 @mainSelectQuery varchar(8000),          
 @searchCriteria varchar(8000)= ''  
 ,@YearStartDate DateTime  
 ,@YearEndDate DateTime           
  --Fiscal Year Formula  
  DECLARE @StartDate DATETIME  
  DECLARE @EndDate DATETIME    
  DECLARE @SCompleteDate nvarchar(20)  
    IF NOT (@componentId = -1 OR @componentId = 0)  
  
  BEGIN  
  
        SET @searchCriteria = @searchCriteria + CHAR(10) +'AND PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId)    
  END  
 IF NOT (@replacementYear = -1 OR @replacementYear = 0)             
  BEGIN  
    
  /*  --Ticket 9785 - Use financial years on scheme list to match listed data from PLANNED_GetReplacementList  
  SET @SCompleteDate = convert(VARCHAR(10),@replacementYear)+RIGHT('0'+ convert(VARCHAR(10),DATEPART(mm,getdate())),2)+RIGHT('0'+ convert(VARCHAR(10),DATEPART(dd,getdate())),2)  
  
  SET @StartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, Convert(DATETIME,@SCompleteDate))) - 4)%12), Convert(DATETIME,@SCompleteDate) ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, Convert(DATETIME,@SCompleteDate))) - 4)%12),Co
nvert(DATETIME,@SCompleteDate) ))+1 ) )  
  SET @EndDate = DATEADD(SS,-1,DATEADD(mm,12,@StartDate))  
  */  
  
  SELECT  
   @StartDate = CONVERT(DATETIME, convert(varchar(8),@replacementYear) + '0401')  
   ,@EndDate = CONVERT(DATETIME, CONVERT(NVARCHAR, @replacementYear + 1) + '0331 23:59:59')   
   --Ticket # 10000 - Generate List in Planned > Lifecycle > Replacement List.
   
  --SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND CONVERT( DateTime, CONVERT( CHAR, PA_PROPERTY_ITEM_DATES.DueDate, 103 ), 103 ) BETWEEN    
  --      CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @StartDate, 103) ) ) + ''', 103 )  AND   
  --      CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @EndDate, 103) ) ) + ''', 103 ) ' 
  -- Problem was in above @StartDate & @EndDate 
    SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND CONVERT( DateTime, CONVERT( CHAR, PA_PROPERTY_ITEM_DATES.DueDate, 103 ), 103 ) BETWEEN    
        CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @replacementYear, 103) ) ) + ''', 103 )  AND   
        CONVERT( DATETIME, ''' + Ltrim( RTrim( CONVERT(Char, @replacementYear+1, 103) ) ) + ''', 103 ) '  
    
   --SET @searchCriteria = @searchCriteria + CHAR(10) +'AND DueDate = '+ ''''+CONVERT(VARCHAR(10),@replacementYear)+'-01-04 00:00:00.000'''         
  END  
  
  
 SET @selectClause = 'SELECT   
 Distinct P_SCHEME.SCHEMEID as DevelopmentId  
 ,P_SCHEME.SCHEMENAME as SchemeName'                                                
  
 SET @fromClause = CHAR(10) + 'FROM   
 PA_PROPERTY_ITEM_DATES  
 Inner JOIN P__PROPERTY ON PA_PROPERTY_ITEM_DATES.PROPERTYID = P__PROPERTY.PROPERTYID   
 INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID   
 INNER JOIN PLANNED_COMPONENT ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PLANNED_COMPONENT.COMPONENTID'  
  
    SET @whereClause =  CHAR(10)+ 'WHERE 1=1 ' + CHAR(10) + @searchCriteria   
  
 Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + CHAR(10)+ 'ORDER BY SchemeName ASC'  
 print @selectClause  
 print @fromClause  
 print @whereClause   
 EXEC (@mainSelectQuery)  
  
  
END  
  