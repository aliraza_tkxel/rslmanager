SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[TO_Customer_GetAccountBalance]
	/*	===============================================================
	'   NAME:           TO_Customer_GetAccountBalance
	'   DATE CREATED:   26 MAY 2008
	'   CREATED BY:     Naveed Iqbal
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To retrieve the Custome balancer information to be displayed on welcome page of the application
	'   IN:             @CustomerTenancyId
	'   OUT:                 No    
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@TenancyId INT
	)
AS

SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AccountBalance
FROM F_RENTJOURNAL J 
LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID
WHERE J.TENANCYID =  @TenancyId  AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))

GO
