
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetReplacementOverdueCount] 

/* ===========================================================================
 '   NAME:           PLANNED_GetReplacementOverdueCount
--EXEC	[dbo].[PLANNED_GetReplacementOverdueCount]	-1
-- Author:		<Ahmed Mehmood>
-- Create date: <10/09/2013>
-- Description:	<Get Replacement Overdue Count>
-- Web Page: Dashboard.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		

		@componentId int = -1

	)
		
AS
	
	DECLARE
	 @SelectClause varchar(8000),
	 @FromClause   varchar(8000),
	 @WhereClause  varchar(8000),	        
	 @mainSelectQuery varchar(5500),        
	 @SearchCriteria varchar(8000)

         
    SET @SearchCriteria = 'YEAR(PA_PROPERTY_ITEM_DATES.DueDate) < YEAR(GETDATE()) AND'
        
    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND'  
    
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + ' P__PROPERTY.STATUS not in (Select STATUSID from P_STATUS Where P_STATUS.DESCRIPTION = ''Sold'' or P_STATUS.DESCRIPTION = ''Demolished'' or P_STATUS.DESCRIPTION = ''Transfer'' or P_STATUS.DESCRIPTION = ''Other Losses'') AND'
	SET @SelectClause = 'SELECT	COUNT(*) as TotalCount '                                            		
	SET @FromClause =	  CHAR(10) +'FROM (	SELECT	PID.SID AS SID
				,PID.PROPERTYID AS PROPERTYID
				,PID.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
				,PID.DueDate as DueDate
				,PID.LastDone AS LastDone
		FROM	PA_PROPERTY_ITEM_DATES AS PID
				INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID.PLANNED_COMPONENTID = PCI.COMPONENTID
								) AS PA_PROPERTY_ITEM_DATES 
	INNER JOIN P__PROPERTY ON PA_PROPERTY_ITEM_DATES.PROPERTYID = P__PROPERTY.PROPERTYID
	INNER JOIN P_DEVELOPMENT ON  P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID 
	INNER JOIN PLANNED_COMPONENT ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
	LEFT OUTER JOIN (	SELECT PROPERTYID as PropertyId,MAX(SurveyDate ) as LastSurveyDate
						FROM PS_SURVEY
						GROUP BY PROPERTYID ) RecentSurvey on PA_PROPERTY_ITEM_DATES.PROPERTYID = RecentSurvey.PropertyId 					
	LEFT OUTER JOIN PLANNED_JOURNAL ON PA_PROPERTY_ITEM_DATES.PROPERTYID = PLANNED_JOURNAL.PROPERTYID  AND PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = PLANNED_JOURNAL.COMPONENTID
	LEFT OUTER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId
	LEFT OUTER JOIN PLANNED_INSPECTION_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_INSPECTION_APPOINTMENTS.JournalId
	LEFT OUTER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID'
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 )'        
	Set @mainSelectQuery = @SelectClause +@FromClause + @WhereClause 
	print (@mainSelectQuery)
	EXEC (@mainSelectQuery)
		
		















GO
