USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetAllDOCUMENTS]    Script Date: 24-Jun-16 10:17:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get Documents for Documents Grid on New Development Page
 
    Author: Salman nazir
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014    Salman Nazir        Get Documents Data
    v1.1		 30/11/2015		Raja Aneeq			Add two new field named DocumentDate,UploadedDate
    
    Execution Command:
    
    Exec PDR_GetAllDOCUMENTS 
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_GetAllDOCUMENTS]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select convert(varchar(10),UploadedDate, 103) As UploadedDate,Category,CreatedBy,DocumentID, ISNULL(Title,'-') as Title, convert(varchar(10),Expires, 103) as Expire,
	 convert(varchar(10),DocumentDate, 103) As DocumentDate , ISNULL(FileType,'-') as FileType
	From PDR_Documents 
END
