SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[E_SSIS_BANKHOLIDAY_ADJ_VARIANCE]
	
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @oBHadj FLOAT, @oEntHours FLOAT, @oEntDays FLOAT, @oProRataHours FLOAT, 
		@oProRataDays FLOAT, @oProRataSameHours FLOAT, @empIdRow INT

DECLARE @employees TABLE (
	rowId INT IDENTITY, 
	empId INT, 
	jobId INT, 
	firstname VARCHAR(50), 
	lastname VARCHAR(100), 
	bankholiday FLOAT, 
	bh FLOAT,
	hoursperweek VARCHAR(30),
	partfulltime VARCHAR(30),
	daysorhours VARCHAR(30)
	)
	
INSERT INTO @employees
        ( empId ,
		  jobId, 
          firstname ,
          lastname ,
          bankholiday ,
          bh,
          hoursperweek,
		  partfulltime,
		  daysorhours
        )
SELECT  e.EMPLOYEEID ,
		jd.JOBDETAILSID,
        e.FIRSTNAME ,
        e.LASTNAME ,
        jd.BANKHOLIDAY,
        0,
		ISNULL(CAST([Hours] AS VARCHAR), ''),
        ISNULL(pft.[DESCRIPTION], ''),
        ISNULL(hi.Indicator, '')
FROM dbo.E__EMPLOYEE e
INNER JOIN dbo.E_JOBDETAILS jd ON (e.EMPLOYEEID = jd.EMPLOYEEID)
INNER JOIN dbo.AC_LOGINS ac ON (ac.EMPLOYEEID = e.EMPLOYEEID)
LEFT JOIN dbo.E_PARTFULLTIME pft ON jd.PARTFULLTIME = pft.PARTFULLTIMEID
LEFT JOIN dbo.E_HOLIDAYENTITLEMENT_INDICATOR hi ON hi.Sid = jd.HolidayIndicator
WHERE jd.ACTIVE = 1 AND ac.ACTIVE = 1 AND JobRoleId <> 101 AND JobRoleId <> 10
	  AND jd.GRADE NOT IN (38)
	  AND BANKHOLIDAY IS NOT NULL 
	  AND bankholiday IS NOT NULL 
	  AND (jd.ENDDATE IS NULL OR jd.enddate > GETDATE())
ORDER BY lastname, firstname

DECLARE @i INT = 1
DECLARE @numrows INT = (SELECT COUNT(*) FROM @employees)
DECLARE @mydump INT 

WHILE (@i <= @numrows)
BEGIN
	SET @empIdRow = (SELECT empId FROM @employees WHERE rowId = @i)
	
	EXEC dbo.E_HOLIDAYADJUSTMENTS 
	@employeeId = @empIdRow, -- int
    @fteholidayentitlement = 27, -- float
    @print = 0, 
    @BHadj = @oBHadj OUTPUT,
    @AdjHours = @oEntHours OUTPUT,
    @AdjDays = @oEntDays OUTPUT,
    @ProRataHours = @oProRataHours OUTPUT,
    @ProRataDays = @oProRataDays OUTPUT,
    @ProRataSameHours = @oProRataSameHours OUTPUT	
    
	UPDATE @employees SET bh = @oBHadj WHERE rowId = @i
	
	SET @i = @i+1
END

SELECT  firstname AS [First Name] ,
        lastname AS [Last Name],
        hoursperweek AS [Hours Per Week] ,
        partfulltime AS [Part/Full Time],
        daysorhours AS [Days or Hours],
        CAST(ROUND(bankholiday, 2) AS VARCHAR) AS [Manual Adjustment] ,
        CAST(ROUND(bh, 2) AS VARCHAR) AS [Auto Calculation] 
FROM    @employees
WHERE   bankholiday <> bh
ORDER BY lastname ,
        firstname

END

GO
