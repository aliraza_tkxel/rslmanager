SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE dbo.FL_GET_ELEMENTS
/* ===========================================================================
 '   NAME:           FL_GET_ELEMENTS
 '   DATE CREATED:   14th Oct 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the elements 
 '   IN:             @AreaID
 '
 '   OUT:            @LocationName
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
	@AreaID INT	
	)	
	
AS 	

SELECT * 
FROM FL_ELEMENT
WHERE areaId = @AreaID --ORDER BY ElementName ASC













GO
