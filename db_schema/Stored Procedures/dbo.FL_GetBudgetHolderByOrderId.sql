USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetBudgetHolderByOrderId]    Script Date: 04/06/2016 11:56:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.FL_GetBudgetHolderByOrderId') IS NULL 
 EXEC('CREATE PROCEDURE dbo.FL_GetBudgetHolderByOrderId AS SET NOCOUNT ON;')
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 06/11/2015
-- Description:	Assign Work to Contractor.
-- Updated 06/11/2015: get Budget Holder detail By OrderId
-- =============================================
--EXEC [FL_GetBudgetHolderByOrderId] 280358,3633
ALTER PROCEDURE [dbo].[FL_GetBudgetHolderByOrderId] 
	-- Add the parameters for the stored procedure here
	@orderId INT,
	@expenditureId INT

AS
BEGIN
SELECT PO.OrderId, emp.EMPLOYEEID,emp.OperativeName,emp.Email ,emp.MinLIMIT as limit                    
FROM F_PURCHASEORDER PO
INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS
LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID Cross APPLY
  (SELECT 
   Distinct lim.EMPLOYEEID,lim.OperativeName,lim.Email,lim.MinLIMIT 
   FROM F_PURCHASEITEM PI
   INNER JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID CROSS APPLY
     (SELECT MinLIMIT, ELInner.EMPLOYEEID,CONVERT(varchar(50),E.FIRSTNAME)+' '+ CONVERT(varchar(50),E.LASTNAME)   As OperativeName,
     
       C.WORKEMAIL  as Email
      FROM F_EMPLOYEELIMITS ELInner
      INNER JOIN E__EMPLOYEE E ON ELInner.EMPLOYEEID = E.EMPLOYEEID
      INNER JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID 
       CROSS APPLY
        (SELECT MIN(LIMIT) MinLIMIT
         FROM F_EMPLOYEELIMITS
         WHERE LIMIT > PI.GROSSCOST
         AND EXPENDITUREID = ELInner.EXPENDITUREID) MinLim
      WHERE LIMIT = MinLIMIT
      AND ELInner.EXPENDITUREID = @expenditureId) lim
      OUTER APPLY
     (SELECT 1 AS IsAbsent
      FROM E_JOURNAL J CROSS APPLY
        (SELECT MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryID
         FROM E_ABSENCE
         WHERE JOURNALID = J.JOURNALID) MA
      INNER JOIN E_ABSENCE A ON MA.MaxAbsenceHistoryID = A.ABSENCEHISTORYID
      AND J.CURRENTITEMSTATUSID IN (1, 5)
      WHERE (getdate() BETWEEN A.STARTDATE AND ISNULL(A.RETURNDATE,getdate()))
        AND J.EMPLOYEEID = lim.EMPLOYEEID) [Absent]
      
      
  WHERE PI.ACTIVE = 1
     AND (PI.PISTATUS = 0 OR PI.PISTATUS = 1)
     AND ORDERID = PO.ORDERID
      AND ((EL.LIMIT >= MinLIMIT)
          OR (IsAbsent = 1
              AND EL.LIMIT > MinLIMIT))) emp
WHERE PO.ORDERID = @orderId

END