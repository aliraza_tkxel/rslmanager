SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE
dbo.TO_RESPONSE_SEARCH

/* ===========================================================================

' NAME: TO_ENQUIRY_LOG_GetCustomersEqnuiries

' DATE CREATED: 03 JULY 2008

' CREATED BY: Naveed Iqbal

' CREATED FOR: Broadland Housing

' PURPOSE: Get the TO_ENQUIRY_LOG records against the specified customer

' to be shown in Message alert system

' IN: @CustomerID

'

' OUT: Nothing

' RETURN: Nothing

' VERSION: 1.0

' COMMENTS:

' MODIFIED ON:

' MODIFIED BY:

' REASON MODIFICATION:

'==============================================================================*/

(

@CustomerID
INT

)

AS

DECLARE
@WhereClause VARCHAR(8000)

DECLARE
@SelectClause VARCHAR(8000)

--IF @ResponseStatus IS NOT NULL

SET
@SelectClause = 'SELECT * ' + CHAR(10) +
' FROM ( ' + CHAR(10) +
' SELECT DISTINCT A.EnquiryLogID, A.CreationDate, A.JournalID, A.ItemNatureID, ' + CHAR(10) +
' (CASE WHEN B.EnquiryLogID IS NULL THEN ''Awaiting Response'' ELSE ''Response Recieved'' END) ' + CHAR(10) +
' AS ResponseStatus, ' + CHAR(10) +
' (CASE WHEN '+ CHAR(10) +
' (SELECT TOP 1 ISNULL( EnquiryLogID, ''True'') ' + CHAR(10) +
' FROM TO_RESPONSE_LOG AS C ' + CHAR(10) +
' WHERE '+ CHAR(10) +
' (EnquiryLogID = A.EnquiryLogID) AND (ReadFlag = 0)) IS NULL ' + CHAR(10) +
' AND B.EnquiryLogID IS NULL THEN ''0'' ' + CHAR(10) +
' WHEN' + CHAR(10) +
' (SELECT TOP 1 ISNULL(EnquiryLogID, ''True'') ' + CHAR(10) +
' FROM TO_RESPONSE_LOG AS C' + CHAR(10) +
' WHERE ' + CHAR(10) +
' (EnquiryLogID = A.EnquiryLogID) AND (ReadFlag = 0)) IS NULL ' + CHAR(10) +
' AND B.EnquiryLogID IS NOT NULL THEN ''1'' ELSE ''2'' END) ' + CHAR(10) +
' AS ImageKeyFlag ' + CHAR(10) +
' FROM ' + CHAR(10) +
' TO_ENQUIRY_LOG AS A LEFT OUTER JOIN' + CHAR(10) +
' TO_RESPONSE_LOG AS B ON A.EnquiryLogID = B.EnquiryLogID ' + CHAR(10) +
' WHERE ' + CHAR(10) +
' A.CustomerId = ' + CHAR(10) + Convert(varchar, @CustomerID) + CHAR(10) + ') Z '

EXEC(@SelectClause)
-- Print (@SelectClause)

GO
