SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_JOB_SHORTLIST_UPDATE]
(
	@EmpId int,
	@JobId bigint,
	@Original_ShortListId bigint,
	@ShortListId bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [I_JOB_SHORTLIST] SET [EmpId] = @EmpId, [JobId] = @JobId WHERE (([ShortListId] = @Original_ShortListId));
	
SELECT ShortListId, EmpId, JobId FROM I_JOB_SHORTLIST WHERE (ShortListId = @ShortListId)
GO
