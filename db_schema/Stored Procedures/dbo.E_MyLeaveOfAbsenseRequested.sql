USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdul Rehman
-- Create date: June 2, 2017
-- =============================================

IF OBJECT_ID('dbo.E_MyLeaveOfAbsenseRequested') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_MyLeaveOfAbsenseRequested AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[E_MyLeaveOfAbsenseRequested] 
	-- Add the parameters for the stored procedure here
	@empId INT
	
AS
BEGIN

	DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME		

	SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE 
		FROM	EMPLOYEE_ANNUAL_START_END_DATE(@empId)

		

		SELECT	e.STARTDATE AS StartDate
				,e.RETURNDATE AS ReturnDate
				,ar.DESCRIPTION as Reason
				,s.DESCRIPTION as Status
				,j.EMPLOYEEID AS EmpId
				,n.ITEMNATUREID AS ItemNatureId
				, n.DESCRIPTION AS Nature
				, Case 
					WHEN e.returndate is null THEN 
						dbo.EMP_SICKNESS_DURATION(Emp.EMPLOYEEID, e.STARTDATE, getdate(), e.JOURNALID) 
					ELSE 
						dbo.EMP_SICKNESS_DURATION(Emp.EMPLOYEEID, e.STARTDATE, e.RETURNDATE, e.JOURNALID) 
					END AS DAYS_ABS
				, e.HOLTYPE as HolType
				,emp.FIRSTNAME AS FirstName
				,emp.LASTNAME AS LastName
				, e.ABSENCEHISTORYID AS AbsenceHistoryId
				, CASE 
					WHEN Jd.HolidayIndicator=2 and Jd.PARTFULLTIME=2 
					THEN e.DURATION_HRS ELSE e.DURATION 
					END AS Duration 
		FROM	E__EMPLOYEE Emp  
				LEFT JOIN E_JOBDETAILS Jd ON Emp.EMPLOYEEID = Jd.EMPLOYEEID  
				LEFT JOIN E_JOURNAL j ON Jd.EMPLOYEEID = j.EMPLOYEEID  AND j.CURRENTITEMSTATUSID = (SELECT ITEMSTATUSID from E_STATUS WHERE DESCRIPTION = 'Pending')
				INNER JOIN E_ABSENCE e on j.JOURNALID = e.JOURNALID	AND e.ABSENCEHISTORYID = (
											SELECT	MAX(ABSENCEHISTORYID) 
											FROM	E_ABSENCE 
											WHERE	JOURNALID = j.JOURNALID
										  ) 
				LEFT JOIN E_ABSENCEREASON ar on e.REASONID = ar.SID
				INNER JOIN E_STATUS s on j.CURRENTITEMSTATUSID = s.ITEMSTATUSID
				INNER JOIN E_NATURE n on j.ITEMNATUREID = n.ITEMNATUREID
		WHERE j.EMPLOYEEID = @empId and j.ITEMNATUREID NOT IN (	SELECT	ITEMNATUREID 
																	FROM	E_NATURE 
																	WHERE	DESCRIPTION  IN ('Sickness','Annual Leave','BRS TOIL Recorded','BRS Time Off in Lieu','BHG TOIL') ) 
																			AND e.STARTDATE >= @BHSTART 
																			AND  e.STARTDATE <= @BHEND
		ORDER BY e.STARTDATE DESC

END