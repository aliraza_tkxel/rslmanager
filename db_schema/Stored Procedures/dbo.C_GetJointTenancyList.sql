
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <25/10/2016>
-- Description:	<Find list of joint tenancy against a customerId>
-- =============================================
IF OBJECT_ID('dbo.[C_GetJointTenancyList]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[C_GetJointTenancyList] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE C_GetJointTenancyList
	-- Add the parameters for the stored procedure here
	@customerId INT
AS
BEGIN
	SELECT c.CUSTOMERID  FROM C__CUSTOMER c
	INNER JOIN C_CUSTOMERTENANCY ct ON c.CUSTOMERID = ct.CUSTOMERID
	WHERE ct.TENANCYID =(SELECT ctt.TENANCYID FROM C_CUSTOMERTENANCY ctt 
	WHERE ctt.CUSTOMERID=@customerId AND ctt.ENDDATE IS NULL)



 
END
GO
