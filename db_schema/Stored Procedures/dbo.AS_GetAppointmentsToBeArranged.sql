USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetAppointmentsToBeArranged]    Script Date: 20-Jul-18 3:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Hussain Ali
-- Create date: 31/10/2012
-- Modified Date: <13 Jun 2013, Aamir Waheed>
-- Description:	This stored procedure returns the "Appointments to be arranged" properties
-- Usage: Dashboard
-- Exec [dbo].[AS_GetAppointmentsToBeArranged]
--		@PatchId = 18,
--		@DevelopmentId = 1
--
-- =============================================
IF OBJECT_ID('dbo.[AS_GetAppointmentsToBeArranged]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetAppointmentsToBeArranged] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetAppointmentsToBeArranged] 
	@PatchId		int,
	@DevelopmentId	int,
	-- parameters for sorting and paging		
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Address',
	@sortOrder varchar (5) = 'ASC',
	@totalCount int=0 output,
	@FuelType varchar (8000)
AS
BEGIN
	declare @offset int
	declare @limit int

	set @offset = 1+(@pageNumber-1) * @pageSize
	set @limit = (@offset + @pageSize)-1	

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@orderClause  varchar(100),	
			@whereClause	varchar(8000),			
			@mainSelectQuery varchar(5000),        
			@rowNumberQuery varchar(5500),
			@finalQuery varchar(6000)                 
---------------------------------------------------- FOR GAS --------------------------------------------		
if @FuelType = 'Gas'
begin		
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+') 
								P__PROPERTY.PROPERTYID,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								P__PROPERTY.HOUSENUMBER as HOUSENUMBER,
								P__PROPERTY.ADDRESS2 as ADDRESS2,
								ISNULL(AS_Status.Title, '''') as StatusTitle'
	
	SET @fromClause		= 'FROM 
								P__PROPERTY
								INNER JOIN AS_JOURNAL ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID AND AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Gas'')

								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID

								LEFT JOIN AS_STATUS ON AS_STATUS.STATUSID = AS_JOURNAL.STATUSID
								LEFT JOIN (SELECT HeatingMappingId,P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
								
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PA_Parameter_value.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.STATUSID = 1
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
end
---------------------------------------------------- FOR Oil --------------------------------------------	
if @FuelType = 'Oil'
begin 
	SET @selectClause	= 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+') 
								P__PROPERTY.PROPERTYID,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								P__PROPERTY.HOUSENUMBER as HOUSENUMBER,
								P__PROPERTY.ADDRESS2 as ADDRESS2,
								ISNULL(AS_Status.Title, '''') as StatusTitle'
	
	SET @fromClause		= 'FROM 
								P__PROPERTY
								INNER JOIN AS_JOURNAL ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID AND AS_JOURNAL.ServicingTypeId = (SELECT ServicingTypeId from P_ServicingType where Description = ''Oil'')

								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID

								LEFT JOIN AS_STATUS ON AS_STATUS.STATUSID = AS_JOURNAL.STATUSID
								LEFT JOIN (SELECT HeatingMappingId,P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
								
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PA_Parameter_value.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''oil'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.STATUSID = 1
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
end

---------------------------------------------------- FOR Alternative servicing --------------------------------------------	
if @FuelType = 'Alternative Servicing'
begin 
	SET @selectClause	= 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+') 
								P__PROPERTY.PROPERTYID,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								P__PROPERTY.HOUSENUMBER as HOUSENUMBER,
								P__PROPERTY.ADDRESS2 as ADDRESS2,
								ISNULL(AS_Status.Title, '''') as StatusTitle'
	
	SET @fromClause		= 'FROM 
								P__PROPERTY
								INNER JOIN AS_JOURNAL ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Alternative Servicing'' )

								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID

								LEFT JOIN AS_STATUS ON AS_STATUS.STATUSID = AS_JOURNAL.STATUSID
								LEFT JOIN (SELECT HeatingMappingId,P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
								
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PA_Parameter_value.IsAlternativeHeating =1										
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.STATUSID = 1						
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 0
										AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56'
end

	IF (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
		
	IF (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	IF (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	IF (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
	SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
	END		


	SET @orderClause =  CHAR(10) + ' ORDER BY ' + @sortColumn + CHAR(10) + @sortOrder
		
	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause

	-- End building the main select Query
	--========================================================================================			
	
	
	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() OVER (ORDER BY '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(VARCHAR(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(VARCHAR(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	PRINT(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	Declare @selectCount nvarchar(2000), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount = 'SELECT @totalCount = COUNT(DISTINCT P__PROPERTY.PROPERTYID)' + @fromClause + @whereClause
	
	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		
	-- End building the Count Query
	--========================================================================================	 	
 
END



