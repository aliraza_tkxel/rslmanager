USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_INSERT_NL_JOURNALENTRY]    Script Date: 11/23/2016 20:43:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[F_INSERT_NL_JOURNALENTRY]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[F_INSERT_NL_JOURNALENTRY] AS SET NOCOUNT ON;')

GO
ALTER  proc [dbo].[F_INSERT_NL_JOURNALENTRY](  
 @TRANSACTIONTYPE int,   
 @TRANSACTIONID int,   
 @TIMEMODIFIED datetime ,   
 @TXNDATE smalldatetime =GETDATE,   
 @REFNUMBER varchar(50),   
 @EXCHANGERATE float,
 @schemeId int =Null,
 @blockId int =Null,
 @propertyId nvarchar(20)=Null   ,
 @companyid int = null
)  
  
as  
begin  
 begin tran  
	DECLARE @count int= (select count(*) from NL_JOURNALENTRY where REFNUMBER is not null AND TRANSACTIONTYPE=13 )
	IF (@REFNUMBER is NULL OR @REFNUMBER = '')
	BEGIN
		SET @REFNUMBER  = 'GJ-' +  CAST((select TOP(1) case when @COUNT > 0 THEN CAST(substring((Select TOP(1) REFNUMBER from NL_JOURNALENTRY where REFNUMBER is not null AND TRANSACTIONTYPE=13 ORDER BY TIMECREATED Desc),4,LEN((Select TOP(1) REFNUMBER from NL_JOURNALENTRY where REFNUMBER is not null AND TRANSACTIONTYPE=13 ORDER BY TIMECREATED Desc))-3) AS int)+1 ELSE 900 END) AS varchar)
	END
	
	INSERT INTO NL_JOURNALENTRY  
	( TRANSACTIONTYPE, TRANSACTIONID, TIMECREATED, TIMEMODIFIED, TXNDATE, REFNUMBER, EXCHANGERATE,SchemeId,BlockId,PropertyId, companyid)  
	VALUES( @TRANSACTIONTYPE, @TRANSACTIONID, getdate(), @TIMEMODIFIED, convert(varchar(10),@TXNDATE,103), @REFNUMBER, @EXCHANGERATE,@schemeId,@blockId,@propertyId, @companyid)  
    
	if @@error <> 0   
	begin   
	rollback tran  
     
	-- RAISERROR ('Error while inserting in to NL_JOURNALENTRY', 17, 1)  
	return -1
	end  
    
 commit  
 return SCOPE_IDENTITY()  
end  