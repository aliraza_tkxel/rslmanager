-- Stored Procedure

-- =============================================
--EXEC	[dbo].[FL_GetPausedFaultList]
--		@searchedText = N'Laundry',
--		@operativeId = 145,
--		@pageSize = 2,
--		@pageNumber = 1,
--		@sortColumn = N'Recorded',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT		
-- Author:		<Ahmed Mehmood>
-- Create date: <7/15/2013>
-- Description:	<Get list of Paused Faults>
-- Web Page: ReportsArea.aspx
-- =============================================

IF OBJECT_ID('dbo.FL_GetPausedFaultList') IS NULL
 EXEC('CREATE PROCEDURE dbo.FL_GetPausedFaultList AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[FL_GetPausedFaultList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@financialYear INT,
		@searchedText VARCHAR(8000) = '',
		@operativeId int,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Recorded',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery NVARCHAR(max),        
        @rowNumberQuery NVARCHAR(max),
        @finalQuery NVARCHAR(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),

        --variables for paging
        @offset int,
		@limit int,

		@ApplianceDefectSelectClause NVARCHAR(MAX),          
		@ApplianceDefectFromClause NVARCHAR(MAX),          
		@ApplianceDefectWhereClause NVARCHAR(MAX),
		@ApplianceDefectOrderClause NVARCHAR(max),

		@PlannedSelectClause NVARCHAR(MAX),          
		@PlannedFromClause NVARCHAR(MAX),          
		@PlannedWhereClause NVARCHAR(MAX),
		@PlannedOrderClause NVARCHAR(max),

		@VoidSelectClause NVARCHAR(MAX),          
		@VoidFromClause NVARCHAR(MAX),          
		@VoidWhereClause NVARCHAR(MAX),
		@VoidOrderClause NVARCHAR(max),

		@unionClause NVARCHAR(MAX)

	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1

	--Set union Clause            
	SET @unionClause = CHAR(10) + CHAR(9) + 'UNION ALL' + CHAR(10) + CHAR(9)    
	--========================================================================================  

	--========================================================================================
	-- Begin building SearchCriteria clause
	-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteria = ' StatusID =16 '

	IF(@searchedText != '' AND @searchedText IS NOT NULL)
	BEGIN			
		--SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+'''))'
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
	END	

	IF @operativeId != -1
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND E__EMPLOYEE.EMPLOYEEID = '+CONVERT(nvarchar(50), @operativeId) 	 	
	-- Filter on the basis of BlockId and SchemeId
	IF(@schemeId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.SchemeId IS NULL 
																	THEN	P__PROPERTY.SchemeId 
																	ELSE 		
																			FL_FAULT_LOG.SchemeId 	
																	END = '+convert(varchar(10),@schemeId)
		END
	IF(@blockId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.BlockId IS NULL 
																	THEN P__PROPERTY.BLOCKID 
																	ELSE 		
																		FL_FAULT_LOG.BlockId 	
																	END = '+convert(varchar(10),@blockId)
		END	
	IF (@financialYear != -1 AND LEN(@financialYear) = 4)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND FL_FAULT_LOG.SubmitDate BETWEEN ''' + CONVERT(VARCHAR,@financialYear) + '0401'' AND ''' + CONVERT(VARCHAR,@financialYear+1) + '0331 23:59:59.997'''
		END
	-- End building SearchCriteria clause   
	--========================================================================================

	SET NOCOUNT ON;
	--========================================================================================	        
	-- Begin building SELECT clause
	-- Insert statements for procedure here

	SET @selectClause = 'SELECT FL_FAULT_LOG.JobSheetNumber as JSN,
	CONVERT(nvarchar(10),FL_FAULT_LOG.SubmitDate, 103) as Recorded,
	ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
	ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '', '' + ISNULL(P__PROPERTY.TOWNCITY,'''') as Address,
	FL_AREA.AreaName Location, FL_FAULT.Description Description, TimeSheetTable.StartTime Start,CONVERT(nvarchar(50),TimeSheetTable.StartTime, 103)+'' ''+CONVERT(VARCHAR(5), TimeSheetTable.StartTime, 108) started, 
	LEFT(E__EMPLOYEE.Firstname, 1)+'' ''+LEFT(E__EMPLOYEE.LASTNAME, 1) as UserInitials,CONVERT(nvarchar(50),FL_FAULT_PAUSED.PausedOn, 103)+'' ''+CONVERT(VARCHAR(5), FL_FAULT_PAUSED.PausedOn, 108) PausedOn,FL_FAULT_PAUSED.Reason Reason,FL_FAULT_LOG.FaultLogID FaultLogID ,FL_FAULT_PAUSED.Notes Notes,FL_FAULT_LOG.SubmitDate Submitted
	,P__PROPERTY.HOUSENUMBER HouseNum,P__PROPERTY.ADDRESS1 PrimaryAddress,FL_FAULT_PAUSED.PausedOn Paused'

	-- End building SELECT clause
	--======================================================================================== 							


	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause =	  CHAR(10) +'FROM (SELECT MAX(FL_FAULT_PAUSED.PauseID) PID,FL_FAULT_PAUSED.FaultLogId,MAX(FL_FAULT_PAUSED.PausedOn) PausedOn
	FROM FL_FAULT_PAUSED
	GROUP BY FL_FAULT_PAUSED.FaultLogId) AS FAULT_PAUSED
	INNER JOIN FL_FAULT_PAUSED on FAULT_PAUSED.PID = FL_FAULT_PAUSED.PauseID 
	INNER JOIN FL_FAULT_LOG on FAULT_PAUSED.FaultLogId = FL_FAULT_LOG.FaultLogID
	INNER JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
	INNER JOIN FL_AREA on FL_FAULT_LOG.AREAID = FL_AREA.AreaID
	INNER JOIN (SELECT max(FL_FAULT_LOG_HISTORY.FaultStatusID) TimeSheetID,FL_FAULT_LOG_HISTORY.FaultLogId, MAX(FL_FAULT_LOG_HISTORY.LastActionDate) StartTime
				FROM FL_FAULT_LOG_HISTORY 
				WHERE FL_FAULT_LOG_HISTORY.FaultStatusID = (SELECT TOP 1 FLS.FaultStatusID FROM FL_FAULT_STATUS FLS WHERE FLS.Description LIKE ''In Progress'')
				group by FL_FAULT_LOG_HISTORY.FaultLogId ) TimeSheetTable on FL_FAULT_PAUSED.FaultLogId = TimeSheetTable.FaultLogId	
	INNER JOIN E__EMPLOYEE on FL_FAULT_PAUSED.PausedBy = E__EMPLOYEE.EMPLOYEEID 
	LEFT JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
	LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
	Case WHEN FL_FAULT_LOG.SchemeId is NULL THEN P__PROPERTY.SchemeId 
	ELSE 		FL_FAULT_LOG.SchemeId 	END
	LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID	=
	Case WHEN FL_FAULT_LOG.BlockId is NULL THEN P__PROPERTY.BLOCKID 
	ELSE 		FL_FAULT_LOG.BlockId 	END		'
	-- End building From clause
	--======================================================================================== 														  



	--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'HouseNum,PrimaryAddress' 		
	END

	IF(@sortColumn = 'Recorded')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Submitted' 		
	END

	IF(@sortColumn = 'JSN')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
	END

	IF(@sortColumn = 'PausedOn')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Paused' 		
	END	

	IF(@sortColumn = 'Scheme')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Scheme' 		
	END	

	IF(@sortColumn = 'Block')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Block' 		
	END	
	IF(@sortColumn = 'Started')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Start' 		
	END	



	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

	-- End building OrderBy clause
	--========================================================================================								

	--========================================================================================
	-- Begin building WHERE clause

	-- This Where clause contains subquery to exclude already displayed records			  

	SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

	-- End building WHERE clause
	--========================================================================================


	-- PRINT '--===================================================================='  
    PRINT @selectClause + @fromClause + @whereClause + @orderClause 

	--===========================================================================================            
	--       APPLIANCES DEFECT ACTIVITIES            
	--===========================================================================================       
   
       
	SET @ApplianceDefectSelectClause = ' SELECT
		ISNULL(''JSD'' + RIGHT(''00000''+ CONVERT(nVARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId),5),''N/A'') AS JSN                   
		,Convert(varchar(10),P_PROPERTY_APPLIANCE_DEFECTS.DefectDate,103) as Recorded  
		,ISNULL(SC.SCHEMENAME,''-'') as Scheme,ISNULL(BL.BLOCKNAME,''-'') AS Block
		,ISNULL(PR.HOUSENUMBER,'''') + '' '' + ISNULL(PR.ADDRESS1,'''') + '', '' + ISNULL(PR.TOWNCITY,'''') as Address
		, ''-'' as Location
		, P_PROPERTY_APPLIANCE_DEFECTS.DefectNotes Description
		, TimeSheetTable.StartTime Start
		,CONVERT(nvarchar(50),TimeSheetTable.StartTime, 103)+'' ''+CONVERT(VARCHAR(5), TimeSheetTable.StartTime, 108) started 
		,LEFT(E__EMPLOYEE.Firstname, 1)+'' ''+LEFT(E__EMPLOYEE.LASTNAME, 1)  AS UserInitials    
		,CONVERT(nvarchar(50),COALESCE(PP.PauseOn,LOGGEDDATE), 103)+'' ''+CONVERT(VARCHAR(5), COALESCE(PP.PauseOn,LOGGEDDATE), 108) PausedOn
		,ISNULL(FR.Reason,'''') AS  Reason 
		, P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId as FaultLogID 
		,ISNULL(PP.PauseNote,'''') AS  Notes 
		,P_PROPERTY_APPLIANCE_DEFECTS.DefectDate Submitted
		,PR.HOUSENUMBER HouseNum
		,PR.ADDRESS1 PrimaryAddress
		,COALESCE(PP.PauseOn,LOGGEDDATE) Paused
	  ' + CHAR(10)    
    
	SET @ApplianceDefectFromClause = CHAR(10)+'
		FROM (SELECT MAX(P_PROPERTY_APPLIANCE_DEFECTS_PAUSE.PauseID) PID,P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PropertyDefectId
			,MAX(P_PROPERTY_APPLIANCE_DEFECTS_PAUSE.PauseOn) PausedOn
			FROM P_PROPERTY_APPLIANCE_DEFECTS_PAUSE
			inner join P_PROPERTY_APPLIANCE_DEFECTS_HISTORY on P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectHistoryId = P_PROPERTY_APPLIANCE_DEFECTS_PAUSE.DefectHistoryId
			GROUP BY P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PropertyDefectId) AS FAULT_PAUSED
		INNER JOIN P_PROPERTY_APPLIANCE_DEFECTS_PAUSE PP on FAULT_PAUSED.PID = PP.PauseID 
		INNER JOIN P_PROPERTY_APPLIANCE_DEFECTS on FAULT_PAUSED.PropertyDefectId = P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId
		INNER JOIN PDR_STATUS ON P_PROPERTY_APPLIANCE_DEFECTS.DefectJobSheetStatus = PDR_STATUS.STATUSID
		inner JOIN FL_PAUSED_REASON FR on PP.PauseReasonId = FR.PauseId
		LEFT JOIN PDR_APPOINTMENTS ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_APPOINTMENTS.JOURNALID
		INNER JOIN (SELECT max(P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectJobSheetStatus) TimeSheetID,P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PropertyDefectId, MAX(P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.ModifiedDate) StartTime
					FROM P_PROPERTY_APPLIANCE_DEFECTS_HISTORY 
					WHERE P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.DefectJobSheetStatus = (SELECT TOP 1 FLS.STATUSID FROM PDR_STATUS FLS WHERE FLS.TITLE LIKE ''Paused'')
					group by P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.PropertyDefectId ) TimeSheetTable on FAULT_PAUSED.PropertyDefectId = TimeSheetTable.PropertyDefectId	
		INNER JOIN E__EMPLOYEE on PP.PauseBy = E__EMPLOYEE.EMPLOYEEID 
		LEFT JOIN P__PROPERTY PR on P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID = PR.PROPERTYID
		LEFT JOIN P_SCHEME SC on SC.SCHEMEID = PR.SchemeId 
		LEFT JOIN P_BLOCK BL on  BL.BLOCKID	 =  PR.BLOCKID 
	'+ CHAR(10)    
    
	SET @ApplianceDefectWhereClause = ' WHERE PDR_STATUS.STATUSID = 8 
		AND (
			 ((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 )) 
			 OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 )) ' + CHAR(10)    
                    
    
	
   PRINT @ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause
    PRINT '--========================= end ==========================================='

	--===========================================================================================            
	--       PLANNED MAINTENANCE ACTIVITIES                       
	--===========================================================================================       
   
       
	SET @PlannedSelectClause = ' 
		SELECT
		ISNULL(( ''JSN'' + Convert( NVarchar, PA.APPOINTMENTID )),''N/A'' ) AS JSN  
		,Convert(varchar(10),PJ.CREATIONDATE ,103) as Recorded          
		,ISNULL(SC.SCHEMENAME,''-'') as Scheme,ISNULL(BL.BLOCKNAME,''-'') AS Block
		,ISNULL(PR.HOUSENUMBER,'''') + '' '' + ISNULL(PR.ADDRESS1,'''') + '', '' + ISNULL(PR.TOWNCITY,'''') as Address
		,ISNULL(PC.COMPONENTNAME,''-'') Location 
		,ISNULL(PA.APPOINTMENTNOTES,'''') AS  Description
		,TimeSheetTable.StartTime Start
		,CONVERT(nvarchar(50),TimeSheetTable.StartTime, 103)+'' ''+CONVERT(VARCHAR(5), TimeSheetTable.StartTime, 108) started 
		,LEFT(OP.FIRSTNAME, 1)+'' ''+LEFT(OP.lastname, 1) as UserInitials
		,CONVERT(nvarchar(50), PP.PausedOn, 103)+'' ''+CONVERT(VARCHAR(5), PP.PausedOn, 108) PausedOn
		,ISNULL(PP.Reason,'''') AS  Reason 
		,PA.APPOINTMENTID as FaultLogID  
		,ISNULL(PP.Notes,'''') AS  Notes  
		,PJ.CREATIONDATE AS Submitted 
		,PR.HOUSENUMBER HouseNum 
		,PR.ADDRESS1 PrimaryAddress
		,PA.LOGGEDDATE Paused 
	  ' + CHAR(10)    
    
	SET @PlannedFromClause = CHAR(10)+'
		FROM (SELECT MAX(PLANNED_PAUSED.PauseID) PID,PLANNED_PAUSED.appointmentID,MAX(PLANNED_PAUSED.PausedOn) PausedOn
		FROM PLANNED_PAUSED
		GROUP BY PLANNED_PAUSED.appointmentID) AS FAULT_PAUSED

		INNER JOIN PLANNED_PAUSED PP on FAULT_PAUSED.PID = PP.PauseID 
		INNER JOIN PLANNED_APPOINTMENTS PA on PP.appointmentID = PA.appointmentID 
		Inner JOIN PLANNED_JOURNAL PJ on PA.JournalId = PJ.JournalId
		LEFT JOIN PLANNED_COMPONENT PC on PJ.COMPONENTID = PC.COMPONENTID
		INNER JOIN (SELECT max(PLANNED_APPOINTMENTS_HISTORY.JOURNALSUBSTATUS) TimeSheetID,PLANNED_APPOINTMENTS_HISTORY.appointmentID, MAX(PLANNED_APPOINTMENTS_HISTORY.LOGGEDDATE) StartTime
					FROM PLANNED_APPOINTMENTS_HISTORY 
					WHERE PLANNED_APPOINTMENTS_HISTORY.JOURNALSUBSTATUS = (SELECT TOP 1 FLS.SUBSTATUSID FROM PLANNED_SUBSTATUS FLS WHERE FLS.TITLE LIKE ''Paused'')
					group by PLANNED_APPOINTMENTS_HISTORY.appointmentID ) TimeSheetTable on PA.appointmentID = TimeSheetTable.appointmentID	
		INNER JOIN E__EMPLOYEE OP on PP.PausedBy = OP.EMPLOYEEID 
		LEFT JOIN P__PROPERTY PR on PJ.PROPERTYID = PR.PROPERTYID
		LEFT JOIN P_SCHEME SC on SC.SCHEMEID = 
		Case WHEN PJ.SchemeId is NULL THEN PR.SchemeId 
		ELSE 		PJ.SchemeId 	END
		LEFT JOIN P_BLOCK BL on  BL.BLOCKID	=
		Case WHEN PJ.BlockId is NULL THEN PR.BLOCKID 
		ELSE 		PJ.BlockId 	END	
	'+ CHAR(10)    
    
	SET @PlannedWhereClause = ' WHERE JOURNALSUBSTATUS = 2 and APPOINTMENTSTATUS = ''InProgress'' ' + CHAR(10)    
                    
    
	
   PRINT @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause
    PRINT '--========================= end ==========================================='

	--===========================================================================================            
	--       VOID ACTIVITIES                       
	--===========================================================================================       
   
       
	SET @VoidSelectClause = ' 
		SELECT
		ISNULL(''JSD'' + RIGHT(''00000''+ CONVERT(nVARCHAR,V_RequiredWorks.RequiredWorksId),5),''N/A'') AS JSN                   
		,Convert(varchar(10),V_RequiredWorks.CreatedDate,103) as Recorded  
		,ISNULL(SC.SCHEMENAME,''-'') as Scheme,ISNULL(BL.BLOCKNAME,''-'') AS Block
		,ISNULL(PR.HOUSENUMBER,'''') + '' '' + ISNULL(PR.ADDRESS1,'''') + '', '' + ISNULL(PR.TOWNCITY,'''') as Address
		, ''-'' as Location
		, V_RequiredWorks.WorkDescription Description
		, TimeSheetTable.StartTime Start
		,CONVERT(nvarchar(50),TimeSheetTable.StartTime, 103)+'' ''+CONVERT(VARCHAR(5), TimeSheetTable.StartTime, 108) started 
		,LEFT(E__EMPLOYEE.Firstname, 1)+'' ''+LEFT(E__EMPLOYEE.LASTNAME, 1)  AS UserInitials    
		,CONVERT(nvarchar(50),ISNULL(PP.PausedOn,''''), 103)+'' ''+CONVERT(VARCHAR(5), ISNULL(PP.PausedOn,''''), 108) PausedOn
		,ISNULL(PP.Reason,'''') AS  Reason 
		, V_RequiredWorks.RequiredWorksId as FaultLogID 
		,ISNULL(PP.Notes,'''') AS  Notes 
		,V_RequiredWorks.CreatedDate Submitted
		,PR.HOUSENUMBER HouseNum
		,PR.ADDRESS1 PrimaryAddress
		,ISNULL(PP.PausedOn,'''') Paused
	  ' + CHAR(10)    
    
	SET @VoidFromClause = CHAR(10)+'
		FROM (SELECT MAX(V_PauseWorks.PauseWorksId) PID,V_PauseWorks.RequiredWorksId
			,MAX(V_PauseWorks.PausedOn) PausedOn
			FROM V_PauseWorks
			GROUP BY V_PauseWorks.RequiredWorksId) AS FAULT_PAUSED
		INNER JOIN V_PauseWorks PP on FAULT_PAUSED.PID = PP.PauseWorksId 
		INNER JOIN V_RequiredWorks on FAULT_PAUSED.RequiredWorksId = V_RequiredWorks.RequiredWorksId
		INNER JOIN PDR_STATUS ON V_RequiredWorks.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN (SELECT max(V_RequiredWorksHISTORY.STATUSID) TimeSheetID,V_RequiredWorksHISTORY.RequiredWorksId, MAX(V_RequiredWorksHISTORY.ModifiedDate) StartTime
					FROM V_RequiredWorksHISTORY 
					WHERE V_RequiredWorksHISTORY.STATUSID = (SELECT TOP 1 FLS.STATUSID FROM PDR_STATUS FLS WHERE FLS.TITLE LIKE ''Paused'')
					group by V_RequiredWorksHISTORY.RequiredWorksId ) TimeSheetTable on FAULT_PAUSED.RequiredWorksId = TimeSheetTable.RequiredWorksId	
		INNER JOIN E__EMPLOYEE on PP.PausedBy = E__EMPLOYEE.EMPLOYEEID 
		inner join PDR_JOURNAL PJ on V_RequiredWorks.WorksJournalId = PJ.JournalId
		inner join PDR_MSAT PM on PJ.MSATID = PM.MSATID

		LEFT JOIN P__PROPERTY PR on PM.PROPERTYID = PR.PROPERTYID
		LEFT JOIN P_SCHEME SC on SC.SCHEMEID = 
		Case WHEN PM.SchemeId is NULL THEN PR.SchemeId 
		ELSE 		PM.SchemeId 	END
		LEFT JOIN P_BLOCK BL on  BL.BLOCKID	=
		Case WHEN PM.BlockId is NULL THEN PR.BLOCKID 
		ELSE 		PM.BlockId 	END	
	'+ CHAR(10)    
    
	SET @VoidWhereClause = ' WHERE PDR_STATUS.STATUSID = 8 ' + CHAR(10)    
                    
    
	
   PRINT @VoidSelectClause + @VoidFromClause + @VoidWhereClause
    PRINT '--========================= end ==========================================='


	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery =	@selectClause +@fromClause + @whereClause 
							+@unionClause
							+@ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause
							+@unionClause
							+@PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause
							+@unionClause
							+@VoidSelectClause + @VoidFromClause + @VoidWhereClause
							--+ @orderClause 
	-- End building the main select Query
	--========================================================================================																																			
	PRINT '--==================@mainSelectQuery start=================================================='
   PRINT @mainSelectQuery
   PRINT '--==================@mainSelectQuery end=================================================='
	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================
	PRINT '--==================@rowNumberQuery start=================================================='
	PRINT @rowNumberQuery
	PRINT '--==================rowNumberQuery end=================================================='
	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	--print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	Declare @selectCount nvarchar(2000), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT @totalCount =  count(FL_FAULT_LOG.FaultLogID) ' + @fromClause + @whereClause

	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

	-- End building the Count Query
	--========================================================================================							
END