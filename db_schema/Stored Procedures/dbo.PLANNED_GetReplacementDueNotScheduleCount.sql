
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetReplacementDueNotScheduleCount] 

/* ===========================================================================
 '   NAME:           PLANNED_GetReplacementDueNotScheduleCount
--EXEC	[dbo].[PLANNED_GetReplacementDueNotScheduleCount] 1	
-- Author:		<Ahmed Mehmood>
-- Create date: <10/08/2013>
-- Description:	<Get Replacement Due NOT Scheduled Count>
-- Web Page: Dashboard.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		
		@componentId int = -1

	)
		
AS
	
	DECLARE
	 @SelectClause varchar(8000),
	 @FromClause   varchar(8000),
	 @WhereClause  varchar(8000),	        
	 @mainSelectQuery varchar(5500),        
	 @SearchCriteria varchar(8000)

         
    SET @SearchCriteria = ''
        
    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND '  
    

	SET @SelectClause = 'SELECT	COUNT( DISTINCT JOURNALID) as TotalCount '                                            		
	SET @FromClause = CHAR(10) +'		
		FROM 
		PLANNED_JOURNAL	
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN (SELECT	P__PROPERTY.PROPERTYID as PID,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
					FROM	P__PROPERTY)AS PROPERTYADDRESS ON P__PROPERTY.PROPERTYID = PROPERTYADDRESS.PID
		INNER JOIN P_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = P_DEVELOPMENT.DEVELOPMENTID
		INNER JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
		INNER JOIN 
		 (	SELECT	PID.SID AS SID
					,PID.PROPERTYID AS PROPERTYID
					,PID.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
					,PID.DueDate  as DueDate
					,PID.LastDone AS LastDone
			FROM	PA_PROPERTY_ITEM_DATES AS PID
					INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID.PLANNED_COMPONENTID = PCI.COMPONENTID
									) AS PA_PROPERTY_ITEM_DATES ON PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID =  PLANNED_JOURNAL.COMPONENTID 
		AND PA_PROPERTY_ITEM_DATES.PROPERTYID   = PLANNED_JOURNAL.PROPERTYID
		INNER JOIN 
		(SELECT	PLANNED_COMPONENT.COMPONENTID AS COMPONENTID , TRADES  = 
		STUFF((SELECT '', '' + G_TRADE.Description 
           FROM PLANNED_COMPONENT_TRADE COMP_TRADE_B 
           INNER JOIN G_TRADE ON COMP_TRADE_B.TRADEID = G_TRADE.TradeId 
           WHERE COMP_TRADE_B.COMPONENTID  = COMP_TRADE_A.COMPONENTID
           ORDER BY COMP_TRADE_B.SORDER ASC
          FOR XML PATH('''')), 1, 2, '''')
          ,SUM (DURATION ) AS DURATION
			FROM	PLANNED_COMPONENT 
		LEFT JOIN PLANNED_COMPONENT_TRADE AS COMP_TRADE_A ON PLANNED_COMPONENT.COMPONENTID = COMP_TRADE_A.COMPONENTID 
		GROUP BY PLANNED_COMPONENT.COMPONENTID,COMP_TRADE_A.COMPONENTID)
		 as ComponentStat ON PLANNED_JOURNAL.COMPONENTID = ComponentStat.COMPONENTID
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID '
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE PLANNED_STATUS.TITLE=''To be Arranged'' AND ' + 
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 '        
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
	print (@mainSelectQuery)
	EXEC (@mainSelectQuery)
GO
