SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================      
-- Author:  <Author,Ali Raza>      
-- Create date: <Create Date,10/13/2014>      
-- Description: <Description,Get the attribute summary detail>      
--EXEC [dbo].[AS_getAttributeSummaryDetail]          
--  @PropertyId = N'BHA0000727'      
-- ============================================= 
CREATE PROCEDURE [dbo].[AS_getAttributeSummaryDetail] 
	 @propertyId nvarchar(20) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 Select PA_LOCATION.LocationName,PA_AREA.AreaName,PA_ITEM.ItemName, PA_ITEM.ItemId,PA_AREA.AreaID ,PA_LOCATION.LocationID FROM PA_ITEM
  INNER JOIN PA_AREA ON PA_ITEM.AreaID=PA_AREA.AreaID
  INNER JOIN PA_LOCATION ON PA_AREA.LocationId=PA_LOCATION.LocationID
  INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
  where PA_ITEM.ItemID IN (
  Select PA_ITEM.ItemID from PA_PARAMETER
  INNER JOIN PA_ITEM_PARAMETER ON PA_PARAMETER.ParameterID=PA_ITEM_PARAMETER.ParameterId
  INNER JOIN PA_ITEM on PA_ITEM_PARAMETER.ItemId= PA_ITEM.ItemID
  where PA_PARAMETER.ShowInApp=0   
  )
  GROUP By  PA_LOCATION.LocationName,PA_AREA.AreaName,PA_ITEM.ItemName, PA_ITEM.ItemId,PA_AREA.AreaID ,PA_LOCATION.LocationID
  ORDER by PA_LOCATION.LocationID
  
  
  ---- Get Preinserted Values 
   SELECT PROPERTYID,PA_PARAMETER.ParameterName,PA_ITEM_PARAMETER.ItemId ,ISNULL(PARAMETERVALUE,'0') AS ParameterValue ,PA_PARAMETER.ParameterSorder
  FROM PA_PARAMETER 
  INNER JOIN PA_ITEM_PARAMETER ON PA_PARAMETER.ParameterID = PA_ITEM_PARAMETER.ParameterId
  INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
  LEFT JOIN PA_PROPERTY_ATTRIBUTES ON PA_PROPERTY_ATTRIBUTES.ITEMPARAMID = PA_ITEM_PARAMETER.ItemParamID And PROPERTYID =  @propertyId  
  WHERE PA_PARAMETER.ShowInApp=0 And PA_PROPERTY_ATTRIBUTES. PARAMETERVALUE <> '0'
  ORDER BY PA_PARAMETER.ParameterSorder ASC
  
  SELECT Top 1 
 PA_PROPERTY_ITEM_IMAGES.ImagePath,'../../../PropertyImages/'+@propertyID+'/Images/'+PA_PROPERTY_ITEM_IMAGES.ImageName as ImageName  
 FROM    
 P__PROPERTY  
 Left Join PA_PROPERTY_ITEM_IMAGES on P__PROPERTY.PropertyPicId =PA_PROPERTY_ITEM_IMAGES.[SID]   
 WHERE    
 P__Property.PROPERTYID=@propertyID 
  
END
GO
