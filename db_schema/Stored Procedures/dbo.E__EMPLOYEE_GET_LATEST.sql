SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[E__EMPLOYEE_GET_LATEST]
@LATEST_EMPID INT OUTPUT
AS
	SET NOCOUNT ON;
SELECT     TOP 1 @LATEST_EMPID = EMPLOYEEID
FROM VW_EMPLOYEES_INTRANET
ORDER BY STARTDATE DESC

SET @LATEST_EMPID = ISNULL(@LATEST_EMPID,0)



GO
