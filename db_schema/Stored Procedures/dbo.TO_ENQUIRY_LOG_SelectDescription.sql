SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[TO_ENQUIRY_LOG_SelectDescription]

/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_SelectDescription
 '   DATE CREATED:   19 JUNE 2008
 '   CREATED BY:     Munawar Nadeem
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To select description(notes) of a particular enquiry
 '   IN:             @enqLogID
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
		@enqLogID int 
	)
	
	AS
	
	
	SELECT  
			TO_ENQUIRY_LOG.Description
         
                        
	FROM
			TO_ENQUIRY_LOG 
			
	WHERE
	
	     (TO_ENQUIRY_LOG.EnquiryLogID = @enqLogID)	     
	    
	     

GO
