SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROC [dbo].[FL_REPORTEDFAULT_REPORT_EXPORT1] 

/* ===========================================================================
 '   NAME:           [FL_REPORTEDFAULT_REPORT]
 '   DATE CREATED:   03 Nov 2009
 '   CREATED BY:     Adnan Mirza
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To shortlist reported faults based on search criteria provided. This report is for Finance
 '   IN:            @locationId, @areaId, @elementId, @priorityId, @status,
                     @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	
	(
	    -- These Parameters are passed as Search Criteria
	    @JS			int	=  NULL,
		@lastName	varchar(50)	=  NULL,

		@nature		int = NULL,
		@FAULTSTATUS		VARCHAR(40) = NULL,    
		
		@text		varchar(4000)    = NULL,
		
		@date		nvarchar(200)	=  NULL,
		@dateTo		nvarchar(200)	=  NULL,
	

	
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID ',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	
	SELECT firstname FROM dbo.E__EMPLOYEE


    



GO
