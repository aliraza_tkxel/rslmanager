SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[C_CUSTOMER_AUDIT_GETLASTUPDATED]
	@CustomerId INT
AS 

DECLARE @LastActionType INT 

SET @LastActionType = (SELECT LastActionType FROM dbo.C__CUSTOMER WHERE CUSTOMERID = @CustomerId)

IF (@LastActionType = 2)
BEGIN 
	SELECT  ( cc.FIRSTNAME + ' ' + cc.LASTNAME ) AS UpdatedBy ,
			Convert(varchar(10),c.LastActionTime,103)+' '+ CONVERT(varchar(5),c.LastActionTime,108) AS UpdatedOn
	FROM    dbo.C__CUSTOMER c
			LEFT JOIN dbo.C__CUSTOMER cc ON c.LastActionUser = cc.CUSTOMERID
	WHERE   c.CUSTOMERID = @CustomerId
END
ELSE
BEGIN
	SELECT  ( e.FIRSTNAME + ' ' + e.LASTNAME ) AS UpdatedBy ,
			Convert(varchar(10),c.LastActionTime,103)+' '+ CONVERT(varchar(5),c.LastActionTime,108) AS UpdatedOn
	FROM    dbo.C__CUSTOMER c
			LEFT JOIN dbo.E__EMPLOYEE e ON c.LastActionUser = e.EMPLOYEEID
	WHERE   c.CUSTOMERID = @CustomerId
END	

GO
