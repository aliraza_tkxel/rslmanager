
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/07/2014>
-- Description:	<Description,,Get the pages for Menu selected on master page.>
-- Web Page: JobRoleAccess.Master
-- =============================================
CREATE PROCEDURE [dbo].[JRA_MasterPageMenusPages](
@employeeId int,
@menuId int
)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT
	AC_PAGES.DESCRIPTION AS PageName,
	AC_MODULES.DIRECTORY + '/' + AC_PAGES.PAGE AS PageNamePath,
	AC_PAGES.PAGEID AS PageId,
	AC_PAGES.MENUID
FROM E__EMPLOYEE
		INNER JOIN AC_PAGES_ACCESS ON E__EMPLOYEE.JobRoleTeamId = AC_PAGES_ACCESS.JobRoleTeamId 
		INNER JOIN AC_PAGES ON AC_PAGES_ACCESS.PageId = AC_PAGES.PAGEID 
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID 
		INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID
WHERE E__EMPLOYEE.EMPLOYEEID = @employeeId
AND AC_PAGES.ACTIVE = 1
AND AC_PAGES.MENUID = @menuId
AND DisplayInTree = 1

ORDER BY AC_PAGES.ORDERTEXT, AC_PAGES.DESCRIPTION

END
GO
