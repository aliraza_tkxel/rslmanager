USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_TrainingApprovalMyStaff]    Script Date: 13/06/2018 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_TrainingApprovalDirector') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_TrainingApprovalDirector AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Saud Ahmed
-- Create date:      03/10/2018
-- Description:      Calculate alert count of Training Approval HR (submitted Training that cost is > 1000 and professional Qual. and require Approval from HR.) on whiteboard  
-- exec  E_TrainingApprovalMyStaff 423
-- =============================================

ALTER PROCEDURE  [dbo].[E_TrainingApprovalDirector]
	@LineMgr INT 
AS
BEGIN
	
	SET NOCOUNT ON;
declare @diretorateid int = 0

select @diretorateid = T.DIRECTORATEID
from E__EMPLOYEE E 
    left join E_JOBDETAILS J on E.EMPLOYEEID = J.EMPLOYEEID 
    left join E_TEAM T on J.TEAM = T.TEAMID
    left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
where
    E.EMPLOYEEID = @LineMgr
                        

SELECT PendingInterest as LeavesCount From (
	SELECT count(*) AS PendingInterest
	from E_EmployeeTrainings EET 
		join E_EmployeeTrainingStatus ES on EET.Status = ES.StatusId
        join E__EMPLOYEE E on EET.EmployeeId = E.EMPLOYEEID
        join E_JOBDETAILS J on E.EMPLOYEEID = J.EMPLOYEEID
        left join E_TEAM T on J.TEAM = T.TEAMID 
        left join E_DIRECTORATES D on T.DIRECTORATEID = D.DIRECTORATEID
	where 
	(
		((EET.ProfessionalQualification = 1 or EET.TotalCost > 1000) and (EET.Status = 6 OR ES.Title = 'Requested')) 
		or
        ((EET.ProfessionalQualification <= 0 or EET.TotalCost <= 1000) and (EET.Status = 6 OR ES.Title = 'Manager Supported'))
	) 
	--(ES.Title = 'Supported' OR ES.Title = 'Requested')
	--	and (EET.ProfessionalQualification = 1 OR EET.TotalCost > 1000) 
		and D.DIRECTORATEID = @diretorateid
		--and T.DIRECTOR = @LineMgr
		and EET.Active = 1
		and J.ACTIVE = 1
)  totalResult



END
