USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDocumentTypes]    Script Date: 03/21/2016 16:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC PDR_GetDocumentTypes 1
-- =============================================

-- Author				Create date			Description						

--Fakhar uz zaman		21st March 2016			Get All Document Types (for Devs, Schemes, Props)
			 

-- Modified By:       Modification Date  

-- Fakhar uz zaman		21/3/2016	

-- Modified By: Saud Ahmed
-- Modification Date: 27/3/2017	
-- Description: remove 'EPC' type from filter -- AND Title !='EPC'


-- =============================================

IF OBJECT_ID('dbo.PDR_GetDocumentTypes') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetDocumentTypes AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[PDR_GetDocumentTypes]
	-- Add the parameters for the stored procedure here
    @ShowActiveOnly BIT = 1,
	@reportFor varchar (8000) = null,
	@categoryId int = null
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
		Declare @reportType varchar (8000)
		Declare @selectClause varchar (MAX)
		Declare @whereClause varchar (MAX)
		Declare @orderClause varchar (MAX)

		if @reportFor = 'Development'
			begin
				set @reportType = 'dt.IsDevelopment'
			end
		if @reportFor = 'Property'
			begin
				set @reportType = 'dt.IsProperty'
			end
		if @reportFor = 'Scheme'
			begin
				set @reportType = 'dt.IsScheme'
			end
   
        IF @ShowActiveOnly = 1 and @reportFor is not null and @categoryId is not null
			begin
			set @selectClause  ='SELECT  -1 AS DocumentTypeId ,
						''Please select'' AS Title ,
						1 AS [Active] ,
						0 AS [Order]
				UNION
				SELECT  dt.DocumentTypeId ,
						dt.Title ,
						dt.Active ,
						dt.[Order]
				FROM    dbo.P_Documents_Type dt
						Inner join P_Category on dt.categoryid = P_Category.categorytypeid'+char(10)
						set @whereClause = ' WHERE 1=1'
				set @whereClause = @whereClause + ' and  dt.active = 1 and '+@reportType+' = 1 and dt.categoryId = '+Convert(Varchar,@categoryId)+char(10)
					
				set @orderClause = ' ORDER BY [Order] ,
						Title'	
			end
			ELSE
				BEGIN
					set @selectClause  ='SELECT  -1 AS DocumentTypeId ,
						''Please select'' AS Title ,
						1 AS [Active] ,
						0 AS [Order]
				UNION
				SELECT  dt.DocumentTypeId ,
						dt.Title ,
						dt.Active ,
						dt.[Order]
				FROM    dbo.P_Documents_Type dt
						Inner join P_Category on dt.categoryid = P_Category.categorytypeid'+char(10)
						set @whereClause = ' WHERE 1=1'
				set @whereClause = @whereClause + ' and  dt.active = 1'
					
				set @orderClause = ' ORDER BY [Order] ,
						Title'	
				END
		Declare @mainQuery varchar (MAX)
		set @mainQuery = @selectClause +@whereClause+@orderClause
		Print(@mainQuery)
		exec(@mainQuery)

    END