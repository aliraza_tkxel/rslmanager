USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetRejectionReasons]    Script Date: 30-Nov-17 3:02:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.DF_GetRejectionReasons') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetRejectionReasons AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[DF_GetRejectionReasons]
AS
BEGIN
SET NOCOUNT ON;
	
	SELECT [ReasonId] AS ReasonId, [Description] AS Reason FROM DF_RejectionReasons

END