USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertySubStatus]    Script Date: 21/03/2019 11:54:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property SubStatus by statusId for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property SubStatus by statusId for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertySubStatus 4
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetPropertySubStatus') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertySubStatus AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
  
ALTER PROCEDURE [dbo].[PDR_GetPropertySubStatus]
(@StatusId int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.SUBSTATUSID,S.DESCRIPTION FROM P_SUBSTATUS S WHERE S.STATUSID=@StatusId ORDER BY S.DESCRIPTION ASC
	
END
