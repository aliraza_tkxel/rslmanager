SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[TO_G_DISABILITY_GetLookup]
/* 
===============================================================
'   NAME:           TO_G_DISABILITY_GetLookup
'   DATE CREATED:   07/05/2013
'   CREATED BY:     Nataliya Alexander
'   CREATED FOR:    Broadland Housing
'   PURPOSE:        To retrieve a list of all DISABILITY records
'   IN:             None 
'   OUT: 		    None   
'   RETURN:         Nothing    
'   VERSION:        1.0           
'   COMMENTS:       
'   MODIFIED ON:    
'   MODIFIED BY:    
'   REASON MODIFICATION: 
'===============================================================
*/
AS

SELECT
	 [DISABILITYID] AS id
	,[DESCRIPTION] AS val
FROM 
	[G_DISABILITY]
ORDER BY 
	[DESCRIPTION] ASC



GO
