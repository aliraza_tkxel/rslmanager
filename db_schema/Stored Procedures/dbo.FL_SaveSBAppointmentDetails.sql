USE [RSLBHALive]
GO
  
-- =====================================================  
-- EXEC [dbo].[FL_SaveSBAppointmentDetails]  
--  @faultsList = N'283,5109',  
--  @operativeId = 480,  
--  @appointmentDate = N'2/4/2013',  
--  @duration = 3.0,  
--  @faultNotes = N'The tenant thinks that she has overtightened the hot tap',  
--  @appointmentNotes = N'The tenant thinks that she has overtightened the hot tap',  
--  @time = N'9:30 AM',  
--  @endTime = N'10:30 AM',  
--      @BlockId = 11137,  
--      @SchemeId = N'A720040007'  
--  @isRecall = 0,  
--  @appointmentID = @appointmentID OUTPUT,  
--  @tempCount = @tempCount OUTPUT  
-- Author:  <Muhammad Abdul Wahhab>  
-- Create date: <1/2/2013>  
-- Description: <Saves all appointment information>  
-- Web Page: AppointmentSummary.aspx  
-- =====================================================  

IF OBJECT_ID('dbo.FL_SaveSBAppointmentDetails') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_SaveSBAppointmentDetails AS SET NOCOUNT ON;') 
GO


Alter PROCEDURE [dbo].[FL_SaveSBAppointmentDetails]  
(   
 @faultsList nvarchar(500),  
 @operativeId int,  
 @appointmentDate datetime,  
 @duration float,  
 @faultNotes varchar(500),  
 @appointmentNotes varchar(500),  
 @time varchar(50),  
 @endTime varchar(50),  
 @BlockId int,  
 @SchemeId nvarchar(20),   
 @isRecall BIT,  
 @isCalendarAppointment BIT,  
 @userID INT,   
 @appointmentID int OUTPUT,  
 @tempCount int OUTPUT  
)  
AS  
BEGIN  
Declare @SelectStmt as nvarchar(3000)  
Declare @Sql as nvarchar(3200)  
  
Declare @FaultId as int  
Declare @Quantity as int  
Declare @ProblemDays as int  
Declare @RecuringProblem as bit  
Declare @CommunalProblem as bit  
Declare @Notes as varchar(4000)  
Declare @ORGID as int  
Declare @ItemActionId as int  
Declare @Recharge as bit  
Declare @StatusID as int  
Declare @IsReported as bit  
Declare @DueDate as DateTime  
Declare @SubmitDate as DateTime  
Declare @FaultLogID as int  
Declare @JournalID as int  
Declare @TempFaultID as int  
Declare @areaId as int  
--Declare @appointmentID as int  
Declare @JSNumberPrefix as varchar(10)  
Declare @ResponseTime as int  
Declare @Days as int  
Declare @ItemID as int  
Declare @ItemNatureID as int  
Declare @TenancyID as int  
Declare @FaultTradeId as int  
Declare @OriginalFaultLogId As Int  
Declare @isFollowon As BIT  
Declare @FollowOnFaultLogID As Int  
  
  
SET @JSNumberPrefix  = 'JS'  
SET @SubmitDate = GETDATE()  
SET @IsReported = 1  
   
SELECT @StatusID = FaultStatusID  
FROM FL_FAULT_STATUS  
WHERE Description = 'Appointment Arranged'  
  
--Get &  Set Tenancy Id   
--SELECT @TenancyId = TENANCYID  
--FROM C_CUSTOMERTENANCY  
--WHERE BlockId = @BlockId AND ENDDATE != NULL  
  
--Get and set the Item id   
SET @ItemId = (SELECT  ItemID FROM C_ITEM WHERE DESCRIPTION = 'Property')  
  
--Get and set the Item nature id   
SET @ItemNatureId = (SELECT  ItemNatureID FROM C_NATURE WHERE DESCRIPTION = 'Reactive Repair')  
  
INSERT INTO FL_CO_APPOINTMENT (AppointmentDate, OperativeID, Time, EndTime, LastActionDate, AppointmentStatus,Notes,isCalendarAppointment)  
VALUES (@appointmentDate, @operativeId, @time, @endTime, @SubmitDate, 'Appointment Arranged',@appointmentNotes,@isCalendarAppointment)  
  
SET @appointmentID = @@IDENTITY  
  
INSERT INTO FL_CO_APPOINTMENT_HISTORY (AppointmentDate, AppointmentId, OperativeID, Time, EndTime, LastActionDate,isCalendarAppointment)  
VALUES (@appointmentDate, @appointmentID, @operativeId, @time, @endTime, @SubmitDate,@isCalendarAppointment)  
  
-- this is a cursor variable and is needed so we can use dynamic-sql which we need to deal with the list of TempFaultIDs  
Declare @FaultCursor CURSOR  
  
Set @SelectStmt = 'SELECT FL_TEMP_FAULT.TempFaultID as TempFaultID,  
  FL_TEMP_FAULT.FaultId as FaultId, FL_TEMP_FAULT.Quantity as Quantity,  
  FL_TEMP_FAULT.ProblemDays as ProblemDays, FL_TEMP_FAULT.RecuringProblem as RecuringProblem,  
  FL_TEMP_FAULT.CommunalProblem as CommunalProblem, FL_TEMP_FAULT.Notes as Notes,  
  FL_TEMP_FAULT.ORGID as ORGID, FL_TEMP_FAULT.ItemActionId as ItemActionId,  
  FL_TEMP_FAULT.Recharge as Recharge, FL_TEMP_FAULT.FaultTradeId as FaultTradeId,  
  FL_TEMP_FAULT.OriginalFaultLogId,   
  FL_TEMP_FAULT.isFollowon,   
  FL_TEMP_FAULT.FollowOnFaultLogID  
  ,FL_TEMP_FAULT.areaId  
  FROM FL_TEMP_FAULT   
  INNER JOIN FL_Fault ON FL_TEMP_FAULT.FaultId = FL_Fault.FaultId    
  INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID  
  WHERE FL_TEMP_FAULT.TempFaultID IN (' + @faultsList + ') And FL_TEMP_FAULT.ISRECALL =' + CONVERT(varchar(2),@isRecall)  
  
Set @Sql = 'Set @FaultCursor = CURSOR FAST_FORWARD FOR ' + @SelectStmt + '; OPEN @FaultCursor'  
  
exec sp_executesql @Sql, N'@FaultCursor CURSOR OUTPUT', @FaultCursor OUTPUT  
  
FETCH NEXT FROM @FaultCursor into @TempFaultID,@FaultId,@Quantity,@ProblemDays,  
@RecuringProblem,@CommunalProblem,@Notes,@ORGID,@ItemActionId,@Recharge,@FaultTradeId,@OriginalFaultLogId  
, @isFollowon, @FollowOnFaultLogID, @areaId  
  
WHILE @@FETCH_STATUS = 0  
BEGIN   
   
 SELECT @ResponseTime = ResponseTime, @Days = Days  
 FROM FL_FAULT_PRIORITY   
 INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PriorityID = FL_FAULT.PriorityID   
 WHERE FaultID = @FaultId  
   
 If @Days = 0  
 SET @DueDate = DateAdd(HH,@ResponseTime,@SubmitDate)  
 ELSE  
 SET @DueDate = DateAdd(DD,@ResponseTime,@SubmitDate)   
   
 --Insert the record in fault log table  
 INSERT INTO FL_FAULT_LOG  
 (FaultID, BlockId, Quantity , ProblemDays, RecuringProblem, CommunalProblem, Notes,  
 ORGID, Recharge, SchemeId, StatusID, IsReported, SubmitDate, DueDate, FaultTradeId , UserId ,FollowOnFaultLogId,areaId)  
 VALUES  
 (@FaultID, @BlockId, @Quantity , @ProblemDays, @RecuringProblem, @CommunalProblem,  
 @Notes, @ORGID, @Recharge, @SchemeId, @StatusID, @IsReported, @SubmitDate, @DueDate, @FaultTradeId, @userID,@FollowOnFaultLogID,@areaId)   
   
 SET @FaultLogID = @@IDENTITY  
   
 -- update the job sheet number  
 UPDATE FL_FAULT_LOG   
 SET JobSheetNumber = (@JSNumberPrefix + CONVERT(VARCHAR,@FaultLogID))   
 WHERE FaultLogId = @FaultLogID  
   
 --Insert the record in Fault Appointment Table  
 INSERT INTO FL_FAULT_APPOINTMENT  
 (FaultLogId, AppointmentId)  
 VALUES  
 (@FaultLogID, @appointmentID)  
   
 --Insert the record in Journal Table  
 INSERT INTO FL_FAULT_JOURNAL  
 (FaultLogID, BlockId, SchemeId, TenancyID, ItemID, ItemNatureID, FaultStatusID, CreationDate, LastActionDate)  
 VALUES  
 (@FaultLogID, @BlockId, @SchemeId, @TenancyID, @ItemID, @ItemNatureID, @StatusID, @SubmitDate, @SubmitDate)  
    
 SET @JournalID = @@IDENTITY  
   
 --Insertion Fault Log History With Repair Reported Status  
 INSERT INTO FL_FAULT_LOG_HISTORY  
 (JournalID, FaultLogID, FaultStatusID, Notes, ORGID, ItemActionID, SchemeId,BlockID, LastActionDate, LastActionUserID)  
 VALUES  
 (@JournalID, @FaultLogID, @StatusID, @Notes, @ORGID, @ItemActionId, @SchemeId,@BlockId, @SubmitDate, @userID)  
   
 UPDATE FL_TEMP_FAULT  
 SET isAppointmentConfirmed = 1, FaultLogId = @FaultLogID  
 WHERE TempFaultID = @TempFaultID   
      
    --TODO:Change the Fault Recall count value  
 If @isRecall = 1  
 BEGIN   
  INSERT INTO FL_FAULT_RECALL (FaultLogId,OriginalFaultLogId) VALUES (@FaultLogID,@OriginalFaultLogId)  
 END  
   
 --Update the record in FL_FAULT_FOLLOWON Table to set/show that the follow on is scheduled  
 IF (EXISTS (SELECT FollowOnID FROM FL_FAULT_FOLLOWON WHERE FaultLogId = @FollowOnFaultLogID ) ) AND (@isFollowon = 1 AND @FollowOnFaultLogID IS NOT NULL)  
 BEGIN  
  UPDATE FL_FAULT_FOLLOWON  
   SET isFollowonScheduled = 1  
  WHERE FaultLogId = @FollowOnFaultLogID  
 END  
   
      
    FETCH NEXT FROM @FaultCursor into @TempFaultID,@FaultId,@Quantity,@ProblemDays,  
 @RecuringProblem,@CommunalProblem,@Notes,@ORGID,@ItemActionId,@Recharge,@FaultTradeId,@OriginalFaultLogId  
 , @isFollowon, @FollowOnFaultLogID, @areaId  
END  
  
CLOSE @FaultCursor  
DEALLOCATE @FaultCursor  
  
--print 'Processing: New Appointment with id = ' + Cast(@appointmentID as varchar(20))  
  
--SET @aptId = @appointmentID  
  
SELECT @tempCount = COUNT (*)  
FROM FL_TEMP_FAULT  
WHERE FL_TEMP_FAULT.BlockId = @BlockId AND FL_TEMP_FAULT.SchemeId = @SchemeId AND FL_TEMP_FAULT.isAppointmentConfirmed IS NULL  
  
END  
  
  
  
  