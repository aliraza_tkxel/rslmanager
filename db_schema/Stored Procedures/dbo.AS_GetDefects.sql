SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetDefects
-- Author:		<Salman Nazir>
-- Create date: <06/11/2012>
-- Description:	<Get Defects information >
-- Web Page: PropertyRecord.aspx => Add Defect Tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetDefects]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM P_DEFECTS
END
GO
