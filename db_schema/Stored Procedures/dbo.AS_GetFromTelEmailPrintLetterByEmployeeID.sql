
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* =============================================
--	EXEC AS_GetFromTelEmailPrintLetterByEmployeeID
		@EmployeeID = 34
-- 	Author:		Aamir Waheed
-- 	Create date: 26 March 2013
-- 	Description:	To Get Employee Direct Dial Contact and Email by Employee ID
-- 	Web Page: PrintLetter.aspx
-- =============================================*/

IF OBJECT_ID('dbo.AS_GetFromTelEmailPrintLetterByEmployeeID') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetFromTelEmailPrintLetterByEmployeeID AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE  [dbo].[AS_GetFromTelEmailPrintLetterByEmployeeID]
	-- Add the parameters for the stored procedure here
	@EmployeeID int
AS
BEGIN
	
	SELECT
		ISNULL(E_CONTACT.WORKDD, 'N/A') AS FromDirectDial,		
		ISNULL(E_CONTACT.WORKEMAIL, 'N/A') AS FromEmail,
		
		ISNULL(JR.JobeRoleDescription, 'Job Title N/A') AS FromJobTitle
		
	FROM
		E__EMPLOYEE
		INNER JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
		INNER JOIN E_JOBDETAILS JD ON E__EMPLOYEE.EMPLOYEEID = JD.EMPLOYEEID
		INNER JOIN E_JOBROLE JR on	JD.JobRoleId = JR.JobRoleId
		
	WHERE
		E__EMPLOYEE.EMPLOYEEID = @EmployeeID
		
END
GO
