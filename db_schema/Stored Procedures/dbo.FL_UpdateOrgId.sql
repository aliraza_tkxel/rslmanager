SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 -- =============================================
-- EXEC FL_UpdateOrgId
--  @TempFaultId = 3
-- Author:		<Ahmed Mehmood>
-- Create date: <28/01/2013>
-- Description:	<Update OrgID in Basket>
-- Web Page: FaultBasket.aspx
-- =============================================
 
CREATE PROCEDURE [dbo].[FL_UpdateOrgId] 

 (	
	@TempFaultId Int,
	@Orgid Int
  )
AS 
BEGIN
      UPDATE   
          [FL_TEMP_FAULT]
      SET  
          [ORGID] = @Orgid  
      WHERE  
      [TempFaultID]=@TempFaultId
       
  END

GO
