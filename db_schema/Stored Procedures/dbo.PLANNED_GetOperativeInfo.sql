
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetOperativeInfo] 

/* ===========================================================================
 '   NAME:           PLANNED_GetOperativeInfo
-- EXEC	[dbo].[PLANNED_GetOperativeInfo]	
-- Author:		<Ahmed Mehmood>
-- Create date: <1/11/2013>
-- Description:	<Get operative's info>
-- Web Page: ReplacementList.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@operativeId int = -1		
	)
		
AS
	SELECT	E__EMPLOYEE.EMPLOYEEID as EmployeeId, E__EMPLOYEE.FIRSTNAME +' '+ E__EMPLOYEE.LASTNAME as Name , E_CONTACT.WORKEMAIL as Email
	FROM	E__EMPLOYEE LEFT JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID 
	WHERE	E__EMPLOYEE.EMPLOYEEID = @operativeId
	
				















GO
