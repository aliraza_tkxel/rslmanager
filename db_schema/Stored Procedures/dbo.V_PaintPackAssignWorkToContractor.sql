USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_PaintPackAssignWorkToContractor]    Script Date: 08/29/2016 14:43:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[V_PaintPackAssignWorkToContractor]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[V_PaintPackAssignWorkToContractor] AS SET NOCOUNT ON;') 
GO    
-- =============================================
-- Author:		Ali Raza
-- Create date: 31/08/2014
-- Description:	Assign Work to Contractor.
-- History:          31/08/2014 AR : Save Purchase order for Paint Pack Assign Work to Contractor
-- =============================================
ALTER PROCEDURE [dbo].[V_PaintPackAssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@pdrContractorId INT,
	@paintPackId INT,
	@contractorId INT,
	@contactId INT,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@POStatus INT,
	@ContractorWorksDetail AS PDR_AssingToContractorWorksRequired READONLY,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


/* Working of this stored Procedure
** 1- Get Status Id of "Assigned To Contractor" from Planned_Status. In case a Status of "Assigned To Contractor" 
**    is not present add a new one and get its Id.

** 2- Insert a new record in F_PURCHASEORDER and get the identity value as Order Id.
**
** 3- Insert a new record in PDR_CONTRACTOR_WORK using the given input data and OrderId
**    and get Identity Value as PDRContractorId.
**
** Loop (Insert Purchase Items and Works Required Items.
**
**   4- Insert a Purchase Item in F_PURCHASEITEM from given an constant data
**      and get Identity Value PURCHASEORDERITEMID
**
**   5- Insert a new work required from given data and also insert PURCHASEORDERITEMID
**
** End Loop
*/


BEGIN TRANSACTION
BEGIN TRY

-- To save same time stamp in all records 
DECLARE @CurrentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusId int = NULL

-- =====================================================
-- get status history id of "Assigned To Contractor"
-- =====================================================

SELECT	@newStatusId = STATUSID
FROM	PDR_STATUS
WHERE	PDR_STATUS.TITLE = 'Assigned To Contractor'



IF (@PdrContractorId = -1)
BEGIN

-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @Active bit = 1
, @POTYPE int = (SELECT	POTYPEID
				FROM F_POTYPE
				WHERE POTYPENAME = 'General') -- 2 = 'Repair'

-- To get Identity Value of Purchase Order.
, @purchaseOrderId int

INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
							POTYPE, POSTATUS, GASSERVICINGYESNO)
VALUES (UPPER('Paint Pack Work Order'), @CurrentDateTime
, 'This purchase order was created for Paint Pack from the new Paint Pack Work process.'
, @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0)

SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================

	INSERT INTO [PDR_CONTRACTOR_WORK]
           ([ContractorId],[ContactId],[AssignedDate],[AssignedBy],[Estimate],[EstimateRef],[PurchaseOrderId],PaintPackId)
	VALUES
           (@ContractorId,@ContactId,@CurrentDateTime,@userId,@Estimate,@EstimateRef,@PurchaseOrderId,@paintPackId)
    SET @PdrContractorId = SCOPE_IDENTITY()    
           
END
ELSE
BEGIN
	UPDATE [PDR_CONTRACTOR_WORK]
	SET [ContractorId] = @ContractorId
	,[ContactId] = @ContactId
	,[AssignedDate] = @CurrentDateTime
	,[AssignedBy] = @userId
	,[Estimate] = @Estimate
	,[EstimateRef] = @EstimateRef
	
	WHERE PDRContractorId = @PdrContractorId
END


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	*
FROM @ContractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE
@WorkDetailId int ,
@ServiceRequired nvarchar(4000),
@NetCost smallmoney,
@VatType int,
@VAT smallmoney,
@GROSS smallmoney,
@PIStatus int,
@ExpenditureId int,
@CostCenterId int,
@BudgetHeadId int

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(20) = 'Paint Pack Work' -- Title for Purchase Items, specially to inset in F_PurchaseItem

		-- =====================================================
		-- Loop (Start) through records and insert works required
		-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId,@ServiceRequired, @NetCost, @VatType, @VAT,
		@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
		WHILE @@FETCH_STATUS = 0 BEGIN
	
		IF (@WorkDetailId = -1)
		BEGIN
			-- =====================================================
			--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
			-- =====================================================

			INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
									NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
			VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @ServiceRequired, 
				@CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

			DECLARE @ORDERITEMID int = SCOPE_IDENTITY()

			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================

			INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL]
           ([PDRContractorId],[ServiceRequired],[NetCost],[VatId],[Vat],[Gross],[ExpenditureId],[CostCenterId],[BudgetHeadId],[PURCHASEORDERITEMID])
			VALUES
           (@pdrContractorId,@ServiceRequired,@NetCost,@VatType,@VAT,@GROSS,@ExpenditureId,@CostCenterId,@BudgetHeadId,@ORDERITEMID)
           
		END

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId, @ServiceRequired, @NetCost, @VatType, @VAT,
				@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = (Select InspectionJournalId from V_PaintPack where PaintPackId=@paintPackId)

END
