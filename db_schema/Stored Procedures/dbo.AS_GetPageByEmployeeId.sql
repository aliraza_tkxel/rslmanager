SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetPageByEmployeeId @employeeId = 616
-- Author:<Salman Nazir>
-- Create date: <10/18/2012>
-- Description:	<Get User's Rights >
-- Web Page: Access.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPageByEmployeeId](
@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM AS_User_Pages
	Where EmployeeId = @employeeId
END
GO
