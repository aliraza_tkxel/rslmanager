SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[TO_C_CUSTOMER_PasswordChange]

/*	===============================================================
	'   NAME:           TO_C_CUSTOMER_PASSWORDCHANGE
	'   DATE CREATED:   03 JULY 2008
	'   CREATED BY:     Naveed Iqbal
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To change password once previous username & password is verified
	'   UPDATE:         C_ADDRESS.LOGIN
	'   IN:             @Email, @Password,@NewPassword
	'   OUT: 	        @CustomerID
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

    @Email varchar (50),
    @Password varchar (50),
    @Newpassword varchar (50),
    @customerID int OUTPUT    
    

AS

    /* This query verifies email & password and selects customerID */ 
 
  SELECT 
		@customerID = Customer.CUSTOMERID
                                                                     
  FROM  
		C__CUSTOMER AS Customer INNER JOIN C_ADDRESS AS CustomerAddress                    
        ON Customer.CUSTOMERID = CustomerAddress.CUSTOMERID
                                  
        INNER JOIN TO_LOGIN AS Login
        ON Customer.CUSTOMERID = Login.CustomerID
                                  
  WHERE 
        (CustomerAddress.EMAIL = @Email) AND (Login.Password = @password)
        AND (CustomerAddress.ISDEFAULT = 1)  AND (Login.Active = 1)
        


/* ' If customerID found (means email & password verified, password will be updated with new value */     

/* 'If customer ID is NULL(authentication failed), return back without updating email */
IF @customerID IS NULL

  BEGIN  
        SET @customerID = -1
        RETURN  
  END
 
 
 
 /* ' Otherwise update password against customerID */
                            
	UPDATE    
	          TO_LOGIN
	
    SET
              TO_LOGIN.PASSWORD = @newPassword

    WHERE 
              ( TO_LOGIN.CUSTOMERID = @customerID )

GO
