USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_Get_CostCenter_Head]    Script Date: 22/3/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.F_Get_CostCenter_Head') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_Get_CostCenter_Head AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Saud Ahmed
-- Create date: 22/3/2017
-- Description:	Get all cost center accounts details 
-- WebPage: BHGFinanceModule/Views/CostCenter/Index
-- EXEC F_Get_Head '01/04/2016 00:00', '31/03/2017 23:59' 

-- =============================================
ALTER PROCEDURE [dbo].[F_Get_CostCenter_Head]				
		@fromDate datetime,
		@toDate datetime,
		@costCenterId int
AS
BEGIN
	
	-- Head
	SELECT F_HEAD.HEADID, F_HEAD.COSTCENTREID, F_HEAD.DESCRIPTION AS HEADNAME, 
			isNull(HEAD_BUDGET.BUDGET,0) as BUDGET
			,(ISNULL(Sum(TOPLEVEL.BALANCE) ,0) + ISNULL(Sum(dl.BALANCE) ,0) - ISNULL(Sum(cl.BALANCE) ,0)) AS NL
			,isNull(Sum(ORDERED_PURCHASES),0) as PURCHASES 
			,ISNULL(BUDGET 
				- (ISNULL(Sum(TOPLEVEL.BALANCE) ,0) + ISNULL(Sum(dl.BALANCE) ,0)) - ISNULL(Sum(cl.BALANCE) ,0)
				- isNull(Sum(ORDERED_PURCHASES),0),0) AS BAL
	FROM F_HEAD 
		inner join F_HEAD_ALLOCATION HDA on F_HEAD.HEADID = HDA.HEADID
		inner join F_EXPENDITURE EX on EX.HEADID = F_HEAD.HEADID
		inner join F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID 
			LEFT JOIN (  
				SELECT SUM(HDA.HEADALLOCATION) as BUDGET, HDA.HEADID
					FROM F_HEAD_ALLOCATION HDA
					INNER JOIN F_HEAD ON HDA.HEADID = F_HEAD.HEADID AND HDA.FISCALYEAR in  (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
				GROUP BY HDA.HEADID ) 
				HEAD_BUDGET ON F_HEAD.HEADID = HEAD_BUDGET.HEADID
			LEFT JOIN (  
				SELECT SUM(GROSSCOST) as ORDERED_PURCHASES, F_EXPENDITURE.EXPENDITUREID 
					FROM F_PURCHASEITEM  
					INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID
					inner join F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = F_EXPENDITURE.EXPENDITUREID  
					INNER JOIN F_HEAD ON F_EXPENDITURE.HEADID = F_HEAD.HEADID
					INNER JOIN F_POSTATUS ON  F_PURCHASEITEM.PISTATUS = F_POSTATUS.POSTATUSID    
					WHERE F_POSTATUS.POSTATUSNAME IN ('Goods Ordered', 'Work Ordered', 'Work Completed', 'Part Completed', 'Goods Received', 'Goods Approved','Invoice Received','Invoice Approved')   
					AND F_PURCHASEITEM.ACTIVE = 1 
					AND PIDATE between @fromDate AND @toDate
					ANd EXA.ACTIVE =1 and FISCALYEAR in (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
				GROUP BY F_EXPENDITURE.EXPENDITUREID ) 
				ORDERED_PURCHASES ON EX.EXPENDITUREID = ORDERED_PURCHASES.EXPENDITUREID 
			LEFT JOIN (
						SELECT	F_EXPENDITURE.EXPENDITUREID ,SUM(GROSSCOST) as Balance
						FROM	F_PURCHASEITEM
								INNER JOIN F_POSTATUS ON  F_PURCHASEITEM.PISTATUS = F_POSTATUS.POSTATUSID
								INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID
						WHERE	F_POSTATUS.POSTATUSNAME IN ('Invoice Paid','Reconciled','Paid By Cheque','Paid By BACS','Paid by Direct Debit','Paid','Paid By Cash','Part Paid')    
								AND F_PURCHASEITEM.ACTIVE = 1 
								AND PIDATE BETWEEN @fromDate AND @toDate
						GROUP BY F_EXPENDITURE.EXPENDITUREID 
				) TOPLEVEL ON TOPLEVEL.EXPENDITUREID = EX.EXPENDITUREID 
			LEFT JOIN (
						select GJDL.ExpenditureId, sum(GJDL.amount) as Balance from NL_JOURNALENTRY nlj
						inner join  NL_JOURNALENTRYDEBITLINE GJDL on nlj.TXNID = GJDL.TXNID 
						inner join NL_TRANSACTIONTYPE TT on nlj.TRANSACTIONTYPE = TT.TRANSACTIONTYPEID
						where GJDL.ExpenditureId is not null 
							AND TT.LONGDESCRIPTION = 'General Journal Reversal' OR TT.LONGDESCRIPTION = 'General Journal' 
							ANd nlj.TXNDATE BETWEEN @fromDate AND @toDate
						group by GJDL.ExpenditureId
				) DL ON DL.EXPENDITUREID = EX.EXPENDITUREID 
			LEFT JOIN (
						select GJCL.ExpenditureId, sum(GJCL.amount) as Balance from NL_JOURNALENTRY nlj
						inner join  NL_JOURNALENTRYCREDITLINE GJCL on nlj.TXNID = GJCL.TXNID 
						inner join NL_TRANSACTIONTYPE TT on nlj.TRANSACTIONTYPE = TT.TRANSACTIONTYPEID
						where GJCL.ExpenditureId is not null
						AND TT.LONGDESCRIPTION = 'General Journal Reversal' OR TT.LONGDESCRIPTION = 'General Journal' 
						ANd nlj.TXNDATE BETWEEN @fromDate AND @toDate
						group by GJCL.ExpenditureId
				) CL ON CL.EXPENDITUREID = EX.EXPENDITUREID 
			where COSTCENTREID = @costCenterId And 
			HDA.ACTIVE = 1 and HDA.FISCALYEAR in (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
								ANd EXA.ACTIVE =1 and EXA.FISCALYEAR in (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
			group by F_HEAD.HEADID, F_HEAD.COSTCENTREID, F_HEAD.DESCRIPTION, HEAD_BUDGET.BUDGET
			ORDER BY F_HEAD.DESCRIPTION 


				
									
END
