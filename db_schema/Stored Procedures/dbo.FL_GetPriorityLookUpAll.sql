SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[FL_GetPriorityLookUpAll]
/* ===========================================================================
 --	EXEC FL_GetPriorityLookUpAll
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To Get List of PriorityName and PriorityID for LookUp/DropDown Lists
--  Webpage:		View/Reports/ReportArea.aspx (For Add/Amend Fault Modal Popup)
 '==============================================================================*/
AS
	SELECT PriorityID AS id,PriorityName AS val
	FROM FL_FAULT_PRIORITY where Status=1
	ORDER BY PriorityID ASC
GO
