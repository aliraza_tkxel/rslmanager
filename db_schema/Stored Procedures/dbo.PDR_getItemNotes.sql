/* =================================================================================        
    Page Description:    Get attribute notes for scheme Block    
     
    Author: Ali Raza    
    Creation Date: jan-2-2015      
    
    Change History:    
    
    Version      Date             By                      Description    
    =======     ============    ========           ===========================    
    v1.0         jan-2-2015      Ali Raza           Get attribute notes for scheme Block    
        
    Execution Command:    
        
    Exec [PDR_getItemNotes] 526,null,74,null   
  =================================================================================*/  
    IF OBJECT_ID('dbo.PDR_getItemNotes') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_getItemNotes AS SET NOCOUNT ON;') 
GO  
ALTER PROCEDURE [dbo].[PDR_getItemNotes] (      
@schemeId int=null,     
@blockId int=null,       
@itemId int,
@heatingMappingId int = null      
)      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
  DECLARE @searchCriteria NVARCHAR(2000),@selectClause NVARCHAR(2000),@whereClause NVARCHAR(1000),@MainQuery NVARCHAR(4000)    
  --========================================================================================        
  -- Begin building SearchCriteria clause        
  -- These conditions will be added into where clause based on search criteria provided        
        
    SET @searchCriteria = 'And 1=1 '  
         
IF @schemeId IS NOT NULL AND @schemeId > 0      
   BEGIN      
    SET @searchCriteria = +'  AND SchemeId = ' + CONVERT(varchar(10), @schemeId) + ' '        
   END         
  IF @blockId IS NOT NULL AND  @blockId >0      
   BEGIN      
    SET @searchCriteria =@searchCriteria + CHAR(10) +'  AND BlockId = ' +  CONVERT(varchar(10), @blockId) + ' '        
   END 
   
   IF @heatingMappingId IS NOT NULL AND @heatingMappingId > 0      
   BEGIN      
    SET @searchCriteria = @searchCriteria +'  AND heatingMappingId = ' + CONVERT(varchar(10), @heatingMappingId) + ' '        
   END      
  -- End building SearchCriteria clause           
  --========================================================================================        
        
    -- Insert statements for procedure here      
    
     
 SET @selectClause = ' SELECT [SID],PROPERTYID,ItemId,Notes, CONVERT(varchar,CreatedOn,103 )   CreatedOn,LEFT(e.FIRSTNAME, 1)+ LEFT(e.LASTNAME, 1)  CreatedBy,
 ShowInScheduling, ShowOnApp,ISActive    
 FROM PA_PROPERTY_ITEM_NOTES    
 LEFT JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy '        
        
 -- Begin building WHERE clause        
        
  -- This Where clause contains subquery to exclude already displayed records             
         
  SET @whereClause = CHAR(10) + ' WHERE  ItemId ='+  CONVERT(varchar(10), @itemId) + CHAR(10) + @searchCriteria        
  --PRINT(@searchCriteria)   
  --PRINT(@whereClause)     
  -- End building WHERE clause        
      
   -- Begin building the main select Query        
        
  SET @MainQuery = @selectClause  + @whereClause      
        
  -- End building the main select Query        
 PRINT(@MainQuery)    
 EXEC(@MainQuery)    
END 