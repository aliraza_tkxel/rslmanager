SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[STP_DEVELOPER_PRINT_SUMMARY_COMMENTS]

@JOBID int

AS

SELECT  RC.COMMENT_ID, RC.COMMENTS, RC.DATE_ENTERED, l.FIRSTNAME + ' ' + l.LASTNAME AS FULLNAME, dbo.H_TYPE.ICON 
	FROM dbo.H_REQUEST_COMMENTS RC 
		INNER JOIN dbo.H_TYPE ON RC.TYPE_ID = dbo.H_TYPE.TYPE_ID 
		LEFT OUTER JOIN dbo.H_LOGIN l ON l.USER_ID = RC.USER_ID 
	WHERE RC.JOB_ID = @JOBID
UNION ALL 
SELECT -1 AS COMMENTID, 'Status updated to <B>' + s.description + '</B>. ' + Comments AS COMMENTS, [timestamp] AS DATE_ENTERD, 
FIRSTNAME + ' ' + LASTNAME as FULLNAME, 'no_icon.gif' as ICON 
	FROM h_request_journal_progress rjp 
		LEFT join h_login l on l.user_id = rjp.user_id 			
		LEFT join h_status s on s.status_id = rjp.status_id 
	WHERE rjp.job_id = @JOBID
UNION ALL 
SELECT -1 AS COMMENTID, 'Comments sent to customer on completion: ' +	SU.COMMENTS, 
[timestamp] as DATE_ENTERD, FIRSTNAME + ' ' + LASTNAME as FULLNAME, 'no_icon.gif' as ICON 
	FROM h_support_units SU 
		LEFT join h_login l on l.user_id = SU.user_id 
	WHERE SU.job_id = @JOBID 
ORDER BY DATE_ENTERED desc



GO
