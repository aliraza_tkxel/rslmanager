SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [diagnostics].[KPIReport_Emergency]

AS 

BEGIN 

DECLARE @Month INT = MONTH(DATEADD(m,-1,GETDATE()))
DECLARE @YearStartDate DATETIME
DECLARE @YearEndDate DATETIME

SELECT @YearStartDate = StartDate, @YearEndDate = EndDate FROM diagnostics.CurrentFiscalYear()

-- Emergency
SELECT  DATENAME(month, CompletedDate) AS [Month] ,
        ISNULL(SupplierName, '') AS NAME ,
        OPERATIVE ,
        JobSheetNumber ,
        DEVELOPMENTNAME AS [Scheme] ,
        PROPERTYID AS [Property] ,
        CAST ([FaultDescription] AS VARCHAR(255)) AS [Repair/Fault] ,
        SUBMITDATE AS LoggedDate ,
        DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                        AppointmentDate)) AS AppointmentEndDateTime ,
        CAST( DATEDIFF(mi, SUBMITDATE,
                 DATEADD(mi, DATEPART(mi, CAST(EndTime AS DATETIME)),
                         DATEADD(hh, DATEPART(hour, CAST(EndTime AS DATETIME)),
                                 AppointmentDate))) - ( 86400 / 60 ) AS INT) AS [Logged vs AppointmentEndDate] ,
        AppointmentDate AS AppointmentDate ,
        StartTime ,
        EndTime ,
        CompletedDate AS CompletionDate ,
        CAST(DATEDIFF(Day, SUBMITDATE, CompletedDate) AS INT) AS [Days] ,
        PATCH ,
        STOCKTYPE
FROM    dbo.FL_FAULT_LIST_COMPLETED_DETAILS
WHERE   CompletedDate >= @YearStartDate
        AND CompletedDate <= @YearEndDate
        AND MONTH(CompletedDate) = @Month
        AND PriorityId IN ( 3, 4 ) -- Emergency
ORDER BY MONTH(CompletedDate)


END 
GO
