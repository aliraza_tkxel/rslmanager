SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[I_NEWS_UPDATE]
(
	@NewsCategory int,
	@Headline varchar(250),
	@ImagePath varchar(250),
	@DateCreated smalldatetime,
	@NewsContent text,
	@Rank int,
	@Active bit,
	@HomePageImage varchar(250),
	@IsHomePage bit,
	@Original_NewsID int,
	@NewsID int,
	@IsTenantOnline bit
)
AS
	SET NOCOUNT OFF;
UPDATE [I_NEWS] SET [NewsCategory] = @NewsCategory, [Headline] = @Headline, [ImagePath] = @ImagePath, [DateCreated] = @DateCreated, [NewsContent] = @NewsContent, [Rank] = @Rank, [Active] = @Active, [HomePageImage] = @HomePageImage, [IsHomePage] = @IsHomePage, [IsTenantOnline]=@IsTenantOnline WHERE (([NewsID] = @Original_NewsID));
	
SELECT NewsID, NewsCategory, Headline, ImagePath, DateCreated, NewsContent, Rank, Active, HomePageImage, IsHomePage, IsTenantOnline FROM I_NEWS WHERE (NewsID = @NewsID)


GO
