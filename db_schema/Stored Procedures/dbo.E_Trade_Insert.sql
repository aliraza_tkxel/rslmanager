SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[E_Trade_Insert]
(
	@EmpId		INT,
	@TradeIds	NVARCHAR(MAX) = NULL,
	@UserId		INT
) AS

-- STEP 1
-- Update the record, so the correct timestamp/userid goes to Audit table
UPDATE E_TRADE SET LASTACTIONUSER = @UserId, LASTACTIONTIME = GETDATE()
WHERE EmpId = @EmpId


-- STEP 2
-- Remove Employee Trade data
DELETE FROM E_TRADE WHERE EmpId = @EmpId

-- STEP 3
-- Add Employee Trade data (Comma delimited)

IF LEN(@TradeIds) > 0
BEGIN
	INSERT INTO E_TRADE
	SELECT @EmpId AS EmpId, COLUMN1 AS TradeId, @UserId AS LASTACTIONUSER, GETDATE() AS LASTACTIONTIME FROM dbo.SPLIT_STRING (@TradeIds,',')
END

-----------------------------------
--EXAMPLE SP EXECUTIONS
-----------------------------------
--EXEC [E_Trade_Insert] 705,'1,2,3'
--EXEC [E_Trade_Insert] 705,null
--EXEC [E_Trade_Insert] 705,''
-----------------------------------
GO
