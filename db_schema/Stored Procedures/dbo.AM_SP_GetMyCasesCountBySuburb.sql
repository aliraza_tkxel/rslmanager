
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adeel Ahmed>
-- Create date: <29/04/2010>
-- Description:	<Counting the My Cases on the basis of Suburb>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetMyCasesCountBySuburb]
	@caseOfficer INT,
	@regionId INT,
	@suburbId INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
	SELECT AM_Case.CaseId  from AM_Case 

	INNER JOIN C_TENANCY on AM_Case.TenancyId = C_TENANCY.TENANCYID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
	LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
	INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID 

	WHERE AM_Case.CaseOfficer = @caseOfficer and PDR_DEVELOPMENT.PATCHID = @regionId and P__PROPERTY.SCHEMEID = @suburbId
END





GO
