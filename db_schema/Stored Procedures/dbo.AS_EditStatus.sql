
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	AS_EditStatus @Title = N'NewStatus',@Ranking = 7,	@ModifiedBy = 1,@InspectionTypeId = 3,@StatusId = 2
-- Author:		<Salman Nazir>
-- Create date: <21/10/2012>
-- Description:	<Description,,Edit the values of Status>
-- Web Page: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_EditStatus] (
	-- Add the parameters for the stored procedure here
	@Title varchar(1000),
	@Ranking int,
	@ModifiedBy int,
	@InspectionTypeId int,
	@StatusId int
	)
AS
BEGIN
Declare @oldStatusId int
Declare @oldRanking int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT @oldStatusId=StatusId FROM AS_Status Where Ranking = @Ranking
    SELECT @oldRanking=Ranking FROM AS_Status Where StatusId = @StatusId
    IF @oldStatusId = @StatusId
		BEGIN
			UPDATE AS_Status SET Title = @Title,Ranking=@Ranking,ModifiedBy=@ModifiedBy,InspectionTypeId=@InspectionTypeId,IsEditable=1,ModifiedDate=GETDATE() 
			WHERE StatusId = @StatusId
			
			INSERT INTO AS_StatusHistory(StatusId,Title,Ranking,CreatedBy,InspectionTypeID,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
			VALUES (@StatusId,@Title,@Ranking,@ModifiedBy,@InspectionTypeId,@modifiedBy,GETDATE(),GETDATE(),1)
			
		END
	ELSE
		BEGIN
			UPDATE AS_Status SET Ranking = @oldRanking WHERE StatusId = @oldStatusId
			declare @oldStatusTitle varchar(100)
			declare @oldStatusInspectionTypeId int
			SELECT @oldStatusTitle = Title,@oldStatusInspectionTypeId = InspectionTypeId FROM AS_Status WHERE StatusId = @oldStatusId
			
			INSERT INTO AS_StatusHistory(StatusId,Title,Ranking,CreatedBy,InspectionTypeID,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
			VALUES (@oldStatusId,@oldStatusTitle,@oldRanking,@ModifiedBy,@oldStatusInspectionTypeId,@modifiedBy,GETDATE(),GETDATE(),1)
			
			
			
			UPDATE AS_Status SET Title = @Title,Ranking=@Ranking,ModifiedBy=@ModifiedBy,InspectionTypeId=@InspectionTypeId,IsEditable=1,ModifiedDate=GETDATE() 
			WHERE StatusId = @StatusId
			
			INSERT INTO AS_StatusHistory(StatusId,Title,Ranking,CreatedBy,InspectionTypeID,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
			VALUES (@StatusId,@Title,@Ranking,@ModifiedBy,@InspectionTypeId,@modifiedBy,GETDATE(),GETDATE(),1)
			
		END	
END
GO
