USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PDR_GetGeneralJournalExProperties') IS NULL 
 EXEC('CREATE PROCEDURE dbo.PDR_GetGeneralJournalExProperties AS SET NOCOUNT ON;')
GO


ALTER PROCEDURE [dbo].[PDR_GetGeneralJournalExProperties] 
	@txnId INT
AS
BEGIN
	
	DECLARE @propertyId nvarchar(40), @schemeId int , @blockId int
	SELECT	 @propertyId = PropertyId, @schemeId = SchemeId, @blockId = BlockId
	FROM	 NL_JOURNALENTRY
	WHERE	TXNID = @txnId

	IF @propertyId IS NOT NULL
	BEGIN

		IF @blockId IS NOT NULL AND @blockId > 0
		BEGIN			
				SELECT	ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
				FROM	P__PROPERTY P
				WHERE	BLOCKID = @blockId AND PROPERTYID != @propertyId
				ORDER BY CAST(SUBSTRING(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,''), 1,CASE	WHEN PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) > 0 THEN 
															PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) - 1 
														ELSE LEN(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) 
														END
														) AS INT) ASC, ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') ASC
		END
		ELSE
		BEGIN								
				SELECT	ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
				FROM	P__PROPERTY P
				WHERE	SCHEMEID = @schemeId AND PROPERTYID != @propertyId
				ORDER BY CAST(SUBSTRING(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,''), 1,CASE	WHEN PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) > 0 THEN 
															PATINDEX('%[^0-9]%',ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) - 1 
														ELSE LEN(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'')) 
														END
														) AS INT) ASC, ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') ASC
		END
		
	END
	ELSE
	BEGIN
			SELECT	ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.ADDRESS3,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' + ISNULL(P.COUNTY,'') + ', ' + ISNULL(P.POSTCODE,'') AS ADDRESS
			FROM	P__PROPERTY P
			WHERE	1=0				
	END
	
END
