SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetPagesByParentId @parentPageId = 2
-- Author:<Salman Nazir>
-- Create date: <10/17/2012>
-- Description:	<Get all pages on Access Right Page>
-- Web Page: Access.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPagesByParentId](
@parentPageId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM AS_Pages
	WHERE ParentPageId = @parentPageId
END
GO
