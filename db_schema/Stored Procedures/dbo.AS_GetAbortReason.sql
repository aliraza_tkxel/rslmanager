USE [RSLBHALive]
GO

-- =============================================
-- Author:			Abdul Rehman
-- Create date:		8/2/2016
-- Description:		Get abort reason from journal ID
-- Webpage:			RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx

-- =============================================
IF OBJECT_ID('dbo.AS_GetAbortReason') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetAbortReason AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetAbortReason]
	@journalId int
AS
BEGIN
	
	
	
	SET NOCOUNT ON;

   select top(1) AS_AbortAppointments.AbortNotes, AS_AbortReason.AbortReason
   FROM AS_AbortAppointments INNER JOIN AS_APPOINTMENTS on  AS_AbortAppointments.AppointmentId = AS_APPOINTMENTS.APPOINTMENTID
   INNER JOIN AS_AbortReason on AS_AbortAppointments.AbortReasonId = AS_AbortReason.AbortReasonId
   where AS_APPOINTMENTS.JournalId = @journalId

END


GO

