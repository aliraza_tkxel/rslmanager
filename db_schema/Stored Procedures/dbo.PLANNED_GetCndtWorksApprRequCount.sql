USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetCndtWorksApprRequCount]    Script Date: 28/04/2017 07:18:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.PLANNED_GetCndtWorksApprRequCount') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_GetCndtWorksApprRequCount AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PLANNED_GetCndtWorksApprRequCount] 
/* ===========================================================================
-- NAME:		PLANNED_GetCndtWorksApprRequCount
-- EXEC			[dbo].[PLANNED_GetCndtWorksApprRequCount] -1
-- Author:		<Muhammad Awais>
-- Create date: <03/02/2015>
-- Description:	<Get Replacement Due Current Year Count>
-- Web Page:	Dashboard.aspx
'==============================================================================*/
(
	@componentId int = -1
)
AS
Begin
	DECLARE @sqlCommand varchar(1000)

	SET @sqlCommand = char(10) + 'SELECT COUNT(PCW.ConditionWorksId) as TotalCount
						FROM PLANNED_CONDITIONWORKS [PCW] 
						INNER JOIN PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID
						INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId
						INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID
						INNER JOIN PA_PARAMETER [PARAM] ON [PARAM].ParameterID = IP.ParameterId AND PARAM.ParameterName LIKE ''Condition Rating'' AND ([ATT].PARAMETERVALUE = ''Unsatisfactory'' OR [ATT].PARAMETERVALUE = ''Potentially Unsatisfactory'')
						INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = [ATT].VALUEID AND (PV.ValueDetail = ''Unsatisfactory'' OR PV.ValueDetail = ''Potentially Unsatisfactory'')
						WHERE [PLA].Title = ''Recommended'' '
						
						IF @componentId != -1
						BEGIN
							SET @sqlCommand = @sqlCommand + CHAR(10) +' AND [PCW].ComponentId = ' + CONVERT(nvarchar(10), ''+@componentId+'')
						END
	EXEC (@sqlCommand)
END 
