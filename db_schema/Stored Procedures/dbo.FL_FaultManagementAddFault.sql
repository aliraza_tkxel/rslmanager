SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FL_FaultManagementAddFault]
/* ===========================================================================
 --	EXEC FL_FaultManagementAddFault
		@AREAID = 1,
		@DESCRIPTION = 'Tap is Broken',
		@PRIORITYID = 1,
		@DURATION = 0.5,
		@RECHARGE = 1, -- Ture
		@NETCOST = 100,
		@VATRATEID = 1,
		@Vat = 20,
		@GROSS = 120,
		@ISCONTRACTOR = 1, --True
		@FAULTACTIVE = 1, --True
		@ISGASSAFE = 1, --True
		@ISOFTEC = 1, --True
		@@USERID = 65,
		@SUBMITDATE = CONVERT(date, CURRENT_TIMESTAMP),
		@RESULT OUTPUT,
		@FAULTID OUTPUT
--  Author:			Aamir Waheed
--  DATE CREATED:	1 March 2013
--  Description:	To Add/Create a new fault with given values and Return FaultID
--  Webpage:		View/Reports/ReportArea.aspx (For Add Fault Modal Popup)
 '==============================================================================*/

@AREAID INT,
@DESCRIPTION NVARCHAR(4000),
@PRIORITYID INT,		
@DURATION FLOAT,
@RECHARGE BIT,		
@NETCOST FLOAT = NULL,
@VATRATEID INT  = NULL,
@Vat FLOAT = NULL,
@GROSS FLOAT = NULL,
@ISCONTRACTOR BIT,
@FAULTACTIVE BIT,
@ISGASSAFE BIT,
@ISOFTEC BIT,

@USERID INT = NULL,
@SUBMITDATE nvarchar(20) = NULL,

-- Output Parameters 
@RESULT  INT OUT,
@FaultID INT OUT

AS
BEGIN 
DECLARE @TempTable AS Table(TempFaultID INT) -- To return Inserted FaultID
SET NOCOUNT ON
BEGIN TRAN

-- START Insert  Fault and Get FaultID to Inset Trades in FL_FAULT_Trade(Many to Many mapping Table)
INSERT INTO FL_FAULT
	(AREAID, DESCRIPTION, PRIORITYID, duration, RECHARGE, NETCOST, VATRATEID, VAT, GROSS,  
		isContractor, FAULTACTIVE, IsGasSafe, IsOftec, SUBMITDATE ) 	
	OUTPUT inserted.FaultID INTO @TempTable
VALUES 	( @AREAID, @DESCRIPTION, @PRIORITYID, @DURATION, @RECHARGE, @NETCOST, @VATRATEID, @VAT, @GROSS, 
			@ISCONTRACTOR, @FAULTACTIVE, @ISGASSAFE, @ISOFTEC, CONVERT(DATETIME,@SUBMITDATE,103)) 
-- END Insert  Fault and Get FaultID to Inset Trades in FL_FAULT_Trade(Many to Many mapping Table)

-- Get Inserted Fault's FaultID INTO @FaultID
SELECT @FaultID = TempFaultID FROM @TempTable

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	

exec FL_FAULT_TRANSACTIONLOG_ADD @FaultID,@USERID,0,@SUBMITDATE;
SET @RESULT=1
RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN
  SET @FaultID = -1
  SET @RESULT=-1
RETURN













GO
