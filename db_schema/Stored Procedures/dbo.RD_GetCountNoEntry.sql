SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC	[dbo].[RD_GetCountNoEntry]
-- Author:		<Ali Raza>
-- Create date: <24/07/2013>
-- Description:	<This stored procedure gets the count of all 'No Entries'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetCountNoEntry]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declaring the variables to be used in this query
SELECT  count(*) 
FROM FL_FAULT_NOENTRY
	JOIN FL_FAULT_LOG on FL_FAULT_NOENTRY.FaultLogId = FL_FAULT_LOG.FaultLogID
		JOIN FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
		JOIN P__PROPERTY on FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		JOIN FL_AREA on FL_FAULT.AREAID = FL_AREA.AreaID
		JOIN FL_FAULT_TRADE on FL_FAULT.FaultID = FL_FAULT_TRADE.FaultId
		JOIN G_TRADE on FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
		JOIN FL_FAULT_PRIORITY on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		
		
END




GO
