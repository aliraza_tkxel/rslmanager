SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.TO_PARKING_SPACE_AddRequest
/* ===========================================================================
 '   NAME:           TO_PARKING_SPACE_AddRequest
 '   DATE CREATED:   07 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add parking space/garage record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 parking record in TO_PARKING_SPACE
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '   IN:             @LookUPValueID
 '
 '   OUT:            @ParkingSpaceId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 
	(
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(500),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	
	@LookUPValueID INT,
	@ParkingSpaceId INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_PARKING_SPACE(
		EnquiryLogID,
		LookUpValueID
		
		)
		VALUES(
		@EnquiryLogID,
		@LookUPValueID
		)
		
		SELECT @ParkingSpaceId=ParkingSpaceID
	FROM TO_PARKING_SPACE
	WHERE ParkingSpaceID = @@IDENTITY

				IF @ParkingSpaceId<0
					BEGIN
						SET @ParkingSpaceId=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @ParkingSpaceId=-1
			ROLLBACK TRAN
			END

GO
