USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_getPropertyAppliancesByApplianceId]    Script Date: 01/06/2016 10:05:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_getPropertyAppliancesByApplianceId @propertyId = 'BHA0000727' , @applianceId = 1
-- Author:		<Salman Nazir>
-- Create date: <11/07/2012>
-- Description:	<Show Defects of an appliance >
-- Web Page: PropertyRecord.aspx => ATTributes Tab
-- =============================================


IF OBJECT_ID('dbo.AS_getPropertyAppliancesByApplianceId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_getPropertyAppliancesByApplianceId AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_getPropertyAppliancesByApplianceId](
@propertyId Varchar(200)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Convert(varchar(10),DefectDate,103) AS Date
	, PropertyDefectId
	,IsDefectIdentified
	,ISNULL(IsActionTaken,0) as IsActionTaken
	,Description
	,ISNULL(IsWarningFixed,0) as IsWarningFixed 
	,ISNULL(IsWarningIssued,0) as IsWarningIssued
	,ISNULL(DefectNotes,'') DefectNotes
	,CASE 
		WHEN ApplianceId IS NOT NULL AND ApplianceId <> 0 THEN
			'Appliance'
		WHEN BoilerTypeId IS NOT NULL AND BoilerTypeId <> 0 THEN
		    'Boiler'
		ELSE
			'Detector'			
	END AS	DefectType
	FROM	P_PROPERTY_APPLIANCE_DEFECTS	
			INNER JOIN	P_DEFECTS_CATEGORY on P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
			LEFT JOIN	GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
	WHERE	P_PROPERTY_APPLIANCE_DEFECTS.PropertyId = @propertyId 
			AND (
					(
						(ApplianceId IS NOT NULL AND ApplianceId <> 0 )
						AND GS_PROPERTY_APPLIANCE.ISACTIVE =1
					) 
					OR 
						(ApplianceId IS NULL or ApplianceId = 0 )
				)
			
			
END
