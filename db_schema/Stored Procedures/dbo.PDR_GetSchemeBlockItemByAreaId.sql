USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.PDR_GetSchemeBlockItemByAreaId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetSchemeBlockItemByAreaId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_GetSchemeBlockItemByAreaId](
@areaId int
)
AS
BEGIN

	SELECT ItemID, ItemName
	FROM PA_ITEM
	WHERE AreaID=@areaId AND IsActive=1 --AND ParentItemId IS NULL

	UNION ALL

	SELECT ItemID, ItemName
	FROM PA_ITEM
	WHERE ParentItemId=@areaId and IsActive=1

END