SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[FL_REPAIR_GETVALUES]
/* ===========================================================================
 '   NAME:          FL_REPAIR_GETVALUES
 '   DATE CREATED:   2nd Dec 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a repair against a particular fault repair id from FL_FAULT_REPAIR_LIST table which will be used to update 
 '		     a fault repair values
 '   IN:             @FaultRepairListID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@FaultRepairListID as Int
AS

SET ANSI_NULLS  OFF

	SELECT  FL_FAULT_REPAIR_LIST.FaultRepairListID, FL_FAULT_REPAIR_LIST.DESCRIPTION,FL_FAULT_REPAIR_LIST.NETCOST, FL_FAULT_REPAIR_LIST.VAT, FL_FAULT_REPAIR_LIST.GROSS, 
	FL_FAULT_REPAIR_LIST.VATRATEID, FL_FAULT_REPAIR_LIST.POSTINSPECTION, FL_FAULT_REPAIR_LIST.STOCKCONDITIONITEM,
	FL_FAULT_REPAIR_LIST.RepairActive, TBL_PDA_ITEM_PARAMETER.ParameterID,  TBL_PDA_ITEM.ItemID,TBL_PDA_AREA.AreaID 	
	
	FROM FL_FAULT_REPAIR_LIST LEFT JOIN TBL_PDA_ITEM_PARAMETER ON FL_FAULT_REPAIR_LIST.PMParameterID = TBL_PDA_ITEM_PARAMETER.ParameterID 
	LEFT JOIN TBL_PDA_ITEM ON TBL_PDA_ITEM_PARAMETER.ItemID = TBL_PDA_ITEM.ItemID 
	LEFT JOIN TBL_PDA_AREA ON TBL_PDA_ITEM.AreaID = TBL_PDA_AREA.AreaID
WHERE     (FaultRepairListID= @FaultRepairListID)












GO
