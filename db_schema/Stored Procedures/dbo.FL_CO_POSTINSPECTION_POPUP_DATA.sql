SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE PROCEDURE dbo.FL_CO_POSTINSPECTION_POPUP_DATA
/* ===========================================================================
 '   NAME:          FL_CO_POSTINSPECTION_POPUP_DATA
 '   DATE CREATED:   5th May, 2009
 '   CREATED BY:     Tahir Gul 
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a Preinspection Pop data from table FL_FAULT_PREINSPECTIONINFO
 '		     
 '   IN:            @TempFaultId
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@FaultLogId as Int,
@RepairId as int
AS
	SELECT E__EMPLOYEE.FIRSTNAME AS UserName,E__EMPLOYEE.EMPLOYEEID AS Operative,  
	E_TEAM.TEAMID AS Team,FL_CO_FAULTLOG_TO_REPAIR.NETCOST, FL_CO_FAULTLOG_TO_REPAIR.DUEDATE,FL_CO_FAULTLOG_TO_REPAIR.INSPECTIONDATE, 
	FL_CO_FAULTLOG_TO_REPAIR.NOTES, FL_CO_FAULTLOG_TO_REPAIR.APPROVED, FL_CO_FAULTLOG_TO_REPAIR.Recharge, FL_CO_FAULTLOG_TO_REPAIR.InspectionTime
	FROM E_JOBDETAILS INNER JOIN
	E__EMPLOYEE ON E_JOBDETAILS.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID INNER JOIN
	E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID INNER JOIN
	FL_CO_FAULTLOG_TO_REPAIR ON E__EMPLOYEE.EMPLOYEEID = FL_CO_FAULTLOG_TO_REPAIR.USERID
	where FL_CO_FAULTLOG_TO_REPAIR.faultlogid = @FaultLogId AND FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID = @RepairId 

GO
