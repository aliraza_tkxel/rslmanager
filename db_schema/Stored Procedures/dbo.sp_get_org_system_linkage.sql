SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create proc [dbo].[sp_get_org_system_linkage]
@orgid int
as
select * from H_org_system_linkage
where org_id = @orgid


GO
