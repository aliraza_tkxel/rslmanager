
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--EXEC	AS_UpdateUser @ModifiedBy = 1,@ModifiedDate = NULL,@IsActive = 1,@UserTypeId = 1,@EmployeeId = 35
-- Author:	<Salman Nazir>	
-- Create date: <28/09/2012>
-- Description:	<If User update the type of and then click on Save button on Resources Page>
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdateUser] 
	-- Add the parameters for the stored procedure here
	@ModifiedBy int,
	@ModifiedDate smallDateTime,
	@IsActive bit,
	@UserTypeId int,
	@EmployeeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update AS_USER SET UserTypeID = @UserTypeId,ModifiedBy=@ModifiedBy,ModifiedDate=@ModifiedDate,IsActive=@IsActive
	Where EmployeeId = @EmployeeId;
	
	SELECT E__EMPLOYEE.FIRSTNAME,E_JOBDETAILS.JOBTITLE,E__EMPLOYEE.EMPLOYEEID,AS_USERTYPE.UserTypeID,AS_USERTYPE.Description,E__EMPLOYEE.LASTNAME
	FROM AS_USER INNER JOIN
	AS_USERTYPE ON AS_USER.UserTypeID = AS_USERTYPE.UserTypeID INNER JOIN
	E__EMPLOYEE ON AS_USER.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN 
	E_JOBDETAILS ON AS_USER.EmployeeId = E_JOBDETAILS.EMPLOYEEID 
	WHERE AS_USER.IsActive=1 AND E_JOBDETAILS.ACTIVE = 1 OR E_JOBDETAILS.ENDDATE > GETDATE()
END
GO
