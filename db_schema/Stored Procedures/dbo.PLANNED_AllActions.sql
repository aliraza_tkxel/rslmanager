SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC PLANNED_AllActions
-- Author:		<Noor Muhammad>
-- Create date: <04/11/2013>
-- Description:	<This Stored Proceedure gets Actions on Status Page >
-- Web Page: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_AllActions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ActionId as ActionId,
	StatusId as StatusId,
	Title as Title, 
	Ranking as Ranking,
	IsEditable as IsEditable
	FROM PLANNED_Action 
END
GO
