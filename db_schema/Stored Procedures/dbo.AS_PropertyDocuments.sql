
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- EXEC AS_PropertyDocuments
		--@journalHistoryId=1
		
-- Author:		<Noor Muhammad>
-- Create date: <10/3/2012>
-- Description:	<This stored procedure returns the all the documnts against the property history>
-- Parameters:	
		--@journalHistoryId int = 0
-- Webpage :PropertyRecord.aspx		
		
-- =============================================
CREATE PROCEDURE [dbo].[AS_PropertyDocuments]
( 
		@journalHistoryId bigint = 0
		
)
AS
BEGIN
SELECT 
AS_JOURNALHISTORY.JOURNALHISTORYID as JournalHistoryId
,AS_Documents.DocumentName as DocumentName
,AS_Documents.DocumentId as DocumentId

FROM AS_JOURNALHISTORY 

INNER JOIN AS_Documents ON  AS_Documents.JOURNALHISTORYID = AS_JOURNALHISTORY.JOURNALHISTORYID
Where AS_JOURNALHISTORY.JournalHistoryId =@journalHistoryId

END

GO
