
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--EXEC FL_CO_REACTIVEREPAIR_JOURNAL 65717,'FL_FAULT_STATUS.FaultStatusId','DESC',1


CREATE PROCEDURE [dbo].[FL_CO_REACTIVEREPAIR_JOURNAL]

/* ===========================================================================
 '   NAME:           FL_REPORTEDFAULTSEARCHLIST
 '   DATE CREATED:   20TH February, 2009
 '   CREATED BY:     Tahir Gul 
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To Populate Reactive Repair Journal
 '   IN:             @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

(
    @faultLogID int,
    @sortColumn nvarchar(500) = 'FL_FAULT_STATUS.FaultStatusId' ,
    @sortOrder nvarchar(100) = 'DESC' ,
    @result int output 
)

		
AS
	-- variables used to build whole query
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),
	        @OrderClause  varchar(8000),
	        @COUNT  AS INT 
	
        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT CASE WHEN FL_FAULT_STATUS.DESCRIPTION in ( ''Appointment Arranged'',''In Progress'') THEN FL_CO_APPOINTMENT.APPOINTMENTDATE + FL_CO_APPOINTMENT.[TIME] ELSE NULL END AppointmentDateTime,' + 
                        	CHAR(10) + CHAR(9) + 'FL_FAULT_LOG_HISTORY.LastActionDate as SortedDate,CONVERT(varchar, FL_FAULT_LOG_HISTORY.LastActionDate, 103) as LastActionDate , CONVERT(varchar, FL_FAULT_LOG_HISTORY.LastActionDate, 103)+'' ''+ CONVERT(varchar, FL_FAULT_LOG_HISTORY.LastActionDate, 108) as HistoryDateTime, '+
                        	CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.DESCRIPTION as FaultStatus, REPAIR.LASTACTIONDATE AS CompletionDate,'+
		CHAR(10) + CHAR(9) + ' FL_FAULT_LOG.DueDate as DueDate,FL_FAULT_LOG.FaultLogID, FL_FAULT.FaultID, CURRSTATUS.DESCRIPTION AS CURRENTSTATUS, ' +
                        	CHAR(10) + CHAR(9) + 'FL_FAULT.[Description] AS FaultDescription,FL_FAULT.NetCost,FL_FAULT.PreInspection, FL_FAULT_LOG.SubmitDate as SubmitDate, ' +
	         	 CHAR(10) + CHAR(9) +  'C_ITEM.ItemID, C_ITEM.[Description] AS ItemDescription, C_NATURE.[Description] as NatureDescription, C_NATURE.ITEMNATUREID, '  +
		CHAR (10) + CHAR(9) + 'FL_FAULT_PREINSPECTIONINFO.faultlogid as PreInspectionInforFaultLogId, Convert(varchar, FL_FAULT_PREINSPECTIONINFO.InspectionDate, 103) as InspectionDate, E.FIRSTNAME + '' '' + ISNULL(E.LASTNAME,'''') AS RAISEDBY, FL_FAULT_BASKET.CancelReason,FL_FAULT_LOG.Notes  '
                       
                    
    -- End building SELECT clause
      
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 

		CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS INNER JOIN FL_FAULT_LOG_HISTORY' +
		CHAR(10) + CHAR(9) + 'ON FL_FAULT_STATUS.FaultStatusId = FL_FAULT_LOG_HISTORY.FaultStatusId' +
		CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_JOURNAL ON' + 
		CHAR(10) + CHAR(9) + 'FL_FAULT_LOG_HISTORY.JournalId = FL_FAULT_JOURNAL.JournalID'+
		CHAR(10) + CHAR(9) + 'INNER JOIN C_ITEM ON' + 
		CHAR(10) + CHAR(9) + 'FL_FAULT_JOURNAL.ItemID = C_ITEM.ItemID' + 
		CHAR(10) + CHAR(9) + 'INNER JOIN C_NATURE ON' + 
		CHAR(10) + CHAR(9) + 'C_ITEM.ItemId = C_NATURE.ItemId'+ 
		CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_LOG ON'+ 
		CHAR(10) + CHAR(9) + 'FL_FAULT_JOURNAL.FaultLogId = FL_FAULT_LOG.FaultLogId' + 
		CHAR(10) + CHAR(9) + 'LEFT JOIN (SELECT EMPLOYEEID,FIRSTNAME,LASTNAME FROM E__EMPLOYEE) E ON E.EMPLOYEEID = FL_FAULT_LOG.USERID '+
		CHAR(10) + CHAR(9) + 'LEFT JOIN (SELECT FAULTSTATUSID,DESCRIPTION FROM FL_FAULT_STATUS) CURRSTATUS ON CURRSTATUS.FAULTSTATUSID = FL_FAULT_LOG.STATUSID ' + 
		--CHAR(10) + CHAR(9) + 'LEFT JOIN FL_CO_APPOINTMENT ON ' + 
		--CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT.JobSheetNumber' +
		CHAR(10) + CHAR(9) + 'LEFT JOIN dbo.FL_FAULT_APPOINTMENT FA ON FA.FaultLogId = dbo.FL_FAULT_LOG.FaultLogID '+
		CHAR(10) + CHAR(9) + 'LEFT JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentId=fa.AppointmentId '+
		CHAR(10) + CHAR(9) + 'LEFT JOIN (SELECT MAX(CJOURNALID) CJOURNALID,FJOURNALID FROM FL_FAULTJOURNAL_TO_CJOURNAL GROUP BY FJOURNALID) FJ_TO_CJ ON FJ_TO_CJ.FJOURNALID=FL_FAULT_JOURNAL.FaultLogId  ' + 
		CHAR(10) + CHAR(9) + 'LEFT JOIN (SELECT LASTACTIONDATE,JOURNALID,ITEMSTATUSID FROM C_REPAIR WHERE ITEMACTIONID=6) REPAIR ON REPAIR.JournalId=FJ_TO_CJ.CJOURNALID AND REPAIR.ITEMSTATUSID = 11  ' + 
		CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ON '+
		CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultId = FL_FAULT.FaultId' + 
		CHAR(10) + CHAR(9) + 'LEFT JOIN FL_FAULT_PREINSPECTIONINFO' + 
		CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultLogId = FL_FAULT_PREINSPECTIONINFO.faultlogid ' + 
		CHAR(10) + CHAR(9) + 'LEFT JOIN FL_FAULT_BASKET' + 
		CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultBasketID = FL_FAULT_BASKET.FaultBasketID '
		
		
                  
                     
    -- End building FROM clause
     
        IF @sortColumn != 'FL_FAULT_STATUS.FaultStatusId' 
	Set @sortColumn = @sortColumn + CHAR(10) + CHAR(9) + @sortOrder + ',FL_FAULT_STATUS.FaultStatusId'


       Set @orderClause = 'Order By' + ' ' + @sortColumn + ' ' + @sortOrder
		--PRINT @orderClause
    -- Begin building WHERE clause
      SET @WhereClause = CHAR(10)+  CHAR(10) + 'WHERE (' +
       		 CHAR(10) + CHAR (9) + ' CONVERT(varchar,  FL_FAULT_LOG_HISTORY.FaultLogID) = '  + CONVERT(varchar, @faultLogID) + ' AND' + ' C_NATURE.DESCRIPTION = ''REACTIVE REPAIR'')'                 
     
    -- End building WHERE clause

      SELECT  @COUNT= COUNT(*)
      FROM 
      FL_FAULT_PREINSPECTIONINFO
     WHERE FL_FAULT_PREINSPECTIONINFO.faultlogid = @faultLogID
    IF @COUNT > 0
BEGIN
SET  @result=-1
END
ELSE
BEGIN 
SET @result=1
END


    PRINT (@SelectClause + @FromClause +@WhereClause+ @orderClause)

    
    EXEC (@SelectClause + @FromClause + @WhereClause + @orderClause)









GO
