USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AreaByLocationId]    Script Date: 28-Feb-17 5:04:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_AreaByLocationId @locationId=1
-- Author:	<Salman Nazir>
-- Create date: <25/10/2012>
-- Description:	<Get Area by location id for Attributes Tree>
-- Webpage: PropertyRecord.aspx
-- =============================================
IF OBJECT_ID('dbo.PDR_AreaByLocationId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PDR_AreaByLocationId AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[PDR_AreaByLocationId](
@locationId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	From PA_AREA
	Where LocationId = @locationId And IsActive=1
	AND AreaName NOT IN ('Services')
	--AND isForSchemeBlock = 0
END
