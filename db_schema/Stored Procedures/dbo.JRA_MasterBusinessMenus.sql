USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetRSLModulesList]    Script Date: 05/23/2017 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
IF OBJECT_ID('dbo.JRA_MasterBusinessMenus') IS NULL 
	EXEC('CREATE PROCEDURE dbo.JRA_MasterBusinessMenus AS SET NOCOUNT ON;') 
GO
  

ALTER PROCEDURE [dbo].[JRA_MasterBusinessMenus](  
@employeeId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
SELECT M.DESCRIPTION,M.MODULEID,
Case When M.DESCRIPTION = 'HR' or  M.DESCRIPTION='My Job' Then 
	 '/' + M.DIRECTORY + '/' + M.PAGE +CONVERT(nvarchar(10), @employeeId)
	 ELSE
	  '/' + M.DIRECTORY + '/' + M.PAGE 
	 END
	 AS THEPATH
FROM AC_MODULES M 
INNER JOIN AC_MODULES_ACCESS on AC_MODULES_ACCESS.ModuleId = M.MODULEID  
INNER JOIN E__EMPLOYEE on E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId  
 Where E__EMPLOYEE.EMPLOYEEID = @employeeId And M.MODULEID != 1 AND M.ACTIVE = 1  
  
 ORDER BY ORDERTEXT ASC  
  
END