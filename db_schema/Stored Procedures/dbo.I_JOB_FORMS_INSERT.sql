SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[I_JOB_FORMS_INSERT]

AS
	SET NOCOUNT OFF;

INSERT INTO [I_JOB_FORMS] ([Name]) VALUES ('Blank Form')
GO
