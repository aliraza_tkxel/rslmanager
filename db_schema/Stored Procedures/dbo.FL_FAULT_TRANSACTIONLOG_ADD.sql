SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE     PROC [dbo].[FL_FAULT_TRANSACTIONLOG_ADD](
/* ===========================================================================
 '   NAME:         FL_FAULT_TRANSACTIONLOG_ADD
 '   DATE CREATED:  3 NOVEMBER 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   Add a transaction record in FL_FAULT_TRANSACTION_LG TABLE
 '
 '   IN:	   @FAULTID INT ,
 '   IN:	   @EMPLOYEEID INT ,
 '   IN:           @FAULTACTION BIT ,
 '   IN:	   @TRANDATE DATETIME,
 '
 '   OUT:           @RESULT    
 '   RETURN:          Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/

@FAULTID INT,
@EMPLOYEEID INT ,
@FAULTACTION BIT ,
@TRANDATE NVARCHAR(20)
)
AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN

INSERT INTO FL_FAULT_TRANSACTION_LOG
                      (EMPLOYEEID, FAULTID, FAULTACTION, TRANDATE)
VALUES     (@EMPLOYEEID,@FAULTID,@FAULTACTION, CONVERT(DateTime,@TRANDATE,103))

-- If insertion fails, goto HANDLE_ERROR block
IF @@ERROR <> 0 GOTO HANDLE_ERROR

COMMIT TRAN	

RETURN

END

/*'=================================*/

HANDLE_ERROR:

   ROLLBACK TRAN

  
RETURN













GO
