
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--<Author : Aqib Javed>
--<Created Date : 25-Nov-2013>
--<Description : This store procedure is keeping record of success login and reset the threshold in AC_LOGINs table  >
--<Web page: rsl_master_site\BHAIntranet\Login.aspx>
-- =============================================
CREATE  PROCEDURE [dbo].[SP_NET_STORE_LOGIN_DETAILS]

@FIRSTNAME varchar (50),
@LASTNAME varchar (50),
@USERNAME varchar (50)

AS

INSERT INTO AC_LOGIN_HISTORY
(FIRSTNAME, LASTNAME, USERNAME)
VALUES (@FIRSTNAME, @LASTNAME, @USERNAME)

--Reset Threshold Count
update AC_LOGINS set THRESHOLD = 0, LASTLOGGEDIN = GETDATE() where [LOGIN] = @USERNAME
GO
