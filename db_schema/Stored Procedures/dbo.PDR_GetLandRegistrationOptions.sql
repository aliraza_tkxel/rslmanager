USE [RSLBHALive]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('dbo.PDR_GetLandRegistrationOptions') IS NULL
 EXEC('CREATE PROCEDURE dbo.PDR_GetLandRegistrationOptions AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PDR_GetLandRegistrationOptions] 
AS
  BEGIN
  

   SELECT	RegistryStatusId, Description
   FROM		PDR_LandRegistryStatus

  END 

GO


