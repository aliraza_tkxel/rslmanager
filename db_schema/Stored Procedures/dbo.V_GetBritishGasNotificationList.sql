USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[V_GetBritishGasNotificationList]    Script Date: 01/21/2016 15:42:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.V_GetBritishGasNotificationList') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.V_GetBritishGasNotificationList AS SET NOCOUNT ON;') 
GO
-- =============================================
/* =================================================================================    
    Page Description: Get British Gas Notification List 
    Author: Noor Muhammad
    Creation Date: May-20-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        May-20-2015     Noor Muhammad      Get British Gas Notification List. Currently for inProgress and Complete list is using the same SP 
												   because the screen data is same except the status. If there is some change in tables or join or some big change then 
												   please consider creating the separate SP for Inporgress and complete list of british gas notification. 
  =================================================================================*/
--DECLARE	@return_value int,
--		@totalCount int
/*
EXEC	[dbo].[V_GetBritishGasNotificationList]
		@searchText = NULL,
		@notificationStatus = N'InProgress',
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = 'NotificationStageId',
		@sortOrder = 'DESC'
*/
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'

--SELECT	'Return Value' = @return_value
-- =============================================
ALTER PROCEDURE [dbo].[V_GetBritishGasNotificationList]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),
		@notificationStatus varchar(20) = 'InProgress',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'NotificationStageId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
		
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        @notificationStatusId int,

        --variables for paging
        @offset int,
		@limit int	
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--=====================Search Criteria===============================		
		IF @notificationStatus = 'InProgress'
		BEGIN
			SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'In Progress'
		END
		ELSE IF @notificationStatus = 'Complete'
		BEGIN
			SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Completed'
		END
		ELSE IF @notificationStatus = 'Logged'
		BEGIN
			SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Logged'
		END
		
		SET @searchCriteria = 'PDR_STATUS.StatusId = '+convert(varchar(10),@notificationStatusId)+''
		
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') + ISNULL(P__PROPERTY.POSTCODE, '''') LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')							 
		ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,ISNULL(B.TenantFwAddress1, '''') + ISNULL('' ''+B.TenantFwAddress2, '''') + ISNULL('' ''+B.TenantFwPostCode, '''') + ISNULL('' ''+B.TenantFwCity, '''') AS FWAddress
		,ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode		
		,CONVERT(nvarchar(50),M.TERMINATIONDATE, 103) as Termination	
		,C__CUSTOMER.CUSTOMERID as CustomerId 
		,P__PROPERTY.PROPERTYID as PropertyId 
		,C_TENANCY.TENANCYID as TenancyId 
		,PDR_STATUS.StatusId as NotificationStatusId
		,V_Stage.StageId as NotificationStageId 
		,PDR_STATUS.Title as NotificationStatusTitle
		,V_Stage.Name as NotificationStageTitle
		,isNotificationSent
		,BritishGasVoidNotificationId as NotificationId
		'
				
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'From V_BritishGasVoidNotification B
		INNER JOIN PDR_STATUS on PDR_STATUS.StatusId = B.StatusId 
		INNER JOIN V_Stage ON B.StageId = V_Stage.StageId
		INNER JOIN P__PROPERTY ON B.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN C__CUSTOMER ON B.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_TENANCY ON B.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		INNER JOIN PDR_MSAT M ON B.PropertyId =  M.PropertyId AND B.CustomerId = M.CustomerId AND B.TenancyId = M.TenancyId
		AND M.MSATTypeId = (Select MSATTypeId from PDR_MSATType where PDR_MSATType.MSATTypeName = ''Void Inspection'')'
							
		--============================Order Clause==========================================		
		IF(@sortColumn = 'NotificationStageId')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'NotificationStageId' 				
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Postcode')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Postcode' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 				
		END
		
		IF(@sortColumn = 'F/WAddress')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FWAddress' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT B.BritishGasVoidNotificationId) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
