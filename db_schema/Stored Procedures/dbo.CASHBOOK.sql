SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE DBO.CASHBOOK
    @pagenum INT = 1, 
    @perpage INT = 50, 
    @STARTDATE SMALLDATETIME,
    @ENDDATE SMALLDATETIME,
    @SHOW INT = 1

AS 
BEGIN 
    SET NOCOUNT ON 
 
    DECLARE 
        @ubound INT, 
        @lbound INT, 
        @pages INT, 
        @rows INT

    DECLARE @TXNDATE SMALLDATETIME, 
	    @TXNTYPE INT,
 	    @TXNID INT,
	    @RECCED INT


    IF @SHOW = 2
    BEGIN

	    SELECT @rows = COUNT(*) FROM CASHBOOK_ALL
	    WHERE TRANSACTIONDATE >= @STARTDATE AND TRANSACTIONDATE <= @ENDDATE AND RECCED = -1
	
	    SET @pages = @rows / @perpage 
	
	    IF @rows % @perpage != 0 SET @pages = @pages + 1 
	    IF @pagenum < 1 SET @pagenum = 1 
	    IF @pagenum > @pages SET @pagenum = @pages 

	    -- GET THE REQUIRED INFORMATION FROM THE PAGING SET
	    SELECT 
	        CurrentPage = @pagenum, 
	        TotalPages = @pages, 
	        TotalRows = @rows 
	 
	    SET @ubound = @perpage * @pagenum  
	    SET @lbound = @ubound - (@perpage - 1) - 1
	 
	    -- this method determines the string values 
	    -- for the first desired row, then sets the 
	    -- rowcount to get it, plus the next n rows 
	 
	    IF (@lbound <= 0) 
	    BEGIN
		SET @TXNDATE = @STARTDATE
		SET @TXNTYPE = 0
		SET @TXNID = 0
		SET @RECCED = 0
	    END
	    ELSE
	    BEGIN
		SET ROWCOUNT @lbound 
		SELECT @TXNDATE = TRANSACTIONDATE, @TXNTYPE = THETYPE, @TXNID = THEID, @RECCED = RECCED FROM CASHBOOK_ALL 
		WHERE TRANSACTIONDATE >= @STARTDATE AND TRANSACTIONDATE <= @ENDDATE AND RECCED = -1
		ORDER BY TRANSACTIONDATE, THETYPE, THEID, RECCED
	    END	
	
	    SET ROWCOUNT @perPage 
	
	    SELECT RECCED, ITEMCOUNT, BOOKDATE, DESCRIPTION, DEBIT, CREDIT FROM CASHBOOK_ALL 
	    WHERE TRANSACTIONDATE >= @STARTDATE AND TRANSACTIONDATE <= @ENDDATE AND RECCED = -1
		 AND (
			(TRANSACTIONDATE > @TXNDATE)
			OR (TRANSACTIONDATE = @TXNDATE AND THETYPE > @TXNTYPE)
			OR (TRANSACTIONDATE = @TXNDATE AND THETYPE = @TXNTYPE AND THEID > @TXNID)
			OR (TRANSACTIONDATE = @TXNDATE AND THETYPE = @TXNTYPE AND THEID = @TXNID AND RECCED > @RECCED)
		     )
	    ORDER BY TRANSACTIONDATE, THETYPE, THEID, RECCED
	 
	    SET ROWCOUNT 0 
    END
    ELSE
    BEGIN
	    SELECT @rows = COUNT(*) FROM CASHBOOK_ALL 
	    WHERE TRANSACTIONDATE >= @STARTDATE AND TRANSACTIONDATE <= @ENDDATE

	    SET @pages = @rows / @perpage 
	
	    IF @rows % @perpage != 0 SET @pages = @pages + 1 
	    IF @pagenum < 1 SET @pagenum = 1 
	    IF @pagenum > @pages SET @pagenum = @pages 

	    -- GET THE REQUIRED INFORMATION FROM THE PAGING SET
	    SELECT 
	        CurrentPage = @pagenum, 
	        TotalPages = @pages, 
	        TotalRows = @rows 
	 
	    SET @ubound = @perpage * @pagenum  
	    SET @lbound = @ubound - (@perpage - 1) - 1
	 
	    -- this method determines the string values 
	    -- for the first desired row, then sets the 
	    -- rowcount to get it, plus the next n rows 
	 
	    IF (@lbound <= 0) 
	    BEGIN
		SET @TXNDATE = @STARTDATE
		SET @TXNTYPE = 0
		SET @TXNID = 0
		SET @RECCED = 0
	    END
	    ELSE
	    BEGIN
		SET ROWCOUNT @lbound 
		SELECT @TXNDATE = TRANSACTIONDATE, @TXNTYPE = THETYPE, @TXNID = THEID, @RECCED = RECCED FROM CASHBOOK_ALL 
		WHERE TRANSACTIONDATE >= @STARTDATE AND TRANSACTIONDATE <= @ENDDATE
		ORDER BY TRANSACTIONDATE, THETYPE, THEID, RECCED
	    END	
	
	    SET ROWCOUNT @perPage 
	
	    SELECT RECCED, ITEMCOUNT, BOOKDATE, DESCRIPTION, DEBIT, CREDIT FROM CASHBOOK_ALL 
	    WHERE TRANSACTIONDATE >= @STARTDATE AND TRANSACTIONDATE <= @ENDDATE
		 AND (
			(TRANSACTIONDATE > @TXNDATE)
			OR (TRANSACTIONDATE = @TXNDATE AND THETYPE > @TXNTYPE)
			OR (TRANSACTIONDATE = @TXNDATE AND THETYPE = @TXNTYPE AND THEID > @TXNID)
			OR (TRANSACTIONDATE = @TXNDATE AND THETYPE = @TXNTYPE AND THEID = @TXNID AND RECCED > @RECCED)
		     )
	    ORDER BY TRANSACTIONDATE, THETYPE, THEID, RECCED
	 
	    SET ROWCOUNT 0 

    END

END
GO
