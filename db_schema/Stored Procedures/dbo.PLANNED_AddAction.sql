
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit

--EXEC	@return_value = [dbo].[PLANNED_AddAction]
--		@statusId = 1,
--		@ranking = 1,
--		@title = N'1',
--		@createdBy = 1,
--		@isSaved = @isSaved OUTPUT
--SELECT	@isSaved as N'@isSaved'
-- Author:		<Noor Muhammad>
-- Create date: <11/04/2013>
-- Description:	<This Stored Proceedure Add Actions on Status Page >
-- Web Page: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_AddAction](
	@statusId int,
	@ranking int,
	@title varchar(1000),
	@createdBy int,
	@isSaved bit out
)
AS
BEGIN
Declare @checkRecord int
Declare @maxRanking int
Declare @actionTableId int
Declare @lastActionId int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		    
    --Find the action that has same ranking as selected by user or as input parameter of stored procedure 
    SELECT @actionTableId = PLANNED_Action.ActionId
    FROM PLANNED_Action
    WHERE Ranking = @ranking AND StatusId = @statusId
    --Select the total record count/ max count
    SELECT @maxRanking = MAX(Ranking) FROM PLANNED_Action WHERE StatusId = @statusId    
    
    BEGIN TRANSACTION
		BEGIN TRY 
		--If some action with status (in input parameters) already have the same rank (as in input parameter)
		if @actionTableId > 0
			BEGIN			
				--First update the rank of action which we found with same rank (as in input parameter)
				UPDATE PLANNED_Action SET Ranking = @maxRanking+1 WHERE ActionId = @actionTableId
				Declare @ActionTitle varchar(500)
				Declare @ActionStatusId int
				Declare @isEditable smallint
				
				--Record this updation in history table
				SELECT @ActionTitle=Title,@ActionStatusId=StatusId,@isEditable=IsEditable FROM PLANNED_Action WHERE ActionId = @actionTableId
				INSERT INTO PLANNED_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				Values(@actionTableId,@ActionStatusId,@ActionTitle,@maxRanking+1,@isEditable,@createdBy,GETDATE(),GETDATE())								
				
				--Now insert the new action 
				INSERT INTO PLANNED_Action (StatusId,Title,Ranking,CreatedBy,IsEditable,CreatedDate ) 
					VALUES (@statusId,@title,@ranking,@createdBy,1,GETDATE())	
				
				--now insert the record in history									
				INSERT INTO PLANNED_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				VALUES (SCOPE_IDENTITY(),@statusId,@title,@ranking,1,@createdBy,GETDATE(),GETDATE())										
			END
		ELSE
		
			BEGIN			
				--Now insert the new action 
				INSERT INTO PLANNED_Action (StatusId,Title,Ranking,CreatedBy,IsEditable,CreatedDate ) 
				VALUES (@statusId,@title,@ranking,@createdBy,1,GETDATE())									
				
				--now insert the record in history					
				INSERT INTO PLANNED_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				VALUES (SCOPE_IDENTITY(),@statusId,@title,@ranking,1,@createdBy,GETDATE(),GETDATE())					
			END			
	END TRY 		
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			   @ErrorSeverity = ERROR_SEVERITY(),
			   @ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END 
	
	EXEC PLANNED_AllActions
END
GO
