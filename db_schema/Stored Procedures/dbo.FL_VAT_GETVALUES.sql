SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_VAT_GETVALUES
/* ===========================================================================
 '   NAME:          FL_VAT_GETVALUES
 '   DATE CREATED:   27th OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Vat values from F_Vat table which will be used in application for various use
 '   IN:           VATID
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@VATID int
AS
	SELECT *
	FROM F_VAT
	WHERE VATID=@VATID
	ORDER BY VATID ASC













GO
