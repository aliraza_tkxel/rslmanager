USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyNROSHAssetTypeMain]    Script Date: 21/03/2019 11:44:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property NROSH AssetType1 for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property NROSH AssetType1 for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyNROSHAssetTypeMain
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetPropertyNROSHAssetTypeMain') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyNROSHAssetTypeMain AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[PDR_GetPropertyNROSHAssetTypeMain]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ATN.Sid,ATN.Description FROM P_ASSETTYPE_NROSH_MAIN ATN where ATN.IsActive=1 ORDER by ATN.Description ASC
	
END
