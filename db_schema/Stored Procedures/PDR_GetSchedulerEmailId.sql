USE [RSLBHALive]
IF EXISTS (SELECT * FROM sys.objects WHERE 
object_id = OBJECT_ID(N'PDR_GetSchedulerEmailId'))
  DROP PROCEDURE PDR_GetSchedulerEmailId

 GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <6/11/2015>
-- Description:	<Get scheduler email address in case of rejected faults by contractor>
-- EXEC PDR_GetSchedulerEmailId 265334
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetSchedulerEmailId] 
	@orderId int
	
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT c.workemail From FL_CONTRACTOR_WORK cw 
INNER JOIN E__EMPLOYEE ec ON ec.Employeeid =cw.assignedby 
INNER JOIN E_Contact c ON c.employeeid =   ec.Employeeid
WHERE purchaseorderid = @orderId
	
END
