
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC FL_FaultAppointmentEngineerInfo
--@tradeId = 1,
--@patchId =18
-- Author:		<Aqib Javed>
-- Create date: <23/01/2013>
-- Description:	<Display the list of available Engineers>
-- Webpage : SchedulingCalendar.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_FaultAppointmentEngineerInfo] 
@tradeId int,
@patchId int
AS

BEGIN
Select DISTINCT E__EMPLOYEE.EMPLOYEEID ,E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME as Name 
FROM dbo.E__EMPLOYEE
INNER JOIN dbo.AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
INNER JOIN dbo.E_JOBDETAILS  ON E__EMPLOYEE.EMPLOYEEID =E_JOBDETAILS.EMPLOYEEID 
INNER JOIN dbo.E_TRADE  ON E__EMPLOYEE.EMPLOYEEID =E_TRADE.EMPID  
WHERE P_INSPECTIONTYPE.[Description]  ='Reactive' 
AND E_JOBDETAILS.ACTIVE =1
AND (E_JOBDETAILS.PATCH =@patchId OR E_JOBDETAILS.PATCH =-1)
AND E_TRADE.TradeId  =@tradeId 

END
GO
