SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_CO_GETVATRATEVALUE 
/* ===========================================================================
 '   NAME:           FL_CO_GETVATRATEVALUE
 '   DATE CREATED:   02 Jan., 2009
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    RSL Work Completion
 '   PURPOSE:        To get value of vaterate from F_VAT 
 
 '   IN:             @vatId

 '
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
	@vatId INT	
  )
AS
		
SELECT VATRATE
FROM F_VAT
WHERE VATID = @vatId












GO
