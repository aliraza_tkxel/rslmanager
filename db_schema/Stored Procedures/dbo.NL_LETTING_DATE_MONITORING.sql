
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--	EXEC NL_LETTING_DATE_MONITORING


CREATE      PROCEDURE [dbo].[NL_LETTING_DATE_MONITORING]

AS
DECLARE @DATE SMALLDATETIME
DECLARE @PROPERTYID VARCHAR(50)
DECLARE @ASSETTYPEID INT
DECLARE @LETTINGDATE SMALLDATETIME
DECLARE @COUNTER INT
DECLARE @LASTFISCALEND SMALLDATETIME

SET @DATE = CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103)  
SET @COUNTER = 0
--SET @DATE = '25 JUL 2005'

-- Added FiscalYearEnd check so that system should not create rebate entries in the last financial year
IF MONTH(@DATE) <=3
	BEGIN
		SELECT @LASTFISCALEND=YEND FROM DBO.F_FISCALYEARS WHERE YEAR(YEND)=YEAR(@DATE) - 1
	END
IF MONTH(@DATE) > 3
	BEGIN
		SELECT @LASTFISCALEND=YEND FROM DBO.F_FISCALYEARS WHERE YEAR(YEND)=YEAR(@DATE)
	END
-- end of comment	


DECLARE 	LETTINGS CURSOR FAST_FORWARD FOR   



  
	-- SELECT ALL LETTING DATES OF LESS THAN OR EQUAL TO TODAY WHICH
	-- HAVE NOT ALREADY BEEN PROCESSED (IGNORIN THOSE WITH NO RENT)
	SELECT	P.PROPERTYID, P.ASSETTYPE, P.LETTINGDATE 
	FROM	P__PROPERTY P 
		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID
		LEFT JOIN P_LETTINGVOIDENTRIES V ON V.PROPERTYID = P.PROPERTYID
		LEFT JOIN C_TENANCY T ON T.PROPERTYID = P.PROPERTYID AND (ENDDATE IS NULL OR ENDDATE >= GETDATE())
	WHERE	P.LETTINGDATE <= @DATE AND V.PROPERTYID IS NULL  AND P.LETTINGDATE IS NOT NULL
		AND P.LETTINGDATE > @LASTFISCALEND
		AND T.TENANCYID IS NULL -- DONR LOOK AT ANYTHING LINKED TO A OPEN OR PENDING TENANCY
		
	
	OPEN LETTINGS
	FETCH NEXT FROM LETTINGS
	INTO @PROPERTYID, @ASSETTYPEID, @LETTINGDATE
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- UPDATE PROPERTY STATUS
		UPDATE P__PROPERTY SET STATUS = 1, SUBSTATUS = NULL WHERE PROPERTYID = @PROPERTYID
		
		/* COMMENTED OUT BY PAUL PATTERSON 12 JUL 2005 DO NOT REINSTATE ***************
		-- UPDATE CURRENT 'AVAILABLE' ROW IN KPI STATUS TRACKER
		UPDATE KPI_STATUSTRACKER SET TENDDATE = @DATE 
			WHERE STATUS = 4 AND SUBSTATUS = 1 AND PROPERTYID = @PROPERTYID AND TENDDATE IS NULL
		-- CREATE NEW TENANCY ROW IN KPI STATUS TRACKER
		INSERT INTO KPI_STATUSTRACKER (PROPERTYID, ASSETTYPE, STATUS, TSTARTDATE)
		VALUES (@PROPERTYID, @ASSETTYPEID, 1, @DATE)
		*******************************************************************************/		

		EXEC NL_LETTINGSDATE_VOIDS @PROPERTYID, @LETTINGDATE
		SET @COUNTER = @COUNTER  + 1
	FETCH NEXT FROM LETTINGS
	INTO @PROPERTYID, @ASSETTYPEID, @LETTINGDATE
	END

-- STORE ACTIVITY
INSERT INTO P_LETTINGJOBHISTORY (AMOUNT) VALUES (@COUNTER)

CLOSE 		LETTINGS
DEALLOCATE 	LETTINGS







GO
