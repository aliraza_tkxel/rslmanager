SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
-- Author:  Abdullah Saeed  
-- Create date: July 23,2014  
-- Description: Return contents for first page of record of inspection  
-- EXEC PROPERTYINSPECTION_InspectionRecordMainPage 'B070050004'  
-- EXEC PROPERTYINSPECTION_InspectionRecordMainPage 'E140150009'  
-- =============================================  
CREATE PROCEDURE [dbo].[PROPERTYINSPECTION_InspectionRecordMainPage]   
(  
@propertyID varchar(100)  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
   
 SELECT Top 1  
   
 ISNULL(HOUSENUMBER,'')+' '+ISNull(ADDRESS1,'')+', '+ISNull(ADDRESS2,'')+' '+ISNull(TOWNCITY,'')+' '+ISNull(COUNTY,'')+' '+ISNULL(POSTCODE,'') AS Property,   
 ISNULL(convert(nvarchar, C_TENANCY.TENANCYID),'N/A') as TenancyRef,  
 @propertyID as PropertyRef,  
 'Stock Condition' as InspectionType,  
 CAST(GETDATE() AS smalldatetime) As 'Date/Time',   
 PS_Survey.SurveyDate As Completed,  
 (E__EMPLOYEE.FIRSTNAME)+' '+ISNULL(E__EMPLOYEE.MIDDLENAME,'')+' '+(E__EMPLOYEE.LASTNAME) AS SurveyorName,  
 C__CUSTOMER.CUSTOMERID as CustomerRef,  
 PA_PROPERTY_ITEM_IMAGES.ImagePath,'../../../PropertyImages/'+@propertyID+'/Images/'+PA_PROPERTY_ITEM_IMAGES.ImageName as ImageName
  
 FROM  
  
 P__PROPERTY  
 LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID   
 LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID   
 LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID  
 LEFT JOIN PS_SURVEY on PS_SURVEY.PROPERTYID=P__PROPERTY.PROPERTYID  
 LEFT JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID=PS_Survey.CompletedBy  
 Left Join PA_PROPERTY_ITEM_IMAGES on P__PROPERTY.PropertyPicId =PA_PROPERTY_ITEM_IMAGES.[SID]     
  
 WHERE  
   
 P__Property.PROPERTYID=@propertyID  
 --AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL)  
   
 --===========================================================================================================================================================================  
   
 SELECT   
   
 distinct(RISKDESCRIPTION) as Asbestos  
 FROM P_ASBESTOS  
 INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID=P_ASBESTOS.ASBESTOSID  
 WHERE P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID=@propertyID  
  
 --=============================================================================================================================================================================  
  
 SELECT max(SurveyDate) as PreviousSurvey from PS_Survey where PROPERTYID=@propertyID  
   
 --==============================================================================================================================================================================  
    
 SELECT  
 RoomName AS Accomodation, ISNULL(RoomWidth,'0') AS Width, ISNull(RoomLength,'0') AS Length,ISNull((RoomWidth*RoomLength),'0') AS 'Sq m'  
 FROM  
 PA_PropertyDimension where PA_PropertyDimension.PropertyId=@propertyID  
   
 --===============================================================================================================================================================================  
  
END  
GO
