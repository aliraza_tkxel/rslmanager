USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_SchedulePlannedWorkAppointment]    Script Date: 12/11/2013 16:37:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---======================================================================
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,12/2/2013>
-- Description:	<Description,Update/Insert PLANNED_JOURNAL,PLANNED_APPOINTMENTS
--Last modified Date:12/2/2013
---======================================================================


IF OBJECT_ID('dbo.PLANNED_SchedulePlannedWorkAppointment') IS NULL 
	EXEC('CREATE PROCEDURE dbo.PLANNED_SchedulePlannedWorkAppointment AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[PLANNED_SchedulePlannedWorkAppointment]
	-- Add the parameters for the stored procedure here
@PropertyId varchar(20)
,@userId int
,@journalId int
,@componentId int
,@customerNotes varchar(1000)
,@appointmentNotes varchar(1000)
,@tenancyId int
,@startDate date
,@endDate date
,@startTime varchar(10)
,@endTime varchar(10)
,@operativeId int
,@duration float
,@tradeId int
,@compTradeId int
--,@isPending int
,@isSaved int = 0 out
,@appointmentIdOut int = -1 out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
         
	DECLARE 
	@AppointmentTypeId int
	,@JournalHistoryId int
	,@AppointmentId int
	,@AppointmentHistoryId int

	SELECT  @AppointmentTypeId = Planned_Appointment_TypeId FROM Planned_Appointment_Type WHERE Planned_Appointment_Type = 'Planned' 
	SELECT  @JournalHistoryId = MAX(JOURNALHISTORYID) FROM PLANNED_JOURNAL_HISTORY WHERE JOURNALID =@journalId


	BEGIN TRANSACTION;
	BEGIN TRY

		
		-- ====================================================================================================
		--										INSERTION (PLANNED_JOURNAL_HISTORY)
		-- ====================================================================================================
		-- =============================================
		-- get status history id of "To Be Arranged"
		-- =============================================
		--Declare @statusHistoryId int
		--SELECT @statusHistoryId= MAX(StatusHistoryId) FROM PLANNED_StatusHistory WHERE StatusId =@ArrangedId

		--INSERT PLANNED_JOURNAL_HISTORY
		--([JOURNALID]
		--,[PROPERTYID]
		--,[COMPONENTID]
		--,[STATUSID]
		--,[ACTIONID]
		--,[CREATIONDATE]
		--,[CREATEDBY]
		--,[NOTES]
		--,[ISLETTERATTACHED]
		--,[StatusHistoryId]
		--,[ActionHistoryId]
		--,[IsDocumentAttached]) VALUES
		--(@journalId,@PropertyId,@componentId,@ArrangedId,null,GETDATE(),@userId,NULL,0,@statusHistoryId,NULL,0)

		--SELECT @JournalHistoryId = SCOPE_IDENTITY()
		--PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)


		-- ====================================================================================================
		--										INSERTION (PLANNED_APPOINTMENTS)
		-- ====================================================================================================


		INSERT INTO PLANNED_APPOINTMENTS
		([TENANCYID]
		,[JournalId]
		,[JOURNALHISTORYID]
		,[APPOINTMENTDATE]
		,[APPOINTMENTENDDATE]
		,[APPOINTMENTSTARTTIME]
		,[APPOINTMENTENDTIME]
		,[ASSIGNEDTO]
		,[CREATEDBY]
		,[LOGGEDDATE]
		,[APPOINTMENTNOTES]
		,[CUSTOMERNOTES]
		,[ISPENDING]
		,[JOURNALSUBSTATUS]
		,[APPOINTMENTALERT]
		,[APPOINTMENTCALENDER]
		,[SURVEYOURSTATUS]
		,[APPOINTMENTSTATUS]
		,[SURVEYTYPE]
		,[COMPTRADEID]
		,[isMiscAppointment]
		,[DURATION]
		,[Planned_Appointment_TypeId])
		VALUES
		(@tenancyId
		,@journalId
		,@JournalHistoryId
		,@startDate
		,@endDate
		,@startTime
		,@endTime
		,@operativeId
		,@userId
		,GETDATE()
		,@appointmentNotes
		,@customerNotes
		,1
		,NULL
		,NULL
		,NULL
		,NULL
		,'NotStarted'
		,NULL
		,@compTradeId
		,0
		,@duration
		,@AppointmentTypeId)

		SELECT @AppointmentId = SCOPE_IDENTITY()
		PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)        

		-- =============================================
		--insert into planned_appointments_History using the following trigger 
		--PLANNED_AFTER_INSERT_PLANNED_APPAOINTMENTS
		-- =============================================
      
      
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isSaved = 0		
		END
		
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		 @ErrorSeverity, -- Severity.
		 @ErrorState -- State.
		 );
	END CATCH;

	IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
	SET @isSaved = 1
	END


SET @appointmentIdOut = @AppointmentId


END