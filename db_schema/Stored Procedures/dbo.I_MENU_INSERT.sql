SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_MENU_INSERT]
(
	@PositionID int,
	@MenuTitle varchar(50),
	@ParentID int,
	@Active bit,
	@Depth int,
	@Rank int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [I_MENU] ([PositionID], [MenuTitle], [ParentID], [Active], [Depth], [Rank]) VALUES (@PositionID, @MenuTitle, @ParentID, @Active, @Depth, @Rank);
	
SELECT MenuID, PositionID, MenuTitle, ParentID, Active, Depth, Rank FROM I_MENU WHERE (MenuID = SCOPE_IDENTITY())
GO
