exec TO_CUSTOMER_GETRENTSTATEMENT @TENANCYID=12696,@PAYMENTSTARTDATE='20180101',@PAYMENTENDDATE='20180824'




USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetCustomerInformation]    Script Date: 25/08/2018 00:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter PROCEDURE [dbo].TO_F_RENTJOURNAL_CALCULATEACCOUNTBALANCE
			@tenancyId	int
AS
BEGIN

SELECT top 1 customer.TENANCYID, 
			   CAST(customer.RentBalance AS DECIMAL(20,2)) rentBalance,
			   CAST(customer.EstimatedHBDue AS DECIMAL(20,2)) EstimatedHBDue,
			   CAST((ISNULL(customer.RentBalance, 0.0) - ISNULL(customer.EstimatedHBDue, 0.0)) AS DECIMAL(20,2)) as OwedToBHA,
			   (ISNULL(customerhistory.RentBalance, 0.0) - ISNULL(customerhistory.EstimatedHBDue, 0.0)) as LastMonthNetArrears,
			   ISNULL(Convert(varchar(100),(customer.LastPaymentDate), 103), '') AS LastPaymentDate, 
			   ISNULL((customer.LastPayment), 0.0) AS LastPayment,
			   P_FINANCIAL.Totalrent as rentAmount,
			   customer.CustomerId,
			   customer.CustomerAddress,

				(SELECT Count(DISTINCT AM_Customer_Rent_Parameters.CustomerId)
						FROM AM_Customer_Rent_Parameters
							INNER JOIN C_CUSTOMERTENANCY ON  C_CUSTOMERTENANCY.CustomerId=AM_Customer_Rent_Parameters.CustomerId
								AND C_CUSTOMERTENANCY.TENANCYID=AM_Customer_Rent_Parameters.TenancyId
						WHERE	AM_Customer_Rent_Parameters.TenancyId =@tenancyId AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
						) as JointTenancyCount,

				P_SCHEME.SCHEMENAME as suburb,P__PROPERTY.HOUSENUMBER,P__PROPERTY.ADDRESS1,P__PROPERTY.ADDRESS2,
				P__PROPERTY.TOWNCITY,P__PROPERTY.POSTCODE,P__PROPERTY.COUNTY
FROM                   AM_Customer_Rent_Parameters customer INNER JOIN
                      C_TENANCY ON C_TENANCY.TENANCYID = customer.TENANCYID INNER JOIN                      
                      P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID 
					  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
					  INNER JOIN P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID LEFT JOIN 
					  AM_Customer_Rent_Parameters_History customerhistory ON customerhistory.customerid=customer.customerid and customerhistory.TenancyId=customer.TenancyId
WHERE customer.TenancyId = @tenancyId

 

END

--exec [PDR_GetCustomerInformation] 972014
