USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidInspectionsToBeArranged]    Script Date: 04/15/2016 14:47:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get First Inspections To Be Arranged List 
    Author: Ali Raza
    Creation Date: May-15-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========			===========================
    v1.0        May-15-2015      Ali Raza			Get First Inspections To Be Arranged List 
	v2.0		May-25-20150	Noor Muhammad		Added property id, customerid, tenancyid in select statement
	v3.0		Nov-10-2015		Raja Aneeq			Add a patch filter 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetVoidInspectionsToBeArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
IF OBJECT_ID('dbo.[V_GetVoidInspectionsToBeArranged]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[V_GetVoidInspectionsToBeArranged] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[V_GetVoidInspectionsToBeArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		@patch int = 0,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        @filterCriteria varchar(200)='',
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Inspection'
		SELECT  @ToBeArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To be Arranged'
		--=====================Search Criteria===============================
		SET @searchCriteria = '1=1'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When T.TITLE=6 then '''' Else  ISNULL(T.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(T.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(T.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		--=================================	Filter Criteria================================
		IF(@patch > 0 )
		BEGIN
		SET @filterCriteria = @filterCriteria + 'AND P.PATCH = ' + CONVERT (VARCHAR,@patch) 
		END

		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
		 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(T.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address
		,	P.TOWNCITY,P.COUNTY,	ISNULL(P.POSTCODE, '''') AS Postcode,T.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When T.TITLE=6 then '''' Else  ISNULL(T.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+T.FIRSTNAME,''''))+'' ''+ CONVERT(VARCHAR, ISNULL(T.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),T.TerminationDate, 103) as Termination	
		,T.Relet as Relet		
		,P.PropertyId as PropertyId
		,T.CUSTOMERID  as CustomerId
		,T.TENANCYID as TenancyId
		,P.PATCH as PATCH
		, T.TerminationDate	'

		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM P__PROPERTY P

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P.PropertyID
AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) 
 GROUP BY PROPERTYID) as CJournal
CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination
Cross Apply (SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
		Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
		ELSE Convert(Varchar(50), M.ReletDate,103)END as ReletDate,PDRJ.JOURNALID,
		C.TITLE AS TITLE
		,C.FIRSTNAME AS FIRSTNAME
		,C.LASTNAME AS LASTNAME
		,C.CUSTOMERID AS CUSTOMERID
		,CT.TENANCYID as TENANCYID
		,G_TITLE.[DESCRIPTION] AS [DESCRIPTION]	
		,Convert(Varchar(50),ISNULL(T.RELETDATE,CONVERT(DATETIME, DATEADD(DAY,7,T.TERMINATIONDATE))),103) AS RELET
		FROM P__PROPERTY P
    INNER JOIN C_JOURNAL J ON CJournal.journalID=J.JOURNALID
	INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID	
	INNER JOIN PDR_MSAT M ON P.PROPERTYID=J.PropertyId And M.CustomerId = J.CUSTOMERID  and M.TenancyId = J.TENANCYID AND M.MSATTypeId=5	
	LEFT JOIN G_TITLE ON C.TITLE=G_TITLE.TITLEID
	INNER JOIN PDR_JOURNAL PDRJ ON M.MSATId = PDRJ.MSATID AND PDRJ.STATUSID=1

 ) As  T
		'

		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' T.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'TerminationDate' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'reletDate' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
