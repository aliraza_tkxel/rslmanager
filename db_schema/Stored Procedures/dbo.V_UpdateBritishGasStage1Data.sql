USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasStage1Data]    Script Date: 04/08/2016 17:54:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.V_UpdateBritishGasStage1Data') IS NULL 
 EXEC('CREATE PROCEDURE dbo.V_UpdateBritishGasStage1Data AS SET NOCOUNT ON;')
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasStage1Data]

  Author: Noor Muhammad
  Creation Date: June-16-2015
  Description: Get British Gas Stage 1 Data
  History:  16/06/2015 Noor : Query for update of british gas data
            30/09/2014 Name : Correct the query to insert 2 records in pdr_journal
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_UpdateBritishGasStage1Data]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@customerId int 
,@propertyId varchar(20)  
,@tenancyId int		
,@isGasElectricCheck bit = null
,@occupancyCeaseDate date = null
,@tenantFwAddress1 varchar(50) = null
,@tenantFwAddress2 varchar(50) = null
,@tenantFwCity varchar(50) = null
,@tenantFwPostCode varchar(30) = null
,@britishGasEmail varchar(100) = null
,@isNotificationSent bit=0
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET [CustomerId] = @customerId
      ,[PropertyId] = @propertyId
      ,[TenancyId] =  @tenancyId      
	  ,[IsGasCheck] = @isGasElectricCheck
	  ,[IsElectricCheck] = @isGasElectricCheck      
      ,[TenantFwAddress1] = @tenantFwAddress1
      ,[TenantFwAddress2] = @tenantFwAddress2
      ,[TenantFwCity] = @tenantFwCity
      ,[TenantFwPostCode] = @tenantFwPostCode
      ,[DateOccupancyCease] = @occupancyCeaseDate
      ,[BritishGasEmail] = @britishGasEmail
      ,[StatusId]=(Select STATUSID from PDR_STATUS where TITLE='In Progress')
      ,[StageId] = (SELECT StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 1')  
      ,[isNotificationSent] = @isNotificationSent      
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
 WHERE 
	   PropertyId = @propertyId
	   AND CustomerId = @customerId
	   AND TenancyId = @tenancyId	
	   AND BritishGasVoidNotificationId = @britishGasId   
	
-------------------------------------------------------------------------------------------------------------------------------------
--INSERT RECORD FOR GAS ELECTRIC CHECK APPOINTMENT IN JOURNAL (PDR_JOURNAL) ONLY IF ITS STAGE 1 NOTIFICATION IS GOING TO BE RECORDED
-------------------------------------------------------------------------------------------------------------------------------------	       
	---FIRST OF ALL CHECK EITHER RECORD IS ALREADY INSERTED IN PDR_JOURNAL FOR GAS APPOINTMENT / ELECTRIC APPOINTMENT OR NOT 
	---IF YES THEN WE WOULD NOT INSERT NEW RECORD IN PDR_JOURNAL FOR GAS OR ELECTRIC APPOINTMENT
	DECLARE @gasCheckJournalId int, @electricCheckJournalId int,@terminationDate DATETIME,@reletDate DATETIME

	SELECT @gasCheckJournalId = GasCheckJournalId, @electricCheckJournalId = ElectricCheckJournalId 
	FROM V_BritishGasVoidNotification
	WHERE BritishGasVoidNotificationId = @britishGasId
	Select @terminationDate=TerminationDate,@reletDate=ReletDate from PDR_MSAT INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId= PDR_MSATType.MSATTypeId	
	  WHERE 
	   PropertyId = @propertyId
	   AND CustomerId = @customerId
	   AND TenancyId = @tenancyId	
		AND MSATTypeName = 'Void Inspection'	
	
	IF @gasCheckJournalId IS NULL AND @electricCheckJournalId IS NULL AND @isGasElectricCheck = 1 
	BEGIN
		DECLARE @electricCheckTypeId int, @gasCheckTypeId int, @msatId int, @toBeArrangedStatusId int, @journalId int

		--GET THE VOID ELECTRIC CHECK / GAS ELECTRIC CHECK STATUS ID
		IF @isGasElectricCheck = 1 
			BEGIN 			
				SELECT @gasCheckTypeId = MSATTypeId
				FROM PDR_MSATType
				WHERE MSATTypeName = 'Void Gas Check'

				SELECT @electricCheckTypeId = MSATTypeId
				FROM PDR_MSATType
				WHERE MSATTypeName = 'Void Electric Check'			
			END	
	
		---GET To Be Arranged Status Id
		SELECT @toBeArrangedStatusId = PDR_STATUS.STATUSID
		FROM PDR_STATUS
		WHERE TITLE = 'To be Arranged'
	
		------------------------------FOR GAS APPOINTMENT TO BE ARRANGED ENTRY----------------------------------
		--------------------------------------------------------------------------------------------------------
		---INSERT RECORD IN PDR_MSAT (FOR GAS)
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,IsActive,TerminationDate,ReletDate)
		VALUES( @propertyId, @gasCheckTypeId, @customerId, @tenancyId,1,@terminationDate,@reletDate)
	
		--Get The Recent MsatId
		SET
		@msatId = SCOPE_IDENTITY()

		--INSERT RECORD IN PDR_JOURNAL
		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
		VALUES (@msatId, @toBeArrangedStatusId, GETDATE(), @userId)

		SET @gasCheckJournalId =  SCOPE_IDENTITY()

		------------------------------FOR ELECTRIC APPOINTMENT TO BE ARRANGED ENTRY----------------------------------
		-------------------------------------------------------------------------------------------------------------
		---INSERT RECORD IN PDR_MSAT (FOR ELECTRIC)
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,IsActive,TerminationDate,ReletDate)
		VALUES( @propertyId, @electricCheckTypeId, @customerId, @tenancyId,1,@terminationDate,@reletDate)
	
		--Get The Recent MsatId
		SET
		@msatId = SCOPE_IDENTITY()

		--INSERT RECORD IN PDR_JOURNAL
		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
		VALUES (@msatId, @toBeArrangedStatusId, GETDATE(), @userId)

		SET @electricCheckJournalId =  SCOPE_IDENTITY()

		--------------------------UPDATE GAS CHECK JOURNAL ID & ELECTRIC CHECK JOURNAL ID IN BRITISH GAS VOID NOTIFICATION-----------------
		-----------------------------------------------------------------------------------------------------------------------------------
		--Update British Gas Void Notification Table With Journal Id		
		UPDATE [dbo].[V_BritishGasVoidNotification]
		SET
		 GasCheckJournalId = @gasCheckJournalId 
		,ElectricCheckJournalId = @electricCheckJournalId	
		WHERE BritishGasVoidNotificationId = @britishGasId
	END
	
			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END