USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[C__RentLetter_MultiPrint]    Script Date: 11/01/2019 2:03:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.C__RentLetter_MultiPrint') IS NULL 
	EXEC('CREATE PROCEDURE dbo.C__RentLetter_MultiPrint AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[C__RentLetter_MultiPrint] --'980376' 
	-- Add the parameters for the stored procedure here
	@tenancyIds  VARCHAR(max)='' 
	,@PrintAction INT = 0
AS
BEGIN
	SELECT DISTINCT YEAR(GETDATE()) AS YEARNUMBER,
                FIN.PROPERTYID,
                ISNULL(FIN.RENT, 0) AS RENT,
                ISNULL(FIN.SERVICES, 0) AS SERVICES,
                ISNULL(FIN.COUNCILTAX, 0) AS COUNCILTAX,
                ISNULL(FIN.WATERRATES, 0) AS WATERRATES,
                ISNULL(FIN.INELIGSERV, 0) AS INELIGSERV,
                ISNULL(FIN.SUPPORTEDSERVICES, 0) AS SUPPORTEDSERVICES,
                ISNULL(FIN.GARAGE, 0) AS GARAGE,
                ISNULL(FIN.TOTALRENT, 0) AS TOTALRENT,
                ISNULL(FIN.RENTTYPE, 0) AS RENTTYPE,
                ISNULL(FIN.OLDTOTAL, 0) AS OLDTOTAL,
               -- FIN.RENTTYPEOLD,
                FIN.DATERENTSET,
                FIN.RENTEFFECTIVE,
                ISNULL(FIN.TARGETRENT, 0) AS TARGETRENT,
                ISNULL(FIN.YIELD, 0) AS YIELD,
                ISNULL(FIN.CAPITALVALUE, 0) AS CAPITALVALUE,
                ISNULL(FIN.INSURANCEVALUE, 0) AS INSURANCEVALUE,
                
                C.CUSTOMERID,
                T.TENANCYID,
                GT.DESCRIPTION AS TITLE,
                RIGHT('0' + cast(DAY(DDPAY.PAYMENTDATE)AS varchar), 2) AS PAYMENTDATE,
                C.FIRSTNAME,
                C.LASTNAME,
                A.HOUSENUMBER,
				ISNULL( ISNULL(A.HOUSENUMBER , '') + 
				  ISNULL(', ' + A.ADDRESS1, '')+
				  ISNULL('<br/> ' + A.ADDRESS2, '') 
					 , '') AS [Address1],
                A.ADDRESS2,
                A.ADDRESS3,
                A.TOWNCITY,
                A.POSTCODE,
                A.COUNTY,
                CASE
                    WHEN FIN.DATERENTSET < TR.CUTOFFDATE THEN 1
                    ELSE 0
                END SETRENTTO,
                TR.TARGETRENTID,
                TR.REASSESMENTDATE,
                ISNULL(ADNL.GARAGE_COUNT, 0) AS GARAGE_COUNT,
                ISNULL(TR.OLDGARAGECHARGE, 0) AS OLDGARAGECHARGE,
                ISNULL(TR.NEWGARAGECHARGE, 0) AS NEWGARAGECHARGE,
                ISNULL(TR.RENT, 0) AS T_RENT,
                ISNULL(TR.SERVICES, 0) AS T_SERVICES,
                ISNULL(TR.COUNCILTAX, 0) AS T_COUNCILTAX,
                ISNULL(TR.WATERRATES, 0) AS T_WATERRATES,
                ISNULL(TR.INELIGSERV, 0) AS T_INELIGSERV,
                ISNULL(TR.SUPPORTEDSERVICES, 0) AS T_SUPPORTEDSERVICES,
                
                ISNULL(ADNL.ADDITIONAL_RENT, 0) AS ADDITIONAL_GARAGE,               
                (ISNULL(P.HOUSENUMBER, '') + ' ' + ISNULL(P.ADDRESS1, '') + ' ' + ISNULL(P.ADDRESS2, '') + ', ' + ISNULL(P.TOWNCITY, '') + ', ' + ISNULL(P.POSTCODE, '')) AS P_FULLADDRESS,
                RE.FIRSTNAME +' '+RE.LASTNAME AS incomeOfficer,
                RE.WORKEMAIL,
                RE.WORKMOBILE
FROM C_TENANCY T
INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID
INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID
INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID
AND A.ISDEFAULT = 1
LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID
INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID OUTER Apply
  (SELECT top 1 RE.FIRSTNAME, RE.LASTNAME, EC.WORKEMAIL, EC.WORKMOBILE
   FROM AM_ResourcePatchDevelopment RPD
   INNER JOIN AM_Resource R ON RPD.ResourceId = R.ResourceId
   AND R.IsActive=1
   INNER JOIN E__EMPLOYEE RE ON R.EmployeeId=RE.EMPLOYEEID
   INNER JOIN E_CONTACT EC ON RE.EMPLOYEEID = EC.EMPLOYEEID
   WHERE RPD.SCHEMEID = P.SCHEMEID
     AND RPD.IsActive=1
   ORDER BY ResourcePatchDevelopmentId DESC) AS RE
INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID
INNER JOIN P_TARGETRENT TR ON TR.PROPERTYID = P.PROPERTYID
AND TR.ACTIVE = 1
LEFT JOIN
  (SELECT DDSCHEDULEID,
          PAYMENTDATE,
          TENANCYID,
          REGULARPAYMENT
   FROM F_DDSCHEDULE
   WHERE ISNULL(SUSPEND, 0) NOT IN (1)
     AND ISNULL(TEMPSUSPEND, 0) NOT IN(1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID
LEFT JOIN
  (SELECT COUNT(*) AS GARAGE_COUNT,
          SUM(F.TOTALRENT) AS ADDITIONAL_RENT,
          A.TENANCYID
   FROM C_ADDITIONALASSET A
   INNER JOIN P__PROPERTY P ON P.PROPERTYID = A.PROPERTYID
   INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID
   WHERE A.ENDDATE IS NULL
   GROUP BY A.TENANCYID) ADNL ON ADNL.TENANCYID = T.TENANCYID
WHERE T.TENANCYTYPE IN (1,8,13,4,6,18,15,20,30)
  AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE())
  AND CT.ENDDATE IS NULL
  AND TR.TARGETRENTID=
    (SELECT MAX(TARGETRENTID)
     FROM dbo.P_TARGETRENT
     WHERE dbo.P_TARGETRENT.PROPERTYID=P.PROPERTYID)
  AND CT.TENANCYID IN (Select COLUMN1 from SPLIT_STRING(@tenancyIds,','))




  ---======================INSERTION FOR FIRST PRINT OR REPRENT---==================================


DECLARE @TENANCYID INT, @TARGETRENTID   int;
 
DECLARE cursor_results CURSOR FOR
 Select Distinct  T.TENANCYID,TR.TARGETRENTID  
FROM C_TENANCY T
INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID
INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID
INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID
AND A.ISDEFAULT = 1
LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID
INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID OUTER Apply
  (SELECT top 1 RE.FIRSTNAME, RE.LASTNAME, EC.WORKEMAIL, EC.WORKMOBILE
   FROM AM_ResourcePatchDevelopment RPD
   INNER JOIN AM_Resource R ON RPD.ResourceId = R.ResourceId
   AND R.IsActive=1
   INNER JOIN E__EMPLOYEE RE ON R.EmployeeId=RE.EMPLOYEEID
   INNER JOIN E_CONTACT EC ON RE.EMPLOYEEID = EC.EMPLOYEEID
   WHERE RPD.SCHEMEID = P.SCHEMEID
     AND RPD.IsActive=1
   ORDER BY ResourcePatchDevelopmentId DESC) AS RE
INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID
INNER JOIN P_TARGETRENT TR ON TR.PROPERTYID = P.PROPERTYID
AND TR.ACTIVE = 1
LEFT JOIN
  (SELECT DDSCHEDULEID,
          PAYMENTDATE,
          TENANCYID,
          REGULARPAYMENT
   FROM F_DDSCHEDULE
   WHERE ISNULL(SUSPEND, 0) NOT IN (1)
     AND ISNULL(TEMPSUSPEND, 0) NOT IN(1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID
LEFT JOIN
  (SELECT COUNT(*) AS GARAGE_COUNT,
          SUM(F.TOTALRENT) AS ADDITIONAL_RENT,
          A.TENANCYID
   FROM C_ADDITIONALASSET A
   INNER JOIN P__PROPERTY P ON P.PROPERTYID = A.PROPERTYID
   INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID
   WHERE A.ENDDATE IS NULL
   GROUP BY A.TENANCYID) ADNL ON ADNL.TENANCYID = T.TENANCYID
WHERE T.TENANCYTYPE IN (1,8,13,4,6,18,15,20,30)
  AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE())
  AND CT.ENDDATE IS NULL
  AND TR.TARGETRENTID=
    (SELECT MAX(TARGETRENTID)
     FROM dbo.P_TARGETRENT
     WHERE dbo.P_TARGETRENT.PROPERTYID=P.PROPERTYID)
  AND CT.TENANCYID IN (Select COLUMN1 from SPLIT_STRING(@tenancyIds,','))
;
 
OPEN cursor_results
FETCH NEXT FROM cursor_results into @TENANCYID, @TARGETRENTID
WHILE @@FETCH_STATUS = 0
BEGIN 
 

 Exec C_RENTLETTER_RECORDPRINT @TENANCYID,@TARGETRENTID,'',@PrintAction;
 --PRINT (@TENANCYID)
FETCH NEXT FROM cursor_results into @TENANCYID, @TARGETRENTID
END
 

CLOSE cursor_results;
DEALLOCATE cursor_results;










END
