USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetBlockDetail]    Script Date: 1/10/2017 7:01:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

IF OBJECT_ID('dbo.[PLANNED_GetBlockDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetBlockDetail] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetBlockDetail](
	@blockId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select 
		-1 as TenancyId,		
		'N/A' as TenantName,
		ISNULL(P_BLOCK.BLOCKNAME,'') +' '+ ISNULL(P_BLOCK.ADDRESS1,'') +' '+ ISNULL(P_BLOCK.ADDRESS2,'') +' '+ ISNULL(P_BLOCK.ADDRESS3,'')+ISNULL(', '+P_BLOCK.TOWNCITY ,'') +' '+ISNULL(P_BLOCK.POSTCODE  ,'') as Address,
		ISNULL(P_BLOCK.BLOCKNAME, '') AS HOUSENUMBER,		
		ISNULL(P_BLOCK.ADDRESS1,'') as ADDRESS1,
		ISNULL(P_BLOCK.ADDRESS2,'') as ADDRESS2,
		ISNULL(P_BLOCK.TOWNCITY,'') as TOWNCITY,
		ISNULL(P_BLOCK.COUNTY,'') as COUNTY,
		ISNULL(P_BLOCK.POSTCODE,'') as POSTCODE,
		'N/A' as TenantNameSalutation,
		'N/A' as Mobile,
		'N/A' as Telephone,
		ISNULL(P_SCHEME.SCHEMENAME,'') as SchemeName,
		'N/A' as Email,
		-1 as CustomerId,
		 ISNULL(P_Block.BLOCKNAME, '') + ' ' + ISNULL(P_Block.ADDRESS1, '') + ' ' + ISNULL(P_Block.ADDRESS2, '') + ' ' + ISNULL(P_Block.ADDRESS3, '') AS Block

	From		
		P_BLOCK 
		LEFT JOIN P_SCHEME ON P_SCHEME.SCHEMEID=P_BLOCK.SchemeId

	WHERE P_BLOCK.BLOCKID = @blockId
END
