SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_ViewActivities]
		@caseId		int	

AS
BEGIN
		SELECT     AM_Activity.ActivityId,AM_Activity.Title as ActivityTitle, AM_Activity.Notes, AM_Activity.IsDocumentAttached, convert(varchar(100), AM_Activity.RecordedDate, 103) as RecordedDate, AM_LookupCode.CodeName, 
						   isnull(E__EMPLOYEE.FIRSTNAME,'') +' '+ isnull(E__EMPLOYEE.LASTNAME,'') as RecordedBy, AM_Status.Title AS StatusTitle,AM_Activity.IsLetterAttached,
							AM_Case.CaseId
		FROM         AM_Activity INNER JOIN
							  AM_Case ON AM_Activity.CaseId = AM_Case.CaseId INNER JOIN
							  AM_LookupCode ON AM_Activity.ActivityLookupId = AM_LookupCode.LookupCodeId INNER JOIN
							  AM_Resource ON AM_Activity.RecordedBy = AM_Resource.ResourceId INNER JOIN
							  AM_Status ON AM_Activity.StatusId = AM_Status.StatusId INNER JOIN
							  E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID

		WHERE		AM_Case.CaseId = @caseId
		ORDER BY CONVERT(SMALLDATETIME,RecordedDate) DESC
    
END





GO
