USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectHistoryReport]    Script Date: 06/10/2016 12:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.DF_GetDefectHistoryReport') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetDefectHistoryReport AS SET NOCOUNT ON;') 
GO
/* =================================================================================      
-- Author:           Noor Muhammad  
-- Create date:      11/09/2015  
-- Description:      Get Defect History Report  
-- History:          11/09/2015 Noor : Created the stored procedure  
--                   11/09/2015 Name : Description of Work  
					 14/12/2015 Ali: Update the Join for P_Scheme.
      
Execution Command:  
----------------------------------------------------  
  
DECLARE @return_value int,  
  @totalCount int  
  
EXEC @return_value = [dbo].[DF_GetDefectHistoryReport]  
  @searchText = N'',  
  @schemeId = NULL,  
  @applianceType = 'Traditional Boiler',   
  @year = 2016,  
  @pageSize = 30,  
  @pageNumber = 1,  
  @sortColumn = N'SchemeName',  
  @sortOrder = N'ASC',  
  @getOnlyCount = 0,
  @defectType='boiler',  
  @totalCount = @totalCount OUTPUT  
  
SELECT @totalCount as N'@totalCount'  
  
SELECT 'Return Value' = @return_value  
  
  
----------------------------------------------------  
*/  
  
ALTER PROCEDURE [dbo].[DF_GetDefectHistoryReport](  
    
  @searchText varchar(5000)='',  
  @schemeId INT = -1,  
  @applianceType varchar(500) = 'All',  
  @year INT = -1,  
  @defectType varchar(500) = '-1',
  --Parameters which would help in sorting and paging  
  @pageSize int = 30,  
  @pageNumber int = 1,  
  @sortColumn varchar(50) = 'SchemeName',  
  @sortOrder varchar (5) = 'ASC',  
  @getOnlyCount bit=0,  
  @totalCount int=0 output  
)  
AS  
BEGIN  
DECLARE   
   
  @SelectClause NVARCHAR(MAX),  
  @SelectClauseCount NVARCHAR(MAX),  
        @fromClause   NVARCHAR(MAX),  
        @whereClause  NVARCHAR(MAX),         
		@SelectClauseScheme NVARCHAR(MAX),  
		@SelectClauseSchemeCount NVARCHAR(MAX),  
        @fromClauseScheme   NVARCHAR(MAX),    
        @whereClauseScheme  NVARCHAR(MAX),   
		@SelectClauseBlock NVARCHAR(MAX),
		@SelectClauseBlockCount NVARCHAR(MAX),  
        @fromClauseBlock   NVARCHAR(MAX),   
        @whereClauseBlock  NVARCHAR(MAX),     
        @orderClause  NVARCHAR(MAX),   
        @mainSelectQuery NVARCHAR(MAX),          
        @rowNumberQuery NVARCHAR(MAX),  
        @finalQuery NVARCHAR(MAX),  
        -- used to add in conditions in WhereClause based on search criteria provided  
        @searchCriteria NVARCHAR(MAX),
		@searchCriteriaScheme NVARCHAR(MAX),
		@searchCriteriaBlock NVARCHAR(MAX),
        @subquerySearchCriteria NVARCHAR(MAX),  
		@subquerySearchCriteriaScheme NVARCHAR(MAX),  
		@subquerySearchCriteriaBlock NVARCHAR(MAX),  
        @unionQuery NVARCHAR(100),  
        --variables for paging  
        @offset int,  
  @limit int    
  
  --Paging Formula  
  SET @offset = 1+(@pageNumber-1) * @pageSize  
  SET @limit = (@offset + @pageSize)-1  
    
  
  --===========================================================================  
  -- SEARCH CRITERIA
  --===========================================================================
  
  -- =======================================
  -- TEMP TABLE FOR PROPERTY INSPECTION DATE 
  -- =======================================

	SELECT PROPERTYID, Max(UPDATEDON) AS UPDATEDON
	INTO	#TempPropertyInspection
	FROM	PA_PROPERTY_ATTRIBUTES  
				INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ItemParamID = PA_PROPERTY_ATTRIBUTES.ItemParamId
				INNER JOIN PA_ITEM ON   PA_ITEM_PARAMETER.ITEMID = PA_ITEM.ITEMID
	WHERE		ITEMNAME = 'HEATING' and PROPERTYID is not NULL
	GROUP BY PROPERTYID

	SELECT PA_PROPERTY_ATTRIBUTES.SchemeId, Max(UPDATEDON) AS UPDATEDON
	INTO #TempSchemeInspection
	FROM	PA_PROPERTY_ATTRIBUTES  
				INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ItemParamID = PA_PROPERTY_ATTRIBUTES.ItemParamId
				INNER JOIN PA_ITEM ON   PA_ITEM_PARAMETER.ITEMID = PA_ITEM.ITEMID
	WHERE		ITEMNAME = 'Boiler Room' AND PA_PROPERTY_ATTRIBUTES.SchemeId IS NOT NULL
	GROUP BY PA_PROPERTY_ATTRIBUTES.SchemeId

	SELECT PA_PROPERTY_ATTRIBUTES.BlockId, Max(UPDATEDON) AS UPDATEDON
	INTO #TempBlockInspection
	FROM	PA_PROPERTY_ATTRIBUTES  
				INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ItemParamID = PA_PROPERTY_ATTRIBUTES.ItemParamId
				INNER JOIN PA_ITEM ON   PA_ITEM_PARAMETER.ITEMID = PA_ITEM.ITEMID
	WHERE		ITEMNAME = 'Boiler Room' AND PA_PROPERTY_ATTRIBUTES.BlockId IS NOT NULL
	GROUP BY PA_PROPERTY_ATTRIBUTES.BlockId


  --==================
  -- MAIN QUERY
  --================== 
  
  SET @searchCriteria = ' 1 = 1 '  
  SET @searchCriteriaScheme = ' 1 = 1 '  
  SET @searchCriteriaBlock = ' 1 = 1 '  
    
  IF(@searchText != '' OR @searchText != NULL)  
  BEGIN        
   SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (ISNULL(P__PROPERTY.HouseNumber, '''') + ISNULL('' ''+P__PROPERTY.ADDRESS1, '''')  + ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''  
   SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' )'        
   SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'AND (ISNULL(P_SCHEME.SCHEMENAME, '''') LIKE ''%' + @searchText + '%'')'  
   SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'AND (ISNULL(P_BLOCK.BLOCKNAME, '''') + ISNULL('' ''+P_BLOCK.ADDRESS1, '''')  + ISNULL('', ''+P_BLOCK.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''  
   SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'OR P_BLOCK.POSTCODE LIKE ''%' + @searchText + '%'' )'  
  END  
    
  IF @schemeId != -1  
  BEGIN  
   SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
   SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
   SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
  END  
  
  --==================
  -- SUB QUERY 
  --==================  
  
  SET @subquerySearchCriteria = '	1 = 1  AND P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID IS NOT NULL
									AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
									AND (((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
									'
  SET @subquerySearchCriteriaScheme = '	1 = 1  AND P_PROPERTY_APPLIANCE_DEFECTS.SCHEMEID IS NOT NULL
									AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
									AND (((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
									'
  SET @subquerySearchCriteriaBlock = '	1 = 1  AND P_PROPERTY_APPLIANCE_DEFECTS.BLOCKID IS NOT NULL
									AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
									AND (((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
									'
  
  
  IF @year <> -1  
  BEGIN  
   SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) +'AND DATEPART(yyyy, P_PROPERTY_APPLIANCE_DEFECTS.DefectDate)= ' + CONVERT(varchar(4),@year) 
   SET @subquerySearchCriteriaScheme = @subquerySearchCriteriaScheme + CHAR(10) +'AND DATEPART(yyyy, P_PROPERTY_APPLIANCE_DEFECTS.DefectDate)= ' + CONVERT(varchar(4),@year) 
   SET @subquerySearchCriteriaBlock = @subquerySearchCriteriaBlock + CHAR(10) +'AND DATEPART(yyyy, P_PROPERTY_APPLIANCE_DEFECTS.DefectDate)= ' + CONVERT(varchar(4),@year) 
  END 

 
  
   IF @defectType = 'appliance'
		BEGIN
		
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
				SET @subquerySearchCriteriaScheme = @subquerySearchCriteriaScheme + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
				SET @subquerySearchCriteriaBlock = @subquerySearchCriteriaBlock + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
			END
			ELSE
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 '
				SET @subquerySearchCriteriaScheme = @subquerySearchCriteriaScheme + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 '
				SET @subquerySearchCriteriaBlock = @subquerySearchCriteriaBlock + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 '
			END
		
		END
		ELSE IF @defectType = 'boiler'
		BEGIN
			
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')'
				SET @subquerySearchCriteriaScheme = @subquerySearchCriteriaScheme + CHAR(10) + ' AND LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')'
				SET @subquerySearchCriteriaBlock = @subquerySearchCriteriaBlock + CHAR(10) + ' AND LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')'
			END
			ELSE
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 '
				SET @subquerySearchCriteriaScheme = @subquerySearchCriteriaScheme + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 '
				SET @subquerySearchCriteriaBlock = @subquerySearchCriteriaBlock + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId <> 0 '
			END
			
		END
		ELSE
		BEGIN
				
			IF @applianceType <> 'All' AND @applianceType <> '-2' AND @applianceType <> '-1'
			BEGIN
				SET @subquerySearchCriteria = @subquerySearchCriteria + CHAR(10) + ' AND  ( (LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')) OR
																							(LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%''))
																						   ) '
				SET @subquerySearchCriteriaScheme = @subquerySearchCriteriaScheme + CHAR(10) + ' AND  ( (LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')) OR
																							(LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%''))
																						   ) '
				SET @subquerySearchCriteriaBlock = @subquerySearchCriteriaBlock + CHAR(10) + ' AND  ( (LOWER(PA_PARAMETER_VALUE.valuedetail) LIKE LOWER(''%'+@applianceType+'%'')) OR
																							(LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%''))
																						   ) '
			END
				
		END
		
    
  --============================From Clause============================================  
  SET @fromClause = CHAR(10) +'FROM  (  
SELECT  P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID, COUNT(PropertyDefectId)	NumberOfDefects,  max(P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId) HeatingMappingId
FROM	P_PROPERTY_APPLIANCE_DEFECTS
		LEFT JOIN PA_PARAMETER_VALUE ON P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId = PA_PARAMETER_VALUE.valueId
		LEFT JOIN GS_PROPERTY_APPLIANCE ON GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID = P_PROPERTY_APPLIANCE_DEFECTS.APPLIANCEID
		LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
WHERE	'+ @subquerySearchCriteria +'           
GROUP BY P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID
 ) AS P_PROPERTY_APPLIANCE_DEFECTS  
INNER JOIN P__PROPERTY ON P_PROPERTY_APPLIANCE_DEFECTS.PropertyId = P__PROPERTY.PropertyId
LEFT JOIN P_SCHEME ON P__Property.SchemeId = P_SCHEME.SCHEMEID
LEFT JOIN P_STATUS ON P__PROPERTY.STATUS = P_STATUS.STATUSID
LEFT JOIN P_LGSR ON  P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId = P_LGSR.HeatingMappingId
LEFT JOIN #TempPropertyInspection AS PROPERTYLASTINSPECTION ON P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID = PROPERTYLASTINSPECTION.PROPERTYID

'  
    SET @fromClauseScheme = CHAR(10) +'FROM  (  
SELECT  P_PROPERTY_APPLIANCE_DEFECTS.SchemeId, COUNT(PropertyDefectId)	NumberOfDefects, max(P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId) HeatingMappingId
FROM	P_PROPERTY_APPLIANCE_DEFECTS
		LEFT JOIN PA_PARAMETER_VALUE ON P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId = PA_PARAMETER_VALUE.valueId
		LEFT JOIN GS_PROPERTY_APPLIANCE ON GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID = P_PROPERTY_APPLIANCE_DEFECTS.APPLIANCEID
		LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
WHERE	'+ @subquerySearchCriteriaScheme +'           
GROUP BY P_PROPERTY_APPLIANCE_DEFECTS.SchemeId
 ) AS P_PROPERTY_APPLIANCE_DEFECTS  
INNER JOIN P_SCHEME ON P_SCHEME.SchemeId = P_PROPERTY_APPLIANCE_DEFECTS.SCHEMEID
LEFT JOIN P_LGSR ON  P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId = P_LGSR.HeatingMappingId
LEFT JOIN #TempSchemeInspection AS PROPERTYLASTINSPECTION ON P_PROPERTY_APPLIANCE_DEFECTS.SchemeId = PROPERTYLASTINSPECTION.SchemeId
'  
    SET @fromClauseBlock = CHAR(10) +'FROM  (  
SELECT  P_PROPERTY_APPLIANCE_DEFECTS.BlockId, COUNT(PropertyDefectId)	NumberOfDefects, max(P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId) HeatingMappingId
FROM	P_PROPERTY_APPLIANCE_DEFECTS
		LEFT JOIN PA_PARAMETER_VALUE ON P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId = PA_PARAMETER_VALUE.valueId
		LEFT JOIN GS_PROPERTY_APPLIANCE ON GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID = P_PROPERTY_APPLIANCE_DEFECTS.APPLIANCEID
		LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
WHERE	'+ @subquerySearchCriteriaBlock +'           
GROUP BY P_PROPERTY_APPLIANCE_DEFECTS.BlockId
 ) AS P_PROPERTY_APPLIANCE_DEFECTS  
INNER JOIN P_BLOCK ON P_PROPERTY_APPLIANCE_DEFECTS.BlockId = P_BLOCK.BLOCKID
LEFT JOIN P_SCHEME ON P_BLOCK.SchemeId = P_SCHEME.SCHEMEID
LEFT JOIN P_LGSR ON  P_PROPERTY_APPLIANCE_DEFECTS.HeatingMappingId = P_LGSR.HeatingMappingId
LEFT JOIN #TempBlockInspection AS PROPERTYLASTINSPECTION ON P_PROPERTY_APPLIANCE_DEFECTS.BlockId = PROPERTYLASTINSPECTION.BlockId

'        
  --================================= Where Clause ================================  
    
  SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria   
          
       + CHAR(10) +' AND STATUS IN (  
               SELECT   
                P_STATUS.STATUSID                  
               FROM   
                P_STATUS   
               WHERE   
                P_STATUS.DESCRIPTION = ''Available to rent'' OR P_STATUS.DESCRIPTION = ''Let''  
              ) '  
	SET @whereClauseScheme = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaScheme
          
       + CHAR(10)

	SET @whereClauseBlock = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaBlock  
          
       + CHAR(10) 

	 --=======================Select Clause=============================================  
    SET @SelectClause = '            
      P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''') AS Address,
	  P__PROPERTY.POSTCODE AS PostCode,
	  P__PROPERTY.PropertyId AS PropertyId,
	  P_SCHEME.SCHEMENAME AS SchemeName,
	  '' '' AS BlockName,
	  P_PROPERTY_APPLIANCE_DEFECTS.NumberOfDefects,
	  CONVERT(VARCHAR(10), PROPERTYLASTINSPECTION.UPDATEDON, 103) AS LastInspectedDate,
	  DATEADD(YEAR, 1, P_LGSR.ISSUEDATE) AS CertificateExpiryDate,
	  PROPERTYLASTINSPECTION.UPDATEDON AS LastInspectedSort,
	  ''Property''as RequestType
	  '   
	  SET @SelectClauseCount = 'SELECT ' + @SelectClause  
	  SET @selectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClause  

	  SET @SelectClauseScheme = '            
     '' '' AS Address,
	  '' '' AS PostCode,
	  Convert(varchar, P_SCHEME.SCHEMEID) AS PropertyId,
	  P_SCHEME.SCHEMENAME AS SchemeName,
	  '' '' AS BlockName,
	  P_PROPERTY_APPLIANCE_DEFECTS.NumberOfDefects,
	  CONVERT(VARCHAR(10), PROPERTYLASTINSPECTION.UPDATEDON, 103) AS LastInspectedDate,
	  DATEADD(YEAR, 1, P_LGSR.ISSUEDATE) AS CertificateExpiryDate,
	  PROPERTYLASTINSPECTION.UPDATEDON AS LastInspectedSort,
	  ''Scheme''as RequestType
	  '   
	  SET @SelectClauseSchemeCount = 'SELECT ' + @SelectClauseScheme  
	  SET @SelectClauseScheme = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseScheme  

	  SET @SelectClauseBlock = '            
     '' '' AS Address,
	  P_BLOCK.POSTCODE AS PostCode,
	  Convert(varchar,P_BLOCK.BLOCKID) AS PropertyId,
	  P_SCHEME.SCHEMENAME AS SchemeName,
	  P_BLOCK.BLOCKNAME + ISNULL('' '' + P_BLOCK.ADDRESS1, '''') + ISNULL('', '' + P_BLOCK.TOWNCITY, '''') AS BlockName,
	  P_PROPERTY_APPLIANCE_DEFECTS.NumberOfDefects,
	  CONVERT(VARCHAR(10), PROPERTYLASTINSPECTION.UPDATEDON, 103) AS LastInspectedDate,
	  DATEADD(YEAR, 1, P_LGSR.ISSUEDATE) AS CertificateExpiryDate,
	  PROPERTYLASTINSPECTION.UPDATEDON AS LastInspectedSort,
	  ''Block''as RequestType
	  '   
	  SET @SelectClauseBlockCount = 'SELECT ' + @SelectClauseBlock  
	  SET @SelectClauseBlock = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseBlock  
      
	  SET @unionQuery = CHAR(10) + ' union all ' + CHAR(10)

  IF(@getOnlyCount=0)  
  BEGIN  
     
         
   --============================Group Clause==========================================  
   --SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY  
   --          , P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '          
   --============================Order Clause==========================================    
          
       IF @sortColumn = 'LastInspectedDate'
       BEGIN
			SET @sortColumn = 'LastInspectedSort'
       END
          
          
   SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder  
     
   --================================= Where Clause ================================  
     
   --SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria   
     
   --===============================Main Query ====================================  
   Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @unionQuery +
						  @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery + 
						  @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock + 
						  @orderClause   
     
   --=============================== Row Number Query =============================  
   Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row   
         FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'  
     
   --============================== Final Query ===================================  
   Set @finalQuery  =' SELECT *  
        FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result   
        WHERE  
        Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)      
     
   --============================ Exec Final Query =================================  
    print(@finalQuery)  
    EXEC (@finalQuery)  
	END
     
   --========================================================================================  
   -- Begin building Count Query   
     
   Declare @selectCount nvarchar(MAX),   
   @parameterDef NVARCHAR(500)  
     
   SET @parameterDef = '@totalCount int OUTPUT';  
   SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@SelectClauseCount +@fromClause + @whereClause + @unionQuery +
																	  @SelectClauseSchemeCount +@fromClauseScheme + @whereClauseScheme + @unionQuery + 
																	  @SelectClauseBlockCount +@fromClauseBlock + @whereClauseBlock+') as Records ) '
     
   --print @selectCount  
   EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;  

   DROP TABLE #TempPropertyInspection
   DROP TABLE #TempSchemeInspection
   DROP TABLE #TempBlockInspection
   -- End building the Count Query  
   --========================================================================================   
  
END  