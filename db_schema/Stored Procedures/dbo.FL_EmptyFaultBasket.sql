SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 -- =============================================
-- EXEC FL_EmptyFaultBasket
-- Author:		<Ahmed Mehmood>
-- Create date: <01/02/2013>
-- Description:	<Remove faults from basket>
-- Web Page: FaultBasket.aspx
-- =============================================
 
CREATE PROCEDURE [dbo].[FL_EmptyFaultBasket] 
AS
TRUNCATE TABLE FL_TEMP_FAULT
GO
