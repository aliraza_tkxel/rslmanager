USE [RSLBHALive]
GO


IF OBJECT_ID('dbo.[CM_GetContractWorkDetail]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[CM_GetContractWorkDetail] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[CM_GetContractWorkDetail]

		@CMContractorId int=-1
		 
AS
BEGIN
SET NOCOUNT ON
	If(OBJECT_ID('tempdb..#temp') Is Not Null)
	Begin
		Drop Table #Temp
	End
	 Select ROW_NUMBER() OVER(ORDER BY cwd.PurchaseOrderItemId ASC)as [row],CONVERT(NVARCHAR(20),cwd.CycleDate,103) as CycleDate 
	 INTO #Temp
	FROM CM_ServiceItems s
	INNER JOIN CM_ContractorWork cw ON s.ServiceItemId = cw.ServiceItemId  and s.PORef = cw.PurchaseOrderId
	INNER JOIN CM_ContractorWorkDetail cwd ON cw.CMContractorId = cwd.CMContractorId
	Where cw.CMContractorId = @CMContractorId


	DECLARE @sb varchar(MAX),@rows int= 0,@firstColumnDataCounter int= 1,@secondColumnDataCounter int= 2
	 DECLARE @totalData int= (Select Count(*) from #Temp)
	SET @sb = '<table>'

	if (@totalData % 2 = 0)
	   BEGIN
		SET @rows = @totalData / 2;
	END
	else
	BEGIN
	 SET @rows = (@totalData / 2) + 1;
	END
	DECLARE @cnt INT = 1;

	WHILE @cnt <= @rows
	BEGIN
	--for (int i = 0; i < @rows; i++)
	--{
	 DECLARE @rowItem int, @CycleDate nvarchar(20),@rowSecondItem int, @CycleDateSecond nvarchar(20)
		if (@totalData % 2 = 0)
		BEGIN
		--SET  @sb=@sb+'0'
		   DECLARE  @dataSecondColumn int = (@totalData / 2) + @firstColumnDataCounter;
	      
		   Select @rowItem=row,@CycleDate=CycleDate FROM #Temp where row = @cnt
		   Select @rowSecondItem=row,@CycleDateSecond=CycleDate FROM #Temp where row = @dataSecondColumn;
	       
		   SET  @sb=@sb+ '<tr><td>' + convert(varchar(10),@rowItem)+ '</td><td style=''padding-left:7px;''>' + ISNULL(@CycleDate,'') + '</td><td style=''padding-left:7px;''>&nbsp;</td><td style=''padding-left:7px;''> ' + ISNULL(convert(varchar(10),@rowSecondItem),'') + '</td><td style=''padding-left:7px;''>' + ISNULL(@CycleDateSecond,'') + '</td></tr>';
		   SET @firstColumnDataCounter=@firstColumnDataCounter+1;
		END
		else
		BEGIN
			DECLARE @dataSecondColumns int= ((@totalData - 1) / 2) + @secondColumnDataCounter;
			 Select @rowItem=row,@CycleDate=CycleDate FROM #Temp where row = @cnt
		   Select @rowSecondItem=row,@CycleDateSecond=CycleDate FROM #Temp where row = @dataSecondColumns;
		   if( @dataSecondColumns>@totalData)
			   BEGIN
				   SET  @rowSecondItem=null;
				   SET @CycleDateSecond=NULL;
			   END
			IF (@cnt = @rows + 1)
				BEGIN
		       
				 SET  @sb=@sb+ '<tr><td>' + convert(varchar(10),@rowItem)+ '</td><td style=''padding-left:7px;''>' + ISNULL(@CycleDate,'') + '</td><td style=''padding-left:7px;''>&nbsp;</td><td style=''padding-left:7px;''> ' 
		        
				END
			else
				BEGIN         
					SET  @sb=@sb+ '<tr><td>' + ISNULL(convert(varchar(10),@rowItem),'')+ '</td><td style=''padding-left:7px;''>' + ISNULL(@CycleDate,'') + '</td><td style=''padding-left:7px;''>&nbsp;</td><td style=''padding-left:7px;''> ' +ISNULL(convert(varchar(10),@rowSecondItem),'') + '</td><td style=''padding-left:7px;''>' + ISNULL(@CycleDateSecond,'') + '</td></tr>';
				END

		   SET  @secondColumnDataCounter=@secondColumnDataCounter+1;
		END
	SET @cnt = @cnt + 1;
	END;


	SET  @sb=@sb+'</table>'
Select @sb as CycleDates
--Print (@sb)
END