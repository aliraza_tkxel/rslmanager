USE [RSLBHALive]
GO


IF OBJECT_ID('dbo.[CM_GetUnassignedCyclicalServices]') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.[CM_GetUnassignedCyclicalServices] AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[CM_GetUnassignedCyclicalServices]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='', 
		@schemeId int=-1,
		--@blockId int=-1,
		@getOnlyCount bit=0, 
		@offset int = 1,
		@limit  int = 100,		 
		@sortColumn varchar(500) = 'ServiceItemId', 
		@sortOrder varchar (5) = 'DESC',		
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        @filterCriteria varchar(200)='',
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
         
		@ToBeArrangedStatusId  int
		--Paging Formula
		 
		
	 
		SELECT  @ToBeArrangedStatusId =  StatusId FROM CM_Status WHERE TITLE = 'To Be Allocated'
		--=====================Search Criteria===============================
		SET @searchCriteria = '1=1 AND SI.StatusId='+ convert(varchar(10),@ToBeArrangedStatusId)  
		
		IF(@searchText != '' OR @searchText != NULL)
			BEGIN						
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( S.SCHEMENAME LIKE ''%' + @searchText + '%'''			
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.BLOCKNAME LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR I.ItemName LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR B.POSTCODE LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR O.NAME LIKE ''%' + @searchText + '%'''
				SET @searchCriteria = @searchCriteria + CHAR(10) +' OR D.POSTCODE LIKE ''%' + @searchText + '%'')'
				
			END	
		  IF(@schemeId > 0)
		 BEGIN
			 SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  SI.SchemeId = ' + convert(varchar(10),@schemeId)
			 SET @searchCriteria = @searchCriteria + CHAR(10) +' OR  B.SchemeId = ' + convert(varchar(10),@schemeId)  	  	
		 END
		--IF(@blockId > 0)
		-- BEGIN
		--	 SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  SI.BlockId = ' + convert(varchar(10),@blockId)  	
		-- END
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select   top ('+convert(varchar(10),@limit)+')
		 SI.ServiceItemId, ISNULL(S.SCHEMENAME,''-'') AS SchemeName, 
		ISNULL(B.BLOCKNAME,''-'') AS BlockName,I.ItemName As  ServiceRequired,
		cast(sI.Cycle AS NVARCHAR)+'' ''+ CT.CycleType AS CYCLE,convert(varchar(10),SI.ContractCommencement,103) as Commencement,
		ISNULL(convert(varchar(10), CC.CycleCompleted,103),''-'') as RecentCompletion,RC.RemainingCycle,
		CASE WHEN RC.RemainingCycle > 0 THEN convert(VARCHAR(10),nxt.CycleDate,103) else ''-'' end as NextCycle,		 		
		O.NAME as Contractor,Case When po.POSTATUS = 0 then ''-'' else CONVERT(VARCHAR(10), SI.PORef) END as PORef,
		CASE
               WHEN CT.CycleType = ''Week(s)'' THEN SI.Cycle * 7
               WHEN CT.CycleType = ''Month(s)'' THEN SI.Cycle * 30
               WHEN CT.CycleType = ''Year(s)'' THEN SI.Cycle * 52* 7
               ELSE SI.Cycle
           END AS CycleDays
		'

		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM  CM_ServiceItems SI
		LEFT JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID
		LEFT JOIN   P_SCHEME S ON SI.SchemeId=S.SCHEMEID Or B.SchemeId=S.SCHEMEID
		Left JOIN PDR_DEVELOPMENT D on S.DEVELOPMENTID= D.DEVELOPMENTID
		INNER JOIN PDR_CycleType CT ON SI.CycleType=CT.CycleTypeId
		INNER JOIN PDR_CycleType PT ON SI.ContractPeriodType= PT.CycleTypeId
		INNER JOIN PA_ITEM I ON SI.ItemId = I.ItemID
		INNER JOIN S_ORGANISATION O ON SI.ContractorId=O.ORGID
		INNER JOIN CM_Status CS on SI.StatusId= CS.StatusID	
		left JOIN CM_ContractorWork cw ON SI.ServiceItemId = cw.ServiceItemId
		Outer APPLY (Select top(1) cwd.CycleCompleted,cwd.WorkDetailId from CM_ContractorWorkDetail cwd where cwd.CMContractorId = cw.CMContractorId and cwd.CycleCompleted is not null ORDER By cwd.WorkDetailId desc ) as CC
		Outer APPLY (Select Count(cwd.WorkDetailId)as RemainingCycle from CM_ContractorWorkDetail cwd
		left JOIN CM_Status cs on cwd.StatusId = cs.StatusId where cwd.CMContractorId = cw.CMContractorId AND cs.Title not in (''Works Completed'',''Invoice Uploaded'')
		  ) as RC
		  OUTER APPLY (Select cwd.CycleDate from CM_ContractorWorkDetail cwd where cwd.WorkDetailId =CC.WorkDetailId+1 )as nxt
		--left JOIN CM_ContractorWorkDetail cwd ON cw.CMContractorId = cwd.CMContractorId
		outer APPLY (SELECT * from F_PURCHASEORDER where F_PURCHASEORDER.ORDERID = SI.PORef) as po
		'

		--============================Order Clause==========================================
		IF(@sortColumn = 'SchemeName')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' SCHEMENAME' 	
			
		END
		
		IF(@sortColumn = 'BlockName')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'BLOCKNAME' 	
			
		END
		IF(@sortColumn = 'CYCLE')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' CycleDays' 	
			
		END
		
	 
		
		IF(@sortColumn = 'ServiceRequired')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'ServiceRequired' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		 
		IF(@getOnlyCount=0)
		BEGIN			
			EXEC (@finalQuery)
		END
		print(@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
