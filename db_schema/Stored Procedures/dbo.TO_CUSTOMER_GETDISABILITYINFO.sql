SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[TO_CUSTOMER_GETDISABILITYINFO]
	/*	===============================================================
	'   NAME:           TO_Customer_GetCustomerInfo
	'   DATE CREATED:   17 AUG 2008
	'   CREATED BY:    Adnan Mirza
	'   CREATED FOR:    Broadland Housing, Tenants Online
	'   PURPOSE:        To retrieve the Customer Disability information to be displayed on My Details page
	'   IN:             @CustomerId 
	'   OUT: 		    No     
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@CustomerId INT
	)
AS

SELECT C.CUSTOMERID,G.DESCRIPTION AS Disability 
FROM CONVERTCOMMA_TO_MANY_TO_ONE_DISABILITY() DIS
INNER JOIN  C__CUSTOMER C ON C.CUSTOMERID= DIS.CUSTOMERID
INNER JOIN G_DISABILITY G ON G.DISABILITYID=DIS.DISABILITYID
where C.CUSTOMERID=@CustomerId



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
