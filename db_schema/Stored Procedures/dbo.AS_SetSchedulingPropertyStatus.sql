USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SetSchedulingPropertyStatus]    Script Date: 18-Jan-17 2:26:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetApplianceType
-- Author:		<Noor Muhammad>
-- Create date: <02/11/2012>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
IF OBJECT_ID('dbo.[AS_SetSchedulingPropertyStatus]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_SetSchedulingPropertyStatus] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].AS_SetSchedulingPropertyStatus
@journalId INT,
@appointmentType varchar(100),
@status int,
@scheduled int,
@lockedBy int,
@lock int out,
@ScheduleStatus int out

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @appointmentType = 'Property'
	begin
		UPDATE AS_JOURNAL
		SET isLock=@status, isScheduled=@scheduled, LockedBy=@lockedBy, LockTime=GETDATE()
		WHERE JOURNALID = @journalId
		and ISCURRENT=1			
		AND (STATUSID=1 or STATUSID=2 or STATUSID=3 or STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
			

		SELECT @lock = MAX(AS_JOURNAL.isLock), @ScheduleStatus = MAX(AS_JOURNAL.isScheduled)
		FROM AS_JOURNAL
		WHERE JOURNALID = @journalId
		AND AS_JOURNAL.ISCURRENT=1
		AND (STATUSID=1 or STATUSID=2 or STATUSID=3 or STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))  
	end
	else if @appointmentType = 'Scheme'
	begin
		UPDATE AS_JOURNAL
		SET isLock=@status, isScheduled=@scheduled, LockedBy=@lockedBy, LockTime=GETDATE()
		WHERE JOURNALID = @journalId
		and ISCURRENT=1			
		AND (STATUSID=1 or STATUSID=2 or STATUSID=3 or STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
			

		SELECT @lock = MAX(AS_JOURNAL.isLock), @ScheduleStatus = MAX(AS_JOURNAL.isScheduled)
		FROM AS_JOURNAL
		WHERE JOURNALID = @journalId
		AND AS_JOURNAL.ISCURRENT=1
		AND (STATUSID=1 or STATUSID=2 or STATUSID=3 or STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
	end
	else if @appointmentType = 'Block'
	begin
		UPDATE AS_JOURNAL
		SET isLock=@status, isScheduled=@scheduled, LockedBy=@lockedBy, LockTime=GETDATE()
		WHERE JOURNALID = @journalId
		and ISCURRENT=1			
		AND (STATUSID=1 or STATUSID=2 or STATUSID=3 or STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
			

		SELECT @lock = MAX(AS_JOURNAL.isLock), @ScheduleStatus = MAX(AS_JOURNAL.isScheduled)
		FROM AS_JOURNAL
		WHERE JOURNALID = @journalId
		AND AS_JOURNAL.ISCURRENT=1
		AND (STATUSID=1 or STATUSID=2 or STATUSID=3 or STATUSID=4 or STATUSID IN (SELECT StatusId from AS_Status WHERE Title = 'Aborted' OR TITLE LIKE 'Cancelled'))
	end
END
