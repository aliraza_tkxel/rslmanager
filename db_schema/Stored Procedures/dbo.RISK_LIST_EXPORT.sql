SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- AUTHOR:		<ADNAN MIRZA>
-- CREATE DATE: <5 NOV 2010>
-- DESCRIPTION:	<FOR RISK MONITORING REPORT XLS>
-- =============================================
ALTER PROCEDURE [dbo].[RISK_LIST_EXPORT] 
	@CUSTOMERID AS INT = NULL,
	@CUSTOMERNAME AS NVARCHAR (60) = NULL,
	@REVIEWMONTH AS INT=NULL,
	@REVIEWYEAR AS INT=NULL,
	@CATEGORYID AS INT = NULL,
	@SUBCATEGORYID AS INT = NULL,
	@STATUS AS INT = NULL,
	@PATCH AS INT = NULL,
	@SCHEME AS INT = NULL
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @STATUSID INT
	DECLARE @RDATE AS SMALLDATETIME

	SET @STATUSID= NULL
	SET @RDATE = NULL
	
	IF @STATUS=1
		BEGIN
			SET @STATUSID = 13
			SET @RDATE = NULL
		END
	ELSE
		IF @STATUS=3
			BEGIN
				SET @STATUSID = 14
				SET @RDATE = NULL
				
			END
	ELSE
		
		IF @STATUS=2
			BEGIN
				SET @STATUSID = NULL 
				SET @RDATE = (SELECT DATEADD(WK,6,GETDATE()))
			END
		
	

	SELECT CR.STARTDATE,J.CUSTOMERID,CUST.FIRSTNAME + ' ' + CUST.LASTNAME AS [NAME],CUSTADD.HOUSENUMBER + ' ' + CUSTADD.ADDRESS1 AS [ADDRESS],CRISK.CATDESC AS CATEGORY,CRISK.SUBCATDESC AS SUBCATEGORY,CR.REVIEWDATE [REVIEW DUE], CR.NOTES,CASE WHEN CR.ITEMSTATUSID=14 THEN DTIMESTAMP END AS CLOSEDATE
	FROM C_JOURNAL J
	INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID
	INNER JOIN C_ADDRESS CUSTADD ON CUSTADD.CUSTOMERID = J.CUSTOMERID AND CUSTADD.ISDEFAULT=1
	INNER JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID=CUSTADD.CUSTOMERID
	INNER JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = CR.ASSIGNTO
	LEFT JOIN C_CUSTOMERTENANCY T  WITH (NOLOCK) ON T.CUSTOMERID= CUST.CUSTOMERID
	LEFT JOIN C_TENANCY CT  WITH (NOLOCK) ON CT.TENANCYID= T.TENANCYID
	LEFT JOIN P__PROPERTY P  WITH (NOLOCK) ON P.PROPERTYID = CT.PROPERTYID
	LEFT JOIN PDR_DEVELOPMENT DEV ON P.DEVELOPMENTID = DEV.DEVELOPMENTID  
	LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 	
	INNER JOIN 
				(
				SELECT CRISK.RISKHISTORYID,CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC,CRISK.JOURNALID FROM C_CUSTOMER_RISK CRISK
				INNER JOIN C_RISK_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID= CRISK.SUBCATEGORYID
				INNER JOIN C_RISK_CATEGORY CAT ON CAT.CATEGORYID= SUBCAT.CATEGORYID
				WHERE CRISK.RISKHISTORYID = (
											SELECT MAX(RISKHISTORYID) FROM C_CUSTOMER_RISK WHERE JOURNALID=CRISK.JOURNALID
											)  
				) CRISK ON CR.JOURNALID=CRISK.JOURNALID  AND CR.RISKHISTORYID=(SELECT MAX(RISKHISTORYID) FROM C_RISK WHERE JOURNALID=J.JOURNALID)

	WHERE J.ITEMNATUREID=63
	
	AND (DATEPART(MM,CR.REVIEWDATE) = @REVIEWMONTH OR @REVIEWMONTH IS NULL) 
	AND (DATEPART(YY,CR.REVIEWDATE) = @REVIEWYEAR OR @REVIEWYEAR IS NULL) 
	AND (P.SCHEMEID = @SCHEME OR @SCHEME IS NULL) 
	AND (DEV.PATCHID = @PATCH OR @PATCH IS NULL)
	AND CR.RISKHISTORYID=(
								SELECT MAX(RISKHISTORYID) FROM C_CUSTOMER_RISK
								INNER JOIN  C_RISK_SUBCATEGORY ON C_RISK_SUBCATEGORY.SubCategoryId= C_CUSTOMER_RISK.SubCategoryId
								WHERE JOURNALID=J.JOURNALID
								AND (C_RISK_SUBCATEGORY.SUBCATEGORYID=@SUBCATEGORYID OR @SUBCATEGORYID IS NULL)
								AND (C_RISK_SUBCATEGORY.CATEGORYID=@CATEGORYID OR @CATEGORYID IS NULL)
								)
		AND (CR.ITEMSTATUSID = @STATUSID OR @STATUSID IS NULL)
		AND (CR.REVIEWDATE < @RDATE OR @RDATE IS NULL)
		ORDER BY J.CUSTOMERID 

END
GO
