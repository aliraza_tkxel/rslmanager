
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetPagesByParentId @employeeId = 616 , @pageId = 1
-- Author:<Author,,Salman Nazir>
-- Create date: <Create Date,,10/18/2012>
-- Description:	<Description,,Save User's Rights on selected pages>
-- Web Page: Access.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveUserRights](
@employeeId int,
@pageId int
)
AS
BEGIN
declare @checkExist int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Check Page Existance
		Select @checkExist = COUNT(*) FROM AS_User_Pages WHERE EmployeeId = @employeeId AND PageId = @pageId
    -- Insert statements for procedure here
		IF @checkExist < 1
			Begin
				INSERT INTO AS_User_Pages (EmployeeId,PageId,IsActive)
				VALUES (@employeeID,@pageId,1)
			End	
END
GO
