SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE dbo.FL_CO_UPDATE_SELECTED_JOB_SHEET_FAULTS 
/* ===========================================================================
 '   NAME:           FL_CO_UPDATE_SELECTED_JOB_SHEET_FAULTS
 '   DATE CREATED:   4th Jan, 2009
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Rsl Manager Contractor Portal Job Sheet
 '   PURPOSE:        To update fault as selected
 
 '   IN:             @FaultLogId
 '   IN:             @IsSelected
 '   IN:             @IsUpdate 
 '
 '   OUT:            @IsUpdate
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
 (
		@OrgId INT,
		@FaultLogId INT,	
		@IsSelected INT,
		@IsUpdated INT OUTPUT
	)
AS
	
	Declare @FaultStatusID INT
	Declare @JournalID INT
	Declare @ScopeID INT
	
	Begin Transaction 
        
        SET @FaultStatusID = (SELECT FAULTSTATUSID FROM FL_FAULT_STATUS WHERE DESCRIPTION='Appointment To Be Arranged')
                              
		UPDATE FL_FAULT_LOG		
		SET 	
		IsSelected = @IsSelected
		WHERE FaultLogId = @FaultLogId 

		If (@@RowCount = 0)
			BEGIN
				SET @IsUpdated =-1
				ROLLBACK TRAN
				RETURN
			END
		
			
		SET @IsUpdated = @FaultLogId

		UPDATE FL_FAULT_LOG 
		SET
		STATUSID=@FaultStatusID
		WHERE FaultLogId=@FaultLogId
		
		
		UPDATE FL_FAULT_JOURNAL 
		SET
		FaultStatusID=@FaultStatusID, LastActionDate= GETDATE()
		WHERE FaultLogId=@FaultLogId
		
		/*Get Journal ID*/
		SELECT @JournalID=JournalID FROM FL_FAULT_JOURNAL WHERE FaultLogID = @FaultLogId
		
		/* Get Value of ScopeID*/
		SELECT @ScopeID=SCOPEID FROM S_SCOPE WHERE ORGID=@OrgId AND AREAOFWORK= (SELECT AREAOFWORKID FROM S_AREAOFWORK WHERE (DESCRIPTION = 'Reactive Repair'))
		
		
		/*Insertion into Fault Log History Table*/
		INSERT INTO FL_FAULT_LOG_HISTORY(
				  JournalID,
				  FaultStatusID,
				  ItemActionID,
				  LastActionDate, 
				  LastActionUserID, 
				  FaultLogID,
				  ORGID,
				  ScopeID,
				  Title,
				  Notes
				  )
				  VALUES
						 (
				  @JOURNALID,
				  @FaultStatusID,
				  NULL,
				  GETDATE(),
				  NULL,
				  @FaultLogId,
				  @OrgId,
				  @ScopeID,
				  NULL,
				  NULL
				  )
		
	COMMIT TRAN















GO
