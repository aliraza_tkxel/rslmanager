SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Exec TSR_GetUpdateReferralData @employeeId = 165 , @actionId=1, @statusId=3, @refHistoryId=756
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,03/02/2014>
-- Description:	<Description,,get the tenant info for Assign PopUp>
-- WebPage: TenancySupportReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_GetUpdateReferralData](
@employeeId int
,@actionId int
,@statusId int
,@refHistoryId int
)
AS
BEGIN
		
	DECLARE @isTsm int
	SET @isTsm = 0
	
	if(@actionId=17  and @statusId =30 )
	BEGIN	
		-- Action: Referred  Status: Pending
		SELECT	ITEMACTIONID AS id, DESCRIPTION AS value
		FROM	C_ACTION
		WHERE	ITEMACTIONID IN (22,23)		
	END
	
	else if((@actionId=18 or @actionId=21) AND @statusId=14)
	BEGIN
		-- Action: Rejected or Closed  Status: Closed
		SELECT	ITEMACTIONID AS id, DESCRIPTION AS value
		FROM	C_ACTION
		WHERE	ITEMACTIONID IN (22)
	END
	else if((@actionId=19 or @actionId=20) AND @statusId=13)
		BEGIN

	-- Action: Assigned or Note  Status: Open
	
	IF EXISTS(	SELECT	1
				FROM	E__EMPLOYEE 
				INNER JOIN E_JOBROLETEAM ON E__EMPLOYEE.JobRoleTeamId = E_JOBROLETEAM.JobRoleTeamId
				INNER JOIN E_JOBROLE ON E_JOBROLETEAM.JobRoleId = E_JOBROLE.JobRoleId
				WHERE	E__EMPLOYEE.EMPLOYEEID  = @employeeId
				AND  E_JOBROLE.JobeRoleDescription ='Tenancy Support Managers'  ) 
			BEGIN

				SELECT	ITEMACTIONID AS id, DESCRIPTION AS value
				FROM	C_ACTION
				WHERE	ITEMACTIONID IN (20,22,21)
				
				SET @isTsm = 1
			END
				
	ELSE
			BEGIN		
				SELECT	ITEMACTIONID AS id, DESCRIPTION AS value
				FROM	C_ACTION
				WHERE	ITEMACTIONID IN (20,21)								
			END	

	END
	
		-- TENANT INFO
		SELECT	
		(C__CUSTOMER.FIRSTNAME +' '+C__CUSTOMER.LASTNAME)as TenantName
		,ISNULL(C_ADDRESS.HOUSENUMBER,'')+', '+ ISNULL(C_ADDRESS.ADDRESS1,' ') +' '+ISNULL(C_ADDRESS.ADDRESS2,' ')+' '+ISNULL(C_ADDRESS.ADDRESS3,' ')+', '+ISNULL(C_ADDRESS.TOWNCITY,'')+', '+ISNULL(C_ADDRESS.POSTCODE,'' ) as TenantAddress
		FROM	C_REFERRAL 
		INNER JOIN C_JOURNAL ON C_REFERRAL.JOURNALID = C_JOURNAL.JOURNALID
		INNER JOIN C_CUSTOMERTENANCY ON C_JOURNAL.TENANCYID = C_CUSTOMERTENANCY.TENANCYID 
		INNER JOIN C__CUSTOMER on C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_ADDRESS on  C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID
		WHERE C_REFERRAL.REFERRALHISTORYID = @refHistoryId and C_ADDRESS.isdefault = 1
				
		-- ASSIGN TO
		EXEC TSR_AssignTODropDown
	
		SELECT 	@isTsm AS isTsm
		
END
GO
