
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[FL_GET_FAULTS]
/* ===========================================================================
 '   NAME:           FL_GET_FAULTS
 '   DATE CREATED:   14th Oct 2008
 '   CREATED BY:     Noor Muhammad
 '   CREATED FOR:    Tenants Online
 '   PURPOSE:        To get the faults
 '   IN:             @ElementId
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	
	@ElementID INT	
	)	
	
AS 	

SELECT fal.FaultId,fal.Description, ele.ElementName, are.AreaName, loc.LocationName 
FROM FL_FAULT fal, FL_ELEMENT ele, FL_AREA are,FL_LOCATION loc
WHERE fal.ElementId = ele.ElementId
AND ele.AreaId = are.AreaId
AND are.LocationId = loc.LocationId
AND fal.ElementId = @ElementID
AND fal.FaultActive=1
order by fal.[Description] ASC



GO
