SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_C_GENERALCUSTOMERLETTERS_GetRentLetter
/* ===========================================================================
 '   NAME:           TO_C_GENERALCUSTOMERLETTERS_GetRentLetter
 '   DATE CREATED:   06 JULY 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Get the letter against specified GENERAL History ID
 '   IN:             @HistoryID
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@HistoryID INT
	)	
AS
	Select LETTERCONTENT AS LetterContents
	FROM  C_GENERALCUSTOMERLETTERS
	Where GENERALHISTORYID=@HistoryID






GO
