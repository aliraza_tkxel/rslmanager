USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[Df_savedetectors]    Script Date: 11/23/2016 6:00:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- Author:          Ali Raza 
-- Create date:      08/10/2015 
-- Description:      Save Detectors 
-- History:          08/10/2015 AR : Save detectors for property 
-- ============================================= 
IF OBJECT_ID('dbo.[Df_savedetectors]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[Df_savedetectors] AS SET NOCOUNT ON;') 
GO 
ALTER PROCEDURE [dbo].[Df_savedetectors] (@propertyId          NVARCHAR(20), 
                                  @detectorType        NVARCHAR(10), 
                                  @Location            NVARCHAR(400), 
                                  @Manufacturer        NVARCHAR(200), 
                                  @SerialNumber        NVARCHAR(200), 
                                  @PowerSource         INT = NULL, 
                                  @InstalledDate       DATETIME=NULL, 
                                  @InstalledBy         INT = NULL, 
                                  @IsLandlordsDetector BIT = NULL, 
                                  @TestedDate          DATETIME = NULL, 
                                  @BatteryReplaced     DATETIME = NULL, 
                                  @Passed              BIT = NULL, 
                                  @Notes               NVARCHAR(2000),
								  @smokeDetectorType   NVARCHAR(100),
								  @noOfSmokeDetector   NVARCHAR(100), 
                                  @saveStatus          BIT output) 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from 
      -- interfering with SELECT statements. 
      SET nocount ON; 

      BEGIN TRANSACTION 

      BEGIN try 
          DECLARE @DetectorTypeId INT 

          SELECT @DetectorTypeId = detectortypeid 
          FROM   as_detectortype 
          WHERE  as_detectortype.detectortype LIKE '%' + @detectorType + '%' 

          INSERT INTO p_detector 
                      (PropertyId, 
                       DetectorTypeId, 
                       Location, 
                       Manufacturer, 
                       SerialNumber, 
                       PowerSource, 
                       InstalledDate, 
                       InstalledBy, 
                       IsLandlordsDetector, 
                       TestedDate, 
                       BatteryReplaced, 
                       Passed, 
                       Notes,
					   SmokeDetectorType,
					   NumberOfSmokeDetectors) 
          VALUES      (@propertyId, 
                       @DetectorTypeId, 
                       @Location, 
                       @Manufacturer, 
                       @SerialNumber, 
                       @PowerSource, 
                       @InstalledDate, 
                       @InstalledBy, 
                       @IsLandlordsDetector, 
                       @TestedDate, 
                       @BatteryReplaced, 
                       @Passed, 
                       @Notes,
					   @smokeDetectorType,
					   @noOfSmokeDetector) 
      END try 

      BEGIN catch 
          IF @@TRANCOUNT > 0 
            BEGIN 
                ROLLBACK TRANSACTION; 

                SET @saveStatus = 0 
            END 

          DECLARE @ErrorMessage NVARCHAR(4000); 
          DECLARE @ErrorSeverity INT; 
          DECLARE @ErrorState INT; 

          SELECT @ErrorMessage = Error_message(), 
                 @ErrorSeverity = Error_severity(), 
                 @ErrorState = Error_state(); 

          -- Use RAISERROR inside the CATCH block to return  
          -- error information about the original error that  
          -- caused execution to jump to the CATCH block. 
          RAISERROR (@ErrorMessage,-- Message text. 
                     @ErrorSeverity,-- Severity. 
                     @ErrorState -- State. 
          ); 
      END catch; 

      IF @@TRANCOUNT > 0 
        BEGIN 
            COMMIT TRANSACTION; 

            SET @saveStatus = 1 
        END 
  END 
  
  