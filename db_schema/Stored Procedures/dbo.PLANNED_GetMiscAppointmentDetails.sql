USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetMiscAppointmentDetails]    Script Date: 11/18/2016 2:52:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.[PLANNED_GetMiscAppointmentDetails]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetMiscAppointmentDetails] AS SET NOCOUNT ON;') 
GO
ALTER  PROCEDURE [dbo].[PLANNED_GetMiscAppointmentDetails] 
	@journalId int
	,@tradeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT DISTINCT
		ISNULL(PMT.APPOINTMENTNOTES,'') as APPOINTMENTNOTES
		,ISNULL(PMT.CUSTOMERNOTES,'') as CUSTOMERNOTES
		,ISNULL(PMT.LOCATION,'') as LOCATION
		,ISNULL(PMT.ADAPTATION,'') as ADAPTATION
		,PAT.Planned_Appointment_TypeId
		,PAT.Planned_Appointment_Type
	FROM 
		PLANNED_MISC_TRADE PMT 
		INNER JOIN PLANNED_JOURNAL J ON J.JOURNALID = PMT.JOURNALID
		INNER JOIN Planned_Appointment_Type PAT ON PAT.Planned_Appointment_TypeId=J.APPOINTMENTTYPEID
	WHERE 
		PMT.JOURNALID = @journalId AND PMT.TradeId=@tradeId
    
END
