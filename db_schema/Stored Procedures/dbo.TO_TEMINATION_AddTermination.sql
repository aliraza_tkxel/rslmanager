SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.TO_TEMINATION_AddTermination
/* ===========================================================================
 '   NAME:           TO_TEMINATION_AddTermination
 '   DATE CREATED:   08 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add termination record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 termination record in TO_TERMINATION
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '
 '   OUT:            @TerminationId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@MovingOutDate SMALLDATETIME,
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(500),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	@TerminationId INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_TERMINATION(
		MovingOutDate,
		EnquiryLogID
		)
		VALUES(
		@MovingOutDate,
		@EnquiryLogID
		)
		
		SELECT @TerminationId=TerminationID
	FROM TO_TERMINATION
	WHERE TerminationID = @@IDENTITY

				IF @TerminationId<0
					BEGIN
						SET @TerminationId=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @TerminationId=-1
			ROLLBACK TRAN
			END
	
	
	
	
	
	
	
	
	
	




GO
