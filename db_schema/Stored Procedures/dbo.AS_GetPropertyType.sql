SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetPropertyType
-- Author:		<Aqib Javed>
-- Create date: <03/12/2012>
-- Description:	<Get information of property types>
-- Web Page: CertificateExpiry.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPropertyType]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PROPERTYTYPEID    as TypeId, [DESCRIPTION]  as Title
	FROM P_PROPERTYTYPE  
END
GO
