SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC: AS_GetTeams
-- Author:		<Hussain Ali>
-- Create date: <15/10/2012>
-- Description:	<This SP displays all the Teams that are active>
-- Webpage: <View Letter>

-- =============================================

CREATE PROCEDURE [dbo].[AS_GetTeams]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		E_Team.TEAMID, 
		E_Team.TEAMNAME 
	FROM
		E_Team 
	WHERE 
		E_TEAM.ACTIVE = 1
END
GO
