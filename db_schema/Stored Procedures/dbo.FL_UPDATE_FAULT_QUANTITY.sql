SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE PROCEDURE [dbo].[FL_UPDATE_FAULT_QUANTITY]
	/*	===============================================================
	'   NAME:           FL_UPDATE_FAULT_QUANTITY
	'   DATE CREATED:   31st Oct,2008
	'   CREATED BY:     Noor Muhammad
	'   CREATED FOR:    Tenants Online
	'   PURPOSE:        To update the fault quantity
	'   IN:             @TempFaultId 
	'   IN:             @Quantity
	'	OUT:			@TmpFault
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@TempFaultId INT,
		@Quantity INT,		
		@TmpFault INT OUTPUT
	)
AS
	
	BEGIN TRAN
                              
		UPDATE FL_TEMP_FAULT
		SET 	
		Quantity = @Quantity		
		WHERE TempFaultID = @TempFaultId 

		If (@@RowCount = 0)
			BEGIN
				SET @TmpFault =-1
				ROLLBACK TRAN
				RETURN
			END
		
			
		SET @TmpFault = @TempFaultId
	COMMIT TRAN














GO
