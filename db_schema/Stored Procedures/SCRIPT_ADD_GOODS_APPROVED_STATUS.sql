/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	3-10-2014
--  Description:	This script added 'Goods Approved' item in F_POSTATUS
--					
 '==============================================================================*/

IF EXISTS (SELECT 1 FROM [RSLBHALive].[dbo].[F_POSTATUS] WHERE POSTATUSNAME = 'Goods Approved')
BEGIN
  PRINT 'ALREADY EXIST'
END
ELSE
BEGIN
  INSERT INTO [RSLBHALive].[dbo].[F_POSTATUS]([POSTATUSID],[POSTATUSNAME],[POTYPE])
  VALUES	(18,'Goods Approved',NULL)
  PRINT 'SUCCESSFULLY ADDED'
END
