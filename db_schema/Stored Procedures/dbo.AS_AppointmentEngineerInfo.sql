USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AppointmentEngineerInfo]    Script Date: 11/28/2012 02:01:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_AppointmentEngineerInfo
-- Author:		<Aqib Javed>
-- Create date: <20/10/2012>
-- Description: <This Store procedure shall provide the engineerings list that are scheduled for GAS inspection.>
-- Webpage : FuelScheduling.aspx
-- =============================================
IF OBJECT_ID('dbo.[AS_AppointmentEngineerInfo]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_AppointmentEngineerInfo] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_AppointmentEngineerInfo] 
	
AS
BEGIN
	--Select DISTINCT E__EMPLOYEE.EMPLOYEEID ,E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME as Name  from AS_APPOINTMENTS INNER JOIN E__EMPLOYEE ON AS_APPOINTMENTS.ASSIGNEDTO =E__EMPLOYEE.EMPLOYEEID   
	
	Select DISTINCT E.EMPLOYEEID ,E.FIRSTNAME+' '+E.LASTNAME as Name 
FROM dbo.AS_USER U
INNER JOIN dbo.E__EMPLOYEE E ON E.EMPLOYEEID=U.EmployeeId
INNER JOIN dbo.E_JOBDETAILS J ON J.EMPLOYEEID=E.EMPLOYEEID AND (J.ACTIVE=1 OR (J.ACTIVE=0 AND J.ENDDATE BETWEEN J.ENDDATE AND DATEADD(D,7,J.ENDDATE)))
LEFT JOIN AS_APPOINTMENTS AP ON AP.ASSIGNEDTO=E.EMPLOYEEID
INNER JOIN AS_USER_INSPECTIONTYPE UI ON E.EMPLOYEEID = UI.EMPLOYEEID
INNER JOIN P_INSPECTIONTYPE PIT ON UI.InspectionTypeId = PIT.InspectionTypeId
WHERE U.UserTypeID = 3 AND  U.IsActive  = 1 AND PIT.DESCRIPTION = 'Appliance Servicing'
AND J.IsGassafe =1

END
