USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_GetPendingAbsence]    Script Date: 05/05/2016 16:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.E_GetPendingAbsence') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_GetPendingAbsence AS SET NOCOUNT ON;')
GO 
-- =============================================
-- Author:           Saud Ahmed
-- Create date:      17/04/2018
-- Description:      Count of Pending LEAVES with absence type
-- exec  E_GetPendingAbsence 113
-- =============================================

ALTER PROCEDURE  [dbo].[E_GetPendingAbsence]
	@empId INT 
AS
BEGIN

	DECLARE @BHSTART SMALLDATETIME
	DECLARE @BHEND SMALLDATETIME
	
	SELECT	@BHSTART = STARTDATE, @BHEND = ENDDATE
	FROM	EMPLOYEE_ANNUAL_START_END_DATE(@empId)

	SELECT MAX(A.AbsenceHistoryID) AS AbsenceHistoryId, en.DESCRIPTION as Nature
      FROM E_JOURNAL J CROSS APPLY
        (SELECT MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryID
         FROM E_ABSENCE
         WHERE JOURNALID = J.JOURNALID) MA
      INNER JOIN E_ABSENCE A ON MA.MaxAbsenceHistoryID = A.ABSENCEHISTORYID --and 
	  join E_STATUS es on J.CURRENTITEMSTATUSID = es.ITEMSTATUSID
	  join E_NATURE en on J.ITEMNATUREID = en.ITEMNATUREID
       WHERE J.EMPLOYEEID = @empId AND es.DESCRIPTION = 'Pending' AND A.STARTDATE >= @BHSTART -- (A.STARTDATE >= CONVERT(date, getdate()) OR A.RETURNDATE >= CONVERT(date, getdate()))
	  group by en.DESCRIPTION

END
