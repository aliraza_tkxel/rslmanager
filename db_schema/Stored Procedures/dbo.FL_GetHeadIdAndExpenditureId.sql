USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetHeadIdAndExpenditureId]    Script Date: 04/06/2016 12:45:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.FL_GetHeadIdAndExpenditureId') IS NULL 
 EXEC('CREATE PROCEDURE dbo.FL_GetHeadIdAndExpenditureId AS SET NOCOUNT ON;')
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 5/4/2016
-- Description:	fetch HEADID and EXPENDITUREID for assign to contractor
-- EXEC  FL_GetHeadIdAndExpenditureId  'Day To Day Repairs'
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetHeadIdAndExpenditureId]
	@ExpenditureType NVARCHAR(100)
AS
BEGIN 

select  HEADID,F_EXPENDITURE.EXPENDITUREID
from F_EXPENDITURE 
INNER JOIN F_EXPENDITURE_ALLOCATION ON F_EXPENDITURE.EXPENDITUREID = F_EXPENDITURE_ALLOCATION.EXPENDITUREID
where DESCRIPTION = @ExpenditureType
AND FISCALYEAR = (Select MAX(YRange) from F_FISCALYEARS) and ACTIVE=1
   order by HEADID asc

	
END
