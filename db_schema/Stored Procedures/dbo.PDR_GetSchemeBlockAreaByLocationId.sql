USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.PDR_GetSchemeBlockAreaByLocationId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetSchemeBlockAreaByLocationId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO


ALTER PROCEDURE [dbo].[PDR_GetSchemeBlockAreaByLocationId](
@locationId int
)
AS
BEGIN

	

	SELECT AreaID, AreaName
	FROM PA_AREA
	WHERE LocationId=@locationId AND IsActive=1 AND IsForSchemeBlock=0

	UNION ALL

	SELECT ItemID AS AreaID, ItemName AS AreaName
	FROM PA_ITEM
	WHERE AreaID IN (SELECT AreaID 
					FROM PA_AREA
					WHERE IsForSchemeBlock = 1 AND AreaID = @locationId)
	AND ParentItemId IS NULL AND IsActive=1 AND IsForSchemeBlock=1 and ItemName not like 'Electrics'

END