USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[CancelFaultPurchaseOrder]    Script Date: 10/7/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.AS_CancelFuelPurchaseOrder') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_CancelFuelPurchaseOrder AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Rehan Baber
-- Create date: 10/7/2017
-- Description:	Cancels a purchase order on cancelling the respective Assign To Contractor Job Sheet
-- EXEC CancelFaultPurchaseOrder 

-- =============================================
ALTER PROCEDURE [dbo].[AS_CancelFuelPurchaseOrder]
@JournalId int,
@UserId int,
@notes nvarchar(max),
@sendEmailOrderID INT OUTPUT	
AS
BEGIN

	DECLARE @PURCHASEORDERID INT
	DECLARE @StatusID INT
	Declare @FuelNotes as varchar(4000)
	Declare @PROPERTYID as nvarchar(20)

	--Select Purchase Order Id to Cancel

	select top 1 @PURCHASEORDERID=Aw.PurchaseOrder from AS_JOURNAL Aj
	inner join AS_CONTRACTOR_WORK Aw on Aw.JournalId=Aj.JournalID
	where AJ.JOURNALID=@JournalId
	ORDER BY 1 DESC

	select @sendEmailOrderID=isnull(ORDERID,0) from F_PURCHASEORDER
	WHERE ORDERID = @PURCHASEORDERID AND POSTATUS IN (SELECT POSTATUSID 
													  FROM F_POSTATUS
													  WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))

	-- Cancel Purchase Order And Insert in to log

	UPDATE F_PURCHASEORDER 
	SET POSTATUS = (SELECT POSTATUSID
					FROM F_POSTATUS
					WHERE POSTATUSNAME LIKE '%Cancelled%')
	WHERE ORDERID = @PURCHASEORDERID AND POSTATUS IN (SELECT POSTATUSID 
													  FROM F_POSTATUS
													  WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))
	
	
	DECLARE @IDENTIFIER VARCHAR
	SELECT @IDENTIFIER = Replace(Replace(Replace((CONVERT(VARCHAR(10), GETDATE(), 103) + ' '  + convert(VARCHAR(8), GETDATE(), 14)), ':', ''), '/', ''),' ','') + '_' + CAST(@UserId AS VARCHAR)
	
	INSERT INTO F_PURCHASEORDER_LOG
	(IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION, image_url)
	(SELECT @IDENTIFIER, PO.ORDERID, 1, PO.POTYPE, PO.POSTATUS, GETDATE(), @UserId, 'Cancelled', NULL FROM F_PURCHASEORDER PO WHERE ORDERID=@PURCHASEORDERID AND POSTATUS IN (SELECT POSTATUSID 
																																													FROM F_POSTATUS
																																										WHERE POSTATUSNAME IN ('Cancelled')))

	--Cancel Purchase Items of the purchase order	
		
	UPDATE F_PURCHASEITEM 
	SET PISTATUS = (SELECT POSTATUSID
					FROM F_POSTATUS
					WHERE POSTATUSNAME LIKE '%Cancelled%')
	WHERE ORDERID = @PURCHASEORDERID AND PISTATUS IN (SELECT POSTATUSID 
													  FROM F_POSTATUS
													  WHERE POSTATUSNAME IN ('Queued', 'Goods Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved'))
	
	
	--Insert in to Cancelled gas servicing repairs table
	
	SELECT @StatusID = StatusId
	FROM AS_Status
	WHERE Title = 'Cancelled'													
	
	INSERT INTO [AS_CANCELLED] ([JOURNALID], [RecordedOn], [Notes]) VALUES(@JournalId, GETDATE(), @notes) 

	SELECT @PROPERTYID=PROPERTYID, @FuelNotes=NOTES
	FROM AS_JOURNAL
	WHERE JournalId = @JournalId order by 1 desc

	--Update journal to Cancelled

	UPDATE AS_JOURNAL
	SET StatusID = @StatusID
	WHERE AS_JOURNAL.JOURNALID = @JournalId
	
	--Update Journal History

	INSERT INTO AS_JOURNALHISTORY (JournalID, PROPERTYID, STATUSID, ACTIONID,
								   INSPECTIONTYPEID, CREATIONDATE, CREATEDBY,
								   NOTES, ISLETTERATTACHED, StatusHistoryId, 
								   ActionHistoryId, IsDocumentAttached)
	VALUES (@JournalId, @PROPERTYID, @StatusID, NULL, 1, GETDATE(),
			@UserId, @FuelNotes, NULL, NULL, NULL, NULL)	



	select @sendEmailOrderID as sendEmail					
END
