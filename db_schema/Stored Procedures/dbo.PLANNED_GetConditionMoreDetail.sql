USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetConditionMoreDetail]    Script Date: 12/16/2015 17:49:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- EXEC PLANNED_GetConditionMoreDetail 126
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,21/07/2014>
-- Description:	<Description,,Condition More Detail>
-- WebPage: ConditionRating.aspx
-- =============================================

IF OBJECT_ID('dbo.[PLANNED_GetConditionMoreDetail]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[PLANNED_GetConditionMoreDetail] AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[PLANNED_GetConditionMoreDetail]
@conditionWorkId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--===================================================================
	-- Property Detail
	--===================================================================

	Select	
		ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme,
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') as Address,
		ISNULL(P__PROPERTY.TOWNCITY,'N/A') as TownCity,
		P__PROPERTY.PROPERTYID as PropertyId,
		ISNULL(P__PROPERTY.COUNTY,'N/A') as County,
		ISNULL(P__PROPERTY.POSTCODE,'N/A') as Postcode,

		ISNULL(C__CUSTOMER.FIRSTNAME+ ' ' + C__CUSTOMER.LASTNAME ,'N/A') as Customer,		
		ISNULL(C_ADDRESS.MOBILE ,'N/A') as Mobile,
		ISNULL(C_ADDRESS.TEL ,'N/A') as Telephone,
		ISNULL(C_ADDRESS.EMAIL,'N/A') as Email

	From	PLANNED_CONDITIONWORKS 
		INNER JOIN PA_PROPERTY_ATTRIBUTES ON PLANNED_CONDITIONWORKS.AttributeId = PA_PROPERTY_ATTRIBUTES.ATTRIBUTEID 
		INNER JOIN P__PROPERTY ON PA_PROPERTY_ATTRIBUTES.PROPERTYID = P__PROPERTY.PROPERTYID 
		LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
		LEFT JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
										AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					AND C_TENANCY.ENDDATE IS NULL)
		LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
		LEFT JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_ADDRESS.ISDEFAULT = 1  
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
		INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 

	WHERE 
		PLANNED_CONDITIONWORKS.ConditionWorksId =@conditionWorkId

	--===================================================================
	-- Condition Work Detail
	--===================================================================

	SELECT	
		ISNULL(L.LocationName + ' > ' ,'') + A.AreaName [Location]
	    ,COALESCE(PC.COMPONENTNAME,I.ITEMNAME,'N/A') AS Component
	    ,COALESCE(PV.ValueDetail,'N/A') AS Status
	    ,COALESCE(PCW.ModifiedDate,PCW.CreatedDate) AS ConditionUpdated
	    ,ISNULL(E.FIRSTNAME +' ' +E.LASTNAME, 'N/A') AS ConditionUpdatedBy
		,PS_Survey.SurveyDate AS LastSurvey	    
		,PS_Survey.CompletedBy AS LastSurveyedBy
		,CASE 
			WHEN PLANNED_Action.Title = 'Recommended' THEN
				'Awaiting Approval'
			ELSE 
				'N/A'
		END AS Approval
		,'N/A' as Appointment

		,CASE 
			WHEN [PCW].ComponentId IS NULL THEN
				'No'
			ELSE 
				'Yes'
		END AS PlannedComponent

		,ISNULL(CONVERT(VARCHAR,[PC].CYCLE) +' '+ [PC].FREQUENCY,'N/A') as ReplacementCycle
		,ISNULL(CONVERT(VARCHAR,YEAR([PID].DueDate)) ,'N/A') AS ReplacementDue
		,[PCW].WorksRequired AS WorksRequired

	FROM	PLANNED_CONDITIONWORKS [PCW] 
		INNER JOIN	PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID 
		INNER JOIN	PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID
		INNER JOIN	PA_PARAMETER [PARAM] ON [PARAM].ParameterID = IP.ParameterId 
					AND PARAM.ParameterName LIKE '%Condition Rating%' 
					AND ([ATT].PARAMETERVALUE = 'Unsatisfactory' 
					OR [ATT].PARAMETERVALUE = 'Potentially Unsatisfactory')
		INNER JOIN	PA_PARAMETER_VALUE PV ON PV.ValueID = [ATT].VALUEID 
					AND (PV.ValueDetail = 'Unsatisfactory' 
					OR PV.ValueDetail = 'Potentially Unsatisfactory')
		INNER JOIN PA_ITEM [I] ON [I].ItemId = [IP].ItemId 
		INNER JOIN PA_AREA [A] ON [A].AreaID = [I].AreaID
		LEFT JOIN PA_LOCATION [L] ON [L].LocationID = [A].LocationId

		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = [ATT].UPDATEDBY
		LEFT JOIN PA_PROPERTY_ITEM_DATES [PID] ON	[PCW].ComponentId = [PID].PLANNED_COMPONENTID 
													AND [ATT].PROPERTYID = [PID].PROPERTYID 
		LEFT JOIN PLANNED_COMPONENT [PC] ON [PC].COMPONENTID = [PCW].COMPONENTID

		LEFT JOIN (	SELECT	PROPERTYID as PropertyId
							,MAX(SurveyId) as SurveyId
					FROM	PS_SURVEY									
					GROUP BY PROPERTYID) Recent_Property_Survey on Recent_Property_Survey.PropertyId = [ATT].PROPERTYID 

		LEFT JOIN PS_Survey ON Recent_Property_Survey.SurveyId = PS_Survey.SurveyId 
		INNER JOIN PLANNED_Action ON [PCW].ConditionAction = PLANNED_Action.ActionId 
	WHERE 
		[PCW].ConditionWorksId = @conditionWorkId

END
