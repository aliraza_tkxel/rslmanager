USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_UpdateFaultNote]    Script Date: 11/11/2016 5:28:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Altamish Arif
-- Modified By:		<Altamish Arif>
-- Create date: 11/11/2016
-- Modified date: <11/11/2016>
-- Description:	This stored procedure changes fault notes of apoointment
-- Usage: Dashboard
 --Exec [dbo].[FL_UpdateFaultNote]
	--	@appointmentId = 18,
	--	@notes = 1
-- =============================================
IF OBJECT_ID('dbo.[AS_UpdateNotes]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_UpdateNotes] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_UpdateNotes]
	@appointmentId		int,
	@notes				varchar(500),
	@isUpdated int OUTPUT
AS
BEGIN

BEGIN TRANSACTION

	BEGIN TRY
	
	Declare @journalId int

    select @journalId=journalId 
	FROM AS_APPOINTMENTS 
	WHERE AppointmentId=@appointmentId

 	UPDATE AS_JOURNAL
	SET Notes=@notes
	WHERE JOURNALID=@journalId;
	END TRY
	
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isUpdated = 0        
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
					@ErrorSeverity, -- Severity.
					@ErrorState -- State.
				);
	END CATCH;

	IF @@TRANCOUNT > 0
		BEGIN  
			COMMIT TRANSACTION;  
			SET @isUpdated = 1
		END
END



