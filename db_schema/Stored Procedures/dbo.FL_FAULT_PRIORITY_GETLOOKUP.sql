SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO














CREATE PROCEDURE dbo.FL_FAULT_PRIORITY_GETLOOKUP
/* ===========================================================================
 '   NAME:          FL_FAULT_PRIORITY_GETLOOKUP
 '   DATE CREATED:   16 OCT 2008
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get Location FL_FAULT_PRIORITY table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT PriorityID AS id,PriorityName AS val
	FROM FL_FAULT_PRIORITY where Status=1
	ORDER BY PriorityID ASC













GO
