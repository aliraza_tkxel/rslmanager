USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AmendPropertyItemDetail]    Script Date: 12/04/2017 10:31:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================   
-- Author:     
-- Create date:    
-- Description:    
-- =============================================   
IF OBJECT_ID('dbo.AS_AmendPropertyItemDetail') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_AmendPropertyItemDetail AS SET NOCOUNT ON;') 
GO    
ALTER PROCEDURE [dbo].[AS_AmendPropertyItemDetail] 
  -- Add the parameters for the stored procedure here        
  @PropertyId    VARCHAR(100), 
  @ItemId        INT, 
  @UpdatedBy     INT, 
  @HeatingMappingId INT=null,
  @ItemDetail    AS AS_ITEMDETAILS readonly, 
  @ItemDates     AS AS_ITEMDATES readonly, 
  @ConditionWork AS AS_CONDITIONRATINGLIST readonly ,  
  @MSATDetail AS AS_MSATDETAIL readonly,
  @ChildAttributeMappingId int =null     
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from        
      -- interfering with SELECT statements.        
      SET nocount ON; 
-- If Item is Heating then add heating mapping first for new heating.

DECLARE @journalId int, @journalHistoryId int, @HeatingType nvarchar(100), @MainsGasCount int, @AlternativeCount int, @ApplianceInspectionTypeId int , @IsAlterNativeHeating bit, @ServiceTypeId int

IF @HeatingMappingId IS NOT NULL 
BEGIN
  IF @ItemId = (Select PA_ITEM.ItemID from PA_ITEM where PA_ITEM.ItemName='Heating')
	BEGIN
		IF @HeatingMappingId = -1
		BEGIN
		
				SELECT	@ApplianceInspectionTypeId = InspectionTypeID
				FROM	P_INSPECTIONTYPE 
				WHERE	[Description] = 'Appliance Servicing'

				INSERT INTO PA_HeatingMapping(PropertyId,HeatingType,IsActive)
				Values(@PropertyId,(SELECT TOP 1 valueid FROM @ItemDetail),1)
				SET	@HeatingMappingId=Scope_identity();	

				--===============================================================================================
				-- Fetch Heating type description and check Alternative Heating status
				-- If Mains Gas already exists in certain property them same journalid will be utilized
				-- If Alternative Heating already exists in certain property them same journalid will be utilized
				--===============================================================================================
				SELECT	@HeatingType =  ValueDetail, @IsAlterNativeHeating = IsAlterNativeHeating
				FROM	PA_HeatingMapping
						INNER JOIN PA_PARAMETER_VALUE ON PA_HeatingMapping.HeatingType = PA_PARAMETER_VALUE.ValueID
				WHERE   HeatingMappingId = @HeatingMappingId

				SELECT		@MainsGasCount = count(*)
				FROM		AS_journalHeatingMapping
							INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
							INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
							INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
				WHERE		ValueDetail = 'Mains Gas' 
							AND AS_JOURNAL.ISCURRENT = 1 
							AND AS_JOURNAL.PROPERTYID=@PropertyId 


				SELECT		@AlternativeCount = count(*)
				FROM		AS_journalHeatingMapping
							INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
							INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
							INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
				WHERE		IsAlterNativeHeating = 1 
							AND AS_JOURNAL.ISCURRENT = 1 
							AND AS_JOURNAL.PROPERTYID=@PropertyId 

				IF @MainsGasCount > 0 AND @HeatingType = 'Mains Gas'
				BEGIN

					SELECT		@journalId = AS_journalHeatingMapping.JOURNALID, @journalHistoryId = AS_journalHeatingMapping.JournalHistoryId
					FROM		AS_journalHeatingMapping
								INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
								INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
								INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
					WHERE		ValueDetail = 'Mains Gas' 
								AND AS_JOURNAL.ISCURRENT = 1 
								AND AS_JOURNAL.PROPERTYID=@PropertyId 

				END 
				ELSE IF @AlternativeCount > 0 AND @IsAlterNativeHeating = 1
				BEGIN
					
					SELECT		@journalId = AS_journalHeatingMapping.JOURNALID, @journalHistoryId = AS_journalHeatingMapping.JournalHistoryId
					FROM		AS_journalHeatingMapping
								INNER JOIN pa_heatingMapping on as_journalHeatingMapping.HeatingMappingId = pa_heatingMapping.HeatingMappingId
								INNER JOIN PA_PARAMETER_VALUE ON pa_heatingMapping.HeatingType = PA_PARAMETER_VALUE.VALUEID	
								INNER JOIN AS_JOURNAL ON  AS_journalHeatingMapping.JOURNALID = AS_JOURNAL.JOURNALID
					WHERE		IsAlterNativeHeating = 1 
								AND AS_JOURNAL.ISCURRENT = 1 
								AND AS_JOURNAL.PROPERTYID=@PropertyId 

				END
				ELSE
				BEGIN
				

					IF EXISTS( SELECT	1
							   FROM		AS_JOURNAL AJ
										LEFT JOIN AS_JournalHeatingMapping AJHM ON AJ.JournalId = AJHM.JournalId
							   WHERE	ISCURRENT = 1 AND AJ.PROPERTYID=@PropertyId AND AJHM.HeatingJournalMappingId IS NULL)
					BEGIN

						  SELECT	@journalId = AJ.JOURNALID
						  FROM		AS_JOURNAL AJ
									LEFT JOIN AS_JournalHeatingMapping AJHM ON AJ.JournalId = AJHM.JournalId
						  WHERE		ISCURRENT = 1 
									AND AJ.PROPERTYID=@PropertyId 
									AND AJHM.HeatingJournalMappingId IS NULL

						  UPDATE	AS_JOURNAL
						  SET		ISCURRENT = 0
						  WHERE	    JOURNALID = @journalId

					END

					SELECT	@ApplianceInspectionTypeId = InspectionTypeID
					FROM	P_INSPECTIONTYPE 
					WHERE	[Description] = 'Appliance Servicing'
					
					INSERT INTO AS_JOURNAL(STATUSID,ACTIONID,INSPECTIONTYPEID,ServicingTypeId,CREATIONDATE,CREATEDBY,ISCURRENT,PROPERTYID)
					VALUES (1,1,@ApplianceInspectionTypeId,dbo.GetServicingTypeByHeating(@HeatingMappingId),GETDATE(),@UpdatedBy,1,@PropertyId)	
					Set @journalId = Scope_identity();
					
					INSERT INTO AS_JOURNALHISTORY(JOURNALID,STATUSID,ACTIONID,INSPECTIONTYPEID,ServicingTypeId,CREATIONDATE,CREATEDBY,PROPERTYID)
					VALUES (@journalId,1,1,@ApplianceInspectionTypeId,dbo.GetServicingTypeByHeating(@HeatingMappingId),GETDATE(),@UpdatedBy,@PropertyId)	
					Set @journalHistoryId = Scope_identity();	

				END

				IF EXISTS(SELECT  1
					FROM	AS_JOURNAL
					WHERE	JOURNALID = @journalId AND ServicingTypeId IS NULL)
					BEGIN
						UPDATE	AS_JOURNAL
						SET		ServicingTypeId = dbo.GetServicingTypeByHeating(@HeatingMappingId)
						WHERE	JOURNALID = @journalId
					END

				IF EXISTS(SELECT  1
					FROM	AS_JOURNALHISTORY
					WHERE	JOURNALHISTORYID = @journalHistoryId AND ServicingTypeId IS NULL)
					BEGIN
						UPDATE	AS_JOURNALHISTORY
						SET		ServicingTypeId = dbo.GetServicingTypeByHeating(@HeatingMappingId)
						WHERE	JOURNALHISTORYID = @journalHistoryId
					END


				INSERT INTO AS_JournalHeatingMapping(JournalId,HeatingMappingId,JournalHistoryId,Createdon)
				VALUES(@journalId,@HeatingMappingId,@journalHistoryId,GETDATE())
				
		END
	END
END
      ---Begin Iterate  @ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES      
      --=================================================================================    
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES        
      DECLARE @attributeId        INT, 
              @itemParamId        INT, 
              @parameterValue     NVARCHAR(1000),     
              @valueId            INT, 
              @isCheckBoxSelected BIT, 
              @Count              INT, 
              @ControlType        NVARCHAR(50), 
              @ParameterName      NVARCHAR(50) 
      ---Declare Cursor variable        
      DECLARE @ItemToInsertCursor CURSOR 

      --Initialize cursor        
      SET @ItemToInsertCursor = CURSOR fast_forward 
      FOR SELECT itemparamid, 
                 parametervalue, 
                 valueid, 
                 ischeckboxselected 
          FROM   @ItemDetail; 

      --Open cursor        
      OPEN @ItemToInsertCursor 

      ---fetch row from cursor        
      FETCH next FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue, 
      @valueId, @isCheckBoxSelected 

      ---Iterate cursor to get record row by row        
      WHILE @@FETCH_STATUS = 0 
        BEGIN 
			SET @attributeId = 0
            --- Select Control type  of parameter          
            SELECT @ControlType = controltype, 
                   @ParameterName = parametername 
            FROM   pa_parameter 
                   INNER JOIN pa_item_parameter 
                           ON pa_item_parameter.parameterid = 
                              pa_parameter.parameterid 
            WHERE  itemparamid = @itemParamId 

            IF @ControlType = 'TextBox' 
                OR @ControlType = 'TextArea' 
              BEGIN 
                  SET @valueId=NULL 

                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId IS NULL  
                  SELECT @attributeId = attributeid 
                  FROM   pa_property_attributes 
                  WHERE  propertyid = @PropertyId 
                         AND itemparamid = @itemParamId 
                         AND valueid IS NULL 
                         --AND PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId
                         AND ((PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId AND @HeatingMappingId >0 )
                   OR (PA_PROPERTY_ATTRIBUTES.HeatingMappingId IS NULL ))
                         
              END 
            ELSE IF @ControlType = 'CheckBoxes' 
              BEGIN 
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId =@valueId  
                  SELECT @attributeId = attributeid 
                  FROM   pa_property_attributes 
                  WHERE  propertyid = @PropertyId 
                         AND itemparamid = @itemParamId 
                         AND valueid = @valueId 
                         AND ((PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId AND @HeatingMappingId >0 )
                   OR (PA_PROPERTY_ATTRIBUTES.HeatingMappingId IS NULL ))
              END 
            ELSE 
              BEGIN 
                  --SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId   
                  SELECT @attributeId = attributeid 
                  FROM   pa_property_attributes 
                  WHERE  propertyid = @PropertyId 
                         AND itemparamid = @itemParamId 
                         AND ((PA_PROPERTY_ATTRIBUTES.HeatingMappingId=@HeatingMappingId AND @HeatingMappingId >0 )
						 OR (PA_PROPERTY_ATTRIBUTES.HeatingMappingId IS NULL ))
              END 

            -- if record exist update that row otherwise insert a new record           
            IF @attributeId > 0 
              BEGIN 
                  IF EXISTS(SELECT worksrequired 
                            FROM   @ConditionWork) 
                     AND EXISTS(SELECT * 
                                FROM   planned_conditionworks 
                                WHERE  attributeid = @attributeId) 
                     AND (Upper(@ParameterName) = Upper('Condition Rating') OR Upper(@ParameterName) = Upper('Boiler Condition Rating'))
                    BEGIN 
                        DECLARE @ConditionWorksId INT, 
                                @WorkCompnentId   INT 
                        DECLARE db_cursor_conditionworks CURSOR FOR 
                          SELECT conditionworksid, 
                                 componentid 
                          FROM   planned_conditionworks 
                          WHERE  attributeid = @attributeId 

                        OPEN db_cursor_conditionworks 

                        FETCH next FROM db_cursor_conditionworks INTO 
                        @ConditionWorksId, 
                        @WorkCompnentId 

                        WHILE @@FETCH_STATUS = 0 
                          BEGIN 
                              DECLARE @ConditionWorksCount INT 

                              SELECT @ConditionWorksCount = Count(*) 
                              FROM   @ConditionWork 

                              IF @ConditionWorksCount = 1 
                                BEGIN 
                                    SET @WorkCompnentId = 
                                    (SELECT TOP 1 componentid 
                                     FROM   @ConditionWork) 

                                    IF @WorkCompnentId = 0 
                                      BEGIN 
                                          SET @WorkCompnentId =NULL 
                                      END 
                                END 

                              UPDATE planned_conditionworks 
                              SET    worksrequired = (SELECT TOP 1 worksrequired 
                                                      FROM   @ConditionWork), 
                                     conditionaction = 
                                     (SELECT actionid 
                                      FROM   planned_action 
                                      WHERE  title = 'Recommended'), 
                                     componentid = @WorkCompnentId, 
                                     modifiedby = @UpdatedBy, 
                                     modifieddate = Getdate() 
                              WHERE  conditionworksid = @ConditionWorksId 
                             -- And PLANNED_CONDITIONWORKS.HeatingMappingId=@HeatingMappingId
                              AND ((PLANNED_CONDITIONWORKS.HeatingMappingId=@HeatingMappingId AND @HeatingMappingId >0 )
                   OR (PLANNED_CONDITIONWORKS.HeatingMappingId IS NULL ))

                              INSERT INTO planned_conditionworks_history 
                                          (conditionworksid, 
                                           worksrequired, 
                                           attributeid, 
                                           valueid, 
                                           conditionaction, 
                                           componentid, 
                                           createdby, 
                                           createddate,HeatingMappingId) 
                              VALUES      (@ConditionWorksId, 
                                           (SELECT TOP 1 worksrequired 
                                            FROM   @ConditionWork), 
                                           @attributeId, 
                                           @valueId, 
                                           (SELECT actionid 
                                            FROM   planned_action 
                                            WHERE  title = 'Recommended'), 
                                           @WorkCompnentId, 
                                           @UpdatedBy, 
                                           Getdate(),@HeatingMappingId) 

                              FETCH next FROM db_cursor_conditionworks INTO 
                              @ConditionWorksId, 
                              @WorkCompnentId 
                          END 

                        CLOSE db_cursor_conditionworks 

                        DEALLOCATE db_cursor_conditionworks 
                    END 
                  ELSE IF EXISTS(SELECT worksrequired 
                            FROM   @ConditionWork) AND (Upper(@ParameterName) = Upper('Condition Rating') OR Upper(@ParameterName) = Upper('Boiler Condition Rating'))
                    BEGIN 				
                    
                        DECLARE @conditionRatingWorksRequired NVARCHAR(4000), 
                                @conditionRatingComponentId   INT, 
                                @conditionRatingValueId       INT 
                        DECLARE db_cursor_insertconditionworks CURSOR FOR 
                          SELECT componentid, 
                                 worksrequired, 
                                 valueid 
                          FROM   @ConditionWork 

                        OPEN db_cursor_insertconditionworks 

                        FETCH next FROM db_cursor_insertconditionworks INTO 
                        @conditionRatingComponentId, 
                        @conditionRatingWorksRequired 
                        , 
                        @conditionRatingValueId 

                        WHILE @@FETCH_STATUS = 0 
                          BEGIN 
                              IF @conditionRatingComponentId = 0 
                                BEGIN 
                                    SET @conditionRatingComponentId =NULL 
                                END 

                              INSERT INTO planned_conditionworks 
                                          (worksrequired, 
                                           attributeid, 
                                           conditionaction, 
                                           componentid, 
                                           createdby, 
                                           createddate,PLANNED_CONDITIONWORKS.HeatingMappingId) 
                              VALUES      (@conditionRatingWorksRequired, 
                                           @attributeId, 
                                           (SELECT actionid 
                                            FROM   planned_action 
                                            WHERE  title = 'Recommended'), 
                                           @conditionRatingComponentId, 
                                           @UpdatedBy, 
                                           Getdate(),@HeatingMappingId) 

                              INSERT INTO planned_conditionworks_history 
                                          (conditionworksid, 
                                           worksrequired, 
                                           attributeid, 
                                           valueid, 
                                           conditionaction, 
                                           componentid, 
                                           createdby, 
                                           createddate,PLANNED_CONDITIONWORKS_HISTORY.HeatingMappingId) 
                              VALUES      (Scope_identity(), 
                                           @conditionRatingWorksRequired, 
                                           @attributeId, 
                                           @conditionRatingValueId, 
                                           (SELECT actionid 
                                            FROM   planned_action 
                                            WHERE  title = 'Recommended'), 
                                           @conditionRatingComponentId, 
                                           @UpdatedBy, 
                                           Getdate(),@HeatingMappingId) 

                              FETCH next FROM db_cursor_insertconditionworks 
                              INTO 
                              @conditionRatingComponentId
                              ,@conditionRatingWorksRequired 
                              ,@conditionRatingValueId 
                          END 

                        CLOSE db_cursor_insertconditionworks 

                        DEALLOCATE db_cursor_insertconditionworks 
                    END 

                  UPDATE pa_property_attributes 
                  SET    itemparamid = @itemParamId, 
                         parametervalue = @parameterValue, 
                         valueid = @valueId, 
                         updatedon = Getdate(), 
                         updatedby = @UpdatedBy, 
                         ischeckboxselected = @isCheckBoxSelected,
                         HeatingMappingId=@HeatingMappingId  
                  WHERE  attributeid = @attributeId 
              END 
            ELSE 
              BEGIN 
                  DECLARE @latestAttributeId INT 

                  INSERT INTO pa_property_attributes 
                              (propertyid, 
                               itemparamid, 
                               parametervalue, 
                               valueid, 
                               updatedon, 
                               updatedby, 
                               ischeckboxselected,PA_PROPERTY_ATTRIBUTES.HeatingMappingId) 
                  VALUES      (@PropertyId, 
                               @itemParamId, 
                               @parameterValue, 
                               @valueId, 
                               Getdate(), 
                               @UpdatedBy, 
                               @isCheckBoxSelected,@HeatingMappingId) 

                  SET @latestAttributeId = Scope_identity() 

                  IF EXISTS(SELECT worksrequired 
                            FROM   @ConditionWork) 
                     AND (Upper(@ParameterName) = Upper('Condition Rating') OR Upper(@ParameterName) = Upper('Boiler Condition Rating'))
                    BEGIN 
                        DECLARE @conditionWorksRequired NVARCHAR(4000), 
                                @conditionComponentId   INT, 
                                @conditionValueId       INT 
                        DECLARE db_cursor_insertconditionworks CURSOR FOR 
                          SELECT componentid, 
                                 worksrequired, 
                                 valueid 
                          FROM   @ConditionWork 

                        OPEN db_cursor_insertconditionworks 

                        FETCH next FROM db_cursor_insertconditionworks INTO 
                        @conditionComponentId, 
                        @conditionWorksRequired, @conditionValueId 

                        WHILE @@FETCH_STATUS = 0 
                          BEGIN 
                              IF @conditionComponentId = 0 
                                BEGIN 
                                    SET @conditionComponentId =NULL 
                                END 

                              INSERT INTO planned_conditionworks 
                                          (worksrequired, 
                                           attributeid, 
                                           conditionaction, 
                                           componentid, 
                                           createdby, 
                                           createddate) 
                              VALUES      (@conditionWorksRequired, 
                                           @latestAttributeId, 
                                           (SELECT actionid 
                                            FROM   planned_action 
                                            WHERE  title = 'Recommended'), 
                                           @conditionComponentId, 
                                           @UpdatedBy, 
                                           Getdate()) 

                              INSERT INTO planned_conditionworks_history 
                                          (conditionworksid, 
                                           worksrequired, 
                                           attributeid, 
                                           valueid, 
                                           conditionaction, 
                                           componentid, 
                                           createdby, 
                                           createddate) 
                              VALUES      (Scope_identity(), 
                                           @conditionWorksRequired, 
                                           @latestAttributeId, 
                                           @conditionValueId, 
                                           (SELECT actionid 
                                            FROM   planned_action 
                                            WHERE  title = 'Recommended'), 
                                           @conditionComponentId, 
                                           @UpdatedBy, 
                                           Getdate()) 

                              FETCH next FROM db_cursor_insertconditionworks 
                              INTO 
                              @conditionComponentId, 
                              @conditionWorksRequired, @conditionValueId 
                          END 

                        CLOSE db_cursor_insertconditionworks 

                        DEALLOCATE db_cursor_insertconditionworks 
                    END 
              END 

            FETCH next FROM @ItemToInsertCursor INTO @itemParamId, 
            @parameterValue 
            , 
            @valueId, @isCheckBoxSelected 
        END 

      --close & deallocate cursor          
      CLOSE @ItemToInsertCursor 

      DEALLOCATE @ItemToInsertCursor 

      --Strart Iterate ItemDates to insert or update data in         
      --=====================================================================================        
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES        
      DECLARE @sid         INT, 
              @parameterId INT, 
              @lastDone    DATETIME, 
              @dueDate     DATETIME, 
              @componentId INT, 
              @psid        INT 

   
      ---Declare Cursor variable        
      DECLARE @ItemToInsertDateCursor CURSOR 
 
      --Initialize cursor        
      SET @ItemToInsertDateCursor = CURSOR fast_forward 
      FOR SELECT ParameterId, 
                  convert( Datetime, LastDone, 103 )as LastDone, 
                 convert( Datetime,DueDate, 103 )as DueDate, 
                 ComponentId 
          FROM   @ItemDates; 

      --Open cursor        
      OPEN @ItemToInsertDateCursor 

      ---fetch row from cursor        
      FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone, 
      @dueDate, @componentId 

      ---Iterate cursor to get record row by row        
      WHILE @@FETCH_STATUS = 0 
        BEGIN 
			select @sid=0
			select @psid=0

           IF @lastDone = '1900-01-01 00:00:00.000' 
               OR @lastDone IS NULL 
              SET @lastDone=NULL 

            IF @dueDate = '1900-01-01 00:00:00.000' 
               OR @dueDate IS NULL 
              SET @dueDate=NULL 

            SELECT @sid = [sid] 
            FROM   pa_property_item_dates 
            WHERE  propertyid = @PropertyId 
                   AND itemid = @ItemId 
                   AND  parameterid = @parameterId  
                   AND ((PA_PROPERTY_ITEM_DATES.HeatingMappingId=@HeatingMappingId AND @HeatingMappingId >0 )
                   OR (PA_PROPERTY_ITEM_DATES.HeatingMappingId IS NULL ))

            SELECT @psid = [sid] 
            FROM   PA_PROPERTY_ITEM_DATES 
            WHERE  propertyid = @PropertyId 
                   AND itemid = @ItemId 
                   AND ( parameterid = 0 
                          OR parameterid IS NULL ) 
                   AND ( lastdone IS NOT NULL 
                   AND duedate IS NOT NULL )
                   AND ((PA_PROPERTY_ITEM_DATES.HeatingMappingId=@HeatingMappingId AND @HeatingMappingId >0 )
                   OR (PA_PROPERTY_ITEM_DATES.HeatingMappingId IS NULL ))

            IF @psid > 0 And ( @parameterId = 0 OR @parameterId IS NULL ) 
              BEGIN 
                
                        UPDATE pa_property_item_dates 
                        SET    lastdone = @lastDone,  
                               duedate = @dueDate, 
                               updatedon = Getdate(), 
                               updatedby = @UpdatedBy, 
                               planned_componentid = @componentId ,
                               HeatingMappingId= @HeatingMappingId
                        WHERE  [sid] = @psid 
                  
              END 
            ELSE IF @sid > 0 
              BEGIN 
                  IF @lastDone IS NOT NULL 
                     AND @dueDate IS NOT NULL 
                    BEGIN 
                        UPDATE pa_property_item_dates 
                        SET    lastdone = @lastDone,  
                               duedate = @dueDate, 
                               updatedon = Getdate(), 
                               updatedby = @UpdatedBy, 
                               planned_componentid = @componentId,
                               HeatingMappingId= @HeatingMappingId 
                        WHERE  [sid] = @sid 
                    END 
                  ELSE IF @dueDate IS NOT NULL 
                    BEGIN 
                        UPDATE pa_property_item_dates 
                        SET    lastdone = @lastDone,  
                               duedate = @dueDate,  
                               updatedon = Getdate(), 
                               updatedby = @UpdatedBy, 
                               planned_componentid = @componentId ,
                               HeatingMappingId= @HeatingMappingId
                        WHERE  [sid] = @sid 
                    END 
              END 
            ELSE 
              BEGIN 
                  IF @lastDone IS NOT NULL 
                     AND @dueDate IS NOT NULL 
                    BEGIN 
                        INSERT INTO pa_property_item_dates 
                                    (propertyid, 
                                     itemid, 
                                     lastdone, 
                                     duedate, 
                                     updatedon, 
                                     updatedby, 
                                     parameterid, 
                                     planned_componentid,PA_PROPERTY_ITEM_DATES.HeatingMappingId) 
                        VALUES      ( @PropertyId, 
                                      @ItemId, 
                                      @lastDone, 
                                      @dueDate, 
                                      Getdate(), 
                                      @UpdatedBy, 
                                      @parameterId, 
                                      @componentId,@HeatingMappingId) 
                    END 
                  ELSE IF @dueDate IS NOT NULL 
                    BEGIN 
                        INSERT INTO pa_property_item_dates 
                                    (propertyid, 
                                     itemid, 
                                     duedate, 
                                     updatedon, 
                                     updatedby, 
                                     parameterid, 
                                     planned_componentid,HeatingMappingId) 
                        VALUES      ( @PropertyId, 
                                      @ItemId, 
                                       @dueDate,  
                                      Getdate(), 
                                      @UpdatedBy, 
                                      @parameterId, 
                                      @componentId,@HeatingMappingId) 
                    END 
              END 
			
            FETCH next FROM @ItemToInsertDateCursor INTO @parameterId, @lastDone, 
            @dueDate, @componentId 
        END 

      --close & deallocate cursor          
      CLOSE @ItemToInsertDateCursor 

      DEALLOCATE @ItemToInsertDateCursor 
        
      --Change : Added @UpdatedBy   
      --By : Ahmed  
      Exec PDR_AmendpMSTAItemDetail @PropertyId,null,null,@ItemId,@UpdatedBy,@ChildAttributeMappingId,@MSATDetail   
  END 
  