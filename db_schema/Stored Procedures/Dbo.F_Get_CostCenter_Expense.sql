USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[F_Get_CostCenter_Expense]    Script Date: 22/3/2017 18:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.F_Get_CostCenter_Expense') IS NULL 
	EXEC('CREATE PROCEDURE dbo.F_Get_CostCenter_Expense AS SET NOCOUNT ON;') 
GO
-- =============================================
-- Author:		Saud Ahmed
-- Create date: 22/3/2017
-- Description:	Get all cost center accounts details 
-- WebPage: BHGFinanceModule/Views/CostCenter/Index
-- EXEC F_Get_Head '01/04/2016 00:00', '31/03/2017 23:59' 

-- =============================================
ALTER PROCEDURE [dbo].[F_Get_CostCenter_Expense]				
		@fromDate datetime,
		@toDate datetime,
		@headId int 
AS
BEGIN
	
	-- Expenditure
	SELECT EX.EXPENDITUREID, EX.HEADID, isNull(BUDGET,0) As BUDGET, DESCRIPTION As ExpenseName
		,(ISNULL(TOPLEVEL.BALANCE ,0) + ISNULL(dl.BALANCE, 0) - ISNULL(cl.BALANCE, 0)) AS NL
		, isNull(ORDERED_PURCHASES,0) as PURCHASES 
		,ISNULL(BUDGET 
			- (ISNULL(TOPLEVEL.BALANCE ,0) + ISNULL(dl.BALANCE, 0)) - ISNULL(cl.BALANCE, 0)
			- ISNULL(E_ORDERED_PURCHASES.ORDERED_PURCHASES,0), 0) AS BAL
	FROM F_EXPENDITURE EX  
		inner join F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = Ex.EXPENDITUREID
		LEFT JOIN (  
			SELECT SUM(EXA.EXPENDITUREALLOCATION) as BUDGET, EXA.EXPENDITUREID
				FROM F_EXPENDITURE_ALLOCATION EXA
				INNER JOIN F_EXPENDITURE ON EXA.EXPENDITUREID = F_EXPENDITURE.EXPENDITUREID AND EXA.FISCALYEAR in (
				select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
					YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd
			)
			GROUP BY EXA.EXPENDITUREID ) 
			EXPENSE_BUDGET ON EX.EXPENDITUREID = EXPENSE_BUDGET.EXPENDITUREID 
		LEFT JOIN (
			SELECT	EXPENDITUREID, SUM(GROSSCOST) AS ORDERED_PURCHASES 
			FROM	F_PURCHASEITEM
					INNER JOIN F_POSTATUS ON  F_PURCHASEITEM.PISTATUS = F_POSTATUS.POSTATUSID  
			WHERE	F_PURCHASEITEM.ACTIVE = 1 
					AND F_POSTATUS.POSTATUSNAME IN ('Goods Ordered', 'Work Ordered', 'Work Completed', 'Part Completed', 'Goods Received', 'Goods Approved','Invoice Received','Invoice Approved')   
					AND PIDATE >= @fromDate AND PIDATE <= @toDate 
				GROUP BY EXPENDITUREID 
			)  
			E_ORDERED_PURCHASES ON E_ORDERED_PURCHASES.EXPENDITUREID = EX.EXPENDITUREID 
		LEFT JOIN (
						SELECT	F_EXPENDITURE.EXPENDITUREID ,SUM(GROSSCOST) as Balance
						FROM	F_PURCHASEITEM
								INNER JOIN F_POSTATUS ON  F_PURCHASEITEM.PISTATUS = F_POSTATUS.POSTATUSID
								INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID
						WHERE	F_POSTATUS.POSTATUSNAME IN ('Invoice Paid','Reconciled','Paid By Cheque','Paid By BACS','Paid by Direct Debit','Paid','Paid By Cash','Part Paid')  
								AND F_PURCHASEITEM.ACTIVE = 1 
								AND PIDATE BETWEEN @fromDate AND @toDate
						GROUP BY F_EXPENDITURE.EXPENDITUREID 
				) TOPLEVEL ON TOPLEVEL.EXPENDITUREID = EX.EXPENDITUREID 
		LEFT JOIN (
						select GJDL.ExpenditureId, sum(GJDL.amount) as Balance from NL_JOURNALENTRY nlj
						inner join  NL_JOURNALENTRYDEBITLINE GJDL on nlj.TXNID = GJDL.TXNID 
						inner join NL_TRANSACTIONTYPE TT on nlj.TRANSACTIONTYPE = TT.TRANSACTIONTYPEID
						where GJDL.ExpenditureId is not null
						AND TT.LONGDESCRIPTION = 'General Journal Reversal' OR TT.LONGDESCRIPTION = 'General Journal' 
						ANd nlj.TXNDATE BETWEEN @fromDate AND @toDate
						group by GJDL.ExpenditureId
				) DL ON DL.EXPENDITUREID = EX.EXPENDITUREID 
		LEFT JOIN (
						select GJCL.ExpenditureId, sum(GJCL.amount) as Balance from NL_JOURNALENTRY nlj
						inner join  NL_JOURNALENTRYCREDITLINE GJCL on nlj.TXNID = GJCL.TXNID 
						inner join NL_TRANSACTIONTYPE TT on nlj.TRANSACTIONTYPE = TT.TRANSACTIONTYPEID
						where GJCL.ExpenditureId is not null
						AND TT.LONGDESCRIPTION = 'General Journal Reversal' OR TT.LONGDESCRIPTION = 'General Journal' 
						ANd nlj.TXNDATE BETWEEN @fromDate AND @toDate
						group by GJCL.ExpenditureId
				) CL ON CL.EXPENDITUREID = EX.EXPENDITUREID 
	WHERE EX.HEADID = @headId ANd EXA.ACTIVE =1 and FISCALYEAR in (
							select YRange from F_FISCALYEARS  where YStart between @fromDate AND @toDate OR
								YEnd between @fromDate AND @toDate OR 
								@fromDate between YStart and YEnd OR 
								@toDate between YStart and YEnd)
	ORDER BY EX.DESCRIPTION 


	END