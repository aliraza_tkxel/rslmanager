USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveAppointment]    Script Date: 12/11/2012 18:03:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- DECLARE	@return_value int,
--		@journalHistoryId int

--EXEC	@return_value = [dbo].[AS_SaveAppointment]
--		@propertyId = 1,
--		@journalId = 1,
--		@isVoid = 0,	
--		@createdBy = 615,
--		@inspectionTypeId = 1,
--		@appointmentDate = '2012-11-03',
--		@statusId = 2,
--		@actionId = 5,
--		@notes = N'This is demo information',
--		@isLetterAttached = false,
--		@isDocumentAttached = false,
--		@assignedTo = 63,
--		@appStartTime = '11:00',
--		@appStartTime = '13:00',
--		@shift = 'AM',
--		@tenancyId = 10065,
--		@journalHistoryId = @journalHistoryId OUTPUT 
-- Author:		<Aqib Javed>
-- Create date: <2/11/2012>
-- Description:	<This SP shall create new appointment from Appointment Arranged Screen>
-- Webpage: FuelScheduling.aspx
-- =============================================

IF OBJECT_ID('dbo.AS_SaveAppointment') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_SaveAppointment AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_SaveAppointment] 
	@journalId int,
	@isVoid int = 0,
	@propertyId varchar(50),
	@schemeId int null,
	@blockId int null,
	@createdBy int,
	@inspectionTypeId int,
	@appointmentDate date,
	@appointmentEndDate date,
	@statusId int,
	@actionId int,
	@notes nvarchar(1000),		
	@isLetterAttached bit,
	@isDocumentAttached bit,
	@appStartTime varchar(10),
	@appEndTime varchar(10),
	@assignedTo int,
	@shift varchar(5),
	@tenancyId int null,
	@customerEmail varchar(50),
	@customerEmailStatus varchar(50),
	@duration Int = 1,
	@journalHistoryId int out,
	@apptId int = null out
AS
BEGIN
   Declare @UserId int
   Declare @AppointmentId int,@abortedStatusId int, @isRecentlyAborted bit= 0,@existingAppointmentId int
   Select @abortedStatusId=StatusId from AS_Status where Title='Aborted'
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;		
	
	if EXISTS (Select 1 from AS_JOURNAL where JOURNALID = @journalId AND isCurrent = 1 And StatusId=@abortedStatusId )
	BEGIN
		SET @isRecentlyAborted = 1;
		Select @existingAppointmentId=APPOINTMENTID from AS_APPOINTMENTS where JournalId = @journalId
	END	

	IF NOT EXISTS (Select 1 from AS_JOURNAL where JOURNALID = @journalId AND isCurrent = 1 And StatusId=@statusId )
	
	BEGIN
	
   	--Update the Journal
	UPDATE [dbo].[AS_JOURNAL]
	SET[PROPERTYID] = @propertyId,
		[SchemeId] = @schemeId,
		[BlockId] = @blockId,
	   [STATUSID] = @statusId
	  ,[ACTIONID] = @actionId
	  ,[INSPECTIONTYPEID] = @inspectionTypeId
	  ,[CREATIONDATE] = GETDATE()
	  ,[CREATEDBY] = @createdBy	
	WHERE JOURNALID = @journalId AND isCurrent = 1
	
	--Insert Record into Journal History
	INSERT INTO [dbo].[AS_JOURNALHISTORY]
           ([JOURNALID]
           ,[PROPERTYID]
		   ,[SchemeId]
		   ,[BlockId]
           ,[STATUSID]
           ,[ACTIONID]
           ,[INSPECTIONTYPEID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]           
           ,[IsDocumentAttached]
		   ,[ServicingTypeid])
     VALUES
           (@journalId
           ,@propertyId
		   ,@schemeId
		   ,@blockId
           ,@statusId
           ,@actionId
           ,@inspectionTypeId
           ,GETDATE() 
           ,@createdBy
           ,@notes
           ,@isLetterAttached
		   ,@isDocumentAttached
		   ,(Select ServicingTypeid from AS_Journal Where JournalId = @journalId) )    

--Getting JournalHistory Id		   
   SELECT @journalHistoryId =  @@identity  
   Declare @statusHistoryId int
   
   SELECT @statusHistoryId = MAX(StatusHistoryId) FROM AS_StatusHistory WHERE StatusId=@statusId
   
	Declare @actionHistoryId int
	SELECT @actionHistoryId = MAX(ActionHistoryId) FROM AS_ActionHistory  where ActionId=@actionId
   
    --Update Journal History Again With Status History and Action History Id
	UPDATE [AS_JOURNALHISTORY]
	SET [StatusHistoryId] = @statusHistoryId
      ,[ActionHistoryId] = @actionHistoryId
      
	WHERE JOURNALHISTORYID = @journalHistoryId
 
   --Insert Record in Appointment 
	Declare @jsgNumber int
	--SELECT @jsgNumber = MAX(JSGNUMBER) FROM AS_APPOINTMENTS
	
	SELECT @jsgNumber = coalesce((select max(JSGNUMBER) + 1 from AS_APPOINTMENTS), 1)
	
	
	--Change - by Behroz - 05/02/2013 - Start
	DECLARE @AppStatus varchar(50)
	
	IF (@statusId = 1 or @statusId = 2 or @statusId = 4)
	BEGIN
		SET @AppStatus = 'NotStarted'
	END
	ELSE IF(@statusId = 3 or @statusId = 5)
	BEGIN
		SET @AppStatus = 'Finished'
	END	
	--Change - by Behroz - 05/02/2013 - End
	
IF @isRecentlyAborted = 0
BEGIN 

	-- Inserting into AS_APPOINTMENTS Table	 
		INSERT INTO [dbo].[AS_APPOINTMENTS]
			   (
				[JSGNUMBER]
			   ,[TENANCYID]
			   ,[JournalId]
			   ,[JOURNALHISTORYID]
			   ,[APPOINTMENTDATE]
			   ,[APPOINTMENTSHIFT]
			   ,[APPOINTMENTSTARTTIME]
			   ,[APPOINTMENTENDTIME]
			   ,[ASSIGNEDTO]
			   ,[CREATEDBY]
			   ,[LOGGEDDATE]
			   ,[NOTES]
			   --Change - by Behroz - 05/02/2013 - Start
			   ,[APPOINTMENTSTATUS]
			   --Change - by Behroz - 05/02/2013 - End
			   ,[ISVOID]
			   ,[CustomerEmail]
			   ,[CustomerEmailStatus]
			   ,[Duration]
			   ,[APPOINTMENTENDDATE]
			   )
		 VALUES
			   (
			   @jsgNumber
			   ,@tenancyId
			   ,@journalId
			   ,@journalHistoryId
			   ,@appointmentDate
			   ,@shift
			   ,@appStartTime
			   ,@appEndTime
			   ,@assignedTo
			   ,@createdBy
			   ,GETDATE() 
			   ,@notes
			   --Change - by Behroz - 05/02/2013 - Start
			   ,@AppStatus
			   --Change - by Behroz - 05/02/2013 - End
			   ,@isVoid
			   ,@customerEmail
			   ,(select [EmailStatusId] from AS_EmailStatus where StatusDescription=@customerEmailStatus)
			   ,@duration
			   ,@appointmentEndDate
			   )     
   
	-- Getting Appointment Id
	SELECT @AppointmentId =  @@identity 

END
ELSE
BEGIN
	UPDATE AS_APPOINTMENTS SET JOURNALHISTORYID=@journalHistoryId,APPOINTMENTDATE=@appointmentDate, [APPOINTMENTSHIFT]=@shift,
	[APPOINTMENTSTARTTIME]=@appStartTime,[APPOINTMENTENDTIME]=@appEndTime,[ASSIGNEDTO]=@assignedTo,[CREATEDBY]=@createdBy,[LOGGEDDATE]=GETDATE() ,[NOTES]=@notes,
    [APPOINTMENTSTATUS]=@AppStatus,[ISVOID]=@isVoid, [Duration] = @duration, [APPOINTMENTENDDATE] = @appointmentEndDate where APPOINTMENTID = @existingAppointmentId

	set @AppointmentId = @existingAppointmentId

END

-- Inserting into AS_APPOINTMENTSHISTORY Table	 
	INSERT INTO [dbo].[AS_APPOINTMENTSHISTORY]
           (
            [APPOINTMENTID],
            [JSGNUMBER],
            [TENANCYID]
           ,[JournalId]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTDATE]
           ,[APPOINTMENTSHIFT]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[NOTES]
           --Change - by Behroz - 05/02/2013 - Start
           ,[APPOINTMENTSTATUS]
           --Change - by Behroz - 05/02/2013 - End
           ,[IsVoid]
	   ,[Duration]
	   ,[APPOINTMENTENDDATE]
           )
     VALUES
           (
           @AppointmentId,
           @jsgNumber
           ,@tenancyId
           ,@journalId
           ,@journalHistoryId
           ,@appointmentDate
           ,@shift
           ,@appStartTime
           ,@appEndTime
           ,@assignedTo
           ,@createdBy
           ,GETDATE() 
           ,@notes
           --Change - by Behroz - 05/02/2013 - Start
           ,@AppStatus
           --Change - by Behroz - 05/02/2013 - End
           ,@isVoid
	   ,@duration
	   ,@appointmentEndDate
           )     
   
   SET @apptId = @appointmentId

   IF EXISTS(
   SELECT	1
   FROM		P_LGSR
   WHERE	P_LGSR.PROPERTYID = @propertyId and P_LGSR.SchemeId = @schemeId and P_LGSR.BlockId=@blockId and INSPECTIONCARRIED=1)
   BEGIN
	 
	 UPDATE P_LGSR
	 SET INSPECTIONCARRIED=0
	 WHERE	P_LGSR.PROPERTYID = @propertyId and P_LGSR.SchemeId = @schemeId and P_LGSR.BlockId = @blockId and INSPECTIONCARRIED=1
	 	 
   END
 
 END
 ELSE
 BEGIN
 
 SET @journalHistoryId = (SELECT MAX(journalHistoryId) FROM AS_JOURNALHISTORY WHERE JournalId=@journalId)

 SELECT @journalHistoryId , @apptId
 
 END
   
END

