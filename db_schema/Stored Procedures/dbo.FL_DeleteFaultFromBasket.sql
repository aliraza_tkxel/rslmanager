SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 -- =============================================
-- EXEC FL_DeleteFaultFromBasket
--  @TempFaultId = 3
-- Author:		<Ahmed Mehmood>
-- Create date: <24/01/2013>
-- Description:	<Delete Fault from basket>
-- Web Page: FaultBasket.aspx
-- =============================================
 
CREATE PROCEDURE [dbo].[FL_DeleteFaultFromBasket] 

 (	
	@TempFaultId Int,
	@Result INT OUTPUT
  )
AS 
IF NOT EXISTS
(
    SELECT NULL FROM   
      [FL_TEMP_FAULT]
         WHERE
      [TempFaultID] = @TempFaultId
)
   BEGIN
      SET @Result = -1
         END
ELSE
   BEGIN
      DELETE 
         [FL_TEMP_FAULT]
      WHERE 
         [TempFaultID] = @TempFaultId
                 
      SET @Result = 0
         END
               
SELECT @Result as Result

GO
