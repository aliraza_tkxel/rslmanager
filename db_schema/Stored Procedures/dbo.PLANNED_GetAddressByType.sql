USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetAddressByType]    Script Date: 1/10/2017 7:01:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[PLANNED_GetAddressByType]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[PLANNED_GetAddressByType] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[PLANNED_GetAddressByType](
	@id varchar(20),
	@type varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@type = 'Scheme')
	BEGIN
		SELECT SCHEMEID, SCHEMENAME as SchemeName 
		FROM P_SCHEME
		WHERE SCHEMEID= @id
	END

	if(@type = 'Block')
	BEGIN
		SELECT BLOCKID, ISNULL(b.BLOCKNAME, '') + ' ' + ISNULL(b.ADDRESS1, '') + ' ' + ISNULL(b.ADDRESS2, '') + ' ' + ISNULL(b.ADDRESS3, '') AS Block,
				s.SCHEMEID, ISNULL(s.SCHEMENAME,'') as SchemeName
		FROM P_BLOCK b
		left join P_SCHEME s on s.SCHEMEID=b.SchemeId
		WHERE BLOCKID= @id
	END
END
