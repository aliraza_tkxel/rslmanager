USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetCountLegalProceedings]    Script Date: 23-Jul-18 11:30:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC	[dbo].[AS_GetCountLegalProceedings]
--		@StatusTitle = N'Legal Proceedings',
--		@PatchId = 18,
--		@DevelopmentId = 1 
-- Author:		<Hussain Ali>
-- Create date: <31/10/2012>
-- Description:	<This stored procedure gets the count of all 'Legal Proceedings'>
-- Useage: <dashboard.aspx>

-- =============================================
IF OBJECT_ID('dbo.[AS_GetCountLegalProceedings]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetCountLegalProceedings] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetCountLegalProceedings]
	-- Add the parameters for the stored procedure here
	@PatchId		int,
	@DevelopmentId	int,
	@FuelType varchar(8000)
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	nvarchar(max),
			@fromClause		nvarchar(max),
			@whereClause	nvarchar(max),
			@fromClauseScheme	nvarchar(max),
			@whereClauseScheme	nvarchar(max),
			@fromClauseBlock	nvarchar(max),
			@whereClauseBlock	nvarchar(max),
			@query			nvarchar(max)      
			
	-- Initalizing the variables to be used in this query
	---------------------------------------------------- FOR Gas --------------------------------------------	
if @FuelType = 'Gas'
begin	
	SET @selectClause	= 'SELECT 
								COUNT(*) AS Number'
	
	SET @fromClause		= 'FROM 
								(	SELECT 
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
								ISNULL(P__PROPERTY.HOUSENUMBER,'''') As HouseNumber,
								ISNULL(P__PROPERTY.ADDRESS2,'''') as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								PV.ValueDetail As FUEL
								FROM 
								AS_JOURNAL
								INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID 
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
									AND P__PROPERTY.STATUS NOT IN (9,5,6) 
									AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
									AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
									AND AS_JOURNAL.IsCurrent = 1
									AND AS_JOURNAL.StatusId = 4'

	SET @fromClauseScheme = ' SELECT
								ISNULL(P.SCHEMENAME,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								PV.ValueDetail As FUEL
								FROM AS_JOURNAL
								INNER JOIN P_Scheme P ON AS_JOURNAL.SchemeId = P.SchemeId
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P.SchemeId = PHM.SchemeId
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId '

	SET @whereClauseScheme = '  Where 1=1 
										AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Gas'' )'

	SET @fromClauseBlock = ' SELECT
								ISNULL(P.BlockName,'''') as Address,
								''''  AS HouseNumber,
								'''' as Address2,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								PV.ValueDetail As FUEL
								FROM AS_JOURNAL
								INNER JOIN P_Block P ON AS_JOURNAL.BlockId = P.BlockId
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P.BlockId = PHM.BlockId
								Inner Join PA_Parameter_value PV on PHM.HeatingType = PV.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId '

	SET @whereClauseBlock = '  Where 1=1 
										AND PV.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''mains gas'')
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Gas'' )'
end	
---------------------------------------------------- FOR Oil --------------------------------------------
if @FuelType = 'Oil'
begin
		SET @selectClause	= 'SELECT 
								COUNT(*) AS Number'
	
	SET @fromClause		= 'FROM 
								(SELECT DISTINCT 
								P__PROPERTY.PROPERTYID,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort
								FROM 
								AS_JOURNAL
								INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID 
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
									AND P__PROPERTY.STATUS NOT IN (9,5,6) 
									AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
									AND PA_Parameter_value.ValueID = (select top 1 ValueId from PA_PARAMETER_VALUE where valuedetail = ''oil'')
									AND AS_JOURNAL.IsCurrent = 1
									AND AS_JOURNAL.StatusId = 4'

end
---------------------------------------------------- FOR Alternative Servicing --------------------------------------------
if @FuelType = 'Alternative Servicing'
begin	
		SET @selectClause	= 'SELECT 
								COUNT(*) AS Number'
	
	SET @fromClause		= 'FROM 
								(
								SELECT DISTINCT 
								P__PROPERTY.PROPERTYID,
								ISNULL(AS_Status.Title, '''') as StatusTitle,
								DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS ExpiryDateSort,
								FuelTypes.FUELS AS FUEL								
								FROM 
								AS_JOURNAL
								INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID 
								INNER JOIN 
										(SELECT	P__PROPERTY.PROPERTYID, FUELS  = STUFF(
													(SELECT '', '' + PV.ValueDetail
														FROM PA_HeatingMapping HM
														Inner JOIN PA_PARAMETER_VALUE PV on HM.HeatingType = PV.ValueId
														WHERE HM.IsActive = 1 AND PV.IsAlterNativeHeating = 1 AND P__PROPERTY.PropertyId = HM.PropertyId AND PV.IsActive = 1
														FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''NVARCHAR(max)''), 1, 2, '''')
										FROM	P__PROPERTY 
										GROUP BY P__PROPERTY.PROPERTYID) As FuelTypes 
										on P__PROPERTY.PROPERTYID = FuelTypes.PropertyId
								LEFT JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
								Inner join PA_HeatingMapping PHM ON P__PROPERTY.PROPERTYID = PHM.PROPERTYID
								Inner Join PA_Parameter_value on PHM.HeatingType = PA_Parameter_value.ValueID
								LEFT JOIN P_LGSR ON PHM.HeatingMappingId = P_LGSR.HeatingMappingId'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
										AND P__PROPERTY.STATUS NOT IN (9,5,6) 
										AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
										AND PA_Parameter_value.IsAlterNativeHeating = 1
										AND AS_JOURNAL.IsCurrent = 1
										AND AS_JOURNAL.StatusId = 4
										AND AS_Journal.ServicingTypeId = (select servicingTypeId from P_ServicingType where Description = ''Alternative Servicing'' )'
end						
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
		SET @whereClauseBlock = @whereClauseBlock + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @PatchId) + CHAR(10)
		SET @whereClauseScheme = @whereClauseScheme + ' P.DEVELOPMENTID = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	

	Declare @unionQuery varchar(100)
	SET @unionQuery = char(10) + ' UNION ALL ' + CHAR(10)
	-- Building the Query	
	if @fuelType = 'Gas'
	Begin
		SET @whereClauseBlock = @whereClauseBlock + ') AS LegalProceeding'
		SET @query = @selectClause + @fromClause + @whereClause + @unionQuery +
					@fromClauseScheme + @whereClauseScheme + @unionQuery +
					@fromClauseBlock + @whereClauseBlock
	End
	Else
	Begin
		SET @whereClause = @whereClause + ') AS LegalProceeding'
		SET @query = @selectClause + @fromClause + @whereClause
	End
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)

END



