SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO












CREATE PROCEDURE [dbo].[FL_UPDATE_FAULT_CONTRACTOR]
	/*	===============================================================
	'   NAME:           FL_UPDATE_FAULT_CONTRACTOR
	'   DATE CREATED:   24TH NOV,2008
	'   CREATED BY:     Usman Sharif
	'   CREATED FOR:    Tenants Online
	'   PURPOSE:        To update the fault contractor
	'   IN:             @TempFaultId 
	'   IN:             @ORGID
	'   IN:             @ITEMACTIONID
	'   IN:             @ITEMSTATUSID
	'	OUT:			@TmpFault
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/
	(
		@TempFaultId INT,
		@ORGID INT,	
		@ITEMACTIONID INT,	
		@ITEMSTATUSID INT,
		@TmpFault INT OUTPUT
	)
AS
	
	BEGIN TRAN
                              
		UPDATE FL_TEMP_FAULT
		SET 	
		ORGID = @ORGID,
		ITEMACTIONID=@ITEMACTIONID,
		ITEMSTATUSID=@ITEMSTATUSID		
		WHERE TempFaultID = @TempFaultId 

		If (@@RowCount = 0)
			BEGIN
				SET @TmpFault =-1
				ROLLBACK TRAN
				RETURN
			END
		
			
		SET @TmpFault = @TempFaultId
	COMMIT TRAN











GO
