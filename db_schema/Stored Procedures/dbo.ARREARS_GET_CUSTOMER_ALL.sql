SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- THIS PROCEDURE IS INCASE THEY WANT AN ARREARS SUMMARY
-- IT GIVES A FULL LIST OF ALL THE ARREARS FOR TENANTS AND ALSO PUTS THE HOUSING OFFICERS NAME IN

--execute ARREARS_GET_CUSTOMER_ALL ' AND P.HOUSINGOFFICER = 0','0.01','','','',''

CREATE    PROCEDURE [dbo].[ARREARS_GET_CUSTOMER_ALL] (@HOUSINGOFFICER VARCHAR(200),@COMPAREVALUE AS NVARCHAR(200),@ASATDATE AS VARCHAR(200),  @SCHEME_SQL AS VARCHAR(200) , @ACTION_SQL AS VARCHAR (200),@ORDERBY AS VARCHAR(200)) as
BEGIN   
   
SET NOCOUNT ON  
SET ANSI_NULL_DFLT_ON ON
SET ANSI_WARNINGS OFF

DECLARE @SQL_CLAUSE VARCHAR(300)
DECLARE @SQL_STR VARCHAR(8000)
DECLARE @THEDATE SMALLDATETIME
DECLARE @REQUIREDDATE SMALLDATETIME
DECLARE @CURRENTDATESTRING VARCHAR (40)
DECLARE @COMPAREDATE SMALLDATETIME
DECLARE @NEXTMONTHDATE SMALLDATETIME
DECLARE @RESPECTIVEENDDATE SMALLDATETIME
DECLARE @YEAREND SMALLDATETIME
DECLARE @YEARSTART SMALLDATETIME
DECLARE @HB_DR MONEY
DECLARE @TENANCYSTART SMALLDATETIME
DECLARE @LASTPAYMENTENDDATE SMALLDATETIME
DECLARE @ADJ_MONTH INT
DECLARE @NON_REQUIRED_DAYS INT
DECLARE @A_12TH_OF_A_YEAR FLOAT
DECLARE @TOTAL_HB_OWED_ON_DATE MONEY
DECLARE @HB_OWED MONEY
DECLARE @ADJ_YEARSTART SMALLDATETIME
DECLARE @ADJ_YEAREND SMALLDATETIME
DECLARE @MASTERAMOUNT MONEY
DECLARE @CUSTOMERID INT
DECLARE @TENANCYID INT
DECLARE @IN_CUSTOMERID INT
DECLARE @FULLNAME VARCHAR(200)

SET @MASTERAMOUNT = 0
SET @THEDATE = GETDATE()

 --  
CREATE TABLE #TBL_ESTHB (
TENANCYID  INT,
FULLNAME VARCHAR(1000), 
ESTHB MONEY, 
ACTIONDESC VARCHAR (800), 
JOURNALID INT, 
STARTDATE SMALLDATETIME,
HOUSENUMBER VARCHAR (100), 
ADDRESS1 VARCHAR (500), 
ADDRESS2 VARCHAR (500), 
TOWNCITY VARCHAR (200), 
POSTCODE VARCHAR (100), 
GROSSARREARS MONEY, 
TOTALARREARS MONEY, 
LASTPAYMENTDATE SMALLDATETIME,
HO VARCHAR(300),
PROPERTYCOUNT INT,
PROPERTY_TENANCIES INT,
PAYMENT_AGREEMENTS INT,
TOTALRENT MONEY
)
/*
CREATE TABLE TBL_ARREARSLIST_END_AUG (
TENANCYID  INT,
FULLNAME VARCHAR(1000), 
ESTHB MONEY, 
ACTIONDESC VARCHAR (800), 
JOURNALID INT, 
STARTDATE SMALLDATETIME,
HOUSENUMBER VARCHAR (100), 
ADDRESS1 VARCHAR (500), 
ADDRESS2 VARCHAR (500), 
TOWNCITY VARCHAR (200), 
POSTCODE VARCHAR (100), 
GROSSARREARS MONEY, 
TOTALARREARS MONEY, 
LASTPAYMENTDATE SMALLDATETIME,
HO VARCHAR(300)
)
*/
--   1. GET ALL THE ARREARS INTO THE TABLE


SET @SQL_STR = 'INSERT INTO #TBL_ESTHB 
			(
			ACTIONDESC,JOURNALID,TENANCYID,FULLNAME,STARTDATE,HOUSENUMBER,ADDRESS1,ADDRESS2,TOWNCITY,
			POSTCODE,GROSSARREARS,LASTPAYMENTDATE,HO,PROPERTYCOUNT,PROPERTY_TENANCIES,PAYMENT_AGREEMENTS,TOTALRENT
			)
			(
			SELECT 	ISNULL(VJ.DESCRIPTION,''''),VJ.JOURNALID,T.TENANCYID,
				ISNULL(CNG.LIST,''''),ISNULL(T.STARTDATE,''''),
				ISNULL(P.HOUSENUMBER,''''),
			 	ISNULL(P.ADDRESS1,''''),ISNULL(P.ADDRESS2,''''),
				ISNULL(P.TOWNCITY,''''),ISNULL(P.POSTCODE,''''),ISNULL(A.AMOUNT,0),
				( SELECT MAX(TRANSACTIONDATE) FROM F_RENTJOURNAL LEFT JOIN F_PAYMENTTYPE PY ON PY.PAYMENTTYPEID = PAYMENTTYPE WHERE TRANSACTIONDATE IS NOT NULL AND TENANCYID = T.TENANCYID AND STATUSID NOT IN (1,4) AND ITEMTYPE = 1 AND PY.RENTRECEIVED IS NOT NULL)
				,(E.FIRSTNAME + '' '' + E.LASTNAME) AS HO,
				ISNULL(PC.PROPERTYCOUNT,0) AS PROPERTYCOUNT,
				ISNULL(PCT.PROPERTYCOUNT,0) AS PROPERTY_TENANCIES,
				ISNULL(PA.PAYMENTAGREEMENT,0) AS PAYMENT_AGREEMENTS,
				F.TOTALRENT
			FROM C_TENANCY T
				INNER JOIN  C_CUSTOMER_NAMES_GROUPED_VIEW CNG ON T.TENANCYID = CNG.I
				LEFT JOIN (	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT,  
							I_J.TENANCYID  
						FROM F_RENTJOURNAL I_J  
							LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID 
							LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID  
							LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID 
						WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)   AND I_J.TRANSACTIONDATE <= getdate() 
						GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID  
			INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID
			INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID
			INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER
			LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID  
			LEFT JOIN (	SELECT J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION, LA.ACTIONID  
					FROM C_JOURNAL  J 
								INNER JOIN C_ARREARS AR ON AR.JOURNALID = J.JOURNALID
								INNER JOIN C_LETTERACTION LA ON LA.ACTIONID = AR.ITEMACTIONID AND AR.ITEMACTIONID NOT IN (12,13) 
							WHERE 	J.JOURNALID = (SELECT MAX(JOURNALID) FROM C_JOURNAL WHERE CUSTOMERID = J.CUSTOMERID)
								AND AR.ARREARSHISTORYID = (select max(ARREARSHISTORYID) from C_ARREARS where JOURNALID = J.JOURNALID) 
							GROUP BY J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION,LA.ACTIONID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID 				 
			LEFT JOIN (  	SELECT COUNT(P.PROPERTYID) AS PROPERTYCOUNT, P.HOUSINGOFFICER 
			     		FROM P__PROPERTY P GROUP BY P.HOUSINGOFFICER) PC ON PC.HOUSINGOFFICER = E.EMPLOYEEID
			LEFT JOIN (	SELECT 	COUNT(P.PROPERTYID) AS PROPERTYCOUNT,  
						P.HOUSINGOFFICER   
					FROM P__PROPERTY P  
						INNER JOIN C_TENANCY T ON T.PROPERTYID = P.PROPERTYID AND T.ENDDATE IS NULL 
							AND (((T.ENDDATE IS NULL OR T.ENDDATE  >= ''' + CAST(GETDATE() AS VARCHAR)+ ''') AND (T.STARTDATE  < ''' + CAST(GETDATE() AS VARCHAR)+ '''))) 
					GROUP BY P.HOUSINGOFFICER 
				  ) PCT ON PCT.HOUSINGOFFICER = E.EMPLOYEEID
			LEFT JOIN (	SELECT COUNT(J.JOURNALID) AS PAYMENTAGREEMENT, CT.TENANCYID  
					FROM C_CUSTOMERTENANCY CT  
						INNER JOIN C_JOURNAL J ON J.CUSTOMERID = CT.CUSTOMERID 
						INNER JOIN C_ARREARS AR ON AR.JOURNALID = J.JOURNALID 
						INNER JOIN C_LETTERACTION LA ON LA.ACTIONID = AR.ITEMACTIONID 
					WHERE J.CREATIONDATE <= ''' + CAST(GETDATE() AS VARCHAR)+ ''' AND LA.ACTIONID = 10  
						AND AR.ARREARSHISTORYID = (SELECT MAX(ARREARSHISTORYID) FROM C_ARREARS WHERE JOURNALID = J.JOURNALID)
					GROUP BY CT.TENANCYID) PA ON PA.TENANCYID = T.TENANCYID  
			WHERE 	((ISNULL(A.AMOUNT,0)) >=   ' + @COMPAREVALUE + ') 
				AND T.ENDDATE IS NULL 
				AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid) 
			)
			'

	EXECUTE (@SQL_STR)

  SET @SQL_STR = '	DECLARE CUSTOMERS_HB CURSOR 
	  	    	FAST_FORWARD 
		    	FOR 
				SELECT 	HBI.CUSTOMERID ,T.TENANCYID, HBI.initialSTARTDATE , ISNULL(HBA.ENDDATE,DATEADD(D,-1,HBI.INITIALSTARTDATE)),
					HB_DR = 
					CASE 
						WHEN HBA.HBID IS NOT NULL THEN ((ABS(HBA.HB)/ (DATEDIFF(D,HBA.STARTDATE,HBA.ENDDATE)+1)))
						WHEN HBA.HBID IS NULL THEN ((ABS(HBI.INITIALPAYMENT)/ (DATEDIFF(D,HBI.INITIALSTARTDATE,HBI.INITIALENDDATE)+1)))
					END					
				FROM C_TENANCY T
					INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID
					INNER JOIN F_HBINFORMATION HBI ON HBI.TENANCYID = T.TENANCYID
					LEFT JOIN F_HBACTUALSCHEDULE HBA ON HBA.HBID = HBI.HBID
									AND HBA.HBROW = (SELECT MAX(HBROW) FROM F_HBACTUALSCHEDULE WHERE HBID = HBA.HBID AND VALIDATED = 1)
					LEFT JOIN F_HBACTUALSCHEDULE HBA2 ON HBA2.HBID = HBI.HBID
									AND HBA2.HBROW = (SELECT min(HBROW) FROM F_HBACTUALSCHEDULE WHERE HBID = HBA2.HBID AND VALIDATED is null)
					
				WHERE  HBI.ACTUALENDDATE IS NULL 
					AND (((T.ENDDATE IS NULL OR T.ENDDATE  >= ''' + CAST( @THEDATE AS VARCHAR)+ ''') AND (T.STARTDATE  < ''' + CAST(@THEDATE AS VARCHAR) + ''')))   ' 

	  EXECUTE (@SQL_STR)	
		
	OPEN CUSTOMERS_HB
	FETCH 	NEXT FROM CUSTOMERS_HB INTO @CUSTOMERID,@TENANCYID, @TENANCYSTART,@LASTPAYMENTENDDATE, @HB_DR
	  WHILE 	@@FETCH_STATUS = 0
	     BEGIN
				set @MASTERAMOUNT = 0
				
				SET @REQUIREDDATE =  '1 ' + DATENAME(M, GETDATE()) + ' ' + CAST(DATEPART( YYYY, GETDATE()) AS VARCHAR)
				
				SET @CURRENTDATESTRING = '1 ' + DATENAME(M, @TENANCYSTART)
				
				SET @COMPAREDATE = @CURRENTDATESTRING + ' ' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)
				
				SET @NEXTMONTHDATE = DATEADD(M,1,@REQUIREDDATE)
				
				IF (@REQUIREDDATE) >= (@COMPAREDATE)
				 BEGIN
					SET @YEARSTART = @COMPAREDATE					
					SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1 , @YEARSTART)	
					SET @RESPECTIVEENDDATE = DATEADD(D, -1 , @RESPECTIVEENDDATE)
					SET @YEAREND = @RESPECTIVEENDDATE					
				
				 END
				ELSE
				 BEGIN
					SET @YEARSTART = @CURRENTDATESTRING + CAST((DATEPART( YYYY, GETDATE())-1) AS VARCHAR)
					SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1 , @YEARSTART)
					SET @RESPECTIVEENDDATE = DATEADD(D, -1 , @RESPECTIVEENDDATE)				
					SET @YEAREND = @RESPECTIVEENDDATE
				
				 END
				
				SET @ADJ_MONTH = DATEDIFF(M, @YEARSTART, GETDATE())+1
				
				IF (DATEADD(D,1,@LASTPAYMENTENDDATE)) < @YEARSTART
				 BEGIN
					
					SET @NON_REQUIRED_DAYS = 0
					SET @A_12TH_OF_A_YEAR = 0.0
					SET @TOTAL_HB_OWED_ON_DATE = 0
					SET @A_12TH_OF_A_YEAR = (DATEDIFF(D, @YEARSTART, @YEAREND)+1)
					set @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12							
					SET @HB_OWED = ((@ADJ_MONTH * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR
					
					SET @ADJ_YEARSTART = DATEADD(YYYY, -1, @YEARSTART)
					SET @ADJ_YEAREND = DATEADD(YYYY, -1, @YEAREND)
					
					--loop whilst we still have whole years to the LAST_PAYMENT_END
						WHILE (@LASTPAYMENTENDDATE < @ADJ_YEARSTART)
						 BEGIN
							SET @HB_OWED = @HB_OWED + (@HB_DR * (DATEDIFF(D, @ADJ_YEARSTART, @ADJ_YEAREND)+1))			
							SET @ADJ_YEARSTART = DATEADD(YYYY, -1, @ADJ_YEARSTART)
							SET @ADJ_YEAREND = DATEADD(YYYY, -1, @ADJ_YEAREND)

						  END		
						
						SET @NON_REQUIRED_DAYS = DATEDIFF(d, @ADJ_YEARSTART, @LASTPAYMENTENDDATE)
						
						SET @A_12TH_OF_A_YEAR = DATEDIFF(d, @ADJ_YEARSTART, @ADJ_YEAREND)+1
						SET @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR /12
						
						SET @TOTAL_HB_OWED_ON_DATE = @HB_OWED + (((12 * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR)
				 		
						SET @MASTERAMOUNT = @MASTERAMOUNT +  ISNULL(@TOTAL_HB_OWED_ON_DATE,0)
											
						
						SET @TOTAL_HB_OWED_ON_DATE = 0
						SET @HB_OWED = 0.00
				 END
				ELSE
				 BEGIN
					SET @A_12TH_OF_A_YEAR = 0.0
					SET @NON_REQUIRED_DAYS = DATEDIFF(D, @YEARSTART, @LASTPAYMENTENDDATE)+1
					
					SET @A_12TH_OF_A_YEAR = (DATEDIFF(D, @YEARSTART, @YEAREND)+1)
					set @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12
					SET @TOTAL_HB_OWED_ON_DATE = (((@ADJ_MONTH * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR)
					SET @MASTERAMOUNT = @MASTERAMOUNT + ISNULL(@TOTAL_HB_OWED_ON_DATE,0)
					
					SET @TOTAL_HB_OWED_ON_DATE = 0
										
				 END
			
			UPDATE #TBL_ESTHB SET ESTHB = ISNULL(ESTHB,0) + ISNULL(@MASTERAMOUNT,0) WHERE TENANCYID = @TENANCYID

			set @MASTERAMOUNT = 0
			FETCH NEXT FROM CUSTOMERS_HB INTO  @CUSTOMERID,@TENANCYID,@TENANCYSTART,@LASTPAYMENTENDDATE, @HB_DR
		END
	END
	CLOSE CUSTOMERS_HB
	DEALLOCATE CUSTOMERS_HB



SELECT  HO, 
	PROPERTYCOUNT, PROPERTY_TENANCIES, 
	COUNT(TENANCYID) AS ARREARS_CASES,
	SUM(TOTALRENT) AS TOTALRENT	,
	SUM(ISNULL(ESTHB,0)) AS ESTHB,
	(SUM(GROSSARREARS) + SUM(ISNULL(ESTHB,0))) AS THEVALUE,
	SUM(GROSSARREARS) AS GROSSARREARS,
	sum(PAYMENT_AGREEMENTS) as PAYMENT_AGREEMENTS 
FROM #TBL_ESTHB
GROUP BY HO, PROPERTYCOUNT,PROPERTY_TENANCIES
ORDER BY HO ASC

--select *  from #TBL_ESTHB where ho = 'Bob Prince'
--INSERT INTO TBL_ARREARSLIST_END_AUG (TENANCYID,FULLNAME, ESTHB ,ACTIONDESC,STARTDATE,	HOUSENUMBER, ADDRESS1,ADDRESS2,TOWNCITY,POSTCODE,GROSSARREARS, LASTPAYMENTDATE,HO )
--(SELECT TENANCYID,FULLNAME, ISNULL(ESTHB,0) AS ESTHB,ACTIONDESC,STARTDATE,HOUSENUMBER, ADDRESS1,ADDRESS2,TOWNCITY,POSTCODE,GROSSARREARS, LASTPAYMENTDATE,HO FROM #TBL_ESTHB)

DROP TABLE #TBL_ESTHB
GO
