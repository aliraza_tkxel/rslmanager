SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Fakhar uz zaman>
-- Create date: <Create Date,,15/03/2016>
-- Description:	<Returns all development documents on ComplianceDocument.aspx>
--USE [RSLBHALive]
--GO

--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_GetAllDevelopmentDocuments]
--		@type = -1,
--		@title = N'Select Document Title',
--		@dateType = N'Expiry',
--		@from = N'03/11/2000',
--		@to = N'03/11/2015',
--		@uploadedOrNot = N'Uploaded',
--		@searchedText = '16',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'By',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'

--SELECT	'Return Value' = @return_value

--GO

-- =============================================

IF OBJECT_ID('dbo.PDR_GetAllDevelopmentDocuments') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetAllDevelopmentDocuments AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE dbo.PDR_GetAllDevelopmentDocuments
(
	--These parameters will be used for search
	@category varchar(15) = 'Compliance', -- (Compliance or General) No need of Category for Development and Scheme
	@type int = -1,
	@title varchar(50) = '',
	@dateType varchar(10)='', --(Documented or Expiry)
	@from varchar(20) = '',
	@to varchar(20) = '',	
	@uploadedOrNot varchar(20)='Uploaded',		--(1=Uploaded & 0=Not Uploaded)
	@searchedText varchar(200)='',	
	
	--Parameters which would help in sorting and paging
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Type',
	@sortOrder varchar (5) = 'DESC',
	@getOnlyCount bit=0,	
	@totalCount int=0 output	
)
AS
BEGIN

		DECLARE @SelectClause nvarchar(MAX),
        @fromClause   nvarchar(MAX),
        @whereClause  nvarchar(MAX),	        
        @orderClause  nvarchar(MAX),	
        @mainSelectQuery nvarchar(MAX),        
        @rowNumberQuery nvarchar(MAX),
        @finalQuery nvarchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria nvarchar(MAX),
        
        --variables for paging
        @offset int,
		@limit int,
		@documentType nvarchar(500)='--',
		@documentSubType nvarchar(500)='--'
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
        		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided

	SET @searchCriteria =  ' 1=1 '		
	
		--CHECK UPLOADED OR NOT
	IF(@uploadedOrNot = 'NotUploaded' )--Not Uploaded. Fetch only Developments. 
		
		BEGIN
			IF(@type > 0)
			BEGIN
				Select @documentType = Title from P_Documents_Type where DocumentTypeId = @type
			END
			
			IF(@title <> 'Select Document Title')
			BEGIN
				Select @documentSubType = Title from P_Documents_SubType where Title=@title
			END
				
			--========================================================================================	
			IF(@searchedText <> '' )
	
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (DEV.POSTCODE LIKE ''%'+@searchedText+'%'' 				
				OR DEV.DEVELOPMENTNAME LIKE ''%'+@searchedText+'%'' 
				OR (ISNULL(DEV.ADDRESS1,'''') + ISNULL('' ''+DEV.ADDRESS2,'''') + ISNULL('' ''+DEV.TOWN,'''')+ ISNULL('' ''+DEV.COUNTY,'''') LIKE ''%'+@searchedText+'%'''   
				+'))'
			END
			
			-- Begin building SELECT clause
			
			SET @SelectClause = 'SELECT  top ('+convert(varchar(10),@limit)+')
								0 AS DocumentId,
								''--'' AS Added,
								''--'' AS AddedSort,
								'''+@documentType+'''  AS ''Type'',
								'''+@documentSubType+'''  AS Title,
								''--'' AS "By",	
								''--'' AS SortWithBy,								
								''--'' AS Document,
								''--'' AS DocumentSort,
								''--'' as Expiry,
								''--'' AS ExpirySort,
								ISNULL(DEV.DEVELOPMENTNAME,''--'')AS Address,
								ISNULL(DEV.PostCode,''--'') AS Postcode'
			
			-- End building SELECT clause
			-- Begin building FROM clause

				SET @fromClause = CHAR(10) + 'FROM dbo.PDR_DEVELOPMENT AS Dev '
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  Dev.DEVELOPMENTID NOT IN(Select DISTINCT Dev.DEVELOPMENTID from PDR_DEVELOPMENT Dev
				Inner join P_DEVELOPMENTDOCUMENTS DD on DD.DevelopmentId = DEV.DEVELOPMENTID
				Inner join PDR_DOCUMENTS Doc on doc.DocumentID = DD.DocumentId where (DocumentTypeId = '+ CONVERT(VARCHAR(10),@type) +' OR '+ CONVERT(VARCHAR(10),@type) +' = -1) AND  (Title = '''+ CONVERT(VARCHAR(10),@documentSubType) +''' OR '''+ CONVERT(VARCHAR(10),@documentSubType) +'''= ''--'') )'

				-- End building From clause		        
		END 
		
	ELSE	-- Uploaded Documents
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND (ALLDOCS.DOCUMENTNAME <> '''' OR ALLDOCS.DOCUMENTNAME IS NOT NULL) '
		
		
			IF(@searchedText <> '')
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (DEV.POSTCODE LIKE ''%'+@searchedText+'%'' 
				OR DOCTYPE.Title LIKE ''%'+@searchedText+'%''  
				OR ALLDOCS.Title LIKE ''%'+@searchedText+'%'' 
				OR EMP.LASTNAME LIKE ''%'+@searchedText+'%'' 
				OR DEV.DEVELOPMENTNAME LIKE ''%'+@searchedText+'%'' 
				OR (ISNULL(DEV.ADDRESS1,'''') + ISNULL('' ''+DEV.ADDRESS2,'''') + ISNULL('' ''+DEV.TOWN,'''')+ ISNULL('' ''+DEV.COUNTY,'''') LIKE ''%'+@searchedText+'%'''   
				+'))'
			END		

			--Check DropDown Filters				
			
			IF(@category <> '' OR @category IS NOT NULL)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  ALLDOCS.Category = '''+ @category + '''' + CHAR(10)
			END
					
			IF(@type > 0)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  ALLDOCS.DocumentTypeId = '+ CONVERT(VARCHAR(10),@type) + CHAR(10)
			END
					
				
			IF(@title <> '' AND @title <> 'Select Document Title' AND @title <> '-1')
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  ALLDOCS.Title = '''+ @title + '''' + CHAR(10)
			END
						
			IF(@dateType <> '') -- Document/Expiry
			BEGIN
					IF(@dateType = 'Document')
					BEGIN
						IF(@to <> ''  AND @from <> '' ) --only if both of i.e. From & To dates are there.
						BEGIN
							SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ALLDOCS.DocumentDate BETWEEN ' + ''''+ CONVERT(VARCHAR(12),@from,103)+ ''''  + CHAR(10) 
													+ ' AND ' + ''''+CONVERT(VARCHAR(12),@to,103) + '''' + CHAR(10)	 													
						END
					END																			
					ELSE	--@dateType = 'Expiry'
					BEGIN
						IF(@to !='' AND @from !='') --only if both of i.e. From & To dates are there.
						BEGIN
							SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ALLDOCS.Expires BETWEEN ' + ''''+CONVERT(VARCHAR(12),@from,103) + ''''  + CHAR(10) 
													+ ' AND ' + ''''+CONVERT(VARCHAR(12),@to,103) + '''' + CHAR(10)	
						END
							
					END
			END
			
					
								
				-- End building SearchCriteria clause   
				--========================================================================================
		--print('Search Criteria : '+@searchCriteria)
				SET NOCOUNT ON;
				--========================================================================================	        
				-- Begin building SELECT clause
				
				SET @SelectClause = 'SELECT  top ('+convert(varchar(10),@limit)+')
									ALLDOCS.DocumentId As DocumentId,
									CONVERT(VARCHAR(12),ALLDOCS.UploadedDate,103) AS Added,
									ALLDOCS.UploadedDate AS AddedSort,
									CONVERT(VARCHAR(200),DOCTYPE.Title,103) AS Type,
									ALLDOCS.Title AS Title,
									ISNULL(EMP.FIRSTNAME,'''') + ISNULL(EMP.LASTNAME,'''') "By",
									ISNULL(EMP.FIRSTNAME,'''') AS SortWithBy,
									ISNULL(CONVERT(VARCHAR(12),ALLDOCS.DocumentDate,103),''--'') AS Document,
									ALLDOCS.DocumentDate AS DocumentSort,
									ISNULL(CONVERT(VARCHAR(12),ALLDOCS.Expires,103),''--'') AS Expiry,
									ALLDOCS.Expires AS ExpirySort,
									ISNULL(DEV.DEVELOPMENTNAME,''--'')AS Address,
									ISNULL(DEV.PostCode,''--'') AS Postcode
									 '
				
				-- End building SELECT clause
				--======================================================================================== 	
		--print('@SelectClause' + @SelectClause)
				--========================================================================================    
				-- Begin building FROM clause
				
				SET @fromClause = CHAR(10) + 'FROM  dbo.PDR_DOCUMENTS ALLDOCS  
											INNER JOIN dbo.P_DEVELOPMENTDOCUMENTS DEVDOCS ON ALLDOCS.DocumentID = DEVDOCS.DocumentID
											INNER JOIN dbo.PDR_DEVELOPMENT DEV ON DEV.DevelopmentId =  DEVDOCS.DevelopmentId
											LEFT JOIN dbo.P_DOCUMENTS_TYPE AS DOCTYPE ON DOCTYPE.DocumentTypeId = ALLDOCS.DocumentTypeId
											--LEFT JOIN dbo.P_DOCUMENTS_SUBTYPE AS DOCSUBTYPE ON DOCSUBTYPE.Title = ALLDOCS.Title
											LEFT JOIN dbo.E__EMPLOYEE EMP ON EMP.EMPLOYEEID = ALLDOCS.CreatedBy
											'
				
				-- End building From clause
END		
		--========================================================================================
--print('@fromClause' + @fromClause)
		--========================================================================================    
		-- Begin building OrderBy clause						
		
		IF(@sortColumn = 'By')
		BEGIN
			SET @sortColumn = 'SortWithBy'
		END
		IF(@sortColumn = 'Added')
		BEGIN
			SET @sortColumn = 'AddedSort'
		END
		
				
		IF(@sortColumn = 'Document')
		BEGIN
			SET @sortColumn = 'DocumentSort'
		END
		
		IF(@sortColumn = 'Expiry')
		BEGIN
			SET @sortColumn = 'ExpirySort'
		END
		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder				
			
		
		
		-- End building OrderBy clause
		--========================================================================================															  
	
		--========================================================================================
		-- Begin building WHERE clause
	    			  				
		SET @whereClause =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
--print('@whereClause' + @whereClause)		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			


		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================	
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================											

	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================				

		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================									
				
END
GO
