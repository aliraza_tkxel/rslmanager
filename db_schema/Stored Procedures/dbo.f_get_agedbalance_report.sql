SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- WHERE ARE THE COMMENTS IN THIS STORED PROCEDURE !!!

--EXEC F_GET_AGEDBALANCE_REPORT 1,7
CREATE     PROC [dbo].[f_get_agedbalance_report](@GETSUM AS BIT,@YRANGE INT)
AS
BEGIN	
	DECLARE @YSTART SMALLDATETIME
	DECLARE @YEND SMALLDATETIME
	DECLARE @VIEWBY AS INT
	--SET @VIEWBY=3
	--SET @YSTART = '1/APR/2004'
	--SET @YEND = '31/MAR/2005'
	
	SELECT @YSTART=YSTART,@YEND=YEND FROM F_FISCALYEARS WHERE YRANGE=@YRANGE 
	IF @GETSUM=0
	BEGIN 
		SELECT SA.TENANCYID, SA.SUPPLIERID, SA.EMPLOYEEID, SA.SALESCUSTOMERID,
		CASE 
			WHEN SA.TENANCYID IS NOT NULL THEN CNGV.LIST
			WHEN SA.SUPPLIERID IS NOT NULL THEN O.[NAME]
			WHEN SA.EMPLOYEEID IS NOT NULL THEN  E.FIRSTNAME + ' ' + E.LASTNAME
			WHEN SA.SALESCUSTOMERID IS NOT NULL THEN  SC.CONTACT
		END AS CNAME,
		CASE 
			WHEN SA.TENANCYID IS NOT NULL THEN 'TENANT'
			WHEN SA.SUPPLIERID IS NOT NULL THEN 'SUPPLIER'
			WHEN SA.EMPLOYEEID IS NOT NULL THEN  'EMPLOYEE'
			WHEN SA.SALESCUSTOMERID IS NOT NULL THEN  'SALES CUSTOMER'
		END AS CAT,
		ISNULL(SUM(GROSSCOST),0) AS INVOIVEAMT, ISNULL(SUM(PAYMENTS),0) AS PAYMENT, ISNULL(SUM(OB),0) AS OPENINGBALANCE ,
		ISNULL(SUM(GROSSCOST)+SUM(OB)+SUM(PAYMENTS),0) AS OUTSTANDING
		FROM (
			--sales invoices from this year
			SELECT 	SI.TENANCYID, SI.EMPLOYEEID, SI.SUPPLIERID, SI.SALESCUSTOMERID, GROSSCOST, 0 AS PAYMENTS, 0 AS OB
		 	FROM 	F_SALESINVOICE SI
		  		INNER JOIN F_SALESINVOICEITEM SII ON SII.SALEID = SI.SALEID AND SII.ACTIVE = 1
		 	WHERE 	SIDATE >= @YSTART AND SIDATE <= @YEND
			   UNION ALL
			--sales payments this year
			SELECT SP.TENANCYID, SP.EMPLOYEEID, SP.SUPPLIERID, SP.SALESCUSTOMERID, 0, AMOUNT, 0
			FROM 	F_SALESPAYMENT SP
		 	WHERE 	ITEMTYPE <> 12 AND TRANSACTIONDATE >= @YSTART AND TRANSACTIONDATE <= @YEND
			   UNION ALL
			--sales credit notes
			SELECT SCNINV.TENANCYID, SCNINV.EMPLOYEEID, SCNINV.SUPPLIERID, SCNINV.SALESCUSTOMERID, 0, -GROSSCOST, 0
			FROM F_SALESCREDITNOTE SCN
				INNER JOIN F_SALESCREDITNOTEITEM SCNI ON SCNI.SCNID = SCN.SCNID
				INNER JOIN F_SALESINVOICE SCNINV ON SCNINV.SALEID = SCN.INVOICEID
		 	WHERE 	SCN.SCNDATE >= @YSTART AND SCN.SCNDATE <= @YEND
			   UNION ALL
			--opening balances
			SELECT 	TENANCYID, EMPLOYEEID, SUPPLIERID, SALESCUSTOMERID, 0, 0, AMOUNT 
			FROM (
				--recharges o/b
		 		SELECT 	SP.TENANCYID, SP.EMPLOYEEID, SP.SUPPLIERID, SP.SALESCUSTOMERID, AMOUNT
		  		FROM 	F_SALESPAYMENT SP
		  		WHERE 	ITEMTYPE = 12 AND TRANSACTIONDATE >= @YSTART AND  TRANSACTIONDATE <= @YEND
		 		UNION ALL
				--sales invoices from before this year
		 		SELECT 	SI.TENANCYID, SI.EMPLOYEEID, SI.SUPPLIERID,  SI.SALESCUSTOMERID, GROSSCOST
		  		FROM 	F_SALESINVOICE SI
		   			INNER JOIN F_SALESINVOICEITEM SII ON SII.SALEID = SI.SALEID AND SII.ACTIVE = 1
		  		WHERE  SIDATE < @YSTART
		 		UNION ALL
				--sales payments from before this year
		 		SELECT 	SP.TENANCYID, SP.EMPLOYEEID, SP.SUPPLIERID, SP.SALESCUSTOMERID, AMOUNT
		  		FROM 	F_SALESPAYMENT SP
		  		WHERE 	TRANSACTIONDATE < @YSTART
				UNION ALL
				--sales credit notes from before this year
				SELECT SCNINV.TENANCYID, SCNINV.EMPLOYEEID, SCNINV.SUPPLIERID, SCNINV.SALESCUSTOMERID, -GROSSCOST
				FROM F_SALESCREDITNOTE SCN
					INNER JOIN F_SALESCREDITNOTEITEM SCNI ON SCNI.SCNID = SCN.SCNID
					INNER JOIN F_SALESINVOICE SCNINV ON SCNINV.SALEID = SCN.INVOICEID
		 		WHERE SCN.SCNDATE < @YSTART
		 		) OB
		    ) SA
		--labelling info
		LEFT JOIN C_TENANCY T ON T.TENANCYID = SA.TENANCYID 
		LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = SA.TENANCYID 
		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = SA.EMPLOYEEID 
		LEFT JOIN S_ORGANISATION O ON O.ORGID = SA.SUPPLIERID 
		LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = SA.SALESCUSTOMERID
		GROUP BY SA.TENANCYID, SA.SUPPLIERID, SA.EMPLOYEEID,SA.SALESCUSTOMERID, SC.CONTACT, CNGV.LIST,O.[NAME], E.FIRSTNAME, E.LASTNAME
		--rder by sa.tenancyid
		-- BELOW HAVING INCLUDED BY PAUL TO REMOVE ROWS WUTH ZERO BALANCE
		HAVING ISNULL(SUM(GROSSCOST)+SUM(OB)+SUM(PAYMENTS),0) <> 0
 	END
 	IF @GETSUM=1
	BEGIN 
		
		SELECT ISNULL(SUM(GROSSCOST),0) AS INVOIVEAMTSUM,
			ISNULL(SUM(PAYMENTS),0) AS PAYMENTSUM,
			ISNULL(SUM(OB),0) AS OPENINGBALANCESUM,
			ISNULL(SUM(GROSSCOST) + SUM(OB) + SUM(PAYMENTS),0) AS OUTSTANDINGSUM
		FROM (
			SELECT 	ISNULL(SUM(GROSSCOST),0) AS GROSSCOST, ISNULL(SUM(PAYMENTS),0) AS PAYMENTS, ISNULL(SUM(OB),0) AS OB
				FROM (
					SELECT 	SI.TENANCYID, SI.EMPLOYEEID, SI.SUPPLIERID, SI.SALESCUSTOMERID, GROSSCOST, 0 AS PAYMENTS, 0 AS OB
				 	FROM 	F_SALESINVOICE SI
				  		INNER JOIN F_SALESINVOICEITEM SII ON SII.SALEID = SI.SALEID AND SII.ACTIVE = 1
				 	WHERE 	SIDATE >= @YSTART AND SIDATE <= @YEND
					   UNION ALL
					SELECT SP.TENANCYID, SP.EMPLOYEEID, SP.SUPPLIERID, SP.SALESCUSTOMERID, 0, AMOUNT, 0
					FROM 	F_SALESPAYMENT SP
				 	WHERE 	ITEMTYPE <> 12 AND TRANSACTIONDATE >= @YSTART AND TRANSACTIONDATE <= @YEND
					   UNION ALL
					SELECT 	TENANCYID, EMPLOYEEID, SUPPLIERID, SALESCUSTOMERID, 0, 0, AMOUNT 
					FROM (
				 		SELECT 	SP.TENANCYID, SP.EMPLOYEEID, SP.SUPPLIERID, SP.SALESCUSTOMERID,  AMOUNT
				  		FROM 	F_SALESPAYMENT SP
				  		WHERE 	ITEMTYPE = 12 AND TRANSACTIONDATE >= @YSTART AND TRANSACTIONDATE <= @YEND
				 		   UNION ALL
				 		SELECT 	SI.TENANCYID, SI.EMPLOYEEID, SI.SUPPLIERID, SI.SALESCUSTOMERID, GROSSCOST
				  		FROM 	F_SALESINVOICE SI
				   			INNER JOIN F_SALESINVOICEITEM SII ON SII.SALEID = SI.SALEID AND SII.ACTIVE = 1
				  		WHERE  SIDATE < @YSTART
				 		   UNION ALL
				 		SELECT 	SP.TENANCYID, SP.EMPLOYEEID, SP.SUPPLIERID, SP.SALESCUSTOMERID, AMOUNT
				  		FROM 	F_SALESPAYMENT SP
				  		WHERE 	TRANSACTIONDATE < @YSTART	
				 	      ) OB
				    ) SA
				GROUP BY SA.TENANCYID, SA.SUPPLIERID, SA.EMPLOYEEID
				-- BELOW HAVING INCLUDED BY PAUL TO REMOVE ROWS WUTH ZERO BALANCE
				HAVING ISNULL(SUM(GROSSCOST)+SUM(OB)+SUM(PAYMENTS),0) <> 0
			     ) MEGATOTAL

		--GROUP BY SA.TENANCYID, SA.SUPPLIERID, SA.EMPLOYEEID,CNGV.LIST,O.[NAME], E.FIRSTNAME, E.LASTNAME
 	END
END
GO
