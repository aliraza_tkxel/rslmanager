
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Adeel Ahmed>
-- Create date: <May 10, 2010>
-- Description:	<Payment Plan Summary>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetPaymentPlanSummary]
	-- Add the parameters for the stored procedure here
	@TenantId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AM_PaymentPlan.StartDate,AM_PaymentPlan.EndDate,AM_LookupCode.CodeName,CONVERT(DECIMAL(30,2),AM_PaymentPlan.AmountToBeCollected),
		   CONVERT(DECIMAL(30,2),AM_PaymentPlan.WeeklyRentAmount),AM_PaymentPlan.ReviewDate,AM_PaymentPlan.FirstCollectionDate,
		   CONVERT(DECIMAL(30,2),AM_PaymentPlan.RentBalance) 
	From AM_PaymentPlan
	INNER JOIN AM_LookupCode on AM_PaymentPlan.FrequencyLookupCodeId = AM_LookupCode.LookupCodeId
	Where AM_PaymentPlan.TennantId = @TenantId and AM_PaymentPlan.IsCreated = 'true'

END




GO
