USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FLS_GetAvailableOperativesLeaves]    Script Date: 11/29/2016 17:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON  
GO
/* =============================================
--EXEC	[dbo].[FLS_GetAvailableOperativesLeaves]
--		@startDate = N'' 
-- Author:		Rehan Baber
-- Create date: Create Date,,18 Jan,2018
-- Description:	This stored procedure fetch  the employees and their leaves.
-- ============================================= */

IF OBJECT_ID('dbo.FLS_GetAvailableOperativesLeaves') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FLS_GetAvailableOperativesLeaves AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FLS_GetAvailableOperativesLeaves] 
	-- Add the parameters for the stored procedure here
	@startDate as datetime,
	@searchText as nvarchar(max)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @operativeIdStr NVARCHAR(MAX)
	
	--=================================================================================================================
	------------------------------------------------------ Step 0------------------------------------------------------
	--Create Temporary Table
	IF 1 = 2
  BEGIN
	SELECT DISTINCT
	CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME)																										AS StartDate
	,CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME)																										AS EndDate
	,0																																					AS OperativeId
	,'F'																																				AS HolType
	,Cast(1 as DOUBLE PRECISION)																														AS duration
	,'00:00 AM'																																			AS StartTime
	,'11:59 PM'																																			AS EndTime
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin
	WHERE
    1 = 2 
  END
	
    DECLARE @AvailableOperatives TABLE (
		EmployeeId int
		,FirstName nvarchar(50)
		,LastName nvarchar(50)
		,FullName nvarchar(100)		
		,PatchId int
		,PatchName nvarchar(20)		
		,Distance nvarchar(10)
		,TradeId INT				
	)

	DECLARE @AbsentOperatives TABLE (
		EMPLOYEEID int			
	)

	INSERT INTO @AbsentOperatives(EmployeeId)
	SELECT 
		EMPLOYEEID 
	FROM E_JOURNAL
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
	AND E_ABSENCE.ABSENCEHISTORYID IN (
										SELECT 
											MAX(ABSENCEHISTORYID) 
											FROM E_ABSENCE 
											GROUP BY JOURNALID
										)
	WHERE ITEMNATUREID = 1
	AND E_ABSENCE.RETURNDATE IS NULL
	AND E_ABSENCE.ITEMSTATUSID = 1
	UNION ALL	
			SELECT DISTINCT
				E_JOURNAL.EMPLOYEEID
			FROM E_JOURNAL
			INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
				AND E_ABSENCE.ABSENCEHISTORYID IN (
													SELECT 
														MAX(ABSENCEHISTORYID) 
														FROM E_ABSENCE 
														GROUP BY JOURNALID
													)
			INNER JOIN E_STATUS ON  E_ABSENCE.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
			INNER JOIN E_NATURE ON  E_JOURNAL.ITEMNATUREID = E_NATURE.ITEMNATUREID
			INNER JOIN E_JOBDETAILS ej ON E_JOURNAL.EMPLOYEEID = ej.EMPLOYEEID
			LEFT JOIN E_HOLIDAYRULE hl ON ej.HOLIDAYRULE = hl.EID
			WHERE E_ABSENCE.startdate <= @startDate and 
					E_ABSENCE.RETURNDATE >= @startDate and
				E_NATURE.ITEMNATUREID in (2,3,4,5,7,8,9,10,11,12,13,14,15,16,32,48,43,52,53,54)
				AND E_ABSENCE.ITEMSTATUSID = 3 and
				ej.ISBRS=1
	 --Trainings
	UNION ALL  
		SELECT  
		EET.EmployeeId
	FROM E_EmployeeTrainings EET    
	INNER JOIN E_JOBDETAILS ej ON EET.EMPLOYEEID = ej.EMPLOYEEID  
	WHERE EET.startdate <= @startDate and 
		  EET.ENDDATE >= @startDate AND EET.Active=1 AND EET.Status in (select StatusId from E_EmployeeTrainingStatus where Title = 'Requested' OR Title = 'Manager Supported' OR Title = 'Exec Supported' OR Title = 'Exec Approved' OR Title = 'HR Approved')
	DECLARE @InsertClause varchar(200)
	DECLARE @SelectClause varchar(800)
	DECLARE @FromClause varchar(800)
	DECLARE @WhereClause varchar(max)		
	DECLARE @MainQuery varchar(max)
	--=================================================================================================================
	------------------------------------------------------ Step 1------------------------------------------------------
	--This query fetches the employees which matches with the following criteria
	--Trade of employee is same as trade of component 
	--User type is M&E Servicing or Cyclic Maintenance		
	--Exclude the sick leaves
	INSERT INTO @AvailableOperatives(EmployeeId,FirstName,LastName,FullName, PatchId, PatchName)
	SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + ' '+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	FROM   E__EMPLOYEE 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	INNER JOIN E_JOBROLETEAM ON E__EMPLOYEE.JobRoleTeamId = E_JOBROLETEAM.JobRoleTeamId
	INNER JOIN E_TEAM ON E_JOBROLETEAM.TeamId = E_TEAM.TEAMID
	INNER JOIN E_JOBROLE ON E_JOBROLETEAM.JobRoleId = E_JOBROLE.JobRoleId
	LEFT JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID
	WHERE E_JOBDETAILS.Active=1 
	AND E_TEAM.TEAMNAME = 'Housing Team'
	AND E_TEAM.ACTIVE = 1
	--AND E_JOBROLE.JobeRoleDescription = 'Neighbourhood Officer'
	AND (@searchText = '' or @searchText is null or E__EMPLOYEE.FIRSTNAME like @searchText or E__EMPLOYEE.LASTNAME like @searchText or E__EMPLOYEE.FIRSTNAME + E__EMPLOYEE.LASTNAME like REPLACE(@searchText,' ', ''))
	AND E__EMPLOYEE.EmployeeId NOT IN (
										SELECT EMPLOYEEID FROM @AbsentOperatives
									  )		
	
	--=================================================================================================================
	------------------------------------------------------ Step 3------------------------------------------------------						
	--This query selects the leaves of employees i.e the employess which we get in step1
	--M : morning - 08:00 AM - 12:00 PM
	--A : mean after noon - 01:00 PM - 05:00 PM
	--F : Single full day
	--F-F : Multiple full days
	--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
	--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days
	
	
	SELECT E_ABSENCE.STARTDATE as StartDate
	,CASE
		WHEN
			(E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.HOLTYPE NOT IN ('M','F-M','A-M')
			AND E_ABSENCE.RETURNDATE IS NOT NULL AND DATEDIFF(day,E_ABSENCE.STARTDATE,E_ABSENCE.RETURNDATE)>0) THEN 
			DATEADD(DD,-1,E_ABSENCE.RETURNDATE)	
		ELSE
			E_ABSENCE.RETURNDATE 
		END 	
	AS EndDate
	, E_JOURNAL.employeeid as OperativeId
	,E_ABSENCE.HolType as HolType
	,E_ABSENCE.duration as Duration
	,CASE
		WHEN (E_ABSENCE.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE
			.STARTDATE)))
			THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, E_ABSENCE.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
		WHEN HolType = '' or HOLTYPE is null 
			THEN '08:00 AM'
		WHEN HolType = 'M'
			THEN '08:00 AM'
		WHEN HolType = 'A'
			THEN '01:00 PM'
		WHEN HolType = 'F'
			THEN '00:00 AM'
		WHEN HolType = 'F-F'
			THEN '00:00 AM'
		WHEN HolType = 'F-M'
			THEN '00:00 AM'
		WHEN HolType = 'A-F'
			THEN '01:00 PM'
		WHEN HolType = 'A-M'
			THEN '01:00 PM'
	END						AS StartTime
	,CASE
			WHEN
				(E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.HOLTYPE NOT IN ('M','F-M','A-M') AND E_ABSENCE.RETURNDATE IS NOT NULL 
				AND DATEDIFF(day,E_ABSENCE.STARTDATE,E_ABSENCE.RETURNDATE)>0) THEN

				CASE WHEN (DATEADD(DD,-1,E_ABSENCE.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,E_ABSENCE.RETURNDATE))))
					THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 0), 8)), 'AM', ' AM'), 'PM', ' PM')
				WHEN HolType = '' or HOLTYPE is null 
					THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00'
				WHEN HolType = 'M'
					THEN '12:00 PM'
				WHEN HolType = 'A'
					THEN '05:00 PM'
				WHEN HolType = 'F'
					THEN '11:59 PM'
				WHEN HolType = 'F-F'
					THEN '11:59 PM'
				WHEN HolType = 'F-M'
					THEN '12:00 PM'
				WHEN HolType = 'A-F'
					THEN '11:59 PM'	
			END 
		 ELSE
			CASE
				WHEN
					(E_ABSENCE.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE
					.RETURNDATE)))
					THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, E_ABSENCE.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
				WHEN HolType = '' or HOLTYPE is null 
					THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00'
				WHEN HolType = 'M'
					THEN '12:00 PM'
				WHEN HolType = 'A'
					THEN '05:00 PM'
				WHEN HolType = 'F'
					THEN '11:59 PM'
				WHEN HolType = 'F-F'
					THEN '11:59 PM'
				WHEN HolType = 'F-M'
					THEN '12:00 PM'
				WHEN HolType = 'A-F'
					THEN '11:59 PM'
				WHEN HolType = 'A-M'
					THEN '12:00 PM'		
			END	
		END			AS EndTime
	,CASE
		WHEN
			(E_ABSENCE.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.STARTDATE
			)))
			THEN DATEDIFF(mi, '1970-01-01', E_ABSENCE.STARTDATE)
		WHEN HolType = '' or HOLTYPE is null 
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '08:00 AM', 103))
		WHEN HolType = 'M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '08:00 AM', 103))
		WHEN HolType = 'A'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '01:00 PM', 103))
		WHEN HolType = 'F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103), 103))
		WHEN HolType = 'F-F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103), 103))
		WHEN HolType = 'F-M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103), 103))
		WHEN HolType = 'A-F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '01:00 PM', 103))
		WHEN HolType = 'A-M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), E_ABSENCE.StartDate, 103) + ' ' + '01:00 PM', 103))
	END						AS StartTimeInMin
	,CASE
		WHEN
			(E_JOURNAL.ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.HOLTYPE NOT IN ('M','F-M','A-M') AND E_ABSENCE.RETURNDATE IS NOT NULL 
			AND DATEDIFF(day,E_ABSENCE.STARTDATE,E_ABSENCE.RETURNDATE)>0) THEN
			CASE
				WHEN
					(DATEADD(DD,-1,E_ABSENCE.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,E_ABSENCE.RETURNDATE))))
					THEN DATEDIFF(mi, '1970-01-01', DATEADD(DD,-1,E_ABSENCE.RETURNDATE))
				WHEN HolType = '' or HOLTYPE is null 
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00', 103))
				WHEN HolType = 'M'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
				WHEN HolType = 'A'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '05:00 PM', 103))
				WHEN HolType = 'F'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
				WHEN HolType = 'F-F'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
				WHEN HolType = 'F-M'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
				WHEN HolType = 'A-F'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,E_ABSENCE.RETURNDATE), 103) + ' ' + '11:59 PM', 103))	
			END 	
		ELSE
			CASE
				WHEN
					(E_ABSENCE.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.RETURNDATE)))
					THEN DATEDIFF(mi, '1970-01-01', E_ABSENCE.RETURNDATE)
				WHEN HolType = '' or HOLTYPE is null 
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00', 103))
				WHEN HolType = 'M'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
				WHEN HolType = 'A'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
				WHEN HolType = 'F'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
				WHEN HolType = 'F-F'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
				WHEN HolType = 'F-M'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
				WHEN HolType = 'A-F'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
				WHEN HolType = 'A-M'
					THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))						
			END	
		END	AS EndTimeInMin
	FROM E_JOURNAL 
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID  
	AND E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID)
	INNER JOIN E_JOBDETAILS  ON E_JOURNAL.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
    LEFT JOIN E_HOLIDAYRULE hl ON E_JOBDETAILS.HOLIDAYRULE = hl.EID
	WHERE
	(
		-- To filter for M&E Servicing or Cyclic Maintenance i.e annual leaves etc. where approval is needed
		( E_ABSENCE.ITEMSTATUSID = 5 AND itemnatureid >= 2 and	itemnatureid <> 47)
		-- To filter for sickness leaves. where the operative is now returned to work.
		OR
		(ITEMNATUREID = 1 AND E_ABSENCE.ITEMSTATUSID = 2 AND E_ABSENCE.RETURNDATE IS NOT NULL )
		OR (E_ABSENCE.ItemStatusId =3 AND ITEMNATUREID<>47 AND E_JOBDETAILS.ISBRS=1)
	)
	AND E_ABSENCE.RETURNDATE >= CONVERT(DATE,@startDate)
	AND E_JOURNAL.employeeid in(SELECT employeeid FROM  @AvailableOperatives)	
-- Check for bank holidays	
 UNION ALL SELECT DISTINCT
	CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS StartDate
	,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS EndDate
	,o.EmployeeId																																		AS OperativeId
	,'F'																																				AS HolType
	,1																																					AS duration
	,'00:00 AM'																																			AS StartTime
	,'11:59 PM'																																			AS EndTime
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin
FROM
	G_BANKHOLIDAYS bh
		CROSS JOIN @AvailableOperatives o
WHERE
	bh.BHDATE >= CONVERT(DATE, GETDATE())	
	AND bh.BHA = 1
	  -- Check for Trainings
 UNION ALL  
		SELECT  
		 CAST(CAST(EET.StartDate AS DATE) AS SMALLDATETIME)																									AS StartDate
		,CAST(CAST(EET.EndDate AS DATE) AS SMALLDATETIME)																									AS EndDate
		,EET.EmployeeId																																		AS OperativeId
		,'F'																																				AS HolType
		,EET.TotalNumberOfDays																																AS duration
		,'00:00 AM'																																			AS StartTime
		,'11:59 PM'																																			AS EndTime
		,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(EET.StartDate AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
		,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(EET.EndDate AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin    
	FROM E_EmployeeTrainings EET    
	INNER JOIN E_JOBDETAILS ej ON EET.EMPLOYEEID = ej.EMPLOYEEID  
	WHERE CONVERT(DATE, EET.EndDate, 103) >= CONVERT(DATE, GETDATE(), 103) AND EET.Active=1 AND EET.Status in (select StatusId from E_EmployeeTrainingStatus where Title = 'Requested' OR Title = 'Manager Supported' OR Title = 'Exec Supported' OR Title = 'Exec Approved' OR Title = 'HR Approved') 
	ORDER by OperativeId, StartDate	
END