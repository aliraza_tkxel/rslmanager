USE [RSLBHALive]

GO
IF OBJECT_ID('dbo.[CM_UpdateWorksCompleted]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[CM_UpdateWorksCompleted] AS SET NOCOUNT ON;') 
GO
/****** Object:  StoredProcedure [dbo].[CM_UpdateWorksCompleted]    Script Date: 	29/08/2017 14:21:37 ******/

-- =============================================
-- Author:		Ali Raza
-- Create date:	07/09/2017
-- Description:	Assign Cyclical Services to  Contractor
-- WebPage:		PropertyDataRestructure/Views/CyclicalServices/AcceptCyclicalPurchaseOrder.aspx
-- =============================================
ALTER PROCEDURE [dbo].[CM_UpdateWorksCompleted]
	@workDetailId int,
	@cmContractorId int,
	@cycleCompleted SMALLDATETIME,
	@status nvarchar(200),
	@purchaseOrderItemId int,
	@purchaseOrderId int,	
	@userId int,
	@transactionIdentity	varchar(100),
	@isSaved BIT = 0 OUTPUT 	
			
AS
BEGIN
---===============Begin Trasaction===================
BEGIN TRANSACTION
BEGIN TRY

--=============Declare Variables ================
DECLARE @totalPIs int,@approvedPIs int,@poStatusId int,@cmStatusId int
	Select @totalPIs=COUNT(*)  from (
	 SELECT DISTINCT PI.ORDERITEMID,PI.ORDERID, PI.GROSSCOST, PS.POSTATUSNAME, PI.ITEMDESC, PI.ITEMNAME,EX.DESCRIPTION AS EXPENDITURE,PI.NETCOST,PI.VAT, PI.PISTATUS 
								 FROM F_PURCHASEITEM PI  
								LEFT JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID  
								INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID							
								 WHERE PI.ORDERID = @purchaseOrderId  AND PI.ACTIVE = 1   
                         		) As result
	GROUP BY result.ORDERID 
	
SELECT @approvedPIs = Count(DISTINCT PI.ORDERITEMID)FROM F_PURCHASEITEM PI WHERE PI.ORDERID = @purchaseOrderId
 AND PI.ACTIVE = 1 AND PISTATUS IN (5,18, 4,7,8,9,10,11,12,13,14, 17,19)

 Select @poStatusId=POSTATUSID from F_POSTATUS where POSTATUSNAME='Goods Approved'
 Select @cmStatusId = StatusId from CM_Status where Title = @status
--==================Update CM_ContractorWorkDetail================================================
 Update CM_ContractorWorkDetail Set StatusId = @cmStatusId,CycleCompleted=@cycleCompleted where WorkDetailId= @workDetailId and CMContractorId=@cmContractorId
--==================Update F_PURCHASEITEM================================================

UPDATE F_PURCHASEITEM SET PISTATUS =@poStatusId where ORDERITEMID = @purchaseOrderItemId
 
---Storing entry in Purchase item History
INSERT INTO F_PURCHASEITEM_LOG (IDENTIFIER, ORDERID, ACTIVE, PITYPE, PISTATUS, TIMESTAMP, ACTIONBY, ACTION,ORDERITEMID) 
				SELECT @transactionIdentity, ORDERID, ACTIVE, PITYPE, PISTATUS, GETDATE(), @userId, 'Goods Approved',ORDERITEMID 
				FROM F_PURCHASEITEM WHERE ORDERITEMID = @purchaseOrderItemId
				
 IF(@totalPIs - @approvedPIs = 1 )
 BEGIN 
	UPDATE F_PURCHASEORDER SET POSTATUS =@poStatusId where ORDERID =  @purchaseOrderId
	Update CM_ServiceItems Set StatusId = @cmStatusId where PORef =@purchaseOrderId 
	
	INSERT INTO F_PURCHASEORDER_LOG (IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION) 
     SELECT @transactionIdentity, ORDERID, ACTIVE, POTYPE, POSTATUS, GETDATE(), @userId, 'Goods Approved' 
     FROM F_PURCHASEORDER WHERE ORDERID = @purchaseOrderId
 END
 

 	

IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;
		SET @isSaved = 1;    	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION; 
		SET @isSaved = 0;    	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

END