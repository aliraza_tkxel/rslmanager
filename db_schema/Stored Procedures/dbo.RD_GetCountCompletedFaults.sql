
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- EXEC  [dbo].[RD_GetCountCompletedFaults]
-- Author:  <Ali Raza>
-- Create date: <24/07/2013>
-- Description: <This stored procedure gets the count of all 'follow on works required'>
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[RD_GetCountCompletedFaults]
 -- Add the parameters for the stored procedure here

 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 
 DECLARE @StartDate DATETIME
 DECLARE @EndDate DATETIME
 DECLARE @StartTime TIME = '00:01:00'
 DECLARE @EndTime TIME = '23:59:00'
 
 SET @StartDate = DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ) )
 SET @EndDate = DATEADD(SS,-1,DATEADD(mm,12,@StartDate))

print(@StartDate)
print(@EndDate)

 SET NOCOUNT ON;
 
	SELECT COUNT(DISTINCT L.FAULTLOGID) FROM DBO.FL_FAULT_LOG L
	INNER JOIN FL_FAULT F ON F.FAULTID= L.FAULTID
	INNER JOIN DBO.FL_FAULT_LOG_HISTORY FH ON FH.FAULTLOGID=L.FAULTLOGID AND FH.FAULTSTATUSID IN (7,17)
	WHERE 1=1  
	AND STATUSID IN (7,17)   
	AND FH.LASTACTIONDATE BETWEEN @STARTDATE AND @ENDDATE 

END	
GO
