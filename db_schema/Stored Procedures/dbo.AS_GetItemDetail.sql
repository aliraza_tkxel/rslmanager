  
-- =============================================            
  
/*	  --------------  
       History  
      --------------  
        
Version: Author:   Create date:    Description:     
  
v1.0   Ali Raza  10/22/2013    Description,Get the detail of tree leaf node    PropertyRecord.aspx -- > Document Tab      
             
   
v1.1   Raja Aneeq  1/12/2015    Add a new column CP12Renewal    
             
EXEC [dbo].[AS_GetItemDetail]            
  @itemId = 74,            
  @PropertyId = 'P100160004'   
    
    
  */           
-- =============================================    
IF OBJECT_ID('dbo.[AS_GetItemDetail]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetItemDetail] AS SET NOCOUNT ON;') 
GO
        
ALTER PROCEDURE [dbo].[AS_GetItemDetail]            
 -- Add the parameters for the stored procedure here            
 --@areaId int,              
 @itemId int,            
 @propertyId nvarchar(20)              
AS            
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from            
-- interfering with SELECT statements.            
SET NOCOUNT ON;  
--=================================================================  
--get Parameters  
--=================================================================  
SELECT  
 ItemParamID  
 ,PA_ITEM.ItemID  
 ,PA_PARAMETER.ParameterID  
 ,ParameterName  
 ,DataType  
 ,ControlType  
 ,IsDate  
 ,ParameterSorder  
 ,  
 (  
  SELECT CONVERT(VARCHAR(10), Max(UPDATEDON), 103) AS UPDATEDON  
  FROM  
   PA_PROPERTY_ATTRIBUTES  
    INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ItemParamID = PA_PROPERTY_ATTRIBUTES.ItemParamId  
  WHERE  
   PROPERTYID = @propertyId  
   AND ItemId = @itemId  
 )        
 AS LastInspected  
 ,PA_PARAMETER.ShowInApp AS ShowInApp 
 ,PA_ITEM_PARAMETER.ParameterValueId 
FROM  
 PA_ITEM_PARAMETER  
  INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID  
  INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId  
  INNER JOIN PA_AREA ON PA_AREA.AreaID = PA_ITEM.AreaID  
WHERE  
 PA_ITEM.ItemID = @itemId  
 AND PA_ITEM_PARAMETER.isActive = 1  
 AND PA_ITEM.isActive = 1  
 AND PA_PARAMETER.isActive = 1  
ORDER BY ParameterSorder  
  
--=================================================================  
---get parameter values  
--=================================================================  
SELECT DISTINCT 
 PV.ValueID  
 ,PV.ParameterID  
 ,PV.ValueDetail  
 ,PV.Sorder
 ,PV.IsAlterNativeHeating  
FROM  
 PA_PARAMETER_VALUE PV  
  INNER JOIN PA_ITEM_PARAMETER IP ON PV.ParameterID = IP.ParameterId  
    AND  
    IP.IsActive = 1  
  INNER JOIN PA_PARAMETER P ON IP.ParameterId = P.ParameterID  
    AND  
    P.IsActive = 1  
  INNER JOIN PA_ITEM I ON IP.ItemId = I.ItemId  
    AND  
    I.IsActive = 1  
WHERE  
 PV.IsActive = 1  
 AND I.ItemID = @itemId  
ORDER BY SOrder  
  
--=================================================================  
---get pre inserted values             
--=================================================================  
SELECT  
 A.PROPERTYID  
 ,ATTRIBUTEID  
 ,A.ITEMPARAMID  
 ,A.PARAMETERVALUE  
 ,A.VALUEID  
 ,A.UPDATEDON  
 ,A.UPDATEDBY  
 ,A.IsCheckBoxSelected
 ,A.HeatingMappingId 
 ,HM.HeatingType 
FROM  
 PA_PROPERTY_ATTRIBUTES A  
  INNER JOIN PA_ITEM_PARAMETER IP ON A.ITEMPARAMID = IP.ItemParamID AND IP.IsActive = 1   
  INNER JOIN PA_PARAMETER P ON IP.ParameterId = P.ParameterID AND P.IsActive = 1  
  INNER JOIN PA_ITEM I ON IP.ItemId = I.ItemId AND I.IsActive = 1  
  left join PA_HeatingMapping HM on A.HeatingMappingId = HM.HeatingMappingId and HM.IsActive=1
WHERE  
 A.PROPERTYID = @propertyId  
 And( (I.ItemName='Heating' And A.HeatingMappingId IS NOT NULL) OR    (I.ItemName<>'Heating' And A.HeatingMappingId IS  NULL) )
 AND IP.ItemId = @itemId
 ORDER BY A.UPDATEDON DESC
   
--=================================================================  
---get item Dates  
--=================================================================  
SELECT  
 [SID]  
 ,PA_PROPERTY_ITEM_DATES.PROPERTYID  
 ,I.ItemId  
 ,LastDone  
 ,DueDate  
 ,PA_PARAMETER.ParameterId  
 ,ParameterName  
 ,PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID AS ComponentId 
 ,PA_PROPERTY_ITEM_DATES.HeatingMappingId 
 ,HM.HeatingType 
FROM  
 PA_PROPERTY_ITEM_DATES  
  LEFT JOIN PA_PARAMETER ON PA_PARAMETER.ParameterID = PA_PROPERTY_ITEM_DATES.ParameterId 
  INNER JOIN PA_ITEM I ON PA_PROPERTY_ITEM_DATES.ItemId = I.ItemId AND I.IsActive = 1  
  left join PA_HeatingMapping HM on PA_PROPERTY_ITEM_DATES.HeatingMappingId = HM.HeatingMappingId and HM.IsActive=1 
WHERE  
 PA_PROPERTY_ITEM_DATES.PROPERTYID = @propertyId
 And( (I.ItemName='Heating' And PA_PROPERTY_ITEM_DATES.HeatingMappingId IS NOT NULL) OR    (I.ItemName<>'Heating' And PA_PROPERTY_ITEM_DATES.HeatingMappingId IS  NULL) )  
 AND PA_PROPERTY_ITEM_DATES.ItemId = @itemId  
 ORDER BY UPDATEDON DESC
  
--=================================================================  
---get Maintenance,Servicing and testing  
--=================================================================  
SELECT  
 MSATId  
 ,PDR_MSAT.ItemId  
 ,PDR_MSAT.CycleTypeId  
 ,IsRequired  
 ,CONVERT(VARCHAR(20), LastDate, 103) AS LastDate  
 ,Cycle  
 ,CONVERT(VARCHAR(20), NextDate, 103) AS NextDate  
 ,AnnualApportionment  
 ,CycleType  
 ,PDR_MSAT.MSATTypeId  
 ,MSATTypeName  
  
FROM  
 PDR_MSAT  
  LEFT JOIN PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId  
    AND  
    PDR_CycleType.IsActive = 1  
  INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId  
    AND  
    PDR_MSATType.IsActive = 1  
  INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId  
  INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PDR_MSAT.ItemId  
WHERE  
 PDR_MSAT.PROPERTYID = @propertyId  
 AND PDR_MSAT.ItemId = @itemId  
  
--=================================================================  
---Get planned_component mapping  
--=================================================================  
SELECT  
 PLANNED_COMPONENT_ITEM.COMPONENTID  
 ,PLANNED_COMPONENT_ITEM.ITEMID  
 ,PLANNED_COMPONENT_ITEM.PARAMETERID  
 ,PLANNED_COMPONENT_ITEM.VALUEID  
 ,CASE  
  WHEN PLANNED_COMPONENT.FREQUENCY = 'yrs'  
   THEN CONVERT(INT, PLANNED_COMPONENT.CYCLE) * 12  
  ELSE CONVERT(INT, PLANNED_COMPONENT.CYCLE)  
 END AS CYCLE  
 ,CASE  
  WHEN PLANNED_COMPONENT.FREQUENCY = 'yrs'  
   THEN CONVERT(VARCHAR(10), PLANNED_COMPONENT.CYCLE) + ' Years'  
  ELSE CONVERT(VARCHAR(10), PLANNED_COMPONENT.CYCLE) + ' Months'  
 END AS LIFECYCLE  
 ,PLANNED_COMPONENT.ISACCOUNTING  
 ,PLANNED_COMPONENT_ITEM.SubParameter  
 ,PLANNED_COMPONENT_ITEM.SubValue  
FROM  
 PLANNED_COMPONENT_ITEM  
  INNER JOIN PLANNED_COMPONENT ON PLANNED_COMPONENT.COMPONENTID = PLANNED_COMPONENT_ITEM.COMPONENTID  
  INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PLANNED_COMPONENT_ITEM.ITEMID  
WHERE  
 PA_ITEM.ItemID = @itemId  
 AND PLANNED_COMPONENT_ITEM.isActive = 1  
  
--=================================================================  
-- Get Property CP12 certificate info in case of heating item.  
--=================================================================  
-- Get Item Name by @itemId  
DECLARE @ItemName NVARCHAR(50)  
SELECT  
 @ItemName = I.ItemName  
FROM  
 PA_ITEM I  
WHERE  
 I.ItemID = @itemId  
  
-- Get Property CP12 Information  
SELECT  
 ISNULL(CP12NUMBER, 'N/A')							   [Certificate Number]  
 ,ISNULL(CONVERT(NVARCHAR, ISSUEDATE, 103), 'N/A')     [Certificate Issued]  
 ,CASE
    WHEN CP12Renewal IS NULL THEN 
		ISNULL(Convert(varchar(100),DATEADD(YEAR,1,ISSUEDATE),103), 'N/A')
		
	ELSE
		ISNULL(CONVERT(NVARCHAR, CP12Renewal, 103), 'N/A') 
	END AS  [Certificate Renewal]  
,P_LGSR.HeatingMappingId	
FROM  
 P_LGSR  
WHERE  
 PROPERTYID = @propertyId  
 AND @ItemName = 'Heating' 
 AND P_LGSR.HeatingMappingId IS NOT NULL
 -- Get CP12 Info only in case of Heating Item  
  
  ----------=============================Get Heating mapping======================================
  Select PA_HeatingMapping.HeatingType,PA_HeatingMapping.HeatingMappingId
  FROM  
 PA_HeatingMapping  where PA_HeatingMapping.PropertyId= @propertyId  and PA_HeatingMapping.IsActive=1
 
 ----------=============================Get LGSR Certificates======================================
 Select P_LGSR_Certificates.Title,P_LGSR_Certificates.HeatingType  from P_LGSR_Certificates where P_LGSR_Certificates.IsActive=1
END