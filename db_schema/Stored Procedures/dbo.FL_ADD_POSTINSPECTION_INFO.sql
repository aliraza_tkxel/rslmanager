SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE dbo.FL_ADD_POSTINSPECTION_INFO
/* ===========================================================================
 '   NAME:          FL_ADD_POSTINSPECTION_INFO
 '   DATE CREATED:   27th Feb 2009
 '   CREATED BY:     Usman Sharif
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get values of a faultLog against a particular faultLogID from FL_FAULT and TempFault Table table which will be used to update 
 '		     a fault values
 '   IN:            @UserID as Int,
 '   IN:	    @InspectionDate as DateTime,
 '   IN:	    @NetCost as float,
 '   IN:	    @DueDate as datetime,
 '   IN:	    @Notes as nvarchar,
 '		    @Approved as int,
 '		    @FaultLogId as int
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
@UserID as Int,
@InspectionDate as DateTime,
@time as nvarchar(100) = NULL,
@NetCost as float,
@DueDate as datetime,
@Notes as nvarchar(500),
@Approved as int = NULL,
@FaultLogId as int,
@RepairId as int,
@Recharge as Bit, 
@RESULT  INT = 1  OUTPUT 

AS

BEGIN TRAN
Declare @IsAppointmentSaved as bit

BEGIN
UPDATE FL_CO_FAULTLOG_TO_REPAIR
SET USERID = @UserID, INSPECTIONDATE = @InspectionDate , INSPECTIONTIME = @time , NETCOST = @NetCost , DUEDATE = @DueDate, NOTES = @Notes , APPROVED = @Approved, IsAppointmentSaved = 1, Recharge = @Recharge
WHERE FaultLogId = @FaultLogId AND FaultRepairListID = @RepairId

SELECT @IsAppointmentSaved = IsAppointmentSaved FROM FL_CO_FAULTLOG_TO_REPAIR WHERE FaultLogId = @FaultLogId AND FaultRepairListID = @RepairId

IF @IsAppointmentSaved <> 1
	Begin
		Rollback Transaction
		SET @Result=-1
		Print 'Unable to update the record'
	End
	ELSE
	Begin					
		SET @Result=1
		COMMIT TRAN
	END
END

GO
