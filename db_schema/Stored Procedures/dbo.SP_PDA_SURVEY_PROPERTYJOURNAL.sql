SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Umair Naeem
-- Create date: 29-April-2008
-- Description:	This procedure will get the journal item information of a survey against 
--				property which will be displayed on iframe iPropertyJournal.asp in Portfolio Area
-- EXEC SP_PDA_SURVEY_PROPERTYJOURNAL 'A140070006'
-- =============================================
CREATE PROCEDURE [dbo].[SP_PDA_SURVEY_PROPERTYJOURNAL] 
	-- parameters for the stored procedure 
	@PROPERTYID NVARCHAR(20) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- statements for procedure
	SELECT ISNULL(CONVERT(NVARCHAR, PS.SURVEYDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), PS.SURVEYDATE, 108) ,'') AS [DATE],
		   'Property' AS ITEM,PS.NATURE,PS.TITLE,
			CASE PSD.SURVEYSTATUS 
				WHEN 0 THEN 'In Progress'
				WHEN 1 THEN 'Completed'
			END AS STATUS,PSD.SDID,PS.SURVEYID AS SID
	FROM TBL_PDA_SURVEY PS
		INNER JOIN TBL_PDA_SURVEY_DETAIL PSD ON PSD.SURVEYID=PS.SURVEYID	
	WHERE PSD.PROPERTYID=COALESCE(@PROPERTYID, PSD.PROPERTYID)
END
GO
