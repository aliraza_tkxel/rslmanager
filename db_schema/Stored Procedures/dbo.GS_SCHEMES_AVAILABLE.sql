USE [RSLBHALive]
GO
-- Stored Procedure    
 IF OBJECT_ID('dbo.GS_SCHEMES_AVAILABLE') IS NULL 
	EXEC('CREATE PROCEDURE dbo.GS_SCHEMES_AVAILABLE AS SET NOCOUNT ON;') 
GO 
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
ALTER PROCEDURE [dbo].[GS_SCHEMES_AVAILABLE]  
  
-- EXEC GS_SCHEMES_AVAILABLE  
  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT  SCHEMEID AS DEVELOPMENTID,SCHEMENAME AS  DEVELOPMENTNAME  
FROM P_SCHEME  
WHERE SCHEMEID NOT IN(   
 SELECT DISTINCT ISNULL(SS.SCHEMEID,-1) 
 FROM S_SCOPETOPATCHANDSCHEME SS   
 INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID   
 WHERE S.RENEWALDATE>=DATEADD(WW,10,GETDATE())  AND SS.BlockId  IS NULL  
 )  
 ORDER By P_SCHEME.SCHEMENAME ASC     
  
END  
  