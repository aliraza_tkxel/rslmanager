USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetNewlyCreatedAppointmentData]    Script Date: 04/10/2013 14:49:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Behroz Sikander>
-- Create date: <15/03/2013>
-- Description:	<This stored procedure fetches the newly inserted appointment data>
-- Webpage: ReArrangingCurrentFault.aspx
--
--  EXEC [FL_GetNewlyCreatedAppointmentData] 
--  @customerId = 2043,
--	@propertyId = 'A720040007',
--	@appointmentId = 2
-- =============================================


IF OBJECT_ID('dbo.FL_GetNewlyCreatedAppointmentData') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetNewlyCreatedAppointmentData AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[FL_GetNewlyCreatedAppointmentData] 
	@customerId int ,
	@propertyId nvarchar(20),	
	@appointmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Select FL_CO_APPOINTMENT.AppointmentID as AppointmentId
			,FL_FAULT_LOG.FaultLogID As FaultLogId 
			,FL_AREA.AreaName as AreaName
			,FL_FAULT.Description as Description
			,FL_FAULT_STATUS.Description as FaultStatus
			,convert(date, FL_FAULT_LOG.SubmitDate,103)as SubmitDate
			,FL_CO_APPOINTMENT.AppointmentDate
			,FL_FAULT.FaultId as FaultId
			,FL_FAULT_LOG.JobSheetNumber as JobSheetNumber		
			,FL_FAULT.Duration
			,G_Trade.Description as Trade
			,G_Trade.Description as TradeName	
			,FL_FAULT_LOG.ProblemDays
			,FL_FAULT_LOG.RecuringProblem
			,FL_FAULT_LOG.CommunalProblem
			,Case days WHEN 1 then convert(char(6), dateadd(day,FL_Fault_Priority.ResponseTime,getdate()),0) else convert(char(6), dateadd(hour,FL_Fault_Priority.ResponseTime,getdate()),0) end AS DueDate
			,FL_FAULT_LOG.Notes
			,FL_FAULT.isGasSafe
			,FL_FAULT.isOftec
			,FL_FAULT_LOG.Quantity
			,FL_FAULT_LOG.Recharge
			,(CASE WHEN FL_Fault_Priority.Days = 1 THEN
			CONVERT(nvarchar(50), FL_Fault_Priority.ResponseTime) + ' days' ELSE
			CONVERT(nvarchar(50), FL_Fault_Priority.ResponseTime) + ' hours' END) as Response
			FROM FL_FAULT 			
			INNER JOIN FL_FAULT_LOG  ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID
			INNER JOIN FL_AREA ON FL_AREA.AreaID = FL_FAULT_LOG.AreaID
			INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID =FL_FAULT_LOG.StatusID
			INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId
			INNER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID												
			INNER JOIN FL_Fault_Priority ON FL_Fault.PriorityId  = FL_Fault_Priority.PriorityId
			INNER JOIN FL_FAULT_TRADE ON FL_FAULT_TRADE.FaultTradeId = FL_FAULT_LOG.FaultTradeId
			INNER JOIN G_Trade ON FL_FAULT_TRADE.TradeId = G_Trade.TradeId													
						
	where FL_FAULT_LOG.CustomerId = @customerId 
			AND FL_FAULT_LOG.PropertyId = @propertyId	
			AND FL_CO_APPOINTMENT.AppointmentID = @appointmentId
END
