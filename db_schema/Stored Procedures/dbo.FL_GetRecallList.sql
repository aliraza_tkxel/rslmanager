USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetRecallList]    Script Date: 01/06/2015 10:21:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[FL_GetRecallList]
--		@searchedText = N'Laundry',
--		@operativeId = 145,
--		@pageSize = 2,
--		@pageNumber = 1,
--		@sortColumn = N'Recorded',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT		
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <19/2/2013>
-- Description:	<Get list of Recalls>
-- Web Page: ReportsArea.aspx
-- =============================================


IF OBJECT_ID('dbo.FL_GetRecallList') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetRecallList AS SET NOCOUNT ON;') 
GO


ALTER PROCEDURE [dbo].[FL_GetRecallList] 
( 
	-- Add the parameters for the stored procedure here
		@schemeId int = -1,
		@blockId int = -1,
		@financialYear INT,
		@searchedText VARCHAR(8000) = '',
		@operativeId int,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'Recorded',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
		--variables for paging
		@offset int,
		@limit int
	
	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1
	
	--========================================================================================
	-- Begin building SearchCriteria clause
	-- These conditions will be added into where clause based on search criteria provided
	
	SET @searchCriteria = ' 1=1 '
	
	IF(@searchedText != '' OR @searchedText IS NOT NULL)
	BEGIN			
		--SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (FREETEXT(P__PROPERTY.HouseNumber ,'''+@searchedText+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@searchedText+'''))'
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND FL_FAULT_LOG.JobSheetNumber LIKE ''%' + @searchedText + '%'''
	END
	
	IF @operativeId > 0	
	BEGIN
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND FL_CO_APPOINTMENT.OperativeID = '+CONVERT(nvarchar(50), @operativeId) 
	END
	
	-- Filter on the basis of BlockId and SchemeId
	IF(@schemeId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.SchemeId IS NULL 
																	THEN	P__PROPERTY.SchemeId 
																	ELSE 		
																			FL_FAULT_LOG.SchemeId 	
																	END = '+convert(varchar(10),@schemeId)
		END
	IF(@blockId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND Case WHEN FL_FAULT_LOG.BlockId IS NULL 
																	THEN P__PROPERTY.BLOCKID 
																	ELSE 		
																		FL_FAULT_LOG.BlockId 	
																	END = '+convert(varchar(10),@blockId)
		END	
	IF (@financialYear != -1 AND LEN(@financialYear) = 4)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND FL_FAULT_LOG.SubmitDate BETWEEN ''' + CONVERT(VARCHAR,@financialYear) + '0401'' AND ''' + CONVERT(VARCHAR,@financialYear+1) + '0331 23:59:59.997'''
		END
	-- End building SearchCriteria clause   
	--========================================================================================
	
	SET NOCOUNT ON;
	--========================================================================================	        
	-- Begin building SELECT clause
	-- Insert statements for procedure here
	
	SET @selectClause = 'SELECT DISTINCT top ('+convert(varchar(10),@limit)+') 
	FL_FAULT_RECALLS.OriginalFaultLogId AS OriginalFaultLogId
	,ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block
	,FL_FAULT_LOG.JobSheetNumber as JSN
	,CONVERT(nvarchar(50),FL_FAULT_LOG.SubmitDate, 103) as Recorded
	,ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS1,'''') as Address
	,FL_AREA.AreaName as Location
	,FL_FAULT.Description as Description
	,ISNULL(CONVERT(nvarchar(50),C_REPAIRS.LASTACTIONDATE, 103), ''N/A'') AS Completed
	,ISNULL(NULLIF(ISNULL(E__EMPLOYEE.FIRSTNAME,'''') + ISNULL('' '' + E__EMPLOYEE.LASTNAME,''''), ''''), ''N/A'') as Operative
	,number
	
	-- Below Colums are just of sorting only
	,P__PROPERTY.HouseNumber as HouseNumber
	,P__PROPERTY.ADDRESS1 as Address1
	,C_REPAIRS.LASTACTIONDATE as LastActionDate
	,FL_FAULT_LOG.SubmitDate as RecordedOn
	,E__EMPLOYEE.FIRSTNAME as FirstName
	,E__EMPLOYEE.LASTNAME as LastName
	,FL_FAULT_LOG.FaultLogID as FaultLogID'
	-- End building SELECT clause
	--======================================================================================== 							
	
	
	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause =	  CHAR(10) +'FROM
						(SELECT OriginalFaultLogId, COUNT(OriginalFaultLogId) AS number FROM FL_FAULT_RECALL GROUP BY OriginalFaultLogId) AS FL_FAULT_RECALLS
						INNER JOIN FL_FAULT_LOG ON FL_FAULT_RECALLS.OriginalFaultLogId = FL_FAULT_LOG.FaultLogID
						LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
						INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
						INNER JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID

						INNER JOIN FL_FAULT_JOURNAL on FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID
						LEFT JOIN FL_FAULTJOURNAL_TO_CJOURNAL on FL_FAULT_JOURNAL.JournalID = FL_FAULTJOURNAL_TO_CJOURNAL.FJOURNALID
						LEFT JOIN (SELECT * FROM C_REPAIR WHERE C_REPAIR.ItemStatusId=11 and C_REPAIR.ItemActionId=6) C_REPAIRS on FL_FAULTJOURNAL_TO_CJOURNAL.CJOURNALID = C_REPAIRS.JOURNALID

						LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId
						LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
						LEFT JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID
						LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = 
						Case WHEN FL_FAULT_LOG.SchemeId is NULL THEN P__PROPERTY.SchemeId 
						ELSE 		FL_FAULT_LOG.SchemeId 	END
						LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID	=
						Case WHEN FL_FAULT_LOG.BlockId is NULL THEN P__PROPERTY.BLOCKID 
						ELSE 		FL_FAULT_LOG.BlockId 	END			'
	-- End building From clause
	--======================================================================================== 														  
	
	
	
	--========================================================================================    
	-- Begin building OrderBy clause		
	
	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Address1'		
	END
	
	IF(@sortColumn = 'Completed')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'LastActionDate'		
	END
	
	IF(@sortColumn = 'Recorded')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'RecordedOn'		
	END
	
	IF(@sortColumn = 'Operative')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'FirstName, LastName'		
	END
	IF(@sortColumn = 'Scheme')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Scheme' 		
	END	
	
	IF(@sortColumn = 'Block')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'Block' 		
	END
	IF(@sortColumn = 'JSN')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
	END			
	
	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
	
	-- End building OrderBy clause
	--========================================================================================								
	
	--========================================================================================
	-- Begin building WHERE clause
    
	-- This Where clause contains subquery to exclude already displayed records			  
	
	SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
	
	-- End building WHERE clause
	--========================================================================================
	
	--========================================================================================
	-- Begin building the main select Query
	
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
	
	-- End building the main select Query
	--========================================================================================																																			

	--========================================================================================
	-- Begin building the row number query
	
	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
	
	-- End building the row number query
	--========================================================================================
	
	--========================================================================================
	-- Begin building the final query 
	
	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
	
	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	PRINT(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									
	
	--========================================================================================
	-- Begin building Count Query 
	
	Declare @selectCount nvarchar(MAX), 
	@parameterDef NVARCHAR(500)
	
	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT @totalCount =  count(DISTINCT FL_FAULT_RECALLS.OriginalFaultLogId) ' + @fromClause + @whereClause
	
	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
	-- End building the Count Query
	--========================================================================================							
		
END




