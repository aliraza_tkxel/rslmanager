SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_G_SEXUALORIENTATION_GetSexualOrientationLookup
/* ===========================================================================
 '   NAME:           TO_G_SEXUALORIENTATION_GetSexualOrientationLookup
 '   DATE CREATED:   16 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get sexual orientation records from G_SEXUALORIENTATION table which will be shown
 '					 as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT SEXUALORIENTATIONID AS id,DESCRIPTION AS val
	FROM G_SEXUALORIENTATION
	ORDER BY DESCRIPTION ASC

GO
