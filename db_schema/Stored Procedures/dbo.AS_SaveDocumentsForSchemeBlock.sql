USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.AS_SaveDocumentsForSchemeBlock') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_SaveDocumentsForSchemeBlock AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[AS_SaveDocumentsForSchemeBlock] 
	-- Add the parameters for the stored procedure here
	@journalHistoryId varchar(max),
	@documentName nvarchar(50),
	@documentPath nvarchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @SplitValues TABLE 
	(
		ID int
	)
	DECLARE @xml xml
	SET @xml = N'<root><r>' + replace(@journalHistoryId,',','</r><r>') + '</r></root>'

	INSERT INTO @SplitValues(ID)
	SELECT r.value('.','int')
	FROM @xml.nodes('//root/r') as records(r)


    INSERT INTO [RSLBHALive].[dbo].[AS_Documents]
           ([DocumentName]
           ,[JournalHistoryId]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[DocumentPath]           
           )
	SELECT @journalHistoryId
			, ID
			, CURRENT_TIMESTAMP
			, CURRENT_TIMESTAMP
			, @documentPath  
	FROM @SplitValues
    
END
