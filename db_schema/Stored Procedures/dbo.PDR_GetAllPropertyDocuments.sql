SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Fakhar uz zaman>
-- Create date: <Create Date,,15/03/2016>
-- Description:	<Returns all Property documents on ComplianceDocument.aspx>
--USE [RSLBHALive]
--GO

--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_GetAllPropertyDocuments]
--		@category = N'Compliance',
--		@type = -1,
--		@title = -1,
--		@dateType = N'Document',
--		@from = N'',
--		@to = N'',
--		@uploadedOrNot = N'NotUploaded',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@searchedText ='Sorrel House',
--		@sortColumn = N'Type',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'

--SELECT	'Return Value' = @return_value

--GO
-- =============================================

IF OBJECT_ID('dbo.PDR_GetAllPropertyDocuments') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetAllPropertyDocuments AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE dbo.PDR_GetAllPropertyDocuments
(
	--These parameters will be used for search
	@category varchar(20) = 'Compliance',
	@type int = -1,
	@title int = -1,	
	@dateType varchar(10)='', --(Documented or Expiry)
	@from varchar(20) = '',
	@to varchar(20) = '',
	@uploadedOrNot varchar(20)='Uploaded',		--(1=Uploaded & 0=Not Uploaded)
	@searchedText varchar(200)='',	
	
	--Parameters which would help in sorting and paging
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Type',
	@sortOrder varchar (5) = 'DESC',
	@getOnlyCount bit=0,	
	@totalCount int=0 output	
)
AS
BEGIN

		DECLARE @SelectClause nvarchar(MAX),
        @fromClause   nvarchar(MAX),
        @whereClause  nvarchar(MAX),	        
        @orderClause  nvarchar(MAX),	
        @mainSelectQuery nvarchar(MAX),        
        @rowNumberQuery nvarchar(MAX),
        @finalQuery nvarchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria nvarchar(MAX),
        
        --variables for paging
        @offset int,
		@limit int,
		@fromDate datetime,
		@toDate datetime,
		@documentType nvarchar(500)='-',
		@documentSubType nvarchar(500)='-'
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
        		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided

		
		SET @searchCriteria =  ' 1=1 '	
		
		IF(@type > 0)
				BEGIN
					Select @documentType = Title from P_Documents_Type where DocumentTypeId = @type
				END
		IF(@title > 0)
				BEGIN
					Select @documentSubType = Title from P_Documents_SubType where DocumentSubtypeId=@title
				END
		
		--CHECK UPLOADED OR NOT
		IF(@uploadedOrNot = 'NotUploaded' )--Not Uploaded. Fetch only Developments. 
			
			BEGIN
				
				-- Begin building FROM clause

				SET @fromClause = CHAR(10) + 'FROM P__PROPERTY  PROPERTY'
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  PROPERTY.PROPERTYID NOT IN(Select PropertyId from P_Documents where (DocumentTypeId = '+ CONVERT(VARCHAR(10),@type) +' OR '+ CONVERT(VARCHAR(10),@type) +' = -1) AND  (DocumentSubTypeId = '+ CONVERT(VARCHAR(10),@title) +' OR '+ CONVERT(VARCHAR(10),@title) +'= -1) AND PROPERTYID is not null )'

				-- End building From clause
				--========================================================================================
				--print('@fromClause' + @fromClause)
				--========================================================================================   
				
				SET @SelectClause = 'SELECT  top ('+convert(varchar(10),@limit)+')
							0 AS DocumentId,
							''--'' AS Added,
							''--'' AS AddedSort,
							'''+@documentType+'''  AS ''Type'',
							'''+@documentSubType+'''  AS Title,
							''--'' AS "By",	
							''--'' AS SortWithBy,								
							''--'' AS Document,
							''--'' AS DocumentSort,
							''--'' as Expiry,
							''--'' AS ExpirySort,
							ISNULL(PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(PROPERTY.ADDRESS3,'''')+'', ''+ ISNULL(PROPERTY.TOWNCITY,'''')  AS ADDRESS, 
							ISNULL(PROPERTY.PostCode,''--'') AS Postcode'
							
					IF(@searchedText <> '' )
		
						BEGIN
							SET @searchedText = REPLACE(@searchedText,'''','@')
							SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (PROPERTY.PostCode LIKE ''%'+@searchedText+'%'' 							
							OR REPLACE((ISNULL(PROPERTY.FLATNUMBER,'''')+ISNULL('' ''+PROPERTY.HouseNumber,'''') + ISNULL('' ''+PROPERTY.ADDRESS1,'''') + ISNULL('' ''+PROPERTY.ADDRESS2,'''') + ISNULL('' ''+PROPERTY.ADDRESS3,'''') + ISNULL(''
							''+PROPERTY.TOWNCITY,'''')),'''''''',''@'')
							LIKE ''%'+@searchedText+'%'')'
						END					
			
			END 
			
		ELSE	-- Uploaded Documents		
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND (ALLDOCS.DOCUMENTNAME <> '''' OR ALLDOCS.DOCUMENTNAME IS NOT NULL) '
			IF(@type > 0)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  ALLDOCS.DocumentTypeId = '+ CONVERT(VARCHAR(10),@type) + CHAR(10)
				END
		    IF(@title > 0)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND  ALLDOCS.DocumentSubTypeId = '+ CONVERT(VARCHAR(10),@title) + CHAR(10)
				END
				--Check DropDown Filters		
		    IF(@category <> '' OR @category IS NOT NULL)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ALLDOCS.Category = ''' + @category + ''''
				END
		SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ALLDOCS.SchemeId IS NULL '
		-- Begin building SELECT clause
		
		SET @SelectClause = 'SELECT  top ('+convert(varchar(10),@limit)+')
							ISNULL(ALLDOCS.DocumentId,0) AS DocumentId,
							CONVERT(VARCHAR(12),ALLDOCS.CreatedDate,103) AS Added,
							ALLDOCS.CreatedDate AS AddedSort,
							Case when '''+@uploadedOrNot+''' = ''NotUploaded '' then '''+@documentType+'''
							ELSE ISNULL(DOCTYPE.Title,'''') END
							 AS ''Type'',
							 
							Case when '''+@uploadedOrNot+''' = ''NotUploaded '' then '''+@documentSubType+'''
							ELSE ISNULL(DOCTITLE.Title,'''') END
							  AS Title,
							ISNULL(ISNULL(EMP.FIRSTNAME,'''') + ISNULL(EMP.LASTNAME,''''),''--'') AS "By",	
							ISNULL(EMP.FIRSTNAME,'''') AS SortWithBy,								
							ISNULL(CONVERT(VARCHAR(12),ALLDOCS.DocumentDate,103),''--'') AS Document,
							ALLDOCS.DocumentDate AS DocumentSort,
							ISNULL(CONVERT(VARCHAR(12),ALLDOCS.ExpiryDate,103),''--'') as Expiry,
							ALLDOCS.ExpiryDate AS ExpirySort,
							ISNULL(PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(PROPERTY.ADDRESS3,'''')+'', ''+ ISNULL(PROPERTY.TOWNCITY,'''')  AS ADDRESS, 
							ISNULL(PROPERTY.PostCode,''--'') AS Postcode'	
			
			
				-- Begin building FROM clause
		
		SET @fromClause = CHAR(10) + 'FROM dbo.P_Documents ALLDOCS 
			INNER JOIN dbo.P_DOCUMENTS_TYPE AS DOCTYPE ON DOCTYPE.DocumentTypeId = ALLDOCS.DocumentTypeId
			INNER JOIN dbo.P_DOCUMENTS_SUBTYPE AS DOCTITLE ON DOCTITLE.DocumentSubtypeId = ALLDOCS.DocumentSubtypeId
			LEFT JOIN dbo.P__PROPERTY AS PROPERTY ON PROPERTY.PROPERTYID= ALLDOCS.PropertyId
			LEFT JOIN dbo.E__EMPLOYEE EMP ON EMP.EMPLOYEEID = ALLDOCS.UploadedBy
			'
		
		-- End building From clause
		--========================================================================================
		--print('@fromClause' + @fromClause)
		--========================================================================================   
				
				IF(@dateType <> '') -- Document/Expiry
		
				BEGIN
						IF(@dateType = 'Document')
						BEGIN
							IF(@to <> ''  AND @from <> '' ) --only if both of i.e. From & To dates are there.
							BEGIN
								SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ALLDOCS.DocumentDate BETWEEN ' + ''''+ CONVERT(VARCHAR(12),@from,103)+ ''''  + CHAR(10) 
														+ ' AND ' + ''''+CONVERT(VARCHAR(12),@to,103) + '''' + CHAR(10)	 													
							END
						END																			
						ELSE	--@dateType = 'Expiry'
						BEGIN
							IF(@to <> '' AND @from <> '') --only if both of i.e. From & To dates are there.
							BEGIN
								SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ALLDOCS.ExpiryDate BETWEEN ' + ''''+CONVERT(VARCHAR(12),@from,103) + ''''  + CHAR(10) 
														+ ' AND ' + ''''+CONVERT(VARCHAR(12),@to,103) + '''' + CHAR(10)	
							END
								
						END
				END	
				
				--SEARCHBOX
		IF(@searchedText <> '' )
		
			BEGIN
				SET @searchedText = REPLACE(@searchedText,'''','@')
				SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (PROPERTY.PostCode LIKE ''%'+@searchedText+'%'' 
				OR DOCTYPE.Title LIKE ''%'+@searchedText+'%''  
				OR DOCTITLE.Title LIKE ''%'+@searchedText+'%'' 
				OR EMP.LASTNAME LIKE ''%'+@searchedText+'%'' 
				OR REPLACE((ISNULL(PROPERTY.FLATNUMBER,'''')+ISNULL('' ''+PROPERTY.HouseNumber,'''') + ISNULL('' ''+PROPERTY.ADDRESS1,'''') + ISNULL('' ''+PROPERTY.ADDRESS2,'''') + ISNULL('' ''+PROPERTY.ADDRESS3,'''') + ISNULL(''
				''+PROPERTY.TOWNCITY,'''')),'''''''',''@'')
				LIKE ''%'+@searchedText+'%'')'
			END			
						
	END
		
		
	
	
		-- Begin building OrderBy clause						
		
--print(@sortColumn)
		IF(@sortColumn = 'By')
		BEGIN
			SET @sortColumn = 'SortWithBy'
		END
		
		IF(@sortColumn = 'Added')
		BEGIN
			SET @sortColumn = 'AddedSort'
		END
		
		IF(@sortColumn = 'Document')
		BEGIN
			SET @sortColumn = 'DocumentSort'
		END
		
		IF(@sortColumn = 'Expiry')
		BEGIN
			SET @sortColumn = 'ExpirySort'
		END
		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
--print('@orderClause' + @orderClause)	

		-- End building OrderBy clause
		--========================================================================================															  
	
		--========================================================================================
		-- Begin building WHERE clause
	    			  				
		SET @whereClause =	CHAR(10) + 'WHERE ' + CHAR(10) + @searchCriteria 
		
		
		-- End building WHERE clause
		--========================================================================================
--print('@whereClause' + @whereClause)		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================
--print('@mainSelectQuery' + @mainSelectQuery)

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================	
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================											

	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================				

		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================									
				
END
GO
