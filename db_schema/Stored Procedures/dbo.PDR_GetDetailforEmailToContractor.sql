SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 29/01/2015
-- Description:	To get detail to send email to contractor, details are: contractor details, property details, tenant details, tenant risk details
-- Web Page:	AssignToContractor.ascx(User Control)(Details for email)
-- =============================================
--EXEC PDR_GetDetailforEmailToContractor @journalId=1381,@empolyeeId=113
ALTER PROCEDURE PDR_GetDetailforEmailToContractor

	@journalId INT
	,@empolyeeId INT
	,@inspectionJournalId INT = NULL		
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @purchaseOrder INT = -1

--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @empolyeeId

--=================================================
--Get Property Detail(s)
--=================================================
SELECT
	PROPERTY.PROPERTYID,
	HOUSENUMBER,
	FLATNUMBER,
	PROPERTY.ADDRESS1,
	PROPERTY.ADDRESS2,
	PROPERTY.ADDRESS3,
	ISNULL(PROPERTY.TOWNCITY,'N/A') AS TOWNCITY,
	PROPERTY.COUNTY,
	ISNULL(PROPERTY.POSTCODE,'N/A') AS POSTCODE,
	ISNULL(NULLIF(ISNULL('Flat No:' + PROPERTY.FLATNUMBER + ', ', '') + ISNULL(PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + PROPERTY.ADDRESS1, '')
	+ ISNULL(' ' + PROPERTY.ADDRESS2, '') + ISNULL(' ' + PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress]
	,ISNULL(P_BLOCK.BLOCKNAME,'N/A') AS Block
	,ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme
	,ISNULL(BEDROOMS.BEDROOMS,'N/A') AS Bedrooms
FROM	PDR_JOURNAL
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		LEFT JOIN P__PROPERTY PROPERTY ON PDR_MSAT.PropertyId = PROPERTY.PropertyId
		LEFT JOIN P_BLOCK ON PROPERTY.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN P_SCHEME ON P_BLOCK.SchemeId = P_SCHEME.SCHEMEID
		LEFT JOIN
		(SELECT 
			PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID,
			ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS
			
			FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES 
			LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId
			LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID
			LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID
			WHERE --PROPERTY_ATTRIBUTES.PROPERTYID = PROPERTY.PROPERTYID
			 --AND  
			 PARAMETER.ParameterName = 'Quantity' 
			 AND  ITEM.ItemName = 'Bedrooms'
		) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID
		
WHERE JOURNALID =  @journalId 

--=================================================
-- Get CustomerId to get Tenant Detail(s) and Risk
--=================================================

DECLARE @customerId int = -1

SELECT
	@customerId = ISNULL(CW.CustomerId, -1)
FROM PLANNED_CONTRACTOR_WORK CW
WHERE CW.JournalId = @journalId


SELECT	@customerId = ISNULL(C__CUSTOMER.CustomerId, -1)		
FROM	PDR_JOURNAL
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN P__PROPERTY ON  PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID 
		INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID  AND C_TENANCY.ENDDATE IS NULL
		INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
										AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID)
																					FROM	C_CUSTOMERTENANCY 
																					WHERE	TENANCYID=C_TENANCY.TENANCYID 
																					)
		INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
WHERE	PDR_JOURNAL.JOURNALID = @journalId

--=================================================
--Get Customer/Tenant Detail(s)
--=================================================

SELECT
	C.CUSTOMERID AS [CUSTOMERID],
	FIRSTNAME AS [FIRSTNAME],
	MIDDLENAME AS [MIDDLENAME],
	LASTNAME AS [LASTNAME],
	TEL AS [TEL],
	T.DESCRIPTION [TITLE],
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '') + ISNULL(' ' + C.MIDDLENAME, '')
	+ ISNULL(' ' + C.LASTNAME, '') + ISNULL(', Tel:' + A.TEL, ''), ''), 'N/A') AS ContactDetail,
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '')
	+ ISNULL(' ' + C.LASTNAME, ''), ''), 'N/A') [FullName]
FROM	C__CUSTOMER C
		INNER JOIN G_TITLE T ON C.TITLE = T.TITLEID
		INNER JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1
WHERE	C.CUSTOMERID = @customerId

--=================================================
--Get Customer/Tenant Risk Detail(s)
--=================================================
SELECT
	RISKHISTORYID,
	CATDESC,
	SUBCATDESC
FROM dbo.RISK_CATS_SUBCATS(@customerId)

--=================================================
--Get Customer/Tenant Vulnerability Detail(s)
--=================================================
DECLARE @VULNERABILITYHISTORYID int = 0

SELECT
	@VULNERABILITYHISTORYID = VULNERABILITYHISTORYID
FROM C_JOURNAL J
INNER JOIN C_VULNERABILITY CV
	ON CV.JOURNALID = J.JOURNALID
WHERE CUSTOMERID = @customerId
AND ITEMNATUREID = 61
AND CV.ITEMSTATUSID <> 14
AND CV.VULNERABILITYHISTORYID = (SELECT
	MAX(VULNERABILITYHISTORYID)
FROM C_VULNERABILITY IN_CV
WHERE IN_CV.JOURNALID = J.JOURNALID)


EXECUTE VULNERABILITY_CAT_SUBCAT @VULNERABILITYHISTORYID


--=================================================
--Get Purchase Order Details.
--=================================================

SELECT 
	CONTRACTOR_WORK.PurchaseOrderId AS ORDERID,
	PURCHASE_ITEM.NETCOST AS NETCOST,
	PURCHASE_ITEM.VAT AS VAT,
	CONVERT(VARCHAR(15), T.ReletDate,103) AS RELETDATE,
	CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,ISNULL(JOURNAL.JOURNALID,-1)),4)) AS OurRef,
	PURCHASE_ITEM.ITEMDESC AS ItemNotes

	
FROM PDR_CONTRACTOR_WORK CONTRACTOR_WORK 
INNER JOIN F_PURCHASEITEM PURCHASE_ITEM ON CONTRACTOR_WORK.PurchaseOrderId = PURCHASE_ITEM.ORDERID
INNER JOIN PDR_JOURNAL JOURNAL ON CONTRACTOR_WORK.JournalId = JOURNAL.JOURNALID
INNER JOIN PDR_MSAT MSAT ON JOURNAL.MSATID = MSAT.MSATId  
INNER JOIN P__PROPERTY P ON MSAT.PropertyId = P.PROPERTYID
Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P.PropertyID
	AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,15) 
	GROUP BY PROPERTYID) as CJournal
Cross APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	 
INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID 
  

WHERE CONTRACTOR_WORK.JournalId = @journalId

--=================================================
--GET FIELD : JSV seperated by /
--=================================================

--SELECT 
		
--		 CAST(CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,ISNULL(RequiredWorksId,-1)),4)) AS VARCHAR(10))+'/'  [text()]
--         FROM V_RequiredWorks 
--         WHERE InspectionJournalId = @inspectionJournalId AND RoomId IS NULL
--         FOR XML PATH(''), TYPE


--=================================================
--GET FIELD : Supervisor seperated by /
--=================================================

SELECT 
	ISNULL(EMPLOYEE.FIRSTNAME,'') + ' ' + ISNULL(EMPLOYEE.LASTNAME,'') AS SUPERVISOR
 FROM 
	PDR_APPOINTMENTS APPOINTMENTS
INNER JOIN 	E__EMPLOYEE EMPLOYEE ON APPOINTMENTS.ASSIGNEDTO = EMPLOYEE.EMPLOYEEID

WHERE JOURNALID = @inspectionJournalId

END
GO