USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_ItemByAreaId]    Script Date: 02/11/2016 14:58:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Exec AS_ItemByAreaId @areaId=1  
-- Author: <Ali Raza>  
-- Create date: <11/02/2016>  
-- Description: <Get Items by Area id for Attributes Tree>  
IF OBJECT_ID('dbo.PDR_ItemByAreaId') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_ItemByAreaId AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
-- =============================================  
ALTER PROCEDURE [dbo].[PDR_ItemByAreaId](  
@areaId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT DISTINCT item.* ,ISNULL( Si.SubItem ,0) as  SubItem
 From PA_ITEM item 
 Outer APPLY (Select Count(I.ItemId)as SubItem  From PA_ITEM I Where I.ParentItemId= item.ItemID and I.IsActive = 1 Group By I.ItemId  ) as SI
 Where item.AreaID = @areaId and item.IsActive = 1  and item.ParentItemId is null AND item.IsForSchemeBlock = 1
 ORDER BY ItemName ASC 
END  
