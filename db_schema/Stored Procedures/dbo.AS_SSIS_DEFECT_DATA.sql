
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[AS_SSIS_DEFECT_DATA]
AS 
    SELECT TOP 100 PERCENT
            CAST(pp.[PROPERTYID] AS NVARCHAR(20)) AS [Property Ref] ,
            CAST(ISNULL(pp.FLATNUMBER, '') AS NVARCHAR(50)) AS [Flat No.] ,
            CAST(ISNULL(pp.HOUSENUMBER, '') AS NVARCHAR(50)) AS [House No.] ,
            CAST(ISNULL(pp.ADDRESS1, '') AS NVARCHAR(100)) AS [Address Line 1] ,
            CAST(ISNULL(pp.ADDRESS2, '') AS NVARCHAR(100)) AS [Address Line 2] ,
            CAST(ISNULL(pp.ADDRESS3, '') AS NVARCHAR(100)) AS [Address Line 3] ,
            CAST(ISNULL(pp.TOWNCITY, '') AS NVARCHAR(80)) AS [Town/City] ,
            CAST(ISNULL(pp.POSTCODE, '') AS NVARCHAR(10)) AS [Postcode] ,
            CAST(ISNULL(pp.COUNTY, '') AS NVARCHAR(50)) AS [County] ,
            CAST(ISNULL(gat.APPLIANCETYPE, '') AS NVARCHAR(100)) AS [ApplianceType] ,
            CAST(ISNULL(ppad.[SerialNumber], '') AS NVARCHAR(10)) AS [Serial Number] ,
            CAST(ISNULL(gm.Manufacturer, '') AS NVARCHAR(50)) AS [Manufacturer] ,
            CAST(ISNULL(gpa.Model, '') AS NVARCHAR(50)) AS [Model] ,
            CAST(ISNULL(gl.[LOCATION], '') AS NVARCHAR(50)) AS [Location] ,
            CAST(ISNULL(pdc.[Description], '') AS NVARCHAR(30)) AS [Category] ,
            CAST(( CASE WHEN ppad.[IsDefectIdentified] = 1 THEN 'Yes'
                        ELSE 'No'
                   END ) AS NVARCHAR(10)) AS [Defect Identified] ,
            CAST(REPLACE(REPLACE(ISNULL(ppad.DefectNotes, ''), '%20', ' '),
                         '%0A', CHAR(13) + CHAR(10)) AS NVARCHAR(200)) AS [Defect Notes] ,
            CAST(( CASE WHEN ppad.[IsActionTaken] = 1 THEN 'Yes'
                        ELSE 'No'
                   END ) AS NVARCHAR(10)) AS [Action Taken] ,
            CAST(REPLACE(REPLACE(ISNULL(ppad.ActionNotes, ''), '%20', ' '),
                         '%0A', CHAR(13) + CHAR(10)) AS NVARCHAR(200)) AS [Action Notes] ,
            CAST(( CASE WHEN ppad.[IsWarningIssued] = 1 THEN 'Yes'
                        ELSE 'No'
                   END ) AS NVARCHAR(10)) AS [Warning Issued] ,
            CAST(( CASE WHEN ppad.[IsWarningFixed] = 1 THEN 'Yes'
                        ELSE 'No'
                   END ) AS NVARCHAR(10)) AS [Warning Fixed] ,
            CAST(CONVERT(VARCHAR(50), ppad.[DefectDate], 103) AS NVARCHAR(25)) AS [Defect Date] ,
            ISNULL(( E.FIRSTNAME + SPACE(1) + E.LASTNAME ), '') AS Operative
    FROM    P_PROPERTY_APPLIANCE_DEFECTS ppad
            LEFT JOIN [dbo].[P_DEFECTS_CATEGORY] AS pdc ON [ppad].[CategoryId] = [pdc].[CategoryId]
            LEFT JOIN [dbo].[P__PROPERTY] AS pp ON PP.[PROPERTYID] = ppad.[PropertyId]
            LEFT JOIN [dbo].[GS_PROPERTY_APPLIANCE] gpa ON gpa.[PROPERTYAPPLIANCEID] = ppad.[ApplianceId]
            LEFT JOIN [dbo].[GS_LOCATION] gl ON GL.[LOCATIONID] = GPA.[LOCATIONID]
            LEFT JOIN [dbo].[GS_MANUFACTURER] AS gm ON gm.[MANUFACTURERID] = gpa.[MANUFACTURERID]
            LEFT JOIN [dbo].[GS_APPLIANCE_TYPE] AS gat ON GAT.[APPLIANCETYPEID] = [gpa].[APPLIANCETYPEID]
            LEFT JOIN [dbo].[AS_APPOINTMENTS] app ON app.JournalId = ppad.JournalId
            LEFT JOIN [dbo].[E__EMPLOYEE] e ON e.EMPLOYEEID = app.ASSIGNEDTO
    WHERE   pp.[FUELTYPE] = 1
            AND CAST(ppad.[DefectDate] AS DATE) >= DATEADD(DAY, -1, CAST(GETDATE() AS DATE))
    ORDER BY ppad.PropertyDefectId ASC







GO
