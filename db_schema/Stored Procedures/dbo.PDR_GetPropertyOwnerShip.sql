USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyOwnerShip]    Script Date: 20/03/2019 5:54:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property OwnerShip for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property OwnerShip for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyOwnerShip
  =================================================================================*/
IF OBJECT_ID('dbo.PDR_GetPropertyOwnerShip') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.PDR_GetPropertyOwnerShip AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
ALTER PROCEDURE [dbo].[PDR_GetPropertyOwnerShip]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OWNERSHIPID, DESCRIPTION FROM P_OWNERSHIP order by Description ASC
	
END
