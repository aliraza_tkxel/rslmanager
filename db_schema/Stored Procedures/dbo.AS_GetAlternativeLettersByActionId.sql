SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_GetAlternativeLettersByActionId @statusId = 0
-- Author:		<Noor Muhammad>
-- Create date: <16/10/2012>
-- Description:	<Get Letters against Action Id>
-- Web Page: PropertyRecord.aspx
-- =============================================
IF OBJECT_ID('dbo.AS_GetAlternativeLettersByActionId') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetAlternativeLettersByActionId AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[AS_GetAlternativeLettersByActionId](
	-- Add the parameters for the stored procedure here
	@actionId int = -1
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	  SELECT StandardLetterId,Title from AS_StandardLetters WHERE ActionId = @actionId AND IsAlternativeServicing = 1

END
GO
