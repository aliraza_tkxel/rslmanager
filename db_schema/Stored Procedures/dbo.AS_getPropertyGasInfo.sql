
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC AS_getPropertyGasInfo @propertyId = 1 
-- Author:		<Salman Nazir>
-- Create date: <30/10/2012>
-- Description:	<Get all Gas Info of a Property on Attribute Tab >
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_getPropertyGasInfo](
@propertyId varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CONVERT(VARCHAR(10), ISSUEDATE, 103) AS ISSUEDATE, CONVERT(VARCHAR(10), DATEADD(YYYY,1,ISSUEDATE),103) AS RENEWALDATE,CP12NUMBER,ISNULL(VALUEID,0)
	FROM P_LGSR left JOIN
	PA_PROPERTY_ATTRIBUTES on P_LGSR.PROPERTYID = PA_PROPERTY_ATTRIBUTES.PROPERTYID
	WHERE P_LGSR.PROPERTYID = @propertyId
END
GO
