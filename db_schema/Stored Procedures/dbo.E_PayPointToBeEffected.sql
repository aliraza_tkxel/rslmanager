USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_PayPointToBeEffected]    Script Date: 07/30/2018 15:53:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.E_PayPointToBeEffected') IS NULL 
	EXEC('CREATE PROCEDURE dbo.E_PayPointToBeEffected AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_PayPointToBeEffected]
AS
BEGIN

	

DECLARE @EmployeeId  INT = 0,
	@Grade  INT = '',
	@GradePoint  INT = 0,
	@Salary  float = 0.0

	DECLARE Notification_CURSOR 
	CURSOR FOR 
	select Salary as Salary, Grade as Grade, GradePoint as GradePoint, employeeId as EmployeeId
	from E_PaypointSubmission where authorized = 1 and cast(DateProposed as Date) = cast(getdate() as Date)
	
	OPEN Notification_CURSOR

	FETCH NEXT FROM Notification_CURSOR	
	into @EmployeeId, @Grade, @GradePoint, @Salary

	WHILE @@FETCH_STATUS = 0 
    BEGIN 
		update E_JOBDETAILS set GRADE= @Grade, GRADEPOINT= @GradePoint, PREVIOUSSALARY = SALARY, SALARY = @Salary where EMPLOYEEID = @EmployeeId

		FETCH NEXT FROM Notification_CURSOR INTO @EmployeeId, @Grade, @GradePoint, @Salary
    END 
CLOSE Notification_CURSOR  
 DEALLOCATE Notification_CURSOR 

END	

	
