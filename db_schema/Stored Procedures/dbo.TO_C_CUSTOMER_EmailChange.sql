SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[TO_C_CUSTOMER_EmailChange]

/*	===============================================================
	'   NAME:           TO_C_CUSTOMER_EMAILCHANGE
	'   DATE CREATED:   27 MAY 2008
	'   CREATED BY:     Munawar Nadeem
	'   CREATED FOR:    Broadland Housing
	'   PURPOSE:        To change Email address (used as Username) once previous username & password is verified
	'   UPDATE:         C_ADDRESS.EMAIL
	'   IN:             @newEmail, @currEmail, @Password
	'   OUT: 	        @CustomerID
	'   RETURN:         Nothing    
	'   VERSION:        1.0           
	'   COMMENTS:       
	'   MODIFIED ON:    
	'   MODIFIED BY:    
	'   REASON MODIFICATION: 
	'===============================================================*/

    @newEmail varchar (50),
    @currEmail varchar (50),
    @password varchar (50),
    
    @customerID int OUTPUT    
    

AS

    /* This query verifies email & password and selects customerID */ 
 
  SELECT 
		@customerID = Customer.CUSTOMERID
                                                                     
  FROM  
		C__CUSTOMER AS Customer INNER JOIN C_ADDRESS AS CustomerAddress                    
        ON Customer.CUSTOMERID = CustomerAddress.CUSTOMERID
                                  
        INNER JOIN TO_LOGIN AS Login
        ON Customer.CUSTOMERID = Login.CustomerID
                                  
  WHERE 
        (CustomerAddress.EMAIL = @currEmail) AND (Login.Password = @password)
        AND (CustomerAddress.ISDEFAULT = 1)  AND (Login.Active = 1)
        


/* ' If customerID found (means email & password verified, email address will be updated with new value */     

/* 'If customer ID is NULL(authentication failed), return back without updating email */
IF @customerID IS NULL

  BEGIN  
        SET @customerID = -1
        RETURN  
  END
 
 
 
 /* ' Otherwise update email against customerID */
                            
	UPDATE    
	          C_ADDRESS
	
    SET
              C_ADDRESS.EMAIL = @newEmail

    WHERE 
              ( C_ADDRESS.CUSTOMERID = @customerID )

GO
