SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.TO_COMPLAINT_AddComplaint
/* ===========================================================================
 '   NAME:           TO_COMPLAINT_AddComplaint
 '   DATE CREATED:   08 JUNE 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To add complaint record, it first adds rocord in TO_ENQUIRY_LOG base table and then CREATE
 '					 complaint record in TO_COMPLAINT
 '   IN:             @MovingOutDate
 '   IN:             @CreationDate
 '   IN:             @Description
 '   IN:             @ItemStatusID
 '   IN:             @TenancyID
 '   IN:             @CustomerID
 '   IN:             @ItemNatureID
 '   IN:             @CategoryId
 '
 '   OUT:            @ComplaintId
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CategoryId INT,
	@CreationDate SMALLDATETIME,
	@Description NVARCHAR(4000),
	@ItemStatusID INT,
	@TenancyID INT,
	@CustomerID INT,
	@ItemNatureID INT,
	@ComplaintId INT OUTPUT
	)
	
AS
	DECLARE @EnquiryLogId INT
	
	BEGIN TRAN
	INSERT INTO TO_ENQUIRY_LOG(
	CreationDate,
	Description,
	ItemStatusID,
	TenancyID,
	CustomerID,
	ItemNatureID
	)

VALUES(
@CreationDate,
@Description,
@ItemStatusID,
@TenancyID,
@CustomerID,
@ItemNatureID
)


SELECT @EnquiryLogID=EnquiryLogID 
FROM TO_ENQUIRY_LOG 
WHERE EnquiryLogID = @@IDENTITY

	IF @EnquiryLogID > 0
		BEGIN
		INSERT INTO TO_COMPLAINT(
		CategoryID,
		EnquiryLogID
		)
		VALUES(
		@CategoryId,
		@EnquiryLogID
		)
		
		SELECT @ComplaintId=ComplaintID
	FROM TO_COMPLAINT
	WHERE ComplaintID = @@IDENTITY

				IF @ComplaintId<0
					BEGIN
						SET @ComplaintId=-1
						ROLLBACK TRAN
					END
				ELSE
					COMMIT TRAN		
		END
		ELSE
			BEGIN
			SET @ComplaintId=-1
			ROLLBACK TRAN
			END
	
	
	
	
	
	
	
	
	
	




GO
