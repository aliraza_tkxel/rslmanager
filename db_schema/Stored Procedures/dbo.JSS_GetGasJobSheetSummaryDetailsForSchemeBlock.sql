USE [RSLBHALive]
GO

IF OBJECT_ID('dbo.[JSS_GetGasJobSheetSummaryDetailsForSchemeBlock]') IS NULL
EXEC ('CREATE PROCEDURE dbo.[JSS_GetGasJobSheetSummaryDetailsForSchemeBlock] AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE [dbo].[JSS_GetGasJobSheetSummaryDetailsForSchemeBlock]
	-- Add the parameters for the stored procedure here
	@JobSheetNumber nvarchar(20)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @appointmentType varchar(50)
	SELECT TOP 1 @appointmentType = CASE WHEN AS_JOURNAL.SchemeID IS NOT NULL THEN 'Scheme' 
				WHEN AS_JOURNAL.BlockID is not null THEN 'Block' 
				END
	FROM AS_APPOINTMENTS
	INNER JOIN AS_JOURNAL ON AS_JOURNAL.JOURNALID = AS_APPOINTMENTS.JOURNALID
	WHERE ('JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber)

	if @appointmentType = 'Scheme'
	BEGIN
		SELECT DISTINCT  
		Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER )
		as JobsheetNumber,
		P_SCHEME.SCHEMEID as PropertyId,
		CONVERT(varchar(20), APPOINTMENTDATE, 103) AS StartDate, 
		CONVERT(varchar(20), APPOINTMENTDATE, 103) AS EndDate,  
		AS_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
		ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, ' ')				AS Operative,
		CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) AS [StartTime],
		AS_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
		CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTENDTIME, 108) AS EndTime,      
		'GAS' AS AppointmentType,
		ISNULL( AS_APPOINTMENTS.NOTES,'' ) AS CustomerNotes ,
		AS_APPOINTMENTS.APPOINTMENTSTATUS as Status,
		'Appliance' AS InspectionType  ,
		ISNULL(AS_APPOINTMENTS.IsVoid,-1) AS IsVoid ,      
		'Gas' as ddlAppointmentType,
		ISNULL(P_SCHEME.SCHEMENAME, '')  AS [Address],
		ISNULL(PropertyScheme.TownCity, 'N/A') AS TownCity, 
		ISNULL(PropertyScheme.County, 'N/A') AS COUNTY,
		ISNULL(PropertyScheme.PostCode, 'N/A') AS POSTCODE,
		'N/A' AS ClientName, 
		'N/A' AS ClientTel,
		'N/A' AS ClientMobile,
		'N/A' AS ClientEmail,
		ISNULL(P_SCHEME.SCHEMENAME, 'N/A') AS SCHEMENAME ,'NA' AS CP12Expiry, 'NA'as CP12Issued
		FROM AS_APPOINTMENTS  
		INNER JOIN E__EMPLOYEE AS E ON AS_APPOINTMENTS.ASSIGNEDTO = E.EMPLOYEEID 
		--CROSS APPLY (SELECT TOP 1 * FROM AS_APPJOURNAL_MAPPING WHERE APPOINTMENTID = AS_APPOINTMENTS.APPOINTMENTID) APPJOURNAL
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID	
		--INNER JOIN PA_HeatingMapping ON PA_HeatingMapping.HeatingMappingId = AS_JOURNAL.HeatingMappingId
		INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID  
		INNER JOIN P_SCHEME ON AS_JOURNAL.SchemeID = P_SCHEME.SCHEMEID
		
		OUTER APPLY
		(SELECT TOP 1 sch.SchemeId, P__Property.TownCity, P__Property.County, P__Property.PostCode from P__Property
		INNER JOIN P_SCHEME sch on P__Property.SchemeId = Sch.SchemeId
		WHERE P__Property.SchemeId = P_SCHEME.SchemeId) as PropertyScheme  
		--LEFT JOIN P_LGSR pl ON pl.HeatingMappingId = PA_HeatingMapping.HeatingMappingId
		WHERE  
		('JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber)
      
	  ---////////////////////////////////////////////////////////////////////
	  ---//////////////////////Get Asbestos risk ///////////////////////////
	  ---//////////////////////////////////////////////////////////////////    
   
		Select P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
			P_Asbestos.RISKDESCRIPTION Description,P_PROPERTY_ASBESTOS_RISKLEVEL.DateAdded
		From (	SELECT (AJ.SchemeID) AS Scheme
				FROM AS_JOURNAL AJ
				--INNER JOIN AS_APPJOURNAL_MAPPING ON AS_APPJOURNAL_MAPPING.JOURNALID = AJ.JOURNALID
				INNER JOIN AS_APPOINTMENTS ON AJ.JOURNALID= AS_APPOINTMENTS.JournalId
				--INNER JOIN PA_HeatingMapping ON PA_HeatingMapping.HeatingMappingId = AJ.HeatingMappingId
				WHERE ( 'JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber) 
				) AS AJS
				INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.SchemeId = AJS.Scheme And P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved IS NULL
				INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
				INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID	  
			Where (P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved is null or CONVERT(DATE,P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved) > convert(date, getdate()) )
		AND P_ASBESTOS.other = 0

		select 'Boiler '+ Convert(varchar,ROW_NUMBER() OVER(ORDER BY AS_JournalHeatingMapping.HeatingMappingId ASC)) AS BoilerName, PV.ValueDetail AS HeatingFuel,
			PVT.ValueDetail AS BoilerType, PVMAN.ValueDetail as Manufacturer,
			ISNULL(RIGHT('00' + CAST(DATEPART(DAY, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS VARCHAR(2)), 2) + ' ' +
						DATENAME(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) + ' ' +
						CAST(DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE))  AS VARCHAR(4)), 'N/A') AS CP12Expiry,
			ISNULL(RIGHT('00' + CAST(DATEPART(DAY, P_LGSR.ISSUEDATE) AS VARCHAR(2)), 2) + ' ' +
						DATENAME(MONTH, P_LGSR.ISSUEDATE) + ' ' +
						CAST(DATEPART(YEAR, P_LGSR.ISSUEDATE)  AS VARCHAR(4)), 'N/A') AS CP12Issued
		FROM P_SCHEME 
		
		INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.SchemeId=P_SCHEME.SchemeId
		INNER JOIN AS_JournalHeatingMapping ON AS_JournalHeatingMapping.JOURNALID=AS_JOURNAL.JOURNALID
		LEFT JOIN AS_APPOINTMENTS APP ON APP.JournalId = AS_JOURNAL.JournalId								
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		LEFT JOIN (SELECT P_LGSR.ISSUEDATE,HeatingMappingId from P_LGSR)as P_LGSR on AS_JournalHeatingMapping.HeatingMappingId = P_LGSR.HeatingMappingId
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.SchemeId = P_SCHEME.SCHEMEID AND A.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
		AND A.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Heating Fuel' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES HT ON HT.SchemeId = P_SCHEME.SCHEMEID AND HT.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
		AND HT.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Boiler Type' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVT ON HT.VALUEID = PVT.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES MANUF ON MANUF.SchemeId = P_SCHEME.SCHEMEID AND MANUF.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
		AND MANUF.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Manufacturer' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVMAN ON MANUF.VALUEID = PVMAN.ValueID
		where ('JSG' + Convert( NVarchar, APP.JSGNUMBER ) = @JobSheetNumber) AND PV.ValueDetail = 'Mains Gas'
				--AND AS_JOURNAL.ISCURRENT = 1
		ORDER BY AS_JournalHeatingMapping.HeatingMappingId

	END
	ELSE IF @appointmentType = 'Block'
	BEGIN
		SELECT DISTINCT  
		Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER )
		as JobsheetNumber,
		P_BLOCK.BLOCKID as PropertyId,
		CONVERT(varchar(20), APPOINTMENTDATE, 103) AS StartDate, 
		CONVERT(varchar(20), APPOINTMENTDATE, 103) AS EndDate,  
		AS_APPOINTMENTS.ASSIGNEDTO AS OperativeID,  
		ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, ' ')				AS Operative,
		CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) AS [StartTime],
		AS_APPOINTMENTS.APPOINTMENTID AS AppointmentID,  
		CONVERT(varchar(5), AS_APPOINTMENTS.APPOINTMENTENDTIME, 108) AS EndTime,      
		'GAS' AS AppointmentType,
		ISNULL( AS_APPOINTMENTS.NOTES,'' ) AS CustomerNotes ,
		AS_APPOINTMENTS.APPOINTMENTSTATUS as Status,
		'Appliance' AS InspectionType  ,
		ISNULL(AS_APPOINTMENTS.IsVoid,-1) AS IsVoid ,      
		'Gas' as ddlAppointmentType,
		ISNULL(ISNULL(P_BLOCK.BLOCKNAME , '') + 
		ISNULL(', ' + P_BLOCK.ADDRESS1, '')+
		ISNULL(', ' + P_BLOCK.ADDRESS2, '') + ISNULL(' ' + P_BLOCK.ADDRESS3, '') 
		, '') AS [Address],
		ISNULL(P_BLOCK.TOWNCITY, '') TownCity, ISNULL(P_BLOCK.COUNTY, '') COUNTY,
		ISNULL(P_BLOCK.POSTCODE,'') POSTCODE,
		'N/A' AS ClientName, 
		'N/A' AS ClientTel,
		'N/A' AS ClientMobile,
		'N/A' AS ClientEmail,
		ISNULL(P_SCHEME.SCHEMENAME, 'N/A') AS SCHEMENAME ,'NA' AS CP12Expiry, 'NA'as CP12Issued
		FROM AS_APPOINTMENTS  
		INNER JOIN E__EMPLOYEE AS E ON AS_APPOINTMENTS.ASSIGNEDTO = E.EMPLOYEEID 
		--CROSS APPLY (SELECT TOP 1 * FROM AS_APPJOURNAL_MAPPING WHERE APPOINTMENTID = AS_APPOINTMENTS.APPOINTMENTID) APPJOURNAL
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID	
		--INNER JOIN PA_HeatingMapping ON PA_HeatingMapping.HeatingMappingId = AS_JOURNAL.HeatingMappingId
		INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID  
		INNER JOIN P_BLOCK ON AS_JOURNAL.BlockId = p_block.BlockId
		INNER JOIN P_SCHEME ON P_SCHEME.SCHEMEID = p_block.SCHEMEID  
		--LEFT JOIN P_LGSR pl ON pl.HeatingMappingId = PA_HeatingMapping.HeatingMappingId
		WHERE  
		('JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber)
      
	  ---////////////////////////////////////////////////////////////////////
	  ---//////////////////////Get Asbestos risk ///////////////////////////
	  ---//////////////////////////////////////////////////////////////////    
   
		Select P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
			P_Asbestos.RISKDESCRIPTION Description,P_PROPERTY_ASBESTOS_RISKLEVEL.DateAdded
		From (	SELECT (AJ.BlockId) AS bLOCK
				FROM AS_JOURNAL AJ
				--INNER JOIN AS_APPJOURNAL_MAPPING ON AS_APPJOURNAL_MAPPING.JOURNALID = AJ.JOURNALID
				INNER JOIN AS_APPOINTMENTS ON AJ.JOURNALID= AS_APPOINTMENTS.JournalId
				--INNER JOIN PA_HeatingMapping ON PA_HeatingMapping.HeatingMappingId = AJ.HeatingMappingId
				WHERE ( 'JSG' + Convert( NVarchar, AS_APPOINTMENTS.JSGNUMBER ) = @JobSheetNumber) 
				) AS AJS
				INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.BlockId = AJS.bLOCK And P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved IS NULL
				INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
				INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID	  
			Where (P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved is null or CONVERT(DATE,P_PROPERTY_ASBESTOS_RISKLEVEL.DateRemoved) > convert(date, getdate()) )
		AND P_ASBESTOS.other = 0

		select 'Boiler '+ Convert(varchar,ROW_NUMBER() OVER(ORDER BY AS_JournalHeatingMapping.HeatingMappingId ASC)) AS BoilerName, PV.ValueDetail AS HeatingFuel,
			PVT.ValueDetail AS BoilerType, PVMAN.ValueDetail as Manufacturer,
			ISNULL(RIGHT('00' + CAST(DATEPART(DAY, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) AS VARCHAR(2)), 2) + ' ' +
						DATENAME(MONTH, DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) + ' ' +
						CAST(DATEPART(YEAR, DATEADD(YEAR,1,P_LGSR.ISSUEDATE))  AS VARCHAR(4)), 'N/A') AS CP12Expiry,
			ISNULL(RIGHT('00' + CAST(DATEPART(DAY, P_LGSR.ISSUEDATE) AS VARCHAR(2)), 2) + ' ' +
						DATENAME(MONTH, P_LGSR.ISSUEDATE) + ' ' +
						CAST(DATEPART(YEAR, P_LGSR.ISSUEDATE)  AS VARCHAR(4)), 'N/A') AS CP12Issued
		FROM p_block 
		--INNER JOIN PA_HeatingMapping HM ON HM.BlockId = p_block.BlockId
		INNER JOIN dbo.AS_JOURNAL ON dbo.AS_JOURNAL.BlockId=p_block.BlockId
		INNER JOIN AS_JournalHeatingMapping ON AS_JournalHeatingMapping.JOURNALID=AS_JOURNAL.JOURNALID
		LEFT JOIN AS_APPOINTMENTS APP ON APP.JOURNALID = AS_JOURNAL.JOURNALID								
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		LEFT JOIN (SELECT P_LGSR.ISSUEDATE,HeatingMappingId from P_LGSR)as P_LGSR on AS_JournalHeatingMapping.HeatingMappingId = P_LGSR.HeatingMappingId
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.BlockId = p_block.BlockId AND A.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
		AND A.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Heating Fuel' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PV ON A.VALUEID = PV.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES HT ON HT.BlockId = p_block.BlockId AND HT.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
		AND HT.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Boiler Type' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVT ON HT.VALUEID = PVT.ValueID
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES MANUF ON MANUF.BlockId = p_block.BlockId AND MANUF.HeatingMappingId = AS_JournalHeatingMapping.HeatingMappingId
		AND MANUF.ITEMPARAMID = (	SELECT	ItemParamID
								FROM	PA_ITEM_PARAMETER
										INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
										INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
								WHERE	ParameterName = 'Manufacturer' AND ItemName = 'Boiler Room')
		LEFT JOIN PA_PARAMETER_VALUE PVMAN ON MANUF.VALUEID = PVMAN.ValueID
		where ('JSG' + Convert( NVarchar, APP.JSGNUMBER ) = @JobSheetNumber) AND PV.ValueDetail = 'Mains Gas'
				--AND AS_JOURNAL.ISCURRENT = 1
		ORDER BY AS_JournalHeatingMapping.HeatingMappingId

	END
END