USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetConfirmedAppointmentByJsn]    Script Date: 02/18/2015 20:12:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Behroz Sikander
-- Create date: 20/03/2013
-- Description:	This stored procedure fetches the data based on JSN number
-- EXEC FL_GetConfirmedAppointmentByJsn
-- @jsn = 'JS6'
-- WebPage: ReArrangingCurrentFault.aspx
-- =============================================

IF OBJECT_ID('dbo.FL_GetConfirmedAppointmentByJsn') IS NULL 
	EXEC('CREATE PROCEDURE dbo.FL_GetConfirmedAppointmentByJsn AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[FL_GetConfirmedAppointmentByJsn]
	-- Add the parameters for the stored procedure here
	@Jsn VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
	DISTINCT
		FL_FAULT_LOG.FaultLogID
		,FL_FAULT.FaultID
		,FL_FAULT_LOG.JobSheetNumber
		,FL_AREA.AreaName
		,FL_FAULT.[Description]
		,COALESCE(FL_FAULT_LOG.Duration,FL_FAULT.Duration) Duration
		,CASE
			WHEN COALESCE(FL_FAULT_LOG.Duration,FL_FAULT.Duration) IS NULL
				THEN 'N/A'
			WHEN COALESCE(FL_FAULT_LOG.Duration,FL_FAULT.Duration) <= 1
				THEN CONVERT(VARCHAR(10), COALESCE(FL_FAULT_LOG.Duration,FL_FAULT.Duration)) + ' hr'
			ELSE CONVERT(VARCHAR(10), COALESCE(FL_FAULT_LOG.Duration,FL_FAULT.Duration)) + ' hrs'
		END																							AS DurationString
		,FL_FAULT_LOG.ProblemDays
		,FL_FAULT_LOG.RecuringProblem
		,FL_FAULT_LOG.CommunalProblem
		,DATENAME(D, FL_FAULT_LOG.DueDate) + ' ' + CONVERT(VARCHAR(3), FL_FAULT_LOG.DueDate, 100)	AS DueDate
		,FL_FAULT_LOG.Notes
		,FL_FAULT.IsGasSafe
		,FL_FAULT.IsOftec
		,FL_FAULT_LOG.Quantity
		,FL_FAULT_LOG.Recharge
		,G_TRADE.[Description]																		AS TradeName
		,''																							AS OrgId
		,''																							AS IsAppointmentConfirmed
		,0																							AS tempFaultId
		,FL_FAULT_LOG.FaultTradeId
		,G_TRADE.TradeId
		,FL_Fault_Priority.PriorityName
		,(CASE
			WHEN FL_Fault_Priority.Days = 1
				THEN CONVERT(NVARCHAR(50), FL_Fault_Priority.ResponseTime) + ' days'
			ELSE CONVERT(NVARCHAR(50), FL_Fault_Priority.ResponseTime) + ' hours'
		END)																						AS Response
		,(CASE
			days
			WHEN 1
				THEN CONVERT(CHAR(12), DATEADD(DAY, FL_Fault_Priority.ResponseTime, GETDATE()), 103)
			ELSE CONVERT(CHAR(12), DATEADD(HOUR, FL_Fault_Priority.ResponseTime, GETDATE()), 103)
		END)																						AS CompleteDueDate
		,FL_FAULT_APPOINTMENT.AppointmentId
	FROM
		FL_FAULT_LOG
			LEFT OUTER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId
			LEFT OUTER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
			INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultId = FL_FAULT.FaultID
			INNER JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID
			INNER JOIN FL_Fault_Trade ON FL_FAULT_LOG.FaultTradeId = FL_Fault_Trade.FaultTradeId
			INNER JOIN G_TRADE ON FL_Fault_Trade.TradeId = G_TRADE.TradeId
			INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
	WHERE
		FL_FAULT_LOG.JobSheetNumber = @Jsn
END