USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AD_SEARCHFORAGEDDEBOTREPORT]    Script Date: 10/11/2013 21:19:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,9/23/2013,>
-- Description:	<Description,,>
--EXEC	[dbo].[AD_SEARCHFORAGEDDEBOTREPORT]
--		@salesCustName = N'',
--		@empName = N'',
--		@orgName = N'',
--		@tanentOnly = 0,
--		@SABalance = 0.0

-- =============================================

ALTER PROCEDURE [dbo].[AD_SEARCHFORAGEDDEBOTREPORT] 
-- Add the parameters for the stored procedure here
( 
@salesCustName varchar(100) ,
 @empName varchar(100) ,
 @orgName varchar(100) ,
 @tanentOnly bit 
 ,@SABalance money
 ,@pageSize int = 25,
 @pageNumber int = 1
 --, @totalCount int = 0 output
 ) AS BEGIN 
 
 DECLARE @SelectClause varchar(2000),
	@fromClause varchar(1500),
	@whereClause varchar(1500),
	@innerWhereClause varchar(1500),
	@orderClause varchar(100),
	@mainSelectQuery varchar(5500),
	@rowNumberQuery varchar(6000),
	@finalQuery varchar(6500),
	@searchCriteria varchar(1500),	
	@InsertClause varchar(2000),
	@offset int,
	@limit int
	
--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1
 --========================================================================================
 -- Begin building SearchCriteria clause
 -- These conditions will be added into where clause based on search criteria provided

SET @searchCriteria = ' 1=1  ' 
IF(@salesCustName != '' OR @salesCustName != NULL) 
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) +' AND SC.CONTACT LIKE ''%'+@salesCustName+'%'' OR SC.ORGANISATION LIKE ''%'+@salesCustName+'%'' ' 
	
	SET @searchCriteria = @searchCriteria + CHAR(10) +' OR (CNGV.List LIKE ''%'+ @salesCustName+'%'') ' 
END
IF(@empName != ''OR @empName != NULL) 
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  E.FIRSTNAME + '' '' + E.LASTNAME LIKE ''%'+ @empName+ '%'' OR E.FIRSTNAME LIKE ''%'+ @empName +'%'' OR E.LASTNAME LIKE ''%'+@empName+ '%'' '
END
IF(@orgName != '' OR @orgName != NULL) 
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) +' AND O.NAME LIKE ''%'+ @orgName+'%''' 
END 

IF(@SABalance != ''   OR @SABalance != NULL)
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (tSAB.BALANCE >' + CHAR(10) +convert(varchar,@SABalance)+' OR SAB.BALANCE > '+ CHAR(10) +convert(varchar,@SABalance)+' OR sSAB.BALANCE >'+ CHAR(10) +convert(varchar,@SABalance)+' OR cSAB.BALANCE >'+ CHAR(10) +convert(varchar,@SABalance) +')'
END 
-- End building SearchCriteria clause
 --========================================================================================
 -- Begin building tenent only
 --=============================================================================================
 IF @tanentOnly = 1 BEGIN
SET @innerWhereClause = 'TENANCYID IS NOT NULL ' END ELSE BEGIN
SET @innerWhereClause = 'TENANCYID IS NOT NULL OR SUPPLIERID IS NOT NULL OR EMPLOYEEID IS NOT NULL OR SALESCUSTOMERID IS NOT NULL '
 END --=============================================================================================

CREATE TABLE #TempTableSearch(AccountType INT, AccountID INT )
--======================================================================================== 
--======================================================================================== 
--Begin Insert Statement
SET @InsertClause ='INSERT INTO #TempTableSearch(AccountType,AccountID)'
--======================================================================================== 

 --======================================================================================== 
 -- Begin building SELECT clause
 -- Insert statements for procedure here

SET @selectClause = 'SELECT row_number() over (order by NAME DESC) AS row,
	 CASE
           WHEN SD.TENANCYID IS NOT NULL THEN ''1''
           WHEN SD.SUPPLIERID IS NOT NULL THEN ''3''
           WHEN SD.EMPLOYEEID IS NOT NULL THEN ''2''
           WHEN SD.SALESCUSTOMERID IS NOT NULL THEN ''4''
       END AccountType,
       CASE
           WHEN SD.TENANCYID IS NOT NULL THEN CAST(SD.TENANCYID AS VARCHAR)
           WHEN SD.SUPPLIERID IS NOT NULL THEN CAST(SD.SUPPLIERID AS VARCHAR)
           WHEN SD.EMPLOYEEID IS NOT NULL THEN CAST(SD.EMPLOYEEID AS VARCHAR)
           WHEN SD.SALESCUSTOMERID IS NOT NULL THEN CAST(SD.SALESCUSTOMERID AS VARCHAR)
       END AccountID '
SET @fromClause=' FROM
(SELECT TENANCYID, SUPPLIERID, EMPLOYEEID, SALESCUSTOMERID FROM F_SALESINVOICE
 WHERE '+@innerWhereClause+' UNION SELECT TENANCYID, SUPPLIERID, EMPLOYEEID, SALESCUSTOMERID
 FROM F_SALESPAYMENT WHERE '+@innerWhereClause+' ) SD
LEFT JOIN C_TENANCY T ON T.TENANCYID = SD.TENANCYID
LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = SD.TENANCYID
LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = SD.EMPLOYEEID
LEFT JOIN S_ORGANISATION O ON O.ORGID = SD.SUPPLIERID
LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = SD.SALESCUSTOMERID
LEFT JOIN F_SALESACCOUNTBALANCE tSAB ON tSAB.TENANCYID  = SD.TENANCYID 
LEFT JOIN F_SALESACCOUNTBALANCE SAB ON SAB.EMPLOYEEID   = SD.EMPLOYEEID 
LEFT JOIN F_SALESACCOUNTBALANCE sSAB ON sSAB.SUPPLIERID   = SD.SUPPLIERID  
LEFT JOIN F_SALESACCOUNTBALANCE cSAB ON cSAB.SALESCUSTOMERID    = SD.SALESCUSTOMERID 
AND CUSACTIVE=1 ' 
-- Begin building OrderBy clause

SET @orderClause = CHAR(10) + ' Order By NAME ' + CHAR(10)
 --========================================================================================
 -- Begin building WHERE clause
 -- This Where clause contains subquery to exclude already displayed records
 --OR @orgName != '' OR @orgName != NULL
 --IF(@salesCustName != '' OR @salesCustName != NULL OR @empName != '' OR @empName != NULL ) 
 --BEGIN
	--SET @whereClause = CHAR(10) + ' WHERE CUSACTIVE=1 AND' + CHAR(10) + @searchCriteria
 --END
 --ELSE
 --BEGIN
 SET @whereClause = CHAR(10) + '  WHERE ' + CHAR(10)+ @searchCriteria
 --END

 --========================================================================================
 -- Begin building the main select Query

SET @mainSelectQuery = @selectClause +@fromClause + @whereClause --+ @orderClause 
-- End building the main select Query
 --========================================================================================
 
--========================================================================================
		-- Begin building the row number query
		
		--Set @rowNumberQuery ='  SELECT  as row	
		--						FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--======================================================================================== 
	--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =@InsertClause +' SELECT AccountType, AccountID
							FROM('+CHAR(10)+@mainSelectQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================											
 --========================================================================================
 -- Begin - Execute the Query
 print(@finalQuery)
  EXEC (@finalQuery) -- End - Execute the Query
--========================================================================================

	--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount varchar(2000) 
		--@parameterDef NVARCHAR(500)
		
		--SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT count(*) as totalCount ' + @fromClause + @whereClause
		
		--print @selectCount
		--EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				exec(@selectCount)
		-- End building the Count Query
		--========================================================================================						

ALTER TABLE #TempTableSearch ADD BALANCE money
--ALTER TABLE #TempTableSearch ADD ID int
ALTER TABLE #TempTableSearch ADD ORGANIZATION varchar(500)
ALTER TABLE #TempTableSearch ADD CONTACT varchar(500)
ALTER TABLE #TempTableSearch ADD POSTCODE varchar(50)
 --=======================================================================================
 -- Variables to store one record from temptable using select statement
DECLARE @accountID INT ,@accountType INT ,@accountBalance money , @minAccountID INT 
, @salesInvoiceBalance money,@SQLFILTER varchar(500),@SQLBalanceQuery varchar(500)

 -- Variables used only for calculation
DECLARE @IsCalculateBalance INT = 0
-- Temp Tables Used for Balance and Detail
---============================================================================================

CREATE TABLE #tempAccountDetail (ID int,TENANCYID int,CONTACT varchar(500),BUILDING varchar(500),ADDRESS1 varchar(500),ADDRESS2 varchar(500),ADDRESS3 varchar(500),TOWNCITY varchar(50),POSTCODE varchar(50),COUNTY varchar(50),EMAIL varchar(50)) --------------------------------------------------------------------------------------------------
-- Iterate through the recoreds in Temp Table get accountid and accountType

SET @minAccountID =(SELECT MIN(AccountID) FROM #TempTableSearch) 
WHILE @minAccountID IS NOT NULL
 BEGIN  
 -- Select one record from temp table for calculation

SELECT @accountID = AccountID , @accountType = AccountType FROM #TempTableSearch WHERE AccountID = @minAccountID 
--------------------------------------------------------------------------------------------------
--Getting Sales account Balance againt account Type and AccountID using stored procedure
 SELECT @salesInvoiceBalance= BALANCE FROM F_SALESACCOUNTBALANCE
  WHERE  (
		(@accountType = 1 AND TENANCYID = @accountID)
	OR	(@accountType = 2 AND EMPLOYEEID = @accountID)
	OR	(@accountType = 3 AND SUPPLIERID = @accountID)
	OR	(@accountType = 4 AND SALESCUSTOMERID = @accountID)  
  )

	--------------------------------------------------------------------------------------------------
	 --Update #TempTableSearch Add Balance against Acc Type and Acc ID

	UPDATE #TempTableSearch
	SET BALANCE = @salesInvoiceBalance WHERE AccountType=@accountType
	AND AccountID=@accountID 
	 --------------------------------------------------------------------------------------------------
	 --Getting  Customer Detail againt account Type and AccountID using stored procedure

	INSERT #tempAccountDetail (ID,TENANCYID,CONTACT,BUILDING,ADDRESS1,ADDRESS2,ADDRESS3,TOWNCITY,POSTCODE,COUNTY,EMAIL)
	 EXEC AD_getAccountDetail @accountType,@accountID 
	 DECLARE @ID int,@BUILDING varchar(500),@CONTACT varchar(500),@POSTCODE varchar(50)
	 SELECT @BUILDING=BUILDING,@CONTACT=CONTACT,@POSTCODE=POSTCODE
	 FROM #tempAccountDetail 
	--------------------------------------------------------------------------------------------------
	 --Update #TempTableSearch Add Account Detail against Acc Type and Acc ID
IF @accountType = 1 OR @accountType = 2 
	BEGIN
	SET @BUILDING='NA'
	END

	IF @accountType = 3
	BEGIN
	UPDATE #TempTableSearch
	SET ORGANIZATION=@CONTACT, CONTACT=@BUILDING, POSTCODE=@POSTCODE WHERE AccountType=@accountType AND AccountID=@accountID
	END
ELSE
BEGIN
	UPDATE #TempTableSearch
	SET ORGANIZATION=@BUILDING, CONTACT=@CONTACT, POSTCODE=@POSTCODE WHERE AccountType=@accountType AND AccountID=@accountID
END	
	TRUNCATE TABLE #tempAccountDetail;
--END
 --DROP TABLE #tempAccountDetail
 -- Iterate through the recoreds in Temp Table
--===============================================================================================
--===============================================================================================

SET @minAccountID =(SELECT MIN(AccountID)FROM #TempTableSearch WHERE AccountID > @minAccountID) END

IF(@SABalance != ''   OR @SABalance != NULL) BEGIN
	SELECT ACCOUNTTYPE,ACCOUNTID,BALANCE,ORGANIZATION,CONTACT,POSTCODE FROM #TempTableSearch
	WHERE BALANCE > @SABalance 
 END
 ELSE BEGIN
	SELECT ACCOUNTTYPE,ACCOUNTID,BALANCE,ORGANIZATION,CONTACT,POSTCODE FROM #TempTableSearch 
END 

 --=============================================================================================
 --Building Sales Invoice Detail
 --===============================================================================================
Select CONVERT(VARCHAR(10), SII.SIDATE,103)DATE,SII.SALEID,SII.ItemName,SI.TenancyID,ts.ACCOUNTID,
GROSSCOST BALANCE,

CASE When DATEDIFF(d,SIDate,GetDate()) < 30 then GrossCost 
else 0  END CURRENTBALANCE,
CASE When DATEDIFF(d,SIDate,GetDate()) between 30 and 59 then GrossCost 
else 0 END BALANCE60DAYS,
CASE When DATEDIFF(d,SIDate,GetDate()) between 60 and 89 then GrossCost 
else 0 END BALANCE90DAYS,
CASE When DATEDIFF(d,SIDate,GetDate()) between 90 and 120 then GrossCost 
else 0 END BALANCE120DAYS,
CASE When DATEDIFF(d,SIDate,GetDate()) > 120 then GrossCost 
else 0 END OLDERBALANCE

FROM F_SALESINVOICEITEM SII
LEFT JOIN F_SALESINVOICE SI ON  SI.SALEID=SII.SALEID
 --INNER JOIN #TempTableSearch ts ON ts.ACCOUNTID = SI.EMPLOYEEID
INNER JOIN #TempTableSearch ts ON ts.ACCOUNTID = 
CASE
  WHEN ts.ACCOUNTTYPE='1' THEN SI.TENANCYID
  WHEN ts.ACCOUNTTYPE='2' THEN SI.EMPLOYEEID
  WHEN ts.ACCOUNTTYPE='3' THEN SI.SUPPLIERID
  WHEN ts.ACCOUNTTYPE='4' THEN SI.SALESCUSTOMERID
END
    WHERE
    (    
    (SELECT SUM(GROSSCOST) AS INVOICEAMOUNT FROM F_SALESINVOICEITEM 
WHERE SALEID = SI.SALEID GROUP BY DBO.F_SALESINVOICEITEM.SALEID)-

(SELECT SUM(AMOUNTPAID) AS PAIDAMOUNT FROM F_SALESINVOICE_PAYMENT WHERE SALEID = SI.SALEID GROUP BY
DBO.F_SALESINVOICE_PAYMENT.SALEID) > 0   AND (Convert(date,SII.SIDATE ) > Convert(date, '2013-09-22' ))
  ) 
 
-- Delete temp table that was created during process in tempdb
 IF OBJECT_ID('tempdb..#TempTableSearch') IS NOT NULL
	DROP TABLE #TempTableSearch 
	IF OBJECT_ID('tempdb..#tempAccountDetail') IS NOT NULL
	DROP TABLE #tempAccountDetail 
END
