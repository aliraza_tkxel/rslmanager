
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================

--EXEC	AS_QuickFindUsers		@str1= 'bo' ,@str2 = '2',
-- Author:<Salman Nazir>
-- Create date: <09/27/2012>
-- Last Modified: <10/04/2012>, <Aamir Waheed, April 09,2013>
-- Description:	<Get Names for Add User Quick Find Field>
-- =============================================
CREATE PROCEDURE [dbo].[AS_QuickFindUsers]
	-- Add the parameters for the stored procedure here
	@str1 varchar(1000),
	@str2 varchar(1000)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @query VARCHAR(MAX) = ''
			,@WhereClaues VARCHAR(5000) = 'WHERE '
	
	IF @str1 = @str2
	BEGIN
		SET @WhereClaues = @WhereClaues + '(
			ltrim(rtrim(E__EMPLOYEE.FIRSTNAME)) like ''%' + ltrim(rtrim(@str1)) + '%'' OR
			ltrim(rtrim(E__EMPLOYEE.LASTNAME)) like ''%' + ltrim(rtrim(@str2))+ '%''
			)'
	END
	ELSE
	BEGIN
		SET @WhereClaues = @WhereClaues + '(
			ltrim(rtrim(E__EMPLOYEE.FIRSTNAME + '' '' +E__EMPLOYEE.LASTNAME)) Like ''%' + LTRIM(RTRIM(@str1 + ' ' + @str2)) + '%'' 			
			)'
	END	
	
	SET @WhereClaues = @WhereClaues + CHAR(10) + 'AND (E_JOBDETAILS.ACTIVE = 1) AND 1=1'
	
	
    -- Insert statements for procedure here
	SET @query = '	SELECT E__EMPLOYEE.FIRSTNAME,E__EMPLOYEE.LASTNAME,ISNULL(E_JOBDETAILS.GSRENo,0)AS GSRENo,E__EMPLOYEE.EMPLOYEEID,E_JOBDETAILS.JOBTITLE
			FROM 			
			E__EMPLOYEE INNER JOIN 
			E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID'
			+ CHAR(10) + @WhereClaues + CHAR(10)+
			'ORDER By E__EMPLOYEE.FIRSTNAME, E__EMPLOYEE.LASTNAME ASC'
	--PRINT (@query)
	EXEC (@query)
END
GO
