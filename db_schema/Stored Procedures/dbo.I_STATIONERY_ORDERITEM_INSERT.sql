SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[I_STATIONERY_ORDERITEM_INSERT]
(
	@OrderId int,
	@PageNo varchar(10),
	@ItemNo varchar(10),
	@ItemDesc varchar(250),
	@Quantity smallint,
	@Size varchar(50),
	@Colour varchar(50),
	@Cost money,
	@PriorityId int, 
	@StatusId int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [I_STATIONERY_ORDER_ITEM] ([OrderId], [PageNo], [ItemNo], [ItemDesc], [Quantity], [Size], [Colour], [Cost], [PriorityId], [StatusId]) VALUES

(@OrderId, @PageNo, @ItemNo, @ItemDesc, @Quantity, @Size, @Colour, @Cost, @PriorityId, @StatusId);
	
SELECT OrderItemId, OrderId, PageNo, ItemNo, ItemDesc, Quantity, Size, Colour, Cost, PriorityId, StatusId FROM I_STATIONERY_ORDER_ITEM WHERE (OrderItemId = SCOPE_IDENTITY())


GO
