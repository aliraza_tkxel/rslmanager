SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[FL_GetTradeLookUpAll]
/* ===========================================================================
 --	EXEC FL_GetTradeLookUpAll		
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To Get List of Trade Name/Description and Trade ID for LookUp/DropDown Lists
--  Webpage:		View/Reports/ReportArea.aspx (For Add/Amend Fault Modal Popup)
 '==============================================================================*/

AS
	SELECT [TradeId] AS id,[Description] AS val
	FROM G_TRADE	
	ORDER BY [Description] ASC
GO
