
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC JRA_GetAllTeams 
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Get intranet pages>
-- Web Page: JobRoles.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetAllTeams]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT	E_TEAM.TEAMID AS TeamId,E_TEAM.TEAMNAME as TeamName
    FROM	E_TEAM  
    WHERE	E_TEAM.Active =1
    ORDER BY E_TEAM.TEAMNAME ASC
    
	
END
GO
