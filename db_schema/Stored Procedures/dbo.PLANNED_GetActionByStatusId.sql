SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC PLANNED_GetActionByStatusId @statusId = 0
-- Author:		<Ahmed Mehmood>
-- Create date: <30/10/2013>
-- Description:	<Get Letters against Action Id>
-- Web Page: ReplacementList.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetActionByStatusId](
	-- Add the parameters for the stored procedure here
	@statusId int = -1	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT PLANNED_Action.ActionId,PLANNED_Action.Title
  FROM PLANNED_Action
  WHERE StatusId = @statusId

END
GO
