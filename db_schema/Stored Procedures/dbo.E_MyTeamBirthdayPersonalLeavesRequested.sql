USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[E_MyTeamLeavesRequested]    Script Date: 22/12/2017 4:38:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.[E_MyTeamBirthdayPersonalLeavesRequested]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[E_MyTeamBirthdayPersonalLeavesRequested] AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[E_MyTeamBirthdayPersonalLeavesRequested] 
	-- Add the parameters for the stored procedure here
	@empId INT,
	@leaveType nvarchar(250)
	
AS
BEGIN

	DECLARE @MyStaffEmpIds TABLE(
		EmpId int
	)

	DECLARE @MyStaffLeavesData TABLE(
		StartDate smalldatetime null,
		ReturnDate smalldatetime null,
		Duration float null,
		Unit varchar(255),
		LeaveStatus varchar(255),
		EmpId int null,
		ItemNatureId int null,
		NatureDescription varchar(255),
		FirstName varchar(255),
		LastName varchar(255),
		AbsenceHistoryId int,
		HolType varchar(255)
	)

	DECLARE @staffId INT
	DECLARE @BHSTART SMALLDATETIME			
	DECLARE @BHEND SMALLDATETIME		

	INSERT INTO @MyStaffEmpIds (EmpId)
	(SELECT E.EMPLOYEEID 
	FROM	E__EMPLOYEE E   
			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
			WHERE	 	J.LINEMANAGER =  @empId   AND J.ACTIVE = 1)

	DECLARE myStaffCursor CURSOR  FOR
	SELECT EmpId from @MyStaffEmpIds

	OPEN myStaffCursor

	FETCH NEXT FROM myStaffCursor INTO @staffId
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT TOP 1 @BHSTART=YStart,@BHEND=YEnd
		 FROM F_FISCALYEARS
			 order by YRange desc

		INSERT INTO @MyStaffLeavesData (StartDate, ReturnDate, Duration, Unit, LeaveStatus, EmpId, ItemNatureId, NatureDescription, FirstName, LastName, AbsenceHistoryId, HolType)

		SELECT	A.STARTDATE,a.RETURNDATE,
		Case when j.HolidayIndicator=2 and j.PARTFULLTIME=2 then case when a.DURATION_HRS is null then duration * 8 else a.DURATION_HRS end else a.DURATION end as duration
		,Case when a.DURATION_TYPE is null then Case when j.HolidayIndicator=2 and j.PARTFULLTIME=2 then 'hrs' else 'days' end else a.DURATION_TYPE end as unit 
		,'Pending' AS Status
		,J.EMPLOYEEID,JNAL.ITEMNATUREID, en.DESCRIPTION, E.FIRSTNAME as FirstName, E.LASTNAME as LastName, A.ABSENCEHISTORYID as AbsenceHistoryId, A.HolType
					FROM 	E__EMPLOYEE E  
							LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  
							LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID  AND JNAL.CURRENTITEMSTATUSID = (SELECT ITEMSTATUSID from E_STATUS WHERE DESCRIPTION = 'Pending')
							--AND JNAL.ITEMNATUREID = 1  
							inner JOIN E_NATURE en on JNAL.ITEMNATUREID = en.ITEMNATUREID
							INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = (
											SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE 
											WHERE JOURNALID = JNAL.JOURNALID AND A.STARTDATE BETWEEN @BHSTART AND @BHEND  --AND E_ABSENCE.STARTDATE < GETDATE()
										  ) 
					WHERE 	E.EMPLOYEEID =  @staffId --and a.STARTDATE != NULL
					and JNAL.ITEMNATUREID  IN (SELECT ITEMNATUREID from E_NATURE 
										where DESCRIPTION=@leaveType)
									      --where DESCRIPTION  in ('Personal Day', 'Birthday Leave') )

		FETCH NEXT FROM myStaffCursor INTO @staffId
	END

	CLOSE myStaffCursor
	DEALLOCATE myStaffCursor

	SELECT StartDate, ReturnDate, Duration, Unit, LeaveStatus, EmpId, ItemNatureId, NatureDescription, FirstName, LastName, AbsenceHistoryId, HolType
	FROM @MyStaffLeavesData


END
