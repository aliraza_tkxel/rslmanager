SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AM_SP_GetRentListCount]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	  SELECT COUNT(*)

	FROM        C_TENANCY INNER JOIN 
				AM_Customer_Rent_Parameters ON C_Tenancy.TenancyId = AM_Customer_Rent_Parameters.TenancyId INNER JOIN       
				P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN
				P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID				

	WHERE (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE>GETDATE())
	--AND dbo.C_TENANCY.TENANCYID NOT IN(131954,971493,972228,972958,972958,974099,974099,974689,974689,974767,974767)
END


-- EXEC [AM_SP_GetRentListCount]



GO
