USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[JSS_GetVoidWorkJobsheetImages]    Script Date: 3/10/2017 2:35:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Abdul Rehman
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


IF OBJECT_ID('dbo.[JSS_GetVoidWorkJobsheetImages]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[JSS_GetVoidWorkJobsheetImages] AS SET NOCOUNT ON;') 
GO



ALTER PROCEDURE [dbo].[JSS_GetVoidWorkJobsheetImages] 
	-- Add the parameters for the stored procedure here
	@requireWorkId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	PropertyId,
	ImageName,
	IsAfterWorkImage,
	CASE
		WHEN IsAfterWorkImage = 1 THEN 'After'
		ELSE 'Before'
	END AS Status

FROM V_Images

WHERE RequiredWorksId = @requireWorkId
END



GO

