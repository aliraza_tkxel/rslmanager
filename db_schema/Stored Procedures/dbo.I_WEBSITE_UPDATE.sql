SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_WEBSITE_UPDATE]
(
	@WebSiteDesc varchar(50),
	@Original_WebsiteID int,
	@WebsiteID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_WEBSITE] SET [WebSiteDesc] = @WebSiteDesc WHERE (([WebsiteID] = @Original_WebsiteID));
	
SELECT WebsiteID, WebSiteDesc FROM I_WEBSITE WHERE (WebsiteID = @WebsiteID)
GO
