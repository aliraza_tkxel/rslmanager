USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetApplianceDefectReport]    Script Date: 05/30/2016 17:52:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.DF_GetApplianceDefectReport') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.DF_GetApplianceDefectReport AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO
/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Appliance Defect Report
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetApplianceDefectReport]
		@searchText = '',
		@appointmentStatusId = -1,
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'JSD',
		@sortOrder = N'DESC',
		@getOnlyCount = 0,
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_GetApplianceDefectReport](
		
		@searchText varchar(5000)='',
		@appointmentStatusId INT = -1,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'JSD',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause nvarchar(max),
        @fromClause   nvarchar(max),
		@fromClauseScheme   nvarchar(max),
		@fromClauseBlock   nvarchar(max),
        @GroupClause  nvarchar(max),
        @whereClause  nvarchar(max),	        
        @orderClause  nvarchar(max),	
        @mainSelectQuery nvarchar(max),      
        @rowNumberQuery nvarchar(max),
        @finalQuery nvarchar(max),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria nvarchar(max),
		@unionQuery nvarchar(max),

		@SelectClauseCount nvarchar(max),
		@SelectClauseScheme nvarchar(max),
		@SelectClauseSchemeCount nvarchar(max),
		@SelectClauseBlock nvarchar(max),
		@SelectClauseBlockCount nvarchar(max),
		@whereClauseScheme  nvarchar(max),
		@whereClauseBlock  nvarchar(max),
		@searchCriteriaScheme nvarchar(max),
		@searchCriteriaBlock nvarchar(max),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 
								AND (P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid IS NULL OR P_PROPERTY_APPLIANCE_DEFECTS.detectortypeid = 0) 
								AND (((P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NOT NULL AND P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId <> 0 ) AND GS_PROPERTY_APPLIANCE.ISACTIVE =1) OR (P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL or P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = 0 ))   
								'
		
		IF @appointmentStatusId != -1
		BEGIN
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND P_DEFECTS_CATEGORY.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'')
																		AND P_PROPERTY_APPLIANCE_DEFECTS.DefectDate >= ''07 Jan 2016'''
		DECLARE @LoggedStatus INT
		SELECT @LoggedStatus = STATUSID from PDR_STATUS WHERE TITLE ='Logged'
		IF @appointmentStatusId = @LoggedStatus
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ApplianceDefectAppointmentJournalId IS NULL'
		END
		
		ELSE
			--SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PDR_STATUS.StatusId = ' + CONVERT(VARCHAR, @appointmentStatusId)	
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_PROPERTY_APPLIANCE_DEFECTS.DefectJobSheetStatus = ' + CONVERT(VARCHAR, @appointmentStatusId)																							
		END

		SET @searchCriteriaScheme = @searchCriteria

		SET @searchCriteriaBlock = @searchCriteria
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ((ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' '
						
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + ' AND ((ISNULL(P.SCHEMENAME, '''') LIKE ''%' + @searchText + '%'' '
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + ' AND ((ISNULL(P.BLOCKNAME, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%''
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'' '

			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR CONVERT(NVARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId) LIKE ''%' + @searchText + '%'' )'
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR GS_APPLIANCE_TYPE.APPLIANCETYPE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P_DEFECTS_CATEGORY.Description LIKE ''%' + @searchText + '%'' )'

			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'OR CONVERT(NVARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId) LIKE ''%' + @searchText + '%'' )'
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'OR GS_APPLIANCE_TYPE.APPLIANCETYPE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteriaScheme = @searchCriteriaScheme + CHAR(10) + 'OR P_DEFECTS_CATEGORY.Description LIKE ''%' + @searchText + '%'' )'
			
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'OR CONVERT(NVARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId) LIKE ''%' + @searchText + '%'' )'
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'OR GS_APPLIANCE_TYPE.APPLIANCETYPE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteriaBlock = @searchCriteriaBlock + CHAR(10) + 'OR P_DEFECTS_CATEGORY.Description LIKE ''%' + @searchText + '%'' )'
		END
	
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'
FROM 
					P_PROPERTY_APPLIANCE_DEFECTS
					INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
					LEFT JOIN PA_PARAMETER_VALUE ON PA_PARAMETER_VALUE.ValueID=P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId
					LEFT JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
					LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
					LEFT JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
					LEFT JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
					LEFT JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
					LEFT JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
					LEFT JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
					INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId'
		SET @fromClauseScheme = CHAR(10) +' FROM
					P_PROPERTY_APPLIANCE_DEFECTS
					INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
					LEFT JOIN PA_PARAMETER_VALUE ON PA_PARAMETER_VALUE.ValueID=P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId
					LEFT JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
					LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
					LEFT JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
					LEFT JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
					LEFT JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
					LEFT JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
					LEFT JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
					INNER JOIN P_SCHEME P ON P.SCHEMEID = P_PROPERTY_APPLIANCE_DEFECTS.SchemeId'

		SET @fromClauseBlock = CHAR(10) +' FROM
					P_PROPERTY_APPLIANCE_DEFECTS
					INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
					LEFT JOIN PA_PARAMETER_VALUE ON PA_PARAMETER_VALUE.ValueID=P_PROPERTY_APPLIANCE_DEFECTS.BoilerTypeId
					LEFT JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
					LEFT JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
					LEFT JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
					LEFT JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
					LEFT JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
					LEFT JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
					LEFT JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
					INNER JOIN P_BLOCK P ON P.BLOCKID = P_PROPERTY_APPLIANCE_DEFECTS.BLOCKID'

		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 	
		SET @whereClauseScheme =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaScheme 	
		SET @whereClauseBlock =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteriaBlock
													
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = '
									  P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId AS JSD
									,P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''')	AS Address
									,P__PROPERTY.POSTCODE AS POSTCODE
									,P__PROPERTY.PropertyId	AS PROPERTYID									
									,CASE WHEN P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL THEN PA_PARAMETER_VALUE.ValueDetail ELSE GS_APPLIANCE_TYPE.APPLIANCETYPE END AS ApplianceType
									,P_DEFECTS_CATEGORY.Description AS Category
									,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME AS RecordedBy
									,ISNULL(LEFT(CONVERT(VARCHAR(5),PDR_APPOINTMENTS.APPOINTMENTSTARTTIME,114),5)
									+'' ''+ 
									LEFT(DATENAME(WEEKDAY, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(3),DATEPART(DAY,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) 
									+'' ''+ 
									LEFT(DATENAME(MONTH, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(4),DATEPART(YEAR,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE))
									+'' (''+ 
									 RTRIM(LTRIM(PDR_STATUS.TITLE)) ++'') '',''Logged'') As AppointmentStatus
									,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE As AppointmentStartDate 
									,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME As AppointmentStartTime 
									,ISNULL(PDR_STATUS.TITLE,''Logged'') AS AppointmentStatusTitle
									, ''''	AS Scheme
									, ''''	AS Block
									,''Property'' AS AppointmentType'

				SET @SelectClauseCount = '  SELECT  ' + @SelectClause
				SET @SelectClause = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClause

				SET @SelectClauseScheme = '
									  P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId AS JSD
									,''''	AS Address
									,'''' AS POSTCODE
									,Convert(varchar,P.SCHEMEID) AS PROPERTYID									
									,CASE WHEN P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL THEN PA_PARAMETER_VALUE.ValueDetail ELSE GS_APPLIANCE_TYPE.APPLIANCETYPE END AS ApplianceType
									,P_DEFECTS_CATEGORY.Description AS Category
									,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME AS RecordedBy
									,ISNULL(LEFT(CONVERT(VARCHAR(5),PDR_APPOINTMENTS.APPOINTMENTSTARTTIME,114),5)
									+'' ''+ 
									LEFT(DATENAME(WEEKDAY, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(3),DATEPART(DAY,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) 
									+'' ''+ 
									LEFT(DATENAME(MONTH, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(4),DATEPART(YEAR,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE))
									+'' (''+ 
									 RTRIM(LTRIM(PDR_STATUS.TITLE)) ++'') '',''Logged'') As AppointmentStatus
									,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE As AppointmentStartDate 
									,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME As AppointmentStartTime 
									,ISNULL(PDR_STATUS.TITLE,''Logged'') AS AppointmentStatusTitle
									, ISNULL(P.SCHEMENAME, '''')	AS Scheme
									, ''''	AS Block
									,''Scheme'' AS AppointmentType'

			SET @SelectClauseSchemeCount = '  SELECT  ' + @SelectClauseScheme
			SET @SelectClauseScheme = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseScheme

			SET @SelectClauseBlock = '
									  P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId AS JSD
									,''''	AS Address
									,P.POSTCODE AS POSTCODE
									,CONVERT(VARCHAR,P.BLOCKID)	AS PROPERTYID									
									,CASE WHEN P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId IS NULL THEN PA_PARAMETER_VALUE.ValueDetail ELSE GS_APPLIANCE_TYPE.APPLIANCETYPE END AS ApplianceType
									,P_DEFECTS_CATEGORY.Description AS Category
									,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME AS RecordedBy
									,ISNULL(LEFT(CONVERT(VARCHAR(5),PDR_APPOINTMENTS.APPOINTMENTSTARTTIME,114),5)
									+'' ''+ 
									LEFT(DATENAME(WEEKDAY, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(3),DATEPART(DAY,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) 
									+'' ''+ 
									LEFT(DATENAME(MONTH, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(4),DATEPART(YEAR,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE))
									+'' (''+ 
									 RTRIM(LTRIM(PDR_STATUS.TITLE)) ++'') '',''Logged'') As AppointmentStatus
									,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE As AppointmentStartDate 
									,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME As AppointmentStartTime 
									,ISNULL(PDR_STATUS.TITLE,''Logged'') AS AppointmentStatusTitle
									, ''''	AS Scheme
									, ISNULL(P.BLOCKNAME, '''') + ISNULL('', '' + P.TOWNCITY, '''')	AS Block
									,''Block'' AS AppointmentType'

			SET @SelectClauseBlockCount = '  SELECT  ' + @SelectClauseBlock
			SET @SelectClauseBlock = '  SELECT TOP ('+convert(VARCHAR(10),@limit)+')  ' + @SelectClauseBlock
							
			--============================Group Clause==========================================
			SET @unionQuery = CHAR(10) + ' UNION ALL ' + CHAR(10)
			--SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
			--										, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '								
			--============================Order Clause==========================================		
								
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--=================================	Where Clause ================================
			
			SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @unionQuery
									+  @SelectClauseScheme +@fromClauseScheme + @whereClauseScheme + @unionQuery 
									+  @SelectClauseBlock +@fromClauseBlock + @whereClauseBlock 
									+ @orderClause 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
			IF(@getOnlyCount=0)
			BEGIN
				--print(@finalQuery)
				EXEC (@finalQuery)
			END
			
			--========================================================================================
			-- Begin building Count Query 
			
			Declare @selectCount nvarchar(max), 
			@parameterDef NVARCHAR(500)
			
			SET @parameterDef = '@totalCount int OUTPUT';
			SET @selectCount= 'SELECT @totalCount =  ( select count(*) FROM ('+@SelectClauseCount+@fromClause+@whereClause+@unionQuery+@SelectClauseSchemeCount+@fromClauseScheme+
													@whereClauseScheme+@unionQuery+@SelectClauseBlockCount+@fromClauseBlock+@whereClauseBlock+') as Records )'
			
			--print @selectCount
			EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		END					
			-- End building the Count Query
			--========================================================================================	

END