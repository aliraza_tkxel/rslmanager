SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[I_SUGGESTION_UPDATE]
(
	@UserID int,
	@DateSubmitted smalldatetime,
	@Topic varchar(50),
	@Suggestion varchar(1500),
	@Action varchar(50),
	@Original_SuggestionID int,
	@SuggestionID int
)
AS
	SET NOCOUNT OFF;
UPDATE [I_SUGGESTIONS] SET [UserID] = @UserID, [DateSubmitted] = @DateSubmitted, [Topic] = @Topic, [Suggestion] = @Suggestion, [Action] = @Action WHERE (([SuggestionID] = @Original_SuggestionID));
	
SELECT SuggestionID, UserID, DateSubmitted, Topic, Suggestion, Action FROM I_SUGGESTIONS WHERE (SuggestionID = @SuggestionID)
GO
