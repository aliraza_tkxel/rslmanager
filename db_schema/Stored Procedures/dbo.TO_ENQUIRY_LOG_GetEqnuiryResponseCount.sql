SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.TO_ENQUIRY_LOG_GetEqnuiryResponseCount
/* ===========================================================================
 '   NAME:           TO_ENQUIRY_LOG_GetEqnuiryResponseCount
 '   DATE CREATED:   04 JULY 2008
 '   CREATED BY:     Naveed Iqbal
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        Get the enquiries response count from TO_ENQUIRY_LOG and TO_RESPONSE_LOG against the specified customer
 '   IN:             @CustomerID
 '
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	(
	@CustomerID INT
	)
	
AS

SELECT COUNT(RES.RESPONSELOGID) as ResponseCount
FROM TO_ENQUIRY_LOG ENQ
INNER JOIN TO_RESPONSE_LOG RES ON ENQ.ENQUIRYLOGID=RES.ENQUIRYLOGID
WHERE ENQ.CUSTOMERID=@CustomerID AND RES.READFLAG=0




GO
