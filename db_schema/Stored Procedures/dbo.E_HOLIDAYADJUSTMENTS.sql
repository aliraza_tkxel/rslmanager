SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Rogers	
-- Create date: November 2014
-- Description:	Script to calculate adjustments to annual leave and bank holidays
-- =============================================
CREATE PROCEDURE dbo.E_HOLIDAYADJUSTMENTS 
	-- Add the parameters for the stored procedure here
	@employeeId INT,
	@fteholidayentitlement FLOAT = 22,
	@print BIT = 0,
	@AdjHours FlOAT OUTPUT, 
	@AdjDays FLOAT OUTPUT,
	@BHadj FLOAT OUTPUT,
	@ProRataHours FLOAT OUTPUT, 
	@ProRataDays FLOAT OUTPUT, 
	@ProRataSameHours FLOAT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @startdate AS SMALLDATETIME
	DECLARE @enddate AS SMALLDATETIME

	DECLARE @ftehours FLOAT = 37.0
	DECLARE @ftedayhours  FLOAT = 7.4
	DECLARE @totalMonths FLOAT

	--IF (@print = 1)
	--BEGIN
	--	SELECT jobtitle, FIRSTNAME, LASTNAME, STARTDATE, ALSTARTDATE
	--	FROM dbo.E_JOBDETAILS jd 
	--	LEFT JOIN dbo.E__EMPLOYEE e ON (jd.EMPLOYEEID = e.EMPLOYEEID)
	--	WHERE e.EMPLOYEEID = @employeeId
	--END 
	
	SET @startdate = (SELECT  CAST(
			CAST(DAY(ALSTARTDATE) AS VARCHAR) + '/'
			+ CAST(MONTH(ALSTARTDATE) AS VARCHAR) + '/' 
			+ CAST(YEAR(GETDATE()) AS VARCHAR) 
			AS smalldatetime) 
			AS [sdate]
	FROM    e_jobdetails
	WHERE   EMPLOYEEID = @employeeId)
	IF (@startdate > GETDATE()) SET @startdate = DATEADD(yyyy,-1,@startdate)
	SET @enddate = (SELECT CASE 
					WHEN enddate IS NULL OR enddate > DATEADD(d,-1,DATEADD(yyyy,+1,@startdate))
					THEN DATEADD(d,-1,DATEADD(yyyy,+1,@startdate)) 
					ELSE enddate 
					END 
					FROM dbo.E_JOBDETAILS WHERE EMPLOYEEID = @employeeId)

	--IF (@print = 1)
	--BEGIN
	--	SELECT @startdate AS startdate, @enddate AS enddate 
	--END
	
	DECLARE @bhhours TABLE(
	hoursId int IDENTITY,
	startdate smalldatetime,
	enddate smalldatetime,
	mon float,
	tue float, 
	wed float, 
	thu float, 
	fri float, 
	sat float, 
	sun float,
	total float, 
	[Type] VARCHAR(50),
	bhmon FLOAT DEFAULT 0,
	bhtue FLOAT DEFAULT 0, 
	bhwed FLOAT DEFAULT 0, 
	bhthu FLOAT DEFAULT 0, 
	bhfri FLOAT DEFAULT 0, 
	bhsat FLOAT DEFAULT 0, 
	bhsun FLOAT DEFAULT 0,
	bhtotal FLOAT DEFAULT 0,
	bhworked FLOAT DEFAULT 0,
	bhnotworked FLOAT DEFAULT 0,
	nodaysworked FLOAT DEFAULT 0,
	calc1 FLOAT DEFAULT 0,
	calc2 FLOAT DEFAULT 0,
	entitlementhours FLOAT DEFAULT 0,
	enetitlementdays FLOAT DEFAULT 0, 
	fte FLOAT DEFAULT 0, 
	fteentitlement FLOAT DEFAULT 0,
	dayentitlement FLOAT DEFAULT 0,
	hoursentitlement FLOAT DEFAULT 0,
	nomonths FLOAT DEFAULT 0,
	prorataholhours	FLOAT DEFAULT 0,
	prorataholdays FLOAT DEFAULT 0,
	proratasamehours FLOAT DEFAULT 0
	)

	INSERT INTO @bhhours (startdate, enddate, mon, tue, wed, thu, fri, sat, sun, total, [Type])
	SELECT CASE WHEN StartDate < @startdate THEN @startdate ELSE StartDate END AS startdate, CAST(@enddate AS SMALLDATETIME), Mon, Tue, Wed, Thu, Fri, Sat, Sun, Total, 'c' AS [Type]  
	FROM dbo.E_WorkingHours 
	WHERE EmployeeId = @employeeId 
	UNION ALL
	SELECT CASE WHEN StartDate < @startdate THEN @startdate ELSE StartDate END AS startdate, CASE WHEN EndDate > @enddate THEN @enddate ELSE EndDate END, Mon, Tue, Wed, Thu, Fri, Sat, Sun, Total, 'h' AS [Type]
	FROM dbo.E_WorkingHours_History 
	WHERE EmployeeId = @employeeId 
	AND EndDate > StartDate
	AND ( (EndDate > @startdate) OR (StartDate >= @startdate) )
	ORDER BY startdate DESC

	UPDATE @bhhours SET nodaysworked = (CASE WHEN mon > 0 THEN 1 ELSE 0 END + 
										CASE WHEN tue > 0 THEN 1 ELSE 0 END +
										CASE WHEN wed > 0 THEN 1 ELSE 0 END +
										CASE WHEN thu > 0 THEN 1 ELSE 0 END +
										CASE WHEN fri > 0 THEN 1 ELSE 0 END +
										CASE WHEN sat > 0 THEN 1 ELSE 0 END +
										CASE WHEN sun > 0 THEN 1 ELSE 0 END)

	DECLARE @i INT = 1
	DECLARE @numrows INT = (SELECT COUNT(*) FROM @bhhours)
	DECLARE @bhstart SMALLDATETIME
	DECLARE @bhend SMALLDATETIME
	DECLARE @bhtotal INT
	DECLARE @tmpdate datetime, @years FLOAT, @months FLOAT, @days FLOAT
	DECLARE @nomonths FLOAT

	IF @numrows > 0
		WHILE (@i <= (SELECT MAX(hoursId) FROM @bhhours))
		BEGIN
		   SET @bhstart = (SELECT startdate FROM @bhhours WHERE hoursId = @i)
		   SET @bhend = (SELECT enddate FROM @bhhours WHERE hoursId = @i)
		   SET @bhtotal = (SELECT COUNT(*) FROM dbo.G_BANKHOLIDAYS WHERE bhdate >= @bhstart AND bhdate <= @bhend 
						   AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS))
		  	   
		  SET @tmpdate = @bhstart
		  SET @years = (SELECT DATEDIFF(yy, @tmpdate, @bhend) - CASE WHEN (MONTH(@bhstart) > MONTH(@bhend)) OR (MONTH(@bhstart) = MONTH(@bhend) AND DAY(@bhstart) > DAY(@bhend)) THEN 1 ELSE 0 END)
		  SET @tmpdate = (SELECT DATEADD(yy, @years, @tmpdate))
		  SET @months = (SELECT DATEDIFF(m, @tmpdate, @bhend) - CASE WHEN DAY(@bhstart) > DAY(@bhend) THEN 1 ELSE 0 END)
		  SET @tmpdate = (SELECT DATEADD(m, @months, @tmpdate))
		  SET @days = (SELECT DATEDIFF(d, @tmpdate, @bhend))
		  SET @days = (SELECT CASE WHEN @days < 6 THEN 0 WHEN @days > 5 AND @days < 12 THEN 0.25 WHEN @days > 11 AND @days < 19 THEN 0.50
					   WHEN @days > 18 AND @days <25 THEN 0.75 WHEN @days > 25 THEN 1 ELSE 0 END)
		  
		  
		  SET @nomonths = (@months + @days)
		   
		   UPDATE @bhhours SET bhtotal = @bhtotal WHERE hoursId = @i
		   
		   UPDATE   @bhhours
		   SET      bhmon =  ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							   AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Monday') AS s),
     				bhtue =  ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							  AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Tuesday') AS s),
					bhwed = ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							  AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Wednesday') AS s),
					bhthu = ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							  AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Thursday') AS s),
					bhfri = ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							  AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Friday') AS s),
					bhsat = ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							  AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Saturday') AS s),
					bhsun = ( SELECT amount FROM (SELECT COUNT(bha) AS amount FROM dbo.G_BANKHOLIDAYS
							  WHERE bhdate >= @bhstart AND bhdate <= @bhend
							  AND bhdate NOT IN (SELECT closedDate FROM dbo.E_CLOSEDDAYS)
							  GROUP BY DATENAME(dw, bhdate)
							  HAVING DATENAME(dw, bhdate) = 'Sunday') AS s)
					WHERE hoursId = @i
	             
	                          
					UPDATE @bhhours SET bhmon = 0 WHERE bhmon IS NULL AND hoursId = @i
					UPDATE @bhhours SET bhtue = 0 WHERE bhtue IS NULL AND hoursId = @i
					UPDATE @bhhours SET bhwed = 0 WHERE bhwed IS NULL AND hoursId = @i
					UPDATE @bhhours SET bhthu = 0 WHERE bhthu IS NULL AND hoursId = @i
					UPDATE @bhhours SET bhfri = 0 WHERE bhfri IS NULL AND hoursId = @i
					UPDATE @bhhours SET bhsat = 0 WHERE bhsat IS NULL AND hoursId = @i
					UPDATE @bhhours SET bhsun = 0 WHERE bhsun IS NULL AND hoursId = @i     
	                
	                    
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND mon > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhmon WHERE hoursId = @i
					
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND tue > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhtue WHERE hoursId = @i
		   
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND wed > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhwed WHERE hoursId = @i
					
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND thu > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhthu WHERE hoursId = @i
					
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND fri > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhfri WHERE hoursId = @i
					
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND sat > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhsat WHERE hoursId = @i
					
					IF EXISTS (SELECT hoursId FROM @bhhours WHERE hoursId = @i AND sun > 0)
					UPDATE @bhhours SET bhworked = bhworked + bhsun WHERE hoursId = @i
					
					UPDATE @bhhours	SET bhnotworked = bhtotal - bhworked WHERE hoursId = @i
					
					UPDATE @bhhours SET nomonths = @nomonths WHERE hoursId = @i
		   
		   SET @i = @i + 1
		END

	UPDATE @bhhours SET calc1 = (total/nodaysworked)*bhtotal/5*nodaysworked,
						calc2 = bhworked*(total/nodaysworked)
						
	UPDATE @bhhours SET	entitlementhours = ROUND(calc1-calc2,2),
						enetitlementdays = ROUND((calc1-calc2)/(total/nodaysworked),2),
						fte = ROUND(total/@ftehours,2),
						fteentitlement = @fteholidayentitlement
						
	UPDATE @bhhours SET dayentitlement = fteentitlement*fte,
						hoursentitlement = ROUND(@ftedayhours*(fteentitlement*fte),2)

	UPDATE @bhhours SET prorataholhours = ROUND(hoursentitlement/12*nomonths,2),
						prorataholdays = ROUND(dayentitlement/12*nomonths,2)
						
	UPDATE @bhhours SET proratasamehours = ROUND(prorataholhours/(total/nodaysworked),2)
	
	--IF (@print = 1)					
	--SELECT * FROM @bhhours

	DECLARE @entHr AS INT = (SELECT SUM(entitlementhours) FROM @bhhours);
	SET @AdjHours = (SELECT SUM(entitlementhours) FROM @bhhours)
	SET @AdjDays  = (SELECT SUM(enetitlementdays) FROM @bhhours) 
	IF ((SELECT HolidayIndicator FROM dbo.E_JOBDETAILS WHERE EMPLOYEEID = @employeeId) = 1) 
	SET @BHadj = @AdjDays
	ELSE 
	SET @BHadj = @AdjHours
	SET @ProRataHours = ROUND((SELECT SUM(prorataholhours) FROM @bhhours) *2,0)/2 
	SET @ProRataDays = ROUND((SELECT SUM(prorataholdays) FROM @bhhours)  *2,0)/2
	SET @ProRataSameHours = ROUND((SELECT SUM(proratasameHours) FROM @bhhours) *2,0)/2 
	SET @totalMonths = (SELECT SUM(nomonths) FROM @bhhours) 

	--IF (@print = 1)
	--SELECT @BHadj AS [Adjustment], @AdjHours AS [AdjHours], @AdjDays AS [AdjDays], @ProRataHours AS [ProRataHours], 
	--	   @ProRataDays AS [ProRataDays], @ProRataSameHours AS [ProRataSameHours], @totalMonths AS Months

	--SELECT * FROM dbo.G_BANKHOLIDAYS WHERE BHDATE >= @startdate AND bhdate <= @enddate
END
GO
