
USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveAppointment]    Script Date: 12/11/2012 18:03:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC AS_PropertyLetters
		--@journalHistoryId=1
		
-- Author:		<Noor Muhammad>
-- Create date: <10/3/2012>
-- Description:	<This stored procedure returns the all the letters against the property history>
-- Parameters:	
		--@journalHistoryId int = 0
-- Webpage :PropertyRecord.aspx		
-- [AS_PropertyLetters] 70809	
-- Modified By: Saud Ahmed	
-- Modified Date: 14/04/2017
-- =============================================

IF OBJECT_ID('dbo.AS_PropertyLetters') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.AS_PropertyLetters AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO




ALTER PROCEDURE [dbo].[AS_PropertyLetters]
( 
		@journalHistoryId bigint = 0
)
AS
BEGIN
SELECT 
AS_JOURNALHISTORY.JOURNALHISTORYID as JournalHistoryId
,AS_SAVEDLETTERS.LetterId as StandardLetterId
,AS_SAVEDLETTERS.SavedLetterId as LetterHistoryId
,AS_StandardLetters.Title as LetterTitle
, isnull(AS_EmailStatus.StatusImagePath, '~/Images/EmailNotSent.png') as EmailImagePath
, AS_APPOINTMENTS.APPOINTMENTID as AppointmentId

FROM AS_JOURNALHISTORY 

INNER JOIN AS_SAVEDLETTERS ON  AS_SAVEDLETTERS.JOURNALHISTORYID = AS_JOURNALHISTORY.JOURNALHISTORYID
INNER JOIN AS_StandardLetters on AS_StandardLetters.StandardLetterId = AS_SAVEDLETTERS.LETTERID
Inner JOIN AS_APPOINTMENTS on AS_APPOINTMENTS.JournalId = AS_JOURNALHISTORY.JournalId
LEFT JOIN AS_EmailStatus on AS_APPOINTMENTS.CustomerEmailStatus = AS_EmailStatus.EmailStatusId
Where AS_JOURNALHISTORY.JournalHistoryId =@journalHistoryId

END
