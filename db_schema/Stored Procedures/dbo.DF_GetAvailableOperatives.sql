/* =================================================================================      
    Page Description: DefectAvailableAppointments.aspx  
    
    Author: Aamir Waheed  
    Creation Date:  26/08/2015  
  
    Change History:  
  
    Version      Date             By                      Description  
    =======     ============    ========           ===========================  
    v1.0         26/08/2015     Aamir Waheed       Created:DF_GetAvailableOperatives, to get available operative  
      
Execution Command:  
----------------------------------------------------  
  
DECLARE @return_value int  
  
EXEC @return_value = [dbo].[DF_GetAvailableOperatives]  
      @tradeIds = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18'  
  
SELECT 'Return Value' = @return_value  
  
----------------------------------------------------  
*/  
  
IF OBJECT_ID('dbo.DF_GetAvailableOperatives') IS NULL 
	EXEC('CREATE PROCEDURE dbo.DF_GetAvailableOperatives AS SET NOCOUNT ON;') 
GO

ALTER PROCEDURE [dbo].[DF_GetAvailableOperatives]  
 -- Add the parameters for the stored procedure here    
 @tradeIds  AS VARCHAR(MAX)   
AS  
BEGIN  
  
SET NOCOUNT ON;  
  
--=================================================================================================================    
------------------------------------------------------ Step 0------------------------------------------------------  
-- Declare table variable for available operatives and other local variables  
  
DECLARE @AvailableOperatives AS TABLE(EmployeeId INT  
, FirstName NVARCHAR(100)  
, LastName NVARCHAR(200)  
, FullName NVARCHAR(400)  
, IsGasSafe BIT  
, IsOftec BIT  
, PatchId INT  
, PatchName NVARCHAR(20)  
, Distance NVARCHAR(10)  
, TradeID INT
, PostCode NVARCHAR(30))  
  
DECLARE @Today DATE = CONVERT(DATE, GETDATE())  
  
--=================================================================================================================    
------------------------------------------------------ Step 1------------------------------------------------------    
--This query fetches the employees which matches with the following criteria    
--Trade of employee is same as trade(s) of defect(s) (or provided trades)  
-- Optional: User type is 'Fuel Servicing'  
  
INSERT INTO @AvailableOperatives (  
    EmployeeId  
    ,FirstName  
    ,LastName  
    ,FullName  
    ,IsGasSafe  
    ,IsOftec  
    ,PatchId  
    ,PatchName  
    ,Distance  
    ,TradeID
	,PostCode  
   )  
 SELECT  
  E.EMPLOYEEID  
  ,E.FIRSTNAME  
  ,E.LASTNAME  
  ,E.FIRSTNAME + ' ' + E.LASTNAME  
  ,ISNULL(JD.IsGasSafe, 0)  
  ,ISNULL(JD.IsOftec, 0)  
  ,ISNULL(JD.PATCH, -1)  
  ,P.LOCATION  
  ,0  
  ,ET.TradeId
  ,EC.postcode AS PostCode  
 FROM  
  E__EMPLOYEE E  
   INNER JOIN E_JOBDETAILS JD ON E.EMPLOYEEID = JD.EMPLOYEEID  
    AND  
    (JD.ACTIVE = 1)  
   LEFT JOIN E_PATCH P ON JD.PATCH = P.PATCHID  
   INNER JOIN E_TRADE ET ON E.EMPLOYEEID = ET.EmpId
   INNER JOIN E_CONTACT EC ON E.EMPLOYEEID=EC.EmployeeId
   -- To filter operatives for a specifiec inspection type (uncomment two joins below)  
   --INNER JOIN AS_USER_INSPECTIONTYPE UIT ON E.EMPLOYEEID = UIT.EmployeeId    
   --INNER JOIN P_INSPECTIONTYPE IT ON UIT.InspectionTypeID = IT.InspectionTypeID -- AND (IT.Description  = 'Fuel Servicing' )  
  
   -- Filter to skip out operative(s) on sick leave (and not returned to work).  
   OUTER APPLY  
   (  
    SELECT  
     EMPLOYEEID  
    FROM  
     E_JOURNAL EJ  
      CROSS APPLY  
      (  
       SELECT  
        MAX(ABSENCEHISTORYID) MaxABSENCEHISTORYID  
       FROM  
        E_ABSENCE iA  
       WHERE  
        iA.JOURNALID = EJ.JOURNALID  
      ) MaxAbsence  
      INNER JOIN E_ABSENCE A ON EJ.JOURNALID = A.JOURNALID  
       AND  
       A.ABSENCEHISTORYID = MaxAbsence.MaxABSENCEHISTORYID  
    WHERE  
     ITEMNATUREID = 1  
     AND A.RETURNDATE IS NULL  
     AND A.ITEMSTATUSID = 1  
     AND EJ.EMPLOYEEID = E.EMPLOYEEID  
   ) SickEmp  
  
 WHERE  
  1 = 1  
  AND ET.TradeId IN  
  (  
   SELECT  
    COLUMN1  
   FROM  
    dbo.SPLIT_STRING(@tradeIds, ',')  
  )  
  -- Filter to skip out operative(s) on sick leave (and not returned to work).  
  AND SickEmp.EMPLOYEEID IS NULL  
  
---------------------------------------------------------    
  
SELECT  
 EmployeeId  
 ,FirstName  
 ,LastName  
 ,FullName  
 ,IsGasSafe  
 ,IsOftec  
 ,PatchId  
 ,PatchName  
 ,Distance  
 ,TradeID
 ,PostCode  
FROM  
 @AvailableOperatives  
--=================================================================================================================    
------------------------------------------------------ Step 2------------------------------------------------------          
--This query selects the leaves of employees i.e the employess which we get in step1    
--M : morning - 08:00 AM - 12:00 PM    
--A : mean after noon - 01:00 PM - 05:00 PM    
--F : Single full day    
--F-F : Multiple full days    
--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM    
--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days    
  
  
SELECT  
 A.STARTDATE  AS StartDate
,CASE
	WHEN
	(EJ.ITEMNATUREID = 1 AND A.ITEMSTATUSID = 2 AND A.HOLTYPE NOT IN ('M','F-M','A-M')
	AND A.RETURNDATE IS NOT NULL AND DATEDIFF(day,A.STARTDATE,A.RETURNDATE)>0) THEN 
	DATEADD(DD,-1,A.RETURNDATE)	
	ELSE
	A.RETURNDATE 
	END 	
	AS EndDate 
 ,EJ.employeeid AS OperativeId  
 ,A.HolType  
 ,A.duration  
 ,CASE  
  WHEN (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A  
   .STARTDATE)))  
   THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')  
  WHEN HolType = ''   or HOLTYPE is null 
   THEN '08:00 AM'  
  WHEN HolType = 'M'  
   THEN '08:00 AM'  
  WHEN HolType = 'A'  
   THEN '01:00 PM'  
  WHEN HolType = 'F'  
   THEN '00:00 AM'  
  WHEN HolType = 'F-F'  
   THEN '00:00 AM'  
  WHEN HolType = 'F-M'  
   THEN '00:00 AM'  
  WHEN HolType = 'A-F'  
   THEN '01:00 PM' 
  WHEN HolType = 'A-M'  
   THEN '01:00 PM'  
 END    AS StartTime
 ,CASE
	WHEN
	(EJ.ITEMNATUREID = 1 AND A.ITEMSTATUSID = 2 AND A.HOLTYPE NOT IN ('M','F-M','A-M') AND A.RETURNDATE IS NOT NULL 
	AND DATEDIFF(day,A.STARTDATE,A.RETURNDATE)>0) THEN

	CASE WHEN (DATEADD(DD,-1,A.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,A.RETURNDATE))))
	THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, DATEADD(DD,-1,A.RETURNDATE), 0), 8)), 'AM', ' AM'), 'PM', ' PM')
	WHEN HolType = '' or HOLTYPE is null 
	THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
	WHEN HolType = 'M'
	THEN '12:00 PM'
	WHEN HolType = 'A'
	THEN '05:00 PM'
	WHEN HolType = 'F'
	THEN '11:59 PM'
	WHEN HolType = 'F-F'
	THEN '11:59 PM'
	WHEN HolType = 'F-M'
	THEN '12:00 PM'
	WHEN HolType = 'A-F'
	THEN '11:59 PM'
	END 
	ELSE
	CASE
	WHEN
	(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
	.RETURNDATE)))
	THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
	WHEN HolType = '' or HOLTYPE is null 
	THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
	WHEN HolType = 'M'
	THEN '12:00 PM'
	WHEN HolType = 'A'
	THEN '05:00 PM'
	WHEN HolType = 'F'
	THEN '11:59 PM'
	WHEN HolType = 'F-F'
	THEN '11:59 PM'
	WHEN HolType = 'F-M'
	THEN '12:00 PM'
	WHEN HolType = 'A-F'
	THEN '11:59 PM'
	WHEN HolType = 'A-M'  
	THEN '12:00 PM'	
	END	
	END			AS EndTime   
 ,CASE  
  WHEN  
   (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.STARTDATE  
   )))  
   THEN DATEDIFF(mi, '1970-01-01', A.STARTDATE)  
  WHEN HolType = ''   or HOLTYPE is null 
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103) + ' ' + '08:00 AM', 103))  
  WHEN HolType = 'M'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103) + ' ' + '08:00 AM', 103))  
  WHEN HolType = 'A'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103) + ' ' + '01:00 PM', 103))  
  WHEN HolType = 'F'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103), 103))  
  WHEN HolType = 'F-F'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103), 103))  
  WHEN HolType = 'F-M'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103), 103))  
  WHEN HolType = 'A-F'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103) + ' ' + '01:00 PM', 103))
  WHEN HolType = 'A-M'  
   THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), A.StartDate, 103) + ' ' + '01:00 PM', 103))   
 END    AS StartTimeInMin  
 ,CASE
	WHEN
	(EJ.ITEMNATUREID = 1 AND A.ITEMSTATUSID = 2 AND A.HOLTYPE NOT IN ('M','F-M','A-M') AND A.RETURNDATE IS NOT NULL 
	AND DATEDIFF(day,A.STARTDATE,A.RETURNDATE)>0) THEN
	CASE
	WHEN
	(DATEADD(DD,-1,A.RETURNDATE) <> DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(DD,-1,A.RETURNDATE))))
	THEN DATEDIFF(mi, '1970-01-01', DATEADD(DD,-1,A.RETURNDATE))
	WHEN HolType = '' or HOLTYPE is null 
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00', 103))
	WHEN HolType = 'M'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
	WHEN HolType = 'A'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '05:00 PM', 103))
	WHEN HolType = 'F'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
	WHEN HolType = 'F-F'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '11:59 PM', 103))
	WHEN HolType = 'F-M'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '12:00 PM', 103))
	WHEN HolType = 'A-F'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD,-1,A.RETURNDATE), 103) + ' ' + '11:59 PM', 103))	
	END 	
	ELSE
	CASE
	WHEN
	(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE)))
	THEN DATEDIFF(mi, '1970-01-01', A.RETURNDATE)
	WHEN HolType = '' or HOLTYPE is null 
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00', 103))
	WHEN HolType = 'M'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
	WHEN HolType = 'A'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
	WHEN HolType = 'F'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
	WHEN HolType = 'F-F'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
	WHEN HolType = 'F-M'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
	WHEN HolType = 'A-F'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
	WHEN HolType = 'A-M'
	THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))	
	END	
	END	AS EndTimeInMin  
FROM  
 E_JOURNAL EJ  
  INNER JOIN @AvailableOperatives AO ON EJ.employeeid = AO.employeeid  
  CROSS APPLY  
  (  
   SELECT  
    MAX(ABSENCEHISTORYID) MaxABSENCEHISTORYID  
   FROM  
    E_ABSENCE iA  
   WHERE  
    iA.JOURNALID = EJ.JOURNALID  
  ) MaxAbsence  
  INNER JOIN E_ABSENCE A ON EJ.JOURNALID = A.JOURNALID  
   AND  
   A.ABSENCEHISTORYID = MaxAbsence.MaxABSENCEHISTORYID
   INNER JOIN E_JOBDETAILS  ON EJ.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
  LEFT JOIN E_HOLIDAYRULE hl ON E_JOBDETAILS.HOLIDAYRULE = hl.EID  
WHERE  
 (  
 -- To filter for planned i.e annual leaves etc. where approval is needed  
 (A.ITEMSTATUSID = 5  AND EJ.ITEMNATUREID >= 2 AND EJ.ITEMNATUREID<>47 )
 -- To filter for sickness leaves. where the operative is now returned to work.  
 OR (A.ITEMSTATUSID = 2 AND ITEMNATUREID = 1 AND A.RETURNDATE IS NOT NULL) 
  OR (A.ItemStatusId =3 AND EJ.ITEMNATUREID<>47 AND E_JOBDETAILS.ISBRS=1)
 )  
 AND A.RETURNDATE >= @Today   
-- Check for bank holidays   
UNION ALL SELECT DISTINCT  
 CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)                          AS StartDate  
 ,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)                          AS EndDate  
 ,AO.EmployeeId                                  AS OperativeId  
 ,'F'                                    AS HolType  
 ,1                                     AS duration  
 ,'00:00 AM'                                   AS StartTime  
 ,'11:59 PM'                                   AS EndTime  
 ,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))      AS StartTimeInMin  
 ,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103)) AS EndTimeInMin  
FROM  
 G_BANKHOLIDAYS bh  
  CROSS JOIN @AvailableOperatives AO  
WHERE  
 bh.BHDATE >= @Today  
 AND bh.BHA = 1  
--=================================================================================================================    
------------------------------------------------------ Step 3------------------------------------------------------          
--This query selects the appointments of employees i.e the employess which we get in step1    
SELECT  
 A.AppointmentDate  
 ,A.OperativeID  
 ,A.Time                    AS StartTime  
 ,A.EndTime                   AS EndTime  
 ,DATEDIFF(SECOND, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10),  
 A.AppointmentDate, 103) + ' ' + Time, 103))           AS StartTimeInSec  
 ,DATEDIFF(SECOND, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10),  
 COALESCE(A.AppointmentEndDate, A.AppointmentDate), 103) + ' ' + A.EndTime, 103)) AS EndTimeInSec  
 ,CASE  
  WHEN FL.PROPERTYID IS NOT NULL  
   THEN ISNULL(P.POSTCODE, '')  
  WHEN FL.BLOCKID IS NOT NULL  
   THEN ISNULL(P_BLOCK.POSTCODE, '')  
  WHEN FL.SCHEMEID IS NOT NULL  
   THEN ISNULL(PDR_DEVELOPMENT.PostCode, '')  
  ELSE ''  
 END                     AS PostCode  
  
 ,CASE  
  WHEN FL.PROPERTYID IS NOT NULL  
   THEN ISNULL(P.HouseNumber + ' ', '')  
    + ISNULL(P.ADDRESS1, '')  
    + ISNULL(', ' + P.ADDRESS2, '')  
    + ISNULL(', ' + P.ADDRESS3, '')  
  WHEN FL.BLOCKID IS NOT NULL  
   THEN ISNULL(P_BLOCK.ADDRESS1, '')  
  WHEN FL.SCHEMEID IS NOT NULL  
   THEN ISNULL(P_SCHEME.SCHEMENAME + ', ', '')  
  ELSE ''  
 END                     AS Address  
 ,CASE  
  WHEN FL.PROPERTYID IS NOT NULL  
   THEN ISNULL(P.TownCity, '')  
  WHEN FL.BLOCKID IS NOT NULL  
   THEN ISNULL(P_BLOCK.TOWNCITY, '')  
  WHEN FL.SCHEMEID IS NOT NULL  
   THEN ISNULL(PDR_DEVELOPMENT.TOWN, '')  
  ELSE ''  
 END                     AS TownCity  
 ,CASE  
  WHEN FL.PROPERTYID IS NOT NULL  
   THEN ISNULL(P.COUNTY, '')  
  WHEN FL.BLOCKID IS NOT NULL  
   THEN ISNULL(P_BLOCK.COUNTY, '')  
  WHEN FL.SCHEMEID IS NOT NULL  
   THEN ISNULL(PDR_DEVELOPMENT.COUNTY, '')  
  ELSE ''  
 END                     AS County  
 ,'Fault Appointment'                AS AppointmentType
 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LastActionDate)) AS CreationDate
FROM  
 FL_CO_Appointment A  
  INNER JOIN FL_FAULT_APPOINTMENT FA ON A.AppointmentID = FA.AppointmentId  
  CROSS APPLY  
  (  
   SELECT  
    COUNT(iFA.FaultLogId) AS [NotPausedCount]  
   FROM  
    FL_FAULT_APPOINTMENT iFA  
     INNER JOIN FL_FAULT_LOG iFL ON iFA.FaultLogId = iFL.FaultLogId  
     INNER JOIN FL_FAULT_STATUS FS ON iFL.StatusID = FS.FaultStatusID  
      AND  
      FS.Description NOT IN ('Cancelled', 'Complete')  
     OUTER APPLY  
     (  
      SELECT  
       MAX(FaultLogHistoryID) FaultLogHistoryIDMax  
      FROM  
       FL_FAULT_LOG_HISTORY  
      WHERE  
       FaultLogID = iFL.FaultLogID  
     ) FLHMAX  
     LEFT JOIN FL_FAULT_LOG_HISTORY FLH ON iFL.FaultLogID = FLH.FaultLogId  
      AND  
      FLH.FaultLogHistoryID = FLHMAX.FaultLogHistoryIDMax  
     LEFT JOIN FL_FAULT_PAUSED FP ON FLH.FaultLogHistoryID = FP.FaultLogHistoryID  
      AND  
      FP.Reason = 'Parts Required'  
   WHERE  
    FP.PauseID IS NULL  
    AND iFA.AppointmentId = A.AppointmentID  
  ) FilteredAppointments  
  INNER JOIN FL_FAULT_LOG FL ON FA.faultlogid = FL.FaultLogID  
  LEFT JOIN P__PROPERTY P ON FL.PROPERTYID = P.PROPERTYID  
  LEFT JOIN P_SCHEME ON FL.SCHEMEID = P_SCHEME.SCHEMEID  
  LEFT JOIN P_BLOCK ON FL.BlockId = P_BLOCK.BLOCKID  
  LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID  
  INNER JOIN @AvailableOperatives AO ON A.OperativeID = AO.employeeid  
WHERE  
 A.AppointmentDate >= @Today  
 AND FilteredAppointments.NotPausedCount > 0  
  
-----------------------------------------------------------------------------------------------------------------------      
--Appliance Appointments       
UNION ALL SELECT  
DISTINCT  
 AS_APPOINTMENTS.AppointmentDate                                 AS AppointmentDate  
 --,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate      
 ,AS_APPOINTMENTS.ASSIGNEDTO                                  AS OperativeId  
 ,APPOINTMENTSTARTTIME                                   AS StartTime  
 ,APPOINTMENTENDTIME                                    AS EndTime  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))      AS StartTimeInSec  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))      AS EndTimeInSec  
 ,p__property.postcode                                   AS PostCode  
 ,ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '') AS Address  
 ,P__PROPERTY.TownCity                                   AS TownCity  
 ,P__PROPERTY.County                                    AS County  
 ,'Gas Appointment'                                    AS AppointmentType  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
FROM  
 AS_APPOINTMENTS  
  INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID  
  INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId  
  INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID  
  INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID  
  LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID  
   AND  
   (DBO.C_TENANCY.ENDDATE IS NULL  
    OR DBO.C_TENANCY.ENDDATE > GETDATE())  
  INNER JOIN @AvailableOperatives AO ON AS_APPOINTMENTS.ASSIGNEDTO = AO.employeeid  
WHERE  
 AS_JOURNAL.IsCurrent = 1  
 AND AS_APPOINTMENTS.AppointmentDate >= @Today  
 AND (AS_Status.Title <> 'Cancelled'  
 AND APPOINTMENTSTATUS <> 'Complete')  
 UNION ALL
 	SELECT
		AS_APPOINTMENTS.AppointmentDate                                 AS AppointmentDate  
 --,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate      
 ,AS_APPOINTMENTS.ASSIGNEDTO                                  AS OperativeId  
 ,APPOINTMENTSTARTTIME                                   AS StartTime  
 ,APPOINTMENTENDTIME                                    AS EndTime  
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,'' 																																			AS PostCode
		,ISNULL(P_SCHEME.SCHEMENAME, '') AS Address
		,''																																		AS TownCity
		,''																																			AS County
		,'Gas Appointment'																																				AS AppointmentType
		 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM
		AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P_SCHEME ON AS_JOURNAL.SchemeId = P_SCHEME.SCHEMEID
		INNER JOIN @AvailableOperatives AO ON AS_APPOINTMENTS.ASSIGNEDTO = AO.employeeid  
	WHERE
		AS_JOURNAL.IsCurrent = 1
		AND AS_APPOINTMENTS.AppointmentDate >= @Today  
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
	UNION ALL
	SELECT
		AS_APPOINTMENTS.AppointmentDate                                 AS AppointmentDate  
 --,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate      
 ,AS_APPOINTMENTS.ASSIGNEDTO                                  AS OperativeId  
 ,APPOINTMENTSTARTTIME                                   AS StartTime  
 ,APPOINTMENTENDTIME                                    AS EndTime  
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,P_BLOCK.POSTCODE																																			AS PostCode
		,ISNULL(P_BLOCK.BLOCKNAME, '') + ' ' + ISNULL(P_BLOCK.ADDRESS1, '') + ' ' + ISNULL(P_BLOCK.ADDRESS2, '') + ' ' + ISNULL(P_BLOCK.ADDRESS3, '')	AS Address
		,P_BLOCK.TownCity																																			AS TownCity
		,P_BLOCK.County																																				AS County
		,'Gas Appointment'																																				AS AppointmentType
		 , DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
	FROM
		AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P_BLOCK ON AS_JOURNAL.BLOCKID = P_BLOCK.BLOCKID
		INNER JOIN @AvailableOperatives AO ON AS_APPOINTMENTS.ASSIGNEDTO = AO.employeeid  
	WHERE
		AS_JOURNAL.IsCurrent = 1
		AND AS_APPOINTMENTS.AppointmentDate >= @Today  
		AND (AS_Status.Title <> 'Cancelled'
			AND APPOINTMENTSTATUS <> 'Complete')
 -----------------------------------------------------------------------------------------------------------------------
		--Trainings
	UNION ALL 
	SELECT 
		EET.STARTDATE as APPOINTMENTDATE
		,EET.EMPLOYEEID  as OperativeId
		,'00:00 AM' as StartTime
		,'11:59 PM' as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), EET.STARTDATE,103) + ' ' + '00:00 AM' ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), EET.ENDDATE,103) + ' ' + '11:59 PM',103)) as EndTimeInSec
		,ISNULL(EET.POSTCODE, ' ')  AS PostCode  
		,ISNULL(EET.LOCATION, '')  AS Address
		,ISNULL(EET.VENUE, '') AS TownCity																																			
		,'' AS County
		,'Training' as AppointmentType
		,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME,EET.CREATEDDATE)) AS CreationDate
	FROM 
	    E_EmployeeTrainings EET	
	WHERE
		Convert(date,EET.ENDDATE,103) >= @Today
		AND EET.Active=1 AND EET.Status in (select StatusId from E_EmployeeTrainingStatus where Title = 'Requested' OR Title = 'Manager Supported' OR Title = 'Exec Supported' OR Title = 'Exec Approved' OR Title = 'HR Approved' )	
		AND EET.EMPLOYEEID in (SELECT employeeid FROM  @AvailableOperatives) 
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------      
--Planned Appointments      
UNION ALL SELECT  
DISTINCT  
 APPOINTMENTDATE                                     AS AppointmentDate  
 --,APPOINTMENTENDDATE as AppointmentEndDate      
 ,ASSIGNEDTO                                      AS OperativeId  
 ,APPOINTMENTSTARTTIME                                   AS StartTime  
 ,APPOINTMENTENDTIME                                    AS EndTime  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103))          AS StartTimeInSec  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))         AS EndTimeInSec  

 ,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.POSTCODE, ' ')
		when PLANNED_JOURNAL.BlockId > 0 then ISNULL(P_BLOCK.POSTCODE,' ')     
		when PLANNED_JOURNAL.SchemeId > 0 then ' '
  end   AS PostCode
    
 ,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '  
          + ISNULL(P__PROPERTY.ADDRESS1, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS2, '') + ', '  
          + ISNULL(P__PROPERTY.ADDRESS3, '')  
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.ADDRESS1, '')  
          + ISNULL(P_BLOCK.ADDRESS2, '') + ', '  
          + ISNULL(P_BLOCK.ADDRESS3, '')  
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
   END AS Address

,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.TownCity, '')
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.TownCity, '')  
          
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ''
          
   END	AS TownCity
   																																			
,CASE  WHEN PLANNED_JOURNAL.PROPERTYID <> '' THEN  
          ISNULL(P__PROPERTY.County, ' ')
        WHEN PLANNED_JOURNAL.BLOCKID > 0 THEN ISNULL(P_BLOCK.County, ' ')  
          
        WHEN PLANNED_JOURNAL.SCHEMEID > 0 THEN ' '
          
      END AS Count  
 ,'Planned Appointment'    AS AppointmentType  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
FROM  
 PLANNED_APPOINTMENTS  
  INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId  
  INNER JOIN PLANNED_STATUS ON PLANNED_STATUS.STATUSID = PLANNED_JOURNAL.STATUSID  
  left JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
  left join P_SCHEME on  PLANNED_JOURNAL.SchemeId = P_SCHEME.SCHEMEID
  left join P_BLOCK on PLANNED_JOURNAL.BlockId = P_BLOCK.BLOCKID  
  INNER JOIN @AvailableOperatives AO ON PLANNED_APPOINTMENTS.ASSIGNEDTO = AO.employeeid  
WHERE  
 (  
 AppointmentDate >= @Today  
 OR AppointmentEndDate >= @Today  
 )  
 AND (PLANNED_STATUS.TITLE <> 'Cancelled'  
 AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')  
 )  
-- PDR Appointments   
UNION ALL SELECT DISTINCT  
 APPOINTMENTSTARTDATE                            AS AppointmentDate  
 ,ASSIGNEDTO                               AS OperativeId  
 ,APPOINTMENTSTARTTIME                            AS StartTime  
 ,APPOINTMENTENDTIME                             AS EndTime  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTSTARTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103)) AS StartTimeInSec  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))  AS EndTimeInSec  
 ,COALESCE(p__property.postcode, P_SCHEME.SCHEMECODE, P_BLOCK.POSTCODE)                AS PostCode  
 ,CASE  
  WHEN PDR_MSAT.propertyid IS NOT NULL  
   THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')  
  WHEN PDR_MSAT.SCHEMEID IS NOT NULL  
   THEN ISNULL(P_SCHEME.SCHEMENAME, '')  
  ELSE ISNULL(P_BLOCK.BLOCKNAME, '') + ' '  
   + ISNULL(P_BLOCK.ADDRESS1, '')  
   + ISNULL(', ' + P_BLOCK.ADDRESS2, '')  
   + ISNULL(' ' + P_BLOCK.ADDRESS3, '')  
 END                                 AS Address  
 ,CASE  
  WHEN PDR_MSAT.propertyid IS NOT NULL  
   THEN P__PROPERTY.TOWNCITY  
  WHEN PDR_MSAT.SCHEMEID IS NOT NULL  
   THEN ''  
  ELSE P_BLOCK.TOWNCITY  
 END                                 AS TownCity  
 ,CASE  
  WHEN PDR_MSAT.propertyid IS NOT NULL  
   THEN P__PROPERTY.COUNTY  
  WHEN PDR_MSAT.SCHEMEID IS NOT NULL  
   THEN ''  
  ELSE P_BLOCK.COUNTY  
 END                                 AS County  
 ,PDR_MSATType.MSATTypeName + ' Appointment'                       AS AppointmentType  
 ,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, LOGGEDDATE)) AS CreationDate
FROM  
 PDR_APPOINTMENTS  
  INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId  
  INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId  
  INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID  
  LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID  
  LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID  
  LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID  
  INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId  
  INNER JOIN @AvailableOperatives AO ON PDR_APPOINTMENTS.ASSIGNEDTO = AO.employeeid  
WHERE  
 (  
 APPOINTMENTSTARTDATE >= @Today  
 OR AppointmentEndDate >= @Today  
 )  
 AND (PDR_STATUS.TITLE <> 'Cancelled'  
 AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')  
 )  
  
END  