
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[SP_NET_UPDATELOGIN]
@login varchar(50),
@password varchar(50),
@newpassword varchar(50),
@retStatus INT = 0 OUTPUT
AS
/*
-- @retStatus:
-- 0 = No error occurred
-- 999 = An exception occurred during insert and/or update.
-- 10 = Username AND/OR Password do not match.
-- 11 = Password do not match, password history policy.
*/
DECLARE @tempError INT = 0
DECLARE @HistoryId INT = NULL

DECLARE @loginId INT
DECLARE @oldPassword NVARCHAR(50)
SELECT @loginId = LOGINID, @oldPassword = PASSWORD FROM AC_LOGINS WHERE LOGIN = @login

IF NOT EXISTS (SELECT LOGINID FROM AC_LOGINS WHERE LOGINID = @loginId AND PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS = @password)
BEGIN 
	SET @retStatus = 10 
	RETURN 10
END

IF EXISTS (SELECT * FROM (SELECT TOP 5 * FROM AC_PASSWORD_HISTORY WHERE	LoginId = @loginId ORDER BY HistoryId DESC) AS Temp WHERE PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS = @newpassword)
   OR EXISTS (SELECT * FROM AC_LOGINS WHERE LOGINID = @loginId AND PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS = @newpassword)
   OR EXISTS (SELECT * FROM AC_LOGINS WHERE LOGINID = @loginId AND DATEDIFF(SECOND, DATEUPDATED, GETDATE()) <= 86400)
BEGIN 
	SET @retStatus = 11
	RETURN 11
END

BEGIN TRANSACTION 	
	
	UPDATE AC_LOGINS SET 
		expires = DATEADD(DAY,90,GETDATE()),
		[PASSWORD] = @newpassword,
		[DATEUPDATED] = GETDATE() 
	WHERE LOGINID = @loginId AND PASSWORD = @password COLLATE SQL_Latin1_General_CP1_CS_AS
	
	SET @tempError = @@ERROR
		
	IF @@ROWCOUNT > 0
	BEGIN		
		PRINT 'ROW COUNT: ' + CONVERT(VARCHAR,@@ROWCOUNT)
		INSERT INTO AC_PASSWORD_HISTORY ([LoginId], [FirstName], [LastName], [UserName], [Password], [DateCreated])
		(SELECT A.LOGINID, E.FIRSTNAME, E.LASTNAME, A.LOGIN, @oldPassword, GETDATE()
		FROM AC_LOGINS A
		INNER JOIN E__EMPLOYEE E ON A.EMPLOYEEID = E.EMPLOYEEID
		WHERE A.LOGINID = @loginId and A.PASSWORD = @newpassword COLLATE SQL_Latin1_General_CP1_CS_AS)	

		WITH CTE(RowNumber, HistoryId)
		AS 
		(
		SELECT ROW_NUMBER() OVER (ORDER BY HistoryId DESC) AS RowNumber, HistoryId FROM AC_Password_History WHERE LoginId = @LoginId
		)
		SELECT @HistoryId=MAX(HistoryId) FROM CTE WHERE RowNumber > 5
		DELETE AC_Password_History WHERE ([LoginId] = @LoginId AND HistoryId <= @HistoryId) 		
	END
	
	IF @tempError <> 0 OR @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		SET @retStatus = 999
		RETURN 999
	END

COMMIT TRANSACTION
SET @retStatus = 0
RETURN 0


GO
