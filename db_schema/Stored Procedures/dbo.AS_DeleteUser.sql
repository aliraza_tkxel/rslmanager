
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
--  EXEC AS_DeleteUser @EmployeeId = 616 (record against this shoukd be existed in Database)
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,28/09/2012>
-- Description:	<Description,,Delete User from the drop down selected Remove User >
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_DeleteUser]
	-- Add the parameters for the stored procedure here
	@EmployeeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update AS_USER SET IsActive = 0 Where AS_USER.EmployeeId = @EmployeeId;
	
	SELECT E__EMPLOYEE.FIRSTNAME,E_JOBDETAILS.JOBTITLE,E__EMPLOYEE.EMPLOYEEID,AS_USERTYPE.UserTypeID,AS_USERTYPE.Description,E__EMPLOYEE.LASTNAME
	FROM AS_USER INNER JOIN
	AS_USERTYPE ON AS_USER.UserTypeID = AS_USERTYPE.UserTypeID INNER JOIN
	E__EMPLOYEE ON AS_USER.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN 
	E_JOBDETAILS ON AS_USER.EmployeeId = E_JOBDETAILS.EMPLOYEEID 
	WHERE AS_USER.IsActive=1 AND (E_JOBDETAILS.ACTIVE = 1 OR E_JOBDETAILS.ENDDATE > GETDATE())
END
GO
