
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- EXEC	[AS_EditAction]
		--@StatusId = 1,
		--@ActionId = 2,
		--@Title = N'test',
		--@Ranking = 2,
		--@ModifiedBy = 1
-- Author:		<Salman Nazir>
-- Create date: <20/11/2012>
-- Description:	<Edit Action's value from table>
-- WebPage: Status.aspx => Edit Action
-- =============================================
CREATE PROCEDURE [dbo].[AS_EditAction]
	(
	@StatusId int,
	@ActionId int,
	@Title varchar(500),
	@Ranking int,
	@ModifiedBy int
	)
AS
BEGIN
Declare @checkOldRanking int
Declare @countRecord int
Declare @actionTableId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT @actionTableId = AS_Action.ActionId 
    FROM AS_Action
    WHERE Ranking = @Ranking AND StatusId = @StatusId
    

    
    SELECT @checkOldRanking = AS_Action.Ranking FROM AS_Action WHERE ActionId = @ActionId
    
   
    
    SELECT @countRecord = COUNT(*) FROM AS_Action WHERE StatusId = @StatusId
    
    if @actionTableId > 0 AND @actionTableId = @ActionId
		BEGIN
   			Update AS_Action SET Title = @Title, Ranking = @Ranking, ModifiedBy = @ModifiedBy
			WHERE ActionId = @ActionId AND StatusId = @StatusId
		END
	ELSE
		BEGIN
			UPDATE AS_Action SET Ranking = @checkOldRanking WHERE ActionId = @actionTableId
			
			Update AS_Action SET Title = @Title, Ranking = @Ranking, ModifiedBy = @ModifiedBy
			WHERE ActionId = @ActionId AND StatusId = @StatusId
		END
END
GO
