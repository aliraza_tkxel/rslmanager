
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[AM_SP_GetFirstDetectionListCount] 
		    @postCode varchar(15),
			@caseOwnedById int=0,
            @regionId	int = 0,
			@suburbId	int = 0,
			@allRegionFlag	bit,
			@allSuburbFlag	bit,
			@surname		varchar(100),
			@address1 varchar(200),
            @tenancyid VARCHAR(15)
AS
BEGIN

declare @RegionSuburbClause varchar(8000)
declare @query varchar(8000)

IF(@caseOwnedById = -1 )
BEGIN
    
	IF(@regionId = -1 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'
		
	END
	ELSE IF(@regionId > 0 and @suburbId = -1)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) 
	END
	ELSE IF(@regionId > 0 and @suburbId > 0)
	BEGIN
		SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId )+ ') ' 
	END
ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END

END
ELSE 
BEGIN
     
		IF(@regionId = -1 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'
		END
		ELSE IF(@regionId > 0 and @suburbId = -1)
		BEGIN
			SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) + ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId 
															FROM AM_ResourcePatchDevelopment 
															WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'
		END
		ELSE IF(@regionId > 0 and @suburbId > 0)
		BEGIN
			SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') ' 
		END
ELSE IF(@regionId = -1 and @suburbId > 0)
    BEGIN
         	SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) 
    END
END

SET @query = ' SELECT COUNT(*) as recordCount FROM(
			SELECT COUNT(*) as recordCount		
			--SELECT P__PROPERTY.DEVELOPMENTID, P__PROPERTY.PATCH
			FROM         AM_FirstDetecionList 
						  --INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
						  --INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID				  
						  --INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
                          --INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
                          --INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
                  INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.TENANCYID = customer.TENANCYID
                  INNER JOIN C_TENANCY ON AM_FirstDetecionList.TENANCYID = C_TENANCY.TENANCYID
				  INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID		
				  LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
				  INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID			  
				  --INNER JOIN AM_Customer_Rent_Parameters customer ON AM_FirstDetecionList.customerId = customer.customerId
                  INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID
			WHERE '+@RegionSuburbClause+' AND dbo.AM_FN_CHECK_OWED_TO_BHA(ISNULL(customer.RentBalance, 0.0),ISNULL(customer.EstimatedHBDue, 0.0))=''true'' 
								AND AM_FirstDetecionList.TenancyId NOT IN (SELECT TenancyId 
																			FROM AM_Case 
																			WHERE AM_Case.IsActive= ''true'' ) 		   
							   AND AM_FirstDetecionList.IsDefaulter = ''true''
							   AND customer.LASTNAME LIKE '''' + CASE WHEN '''' = '''+ REPLACE(@surname,'''','''''') +''' THEN customer.LASTNAME ELSE '''+ REPLACE(@surname,'''','''''') +''' END + ''%''
							   --AND C_ADDRESS.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END 
							   AND P__PROPERTY.POSTCODE=CASE WHEN ''''='''+@postCode+''' THEN P__PROPERTY.POSTCODE ELSE '''+@postCode+''' END 
							   AND (
									(P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR P__PROPERTY.HOUSENUMBER + '' '' + P__PROPERTY.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')  
									--OR 
									--(C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS2 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.ADDRESS3 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'' OR C_ADDRESS.HOUSENUMBER + '' '' + C_ADDRESS.ADDRESS1 LIKE ''%'+ REPLACE(@address1,'''','''''') + '%'')
									)
							   AND customer.TenancyId='''' + CASE WHEN '''' = '''+ REPLACE(@tenancyid,'''','''''') +''' THEN customer.TenancyId ELSE '''+ REPLACE(@tenancyid,'''','''''') +''' END + ''''
							   AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE>GETDATE())
			GROUP By	AM_FirstDetecionList.TENANCYID) as Temp

'
PRINT(@query);
EXEC(@query);



END
GO
