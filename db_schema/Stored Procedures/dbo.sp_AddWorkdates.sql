SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_AddWorkdates]

@DaysAction int,
@StartDate datetime,
@Days int,
@JobID int

AS

if (@DaysAction = 1)
	BEGIN
		insert into H_WORKDATES (StartDate, Days, JobID) 
		VALUES (@StartDate, @Days, @JobID)
	END
else 
 IF (@DaysAction = 2)
	BEGIN
		update H_WORKDATES set StartDate = @StartDate, Days = @Days  where jobid = @JobID
	END 




GO
