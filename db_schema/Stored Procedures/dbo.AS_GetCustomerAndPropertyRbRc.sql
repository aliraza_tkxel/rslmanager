
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- EXEC AS_GetCustomerAndPropertyRbRc 
	--@propertyId = 'BHA0000819'
	--@rentBalance OUTPUT,
	--@rentCharge OUTPUT
-- Author:		<Author,,NoorMuhammad>
-- Create date: <Create Date,,25/Mar/2013>
-- Description:	<Description,,AS_GetCustomerAndPropertyRbRc >
-- Web Page: EditLetter.aspx => Get the customer info and property information 
-- =============================================


IF OBJECT_ID('dbo.AS_GetCustomerAndPropertyRbRc') IS NULL 
	EXEC('CREATE PROCEDURE dbo.AS_GetCustomerAndPropertyRbRc AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE  [dbo].[AS_GetCustomerAndPropertyRbRc]
	-- Add the parameters for the stored procedure here
	@propertyId varchar(12),
	@rentBalance float output,
	@rentCharge float output
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Select 
		C_TENANCY.TENANCYID as Tenancy,		
		ISNULL(CG.LIST,'N/A')  as TenantName,
		ISNULL(P__PROPERTY.HOUSENUMBER, '') AS HOUSENUMBER,		
		ISNULL(P__PROPERTY.ADDRESS1,'') as ADDRESS1,
		ISNULL(P__PROPERTY.ADDRESS2,'') as ADDRESS2,
		ISNULL(P__PROPERTY.TOWNCITY,'') as TOWNCITY,
		ISNULL(P__PROPERTY.COUNTY,'') as COUNTY,
		ISNULL(P__PROPERTY.POSTCODE,'') as POSTCODE,
		ISNULL(CG.LIST,'N/A')  as  TenantNameSalutation
		
	From		
		P__PROPERTY 
		INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE > GETDATE())
		LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I=C_TENANCY.TENANCYID AND CG.ID IN (SELECT MAX(ID) FROM C_CUSTOMER_NAMES_GROUPED GROUP BY I)

	WHERE 
		P__PROPERTY.PROPERTYID = @propertyId	
	
	--EXEC AS_PropertyRbRc @propertyId, @rentBalance output, @rentCharge output
		
IF(@rentCharge <>-1 AND @rentBalance<>-1) 
BEGIN
   EXEC AS_PropertyRbRc @propertyId, @rentBalance output, @rentCharge output;
END
--ELSE
--BEGIN
--   SELECT  @rentBalance  output, @rentCharge output;
--END	
	 

END
GO
