CREATE TYPE [dbo].[PLANNED_AssingToContractorWorksRequired] AS TABLE
(
[WorksRequired] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NetCost] [smallmoney] NOT NULL,
[VatType] [int] NOT NULL,
[VAT] [smallmoney] NOT NULL,
[GROSS] [smallmoney] NOT NULL,
[PIStatus] [int] NOT NULL,
[ExpenditureId] [int] NOT NULL
)
GO
