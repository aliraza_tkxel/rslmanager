CREATE TYPE [dbo].[AS_OutOfHoursInfo] AS TABLE
(
[OutOfHoursId] [int] NOT NULL,
[TypeId] [int] NOT NULL,
[Type] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[StartTime] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndTime] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralStartDate] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralEndDate] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
