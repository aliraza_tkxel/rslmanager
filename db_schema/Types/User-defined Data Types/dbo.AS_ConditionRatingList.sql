CREATE TYPE [dbo].[AS_ConditionRatingList] AS TABLE
(
[ComponentId] [int] NULL,
[WorksRequired] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValueId] [int] NULL
)
GO
