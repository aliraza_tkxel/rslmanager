CREATE TYPE [dbo].[AS_ItemDates] AS TABLE
(
[ParameterId] [int] NULL,
[LastDone] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DueDate] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParameterName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComponentId] [int] NULL
)
GO
