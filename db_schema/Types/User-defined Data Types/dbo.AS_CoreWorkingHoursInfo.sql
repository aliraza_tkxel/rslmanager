CREATE TYPE [dbo].[AS_CoreWorkingHoursInfo] AS TABLE
(
[DayId] [int] NOT NULL,
[StartTime] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndTime] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
