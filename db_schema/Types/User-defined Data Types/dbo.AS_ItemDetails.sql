CREATE TYPE [dbo].[AS_ItemDetails] AS TABLE
(
[ItemParamId] [int] NULL,
[ParameterValue] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValueId] [int] NULL,
[IsCheckBoxSelected] [bit] NULL
)
GO
