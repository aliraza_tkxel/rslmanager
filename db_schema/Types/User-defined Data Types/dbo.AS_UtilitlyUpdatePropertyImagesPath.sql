
/****** Object:  UserDefinedTableType [dbo].[AS_UtilitlyUpdatePropertyImagesPath]    Script Date: 12/03/2013 17:07:49 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'AS_UtilitlyUpdatePropertyImagesPath' AND ss.name = N'dbo')
DROP TYPE [dbo].[AS_UtilitlyUpdatePropertyImagesPath]

/****** Object:  UserDefinedTableType [dbo].[AS_UtilitlyUpdatePropertyImagesPath]    Script Date: 12/03/2013 17:07:49 ******/
CREATE TYPE [dbo].[AS_UtilitlyUpdatePropertyImagesPath] AS TABLE(
	[SId] [int] NULL,
	[PropertyId] [nvarchar](100) NULL,
	[Type] [nvarchar](100) NULL,
	[ImagePath] [nvarchar](300) NULL,
	[ImageName] [nvarchar](300) NULL
)
GO


