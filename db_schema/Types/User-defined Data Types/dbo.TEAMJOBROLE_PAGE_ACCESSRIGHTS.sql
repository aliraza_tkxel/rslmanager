CREATE TYPE [dbo].[TEAMJOBROLE_PAGE_ACCESSRIGHTS] AS TABLE
(
[JobRoleTeamId] [int] NULL,
[PageId] [int] NULL
)
GO
