CREATE TYPE [dbo].[PLANNEDCOMPONENTTRADE] AS TABLE
(
[TRADEID] [int] NULL,
[DURATION] [float] NULL,
[SORDER] [int] NULL
)
GO
