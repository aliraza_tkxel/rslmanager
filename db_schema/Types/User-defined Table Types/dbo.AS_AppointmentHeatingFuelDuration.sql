USE [RSLBHALive]
GO

IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id 
WHERE st.name = N'AS_AppointmentHeatingFuelDuration' AND ss.name = N'dbo')
BEGIN
 
	CREATE TYPE dbo.AS_AppointmentHeatingFuelDuration AS TABLE(
		[HeatingTypeId] INT,
		[Duration] INT,
		[TradeId] INT
	);

 END


