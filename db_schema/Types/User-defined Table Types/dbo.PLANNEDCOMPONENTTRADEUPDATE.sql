USE [RSLBHALive]
GO

IF NOT EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'[dbo].[PLANNEDCOMPONENTTRADEUPDATE]'))
  BEGIN
CREATE TYPE [dbo].[PLANNEDCOMPONENTTRADEUPDATE] AS TABLE(
	[TRADEID] [int] NULL,
	[DURATION] [float] NULL,
	[SORDER] [int] NULL,
	[COMPTRADEID] [int]
)

  END 