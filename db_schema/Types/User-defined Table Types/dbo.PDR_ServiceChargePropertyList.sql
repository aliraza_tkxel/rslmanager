USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PDR_ServiceChargePropertyList]    Script Date: 31-Oct-17 6:15:29 PM ******/
IF NOT EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'[dbo].[PDR_ServiceChargePropertyList]'))
BEGIN
CREATE TYPE [dbo].[PDR_ServiceChargePropertyList] AS TABLE(
	[PropertyId] NVARCHAR(MAX) NULL,
	[IsIncluded] [int] NULL
)
END


