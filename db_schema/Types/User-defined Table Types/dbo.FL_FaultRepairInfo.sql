USE [RSLBHALive]
GO

IF NOT EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'[dbo].[FL_FaultRepairInfo]'))
  BEGIN
      CREATE TYPE [dbo].[FL_FaultRepairInfo] AS TABLE(
	[faultRepairId] [int] NOT NULL,
	[repairId] [int] NULL,
	[repair] [nvarchar](50) NOT NULL,
	[isDeleted] [bit] NULL
	)

  END 