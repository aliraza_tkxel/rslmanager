USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_UtilitlyUpdatePropertyImagesPath]    Script Date: 12/03/2013 17:07:49 ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id 
WHERE st.name = N'CM_ContractorWorkDetail' AND ss.name = N'dbo')
BEGIN

	 
	CREATE TYPE dbo.CM_ContractorWorkDetail AS TABLE(
		[ServiceItemId] [int] NULL,
		[CycleDate] [smalldatetime] NULL,
		[NetCost] [money] NULL,
		[VatId] [int] NULL,
		[VAT] [money] NULL,
		[GrossCost] [money] NULL,
		[WorksRequired] NVARCHAR(500) NULL
	);

 END


