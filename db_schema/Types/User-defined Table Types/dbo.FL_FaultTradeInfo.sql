USE [RSLBHALive]
GO

IF NOT EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'[dbo].[FL_FaultTradeInfo]'))
  BEGIN
CREATE TYPE [dbo].[FL_FaultTradeInfo] AS TABLE(
	[FaultTradeId] [int] NOT NULL,
	[TradeId] [int] NOT NULL,
	[TradeName] [nvarchar](50) NOT NULL,
	[IsDeleted] [bit] NULL
)

  END 

