USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_UtilitlyUpdatePropertyImagesPath]    Script Date: 12/03/2013 17:07:49 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id 
WHERE st.name = N'AS_MSATDetail' AND ss.name = N'dbo')
BEGIN

	EXEC sys.sp_rename 'dbo.AS_MSATDetail', 'zAS_MSATDetail';

	CREATE TYPE dbo.AS_MSATDetail AS TABLE(
		[IsRequired] [bit] NULL,
		[LastDate] [datetime] NULL,
		[Cycle] [int] NULL,
		[CycleTypeId] [int] NULL,
		[CycleCost] [float] NULL,
		[NextDate] [datetime] NULL,
		[AnnualApportionment] [float] NULL,
		[MSATTypeId] [int] NULL
	);

	DECLARE @Name NVARCHAR(776);

	DECLARE REF_CURSOR CURSOR FOR
	SELECT referencing_schema_name + '.' + referencing_entity_name
	FROM sys.dm_sql_referencing_entities('dbo.AS_MSATDetail', 'TYPE');

	OPEN REF_CURSOR;

	FETCH NEXT FROM REF_CURSOR INTO @Name;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		EXEC sys.sp_refreshsqlmodule @name = @Name;
		FETCH NEXT FROM REF_CURSOR INTO @Name;
	END;

	CLOSE REF_CURSOR;
	DEALLOCATE REF_CURSOR;

	DROP TYPE dbo.zAS_MSATDetail;

END



