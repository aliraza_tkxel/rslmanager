USE [RSLBHALive]
GO


IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id 
WHERE st.name = N'PDR_EXCLUDEDPROPERTY' AND ss.name = N'dbo')
BEGIN

	 
	CREATE TYPE dbo.PDR_EXCLUDEDPROPERTY AS TABLE(
		[propertyId] NVARCHAR(40) NULL
	);

 END


