USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_UtilitlyUpdatePropertyImagesPath]    Script Date: 12/03/2013 17:07:49 ******/
IF Not EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id 
WHERE st.name = N'CM_ContractorWorkType' AND ss.name = N'dbo')
BEGIN

	 

	CREATE TYPE dbo.CM_ContractorWorkType AS TABLE(
		[ServiceItemId] [int] NULL,
		[ContractorId] [int] NULL,
		[ContactId] [int] NULL,
		[ExpenditureId] [int] NULL,
		[ContractNetValue] [money] NULL,
		[VatId] [int] NULL,
		[Vat] [money] NULL,
		[ContractGrossValue] [money] NULL,
		[SendApprovalLink] [bit] NULL,
		[Commence] [datetime] NULL,
		[AssignedBy] [int] NULL,
		[AssignedDate] [datetime] NULL,
		[POStatus] [int] NULL,
		[RequiredWorks] [nvarchar](1000) NULL
	);

	END



