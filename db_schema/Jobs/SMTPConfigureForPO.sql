
USE [msdb]
GO

SP_CONFIGURE 'show advanced options', 1
RECONFIGURE WITH OVERRIDE
GO

/* Enable Database Mail XPs Advanced Options in SQL Server */
SP_CONFIGURE 'Database Mail XPs', 1
RECONFIGURE WITH OVERRIDE
GO

SP_CONFIGURE 'show advanced options', 0
RECONFIGURE WITH OVERRIDE
GO

--step 1
EXEC msdb.dbo.sysmail_add_profile_sp
    @profile_name = 'EMailProfilePO'
  , @description = 'Email Profile for PO Queued Repairs list'
Go

--step 2
EXEC msdb.dbo.sysmail_add_account_sp
   @account_name = 'Office365EMailAccount'
  , @description = 'Sending SMTP mails to users'
  , @email_address = 'noreply@broadlandgroup.org'
  , @display_name = 'no-reply'
  , @replyto_address = 'noreply@broadlandgroup.org'
  , @mailserver_name = 'smtp.office365.com'
  , @port = 25
  , @username = 'outgoingmail@broadlandgroup.org'
  , @password = 'S@xupad2'
  , @enable_ssl = 1
Go

--step 3
EXEC msdb.dbo.sysmail_add_profileaccount_sp
    @profile_name = 'EMailProfilePO'
  , @account_name = 'Office365EMailAccount'
  , @sequence_number = 1
GO


