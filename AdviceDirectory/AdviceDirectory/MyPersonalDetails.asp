﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    If Customerid <> "" Then
        ' ALL IS OK
    Else
        Response.Redirect("Login.asp?Session=0")
    End If

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>

<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
If Customerid = "" Then Customerid = -1 End If

Dim FIRSTNAME
Dim SURNAME
Dim GENDER
Dim ETHNICITY
Dim DOB
Dim DISABLED
Dim BENEFITRECIPIENT
Dim LONEPARENT
Dim EMPLOYMENTSTATUS
Dim LENGTHOFUNEMPLOYMENT
Dim NINUMBER
Dim ADDRESSLINE1
Dim ADDRESSLINE2
Dim ADDRESSLINE3
Dim LAA
Dim POSTCODE
Dim TELEPHONE
Dim EMAIL
Dim MOBILE

Function LoadRecord()

Call OpenDB()
SQL = "SELECT * FROM DBO.CUSTOMERID C INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID WHERE C.CUSTOMERID = " & Customerid
Call OpenRs(rsDetails,SQL)
If NOT rsDetails.EOF Then
    FIRSTNAME = rsDetails("FIRSTNAME")
    SURNAME = rsDetails("SURNAME")
    DOB = rsDetails("DOB")
    HOUSENUMBER = rsDetails("HOUSENUMBER")
    ADDRESSLINE1 = rsDetails("ADDRESSLINE1")
    ADDRESSLINE2 = rsDetails("ADDRESSLINE2")
    ADDRESSLINE3 = rsDetails("ADDRESSLINE3")
    LAA = rsDetails("LAA")
    POSTCODE = rsDetails("POSTCODE")
    EMAIL = rsDetails("EMAIL")
    TELEPHONE = rsDetails("TELEPHONE")
    MOBILE = rsDetails("MOBILE")
    GENDER = rsDetails("GENDER")
    DISABLED = rsDetails("DISABLED")
    ETHNICITY = rsDetails("ETHNICITY")
    BENEFITRECIPIENT = rsDetails("BENEFITRECIPIENT")
    LONEPARENT = rsDetails("LONEPARENT")
    EMPLOYMENTSTATUS = rsDetails("EMPLOYMENTSTATUS")
    LENGTHOFUNEMPLOYMENT = rsDetails("LENGTHOFUNEMPLOYMENT")
    NINUMBER = rsDetails("NINUMBER")
End If
Call CloseRs(rsDetails)
Call CloseDB()

End Function


Function EditRecord()

On Error Resume Next
'Call Debug()
FIRSTNAME = nulltest(Request.Form("txt_FIRSTNAME"))
SURNAME = nulltest(Request.Form("txt_SURNAME"))
GENDER = nulltest(Request.Form("sel_GENDER"))
ETHNICITY = nulltest(Request.Form("sel_ETHNICITY"))
DOB = nulltest(Request.Form("txt_DOB"))
DISABLED = nulltest(Request.Form("sel_DISABILITY"))
BENEFITRECIPIENT = nulltest(Request.Form("sel_BENEFITRECIPIENT"))
LONEPARENT = nulltest(Request.Form("sel_LONEPARENT"))
EMPLOYMENTSTATUS = nulltest(Request.Form("sel_EMPLOYMENTSTATUS"))
LENGTHOFUNEMPLOYMENT = nulltest(Request.Form("sel_LENGTHOFUNEMPLOYMENT"))
NINUMBER = nulltest(Request.Form("txt_NINUMBER"))
ADDRESSLINE1 = nulltest(Request.Form("txt_ADDRESSLINE1"))
ADDRESSLINE2 = nulltest(Request.Form("txt_ADDRESSLINE2"))
ADDRESSLINE3 = nulltest(Request.Form("txt_ADDRESSLINE3"))
LAA = nulltest(Request.Form("sel_LAA"))
POSTCODE = nulltest(Request.Form("txt_POSTCODE"))
TELEPHONE = nulltest(Request.Form("txt_TELEPHONE"))
EMAIL = nulltest(Request.Form("txt_EMAIL"))
MOBILE = nulltest(Request.Form("txt_MOBILE"))
BENEFITS = nulltest(Request.Form("checkbox"))

Set objConn	= Server.CreateObject("ADODB.Connection")
    	set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_CLIENT_PERSONALDETAILS_UPDATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, FIRSTNAME)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, SURNAME)
                .Parameters.Append .createparameter("@GENDER", adVarWChar, adParamInput, 1, GENDER)
                .Parameters.Append .createparameter("@ETHNICITY", adVarWChar, adParamInput, 2, ETHNICITY)
                .Parameters.Append .createparameter("@DOB", adDBTimeStamp, adParamInput, 0, DOB)
                .Parameters.Append .createparameter("@DISABLED", adInteger, adParamInput, 0, DISABLED)
                .Parameters.Append .createparameter("@BENEFITRECIPIENT", adInteger, adParamInput, 0, BENEFITRECIPIENT)
                .Parameters.Append .createparameter("@LONEPARENT", adInteger, adParamInput, 0, LONEPARENT)
                .Parameters.Append .createparameter("@EMPLOYMENTSTATUS", adVarWChar, adParamInput, 2, EMPLOYMENTSTATUS)
                .Parameters.Append .createparameter("@LENGTHOFUNEMPLOYMENT", adVarWChar, adParamInput, 2, LENGTHOFUNEMPLOYMENT)
                .Parameters.Append .createparameter("@NINUMBER", adVarWChar, adParamInput, 9, NINUMBER)
                .Parameters.Append .createparameter("@ADDRESSLINE1", adVarWChar, adParamInput, 100, ADDRESSLINE1)
                .Parameters.Append .createparameter("@ADDRESSLINE2", adVarWChar, adParamInput, 50, ADDRESSLINE2)
                .Parameters.Append .createparameter("@ADDRESSLINE3", adVarWChar, adParamInput, 50, ADDRESSLINE3)
                .Parameters.Append .createparameter("@LAA", adInteger, adParamInput, 0, LAA)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 8, POSTCODE)
                .Parameters.Append .createparameter("@TELEPHONE", adVarWChar, adParamInput, 20, TELEPHONE)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, EMAIL)
                .Parameters.Append .createparameter("@MOBILE", adVarWChar, adParamInput, 20, MOBILE)
                .Parameters.Append .createparameter("@BENEFITS", adVarWChar, adParamInput, 1073741823, BENEFITS)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMERID)
                'Execute the function
			    .execute ,,adexecutenorecords
		    End With
	    objConn.Close
    Set objConn = Nothing
        
          ' check for error & set boolean value
  If Err.Number <> 0 Then
    retError = False
  Else
    retError = True
  End If

  On Error GoTo 0

  ' return boolean value
  EditRecord = retError

End Function

If Request.Form("Save") <> "" Then

  
Call EditRecord()

Dim ErrorClass
Dim passcheck 		' EMAIL SENT YES/NO (REASON WHY)
	passcheck = ""
		
	If EditRecord() Then
		passcheck_text = passcheck_text & "<div style='padding:10px;'>Personal Details have been updated.</div>"
		ErrorClass = " class=""no_error"""
	Else
		passcheck_text = passcheck_text & "<div style='padding:10px;'>There was an error updating Personal Details. Please try again.</div>"
		ErrorClass = " class=""er"""
	End If

End If

Call LoadRecord()

Dim STR_LAA
Dim STR_GENDER
Dim STR_DISABILITY
Dim STR_ETHNICITY
Dim STR_EMPLOYMENTSTATUS
Dim STR_LENGTHOFUNEMPLOYMENT
Dim STR_LONEPARENT
Dim STR_BENEFITRECIPIENT

Call OpenDB()
    Call BuildSelect(STR_LAA,"sel_LAA","LAA","ID,DESCRIPTION","DESCRIPTION","Please Select",LAA,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Local Authority"" ") 
    Call BuildSelect(STR_GENDER,"sel_GENDER","L13","L13ID,DESCRIPTION","L13ID","Please Select",GENDER,Null,Null," required=""N"" validate=""/([M|F|O])$/""  validateMsg=""Gender"" ") 
    Call BuildSelect(STR_DISABILITY,"sel_DISABILITY","L14","L14ID,DESCRIPTION","L14ID","Please Select",DISABLED,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Disability"" ") 
    Call BuildSelect(STR_ETHNICITY,"sel_ETHNICITY","L12","L12ID,DESCRIPTION","L12ID","Please Select",ETHNICITY,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Ethnicity"" ") 
    Call BuildSelect(STR_EMPLOYMENTSTATUS,"sel_EMPLOYMENTSTATUS","E12","E12ID,DESCRIPTION","DESCRIPTION","Please Select",EMPLOYMENTSTATUS,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Employment Status"" ") 
    Call BuildSelect(STR_LENGTHOFUNEMPLOYMENT,"sel_LENGTHOFUNEMPLOYMENT","E14","E14ID,DESCRIPTION","DESCRIPTION","Please Select",LENGTHOFUNEMPLOYMENT,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Length of Unemployment"" ") 
    Call BuildSelect(STR_LONEPARENT,"sel_LONEPARENT","N06","N06ID,DESCRIPTION","DESCRIPTION","Please Select",LONEPARENT,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Lone Parent"" ") 
    'Call BuildSelect(STR_BENEFITRECIPIENT,"sel_BENEFITRECIPIENT","N05","N05ID,DESCRIPTION","DESCRIPTION","Please Select",BENEFITRECIPIENT,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Benefit Recipient"" ") 
    Call BuildSelect(STR_BENEFITRECIPIENT,"sel_BENEFITRECIPIENT","YesNo","YesNoID,DESCRIPTION","DESCRIPTION","Please Select",BENEFITRECIPIENT,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Benefit Recipient"" onchange=""toggleBR(this.value)"" ") 
Call CloseDB()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);

function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}

function toggleBR(value)
{
    if (value !="")
    {
        if (value == 1)
        {
            //checkAll(document.Form1.checkbox)
        }
        if (value == 0)
        {
            uncheckAll(document.Form1.checkbox)
        }
    }
}


function checkbox_checker(noReq)
{
    // set var checkbox_choices to zero
var checkbox_choices = 0;
    // Loop from zero to the one minus the number of checkbox button selections
for (counter = 0; counter < document.Form1.checkbox.length; counter++)
{
    // If a checkbox has been selected it will return true
    // (If not it will return false)
    if (document.Form1.checkbox[counter].checked)
        { checkbox_choices = checkbox_choices + 1; }
}
//if (checkbox_choices > noReq)
//{
//    // If there were more than three selections made display an alert box 
//    return (false);
//}
if (checkbox_choices < noReq)
{
    // If there were less then selections made display an alert box 
    return (false);
}
    // If three were selected then display an alert box stating input was OK
    return (true);
}


function cck(frm)
{
    if (document.getElementById("sel_BENEFITRECIPIENT").value == 1)
    {
        if (!(checkbox_checker(1)))
        {
            alert('Please select 1 Type of Benefit')
            return false;
        }
        else
        {
            return checkMyForm(frm)
        }
    }
    else
    {
        return checkMyForm(frm)
    }

    //if (document.getElementById("sel_BENEFITRECIPIENT").value = "")
    //return checkMyForm(frm)
}


//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
                    <form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>" onsubmit="return cck(this);">
                        <fieldset>
	                        <legend>Login to this site</legend>
	                        <div id="Frm" style="float:left; width:100%;">
	                            <div class="row">
	                                <div style="float:left; width:100%; background-color:#cc0000; color:white; padding-bottom:1em; padding-top:1em; margin-bottom:1em;">
	                                    <div style="float:left; margin-left:1em">
	                                        Personal Details
	                                    </div>
	                                    <div style="float:right; margin-right:1em">
	                                        <input class="submit" type="button" accesskey="B" name="Back" title="Button : My Details" value="Back" onclick="Javascript:window.location.href='MyDetails.asp'" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
                                    <span class="label">
                                        <label for="txt_FIRSTNAME" accesskey="F">
                                            <span style="text-decoration:underline; cursor:pointer">F</span>irstname:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="40" value="<%=FIRSTNAME%>" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Firstname" />
                                        <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_SURNAME" accesskey="S">
                                            <span style="text-decoration:underline; cursor:pointer">S</span>urname:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_SURNAME" id="txt_SURNAME" maxlength="40" value="<%=SURNAME%>" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Surname" />
                                        <img src="/js/FVS.gif" name="img_SURNAME" id="img_SURNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_DOB" accesskey="D">
                                            <span style="text-decoration:underline; cursor:pointer">D</span>OB:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_DOB" id="txt_DOB" maxlength="10" value="<%=DOB%>" required="Y" validate="/^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4}|[0-9]{2})$/" validateMsg="Date of Birth (dd/mm/yyyy)" />
                                        <img src="/js/FVS.gif" name="img_DOB" id="img_DOB" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_ADDRESSLINE1" accesskey="A">
                                            <span style="text-decoration:underline; cursor:pointer">A</span>ddress Line 1:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ADDRESSLINE1" id="txt_ADDRESSLINE1" maxlength="100" value="<%=ADDRESSLINE1%>" required="N" validate="/([a-zA-Z0-9,\s]{4,100})$/" validateMsg="Address line 1" />
                                        <img src="/js/FVS.gif" name="img_ADDRESSLINE1" id="img_ADDRESSLINE1" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_ADDRESSLINE2" accesskey="A">
                                            <span style="text-decoration:underline; cursor:pointer">A</span>ddress Line 2:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ADDRESSLINE2" id="txt_ADDRESSLINE2" maxlength="50" value="<%=ADDRESSLINE2%>" required="N" validate="/([a-zA-Z0-9,\s]{4,50})$/" validateMsg="Address line 2" />
                                        <img src="/js/FVS.gif" name="img_ADDRESSLINE2" id="img_ADDRESSLINE2" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_ADDRESSLINE3" accesskey="A">
                                            <span style="text-decoration:underline; cursor:pointer">A</span>ddress Line 3:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ADDRESSLINE3" id="txt_ADDRESSLINE3" maxlength="50" value="<%=ADDRESSLINE3%>" required="N" validate="/([a-zA-Z0-9,\s]{4,50})$/" validateMsg="Address line 3" />
                                        <img src="/js/FVS.gif" name="img_ADDRESSLINE3" id="img_ADDRESSLINE3" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_LAA" accesskey="L">
                                            <span style="text-decoration:underline; cursor:pointer">L</span>ocal Authority:
                                        </label>
                                    </span>
                                    <span class="formw">
                                         <%=STR_LAA%>
                                         <img src="/js/FVS.gif" name="img_LAA" id="img_LAA" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_POSTCODE" accesskey="P">
                                            <span style="text-decoration:underline; cursor:pointer">P</span>ostcode:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="8" value="<%=POSTCODE%>" required="Y" validate="/^[A-Za-z]{1,2}\d{1,2}[A-Za-z]? \d[A-Za-z]{2}$/" validateMsg="Postcode" />
                                        <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_EMAIL" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">E</span>mail:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_EMAIL" id="txt_EMAIL" maxlength="250" value="<%=EMAIL%>" required="N" validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" validateMsg="Email" />
                                        <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
	                            <div class="row">
	                                <span class="label">
	                                    <label for="txt_TELEPHONE" accesskey="T">
	                                        <span style="text-decoration:underline; cursor:pointer">T</span>elephone:
	                                    </label></span>
	                                <span class="formw">
		                                <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_TELEPHONE" id="txt_TELEPHONE" maxlength="20" value="<%=TELEPHONE%>" required="N" validate="/^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/" validateMsg="Telephone" />
	                                </span>
	                            </div>
	                            <div class="row">
	                                <span class="label">
	                                    <label for="txt_MOBILE" accesskey="M">
	                                        <span style="text-decoration:underline; cursor:pointer">M</span>obile:
	                                    </label>
	                                </span>
	                                <span class="formw">
		                                <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="text" name="txt_MOBILE" id="txt_MOBILE" maxlength="20" value="<%=MOBILE%>" required="N" validate="/^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/" validateMsg="Mobile" />
	                                </span>
	                            </div>
	                            <div class="row">
                                    <span class="label">
                                        <label for="txt_NINUMBER" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">N</span>ational Insurance Number:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_NINUMBER" id="txt_NINUMBER" maxlength="12" value="<%=NINUMBER%>" required="N" validate="/^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$/" validateMsg="National Insurance Number" />
                                        <img src="/js/FVS.gif" name="img_NINUMBER" id="img_NINUMBER" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="sel_GENDER" accesskey="G">
                                            <span style="text-decoration:underline; cursor:pointer">G</span>ender:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_GENDER%>
                                        <img src="/js/FVS.gif" name="img_GENDER" id="img_GENDER" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="sel_DISABILITY" accesskey="D">
                                            <span style="text-decoration:underline; cursor:pointer">D</span>isability:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_DISABILITY%>
                                        <img src="/js/FVS.gif" name="img_DISABILITY" id="img_DISABILITY" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="sel_ETHNICITY" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">E</span>thnicity:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_ETHNICITY%>
                                        <img src="/js/FVS.gif" name="img_ETHNICITY" id="img_ETHNICITY" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="sel_BENEFITRECIPIENT" accesskey="B">
                                            <span style="text-decoration:underline; cursor:pointer">B</span>enefit Recipient:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_BENEFITRECIPIENT%>
                                        <img src="/js/FVS.gif" name="img_BENEFITRECIPIENT" id="img_BENEFITRECIPIENT" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
	                                    <input type="button" value=" Select All Benefits     " onclick="checkAll(document.Form1.checkbox)" style="width:auto;" /><input type="button" value=" Deselect All Benefits " onclick="uncheckAll(document.Form1.checkbox)" style="width:auto; clear:left" />
                                    </span>
                                    <span class="formw" style="border:solid 1px #cc0000"> 
                                        <span class="formw" style="background-color:#cc0000; color:White; width:100%; margin-bottom:5px">
                                            <span style="padding:5px">Type of Benefit</span>
                                        </span>
                                        <%
                                        set rsBooks = Server.CreateObject("ADODB.Recordset")
                                            rsBooks.ActiveConnection = DSN_CONNECTION_STRING
                                            rsBooks.Source = "SELECT N05.N05ID AS ID, N05.DESCRIPTION, CN.CUSTOMERID_N05 AS SELECTED " &_
                                                             "FROM DBO.CUSTOMERID_N05 CN " &_
                                                             "RIGHT OUTER JOIN N05 ON N05.N05ID = CN.N05ID AND CN.CUSTOMERID = " & Customerid
                                            rsBooks.CursorType = 0
                                            rsBooks.CursorLocation = 2
                                            rsBooks.LockType = 3
                                            rsBooks.Open()

                                        Dim numberColumns
                                        Dim startrw
                                        Dim endrw
                                        Dim numrows
                                            startrw = 0
                                            endrw = Repeat1__index
                                            numberColumns = 2
                                        while Not rsBooks.EOF
	                                        startrw = endrw + 1
	                                        endrw = endrw + numberColumns
                                        %>
                                        <span style="float:left; border: solid 0px green; width:100%; margin-bottom:1em; padding-left:5px">
                                        <%
                                        While ((startrw <= endrw) AND (Not rsBooks.EOF))
                                        %>
                                        <span style="float:left; border: solid 0px blue; width:50%;">
                                            <span class="formw" style="white-space:nowrap; float:left; border: solid 0px red"> <label for="rdo_ASSESSMENT_<%=rsBooks("ID")%>"><%=rsBooks("DESCRIPTION")%>:</label> </span>
                                            <span class="label" style="float:right; border: solid 0px green">
                                                <input
                                                    style="width:auto"
                                                    type="checkbox"
                                                    name="checkbox"
                                                    <%If rsBooks("selected") <> "" Then Response.Write "Checked=""Checked""" End If %>
                                                    value="<%=rsBooks("ID")%>" />
                                            </span>
                                        </span>
	                                        <%
	                                        startrw = startrw + 1
	                                        rsBooks.MoveNext()
	                                        Wend
	                                        %>
		                                        </span>
		                                        <%
                                         numrows=numrows-1
                                         Wend
                                         %> 
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="sel_LONEPARENT" accesskey="L">
                                            <span style="text-decoration:underline; cursor:pointer">L</span>one Parent:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_LONEPARENT%>
                                        <img src="/js/FVS.gif" name="img_LONEPARENT" id="img_LONEPARENT" width="15px" height="15px" alt="" />
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        <label for="sel_EMPLOYMENTSTATUS" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">E</span>mployment Status:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_EMPLOYMENTSTATUS%>
                                        <img src="/js/FVS.gif" name="img_EMPLOYMENTSTATUS" id="img_EMPLOYMENTSTATUS" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="sel_LENGTHOFUNEMPLOYMENT" accesskey="L">
                                            <span style="text-decoration:underline; cursor:pointer">L</span>ength Of Unemployment:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_LENGTHOFUNEMPLOYMENT%>
                                        <img src="/js/FVS.gif" name="img_LENGTHOFUNEMPLOYMENT" id="img_LENGTHOFUNEMPLOYMENT" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                               <div class="row">
                                    <span class="label">&nbsp;</span>
                                    <span class="formw">
                                        <input class="submit" type="submit" name="Save" id="Save" value=" Save " title="Submit Button : Save your details" style="width:auto"/>
                                    </span>
                               </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>