﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
If Request.Form("Login") <> "" Then

    Dim txt_USERNAME	' FORM FIELD - USERNAME
    Dim strPASSWORD 	' FORM FIELD - PASSWORD
    Dim rsLogin			' RECORDSET
    Dim passcheck 		' USER OK/NOT OK TO LOGIN
    Dim passcheck_text

	    txt_USERNAME = "xyz"
	    txt_password = "xyz"
	    passcheck = ""

        ' VALIDATE USERNAME
	    If (Request.Form("txt_USERNAME") <> "") Then
		    If isUsername(Request.Form("txt_USERNAME")) = True Then
			    strUSERNAME = Request.Form("txt_USERNAME")
		    Else
			    strUSERNAME = isUsername(Request.Form("txt_USERNAME"))
		    End If
	    End If
        ' VALIDATE PASSWORD
	    If (Request.Form("txt_PASSWORD") <> "") Then
		    strPASSWORD = RegExpTrimAll(Request.Form("txt_PASSWORD"))
	    End If

        ' LOGIN USING SPECIFIED DETAILS
	    Call Login(strUSERNAME, strPASSWORD)

        ' SET ERROR FLAG - CSS STYLE SHEET USED
	    If passcheck = 0 Then
            ErrorClass = " class=""er"""
        Else
            ErrorClass = " class=""no_error"""
        End If

End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:5px; padding-bottom:5px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

-->
</style>

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                     <!-- ERRORS/PAGE LOAD -->
                     <div id="errordiv" style="float:left; width:100%;" <%=ErrorClass%>>
                            <%=passcheck_text%>
                    </div>
    <div id="FormDetails" style="float:left;width:100%;">
	  <fieldset>
		    <legend>Login to this site</legend>
			            <div id="Frm" style="float:left; width:48%;">
			            <p class="H4">
			            Already got a Username and Password? Login now:
			            </p>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_USERNAME" accesskey="U">
                                        <span style="text-decoration:underline; cursor:pointer">U</span>sername:
                                    </label> 
                                </span>
                                <span class="formw">
                                    <input 
                                    class="border-white" 
                                    onblur="this.className='border-white'" 
                                    onfocus="this.className='border-red'" 
                                    type="text" 
                                    size="40" 
                                    name="txt_USERNAME" 
                                    id="txt_USERNAME" 
                                    maxlength="100" 
                                    value="" 
                                    tabindex="1" 
                                    required="Y" 
                                    validate="/[a-zA-Z0-9_\@\.\-]{6,70}$/"
                                    validateMsg="Username :- Must be 6 - 70 characters in length. Letters and Numbers are permitted. Spaces and most special characters are not permitted." />
                                    <!-- /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/ //-->
                                    <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label">
                                    <label for="txt_PASSWORD" accesskey="P">
                                        <span style="text-decoration:underline; cursor:pointer">P</span>assword:
                                    </label>
                                </span>
                                <span class="formw">
                                    <input 
                                        class="border-white" 
                                        onblur="this.className='border-white'" 
                                        onfocus="this.className='border-red'" 
                                        type="password" 
                                        size="40" 
                                        name="txt_PASSWORD" 
                                        id="txt_PASSWORD" 
                                        maxlength="10" 
                                        tabindex="2" 
                                        value="" 
                                        required="Y" 
                                        validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                        validateMsg="Password - Password must be between 6 and 10 Characters and contain at least 1 number." />
                                    <img src="/js/FVS.gif" name="img_password" id="img_password" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label">&nbsp;</span>
                                <span class="formw">
                                    <input class="submit" 
                                        type="submit" 
                                        name="Login" 
                                        id="Login" 
                                        value=" Login " 
                                        title="Submit Button : Login" 
                                        tabindex="3" 
                                        style="width:auto"/>
                                </span>
                           </div>
                        </div> 
		</fieldset>
    </div>
     <a href="/forgottenyourpassword.asp" title="Forgotten your password?">Forgotten your password?</a>
</div>
			<div style="float:left; padding-top:2em; height:40px; width:100%; white-space:nowrap">
                <h1 class="title" style="padding-bottom: 0.5em; margin-bottom:1em; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left bottom;">Register</h1><b><a href="/Register.asp" title="Create your own Greater Manchester Advancement Network account">Create</a> your own Greater Manchester Advancement Network account.</b>
            </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif" width="10px" height="10px" name="FVS_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif" width="10px" height="10px" name="FVW_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
</form>
</body>
</html>