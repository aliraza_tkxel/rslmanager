﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/Calendar|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	'Call PageContent(GP_curPath)
	Call PageContent("Calendar")
%>
<%
If Request.Form("Record2Edit") = "" Then
	Response.Redirect("editevent.asp")
End If
'Create a connection object
Set Conn = Server.CreateObject("ADODB.Connection")
'Decide what data source to use
Conn.Open Application("MSSQLConnectionString")
strQuery = "SELECT * FROM event WHERE event_id = " & Request.Form("Record2Edit")
set rs = Conn.Execute(strQuery)
'Return a recordset containing all the event types...
strQuery = "SELECT * FROM event_type ORDER BY event_type_desc"
set objRs = Conn.Execute(strQuery)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	/*border-top:1px dotted #cc0000;*/
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	/*background:gray;
	color:#ffffff;
	cursor:pointer;*/
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.higlight
{
background-color:yellow
}

td a {color: <%=Application("CalLinkColor")%> !important;	text-decoration: none !important;}
td a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
td a:hover {color: <%=Application("CalHoverColor")%> !important;	text-decoration: none !important;}

span.calTxt a {color: #cc0000 !important;	text-decoration: none !important;}
span.calTxt a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
span.calTxt a:hover {color: <%=Application("CalHoverColor")%> !important; text-decoration: none !important;}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">

function setEndTime()
{
	document.getElementById("postendtime").selectedIndex = document.getElementById("poststarttime").selectedIndex;
}

function checkEndTime()
{
	if (document.getElementById("postendtime").value < document.getElementById("poststarttime").value)
	{
		alert("The End Time for the event must be later than the start time");
		document.getElementById("postendtime").selectedIndex = document.getElementById("poststarttime").selectedIndex;
	}
}

function AddTheItem()
{
	temp2 = document.getElementById("selListNames").value;
	if (temp2 == "") return true;
	temp = document.getElementById("tbListNames").value + "";
	if (temp == "") 
	{
		document.getElementById("tbListNames").value = "a" + temp2 + "a";
		populatenames();
		return true;
	}
	thearray = temp.split(",");
	for (var i=0; i<thearray.length; i++)
	{
		temp3 = "a" + temp2 + "a";
		if (temp3 == thearray[i])
		{
			return true;
		}
	}
	document.getElementById("tbListNames").value += "," + temp3;
	populatenames();
}

function clearall()
{
	document.getElementById("tbListNames").value = "";
	populatenames();
}

function RemoveTheItem()
{
	temp2 = document.getElementById("selListNames").value;
	if (temp2 == "") return true;	
	temp = document.getElementById("tbListNames").value + "";
	if (temp == "")
	{
		populatenames();	
		return true;
	}
	thearray = temp.split(",");
	for (var i=0; i<thearray.length; i++)
	{
		temp3 = "a" + temp2 + "a";
		if (temp3 == thearray[i])
		{
			newstring = ""
			for (var j=0; j<thearray.length;j++)
			{
				if (j != i)
				{
					if (newstring != "")
						newstring += "," + thearray[j] + "";
					else
						newstring += "" + thearray[j] + "";
				}
			}
			document.getElementById("tbListNames").value = newstring;
			populatenames();
			return true;
		}
	}
}


function populatenames()
{
	document.getElementById("selListNames").disabled = true;
	mystring = ""
	document.getElementById("selListNames").options[document.getElementById("selListNames").selectedIndex].text;
	temp = document.getElementById("tbListNames").value + "";
	if (temp == "")
	{
		document.getElementById("taListNames").value = "";
		document.getElementById("selListNames").disabled = false;
		return true;
	}
	temp = temp.replace(/\a/g, "");
	thearray = temp.split(",");
	mystring = new Array();
	for (var i=0; i<thearray.length; i++)
	{
		document.getElementById("selListNames").value = thearray[i];
		if (document.getElementById("selListNames").value == "")
			mystring[i] = "Unknown User";
		else
			mystring[i] = document.getElementById("selListNames").options[document.getElementById("selListNames").selectedIndex].text;
	}
	mystring = mystring.sort();
	mystring = mystring + "";
	mystring = mystring.replace(/\,/g, ",  ");
	document.getElementById("taListNames").value = mystring;
	document.getElementById("selListNames").value = "";
	document.getElementById("selListNames").disabled = false;
}

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}

/*
function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}
*/

function buildQueryString(theFormName) {

  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            
            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function HTTPDrawFieldItem(FieldItem, strURL)
{
	//call xmlhttp functions
    var field = document.getElementById(FieldItem);
    if (field)
    {
        for(var i=field.length-1; i>=0; i--)
        {
            field.options[i]=null 
        }
        document.getElementById(FieldItem).add(new Option("Retrieving Data....",null));

	    xmlhttp.open("GET",strURL,true);
 	    xmlhttp.onreadystatechange=function()
 	    {
		    if (xmlhttp.readyState == 4)
		    {
		       strResponse = xmlhttp.responseText;
		       switch (xmlhttp.status)
		       {
				       // Page-not-found error
				       case 404:
						       alert("Error: Not Found. The requested URL could not be found.");
						       document.getElementById(FieldItem).outerHTML = " "
						       break;
				       // Display results in a full window for server-side errors
				       case 500:
						       alert("Error: Server Error. An unexpected errror 500 has occurred.");
						       break;
				       default:
						        // Call JS alert for custom error or debug messages
						        if (strResponse.indexOf('Error:') > -1 || strResponse.indexOf('Debug:') > -1) 
							    {
								    alert("Error: A JavaScript Error has been encountered. " + strResponse);
								    document.getElementById(FieldItem).outerHTML = " "
						        }
						       // Call the desired result function
						       else 
						       {
								       document.getElementById(FieldItem).outerHTML = strResponse
								      // try {
								    //	alert('hum!')
								    //	   }
								    //	catch(e){
								    //		}
						       }
						       break;
			        }
	            }
 	        }
        xmlhttp.send(null) 
    }
}

function DrawFieldItem(FieldItem)
{
	HTTPDrawFieldItem(FieldItem,'svr/lb.asp' + buildQueryString('Form1'))
}

function SubmitMe()
{
    var btn = valButton(Form1.Radio1);
    if (btn == null) 
    {
        document.Form1.submit();
    }
    //alert('No radio button selected');
    else //alert('Button value ' + btn + ' selected');
    {
        if ((document.getElementById("tbListNames").value == "") && (btn == 1))
        {
            alert('Please specify a "Name" to assign to the Calendar Entry')
        }
        else
        {
            document.Form1.submit();
        }
    }
}

function valButton(btn)
{
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--)
    {
       if (btn[i].checked) {cnt = i;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null; 
}

</script>
</head>
<body lang="en" onload="populatenames();">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%> : Edit calendar entry</h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
	        <table border="0" cellspacing="0" cellpadding="5">
		        <tr>
			        <td valign="top">
				        <form action="editevent3.asp" method="POST" name="Form1" id="Form1">	
				        <input type="hidden" name="postid" id="postid" value="<%=Request.Form("Record2Edit")%>" />
				        <table border="0">
					        <tr>
					            <td colspan="2"><strong>NOTE:</strong> You must fill out the date and title fields or event will not post.</td></tr>
					        <tr>
						        <td align="right" nowrap="nowrap">
							        Event Date:
						        </td>
						        <td>
							        <input type="text" name="postdate" id="postdate" value="<%=rs("event_date")%>" size="14" maxlength="14" />
						        </td>
					        </tr>
					        <tr>
						        <td align="right">Start Time:</td>
						        <td>
                                    <select name="poststarttime" id="poststarttime" onchange="setEndTime()">
                                        <%
									        strQuery = "SELECT * FROM DiaryTime ORDER BY ActTime"
									        Set objRs2 = Conn.Execute(strQuery)
									        Dim alloptions
									            alloptions = ""
									        Dim alloptions2
									            alloptions2 = ""
									        Dim temp
											Dim selected
											Dim selected2
											Do While Not objRs2.EOF
												temp = objRs2("ActTime")
												selected = ""
												if (Left(CStr(temp),4) = Left(CStr(rs("event_start_time")),4)) then 
													selected = "selected" 
												end if
												selected2 = ""
												if (Left(CStr(temp),4) = Left(CStr(rs("event_end_time")),4)) then 
													selected2 = "selected" 
												end if
												alloptions = alloptions & "<option value='" & temp & "' " & selected & ">" & temp & "</option>"
												alloptions2 = alloptions2 & "<option value='" & temp & "' " & selected2 & ">" & temp & "</option>"												
												objRs2.MoveNext
											Loop
											    objRs2.close
											Set objRs2 = Nothing
											rw alloptions
								%>
						    </select> 
						</td>
					</tr>
					<tr>
						<td align="right">End Time:</td>
						<td>						
                            <select name="postendtime" id="postendtime" onblur="checkEndTime()">
                                <%=alloptions2%>
                            </select>
						</td>
					</tr>
					<tr>
						<td align="right">Title:</td>
						
                  <td>
                    <input type="text" name="posttitle" id="posttitle" size="50" maxlength="30" value="<%=rs("event_title")%>" />
                    </td>
					</tr>
					<tr>
						<td align="right">URL:</td>
						<td>
                    <input type="text" name="posturl" id="posturl" size="50" maxlength="30" value="<%=rs("event_url")%>" />
                    </td>
					</tr>
					<tr>
						<td align="right">Event Type:</td>
						<td>
                            <select name="posttype" id="posttype">
                            <%Do While Not objRs.EOF%>
						        <option value="<%=objRs("event_type_id")%>" <%If objRs("event_type_id") = rs("event_type_id") Then%>SELECTED<%End If%>><%=objRs("event_type_desc")%></option>
					        <%objRs.MoveNext
						        Loop
						        objRs.close
						        Set objRs = Nothing
						    %>
					        </select>
						</td>
				</tr>					
				<tr>
					<td align="right" valign="top">Notes:</td>
					<td>
                        <textarea name="postnotes" id="postnotes" rows="5" cols="49"><%=rs("event_notes")%></textarea>
                    </td>
				</tr>
				<tr>
						<td></td>
						<td></td>
				</tr>	
				<tr> 
                  <td align="right">Submit To:</td>
				  <td>&nbsp All:
                    <% If (isnull(( rs("event_submit_to"))) OR (rs("event_submit_to") = "")) Then %>
                    <input type="radio" name="Radio1" id="Radio1" value="0" checked="checked" onclick={document.getElementById('SubmitTo').style.display="none";tbListNames.value=""} />
                    Specify:                  
                    <input type="radio" name="Radio1" id="Radio2" value="1" onclick={document.getElementById('SubmitTo').style.display="block";populatenames()} />
					<%	Else %>
                    <input type="radio" name="Radio1" id="Radio3" value="0" onclick={document.getElementById('SubmitTo').style.display="none";tbListNames.value=""} />
                    Specify:                  
                    <input type="radio" name="Radio1" id="Radio4" value="1" checked="checked" onclick={document.getElementById('SubmitTo').style.display="block";populatenames()} />
                    <% End If %>
                  </td>                  
                </tr>
                <%
'Retrieve all first and last names from the userdate table
Dim Fullname
Dim strNameOptions
Dim OptionValue

Dim STR_Provider
Dim BooleanProvider
Dim ProviderID
    ProviderID = nulltest(Session("svPROVIDERID"))

    If (NullNestTF(ProviderID) = True) Then
        BooleanProvider = False
    Else
        If NOT IsNumeric(ProviderID) Then
            BooleanProvider = False
        Else
            BooleanProvider = True
        End If
    End If

    'If BooleanProvider = True Then
    '    STR_Provider = " AND P.PROVIDERID = " & ProviderID & " AND P.ORGSTATUS = 1 AND L.LOGINSTATUS = 1"
    'Else
    '    STR_Provider = ""
    'End If

'Set objConn3 = Server.CreateObject("ADODB.Connection")
'objConn3.Open Application("MSSQLConnectionString")
'Create a recordset to return all the event types in the database
'strQuery3 = "SELECT FIRSTNAME AS USERFNAME, LASTNAME AS USERSNAME, LOGINID AS WUSERID " &_
'"FROM TBL_MD_LOGIN L " &_
'"INNER JOIN TBL_MD_PROVIDER P ON L.PROVIDERID = P.PROVIDERID " & STR_Provider & " " &_
'"ORDER BY USERFNAME, USERSNAME "

'Set objRs3 = objConn3.Execute(strQuery3)
'strNameOptions = "<option value="""">Please Select</option>"
'strFullname = ""
'OptionValue = 0
'If Not objRs3.EOF Then
'        objRs3.MoveFirst
'    Do While Not objRs3.EOF
'        Fullname = objRs3("UserFName") & " " &  objRs3("UserSName")
'        strNameOptions = strNameOptions & "<OPTION VALUE=" & objRs3("WUserid") & ">" & Fullname & "</OPTION>"
'        OptionValue = OptionValue + 1
'        objRs3.MoveNext
'    Loop
'End If

    Call OpenDB()
    
    Set rsLang = server.CreateObject("ADODB.Recordset")
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "CRMS_ORGANISATIONS"
      set .ActiveConnection=conn
      set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
      .parameters.append param
      Set rsProvider = .execute 
    end with
    
    Call BuildSelect_SP(strProvider,"sel_Provider",rsProvider,"Please Select",null,"width:auto",null,"onchange=""DrawFieldItem('selListNames')""")
    Call CloseDB()

%>
<% IF (( isnull(rs("event_submit_to"))) OR ( rs("event_submit_to") = "")) then %>
                <tbody id="SubmitTo" style="display:none"> 
<% ELSE %>			
                <tbody id="TBODY1">
<% END IF %>
                <tr>
                    <td>Organisation:</td>
                    <td><%=strProvider%></td>
                </tr>
                <tr> 
                  <td align="right">Name(s):</td>
                  <td>
                    <select name="selListNames" id="selListNames" onblur="checkEndTime()">
                      <%'=strNameOptions%> <option value="">Please Select</option>
                    </select>
                    </td>
                  <td></td>
                </tr>
                <tr> 
                  <td align="right"></td>
                  <td> 
                    <textarea name="taListNames" id="taListNames" readonly="readonly" rows="3" cols="49"></textarea>
                    <input type="text" name="tbListNames" id="tbListNames" style="display:none" value="<%=rs("event_submit_to")%>" readonly="readonly" />
                  </td>
                
                </tr>
                <tr>
                  <td></td>
                  <td align="right">  
                    <input type="button" name="Add" onclick="AddTheItem()" value=' Add ' />
                    <input type="button" name="Remove" onclick="RemoveTheItem()" value=' Remove ' />
                    <input type="button" name="Remove2" onclick="clearall()" value=' Clear' />
                  </td>
				  </tr>
                </tbody> 				
				</table>
				<table border="0">
					<tr>
						<td colspan="2">
							<input type="button" name="PostRecord" value=" Update Event " onclick="SubmitMe()" />
							<input type="button" name="BtnCancel" value=" Cancel Changes " onclick="window.location.href='/Calendar/'" />
						</td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
	</table>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="serverFrame" id="serverFrame"                   width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 	
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>