﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/Calendar|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	'Call PageContent(GP_curPath)
	Call PageContent("Calendar")
%>
<% 
Set Conn = Server.CreateObject("ADODB.Connection")
'Decide what data source to use and build a query to grab an event to view.
Conn.Open Application("MSSQLConnectionString")
strEventQuery = "SELECT " &_
"1 as EntryActive, DEL = CASE " &_
"			WHEN " &_
"			(  " &_
"				(EVENT_DATE IS NOT NULL) AND  " &_
"				(DATEDIFF(DAY, CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103), DATEADD(DAY, 0, EVENT_DATE)) < 0 ) " &_
"			) " &_
"			THEN 'NO' " &_
"			ELSE 'YES' END " &_
",EVENT.*, L.FIRSTNAME+SPACE(1)+L.LASTNAME AS SUBMITTEDBY, L.EMAIL, P.PROVIDERID, P.PROVIDERNAME, CASE WHEN L.MOBILE IS NOT NULL THEN L.MOBILE ELSE L.TELEPHONE END AS CONTACTNUMBER " &_
"FROM EVENT " &_
"INNER JOIN DBO.TBL_MD_LOGIN L ON EVENT_SUBMIT_BY = L.LOGINID " &_
"INNER JOIN DBO.TBL_MD_PROVIDER P ON L.PROVIDERID = P.PROVIDERID " &_
"WHERE EVENT_ID=" & Request.QueryString("ID") & ";"

Set rs = Conn.Execute(strEventQuery)
If Not rs.eof Then
    EntryActive = rs("EntryActive")
End If

If (rs("ProviderID") = "" Or IsNull(rs("ProviderID"))) Then
    strProvider = ""
Else
    strProvider = rs("ProviderName")
End If
If (rs("CONTACTNUMBER") = "" Or IsNull(rs("CONTACTNUMBER"))) Then
    STRcONTACT = ""
Else
    STRcONTACT = "onmouseover=""Tip('"&rs("CONTACTNUMBER")&"', TITLE, 'Adviser Contact Number', TITLEBGCOLOR, '#cc0000', BORDERCOLOR, '#cc0000', SHADOW, true, SHADOWWIDTH, 7)"" onmouseout=""UnTip()"""
End If  
If (rs("EMAIL") = "" Or IsNull(rs("EMAIL"))) Then
    strLink = rs("CREATEDBYNAME")
Else
    strLink = "<a href=""mailto:"& rs("EMAIL")& """ " & STRcONTACT & ">" & rs("SUBMITTEDBY") & "</a> - " & strProvider 
End If


'Create recordset to grab the event type for this particular event
strEventTypeQuery = "SELECT event_type_desc FROM event_type WHERE event_type_id=" & rs("event_type_id")
Set objEventTypeRs = Conn.Execute(strEventTypeQuery)

strURL = Trim(rs("event_url"))
length = len(strURL)

Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function
	

'If the URL field is not blank, grab the prefix of the url and match it with the most common valid url prefixes
If strURL <> "" Then
	For iCounter = 1 to length
		char = mid(strURL, iCounter, 1)
		If char <> ":" Then
			strPrefix = strPrefix + char
			isPrefix = false
		Else
			isPrefix = true
			Exit For
		End If
	Next
	strPrefix = Trim(LCASE(strPrefix))

	'If there is a prefix, check to see if it is a valid prefix
	If isPrefix = true AND strPrefix <> "http" AND strPrefix <> "ftp" AND strPrefix <> "telnet" AND strPrefix <> "file" Then
		strURL = "invalid link"
	End If
Else
	strURL = "invalid link"
End If

If Not isNull(rs("event_submit_to")) Then

Dim OriginalValues
Dim SplitValues 
Dim counter
Dim strNameString

'strNameString = ""

OriginalValues = rs("event_submit_to")

SplitValues = Split(OriginalValues,",")

Set objConn3 = Server.CreateObject("ADODB.Connection")
objConn3.Open Application("MSSQLConnectionString")

For counter = 0 to Ubound(SplitValues) 
	    
	compvalue = Split(SplitValues(counter),"a")
	
	strQuery3 = "SELECT FIRSTNAME, LASTNAME, LOGINID FROM TBL_MD_LOGIN WHERE LOGINID LIKE '%"& compvalue(1) & "%'"
	Set objRs3 = objConn3.Execute(strQuery3)
   	tempstring = Pcase(objRs3(0) & " " & objRs3(1))
	If counter = Ubound(SplitValues) then
		strNameString = strNameString & " " &  tempstring & "."
	Else
		strNameString = strNameString & " " &  tempstring & ", "
	End if		
	objRs3.close
	
Next 
objConn3.close

set objConn3 = nothing
set objRs3 = nothing
End if
%>
<%
If Request.Form("EntryActive") <> "" then

    If Request.Form("EntryActive") = "0" then

        Response.Write "In Active"
    
    End If
    
End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	/*border-top:1px dotted #cc0000;*/
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	/*background:gray;
	color:#ffffff;
	cursor:pointer;*/
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.higlight
{
    background-color:yellow
}


</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">

function setStatus() 
{
	document.getElementById("theStatus").innerHTML = "<font color='red'>Loading...</font>";
}

function RemoveEntry(evt, str)
{

    var theResult = window.confirm(str)
    if (theResult == true)
    {
        document.getElementById("EntryActive").value = 0;
        document.thisForm.action = "editevent4.asp";
        document.thisForm.submit();
    }
    
    var e=(evt)?evt:window.event;
    if (window.event) {
        e.cancelBubble=true;
    } else {
        //e.preventDefault();
        e.stopPropagation();
    }
}

</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<script type="text/javascript" src="/ClientScripting/wz_tooltip.js"></script>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%> : <%If strURL = "invalid link" OR strURL = "N/A" Then%>
										<%=rs("event_title")%>
									<%Else%>
										<%If isPrefix = true Then%>
											<a href="<%=Trim(rs("event_url"))%>" target="_blank"><%=rs("event_title")%></a>
										<%Else%>
											<a href="http://<%=Trim(rs("event_url"))%>" target="_blank"><%=rs("event_title")%></a>
										<%End If%>
									<%End If%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<form name="thisForm" action="editevent2.asp" method="post" target="_self">
				<table border="0" cellpadding="2" cellspacing="0">
					<tr>
					    <td align="right" nowrap="nowrap">
					        <strong>Date:</strong>
					    </td>
						<td><%=FormatDateTime(rs("event_date"),1)%></td>
					</tr>
					<tr>
						<td align="right" nowrap="nowrap">
						    <strong>Start Time:</strong>
						</td>
						<td><%=rs("event_start_time")%></td>
					</tr>
					<tr>
						<td align="right" nowrap="nowrap">
						    <strong>End Time:</strong>
						</td>
						<td><%=rs("event_end_time")%></td>
					</tr>
					<tr>
						<td align="right" nowrap="nowrap">
						    <strong>Type:</strong>
						</td>
						<td>
						<% If (NOT objEventTypeRs.EOF) Then
							rw objEventTypeRs("event_type_desc")
						   End If
						%>
					    </td>
					</tr>
					<tr>
						<td align="right" nowrap="nowrap" valign="top">
						    <strong>Details:</strong>
						</td>
						<td>
						    <%=rs("event_notes")%>
						</td>
					</tr>
					<tr>
						<td align="right" nowrap="nowrap" valign="top">
						    <strong>Created By:</strong>
						</td>
						<td>						    
						    <%=strLink %>
						</td>
					</tr>
					<%if isnull(rs("event_submit_to")) Then %>
					<tr>
					    <td align="right" nowrap="nowrap">
					        <strong>Submitted To:</strong>
					    </td>
						<td>
						    ALL
						</td>
					</tr>
					<%Else%>
					<tr>
					    <td nowrap="nowrap">
					        <strong>Submitted To:</strong>
					    </td>
						<td>
						    <%=strNameString%>
						</td>
					</tr>
					<%End If%>
					<tr>
						<td>
						    <input type="hidden" name="Record2Edit" value="<%=rs("EVENT_ID")%>" />
						    <input type="hidden" name="EntryActive" id="EntryActive" value="<%=EntryActive%>" />
						</td>
						<td>
						    <input type="submit" name="BtnEdit" value="Edit" <% If ((rs("DEL") = "NO") OR (rs("EVENT_SUBMIT_BY") <> CLng(LOGINID))) Then Response.Write " Disabled=""Disabled"" " End If  %> />
						    <input type="button" name="BtnRemove" value="Remove" <% If ((rs("DEL") = "NO") OR (rs("EVENT_SUBMIT_BY") <> CLng(LOGINID))) Then Response.Write " Disabled=""Disabled"" " Else Response.Write " onclick=""RemoveEntry(event,'Do you wish to remove this Calendar Entry?')"" " End If  %> />
						</td>
					</tr>
				</table>
			</form>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="serverFrame" id="serverFrame"                   width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 	
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>
<%
objEventTypeRs.Close
rs.close
Conn.Close
%>