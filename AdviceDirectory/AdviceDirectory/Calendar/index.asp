﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))
    Dim PROVIDERID
        PROVIDERID = nulltest(Session("svPROVIDERID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/Calendar|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	'Call PageContent(GP_curPath)
	Call PageContent("Calendar")
%>
<%
'First initalize all variables
Dim dToday,iMonth,strMonth,iYear,dFirstDay,iFirstDay,strFirstDay,iLastDay,iCurDate,iDayCounter,iWeekCounter,strEventQuery

'Set the correct date
If (Session("CurViewMonth") = 0) Then
	dToday = Date
	iMonth = Month(dToday)
	iYear = Year(dToday)
	Session("CurViewMonth") = iMonth
	Session("CurViewYear") = iYear
Else
	iMonth = Session("CurViewMonth")
	iYear = Session("CurViewYear")
	'Let's figure out what the user wants to do
	Select Case Request.Form("yearmove")
		Case "previous"
			If(iYear = 1977) Then
				iYear = 1977
			Else
				iYear = iYear - 1
			End If
		Case "next"
			If(iYear = 2050) Then
				iYear = 2050
			Else
				iYear = iYear + 1
			End If
	End Select
	Select Case Request.Form("move")
		Case "previous"
			If(iMonth = 1) Then
				iMonth = 12
				iYear = iYear - 1
			Else
				iMonth = iMonth -1
			End if
		Case "next"
			If(iMonth = 12) Then
				iMonth = 1
				iYear = iYear + 1
			Else
				iMonth = iMonth + 1
			End if
	End Select
	Session("CurViewMonth") = iMonth
	Session("CurViewYear") = iYear
End if

Select Case Application("Cal_Date_Format")
	   Case 1
	   		strFirstDay = CStr(iMonth) & "/1/" & CStr(iYear)	   
	   Case 2
	   		strFirstDay = "1/" & CStr(iMonth) & "/" & CStr(iYear)
End Select
			
dFirstDay = CDate(strFirstDay)
iFirstDay = Weekday(dFirstDay)
'Set the information for the current month
Select Case iMonth
	Case 1
		strMonth = "January"
		iLastDay = 31
	Case 2
		strMonth = "February"
		If( (iYear Mod 4) = 0) Then
			iLastDay  = 29
		Else
			iLastDay = 28
		End If
	Case 3
		strMonth = "March"
		iLastDay = 31
	Case 4
		strMonth = "April"
		iLastDay = 30
	Case 5
		strMonth = "May"
		iLastDay = 31
	Case 6
		strMonth = "June"
		iLastDay = 30
	Case 7
		strMonth = "July"
		iLastDay = 31
	Case 8
		strMonth = "August"
		iLastDay = 31
	Case 9
		strMonth = "September"
		iLastDay = 30
	Case 10
		strMonth = "October"
		iLastDay = 31
	Case 11
		strMonth = "November"
		iLastDay = 30
	Case 12
		strMonth = "December"
		iLastDay = 31
End Select

iCurDate = 1

Set Conn = Server.CreateObject("ADODB.Connection")
'Decide what data source to use
Conn.Open Application("MSSQLConnectionString")
'strEventQuery = "SELECT event_id, event_title, event_start_time, event_end_time FROM EVENT WHERE (event_submit_to IS NULL OR  event_submit_to LIKE '%a" & Session("svUserID") & "a%') and event_date= '"
'strPersonalEventQuery = "SELECT event_id, event_title, event_start_time, event_end_time FROM EVENT WHERE event_submit_to LIKE '%a" & Session("svUserID") & "a%' and event_date= '"
'strEventQuery = "SELECT DISTINCT(e.EVENT_ID), ET.EVENT_TYPE_COLOUR, e.EVENT_TITLE, e.EVENT_START_TIME, e.EVENT_END_TIME " &_
'                "FROM DBO.EVENT E " &_
'                "INNER JOIN EVENT_TYPE ET ON ET.EVENT_TYPE_ID = E.EVENT_TYPE_ID " &_
'                "LEFT JOIN DBO.CONVERTCOMMA_TO_MANY_TO_ONE() CE ON E.EVENT_ID = CE.EVENT_ID " &_
'                "WHERE EVENT_ACTIVE = 1 AND (ce.EVENT_SUBMIT_TO IS NULL OR CE.EVENT_SUBMIT_TO = " & LOGINID & " OR E.EVENT_SUBMIT_BY = " & LOGINID & ") AND EVENT_DATE = '"
strEventQuery = "EXEC CRMS_CALENDAR_ENTRIES " & LOGINID & "," & PROVIDERID & ","
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	text-decoration:none;
}

table a:link {}

table a:visited {}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	/*border-top:1px dotted #cc0000;*/
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	/*background:gray;
	color:#ffffff;
	cursor:pointer;*/
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.higlight
{
background-color:yellow
}

td a {color: <%=Application("CalLinkColor")%> !important;	text-decoration: none !important;}
td a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
td a:hover {color: <%=Application("CalHoverColor")%> !important;	text-decoration: none !important;}

span.calTxt a {color: #cc0000 !important;	text-decoration: none !important;}
span.calTxt a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
span.calTxt a:hover {color: <%=Application("CalHoverColor")%> !important; text-decoration: none !important;}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">

function setStatus() 
{
	document.getElementById("theStatus").innerHTML = "<font color='red'>Loading...</font>";
}

</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
		    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" width="50%">
                                    <form name="thisForm" action="ServerMonthly.asp" method="POST" target="serverFrame">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr> 
                                                <td>
                                                  <input name="BtnFirst" type="submit" value="&lt;&lt;" title="Previous Year" onclick="JavaScript:thisForm.yearmove.value='previous';setStatus()" />
                                                </td>
                                                <td>
                                                  <input name="BtnPrevious" type="submit" value="&lt;&lt;" title="Previous Month" onclick="JavaScript:thisForm.move.value='previous';setStatus()" />
                                                </td>
                                                <td align="center" nowrap="nowrap" valign="middle">
                                                    <div id="monthName" style="font-weight:bold; color:#cc0000"><%=strMonth%>, <%=iYear%></div>
                                                </td>
                                                <td>
                                                    <input name="BtnNext" type="submit" value="&gt;&gt;" title="Next Month" onclick="JavaScript:thisForm.move.value='next';setStatus()" />
                                               </td>
                                               <td>
                                                  <input name="BtnLast" type="submit" value="&gt;&gt;" title="Next Year" onclick="JavaScript:thisForm.yearmove.value='next';setStatus()" />
                                               </td>
                                               <td><div id="theStatus" style="font-weight:bold"></div></td>
                                            </tr>
                                        </table>
                                        <input type="hidden" name="move" id="move" value="" />
                                        <input type="hidden" name="yearmove" id="yearmove" value="" />
                                    </form>
                                </td>
                                <td width="50%">                                
                                    <form method="post" id="Form1" name="Form1" action="searchresult.asp" onsubmit="return checkMyForm(this);">
                                        <div id="nsform" style="width:99%; border:solid 1px #cc0000;">
                                            <div id="errordiv" style="padding:5px;"><%=passcheck_text%></div>
                                            <div style="width:100%;">
                                                <fieldset>
	                                                <legend>Network Calendar Search</legend>
		                                            <div id="Frm" style="width:100%; float:left; padding-bottom:1em">
                                                        <div class="row">
                                                            <span class="label">
                                                                <label for="txt_SEARCH" accesskey="F">
                                                                    <span style="text-decoration:underline; cursor:pointer">S</span>earch :
                                                                </label>
                                                            </span>
                                                            <span class="formw">
                                                                <input class="border-white"
                                                                    onblur="this.className='border-white'"
                                                                    onfocus="this.className='border-red'"
                                                                    type="text"
                                                                    size="20"
                                                                    name="txt_SEARCH"
                                                                    id="txt_SEARCH"
                                                                    maxlength="100"
                                                                    value="<%=SEARCHSTRING%>"
                                                                    tabindex="1"
                                                                    required="Y" 
                                                                    validate="/([a-zA-Z0-9,\s]{1,100})$/"
                                                                    validateMsg="The field search should contain a keyword greater than 3 characters in length." />
                                                                <img src="/js/FVS.gif" name="img_SEARCH" id="img_SEARCH" width="15px" height="15px" alt="" />
                                                                <input class="submit" type="submit" name="Search" id="Search" value="Search" title="Submit Button : Search News" style="width:auto; margin-top:1em"/>
                                                            </span>
                                                        </div>
                                                    </div>
     	                                        </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="MainDiv">
                            <table border="1" bordercolor="#cc0000" cellpadding="2" cellspacing="0" width="99%" height="89%" bgcolor="<%=Application("CalTableBGColor")%>" style='border-collapse:collapse'>
                                <tr bgcolor="#C0C0C0" class="iagTitleWhite">
                                  <td width=" 9%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Sun</font></td>
                                  <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Monday</font></td>
                                  <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Tuesday</font></td>
                                  <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Wednesday</font></td>
                                  <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Thursday</font></td>
                                  <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Friday</font></td>
                                  <td width=" 9%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Sat</font></td>
                                </tr>
                                            <!-- Now let's build the calendar -->
                                            <%
iDayCounter = 1
iWeekCounter = 1

'This code has been changed becuase the WeekDay function only seems to support the american format.
Select Case Application("Cal_Date_Format")
	   Case 1
	   		iFirstDay = WeekDay(iMonth & "/" & iDayCounter & "/" & iYear)
	   Case 2
	   		iFirstDay = WeekDay(iDayCounter & "/" & iMonth & "/" & iYear)
End Select

Do While iDayCounter <= iLastDay 
	Do 
		rw("			<tr>" & vbCrLf)
		For x = 1 To 7
			rw("				<td valign=""top"">")
			Select Case Application("Cal_Date_Format")
				   Case 1
				   		dCurrentDate = iMonth & "/" & iDayCounter & "/" & iYear
				   Case 2
				   	   	dCurrentDate = iDayCounter & "/" & iMonth & "/" & iYear
			End Select


			If iWeekCounter = 1 Then
				If x >= iFirstDay Then
					rw("<a href=viewday.asp?thedate=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & ">")
					rw("<font class='calText'>")
					rw("<strong>" & iDayCounter & "</strong><br/>")
					rw("</font>")
					rw("</a>")
					rw("<span style=""valign:bottom; float:right; padding:5px;"" class=""calTxt""><a title=""Add Calendar Item"" href=""addevent.asp?d=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & """>[+]</a></span>")

					' check for data on this date
					' Set rs = Conn.Execute(strEventQuery + FormatDateTime(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear),1) + "' ORDER BY EVENT_START_TIME, EVENT_TITLE;")
					' Set rs = Conn.Execute(strEventQuery + "'" + FormatMyDate(CStr(Cdate(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear)))) + "'")
					
					Set rs = Conn.Execute(strEventQuery + "'" + CStr(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear)) + "'")
					
					If (rs.BOF AND rs.EOF) Then
						rw "<p>&nbsp;"
					Else
						Do While (NOT(rs.EOF))
							rw("<span><a title=""View Calendar Item"" href=viewevent.asp?id=" & rs("EVENT_ID") & "><font style=""COLOR:"& rs("EVENT_TYPE_COLOUR") &""">" & Left(rs("Event_Start_time"),5) & " - " & rs("EVENT_TITLE") & "</font></a></span><br/>")
							rs.MoveNext
						Loop
					End If
					iDayCounter = iDayCounter + 1
				Else
					rw("<p>&nbsp;</p>")
				End If
			Else
				If iDayCounter <= iLastDay Then
					rw("<a href=viewday.asp?thedate=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & ">")
					rw("<font class='calText'>")
					rw("<strong>" & iDayCounter & "</strong>")
					rw("</font>")
					rw("</a><br/>")
					rw("<span style=""valign:bottom; float:right; padding:5px;"" class=""calTxt""><a title=""Add Calendar Item"" href=""addevent.asp?d=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & """>[+]</a></span>")
					
					' check for data on this date
					' Set rs = Conn.Execute(strEventQuery + FormatDateTime(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear),1) + "' ORDER BY EVENT_START_TIME, EVENT_TITLE;")
					' Set rs = Conn.Execute(strEventQuery + "'" + FormatMyDate(CStr(Cdate(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear)))) + "'")
					
					Set rs = Conn.Execute(strEventQuery + "'" + CStr(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear)) + "'")
					
					If (rs.BOF AND rs.EOF) Then
						rw "<p>&nbsp;"
					Else
						Do While (NOT(rs.EOF))
							rw("<span><a title=""View Calendar Item"" href=viewevent.asp?id=" & rs("EVENT_ID") & "><font style=""COLOR:"& rs("EVENT_TYPE_COLOUR") &""">"  & Left(rs("Event_Start_time"),5) & " - " & rs("EVENT_TITLE") & "</font></a></span><br/>")
							rs.MoveNext
						Loop
					End If
					iDayCounter = iDayCounter + 1
				Else
					rw("<p>&nbsp;</p>")
				End If
			End If
			
			rw("</td>" & vbCrLf)
		Next
		rw("			</tr>" & vbCrLf)
		iWeekCounter = iWeekCounter + 1
		Exit Do
	Loop
Loop
%>
                                          </table>
                                          </div>
                                          <br />
                                          <div id="Div1" style="width:auto; border:solid 1px #cc0000; padding:5px">
                                          <b>Key</b><br />
                                          <%
                    Set rs = Conn.Execute("SELECT * FROM EVENT_TYPE ORDER BY EVENT_TYPE_ID")
					If (rs.BOF AND rs.EOF) Then
						Response.Write ""
					Else
						Do While (NOT(rs.EOF))
							rw("<span style=""color:"& rs("EVENT_TYPE_COLOUR") &""">" & rs("EVENT_TYPE_DESC") & "</span><br/>")
							rs.MoveNext
						Loop
					End If
                         %>                  </div>
                                          </td>
                                      </tr>
                                    </table>

	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="serverFrame" id="serverFrame"                   width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 	
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>