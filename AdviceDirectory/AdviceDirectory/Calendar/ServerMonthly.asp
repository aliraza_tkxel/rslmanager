<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))
    Dim PROVIDERID
        PROVIDERID = nulltest(Session("svPROVIDERID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Response.write "The server " & Request.form("yearmove")
'First initalize all variables
Dim dToday,iMonth,strMonth,iYear,dFirstDay,iFirstDay,strFirstDay,iLastDay,iCurDate,iDayCounter,iWeekCounter,strEventQuery

'Set the correct date
If (Session("CurViewMonth") = 0) Then
	dToday = Date
	iMonth = Month(dToday)
	iYear = Year(dToday)
	Session("CurViewMonth") = iMonth
	Session("CurViewYear") = iYear
Else
	iMonth = Session("CurViewMonth")
	iYear = Session("CurViewYear")
	'Let's figure out what the user wants to do
	Select Case Request.Form("yearmove")
		Case "previous"
			If(iYear = 1977) Then
				iYear = 1977
			Else
				iYear = iYear - 1
			End If
		Case "next"
			If(iYear = 2050) Then
				iYear = 2050
			Else
				iYear = iYear + 1
			End If
	End Select
	Select Case Request.Form("move")
		Case "previous"
			If(iMonth = 1) Then
				iMonth = 12
				iYear = iYear - 1
			Else
				iMonth = iMonth -1
			End if
		Case "next"
			If(iMonth = 12) Then
				iMonth = 1
				iYear = iYear + 1
			Else
				iMonth = iMonth + 1
			End if
	End Select
	Session("CurViewMonth") = iMonth
	Session("CurViewYear") = iYear
Response.write iYear
Response.write iMonth
End if

Select Case Application("Cal_Date_Format")
	   Case 1
	   		strFirstDay = CStr(iMonth) & "/1/" & CStr(iYear)	   
	   Case 2
	   		strFirstDay = "1/" & CStr(iMonth) & "/" & CStr(iYear)
End Select
			
dFirstDay = CDate(strFirstDay)
iFirstDay = Weekday(dFirstDay)
'Set the information for the current month
Select Case iMonth
	Case 1
		strMonth = "January"
		iLastDay = 31
	Case 2
		strMonth = "February"
		If( (iYear Mod 4) = 0) Then
			iLastDay  = 29
		Else
			iLastDay = 28
		End If
	Case 3
		strMonth = "March"
		iLastDay = 31
	Case 4
		strMonth = "April"
		iLastDay = 30
	Case 5
		strMonth = "May"
		iLastDay = 31
	Case 6
		strMonth = "June"
		iLastDay = 30
	Case 7
		strMonth = "July"
		iLastDay = 31
	Case 8
		strMonth = "August"
		iLastDay = 31
	Case 9
		strMonth = "September"
		iLastDay = 30
	Case 10
		strMonth = "October"
		iLastDay = 31
	Case 11
		strMonth = "November"
		iLastDay = 30
	Case 12
		strMonth = "December"
		iLastDay = 31
End Select

iCurDate = 1
	
Set Conn = Server.CreateObject("ADODB.Connection")
'Decide what data source to use
Conn.Open Application("MSSQLConnectionString")
'strEventQuery = "SELECT event_id, event_title, event_start_time FROM event WHERE EVENT_ACTIVE = 1 AND (event_submit_to IS NULL OR  event_submit_to LIKE '%a" & Session("svUserID") & "a%') and event_date= '"
'strEventQuery = "SELECT DISTINCT(e.EVENT_ID), ET.EVENT_TYPE_COLOUR, e.EVENT_TITLE, e.EVENT_START_TIME, e.EVENT_END_TIME " &_
'                "FROM DBO.EVENT E " &_
'               "INNER JOIN EVENT_TYPE ET ON ET.EVENT_TYPE_ID = E.EVENT_TYPE_ID " &_
'                "LEFT JOIN DBO.CONVERTCOMMA_TO_MANY_TO_ONE() CE ON E.EVENT_ID = CE.EVENT_ID " &_
'                "WHERE EVENT_ACTIVE = 1 AND (ce.EVENT_SUBMIT_TO IS NULL OR CE.EVENT_SUBMIT_TO = " & LOGINID & " OR E.EVENT_SUBMIT_BY = " & LOGINID & ") AND EVENT_DATE = '"
strEventQuery = "EXEC CRMS_CALENDAR_ENTRIES " & LOGINID & "," & PROVIDERID & ","
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<script language="javascript" type="text/javascript" defer="defer">
function SendData()
{
    parent.document.getElementById("MainDiv").innerHTML = document.getElementById("CalendarData").innerHTML
    parent.document.getElementById("monthName").innerHTML = "<%=strMonth%>, <%=iYear%>";
	parent.document.getElementById("move").value = "";
	parent.document.getElementById("yearmove").value = "";
	parent.document.getElementById("theStatus").innerHTML = "Ready";
}
</script>
</head>
<body onload="SendData()">
<div id="CalendarData">
                                          <table border="1" bordercolor="#cc0000" cellpadding="2" cellspacing="0" width="99%" height="89%" bgcolor="<%=Application("CalTableBGColor")%>" style="border-collapse:collapse">
                                            <tr bgcolor="#C0C0C0"> 
                                              <td width=" 9%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Sun</font></td>
                                              <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Monday</font></td>
                                              <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Tuesday</font></td>
                                              <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Wednesday</font></td>
                                              <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Thursday</font></td>
                                              <td width="16%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Friday</font></td>
                                              <td width=" 9%" height="25" align="center" bgcolor="<%=Application("CalHeaderBGColor")%>"><font class="calText" color="<%=Application("CalHeaderTextColor")%>">Sat</font></td>
                                            </tr>
                                            <!-- Now let's build the calendar -->
                                            <%
iDayCounter = 1
iWeekCounter = 1

'This code has been changed becuase the WeekDay function only seems to support the american format.
Select Case Application("Cal_Date_Format")
	   Case 1
	   		iFirstDay = WeekDay(iMonth & "/" & iDayCounter & "/" & iYear)	   
	   Case 2
	   		iFirstDay = WeekDay(iDayCounter & "/" & iMonth & "/" & iYear)
End Select

Do While iDayCounter <= iLastDay 
	Do 
		rw("			<tr>" & vbCrLf)
		For x = 1 To 7
			rw("				<td valign=""top"">")
			Select Case Application("Cal_Date_Format") 
				   Case 1
				   		dCurrentDate = iMonth & "/" & iDayCounter & "/" & iYear
				   Case 2
				   	   	dCurrentDate = iDayCounter & "/" & iMonth & "/" & iYear
			End Select										
								  
				   
			If iWeekCounter = 1 Then
				If x >= iFirstDay Then
					rw("<a href=viewday.asp?thedate=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & ">")
					rw("<font class='calText'>")
					rw("<strong>" & iDayCounter & "</strong><br/>")
					rw("</font>")
					rw("</a>")
					rw("<span style=""valign:bottom; float:right; padding:5px;"" class=""calTxt""><a title=""Add Calendar Item"" href=""addevent.asp?d=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & """>[+]</a></span>")
                    
                    ' check for data on this date
					' Set rs = Conn.Execute(strEventQuery + FormatDateTime(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear),1) + "' ORDER BY EVENT_START_TIME, EVENT_TITLE;")
					' Set rs = Conn.Execute(strEventQuery + "'" + CStr(Cdate(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear))) + "'")
					
					Set rs = Conn.Execute(strEventQuery + "'" + CStr(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear)) + "'")
					
					If (rs.BOF AND rs.EOF) Then
						rw "<p>&nbsp;"
					Else
						Do While (NOT(rs.EOF))
							rw("<span><a title=""View Calendar Item"" href=viewevent.asp?id=" & rs("EVENT_ID") & "><font style=""COLOR:"& rs("EVENT_TYPE_COLOUR") &""">" & Left(rs("Event_Start_time"),5) & " - " & rs("EVENT_TITLE") & "</font></a></span><br/>")
							rs.MoveNext
						Loop
					End If
					iDayCounter = iDayCounter + 1
				Else
					rw("<p>&nbsp;</p>")
				End If
			Else
				If iDayCounter <= iLastDay Then
					rw("<a href=viewday.asp?thedate=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & ">")
					rw("<font class='calText'>")
					rw("<strong>" & iDayCounter & "</strong>")
					rw("</font>")
					rw("</a><br/>")
					rw("<span style=""valign:bottom; float:right; padding:5px;"" class=""calTxt""><a title=""Add Calendar Item"" href=""addevent.asp?d=" & CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear) & """>[+]</a></span>")
                    
                    ' check for data on this date
					' Set rs = Conn.Execute(strEventQuery + FormatDateTime(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear),1) + "';")
					' Set rs = Conn.Execute(strEventQuery + "'" + CStr(Cdate(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear))) + "'")
					
					Set rs = Conn.Execute(strEventQuery + "'" + CStr(CStr(iDayCounter) + "/" + CStr(iMonth) + "/" + CStr(iYear)) + "'")

					If (rs.BOF AND rs.EOF) Then
						rw "<p>&nbsp;"
					Else
						Do While (NOT(rs.EOF))
							rw("<span><a title=""View Calendar Item"" href=viewevent.asp?id=" & rs("EVENT_ID") & "><font style=""COLOR:"& rs("EVENT_TYPE_COLOUR") &""">"  & Left(rs("Event_Start_time"),5) & " - " & rs("EVENT_TITLE") & "</font></a></span><br/>")
							rs.MoveNext
						Loop
					End If
					iDayCounter = iDayCounter + 1
				Else
					rw("<p>&nbsp;</p>")	
				End If
			End If
			
			rw("</td>" & vbCrLf)							
		Next
		rw("			</tr>" & vbCrLf)						
		iWeekCounter = iWeekCounter + 1
		Exit Do
	Loop
Loop
%>
</table>
	<% Conn.Close %>
</div>
</body>
</html>