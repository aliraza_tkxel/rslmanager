<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        LOGINID = LOGINID
    Else
        If NOT IsNumeric(LOGINID) Then
            LOGINID = -1
        End If
    End If
    
    Dim PROVIDERID
        PROVIDERID = nulltest(Request("sel_Provider"))

    If (NullNestTF(PROVIDERID) = True) Then
        PROVIDERID = PROVIDERID
    Else
        If NOT IsNumeric(PROVIDERID) Then
            PROVIDERID = -1
        End If
    End If
    
   'rw "exec CRMS_ADVISERS" & LOGINID & ", " & PROVIDERID

Call OpenDB()

    Set rsLang = server.CreateObject("ADODB.Recordset")
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "CRMS_ADVISERS"
      set .ActiveConnection=conn
      set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
      .parameters.append param
      set param = .createparameter("@PROVIDERID", adInteger, adParamInput, 0, PROVIDERID)
      .parameters.append param
      Set rsAdviser = .execute 
    end with

    Call BuildSelect_SP(strAdviser,"selListNames",rsAdviser,"Please Select",null,"width:auto",null,"onchange=""checkEndTime()""")
    rw strAdviser
Call CloseDB()
%>