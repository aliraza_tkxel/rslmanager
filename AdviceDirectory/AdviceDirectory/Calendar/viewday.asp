﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))
    Dim PROVIDERID
        PROVIDERID = nulltest(Session("svPROVIDERID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/Calendar|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	'Call PageContent(GP_curPath)
	Call PageContent("Calendar")
%>
<% 
if Request.QueryString("THEDATE") <> Session("CurViewDate") Then
	Session("CurViewDate") = Request.QueryString("THEDATE")
End If
Set Conn = Server.CreateObject("ADODB.Connection")
Conn.Open Application("MSSQLConnectionString")             
'strEventQuery = "SELECT e.EVENT_ID, e.EVENT_TITLE, e.EVENT_START_TIME, e.EVENT_END_TIME FROM DBO.EVENT E " &_
'                "LEFT JOIN DBO.CONVERTCOMMA_TO_MANY_TO_ONE() CE ON E.EVENT_ID = CE.EVENT_ID " &_
'                "WHERE EVENT_ACTIVE = 1 AND (ce.EVENT_SUBMIT_TO IS NULL OR CE.EVENT_SUBMIT_TO = " & LOGINID & " OR E.EVENT_SUBMIT_BY = " & LOGINID & ") AND EVENT_DATE = '" + FormatDateTime(Session("CurViewDate"),1) + "';"

'strEventQuery = "EXEC [CRMS_CALENDAR_ENTRIES] " & LOGINID & "," & PROVIDERID & ",'" & FormatDateTime(Session("CurViewDate"),1) & "'"

Datemmddyyyy=Session("CurViewDate")
Dateddmmyyyy=day(Datemmddyyyy)&"/"&month(Datemmddyyyy)&"/"&year(Datemmddyyyy)

strEventQuery = "EXEC [CRMS_CALENDAR_ENTRIES] " & LOGINID & "," & PROVIDERID & ",'" & Datemmddyyyy & "'"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	/*border-top:1px dotted #cc0000;*/
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	/*background:gray;
	color:#ffffff;
	cursor:pointer;*/
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.higlight
{
background-color:yellow
}

td a {color: <%=Application("CalLinkColor")%> !important;	text-decoration: none !important;}
td a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
td a:hover {color: <%=Application("CalHoverColor")%> !important;	text-decoration: none !important;}

span.calTxt a {color: #cc0000 !important;	text-decoration: none !important;}
span.calTxt a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
span.calTxt a:hover {color: <%=Application("CalHoverColor")%> !important; text-decoration: none !important;}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%> : Entries for <% =FormatDateTime(Session("CurViewDate"),1) %></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
                        <table border="0" cellspacing="0" cellpadding="2">
                            <%
						'check for data on this date
						Set rs = Conn.Execute(strEventQuery)
						If (rs.BOF AND rs.EOF) Then
                            %>
                            <tr>
                                <td colspan="3">                                   
                                        <p>
                                            <strong>Sorry... No entries for this date.</strong></p>                                   
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;</td>
                            </tr>
                            <%	Else %>
                            <tr>
                                <td>
                                    <b>Event Title</b></td>
                                <td width="20%">
                                    <b>Start Time</b></td>
                                <td width="20%">
                                   <b>End Time</b></td>
                            </tr>
                            <%
						Do While (NOT(rs.EOF))
                            %>
                            <tr>
                                <td>
                                    <a href="viewevent.asp?ID=<%=rs("event_id")%>"><strong><font color="#FF9933">
                                        <%=rs("event_title")%>
                                    </font></strong></a>
                                </td>
                                <td width="20%">                                    
                                        <%=rs("event_start_time")%>                                    
                                </td>
                                <td width="20%">                                    
                                        <%=rs("event_end_time")%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center" style='border-top: 1px solid black; line-height: 1px'>
                                    &nbsp;</td>
                            </tr>
                            <%
						rs.MoveNext
						Loop
                            %>      
                            <% End If %>
                        </table>                
    	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="serverFrame" id="serverFrame"                   width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 	
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>