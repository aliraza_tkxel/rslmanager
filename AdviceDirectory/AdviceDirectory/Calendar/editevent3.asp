﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/Calendar|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	'Call PageContent(GP_curPath)
	Call PageContent("Calendar")
%>
<%
'Open a connection.
Set Conn = Server.CreateObject("ADODB.Connection")
'Check to see which data source to use.
Conn.Open Application("MSSQLConnectionString")

'Create the recordset and open Event table
Set Rs = Server.CreateObject("ADODB.Recordset")
Rs.Open "SELECT * FROM event WHERE event_id = " & Request.Form("postid") & ";",Conn,,adLockOptimistic,adCmdText
	'Update the record to the event table
	Rs("event_date") = CDate(Request.Form("postdate"))
	If Request.Form("poststarttime") = "" Then
		Rs("event_start_time") = 0
	Else
		Rs("event_start_time") = Request.Form("poststarttime")
	End If
	If Request.Form("postendtime") = "" Then
		Rs("event_end_time") = 0
	Else
		Rs("event_end_time") = Request.Form("postendtime")
	End If
	Rs("event_title") = Request.Form("posttitle")
	If Request.Form("posturl") = "" Then
		Rs("event_url") = "N/A"
	Else
		Rs("event_url") = Request.Form("posturl")
	End If
	Rs("event_type_id") = Request.Form("posttype")
	If Request.Form("postnotes") = "" Then
		Rs("event_notes") = "N/A"
	Else
		Rs("event_notes") = Request.Form("postnotes")
	End If
	
	If Request.Form("tbListNames")  = "" Then
		Rs("event_submit_to") = NULL
	Else
	    Rs("event_submit_to") = Request.Form("tbListNames")
	End If
	
	Rs("event_submit_user") = Session("svLOGINID")
	Rs("EVENT_SUBMIT_BY") = Session("svLOGINID")
	Rs("event_submit_date") = FormatDateTime(Date(),2)
	Rs.Update
	'Close all open objects
	Rs.Close
	Conn.Close
	Set Rs = Nothing
	Set Conn = Nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	/*border-top:1px dotted #cc0000;*/
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	/*background:gray;
	color:#ffffff;
	cursor:pointer;*/
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.higlight
{
background-color:yellow
}

td a {color: <%=Application("CalLinkColor")%> !important;	text-decoration: none !important;}
td a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
td a:hover {color: <%=Application("CalHoverColor")%> !important;	text-decoration: none !important;}

span.calTxt a {color: #cc0000 !important;	text-decoration: none !important;}
span.calTxt a:active {color: <%=Application("CalVLinkColor")%> !important; text-decoration: none !important;}
span.calTxt a:hover {color: <%=Application("CalHoverColor")%> !important; text-decoration: none !important;}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en" onload="populatenames();">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%> : Edit calendar entry</h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
            <table border="0" cellspacing="5" cellpadding="5" width="100%" height="100%">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" width="80%">
                            <tr>
                                <td align="center">
                                    <strong>Thank You!</strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    Your entry has been successfully updated. Please <a href="/Calendar/"><strong><font color="#cc0000">return to the calendar</font></strong></a> to continue.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="serverFrame" id="serverFrame"                   width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 	
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="1" height="1" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>