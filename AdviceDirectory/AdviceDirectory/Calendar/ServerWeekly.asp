<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim iMonth,strMonth,iYear,dFirstDay,iFirstDay,strFirstDay,iLastDay,iCurDate,strEventQuery
If  IsEmpty(Session("CalCurrentDate")) Then
	'Get the current day, month and year
	Session("CalCurrentDate") = Date
Else
	' Let's figure out what the user wants to do
	Select Case Request.Form("move")
		Case "previous"
			Session("CalCurrentDate") = Session("CalCurrentDate") - 7
		Case "next"
			Session("CalCurrentDate") = Session("CalCurrentDate") + 7
	End Select
End If		
'Figure out the start day of the current week
Select Case WeekDay(Session("CalCurrentDate"),vbUseSystem)
	Case vbSunday
		dStartDate = Session("CalCurrentDate")
	Case vbMonday
		dStartDate = Session("CalCurrentDate") - 1		
	Case vbTuesday
		dStartDate = Session("CalCurrentDate") - 2		
	Case vbWednesday
		dStartDate = Session("CalCurrentDate") - 3		
	Case vbThursday
		dStartDate = Session("CalCurrentDate") - 4
	Case vbFriday
		dStartDate = Session("CalCurrentDate") - 5		
	Case vbSaturday
		dStartDate = Session("CalCurrentDate") - 6		
End Select

iMonth = Month(Session("CalCurrentDate"))
iYear = Year(Session("CalCurrentDate"))

Select Case iMonth
	Case 1
		strMonth = "January"
	Case 2
		strMonth = "February"
	Case 3
		strMonth = "March"
	Case 4
		strMonth = "April"
	Case 5
		strMonth = "May"
	Case 6
		strMonth = "June"
	Case 7
		strMonth = "July"
	Case 8
		strMonth = "August"
	Case 9
		strMonth = "September"
	Case 10
		strMonth = "October"
	Case 11
		strMonth = "November"
	Case 12
		strMonth = "December"
End Select

'Setup the data source
Set Conn = Server.CreateObject("ADODB.Connection")
Conn.Open Application("MSSQLConnectionString")
strEventQuery = "SELECT event_id, event_title, event_start_time FROM event WHERE event_date = '"
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<script language="javascript" type="text/javascript" defer="defer">
function SendData()
{
    parent.document.getElementById("weekData").innerHTML = document.getElementById("weeklyData").innerHTML
    parent.document.getElementById("weekName").innerHTML = "<%=strMonth%>, <%=iYear%><BR>Week of <%=dStartDate%>";
	parent.document.getElementById("move").value = "";
	parent.document.getElementById("theStatus").innerHTML = "Ready";
}
</script>
</head>
<body onload="sendData()">
<div id="weeklyData">
				<TABLE BORDER="1" BORDERCOLOR="#cc0000" CELLPADDING="2" CELLSPACING="0" WIDTH="99%" HEIGHT="93%" BGCOLOR="<%=Application("CalTableBGColor")%>" style="border-collapse:collapse">
					<TR BGCOLOR="#C0C0C0">
						<TD ALIGN="center" WIDTH="9%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Sun</B></FONT></TD>
						<TD ALIGN="center" WIDTH="16%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Monday</B></FONT></TD>
						<TD ALIGN="center" WIDTH="16%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Tuesday</B></FONT></TD>
						<TD ALIGN="center" WIDTH="16%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Wednesday</B></FONT></TD>
						<TD ALIGN="center" WIDTH="16%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Thursday</B></FONT></TD>
						<TD ALIGN="center" WIDTH="16%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Friday</B></FONT></TD>
						<TD ALIGN="center" WIDTH="9%" HEIGHT = "40" BGCOLOR="<%=Application("CalHeaderBGColor")%>"><FONT class="calText" COLOR="<%=Application("CalHeaderTextColor")%>"><B>Sat</B></FONT></TD>
					</TR>
					<TR>
						<%
							iDayCounter = -1
							Do While iDayCounter < 6
						%>
		    			<TD VALIGN="top">
							<FONT class="calText" COLOR="<%=Application("CalTextColor")%>">
							<A HREF="viewday.asp?THEDATE=<% =CStr(dStartDate + iDayCounter) %>"><B><%=CStr(Day(dStartDate + iDayCounter))%></B></A><BR>
							<%
								' check for data on this date
								set rs = Conn.Execute(strEventQuery + FormatDateTime(dStartDate +iDayCounter,1) + "' ORDER BY EVENT_START_TIME, EVENT_TITLE;")
								if (rs.BOF AND rs.EOF) then
									response.write "<P>&nbsp;"
								end if
								do while (NOT(rs.EOF))
							%>
							<FONT class="calTextSml" FACE="Verdana,Arial,Helv"><A HREF="viewevent.asp?ID=<%=rs("EVENT_ID")%>"><%=Left(rs("event_start_time"),5)%> - <%=rs("event_title")%></A></FONT><BR>
							<%
								rs.MoveNext
								loop
							%>
							</FONT>
						</TD>
					<%
						iDayCounter=iDayCounter+1
						Loop
					%>
					</TR>
				</TABLE>
	<% Conn.Close %>
</div>
</body>
</html>