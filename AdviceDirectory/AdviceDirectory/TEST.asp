﻿<%@LANGUAGE="VBSCRIPT"%> 
<% 
	' 
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
	
	if request.cookies("cssstyle") = "fullsite" or request.cookies("cssstyle") = "" then
		
		strCSS1 = "NewCSSMenu.css"
		strCSS2 = "Portal.css"
		'Access = "Text Only"
			
	else 
		
		strCSS1 = "TextOnlyCSS.css"
		strCSS2 = "amp.css"	
		'Access = "Full Site"	
	
	end if

Dim PAGE_TITLE
Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

Dim GP_curPath
	GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = true
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
	RW GP_curPath

	Dim Customerid
	Dim JS_RUN
	Dim MenuToggle 					' TOGGLE BETWEEN LOGIN/LOGOUT
	Dim SavePage
	
		Customerid = Session("Customerid")
		MenuToggle = "<a href=""login.asp"" title=""Log In"">Log In</a>"
	
	If IsNull(CustomerID) OR (CustomerID = "") Then
		JS_RUN = "NO"
	Else
		If IsNumeric(Customerid) Then
			JS_RUN = "YES"
			MenuToggle = "<a href=""logout.asp"" title=""Log Out"">Log Out</a>"
			SavePage = "<p class=""usrlink""><a href=""JavaScript://"" onclick=""SavePage('"&GP_curPath&"')"" title=""Save Page to My Virtual Centre"">Save</a></p>"
		Else
			JS_RUN = "NO"
		End if			
	End If
	
	Call PageContent(GP_curPath)
%>
<%
If Request("hid_form") <> "" Then

Dim txt_username	' FORM FIELD - USERNAME [LOG-IN.ASP]
Dim txt_password	' FORM FIELD - PASSWORD [LOG-IN.ASP]
Dim rsLogin			' RECORDSET
Dim passcheck 		' USER OK/NOT OK TO LOGIN	

	txt_username = "xyz"
	txt_password = "xyz"
	passcheck = ""
	
	If (Request.Form("txt_username") <> "") Then 
		If  isValidEmail(Request.Form("txt_username")) = True Then
			strUsername = Request.Form("txt_username")
		Else
			strUsername = isValidEmail(Request.Form("txt_username")) 
		End If
		
	End If  
		
	If (Request.Form("txt_password") <> "") Then 
		strPassword = RegExpTrimAll(Request.Form("txt_password"))   
	End If 	
	
	Call Login(strUsername, strPassword)
	
End If	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en">
<head>
<title><%=nextstep_name%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" id="NewCSSMenu" href="<% = strCSS1 %>" />
<link rel="stylesheet" type="text/css" id="Portal" href="<% = strCSS2  %>" />

<script type="text/javascript" src="ClientScripting/FormValidation_XHTML.js"></script>
<script type="text/javascript" src="ClientScripting/General_XHTML.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function CheckData(FormID){
switch (FormID) {   
		case 4 : document.getElementById('error').innerHTML=""; FormFields = new Array("txt_username|Username|EMAIL|Y", "txt_password|Password|TEXT|Y"); break;
		case 6 : FormFields = new Array("txt_SiteSearch|Site Search|TEXT|Y"); break; 
	}
	
	if (!checkForm()) return false;	
		switch (FormID) { 
		case 4 : if(document.getElementById('nsCSSform').elements['txt_password'].value.length >= 2) {return true; } else {alert('Your Password should be at least 6 characters long.'); document.getElementById('txt_password').select(); document.getElementById('txt_password').focus(); return false;} ; break;
		case 6 : if (document.getElementById('thisForm').elements['txt_SiteSearch'].value == 'Search here') {document.getElementById('thisForm').elements['txt_SiteSearch'].value = ""} 	
		}
}
//--><!]]>
</script>


<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);

//--><!]]>
</script>

<!-- #include virtual="/includes/GlobalJS.asp" -->

</head>
<body lang="en">
	<div id="nsform" style="border:solid 1px #cc0000; padding:1em">
        <form id="nsCSSform" method="post" action="/test.asp" onsubmit="return CheckData(4)">          
		  <div id="error" style="color:#FF0000; font-weight:bold; height:30px; vertical-align:middle"><%=passcheck%></div>
          	<fieldset>
			<legend>Login to this site</legend>
			<p>
			<label for="txt_username" accesskey="U"><span style="text-decoration:underline">U</span>sername:</label> 
			<input maxlength="100" size="40" type="text" name="txt_username" id="txt_username" value="<%=Request("txt_username")%>" />
			</p>
			<p>
			<label for="txt_password" accesskey="P"><span style="text-decoration:underline">P</span>assword:</label> 
			<input maxlength="10" size="36" type="password" name="txt_password" id="txt_password" value="<%=Request("txt_password")%>" />
			<input type="hidden" name="hid_form" id="hid_form" value="nsCSSform" />
			<input type="image" id="btn_login" src="../../images/im_GO.gif" title="Login Button : Submit Form" alt="Login Button : Submit Form"/>
			</p>
			<p>&nbsp;</p>
			<p><a href="/forgottenyourpassword.asp" title="Request the password you originally provided">Forgotten your password?</a><br>Request the password you originally provided. Your password will be sent to the email address that we hold.</p>
			</fieldset>			
        </form>
      </div>
</body>
</html>