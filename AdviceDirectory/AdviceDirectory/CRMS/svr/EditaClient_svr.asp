<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim isSaved
    isSaved = False

Dim CUSTOMER_REFERENCE
Dim FIRSTNAME
Dim SURNAME
Dim GENDER
Dim ETHNICITY
Dim DOB
Dim DISABLED
Dim BENEFITRECIPIENT
Dim LONEPARENT
Dim EMPLOYMENTSTATUS
Dim LENGTHOFUNEMPLOYMENT
Dim NINUMBER
Dim ADDRESSLINE1
Dim ADDRESSLINE2
Dim ADDRESSLINE3
Dim LAA
Dim POSTCODE
Dim TELEPHONE
Dim EMAIL
Dim MOBILE

Dim SECURITYQUESTION
Dim SECURITYANSWER
Dim USERNAME
Dim PASSWORD

Function Record_Save()

    
Dim LOGINID
    LOGINID = nulltest(Session("svLOGINID"))

If (NullNestTF(LOGINID) = True) Then
    isSaved = False
    rw isSaved
    Response.End()
    Exit Function
Else
    If NOT IsNumeric(LOGINID) Then
        isSaved = False
        rw isSaved
        Response.End()
        Exit Function
    End If
End If

FIRSTNAME = nulltest(Request("txt_FIRSTNAME2"))
SURNAME = nulltest(Request("txt_SURNAME2"))
GENDER = nulltest(Request("sel_GENDER"))
ETHNICITY = nulltest(Request("sel_ETHNICITY"))
DOB = nulltest(Request("txt_DOB2"))
DISABLED = nulltest(Request("sel_DISABILITY"))
BENEFITRECIPIENT = nulltest(Request("sel_BENEFITRECIPIENT"))
LONEPARENT = nulltest(Request("sel_LONEPARENT"))
EMPLOYMENTSTATUS = nulltest(Request("sel_EMPLOYMENTSTATUS"))
LENGTHOFUNEMPLOYMENT = nulltest(Request("sel_LENGTHOFUNEMPLOYMENT"))
NINUMBER = nulltest(Request("txt_NINUMBER"))
ADDRESSLINE1 = nulltest(Request("txt_ADDRESSLINE1"))
ADDRESSLINE2 = nulltest(Request("txt_ADDRESSLINE2"))
ADDRESSLINE3 = nulltest(Request("txt_ADDRESSLINE3"))
LAA = nulltest(Request("sel_LAA"))
POSTCODE = nulltest(Request("txt_POSTCODE2"))
TELEPHONE = nulltest(Request("txt_TELEPHONE"))
EMAIL = nulltest(Request("txt_EMAIL2"))
MOBILE = nulltest(Request("txt_MOBILE"))
SECURITYQUESTION = nulltest(Request("sel_SECURITYQUESTION"))
SECURITYANSWER = nulltest(Request("txt_SECURITYANSWER"))
USERNAME = nulltest(Request("txt_USERNAME"))
PASSWORD = nulltest(Request("txt_PASSWORD"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_CREATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, FIRSTNAME)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, SURNAME)
                .Parameters.Append .createparameter("@GENDER", adVarWChar, adParamInput, 1, GENDER)
                .Parameters.Append .createparameter("@ETHNICITY", adVarWChar, adParamInput, 2, ETHNICITY)
                .Parameters.Append .createparameter("@DOB", adDBTimeStamp, adParamInput, 0, DOB)
                .Parameters.Append .createparameter("@DISABLED", adInteger, adParamInput, 0, DISABLED)
                .Parameters.Append .createparameter("@BENEFITRECIPIENT", adInteger, adParamInput, 1, BENEFITRECIPIENT)
                .Parameters.Append .createparameter("@LONEPARENT", adInteger, adParamInput, 1, LONEPARENT)
                .Parameters.Append .createparameter("@EMPLOYMENTSTATUS", adVarWChar, adParamInput, 2, EMPLOYMENTSTATUS)
                .Parameters.Append .createparameter("@LENGTHOFUNEMPLOYMENT", adVarWChar, adParamInput, 2, LENGTHOFUNEMPLOYMENT)
                .Parameters.Append .createparameter("@NINUMBER", adVarWChar, adParamInput, 9, NINUMBER)
                .Parameters.Append .createparameter("@ADDRESSLINE1", adVarWChar, adParamInput, 100, ADDRESSLINE1)
                .Parameters.Append .createparameter("@ADDRESSLINE2", adVarWChar, adParamInput, 50, ADDRESSLINE2)
                .Parameters.Append .createparameter("@ADDRESSLINE3", adVarWChar, adParamInput, 50, ADDRESSLINE3)
                .Parameters.Append .createparameter("@LAA", adInteger, adParamInput, 0, LAA)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 8, POSTCODE)
                .Parameters.Append .createparameter("@TELEPHONE", adVarWChar, adParamInput, 20, TELEPHONE)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, EMAIL)
                .Parameters.Append .createparameter("@MOBILE", adVarWChar, adParamInput, 20, MOBILE)                
                .Parameters.Append .createparameter("@SECURITYQUESTION", adInteger, adParamInput, 0, SECURITYQUESTION)
                .Parameters.Append .createparameter("@SECURITYANSWER", adVarWChar, adParamInput, 300, SECURITYANSWER)
                .Parameters.Append .createparameter("@USERNAME", adVarWChar, adParamInput, 250, USERNAME)
                .Parameters.Append .createparameter("@PASSWORD", adVarWChar, adParamInput, 50, PASSWORD)
                .Parameters.Append .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing

End Function

Function Record_Update()

    
Dim LOGINID
    LOGINID = nulltest(Session("svLOGINID"))

If (NullNestTF(LOGINID) = True) Then
    isSaved = False
    rw isSaved
    Response.End()
    Exit Function
Else
    If NOT IsNumeric(LOGINID) Then
        isSaved = False
        rw isSaved
        Response.End()
        Exit Function
    End If
End If

CUSTOMER_REFERENCE = nulltest(Request("hid_CUSTOMER_REFERENCE"))
FIRSTNAME = nulltest(Request("txt_FIRSTNAME2"))
SURNAME = nulltest(Request("txt_SURNAME2"))
GENDER = nulltest(Request("sel_GENDER"))
ETHNICITY = nulltest(Request("sel_ETHNICITY"))
DOB = nulltest(Request("txt_DOB2"))
DISABLED = nulltest(Request("sel_DISABILITY"))
BENEFITRECIPIENT = nulltest(Request("sel_BENEFITRECIPIENT"))
LONEPARENT = nulltest(Request("sel_LONEPARENT"))
EMPLOYMENTSTATUS = nulltest(Request("sel_EMPLOYMENTSTATUS"))
LENGTHOFUNEMPLOYMENT = nulltest(Request("sel_LENGTHOFUNEMPLOYMENT"))
NINUMBER = nulltest(Request("txt_NINUMBER"))
ADDRESSLINE1 = nulltest(Request("txt_ADDRESSLINE1"))
ADDRESSLINE2 = nulltest(Request("txt_ADDRESSLINE2"))
ADDRESSLINE3 = nulltest(Request("txt_ADDRESSLINE3"))
LAA = nulltest(Request("sel_LAA"))
POSTCODE = nulltest(Request("txt_POSTCODE2"))
TELEPHONE = nulltest(Request("txt_TELEPHONE"))
EMAIL = nulltest(Request("txt_EMAIL2"))
MOBILE = nulltest(Request("txt_MOBILE"))
SECURITYQUESTION = nulltest(Request("sel_SECURITYQUESTION"))
SECURITYANSWER = nulltest(Request("txt_SECURITYANSWER"))
USERNAME = nulltest(Request("txt_USERNAME"))
PASSWORD = nulltest(Request("txt_PASSWORD"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_UPDATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, FIRSTNAME)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, SURNAME)
                .Parameters.Append .createparameter("@GENDER", adVarWChar, adParamInput, 1, GENDER)
                .Parameters.Append .createparameter("@ETHNICITY", adVarWChar, adParamInput, 2, ETHNICITY)
                .Parameters.Append .createparameter("@DOB", adDBTimeStamp, adParamInput, 0, DOB)
                .Parameters.Append .createparameter("@DISABLED", adInteger, adParamInput, 0, DISABLED)
                .Parameters.Append .createparameter("@BENEFITRECIPIENT", adInteger, adParamInput, 1, BENEFITRECIPIENT)
                .Parameters.Append .createparameter("@LONEPARENT", adInteger, adParamInput, 1, LONEPARENT)
                .Parameters.Append .createparameter("@EMPLOYMENTSTATUS", adVarWChar, adParamInput, 2, EMPLOYMENTSTATUS)
                .Parameters.Append .createparameter("@LENGTHOFUNEMPLOYMENT", adVarWChar, adParamInput, 2, LENGTHOFUNEMPLOYMENT)
                .Parameters.Append .createparameter("@NINUMBER", adVarWChar, adParamInput, 9, NINUMBER)
                .Parameters.Append .createparameter("@ADDRESSLINE1", adVarWChar, adParamInput, 100, ADDRESSLINE1)
                .Parameters.Append .createparameter("@ADDRESSLINE2", adVarWChar, adParamInput, 50, ADDRESSLINE2)
                .Parameters.Append .createparameter("@ADDRESSLINE3", adVarWChar, adParamInput, 50, ADDRESSLINE3)
                .Parameters.Append .createparameter("@LAA", adInteger, adParamInput, 0, LAA)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 8, POSTCODE)
                .Parameters.Append .createparameter("@TELEPHONE", adVarWChar, adParamInput, 20, TELEPHONE)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, EMAIL)
                .Parameters.Append .createparameter("@MOBILE", adVarWChar, adParamInput, 20, MOBILE)
                .Parameters.Append .createparameter("@SECURITYQUESTION", adInteger, adParamInput, 0, SECURITYQUESTION)
                .Parameters.Append .createparameter("@SECURITYANSWER", adVarWChar, adParamInput, 300, SECURITYANSWER)
                .Parameters.Append .createparameter("@USERNAME", adVarWChar, adParamInput, 250, USERNAME)
                .Parameters.Append .createparameter("@PASSWORD", adVarWChar, adParamInput, 50, PASSWORD)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing

End Function

Dim action
    action = nulltest(Request("hid_action"))

If action = "new" Then
    Call Record_Save()
Else
    Call Record_Update()
End If

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<script language="javascript" type="text/javascript">

function respondToReadyState2()
{
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Error Saving Information. Please Login and try again.</div><div style='float:right;padding:10px;'><img src='/images/close.gif' onclick='HTTPgo()' style='cursor:pointer' alt='Close' /></div>";
    parent.document.getElementById("light_error").className = "er";
}

function respondToReadyState()
{
    parent.document.getElementById("FrmEr_Edit").innerHTML = "<div style='float:left; padding:10px;'>Saved</div><div style='float:right;padding:10px;'><img src='/images/close.gif' onclick='HTTPgo()' style='cursor:pointer' alt='Close' /></div>"
    parent.document.getElementById("FrmEr_Edit").className = "no_error"
}
    parent.document.getElementById("FrmEr_Edit").innerHTML = "<div style='float:left; padding:10px;'>Please wait. Saving...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif' alt='Loading' /></div>"
    parent.document.getElementById("FrmEr_Edit").className = "no_error"
    
    <% If isSaved = True Then %>
    setTimeout((function() {respondToReadyState()}), 1000);
    <% Else %>
    setTimeout((function() {respondToReadyState2()}), 1000);
    <% End If %>
</script>
</head>
<body>
</body>
</html>