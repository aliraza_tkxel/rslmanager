<%@LANGUAGE="VBSCRIPT" %>
<%
	'
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim isSaved
    isSaved = False

Dim CUSTOMER_REFERENCE
    CUSTOMER_REFERENCE = nulltest(Request("CID"))
    
Dim ORGANISATION_REFERENCE
Dim SITE_REFERENCE
Dim CONTACT_REFERENCE
Dim APPOINTMENTDATE
Dim TIME_REFERENCE
Dim APPOINTMENT_REFERENCE

    ORGANISATION_REFERENCE = nulltest(Request("hid_ORGANISATION_REFERENCE"))
    SITE_REFERENCE = nulltest(Request("hid_SITE_REFERENCE"))
    CONTACT_REFERENCE = nulltest(Request("sel_CONTACT"))
    APPOINTMENTDATE = nulltest(Request("txt_DATE"))
    TIME_REFERENCE = nulltest(Request("sel_TIME"))
    APPOINTMENT_REFERENCE = nulltest(Request("hid_APPOINTMENT_REFERENCE"))

Function Record_New()

If (NullNestTF(CUSTOMER_REFERENCE) = True) Then
    isSaved = False
    Exit Function
Else
    If NOT IsNumeric(CUSTOMER_REFERENCE) Then
        isSaved = False
        Exit Function
    End If
End If

'On Error Resume Next

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_APPOINTMENT_INSERT"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@ORGANISATIONID", adInteger, adParamInput, 0, ORGANISATION_REFERENCE)
                .Parameters.Append .createparameter("@SITEID", adInteger, adParamInput, 0, SITE_REFERENCE)
                .Parameters.Append .createparameter("@CONTACTID", adInteger, adParamInput, 0, CONTACT_REFERENCE)
                .Parameters.Append .createparameter("@APPOINTMENTDATE", adDBTimeStamp, adParamInput, 0, APPOINTMENTDATE)
                .Parameters.Append .createparameter("@TIMEID", adInteger, adParamInput, 0, TIME_REFERENCE)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@LOGINID", adInteger, adParamInput, 0, NULL)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing
    
End Function


Function Record_Update()

Dim CUSTOMER_REFERENCE
    CUSTOMER_REFERENCE = nulltest(Session("svCustomerID"))

If (NullNestTF(CUSTOMER_REFERENCE) = True) Then
    isSaved = False
    Exit Function
Else
    If NOT IsNumeric(CUSTOMER_REFERENCE) Then
        isSaved = False
        Exit Function
    End If
End If

'On Error Resume Next

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_APPOINTMENT_UPDATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@ORGANISATIONID", adInteger, adParamInput, 0, ORGANISATION_REFERENCE)
                .Parameters.Append .createparameter("@SITEID", adInteger, adParamInput, 0, SITE_REFERENCE)
                .Parameters.Append .createparameter("@CONTACTID", adInteger, adParamInput, 0, CONTACT_REFERENCE)
                .Parameters.Append .createparameter("@APPOINTMENTDATE", adDBTimeStamp, adParamInput, 0, APPOINTMENTDATE)
                .Parameters.Append .createparameter("@TIMEID", adInteger, adParamInput, 0, TIME_REFERENCE)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@LOGINID", adInteger, adParamInput, 0, NULL)
                .Parameters.Append .createparameter("@APPOINTMENTID", adInteger, adParamInput, 0, APPOINTMENT_REFERENCE)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing

End Function

Dim action
    action = nulltest(Request("hid_action"))

If action = "new" Then
    Call Record_New()
Else
    Call Record_Update()
End If

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<script language="javascript" type="text/javascript">

function respondToReadyState2()
{
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Error Saving Information. Please Login and try again.</div><div style='float:right;padding:10px;'><img src='/images/close.gif' onclick='Close()' style='cursor:pointer' alt='Close' /></div>";
    parent.document.getElementById("light_error").className = "er";
}

function respondToReadyState()
{
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Saved</div><div style='float:right;padding:10px;'><img src='/images/close.gif' onclick='Close()' style='cursor:pointer' alt='Close' /></div>";
    parent.document.getElementById("light_error").className = "no_error";    
}
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Please wait. Saving...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif' alt='Loading' /></div>";
    parent.document.getElementById("light_error").className = "no_error";
    
    <% If isSaved = True Then %>
    setTimeout((function() {respondToReadyState()}), 1000);
    <% Else %>
    setTimeout((function() {respondToReadyState2()}), 1000);
    <% End If %>

</script>
</head>
<body>
</body>
</html>