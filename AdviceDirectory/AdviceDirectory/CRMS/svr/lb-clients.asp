<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%   
    Dim ADVISERID
        ADVISERID = nulltest(Request("sel_Adviser"))

    If (NullNestTF(ADVISERID) = True) Then
        ADVISERID = -1
    Else
        If NOT IsNumeric(ADVISERID) Then
            ADVISERID = -1
        End If
    End If

    Dim PROVIDERID
        PROVIDERID = nulltest(Request("sel_Provider"))

    If (NullNestTF(PROVIDERID) = True) Then
        PROVIDERID = -1
    Else
        If NOT IsNumeric(PROVIDERID) Then
            PROVIDERID = -1
        End If
    End If

'RW NullNestTF(PROVIDERID) & "<BR/>"
'RW PROVIDERID & "<BR/>"
'RW NullNestTF(ADVISERID) & "<BR/>"
'RW ADVISERID & "<BR/>"

Function MyCustomers(ADVISERID)

SQL = " SELECT " &_
    "C.CUSTOMERID AS CUSTOMERID, C.FIRSTNAME+SPACE(1)+C.SURNAME AS CUSTOMER_NAME " &_
    ",DOB " &_
    ",POSTCODE " &_
    ",J.ACTIONTAKEN " &_
    ",J.DATERECORDED " &_
    ",J.CREATEDBYNAME " &_
    ",IsNull(CP.DESCRIPTION,'Not Available') AS PRIORITY " &_
    "FROM dbo.CUSTOMERID C " &_
    "INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID " &_
    "LEFT JOIN DBO.TBL_MD_LOGIN L ON c.LOGINID = L.LoginID " &_
    "LEFT JOIN ( " &_
	"    SELECT AO.DESCRIPTION, CP.CUSTOMERID " &_
	"    FROM CUSTOMERID_PRIORITIES CP " &_
	"    INNER JOIN dbo.ASSESSMENT_OPTIONS AO ON CP.ASSESSMENTOPTIONID = AO.ID " &_
	"    WHERE CP.PRIORITY = 1 " &_
    ") CP ON C.CUSTOMERID = CP.CUSTOMERID " &_
    "INNER JOIN VW_JOURNAL J ON C.CUSTOMERID = J.CUSTOMERID " &_
    "INNER JOIN " &_
    "( " &_
    "SELECT MAX(DATERECORDED) AS DATERECORDED, CUSTOMERID  " &_
    "FROM VW_JOURNAL " &_
    "WHERE CUSTOMERID IS NOT NULL " &_
    "GROUP BY CUSTOMERID " &_
    ") " &_
    "J2 ON J2.DATERECORDED = J.DATERECORDED AND J.CUSTOMERID = J2.CUSTOMERID " &_
    "WHERE L.LoginID = "&ADVISERID&" "

Call OpenDB()
Call OpenRs(rsPage2,SQL)
If rsPage2.EOF Then
            strResult2 = strResult2 & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th colspan=""2"">Last Action</th>" & vbCrLf &_
                                        " <th>By</th>" & vbCrLf &_
                                        " <th>Current Priority</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult2 = strResult2 & "<tr>" &_
	    "<td valign=""top"" colspan=""8"">No Data Exists</td>" &_
	"</tr>" 
Else
        strResult2 = strResult2 & "<br/>" &_
                                "<div>" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px""></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th colspan=""2"">Last Action</th>" & vbCrLf &_     
                                        " <th>By</th>" & vbCrLf &_
                                        " <th>Current Priority</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"

	    Do until rsPage2.EOF

            CUSTOMERID = rsPage2("CUSTOMERID")
		    Name = rsPage2("CUSTOMER_NAME")
		    DOB = rsPage2("DOB")
		    POSTCODE = rsPage2("POSTCODE")
		    ACTIONTAKEN = rsPage2("ACTIONTAKEN")
		    CREATEDBYNAME = rsPage2("CREATEDBYNAME")
		    DATERECORDED = rsPage2("DATERECORDED")
		    PRIORITY = rsPage2("PRIORITY")

		    strResult2 = strResult2 & "<tr onclick=""GoTo('ClientJourney.asp?CUSTOMERID="&CUSTOMERID&"')"">" & vbCrLf &_
		        "<td>" & Name           & "</td>" & vbCrLf &_
		        "<td>" & DOB            & "</td>" & vbCrLf &_
		        "<td>" & POSTCODE       & "</td>" & vbCrLf &_
		        "<td>" & DATERECORDED   & "</td>" & vbCrLf &_
		        "<td>" & ACTIONTAKEN    & "</td>" & vbCrLf &_
		        "<td>" & CREATEDBYNAME  & "</td>" & vbCrLf &_
		        "<td>" & PRIORITY       & "</td>" & vbCrLf &_
		       "</tr>" 
            rsPage2.MoveNext
	    Loop

	    rsPage2.Close
	set rsPage2 = Nothing

	strResult2 = strResult2 & "</tbody>" & vbCrLf	
	strResult2 = strResult2 & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""7"">"

End if

    strResult2 = strResult2 & "</td></tr></tfoot>" & vbCrLf &_
	                        "</table></div>"
    MyCustomers = strResult2

'Call CloseDB()

End Function

Function MyReferrals(ADVISERID)

       SQL = " SELECT REFERRALID, RTDATE AS REFERRED, C.CUSTOMERID, C.FIRSTNAME+SPACE(1)+C.SURNAME AS FULLNAME, c.DOB, CD.POSTCODE, PROVIDERNAME AS REFERREDFROM, AO.DESCRIPTION AS PRIORITY " &_
         " FROM DBO.CRMS_REFERRAL R " &_
         " INNER JOIN DBO.CUSTOMERID C ON C.CUSTOMERID = R.CUSTOMERID " &_
         " INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID " &_
         " INNER JOIN DBO.TBL_MD_PROVIDER P ON P.PROVIDERID = R.RFORGANISATION " &_
         " INNER JOIN dbo.ASSESSMENT_OPTIONS AO ON AO.ID = R.ASSESSMENTOPTIONID " &_
         " WHERE ((R.RTCONTACT = " & ADVISERID & " AND RTORGANISATION = " & PROVIDERID & ") OR (RTORGANISATION = " & PROVIDERID & " AND R.RTCONTACT IS NULL))"

Call OpenDB()
Call OpenRs(rsPage,SQL)
If rsPage.EOF Then

            strResult = strResult & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""5"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Referred</th>" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th>From</th>" & vbCrLf &_
                                        " <th>Priority</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult = strResult & "<tr>" &_
	    "<td valign=""top"" colspan=""6"">No Referrals Exist</td>" &_
	"</tr>" 
Else
        strResult = strResult & "<br/>" &_
                                "<div> <span style=""font-weight:bold; color:#cc0000"">Referred</span>" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px""></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Referred</th>" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th>From</th>" & vbCrLf &_
                                        " <th>Priority</th>" & vbCrLf &_
                                        " <th></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"

	    Do until rsPage.EOF

            Referred = rsPage("REFERRED")
		    Name = rsPage("FULLNAME")
		    DOB = rsPage("DOB")
		    POSTCODE = rsPage("POSTCODE")
		    REFERREDFROM = rsPage("REFERREDFROM")
		    PRIORITY = rsPage("PRIORITY")

		    strResult = strResult & "<tr>" & vbCrLf &_
		    "<td>" & Referred &"</td>" & vbCrLf &_
		    "<td>" & Name & "</td>" & vbCrLf &_
		    "<td>" & DOB & "</td>" & vbCrLf &_
		    "<td>" & POSTCODE & "</td>" & vbCrLf &_
		    "<td>" & REFERREDFROM & "</td>" & vbCrLf &_
		    "<td>" & PRIORITY & "</td>" & vbCrLf &_
		    "<td align=""right""><ul class=""navmenu2""><li style=""float:right""><a href=""JavaScript:GoTo('CurrentContacts.asp?CUSTOMERID="&rsPage("CUSTOMERID")&"')"" title=""View Customer Details"">View</a></li></ul></td>" & vbCrLf &_
		    "</tr>" 
'-- SITE , PRIORITY ITEM E.G. HOUSING, PRIORITY ORDER (SPECIFIED BY USER)
'"<td align=""right""><ul class=""navmenu2""><li style=""float:right""><a href=""JavaScript:ReferaClient("&rsPage("REFERRALID")&")"" title=""View Referral Details"">View</a></li></ul></td>" & vbCrLf &_
	        rsPage.MoveNext
	    Loop

	    rsPage.Close
	set rsPage = Nothing

	'totalrows = cmd.Parameters("@TOTALROWS").Value	

	strResult = strResult & "</tbody>" & vbCrLf
	
	strResult = strResult & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""7"">"

    'if cint(page) > cint(0) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	'end if	
	'if cint(page) + cint(maxrows) < cint(totalrows) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	'end if

End if

    strResult = strResult & "</td></tr></tfoot>" & vbCrLf &_
	                        "</table></div>"
    MyReferrals = strResult


'Call CloseDB()

End Function

Dim RefString
    RefString = MyReferrals(ADVISERID)
Dim CusString
    CusString = MyCustomers(ADVISERID)
%>
<div id="DivRes">
    <div id="DivC">
        <%=CusString%>
    </div>
    <div id="DivR" style="float:left;width:100%;">
        <%=RefString%>
    </div>
</div>
