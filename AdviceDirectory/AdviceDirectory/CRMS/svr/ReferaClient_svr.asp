<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim isSaved
    isSaved = False

'Call Debug()

Function Record_New()

Dim LOGINID
    LOGINID = nulltest(Session("svLOGINID"))

If (NullNestTF(LOGINID) = True) Then
    isSaved = False
    Exit Function
Else
    If NOT IsNumeric(LOGINID) Then
        isSaved = False
        Exit Function
    End If
End If

'On Error Resume Next

Dim CUSTOMER_REFERENCE
Dim RFORGANISATION
Dim RFCONTACT
Dim RTDATE
Dim RTORGANISATION
Dim RTCONTACT
Dim NOTES
Dim ASSESSMENTOPTION

    CUSTOMER_REFERENCE = nulltest(Request("hid_CUSTOMER_REFERENCE"))
    RFORGANISATION = nulltest(Request("sel_RFORGANISATION"))
    RFCONTACT = nulltest(Request("sel_RFCONTACT"))
    RTDATE = nulltest(Request("txt_RTDATE"))
    RTORGANISATION = nulltest(Request("sel_RTORGANISATION"))
    RTCONTACT = nulltest(Request("sel_RTCONTACT"))
    NOTES = nulltest(Request("txt_NOTES"))
    ASSESSMENTOPTION =  nulltest(Request("hid_ASSESSMENTOPTION"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_REFERRAL_INSERT"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@RFORGANISATION", adInteger, adParamInput, 0, RFORGANISATION)
                .Parameters.Append .createparameter("@RFCONTACT", adInteger, adParamInput, 0, RFCONTACT)
                .Parameters.Append .createparameter("@RTDATE", adDBTimeStamp, adParamInput, 0, RTDATE)
                .Parameters.Append .createparameter("@RTORGANISATION", adInteger, adParamInput, 0, RTORGANISATION)
                .Parameters.Append .createparameter("@RTCONTACT", adInteger, adParamInput, 0, RTCONTACT)
                .Parameters.Append .createparameter("@NOTES", adVarWChar, adParamInput, 1073741823, NOTES)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
                .Parameters.Append .createparameter("@ASSESSMENTOPTION", adInteger, adParamInput, 0, ASSESSMENTOPTION)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing
    
    
    Call SE(RTORGANISATION, RTCONTACT)

End Function

Function SE(OrgID, AdviserID)

    SQL = "SELECT EMAIL, FIRSTNAME+SPACE(1)+LASTNAME AS ADVISER FROM TBL_MD_LOGIN WHERE (ACCESSID = 3 AND PROVIDERID = 11)"
    If (NullNestTF(AdviserID) = False) Then
        SQL = SQL & " OR (LoginId = " & AdviserID & " )"
    End If

    Dim EMAIL
    Call OpenDB()
    Call OpenRs(rs,SQL)
        If NOT rs.EOF Then
            While NOT rs.EOF
                'TEST DEFAULTS
                'strUserName = "Robert Egan"
                'strRecipient = "robert.egan@reidmark.com"
                'REAL RECIPIENTS
                strUserName = ADVISER
                strRecipient = rs("EMAIL")
                Call SendEMail(strUserName, strRecipient, constReplyToName, constReplyToEmail, SITE_NAME&" : Referral Details", "You have recieved a Referal. Please check your 'My Clients' in the ANP Portal", "")
            rs.moveNext
            Wend
        End If
    Call CloseRs(rs)
    Call CloseDB()

End Function


Function Record_Update()

Dim LOGINID
    LOGINID = nulltest(Session("svLOGINID"))

If (NullNestTF(LOGINID) = True) Then
    isSaved = False
    Exit Function
Else
    If NOT IsNumeric(LOGINID) Then
        isSaved = False
        Exit Function
    End If
End If

'On Error Resume Next

Dim REFFERAL_REFERENCE
Dim CUSTOMER_REFERENCE
Dim RFORGANISATION
Dim RFCONTACT
Dim RTDATE
Dim RTORGANISATION
Dim RTCONTACT
Dim NOTES
Dim ASSESSMENTOPTION

    REFFERAL_REFERENCE = nulltest(Request("hid_REFFERAL_REFERENCE"))
    CUSTOMER_REFERENCE = nulltest(Request("hid_CUSTOMER_REFERENCE"))
    RFORGANISATION = nulltest(Request("sel_RFORGANISATION"))
    RFCONTACT = nulltest(Request("sel_RFCONTACT"))
    RTDATE = nulltest(Request("txt_RTDATE"))
    RTORGANISATION = nulltest(Request("sel_RTORGANISATION"))
    RTCONTACT = nulltest(Request("sel_RTCONTACT"))
    NOTES = nulltest(Request("txt_NOTES"))
    ASSESSMENTOPTION =  nulltest(Request("hid_ASSESSMENTOPTION"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_REFERRAL_UPDATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@RFORGANISATION", adInteger, adParamInput, 0, RFORGANISATION)
                .Parameters.Append .createparameter("@RFCONTACT", adInteger, adParamInput, 0, RFCONTACT)
                .Parameters.Append .createparameter("@RTDATE", adDBTimeStamp, adParamInput, 0, RTDATE)
                .Parameters.Append .createparameter("@RTORGANISATION", adInteger, adParamInput, 0, RTORGANISATION)
                .Parameters.Append .createparameter("@RTCONTACT", adInteger, adParamInput, 0, RTCONTACT)
                .Parameters.Append .createparameter("@NOTES", adVarWChar, adParamInput, 1073741823, NOTES)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
                .Parameters.Append .createparameter("@ASSESSMENTOPTION", adInteger, adParamInput, 0, ASSESSMENTOPTION)
                .Parameters.Append .createparameter("@REFERRALID", adInteger, adParamInput, 0, REFFERAL_REFERENCE)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing

End Function

Dim action
    action = nulltest(Request("hid_action"))

If action = "new" Then
    Call Record_New()
Else
    Call Record_Update()
End If

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<script language="javascript" type="text/javascript">

function respondToReadyState2()
{
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Error Saving Information. Please Login and try again.</div><div style='float:right;padding:10px;'><img src='/images/close.gif' onclick='Cancel()' style='cursor:pointer' alt='Close' /></div>";
    parent.document.getElementById("light_error").className = "er";
}

function respondToReadyState()
{
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Saved</div><div style='float:right;padding:10px;'><img src='/images/close.gif' onclick='Cancel()' style='cursor:pointer' alt='Close' /></div>";
    parent.document.getElementById("light_error").className = "no_error";    
}
    parent.document.getElementById("light_error").innerHTML = "<div style='float:left; padding:10px;'>Please wait. Saving...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif' alt='Loading' /></div>";
    parent.document.getElementById("light_error").className = "no_error";
    
    <% If isSaved = True Then %>
    setTimeout((function() {respondToReadyState()}), 1000);
    <% Else %>
    setTimeout((function() {respondToReadyState2()}), 1000);
    <% End If %>

</script>
</head>
<body>
</body>
</html>