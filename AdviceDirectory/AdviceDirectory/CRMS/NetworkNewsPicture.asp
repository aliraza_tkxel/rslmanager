<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Call Debug()
Dim ID
Dim PHOTOGRAPH

    ID = Request("NEWSID")
    If ID = "" Or IsNull(ID) Then ID = -1 End If

  	SQL = "SELECT PHOTOGRAPH " &_
  	        "FROM CRMS_NETWORKNEWS " &_
	        "WHERE NEWSID = " & ID
	Call OpenDB()
	Call OpenRs(rsClient, SQL)
		If NOT (rsClient.EOF) Then
			PHOTOGRAPH = rsClient(0)
		End If
	Call CloseRS(rsClient)
	Call CloseDB()
%>
<%
    Dim UploadDirectory
        UploadDirectory = "images"
    Dim GP_curPath
        GP_curPath = Request.ServerVariables("PATH_INFO")
        GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
        If Mid(GP_curPath,Len(GP_curPath),1)  <> "/" Then
            GP_curPath = GP_curPath & "/"
        End If
    Dim FILE 
        FILE = Server.mappath(GP_curPath & "defaultnews.jpg")

If IsNull(PHOTOGRAPH) Then
   
    Const adTypeBinary = 1
    Dim objStream
    Dim objFileContents
    Set objStream = CreateObject("ADODB.Stream")
        objStream.Type = adTypeBinary
        objStream.Open
        objStream.LoadFromFile FILE
        PHOTOGRAPH = objStream.Read
        'CLEANUP
        objStream.Close
    Set objStream = Nothing
    
    Response.ContentType = "image/jpeg"
    Response.Clear
    Response.BinaryWrite(PHOTOGRAPH)
    Response.End()    

Else

    Response.ContentType = "image/gif"
    Response.Clear
    Response.BinaryWrite(PHOTOGRAPH)
    Response.End()

End If
%>