<%@ Page Language="VB" AutoEventWireup="false" Trace="false" CodeFile="Report2-ReferralAnalysis.aspx.vb"
    Inherits="CRMS_NewTenancyList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ANP Portal > Reports > Professional/Partner Login Analysis</title>

    <script type="text/javascript" language="javascript">
        function ExportXlsGo()
        {            
            window.open('XlsExport.aspx?cache=RA','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }       
        function PrintGo()
        {            
            window.open('PrintExport.aspx?cache=RA','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ext:PageManager ID="pmPageManager" runat="server" EnableAjax="false" />
            <ext:PageLoading ID="PageLoading1" runat="server" ImageUrl="~\IMAGES\ajax-loader.gif"
                EnableAjax="true" EnableFadeOut="true" ShowOnPostBack="true">
            </ext:PageLoading>
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="false" Title="Referral Analysis" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server" CssStyle="float:left; width:100%">
                        <Items>
                            <ext:Grid ID="gvReport" EnableCollapse="true" Title="Referral Analysis" ShowBorder="true"
                                AllowPaging="true" ShowHeader="true" Icon="Table" AllowSorting="true" EnableHeaderMenu="true"
                                AutoWidth="true" PageSize="10" AutoHeight="true" runat="server" DataKeyNames="CUSTOMERID,FIRSTNAME,SURNAME,REC_COUNT"
                                OnSort="gvReport_Sort" EnableRowNumber="True" AutoPostBack="true" OnRowClick="gvReport_RowClick">
                                <Toolbars>
                                    <ext:Toolbar runat="server" ID="tbGrid">
                                        <Items>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblFrom" runat="server" Text="From:">
                                            </ext:Label>
                                            <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                                ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblTo" runat="server" Text="To:">
                                            </ext:Label>
                                            <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                                DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                                Label="To Date" runat="server">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label Text="Local Authority Area: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                                DataTextField="LocalAuthorityAreaId" DataValueField="LocalAuthorityArea">
                                            </ext:DropDownList>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblResidentName" runat="server" Text="Resident Name:">
                                            </ext:Label>
                                            <ext:TextBox ID="txtResidentName" runat="server">
                                            </ext:TextBox>
                                            <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                                Icon="reload">
                                            </ext:Button>
                                            <ext:ToolbarSeparator runat="server" ID="ts1">
                                            </ext:ToolbarSeparator>
                                            <ext:ToolbarFill ID="tbFill" runat="server">
                                            </ext:ToolbarFill>
                                            <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Text="To Xls" ToolTip="Export to Excel"
                                                runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                            </ext:Button>
                                            <ext:ToolbarSeparator ID="ts2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" ToolTip="Print" Text="Print"
                                                runat="server" Icon="Printer">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField DataField="FIRSTNAME" SortField="FIRSTNAME" HeaderText="First Name" />
                                    <ext:BoundField DataField="SURNAME" SortField="SURNAME" HeaderText="Surname" />
                                    <ext:BoundField DataField="REC_COUNT" SortField="REC_COUNT" HeaderText="Number of Referrals" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>
        <ext:Label ID="labResult" EncodeText="false" runat="server" />
        <asp:HiddenField ID="CustomerId" runat="server" EnableViewState="true" />
        <br />
        <%-- 
        '<ext:Button ID="Button1" runat="server" CssStyle="margin-right:5px;float:left;" Text="How many lines were selected" OnClick="Button1_Click"></ext:Button>
        --%>
        <div id="ResultDetail" runat="server" visible="false">
            <ext:Panel ID="Panel1" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="false" Title="Referral Details" Icon="table" CssStyle="float:left; width:100%">
                <Items>
                    <ext:Panel ID="Panel2" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                            <ext:Grid ID="Grid1" EnableCollapse="true" Title="Referral Details" ShowBorder="true"
                                AllowPaging="true" ShowHeader="true" Icon="Table" AllowSorting="true" EnableHeaderMenu="true"
                                AutoWidth="true" PageSize="10" AutoHeight="true" runat="server" DataKeyNames="RDATE, RFP, RTP"
                                EnableRowNumber="True" AutoPostBack="true">
                                <Columns>
                                    <ext:BoundField DataField="RDATE" SortField="RDATE" HeaderText="Referral Date" DataFormatString="{0:d}" />
                                    <ext:BoundField DataField="RFP" SortField="RFP" HeaderText="Referral From" ExpandUnusedSpace="false" Width="200px"  />
                                    <ext:BoundField DataField="RTP" SortField="RTP" HeaderText="Referral To" ExpandUnusedSpace="true" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>
    </form>
</body>
</html>
