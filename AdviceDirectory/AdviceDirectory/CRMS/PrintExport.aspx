<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintExport.aspx.vb" EnableTheming="false" 
Inherits="Customer_PrintExport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        body
        {
            background:none;
        }
    </style>
    
    <script type="text/javascript" language="javascript">               
        print();        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvPrintReport" runat="server">
        </asp:GridView>
    </div>
    </form>
</body>
</html>
