<%@ Page Language="VB" AutoEventWireup="false" Trace="false" CodeFile="Report2-LoginAnalysis-PP.aspx.vb"
    Inherits="CRMS_NewTenancyList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ANP Portal > Reports > Professional/Partner Login Analysis</title>

    <script type="text/javascript" language="javascript">

        function ExportXlsGo()
        {
            window.open('XlsExport.aspx?cache=PPLA','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }

        function PrintGo()
        {
            window.open('PrintExport.aspx?cache=PPLA','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
        <asp:Label ID="lblAccessRights" runat="server"></asp:Label>
            <ext:PageManager ID="pmPageManager" runat="server" />
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="false" Title="Professional/Partner Login Analysis" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                            <ext:Grid ID="gvReport" EnableCollapse="true" Title="Professional/Partner Login Analysis"
                                ShowBorder="true" AllowPaging="true" ShowHeader="true" Icon="Table" AllowSorting="true"
                                EnableHeaderMenu="true" AutoWidth="true" PageSize="15" AutoHeight="true" runat="server"
                                DataKeyNames="LOGINID,FIRSTNAME,LASTNAME" OnSort="gvReport_Sort">
                                <Toolbars>
                                    <ext:Toolbar runat="server" ID="tbGrid" Hidden="false">
                                        <Items>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblFrom" runat="server" Text="From:">
                                            </ext:Label>
                                            <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                                ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblTo" runat="server" Text="To:">
                                            </ext:Label>
                                            <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                                DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                                Label="To Date" runat="server">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label Text="Local Authority Area: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                                DataTextField="LocalAuthorityArea" DataValueField="LocalAuthorityAreaId">
                                            </ext:DropDownList>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator5" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label Text="Organisation: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selOrganisation" ID="selOrganisation" DataTextField="PROVIDERNAME"
                                                DataValueField="PROVIDERNAMEID">
                                            </ext:DropDownList>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblAdviserName" runat="server" Text="Adviser Name:">
                                            </ext:Label>
                                            <ext:TextBox ID="txtAdviserName" runat="server">
                                            </ext:TextBox>
                                            <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                                Icon="reload">
                                            </ext:Button>
                                            <ext:ToolbarSeparator runat="server" ID="ts1">
                                            </ext:ToolbarSeparator>
                                            <ext:ToolbarFill ID="tbFill" runat="server">
                                            </ext:ToolbarFill>
                                            <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Text="To Xls" ToolTip="Export to Excel"
                                                runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                            </ext:Button>
                                            <ext:ToolbarSeparator ID="ts2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" ToolTip="Print" Text="Print"
                                                runat="server" Icon="Printer">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField DataField="LOGINID" SortField="LOGINID" HeaderText="Login Id" ExpandUnusedSpace="false" />
                                    <ext:BoundField DataField="FIRSTNAME" SortField="FIRSTNAME" HeaderText="First Name" />
                                    <ext:BoundField DataField="LASTNAME" SortField="LASTNAME" HeaderText="Last Name" />
                                    <ext:BoundField DataField="DATECREATED" SortField="DATECREATED" HeaderText="Date"
                                        DataFormatString="{0:d}" />
                                    <ext:BoundField DataField="TIMECREATED" SortField="TIMECREATED" HeaderText="Time Created"
                                        DataFormatString="{0:c}" />
                                    <ext:BoundField DataField="ORGANISATION" ExpandUnusedSpace="true" SortField="ORGANISATION"
                                        HeaderText="Organisation" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>
    </form>
</body>
</html>
