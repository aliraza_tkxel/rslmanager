﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, "")) 			
	'Call PageContent(GP_curPath)
	Call PageContent("NetworkNews")	
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%
	'Response.Write Request("PHOTOGRAPH")
	Session("action") = "none"
	
    Const adTypeBinary = 1
    Dim FileConents
    Dim FName

Function AddFileToDB(strFilePath,ID)
    'ADD THE CSV TO THE DATABASE

    'SETTING UP STREAM OBJECT
    Dim objStream
    Dim objFileContents
    Set objStream = CreateObject("ADODB.Stream")
        objStream.Type = adTypeBinary
        objStream.Open
        objStream.LoadFromFile strFilePath
        objFileContents = objStream.Read
        'CLEANUP
        objStream.Close
    Set objStream = Nothing

    'SETTING SQL QUERY
    'SQL = "SELECT PHOTOGRAPH FROM CRMS_NETWORKNEWS WHERE NEWSID = " & ID
    'Call OpenRs(rsUpdate, SQL)
    'If(NOT rsUpdate.EOF) Then
    '    rsUpdate.Fields("PHOTOGRAPH").Value = objFileContents
    '    rsUpdate.Update()
    'End If
    'Call CloseRs(rsUpdate)
    
    Dim adoCon, sqlstr, htmlstr, rsUpdate      
    Set adoCon = Server.CreateObject("ADODB.Connection")      
        adoCon.Open DSN_CONNECTION_STRING      
    Set rsUpdate = Server.CreateObject("ADODB.Recordset")      
        sqlstr = "SELECT PHOTOGRAPH FROM CRMS_NETWORKNEWS WHERE NEWSID = " & ID      
        rsUpdate.Open sqlstr, adoCon, 3, 3 
        If(NOT rsUpdate.EOF) Then     
            rsUpdate.Fields("PHOTOGRAPH").Value = objFileContents
            rsUpdate.Update()
        End If

    'DELETE PHYSICAL FILE
    Dim fso
    Set fso = CreateObject("Scripting.FileSystemObject")
        'If FolderExists() Then
            If (fso.FileExists(strFilePath)) Then
               fso.DeleteFile(strFilePath)
  	        End if
  	    'Else
  	        '
        'End If
	Set fso = Nothing

End Function


Sub BuildUploadRequest(RequestBin,UploadDirectory,storeType,sizeLimit,nameConflict)
  'Get the boundary
  PosBeg = 1
  PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
  if PosEnd = 0 then
    Response.Write "<b>Form was submitted with no ENCTYPE=""multipart/form-data""</b><br/>"
    Response.Write "Please correct the form attributes and try again."
    Response.End
  end if
  'Check ADO Version
	set checkADOConn = Server.CreateObject("ADODB.Connection")
	adoVersion = CSng(checkADOConn.Version)
	set checkADOConn = Nothing
	if adoVersion < 2.5 then
    Response.Write "<b>You don't have ADO 2.5 installed on the server.</b><br/>"
    Response.Write "The File Upload extension needs ADO 2.5 or greater to run properly.<br/>"
    Response.Write "You can download the latest MDAC (ADO is included) from <a href=""www.microsoft.com/data"">www.microsoft.com/data</a><br/>"
    Response.End
	end if		
  'Check content length if needed
	Length = CLng(Request.ServerVariables("HTTP_Content_Length")) 'Get Content-Length header
	If "" & sizeLimit <> "" Then
    sizeLimit = CLng(sizeLimit)
    If Length > sizeLimit Then
      Request.BinaryRead (Length)
      Response.Write "Upload size " & FormatNumber(Length, 0) & "B exceeds limit of " & FormatNumber(sizeLimit, 0) & "B"
      Response.End
    End If
  End If
  boundary = MidB(RequestBin,PosBeg,PosEnd-PosBeg)
  boundaryPos = InstrB(1,RequestBin,boundary)
  'Get all data inside the boundaries
  Do until (boundaryPos=InstrB(RequestBin,boundary & getByteString("--")))
    'Members variable of objects are put in a dictionary object
    Dim UploadControl
    Set UploadControl = CreateObject("Scripting.Dictionary")
    'Get an object name
    Pos = InstrB(BoundaryPos,RequestBin,getByteString("Content-Disposition"))
    Pos = InstrB(Pos,RequestBin,getByteString("name="))
    PosBeg = Pos+6
    PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(34)))
    Name = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
    PosFile = InstrB(BoundaryPos,RequestBin,getByteString("filename="))
    PosBound = InstrB(PosEnd,RequestBin,boundary)
    'Test if object is of file type
    If  PosFile<>0 AND (PosFile<PosBound) Then
      'Get Filename, content-type and content of file
      PosBeg = PosFile + 10
      PosEnd =  InstrB(PosBeg,RequestBin,getByteString(chr(34)))
      FileName = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      FileName = Mid(FileName,InStrRev(FileName,"\")+1)
      'Add filename to dictionary object
      UploadControl.Add "FileName", FileName
      Pos = InstrB(PosEnd,RequestBin,getByteString("Content-Type:"))
      PosBeg = Pos+14
      PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
      'Add content-type to dictionary object
      ContentType = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      UploadControl.Add "ContentType",ContentType
      'Get content of object
      PosBeg = PosEnd+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = FileName
      ValueBeg = PosBeg-1
      ValueLen = PosEnd-Posbeg
    Else
      'Get content of object
      Pos = InstrB(Pos,RequestBin,getByteString(chr(13)))
      PosBeg = Pos+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      ValueBeg = 0
      ValueEnd = 0
    End If
    'Add content to dictionary object
    UploadControl.Add "Value" , Value	
    UploadControl.Add "ValueBeg" , ValueBeg
    UploadControl.Add "ValueLen" , ValueLen	
    'Add dictionary object to main dictionary
    UploadRequest.Add name, UploadControl	
    'Loop to next object
    BoundaryPos=InstrB(BoundaryPos+LenB(boundary),RequestBin,boundary)
  Loop

  GP_keys = UploadRequest.Keys
  for GP_i = 0 to UploadRequest.Count - 1
    GP_curKey = GP_keys(GP_i)
    'Save all uploaded files
    if UploadRequest.Item(GP_curKey).Item("FileName") <> "" then
      GP_value = UploadRequest.Item(GP_curKey).Item("Value")
      GP_valueBeg = UploadRequest.Item(GP_curKey).Item("ValueBeg")
      GP_valueLen = UploadRequest.Item(GP_curKey).Item("ValueLen")

      if GP_valueLen = 0 then
        Response.Write "<b>An error has occured saving uploaded file!</b><br/><br/>"
        Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
        Response.Write "File does not exists or is empty.<br/>"
        Response.Write "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
	  	  response.End
	    end if
      
      'Create a Stream instance
      Dim GP_strm1, GP_strm2
      Set GP_strm1 = Server.CreateObject("ADODB.Stream")
      Set GP_strm2 = Server.CreateObject("ADODB.Stream")
      
      'Open the stream
      GP_strm1.Open
      GP_strm1.Type = 1 'Binary
      GP_strm2.Open
      GP_strm2.Type = 1 'Binary
        
      GP_strm1.Write RequestBin
      GP_strm1.Position = GP_ValueBeg
      GP_strm1.CopyTo GP_strm2,GP_ValueLen
    
      'Create and Write to a File
      GP_curPath = Request.ServerVariables("PATH_INFO")
      GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
      if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
        GP_curPath = GP_curPath & "/"
      end if 
      GP_CurFileName = UploadRequest.Item(GP_curKey).Item("FileName")
      GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & GP_CurFileName
  FName = GP_FullFileName
      'Check if the file already exist
      GP_FileExist = false
      Set fso = CreateObject("Scripting.FileSystemObject")
      If (fso.FileExists(GP_FullFileName)) Then
        GP_FileExist = true
      End If      
      if nameConflict = "error" and GP_FileExist then
        Response.Write "<b>File already exists!</b><br/><br/>"
        Response.Write "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
				GP_strm1.Close
				GP_strm2.Close
	  	  response.End
      end if
      if ((nameConflict = "over" or nameConflict = "uniq") and GP_FileExist) or (NOT GP_FileExist) then
        if nameConflict = "uniq" and GP_FileExist then
          Begin_Name_Num = 0
          while GP_FileExist    
            Begin_Name_Num = Begin_Name_Num + 1
            GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            GP_FileExist = fso.FileExists(GP_FullFileName)
          wend  
          UploadRequest.Item(GP_curKey).Item("FileName") = fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
					UploadRequest.Item(GP_curKey).Item("Value") = UploadRequest.Item(GP_curKey).Item("FileName")
        end if
        on error resume next
        GP_strm2.SaveToFile GP_FullFileName,2
        if err then
          Response.Write "<b>An error has occured saving uploaded file!</b><br/><br/>"
          Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
          Response.Write "Maybe the destination directory does not exist, or you don't have write permission.<br/>"
          Response.Write "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
    		  err.clear
  				GP_strm1.Close
  				GP_strm2.Close
  	  	  response.End
  	    end if
  			GP_strm1.Close
  			GP_strm2.Close
  			if storeType = "path" then
  				UploadRequest.Item(GP_curKey).Item("Value") = GP_curPath & UploadRequest.Item(GP_curKey).Item("Value")
  			end if
        on error goto 0
      end if
    end if
  next

End Sub

'String to byte string conversion
Function getByteString(StringStr)
  For i = 1 to Len(StringStr)
 	  char = Mid(StringStr,i,1)
	  getByteString = getByteString & chrB(AscB(char))
  Next
End Function

'Byte string to string conversion
Function getString(StringBin)
  getString =""
  For intCount = 1 to LenB(StringBin)
	  getString = getString & chr(AscB(MidB(StringBin,intCount,1))) 
  Next
End Function

Function UploadFormRequest(name)
  on error resume next
  if UploadRequest.Item(name) then
    UploadFormRequest = UploadRequest.Item(name).Item("Value")
  end if  
End Function

'Process the upload
'UploadQueryString = Replace(Request.QueryString,"GP_upload=true","")
'if mid(UploadQueryString,1,1) = "&" then
'	UploadQueryString = Mid(UploadQueryString,2)
'end if

'GP_uploadAction = "NetworkNews_Edit.asp.asp?MaxID=" & Session("NewsID")

'CStr(Request.ServerVariables("URL")) & "?GP_upload=true"
'If (Request.QueryString <> "") Then  
  'if UploadQueryString <> "" then
	 ' GP_uploadAction = GP_uploadAction & "&" & UploadQueryString
  'end if 
'End If

If (CStr(Request.QueryString("GP_upload")) <> "") Then
  GP_redirectPage = ""
  If (GP_redirectPage = "") Then
    GP_redirectPage = CStr(Request.ServerVariables("URL"))
  end if
    
  RequestBin = Request.BinaryRead(Request.TotalBytes)
  Dim UploadRequest
  Set UploadRequest = CreateObject("Scripting.Dictionary")  
  BuildUploadRequest RequestBin, "UploadImg", "file", "", "over"
  
  '*** GP NO REDIRECT
end if  
if UploadQueryString <> "" then
  UploadQueryString = UploadQueryString & "&GP_upload=true"
else  
  UploadQueryString = "GP_upload=true"
end if
%>
<%
' *** Edit Operations: (Modified for File Upload) declare variables
MM_editAction = CStr(Request.ServerVariables("URL")) 'MM_editAction = CStr(Request("URL"))
If (UploadQueryString <> "") Then
  MM_editAction = MM_editAction & "?" & UploadQueryString
End If
' boolean to abort record edit
MM_abortEdit = false
' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: (Modified for File Upload) set variables

If (CStr(UploadFormRequest("MM_insert")) <> "") Then

  MM_editConnection = DSN_CONNECTION_STRING
  MM_editTable = "CRMS_NETWORKNEWS"
  'MM_editRedirectUrl = "mike.asp"
  MM_fieldsStr  = "txt_ProviderID|value|txt_LoginID|value|txt_Headline|value|txt_Date|value|NEWSGLOBAL|value"
  MM_columnsStr = "ProviderID|',none,''|LoginID|',none,''|NEWSHEADLINE|',none,''|NewsDate|',none,''|NEWSGLOBAL|none,1,0"
  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")  
  ' set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(UploadFormRequest(MM_fields(i)))
  Next
'  ' append the query string to the redirect URL
'  If (MM_editRedirectUrl <> "" And UploadQueryString <> "") Then
'    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And UploadQueryString <> "") Then
'      MM_editRedirectUrl = MM_editRedirectUrl & "?" & UploadQueryString
'    Else
'      MM_editRedirectUrl = MM_editRedirectUrl & "&" & UploadQueryString
'    End If
'  End If

End If
%>
<%
' *** Insert Record: (Modified for File Upload) construct a sql insert statement and execute it

If (CStr(UploadFormRequest("MM_insert")) <> "") Then

  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End if
    MM_tableValues = MM_tableValues & MM_columns(i)
    MM_dbValues = MM_dbValues & FormVal
  Next
  MM_editQuery = "INSERT INTO " & MM_editTable & " (" & MM_tableValues & ") VALUES (" & MM_dbValues & ")"

  If (Not MM_abortEdit) Then
    ' execute the insert
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
        MM_editCmd.ActiveConnection = MM_editConnection
        MM_editCmd.CommandText = MM_editQuery
        MM_editCmd.Execute
    
    'MM_editCmd.ActiveConnection.Close
    'If (MM_editRedirectUrl <> "") Then
      'Response.Redirect(MM_editRedirectUrl)
    'End If
  End If

End If

If (CStr(UploadFormRequest("MM_insert")) <> "") Then
    SQL = "SELECT MAX(NEWSID) AS MAXID FROM CRMS_NETWORKNEWS"
    Call OpenDB()
    Call OpenRs(rs,SQL)
        If NOT rs.EOF Then
            NEWSID = rs("MAXID")
        End If
    Call CloseRs(rs)
    If Len(UploadFormRequest("PHOTOGRAPH")) <> 0 Then
        Call AddFileToDB(FName, NEWSID)
    End If
    Call CloseDB()

    MM_Pg = "NetworkNews_Edit.asp?MAXID=" & NEWSID
    Response.Redirect(MM_Pg)
End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />
<style type="text/css">
select 
{
    width:auto;
}
</style>
<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script language="JavaScript" type="text/javascript">
function getFileExtension(filePath) { //v1.0
  fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/')+1,filePath.length) : filePath.substring(filePath.lastIndexOf('\\')+1,filePath.length));
  return fileName.substring(fileName.lastIndexOf('.')+1,fileName.length);
}

function checkFileUpload(form,extensions) { //v1.0
  document.MM_returnValue = true;
  if (extensions && extensions != '') {
    for (var i = 0; i<form.elements.length; i++) {
      field = form.elements[i];
      if (field.type.toUpperCase() != 'FILE') continue;
      if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
        alert('This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.');
        document.MM_returnValue = false;field.focus();break;
  } } }
}

function RemoveBad()
{
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\;|\(|\)|\&|\+|\-/g,"");
	event.srcElement.value = strTemp;
}
function BtnSubmit()
{
	checkFileUpload(Form1,'gif;jpg;jpeg');
	if (!document.MM_returnValue) return;
	if (document.getElementById("txt_Headline").value == "")
		alert("Please enter a Headline");
	else if (document.getElementById("txt_Date").value == "")
		alert("Please enter a value for News Date");
	else goNews();
}

function goNews()
{
	document.Form1.action = "<%=MM_editAction%>";
	document.Form1.submit();
}

function AddNews()
{
   window.location.href = "NetworkNews_Add.asp";
}

function EditNews()
{
   window.location.href = "NetworkNews_Amend.asp";
}

function DeleteNews()
{
   window.location.href = "NetworkNews_Delete.asp";
}

</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="width:99%; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<form method="POST" id="Form1" name="Form1" action="<%=MM_editAction%>" enctype="multipart/form-data">
			<div>
			    <div style="width:100%">
			        <div id="nsform" style="float:right; width:100%; border:solid 1px #cc0000; padding-bottom:1em;">
                        <div id="errordiv" style="margin:10px; padding:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
                        
                        <div style="float:right; margin-right:10px; clear:right">
                                <ul class="navmenu2">
                                <li><a href="NetworkNews_Add.asp">Add News</a></li>
                                <li><a href="NetworkNews_Amend.asp">Amend News</a></li>
                                <li><a href="NetworkNews_Delete.asp">Delete News</a></li>
                            </ul>
                        </div>
                        <div style="float:right;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="10">&nbsp;</td>
                                <td valign="top"> 
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td width="98%" height="150"> 
                                        <% If Request.QueryString("Add") = "Success" Then %>
                                        Your News Story has been added
                                        <% Else %>
                                          <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                            <tr>
                                              <td colspan="2"><br/>
                                                To add a new news item , please enter or select values 
                                                for the details requested below.<br/>
                                                <font color="red"><b>Please Note:</b></font> All images 
                                                must be 145px by 145px as they will be reshaped to this 
                                                size automatically, thus causing blurring for images that 
                                                are not the same size.<br/>
                                                <br/>
                                              </td>
                                            </tr>
                                            <tr> 
                                              <td nowrap="nowrap"><strong>Headline</strong></td>
                                              <td> 
                                                <input name="txt_Headline" id="txt_Headline" type="text" maxlength="24" size="53" onblur="RemoveBad()" />
                                              </td>
                                            </tr>
                                            <tr> 
                                              <td> 
                                                <a href="javascript:;" onclick="YY_Calendar('txt_Date',480,135,'de','#FFFFFF','#cc0000','YY_calendar1')">News Date</a>
                                              </td>
                                              <td> 
                                                <input name="txt_Date" id="txt_Date" type="text" size="53" readonly="readonly" />
                                              </td>
                                            </tr>
                                            <tr> 
                                              <td><strong>Image</strong></td>
                                              <td> 
                                                <input type="file" id="file" size="50" name="PHOTOGRAPH" />
                                              </td>
                                            </tr>
                                            <tr> 
                                              <td><b>Syndicate</b></td>
                                              <td><input type="checkbox" name="NEWSGLOBAL" value="1" /></td>
                                            </tr>
                                            </tr>
                                            <tr> 
                                              <td>&nbsp;</td>
                                              <td> 
                                                <input type="button" name="Submit" value="Add News Item" onclick="BtnSubmit()" />
                                              </td>
                                            </tr>
                                          </table>
                                          <input type="hidden" name="MM_insert" value="true" />
                                          <input type="hidden" name="txt_ProviderID" id="txt_ProviderID" value="<%=Session("svPROVIDERID")%>" />
                                          <input type="hidden" name="txt_LoginID" id="txt_LoginID" value="<%=Session("svLOGINID")%>" />  
                                          <% End IF %>
                                      </td>
                                    </tr>
                                  </table>
                                  </div>


                    </div>
			    </div>
			</div>
			</form>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" id="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" id="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image" id="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image" id="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" id="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>