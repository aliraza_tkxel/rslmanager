<%@ Page Language="VB" AutoEventWireup="false" Trace="false" CodeFile="Report2-SelfAssessment.aspx.vb"
    Inherits="CRMS_NewTenancyList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ANP Portal > Reports > Professional/Partner Login Analysis</title>

    <script type="text/javascript" language="javascript">
        function ExportXlsGo()
        {            
            window.open('XlsExport.aspx?cache=SAUA','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }       
        function PrintGo()
        {            
            window.open('PrintExport.aspx?cache=SAUA','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ext:PageManager ID="pmPageManager" runat="server" />
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="false" Title="Self Assessment Usage Analysis" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                            <ext:Grid ID="gvReport" EnableCollapse="true" Title="Self Assessment Usage Analysis"
                                ShowBorder="true" AllowPaging="true" ShowHeader="true" Icon="Table" AllowSorting="true"
                                EnableHeaderMenu="true" AutoWidth="true" PageSize="15" AutoHeight="true" runat="server"
                                DataKeyNames="DATECREATED, POSTCODE, CUSTOMER_NAME, PROVIDERNAME, ADVISER_NAME, PRIORITY_1, PRIORITY_2, PRIORITY_3, DISTANCE_1, DISTANCE_2, DISTANCE_3"
                                OnSort="gvReport_Sort">
                                <Toolbars>
                                    <ext:Toolbar runat="server" ID="tbGrid">
                                        <Items>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblFrom" runat="server" Text="From:">
                                            </ext:Label>
                                            <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                                ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblTo" runat="server" Text="To:">
                                            </ext:Label>
                                            <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                                DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                                Label="To Date" runat="server">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label Text="Local Authority Area: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                                DataTextField="LocalAuthorityAreaId" DataValueField="LocalAuthorityArea">
                                            </ext:DropDownList>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblPostcode" runat="server" Text="Postcode:">
                                            </ext:Label>
                                            <ext:TextBox ID="txtPostcode" runat="server">
                                            </ext:TextBox>
                                            <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                                Icon="reload">
                                            </ext:Button>
                                            <ext:ToolbarSeparator runat="server" ID="ts1">
                                            </ext:ToolbarSeparator>
                                            <ext:ToolbarFill ID="tbFill" runat="server">
                                            </ext:ToolbarFill>
                                            <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Text="To Xls" ToolTip="Export to Excel"
                                                runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                            </ext:Button>
                                            <ext:ToolbarSeparator ID="ts2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" ToolTip="Print" Text="Print"
                                                runat="server" Icon="Printer">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField DataField="DATECREATED" SortField="DATECREATED" HeaderText="Date"
                                        DataFormatString="{0:d}" />
                                    <ext:BoundField DataField="POSTCODE" SortField="POSTCODE" HeaderText="Postcode" />
                                    <ext:BoundField DataField="CUSTOMER_NAME" SortField="CUSTOMER_NAME" HeaderText="Customer" />
                                    <ext:BoundField DataField="PROVIDERNAME" SortField="PROVIDERNAME" HeaderText="Provider" />
                                    <ext:BoundField DataField="ADVISER_NAME" SortField="ADVISER_NAME" HeaderText="Adviser" />
                                    <ext:BoundField DataField="PRIORITY_1" SortField="PRIORITY_1" HeaderText="Barrier 1" />
                                    <ext:BoundField DataField="DISTANCE_1" SortField="DISTANCE_1" HeaderText="Distance 1" />
                                    <ext:BoundField DataField="PRIORITY_2" SortField="PRIORITY_2" HeaderText="Barrier 2" />
                                    <ext:BoundField DataField="DISTANCE_2" SortField="DISTANCE_2" HeaderText="Distance 2" />
                                    <ext:BoundField DataField="PRIORITY_3" SortField="PRIORITY_3" HeaderText="Barrier 3" />
                                    <ext:BoundField DataField="DISTANCE_3" SortField="DISTANCE_3" HeaderText="Distance 3" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>
    </form>
</body>
</html>
