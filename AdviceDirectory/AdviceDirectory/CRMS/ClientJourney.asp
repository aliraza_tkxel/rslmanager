﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")
%>
<%
Dim CUSTOMER_REFERENCE
    CUSTOMER_REFERENCE = nulltest(Request("CUSTOMERID"))
    
If (NullNestTF(CUSTOMER_REFERENCE) = True) Then
    Response.Redirect ("/crms/login.asp")
Else
    If NOT IsNumeric(CUSTOMER_REFERENCE) Then
        Response.Redirect ("/crms/NoRecord.asp?Er=1")
    End If
End If
%>
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%
Dim FULLNAME
Dim PRIORITIESSAVED
'Dim BtnDisabled 
 '   BtnDisabled = True

Function Record_Search(Id)

Dim rsBU

    CUSTOMER_REFERENCE = nulltest(Request("CUSTOMERID"))
    
        Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_FIND_CUSTOMER"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, NULL)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, NULL)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 7, NULL)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, NULL)
                'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsBU = .Execute
					If NOT rsBU.EOF Then
					    FULLNAME = rsBU("FULLNAME")
					Else
					    Response.Redirect ("/NoRecord.asp?Er=2")
					End If
		    End With
	    objConn.Close
    Set objConn = Nothing
    
    Call BuildJourney(id)

End Function


TBL = ""

Function BuildJourney(CUSTOMER_REFERENCE)

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_JOURNAL"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsBU = .Execute
    				If NOT rsBU.EOF Then
					        TBL = TBL & "<div>" &_
					        "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" &_
					        "<thead>" &_
                            "<tr>" &_
                                " <th colspan=""4"" style=""background-color:white; color:black; padding-left:0px"">Customer's Journey</th>" &_
                             "</tr>" &_
                              "<tr class="""">" &_
                                " <th style=""white-space:pre; text-align:middle"">Date Recorded</th>" &_
                                " <th style=""white-space:pre; text-align:middle"">Action Taken</th>" &_
                                " <th style=""white-space:pre; text-align:middle"">Created By</th>" &_
                                " <th style=""white-space:pre"">&nbsp;</th>" &_
                             "</tr>" &_
                         "</thead>" &_
                         "<tbody>"
					    While NOT rsBU.EOF
					    If (rsBU("HISTORYID") = "" Or IsNull(rsBU("HISTORYID"))) Then
					        HISTORYID = 0
					    Else
					        HISTORYID = rsBU("HISTORYID")
					    End If
					    
					     If (rsBU("ProviderID") = "" Or IsNull(rsBU("ProviderID"))) Then
					        strProvider = ""
					     Else
					        strProvider = rsBU("ProviderName")
					        'strProvider = "<a href=""/membersarea/contactdetails.asp?pg=ClientJourney&CustomerID="&CUSTOMER_REFERENCE&"&ProviderID="& rsBU("ProviderID")& """>" & rsBU("ProviderName") & "</a>"
					     End If
					     
					    If (rsBU("ADVISERCONTACT") = "" Or IsNull(rsBU("ADVISERCONTACT"))) Then
					        STRcONTACT = ""
					    Else
					        STRcONTACT = "onmouseover=""Tip('"&rsBU("ADVISERCONTACT")&"', TITLE, 'Adviser Contact Number', TITLEBGCOLOR, '#cc0000', BORDERCOLOR, '#cc0000', SHADOW, true, SHADOWWIDTH, 7)"" onmouseout=""UnTip()"""
					    End If
					      
					    If (rsBU("EMAIL") = "" Or IsNull(rsBU("EMAIL"))) Then
					        strLink = rsBU("CREATEDBYNAME")
					    Else
					        strLink = "<a href=""mailto:"& rsBU("EMAIL")& """ " & STRcONTACT & ">" & rsBU("CREATEDBYNAME") & "</a> - " & strProvider 
					    End If
					    '"<a>" & strProvider & "</a></td>" & vbCrLf &_
                         TBL = TBL & " <tr style=""cursor:default"">" &_
                             " <td nowrap=""nowrap"" style=""white-space:pre; text-align:left"">"& FormatDateTime(rsBU("DATERECORDED"),1) &"</td>" &_
                             " <td nowrap=""nowrap"" style=""white-space:pre; text-align:left"">"& rsBU("ACTIONTAKEN") &"</td>" &_
                             " <td width=""100%"">"& strLink &"</td>"
                            If IsNull(rsBU(0)) Then
                                TBL = TBL & " <td nowrap=""nowrap"" style=""white-space:pre; text-align:right""></td>"
                            Else
                                TBL = TBL & " <td nowrap=""nowrap"" style=""white-space:pre; text-align:right""><input type=""button"" accesskey=""V"" id=""View"" name=""View"" title=""View"" value=""View"" onclick="""&rsBU(0)&"("&HISTORYID&",'"&rsBU("SESSIONID")&"')"" /></td>"
                            End If
                         TBL = TBL & " </tr>"
					       rsBU.moveNext
					    Wend
					      TBL = TBL & "</tbody>" &_
                         "<tfoot>" &_
                         "<tr>" &_
                             "<td colspan=""4"">" &_
                                 "&nbsp;" &_
                             "</td>" &_
                         "</tr>" &_
                         "</tfoot>" &_
                     "</table>" &_
                     "</div>"
					Else
					    rw "No Records"
					End If
    				'BuildJourney = TBL
		    End With
	    objConn.Close
    Set objConn = Nothing

End Function

Call Record_Search(CUSTOMER_REFERENCE)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

-->
</style>

<style type="text/css" media="screen">

.black_overlay
{
	display: none;
	position: absolute;
	top:0px; left:0px;
	width: 100%; height: 100%;
	background-color: #BDBDBD;
	z-index:1001;
	overflow: hidden;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}

.white_content
{
	display: none;
	position: absolute;
	top: 25%; left: 10%;
	width: 80%;	height: 50%;
	padding: 16px;
	border: 1px solid #cc0000;
	background-color: white;
	z-index:1002;
	overflow: hidden;
}

.white_content_referral
{
	display: none;
	position: absolute;
	top: 25%; left: 10%;
	width: 80%;	height: 190px;
	padding: 16px;
	border: 1px solid #cc0000;
	background-color: white;
	z-index:1002;
	overflow: hidden;
}

</style>

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" src="/ClientScripting/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">
<!--
var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}


/*function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}*/


function buildQueryString(theFormName) {

  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            
            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function BuildXMLReport(str_url,elId,elED,elheightdefault,elheight){

	//set the loading parameter
    document.getElementById(elED).innerHTML = "<div style='float:left; padding:10px;'>Please wait. Loading...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById(elED).className = "no_error"
    document.getElementById("light").style.height = elheightdefault+"px";
	//call xmlhttp functions
	var strURL = str_url
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  +
									   str_url + "' could not be found.");
							   document.getElementById(elED).innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById(elED).innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById(elED).innerHTML = ""
							   }
							   // Call the desired result function
							   else {
							            //alert(elId)
							           //document.getElementById("light").className = "white_content_referral";
									   document.getElementById("light").style.height = elheight+"px";
									   document.getElementById(elId).innerHTML = strResponse;
									   document.getElementById(elED).innerHTML = "";
									   document.getElementById(elED).className = "";
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}
//-->
</script>

<script type="text/javascript" language="javascript">

function GoTo(strURL)
{
    window.location.href = strURL;
}


function Cancel()
{
    LightBox();
    document.getElementById("light").style.height = "50px"
    document.getElementById("light_response").innerHTML = ""
    document.getElementById("light_error").innerHTML = ""
}

function Close()
{
   Cancel()
}

function LightBox()
{
    var thearray= new Array("light","fade");
    for(i=0; i<thearray.length; i++)
    {
        toggle(thearray[i])
    }
}


function toggle(id)
{
	var state = document.getElementById(id).style.display;
	if (state == 'block')
	{
		document.getElementById(id).style.display = 'none';
	} 
	else
	{
		document.getElementById(id).style.display = 'block';
	}
}

var FormFields = new Array()

function gg(ED)
{
    FormFields[0] = "sel_RFORGANISATION|Refer From : Organisation|SELECT|Y"
    FormFields[1] = "sel_RFCONTACT|Refer From : Contact|SELECT|N"
    FormFields[2] = "txt_RTDATE|Refer To : Date of Referral|DATE|Y"
    FormFields[3] = "sel_RTORGANISATION|Refer To : Organisation|SELECT|Y"
    FormFields[4] = "sel_RTCONTACT|Refer To : Contact|SELECT|N"
    FormFields[5] = "txt_NOTES|Notes|TEXT|N"

	if (!checkForm(ED)) return;
	document.Form1.action = '/CRMS/svr/ReferaClient_svr.asp'+ buildQueryString('Form1')
	document.Form1.target = "iFrame<%=TimeStamp%>" //
 	document.Form1.submit()
 	//document.getElementsByTagName( 'form' )[0].submit();
}


function FnCP(HID,SID,action)
{
    LightBox();
    setTimeout((function() {BuildXMLReport('ClientPriorities.asp?HID='+HID+'&SID='+SID+'&action=update','light_response','light_error',50,190)}), 1000);
}

function FnR(HID,JID, action)
{
    LightBox();
    setTimeout((function() {BuildXMLReport('ClientReferral.asp?JID='+JID+'&action=update','light_response','light_error',50,365)}), 1000);
}

function FnAA(HID,SID,action)
{
    LightBox();
    setTimeout((function() {BuildXMLReport('ClientAppointment.asp?HID='+HID+'&SID='+SID+'&action=update','light_response','light_error',50,280)}), 1000);
}

function Appointment(SID)
{
    LightBox('light','fade');
    setTimeout((function() {BuildXMLReport('svr/SaveAnAppointment.asp?SITEID='+SID,'light_response','light_error')}), 100);
}


</script>

<script type="text/javascript" language="javascript">

function DynamicLayerReSize()
{

//-- Size

  var myWidth = 0, myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }

//-- Scroll

var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }

   var mydiv = document.getElementById("fade");
      mydiv.style.height = myHeight + scrOfY
      mydiv.style.width = myWidth
}


</script>

</head>
<body lang="en" onload="DynamicLayerReSize()" onresize="DynamicLayerReSize()">
<script type="text/javascript" src="/ClientScripting/wz_tooltip.js"></script>
<div id="fade" class="black_overlay"></div>
<div id="light" class="white_content">
    <div id="light_error" style="float:left;width:100%;"></div>
    <div id="light_response" style="float:left;width:100%;"></div>
</div>
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		    <div style="float:left; width:100%; white-space:nowrap;">
		        <div style="float:left">
		            <h1><%=FULLNAME%></h1>
		        </div>
		        <div style="float:left">
		            <ul class="navmenu2">
		                <li><a href="JavaScript:GoTo('ClientJourney.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Journey">Journey</a></li>
		                <li><a href="JavaScript:GoTo('CurrentPriorities.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Priorities">Current Priorities</a></li>
		                <li><a href="JavaScript:GoTo('CurrentContacts.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Contacts">Current Contacts</a></li>
		                <li><a href="JavaScript:GoTo('SelfAssessment.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Contacts">Self Assessment</a></li>
		                <li><a href="JavaScript:GoTo('Appointments.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Appointments">Appointments</a></li>
		                <li><a href="JavaScript:GoTo('ClientDetails.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Details">Details</a></li>
		            </ul>
		        </div>
		    </div>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red;">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div style="float:left; width:100%">
			        <div id="nsform" style="float:left; width:96%; border:solid 0px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                        <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>><%=passcheck_text%></div>
                        <%=TBL %>                        
                    </div>
			    </div>
			</div>
			
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif" width="10px" height="10px" name="FVS_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif" width="10px" height="10px" name="FVW_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>