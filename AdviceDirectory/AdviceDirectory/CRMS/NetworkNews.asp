﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%

Function stringReplace(strSearchWithin,strSearchFor)
If Len(strSearchWithin) > 0 And Len(strSearchFor) > 0 Then
intStart = 1
intFound = InStr(intStart,strSearchWithin,strSearchFor,1)

Do While intFound > 0
strReplaced = strReplaced & Mid(strSearchWithin,intStart,intFound - intStart) & "<span class=""higlight"">" & mid(strSearchWithin,intFound,len(strSearchFor)) & "</span>"
intStart = intFound + len(strSearchFor)
intFound = InStr(intStart,strSearchWithin,strSearchFor,1)
Loop

stringReplace = strReplaced & Mid(strSearchWithin,intStart)
Else
stringReplace = strSearchWithin
End If

End Function

Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
	
	If Request("q") <> "" Then
        SEARCHSTRING = Request("q")
    Else
        SEARCHSTRING = Request("txt_SEARCH")
    End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.higlight
{
background-color:yellow
}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
  xmlhttp = new XMLHttpRequest();
}

function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
        qs+=(qs=='')?'?':'&'
        qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
	    }
      }
    }
  return qs
}

function BuildXMLReport(str_url){

	//set the loading parameter
    document.getElementById("errordiv").innerHTML = "<div style='float:left; padding:10px;'>Please wait while your results are loading</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById("errordiv").className = "no_error"
	//call xmlhttp functions
	var currentTime = new Date()
	str_url = str_url
	var strURL = ''+str_url+'&dt='+currentTime
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   str_url + "' could not be found.");
							   document.getElementById("errordiv").innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById("errordiv").innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById("errordiv").innerHTML = ""
							   }
							   // Call the desired result function
							   else {
							            if (strResponse == 'False')
							            {
							                window.location.href = '/crms/login.asp'
							            }
							            else
							            {
									        document.getElementById("Search_Restults").innerHTML = strResponse
									        document.getElementById("errordiv").innerHTML = ""
									        document.getElementById("errordiv").className = ""
									   }
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}


function GoTo(strURL)
{
    window.location.href = strURL;
}


function HTTPgo()
{
    go(document.Form1)
}


function go(frm)
{
    //document.getElementById("Search_Restults").innerHTML = ""
    if (checkMyForm(frm) == false)
    {
       return false;
    }
    //return true;
    document.Form1.frm.value = 1;
    //document.Form1.action = "NetworkNews.asp"
	//document.Form1.target = "_self"
 	document.Form1.submit()
    //BuildXMLReport('/crms/FindaClient2_svr.asp'+ buildQueryString('Form1'))
}


var FormFields = new Array()

function gg(frm)
{
    FormFields[0] = "txt_SEARCH|Search|TEXT|Y"

	if (!checkForm('FrmEr_Edit')) return;
	document.Form1.action = "/CRMS/svr/EditaClient_svr.asp"
	document.Form1.target = "iFrame<%=TimeStamp%>"
 	document.Form1.submit()
}

function AddNews()
{
   window.location.href = "NetworkNews_Add.asp";
}

function EditNews()
{
   window.location.href = "NetworkNews_Amend.asp";
}

function DeleteNews()
{
   window.location.href = "NetworkNews_Delete.asp";
}

</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="margin-right:10px;">
		<!-- Start Content -->
		<div id="maincontent" style="background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
			<div><%=PAGE_CONTENT%></div>
			<br />
			<div>
			    <div style="width:99%">
			        <div id="nsform" style="width:100%; border:solid 1px #cc0000; padding-bottom:1em;">
                        <div id="errordiv" style="margin:10px; padding:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
                        <div style="width:100%;">
                            <fieldset>
	                            <legend>Network News Search</legend>
		                        <div id="Frm" style="width:48%; float:left">
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_SEARCH" accesskey="F">
                                                <span style="text-decoration:underline; cursor:pointer">S</span>earch :
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white"
                                                onblur="this.className='border-white'"
                                                onfocus="this.className='border-red'"
                                                type="text"
                                                size="40"
                                                name="txt_SEARCH"
                                                id="txt_SEARCH"
                                                maxlength="100"
                                                value="<%=SEARCHSTRING%>"
                                                tabindex="1"
                                                required="Y" validate="/([a-zA-Z0-9,\s]{1,100})$/"
                                                validateMsg="The field search should contain a keyword greater than 3 characters in length." />
                                            <img src="/js/FVS.gif" name="img_SEARCH" id="img_SEARCH" width="15px" height="15px" alt="" />
                                            <input class="submit" type="submit" name="Search" id="Search" value="Search" title="Submit Button : Search News" style="width:auto"/>
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">&nbsp;</span>
                                        <span class="formw">
                                            </span>
                                    </div>
                                </div>
                                <div style="width:48%; float:right">
                                    <span style="float:right; padding-right:20px">
                                        <input type="button" id="BtnAdd" name="BtnAdd" value="Add News" onclick="AddNews()" />
                                        <input type="button" id="BtnEdit" name="BtnEdit" value="Edit News" onclick="EditNews()" />
                                        <input type="button" id="ButDelete" name="ButDelete" value="Delete News" onclick="DeleteNews()" />
                                    </span>
                                </div>
	                        </fieldset>
                        </div>
                        
                        <div style="border:solid 0px green">
                        <%
                         ' BEGIN RUNTIME CODE
                        ' Declare our vars
                        'Dim strSQL          'SQL command to execute
                        Dim iPageSize       'How big our pages are
                        Dim iPageCount      'The number of pages we get back
                        Dim iPageCurrent    'The page we want to show
                        Dim strOrderBy      'A fake parameter used to illustrate passing them
                        Dim objPagingConn   'The ADODB connection object
                        Dim objPagingRS     'The ADODB recordset object
                        Dim iRecordsShown   'Loop controller for displaying just iPageSize records
                        Dim I               'Standard looping var

                        ' Get parameters
                        iPageSize = 3 ' You could easily allow users to change this

                        ' Retrieve page to show or default to 1
                        If Request.QueryString("page") = "" Then
	                        iPageCurrent = 1
                        Else
	                        iPageCurrent = CInt(Request.QueryString("page"))
                        End If

                        'SEARCHSTRING = Request("txt_SEARCH")

                        ' Make sure the input is one of our fields.
                        strOrderBy = LCase(Request.QueryString("order"))
                        'Select Case strOrderBy
                        '	Case "last_name", "first_name", "sales"
                        '		' A little pointless, but...
                        '		strOrderBy = strOrderBy
                        '	Case Else
		                        strOrderBy = "NEWSID"
                        'End Select

                        ' Build our SQL String using the parameters we just got.
                        'strSQL = "SELECT *, DATENAME(M, NEWSTIMESTAMP) AS NEWSDATE, FIRSTNAME+SPACE(1)+LASTNAME AS FULLNAME, PROVIDERNAME " &_
                        '        "FROM CRMS_NETWORKNEWS N WITH (NOLOCK) " &_ 
                        '        "INNER JOIN DBO.TBL_MD_LOGIN L ON N.LOGINID = L.LOGINID " &_
                        '        "INNER JOIN DBO.TBL_MD_PROVIDER P ON L.PROVIDERID = P.PROVIDERID " &_
                        '        "WHERE NEWSHEADLINE LIKE '%" & SEARCHSTRING & "%' OR NEWSCONTENT LIKE '%" & SEARCHSTRING & "%' " &_ 
                        '        "ORDER BY NEWSTIMESTAMP DESC, " & strOrderBy & ";"

                        'If SEARCHSTRING <> "" Then
                            strSQL = "SELECT *, CAST(DATEPART(DAY, NEWSDATE) AS VARCHAR(2)) + SPACE(1) + DATENAME(M, NEWSDATE) + SPACE(1) + CAST(DATEPART(YEAR, NEWSDATE) AS VARCHAR(4)) AS PUBLISHED, " &_
                                       " CONVERT(char(10), NEWSDATE, 101) AS PUBLISHED_DATE, " &_
                                       " N.NEWSDATE AS NEWSDATE, " &_
                                       " DATENAME(M, N.NEWSDATE) AS NEWSMONTH, " &_
                                       " FIRSTNAME+SPACE(1)+LASTNAME AS FULLNAME, PROVIDERNAME " &_
                                "FROM CRMS_NETWORKNEWS N WITH (NOLOCK) " &_
                                "INNER JOIN DBO.TBL_MD_PROVIDER P ON N.PROVIDERID = P.PROVIDERID " &_
                                "LEFT JOIN DBO.TBL_MD_LOGIN L ON N.LOGINID = L.LOGINID " &_
                                "WHERE NEWSACTIVE = 1 AND (NEWSHEADLINE LIKE '%" & SEARCHSTRING & "%' OR NEWSCONTENT LIKE '%" & SEARCHSTRING & "%') " &_ 
                                "ORDER BY N.NEWSDATE DESC, " & strOrderBy & ";"
                        'Else
                        '    strSQL = "SELECT TOP 3 *, DATENAME(M, NEWSTIMESTAMP) AS NEWSDATE, FIRSTNAME+SPACE(1)+LASTNAME AS FULLNAME, PROVIDERNAME " &_
                        '        "FROM CRMS_NETWORKNEWS N WITH (NOLOCK) " &_ 
                        '        "INNER JOIN DBO.TBL_MD_LOGIN L ON N.LOGINID = L.LOGINID " &_
                        '        "INNER JOIN DBO.TBL_MD_PROVIDER P ON L.PROVIDERID = P.PROVIDERID " &_
                        '        "WHERE NEWSHEADLINE LIKE '%" & SEARCHSTRING & "%' OR NEWSCONTENT LIKE '%" & SEARCHSTRING & "%' " &_ 
                        '        "ORDER BY NEWSTIMESTAMP DESC, " & strOrderBy & ";"
                        'End If
                        
                        ' Create and open our connection
                        Set objPagingConn = Server.CreateObject("ADODB.Connection")
                        objPagingConn.Open DSN_CONNECTION_STRING
                        ' Create recordset and set the page size
                        Set objPagingRS = Server.CreateObject("ADODB.Recordset")
                        objPagingRS.PageSize = iPageSize
                        ' You can change other settings as with any RS
                        'objPagingRS.CursorLocation = adUseClient
                        objPagingRS.CacheSize = iPageSize
                        ' Open RS
                        objPagingRS.Open strSQL, objPagingConn, adOpenStatic, adLockReadOnly, adCmdText
                        ' Get the count of the pages using the given page size
                        iPageCount = objPagingRS.PageCount
                        ' If the request page falls outside the acceptable range,
                        ' give them the closest match (1 or max)
                        If iPageCurrent > iPageCount Then iPageCurrent = iPageCount
                        If iPageCurrent < 1 Then iPageCurrent = 1
                        ' Check page count to prevent bombing when zero results are returned!
                        If iPageCount = 0 Then
	                        Response.Write "No records found!"
                        Else
	                        ' Move to the selected page
	                        objPagingRS.AbsolutePage = iPageCurrent
	                        ' Start output with a page x of n line
	                        %>
	                        <%
                        	
	                        ' Show "previous" and "next" page links which pass the page to view
                        ' and any parameters needed to rebuild the query.  You could just as
                        ' easily use a form but you'll need to change the lines that read
                        ' the info back in at the top of the script.
                        If iPageCurrent > 1 Then
	                        %>
	                        <a href="NetworkNews.asp?q=<%=Server.URLEncode(SEARCHSTRING)%>&page=<%= iPageCurrent - 1 %>&order=<%= Server.URLEncode(strOrderBy) %>">[&lt;&lt; Prev]</a>
	                        <%
                        End If

                        ' You can also show page numbers:
                        'If iPageCount <> 0 Then
                        '    rw "Page 1 of <br/>"
                        'End If

                        For I = 1 To iPageCount
	                        If I = iPageCurrent Then
		                        %>
		                        <% If iPageCount = 1 Then rw "" Else rw I %>
		                        <%
	                        Else
		                        %>
		                        <a href="NetworkNews.asp?q=<%=Server.URLEncode(SEARCHSTRING)%>&page=<%= I %>&order=<%= Server.URLEncode(strOrderBy) %>"><%= I %></a>
		                        <%
	                        End If
                        Next 'I

                        If iPageCurrent < iPageCount Then
	                        %>
	                        <a href="NetworkNews.asp?q=<%=Server.URLEncode(SEARCHSTRING)%>&page=<%= iPageCurrent + 1 %>&order=<%= Server.URLEncode(strOrderBy) %>">[Next &gt;&gt;]</a>
	                        <%
                        End If
                        	
	                        ' Spacing
	                        'Response.Write vbCrLf
	                        ' Continue with a title row in our table
	                        'Response.Write "<table border=""1"">" & vbCrLf
	                        ' Show field names in the top row
	                        'Response.Write vbTab & "<tr>" & vbCrLf
	                        'For I = 0 To objPagingRS.Fields.Count - 1
	                        '    Response.Write vbTab & vbTab & "<th>"
	                        '    Response.Write objPagingRS.Fields(I).Name
	                        '    Response.Write "</th>" & vbCrLf
	                        'Next 'I
	                        'Response.Write vbTab & "</tr>" & vbCrLf
	                        ' Loop through our records and ouput 1 row per record
	                        
	                        NEWSMONTH = "Nothing"
	                        
	                        iRecordsShown = 0
	                        rw "<div style=""padding:1em; border:solid 0px red"">"
	                        Do While iRecordsShown < iPageSize And Not objPagingRS.EOF	

	                        NEWSHEADLINE = stringReplace(objPagingRS.Fields("NEWSHEADLINE"),SEARCHSTRING)
                            NEWSCONTENT = stringReplace(objPagingRS.Fields("NEWSCONTENT"),SEARCHSTRING)
                                                        
                            If NEWSMONTH = "" then                            
                               NEWSMONTH = ""                            
                            Else                            
                                If NEWSMONTH <> objPagingRS.Fields("NEWSMONTH") Then
                                    NEWSMONTH = objPagingRS.Fields("NEWSMONTH")
                                Else
                                    NEWSMONTH = ""
                                End If                                
                                If IsDate(NEWSMONTH) Then
                                    NEWSMONTH = CDate(NEWSMONTH)
                                End If                            
                            End If

                                rw "<div style=""float:left; width:100%; border:solid 0px red; padding-bottom:1em"" id=""NEWSID_" & objPagingRS.Fields("NEWSID") & """>" & vbCrLf
                                        rw "<span style=""padding: 3px 3px 0px 3px; font-size: 150%; color:gray"">" & NEWSMONTH & "</span><br/>" & vbCrLf
                                        rw "<div style=""float:left; width:100%; border:solid 0px red""><img style=""float: left; padding: 3px 10px 0px 3px;"" src=""NetworkNewsPicture.asp?NEWSID=" & objPagingRS.Fields("NEWSID") & """ width=""80"" height=""80"" /><b>" & NEWSHEADLINE & "</b><br/><span style=""line-height:1.95em; color:gray; font-weight:bold; border:0px solid red; white-space:nowrap; float:left "">Date : " & objPagingRS.Fields("PUBLISHED") & " by " & objPagingRS.Fields("FULLNAME") & ", " & objPagingRS.Fields("PROVIDERNAME") & "</span><span style=""float:right""><a href=""NetworkNewsArticle.asp?NEWSID=" & objPagingRS.Fields("NEWSID") & """>Read the full article</a></span><br/><br/>" & NEWSCONTENT &" </div>" & vbCrLf
                                        rw "" & vbCrLf
                                rw "</div>" & vbCrLf
                                rw "<div style=""clear:left""></div>" & vbCrLf
		                        ' Increment the number of records we've shown
		                        iRecordsShown = iRecordsShown + 1
		                        ' Can't forget to move to the next record!
		                        objPagingRS.MoveNext
	                        Loop
                            rw "</div>"
	                        ' All done - close table
	                        'Response.Write "</table>" & vbCrLf
                        End If

                        ' Close DB objects and free variables
                        objPagingRS.Close
                        Set objPagingRS = Nothing
                        objPagingConn.Close
                        Set objPagingConn = Nothing

                        ' END RUNTIME CODE
                        %>                     
                        </div>
                        
                    </div>
			    </div>
			</div>
			</form>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>