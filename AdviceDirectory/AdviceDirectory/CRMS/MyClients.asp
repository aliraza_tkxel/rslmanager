﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")
%>
<%
Dim ADVISER_REFERENCE
    ADVISER_REFERENCE = nulltest(Session("svLOGINID"))
Dim ORGANISATION_REFERENCE 
    ORGANISATION_REFERENCE = nulltest(Session("svPROVIDERID"))

'If (NullNestTF(ADVISER_REFERENCE) = True) Then
'    Response.Redirect ("/crms/login.asp")
'Else
'    If NOT IsNumeric(ADVISER_REFERENCE) Then
'        Response.Redirect ("/crms/NoRecord.asp?Er=1")
'    End If
'End If
%>
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%
Function MyReferrals(ADVISER_REFERENCE)

   SQL = " SELECT L.FIRSTNAME+SPACE(1)+L.LASTNAME AS RVIEWADVISERNAME, REFERRALID, RTDATE AS REFERRED, C.CUSTOMERID, C.FIRSTNAME+SPACE(1)+C.SURNAME AS FULLNAME, c.DOB, CD.POSTCODE, PROVIDERNAME AS REFERREDFROM, AO.DESCRIPTION AS PRIORITY, R.RVIEWDATE, R.RVIEWBY " &_
         " FROM DBO.CRMS_REFERRAL R " &_
         " INNER JOIN DBO.CUSTOMERID C ON C.CUSTOMERID = R.CUSTOMERID " &_
         " INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID " &_
         " INNER JOIN DBO.TBL_MD_PROVIDER P ON P.PROVIDERID = R.RFORGANISATION " &_
         " INNER JOIN dbo.ASSESSMENT_OPTIONS AO ON AO.ID = R.ASSESSMENTOPTIONID " &_
         " LEFT JOIN dbo.TBL_MD_LOGIN L ON R.RVIEWBY = L.LOGINID " &_
         " WHERE ((R.RTCONTACT = " & ADVISER_REFERENCE & " AND RTORGANISATION = " & ORGANISATION_REFERENCE & ") OR (RTORGANISATION = " & ORGANISATION_REFERENCE& " AND R.RTCONTACT IS NULL))"

Call OpenDB()
Call OpenRs(rsPage,SQL)
If rsPage.EOF Then

            strResult = strResult & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""5"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Referred</th>" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th>From</th>" & vbCrLf &_
                                        " <th>Priority</th>" & vbCrLf &_
                                        " <th>Viewed</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult = strResult & "<tr>" &_
	    "<td valign=""top"" colspan=""6"">No Referrals Exist</td>" &_
	"</tr>"
	strResult = strResult & "</tbody>" & vbCrLf
	strResult = strResult & "<tfoot>" & vbCrLf
	
Else
        strResult = strResult & "<br/>" &_
                                "<span style=""font-weight:bold; color:#cc0000"">Referred</span>" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px""></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Referred</th>" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th>From</th>" & vbCrLf &_
                                        " <th>Priority</th>" & vbCrLf &_
                                        " <th>Viewed</th>" & vbCrLf &_
                                        " <th></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"

	    Do until rsPage.EOF

            REFERRALID = rsPage("REFERRALID")
            Referred = rsPage("REFERRED")
		    Name = rsPage("FULLNAME")
		    DOB = rsPage("DOB")
		    POSTCODE = rsPage("POSTCODE")
		    REFERREDFROM = rsPage("REFERREDFROM")
		    PRIORITY = rsPage("PRIORITY")

            RVIEWDATE = rsPage("RVIEWDATE")
            RVIEWADVISERNAME = rsPage("RVIEWADVISERNAME")

            If IsNull(RVIEWDATE) Then
                RVIEWDATE = "No"
                RecordView = "&RV="&REFERRALID&""
            Else
                RVIEWDATE = "Yes : (" & rsPage("RVIEWDATE") & " : " & RVIEWADVISERNAME & ")"
            End If

		    strResult = strResult & "<tr>" & vbCrLf &_
		    "<td>" & Referred &"</td>" & vbCrLf &_
		    "<td>" & Name & "</td>" & vbCrLf &_
		    "<td>" & DOB & "</td>" & vbCrLf &_
		    "<td>" & POSTCODE & "</td>" & vbCrLf &_
		    "<td>" & REFERREDFROM & "</td>" & vbCrLf &_
		    "<td>" & PRIORITY & "</td>" & vbCrLf &_
		    "<td>" & RVIEWDATE & "</td>" & vbCrLf &_
		    "<td align=""right""><ul class=""navmenu2""><li style=""float:right""><a href=""JavaScript:GoTo('CurrentContacts.asp?CUSTOMERID="&rsPage("CUSTOMERID")&RecordView&"')"" title=""View Customer Details"">View</a></li></ul></td>" & vbCrLf &_
		    "</tr>" 
'-- SITE , PRIORITY ITEM E.G. HOUSING, PRIORITY ORDER (SPECIFIED BY USER)
'"<td align=""right""><ul class=""navmenu2""><li style=""float:right""><a href=""JavaScript:ReferaClient("&rsPage("REFERRALID")&")"" title=""View Referral Details"">View</a></li></ul></td>" & vbCrLf &_
	        rsPage.MoveNext
	    Loop

	    rsPage.Close
	set rsPage = Nothing

	'totalrows = cmd.Parameters("@TOTALROWS").Value	

	strResult = strResult & "</tbody>" & vbCrLf	
	strResult = strResult & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""7""></td></tr>"

    'if cint(page) > cint(0) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	'end if	
	'if cint(page) + cint(maxrows) < cint(totalrows) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	'end if

End if

    strResult = strResult & "</tfoot>" & vbCrLf &_
	                        "</table>"
    MyReferrals = strResult


'Call CloseDB()

End Function


Function MyCustomers(ADVISER_REFERENCE)

SQL = " SELECT " &_
    "C.CUSTOMERID AS CUSTOMERID, C.FIRSTNAME+SPACE(1)+C.SURNAME AS CUSTOMER_NAME " &_
    ",DOB " &_
    ",POSTCODE " &_
    ",J.ACTIONTAKEN " &_
    ",J.DATERECORDED " &_
    ",J.CREATEDBYNAME " &_
    ",IsNull(CP.DESCRIPTION,'Not Available') AS PRIORITY " &_
    "FROM dbo.CUSTOMERID C " &_
    "INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID " &_
    "LEFT JOIN DBO.TBL_MD_LOGIN L ON c.LOGINID = L.LoginID " &_
    "LEFT JOIN ( " &_
	"    SELECT AO.DESCRIPTION, CP.CUSTOMERID " &_
	"    FROM CUSTOMERID_PRIORITIES CP " &_
	"    INNER JOIN dbo.ASSESSMENT_OPTIONS AO ON CP.ASSESSMENTOPTIONID = AO.ID " &_
	"    WHERE CP.PRIORITY = 1 " &_
    ") CP ON C.CUSTOMERID = CP.CUSTOMERID " &_
    "INNER JOIN VW_JOURNAL J ON C.CUSTOMERID = J.CUSTOMERID " &_
    "INNER JOIN " &_
    "( " &_
    "SELECT MAX(DATERECORDED) AS DATERECORDED, CUSTOMERID  " &_
    "FROM VW_JOURNAL " &_
    "WHERE CUSTOMERID IS NOT NULL " &_
    "GROUP BY CUSTOMERID " &_
    ") " &_
    "J2 ON J2.DATERECORDED = J.DATERECORDED AND J.CUSTOMERID = J2.CUSTOMERID " &_
    "WHERE L.LoginID = "&ADVISER_REFERENCE&" "

Call OpenDB()
Call OpenRs(rsPage2,SQL)
If rsPage2.EOF Then
            strResult2 = strResult2 & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th colspan=""2"">Last Action</th>" & vbCrLf &_
                                        " <th>By</th>" & vbCrLf &_
                                        " <th>Current Priority</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult2 = strResult2 & "<tr>" &_
	    "<td valign=""top"" colspan=""8"">No Data Exists</td>" &_
	"</tr>" 
	strResult2 = strResult2 & "</tbody>" & vbCrLf	
	strResult2 = strResult2 & "<tfoot>" & vbCrLf
Else
        strResult2 = strResult2 & "<br/>" &_
                                "" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px""></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Name</th>" & vbCrLf &_
                                        " <th>DoB</th>" & vbCrLf &_
                                        " <th>Postcode</th>" & vbCrLf &_
                                        " <th colspan=""2"">Last Action</th>" & vbCrLf &_     
                                        " <th>By</th>" & vbCrLf &_
                                        " <th>Current Priority</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"

	    Do until rsPage2.EOF

            CUSTOMERID = rsPage2("CUSTOMERID")
		    Name = rsPage2("CUSTOMER_NAME")
		    DOB = rsPage2("DOB")
		    POSTCODE = rsPage2("POSTCODE")
		    ACTIONTAKEN = rsPage2("ACTIONTAKEN")
		    CREATEDBYNAME = rsPage2("CREATEDBYNAME")
		    DATERECORDED = rsPage2("DATERECORDED")
		    PRIORITY = rsPage2("PRIORITY")

		    strResult2 = strResult2 & "<tr onclick=""GoTo('ClientJourney.asp?CUSTOMERID="&CUSTOMERID&"')"">" & vbCrLf &_
		        "<td>" & Name           & "</td>" & vbCrLf &_
		        "<td>" & DOB            & "</td>" & vbCrLf &_
		        "<td>" & POSTCODE       & "</td>" & vbCrLf &_
		        "<td>" & DATERECORDED   & "</td>" & vbCrLf &_
		        "<td>" & ACTIONTAKEN    & "</td>" & vbCrLf &_
		        "<td>" & CREATEDBYNAME  & "</td>" & vbCrLf &_
		        "<td>" & PRIORITY       & "</td>" & vbCrLf &_
		       "</tr>" 
            rsPage2.MoveNext
	    Loop

	    rsPage2.Close
	set rsPage2 = Nothing

	strResult2 = strResult2 & "</tbody>" & vbCrLf	
	strResult2 = strResult2 & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""7""></td></tr>"

End if

    strResult2 = strResult2 & "</tfoot>" & vbCrLf &_
	                        "</table>"
    MyCustomers = strResult2

'Call CloseDB()

End Function

Dim RefString
    RefString = MyReferrals(ADVISER_REFERENCE)
Dim CusString
    CusString = MyCustomers(ADVISER_REFERENCE)
    
    Set rsLang = server.CreateObject("ADODB.Recordset")
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "CRMS_ORGANISATIONS"
      set .ActiveConnection=conn
      set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, ADVISER_REFERENCE)
      .parameters.append param
      Set rsProvider = .execute 
    end with

    Call OpenDB()
    Call BuildSelect_SP(strProvider,"sel_Provider",rsProvider,"Please Select",null,"width:auto",null,"onchange=""DrawFieldItem('sel_Adviser')""")
    Call CloseDB()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:5px; padding-bottom:5px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

.no_error2
{
	color:#cc0000;
	border: solid 1px #cc0000;
	margin-bottom:10px;
	height:35px;
}

-->
</style>

<style type="text/css" media="screen">

.black_overlay
{
	display: none;
	position: absolute;
	top:0px; left:0px;
	width: 100%; height: 100%;
	background-color: #BDBDBD;
	z-index:1001;
	overflow: hidden;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}

.white_content
{
	display: none;
	position: absolute;
	top: 25%; left: 10%;
	width: 80%;	height: 50%;
	padding: 16px;
	border: 1px solid #cc0000;
	background-color: white;
	z-index:1002;
	overflow: hidden;
}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">
<!--
var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}

/*
function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}
*/

function buildQueryString(theFormName) {

  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            
            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function BuildXMLReport(str_url,elId,elED){

	//set the loading parameter
    document.getElementById(elED).innerHTML = "<div style='float:left; padding:10px;'>Please wait. Loading...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById(elED).className = "no_error2"
	//call xmlhttp functions
	var strURL = str_url
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  +
									   str_url + "' could not be found.");
							   document.getElementById(elED).innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById(elED).innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById(elED).innerHTML = ""
							   }
							   // Call the desired result function
							   else {
									   document.getElementById(elId).innerHTML = strResponse
									   document.getElementById(elED).innerHTML = ""
									   document.getElementById(elED).className = ""
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}
//-->
</script>

<script type="text/javascript" language="javascript">

function GoTo(strURL)
{
    window.location.href = strURL;
}


function Cancel()
{
    LightBox();
    document.getElementById("light_response").innerHTML = ""
    document.getElementById("light_error").innerHTML = ""
}


function LightBox()
{
    var thearray= new Array("light","fade");
    for(i=0; i<thearray.length; i++)
    {
        toggle(thearray[i])
    }
}


function toggle(id)
{
	var state = document.getElementById(id).style.display;
	if (state == 'block')
	{
		document.getElementById(id).style.display = 'none';
	} 
	else
	{
		document.getElementById(id).style.display = 'block';
	}
}

function ChangeMe(blar)
{
    //BuildXMLReport('svr/lb-clients.asp'+ buildQueryString('Form1'),'loader','sel_Adviser')
    //LightBox();
    setTimeout((function() {BuildXMLReport('svr/lb-clients.asp'+ buildQueryString('Form1'),'DivRes','loader')}), 100);
}

function HTTPDrawFieldItem(FieldItem, strURL)
{
	//call xmlhttp functions
    var field = document.getElementById(FieldItem);
    if (field)
    {
        for(var i=field.length-1; i>=0; i--)
        {
            field.options[i]=null 
        }
        document.getElementById(FieldItem).add(new Option("Retrieving Data....",null));

	    xmlhttp.open("GET",strURL,true);
 	    xmlhttp.onreadystatechange=function()
 	    {
		    if (xmlhttp.readyState == 4)
		    {
		       strResponse = xmlhttp.responseText;
		       switch (xmlhttp.status)
		       {
				       // Page-not-found error
				       case 404:
						       alert("Error: Not Found. The requested URL could not be found.");
						       document.getElementById(FieldItem).outerHTML = " "
						       break;
				       // Display results in a full window for server-side errors
				       case 500:
						       alert("Error: Server Error. An unexpected errror 500 has occurred.");
						       break;
				       default:
						        // Call JS alert for custom error or debug messages
						        if (strResponse.indexOf('Error:') > -1 || strResponse.indexOf('Debug:') > -1) 
							    {
								    alert("Error: A JavaScript Error has been encountered. " + strResponse);
								    document.getElementById(FieldItem).outerHTML = " "
						        }
						       // Call the desired result function
						       else 
						       {
								       document.getElementById(FieldItem).outerHTML = strResponse
								      // try {
								    //	alert('hum!')
								    //	   }
								    //	catch(e){
								    //		}
						       }
						       break;
			        }
	            }
 	        }
        xmlhttp.send(null) 
    }
}

function DrawFieldItem(FieldItem)
{
	HTTPDrawFieldItem(FieldItem,'svr/lb.asp' + buildQueryString('Form1'))
}

</script>

</head>
<body lang="en">
<form id="Form1" name="Form1">
<div id="fade" class="black_overlay"></div>
<div id="light" class="white_content">
    <div id="light_error" style="float:left;width:100%;"></div>
    <div id="light_response" style="float:left;width:100%;"></div>
</div>
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <!-- ERRORS/PAGE LOAD -->
                    <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>>
                        <%=passcheck_text%>
                    </div>
                    <!-- ERRORS/PAGE LOAD -->
                    <!-- DYNAMIC PAGE LOAD -->
                    <div id="loader"></div>
                    <div id="" style="float:left;width:100%;">
                        <div style="float:left">Organisation : </div>
                        <div style="float:left; padding-left:10px; padding-right:10px"><%=strProvider %></div>
                        <div style="float:left">Adviser : </div>
                        <div id="div_selAdviser" style="float:left; padding-left:10px">
                            <select id="sel_Adviser" name="sel_Adviser" style="width:auto">
                                <option value="">Please Select</option>
                            </select>
                            </div>
                    </div>
                    <div id="DivRes">
                        <div id="DivC">
                            <%=CusString%>
                        </div>
                        <div id="DivR" style="float:left;width:100%;">
                            <%=RefString%>
                        </div>
                    </div>
                    <!-- DYNAMIC PAGE LOAD -->
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
</form>
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>