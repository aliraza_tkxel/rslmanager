Imports System.Data
Imports System.Data.SqlClient
Imports System.io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
'Imports MSDN.SessionPage
'Imports System.Web.UI.Page

Partial Class CRMS_NewTenancyList
    Inherits MSDN.SessionPage

    'Inherits System.Web.UI.Page

    Public Shared Function stringToDate(ByVal str As String) As DateTime

        Return DateTime.ParseExact(str, "dd/MM/yyyy", Nothing)
        Return Nothing

    End Function

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'ResultDetail.Visible = False

        'If user isn't authenticated, he/she will be redirected to Login
        If IsNothing(ASPSession("svLOGINID")) Then
            Response.Redirect("http://www.greatermanchester-anp.org.uk/CRMS")
        End If

        If Not Page.IsPostBack Then

            dpFrom.Text = Date.Today.AddMonths(-1).ToString
            dpTo.Text = Date.Today.ToString
            'Trace.Warn("Page_Load")
            'ResultDetail.Visible = True

            BindDDListBox()

            If selLocalAuthorityArea.SelectedValue = Nothing Then
            Else
                BindReport()
            End If

        End If

    End Sub

    Private Sub BindReport(Optional ByVal e As ExtAspNet.GridSortEventArgs = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()

                    Dim ResidentName As String
                    If Not String.IsNullOrEmpty(txtResidentName.Text) Then
                        ResidentName = txtResidentName.Text.Trim
                    Else
                        ResidentName = Nothing
                    End If

                    Dim LocalAuthorityAreaValue As Nullable(Of Integer)
                    If Not String.IsNullOrEmpty(selLocalAuthorityArea.SelectedValue) Then
                        LocalAuthorityAreaValue = Convert.ToInt32(selLocalAuthorityArea.SelectedValue)
                    End If

                    Dim procFromDate As DateTime = stringToDate(Me.dpFrom.Text)
                    Dim procToDate As DateTime = stringToDate(Me.dpTo.Text)

                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_REPORTS_REFERRAL_ANALYSIS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@LOGINID", ASPSession("svLOGINID"))
                    sqlCmd.Parameters.AddWithValue("@FROMDATE", procFromDate)
                    sqlCmd.Parameters.AddWithValue("@TODATE", procToDate)
                    sqlCmd.Parameters.AddWithValue("@LocalAuthorityAreaId", LocalAuthorityAreaValue)
                    sqlCmd.Parameters.AddWithValue("@ResidentName", ResidentName)

                    Dim dt As New DataTable

                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(dt)

                    Dim dv As New DataView(dt)

                    If e IsNot Nothing Then
                        dv.Sort = String.Format("{0} {1}", e.SortField, e.SortDirection)
                    Else
                        gvReport.CurrentSortColumnIndex = 0
                        gvReport.Columns(0).SortDirection = "DESC"
                    End If

                    dt = dv.ToTable()
                    Cache("RA") = dt
                    gvReport.DataSource = dt
                    gvReport.DataBind()
                    sqlCon.Close()

                End Using
            End Using
        End Using

    End Sub


    Private Sub BindDDListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_LOCALAUTHORITYAREA"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    selLocalAuthorityArea.DataSource = ds.Tables(0)
                    selLocalAuthorityArea.DataTextField = ds.Tables(0).Columns("LocalAuthorityArea").ColumnName.ToString()
                    selLocalAuthorityArea.DataValueField = ds.Tables(0).Columns("LocalAuthorityAreaId").ColumnName.ToString()
                    selLocalAuthorityArea.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Protected Sub gvReport_PageIndexChange(ByVal sender As Object, ByVal e As ExtAspNet.GridPageEventArgs) Handles gvReport.PageIndexChange
        gvReport.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvReport_Sort(ByVal sender As Object, ByVal e As ExtAspNet.GridSortEventArgs) Handles gvReport.Sort
        BindReport(e)
    End Sub

    Protected Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        gvReport.DataBind()
    End Sub

    Protected Sub btnRefresh_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        ResultDetail.Visible = False
        BindReport()
    End Sub

    Protected Sub selLocalAuthorityArea_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selLocalAuthorityArea.PreRender
        If selLocalAuthorityArea.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selLocalAuthorityArea.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selLocalAuthorityArea.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
    End Sub

    Protected Sub gvReport_RowClick(ByVal sender As Object, ByVal e As ExtAspNet.GridRowClickEventArgs)
        'ExtAspNet.Alert.Show(String.Format("You click line: {0}", e.RowIndex + 1))
        'Trace.Warn("gvReport_RowClick")
        Button1_Click(sender, e)
        ResultDetail.Visible = True
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs)

        'Dim sb = New StringBuilder()

        'If (gvReport.SelectedRowIndexArray as System.Nullable(Of Integer) And gvReport.SelectedRowIndexArray.Length > 0) Then
        'sb.Append("<span style=\'font-weight:bold;\'>Following lines were selected: </span>")

        For i As Integer = 0 To gvReport.SelectedRowIndexArray.Length - 1
            Dim RowIndex As Integer
            RowIndex = gvReport.SelectedRowIndexArray(i)
            'ExtAspNet.Alert.Show(String.Format("Customer Reference: {0}", gvReport.DataKeys(RowIndex)(0)))

            'sb.AppendFormat("<br/><span style=\'font-weight:bold;\'>Line {0}: </span>", rowIndex + 1)
            'sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;")
            'For Each obj As Object In gvReport.DataKeys(RowIndex)
            'sb.Append(gvReport.DataKeys(RowIndex)(0).ToString())

            Dim CustomerReference As Nullable(Of Integer)
            CustomerReference = gvReport.DataKeys(RowIndex)(0)
            CustomerId.Value = gvReport.DataKeys(RowIndex)(0)
            'labResult.Text = gvReport.DataKeys(RowIndex)(0)

            BindReport2(CustomerReference)

            'sb.Append(gvReport.DataKeys(gvReport.ExtAspNet.RowIndex).Values("CUSTOMERID").ToString())
            'gvReport.DataKeys(gvReport.SelectedRowIndexArray.GetValue(0).ToString           
            'sb.Append(blar)
            'Dim blar = gvReport.DataKeys[gvReport.RowIndex].Values["CUSTOMERID"].ToString()
            'GridView1.DataKeys[GridView1.RowIndex].Values["job_id"].ToString();
            'sb.Append(obj)

            'sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;")
            'Next
        Next
        'labResult.Text = sb.ToString()
        'End If

    End Sub


    Private Sub BindReport2(ByVal CustomerReference As Integer, Optional ByVal e As ExtAspNet.GridSortEventArgs = Nothing)

        'ResultDetail.Visible = True

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()

                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_REPORTS_REFERRAL_DETAIL"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@CustomerReference", CustomerReference)

                    Dim dt As New DataTable

                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(dt)

                    Dim dv As New DataView(dt)

                    If e IsNot Nothing Then
                        dv.Sort = String.Format("{0} {1}", e.SortField, e.SortDirection)
                    Else
                        Grid1.CurrentSortColumnIndex = 0
                        Grid1.Columns(0).SortDirection = "DESC"
                    End If

                    dt = dv.ToTable()
                    'Cache("RA") = dt
                    Grid1.DataSource = dt
                    Grid1.DataBind()
                    sqlCon.Close()

                End Using
            End Using
        End Using

    End Sub

    Protected Sub Grid1_PageIndexChange(ByVal sender As Object, ByVal e As ExtAspNet.GridPageEventArgs) Handles Grid1.PageIndexChange
        Grid1.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub Grid1_Sort(ByVal sender As Object, ByVal e As ExtAspNet.GridSortEventArgs) Handles Grid1.Sort
        BindReport2(Convert.ToInt32(CustomerId.Value), e)
    End Sub

    Protected Sub Grid1_RowClick(ByVal sender As Object, ByVal e As ExtAspNet.GridRowClickEventArgs)
        'ResultDetail.Visible = True
    End Sub

End Class
