<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim isSaved
    isSaved = False

Dim action
    action = "new"

'TEXTBOXES
Dim FIRSTNAME
Dim SURNAME
Dim DOB
Dim ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3
Dim LAA
Dim POSTCODE
Dim EMAIL
Dim TELEPHONE, MOBILE
Dim NINUMBER
Dim GENDER
Dim DISABILITY
Dim ETHNICITY
Dim BENEFITRECIPIENT
Dim EMPLOYMENTSTATUS
Dim LENGTHOFUNEMPLOYMENT
Dim SECURITYQUESTION
Dim SECURITYANSWER
Dim USERNAME
Dim PASSWORD

'DROP DOWN MENUS
Dim STR_LAA
Dim STR_GENDER
Dim STR_DISABILITY
Dim STR_ETHNICITY
Dim STR_EMPLOYMENTSTATUS
Dim STR_LENGTHOFUNEMPLOYMENT
Dim STR_LONEPARENT
Dim STR_BENEFITRECIPIENT
Dim STR_SECURITYQUESTION

Function getRecord()

Dim LOGINID
    LOGINID = nulltest(Session("svLOGINID"))

If (NullNestTF(LOGINID) = True) Then
    isSaved = False
    rw isSaved
    Response.End()
    Exit Function
Else
    If NOT IsNumeric(LOGINID) Then
        isSaved = False
        rw isSaved
        Response.End()
        Exit Function
    End If
End If

    CUSTOMERID = Request("CUSTOMERID")
    If CUSTOMERID = "" Or IsNull(CUSTOMERID) Then CUSTOMERID = -1 End If

  	SQL = "SELECT dbo.fn_FormatDate(DOB,'dd')+'/'+dbo.fn_FormatDate(DOB,'mm')+'/'+dbo.fn_FormatDate(DOB,'yyyy') as DOBFORMATTED, * FROM CUSTOMERID C INNER JOIN dbo.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID WHERE C.CUSTOMERID = " & CUSTOMERID
	Call OpenDB()
	Call OpenRs(rsClient, SQL)
		If NOT (rsClient.EOF) Then
			FIRSTNAME	= rsClient("FIRSTNAME")
			SURNAME	= rsClient("SURNAME")
			DOB = rsClient("DOBFORMATTED")
			ADDRESSLINE1 = rsClient("ADDRESSLINE1")
			ADDRESSLINE2 = rsClient("ADDRESSLINE2")
			ADDRESSLINE3 = rsClient("ADDRESSLINE3")
			LAA = rsClient("LAA")
			POSTCODE = rsClient("POSTCODE")
			EMAIL = rsClient("EMAIL")
			TELEPHONE = rsClient("TELEPHONE")
			MOBILE = rsClient("MOBILE")
			NINUMBER = rsClient("NINUMBER")
			GENDER	= rsClient("GENDER")
			DISABILITY = rsClient("DISABLED")
			ETHNICITY = rsClient("ETHNICITY")
			BENEFITRECIPIENT = rsClient("BENEFITRECIPIENT")
			LONEPARENT = rsClient("LONEPARENT")
			EMPLOYMENTSTATUS = rsClient("EMPLOYMENTSTATUS")
			LENGTHOFUNEMPLOYMENT = rsClient("LENGTHOFUNEMPLOYMENT")
			USERNAME = rsClient("USERNAME")
			PASSWORD = rsClient("PASSWORD")
			SECURITYQUESTION = rsClient("QUESTION")
            SECURITYANSWER = rsClient("ANSWER")
            
			action = "update"

	    Else

			action = "new"

		End If
	Call CloseRS(rsClient)
	Call CloseDB()

Call OpenDB()
    Call BuildSelect(STR_LAA,"sel_LAA","LAA","ID,DESCRIPTION","DESCRIPTION","Please Select",LAA,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Local Authority"" ") 
    Call BuildSelect(STR_GENDER,"sel_GENDER","L13","L13ID,DESCRIPTION","L13ID","Please Select",GENDER,Null,Null," required=""Y"" validate=""/([M|F|O])$/""  validateMsg=""Gender"" ") 
    Call BuildSelect(STR_DISABILITY,"sel_DISABILITY","L14","L14ID,DESCRIPTION","L14ID","Please Select",DISABILITY,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Disability"" ") 
    Call BuildSelect(STR_ETHNICITY,"sel_ETHNICITY","L12","L12ID,DESCRIPTION","L12ID","Please Select",ETHNICITY,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Ethnicity"" ") 
    Call BuildSelect(STR_EMPLOYMENTSTATUS,"sel_EMPLOYMENTSTATUS","E12","E12ID,DESCRIPTION","DESCRIPTION","Please Select",EMPLOYMENTSTATUS,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Employment Status"" ") 
    Call BuildSelect(STR_LENGTHOFUNEMPLOYMENT,"sel_LENGTHOFUNEMPLOYMENT","E14","E14ID,DESCRIPTION","DESCRIPTION","Please Select",LENGTHOFUNEMPLOYMENT,Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Length of Unemployment"" ") 
    Call BuildSelect(STR_LONEPARENT,"sel_LONEPARENT","N06","N06ID,DESCRIPTION","DESCRIPTION","Please Select",LONEPARENT,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Lone Parent"" ") 
    Call BuildSelect(STR_BENEFITRECIPIENT,"sel_BENEFITRECIPIENT","N05","N05ID,DESCRIPTION","DESCRIPTION","Please Select",BENEFITRECIPIENT,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Benefit Recipient"" onchange=""bb()"" ") 
    Call BuildSelect(STR_SECURITYQUESTION,"sel_SECURITYQUESTION","SECURITY_QUESTION","ID,DESCRIPTION","DESCRIPTION","Please Select",SECURITYQUESTION,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Security Question"" ") 
Call CloseDB()

End Function

Call getRecord()

%>
<div id="nsform" style="float:left; clear:right; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
   <div id="FrmEr_Edit" style="float:left;width:100%;"></div>
   <div style="float:left; width:100%;">
    <fieldset>
        <legend>Login to this site</legend>
        <div id="Frm" style="float:left; width:100%;">
            <p class="_H4">
            Client Form:
            </p>
            <div class="row">
                <span class="label"> <label for="txt_FIRSTNAME2" accesskey="F"><span style="text-decoration:underline; cursor:pointer">F</span>irstname:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_FIRSTNAME2" id="txt_FIRSTNAME2" maxlength="100" value="<%=FIRSTNAME%>" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Firstname" />
                    <img src="/js/FVS.gif" name="img_FIRSTNAME2" id="img_FIRSTNAME2" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="txt_SURNAME2" accesskey="S"><span style="text-decoration:underline; cursor:pointer">S</span>urname:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_SURNAME2" id="txt_SURNAME2" maxlength="100" value="<%=SURNAME%>" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Surname" />
                    <img src="/js/FVS.gif" name="img_SURNAME2" id="img_SURNAME2" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="txt_DOB2" accesskey="D"><span style="text-decoration:underline; cursor:pointer">D</span>OB:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_DOB2" id="txt_DOB2" maxlength="10" value="<%=DOB%>" required="Y" validate="/^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4}|[0-9]{2})$/" validateMsg="Date of Birth (dd/mm/yyyy)" />
                    <img src="/js/FVS.gif" name="img_DOB2" id="img_DOB2" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="txt_ADDRESSLINE1" accesskey="A"><span style="text-decoration:underline; cursor:pointer">A</span>ddress Line 1:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ADDRESSLINE1" id="txt_ADDRESSLINE1" maxlength="100" value="<%=ADDRESSLINE1%>" required="N" validate="/([a-zA-Z0-9,\s]{4,100})$/" validateMsg="Address line 1" />
                    <img src="/js/FVS.gif" name="img_ADDRESSLINE1" id="img_ADDRESSLINE1" width="15px" height="15px" alt="" />
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="txt_ADDRESSLINE2" accesskey="A"><span style="text-decoration:underline; cursor:pointer">A</span>ddress Line 2:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ADDRESSLINE2" id="txt_ADDRESSLINE2" maxlength="50" value="<%=ADDRESSLINE2%>" required="N" validate="/([a-zA-Z0-9,\s]{4,50})$/" validateMsg="Address line 2" />
                    <img src="/js/FVS.gif" name="img_ADDRESSLINE2" id="img_ADDRESSLINE2" width="15px" height="15px" alt="" />
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="txt_ADDRESSLINE3" accesskey="A"><span style="text-decoration:underline; cursor:pointer">A</span>ddress Line 3:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ADDRESSLINE3" id="txt_ADDRESSLINE3" maxlength="50" value="<%=ADDRESSLINE3%>" required="N" validate="/([a-zA-Z0-9,\s]{4,50})$/" validateMsg="Address line 3" />
                    <img src="/js/FVS.gif" name="img_ADDRESSLINE3" id="img_ADDRESSLINE3" width="15px" height="15px" alt="" />
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="sel_LAA" accesskey="L"><span style="text-decoration:underline; cursor:pointer">L</span>ocal Authority:</label> </span>
                <span class="formw">
                     <%=STR_LAA%>
                     <img src="/js/FVS.gif" name="img_LAA" id="img_LAA" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="txt_POSTCODE2" accesskey="P"><span style="text-decoration:underline; cursor:pointer">P</span>ostcode:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_POSTCODE2" id="txt_POSTCODE2" maxlength="8" value="<%=POSTCODE%>" required="Y" validate="/^[A-Za-z]{1,2}\d{1,2}[A-Za-z]? \d[A-Za-z]{2}$/" validateMsg="Postcode" />
                    <img src="/js/FVS.gif" name="img_POSTCODE2" id="img_POSTCODE2" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="txt_EMAIL2" accesskey="E"><span style="text-decoration:underline; cursor:pointer">E</span>mail:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_EMAIL2" id="txt_EMAIL2" maxlength="250" value="<%=EMAIL%>" required="N" validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" validateMsg="Email" />
                    <img src="/js/FVS.gif" name="img_EMAIL2" id="img_EMAIL2" width="15px" height="15px" alt="" />
                </span>
           </div>
            <div class="row">
                <span class="label"><label for="txt_TELEPHONE" accesskey="T"><span style="text-decoration:underline; cursor:pointer">T</span>elephone:</label></span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_TELEPHONE" id="txt_TELEPHONE" maxlength="20" value="<%=TELEPHONE%>" required="N" validate="/^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/" validateMsg="Telephone" />
                    <img src="/js/FVS.gif" name="img_TELEPHONE" id="img_TELEPHONE" width="15px" height="15px" alt="" />
                </span>
            </div>
            <div class="row">
                <span class="label"><label for="txt_MOBILE" accesskey="M"><span style="text-decoration:underline; cursor:pointer">M</span>obile:</label></span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="text" name="txt_MOBILE" id="txt_MOBILE" maxlength="20" value="<%=MOBILE%>" required="N" validate="/^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/" validateMsg="Mobile" />
                    <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" alt="" />
                </span>
            </div>
            <div class="row">
                <span class="label"> <label for="txt_NINUMBER" accesskey="E"><span style="text-decoration:underline; cursor:pointer">N</span>ational Insurance No.:</label> </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_NINUMBER" id="txt_NINUMBER" maxlength="12" value="<%=NINUMBER%>" required="N" validate="/^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$/" validateMsg="National Insurance Number" />
                    <img src="/js/FVS.gif" name="img_NINUMBER" id="img_NINUMBER" width="15px" height="15px" alt="" />
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="sel_GENDER" accesskey="G"><span style="text-decoration:underline; cursor:pointer">G</span>ender:</label> </span>
                <span class="formw">
                    <%=STR_GENDER%>
                    <img src="/js/FVS.gif" name="img_GENDER" id="img_GENDER" width="15px" height="15px" alt="" />
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="sel_DISABILITY" accesskey="D"><span style="text-decoration:underline; cursor:pointer">D</span>isability:</label> </span>
                <span class="formw">
                    <%=STR_DISABILITY%>
                    <img src="/js/FVS.gif" name="img_DISABILITY" id="img_DISABILITY" width="15px" height="15px" alt="" />
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="sel_ETHNICITY" accesskey="E"><span style="text-decoration:underline; cursor:pointer">E</span>thnicity:</label> </span>
                <span class="formw">
                    <%=STR_ETHNICITY%>
                    <img src="/js/FVS.gif" name="img_ETHNICITY" id="img_ETHNICITY" width="15px" height="15px" alt="" />
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="sel_BENEFITRECIPIENT" accesskey="B"><span style="text-decoration:underline; cursor:pointer">B</span>enefit Recipient:</label> </span>
                <span class="formw">
                    <%=STR_BENEFITRECIPIENT%>
                    <img src="/js/FVS.gif" name="img_BENEFITRECIPIENT" id="img_BENEFITRECIPIENT" width="15px" height="15px" alt="" />
                </span>
           </div>
            <div class="row">
                <span class="label"> <label for="sel_LONEPARENT" accesskey="L"><span style="text-decoration:underline; cursor:pointer">L</span>one Parent:</label> </span>
                <span class="formw">
                    <%=STR_LONEPARENT%>
                    <img src="/js/FVS.gif" name="img_LONEPARENT" id="img_LONEPARENT" width="15px" height="15px" alt="" />
                </span>
           </div>
           <div class="row">
                <span class="label"> <label for="sel_EMPLOYMENTSTATUS" accesskey="E"><span style="text-decoration:underline; cursor:pointer">E</span>mployment Status:</label> </span>
                <span class="formw">
                    <%=STR_EMPLOYMENTSTATUS%>
                    <img src="/js/FVS.gif" name="img_EMPLOYMENTSTATUS" id="img_EMPLOYMENTSTATUS" width="15px" height="15px" alt="" />
                </span>
           </div>
           <div class="row">
                <span class="label" style="white-space:nowrap;"> <label for="sel_LENGTHOFUNEMPLOYMENT" accesskey="L"><span style="text-decoration:underline; cursor:pointer">L</span>ength Of Unemployment:</label> </span>
                <span class="formw">
                    <%=STR_LENGTHOFUNEMPLOYMENT%>
                    <img src="/js/FVS.gif" name="img_LENGTHOFUNEMPLOYMENT" id="img_LENGTHOFUNEMPLOYMENT" width="15px" height="15px" alt="" />
                </span>
            </div>
            <div class="row" style="border-top:1px solid #cc0000; margin-top:5px; height:5px"></div>
            <div class="row">
                <span class="label" style="white-space:nowrap;">
                    <label for="sel_SECURITYQUESTION" accesskey="S">
                        <span style="text-decoration:underline; cursor:pointer">S</span>ecurity Question:
                    </label>
                </span>
                <span class="formw">
                    <%=STR_SECURITYQUESTION %>
                    <img src="/js/FVS.gif" name="img_SECURITYQUESTION" id="img_SECURITYQUESTION" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
            </div>
            <div class="row">
                <span class="label" style="white-space:nowrap;">
                    <label for="txt_SECURITYANSWER" accesskey="S">
                        <span style="text-decoration:underline; cursor:pointer">S</span>ecurity Answer:
                    </label>
                </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="text" name="txt_SECURITYANSWER" id="txt_SECURITYANSWER" maxlength="300" value="<%=SECURITYANSWER%>" required="Y" validate="/^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/" validateMsg="Security Answer" />
                    <img src="/js/FVS.gif" name="img_SECURITYANSWER" id="img_SECURITYANSWER" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
            </div>
            <div class="row">
                <span class="label" style="white-space:nowrap;">
                    <label for="txt_USERNAME" accesskey="U">
                        <span style="text-decoration:underline; cursor:pointer">U</span>sername:
                    </label>
                </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="text" name="txt_USERNAME" id="txt_USERNAME" maxlength="250" value="<%=USERNAME%>" required="Y" 
                        validate="/(^[a-zA-Z0-9_\@\.\-]{6,70})$/"
                        validateMsg="Username :- Must be 6 - 70 characters in length. Letters and Numbers are permitted. Spaces and most special characters are not permitted." />
                    <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
            </div>
            <div class="row">
                <span class="label" style="white-space:nowrap;">
                    <label for="txt_PASSWORD" accesskey="P">
                        <span style="text-decoration:underline; cursor:pointer">P</span>assword:
                    </label>
                </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="password" name="txt_PASSWORD" id="txt_PASSWORD" maxlength="50" value="<%=PASSWORD%>" required="N" validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" validateMsg="Password" />
                    <img src="/js/FVS.gif" name="img_PASSWORD" id="img_PASSWORD" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
            <div class="row">
                <span class="label" style="white-space:nowrap;">
                    <label for="txt_RETYPEPASSWORD" accesskey="P">
                        <span style="text-decoration:underline; cursor:pointer">P</span>assword (re-type):
                    </label>
                </span>
                <span class="formw">
                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="password" name="txt_RETYPEPASSWORD" id="txt_RETYPEPASSWORD" maxlength="50" value="" required="N" validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" validateMsg="Password (re-type)" />
                    <img src="/js/FVS.gif" name="img_RETYPEPASSWORD" id="img_RETYPEPASSWORD" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                </span>
           </div>
           <div class="row">
                <span class="label">&nbsp;</span>
                <span class="formw">
                    <input type="hidden" name="hid_CUSTOMER_REFERENCE" id="hid_CUSTOMER_REFERENCE" value="<%=CUSTOMERID%>" />
                    <input type="hidden" name="hid_action" id="hid_action" value="<%=action%>" />
                    <input class="submit" type="button" name="BtnSave" id="BtnSave" value=" Save " title="Submit Button : Save" style="width:auto" onclick="gg('Form2')"/>
                </span>
           </div>
        </div>
    </fieldset>
    </div>
</div>