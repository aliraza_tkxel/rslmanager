<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Call Debug()

Call OpenDB()

Dim action
    action = Request("action")

Dim CUSTOMER_REFERENCE
Dim ASSESSMENTOPTION
Dim REFERRED_FROM_PROVIDER_REFERENCE
Dim REFERRED_FROM_PROVIDER_USER
Dim DATE_OF_REFERRAL
Dim REFERRED_TO_PROVIDER_REFERENCE
Dim REFERRED_TO_PROVIDER_USER
Dim NOTES
Dim REFFERAL_REFERENCE
Dim thisSQL

If action = "new" Then

        CUSTOMER_REFERENCE                  = Request("CID")
        ASSESSMENTOPTION                    = Request("AOID")
        REFERRED_FROM_PROVIDER_REFERENCE    = Session("svPROVIDERID")
        REFERRED_FROM_PROVIDER_USER         = Session("svLOGINID")
        DATE_OF_REFERRAL                    = CDATE(DATE())
        REFERRED_TO_PROVIDER_REFERENCE      = Request("PID")
        REFERRED_TO_PROVIDER_USER           = NULL
        NOTES                               = NULL
        REFFERAL_REFERENCE                  = NULL

Else

    SQL = "SELECT CUSTOMERID, ASSESSMENTOPTIONID, RFORGANISATION, RFCONTACT, RTORGANISATION, RTCONTACT, RTDATE, NOTES, REFERRALID FROM CRMS_REFERRAL where journeyid = " & Request("JID")
    Call OpenRs(rs,SQL)
        If NOT rs.EOF Then
            CUSTOMER_REFERENCE              = rs("CUSTOMERID")
            ASSESSMENTOPTION                = rs("ASSESSMENTOPTIONID")
            REFERRED_FROM_PROVIDER_REFERENCE= rs("RFORGANISATION")
            REFERRED_FROM_PROVIDER_USER     = rs("RFCONTACT")
            DATE_OF_REFERRAL                = rs("RTDATE")
            REFERRED_TO_PROVIDER_REFERENCE  = rs("RTORGANISATION")
            REFERRED_TO_PROVIDER_USER       = rs("RTCONTACT")
            NOTES                           = rs("NOTES")
            REFFERAL_REFERENCE              = rs("REFERRALID")
        End If
    Call CloseRs(rs)

End If

If REFERRED_FROM_PROVIDER_REFERENCE <> "" then
    thisSQL = "TBL_MD_LOGIN WHERE PROVIDERID = " & REFERRED_FROM_PROVIDER_REFERENCE & " "
Else
    thisSQL = "TBL_MD_LOGIN"  
End If

' WHERE PROVIDERID = " & REFERRED_FROM_PROVIDER_REFERENCE & " 

    Call BuildSelect(STR_RFORGANISATION,"sel_RFORGANISATION","TBL_MD_PROVIDER","PROVIDERID,PROVIDERNAME","PROVIDERNAME","Please Select",REFERRED_FROM_PROVIDER_REFERENCE,Null,Null," tabindex=""1"" onchange=""getRFContact(this.value)"" ") 
    Call BuildSelect(STR_RFCONTACT,"sel_RFCONTACT",thisSQL,"LOGINID,FIRSTNAME+SPACE(1)+LASTNAME","LASTNAME,FIRSTNAME","Please Select",REFERRED_FROM_PROVIDER_USER,Null,Null," tabindex=""2"" ") 
    Call BuildSelect(STR_RTORGANISATION,"sel_RTORGANISATION","TBL_MD_PROVIDER","PROVIDERID,PROVIDERNAME","PROVIDERNAME","Please Select",REFERRED_TO_PROVIDER_REFERENCE,Null,Null," tabindex=""4"" onchange=""getRTContact(this.value)"" ") 
    Call BuildSelect(STR_RTCONTACT,"sel_RTCONTACT","TBL_MD_LOGIN WHERE PROVIDERID = " & REFERRED_TO_PROVIDER_REFERENCE & " ","LOGINID,FIRSTNAME+SPACE(1)+LASTNAME","LASTNAME,FIRSTNAME","Please Select",REFERRED_TO_PROVIDER_USER,Null,Null," tabindex=""5"" ") 

Call CloseDB()
%>
<form method="post" id="Form1" name="Form1" action="svr/ReferaClient_svr.asp"> <!-- onsubmit="return checkMyForm(this)" -->
	<div><h4>Refer Client</h4></div>
	<br />
	<div>
	    <div style="float:left; width:98%">
	        <div id="nsform" style="float:left; width:100%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                <div style="float:left; width:100%;">
                    Refer From:
                    <fieldset>
                        <legend>Refer Client - Referral From:</legend>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_RFORGANISATION" accesskey="O">
                                        <span style="text-decoration:underline; cursor:pointer">O</span>rganisation:
                                    </label>
                                </span>
                                <span class="formw">
                                    <%=STR_RFORGANISATION%>
                                    <img src="/js/FVS.gif" name="img_RFORGANISATION" id="img_RFORGANISATION" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_RFCONTACT" accesskey="C">
                                        <span style="text-decoration:underline; cursor:pointer">C</span>ontact:
                                    </label>
                                </span>
                                <span class="formw">                                    
                                    <%=STR_RFCONTACT%>
                                    <img src="/js/FVS.gif" name="img_RFCONTACT" id="img_RFCONTACT" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            </fieldset>
                            <br />
                            Refer To:
                            <fieldset>
                        <legend>Refer Client - Referral To:</legend>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_RTDATE" accesskey="D">
                                        <span style="text-decoration:underline; cursor:pointer">D</span>ate of Referral:
                                    </label>
                                </span>
                                <span class="formw">
                                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_RTDATE" id="txt_RTDATE" maxlength="10" value="<%=DATE_OF_REFERRAL%>" tabindex="3" />
                                    <img src="/js/FVS.gif" name="img_RTDATE" id="img_RTDATE" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_RTORGANISATION" accesskey="O">
                                        <span style="text-decoration:underline; cursor:pointer">O</span>rganisation:
                                    </label>
                                </span>
                                <span class="formw">
                                    <%=STR_RTORGANISATION%>
                                    <img src="/js/FVS.gif" name="img_RTORGANISATION" id="img_RTORGANISATION" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_RTCONTACT" accesskey="C">
                                        <span style="text-decoration:underline; cursor:pointer">C</span>ontact:
                                    </label>
                                </span>
                                <span class="formw">
                                    <%=STR_RTCONTACT%>
                                    <img src="/js/FVS.gif" name="img_RTCONTACT" id="img_RTCONTACT" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label">
                                    <label for="txt_NOTES" accesskey="N">
                                        <span style="text-decoration:underline; cursor:pointer">N</span>otes:
                                    </label>
                                </span>
                                <span class="formw">
                                    <textarea rows="5" cols="30" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" name="txt_NOTES" id="txt_NOTES" tabindex="6"><%=NOTES%></textarea>
                                    <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                            <div class="row">
                                <span class="label">&nbsp;</span>
                                <span class="formw">
                                        <input type="hidden" name="hid_CUSTOMER_REFERENCE" id="hid_CUSTOMER_REFERENCE" value="<%=CUSTOMER_REFERENCE%>" />
                                        <input type="hidden" name="hid_ASSESSMENTOPTION" id="hid_ASSESSMENTOPTION" value="<%=ASSESSMENTOPTION%>" />
                                        <input type="hidden" name="hid_action" id="hid_action" value="<%=action%>" />
                                        <input type="hidden" name="hid_REFFERAL_REFERENCE" id="hid_REFFERAL_REFERENCE" value="<%=REFFERAL_REFERENCE%>" />
                                        <input class="submit" type="button" name="BtnClose" id="BtnClose" value=" Close " title="Button : Close" tabindex="7" style="width:auto; margin-right:10px" onclick="Close()"/>
                                </span>
                            </div>
                    </fieldset>
                </div>
            </div>
	    </div>
	</div>
</form>