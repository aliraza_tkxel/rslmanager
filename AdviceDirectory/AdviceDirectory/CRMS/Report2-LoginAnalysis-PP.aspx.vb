Imports System.Data
Imports System.Data.SqlClient
Imports System.io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
'Imports MSDN.SessionPage
'Imports System.Web.UI.Page

Partial Class CRMS_NewTenancyList
    Inherits MSDN.SessionPage

    'Inherits System.Web.UI.Page

    Public Shared Function stringToDate(ByVal str As String) As DateTime

        Return DateTime.ParseExact(str, "dd/MM/yyyy", Nothing)
        Return Nothing

    End Function

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim SelectedIndex As Int32
        'SelectedIndex = selOrganisation.SelectedIndex.ToString()
        'Response.Write("Organisation Selected :" & SelectedIndex & "")

        'If user isn't authenticated, he/she will be redirected to Login
        If IsNothing(ASPSession("svLOGINID")) Then
            Response.Redirect("http://www.greatermanchester-anp.org.uk/CRMS")
        End If

        If Not Page.IsPostBack Then

            dpFrom.Text = Date.Today.AddMonths(-1).ToString
            dpTo.Text = Date.Today.ToString
            'Trace.Warn("Page_Load")

            BindDDListBox()
            BindLAAListBox()

            If selOrganisation.SelectedValue = Nothing Then
            Else
                BindReport()
            End If

        End If

    End Sub

    Private Sub BindReport(Optional ByVal e As ExtAspNet.GridSortEventArgs = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)

            Using sqlCmd As New SqlCommand
                Dim sqlDa As New SqlDataAdapter
                sqlCon.Open()

                Dim AdviserName As String
                If Not String.IsNullOrEmpty(txtAdviserName.Text) Then
                    AdviserName = txtAdviserName.Text.Trim
                Else
                    AdviserName = Nothing
                End If

                Dim OrgValue As Nullable(Of Integer)
                If Not String.IsNullOrEmpty(selOrganisation.SelectedValue) Then
                    OrgValue = Convert.ToInt32(selOrganisation.SelectedValue)
                End If

                    Dim LAAValue As Nullable(Of Integer)
                    If Not String.IsNullOrEmpty(selLocalAuthorityArea.SelectedValue) Then
                        LAAValue = Convert.ToInt32(selLocalAuthorityArea.SelectedValue)
                    End If

                    'Trace.Warn(OrgValue)
                Dim procFromDate As DateTime = stringToDate(Me.dpFrom.Text)
                Dim procToDate As DateTime = stringToDate(Me.dpTo.Text)

                sqlCmd.Connection = sqlCon
                sqlCmd.CommandText = "CRMS_REPORTS_PP_LOGIN_ANALYSIS"
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.AddWithValue("@LOGINID", ASPSession("svLOGINID"))
                sqlCmd.Parameters.AddWithValue("@FROMDATE", procFromDate)
                sqlCmd.Parameters.AddWithValue("@TODATE", procToDate)
                    sqlCmd.Parameters.AddWithValue("@LOCALAUTHORITYAREAID", LAAValue)
                sqlCmd.Parameters.AddWithValue("@ORGANISATIONID", OrgValue)
                sqlCmd.Parameters.AddWithValue("@ADVISERNAME", AdviserName)

                'Response.Write("dpFrom" & dpFrom.Text & "<br/>")
                'Response.Write("dpTo" & dpTo.Text & "<br/>")

                'Response.Write("dpFrom" & procFromDate & "<br/>")
                'Response.Write("dpTo" & procToDate & "<br/>")

                'Response.Write("OrgValue ")
                'Response.Write(OrgValue)
                'Response.Write("<br/>")
                'Response.Write("AdviserName" & AdviserName & "<br/>")
                'Response.End()

                Dim dt As New DataTable

                sqlDa.SelectCommand = sqlCmd
                sqlDa.Fill(dt)

                Dim dv As New DataView(dt)

                If e IsNot Nothing Then
                    dv.Sort = String.Format("{0} {1}", e.SortField, e.SortDirection)
                Else
                    gvReport.CurrentSortColumnIndex = 0
                    gvReport.Columns(0).SortDirection = "DESC"
                End If

                dt = dv.ToTable()
                Cache("PPLA") = dt
                gvReport.DataSource = dt
                gvReport.DataBind()
                sqlCon.Close()

            End Using
        End Using
    End Sub


    Private Sub BindDDListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_ORGANISATIONS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@LOGINID", ASPSession("svLOGINID"))
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    'selOrganisation.DataSource = ds
                    selOrganisation.DataSource = ds.Tables(0)
                    selOrganisation.DataTextField = ds.Tables(0).Columns("ProviderName").ColumnName.ToString()
                    selOrganisation.DataValueField = ds.Tables(0).Columns("ProviderID").ColumnName.ToString()
                    'NORMAL ASP.NET WAY
                    'selOrganisation.Items.Insert(0, New ListItem("***Select ***", "0"))
                    'EXT ASPNET WAY
                    'see PreRender
                    selOrganisation.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Private Sub BindLAAListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_LOCALAUTHORITYAREA"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    selLocalAuthorityArea.DataSource = ds.Tables(0)
                    selLocalAuthorityArea.DataTextField = ds.Tables(0).Columns("LocalAuthorityArea").ColumnName.ToString()
                    selLocalAuthorityArea.DataValueField = ds.Tables(0).Columns("LocalAuthorityAreaId").ColumnName.ToString()
                    selLocalAuthorityArea.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Protected Sub gvReport_PageIndexChange(ByVal sender As Object, ByVal e As ExtAspNet.GridPageEventArgs) Handles gvReport.PageIndexChange
        gvReport.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvReport_Sort(ByVal sender As Object, ByVal e As ExtAspNet.GridSortEventArgs) Handles gvReport.Sort
        BindReport(e)
    End Sub


    Protected Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        gvReport.DataBind()
    End Sub


    Protected Sub btnRefresh_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReport()
        'lblFrom.Text = selOrganisation.SelectedValue
    End Sub

    Protected Sub selOrganisation_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selOrganisation.PreRender
        'selOrganisation.Items.Clear()
        If selOrganisation.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selOrganisation.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selOrganisation.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
        'selOrganisation.Items.Add("***Please Select***", "-1")
        'selOrganisation.Items(0).Selected = True
    End Sub

    Protected Sub selLocalAuthorityArea_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selLocalAuthorityArea.PreRender
        If selLocalAuthorityArea.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selLocalAuthorityArea.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selLocalAuthorityArea.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
    End Sub

End Class
