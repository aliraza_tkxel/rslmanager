<%@ Page Language="VB" AutoEventWireup="false" Trace="false" CodeFile="Report2-ResponseTime.aspx.vb"
    Inherits="CRMS_NewTenancyList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ANP Portal > Reports > Professional/Partner Login Analysis</title>

    <script type="text/javascript" language="javascript">
        function ExportXlsGo()
        {            
            window.open('XlsExport.aspx?cache=RT','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }       
        function PrintGo()
        {            
            window.open('PrintExport.aspx?cache=RT','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ext:PageManager ID="pmPageManager" runat="server" />
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="false" Title="Response Time : Number of days between the date of referral and the next action recorded" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                            <ext:Grid ID="gvReport" EnableCollapse="true" Title="Response Time : Number of days between the date of referral and the next action recorded"
                                ShowBorder="true" AllowPaging="true" ShowHeader="true" Icon="Table" AllowSorting="true"
                                EnableHeaderMenu="true" AutoWidth="true" PageSize="15" AutoHeight="true" runat="server"
                                DataKeyNames="ORGANISATION,0,1,2,3,4,5,6" OnSort="gvReport_Sort">
                                <Toolbars>
                                    <ext:Toolbar runat="server" ID="tbGrid">
                                        <Items>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblFrom" runat="server" Text="From:">
                                            </ext:Label>
                                            <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                                ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblTo" runat="server" Text="To:">
                                            </ext:Label>
                                            <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                                DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                                Label="To Date" runat="server">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label Text="Local Authority Area: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                                DataTextField="LocalAuthorityAreaId" DataValueField="LocalAuthorityArea">
                                            </ext:DropDownList>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                                Icon="reload">
                                            </ext:Button>
                                            <ext:ToolbarSeparator runat="server" ID="ts1">
                                            </ext:ToolbarSeparator>
                                            <ext:ToolbarFill ID="tbFill" runat="server">
                                            </ext:ToolbarFill>
                                            <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Text="To Xls" ToolTip="Export to Excel"
                                                runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                            </ext:Button>
                                            <ext:ToolbarSeparator ID="ts2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" ToolTip="Print" Text="Print"
                                                runat="server" Icon="Printer">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField DataField="ORGANISATION" SortField="ORGANISATION" HeaderText="Organisation" />
                                    <ext:BoundField DataField="0" SortField="0" HeaderText="0 Day(s)" />
                                    <ext:BoundField DataField="1" SortField="1" HeaderText="1 Day(s)" />
                                    <ext:BoundField DataField="2" SortField="2" HeaderText="2 Day(s)" />
                                    <ext:BoundField DataField="3" SortField="3" HeaderText="3 Day(s)" />
                                    <ext:BoundField DataField="4" SortField="4" HeaderText="4 Day(s)" />
                                    <ext:BoundField DataField="5" SortField="5" HeaderText="5-10 Day(s)" />
                                    <ext:BoundField DataField="6" SortField="6" HeaderText=">10 Day(s)" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>
    </form>
</body>
</html>
