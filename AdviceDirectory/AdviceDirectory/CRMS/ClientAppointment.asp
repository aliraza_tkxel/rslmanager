<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim JOURNEYID
Dim SITENAME
Dim PROVIDERID, PROVIDERNAME
Dim PHOTOGRAPH
Dim ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3
Dim TELEPHONE
Dim FAX
Dim EMAIL
Dim LOGINID

Dim TIME

Dim DATE_OF_APPOINTMENT

Dim HISTORYID

    PROVIDERID = NULL
    ADDRESS = ""
    DATE_OF_APPOINTMENT = CDATE(DATE())
    JOURNEYID = Request("SID")
    HISTORYID = Request("HID")

    If JOURNEYID = "" Or IsNull(JOURNEYID) Then JOURNEYID = -1 End If

  	SQL = "SELECT L.LOGINID, T.TIMEID, T.TIME, APPOINTMENTDATE, DATECREATED, SITENAME, P.PROVIDERID, PROVIDERNAME, PHOTOGRAPH, S.ADDRESSLINE1, S.ADDRESSLINE2, S.ADDRESSLINE3, S.ADDRESSLINE4, S.TELEPHONE, S.FAX, S.EMAIL, S.CLIENTCONTACT " &_
           " FROM CRMS_APPOINTMENT A " &_
           " INNER JOIN TBL_MD_PROVIDER_SITE S ON A.SITEID = S.SITEID " &_
           " INNER JOIN TBL_MD_PROVIDER P ON P.PROVIDERID = S.PROVIDERID " &_
           " INNER JOIN TBL_MD_PROVIDER_DETAIL PD ON p.PROVIDERID = PD.PROVIDERID " &_
           " LEFT JOIN TBL_MD_LOGIN L ON L.LOGINID = A.CONTACTID " &_
           " INNER JOIN dbo.TIME T ON T.TIMEID = A.TIMEID " &_
           " WHERE A.JOURNEYID = " & JOURNEYID & " AND A.HISTORYID = " & HISTORYID

	Call OpenDB()
	Call OpenRs(rsClient, SQL)
		If NOT (rsClient.EOF) Then
			SITENAME	= rsClient("SITENAME")
			PROVIDERID = rsClient("PROVIDERID")
			PROVIDERNAME	= rsClient("PROVIDERNAME")
			PHOTOGRAPH = rsClient("PHOTOGRAPH")
			ADDRESSLINE1 = rsClient("ADDRESSLINE1")
			ADDRESSLINE2 = rsClient("ADDRESSLINE2")
			ADDRESSLINE3 = rsClient("ADDRESSLINE3")
			TELEPHONE = rsClient("TELEPHONE")
			FAX = rsClient("FAX")
			EMAIL = rsClient("EMAIL")
			CLIENTCONTACT = rsClient("CLIENTCONTACT")
			TIME = rsClient("TIMEID")
			DATE_OF_APPOINTMENT = rsClient("APPOINTMENTDATE")
	        LOGINID = rsClient("LOGINID")
	        
			If Isnull(EMAIL) or EMAIL = "" Then EMAIL = NULL Else EMAIL = "<a href=""mailto:"&EMAIL&""" title=""Email : "&PROVIDERNAME&"," &SITENAME& """/>"&EMAIL&"</a>" End If
			If ADDRESSLINE1 <> "" Then ADDRESS = ADDRESSLINE1 End If
			If ADDRESSLINE2 <> "" Then ADDRESS = ADDRESS & ", " & ADDRESSLINE2 End If
			If ADDRESSLINE2 <> "" Then ADDRESS = ADDRESS & ", " & ADDRESSLINE3  End If

		End If
	Call CloseRS(rsClient)
	Call CloseDB()

Dim action
    action = Request("action")

    If action = "" Then
        action= "new"
    End If

    thisSQL = "TBL_MD_LOGIN WHERE PROVIDERID = " & PROVIDERID

Dim STR_CONTACT
Dim STR_TIME

Call OpenDB()
Call BuildSelect(STR_CONTACT,"sel_CONTACT",thisSQL,"LOGINID,FIRSTNAME+SPACE(1)+LASTNAME","LASTNAME,FIRSTNAME","Please Select",LOGINID,Null,Null," tabindex=""1""  disabled=""disabled"" ")
Call BuildSelect(STR_TIME,"sel_TIME","TIME","TIMEID,TIME","TIMEID,TIME","Please Select",TIME,Null,Null," tabindex=""3"" disabled=""disabled"" ")
Call CloseDB()
%>
    <div>
	    <div style="width:100%; background-color:white; border:solid 1px #cc0000; border-bottom:0px; color:#cc0000; text-align:left">
	        <div style="padding:10px;">Client Appointment</div>
	    </div>
	</div>
	<div>
        <div id="nsform" style="width:100%; border:solid 1px #cc0000;">
            <div style="width:100%;">
                <fieldset>
                    <legend>Client Appointment</legend>
                    <div style="padding:10px;">
                    <%
                    rw "<b>" & PROVIDERNAME & "</b>" & "<br/><br/>"
                    rw "<div style=""padding-bottom:0.5em"">" & SITENAME & "</div>"
                    rw "<div style=""padding-bottom:0.5em"">" & ADDRESS & "</div>"
                    If TELEPHONE <> "" Then
                        rw "<div style=""padding-bottom:0.5em"">Telephone: " & TELEPHONE & "</div>"
                    End If
                    If FAX <> "" Then
                        rw "<div style=""padding-bottom:0.5em"">Fax: " & FAX & "</div>"
                    End If
                    If EMAIL <> "" Then
                        rw "<div style=""padding-bottom:0.5em"">Email: " & EMAIL & "</div>"
                    End If
                    %>
                    </div>
                    <div class="row">
                        <span class="label">
                            <label for="txt_CONTACT" accesskey="C">
                                <span style="text-decoration:underline; cursor:pointer">C</span>ontact:
                            </label>
                        </span>
                        <span class="formw">
                            <%=STR_CONTACT%>
                            <img src="/js/FVS.gif" name="img_CONTACT" id="img_CONTACT" width="15px" height="15px" alt="" />
                        </span>
                    </div>
                    <div class="row">
                        <span class="label">
                            <label for="txt_DATE" accesskey="D">
                                <span style="text-decoration:underline; cursor:pointer">D</span>ate:
                            </label>
                        </span>
                        <span class="formw">
                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_DATE" id="txt_DATE" maxlength="10" value="<%=DATE_OF_APPOINTMENT%>" tabindex="2" disabled="disabled" />
                            <img src="/js/FVS.gif" name="img_DATE" id="img_DATE" width="15px" height="15px" alt="" />
                        </span>
                    </div>
                    <div class="row">
                        <span class="label">
                            <label for="txt_TIME" accesskey="T">
                                <span style="text-decoration:underline; cursor:pointer">T</span>ime:
                            </label>
                        </span>
                        <span class="formw">
                            <%=STR_TIME%>
                            <img src="/js/FVS.gif" name="img_TIME" id="img_TIME" width="15px" height="15px" alt="" />
                        </span>
                    </div>
                    <div class="row">
                        <span class="label">&nbsp;</span>
                        <span class="formw">
                                <input type="hidden" name="hid_SITE_REFERENCE" id="hid_SITE_REFERENCE" value="<%=SITEID%>" />
                                <input type="hidden" name="hid_ORGANISATION_REFERENCE" id="hid_ORGANISATION_REFERENCE" value="<%=PROVIDERID%>" />
                                <input type="hidden" name="hid_action" id="hid_action" value="<%=action%>" />
                                <input type="hidden" name="hid_APPOINTMENT_REFERENCE" id="hid_APPOINTMENT_REFERENCE" value="<%=APPOINTMENT_REFERENCE%>" />
                                <input class="submit" type="button" name="BtnClose" id="BtnClose" value=" Close " title="Submit Button : Close Appointment Window" tabindex="5" style="width:auto; margin-right:10px" onclick="Close()"/>
                        </span>
                    </div>
                </fieldset>
            </div>
	    </div>
	</div>