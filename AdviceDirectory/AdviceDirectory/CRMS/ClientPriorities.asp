<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Call OpenDB()

Dim action
    action = Request("action")
  
Dim HISTORICAL_REFERENCE
    HISTORICAL_REFERENCE = Request("HID")
    
Dim SESSION_REFERENCE
    SESSION_REFERENCE = Request("SID")
    
SQL = "SELECT COUNT(1) " &_
            "FROM ASSESSMENT_OPTIONS AO " &_
            "INNER JOIN CUSTOMERID_PRIORITIES CP ON CP.ASSESSMENTOPTIONID = AO.ID " &_
            "WHERE HISTORYID = " & HISTORICAL_REFERENCE & " AND SESSIONID = '" & SESSION_REFERENCE & "' "
    Call OpenDB()
    Call OpenRs (rs,SQL)
        Dim ArrayLength 
            ArrayLength = rs(0)
    Call CloseRs(rs)
    Call CloseDB()
    
If ArrayLength >= 1 Then  
%>
    <div>
	    <div style="width:100%; background-color:white; border:solid 1px #cc0000; border-bottom:0px; color:#cc0000; text-align:left">
	        <div style="padding:10px;">Client Priorities</div>
	    </div>
	</div>
	<div>
        <div id="Div1" style="width:100%; border:solid 1px #cc0000;">
            <div style="width:100%; padding:10px;">
                <fieldset>
                    <legend>Client Priorities</legend>                    
<%        
    ReDim Preserve ArrayID(ArrayLength)
    ReDim Preserve ArrayDescription(ArrayLength)
    ReDim Preserve ArrayPriority(ArrayLength)    
    SQL = "SELECT AO.ID, AO.DESCRIPTION, CP.PRIORITY " &_
          "FROM ASSESSMENT_OPTIONS AO " &_
          "INNER JOIN CUSTOMERID_PRIORITIES CP ON CP.ASSESSMENTOPTIONID = AO.ID " &_
          "WHERE HISTORYID = " & HISTORICAL_REFERENCE & " AND SESSIONID = '" & SESSION_REFERENCE & "' " &_
          "ORDER BY CP.HISTORYID DESC, CP.PRIORITY, CP.ID, ID"          
    Call OpenDB()
    Call OpenRs (rsPostCode,SQL)
        If NOT rsPostCode.EOF Then
        i = 1
            while not rsPostCode.eof            
                ArrayID(i) = rsPostCode("ID")
                ArrayDescription(i) = rsPostCode("DESCRIPTION")
                ArrayPriority(i) = rsPostCode("PRIORITY")            
              %>
                <div class="row" style="padding-top:1em; padding-left:1em">
                    <span class="formw" style="width:auto">
                        <select disabled="disabled" name="ASSESSMENT_<%=ArrayID(i)%>" id="ASSESSMENT_<%=ArrayID(i)%>" style="width:auto" onchange="blar(<%=ArrayID(i)%>,this.value)">
                            <option value=""> </option>
                                <%
                                For j=1 to ArrayLength
                                    rw "<option value="""&j&""""
                                    If j = CInt(ArrayPriority(i)) Then 
                                        rw "selected=""selected"">"
                                    Else
                                        rw ">"
                                    End If
                                    rw "" & j & "</option>"
                                Next
                                %>
                        </select>
                    </span>
                    <span class="label">
                        <%=ArrayDescription(i)%>
                    </span>
                </div>
                <%
          	    rsPostCode.MoveNext()
          	    i = i + 1
            Wend            
            %>
                    <div class="row" style="padding-top:1em;">
                        <span class="label"> 
                            <input type="hidden" name="CUSTOMERID" id="CUSTOMERID" value="<%=CUSTOMER_REFERENCE%>" />
                            <input class="submit" type="button" name="BtnCancel" id="BtnCancel" value=" Close " title="Submit Button : Close" tabindex="7" style="width:auto; margin-right:10px" onclick="Cancel()"/>
                        </span>
                    </div>
            <%
        Else
            '
        End If
    Call CloseRs(rsPostCode)
    Call CloseDB()
    %>          </fieldset>
            </div>
	    </div>
	</div>	
<%  
    End If
%>