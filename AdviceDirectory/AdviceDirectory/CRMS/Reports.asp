﻿<%@  language="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If
    
    If LOGINID <> 721 Then
        'Response.Redirect(Request.ServerVariables("HTTP_REFERER"))
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	Dim regEx, Match, Matches
 	Set regEx = New RegExp
	  	regEx.Global = True
	  	regEx.IgnoreCase = True
	   	regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
	
	Dim tomorrow
	 tomorrow = DateAdd("y",1,Date())
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=SITE_NAME%>
        :
        <%=PAGE_TITLE%>
    </title>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    <meta name="Title" content="<%=nextstep_name%>" />
    <meta name="Description" content="<%=PAGE_DESCRIPTION%>" />
    <meta name="Keywords" content="<%=PAGE_KEYWORDS%>" />
    <meta name="Author" content="<%=nextstep_name%>" />
    <meta http-equiv="EXPIRES" content="+30 days" />
    <meta http-equiv="content-LANGUAGE" content="English" />
    <meta name="ROBOTS" content="index,follow" />
    <meta name="REVISIT-AFTER" content="15 days" />
    <meta name="ROBOTS" content="ALL" />
    <meta name="DSTRIBUTION" content="global" />
    <link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
    <link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
    <link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
    <link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />
    <style type="text/css">

table.table_box {
    background:#fff;
	padding:2px;
	border-top:1px solid #CCCCCC;
	border-left:1px solid #CCCCCC;
	border-right:1px solid #999999;
	border-bottom:1px solid #999999;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;	
}

#jquerycalendar {

}

input, select, textarea {
	cursor:pointer;
	background:#fff;
	margin-left:auto;
	margin-right:auto;
	padding:2px;
	border-top:1px solid #CCCCCC;
	border-left:1px solid #CCCCCC;
	border-right:1px solid #999999;
	border-bottom:1px solid #999999;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
h3 {
	color:#00a0d1;
	font-weight: bold;
}

</style>

    <script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>

    <script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>

    <script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>

    <script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>

    <script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
    </script>
    
    <script type="text/javascript" language="javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="/js/datepicker.js"></script>
    <script type="text/javascript" language="javascript" src="/js/eye.js"></script>
    <script type="text/javascript" language="javascript" src="/js/utils.js"></script>
    <script type="text/javascript" language="javascript" src="/js/layout.js?ver=1.0.2"></script>
    
    <link rel="stylesheet" href="/css/datepicker.css" type="text/css" />
    
    <script type="text/javascript" language="javascript">
    function GenExport()
    {
        var FromDate = document.getElementById("FromDate").value;
        var ToDate = document.getElementById("ToDate").value;
        window.open("Popups/Excel.asp?FromDate="+FromDate+"&ToDate="+ToDate+"&Random=" + new Date(), "_blank", "height=200px,width=400px,resizable=yes,status=yes,scrollbars=no")
    }

    function JsOpenWindow(theURL)
    {
	window.open(theURL, '', 'fullscreen=yes, scrollbars=auto');
    }
    </script>

</head>
<body lang="en">
    <p id="access">
        <a href="#content" accesskey="g">Go to page content</a></p>
    <!-- Start Container //-->
    <div id="group" style="border: solid 0px red">
        <!-- #include virtual="/includes/Header.asp" -->
        <div id="M_Container">
            <!-- Start Page Title //-->
            <div id="MiddleMC">
                <h1>
                    <%=PAGE_TITLE%>
                </h1>
            </div>
            <!-- End Page Title //-->
        </div>
        <!-- Start Left -->
        <div id="navigation">
            <!-- Start Navigation -->
            <div id="primary-nav">
                <% Call listCatDIR(404, "navmenu" , "CRMS") %>
            </div>
            <!-- End Navigation -->
        </div>
        <!-- End Left -->
        <!-- Start Content Container -->
        <div id="content" style="border: solid 0px red">
            <!-- Start Content -->
            <div id="maincontent" style="margin-left: 10px; margin-right: 10px; background-attachment: fixed;
                background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
                <form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>" enctype="multipart/form-data"
                    onsubmit="return cck(this);">
                    <div style="margin-left: -10px;">
                        <%=PAGE_CONTENT%>
                    </div>
                    <div style="margin-left: -10px;">
                    <div style="float:left">
                          <table class="table_box" width="300">
                            <tr>
                                <td colspan="2">
                                    <h3>Customer Data Export</h3>
                                    <hr style="margin-bottom:5px; margin-top:5px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Customer Data Export
                                </td>
                                <td align="right">
                                    	<input type="button" id="BtnCDE_" name="BtnCDE_" value="View" onclick="window.location.href='CustomerDataExport.asp'" style="display:none" />
					<input type="button" id="BtnCDE" name="BtnCDE" value="View" onclick="JsOpenWindow('CustomerDataExport.aspx')" />
				</td>
                            </tr>
                        </table>
                        <br />
                        <table class="table_box" width="300">
                            <tr>
                                <td colspan="2">
                                    <h3>Usage</h3>
                                    <hr style="margin-bottom:5px; margin-top:5px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Self Assessment                                    
                                </td>
                                <td align="right">
                                     	<input type="button" id="BtnSA_" name="BtnSA_" value="View" onclick="window.location.href='Report2-SelfAssessment.asp'" style="display:none" />
					<input type="button" id="BtnSA" name="BtnSA" value="View" onclick="JsOpenWindow('Report2-SelfAssessment.aspx')" />
				</td>
                            </tr>
                            <tr>
                                <td>
                                    Partner Logins
                                </td>
                                <td align="right">
                                     	<input type="button" id="BtnPL_" name="BtnPL_" value="View" onclick="window.location.href='Report2-LoginAnalysis-PP.asp'" style="display:none" />
					<input type="button" id="BtnPL" name="BtnPL" value="View" onclick="JsOpenWindow('Report2-LoginAnalysis-PP.aspx')" />
				</td>                                   
                            </tr>
                          </table>
                          </div>
                          <div style="float:left; margin-left:10px">
                              <table class="table_box" width="300">
                                <tr>
                                    <td colspan="2">
                                        <h3>Performance</h3>
                                        <hr style="margin-bottom:5px; margin-top:5px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Referrals Received
                                    </td>
                                    <td align="right">
                                        <input type="button" id="BtnRR_" name="BtnRR_" value="View" onclick="window.location.href='Performance/ReferralsReceived.asp'" style="display:none" />
					<input type="button" id="BtnRR" name="BtnRR" value="View" onclick="JsOpenWindow('Performance/ReferralsRecieved.aspx')" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Referrals Created
                                    </td>
                                    <td align="right">
                                        <input type="button" id="BtnRC_" name="BtnRC_" value="View" onclick="window.location.href='Performance/ReferralsCreated.asp'" style="display:none" />
					<input type="button" id="BtnRC" name="BtnRC" value="View" onclick="JsOpenWindow('Performance/ReferralsCreated.aspx')" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Response Time
                                    </td>
                                    <td align="right">
                                        <input type="button" id="BtnRT_" name="BtnRT_" value="View" onclick="window.location.href='Report2-ResponseTime.asp'" style="display:none" />
					<input type="button" id="BtnRT" name="BtnRT" value="View" onclick="JsOpenWindow('Report2-ResponseTime.aspx')" />
                                    </td>
                                </tr>
                                </table>
                                <br />
                                <table class="table_box" width="300">
                                    <tr>
                                        <td colspan="2">
                                            <h3>Tracking</h3>
                                            <hr style="margin-bottom:5px; margin-top:5px" />
                                        </td>
                                    </tr>
                                     <tr>
                                    <td>
                                        Referral Analysis
                                    </td>
                                    <td align="right">
                                        <input type="button" id="BtnRA_" name="BtnRA_" value="View" onclick="window.location.href='Report2-ReferralAnalysis.asp'" style="display:none" />
					<input type="button" id="BtnRA" name="BtnRA" value="View" onclick="JsOpenWindow('Report2-ReferralAnalysis.aspx')" />
                                    </td>
                                </tr>
                                </table>                          
                          </div>
                    </div>
                </form>
            </div>
            <!-- End Content -->
        </div>
        <!-- End Content Container -->
        <!-- Start Navigation //-->
        <div id="footer">
            <ul>
                <% Call listCat2(41, "ulFooter") %>
            </ul>
        </div>
        <!-- End Navigation -->
    </div>
    <!-- End Container //-->
    <img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility: hidden" />
    <img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility: hidden" />
    <img src="/js/img/FVS.gif" width="10px" height="10px" name="FVS_Image" alt="" style="visibility: hidden" />
    <img src="/js/img/FVW.gif" width="10px" height="10px" name="FVW_Image" alt="" style="visibility: hidden" />
    <img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility: hidden" />
    <iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600"
        style="display: none" src="/dummy.asp"></iframe>
</body>
</html>
