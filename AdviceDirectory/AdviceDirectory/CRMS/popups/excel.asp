<%
Dim FromDate
Dim ToDate

FromDate = Request("FromDate")
ToDate = Request("ToDate")
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Untitled Page</title>
    <style type="text/css">
    body {  
        font-family: Tahoma, Arial, Helvetica, sans-serif; 
        font-size: 10pt; 
        color: #003366; 
        background-color:#ffffff;
    }
    </style>
</head>
<script type="text/javascript" language="javascript" src="barloader.js"></script>

<script type="text/javascript" language="javascript">
progress_start();
</script>

<body lang="en" onload="window.focus();">
    <div style='border: 1px solid #00a0d1; padding: 10px; background-color: #ffffff; text-align:center'>
        <div id="titlebar">
            <b>Excel file is being generated:</b></div>
        <br/>
        <div id="parentbar" style="width: 300px; font-size: 8pt; padding: 2px; border: solid black 1px">
            <div id="progressbar" style='width: 294px; overflow: hidden; color: #FFFFFF; background-color: #003366'>
            </div>
        </div>
    </div>
    <iframe src="ExcelReporter.asp?FromDate=<%=FromDate%>&ToDate=<%=ToDate%>" width="1" height="1" style="display: none">
    </iframe>
</body>
</html>
