var progressEnd = 294;		// set to number of progress <span>'s.
var progressColor = 'blue';	// set to progress bar color
var progressInterval = 50;	// set to time between updates (milli-seconds)
var progressdirection = "forward";
var progressStarted = false;

var progressAt = 0;
var progressTimer;
function progress_change() {
	if (progressdirection == "forward") progressdirection = "back";
	else progressdirection = "forward";
}

function progress_clear(){
	document.getElementById('progressbar').style.width = "0px";
	progressdirection = "forward";
	}
	
function progress_update() {
	if (progressdirection == "forward") progressAt = progressAt+1;
	else progressAt = progressAt-1;
	if (progressAt > progressEnd || progressAt < 0) progress_change();
	else document.getElementById('progressbar').style.width = progressAt + "px";
	progressTimer = setTimeout('progress_update()',progressInterval);
}
function progress_stop() {
	if (!progressStarted) return true;
	progressStarted = false;
	clearTimeout(progressTimer);
	progress_clear();
}

function progress_start(){
	if (progressStarted) return true;
	progressStarted = true;
	progressAt = 0
	progress_update();		// start progress bar
	}
