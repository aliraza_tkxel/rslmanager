<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim retError

Function Record_Search()

'On Error Resume Next

Dim CUSTOMER_REFERENCE
Dim FIRSTNAME
Dim SURNAME
Dim POSTCODE
Dim EMAIL
Dim DOB

Dim rsBU

    CUSTOMER_REFERENCE = nulltest(Request("txt_CUSTOMER_REFERENCE"))

    If (NullNestTF(CUSTOMER_REFERENCE) = True) Then
        CUSTOMER_REFERENCE = NULL
    Else
        If NOT IsNumeric(CUSTOMER_REFERENCE) Then
            CUSTOMER_REFERENCE = NULL
        End If
    End If

    FIRSTNAME = nulltest(Request("txt_FIRSTNAME"))
    SURNAME = nulltest(Request("txt_SURNAME"))
    POSTCODE = nulltest(Request("txt_POSTCODE"))
    EMAIL = nulltest(Request("txt_EMAIL"))
    DOB = nulltest(Request("txt_DOB"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_FIND_CUSTOMER"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, FIRSTNAME)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, SURNAME)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 8, POSTCODE)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, EMAIL)
                .Parameters.Append .createparameter("@DOB", adDBTimeStamp, adParamInput, 0, DOB)
                'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsBU = .Execute
					If NOT rsBU.EOF Then
					        rw "<div>"
					        rw "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">"
					        rw "<thead>"
                            rw "<tr>"
                               rw " <th colspan=""4"" style=""background-color:white; color:black; padding-left:0px"">Search Results (Possible Matches)</th>"
                            rw "</tr>"
                             rw "<tr class="""">"
                               rw " <th>Name</th>"
                               rw " <th>Postcode</th>"
                               rw " <th>Email</th>"
                               rw " <th>&nbsp;</th>"
                            rw "</tr>"
                        rw "</thead>"
                        rw "<tbody>"
					    While NOT rsBU.EOF
                        rw " <tr>"
                            rw " <td>"&rsBU("FULLNAME")&"</td>"
                            rw " <td>"&rsBU("POSTCODE")&"</td>"
                            rw " <td>"&rsBU("EMAIL")&"</td>"
                            rw " <td nowrap=""nowrap"" style=""white-space:pre; background-color:white""><div style=""float:right;""><input type=""button"" accesskey=""E"" id=""Edit"" name=""Edit"" title=""Edit"" value=""Edit"" onclick=""BuildXMLReport('EditaClient.asp?CUSTOMERID="&rsBU("CUSTOMERID")&"')"" /></div><div style=""float:right; margin-right:5px""><input type=""button"" accesskey=""V"" id=""View"" name=""View"" title=""View"" value=""View"" onclick=""GoTo('ClientJourney.asp?CUSTOMERID="&rsBU("CUSTOMERID")&"')"" /></div></td>"
                        rw " </tr>"   
					       rsBU.moveNext
					    Wend
					        rw "</tbody>"
                        rw "<tfoot>"
                        rw "<tr>"
                            rw "<td colspan=""4"" style=""padding-top:10px"">"
                                rw "<input type=""button"" accesskey=""C"" id=""Create"" name=""Create"" title=""Create"" value=""Create"" onclick=""BuildXMLReport('EditaClient.asp?CUSTOMERID=-1')"" />"
                            rw "</td>"
                        rw "</tr>"
                        rw "</tfoot>"
                    rw "</table>"
                    rw "</div>"
					Else
					     rw "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">"
					        rw "<tbody>"
					        rw "<tr>"
                               rw " <th style=""background-color:white; color:black; padding-left:0px"">Search Results (0 Possible Matches)</th>"
                            rw "</tr>"
                            rw "</tbody>"
                            rw "<tfoot>"
                            rw "<tr>"
                            rw "<td colspan=""4"" style=""padding-top:10px"">"
                                rw "<input type=""button"" accesskey=""C"" id=""Create"" name=""Create"" title=""Create"" value=""Create"" onclick=""BuildXMLReport('EditaClient.asp?CUSTOMERID=-1')"" />"
                            rw "</td>"
                        rw "</tr>"
                        rw "</tfoot>"
					    rw "</div>"
					End If
			    End With
	    objConn.Close
    Set objConn = Nothing

'    ' check for error & set boolean value
'    If Err.Number <> 0 Then
'        retError = False
'    Else
'        retError = True
'    End If
'    
' On Error GoTo 0

  ' return boolean value
'  Record_Search = retError

End Function
%>
<div style="border:solid 1px #cc0000; padding:1em;">
<% 
Call Record_Search() 
'  If Record_Search() Then
'    rw "ERRRR"
'  End If
%>
</div>