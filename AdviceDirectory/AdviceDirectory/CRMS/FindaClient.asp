﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%
If Request.Form("Find_") <> "" Then

    Dim txt_USERNAME	' FORM FIELD - USERNAME
    Dim strPASSWORD 	' FORM FIELD - PASSWORD
    Dim rsLogin			' RECORDSET
    Dim passcheck 		' USER OK/NOT OK TO LOGIN
    Dim passcheck_text

	    txt_USERNAME = "xyz"
	    txt_password = "xyz"
	    passcheck = ""

        ' VALIDATE USERNAME
	    If (Request.Form("txt_USERNAME") <> "") Then
		    If isValidEmail(Request.Form("txt_USERNAME")) = True Then
			    strUSERNAME = Request.Form("txt_USERNAME")
		    Else
			    strUSERNAME = isValidEmail(Request.Form("txt_USERNAME"))
		    End If
	    End If
        ' VALIDATE PASSWORD
	    If (Request.Form("txt_PASSWORD") <> "") Then
		    strPASSWORD = RegExpTrimAll(Request.Form("txt_PASSWORD"))
	    End If

        ' LOGIN USING SPECIFIED DETAILS
	    Call MD_FindClient(strUSERNAME, strPASSWORD)

        ' SET ERROR FLAG - CSS STYLE SHEET USED
	    If passcheck = 0 Then
            ErrorClass = " class=""er"""
        Else
            ErrorClass = " class=""no_error"""
        End If

End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">
<!--
var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
  xmlhttp = new XMLHttpRequest();
}

function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
        qs+=(qs=='')?'?':'&'
        qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
	    }
      }
    }
  return qs
}

function BuildXMLReport(str_url){

	//set the loading parameter
    document.getElementById("errordiv").innerHTML = "<div style='float:left; padding:10px;'>Please wait while your results are loading</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById("errordiv").className = "no_error"
	//call xmlhttp functions
	var currentTime = new Date()
	str_url = str_url
	var strURL = ''+str_url+'&dt='+currentTime
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   str_url + "' could not be found.");
							   document.getElementById("errordiv").innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById("errordiv").innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById("errordiv").innerHTML = ""
							   }
							   // Call the desired result function
							   else {
							            if (strResponse == 'False')
							            {
							                window.location.href = '/crms/login.asp'
							            }
							            else
							            {
									        document.getElementById("Search_Restults").innerHTML = strResponse
									        document.getElementById("errordiv").innerHTML = ""
									        document.getElementById("errordiv").className = ""
									   }
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}


function GoTo(strURL)
{
    window.location.href = strURL;
}


function HTTPgo()
{
    go(document.Form1)
}


function go(frm)
{
    document.getElementById("Search_Restults").innerHTML = ""
    if (checkMyForm(frm) == false)
    {
       return false;
    }
    BuildXMLReport('/crms/FindaClient2_svr.asp'+ buildQueryString('Form1'))
}


var FormFields = new Array()

function gg(frm)
{
    FormFields[0] = "txt_FIRSTNAME2|Firstname|TEXT|Y"
    FormFields[1] = "txt_SURNAME2|Surname|TEXT|Y"
    FormFields[2] = "txt_DOB|Date of Birth|DATE|N"
    FormFields[3] = "txt_ADDRESSLINE1|Address 1|TEXT|N"
    FormFields[4] = "txt_ADDRESSLINE2|Address 2|TEXT|N"
    FormFields[5] = "txt_ADDRESSLINE3|Address 3|TEXT|N"
    FormFields[6] = "sel_LAA|Local Authority Area|SELECT|Y"
    FormFields[7] = "txt_POSTCODE2|Postcode|POSTCODE|Y"
    FormFields[8] = "txt_EMAIL2|Email|EMAIL|N"
    FormFields[9] = "txt_TELEPHONE|Telephone|TELEPHONE|N"
    FormFields[10] = "txt_MOBILE|Mobile|TELEPHONE|N"
    FormFields[11] = "txt_NINUMBER|National Insurance|NATIONAL INSURANCE|N"
    FormFields[12] = "sel_GENDER|Gender|SELECT|N"
    FormFields[13] = "sel_DISABILITY|Disability|SELECT|N"
    FormFields[14] = "sel_ETHNICITY|Ethnicity|SELECT|N"
    FormFields[15] = "sel_BENEFITRECIPIENT|Benefit Recipient|SELECT|N"
    FormFields[16] = "sel_LONEPARENT|Lone Parent|SELECT|N"
    FormFields[17] = "sel_EMPLOYMENTSTATUS|Employment Status|SELECT|N"
    FormFields[18] = "sel_LENGTHOFUNEMPLOYMENT|Length Of Unemployment|SELECT|N"
    FormFields[19] = "txt_CUSTOMER_REFERENCE|CUSTOMER REFERENCE|INTEGER|N"
    FormFields[20] = "txt_FIRSTNAME|Firstname|TEXT|N"
    FormFields[21] = "txt_SURNAME|Surname|TEXT|N"
    FormFields[22] = "txt_POSTCODE|Postcode|POSTCODE|N"
    FormFields[23] = "txt_EMAIL|Email|EMAIL|N"
    FormFields[24] = "sel_SECURITYQUESTION|Security Question|SELECT|Y"
    FormFields[25] = "txt_SECURITYANSWER|Security Answer|TEXT|Y"
    FormFields[26] = "txt_USERNAME|Username|USERNAME|Y"
    FormFields[27] = "txt_PASSWORD|Password|PASSWORD|Y"
    FormFields[28] = "txt_RETYPEPASSWORD|Password (re-type)|RETYPEPASSWORD|Y"
    FormFields[29] = "txt_DOB2|Date of Birth|DATE|Y"

	if (!checkForm('FrmEr_Edit')) return;
	document.Form1.action = "/CRMS/svr/EditaClient_svr.asp"
	document.Form1.target = "iFrame<%=TimeStamp%>"
 	document.Form1.submit()
}


function bb()
{
//alert(document.getElementById("sel_BENEFITRECIPIENT").value)
}

-->
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div style="float:left; width:48%">
			        <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                        <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>><%=passcheck_text%></div>
                        <div style="float:left; width:100%;">
                            <fieldset>
	                            <legend>Find a Client</legend>
		                        <div id="Frm" style="float:left; width:100%;">
		                        <p class="_H4">
		                        Find a Client:
		                        </p>
		                            <div class="row">
                                        <span class="label">
                                            <label for="txt_CUSTOMER_REFERENCE" accesskey="C">
                                                <span style="text-decoration:underline; cursor:pointer">C</span>ustomer Reference:</label>
                                            </span>
                                        <span class="formw">
                                            <!--
                                            validate="/([0-9,\s]{1,40})$/"
                                            //-->
                                            <input class="border-white" 
                                                onblur="this.className='border-white'" 
                                                onfocus="this.className='border-red'" 
                                                type="text" 
                                                size="40" 
                                                name="txt_CUSTOMER_REFERENCE" 
                                                id="txt_CUSTOMER_REFERENCE" 
                                                maxlength="10" 
                                                value="" 
                                                tabindex="1" 
                                                required="N" 
                                                validate="/^(\d|\d{1,9}|1\d{1,9}|20\d{8}|213\d{7}|2146\d{6}|21473\d{5}|214747\d{4}|2147482\d{3}|21474835\d{2}|214748364[0-7])$/" 
                                                validateMsg="Customer Reference" />
                                            <img src="/js/FVS.gif" name="img_CUSTOMER_REFERENCE" id="img_CUSTOMER_REFERENCE" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_FIRSTNAME" accesskey="F">
                                                <span style="text-decoration:underline; cursor:pointer">F</span>irst Name:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="40" value="" tabindex="2" required="N" validate="/([a-zA-Z0-9,\s]{1,40})$/" validateMsg="First Name" />
                                            <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_SURNAME" accesskey="S">
                                                <span style="text-decoration:underline; cursor:pointer">S</span>urname:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_SURNAME" id="txt_SURNAME" maxlength="40" value="" tabindex="3" required="N" validate="/([a-zA-Z0-9,\s]{1,40})$/" validateMsg="Surname" />
                                            <img src="/js/FVS.gif" name="img_SURNAME" id="img_SURNAME" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_POSTCODE" accesskey="P">
                                                <span style="text-decoration:underline; cursor:pointer">P</span>ostcode:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="8" value="" tabindex="4" required="N" validate="/^[A-Za-z]{1,2}\d{1,2}[A-Za-z]? \d[A-Za-z]{2}$/" validateMsg="Postcode" />
                                            <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_EMAIL" accesskey="E">
                                                <span style="text-decoration:underline; cursor:pointer">E</span>mail:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_EMAIL" id="txt_EMAIL" maxlength="250" tabindex="5" required="N" validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" validateMsg="Email" />
                                            <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                        </span>
                                   </div>
                                   <div class="row">
                                        <span class="label">
                                            <label for="txt_DOB" accesskey="E">
                                                <span style="text-decoration:underline; cursor:pointer">D</span>oB:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_DOB" id="txt_DOB" maxlength="10" value="<%=DOB%>" required="N" validate="/^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4}|[0-9]{2})$/" validateMsg="Date of Birth (dd/mm/yyyy)" />
                                            <img src="/js/FVS.gif" name="img_DOB" id="img_DOB" width="15px" height="15px" alt="" />
                                        </span>
                                   </div>
                                    <div class="row">
                                        <span class="label">&nbsp;</span>
                                        <span class="formw">
                                            <input class="submit" type="button" name="Find" id="Find" value=" Find " title="Submit Button : Find" tabindex="6" onclick="go(document.Form1)"/>
                                        </span>
                                    </div>
                                </div>
	                        </fieldset>
                        </div>
                        
                    </div>
			    </div>
			    <div id="Search_Restults" style="float:left; width:48%; margin-left:10px">

			    </div>
			</div>
			</form>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>