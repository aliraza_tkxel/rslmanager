﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%
Dim FIRSTNAME
Dim LASTNAME
Dim EMAIL
Dim TELEPHONE
Dim MOBILE
Dim PROVIDERNAME

    Const adTypeBinary = 1

Function AddFileToDB(strFilePath,ID)

On Error Resume Next

    'ADD THE FILE TO THE DATABASE
    'SETTING UP STREAM OBJECT
    Dim objStream
    Dim objFileContents
    Set objStream = CreateObject("ADODB.Stream")
        objStream.Type = adTypeBinary
        objStream.Open
        objStream.LoadFromFile strFilePath
        objFileContents = objStream.Read
        'CLEANUP
        objStream.Close
    Set objStream = Nothing

  Call OpenDB()
   Set rsUpdate = Server.CreateObject ("ADODB.Recordset")
   SQL = "SELECT * FROM TBL_MD_LOGIN WHERE LOGINID = " & ID
        rsUpdate.Open SQL, Conn, adOpenKeyset, adLockPessimistic
    If(NOT rsUpdate.EOF) Then
        rsUpdate.Fields("PHOTO").Value = objFileContents
        rsUpdate.Update()
    End If
    Set rsUpdate = Nothing
  Call CloseDB()
    'DELETE PHYSICAL FILE
    Dim fso
    Set fso = CreateObject("Scripting.FileSystemObject")
        'If FolderExists() Then
            If (fso.FileExists(strFilePath)) Then
               fso.DeleteFile(strFilePath)
  	        End if
  	    'Else
  	        '
        'End If
	Set fso = Nothing
	
' check for error & set boolean value
    'rw "AddFileToDB" & "<br/>"
    'rw Err.Number & "<br/>"
    'rw Err.source & "<br/>" 
    'rw err.description & "<br/>"
    'rw err.HelpContext
    'rw err.Source
    'err.Clear

  If Err.Number <> 0 Then
    retError = False
  Else
    retError = True
  End If

  On Error GoTo 0
  '  RW "FILE " & "<br/>" & retError
  ' return boolean value
  AddFileToDB = retError

End Function


Sub BuildUploadRequest(RequestBin,UploadDirectory,storeType,sizeLimit,nameConflict)
  'GET THE BOUNDARY
  PosBeg = 1
  PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
  if PosEnd = 0 then
    rw "<b>Form was submitted with no ENCTYPE=""multipart/form-data""</b><br>"
    rw "Please correct the form attributes and try again."
    Response.End
  End If
  'Check ADO Version
	Set checkADOConn = Server.CreateObject("ADODB.Connection")
	    adoVersion = CSng(checkADOConn.Version)
	Set checkADOConn = Nothing
	If adoVersion < 2.5 Then
        rw "<b>You don't have ADO 2.5 installed on the server.</b><br/>"
        rw "The File Upload extension needs ADO 2.5 or greater to run properly.<br/>"
        rw "You can download the latest MDAC (ADO is included) from <a href=""www.microsoft.com/data"">www.microsoft.com/data</a><br>"
        Response.End
	End If
    'CHECK CONTENT LENGTH IF NEEDED
	Length = CLng(Request.ServerVariables("HTTP_Content_Length")) 'Get Content-Length header
	If "" & sizeLimit <> "" Then
    sizeLimit = CLng(sizeLimit)
    If Length > sizeLimit Then
      Request.BinaryRead (Length)
      rw "Upload size " & FormatNumber(Length, 0) & "B exceeds limit of " & FormatNumber(sizeLimit, 0) & "B"
      Response.End
    End If
  End If
  boundary = MidB(RequestBin,PosBeg,PosEnd-PosBeg)
  boundaryPos = InstrB(1,RequestBin,boundary)
  'GET ALL DATA INSIDE THE BOUNDARIES
  Do until (boundaryPos=InstrB(RequestBin,boundary & getByteString("--")))
    'MEMBERS VARIABLE OF OBJECTS ARE PUT IN A DICTIONARY OBJECT
    Dim UploadControl
    Set UploadControl = CreateObject("Scripting.Dictionary")
    'GET AN OBJECT NAME
    Pos = InstrB(BoundaryPos,RequestBin,getByteString("Content-Disposition"))
    Pos = InstrB(Pos,RequestBin,getByteString("name="))
    PosBeg = Pos+6
    PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(34)))
    Name = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
    PosFile = InstrB(BoundaryPos,RequestBin,getByteString("filename="))
    PosBound = InstrB(PosEnd,RequestBin,boundary)
    'TEST IF OBJECT IS OF FILE TYPE
    If  PosFile<>0 AND (PosFile<PosBound) Then
      'GET FILENAME, CONTENT-TYPE AND CONTENT OF FILE
      PosBeg = PosFile + 10
      PosEnd =  InstrB(PosBeg,RequestBin,getByteString(chr(34)))
      FileName = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      FileName = Mid(FileName,InStrRev(FileName,"\")+1)
      'ADD FILENAME TO DICTIONARY OBJECT
      UploadControl.Add "FileName", FileName
      Pos = InstrB(PosEnd,RequestBin,getByteString("Content-Type:"))
      PosBeg = Pos+14
      PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
      'ADD CONTENT-TYPE TO DICTIONARY OBJECT
      ContentType = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      UploadControl.Add "ContentType",ContentType
      'GET CONTENT OF OBJECT
      PosBeg = PosEnd+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = FileName
      ValueBeg = PosBeg-1
      ValueLen = PosEnd-Posbeg
    Else
      'GET CONTENT OF OBJECT
      Pos = InstrB(Pos,RequestBin,getByteString(chr(13)))
      PosBeg = Pos+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      ValueBeg = 0
      ValueEnd = 0
    End If
    'ADD CONTENT TO DICTIONARY OBJECT
    UploadControl.Add "Value" , Value
    UploadControl.Add "ValueBeg" , ValueBeg
    UploadControl.Add "ValueLen" , ValueLen
    'ADD DICTIONARY OBJECT TO MAIN DICTIONARY
    UploadRequest.Add name, UploadControl
    'LOOP TO NEXT OBJECT
    BoundaryPos=InstrB(BoundaryPos+LenB(boundary),RequestBin,boundary)
  Loop

  GP_keys = UploadRequest.Keys
  for GP_i = 0 to UploadRequest.Count - 1
    GP_curKey = GP_keys(GP_i)
    'SAVE ALL UPLOADED FILES
    If UploadRequest.Item(GP_curKey).Item("FileName") <> "" Then
      GP_value = UploadRequest.Item(GP_curKey).Item("Value")
      GP_valueBeg = UploadRequest.Item(GP_curKey).Item("ValueBeg")
      GP_valueLen = UploadRequest.Item(GP_curKey).Item("ValueLen")

      If GP_valueLen = 0 Then
        rw "<b>An error has occured saving uploaded file!</b><br/><br/>"
        rw "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br>"
        rw "File does not exists or is empty.<br/>"
        rw "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
	  	Response.End
	  End If

      'CREATE A STREAM INSTANCE
      Dim GP_strm1, GP_strm2
      Set GP_strm1 = Server.CreateObject("ADODB.Stream")
      Set GP_strm2 = Server.CreateObject("ADODB.Stream")

      'OPEN THE STREAM
      GP_strm1.Open
      GP_strm1.Type = 1 'BINARY
      GP_strm2.Open
      GP_strm2.Type = 1 'BINARY

      GP_strm1.Write RequestBin
      GP_strm1.Position = GP_ValueBeg
      GP_strm1.CopyTo GP_strm2,GP_ValueLen

      'CREATE AND WRITE TO A FILE
      GP_curPath = Request.ServerVariables("PATH_INFO")
      GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
      If Mid(GP_curPath,Len(GP_curPath),1)  <> "/" Then
        GP_curPath = GP_curPath & "/"
      End If
      GP_CurFileName = UploadRequest.Item(GP_curKey).Item("FileName")
      GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & GP_CurFileName
      'Check if the file alreadu exist
      GP_FileExist = False
      Set fso = CreateObject("Scripting.FileSystemObject")
      If (fso.FileExists(GP_FullFileName)) Then
        GP_FileExist = True
      End If
      If nameConflict = "error" AND GP_FileExist Then
	  	Response.Redirect "FileAlreadyExists.asp?FileName=" & GP_CurFileName
        rw "<b>File already exists!</b><br/><br/>"
        rw "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
		GP_strm1.Close
		GP_strm2.Close
	  	Response.End
      End If
        If ((nameConflict = "over" OR nameConflict = "uniq") AND GP_FileExist) OR (NOT GP_FileExist) Then
            If nameConflict = "uniq" AND GP_FileExist Then
                Begin_Name_Num = 0
                    While GP_FileExist
                        Begin_Name_Num = Begin_Name_Num + 1
                        GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
                        GP_FileExist = fso.FileExists(GP_FullFileName)
                    Wend
                UploadRequest.Item(GP_curKey).Item("FileName") = fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
                UploadRequest.Item(GP_curKey).Item("Value") = UploadRequest.Item(GP_curKey).Item("FileName")
            End If
        on error resume next
        GP_strm2.SaveToFile GP_FullFileName,2
        if err then
          rw "<b>An error has occured saving uploaded file!</b><br/><br/>"
          rw "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
          rw "Maybe the destination directory does not exist, or you don't have write permission.<br/>"
          rw "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
    		  err.clear
  				GP_strm1.Close
  				GP_strm2.Close
  	  	  response.End
  	    End If
  			GP_strm1.Close
  			GP_strm2.Close
  			if storeType = "path" then
  				UploadRequest.Item(GP_curKey).Item("Value") = GP_curPath & UploadRequest.Item(GP_curKey).Item("Value")
  			End If
        on error goto 0
      End If
    End If
  next

End Sub

'String to byte string conversion
Function getByteString(StringStr)
  For i = 1 to Len(StringStr)
 	  char = Mid(StringStr,i,1)
	  getByteString = getByteString & chrB(AscB(char))
  Next
End Function

'Byte string to string conversion
Function getString(StringBin)
  getString = ""
  For intCount = 1 to LenB(StringBin)
	  getString = getString & chr(AscB(MidB(StringBin,intCount,1)))
  Next
End Function

Function UploadFormRequest(name)
  on error resume next
  if UploadRequest.Item(name) then
    UploadFormRequest = UploadRequest.Item(name).Item("Value")
  End If
End Function

'START THE UPLOAD REQUEST IF THE APPROPRIATE VARIABLE IS NOT EMPTY

Call LoadRecord()

Dim ErrorClass
Dim passcheck 		' EMAIL SENT YES/NO (REASON WHY)
    passcheck = ""
    
    
'If (CStr(Request("Save")) <> "") Then
' 
'End If

If (CStr(Request.QueryString("GP_upload")) <> "") Then
    GP_redirectPage = ""
    If (GP_redirectPage = "") Then
      GP_redirectPage = CStr(Request.ServerVariables("URL"))
    End If

        RequestBin = Request.BinaryRead(Request.TotalBytes)
    Dim UploadRequest
    Set UploadRequest = CreateObject("Scripting.Dictionary")
        BuildUploadRequest RequestBin, "PaymentCardFiles_Temp", "file", "", "error"

        UploadedFileName = UploadFormRequest("txt_PHOTO")
        'GET THE PATH OF THE FILE TO WHERE IT WAS UPLOADED...
        GP_curPath = Request.ServerVariables("PATH_INFO")
        GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
            If Mid(GP_curPath,Len(GP_curPath),1)  <> "/" Then
                GP_curPath = GP_curPath & "/"
            End If
        FullPath = Trim(Server.mappath(GP_curPath)) & "\PaymentCardFiles_Temp\"
        ActualUploadedFileName = UploadedFileName
        strFilePath = FullPath & ActualUploadedFileName

    If AddFileToDB(strFilePath,Session("svLOGINID")) Then
       passcheck_text = passcheck_text & "<div style='padding:10px;'>Photo have been updated.</div>"
        ErrorClass = " class=""no_error"""
        
        If EditRecord() Then
            passcheck_text = passcheck_text & "<div style='padding:10px;'>Adviser Record Updated.</div>"
            ErrorClass = " class=""no_error"""
        Else
            passcheck_text = passcheck_text & "<div style='padding:10px;'>There was an error updating Adviser Record Updated. Please try again.</div>"
            ErrorClass = " class=""er"""
        End If
        'Call EditRecord()
        Call LoadRecord()
        
        
    Else
        passcheck_text = passcheck_text & "<div style='padding:10px;'>There was an error updating Personal Details. Please try again.</div>"
        ErrorClass = " class=""er"""
    End If
Else
       ' NO FILE - SAVE
       'Call EditRecord()
       'Call LoadRecord()
        If (CStr(Request("Save")) <> "") Then
            'Call EditRecord()
            
            If EditRecord() Then
                passcheck_text = passcheck_text & "<div style='padding:10px;'>Adviser Record Updated.</div>"
                ErrorClass = " class=""no_error"""
            Else
                passcheck_text = passcheck_text & "<div style='padding:10px;'>There was an error updating Adviser Record Updated. Please try again.</div>"
                ErrorClass = " class=""er"""
            End If
            
        End If
        Call LoadRecord()
End If




Function EditRecord()

Call OpenDB()

    'rw "init" & "<br/>"
    'rw Err.Number & "<br/>"
    'rw Err.source & "<br/>" 
    'rw err.description & "<br/>"
    'rw err.HelpContext
    'rw err.Source
    'err.Clear

On Error Resume Next

Dim FIRSTNAME
Dim LASTNAME
Dim TELEPHONE
Dim EMAIL
Dim MOBILE

   ' If Request.TotalBytes <> 0 Then
   'rw "-" & Request.TotalBytes & "-"
   If Request("txt_PHOTO") <> "" OR  Request.TotalBytes > "142" Then
        'rw "update 1"
        FIRSTNAME = nulltest(UploadFormRequest("txt_FIRSTNAME"))
        LASTNAME = nulltest(UploadFormRequest("txt_SURNAME"))
        TELEPHONE = nulltest(UploadFormRequest("txt_TELEPHONE"))
        EMAIL = nulltest(UploadFormRequest("txt_EMAIL"))
        MOBILE = nulltest(UploadFormRequest("txt_MOBILE"))    
    Else
        'rw "update 2"
        FIRSTNAME = nulltest(Request.Form("txt_FIRSTNAME"))
        LASTNAME = nulltest(Request.Form("txt_SURNAME"))
        TELEPHONE = nulltest(Request.Form("txt_TELEPHONE"))
        EMAIL = nulltest(Request.Form("txt_EMAIL"))
        MOBILE = nulltest(Request.Form("txt_MOBILE"))    
    End If
    
    'rw FIRSTNAME & "<br/>"
    'rw LASTNAME & "<br/>"
    'rw TELEPHONE & "<br/>"
    'rw EMAIL & "<br/>"
    'rw MOBILE & "<br/>"
    'rw cint(Session("svLOGINID")) & "<br/>"

    ' .Parameters.Append .createparameter("@MOBILE",       adVarWChar, adParamInput,        20, MOBILE)
	'DSN_CONNECTION_STRING = "Provider=SQLOLEDB;Locale Identifier=2057;Data Source=(local);Initial Catalog=GM_ADVANCEMENT_NETWORK;User Id=GM_AN_User;Password=qwerty;" 

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_ADVISER_PERSONALDETAILS_UPDATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger,  adParamReturnValue,   0)
                .Parameters.Append .createparameter("@FIRSTNAME",    adVarWChar, adParamInput,        40, FIRSTNAME)
                .Parameters.Append .createparameter("@LASTNAME",     adVarWChar, adParamInput,        40, LASTNAME)               
                .Parameters.Append .createparameter("@TELEPHONE",    adVarWChar, adParamInput,        20, TELEPHONE)
                .Parameters.Append .createparameter("@EMAIL",        adVarWChar, adParamInput,       250, EMAIL)
                .Parameters.Append .createparameter("@MOBILE",       adVarWChar, adParamInput,        20, MOBILE)
                .Parameters.Append .createparameter("@LOGINID",      adInteger,  adParamInput,         0, CInt(Session("svLOGINID")))
                'Execute the function
			    .execute ,,adexecutenorecords
		    End With
	    objConn.Close
    Set objConn = Nothing

    'rw "UPDATE" & "<br/>"
    'rw Err.Number & "<br/>"
    'rw Err.source & "<br/>" 
    'rw err.description & "<br/>"
    'rw err.HelpContext
    'rw err.Source
    'err.Clear

  ' check for error & set boolean value
  If Err.Number <> 0 Then
    retError = False
  Else
    retError = True
  End If

  On Error GoTo 0
  ' return boolean value
  EditRecord = retError
  '  RW "EDIT " & "<br/>" & retError
  
  Call CloseDB()
  
End Function


Function LoadRecord()

    'rw "<br/>" & "LOAD init" & "<br/>"
    'rw Err.Number & "<br/>"
    'rw Err.source & "<br/>" 
    'rw err.description & "<br/>"
    'rw err.HelpContext
    'rw err.Source
    'err.Clear

    Call OpenDB()
    SQL = "SELECT * " &_
          " FROM DBO.TBL_MD_LOGIN  L " &_
          " INNER JOIN TBL_MD_PROVIDER P ON L.PROVIDERID = P.PROVIDERID " &_
          " WHERE LOGINID = " & Session("svLOGINID")
    Call OpenRs(rsDetails,SQL)
    If NOT rsDetails.EOF Then
        FIRSTNAME = rsDetails("FIRSTNAME")
        LASTNAME = rsDetails("LASTNAME")
        EMAIL = rsDetails("EMAIL")
	    TELEPHONE = rsDetails("TELEPHONE")
	    MOBILE = rsDetails("MOBILE")
	    PROVIDERNAME = rsDetails("PROVIDERNAME")
    End If
    Call CloseRs(rsDetails)
    Call CloseDB()

    'rw "<br/>" & "LOAD end" & "<br/>"
   ' rw Err.Number & "<br/>"
    'rw Err.source & "<br/>"
    'rw err.description & "<br/>"
    'rw err.HelpContext
    'rw err.Source
    'err.Clear

End Function

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:2px; padding-bottom:2px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);

function cck(frm)
{
    if (document.getElementById("txt_PHOTO").value == "")
    {        
        //document.forms[0].encoding = "multipart/form-data";
        document.forms[0].encoding = "";
        //document.forms[0].action = "<%=strIncludeFile%>?GP_upload=true";
        return checkMyForm(frm)
    }
    else
    {
        //document.forms[0].encoding = "multipart/form-data";
        //document.getElementById('Form1').enctype = 'multipart/form-data';
        document.forms[0].action = "<%=strIncludeFile%>?GP_upload=true";
        return checkMyForm(frm)
    }
}

//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; margin-right:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>" enctype="multipart/form-data" onsubmit="return cck(this);">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>			
			<div style="margin-left:-10px;">
			    <div style="float:left; width:48%">
			        <div id="nsform" style="float:left; width:96%; border:solid 0px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                        <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>><%=passcheck_text%></div>
                        <%'=Now() %>
                        <div style="float:left; width:100%;">
                            <fieldset>
	                            <legend>My Details</legend>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_FIRSTNAME" accesskey="F">
                                            <span style="text-decoration:underline; cursor:pointer">F</span>irst Name:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            type="text" 
                                            size="40" 
                                            name="txt_FIRSTNAME" 
                                            id="txt_FIRSTNAME" 
                                            maxlength="40" 
                                            value="<%=FIRSTNAME%>" 
                                            tabindex="2" 
                                            required="N" 
                                            validate="/([a-zA-Z0-9,\s]{1,40})$/" 
                                            validateMsg="First Name" />
                                        <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_SURNAME" accesskey="S">
                                            <span style="text-decoration:underline; cursor:pointer">S</span>urname:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            type="text" 
                                            size="40" 
                                            name="txt_SURNAME" 
                                            id="txt_SURNAME" 
                                            maxlength="40" 
                                            value="<%=LASTNAME%>"
                                            tabindex="3" 
                                            required="N" 
                                            validate="/([a-zA-Z0-9,\s]{1,40})$/" 
                                            validateMsg="Surname" />
                                        <img src="/js/FVS.gif" name="img_SURNAME" id="img_SURNAME" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_TELEPHONE" accesskey="P">
                                            <span style="text-decoration:underline; cursor:pointer">T</span>elephone:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input  
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            type="text" 
                                            size="20" 
                                            name="txt_TELEPHONE" 
                                            id="txt_TELEPHONE" 
                                            maxlength="20" 
                                            value="<%=TELEPHONE%>"
                                            tabindex="4" 
                                            required="N" 
                                            validate="/^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/" 
                                            validateMsg="Telephone" />
                                        <img src="/js/FVS.gif" name="img_TELEPHONE" id="img_TELEPHONE" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_EMAIL" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">E</span>mail:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            type="text" 
                                            size="40" 
                                            name="txt_EMAIL" 
                                            id="txt_EMAIL" 
                                            maxlength="250"
                                            value="<%=EMAIL%>"
                                            tabindex="5" 
                                            required="N" 
                                            validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" 
                                            validateMsg="Email" />
                                        <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_MOBILE" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">M</span>obile:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input 
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            type="text" 
                                            size="40" 
                                            name="txt_MOBILE" 
                                            id="txt_MOBILE" 
                                            maxlength="250"
                                            value="<%=MOBILE%>"
                                            tabindex="5" 
                                            required="N" 
                                            validate="/^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/"
                                            validateMsg="MOBILE" />
                                        <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" alt="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        <label for="txt_DOB" accesskey="E">
                                            <span style="text-decoration:underline; cursor:pointer">U</span>pload Photo:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input type="file" name="txt_PHOTO" id="txt_PHOTO" value="" />
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">&nbsp;</span>
                                    <span class="formw">
                                        <input class="submit" type="submit" name="Save" id="Save" value="Save" title="Submit Button : Save your details" style="width:auto"/>
                                    </span>
                                </div>
	                        </fieldset>
                        </div>
                    </div>
			    </div>
			    <div id="Search_Restults" style="float:right; width:48%;">
                    <!-- #include virtual="/CRMS/includes/photo.asp" -->
			    </div>
			</div>
			</form>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" name="FVS_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" name="FVW_Image"  alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>