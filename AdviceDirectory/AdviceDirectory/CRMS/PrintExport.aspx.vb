Imports System.Data

Partial Class Customer_PrintExport
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

	'If user isn't authenticated, he/she will be redirected to Login
        If IsNothing(ASPSession("svLOGINID")) Then
            Response.Redirect("http://www.greatermanchester-anp.org.uk/CRMS")
        End If

	If Not Page.IsPostBack Then
            Dim sQs As String = Request.QueryString("cache")
            If Not String.IsNullOrEmpty(sQs) Then
                Dim dt As New DataTable
                dt = Cache(sQs)                
                gvPrintReport.DataSource = dt
                gvPrintReport.DataBind()
            End If
            
        End If
    End Sub
End Class
