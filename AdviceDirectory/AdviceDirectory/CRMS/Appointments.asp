﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")
%>
<%
Dim CUSTOMER_REFERENCE
    CUSTOMER_REFERENCE = nulltest(Request("CUSTOMERID"))
    
If (NullNestTF(CUSTOMER_REFERENCE) = True) Then
    Response.Redirect ("/crms/login.asp")
Else
    If NOT IsNumeric(CUSTOMER_REFERENCE) Then
        Response.Redirect ("/crms/NoRecord.asp?Er=1")
    End If
End If
%>
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%
Dim FULLNAME

Function Record_Search(Id)

Dim rsBU

    CUSTOMER_REFERENCE = nulltest(Request("CUSTOMERID"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_FIND_CUSTOMER"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, NULL)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, NULL)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 7, NULL)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, NULL)
                'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsBU = .Execute
					If NOT rsBU.EOF Then
					    FULLNAME = rsBU("FULLNAME")
					Else
					    Response.Redirect ("/NoRecord.asp?Er=2")
					End If
		    End With
	    objConn.Close
    Set objConn = Nothing

End Function

Call Record_Search(CUSTOMER_REFERENCE)
%>
<%
Function CurrentContacts(CUSTOMER_REFERENCE)
       
'   SQL = " SELECT CONVERT(NVARCHAR,APPOINTMENTDATE, 103) AS APPOINTMENTDATE, T.TIME AS APPOINTMENTTIME, P.PROVIDERID, PROVIDERNAME, SITENAME, S.ADDRESSLINE1, S.ADDRESSLINE2, S.ADDRESSLINE3, S.ADDRESSLINE4, S.TELEPHONE, S.EMAIL, FIRSTNAME+SPACE(1)+LASTNAME AS CONTACTNAME  " &_
'        "FROM CRMS_APPOINTMENT A " &_
'	    "    INNER JOIN TBL_MD_PROVIDER P ON A.ORGANISATIONID = P.PROVIDERID " &_
'	    "    INNER JOIN TBL_MD_PROVIDER_DETAIL PD ON p.PROVIDERID = PD.PROVIDERID " &_
'	    "    INNER JOIN TBL_MD_PROVIDER_SITE S ON p.PROVIDERID = p.PROVIDERID AND A.SITEID = S.SITEID " &_
'	    "    INNER JOIN TIME T ON T.TIMEID = A.TIMEID " &_
'	    "    LEFT  JOIN TBL_MD_LOGIN AS C ON C.LOGINID = A.CONTACTID " &_
'        "WHERE CUSTOMERID = " & CUSTOMER_REFERENCE
        
  SQL = " SELECT IsNull(pd.ProviderSummary,'Awaiting Information') As ProviderSummary, JOURNEYID, HISTORYID, CONVERT(NVARCHAR,APPOINTMENTDATE, 103) AS APPOINTMENTDATE, T.TIME AS APPOINTMENTTIME, P.PROVIDERID, PROVIDERNAME, SITENAME, S.ADDRESSLINE1, S.ADDRESSLINE2, S.ADDRESSLINE3, S.ADDRESSLINE4, S.TELEPHONE, S.EMAIL, FIRSTNAME+SPACE(1)+LASTNAME AS CONTACTNAME " &_
		"FROM CRMS_APPOINTMENT OUT_A " &_
		"INNER JOIN TBL_MD_PROVIDER P ON OUT_A.ORGANISATIONID = P.PROVIDERID " &_
		"INNER JOIN TBL_MD_PROVIDER_DETAIL PD ON P.PROVIDERID = PD.PROVIDERID " &_
		"INNER JOIN TBL_MD_PROVIDER_SITE S ON P.PROVIDERID = P.PROVIDERID AND OUT_A.SITEID = S.SITEID " &_ 
		"INNER JOIN TIME T ON T.TIMEID = OUT_A.TIMEID " &_
		"LEFT JOIN TBL_MD_LOGIN AS C ON C.LOGINID = OUT_A.CONTACTID  " &_
		"WHERE CUSTOMERID = " & CUSTOMER_REFERENCE & " " &_
		"AND HISTORYID = ( " &_
		"	SELECT MAX(HISTORYID) AS HISTORYID " &_
		"	FROM CRMS_APPOINTMENT IN_A " &_
		"	WHERE OUT_A.JOURNEYID = IN_A.JOURNEYID " &_
		"	GROUP BY IN_A.JOURNEYID " &_
		")"

Call OpenDB()
Call OpenRs(rsPage,SQL)
If rsPage.EOF Then

            strResult = strResult & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""7"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Date</th>" & vbCrLf &_
                                        " <th>Time</th>" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Contact Name</th>" & vbCrLf &_
                                        " <th></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult = strResult & "<tr>" &_
	    "<td valign=""top"" colspan=""8"">There were no appointment currently saved in your calendar</td>" &_
	"</tr>" 
Else
        strResult = strResult & "<br/>" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""8"" style=""background-color:white; color:black; padding-left:0px"">Arranged Appointments</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Date</th>" & vbCrLf &_
                                        " <th>Time</th>" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Contact Name</th>" & vbCrLf &_
                                        " <th></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
   If IsNull(rsPage("ProviderID")) Then
	    strResult = strResult & "<tr>" &_
	        "<td valign=""top"" colspan=""8"">There were no results</td>" &_
	    "</tr>"
	Else

	    Do until rsPage.EOF
	        ProviderSummary = rsPage("ProviderSummary")

            APPOINTMENTDATE = rsPage("APPOINTMENTDATE")
            APPOINTMENTTIME = rsPage("APPOINTMENTTIME")
            PROVIDERNAME = rsPage("PROVIDERNAME")
            ADDRESSLINE1 = rsPage("ADDRESSLINE1")
		    TELEPHONE = rsPage("TELEPHONE")
		    EMAIL = rsPage("EMAIL")
		    If Isnull(EMAIL) or EMAIL = "" Then EMAIL = NULL Else EMAIL = "<a href=""mailto:"&EMAIL&""" title=""email:"&strProvider&""">"&EMAIL&"</a>" End If
		    CONTACTNAME = rsPage("CONTACTNAME")
		    JOURNEYID = rsPage("JOURNEYID")
		    HISTORYID = rsPage("HISTORYID")

		    strResult = strResult & "<tr>" & vbCrLf &_
		    "<td>" & APPOINTMENTDATE & "</td>" & vbCrLf &_
		    "<td>" & APPOINTMENTTIME & "</td>" & vbCrLf &_
		    "<td onmouseover=""Tip('"&ProviderSummary&"', TITLE, 'Organisation Summary', TITLEBGCOLOR, '#cc0000', BORDERCOLOR, '#cc0000', SHADOW, true, SHADOWWIDTH, 7)"" onmouseout=""UnTip()"">" & PROVIDERNAME & "</td>" & vbCrLf &_
		    "<td>" & ADDRESSLINE1 & "</td>" & vbCrLf &_
		    "<td>" & TELEPHONE & "</td>" & vbCrLf &_
		    "<td>" & EMAIL & "</td>" & vbCrLf &_
		    "<td>" & CONTACTNAME & "</td>" & vbCrLf &_
		    "<td><input type=""button"" name=""BtnEdit"" id=""BtnEdit"" value=""EDIT"" style=""float:right"" onclick=""Edit("&JOURNEYID&","&HISTORYID&")"" /></td>" & vbCrLf &_
		    "</tr>"

	        rsPage.MoveNext
	    Loop

	    End If

	    rsPage.Close
	set rsPage = Nothing

	strResult = strResult & "</tbody>" & vbCrLf
	
	strResult = strResult & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""8"">"

    'if cint(page) > cint(0) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	'end if	
	'if cint(page) + cint(maxrows) < cint(totalrows) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	'end if

End if

    strResult = strResult & "</td></tr></tfoot>" & vbCrLf &_
	                        "</table><br/><input type='button' id='BtnNew' name='BtnNew' value='New' style='float:right' onclick='New()' />"
  
    CurrentContacts = strResult

End Function

Dim ResultString
    ResultString = CurrentContacts(CUSTOMER_REFERENCE)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

-->
</style>

<style type="text/css" media="screen">

.black_overlay
{
	display: none;
	position: absolute;
	top:0px; left:0px;
	width: 100%; height: 100%;
	background-color: #BDBDBD;
	z-index:1001;
	overflow: hidden;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}

.white_content
{
	display: none;
	position: absolute;
	top: 10%; left: 10%;
	width: 400px;	height: 350px;
	padding: 16px;
	border: 1px solid #cc0000;
	background-color: white;
	z-index:1002;
	overflow: hidden;
}

</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script language="javascript" type="text/javascript">

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}

function buildQueryString(theFormName) {

  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            
            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function BuildXMLReport(str_url,elId,elED){

	//set the loading parameter
    document.getElementById(elED).innerHTML = "<div style='float:left; padding:10px;'>Please wait. Loading...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById(elED).className = "no_error"
	//call xmlhttp functions
	var strURL = str_url
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  +
									   str_url + "' could not be found.");
							   document.getElementById(elED).innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById(elED).innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById(elED).innerHTML = ""
							   }
							   // Call the desired result function
							   else {
									    document.getElementById(elId).innerHTML = strResponse
									    document.getElementById(elED).innerHTML = ""
									    document.getElementById(elED).className = ""

									    if (document.getElementById(elId).innerHTML = strResponse)
									    {
									        Toggle('1')
									    }

									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}

</script>

<script language="javascript" type="text/javascript">

function LightBox(strDiv)
{
    var thearray= new Array(strDiv);
    for(i=0; i<thearray.length; i++)
    {
        toggle(thearray[i])
    }
}


function toggle(id)
{
	var state = document.getElementById(id).style.display;
	if (state == 'block')
	{
		document.getElementById(id).style.display = 'none';
	} 
	else
	{
		document.getElementById(id).style.display = 'block';
	}
}


function New()
{
    LightBox('errordiv');
    BuildXMLReport('svr/Appointments.asp'+ buildQueryString('Form1'),'OrgDetails','errordiv')   
}


function Edit(JID, HID)
{
    LightBox('light','fade');
    setTimeout((function() {BuildXMLReport('svr/SaveAnAppointment.asp?JID='+JID+'&HID='+HID+'&action=edit','light_response','light_error')}), 100);
}


function Appointment(SID)
{
    LightBox('light','fade');
    setTimeout((function() {BuildXMLReport('svr/SaveAnAppointment.asp?SITEID='+SID,'light_response','light_error')}), 100);
}

var FormFields = new Array()

function SaveForm(ErDiv)
{
    FormFields[0] = "sel_CONTACT|Contact|SELECT|N"
    FormFields[1] = "txt_DATE|Date|DATE|Y"
    FormFields[2] = "sel_TIME|Time|SELECT|Y"

	if (!checkForm(ErDiv)) return;
	document.Form1.action = 'svr/SaveAnAppointment_svr.asp'
	document.Form1.target = "iFrame<%=TimeStamp%>";
 	document.Form1.submit();
}

function Close()
{
    LightBox('light','fade');
    document.getElementById("light_response").innerHTML = ""
    document.getElementById("light_error").innerHTML = ""
}

var plus = new Image();
    plus.src = "/images/plusImage.gif";
var minus = new Image();
    minus.src = "/images/minusImage.gif";

function toggleImg(imgName)
{
	//document.images[imgName].src = (document.images[imgName].src==plus.src) ? minus.src:plus.src;
	document.getElementById(imgName).src = (document.getElementById(imgName).src==plus.src) ? minus.src:plus.src;
	return false;
}

function Toggle(refByName)
{
    if (document.getElementById("Div_"+refByName).style.display == "block")
    {
	    document.getElementById("Div_"+refByName).style.display = "none";
	}
    else
    {
	    document.getElementById("Div_"+refByName).style.display = "block";
	}
	toggleImg('IMG_'+refByName)
}

function GoTo(strURL)
{
    window.location.href = strURL;
}


</script>

</head>
<body lang="en">
<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>">
<script type="text/javascript" src="/ClientScripting/wz_tooltip.js"></script>
<div id="fade" class="black_overlay"></div>
<div id="light" class="white_content">
    <div id="light_error" style="float:left;width:100%;"></div>
    <div id="light_response" style="float:left;width:100%;"></div>
</div>
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		    <div style="float:left; width:100%; white-space:nowrap;">
		        <div style="float:left">
		            <h1><%=FULLNAME%></h1>
		        </div>
		        <div style="float:left">
		            <ul class="navmenu2">
		                <li><a href="JavaScript:GoTo('ClientJourney.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Journey">Journey</a></li>
		                <li><a href="JavaScript:GoTo('CurrentPriorities.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Priorities">Current Priorities</a></li>
		                <li><a href="JavaScript:GoTo('CurrentContacts.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Contacts">Current Contacts</a></li>
		                <li><a href="JavaScript:GoTo('SelfAssessment.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Contacts">Self Assessment</a></li>
		                <li><a href="JavaScript:GoTo('Appointments.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Appointments">Appointments</a></li>
		                <li><a href="JavaScript:GoTo('ClientDetails.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Details">Details</a></li>
		            </ul>
		        </div>
		    </div>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red;">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <!-- ERRORS/PAGE LOAD -->
                    <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>>
                        <%=passcheck_text%>
                    </div>
                    <!-- ERRORS/PAGE LOAD -->
                    <!-- DYNAMIC PAGE LOAD -->
                    <div id="OrgDetails" style="float:left;width:100%;"><%=ResultString%></div>
                    <!-- DYNAMIC PAGE LOAD -->
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<input type="hidden" name="CID" id="CID" value="<%=Request("Customerid") %>" />
</form>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif" width="10px" height="10px" name="FVS_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif" width="10px" height="10px" name="FVW_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<iframe name="iFrame<%=TimeStamp%>" id="iFrame<%=TimeStamp%>" width="600" height="600" style="display:none" src="/dummy.asp"></iframe> 
</body>
</html>