<%@ Page Language="VB" AutoEventWireup="false" Trace="false" CodeFile="CustomerDataExport.aspx.vb"
    Inherits="ANP_Resports" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ANP Portal > Reports > Professional/Partner Login Analysis</title>

    <script type="text/javascript" language="javascript">
        function ExportXlsGo()
        {
            window.open('XlsExport.aspx?cache=CDE','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
        function PrintGo()
        {
            window.open('PrintExport.aspx?cache=CDE','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
	
	<ext:PageManager ID="pmPageManager" runat="server" />
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="false" Title="Customer Data Export" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                            <ext:Grid ID="gvReport" EnableCollapse="true" Title="Customer Data Export"
                                ShowBorder="true" AllowPaging="true" ShowHeader="true" Icon="Table" AllowSorting="true"
                                EnableHeaderMenu="true" AutoWidth="true" PageSize="15" AutoHeight="true" runat="server"
                                DataKeyNames="CUSTOMER_REF, NINUMBER, FIRSTNAME, SURNAME, ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3, POSTCODE, LOCAL_AUTHORITY,
                                            CEMAIL,
            TELEPHONE,
            MOBILE,
            DOB,
            AGEGROUPNAME,
            GENDER,
            ETHNICITY,
DISABILITY,
BENEFITS,
LONEPARENT,
EMPLOYMENT_STATUS,
LENGTH_OF_UNEMPLOYMENT,
CREATION_DATE,
ENTERED_BY"
                                OnSort="gvReport_Sort">
                                <Toolbars>
                                    <ext:Toolbar runat="server" ID="tbGrid">
                                        <Items>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblFrom" runat="server" Text="From:">
                                            </ext:Label>
                                            <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                                ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblTo" runat="server" Text="To:">
                                            </ext:Label>
                                            <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                                DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                                Label="To Date" runat="server">
                                            </ext:DatePicker>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator3" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label Text="Local Authority Area: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                                DataTextField="LocalAuthorityAreaId" DataValueField="LocalAuthorityArea">
                                            </ext:DropDownList>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator4" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Label ID="lblAgeRange" runat="server" Text="Age Range:">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selAgeRange" ID="selAgeRange"
                                                DataTextField="AgeRangeId" DataValueField="AgeRange">
                                            </ext:DropDownList>
                                            <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                                Icon="reload">
                                            </ext:Button>
                                            <ext:ToolbarSeparator runat="server" ID="ts1">
                                            </ext:ToolbarSeparator>
                                            <ext:ToolbarFill ID="tbFill" runat="server">
                                            </ext:ToolbarFill>
                                            <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Text="To Xls" ToolTip="Export to Excel"
                                                runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                            </ext:Button>
                                            <ext:ToolbarSeparator ID="ts2" runat="server">
                                            </ext:ToolbarSeparator>
                                            <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" ToolTip="Print" Text="Print"
                                                runat="server" Icon="Printer">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </Toolbars>
                                <Columns>
                                    <ext:BoundField DataField="CUSTOMER_REF" SortField="CUSTOMER_REF" HeaderText="Customer Reference" />
                                    <ext:BoundField DataField="NINUMBER" SortField="NINUMBER" HeaderText="National Insurance Number" />
                                    <ext:BoundField DataField="FIRSTNAME" SortField="FIRSTNAME" HeaderText="Firstname" />
                                    <ext:BoundField DataField="SURNAME" SortField="SURNAME" HeaderText="Surname" />
                                    <ext:BoundField DataField="ADDRESSLINE1" SortField="ADDRESSLINE1" HeaderText="Address Line 1" />
                                    <ext:BoundField DataField="ADDRESSLINE2" SortField="ADDRESSLINE2" HeaderText="Address Line 2" />
                                    <ext:BoundField DataField="ADDRESSLINE3" SortField="ADDRESSLINE3" HeaderText="Address Line 3" />
                                    <ext:BoundField DataField="POSTCODE" SortField="POSTCODE" HeaderText="Postcode" />
                                    <ext:BoundField DataField="LOCAL_AUTHORITY" SortField="LOCAL_AUTHORITY" HeaderText="Local Authority" />
                                    <ext:BoundField DataField="EMAIL" SortField="EMAIL" HeaderText="Email" />
                                    <ext:BoundField DataField="TELEPHONE" SortField="TELEPHONE" HeaderText="Telephone" />
                                    <ext:BoundField DataField="MOBILE" SortField="MOBILE" HeaderText="Mobile" />
                                    <ext:BoundField DataField="DOB" SortField="DOB" HeaderText="DOB" DataFormatString="{0:d}" />
                                    <ext:BoundField DataField="AGEGROUPNAME" SortField="AGEGROUPNAME" HeaderText="Agegroup" />
                                    <ext:BoundField DataField="GENDER" SortField="GENDER" HeaderText="Gender" />
                                    <ext:BoundField DataField="ETHNICITY" SortField="ETHNICITY" HeaderText="Ethnicity" />
                                    <ext:BoundField DataField="DISABILITY" SortField="DISABILITY" HeaderText="Disability" />
                                    <ext:BoundField DataField="BENEFITS" SortField="BENEFITS" HeaderText="Benefits" />
                                    <ext:BoundField DataField="LONEPARENT" SortField="LONEPARENT" HeaderText="Loneparent" />
                                    <ext:BoundField DataField="EMPLOYMENT_STATUS" SortField="EMPLOYMENT_STATUS" HeaderText="Employment Status" />
                                    <ext:BoundField DataField="LENGTH_OF_UNEMPLOYMENT" SortField="LENGTH_OF_UNEMPLOYMENT" HeaderText="Length of Unemployment" />
                                    <ext:BoundField DataField="CREATION_DATE" SortField="CREATION_DATE" HeaderText="Creation Date" DataFormatString="{0:d}" />
                                    <ext:BoundField DataField="ENTERED_BY" SortField="ENTERED_BY" HeaderText="Entered By" />
                                </Columns>
                            </ext:Grid>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
 
    </form>
</body>
</html>
