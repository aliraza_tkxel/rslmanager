Imports System.Data
Imports System.Data.SqlClient
Imports System.io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
'Imports MSDN.SessionPage
'Imports System.Web.UI.Page

Partial Class ANP_Resports
    Inherits MSDN.SessionPage

    'Inherits System.Web.UI.Page

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim SelectedIndex As Int32
        'SelectedIndex = selOrganisation.SelectedIndex.ToString()
        'Response.Write("Organisation Selected :" & SelectedIndex & "")

        'If user isn't authenticated, he/she will be redirected to Login
        If IsNothing(ASPSession("svLOGINID")) Then
            Response.Redirect("http://www.greatermanchester-anp.org.uk/CRMS")
        End If

        If Not Page.IsPostBack Then

            dpFrom.Text = Date.Today.AddMonths(-1).ToString
            dpTo.Text = Date.Today.ToString
            'Trace.Warn("Page_Load")

            BindDDListBox()
            BindAgeRangeListBox()

            If selLocalAuthorityArea.SelectedValue = Nothing Then
            Else
                BindReport()
            End If

        End If

    End Sub

    Private Sub BindReport(Optional ByVal e As ExtAspNet.GridSortEventArgs = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)

            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()

                    Dim Postcode As String
                    'If Not String.IsNullOrEmpty(txtPostcode.Text) Then
                    'Postcode = txtPostcode.Text.Trim
                    'Else
                    Postcode = Nothing
                    'End If

                    Dim LocalAuthorityAreaValue As Nullable(Of Integer)
                    If Not String.IsNullOrEmpty(selLocalAuthorityArea.SelectedValue) Then
                        LocalAuthorityAreaValue = Convert.ToInt32(selLocalAuthorityArea.SelectedValue)
                    End If

                    Dim AgeRangeValue As Nullable(Of Integer)
                    If Not String.IsNullOrEmpty(selAgeRange.SelectedValue) Then
                        AgeRangeValue = Convert.ToInt32(selAgeRange.SelectedValue)
                    End If

                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_REPORTS_GETCUSTOMERS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@FROMDATE", dpFrom.Text)
                    sqlCmd.Parameters.AddWithValue("@TODATE", dpTo.Text)
                    sqlCmd.Parameters.AddWithValue("@LOCALAUTHORITYAREAID", LocalAuthorityAreaValue)
                    sqlCmd.Parameters.AddWithValue("@AGERANGEID", AgeRangeValue)

                    Dim dt As New DataTable

                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(dt)

                    Dim dv As New DataView(dt)

                    If e IsNot Nothing Then
                        dv.Sort = String.Format("{0} {1}", e.SortField, e.SortDirection)
                    Else
                        gvReport.CurrentSortColumnIndex = 0
                        gvReport.Columns(0).SortDirection = "DESC"
                    End If

                    dt = dv.ToTable()
                    Cache("CDE") = dt
                    gvReport.DataSource = dt
                    gvReport.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using
    End Sub


    Private Sub BindDDListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_LOCALAUTHORITYAREA"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    selLocalAuthorityArea.DataSource = ds.Tables(0)
                    selLocalAuthorityArea.DataTextField = ds.Tables(0).Columns("LocalAuthorityArea").ColumnName.ToString()
                    selLocalAuthorityArea.DataValueField = ds.Tables(0).Columns("LocalAuthorityAreaId").ColumnName.ToString()
                    selLocalAuthorityArea.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub


    Private Sub BindAgeRangeListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_AGERANGE"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    selAgeRange.DataSource = ds.Tables(0)
                    selAgeRange.DataTextField = ds.Tables(0).Columns("AGEGROUP").ColumnName.ToString()
                    selAgeRange.DataValueField = ds.Tables(0).Columns("AGEGROUPID").ColumnName.ToString()
                    selAgeRange.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Protected Sub gvReport_PageIndexChange(ByVal sender As Object, ByVal e As ExtAspNet.GridPageEventArgs) Handles gvReport.PageIndexChange
        gvReport.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvReport_Sort(ByVal sender As Object, ByVal e As ExtAspNet.GridSortEventArgs) Handles gvReport.Sort
        BindReport(e)
    End Sub


    Protected Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        gvReport.DataBind()
    End Sub


    Protected Sub btnRefresh_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReport()
    End Sub

    Protected Sub selLocalAuthorityArea_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selLocalAuthorityArea.PreRender
        If selLocalAuthorityArea.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selLocalAuthorityArea.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selLocalAuthorityArea.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
    End Sub

    Protected Sub selAgeRange_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selAgeRange.PreRender
        If selAgeRange.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selAgeRange.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selAgeRange.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
    End Sub

End Class
