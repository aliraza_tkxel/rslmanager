Imports System.Data
Imports System.Data.SqlClient
Imports System.io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
'Imports MSDN.SessionPage
'Imports System.Web.UI.Page

Partial Class CRMS_NewTenancyList
    Inherits MSDN.SessionPage

    'Inherits System.Web.UI.Page

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim SelectedIndex As Int32
        'SelectedIndex = selOrganisation.SelectedIndex.ToString()
        'Response.Write("Organisation Selected :" & SelectedIndex & "")

        If Not Page.IsPostBack Then

            dpFrom.Text = Date.Today.AddMonths(-1).ToString
            dpTo.Text = Date.Today.ToString
            'Trace.Warn("Page_Load")

            BindDDListBox()

            If selOrganisation.SelectedValue = Nothing Then
            Else
                BindReport()
            End If

        End If

    End Sub

    Private Sub BindReport(Optional ByVal e As ExtAspNet.GridSortEventArgs = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)

            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()

                    Dim AdviserName As String
                    If Not String.IsNullOrEmpty(txtAdviserName.Text) Then
                        AdviserName = txtAdviserName.Text.Trim
                    Else
                        AdviserName = Nothing
                    End If

                    Dim OrgValue As Nullable(Of Integer)
                    If Not String.IsNullOrEmpty(selOrganisation.SelectedValue) Then
                        OrgValue = Convert.ToInt32(selOrganisation.SelectedValue)
                    End If

                    'Trace.Warn(OrgValue)

                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_REPORTS_PP_LOGIN_ANALYSIS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@FROMDATE", dpFrom.Text)
                    sqlCmd.Parameters.AddWithValue("@TODATE", dpTo.Text)
                    sqlCmd.Parameters.AddWithValue("@ORGANISATIONID", OrgValue)
                    sqlCmd.Parameters.AddWithValue("@ADVISERNAME", AdviserName)

                    Dim dt As New DataTable

                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(dt)

                    Dim dv As New DataView(dt)

                    If e IsNot Nothing Then
                        dv.Sort = String.Format("{0} {1}", e.SortField, e.SortDirection)
                    Else
                        gvReport.CurrentSortColumnIndex = 0
                        gvReport.Columns(0).SortDirection = "DESC"
                    End If

                    dt = dv.ToTable()
                    Cache("PPLA") = dt
                    gvReport.DataSource = dt
                    gvReport.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using
    End Sub


    Private Sub BindDDListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_ORGANISATIONS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@LOGINID", "1")
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    'selOrganisation.DataSource = ds
                    selOrganisation.DataSource = ds.Tables(0)
                    selOrganisation.DataTextField = ds.Tables(0).Columns("ProviderName").ColumnName.ToString()
                    selOrganisation.DataValueField = ds.Tables(0).Columns("ProviderID").ColumnName.ToString()
                    'NORMAL ASP.NET WAY
                    'selOrganisation.Items.Insert(0, New ListItem("***Select ***", "0"))
                    'EXT ASPNET WAY
                    'see PreRender
                    selOrganisation.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Protected Sub gvReport_PageIndexChange(ByVal sender As Object, ByVal e As ExtAspNet.GridPageEventArgs) Handles gvReport.PageIndexChange
        gvReport.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvReport_Sort(ByVal sender As Object, ByVal e As ExtAspNet.GridSortEventArgs) Handles gvReport.Sort
        BindReport(e)
    End Sub


    Protected Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        gvReport.DataBind()
    End Sub


    Protected Sub btnRefresh_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReport()
        'lblFrom.Text = selOrganisation.SelectedValue
    End Sub

    Protected Sub selOrganisation_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selOrganisation.PreRender
        'selOrganisation.Items.Clear()
        If selOrganisation.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selOrganisation.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selOrganisation.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
        'selOrganisation.Items.Add("***Please Select***", "-1")
        'selOrganisation.Items(0).Selected = True
    End Sub
End Class
