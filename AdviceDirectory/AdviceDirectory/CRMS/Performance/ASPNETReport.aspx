﻿<%@ Page Language="VB" AutoEventWireup="false" Trace="false" CodeFile="ASPNETReport.aspx.vb"
    Inherits="CRMS_NewTenancyList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>
        <!--Graph google spreadsheet data Raphael.js//-->
    </title>
    <link rel="stylesheet" href="tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />

    <script type="text/javascript" language="javascript" src="raphael.js"></script>

    <script type="text/javascript" language="javascript" src="g.raphael.js"></script>

    <script type="text/javascript" language="javascript" src="g.bar.js"></script>

    <script type="text/javascript" language="javascript" src="jquery-1.3.2.min.js"></script>

    <script type="text/javascript" language="javascript" src="tablesorter/jquery.tablesorter.js"></script>

    <script type="text/javascript" language="javascript" src="tablesorter/addons/pager/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript"> 
        /*
         * Specify the information about our spreadsheet
         */
        var spreadsheet_id = "tiRm6cBSE3AZJnyLC6Wlysw",
        worksheet_id = "od6";
        /*
         * Creates a script tag in the page that loads in the 
         * JSON feed for the specified key/ID. 
         * Once loaded, it calls loadGraphJSON.
         */
        function getJSON() {
          var script = document.createElement('script');
          
          script.setAttribute('src', 'http://spreadsheets.google.com/feeds/list'
                                 + '/' + spreadsheet_id + '/' + worksheet_id + '/public/values' +
                                '?alt=json-in-script&callback=loadGraphJSON');
          // insert the script tag into the header
          document.documentElement.firstChild.appendChild(script);
        }
        
        /**
         * Extract the information we need into 2 lists (data and labels)
         * We iterate over the rows (json.feed.entry) of the json object
         * and extract our day and distance columns.
         * Once this list created, we call drawChart to draw the graph
         */
        function loadGraphJSON(json) {
            var data = [];
            var labels = [];
            for (var i = 0; i < json.feed.entry.length; i++) {
                var entry = json.feed.entry[i];
                //this is our first column
                labels.push(entry["gsx$day"].$t);
                // this is our second column
                // We need to convert it to float because we get the data as string
                data.push(parseFloat(entry["gsx$distance"].$t));
            }
            drawChart(labels, data);
        }
        
        function drawChart(labels, data) {
            /* 
             * Create an instance of raphael and specify:
             * the ID of the div where to insert the graph
             * the width
             * the height
             */
            var r = Raphael("dv_graph", 600, 300);
            
            /*
             * Create the chart at the position with the parameters:
             * * pos on the x axis where the drawing start
             * * pos on the y axis where is drawing start
             * * width
             * * height
             * * the values: it needs to be a list of list since you can have multiple data
             * * extra parameters:
             *      stacked: Putting stacked to false seems to create a problem with the labels
             *      type: the end of the bar, it can be: round sharp soft
             */
            //var chart = r.g.barchart(10, 10, 580, 280, [data], {stacked: true, type: "soft"});
            var chart = r.g.barchart(10, 10, 580, 280, [data], {stacked: true, type: "soft", legend: ["%%.%% – Enterprise Users", "IE Users"], legendpos: "west", id: [1,2,3,4,5,6,7], href: ["JavaScript:JsMe(1)", "JavaScript:JsMe(2)", "JavaScript:JsMe(3)", "JavaScript:JsMe(4)", "JavaScript:JsMe(5)", "JavaScript:JsMe(6)", "JavaScript:JsMe(7)"]});
            
            //var chart = r.g.barchart(30, 30, 500, 400, [data]).label(["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"], true, true);

            /*
             * Create an hover effect to display the value when the mouse is over the graph.
             */
            chart.hover(function() {
                // Create a popup element on top of the bar
                this.flag = r.g.popup(this.bar.x, this.bar.y, (this.bar.value || "0") + "").insertBefore(this);
            }, function() {
                // hide the popup element with an animation and remove the popup element at the end
                this.flag.animate({opacity: 0}, 300, function () {this.remove();});
            });
            
             chart.click(function () {
             
             for (var i = 0; i < chart.bars[0].length; i++) {
                var bar = chart.bars[0][i];
                    bar.attr("fill", "#2f69bf");
                    bar.attr("stroke", "#2f69bf");
             }
             
              //if (this.bar.label) {
                 //   alert(this.value)
                 //      }
                 this.bar.attr("fill", "red");
                 this.bar.attr("stroke", "red");
                 //this.bar.attr("id", "booo");
               });

//             chart.click(function () {
//              if (this.value) {
//                    alert(this.value)
//                    //this.bar.animate({scale: [1, 1, this.bar.x, this.bar.y]}, 500, "bounce");
//                       }
//            });
                              

               
                chart.click(function () {
                    this.bar.scale(1.1, 1.1, this.cx, this.cy);
                    this.bar.animate({scale: 1}, 500, "bounce");
                }//, function () {
                    //alert(this.value)
                    //this.bar.animate({href: "JavaScript:alert('hi')"}, 500, "bounce");
                    //if (this.label) {
                    //    this.label[0].animate({scale: 1}, 500, "bounce");
                    //    this.label[1].attr({"font-weight": 400});
                    //}
                //}
                );
 
                      
            
            /*
             * Define the default text attributes before writing the labels
             * you can find pore information about the available attributes at:
             *   http://raphaeljs.com/reference.html#attr
             * and in the SVG specification:
             *   http://www.w3.org/TR/SVG/
             */ 
            r.g.txtattr = {font:"12px Fontin-Sans, Arial, sans-serif", fill:"#000", "font-weight": "bold"};
            
            /*
             * We write the labels.
             * There is a bug not fixed at the time of writing this article. We added a patch to 
             * g.bar.js : http://github.com/DmitryBaranovskiy/g.raphael/issues#issue/11
             */
            chart.label(labels);
            
            /*
             * Set all the bar greater or equals than 15 to red
             */
            // iterate over all the bar
             
            //for (var i = 0; i < chart.bars[0].length; i++) {
                //var bar = chart.bars[0][i];
                //alert(chart.labels[i].value)
                //$("svg rect").attr("id",chart.label[i]);
                
                // alert(bar)
                // bar.attr("id",opts.id[i])
                // if the value of the bar is greater or equals to 15 we change the color to red                
                //if (bar.value >= 15) {
                    //bar.attr("fill", "#bf2f2f");
                    //bar.attr("fill", "black");
                    //bar.attr("id", "blar"+i+"");
                //}
            //}
        }
        
        window.onload = function() {
            //getJSON();            
            drawChart(['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],[1,10,15,2,7,10,21])
            //drawChart([1,2,3,4,5,6,7],[1,10,15,2,7,10,21])
        }   
        
        
$(document).ready(function() 
{    
    //$("body").attr("id","home-page");
    // Match all link elements with href attributes within the content div
    //$('#content a[href]').qtip(
    //{    
    //    content: '<b>Some basic content for the tooltip</b>' // Give it some content, in this case a simple string
    //})
});


//$(document).ready(function() {
//$('a').hover(function() {
//$.ajax({ url: "main_rpc.php", context: $(this).parent(), success: function(data, textStatus){
////this.context.html('ok');
////alert(data)
//}});
//return false;
//});
//});

//$(document).ready(function(){ 
//   $("#generate").click(function(){ 
//   
//   	
//		$('#waiting').show(500);
//		$('#demoForm').hide(0);
//		$('#message').hide(0);
//		
//		$.ajax({
//			type : 'POST',
//			url : 'post.txt',
//			dataType : 'json',
//			data: {
//				email : $('#email').val(),
//				url	  : $('#url').val(),
//				name  : $('#name').val()
//			},
//			success : function(data){
//				$('#waiting').hide(500);
//				$('#message').removeClass().addClass((data.error === true) ? 'error' : 'success')
//					.text(data.msg).show(500);
//				if (data.error === true)
//					$('#demoForm').show(500);
//			},
//			error : function(XMLHttpRequest, textStatus, errorThrown) {
//				$('#waiting').hide(500);
//				$('#message').removeClass().addClass('error')
//					.text('There was an error.').show(500);
//				$('#demoForm').show(500);
//			}
//		});
//		
//		return false;

//   }); 
// }); 
    
   
   
     //$("#quote").load("script.txt");
     
//     $.get(
//	"http://gmanc.localhost/data.asp",
//	"{key:value}",
//	function(data) { alert(data); },
//	"html"
//);

//$.getJSON("http://api.flickr.com/services/feeds/photos_public.gne?tags=cat&tagmode=any&format=json&jsoncallback=?",
//        function(data){
//          $.each(data.items, function(i,item){
//            $("<img/>").attr("src", item.media.m).appendTo("#images");
//            if ( i == 3 ) return false;
//          });
//        });

       


 
    </script>

    <script type="text/javascript">
//$(document).ready(function() { 
    //$("table") 
    //.tablesorter({widthFixed: true, widgets: ['zebra']}) 
    //.tablesorterPager({container: $("#pager")});     
//}); 

//$(function() {		
		//$("#tablesorter-demo").tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']});
		//$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}});
	//});	
	
	function JsMe(rsId)
	{
	    //alert(rsId);
	    $('[name=ColId]').val(rsId);

	    $('#waiting').show(500);
	    $('#demoForm').hide(0);
	    $('#message').hide(0);
	    $('#demoForm').show(500);
	    	
	    $.ajax({
			type : 'POST',
			url : 'script3.asp',
			dataType : 'json',
			data: {
				dtfrom  : $('#dtfrom').val(),
				dtto	: $('#dtto').val(),
				rsId    : $('#ColId').val(),
				LAA     : $('#LAA').val()
			},
			success : function(data){
			$('#dv_tbl').html(data.msg);
			$("table")
			    //.tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']})
			    //.tablesorter({sortList:[[0,0], [2,0]], widgets: ['zebra']})
			    .tablesorter({sortList:[[2,1]], widgets: ['zebra']})
		        .tablesorterPager({container: $("#pager")});
		        
		    	$('#waiting').hide(500);
				$('#message').removeClass().addClass((data.error === true) ? 'error' : 'success')
				if (data.error === true)
					$('#demoForm').show(500);
		    
            }
            });
        
	}
	
	
//	$(function() {	
//	//function JsMe(val)
//	//{

//	   $('#submit').click(function() {
//	   
//		$('#waiting').show(500);
//		$('#demoForm').hide(0);
//		$('#message').hide(0);
//		
//		$.ajax({
//			type : 'POST',
//			url : 'script3.txt',
//			dataType : 'json',
//			data: {
//				email : $('#email').val(),
//				url	  : $('#url').val(),
//				name  : $('#name').val()
//			},
//			success : function(data){
//			$('#dv_tbl').html(data.msg);
//			
//		    $("table")
//			    .tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']})
//		        .tablesorterPager({container: $("#pager")});

//			
//				$('#waiting').hide(500);
//				$('#message').removeClass().addClass((data.error === true) ? 'error' : 'success')
//				if (data.error === true)
//					$('#demoForm').show(500);
//			},
//			error : function(XMLHttpRequest, textStatus, errorThrown) {
//				$('#waiting').hide(500);
//				$('#message').removeClass().addClass('error')
//					.text('There was an error.').show(500);
//				$('#demoForm').show(500);
//			}
//		});
//		
//		return false;
//	});
//	

//	 });	  

	//}
	
</script>

</head>
<body>
<a href="#" onclick="JavaScript:JsMe(1)">click me</a>
<form action="" id="demoForm" method="post">
        <div id="wrapper" style="clear:both">
            <div id="message" style="display: none;">
            </div>
            <div id="waiting" style="display: none;">
                Please wait<br />
                <img src="images/ajax-loader.gif" title="Loader" alt="Loader" />
            </div>
            <fieldset>
                <legend>Referrals Recieved</legend>
                <p>
                    <label for="dtfrom">
                        from:</label>
                    <input type="text" name="dtfrom" id="dtfrom" value="" />
                </p>
                <p>
                    <label for="dtto">
                        to:</label>
                    <input type="text" name="dtto" id="dtto" value="" />
                </p>
                 <p>
                    <label for="ColId">
                        ColId:</label>
                    <input type="text" name="ColId" id="ColId" value="" />
                    <input type="hidden" name="LAA" id="LAA" value="" />
                </p>
                <p>
                    <input type="submit" name="submit" id="submit" value="Submit" />
                </p>
            </fieldset>
        </div>
    <div style="float: left">
        <div id="dv_graph">
        </div>
    </div>
    <div style="float: left">
        <div id="dv_tbl">
        </div>
    </div>   

        <img src="" alt="" id="images" style="visibility: hidden" />
        <div id="content" style="visibility: hidden"><a href="#">xx</a></div>
        <input type="submit" id="generate" value="Generate!" style="visibility: hidden" />
    </form>
</body>
</html>
