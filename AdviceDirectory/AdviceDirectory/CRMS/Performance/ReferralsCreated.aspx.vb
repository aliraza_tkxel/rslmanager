Imports System.Data
Imports System.Data.SqlClient
Imports System.io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.HtmlControls.HtmlGenericControl
Imports System
Imports System.Globalization
Imports System.Threading

Partial Class ReferralsCreated
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

    Public Shared Function stringToDate(ByVal str As String) As DateTime

        Return DateTime.ParseExact(str, "dd/MM/yyyy", Nothing)
        Return Nothing

    End Function

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim date1 As Date = #8/29/2008 7:27:15 PM#
        'Response.Write(date1.ToString("d, M", _
        '                 CultureInfo.InvariantCulture))
        ' Displays 29, 8
        'Response.Write(date1.ToString("d MMMM yyyy", _
        '                 CultureInfo.CreateSpecificCulture("en-GB")))
        ' Displays 29 August


        'Dim SelectedIndex As Int32
        'SelectedIndex = selOrganisation.SelectedIndex.ToString()
        'Response.Write("Organisation Selected :" & SelectedIndex & "")

        'Dim BLAR As String
        'BLAR = ""

        If Not Page.IsPostBack Then

            dpFrom.Text = Date.Today.AddMonths(-1).ToString
            dpTo.Text = Date.Today.ToString

            'Response.Write("Not Post Back")

            'GraphBind()
            'Response.Write(Date.Today.ToString("d MMMM yyyy", _
            '             CultureInfo.CreateSpecificCulture("en-GB")))
            'Trace.Warn("Page_Load")

            BindDDListBox()

            If selLocalAuthorityArea.SelectedValue = Nothing Then
            Else
                'BindReport()
            End If
        Else

            'Response.Write("From <br/>")
            'Response.Write(dpFrom.Text)
            'Response.Write("To <br/>")
            'Response.Write(dpTo.Text)

        End If

    End Sub


    Private Function PopulateJavaScriptArray2(ByVal BLAR) As String
        Return BLAR
    End Function


    Protected Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        GraphBind()
    End Sub


    Protected Sub btnRefresh_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        GraphBind()
    End Sub


    Private Sub GraphBind(Optional ByVal e As ExtAspNet.GridSortEventArgs = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()

                    Dim LocalAuthorityAreaValue As Nullable(Of Integer)
                    If Not String.IsNullOrEmpty(selLocalAuthorityArea.SelectedValue) Then
                        LocalAuthorityAreaValue = Convert.ToInt32(selLocalAuthorityArea.SelectedValue)
                    End If

                    Dim procFromDate As DateTime = stringToDate(Me.dpFrom.Text)
                    Dim procToDate As DateTime = stringToDate(Me.dpTo.Text)

                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_REPORTS_REFERRALS_CREATED_GRAPH"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.AddWithValue("@LOGINID", ASPSession("svLOGINID"))
                    sqlCmd.Parameters.AddWithValue("@FROMDATE", procFromDate)    'dpFrom.Text
                    sqlCmd.Parameters.AddWithValue("@TODATE", procToDate)        'dpTo.Text
                    sqlCmd.Parameters.AddWithValue("@LocalAuthorityAreaId", LocalAuthorityAreaValue)

                    Dim dtbl As New DataTable

                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(dtbl)

                    Dim dv As New DataView(dtbl)

                    dtbl = dv.ToTable()

                    Dim dr As DataRow

                    If dtbl.Rows.Count > 0 Then
                        Dim myJS As StringBuilder = New StringBuilder()
                        'myJS.Append("<script language='Javascript'>")
                        For Each dr In dtbl.Rows
                            myJS.Append("OrgId.push('JavaScript:JsMe(" & dr("ProviderId") & ")');")
                            myJS.Append("OrgLbl.push('" & Replace(dr("ProviderName"), "'", "") & "');")
                            myJS.Append("OrgVal.push(" & dr("REC_COUNT") & ");")
                        Next
                        'myJS.Append("</script>")
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "MyScript", _
                        PopulateJavaScriptArray2(myJS.ToString()), True)
                    Else
                        'dv_graph.innerHTML = "No Results"
                        ExtAspNet.Alert.Show("No Results")
                    End If

                    sqlCon.Close()

                End Using
            End Using
        End Using

    End Sub

    Private Sub BindDDListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_LOCALAUTHORITYAREA"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    selLocalAuthorityArea.DataSource = ds.Tables(0)
                    selLocalAuthorityArea.DataTextField = ds.Tables(0).Columns("LocalAuthorityArea").ColumnName.ToString()
                    selLocalAuthorityArea.DataValueField = ds.Tables(0).Columns("LocalAuthorityAreaId").ColumnName.ToString()
                    selLocalAuthorityArea.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Protected Sub selLocalAuthorityArea_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selLocalAuthorityArea.PreRender
        If selLocalAuthorityArea.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selLocalAuthorityArea.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selLocalAuthorityArea.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
    End Sub

End Class
