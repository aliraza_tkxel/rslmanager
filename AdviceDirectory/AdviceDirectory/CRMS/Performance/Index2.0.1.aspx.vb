Imports System.Data
Imports System.Data.SqlClient
Imports System.io
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.HtmlControls.HtmlGenericControl

Partial Class CRMS_Referral_Index2
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim SelectedIndex As Int32
        'SelectedIndex = selOrganisation.SelectedIndex.ToString()
        'Response.Write("Organisation Selected :" & SelectedIndex & "")

        If Not Page.IsPostBack Then

            dpFrom.Text = Date.Today.AddMonths(-1).ToString
            dpTo.Text = Date.Today.ToString
            'Trace.Warn("Page_Load")

            BindDDListBox()

            If selLocalAuthorityArea.SelectedValue = Nothing Then
            Else
                'BindReport()
            End If

        End If

    End Sub

    Protected Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        'gvReport.DataBind()
    End Sub


    Protected Sub btnRefresh_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        'BindReport()
    End Sub

    Private Sub BindDDListBox(Optional ByVal e As ExtAspNet.DropDownList = Nothing)

        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("ANP_Portal").ConnectionString)
            Using sqlCmd As New SqlCommand
                Using sqlDa As New SqlDataAdapter
                    sqlCon.Open()
                    sqlCmd.Connection = sqlCon
                    sqlCmd.CommandText = "CRMS_LOCALAUTHORITYAREA"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    ds = New DataSet
                    sqlDa.SelectCommand = sqlCmd
                    sqlDa.Fill(ds, "AllTables")
                    selLocalAuthorityArea.DataSource = ds.Tables(0)
                    selLocalAuthorityArea.DataTextField = ds.Tables(0).Columns("LocalAuthorityArea").ColumnName.ToString()
                    selLocalAuthorityArea.DataValueField = ds.Tables(0).Columns("LocalAuthorityAreaId").ColumnName.ToString()
                    selLocalAuthorityArea.DataBind()
                    sqlCon.Close()
                End Using
            End Using
        End Using

    End Sub

    Protected Sub selLocalAuthorityArea_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles selLocalAuthorityArea.PreRender
        If selLocalAuthorityArea.Items(0).Value = -1 Then
            ' Do nothing
        Else
            selLocalAuthorityArea.Items.Insert(0, New ExtAspNet.ListItem("***Please Select***", "-1"))
            selLocalAuthorityArea.Items.Insert(1, New ExtAspNet.ListItem("All", Nothing))
        End If
    End Sub

End Class
