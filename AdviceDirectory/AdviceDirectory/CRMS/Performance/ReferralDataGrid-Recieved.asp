<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" --> 
<%
Dim rsId
    rsId = Request("rsId")

    FROMDATE = nulltest(Request("dtfrom"))
    FROMTO = nulltest(Request("dtto"))
    LAA = nulltest(Request("LAA"))

    If LAA = "All" Then
        LAA = Null
    End If

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_REPORTS_REFERRALS_RECIEVED_JQUERY"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@FROMDATE", adDBTimeStamp, adParamInput, 0, FROMDATE)
                .Parameters.Append .createparameter("@TODATE", adDBTimeStamp, adParamInput, 40, FROMTO)
                .Parameters.Append .createparameter("@LocalAuthorityAreaId", adVarChar, adParamInput, 2147483647, LAA)
                .Parameters.Append .createparameter("@PROVIDERID", adInteger, adParamInput, 0, rsId)
                'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsBU = .Execute
					If NOT rsBU.EOF Then
					i = 0
					rw ("{""success"":true,""msg"":""<br/><b>To: "&rsBU(0)&"</b><br/><table id='tablesorter-demo' class='tablesorter' border='0' cellpadding='0' cellspacing='1'><colgroup><col style='width: 211px'><col style='width: 211px'><col style='width: 121px'><col style='width: 153px'><col style='width: 194px'><col style='width: 175px'><col style='width: 201px'></colgroup><thead><tr><th>From</th><th>By</th><th>Referrals</th></tr></thead><tbody>")
					    While NOT rsBU.EOF
					    RowCount = rsBU(3)
    					rw "<tr><td>"&rsBU(1)&"</td><td>"&rsBU(2)&"</td><td>"&rsBU(3)&"</td></tr>"
					    rsBU.moveNext
					    i = i + RowCount
					    Wend
					    rw ("<tfoot><tr><th colspan='3' style='text-align:right'>Total "&i&"</th></tr></tfoot>")
					    rw ("</tbody></table><div id='pager' class='pager'><img src='tablesorter/addons/pager/icons/first.png' alt='' class='first'/><img src='tablesorter/addons/pager/icons/prev.png' alt='' class='prev'/><input type='text' class='pagedisplay'/><img src='tablesorter/addons/pager/icons/next.png' alt='' class='next'/><img src='tablesorter/addons/pager/icons/last.png' alt='' class='last'/><select class='pagesize'><option selected='selected' value='10'>10</option><option value='20'>20</option><option value='30'>30</option><option value='40'>40</option></select></div>""}")
					End If
				End With
	    objConn.Close
    Set objConn = Nothing

 '{"error":true,"msg":"<table id='tablesorter-demo' class='tablesorter' border='0' cellpadding='0' cellspacing='1'><colgroup><col style='width: 211px'><col style='width: 211px'><col style='width: 121px'><col style='width: 153px'><col style='width: 194px'><col style='width: 175px'><col style='width: 201px'></colgroup><thead><tr><th>From</th><th>By</th><th>Referrals</th></tr></thead><tfoot><tr><th colspan='3'>Total x</th></tr></tfoot><tbody><tr><td>Wigan DAS</td><td>John Smith</td><td>xx</td></tr><tr><td>BASIC</td><td>June Jones</td><td>3</td></tr><tr><td>Student03</td><td>Languages</td><td>female</td></tr><tr><td>Student04</td><td>Languages</td><td>male</td></tr><tr><td>Student05</td><td>Languages</td><td>female</td></tr><tr><td>Student06</td><td>Mathematics</td><td>male</td></tr><tr><td>Student07</td><td>Mathematics</td><td>male</td></tr><tr><td>Student08</td><td>Languages</td><td>male</td></tr><tr><td>Student09</td><td>Mathematics</td><td>male</td></tr><tr><td>Student10</td><td>Languages</td><td>male</td></tr><tr><td>Student11</td><td>Languages</td><td>male</td></tr><tr><td>Student12</td><td>Mathematics</td><td>female</td></tr><tr><td>Student13</td><td>Languages</td><td>female</td></tr><tr><td>Student14</td><td>Languages</td><td>female</td></tr><tr><td>Student15</td><td>Languages</td><td>male</td></tr><tr><td>Student16</td><td>Languages</td><td>female</td></tr><tr><td>Student17</td><td>Languages</td><td>female</td></tr><tr><td>Student18</td><td>Mathematics</td><td>male</td></tr><tr><td>Student19</td><td>Languages</td><td>male</td></tr><tr><td>Student20</td><td>Mathematics</td><td>male</td></tr><tr><td>Student21</td><td>Languages</td><td>male</td></tr><tr><td>Student22</td><td>Mathematics</td><td>male</td></tr><tr><td>Student23</td><td>Languages</td><td>female</td></tr></tbody></table><div id='pager' class='pager'><img src='tablesorter/addons/pager/icons/first.png' alt='' class='first'/><img src='tablesorter/addons/pager/icons/prev.png' alt='' class='prev'/><input type='text' class='pagedisplay'/><img src='tablesorter/addons/pager/icons/next.png' alt='' class='next'/><img src='tablesorter/addons/pager/icons/last.png' alt='' class='last'/><select class='pagesize'><option selected='selected' value='10'>10</option><option value='20'>20</option><option value='30'>30</option><option value='40'>40</option></select></div>"}
%>