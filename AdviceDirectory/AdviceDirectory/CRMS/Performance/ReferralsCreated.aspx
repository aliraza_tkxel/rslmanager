<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReferralsCreated.aspx.vb" Inherits="ReferralsCreated" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Greater Manchester ANP Portal > Reports > Referrals > Referrals Created</title>
    <link rel="stylesheet" href="tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />

    <script type="text/javascript" language="javascript" src="raphael.js"></script>

    <script type="text/javascript" language="javascript" src="g.raphael.js"></script>

    <script type="text/javascript" language="javascript" src="g.bar.js"></script>

    <script type="text/javascript" language="javascript" src="jquery-1.4.2.min.js"></script>

    <script type="text/javascript" language="javascript" src="tablesorter/jquery.tablesorter.js"></script>

    <script type="text/javascript" language="javascript" src="tablesorter/addons/pager/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript" language="javascript" src="JSON2.js"></script>

    <script type="text/javascript"> 

        function drawChart(labels, data) {
            var r = Raphael("dv_graph", 600, 300);
            var chart = r.g.barchart(10, 10, 580, 280, [data], {stacked: true, type: "soft", legend: ["%%.%% � Enterprise Users", "IE Users"], legendpos: "west", id: [1,2,3,4,5,6,7], href: eval(OrgId)});

                chart.hover(function() {
                    this.flag = r.g.popup(this.bar.x, this.bar.y, (this.bar.value || "0") + "").insertBefore(this);
                }, function() {
                    this.flag.animate({opacity: 0}, 300, function () {this.remove();});
                });

                 chart.click(function () {
                 for (var i = 0; i < chart.bars[0].length; i++) {
                    var bar = chart.bars[0][i];
                        bar.attr("fill", "#2f69bf");
                        bar.attr("stroke", "#2f69bf");
                    }
                this.bar.attr("fill", "red");
                this.bar.attr("stroke", "red");
                });

                chart.click(function () {
                    this.bar.scale(1.1, 1.1, this.cx, this.cy);
                    this.bar.animate({scale: 1}, 500, "bounce");
                });

            r.g.txtattr = {font:"12px Fontin-Sans, Arial, sans-serif", fill:"#000", "font-weight": "bold"};
            chart.label(labels);
        }


    var OrgLbl = new Array();
    var OrgVal = new Array();
    var OrgId = new Array();

        window.onload = function() {
            drawChart(eval(OrgLbl),eval(OrgVal))
        }
 
    </script>

    <script type="text/javascript">

	function JsMe(rsId)
	{
	    $('[name=ColId]').val(rsId);
	    $('#waiting').show(500);
	    $('#demoForm').hide(0);
	    $('#message').hide(0);
	    $('#demoForm').show(500);
	    	
	    $.ajax({
			type : 'POST',
			url : 'ReferralDataGrid-Created.asp',
			dataType : 'json',
			data: {
				dtfrom  : $('#pnlOuter_pnlGrid_tbGrid_dpFrom').val(),
				dtto	: $('#pnlOuter_pnlGrid_tbGrid_dpTo').val(),
				rsId    : $('#ColId').val(),
				LAA     : $('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea').val()
			},
			success : function(data){
			    if(data !== null && typeof data == 'object')
			    {
			        $('#dv_tbl').html(data.msg);
			        $("table")
			            .tablesorter({sortList:[[2,1]], widgets: ['zebra']})
		                .tablesorterPager({container: $("#pager")});
		    	    $('#waiting').hide(500);
				    $('#message').removeClass().addClass((data.error === true) ? 'error' : 'success')
				    if (data.error === true)
					    $('#demoForm').show(500);
			    }
			    else
			    {
			        $('#waiting').hide(500);
			        $('#dv_tbl').html("No Results")}
                }
            });
	    }

    </script>

    <script type="text/javascript" language="javascript">
        function ExportXlsGo()
        {
            window.open('XlsExport.aspx?cache=RT','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
        function PrintGo()
        {
            window.open('PrintExport.aspx?cache=RT','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
    </script>

</head>
<body>
    <form id="demoForm" method="post" runat="server">
        <div>
            <ext:PageManager ID="pmPageManager" runat="server" EnableAjax="False" />
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="true" Title="Referrals Created" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                            <ext:Toolbar runat="server" ID="tbGrid">
                                <Items>
                                    <ext:Label ID="lblFrom" runat="server" Text="From:">
                                    </ext:Label>
                                    <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                        ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                    </ext:DatePicker>
                                    <ext:Label ID="lblTo" runat="server" Text="To:">
                                    </ext:Label>
                                    <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                        DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                        Label="To Date" runat="server">
                                    </ext:DatePicker>
                                    <ext:Label ID="Label1" Text="Local Authority Area: " runat="server">
                                    </ext:Label>
                                    <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                        DataTextField="LocalAuthorityAreaId" DataValueField="LocalAuthorityArea">
                                    </ext:DropDownList>
                                    <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                        Icon="reload">
                                    </ext:Button>
                                    <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Enabled="false" Text="To Xls" ToolTip="Export to Excel"
                                        runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                    </ext:Button>
                                    <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" Enabled="false" ToolTip="Print" Text="Print"
                                        runat="server" Icon="Printer">
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>
        <div style="float: left">
        <input type="hidden" name="ColId" id="ColId" value="" />
            <div id="dv_graph"></div>
        </div>
        <div id="message" style="display: none; float: left"></div>
        <div id="waiting" style="display: none; float: left">Please wait<br /><img src="images/ajax-loader.gif" title="Loader" alt="Loader" /></div>            
        <div id="dv_tbl" style="width:100%;"></div>           
    </form>
</body>
</html>
