<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Index2.0.1.aspx.vb" Inherits="CRMS_Referral_Index2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<link rel="stylesheet" href="tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />

    <script type="text/javascript" language="javascript" src="raphael.js"></script>

    <script type="text/javascript" language="javascript" src="g.raphael.js"></script>

    <script type="text/javascript" language="javascript" src="g.bar.js"></script>

    <script type="text/javascript" language="javascript" src="jquery-1.4.2.min.js"></script>

    <script type="text/javascript" language="javascript" src="tablesorter/jquery.tablesorter.js"></script>

    <script type="text/javascript" language="javascript" src="tablesorter/addons/pager/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript"> 

        function drawChart(labels, data) {
            var r = Raphael("dv_graph", 600, 300);
            
            var chart = r.g.barchart(10, 10, 580, 280, [data], {stacked: true, type: "soft", legend: ["%%.%% � Enterprise Users", "IE Users"], legendpos: "west", id: [1,2,3,4,5,6,7], href: ["JavaScript:JsMe(1)", "JavaScript:JsMe(2)", "JavaScript:JsMe(3)", "JavaScript:JsMe(4)", "JavaScript:JsMe(5)", "JavaScript:JsMe(6)", "JavaScript:JsMe(7)"]});
                
                chart.hover(function() {
                    this.flag = r.g.popup(this.bar.x, this.bar.y, (this.bar.value || "0") + "").insertBefore(this);
                }, function() {
                    this.flag.animate({opacity: 0}, 300, function () {this.remove();});
                });            
                 
                 chart.click(function () {
                 for (var i = 0; i < chart.bars[0].length; i++) {
                    var bar = chart.bars[0][i];
                        bar.attr("fill", "#2f69bf");
                        bar.attr("stroke", "#2f69bf");
                    }
                this.bar.attr("fill", "red");
                this.bar.attr("stroke", "red");
                });

                chart.click(function () {
                    this.bar.scale(1.1, 1.1, this.cx, this.cy);
                    this.bar.animate({scale: 1}, 500, "bounce");
                });
                
            r.g.txtattr = {font:"12px Fontin-Sans, Arial, sans-serif", fill:"#000", "font-weight": "bold"};
            chart.label(labels);
        }
        
        window.onload = function() {        
            drawChart(['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],[1,10,15,2,7,10,21])
        }  
 
    </script>

    <script type="text/javascript">	
	
	//alert($('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea option:selected').val(selected.data[1]));

    //alert($('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea :selected').text())

	//var text = $('#selectId option:selected').val();
	//alert(text)
	//var text = $('#selectId:selected').val();
	//alert(text)
	
	//if( $("select#selectId option :selected").val()!="" || $("select#selectId option).has(":selected")) {
    //    alert("hi")
    //});
    
	function JsMe(rsId)
	{
	    $('[name=ColId]').val(rsId);
	    //alert(document.getElementById("pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea").value);
	    
	    //var me = $('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea option:selected').text();
        //alert(me)
        //alert($('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea').val("0"));
        //alert($('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea').val("1"));
        
	    $('#waiting').show(500);
	    $('#demoForm').hide(0);
	    $('#message').hide(0);
	    $('#demoForm').show(500);
	    	
	    $.ajax({
			type : 'POST',
			url : 'script3.asp',
			dataType : 'json',
			data: {
				dtfrom  : $('#pnlOuter_pnlGrid_tbGrid_dpFrom').val(),
				dtto	: $('#pnlOuter_pnlGrid_tbGrid_dpTo').val(),
				rsId    : $('#ColId').val(),
				//LAA     : $('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea option:selected').val()
				LAA     : $('#pnlOuter_pnlGrid_tbGrid_selLocalAuthorityArea').val()
			},
			success : function(data){
			
			if(data !== null && typeof data == 'object')
			{
			    $('#dv_tbl').html(data.msg);
			    $("table")
			        .tablesorter({sortList:[[2,1]], widgets: ['zebra']})
		            .tablesorterPager({container: $("#pager")});
    		        
		    	    $('#waiting').hide(500);
				    $('#message').removeClass().addClass((data.error === true) ? 'error' : 'success')
				    if (data.error === true)
					    $('#demoForm').show(500);
			}
			else {$('#dv_tbl').html("No Results")}
            }
            });        
	}
	
    </script>
    <script type="text/javascript" language="javascript">
        function ExportXlsGo()
        {
            window.open('XlsExport.aspx?cache=RT','_blank','height=10,width=10,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
        function PrintGo()
        {
            window.open('PrintExport.aspx?cache=RT','_blank','height=400,width=600,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );
        }
    </script>

</head>
<body>
<form action="" id="demoForm" method="post" runat="server">
<select id='selectId'>
<option value='value1'>Name 1</option>
<option value='value2'>Name 2</option>
<option value='value3'>Name 3</option>
</select>
<div>
            <ext:PageManager ID="pmPageManager" runat="server" EnableAjax="false" />
            <ext:Panel ID="pnlOuter" runat="server" EnableBackgroundColor="true" ShowBorder="false"
                ShowHeader="true" Title="Referrals Recieved" Icon="table">
                <Items>
                    <ext:Panel ID="pnlGrid" ShowBorder="True" ShowHeader="false" runat="server">
                        <Items>
                                 
                                    <ext:Toolbar runat="server" ID="tbGrid">
                                        <Items>
                                            
                                            <ext:Label ID="lblFrom" runat="server" Text="From:">
                                            </ext:Label>
                                            <ext:DatePicker runat="server" Required="false" Label="From Date" EmptyText="Date From"
                                                ID="dpFrom" DateFormatString="dd/MM/yyyy">
                                            </ext:DatePicker>
                                            
                                            <ext:Label ID="lblTo" runat="server" Text="To:">
                                            </ext:Label>
                                            <ext:DatePicker ID="dpTo" Required="false" EmptyText="Date To" CompareControl="dpFrom"
                                                DateFormatString="dd/MM/yyyy" CompareOperator="GreaterThanEqual" CompareMessage="From date should be less than or equal to date."
                                                Label="To Date" runat="server">
                                            </ext:DatePicker>
                                            
                                            <ext:Label ID="Label1" Text="Local Authority Area: " runat="server">
                                            </ext:Label>
                                            <ext:DropDownList runat="server" Label="selLocalAuthorityArea" ID="selLocalAuthorityArea"
                                                DataTextField="LocalAuthorityAreaId" DataValueField="LocalAuthorityArea">
                                            </ext:DropDownList>
                                            
                                            <ext:Button ID="btnRefrsh" OnClick="btnRefresh_OnClick" Text="Refresh" runat="server"
                                                Icon="reload">
                                            </ext:Button>
                                           
                                           
                                            <ext:Button ID="btnExportXls" OnClientClick="ExportXlsGo();" Text="To Xls" ToolTip="Export to Excel"
                                                runat="server" DisableControlBeforePostBack="false" Icon="pageexcel">
                                            </ext:Button>
                                           
                                            <ext:Button ID="btnPrintGrid" OnClientClick="PrintGo();" ToolTip="Print" Text="Print"
                                                runat="server" Icon="Printer">
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                
     
                           
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
        </div>

        <div id="wrapper" style="clear:both">
            <div id="message" style="display: none;">
            </div>
            <div id="waiting" style="display: none;">
                Please wait<br />
                <img src="images/ajax-loader.gif" title="Loader" alt="Loader" />
            </div>
            <fieldset>                
                <p>
                    <label for="dtfrom">
                        from:</label>
                    <input type="text" name="dtfrom" id="dtfrom" value="" />
                </p>
                <p>
                    <label for="dtto">
                        to:</label>
                    <input type="text" name="dtto" id="dtto" value="" />
                </p>
                 <p>
                    <label for="ColId">
                        ColId:</label>
                    <input type="text" name="ColId" id="ColId" value="" />
                    <input type="text" name="LAA" id="LAA" value="" />
                </p>
                <p>
                    <input type="submit" name="submit" id="submit" value="Submit" />
                </p>
            </fieldset>
        </div>
    <div style="float: left">
        <div id="dv_graph">
        </div>
    </div>
    <div style="float: left">
        <div id="dv_tbl">
        </div>
    </div>   

        <img src="" alt="" id="images" style="visibility: hidden" />
        <div id="content" style="visibility: hidden"><a href="#">xx</a></div>
        <input type="submit" id="generate" value="Generate!" style="visibility: hidden" />
    </form>
</body>
</html>
