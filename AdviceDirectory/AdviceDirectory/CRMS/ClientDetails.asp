﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")
%>
<%
Dim CUSTOMER_REFERENCE
    CUSTOMER_REFERENCE = nulltest(Request("CUSTOMERID"))
    
If (NullNestTF(CUSTOMER_REFERENCE) = True) Then
    Response.Redirect ("/crms/login.asp")
Else
    If NOT IsNumeric(CUSTOMER_REFERENCE) Then
        Response.Redirect ("/crms/NoRecord.asp?Er=1")
    End If
End If
%>
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>

<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
Dim FIRSTNAME
Dim SURNAME
Dim FULLNAME
Dim GENDER
Dim ETHNICITY
Dim DOB
Dim DISABILITY
Dim BENEFITRECIPIENT
Dim LONEPARENT
Dim EMPLOYMENTSTATUS
Dim LENGTHOFUNEMPLOYMENT
Dim NINUMBER
Dim ADDRESSLINE1
Dim ADDRESSLINE2
Dim ADDRESSLINE3
Dim LAA
Dim POSTCODE
Dim TELEPHONE
Dim EMAIL
Dim MOBILE

Dim SECURITYQUESTION
Dim SECURITYANSWER
Dim USERNAME
Dim PASSWORD

Dim BENEFIT_LIST


Function LoadRecord()
Call OpenDB()

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_CLIENTDEATAILS"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
                'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsDetails = .Execute
					If NOT rsDetails.EOF Then
                        FIRSTNAME = rsDetails("FIRSTNAME")
                        SURNAME = rsDetails("SURNAME")
                        FULLNAME = FIRSTNAME + " " + SURNAME
                        DOB = rsDetails("DOBFORMATTED")
                        HOUSENUMBER = rsDetails("HOUSENUMBER")
                        ADDRESSLINE1 = rsDetails("ADDRESSLINE1")
                        ADDRESSLINE2 = rsDetails("ADDRESSLINE2")
                        ADDRESSLINE3 = rsDetails("ADDRESSLINE3")
                        LAA = rsDetails("LAA")
                        POSTCODE = rsDetails("POSTCODE")
                        EMAIL = rsDetails("EMAIL")
                        TELEPHONE = rsDetails("TELEPHONE")
                        MOBILE = rsDetails("MOBILE")
                        GENDER = rsDetails("GENDER")
                        DISABILITY = rsDetails("DISABILITY")
                        ETHNICITY = rsDetails("ETHNICITY")
                        BENEFITRECIPIENT = rsDetails("BENEFITRECIPIENT")
                        LONEPARENT = rsDetails("LONEPARENT")
                        EMPLOYMENTSTATUS = rsDetails("EMPLOYMENTSTATUS")
                        LENGTHOFUNEMPLOYMENT = rsDetails("LENGTHOFUNEMPLOYMENT")
                        NINUMBER = rsDetails("NINUMBER")
	                    USERNAME = rsDetails("USERNAME")
	                    PASSWORD = rsDetails("PASSWORD")
	                    SECURITYQUESTION = rsDetails("SECURITYQUESTION")
                        SECURITYANSWER = rsDetails("SECURITYANSWER")
                End If
            End With
	    objConn.Close
    Set objConn = Nothing
    
    Set objConn	= Server.CreateObject("ADODB.Connection")
	Set objCmd	= Server.CreateObject("ADODB.Command")
		objConn.Open DSN_CONNECTION_STRING
        With objCmd
            .ActiveConnection = objConn 'You can also just specify a connection string here
            .CommandText = "PU_CLIENT_BENEFIT_DETAILS_SELECT"
            .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
            .CommandTimeout = 0
            'Add Input Parameters
            .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
            .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMER_REFERENCE)
            'Execute the function
		    '.execute ,,adexecutenorecords
		    Set rs = .execute
	    End With
	 
        BENEFIT_LIST = "<table style='border:solid 1px #cc0000'>"
        If NOT rs.EOF Then
            BENEFIT_LIST = BENEFIT_LIST & "<tr><td style='border-bottom:solid 1px #cc0000'>Benefits Listed<td></tr>"
		    Do until rs.eof
                BENEFIT_LIST = BENEFIT_LIST & "<tr><td>" & rs("DESCRIPTION") & "</td></tr>"
		        rs.MOVENEXT
		    Loop
		Else
		    BENEFIT_LIST = BENEFIT_LIST & "<tr><td>-- No Benefits Listed --</td></tr>"
	    End If
	    BENEFIT_LIST = BENEFIT_LIST & "</table>"
	    
        objConn.Close
    Set objConn = Nothing
    
    
    
Call CloseDB()
End Function


Call LoadRecord()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function GoTo(strURL)
{
    window.location.href = strURL;
}
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		    <div style="float:left; width:100%; white-space:nowrap;">
		        <div style="float:left">
		            <h1><%=FULLNAME%></h1>
		        </div>
		        <div style="float:left">
		            <ul class="navmenu2">
		                <li><a href="JavaScript:GoTo('ClientJourney.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Journey">Journey</a></li>
		                <li><a href="JavaScript:GoTo('CurrentPriorities.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Priorities">Current Priorities</a></li>
		                <li><a href="JavaScript:GoTo('CurrentContacts.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Contacts">Current Contacts</a></li>
		                <li><a href="JavaScript:GoTo('SelfAssessment.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Current Contacts">Self Assessment</a></li>
		                <li><a href="JavaScript:GoTo('Appointments.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Appointments">Appointments</a></li>
		                <li><a href="JavaScript:GoTo('ClientDetails.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>')" title="Details">Details</a></li>
		            </ul>
		        </div>
		    </div>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
                    <form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
                        <fieldset>
	                        <legend>Login to this site</legend>
	                        <div id="Frm" style="float:left; width:100%;">
	                            <div class="row">
	                                <div style="float:left; width:100%; background-color:#cc0000; color:white; padding-bottom:1em; padding-top:1em; margin-bottom:1em;">
	                                    <div style="float:left; margin-left:1em">
	                                        Personal Details
	                                    </div>
	                                    <div style="float:right; margin-right:1em">
	                                        <input class="submit" type="button" accesskey="E" name="Edit" title="Button : Edit Personal Details" value="Edit" onclick="Javascript:window.location.href='ClientPersonalDetails.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>'" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
                                    <span class="label">
                                        Firstname:
                                    </span>
                                    <span class="formw">
                                        <%=FIRSTNAME%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        Surname:
                                    </span>
                                    <span class="formw">
                                        <%=SURNAME%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        DOB:
                                    </span>
                                    <span class="formw">
                                        <%=DOB%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        Address Line 1:
                                    </span>
                                    <span class="formw">
                                        <%=ADDRESSLINE1%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        Address Line 2:
                                    </span>
                                    <span class="formw">
                                        <%=ADDRESSLINE2%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        Address Line 3:
                                    </span>
                                    <span class="formw">
                                        <%=ADDRESSLINE3%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        Local Authority:
                                    </span>
                                    <span class="formw">
                                         <%=LAA%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        Postcode:
                                    </span>
                                    <span class="formw">
                                        <%=POSTCODE%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        Email:
                                     </span>
                                    <span class="formw">
                                        <%=EMAIL%>
                                    </span>
                               </div>
	                            <div class="row">
	                                <span class="label">
	                                    Telephone:
	                                </span>
	                                <span class="formw">
		                                <%=TELEPHONE%>
	                                </span>
	                            </div>
	                            <div class="row">
	                                <span class="label">
	                                    Mobile:
	                                </span>
	                                <span class="formw">
		                                <%=MOBILE%>
	                                </span>
	                            </div>
	                            <div class="row">
                                    <span class="label">
                                        National Insurance Number:                                       
                                    </span>
                                    <span class="formw">
                                        <%=NINUMBER%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        Gender:
                                    </span>
                                    <span class="formw">
                                        <%=GENDER%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">   
                                        Disability:
                                    </span>
                                    <span class="formw">
                                        <%=DISABILITY%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        Ethnicity:
                                     </span>
                                    <span class="formw">
                                        <%=ETHNICITY%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        Benefit Recipient:
                                    </span>
                                    <span class="formw">
                                        <%=BENEFITRECIPIENT%>
                                        <br />
                                        <%=BENEFIT_LIST%>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label">
                                        Lone Parent:
                                    </span>
                                    <span class="formw">
                                        <%=LONEPARENT%>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">
                                        Employment Status:
                                     </span>
                                    <span class="formw">
                                        <%=EMPLOYMENTSTATUS%>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label">
                                        Length Of Unemployment:
                                    </span>
                                    <span class="formw">
                                        <%=LENGTHOFUNEMPLOYMENT%>
                                    </span>
                                </div>
                               <!--
                                <div class="row">
                                    <span class="label">&nbsp;</span>
                                    <span class="formw" style="float:right">
                                        <input class="submit" type="button" name="Edit" id="BtnEditPD" value=" Edit " title="Submit Button : Edit Personal Details" style="float:right" onclick="Javascript:window.location.href='MyPersonalDetails.asp'" />
                                    </span>
                                </div>
                                //-->
                                <div class="row" style="padding-top:1em;">
                                    <div style="float:left; width:100%; background-color:#cc0000; color:white; padding-bottom:1em; padding-top:1em; margin-bottom:1em;">
	                                    <div style="float:left; margin-left:1em">
	                                        Login Details
	                                    </div>
	                                    <div style="float:right; margin-right:1em">
	                                        <input class="submit" type="button" accesskey="E" name="Edit" title="Button : Edit Login Details" value="Edit" onclick="Javascript:window.location.href='ClientLoginDetails.asp?CUSTOMERID=<%=CUSTOMER_REFERENCE%>'" />
	                                    </div>
	                                </div>
	                            </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        Security Question:
                                    </span>
                                    <span class="formw">
                                        <%=SECURITYQUESTION%>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        Security Answer:
                                    </span>
                                    <span class="formw">
                                        <%=SECURITYANSWER%>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        Username:
                                    </span>
                                    <span class="formw">
                                        <%=USERNAME%>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        Password:
                                    </span>
                                    <span class="formw">
                                        <%=PASSWORD%>
                                    </span>
                               </div>
                               <!--                              
                               <div class="row">
                                    <span class="label">&nbsp;</span>
                                    <span class="formw" style="float:right">
                                        <input class="submit" type="button" name="Edit" id="BtnEditLD" value=" Edit " title="Submit Button : Edit Login Details" style="float:right" onclick="Javascript:window.location.href='MyLoginDetails.asp" />
                                    </span>
                               </div>
                               //-->
                            </div>
                        </fieldset>
                    </form>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>