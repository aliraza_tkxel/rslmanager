<%
LOGINID = Session("svLOGINID")

Call OpenDB()

Set cmd=server.CreateObject("ADODB.Command")
With cmd
  .CommandType=adcmdstoredproc
  .CommandText = "STP_ADVISER_PHOTO_SELECT"
  set .ActiveConnection=conn
  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
  .parameters.append param
  set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
  .parameters.append param
  set param = .createparameter("@HASPHOTO", adInteger, adParamInputOutput, 0,1)
  .parameters.append param
  .execute ,,adexecutenorecords
  hasphoto = .Parameters("@HASPHOTO").Value 
end with

Call CloseDB()

	If CInt(hasphoto) = 1 Then
		strphoto = "/CRMS/includes/RetrieveImg.asp?LOGINID=" & LOGINID 
	Else
		strPhoto = "/images/spacer.gif"
	End If
%>
<img src="<%=strphoto%>" alt="" />