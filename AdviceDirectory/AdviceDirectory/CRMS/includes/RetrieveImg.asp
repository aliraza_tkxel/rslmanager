<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Call Debug()
Dim PHOTOGRAPH

    LOGINID = Request("LOGINID")
    If LOGINID = "" Or IsNull(LOGINID) Then LOGINID = -1 End If

  	SQL = "SELECT PHOTO " &_
  	        "FROM TBL_MD_LOGIN " &_
	        "WHERE LOGINID = " & LOGINID
	Call OpenDB()
	Call OpenRs(rsClient, SQL)
		If NOT (rsClient.EOF) Then
			PHOTOGRAPH = rsClient("PHOTO")
		End If
	Call CloseRS(rsClient)
	Call CloseDB()
%>
<%
If IsNull(PHOTOGRAPH) Then
    rw "spacer.gif"
Else
    Response.ContentType = "image/gif"
    Response.Clear
    Response.BinaryWrite(PHOTOGRAPH)
    Response.End()
End If
%>