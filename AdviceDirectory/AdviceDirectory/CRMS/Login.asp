﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")
'rw "SCRIPT : " & GP_curPath
'rw "<br/>"
	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
'rw "Pattern 1 : " & GP_curPath
		regEx.Pattern = "\/crms|\!|\.|\?|\;|\,|\:|/"
'rw "<br/>"
  			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))
'rw "Pattern 2 : " & GP_curPath
	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
	'rw "<br/>"
	'rw "Include File : " & strIncludeFile
%>
<% 
If Request.Form("Login") <> "" Then

    Dim txt_USERNAME	' FORM FIELD - USERNAME
    Dim strPASSWORD 	' FORM FIELD - PASSWORD
    Dim rsLogin			' RECORDSET
    Dim passcheck 		' USER OK/NOT OK TO LOGIN
    Dim passcheck_text

	    txt_USERNAME = "xyz"
	    txt_password = "xyz"
	    passcheck = ""

        ' VALIDATE USERNAME
	    If (Request.Form("txt_USERNAME") <> "") Then
		    If isUsername(Request.Form("txt_USERNAME")) = True Then
			    strUSERNAME = Request.Form("txt_USERNAME")
		    Else
			    strUSERNAME = isUsername(Request.Form("txt_USERNAME"))
		    End If
	    End If
        ' VALIDATE PASSWORD
	    If (Request.Form("txt_PASSWORD") <> "") Then
		    strPASSWORD = RegExpTrimAll(Request.Form("txt_PASSWORD"))
	    End If

        ' LOGIN USING SPECIFIED DETAILS
	    Call LoginMD(strUSERNAME, strPASSWORD)
	    
	    If txt_USERNAME = "xyz" Then txt_USERNAME = Null End If

        ' SET ERROR FLAG - CSS STYLE SHEET USED
	    If passcheck = 0 Then
            ErrorClass = " class=""er"""
        Else
            ErrorClass = " class=""no_error"""
        End If

End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">			
			    <div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">				
                    <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
                    <form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
                      <fieldset>
	                        <legend>Login to the CRMS</legend>
                                <div id="Frm" style="float:left; width:48%;">
                                    <p class="H4">
                                        Already got a Username and Password? Login now:
                                    </p>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_USERNAME" accesskey="U">
                                                <span style="text-decoration:underline; cursor:pointer">U</span>sername:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input  class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    type="text" 
                                                    size="40" 
                                                    name="txt_USERNAME" 
                                                    id="txt_USERNAME" 
                                                    maxlength="100" 
                                                    value="<%=txt_USERNAME%>" 
                                                    tabindex="1" 
                                                    required="Y"
                                                    validate="/[a-zA-Z0-9_\@\.\-]{6,70}$/"
                                                    validateMsg="Username :- Must be 6 - 70 characters in length. Letters and Numbers are permitted. Spaces and most special characters are not permitted." />
                                                    <!-- 
                                                    validate="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/"
                                                    //-->
                                            <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" />
                                        </span>
                                   </div>
                                   <div class="row">
                                        <span class="label">
                                            <label for="txt_PASSWORD" accesskey="P">
                                                <span style="text-decoration:underline; cursor:pointer">P</span>assword:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input  class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    type="password" 
                                                    size="40" 
                                                    name="txt_PASSWORD" 
                                                    id="txt_PASSWORD" 
                                                    maxlength="10" 
                                                    tabindex="2" 
                                                    value="" 
                                                    required="Y" 
                                                    validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                                    validateMsg="Password - Password must be between 6 and 10 Characters and contain at least 1 number." />
                                            <img src="/js/FVS.gif" name="img_password" id="img_password" width="15px" height="15px" alt="" />
                                        </span>
                                   </div>
                                   <div class="row">
                                        <span class="label">&nbsp;</span>
                                        <span class="formw">
                                            <input  class="submit" 
                                                    type="submit" 
                                                    name="Login" 
                                                    id="Login" 
                                                    value=" Login " 
                                                    title="Submit Button : Login" 
                                                    tabindex="3" 
                                                    style="width:auto"/>
                                        </span>
                                   </div>
                                </div> 
	                    </fieldset>
                    </form>
                     <a href="ForgotPassword.asp" title="Forgotten your password?">Forgotten your password?</a>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>