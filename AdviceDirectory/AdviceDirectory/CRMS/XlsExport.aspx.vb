Imports System.Data
Imports system.io

Partial Class Customer_XlsExport
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ' MyBase.VerifyRenderingInServerForm(control)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

	'If user isn't authenticated, he/she will be redirected to Login
        If IsNothing(ASPSession("svLOGINID")) Then
            Response.Redirect("http://www.greatermanchester-anp.org.uk/CRMS")
        End If

        Dim sQs As String = Request.QueryString("cache")
        If Not String.IsNullOrEmpty(sQs) Then
            Dim dt As New DataTable
            dt = Cache(sQs)
            gvExcelExport.DataSource = dt
            gvExcelExport.DataBind()
        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Response.Clear()
        Response.ClearContent()
        Response.Charset = ""
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=XlsExport.xls")
        'Response.ContentType = "application/vnd.xls"
	Response.ContentType = "application/msexcel.xls"
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        gvExcelExport.RenderControl(htw)
        Response.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub

End Class
