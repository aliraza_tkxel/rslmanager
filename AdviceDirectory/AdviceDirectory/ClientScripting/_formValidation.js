var isValidationErrors = false
var isFieldRequirements = false
var FormListStatus = new Array(50)
var FormCurrentItem 

var errTxt = "";
var errorTColour = "#FFFFFF";
var TColour = "#000000";

function checkForm(div,single_bool){

	var errTxt = "";
	isValidationErrors = false
	isFieldRequirements = false

	// This part checks for the single validation call, and sets the
	// Counters appropriately.
	if (single_bool)
	{
		eventCaller =(window.event.target) ? window.event.target : window.event.srcElement;
		var theI = ""
		for (i=0; i<FormFields.length; i++)
		{
			tpStr = FormFields[i];
			tpStr = tpStr.split("|");

			alert(tpStr[0])
			alert(eventCaller)

			if (tpStr[0] == eventCaller) 
				theI = i
			}
		    if (theI == "")
		    {
			    alert("Field does not appear in validation list."+theI);
			    return false;
		    }
		var iStart = theI;
		var iEnd = theI+1;
		}
	else 
	    {	
		    var iStart = 0;
		    var iEnd = FormFields.length;		
		}	

	for (i=iStart; i<iEnd; i++)
	{
		tpStr = FormFields[i];
		FormCurrentItem = i
		tpStr = tpStr.split("|");
		FormListStatus[i] = -1
		if (tpStr[3] != "I_Y" && tpStr[3] != "I_N")
		{
			if (tpStr[2] == "RADIO")
			{
				blankStatus = true
				for (m=0; m <document.getElementsByName(tpStr[0]).length; m++)
				{
					if (document.getElementsByName(tpStr[0])[m].checked == true) 
						blankStatus = false
				}
			}
			else 
			{
				itemValue = document.getElementById(tpStr[0]).value;
				blankStatus = isBlank(itemValue);
			}
			if (tpStr[3] == "Y") {
				FormListStatus[i] = 0
				if (blankStatus) {
				
				
				if (tpStr[2] != "RADIO")
			        document.getElementById(tpStr[0]).style.color = TColour;
		        if (tpStr[3] == "Y" && blankStatus) {
			        if (tpStr[2] == "SELECT" || tpStr[2] == "TEXT")
				        errTxt += "  " + tpStr[1] + "\<br/>";
			        else if(tpStr[2] == "EMAIL" || tpStr[2] == "INTEGER")
				        errTxt += "  " + tpStr[1] + " - must be an " + tpStr[2].toLowerCase() + "\<br/>";
			        else if(tpStr[2] == "TELEPHONE")
				        errTxt += "  " + tpStr[1] + " - must be a number\<br/>";
			        else if(tpStr[2] == "NATIONAL INSURANCE")
				        errTxt += "  " + tpStr[1] + " - must be a " + tpStr[2].toLowerCase() + " number\<br/>";
			        else if(tpStr[2] == "DATE")
				        errTxt += "  " + tpStr[1] + " - must be a date\<br/>";
			        else							
				        errTxt += "  " + tpStr[1] + " - must be a " + tpStr[2].toLowerCase() + "\<br/>";
			        if (tpStr[2] != "RADIO")
				        document.getElementById(tpStr[0]).style.color = "red";
			        }
					
					switch (tpStr[2]) {
						case "EMAIL"    : RequiredError(tpStr[0], tpStr[1], "This field is required and must be a valid email address."); break;							
						case "CURRENCY" : RequiredError(tpStr[0], tpStr[1], "This field is required and must be a valid monetary value."); break;							
						case "INTEGER"  : RequiredError(tpStr[0], tpStr[1], "This field is required and must be a valid integer number."); break;							
						case "FLOAT"  	: RequiredError(tpStr[0], tpStr[1], "This field is required and must be a valid integer/decimal number."); break;							
						case "TELEPHONE": RequiredError(tpStr[0], tpStr[1], "This field is required and can only contain numbers and spaces."); break;							
						case "DATE"     : RequiredError(tpStr[0], tpStr[1], "This field is required and must be a date."); break;							
						case "RADIO"    : RequiredError(tpStr[0], tpStr[1], "Please select at least one radio button from the selection."); break;							
						case "LONGNUMBER":RequiredError(tpStr[0], tpStr[1], "This field is required and must be an " + tpStr[4] + " digit number."); break;						
						default         : RequiredError(tpStr[0], tpStr[1], "This field is required."); break; //Includes SELECT and TEXT options.
						}
					}
				}
			}
		}
	
	for (i=iStart; i<iEnd; i++){
		tpStr = FormFields[i];
		FormCurrentItem = i		
		tpStr = tpStr.split("|");
		if (tpStr[3] != "I_Y" && tpStr[3] != "I_N") {
		    if (tpStr[2] == "RADIO")
		    {
		        itemValue = document.getElementsByName(tpStr[0]).value;
		    }
		    else
		    {
		        itemValue = document.getElementById(tpStr[0]).value;
		    }
			
			blankStatus = isBlank(itemValue);			
			if (!blankStatus) {
				if (FormListStatus[i] == -1) FormListStatus[i] = 0		
				switch (tpStr[2]) {
					case "CURRENCY" : if (isNumeric(tpStr[0], tpStr[1])) document.getElementById(tpStr[0]).value = FormatCurrency(itemValue, 2); break;
					case "INTEGER"  : isInteger(tpStr[0], tpStr[1]); break;
					case "FLOAT"    : if (isNumeric(tpStr[0], tpStr[1])) document.getElementById(tpStr[0]).value = FormatCurrency(itemValue, tpStr[4]); break;					
					case "POSTCODE" : isUKPostCode(tpStr[0], tpStr[1]); break;
					case "EMAIL"    : checkEmail(tpStr[0], tpStr[1]); break;
					case "TELEPHONE": isTelephone(tpStr[0], tpStr[1]); break;
					case "TEXT"     : filterText(tpStr[0], tpStr[1]); break;
					case "DATE"     : isDate(tpStr[0], tpStr[1]); break;
					case "LONGNUMBER":isLongNumber(tpStr[0], tpStr[1], tpStr[4]); break;
					}
				}
			}
		}
	
	if (!single_bool) {
		FieldsCorrect()
		if (isValidationErrors == true || isFieldRequirements == true) return false; //If failed to validate all non empty fields return false
		}
	else {
		if (isValidationErrors == true || isFieldRequirements == true) return false
		ManualError("img_" + tpStr[0].substring(4, tpStr[0].length), "", 3)
		}
	return true;
	}

function targetError(iItem, iColor){
	document.getElementById(iItem).style.color = iColor
	document.getElementById(iItem).focus();
	}

function AutoFiltered(iItem, ErrorMessage){
	isValidationErrors = true
	FormListStatus[FormCurrentItem] = 3 //Automatically Filtered
	ImageID = "img_" + iItem.substring(4, iItem.length)
	document.images[ImageID].src = document.images["FVEB_Image"].src
	document.getElementById(ImageID).title = ErrorMessage	
	document.getElementById(ImageID).style.cursor = "hand"
	}

function RequiredError(iItem, nametext, ErrorMessage){
	isFieldRequirements = true
	FormListStatus[FormCurrentItem] = 1 //RequiredFailure
	ImageID = "img_" + iItem.substring(4, iItem.length)
	document.images[ImageID].src = document.images["FVER_Image"].src
	document.getElementById(ImageID).title = ErrorMessage
	document.getElementById(ImageID).style.paddingLeft = "5px"
	document.getElementById(ImageID).style.cursor = "hand"
	}

function validationError(iItem, ErrorMessage){
	isValidationErrors = true
	if (FormListStatus[FormCurrentItem] == 0 || FormListStatus[FormCurrentItem] == -1) {
		FormListStatus[FormCurrentItem] = 2 //ValidationFailure
		ImageID = "img_" + iItem.substring(4, iItem.length)
		document.images[ImageID].src = document.images["FVW_Image"].src
		document.getElementById(ImageID).title = ErrorMessage
		document.getElementById(ImageID).style.paddingLeft = "5px"
		document.getElementById(ImageID).style.cursor = "hand"
		}
	}

function ManualError(targetImage, ErrorMessage, ImageCode){
	ImageArray = new Array("FVW_Image", "FVER_Image", "FVEB_Image", "FVS_Image")
	document.images[targetImage].src = document.images[ImageArray[ImageCode]].src
	document.getElementById(targetImage).title = ErrorMessage
	document.getElementById(ImageID).style.paddingLeft = "5px"
	document.getElementById(targetImage).style.cursor = "hand"
	}
		
function FieldsCorrect(){
	for (i=0; i<FormFields.length; i++){
		tpStr = FormFields[i];
		tpStr = tpStr.split("|");
		if (FormListStatus[i] == 0){
			iItem = tpStr[0]
			ImageID = "img_" + iItem.substring(4, iItem.length)
			document.images[ImageID].src = document.images["FVS_Image"].src
			document.getElementById(ImageID).title = ""	
			document.getElementById(ImageID).style.cursor = "default"
			}
		else if (FormListStatus[i] == -1){
			iItem = tpStr[0]
			ImageID = "img_" + iItem.substring(4, iItem.length)
			document.images[ImageID].src = document.images["FVS_Image"].src
			document.getElementById(ImageID).title = ""	
			document.getElementById(ImageID).style.cursor = "default"
			}
		}
	}
	
function checkEmail(itemName, errName) {
	elVal = document.getElementById(itemName).value;
	if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,7}|[0-9]{1,3})(\]?)$/.test(elVal))
		return true;
	else {
		validationError(itemName, "You have entered an invalid email for '" + errName + "', please re-enter.\nCorrect formats: [support@reidmark.com][elephant@inet.org.uk]");
		return false;
		}
	}

function isBlank(val){
	if(val==null){return true;}
	for(var i=0;i<val.length;i++) {
		if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r")){return false;}
		}
	return true;
	}

function isDigit(num) {
	if (num.length>1){return false;}
	var string="1234567890";
	if (string.indexOf(num)!=-1){return true;}
	return false;
	}

function isUKPostCode (itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = TrimAll(elVal.toUpperCase());
	result = elVal.match(/^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/g);
	if (!result) {
		validationError(itemName,"You must input a valid postcode for '" + errName + "',\nCorrect formats: [SW12 2EG][L4 5PQ][L17 2EA].");
		return false;	
		}
	else {
		elVal = String(elVal.replace(/\s/g,""));
		elValLen = elVal.length;
		//elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		document.getElementById(itemName).value = elVal;
		return true;
		}
	}

function isTelephone(itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = elVal.replace(/\s/g, "");	
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			validationError(itemName,"You must input a valid Telephone Number for '" + errName + "'.");
			return false;
			}
		}
	document.getElementById(itemName).value = TrimAll(document.getElementById(itemName).value);		
	return true;
	}

function filterText(itemName, errName) { 
	elVal = document.getElementById(itemName).value;
	if (elVal.match(/\<|\>|\"|\%|\;|\&/)){
		elVal = elVal.replace(/\<|\>|\"|\%|\;|\&/g,""); 
		document.getElementById(itemName).value = elVal;
		AutoFiltered(itemName, "Warning: This field was automatically filtered. The following characters will have been removed: < > \" \% ; &.")
		return false;
		}
	else
		return true;
	} 
	
function isInteger(itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = TrimAll(elVal);	
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			validationError(itemName,"You must input a positive valid number for '" + errName + "'.");
			return false;
			}
		}
	document.getElementById(itemName).value = elVal;		
	return true;
	}
	
function isLongNumber (itemName, errName, numDigits){
    var match = new RegExp('^([0-9]{' + numDigits + '})$');
    
	elVal = document.getElementById(itemName).value;
	
	result = elVal.match(match);
	if (!result) {
		validationError(itemName,"You must input " + numDigits + " digits for '" + errName + "'.");
		return false;	
		}
	else {				
		return true;
		}
	}

function LTrimAll(str) {
	if (str==null){return str;}
	for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	return str.substring(i,str.length);
	}
	
function RTrimAll(str) {
	if (str==null){return str;}
	for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	return str.substring(0,i+1);
	}
	
function TrimAll(str) {
	return LTrimAll(RTrimAll(str));
	}

function isNumeric(itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = TrimAll(elVal);
	if ( parseFloat(elVal,10) == ( elVal*1) ){
		document.getElementById(itemName).value = elVal;
		return true;
		}
	else {
		validationError(itemName,"You must input a valid integer/decimal number for '" + errName + "'.");
		return false;
		}
	}

//this function will only be accurate to about 10 decimal places.
function FormatCurrency ( fPrice, decimals) { 
   fPrice = Math.round(fPrice*Math.pow(10,10))/Math.pow(10,10);
   if (decimals) {
   	  if (!isNaN(decimals)) indexCount = parseInt(decimals) + 1
	  else indexCount = 3
	  }
   else indexCount = 3 //default to two decimal places
   plusString = "0."
   for (i=1; i<11; i++){
   	  if (indexCount == i) plusString += "5"
	  else plusString += "0"
	  }
   Negate = false
   if (parseFloat(fPrice) < 0){
		fPrice = 0 - fPrice
		Negate = true
		}
   var sCurrency = "" + ( parseFloat(fPrice) + parseFloat(plusString) ); 
   var nPos = sCurrency.indexOf ( '.' ); 
   if ( nPos < 0 && indexCount > 0){ 
      sCurrency += '.'; 
   } 
   sCurrency = sCurrency.slice ( 0, nPos + indexCount ); 
   var nZero = indexCount - ( sCurrency.length - nPos ); 
   for ( var i=0; i<nZero; i++ ) 
	 sCurrency += '0'; 
   if (Negate)
	   return "-" + sCurrency; 
   else
	   return sCurrency; 	
}

function alignLeft(){
    event.srcElement.style.textAlign = "left";
    }

var dtCh= "/";
var minYear=1900;
var maxYear=2100;
	
function isIntegerScan(s){
	var i;
	for (i = 0; i < s.length; i++){   
		// Check that current character is number.
		var c = s.charAt(i);
		if (((c < "0") || (c > "9"))) return false;
	}
	// All characters are numbers.
	return true;
}
	
function stripCharsInBag(s, bag){
	var i;
	var returnString = "";
	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for (i = 0; i < s.length; i++){   
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1) returnString += c;
	}
	return returnString;
}
	
function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}
		
function isDate(itemName, errName){
	dtStr = document.getElementById(itemName).value;
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	STANDARD_ERROR = "You have entered an invalid date for '" + errName + "'.\nPlease re-enter in the format dd/mm/yyyy."
	if (pos1==-1 || pos2==-1){
		validationError(itemName,STANDARD_ERROR);
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		validationError(itemName,STANDARD_ERROR);
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		validationError(itemName,STANDARD_ERROR);
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		validationError(itemName,STANDARD_ERROR);
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isIntegerScan(stripCharsInBag(dtStr, dtCh))==false){
		validationError(itemName,STANDARD_ERROR);
		return false
	}
	return true
}