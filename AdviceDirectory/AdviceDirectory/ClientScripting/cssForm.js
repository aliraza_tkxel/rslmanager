// JavaScript Document

function getLabelForId(id) {

    var label, labels = document.getElementsByTagName('label');
    for (var i = 0; (label = labels[i]); i++) {
        if (label.htmlFor == id) {
            return label;
        }
    }
    return false;
}

function isBlank(val){
	if(val==null){return true;}
	for(var i=0;i<val.length;i++) {
		if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r")){return false;}
		}
	return true;
	}



function chkObject(theVal) 
{
if (document.getElementById(theVal) != null) 
    {
    return true;
    } 
else 
    {
    return false;
    }
}



 function checkMyForm(formObj)
 { 
 //alert(formObj)
//alert("me")
//if  (!chkObject("Back"))
//{
 		document.getElementById("errordiv").innerHTML = "";
        var msg="";
		var innermsg = "";

        for (var i=0; i < formObj.elements.length; i++) {	
		var label = getLabelForId(formObj.elements[i].getAttribute("id"));
		
               // check if the form element has a validate attribute. 
               if (formObj.elements[i].name !=null && formObj.elements[i].getAttribute("validate")){
                 var validationRule = eval(formObj.elements[i].getAttribute("validate"));
				 var validationDo = formObj.elements[i].getAttribute("required");				 
				 var blankStatus = isBlank(formObj.elements[i].value);
				 
				 if (validationDo == "Y" && blankStatus) 
				 { 
				 	label.className = 'problem';
				 	msg += formObj.elements[i].getAttribute("validateMsg")+"\n";
					innermsg += formObj.elements[i].getAttribute("validateMsg")+"<br/>";
				 }
				 else 
				 {				 	
				 	label.className = 'completed';
				 }	
					
				 if (!blankStatus) 
				 {
				 	 if (!validationRule.test(formObj.elements[i].value)) 
				 	 {
                    	var obj = formObj.elements[i].parentNode; 
                    	if (obj.nodeName=="SPAN")
							label.className = 'problem';				
                    		obj.innerHTML = obj.innerHTML 		 
          					msg += formObj.elements[i].getAttribute("validateMsg")+"\n"; 
							innermsg += formObj.elements[i].getAttribute("validateMsg")+"<br/>";
                   }
				   else 
				   {				 	
				 	label.className = 'completed';
				 	}			
				 }
						  
                 } // end if element has validate attribute	
                 
       if (formObj.elements[i].getAttribute("id") == "txt_RETYPEPASSWORD")
		{
		    if (document.getElementById("txt_RETYPEPASSWORD").value != document.getElementById("txt_PASSWORD").value)
		        {
		        msg += "Passwords must be the same" +"\n";
		        innermsg += "Passwords must be the same" +"<br/>";
		        }
		
		}

                 
                 				
                } // end loop through the form elements. 
                
               
                
                
               if (msg.length > 0)
               {			   	
                 //alert ("There are errors with the following form fields.\n" + msg);		
				 //document.getElementById("errordiv").innerHTML = "<h4>There are errors with the following:</h4><p>" + msg.replace("\n", "<br/>")+"</p>" ; 
				 document.getElementById("errordiv").innerHTML = "<div style='padding:10px'><h4>There are errors with the following:</h4>" + innermsg +"</div>" ; 
                 document.getElementById("errordiv").className = "er"
                 return false;
               }
                 else
                 {
                  return true;
                 }
                 
// }
 // else
 // {
 // return true
 // }               
            }
            // end function
            

function valButton(btn)
{
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--)
    {
       if (btn[i].checked) {cnt = i;}
    }
    if (cnt > -1)
        return btn[cnt].value;
    else
        return null;
}

function clickFunction()
{
    if(document.getElementById("DPA_AWARE").checked == true)
    {
        document.getElementById("hid_DPA_AWARE").value = 1;
    }
    else
    {
        document.getElementById("hid_DPA_AWARE").value = 0;
    }
}