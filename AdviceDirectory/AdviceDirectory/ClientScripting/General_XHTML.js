// GENERAL SITE FUNCTIONS

function HotCourseSearch(){	
		FormFields = new Array("PhraseSearch|Keyword|TEXT|Y", "Level|Level|SELECT|Y", "postcode|Postcode|POSTCODE|Y", "GetXRecords|Mzx. No. of Records to be Returned|NUMERIC|Y");
		if (!checkForm()) return false;	
}


sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}

//if (window.attachEvent) window.attachEvent("onload", sfHover);


function PCase(STRING){
var strReturn_Value = "";
var iTemp = STRING.length;
if(iTemp==0){
return"";
}
var UcaseNext = false;
strReturn_Value += STRING.charAt(0).toUpperCase();
for(var iCounter=1;iCounter < iTemp;iCounter++){
if(UcaseNext == true){
strReturn_Value += STRING.charAt(iCounter).toUpperCase();
}
else{
strReturn_Value += STRING.charAt(iCounter).toLowerCase();
}
var iChar = STRING.charCodeAt(iCounter);
if(iChar == 32 || iChar == 45 || iChar == 46){
UcaseNext = true;
}
else{
UcaseNext = false
}
if(iChar == 99 || iChar == 67){
if(STRING.charCodeAt(iCounter-1)==77 || STRING.charCodeAt(iCounter-1)==109){
UcaseNext = true;
}
}


} //End For

return strReturn_Value;
} //End Function


function OpenWinCenter(str_page, WinH, WinW){
		var winl = (screen.width - WinW) / 2;
		var wint = (screen.height - WinH) / 2;
		winprops = 'height='+WinH+',width='+WinW+',top='+wint+',left='+winl+',scrollbars=yes,toolbar=no'
	    window.open(str_page, "Guidance", winprops)
}

function PreviewCV(){
pop_window_centre("CVPreview.asp",500,670)
//pop_up_window("CVPreview.asp",'CV','width=500,height=700,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no')
//Go2Target("Preview.asp",'NEW','_self')
}

// FUNCTION TO CHECK RADIO BUTTON IS SELECTED. SEE CALLING PAGE FOR HOW TO....
function valButton(btn) {
var cnt = -1;
for (var i=btn.length-1; i > -1; i--) {
   if (btn[i].checked) {cnt = i;}
   }
if (cnt > -1) return btn[cnt].value;
else return null; 
}

// CHECKS OR UNCHECKS RADIO OR CECKBOXES DEPENDING ON VALUE PROVIDED
function assign_radchk(rad_value, rad_name) {
	//document.all(""+rad_name+"")[rad_value].checked = true;
	if (parseInt(rad_value) == 1)
		document.all(""+rad_name+"")[0].checked = true;
	else if(parseInt(rad_value) == 0)
		document.all(""+rad_name+"")[1].checked = true;
	
}

function TransferData(iframe) {

var transferArray

	switch (iframe) {
		case 1 : transferArray = new Array("txt_FirstName", "txt_LastName", "txt_HouseNoName", "txt_Street", "txt_Town", "txt_City", "txt_County", "txt_Postcode", "txt_Telephone", "txt_Mobile", "txt_Email"); break; 
		case 2 : transferArray = new Array("txt_PP"); break; 
		case 3 : transferArray = new Array("txt_KS"); break; 
		case 4 : transferArray = new Array("txt_KE"); break; 
		case 51 : transferArray = new Array("txt_CompanyName1","txt_StartDate1","txt_EndDate1","txt_JobTitle1"); break; 
		case 52 : transferArray = new Array("txt_CompanyName2","txt_StartDate2","txt_EndDate2","txt_JobTitle2"); break; 
		case 53 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		case 54 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		case 55 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		case 56 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		case 57 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		case 58 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		case 59 : transferArray = new Array("txt_CompanyName","txt_StartDate","txt_EndDate","txt_JobTitle"); break; 
		
		case 7 : transferArray = new Array("txtDate_dd","txtDate_mm","txtDate_yyyy","txt_FCDL","txt_References","txt_Referee1","txt_Referee2","txt_Interests"); break; 
	}
	for (k=0; k<transferArray.length; k++) {
		alert(transferArray[k])
		parent.document.getElementById(transferArray[k]).value = document.getElementById(transferArray[k]).value;
	}
	GO('ServerSide/TransferData.asp?TabID='+iframe,'');
	document.getElementById("theAction").value = 'Edit';
}

// REMOVE ILLEGAL CHARACTERS - PROTECT WILMA!
	function RemoveBad() { 
		strTemp = event.srcElement.value;
		strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;|\(|\)|\&|\+/g,"");
		event.srcElement.value = strTemp;
		}

// DISPLAYS/HIDE GIVEN HTML ELEMENT IDENTITIFIED BY IT'S ID - DIV	
	function displayDiv(which,display) {
		 temp = document.getElementById(which);
		 temp.style.display = display ;
		 //temp.className="Noborder";
		 }
// DISPLAYS/HIDE GIVEN HTML ELEMENT IDENTITIFIED BY IT'S ID WHEN PLACED IN TBODY - IE ERROR IF USE ABOVE FUNCTION
	function elDisplay(which,display) {
    	 obj = document.getElementsByTagName('TBODY');
		 alert(which+'::'+display)
     	 for (i=0; i<obj.length; i++) {
          if (obj[i].id == which)
          obj[i].style.display = display;
     		}
		 } 
// REMOVE PROTENTIALLY SQL CORRUPTING CODES		 
	function RemoveBad() { 
		strTemp = event.srcElement.value;
		strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;|\(|\)|\&|\*|\+/g,""); 
		event.srcElement.value = strTemp;
	}
// REMOVE PROTENTIALLY SQL CORRUPTING CODES		 
	function RemoveBad_el(el) { 
		strTemp = document.getElementById(el).value;
		strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;|\(|\)|\&|\*|\+/g,""); 
		document.getElementById(el).value = strTemp;
	}
// LISTBOX JUMP FUNCTION - TARGETS SELF	
	function Finda(s){ 
		var x = s.options[s.selectedIndex].value;
		window.location = x;
		s.selectedIndex=0;
	}
	
function pop_window_centre(str, popupwidth, popupheight){
	var leftPos = 0; topPos = 0
		if (screen) {
			leftPos = (screen.width / 2) - 250
			topPos = (screen.height / 2) - 375
		}
	window.open(str,'Win','width='+popupwidth+',height='+popupheight+',left='+leftPos+',top='+topPos)
}
	
// POP-UP FUNCTION
function pop_up_window(str_page,winName,features) {
//features = "menubar=yes"

	var newWind=window.open(str_page,winName,features);
		(newWind.opener == null)
 		newWind.focus(); {
		newWind.opener = window;
		}  
}
// POP-UP FUNCTION - WRITES A HEADER PAGE TO THE POP-UP - LETS USER KNOW THEY ARE TRAVELING OUTSIDE YOUR SITE - NOT DONE REALLY IS IT?	
	function MM_openBrWindow(theURL,winName,features,linkURL) { //v2.0
		newWindow = window.open(theURL,winName,features);
		newWindow.document.write("<html><head>")
		newWindow.document.write("<title>nextstep Greater Merseyside<\/title><\/head>")
		newWindow.document.write("<frameset rows=\"70,*\" frameborder=\"0\" border=0 framespacing=\"0\">")
		newWindow.document.write("<frame name=\"Header\" src=\"Top.html\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"No\" frameborder=\"0\" noresize framespacing=\"0\">")
		newWindow.document.write("<frame name=\"TheLink\" src=\"")  
		newWindow.document.writeln(linkURL)
		newWindow.document.write("\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" noresize framespacing=\"0\">")
		newWindow.document.write("<\/frameset><body></body></html>")	
	}	
// POP-UP FUNCTION	
	function CallLinks(target, linkURL) {
	if (target == "ServerFrame") {
		document.getElementById("ServerFrame").src = linkURL;
		}
	else {
		MM_openBrWindow('','newWindow','width=800,height=600,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no', linkURL)
		//var newWind=window.open(linkURL,"display","width=425,height=550");
		//(newWind.opener == null)
 		//newWind.focus(); {
		//newWind.opener = window;	
		//}	
	}
}
 // GENERAL FORM SUBMITTING FUNCTION - TARGETS SERVERFRAME	
	function GO(thePage, myAction){document.thisForm.action = thePage; document.thisForm.target="ServerFrame"; document.thisForm.theAction.value = myAction;document.thisForm.submit();}

	function GOForm(theFrom, thePage, myAction){
		thisTempForm = eval('document.'+theFrom);
		thisTempForm.action = thePage
		thisTempForm.target = "ServerFrame"; 
		thisTempForm.theAction.value = myAction; 
		thisTempForm.submit()
		}

	function GoXHTML(theFrom, thePage){
		alert(theFrom)
		//var thisTempForm = eval('document.'+theFrom);
		//alert(thisTempForm)
		//thisTempForm.action = thePage
		//thisTempForm.target = "ServerFrame"; 
		//thisTempForm.submit()
		}
		
// GENERAL FORM SUBMITTING FUNCTION - TARGETS BROWSER WINDOW	
	function GO2(thePage, myAction){document.thisForm.action = thePage; document.thisForm.target="_self"; document.thisForm.submit();}
	 
	function Go2Target(thePage, myAction, myTarget){
	 thisForm.action = thePage; 
	 thisForm.target = myTarget; 
	 thisForm.theAction.value = myAction;
	 thisForm.submit();
	 }
		

	
// FIND - A - COURSE

// THIS SELECTS ALL CHECKBOXES THAT HAVE AN ID OF 'LearningType_1' ...ETC	
		function allNone(){
		var strName;
		for (j=1; j<7; j++){
			strName = 'LearningType_' + j;
			if (document.getElementById('allchk').checked == true)
				document.getElementById(strName).checked = true;
			else
				document.getElementById(strName).checked = false;
		}
	}

// MY VIRTUAL CENTRE

// loading bar. .. ...
var TOPLOADER;
var LoaderStatus_Top = "";
var LOADINGSIZE_TOP = 0
var LOADING_TEXT_DOTS = new Array(".", "..", "...", "....", ".....")

function STOPLOADER(which){
	try {
		clearTimeout(TOPLOADER);
		LoaderStatus_Top = "";
		document.getElementById(which).innerHTML = "";
		//document.getElementById(which).className="Noborder";
		}
	catch (e) {
		temp = 1
		}
	}

function STARTLOADER(which) {
	clearTimeout(TOPLOADER);
	LoaderStatus_Top = "working"
	CALLLOADER(which)
	}

function CALLLOADER(which) {
	if (LoaderStatus_Top == "working"){
		LOADINGSIZE_TOP++;
		if (LOADINGSIZE_TOP == 5) LOADINGSIZE_TOP = 0;
		//document.getElementById(which).className="LoadingBorder"; 
		document.getElementById(which).innerHTML = "<font color='#000000'>Loading" + LOADING_TEXT_DOTS[LOADINGSIZE_TOP] + "</font>";
		TOPLOADER = setTimeout("CALLLOADER('"+which+"')",300);
		}
	else 
		document.getElementById(which).innerHTML = "";
	}
	
function Vote(lAnketID){
	thisForm.action = "register.asp?ID="+lAnketID
	thisForm.target = "_self"
	thisForm.submit()
}


function CustDetails(action){

var inputs = Array("inpCUSTOMERID", "inpFirstName", "inpSurname", "inpGender", "inpDOB", "inpStreet", "inpTown", "inpCity", "inpPostCode", "inpTelephone", "inpEmail")
		
	if (action == "EDIT") {
		for (i = 1; i < 11; i++) {
			if (document.getElementById(inputs[i]).value == "---") { document.getElementById(inputs[i]).value = ""}
			document.getElementById(inputs[i]).className = 'EDIT_FIELD';
			document.getElementById(inputs[i]).readOnly = false;				
		}
		document.getElementById("Btn_Cust").value = "  SAVE "
		document.getElementById("theCustAction").value = "SAVE"
	}
	else {
		FormFields = new Array("inpFirstName|First Name|TEXT|Y", "inpSurname|Surname|TEXT|Y", "inpGender|Gender|TEXT|Y","inpDOB|Date of Birth|DATE|Y","inpStreet|Street|TEXT|Y","inpTown|Town|TEXT|Y","inpCity|City|TEXT|Y","inpPostCode|Postcode|POSTCODE|Y","inpTelephone|Telephone|TELEPHONE|N","inpEmail|Email|EMAIL|N");
		if (!checkForm()) return false;
		//return true;
		
		for (i = 1; i < 11; i++) {
			document.getElementById(inputs[i]).className = 'NO_EDIT_FIELD';
			document.getElementById(inputs[i]).readOnly = true;					
		}
		
		//GO('ServerSide/UpdateCustDetails.asp','')
		//alert('ServerSide/UpdateCustDetails.asp'+buildQueryString('mVcForm'))
		SendData('ServerSide/UpdateCustDetails.asp'+buildQueryString('mVcForm'));
		document.getElementById("Btn_Cust").value = "  AMEND "
		document.getElementById("theCustAction").value = "EDIT"			
		
	}	
	
}

//--- XHTML SITE FUNCTIONS

// Get the domain name in the URL of the current page.
var thispagedomain = ExtractDomainName(document.URL);

// Iterate over any links on the current page.
for(var i = 0; i <= document.links.length - 1; i++) {

// Note: document.links[i].hostname or document.links[i].host 
//   could have been used to obtain the domain name in the 
//   link href. However, we don't want any subdomains that 
//   might be part of the domain. Therefore, we just strip 
//   the domain name from the link href URL with the custom 
//   function ExtractDomainName()

   // Store lowercased href info in variable url.
   var url = document.links[i].href.toLowerCase();
   // Go to the top of the loop and continue if variable 
   //   url doesn't start with "http://" (thus, must 
   //   be a URL to the same domain).
   if(url.indexOf('http://') != 0) { continue; }
   // Extract the domain name from variable url.
   var hrefdomain = ExtractDomainName(url);
   // If the extracted domain name is different than the 
   //   domain name of the current web page, give the  
   //   link a target="_blank" attribute.
   if(thispagedomain != hrefdomain) { 
      document.links[i].target = '_blank';
      }
   }


function ExtractDomainName(s) {
// Feed this function anything that looks like 
//   a domain name or URL. It will do its best 
//   to extract the domain without subdomains.

// For the comments, let's assume that s equals
//   "http://www.books.Example.co.uk:80/page.html"

// Remove http://, if present.
// (Result is "www.books.Example.co.uk:80/page.html")
var i = s.indexOf('//');
if(i > -1) { s = s.substr(i+2); }

// Remove path/file information, if present.
// (Result is "www.books.Example.co.uk:80")
i = s.indexOf('/');
if(i > -1) { s = s.substr(0,i); }

// Remove port number, if present.
// (Result is "www.books.Example.co.uk")
i = s.indexOf(':');
if(i > -1) { s = s.substr(0,i); }

// Return s if no letters (could be an IP address).
// (Doesn't apply to the example)
var re = /[a-z]/i;
if(! re.test(s)) { return s; }

// Split s into chunks on periods, store in array a.
// (Result is 
//          "www"
//          "books"
//          "Example"
//          "co"
//          "uk"
//    (5 chunks) )
var a = s.split('.');

// If less than 2 chunks, it's not really a domain name.
//   Just return s and be done with it.
// (Doesn't apply to the example)
if(a.length < 2) { return s; }

// Create domain name with last 2 chunks of array a.
// (Result is "co.uk")
var domain = a[a.length-2] + '.' + a[a.length-1];

// If more than 2 chunks ...
// (Yes, the example has 5 chunks)
if(a.length > 2) {
   // ... and if the last two chunks are each exactly 
   //   2 characters long, it's probably a domain with 
   //   the format Example.co.uk. Therefore, insert the 
   //   third from last chunk of array a into the front 
   //   of the domain name.
   // (The example "co.uk" matches those criteria where, if  
   //   "example.com" had been the domain, it would not.)
   // (Result is "Example.co.uk")
   if(a[a.length-2].length==2 && a[a.length-1].length==2) {
      domain = a[a.length-3] + '.' + domain;
      }
   }

// Lowercase the domain name and return it.
// (Result is "example.co.uk")
return domain.toLowerCase();
} // end of function ExtractDomainName()


function externalLinks() {
 if (!document.getElementsByTagName) return;
 var anchors = document.getElementsByTagName("a");

 for (var i=0; i<anchors.length; i++) {
   var anchor = anchors[i];  
     
	 // The following is no longer uses, the below code is an enhancemet 
	 // if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external")
     // anchor.target = "_blank";  
	  
    var url = anchor.getAttribute("href")		
	if(url.indexOf('http://') != 0) { continue; }
  	var hrefdomain = ExtractDomainName(url);
   	if(thispagedomain != hrefdomain) { 
	  anchor.target = "_blank";
   }	
	   
 }
}
window.onload = externalLinks;


function switchText(object, baseText, eventName) {
if (eventName == "blur") {
	if (object.value == "") {
		object.value = baseText;
	}
}
else {
	if (object.value == baseText) {
		object.value = "";
		}
	}
}