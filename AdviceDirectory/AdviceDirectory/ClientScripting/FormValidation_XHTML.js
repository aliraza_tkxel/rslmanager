errorTColour = "#FFFFFF";TColour = "#000000";

function checkItem(){
	elName = event.srcElement.name;
	for (i=0;i<FormFields.length;i++){
		tpStr = FormFields[i];
		tpStr = tpStr.split("|");
		if (tpStr[0] == elName){
			itemValue = document.getElementById(tpStr[0]).value;
			blankStatus = isBlank(itemValue);
			document.getElementById(tpStr[0]).style.color = TColour;
			if (!blankStatus) {
				if (tpStr[2] == "CURRENCY") {
					if (isNumeric(tpStr[0], tpStr[1])){
						document.getElementById(tpStr[0]).value = FormatCurrency(itemValue);
						document.getElementById(tpStr[0]).style.textAlign = "right";
						}
					else {
						targetError(tpStr[0],"red");
						return false;
						}
					}
				else if (tpStr[2] == "INTEGER"){
					if (!isInteger(tpStr[0], tpStr[1])){
						targetError(tpStr[0],"red");
						return false;
						}
					}
				else if (tpStr[2] == "POSTCODE"){
					if (!isUKPostCode(tpStr[0], tpStr[1])){
					    alert("eeeeeee")
				        errTxt = "You must input a valid postcode for '" + tpStr[0] + "',\nCorrect formats: [SW12 2EG][L4 5PQ][L17 2EA]."
					
						targetError(tpStr[0],"red");
						return false;
						}
					}
				else if (tpStr[2] == "EMAIL"){
					if (!checkEmail(tpStr[0], tpStr[1])){
						targetError(tpStr[0],"red");
						return false;
						}
					}
				else if (tpStr[2] == "TELEPHONE"){
					if (!isTelephone(tpStr[0], tpStr[1])){
						targetError(tpStr[0],"red");
						return false;
						}
					}
				else if (tpStr[2] == "TEXT"){
					if (!filterText(tpStr[0], tpStr[1])){
						targetError(tpStr[0],"blue");
						return false;
						}
					}
				else if (tpStr[2] == "NATIONAL INSURANCE"){
				if (!filterText(tpStr[0], tpStr[1])){
					targetError(tpStr[0],"blue");
					return false;
					}
				}
				else if (tpStr[2] == "DATE"){
					if (!isDate(tpStr[0], tpStr[1])){
						targetError(tpStr[0],"red");
						return false;
						}
					}
					alert("ss")
				}
		return true;
			}
	}
}


function checkForm(div){
//alert('doc: '+ document.getElementById("txt_SiteSearch").value)
//alert('cf1')
//	doc = document.getElementById;
//alert(FormFields.length)
	var errTxt = "";
//alert('Error text')
	for (i=0; i<FormFields.length; i++){
//alert(FormFields.length)
		var tpStr = FormFields[i];
		tpStr = tpStr.split("|");
//alert('tpStr: '+tpStr)
//alert(tpStr[0])
		var itemValue = document.getElementById(tpStr[0]).value;
//alert(itemValue)
//alert('cf2'+itemValue)
		blankStatus = isBlank(itemValue);

		if (tpStr[2] != "RADIO")
			document.getElementById(tpStr[0]).style.color = TColour;
		if (tpStr[3] == "Y" && blankStatus) {
			if (tpStr[2] == "SELECT" || tpStr[2] == "TEXT")
				errTxt += "  " + tpStr[1] + "\<br/>";
			else if(tpStr[2] == "EMAIL" || tpStr[2] == "INTEGER")
				errTxt += "  " + tpStr[1] + " - must be an " + tpStr[2].toLowerCase() + "\<br/>";
			else if(tpStr[2] == "TELEPHONE")
				errTxt += "  " + tpStr[1] + " - must be a number\<br/>";
			else if(tpStr[2] == "NATIONAL INSURANCE")
				errTxt += "  " + tpStr[1] + " - must be a " + tpStr[2].toLowerCase() + "number\<br/>";
			else if(tpStr[2] == "DATE")
				errTxt += "  " + tpStr[1] + " - must be a date\<br/>";
			else							
				errTxt += "  " + tpStr[1] + " - must be a " + tpStr[2].toLowerCase() + "\<br/>";
			if (tpStr[2] != "RADIO")
				document.getElementById(tpStr[0]).style.color = "red";
			}
		}
		if (errTxt != "") {
			//alert("You must complete the following fields to continue:\n" + errTxt);
			document.getElementById("FrmEr_Edit").innerHTML = "<div style='padding:10px'><h4>You must complete the following fields to continue:</h4>" + errTxt +"</div>" ; 
            document.getElementById("FrmEr_Edit").className = "er"
			return false;
			}

	for (i=0; i<FormFields.length; i++){
		tpStr = FormFields[i];
		tpStr = tpStr.split("|");
		itemValue = document.getElementById(tpStr[0]).value;
		blankStatus = isBlank(itemValue);
		if (!blankStatus) {
			if (tpStr[2] == "CURRENCY") {
				if (isNumeric(tpStr[0], tpStr[1]))
					document.getElementById(tpStr[0]).value = FormatCurrency(itemValue);
				else {
					targetError(tpStr[0],"red");
					return false;
					}
				}
			else if (tpStr[2] == "INTEGER"){
				if (!isInteger(tpStr[0], tpStr[1])){
					targetError(tpStr[0],"red");
					return false;
					}
				}
			else if (tpStr[2] == "POSTCODE"){
				if (!isUKPostCode(tpStr[0], tpStr[1])){
				    targetError(tpStr[0],"red");
					return false;
					}
				}
			else if (tpStr[2] == "EMAIL"){
				if (!checkEmail(tpStr[0], tpStr[1])){
					targetError(tpStr[0],"red");
					return false;
					}
				}
			else if (tpStr[2] == "TELEPHONE"){
				if (!isTelephone(tpStr[0], tpStr[1])){
				    targetError(tpStr[0],"red");
					return false;
					}
				}
			else if (tpStr[2] == "TEXT"){
				if (!filterText(tpStr[0], tpStr[1])){
					targetError(tpStr[0],"blue");
					return false;
					}
				}
			else if (tpStr[2] == "NATIONAL INSURANCE"){
				if (!isNI(tpStr[0], tpStr[1])){
					targetError(tpStr[0],"blue");
					return false;
					}
				}
			else if (tpStr[2] == "DATE"){
				if (!isDate(tpStr[0], tpStr[1])){
					targetError(tpStr[0],"red");
					return false;
					}
				}
			}			
		}
	return true;
	}

function targetError(iItem, iColor){
	document.getElementById(iItem).style.color = iColor
	document.getElementById(iItem).focus();
	}

function isNI(itemName, errName){
	doc = document.all;
	elVal = doc[itemName].value;
    elVal = TrimAll(elVal.toUpperCase());
	if (/^[A-CEGHJ-NOPR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[ABCD]{0,1}$/g.test(elVal))
		return (true);
	else {
		//alert("You must input a valid National Insurance Number for '" + errName + "'\nCorrect format: [JP020456Z].");
		//document.getElementById("FrmEr_Edit").innerHTML = "<div style='padding:10px'>You must input a valid National Insurance Number for '" + errName + "'\nCorrect format: [JP020456Z]</div>" ; 
        //document.getElementById("FrmEr_Edit").className = "er"
		targetError(itemName,"red");
		return (false);
		}
}

function checkEmail(itemName, errName) {
	elVal = document.getElementById(itemName).value;
	if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,7}|[0-9]{1,3})(\]?)$/.test(elVal))
		return (true);
	else {
		//alert("You have entered an invalid email for '" + errName + "', please re-enter.\nCorrect formats: [support@reidmark.com][elephant@inet.org.uk]");
		targetError(itemName,"red");
		return (false);
		}
	}

function isBlank(val){
	if(val==null){return true;}
	for(var i=0;i<val.length;i++) {
		if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r")){return false;}
		}
	return true;
	}

function isDigit(num) {
	if (num.length>1){return false;}
	var string="1234567890";
	if (string.indexOf(num)!=-1){return true;}
	return false;
	}

function isUKPostCode (itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = TrimAll(elVal.toUpperCase());
	result = elVal.match(/^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/g);
	if (!result) {
	    //document.getElementById("FrmEr_Edit").innerHTML = "<div style='padding:10px'>You must input a valid postcode for '" + errName + "',\nCorrect formats: [SW12 2EG][L4 5PQ][L17 2EA].</div>" ; 
        //document.getElementById("FrmEr_Edit").className = "er"
		//alert("You must input a valid postcode for '" + errName + "',\nCorrect formats: [SW12 2EG][L4 5PQ][L17 2EA].");
		targetError(itemName,"red");
		return false;	
		}
	else {
		elVal = String(elVal.replace(/\s/g,""));
		elValLen = elVal.length;
		elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		document.getElementById(itemName).value = elVal;
		return true;
		}
	}


function isTelephone(itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = elVal.replace(/\s/g, "");	
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			alert("You must input a valid Telephone Number for '" + errName + "'.");
			targetError(itemName,"red");
			return false;
			}
		}
	document.getElementById(itemName).value = TrimAll(document.getElementById(itemName).value);		
	return true;
	}

function filterText(itemName, errName) { 
	elVal = document.getElementById(itemName).value;
	if (elVal.match(/\<|\>|\"|\'|\%|\;|\&/)){
		elVal = elVal.replace(/\<|\>|\"|\'|\%|\;|\&/g,""); 
		document.getElementById(itemName).value = elVal;
		return false;
		}
	else
		return true;
} 
	
function isInteger(itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = TrimAll(elVal);	
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			alert("You must input a positive valid number for '" + errName + "'.");
			targetError(itemName,"red");
			return false;
			}
		}
	document.getElementById(itemName).value = elVal;		
	return true;
	}

function LTrimAll(str) {
	if (str==null){return str;}
	for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	return str.substring(i,str.length);
	}
	
function RTrimAll(str) {
	if (str==null){return str;}
	for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	return str.substring(0,i+1);
	}
	
function TrimAll(str) {
	return LTrimAll(RTrimAll(str));
	}

function isNumeric(itemName, errName){
	elVal = document.getElementById(itemName).value;
	elVal = TrimAll(elVal);
	if ( parseFloat(elVal,10) == ( elVal*1) ){
		document.getElementById(itemName).value = elVal;
		return true;
		}
	else {
		alert("You must input a positive valid number for '" + errName + "'.");
		targetError(itemName,"red");
		return false;
		}
	}

function FormatCurrency ( fPrice ) { 
   var sCurrency = "" + ( parseFloat(fPrice,10) + 0.00500000001 ); 
   var nPos = sCurrency.indexOf ( '.' ); 
   if ( nPos < 0 ){ 
      sCurrency += '.00'; 
   } 
   else { 
      sCurrency = sCurrency.slice ( 0, nPos + 3 ); 
      var nZero = 3 - ( sCurrency.length - nPos ); 
      for ( var i=0; i<nZero; i++ ) 
         sCurrency += '0'; 
   } 
   return sCurrency; 
}

function alignLeft(){
    event.srcElement.style.textAlign = "left";
    }

var dtCh= "/";
var minYear=1900;
var maxYear=2100;
	
function isIntegerScan(s){
	var i;
	for (i = 0; i < s.length; i++){   
		// Check that current character is number.
		var c = s.charAt(i);
		if (((c < "0") || (c > "9"))) return false;
	}
	// All characters are numbers.
	return true;
}
	
function stripCharsInBag(s, bag){
	var i;
	var returnString = "";
	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for (i = 0; i < s.length; i++){   
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1) returnString += c;
	}
	return returnString;
}
	
function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

	function DaysArray(n) {
		for (var i = 1; i <= n; i++) {
			this[i] = 31
			if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
			if (i==2) {this[i] = 29}
	   } 
	   return this
	}
		
function isDate(itemName, errName){
	dtStr = document.getElementById(itemName).value;
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	STANDARD_ERROR = "You have entered an invalid date for '" + errName + "'.\nPlease re-enter in the format dd/mm/yyyy."
	if (pos1==-1 || pos2==-1){
		alert(STANDARD_ERROR);
		targetError(itemName,"red");
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert(STANDARD_ERROR);
		targetError(itemName,"red");
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert(STANDARD_ERROR);
		targetError(itemName,"red");
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert(STANDARD_ERROR);
		targetError(itemName,"red");
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isIntegerScan(stripCharsInBag(dtStr, dtCh))==false){
		alert(STANDARD_ERROR);
		targetError(itemName,"red");
		return false
	}
	return true
}