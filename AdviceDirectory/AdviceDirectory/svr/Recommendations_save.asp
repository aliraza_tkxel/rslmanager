<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%	
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Session("svOurRecommendations") = Request("CheckboxSite")

If Session("svCustomerID") <> "" Then
    'SAVE SELECTED ie ALL RESULTS
    Call SaveRecommendations(Session("svCustomerID"))
    rw "Your Self Assessment has been saved."
Else
    rw "To save your results, please login to " & SITE_LOGIN_URL & ", or click here to " & SITE_REGISTER_URL & " and your results will automatically be saved."
End If

myStrPL = Session("PriorityList")

If myStrPL <> "" Then
    MyArray = split(myStrPL,"|")
    For Each item In MyArray
        MyArray2 = split(item,"-")
        'rw "Session(" & MyArray2(0) & ")s = " & MyArray2(1) & "<br/>"
        Session(MyArray2(0)) = CInt(MyArray2(1))
    Next
End If
%>