<%@LANGUAGE="VBSCRIPT" %>
<%
	
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
If (Session("svCustomerID") = "" OR IsNull(Session("svCustomerID")) OR Session("svCustomerID") = -1) Then
    Response.Redirect("Login.asp?Session=0")
End If
%>
<%
'Uses pythag to work out the distance between 2 points
Function Distance (e1, e2, n1, n2)
	Distance = ((e1-e2)^2 + (n1-n2)^2)^0.5
End Function

Function CurrentContacts(CustomerID, Priority, display)

   SQL = " SELECT CP.SESSIONID, AO.DESCRIPTION AS BARRIER, CP.PRIORITY, P.PROVIDERID, P.PROVIDERNAME, S.SITEID, S.SITENAME, S.ADDRESSLINE1, S.TELEPHONE, S.EMAIL, R.EASTING, R.NORTHING, '' AS DISTANCE, R.CUSTOMERID " &_
       " FROM dbo.TBL_MD_TMPSEARCH_RESULT_SAVED RS " &_
       " INNER JOIN TBL_MD_TMPSEARCH_RESULT R ON R.SITEID = RS.SITEID AND R.SESSIONID = RS.SESSIONID " &_
       " RIGHT OUTER JOIN CUSTOMERID_PRIORITIES CP ON CP.SESSIONID = R.SESSIONID AND CP.ASSESSMENTOPTIONID = RS.ASSESSMENTOPTIONID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER P ON P.PROVIDERID = R.PROVIDERID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER_DETAIL PD ON R.PROVIDERID = PD.PROVIDERID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER_SITE S ON S.SITEID = R.SITEID " &_
       " INNER JOIN ASSESSMENT_OPTIONS AO ON AO.ID = CP.ASSESSMENTOPTIONID " &_
       " WHERE CP.CUSTOMERID = " & CustomerID & " AND CP.PRIORITY = " & Priority & " " &_
       " ORDER BY CP.PRIORITY"
       
 SQL = " SELECT CP.SESSIONID, AO.ID AS AOID, AO.DESCRIPTION AS BARRIER, CP.PRIORITY, P.PROVIDERID, P.PROVIDERNAME, S.SITEID, S.SITENAME, S.ADDRESSLINE1, S.TELEPHONE, S.EMAIL, R.EASTING, R.NORTHING, '' AS DISTANCE, R.CUSTOMERID " &_
       " FROM dbo.TBL_MD_TMPSEARCH_RESULT_SAVED RS " &_
       " INNER JOIN TBL_MD_TMPSEARCH_RESULT R ON R.SITEID = RS.SITEID AND R.SESSIONID = RS.SESSIONID " &_
       " RIGHT OUTER JOIN " &_
       " ( " &_
       "  	SELECT SESSIONID, ASSESSMENTOPTIONID, HISTORYID, PRIORITY, CUSTOMERID " &_
	   "	FROM CUSTOMERID_PRIORITIES OUT_CP " &_
	   "	WHERE HISTORYID = ( " &_
	   "				SELECT MAX(HISTORYID) AS HISTORYID " &_
	   "				FROM CUSTOMERID_PRIORITIES IN_CP " &_
	   "				WHERE OUT_CP.SESSIONID = IN_CP.SESSIONID " &_
	   "				GROUP BY IN_CP.SESSIONID " &_
	   "				) " &_
       " ) " &_
       " CP ON CP.SESSIONID = R.SESSIONID AND CP.ASSESSMENTOPTIONID = RS.ASSESSMENTOPTIONID  " &_
	   "     LEFT JOIN TBL_MD_PROVIDER P ON P.PROVIDERID = R.PROVIDERID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER_DETAIL PD ON R.PROVIDERID = PD.PROVIDERID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER_SITE S ON S.SITEID = R.SITEID " &_
       " INNER JOIN ASSESSMENT_OPTIONS AO ON AO.ID = CP.ASSESSMENTOPTIONID " &_
       " WHERE CP.CUSTOMERID = " & CustomerID & " AND CP.PRIORITY = " & Priority & " " &_
       " ORDER BY CP.PRIORITY"

Call OpenDB()
Call OpenRs(rsPage,SQL)
If rsPage.EOF Then

            strResult = strResult & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""5"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Distance</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult = strResult & "<tr>" &_
	    "<td valign=""top"" colspan=""5"">There were no results</td>" &_
	"</tr>" 
Else
        strResult = strResult & "<br/><b>Priority " & Priority & " ("& rsPage("BARRIER") &") " & "</b><a href=""JavaScript:Toggle('"&Priority&"')""><img src=""/images/plusImage.gif"" width=""20"" height=""20"" border=""0"" align=""absmiddle"" name=""IMG_"&Priority&""" /></a>" &_
                                "<div style=""display:"&display&";"" id=""Div_"&Priority&""">" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""5"" style=""background-color:white; color:black; padding-left:0px""></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Distance</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
   If IsNull(rsPage("ProviderID")) Then
	    strResult = strResult & "<tr>" &_
	        "<td valign=""top"" colspan=""5"">There were no results</td>" &_
	    "</tr>"
	Else
'i = 0
	    Do until rsPage.EOF
	    'i = i + 1
	        dblCustomerEasting = "362196"
	        dblCustomerNorthing = "404438"
	        dblDistance = Distance(dblCustomerEasting, rsPage("easting"), dblCustomerNorthing, rsPage("northing"))
			'Convert the distance in metres to miles
			dblDistance = dblDistance / 1609 

		    If not isnumeric(dblDistance) Then
			    strDistance = "N/A" 
		    Else
			    strDistance = FormatNumber(dblDistance,2) & " miles"
		    End If

		    strProvider = ""
		    If trim(rsPage("ProviderName") ) = trim(rsPage("SiteName") ) Then
			    strProvider = rsPage("ProviderName")
		    Else
			    strProvider = rsPage("ProviderName") & ", " & rsPage("SiteName")
		    End If

            ADDRESSLINE1 = rsPage("ADDRESSLINE1")
		    EMAIL = rsPage("EMAIL")
		    If Isnull(EMAIL) or EMAIL = "" Then EMAIL = NULL Else EMAIL = "<a href=""mailto:"&EMAIL&""" title=""email:"&strProvider&""">"&EMAIL&"</a>" End If
		    TELEPHONE = rsPage("TELEPHONE")

		    strResult = strResult & "<tr>" & vbCrLf &_
		    "<td><a onclick=""go(buildQueryString('Form1'),'OrgDetails',"&rsPage("SITEID")&")"" title="""&strProvider&""">" & strProvider & "</a></td>" & vbCrLf &_
		    "<td>" & ADDRESSLINE1 & "</td>" & vbCrLf &_
		    "<td>" & TELEPHONE & "</td>" & vbCrLf &_
		    "<td>" & EMAIL & "</td>" & vbCrLf &_
		    "<td>" & strDistance & "</td>" & vbCrLf &_
		    "</tr>" 
'-- SITE , PRIORITY ITEM E.G. HOUSING, PRIORITY ORDER (SPECIFIED BY USER)

	        rsPage.MoveNext
	        'rw i
	    Loop

	    End If

	    rsPage.Close
	set rsPage = Nothing

	'totalrows = cmd.Parameters("@TOTALROWS").Value	

	strResult = strResult & "</tbody>" & vbCrLf
	
	strResult = strResult & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""5"">"

    'if cint(page) > cint(0) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	'end if	
	'if cint(page) + cint(maxrows) < cint(totalrows) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	'end if

End if

    strResult = strResult & "</td></tr></tfoot>" & vbCrLf &_
	                        "</table></div>"
	'strResult = strResult & "</table></div><br/>"
   
    'Next
    CurrentContacts = strResult
    'strResult = ""

'end if

'Call CloseDB()

End Function

Dim ResultString
    ResultString = CurrentContacts(Session("svCustomerID"),1, "block")
    ResultString = ResultString & CurrentContacts(Session("svCustomerID"),2, "none")
    ResultString = ResultString & CurrentContacts(Session("svCustomerID"),3, "none")
%>
<%=ResultString%>