<%@  language="VBSCRIPT" %>
<%
'http://www.zipcodeworld.com/samples/distance.asp.html
'http://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Validation
const pi = 3.14159265358979323846

Function distance(lat1, lon1, lat2, lon2, unit)
  Dim theta, dist
  theta = lon1 - lon2
  dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
  response.write "dist = " & dist & "<br>"
  dist = acos(dist)
  dist = rad2deg(dist)
  response.write "dist = " & dist & "<br>"
  distance = dist * 60 * 1.1515
  Select Case lcase(unit)
    Case "k"
      distance = distance * 1.609344
    Case "n"
      distance = distance * 0.8684
  End Select
End Function 


'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function get the arccos function using arctan function   :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function acos(rad)
  If abs(rad) <> 1 Then
    acos = pi/2 - atn(rad / sqr(1 - rad * rad))
  ElseIf rad = -1 Then
    acos = pi
  End If
End function


'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function converts decimal degrees to radians             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function deg2rad(deg)
	deg2rad = cdbl(deg * pi / 180)
End Function

'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function converts radians to decimal degrees             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function rad2deg(rad)
	rad2deg = cdbl(rad * 180 / pi)
End Function

'Response.Write(distance(32.9697, -96.80322, 29.46786, -98.53506, "m") & " miles<br>")
'Response.Write(distance(32.9697, -96.80322, 29.46786, -98.53506, "k") & " kilometers<br>")
'Response.Write(distance(32.9697, -96.80322, 29.46786, -98.53506, "n") & " nautical miles<br>")

Response.Write(distance(53.535962, -2.573546, 52.62238, 1.308603, "m") & " miles<br>")
Response.Write(distance(53.535962, -2.573546, 52.62238, 1.308603, "k") & " kilometers<br>")
Response.Write(distance(53.535962, -2.573546, 52.62238, 1.308603, "n") & " nautical miles<br>")
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="page-enter" content="revealtrans(duration=0.000,transition=5)" />
    <meta http-equiv="page-exit" content="revealtrans(duration=0.000,transition=5)" />
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <title>Google</title>
    <style type="text/css">
        /*html { height: 100% }
        body { height: 100%; margin: 0; padding: 0 }
        #map_canvas { height: 100% }*/
        #map
        {
	        height: 500px;
	        width: 500px;
	        display:block;
        }
    </style>

    <script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="gmap.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

    function PCode(){
        if (document.getElementById("postcode").value != "")
	    {
	        return isUKPostCode ("postcode", "You must enter a valid postcode")
	    }
	    else {
	        document.getElementById("map").style.display = "none";
	        document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
	    }
    }

    function isUKPostCode (itemName, errName){
	    elVal = document.getElementById(itemName).value;
	    elVal = TrimAll(elVal.toUpperCase());
	    result = elVal.match(/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) [0-9][ABD-HJLNP-UW-Z]{2})$/g);
	    if (!result) {
		    targetError(itemName,"red");
		    document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
		    //console.log(errName);
		    document.getElementById("map").style.display = "none"
		    return false;
		    }
	    else {
		    elVal = String(elVal.replace(/\s/g,""));
		    elValLen = elVal.length;
		    elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		    document.getElementById(itemName).value = elVal;
		    targetError(itemName,"black");
		    //console.log(elVal);
		    document.getElementById("map").style.display = "block";
		    usePointFromPostcode(document.getElementById('postcode').value, setLatLng);
		    return true;
		    }
	    }

    function TrimAll(str) {
	    return LTrimAll(RTrimAll(str));
	    }

    function LTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	    return str.substring(i,str.length);
	    }

    function RTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	    return str.substring(0,i+1);
	    }

    function targetError(iItem, iColor){
	    document.getElementById(iItem).style.color = iColor
	    document.getElementById(iItem).focus();
	    }

    </script>

</head>
<body>
    PostCode :<input type="text" id="postcode" size="10" onchange="PCode()" /><br />
    Longitude :<input type="text" id="hiddenLng" readonly="readonly" style="border: 0px" /><br />
    Latitude :<input type="text" id="hiddenLat" readonly="readonly" style="border: 0px" /><br />
    <input style="display: none" type="submit" value="Place Marker" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, placeMarkerAtPoint)" />
    <input style="display: none" type="submit" value="Center Map" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, setCenterToPoint)" />
    <input style="display: none" type="submit" value="Show Lat/Lng" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, showPointLatLng)" />
    <input style="display: none" type="submit" value="Validate" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, setLatLng)" />
    <div id="map">
    </div>
</body>
</html>
