<script type="text/javascript">
<!--//--><![CDATA[//><!--
<%' If JS_RUN = "YES" then %>

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}


function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) { 
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}

function SendData(strURL)
{
	//alert("SendData"+strURL)
	//var strFullURL = strURL+buildQueryString('cssForm');

	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
					   			alert('Server Error\n'+strResponse);
							//	document.getElementById('search').innerHTML = strResponse;
							  // handleErr(strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("An JavaScript error has occured");
							//	document.getElementById('search').innerHTML = strResponse;
							   }
							   // Call the desired result function
							   else {
									     //eval(strResponse);		
									   document.getElementById("Data").innerHTML = strResponse	
									   document.getElementById("Data").style.display = 'block'
									   STOPLOADER('LoadingText');				   
									   //eval(strResultFunc + '(strResponse);');
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}



function ReturnData(strURL, ReturnDiv)
{
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
					   			alert('Server Error\n'+strResponse);
							  // handleErr(strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert(strResponse);
							   }
							   else {
									   document.getElementById(ReturnDiv).innerHTML = strResponse	
									   document.getElementById(ReturnDiv).style.display = 'block'
									   //STOPLOADER('LoadingText');				   
							   }
							   break;
			   }
	   }	  
 	}
 xmlhttp.send(null)   
}


function SaveLink(strURL){

	//alert("SaveLink"+strURL)
	
	xmlhttp.open("GET", "ServerSide/PORTAL_SAVE_LINK.asp?URL="+strURL,true);
 	xmlhttp.onreadystatechange=function() {	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //document.getElementById('search').innerHTML = strResponse;
							   alert('Server Error\n'+strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("JavaScript Error");
							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');									   
									   alert(strResponse)
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}


function SavePage(strURL){

	//alert("SavePage"+strURL)
	
	xmlhttp.open("GET", "ServerSide/PORTAL_SAVE_PAGE.asp?URL="+strURL,true);
 	xmlhttp.onreadystatechange=function() {	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //document.getElementById('search').innerHTML = strResponse;
							   alert('Server Error\n'+strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("JavaScript Error");
							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');									   
									   alert(strResponse)
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
  		//Good for debugging if erros occur
		//thisForm.action = "ServerSide/PORTAL_SAVE_PAGE.asp?URL="+strURL	
		//thisForm.target = "_blank"
		//thisForm.submit()
  
}



function SaveCourse(strURL){
	
	xmlhttp.open("GET", strURL,true);
 	xmlhttp.onreadystatechange=function() {	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //document.getElementById('search').innerHTML = strResponse;
							   alert('Server Error\n'+strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("JavaScript Error");
							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');									   
									   alert(strResponse)
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}


function DeleteLink(LoadPage,strURL){

	//alert("DeleteLink")
	//alert("LoadPage"+LoadPage+"strURL"+strURL)

	xmlhttp.open("GET", "ServerSide/PORTAL_DELETE_LINK.asp?URL="+strURL,true);
 	xmlhttp.onreadystatechange=function() {	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //document.getElementById('search').innerHTML = strResponse;
							   alert('Server Error\n'+strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("JavaScript Error");
							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');									   
									   //alert(strResponse)
									   Fload(toLoadPage[LoadPage])
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}


function DeleteCourse(LoadPage,strURL){

	//alert("DeleteCourse")
	//alert("LoadPage"+LoadPage+"strURL"+strURL)

	xmlhttp.open("GET", "ServerSide/PORTAL_DELETE_COURSE.asp?ID="+strURL,true);
 	xmlhttp.onreadystatechange=function() {	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //document.getElementById('search').innerHTML = strResponse;
							   alert('Server Error\n'+strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("JavaScript Error");
							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');									   
									   //alert(strResponse)
									   Fload(toLoadPage[LoadPage])
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}


function DeletePage(LoadPage,strURL){

	//alert("DeletePage")
	//alert("LoadPage"+LoadPage+"strURL"+strURL)

	xmlhttp.open("GET", "ServerSide/PORTAL_DELETE_PAGE.asp?URL="+strURL,true);
 	xmlhttp.onreadystatechange=function() {	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //document.getElementById('search').innerHTML = strResponse;
							   alert('Server Error\n'+strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("JavaScript Error");
							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');									   
									   //alert(strResponse)
									   Fload(toLoadPage[LoadPage])
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}
<%' End If %>


//-- submit asknow form

function Asknow(strURL)
{
	document.getElementById("CSSValidationBox").style.display = 'none';
	
	var strFullURL = strURL+buildQueryString('asknow');
	var strMsg = "";

	xmlhttp.open("GET",strFullURL,true);
 	xmlhttp.onreadystatechange=function() {
	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strURL + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
					   			alert(strResponse);
							  // handleErr(strResponse);
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert(strResponse);
							   }
							   // Call the desired result
							   else {
							   
							  //document.getElementById("CSSValidationBox").style.display = 'block';
							  //document.getElementById("CSSValidationBox").innerHTML = strResponse;
							   
							   
							   			switch (parseInt(strResponse)) 
										{				
											case 1: strMsg = "We do not hold these details on file. Please <a href='/RegisterCSS.asp'>register</a>."; borderColor = 'red' ; break;							
									  		case 2: strMsg = "Insufficient information was collected to process the request."; borderColor = 'red' ; break;
											case 3: strMsg = "Thank you for your question - an adviser will reply shortly."; borderColor = 'green' ; break;
											default: strMsg = "Insufficient information was collected to process the request."; borderColor = 'red' ; break;
										}
											document.getElementById("CSSValidationBox").style.display = 'block';
											document.getElementById("CSSValidationBox").style.borderColor = borderColor;
											document.getElementById("CSSValidationBox").innerHTML = strMsg
										document.getElementById('asknow').reset()
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}

//--><!]]>
</script>