<% 
Response.Buffer = True

Dim Session
Set Session = server.CreateObject("SessionMgr.Session2")

Const page_title_prefix = "BHG Advice Directory"
Const SITE_NAME =  "BHG Advice Directory"
Const nextstep_area = "BHG Advice Directory"

Const email_replyto = "robert.egan@reidmark.com"
Const email_recipient = "robert.egan@reidmark.com"
Const email_bcc = "robert.egan@reidmark.com"
Const constReplyToName = "nextstepgreatermanchester"
Const constReplyToEmail = "anp.info@manchester-solutions.co.uk"

' DEFINE INTERNET ADDRESS OF SITE
Const SiteURL = "http://greatermanchester-anp.org.uk"
Const WebLogo = "Greater_Manchester_Advancement_Network.gif"
Const WebLogoH = 63
Const WebLogoW = 200
'Const LocalConnexions = "http://www.gmconnexions.com"
'Const FreePhone = "0161 245 4858" 

Const CSSstyle = "style3"
Const Access = "Text Only"

Const FAQ_RECIPIENT_NAME = "ANP FAQ"
Const FAQ_RECIPIENT_EMAIL = "anp.info@manchester-solutions.co.uk"

Dim CustomerID
	CustomerID = Session("svCustomerID")

Const SITE_LOGIN_URL = "<a href=""/Login.asp"" title=""Login"">Login</a>"
Const SITE_REGISTER_URL = "<a href=""/Register.asp"" title=""Register"">Register</a>"
%>