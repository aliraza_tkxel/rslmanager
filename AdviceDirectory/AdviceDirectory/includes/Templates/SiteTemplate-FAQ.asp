﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	'Response.Buffer = false
	'Response.Expires=-1000
	'Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
If Customerid = "" Then Customerid = -1 End If
Call OpenDB()
SQL = "SELECT * FROM DBO.CUSTOMERID C INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID WHERE C.CUSTOMERID = " & Customerid
Call OpenRs(rsDetails,SQL)
If NOT rsDetails.EOF Then
    FIRSTNAME = rsDetails("FIRSTNAME")
    SURNAME = rsDetails("SURNAME")
    NAME = FIRSTNAME & " " & SURNAME
    EMAIL = rsDetails("EMAIL")
    POSTCODE = rsDetails("POSTCODE")
    CSS = " STYLE=""COLOR:GRAY; BORDER: SOLID 1PX GRAY"" "
    FIELD_STATUS = " READONLY=""READONLY"" "
End If
Call CloseRs(rsDetails)
Call CloseDB()

If Request("Send") <> "" Then

Dim txt_NAME	    ' FORM FIELD - NAME
Dim txt_EMAIL	    ' FORM FIELD - EMAIL
Dim txt_POSTCODE	' FORM FIELD - POSTCODE
Dim txt_QUESTION	' FORM FIELD - QUESTION
Dim rsEmail			' RECORDSET
Dim ErrorCode
Dim ErrorClass
Dim passcheck 		' EMAIL SENT YES/NO (REASON WHY)

	ErrorCode = 0
	txt_EMAIL = "xyz"
	passcheck = ""

    If (Request.Form("txt_EMAIL") <> "") Then

        txt_NAME = Request("txt_NAME")

	    If isValidEmail(Request.Form("txt_EMAIL")) = True Then
		    txt_EMAIL = Request.Form("txt_EMAIL")
	    Else
		    txt_EMAIL = isValidEmail(Request.Form("txt_EMAIL"))
	    End If

	    txt_POSTCODE = Request.Form("txt_POSTCODE")
	    txt_QUESTION = Request.Form("txt_QUESTION")

    End If

    'Dim myArray(4) 'Fixed size array
    'Dim i    
    'If Request.Form <> "" Then
    '    i = 0
    '	For Each Item In Request.Form
    '		myArray(i) = Item 'Request(Item)
    '		RW myArray(i)
    '		i = i + 1
    '	Next
     'End If

    strToName = FAQ_RECIPIENT_NAME
    strToEmail = FAQ_RECIPIENT_EMAIL
    strFromName = txt_NAME
    strFromEmail = txt_EMAIL
    strSubject = "FAQ - Ask a Question"
    strTextBody = "PostCode: " &  txt_POSTCODE & vbCrLf & "Question: " & txt_QUESTION
    strHTMLBody = null

	If SendEmail(strToName, strToEmail, strFromName, strFromEmail, strSubject, strTextBody, strHTMLBody) Then
		passcheck_text = passcheck_text & "Your email has been successfully sent. Thank you."
		ErrorClass = " class=""no_error"""
	Else
		passcheck_text = passcheck_text & "There was an error sending your email. Please try again."
		ErrorClass = " class=""er"""
	End If

End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--


-->
</style>

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="width:70%; float:left; margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<div style="width:30%; float:right; border:solid 1px #cc0000;">
			    <div id="nsform" style="padding-top:1em; padding-bottom:1em; padding-left:1em; padding-right:1em;">				
			        <h3>Ask a Question:</h3>
			        <div style="height:1px; background-color: #cc0000; margin-bottom:5px">&nbsp;</div>
			        <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
			        <form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">            
	                    <fieldset>
		                    <legend>Frequently Asked Questions - Ask as FAQ</legend>
	                            <div id="Frm" style="float:left; width:100%">
	                                <div class="row">
                                        <span class="label">
                                            <label for="txt_NAME" accesskey="N">
                                                <span style="text-decoration:underline; cursor:pointer">N</span>ame:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_NAME" id="txt_NAME" maxlength="100" value="<%=NAME%>" <%=CSS%> <%=FIELD_STATUS%> tabindex="1" required="Y" validate="/^([a-zA-Z0-9_\s-]{2,100})+$/" validateMsg="Name" />
                                            <img src="/js/FVS.gif" name="img_NAME" id="img_NAME" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_EMAIL" accesskey="E">
                                                <span style="text-decoration:underline; cursor:pointer">E</span>mail:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_EMAIL" id="txt_EMAIL" maxlength="250" value="<%=EMAIL%>" <%=CSS%> <%=FIELD_STATUS%> tabindex="1" required="Y" validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" validateMsg="Email" />
                                            <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_POSTCODE" accesskey="P">
                                                <span style="text-decoration:underline; cursor:pointer">P</span>ostcode:
                                            </label>
                                        </span>
                                        <span class="formw">
                                            <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="8" value="<%=POSTCODE%>" <%=CSS%> <%=FIELD_STATUS%> tabindex="1" required="Y" validate="/^[A-Za-z]{1,2}\d{1,2}[A-Za-z]? \d[A-Za-z]{2}$/" validateMsg="Postcode" />
                                            <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label">
                                            <label for="txt_QUESTION" accesskey="Q">
                                                <span style="text-decoration:underline; cursor:pointer">Q</span>uestion:
                                            </label>
                                        </span>
                                        <span class="formw">
                                        <textarea class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" cols="22" rows="15" name="txt_QUESTION" id="txt_QUESTION" required="Y" validate="/([a-zA-Z0-9,?!\s]{4,1000})$/" validateMsg="Question"> </textarea>
                                            <img src="/js/FVS.gif" name="img_QUESTION" id="img_QUESTION" width="15px" height="15px" alt="" />
                                        </span>
                                    </div>
                                    <div class="row">
                                        <span class="label"> </span>
                                        <span class="formw">
                                            <input class="submit" type="submit" name="Send" id="Send" value=" Send " title="Submit Button : Ask a Question" style="width:auto"/>
                                        </span>
                                    </div>
                                </div>
		                </fieldset>
                    </form>
			    </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>