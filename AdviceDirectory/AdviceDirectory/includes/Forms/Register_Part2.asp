﻿<%
PartNo = 2

Call DebugRquests()

If Request.Form("Back") <> "" Then
rw "ccc"
    Call GetFormFields_Part2()
    Response.Redirect "/Register.asp"   
End If

Call OpenDB()
Call BuildSelect(STR_SECURITY_QUESTION,"sel_SECURITY_QUESTION","SECURITY_QUESTION","ID,DESCRIPTION","DESCRIPTION","Please Select",intID,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Security Question"" ")
Call CloseDB()
%>
<div id="Div1" style="text-align:right; background-color: #cc0000; color:White; border:solid 1px #cc0000; border-bottom:0px; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
    <h3>Step <%=PartNo%> of 2</h3>
</div>
<div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
    <div id="errordiv"></div> 
    <form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
    <div id="Part1">
		<fieldset>
		    <legend>Register Part 2</legend>
			            <div id="Div2" style="float:left; width:48%;">
			            <p class="H4">
			            -
			            </p>
                            <div class="row">
                                <span class="label"> <label for="TXT_USERNAME" accesskey="U"><span style="text-decoration:underline">U</span>sername:</label> </span>
                                <span class="formw">
                                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="TXT_USERNAME" id="TXT_USERNAME" maxlength="100" value="" tabindex="1" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Username" />
                                    <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label"> <label for="txt_PASSWORD" accesskey="P"><span style="text-decoration:underline">P</span>assword:</label> </span>
                                <span class="formw">
                                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="password" size="40" name="txt_PASSWORD" id="txt_PASSWORD" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Password" />
                                    <img src="/js/FVS.gif" name="img_PASSWORD" id="img_PASSWORD" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label"> <label for="txt_RETYPEPASSWORD" accesskey="R"><span style="text-decoration:underline">R</span>etype Password:</label> </span>
                                <span class="formw">
                                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="password" size="40" name="txt_RETYPEPASSWORD" id="txt_RETYPEPASSWORD" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Retype Password" />
                                    <img src="/js/FVS.gif" name="img_RETYPEPASSWORD" id="img1" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label"> <label for="sel_STR_SECURITY_QUESTION" accesskey="S"><span style="text-decoration:underline">S</span>ecurity question:</label> </span>
                                <span class="formw">
                                    <%=STR_SECURITY_QUESTION%>
                                    <img src="/js/FVS.gif" name="img_STR_SECURITY_QUESTION" id="img_STR_SECURITY_QUESTION" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label"> <label for="txt_ANSWER" accesskey="R"><span style="text-decoration:underline">A</span>nswer:</label> </span>
                                <span class="formw">
                                    <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ANSWER" id="txt_ANSWER" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Answer" />
                                    <img src="/js/FVS.gif" name="img_ANSWER" id="img2" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label"> <label for="sel_DPA_AWARE" accesskey="D"><span style="text-decoration:underline">D</span>PA aware:</label> </span>
                                <span class="formw">
                                    <input class="border-white" type="checkbox" name="DPA_AWARE" id="DPA_AWARE" style="width:auto" onclick="clickFunction()" />
                                    <input type="hidden" value="" id="hid_DPA_AWARE" name="hid_DPA_AWARE" required="Y" validate="/[1]/" validateMsg="DPA" /> <img src="/js/FVS.gif" name="img_DPA_AWARE" id="img_DPA_AWARE" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                            <div class="row">
                                <span class="label"> </span>
                                <span class="formw">
                                    <input class="submit" type="submit" name="Back" id="Back" value=" Back " title="Submit Button : Back" style="width:auto"/>
                                    <input class="submit" type="submit" name="Save" id="Save" value=" Save " title="Submit Button : Save" style="width:auto"/>
                                </span>
                            </div>
                        </div> 
		</fieldset>
		</div>	
    </form>
<br />
Don't have an email address? Click here <a href="/?.asp" title="Don't have an email address? Click here">Click here</a>
</div>