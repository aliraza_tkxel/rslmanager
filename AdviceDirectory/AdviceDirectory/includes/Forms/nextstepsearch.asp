﻿<%
Function isreq_null(str)
	If isnull(str) Or str = "" Then
		str = -1
	End If
	isreq_null = str
End Function

If postcodemandatory <> "true" Then
	postcodemandatory = "false"
End If

Dim cmd, param
Call OpenDB()

    Set rsLang = server.CreateObject("ADODB.Recordset")
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "STP_LANGAUAGES"
      set .ActiveConnection=conn
      set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
      .parameters.append param
      Set rsLang = .execute
    end with

    Call BuildSelect_SP(strLang,"selLang",rsLang,"Please Select","","width:280px","portal_search_select","")


    Set cmd=server.CreateObject("ADODB.Command")
    Set rsAccess = server.CreateObject("ADODB.Recordset")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "STP_ACCESSIBILITY"
      set .ActiveConnection=conn
      set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
      .parameters.append param
      set rsAccess = .execute 
    end with

    Call BuildSelect_SP(strAccess,"selAccess",rsAccess,"Please Select","","width:280px","portal_search_select","")

    Set cmd=server.CreateObject("ADODB.Command")
    Set rsServiceType = server.CreateObject("ADODB.Recordset")
    With cmd
          .CommandType=adcmdstoredproc
          .CommandText = "STP_ASSESSMENT_OPTIONS_ACTIVE" '-- TBL_MD_SERVICETYPE
          set .ActiveConnection=conn
          set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
          .parameters.append param
          Set rsServiceType = .execute
    end with

    Call BuildSelect_SP(strServiceType,"selServiceType",rsServiceType,"Please Select ","","width:280px","portal_search_select","")

    Set cmd=server.CreateObject("ADODB.Command")
    Set rsProviderType = server.CreateObject("ADODB.Recordset")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "STP_PROVIDER_TYPE"
      set .ActiveConnection=conn
      set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
      .parameters.append param
      set rsProviderType = .execute
    end with

    Call BuildSelect_SP(strProviderType,"selProviderType",rsProviderType,"Please Select","","width:280px","portal_search_select","")

Call CloseDB()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Search</title>
    <style type="text/css">
        #map
        {
	        height: 500px;
	        width: 500px;
	        display:block;
        }
    </style>

<script type="text/javascript">

var postcodemandatory = <%=postcodemandatory%>

function ToggleServices()
{ 
    if (document.getElementById("servicelist").style.display == "none")
    {
        document.getElementById("servicelist").style.display = "block";
        document.getElementById("btnSelectServices").value = "Click to Hide Services"
    }
    else
    {
        document.getElementById("servicelist").style.display = "none";
        document.getElementById("btnSelectServices").value = "Click to Choose Services"
    }
}

function ToggleTargets()
{
    if (document.getElementById("TargetList").style.display == "none")
    {
        document.getElementById("TargetList").style.display = "block";
        document.getElementById("btnSelectTarget").value = "Click to Hide Client Groups"
    }
    else
    {
        document.getElementById("TargetList").style.display = "none";
        document.getElementById("btnSelectTarget").value = "Click to Choose Client Groups"
    }
}

function ToggleAreas()
{ 
    if (document.getElementById("arealist").style.display == "none")
    {
        document.getElementById("arealist").style.display = "block";
        document.getElementById("btnSelectAreas").value = "Click to Hide Areas"
    }
    else
    {
        document.getElementById("arealist").style.display = "none";
        document.getElementById("btnSelectAreas").value = "Click to Choose Areas"
    }
}

function validate_search()
{
	if (postcodemandatory == false)
	{
		if (document.getElementById("txtPostcode").value == '')
		{
		    alert("You must enter a postcode")
		    return false;
		}
	}
	else
	{
		if (document.getElementById("txtPostcode").value == '')
		{
		    alert("You must enter a postcode")
		    return false;
		}
	}
	if (document.getElementById("txtPostcode").value != "")
	{
	    return isUKPostCode2 ("txtPostcode", "You must enter a valid postcode");
	    PCode();
	}
}

function submitform()
{
	var pageid = <%=isreq_null(request("currentpage"))%>
	if (pageid >= 0)
	{
	    document.hid_form.submit();
	}
}

window.onload = submitform;

</script>
<script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script type="text/javascript" language="javascript" src="/ClientScripting/gmap.js"></script>

    <script type="text/javascript" language="javascript">

    function PCode(){
        document.getElementById("BtnPCcodeSubmit").disabled = true;
        if (document.getElementById("txtPostcode").value != "")
	    {
	        return isUKPostCode2 ("txtPostcode", "You must enter a valid postcode")
	    }
	    else {
	        document.getElementById("map").style.display = "none";
	        document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
	    }
    }

    function isUKPostCode2 (itemName, errName){
	    elVal = document.getElementById(itemName).value;
	    elVal = TrimAll(elVal.toUpperCase());
	    result = elVal.match(/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) [0-9][ABD-HJLNP-UW-Z]{2})$/g);
	    if (!result) {
		    targetError(itemName,"red");
		    document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
		    document.getElementById("map").style.display = "none"
		    return false;
		    }
	    else {
		    elVal = String(elVal.replace(/\s/g,""));
		    elValLen = elVal.length;
		    elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		    document.getElementById(itemName).value = elVal;
		    targetError(itemName,"black");
		    //document.getElementById("map").style.display = "block";
		    usePointFromPostcode(document.getElementById('txtPostcode').value, setLatLng);
		    return true;
		    }
	    }

    function TrimAll(str) {
	    return LTrimAll(RTrimAll(str));
	    }

    function LTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	    return str.substring(i,str.length);
	    }

    function RTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	    return str.substring(0,i+1);
	    }

    function targetError(iItem, iColor){
        if (iColor == 'red'){
           if (document.createTextNode){
                if (document.getElementById("dv"+iItem).childNodes.length >= 1){
                    document.getElementById("dv"+iItem).removeChild(document.getElementById("dv"+iItem).firstChild)
                }
            var mytext=document.createTextNode("Sorry! This is not a valid postcode")
            document.getElementById("dv"+iItem).appendChild(mytext)
            }
        }
        else {
            var label=document.getElementById("dv"+iItem);
            if (label != null)
            label.innerHTML = ""
        }
	document.getElementById(iItem).style.color = iColor
	document.getElementById(iItem).focus();
	}

    function EnableBtn() {
        document.getElementById("BtnPCcodeSubmit").disabled = false;
    }

    function DisableBtn(BtnId) {
        document.getElementById(BtnId).disabled = true;
    }
    </script>
</head>
<body>
    <div id="maincontent">
        <div id="portal_search_content">
            <div class="membersheader">Find your nearest provider</div>
            <div class="memberssearchbody">
                <form action="/membersarea/searchresult.asp" target="searchresult" onsubmit="javascript:return validate_search(this)">
                    <p>
                        <label for="name" class="lblsearch_wider">
                            <input type="hidden" value="isform" name="hid_isForm" />
                            Your Full Postcode:</label>
                        <input type="text" class="portal_search_postcode" name="txtPostcode" id="txtPostcode" onchange="PCode()" />
                        <input type="hidden" id="hiddenLng" name="hiddenLng" />
                        <input type="hidden" id="hiddenLat" name="hiddenLat" />
                        <span id="dvtxtPostcode" style="color:Red; font-weight:bold"></span>
                    </p>
                    <% If nextstepfilter = False Then %>
                    <p>
                        <label for="selServiceType" class="lblsearch_wider">
                            Type of Service:</label>
                        <%=strServiceType%>
                    </p>
                    <% Else %>
                        <input id="selServiceType" name="selServiceType" type="hidden" value="4" />
                    <% End If %>
                    <br />
                    Does the organisation provide for:<br />
                    <br />
                    <p>
                        <label for="selAccess" class="lblsearch_wider">
                            Accessibility:</label>
                        <%=strAccess%>
                        <br />
                    </p>
                    <p>
                        <label for="selLang" class="lblsearch_wider">
                            Language/Interpreted Support:</label>
                        <%=strLang%>
                        <br />
                    </p>
                    <p class="submit">
                        <input type="submit" id="BtnPCcodeSubmit" name="BtnPCcodeSubmit" value="Find my nearest provider" style="cursor: pointer;" disabled="disabled" />
                    </p>
                    <div id="map"></div>
                </form>
                <div id="searchresult">
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
    <iframe src="/default.asp" name="searchresult" width="600" height="600" style="display: none">
    </iframe>
    <form action="/membersarea/searchresult.asp" name="hid_form" target="searchresult">
        <input type="hidden" value="<%=request("currentpage")%>" name="page" />
    </form>
</body>
</html>
