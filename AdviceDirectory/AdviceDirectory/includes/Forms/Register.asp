﻿<%
PartNo = 1

Call OpenDB()
Call BuildSelect(STR_HEAR,"sel_HEAR","HEAR_ABOUT_US","ID,DESCRIPTION","DESCRIPTION","Please Select",Session("HEAR"),Null,Null," required=""N"" validate=""/([0-9])$/""  validateMsg=""Hear about us"" ") 
Call CloseDB()

%>
<div id="Div1" style="text-align:right; background-color: #cc0000; color:White; border:solid 1px #cc0000; border-bottom:0px; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
    <h3>Step <%=PartNo%> of 2</h3>
</div>
<div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
    <div id="errordiv"></div> 
    <form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
    <div id="Part1">
	  <fieldset>
		    <legend>Register Part 1</legend>
			            <p class="H4">
			            To Create your own Greater Manchester Advancment Network account, please complete the following details:
			            </p>
			            <div id="Frm" style="float:left; width:48%;">
                            <div class="row">
                                <span class="label"> <label for="txt_FIRSTNAME" accesskey="F"><span style="text-decoration:underline">F</span>irstname:</label> </span>
                                <span class="formw">
                                    <input value="<%=Session("FIRSTNAME")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="100" value="" tabindex="1" required="Y" validate="/([a-zA-Z0-9,\s]{1,250})$/" validateMsg="Firstname" />
                                    <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label"> <label for="txt_SURNAME" accesskey="P"><span style="text-decoration:underline">S</span>urname:</label> </span>
                                <span class="formw">
                                    <input value="<%=Session("SURNAME")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_SURNAME" id="txt_SURNAME" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{1,250})$/" validateMsg="Surname" />
                                    <img src="/js/FVS.gif" name="img_SURNAME" id="img_SURNAME" width="15px" height="15px" alt="" />
                                </span>
                            </div>
                            <div class="row">
                                <span class="label"> <label for="txt_HOUSENUMBER" accesskey="H"><span style="text-decoration:underline">H</span>ouse Number:</label> </span>
                                <span class="formw">
                                    <input value="<%=Session("HOUSENUMBER")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_HOUSENUMBER" id="txt_HOUSENUMBER" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{1,20})$/" validateMsg="House Number" />
                                    <img src="/js/FVS.gif" name="img_HOUSENUMBER" id="img_HOUSENUMBER" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label"> <label for="txt_POSTCODE" accesskey="P"><span style="text-decoration:underline">P</span>ostcode:</label> </span>
                                <span class="formw">
                                    <input value="<%=Session("POSTCODE")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{4,6})$/" validateMsg="Postcode" />
                                    <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label"> <label for="txt_EMAIL" accesskey="E"><span style="text-decoration:underline">E</span>mail:</label> </span>
                                <span class="formw">
                                    <input value="<%=Session("EMAIL")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_EMAIL" id="txt_EMAIL" maxlength="250" value="" required="Y" validate="/([a-zA-Z0-9,\s]{1,250})$/" validateMsg="Email" />
                                    <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label"> <label for="txt_TELEPHONE" accesskey="T"><span style="text-decoration:underline">T</span>elephone:</label> </span>
                                <span class="formw">
                                    <input value="<%=Session("TELEPHONE")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_TELEPHONE" id="txt_TELEPHONE" maxlength="20" value="" required="Y" validate="/([a-zA-Z0-9,\s]{1,20})$/" validateMsg="Telephone" />
                                    <img src="/js/FVS.gif" name="img_TELEPHONE" id="img_TELEPHONE" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label"> <label for="sel_HEAR" accesskey="H"><span style="text-decoration:underline">H</span>ear:</label> </span>
                                <span class="formw">
                                    <%=STR_HEAR%>
                                    <img src="/js/FVS.gif" name="img_HEAR" id="img_HEAR" width="15px" height="15px" alt="" />
                                </span>
                           </div>
                           <div class="row">
                                <span class="label"> </span>
                                <span class="formw">
                                    <input class="submit" type="submit" name="Next" id="Next" value=" Next " title="Submit Button : Next" style="width:auto"/>
                                </span>
                           </div>
                        </div>
		</fieldset>
		</div>	
    </form>
<br />
Don't have an email address? Click here <a href="/?.asp" title="Don't have an email address? Click here">Click here</a>
</div>
<%
Call DebugRquests()
rw Session("FIRSTNAME")
If Request.Form("Next") <> "" Then
rw "xxx"
    Session("FIRSTNAME") = Request.Form("txt_FIRSTNAME")
    rw Session("FIRSTNAME")
    'Call GetFormFields_Part1()
    Response.Redirect "/Register_part2.asp"   
End If
 %>