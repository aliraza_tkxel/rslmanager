  <!-- Start Logo And Banner Graphics //-->
	<div id="header">		
		<div id="headercontent">
			<div id="header-left"><a href="/"><img src="/images/im_nextsteplogo_small.gif" width="128" height="63" alt="next step logo and link to home page" title="next step logo and link to home page" /></a></div>
			<div id="header-right">
				<div id="header-logotext"><img src="/images/<%=WebLogo%>" alt="<%=nextstep_name%> advice on learning &amp; work in <%=nextstep_area%>" title="<%=nextstep_name%> advice on learning &amp; work in <%=nextstep_area%>" width="<%=WebLogoW%>" height="<%=WebLogoH%>" /></div>
				<div id="header-strip">
				<ul style="width:300px">
					<li><img src="/images/home_bar_anim_2.gif" width="57" height="57" alt="graphic : animation" /></li>
					<li><img src="/images/home_bar_box2.gif" width="57" height="57" alt="graphic" /></li>
					<li><img src="/images/anim_boxes_02_57.gif" width="57" height="57" alt="graphic : animation" /></li>
					<li><img src="/images/home_bar_box1.gif" width="57" height="57" alt="graphic" /></li>
				</ul>
				</div>			
			</div>
		</div>		
	</div>
	<!-- End Logo And Banner Graphics //-->