  <!-- Start Logo And Banner Graphics //-->
	<div id="header">
		<div id="headercontent">
			<div id="header-left"><a href="/"><img src="/images/bha_logo.gif" width="77" height="65" alt="Broadland Housing" title="Broadland Housing" /></a></div>
			<div id="header-right">
				<% If NullTest(Session("svLOGINID")) or NullTest(Session("svCustomerID")) Then %>
                    <div style="float:right">
                        <div style="float:right">
                            <div>Logged in as : <%=Session("svUsername")%></div>
                            <% If (NullNestTF(Session("svLOGINID")) = False) Then %>
                            <div>Organisation : <%=Session("svPROVIDERNAME")%></div>
                            <% End If %>
                            <div>Last Logged in : <%=Session("svLastLoggedIn")%></div>
                        </div>
                    </div>
                <% Else %>
                    <a href="/SelfAssessment.asp"><img id="Header_Banner" name="Header_Banner" src="/images/directory_icon.gif" width="68" height="102" alt="Broadland Housing : Self Assessment" /></a>
			    <% End If %>
			</div>
		</div>
	</div>
	<!-- End Logo And Banner Graphics //-->