<%
Function FormatMyDate(dtmDate)
FormatMyDate = "NAD"
If IsDate(dtmDate) Then
Dim arData(2)
arData(0) = Right("00" & Month(dtmDate),2)
arData(1) = Right("00" & Day(dtmDate),2)
arData(2) = Year(dtmDate)
FormatMyDate = Join(arData,"/")
End If
End Function

' Regular Expression to remove unwanted stuff from any string
Function makesafe(strInput)
	Dim objRegExp, strOutput
	Set objRegExp = New Regexp
	objRegExp.IgnoreCase = True
	objRegExp.Global = True
	objRegExp.Pattern = "[^A-Z^a-z^0-9\s]"
	makesafe = objRegExp.Replace(strInput, "")
End Function

'Pass the name of the file to the function.
Function getFileContents(strIncludeFile)

    myFileName = "/includes/Forms"&strIncludeFile

	Dim objFSO
	Dim objText
	Dim strPage
  	Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
  	
  	If (objFSO.FileExists(Server.MapPath(myFileName))) then
    'yes, it does exist
        Set objText = objFSO.OpenTextFile(Server.MapPath(myFileName))
            getFileContents = objText.ReadAll
            getFileContents = Replace(getFileContents,"[$$]",strIncludeFile)
            objText.Close
        Set objText = Nothing
    Else
    'no, it does not exist
        getFileContents = ""
    End If
  	
    '  	Set objText = objFSO.OpenTextFile(Server.MapPath("/includes/Forms"&strIncludeFile))
    '		getFileContents = objText.ReadAll
    '  	'Create Regular Expression Object
    ' 		getFileContents = Replace(getFileContents,"[$$]",strIncludeFile)
    '  	  objText.Close
    '  Set objText = Nothing

  Set objFSO = Nothing
End Function


Function characterPad(strWord,chCharacter,chLOR,intTotal)  	
Dim intNumChars 
	intNumChars = intTotal - len(strWord)  
	If intNumChars > 0 Then  
		If chLOR="l" Then  
			characterPad=string(intNumChars,chCharacter) & strWord  
		Else  
			characterPad=strWord & string(intNumChars,chCharacter)  
		End If  
	Else  
		characterPad = strWord  
	End If  
End Function
	
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function
	
'-- PORTAL FUNCTIONS
Function PageContent(GP_curPath)

  	SQL = "SELECT PAGE_TITLE, PAGE_CONTENT, PAGE_DESCRIPTION, PAGE_KEYWORDS FROM CMS_PAGES WHERE PAGE_NAME = '" & GP_curPath & "'"		
	Call OpenDB()
	Call OpenRs(rsPageContent, SQL)		
		If (rsPageContent.EOF) Then			 	
			PAGE_TITLE = "[Please add a title]"
			PAGE_CONTENT = "[Please add content]"
			PAGE_DESCRIPTION = ""
			PAGE_KEYWORDS = ""	
		Else	 ' USER EXISTS IN THE IAGNOW DATABASE
			PAGE_TITLE= rsPageContent(0)
			If NullNestTF(rsPageContent(1)) = True Then
			    PAGE_CONTENT = ""
			Else
			    PAGE_CONTENT = Replace(rsPageContent(1),"rel=""external""","")	
			End If
			PAGE_DESCRIPTION = rsPageContent(2)
			PAGE_KEYWORDS = rsPageContent(3)
		End If		
	Call CloseRS(rsPageContent)
	Call CloseDB()
	
	'Create Regular Expression Object
	Dim objRegExp
	Set objRegExp = New RegExp
		objRegExp.IgnoreCase = True
		objRegExp.Global = True
		objRegExp.Pattern = "<a\s+href=""http://(.*?)"">\s*((\n|.)+?)\s*</a>" ' will not match the external links delt with by javascript
		'objRegExp.Pattern = "<a\s+href=""http://(.*?)""\s+rel=""external"">\s*((\n|.)+?)\s*</a>"
	'customerid = 1
	If (IsNull(CustomerID) OR (CustomerID = "") OR (CustomerId = -1)) Then
	' title=""$2"" --this was commented out cos it was causing a problem with images. taken out on 25/01/08
		PAGE_CONTENT = objRegExp.Replace(PAGE_CONTENT, "<a href=""http://$1"">$2</a>")
	Else
	    PAGE_CONTENT = objRegExp.Replace(PAGE_CONTENT, "<a href=""http://$1"">$2</a>")
		'PAGE_CONTENT = objRegExp.Replace(PAGE_CONTENT, "<a href=""http://$1"" rel=""external"">$2</a> <a href=""JavaScript:SaveLink('http://$1')"" title=""Click here to save this link to your 'virtual nextstep centre'"">[SAVE]</a>")
	End If

End Function


Function TrimAll(Str)
'remove all non ASCI chrs and reduce internal whitespace to single
    Dim i, strTemp, strOut, strCh

    strTemp = Str
    For i = 1 To Len(strTemp)
        strCh = Mid(strTemp,i,1) 'look at each character in turn
        'if the chr is a space and the previous added chr was a space ignore it
        'otherwise add it on
        If Not (strCh = " " and Right(strOut,1) = " ") _
        And ((Asc(strCh) >= 64 And Asc(strCh) <= 122) _
        Or (Asc(strCh) >= 48 And Asc(strCh) <= 57) Or Asc(strCh) = 45 Or Asc(strCh) = 32 Or strCh = ".") Then
            strOut = strOut & strCh
        End If
    Next
    TrimAll = strOut
End Function

Function RegExpTrimAll(Str)

 'Remove extraneous spaces
  Dim regEx, Match, Matches
  Set regEx = New RegExp
	  regEx.Global = true
	  regEx.IgnoreCase = True
	  regEx.Pattern = "\s{1,}"
  		Str = Trim(regEx.Replace(str, ""))
  'Remove the legal characters
	  regEx.Pattern = "\&\!|\.|\?|\;|\,|\:|/"
	 	Str = regEx.Replace(Str, "")
	  
	RegExpTrimAll = Str

End function

Dim newstring
Function RegExpTrimAll_Version2(Str)

 'Remove extraneous spaces
  Dim regEx, Match, Matches
  Set regEx = New RegExp
	  regEx.Global = true
	  regEx.IgnoreCase = True
	  regEx.Pattern = "\s{1,}"
  		Str = Trim(regEx.Replace(str, "_"))
  'Remove the legal characters
	  regEx.Pattern = "\&\!|\.|\?|\;|\,|\:"
	 	Str = regEx.Replace(Str, "")
	  
	RegExpTrimAll_Version2 = Str
	newstring = RegExpTrimAll_Version2

End function

Function isValidEmail(myEmail)
  dim isValidE
  dim regEx
  
  isValidE = True
  set regEx = New RegExp
  
  regEx.IgnoreCase = False
  
  regEx.Pattern = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
  isValidE = regEx.Test(myEmail)
  
  isValidEmail = isValidE
End Function

Function isPassword(Str)
  dim isValidP
  dim regEx
  
  isValidP = True
  set regEx = New RegExp
  
  regEx.IgnoreCase = False
  
  regEx.Pattern = "(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$"
  isValidP = regEx.Test(Str)
  
  isPassword = isValidP
End Function

Function isUsername(Str)
  dim isValidU
  dim regEx
  
  isValidU = True
  set regEx = New RegExp
  
  regEx.IgnoreCase = False
  
  regEx.Pattern = "^[a-zA-Z0-9_\@\.\-]{6,70}$"
  isValidU = regEx.Test(Str)
  
  isUsername = isValidU
End Function

Function mySQL(val)
    'return a string with legal SQL characters
        Dim strTemp, Illegal, IntI
        	strTemp = val
    'URL unencode
    strTemp = Replace(strTemp, "%20", " ")
    strTemp = Replace(strTemp, "+", " ")
    strTemp = Replace(strTemp, "%257C", "|")
    'secure against script injection attacks
    'words/characters to exclude
        Illegal = Array("'", Chr(34), "--", VbCrlf, ";", "\", "/", _
                        "%", "*", "#", "?")
        'remove illegal words/characters
        For IntI = LBound(Illegal) To UBound(Illegal)
            If InStr(strTemp, Illegal(IntI)) Then
                strTemp = Replace(strTemp, Illegal(IntI), "")
            End If
        Next 
	mySQL = TrimAll(strTemp)
End Function

Sub BuildMenuByName(CatID, ulName, ulTitle)

	' CHECK STATUS OF THE MAIN MENU
	' EXIT IF NOT ACTIVE AND DO NOT DRAW THE MENU ITEMS OR THE SUB MENU ITEMS
	sql = "SELECT COUNT(1) FROM CMS_MENUS WHERE MENUID = "&CatID&" AND ACTIVE = 1 "
	Set Checkstatus = Server.CreateObject("ADODB.Recordset")
		Checkstatus.ActiveConnection = DSN_CONNECTION_STRING
		Checkstatus.Source = sql
		Checkstatus.CursorType = 0
		Checkstatus.CursorLocation = 2
		Checkstatus.LockType = 1
		Checkstatus.Open()
		
		If Checkstatus(0) = 0 Then
			Exit Sub
		End if	

	  sql = "SELECT MENUID as CatID, MENUTITLE as CatName, isInternal, ExternalURL " &_ 
			"FROM CMS_MENUS " &_ 
			"WHERE ACTIVE = 1 AND (PARENTMENUID IS NOT NULL AND PARENTMENUID = "&CatID&") " &_ 
			"ORDER BY RANK "

	Set Category = Server.CreateObject("ADODB.Recordset")
		Category.ActiveConnection = DSN_CONNECTION_STRING
		Category.Source = sql
		Category.CursorType = 0
		Category.CursorLocation = 2
		Category.LockType = 1
		Category.Open()
		
		If Not Category.EOF Or Not Category.BOF Then
			
			Response.Write "<ul id="""&ulName&""">"
			Response.Write "<li class='H3'>"&ulTitle&"</li>"
		
			While NOT Category.EOF
			
			If Category("isInternal") = 0 AND Category("ExternalURL")  <> "" Then
				linkage = Category("ExternalURL")
				external_attribute = """ rel=""external"" "
			Else
				linkage = RegExpTrimAll_Version2(Category("CatName")) & ".asp" &  """"
				external_attribute = ""	
			End If
			
				Response.Write Space(2) & "<li>" 
					Response.Write "<a href=""" & linkage & external_attribute & "" 
					Response.Write " title=""" & mySQL(Category("CatName")) & """"
					Response.Write ">" & mySQL(Category("CatName")) 
					Response.Write "</a>"
					Response.Write vbCRLF
			
			Category.MoveNext()
			 
			Response.Write Space(2) & "</li>" & vbCRLF			
		Wend	
			'Response.Write "<li><a href=""/SkillsForJobsIntro.asp"" class=""H"">Introduction</a></li>" & vbCRLF
			Response.Write "</ul>"
		End If
		
		Category.Close()
	Set Category = Nothing
	
End Sub


Sub listCat2(CatID, ulName)

	' CHECK STATUS OF THE MAIN MENU
	' EXIT IF NOT ACTIVE AND DO NOT DRAW THE MENU ITEMS OR THE SUB MENU ITEMS
	sql = "SELECT COUNT(1) FROM CMS_MENUS WHERE MENUID = "&CatID&" AND ACTIVE = 1 "
	Set Checkstatus = Server.CreateObject("ADODB.Recordset")
		Checkstatus.ActiveConnection = DSN_CONNECTION_STRING
		Checkstatus.Source = sql
		Checkstatus.CursorType = 0
		Checkstatus.CursorLocation = 2
		Checkstatus.LockType = 1
		Checkstatus.Open()
		
		If Checkstatus(0) = 0 Then
			Exit Sub
		End if	

	  sql = "SELECT MENUID as CatID, MENUTITLE as CatName, isInternal, ExternalURL " &_ 
			"FROM CMS_MENUS " &_ 
			"WHERE ACTIVE = 1 AND (PARENTMENUID IS NOT NULL AND PARENTMENUID = "&CatID&") " &_ 
			"ORDER BY RANK "
		  
	Set Category = Server.CreateObject("ADODB.Recordset")
		Category.ActiveConnection = DSN_CONNECTION_STRING
		Category.Source = sql
		Category.CursorType = 0
		Category.CursorLocation = 2
		Category.LockType = 1
		Category.Open()
		
		If Not Category.EOF Or Not Category.BOF Then
		
			While NOT Category.EOF
			
			If Category("isInternal") = 0 AND Category("ExternalURL")  <> "" Then
				linkage = Category("ExternalURL")
				external_attribute = """ rel=""external"" "
			Else
				linkage = "/" & dir & RegExpTrimAll("/" & Category("CatName")) & ".asp" &  """"
				external_attribute = ""	
			End If
			
				Response.Write Space(2) & "<li>" 
					Response.Write "<a href=""" & linkage & external_attribute & "" 
					Response.Write " title=""" & mySQL(Category("CatName")) & """"
					Response.Write ">" & mySQL(Category("CatName")) 
					Response.Write "</a>"
					Response.Write vbCRLF
			
			Call listSubCat2(Category("CatID"),1)
			Category.MoveNext()
			 
			Response.Write Space(2) & "</li>" & vbCRLF
		Wend	
			
			'Response.Write "</ul>" & vbCRLF
		End If
		
		Category.Close()
	Set Category = Nothing
	
End Sub


Sub listSubCat2(CatID, Counter)
	  
	  sql = "SELECT MENUID as CatID, MENUTITLE as CatName, isInternal, ExternalURL  " &_
			"FROM CMS_MENUS " &_
			"WHERE ACTIVE = 1 AND PARENTMENUID = " & CatID &_
			"ORDER BY RANK "

	Set SubCat = Server.CreateObject("ADODB.Recordset")
		SubCat.ActiveConnection = DSN_CONNECTION_STRING
		SubCat.Source = sql
		SubCat.CursorType = 0
		SubCat.CursorLocation = 2
		SubCat.LockType = 1
		SubCat.Open()
			
		If Not SubCat.EOF Or Not SubCat.BOF Then
		
		Response.Write Space(2+Counter) & "<ul>" & vbCRLF
		
			While NOT SubCat.EOF
			
			If SubCat("isInternal") = 0 AND SubCat("ExternalURL")  <> "" Then
				linkage = SubCat("ExternalURL") 
				'external_attribute = """ rel=""external"" "
				external_attribute = """ target=""_blank"" "
			Else
				linkage = "/" & dir & RegExpTrimAll("/" & SubCat("CatName")) & ".asp" &  """"
				external_attribute = ""	
			End If
			
				'Indented Response.Write indicates optional output
			Response.Write Space(4+Counter) & "<li>"	 
				Response.Write "<a href=""" & linkage & external_attribute & "" 
				Response.Write " title=""" & mySQL(SubCat("CatName")) & """"
				Response.Write ">"
				Response.Write mySQL(SubCat("CatName"))
				
				Counter = Counter + 4
				Call listSubCat(SubCat("CatID"), Counter)
				Counter = Counter - 4
				
				Response.Write "</a>"		
			Response.Write "</li>" & vbCRLF
		
			SubCat.MoveNext()
			Wend			
			
		Response.Write Space(2+Counter) & "</ul>" & vbCRLF
			
		End If ' end Not SubCat.EOF Or NOT SubCat.BOF 
		
		SubCat.Close()
	Set SubCat = Nothing
End Sub

			   
Sub listCatDIR(CatID, ulName, Directory)

    dir = Directory & "/"
    CustomerID = Session("svLOGINID")

	' CHECK STATUS OF THE MAIN MENU
	' EXIT IF NOT ACTIVE AND DO NOT DRAW THE MENU ITEMS OR THE SUB MENU ITEMS
	If CatID = 10 Then
	    EXTRA_SQL = " AND (LOGGEDIN = 2 OR LOGGEDIN IS NULL)"
	End If			
	
	sql = "SELECT COUNT(1) FROM CMS_MENUS WHERE MENUID = "&CatID&" AND ACTIVE = 1 " & EXTRA_SQL

	Set Checkstatus = Server.CreateObject("ADODB.Recordset")
		Checkstatus.ActiveConnection = DSN_CONNECTION_STRING
		Checkstatus.Source = sql
		Checkstatus.CursorType = 0
		Checkstatus.CursorLocation = 2
		Checkstatus.LockType = 1
		Checkstatus.Open()
		
		If Checkstatus(0) = 0 Then
			Exit Sub
		End if		
	

	If CatID = 404 Then
	    If (IsNull(CustomerID) OR (CustomerID = "") OR (CustomerId = -1) OR (CustomerID = Null)) Then
	        EXTRA_SQL = " AND (LOGGEDIN IS NULL OR LOGGEDIN = 2 OR LOGGEDIN = 0)"
	    Else
	        EXTRA_SQL = " AND (LOGGEDIN = 2 OR LOGGEDIN = 1)"
	    End If
	End If	


	  sql = "SELECT TM.MENUID as CatID, TM.MENUTITLE as CatName, TM.isInternal, TM.ExternalURL, TM.isLink, TP.FILE_NAME " &_ 
			"FROM CMS_MENUS TM " &_
			"LEFT JOIN CMS_PAGES TP ON TP.MENUID = TM.MENUID "  &_
			"WHERE ACTIVE = 1 AND (PARENTMENUID IS NOT NULL AND PARENTMENUID = "&CatID&") " & EXTRA_SQL & " " &_ 
			"ORDER BY RANK "

	Set Category = Server.CreateObject("ADODB.Recordset")
		Category.ActiveConnection = DSN_CONNECTION_STRING
		Category.Source = sql
		Category.CursorType = 0
		Category.CursorLocation = 2
		Category.LockType = 1
		Category.Open()
		
		If Not Category.EOF Or Not Category.BOF Then
				Response.Write "<ul id="""&ulName&"""" 
				Response.Write ">" & vbCRLF
		
			While NOT Category.EOF
				if Category("FILE_NAME") = "" or isnull(Category("FILE_NAME")) then
					ThisFileName = Category("CatName")
				else 
					ThisFileName = Category("FILE_NAME")
				end if
			'If it is a link to an external site then
			If Category("isInternal") = 0 AND Category("ExternalURL")  <> "" Then
				linkage = Category("ExternalURL")
				external_attribute = """ rel=""external"" "
			Else ' if it is a link to a page within the site
				linkage = "/" & dir & RegExpTrimAll(ThisFileName) & ".asp" &  """"
				external_attribute = ""	
			End If

			If Category("isLink") = 1 Then
				Response.Write Space(2) & "<li>"
					Response.Write "<a href=""" & linkage & external_attribute & ""
					Response.Write " title=""" & mySQL(Category("CatName")) & """"
					Response.Write ">" & mySQL(Category("CatName"))
					Response.Write "</a>"
					Response.Write vbCRLF
			Else			
				Response.Write Space(2) & "<li class=""MenuMore"">"
					Response.Write "<a href=""JavaScript://"">" & mySQL(Category("CatName")) & "</a>"
					Response.Write vbCRLF
			End If

			Call listSubCat(Category("CatID"),1)
			Category.MoveNext()

			rw Space(2) & "</li>" & vbCRLF

		Wend

			'-- MAIN MENU CONDITIONAL STATEMENT
			'If CatID = 10 Then
			'    If (IsNull(CustomerID) OR (CustomerID = "") OR (CustomerId = -1) OR (CustomerID = Null)) Then
			'		rw "<li><a href=""/Login.asp"" title=""Register / Login"" class=""Reg"">Register / Login</a></li>"
			'	Else
			'		rw "<li><a href=""/Logout.asp"" title=""Logout"" class=""Reg"">Logout</a></li>"
			'	End If
			'End If

			rw "</ul>" & vbCRLF

		End If

		Category.Close()
	Set Category = Nothing
	
End Sub


Sub listCat(CatID, ulName)

    CustomerID = Session("svCustomerID")

	' CHECK STATUS OF THE MAIN MENU
	' EXIT IF NOT ACTIVE AND DO NOT DRAW THE MENU ITEMS OR THE SUB MENU ITEMS
	If CatID = 10 Then
	    'If (IsNull(CustomerID) OR (CustomerID = "") OR (CustomerId = -1) OR (CustomerID = Null)) Then
	        'EXTRA_SQL = " AND (LOGGEDIN IS NULL OR LOGGEDIN = 2 OR LOGGEDIN = 0)"
	        'Else
	        EXTRA_SQL = " AND (LOGGEDIN = 2 OR LOGGEDIN IS NULL)"
	    'End If
	End If			
	
	sql = "SELECT COUNT(1) FROM CMS_MENUS WHERE MENUID = "&CatID&" AND ACTIVE = 1 " & EXTRA_SQL

	Set Checkstatus = Server.CreateObject("ADODB.Recordset")
		Checkstatus.ActiveConnection = DSN_CONNECTION_STRING
		Checkstatus.Source = sql
		Checkstatus.CursorType = 0
		Checkstatus.CursorLocation = 2
		Checkstatus.LockType = 1
		Checkstatus.Open()
		
		If Checkstatus(0) = 0 Then
			Exit Sub
		End if		
	

	If CatID = 10 Then
	    If (IsNull(CustomerID) OR (CustomerID = "") OR (CustomerId = -1) OR (CustomerID = Null)) Then
	        EXTRA_SQL = " AND (LOGGEDIN IS NULL OR LOGGEDIN = 2 OR LOGGEDIN = 0)"
	    Else
	        EXTRA_SQL = " AND (LOGGEDIN = 2 OR LOGGEDIN = 1)"
	    End If
	End If	


	  sql = "SELECT TM.MENUID as CatID, TM.MENUTITLE as CatName, TM.isInternal, TM.ExternalURL, TM.isLink, TP.FILE_NAME " &_ 
			"FROM CMS_MENUS TM " &_
			"LEFT JOIN CMS_PAGES TP ON TP.MENUID = TM.MENUID "  &_
			"WHERE ACTIVE = 1 AND (PARENTMENUID IS NOT NULL AND PARENTMENUID = "&CatID&") " & EXTRA_SQL & " " &_ 
			"ORDER BY RANK "

	Set Category = Server.CreateObject("ADODB.Recordset")
		Category.ActiveConnection = DSN_CONNECTION_STRING
		Category.Source = sql
		Category.CursorType = 0
		Category.CursorLocation = 2
		Category.LockType = 1
		Category.Open()
		
		If Not Category.EOF Or Not Category.BOF Then
				Response.Write "<ul id="""&ulName&"""" 
				Response.Write ">" & vbCRLF
		
			While NOT Category.EOF
				if Category("FILE_NAME") = "" or isnull(Category("FILE_NAME")) then
					ThisFileName = Category("CatName")
				else 
					ThisFileName = Category("FILE_NAME")
				end if
			'If it is a link to an external site then
			If Category("isInternal") = 0 AND Category("ExternalURL")  <> "" Then
				linkage = Category("ExternalURL")
				external_attribute = """ rel=""external"" "
			Else ' if it is a link to a page within the site
				linkage = "/" & dir & RegExpTrimAll(ThisFileName) & ".asp" &  """"
				external_attribute = ""	
			End If

			If Category("isLink") = 1 Then
				Response.Write Space(2) & "<li>"
					Response.Write "<a href=""" & linkage & external_attribute & ""
					Response.Write " title=""" & mySQL(Category("CatName")) & """"
					Response.Write ">" & mySQL(Category("CatName"))
					Response.Write "</a>"
					Response.Write vbCRLF
			Else			
				Response.Write Space(2) & "<li class=""MenuMore"">"
					Response.Write "<a href=""JavaScript://"">" & mySQL(Category("CatName")) & "</a>"
					Response.Write vbCRLF
			End If

			Call listSubCat(Category("CatID"),1)
			Category.MoveNext()

			rw Space(2) & "</li>" & vbCRLF

		Wend

			'-- MAIN MENU CONDITIONAL STATEMENT
			If CatID = 10 Then
			'    If (IsNull(CustomerID) OR (CustomerID = "") OR (CustomerId = -1) OR (CustomerID = Null)) Then
			'		rw "<li><a href=""/Login.asp"" title=""Register / Login"" class=""Reg"">Register / Login</a></li>"
			'	Else
			'		rw "<li><a href=""/Logout.asp"" title=""Logout"" class=""Reg"">Logout</a></li>"
			'	End If			    
			'       rw "<li id=""FULLSITE""><a href=""#"" onclick=""setActiveStyleSheet('default'); return false;"">Full Site</a></li>" & vbCRLF
			 rw "<li><a href=""#"" onclick=""JavaScript:self.close()"">Close Window</a></li>" & vbCRLF
			End If			

			rw "</ul>" & vbCRLF

		End If

		Category.Close()
	Set Category = Nothing
	
End Sub


Sub listSubCat(CatID, Counter)
	  
	  sql = "SELECT TM.MENUID as CatID, TM.MENUTITLE as CatName, TM.isInternal, TM.ExternalURL, TM.isLink, TP.FILE_NAME  " &_
			"FROM CMS_MENUS TM " &_
			"LEFT JOIN CMS_PAGES TP ON TP.MENUID = TM.MENUID "  &_
			"WHERE ACTIVE = 1 AND PARENTMENUID = " & CatID &_
			"ORDER BY RANK "

	Set SubCat = Server.CreateObject("ADODB.Recordset")
		SubCat.ActiveConnection = DSN_CONNECTION_STRING
		SubCat.Source = sql
		SubCat.CursorType = 0
		SubCat.CursorLocation = 2
		SubCat.LockType = 1
		SubCat.Open()
			
		If Not SubCat.EOF Or Not SubCat.BOF Then
		
		Response.Write Space(2+Counter) & "<ul>" & vbCRLF
		
			While NOT SubCat.EOF
			
				if SubCat("FILE_NAME") = "" or isnull(SubCat("FILE_NAME")) then
					ThisFileName = SubCat("CatName")
				else 
					ThisFileName = SubCat("FILE_NAME")
				end if
			
			If SubCat("isInternal") = 0 AND SubCat("ExternalURL")  <> "" Then
				linkage = SubCat("ExternalURL") 
				external_attribute = """ rel=""external"" "
			Else
				linkage = "/" & dir & RegExpTrimAll(ThisFileName) & ".asp" &  """"
				external_attribute = ""	
			End If
			
			If SubCat("isLink") = 1 then			
				Response.Write Space(2) & "<li>" 
				Response.Write "<a href=""" & linkage & external_attribute & "" 
				Response.Write " title=""" & mySQL(SubCat("CatName")) & """"
					Response.Write ">" & mySQL(SubCat("CatName")) 
				Response.Write "</a>"
					Response.Write vbCRLF					
			Else			
				Response.Write Space(2) & "<li class=""MenuMore"">" 
					Response.Write "<a href=""JavaScript://"">" & mySQL(SubCat("CatName")) & "</a>" 
					Response.Write vbCRLF					
			End If
			
				'Indented Response.Write indicates optional output
			'Response.Write Space(4+Counter) & "<li>"	 
			'	Response.Write "<a href=""" & linkage & external_attribute & "" 
			'	Response.Write " title=""" & mySQL(SubCat("CatName")) & """"
			'	Response.Write ">"
			'	Response.Write mySQL(SubCat("CatName"))				
			'	Response.Write "</a>"
				
				Counter = Counter + 4
				Call listSubCat(SubCat("CatID"), Counter)
				Counter = Counter - 4
					
			Response.Write "</li>" & vbCRLF
		
			SubCat.MoveNext()
			Wend			
			
		Response.Write Space(2+Counter) & "</ul>" & vbCRLF
			
		End If ' end Not SubCat.EOF Or NOT SubCat.BOF 
		
		SubCat.Close()
	Set SubCat = Nothing
End Sub


'-- REGISTERING FUNCTIONS

Function CheckRegistered()

	If (Session("RF_FIRSTNAME") <> "" AND Session("RF_SURNAME") <> "" AND Session("RF_DOB") <> "") then

		SQL =	"SELECT  	C.CUSTOMERID " &_
				"FROM      	CUSTOMERDETAILS CD " &_
				"INNER JOIN CUSTOMERID C ON CD.CUSTOMERID = C.CUSTOMERID " &_
	   			"WHERE     	((C.FIRSTNAME = '"&Session("RF_FIRSTNAME")&"' AND C.SURNAME = '"&Session("RF_SURNAME")&"' AND C.DOB = CONVERT(SMALLDATETIME,'"&Session("RF_DOB")&"',103)) OR (CD.EMAIL = '"&Session("RF_EMAIL")&"')) "
	    Call OpenRs(rsCheck, SQL)		
			If (rsCheck.EOF) Then	' USER IS NOT REGISTERED		
				Call CheckRecord()				
			Else	 				' USER IS REGISTERED OR DOESNT EXIST
				JS_OUT = 1
				'Session.Abandon
				Session("svCustomerID")	=	""
	            Session("svFirstName")	=	""
	            Session("svLastName")	=	""
	            Session("svEmail")		= 	""
	            Session("svName")		= 	""
			End If		
		Call CloseRs(rsCheck)
	Else	
		JS_OUT = 2		
	End If	
		
End Function


Function CheckRecord()

	If (Session("RF_FIRSTNAME") <> "" AND Session("RF_SURNAME") <> "" AND Session("RF_DOB") <> "") then

		SQL =	"SELECT  	C.CustomerID " &_
				"FROM      	CustomerID C " &_
	   			"WHERE     	((C.FIRSTNAME = '"&Session("RF_FIRSTNAME")&"') AND (C.SURNAME = '"&Session("RF_SURNAME")&"') AND (C.DOB = CONVERT(SMALLDATETIME,'"&Session("RF_DOB")&"',103))) "
		Call OpenRs(rsCheck, SQL)		
			If (rsCheck.EOF) Then				' USER DOES NOT EXIST IN DATABASE		
				Call NewRecord()				
			Else	 							' USER EXISTS IN DATABASE, THEREFORE UPDATE
			    JS_OUT = 1
				Session.Abandon
				'Call UpdateRecord(rsCheck(0))	' I.E. ADD USENAME AND PASSWORD
			End If	
		Call CloseRs(rsCheck)	
	Else	
		JS_OUT = 2		
	End If	
		
End Function


Function NewRecord()

    FIRSTNAME = nulltest(Session("RF_FIRSTNAME"))
    SURNAME = nulltest(Session("RF_SURNAME"))
    GENDER = null
    ETHNICITY = null

    DOB = Session("RF_DOB")
    'Response.Write DOB
    'Response.End()       
    If NullNestTF(DOB) = True Then
        DOB = CDate(Date())
    Else
        If IsDate(DOB) Then
            DOB = CDate(DOB)
        Else
            DOB = CDate(Date())
        End If
    End If

    DISABLED = null
    BENEFITRECIPIENT = null
    LONEPARENT = null
    EMPLOYMENTSTATUS = null
    LENGTHOFUNEMPLOYMENT = null
    NINUMBER = null
    ADDRESSLINE1 = nulltest(Session("RF_HOUSENUMBER"))
    ADDRESSLINE2 = null
    ADDRESSLINE3 = null
    LAA = null
    POSTCODE = nulltest(Session("RF_POSTCODE"))
    TELEPHONE = nulltest(Session("RF_TELEPHONE"))
    EMAIL = nulltest(Session("RF_EMAIL"))
    MOBILE = nulltest(Session("RF_MOBILE"))
    HEAR = nulltest(Session("RF_HEAR"))
    HEAR_OTHER = nulltest(Session("RF_HEAR_OTHER"))
    SECURITYQUESTION = nulltest(Request("sel_SECURITY_QUESTION"))
    SECURITYANSWER = nulltest(Request("txt_ANSWER"))
    USERNAME = nulltest(Request("txt_USERNAME"))
    PASSWORD = nulltest(Request("txt_PASSWORD"))

    Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_REGISTER"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@FIRSTNAME", adVarWChar, adParamInput, 40, FIRSTNAME)
                .Parameters.Append .createparameter("@SURNAME", adVarWChar, adParamInput, 40, SURNAME)
                .Parameters.Append .createparameter("@GENDER", adVarWChar, adParamInput, 1, GENDER)
                .Parameters.Append .createparameter("@ETHNICITY", adVarWChar, adParamInput, 2, ETHNICITY)
                .Parameters.Append .createparameter("@DOB", adDBTimeStamp, adParamInput, 0, DOB)
                .Parameters.Append .createparameter("@DISABLED", adInteger, adParamInput, 0, DISABLED)
                .Parameters.Append .createparameter("@BENEFITRECIPIENT", adInteger, adParamInput, 1, BENEFITRECIPIENT)
                .Parameters.Append .createparameter("@LONEPARENT", adInteger, adParamInput, 1, LONEPARENT)
                .Parameters.Append .createparameter("@EMPLOYMENTSTATUS", adVarWChar, adParamInput, 2, EMPLOYMENTSTATUS)
                .Parameters.Append .createparameter("@LENGTHOFUNEMPLOYMENT", adVarWChar, adParamInput, 2, LENGTHOFUNEMPLOYMENT)
                .Parameters.Append .createparameter("@NINUMBER", adVarWChar, adParamInput, 9, NINUMBER)
                .Parameters.Append .createparameter("@ADDRESSLINE1", adVarWChar, adParamInput, 100, ADDRESSLINE1)
                .Parameters.Append .createparameter("@ADDRESSLINE2", adVarWChar, adParamInput, 50, ADDRESSLINE2)
                .Parameters.Append .createparameter("@ADDRESSLINE3", adVarWChar, adParamInput, 50, ADDRESSLINE3)
                .Parameters.Append .createparameter("@LAA", adInteger, adParamInput, 0, LAA)
                .Parameters.Append .createparameter("@POSTCODE", adVarWChar, adParamInput, 8, POSTCODE)
                .Parameters.Append .createparameter("@TELEPHONE", adVarWChar, adParamInput, 20, TELEPHONE)
                .Parameters.Append .createparameter("@EMAIL", adVarWChar, adParamInput, 250, EMAIL)
                .Parameters.Append .createparameter("@MOBILE", adVarWChar, adParamInput, 20, MOBILE)                
                .Parameters.Append .createparameter("@HEAR", adInteger, adParamInput, 0, HEAR)
                .Parameters.Append .createparameter("@HEAR_OTHER", adVarWChar, adParamInput, 250, HEAR_OTHER)
                .Parameters.Append .createparameter("@SECURITYQUESTION", adInteger, adParamInput, 0, SECURITYQUESTION)
                .Parameters.Append .createparameter("@SECURITYANSWER", adVarWChar, adParamInput, 300, SECURITYANSWER)
                .Parameters.Append .createparameter("@USERNAME", adVarWChar, adParamInput, 250, USERNAME)
                .Parameters.Append .createparameter("@PASSWORD", adVarWChar, adParamInput, 50, PASSWORD)
                'Execute the function
			    .execute ,,adexecutenorecords
                isSaved = True
		    End With
	    objConn.Close
    Set objConn = Nothing

    intRetVal = CInt(objCmd.Parameters("@RETURN_VALUE").Value)
    '-------------------------------------------------------------------------------------- 
    ' SAVE ANY SELF ASSESSMENT STUFF AUTOMATICALLY WHETHER THEY LIKE IT OR NOT (AS IN SPEC)
    '--------------------------------------------------------------------------------------
    SaveRecommendations(intRetVal)

	JS_OUT = 3

	'Session.Abandon

End Function


Function JXIsoDate(dteDate)
'Version 1.0
   If IsDate(dteDate) = True Then
      DIM dteDay, dteMonth, dteYear
      dteDay = Day(dteDate)
      dteMonth = Month(dteDate)
      dteYear   = Year(dteDate)
      JXIsoDate = dteYear & _
         "-" & Right(Cstr(dteMonth + 100),2) & _
         "-" & Right(Cstr(dteDay + 100),2)
   Else
      JXIsoDate = Null
   End If
End Function

Function UpdateRecord(ID)
	Set rsCC = Server.CreateObject("ADODB.Recordset")
		rsCC.Open "SELECT USERNAME,PASSWORD FROM CUSTOMERID WHERE CUSTOMERID = " & ID, Conn, adopenstatic, adLockOptimistic	
		rsCC("Username") = USERNAME
		rsCC("Password") = PASSWORD
		rsCC.Update()
		rsCC.Close()
End Function

'-- REGISTERING FUNCTIONS

'-- SELF ASSESSMENT FUNCTIONS (SAVE RESULTS)

Function UpSert(CID,OSID,CSID)

'rw "Function UpSert" & "<br/>"
'rw "CustomerID" & CID & "<br/>"
'rw "OSID" & OSID & "<br/>"
'rw "CSID" & CSID & "<br/>"

    ' UPDATE PRIORITIES
    
    If Session("svP1") = "" or IsNull(Session("svP1")) Then
        Exit Function
    End If

    Dim myStrPL
    Dim PRIORITIES
    Dim ASSESSMENTOPTIONS

        myStrPL = Session("PriorityList")

'rw "myStrPL" & myStrPL & "<br/>"

    If myStrPL <> "" Then
        MyArray = split(myStrPL,"|")
        PRIORITIES = ""
        ASSESSMENTOPTIONS = ""
        For Each item In MyArray
            MyArray2 = split(item,"-")
            PRIORITIES = PRIORITIES & MyArray2(0) & ","
            ASSESSMENTOPTIONS = ASSESSMENTOPTIONS & MyArray2(1) & ","
        Next
    End If

    if Right(PRIORITIES, 1) = "," then  
        PRIORITIES = Left(PRIORITIES,Len(PRIORITIES)-1) 
    else
        PRIORITIES = PRIORITIES
    end If
    
    if Right(ASSESSMENTOPTIONS, 1) = "," then 
        ASSESSMENTOPTIONS = Left(ASSESSMENTOPTIONS,Len(ASSESSMENTOPTIONS)-1)
    else
        ASSESSMENTOPTIONS = ASSESSMENTOPTIONS
    end If
    
'rw "PRIORITIES" & PRIORITIES & "<br/>"
'rw "ASSESSMENTOPTIONS" & ASSESSMENTOPTIONS & "<br/>"

' this is the problem   
    Conn.execute "UPDATE CUSTOMERID_PRIORITIES SET CUSTOMERID = " & CID & " WHERE SESSIONID = '" & OSID & "'"
' this is the problem     

    Conn.execute "EXEC CRMS_CURRENTPRIORITIES_UPDATE " & CID & ",null,'" & ASSESSMENTOPTIONS & "','" & PRIORITIES & "'"

'rw "EXEC CRMS_CURRENTPRIORITIES_UPDATE " & CID & ",null,'" & ASSESSMENTOPTIONS & "','" & PRIORITIES & "'"

'rw " CUSTOMERID : " &  CID & "<br/>"
'rw "OLD SESSION : " & OSID & "<br/>"
'rw "NEW SESSION : " & CSID & "<br/>"

    ' DELETE OLD DATA THAT WAS SAVED (TEMP FOR PAGING)
    Conn.Execute "DELETE FROM TBL_MD_TMPSEARCH_RESULT WHERE SESSIONID = '" & OSID & "'"
    ' DELETE OLD DATA (SAVED TABLE)
    Conn.Execute "DELETE FROM TBL_MD_TMPSEARCH_RESULT_SAVED WHERE SESSIONID = '" & OSID & "'"
    ' INSERT NEW DATA (SAVED TABLE)
    
    Dim My_String
        My_String=Session("svOurRecommendations")
        
        If My_String <> "" Then
        My_Array=split(My_String,",")
        For Each item In My_Array
            My_Array2=split(item,"_")
            GP_curPath = "-"
            GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & My_Array2(1))
            If Mid(GP_curPath,Len(GP_curPath),1)  <> "-" then
                GP_curPath = GP_curPath
            End If
            ddArray = Split(GP_curPath,"-")
            conn.execute "INSERT INTO TBL_MD_TMPSEARCH_RESULT_SAVED (SESSIONID, SITEID, ASSESSMENTOPTIONID, PRIORITY) VALUES ('" & OSID & "'," & My_Array2(0) &"," & ddArray(0) & "," & Right(GP_curPath,1) & ")" 
        Next
        End If
    
    ' UPDATE OLD DATA TO NEW
    Conn.Execute "UPDATE TBL_MD_TMPSEARCH_RESULT SET SESSIONID = '" & OSID & "', CUSTOMERID = " & CID & " WHERE SESSIONID = '" & CSID & "'"

    passcheck_text = passcheck_text & " Your Self Assessment Recommendations have been saved."

    'Call CloseDB()
    
    'Response.End()

End Function


Function SaveRecommendations(CID)
'RW "SaveRecommendations"

If IsNull(CID) or CID = "" Then
    Exit Function
End If

Dim OldSessionID
Dim SessionID
    SessionID = Session("searchid")

Dim My_String
Dim My_Array
Dim My_Array2

    My_String=Session("svOurRecommendations")

'RW "REIDMARK SessionID: " & SessionID & "<br/>"
'RW "REIDMARK My_String: " & My_String & "<br/>"

    If (( Session("svOurRecommendations") = "" OR IsNull(Session("svOurRecommendations"))) OR ((SessionID = "" OR IsNull(SessionID)))) Then
'RW "REIDMARK Exit Function" & "<br/>"
        Exit Function
    Else

        Call OpenDB()
        SQL = "SELECT DISTINCT(SESSIONID) AS SESSIONID FROM CUSTOMERID_PRIORITIES WHERE CUSTOMERID = " & CID
'rw SQL & "<br/>"
        Call OpenRs(rs,SQL)
            If NOT rs.EOF Then
'rw "REIDMARK IN HERE"
                   Call UpSert(CID,rs("SESSIONID"),SessionID)
                   Exit Function
            End If
'       Call CloseDB

        Call SavePRIORITIES(CID,SessionID)

        Call OpenDB()
        SQL = "SELECT SESSIONID FROM TBL_MD_TMPSEARCH_RESULT_SAVED WHERE SESSIONID = '" & SessionID & "'"   
        Call OpenRs(rs,SQL)
            If rs.EOF Then
                If My_String <> "" Then
                    My_Array=split(My_String,",")
                    For Each item In My_Array
                        My_Array2=split(item,"_")
       	                GP_curPath = "-"
                        GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & My_Array2(1))
                        If Mid(GP_curPath,Len(GP_curPath),1)  <> "-" then
                            GP_curPath = GP_curPath
                        End If
                        ddArray = Split(GP_curPath,"-")
                        conn.execute "INSERT INTO TBL_MD_TMPSEARCH_RESULT_SAVED (SESSIONID, SITEID, ASSESSMENTOPTIONID, PRIORITY) VALUES ('" & SessionID & "'," & My_Array2(0) &"," & ddArray(0) & "," & Right(GP_curPath,1) & ")" 
                    Next

                    If Session("svCustomerID") = "" Then
                        CUST = CID
                    Else
                        CUST = Session("svCustomerID")
                    End If

                    If ((Session("svCustomerID") <> "") or (CID <> "")) Then
                        conn.execute "UPDATE TBL_MD_TMPSEARCH_RESULT SET CUSTOMERID = " & CID & " WHERE SESSIONID = '" & SessionID & "'"
                    End If

                    passcheck_text = passcheck_text & " Your Self Assessment Recommendations have been saved."

                End If
            Else
                Exit Function
            End If
        'Call CloseDB()

    End If
    
    Session("svOurRecommendations") = NULL

End Function


Function SavePRIORITIES(CID,SessionID)

'rw "REIDMARK SavePRIORITIES"

Dim LOGINID
    LOGINID = nulltest(Session("svLOGINID"))
    If IsNull(LOGINID) Then LOGINID = "NULL" End If

If Session("svP1") = "" or IsNull(Session("svP1")) Then
'rw "REIDMARK Exit Function"
    Exit Function
End If

    Dim myStrPL
    Dim PRIORITIES
    Dim ASSESSMENTOPTIONS

        myStrPL = Session("PriorityList")

    If myStrPL <> "" Then
        MyArray = split(myStrPL,"|")
        PRIORITIES = ""
        ASSESSMENTOPTIONS = ""
        For Each item In MyArray
            MyArray2 = split(item,"-")
            PRIORITIES = PRIORITIES & MyArray2(0) & ","
            ASSESSMENTOPTIONS = ASSESSMENTOPTIONS & MyArray2(1) & ","
        Next
    End If

        PRIORITIES = Left(PRIORITIES,Len(PRIORITIES)-1) 
        ASSESSMENTOPTIONS = Left(ASSESSMENTOPTIONS,Len(ASSESSMENTOPTIONS)-1)

'rw "REIDMARK PRIORITIES:" & PRIORITIES & "<br/>"
'rw "REIDMARK ASSESSMENTOPTIONS:" & ASSESSMENTOPTIONS & "<br/>"

    If Session("svCustomerID") = "" Then
        CUST = CID
    Else
        CUST = Session("svCustomerID")
    End If

Call OpenDB()
    SQL = "SELECT SESSIONID FROM TBL_MD_TMPSEARCH_RESULT_SAVED WHERE SESSIONID = '" & SessionID & "'"
    Call OpenRs(rs,SQL)
    If rs.EOF Then
        For Each item In MyArray
            MyArray2 = split(item,"-")
            conn.execute "INSERT INTO CUSTOMERID_PRIORITIES (SessionID, ASSESSMENTOPTIONID, PRIORITY, HISTORYID, LOGINID) VALUES ('" & SessionID & "'," & MyArray2(1) &"," & MyArray2(0) & ",1," & LOGINID & ")"
         Next
    Else
        Conn.execute "EXEC CRMS_CURRENTPRIORITIES_UPDATE " & CUST & ",null,'" & ASSESSMENTOPTIONS & "','" & PRIORITIES & "'"
        ' if session exist - update table
        ' if session does not exist insert into table
    End If

    If ((Session("svCustomerID") <> "") or (CID <> "")) Then
        conn.execute "UPDATE CUSTOMERID_PRIORITIES SET CUSTOMERID = " & CUST & " WHERE SESSIONID = '" & SessionID & "'"
    End If

'Call CloseDB()

'response.End()

End Function


'-- SELF ASSESSMENT FUNCTIONS (SAVE RESULTS)

'-- PASSWORD RECOVERY

Function SaveLoginHistory(ID)
Call OpenDB()
conn.execute "INSERT INTO LOGIN_HISTORY (CUSTOMERID) VALUES ("&ID&")"
Session("svLastLoggedIn") = FormatDateTime(Now(),1)
Call CloseDB()
End Function

Function SaveLoginHistoryMD(ID)
Call OpenDB()
conn.execute "INSERT INTO CRMS_LOGIN_HISTORY (LOGINID) VALUES ("&ID&")"
Session("svLastLoggedIn") = FormatDateTime(Now(),1)
Call CloseDB()
End Function

Function Login(strUsername, strPassword)
rw "dddddddddddddddddddd"
'--encLib.decryptpwd(rs(0))
	passcheck = 0
	passcheck_text = "<div style='padding:10px;'>"

	SQL = "SELECT CUSTOMERID.CUSTOMERID, FIRSTNAME, SURNAME, EMAIL, USERNAME, PASSWORD, FIRSTNAME+SPACE(1)+SURNAME As FULLNAME " &_
		  "FROM   CUSTOMERID, CUSTOMERDETAILS " &_
		  "WHERE  CUSTOMERID.CUSTOMERID = CUSTOMERDETAILS.CUSTOMERID AND " &_
		  "	USERNAME = '" + Replace(strUsername, "'", "''") + "'  AND " &_
		  "	PASSWORD = '" + Replace(strPassword, "'", "''") + "'"

	Call OpenDB()
	Call OpenRs(rsLogin,SQL)

		If Not rsLogin.EOF Then
			If strUsername <> "xyz" Then
				Dim strUser, strPass, intCompPass
					strUser = rsLogin.Fields.Item("USERNAME").Value
					strPass = rsLogin.Fields.Item("PASSWORD").Value
					intCompPass = StrComp(strPassword, StrPass)

				If intCompPass <> 0 Or strUser = "" Or strPass = "" Then
					passcheck = 0
				Else
						Session("svCustomerID")	=	rsLogin.Fields.Item("CUSTOMERID").Value
						Session("svFirstName")	=	rsLogin.Fields.Item("FIRSTNAME").Value
						Session("svLastName")	=	rsLogin.Fields.Item("SURNAME").Value
						Session("svEmail")		=	rsLogin.Fields.Item("EMAIL").Value
						Session("svName")		=	rsLogin.Fields.Item("FULLNAME").Value
						Session("svUsername")	=	strUser
						passcheck 				=	1
				End If
			End If
		Else
			passcheck = 0
		End If

		If passcheck = 0 Then
			passcheck_text = passcheck_text & "Your username/password combination is not recoginsed."
		Else
			If Session("svCustomerID") <> "" Then
			    passcheck_text = passcheck_text & "You have successfully logged into " & SITE_NAME & "."
			Else
			   passcheck = 0
			   passcheck_text = passcheck_text & "your browser is running in Private mode!"
			End If
		End If

	Call CloseRs(rsLogin)
	Call CloseDB()

	Call SaveRecommendations(Session("svCustomerID"))

		passcheck_text = passcheck_text & "</div>"

	If passcheck = 1 Then
	    Call SaveLoginHistory(Session("svCustomerID"))
	End If

End Function



Function LoginMD(strUsername, strPassword)

    Dim encLib
    Set encLib = Server.CreateObject("EncLib.EncDec")
        strPassword = Replace(strPassword, "'", "''")
        strPassword = encLib.encryptpwd(strPassword)

        passcheck = 0
        passcheck_text = "<div style='padding:10px;'>"

    SQL = "SELECT A.ACCESSID, L.LOGINID, FIRSTNAME, LASTNAME, FIRSTNAME+SPACE(1)+LASTNAME As FULLNAME, EMAIL, PASSWORD, p.PROVIDERID, PROVIDERNAME, LH.DATECREATED " &_
          "FROM DBO.TBL_MD_LOGIN L " &_
          "INNER JOIN TBL_MD_PROVIDER P ON L.PROVIDERID = P.PROVIDERID " &_
          "INNER JOIN TBL_MD_LOGIN_ACCESS A ON A.ACCESSID = L.ACCESSID " &_
          "LEFT JOIN (SELECT LOGINID, MAX(DATECREATED) AS DATECREATED FROM CRMS_LOGIN_HISTORY GROUP BY LOGINID) LH ON L.LOGINID = LH.LOGINID " &_
          "WHERE A.ACCESSID IN(1,2,3,4) AND ORGSTATUS = 1 AND " &_
          "	USERNAME = '" + Replace(strUsername, "'", "''") + "' AND " &_
		  "	PASSWORD = '" + strPassword + "'"

	Call OpenDB()
	Call OpenRs(rsLogin,SQL)

		If Not rsLogin.EOF Then
			If strUsername <> "xyz" Then
				Dim strUser, strPass, intCompPass
					strUser = rsLogin.Fields.Item("EMAIL").Value
					strPass = rsLogin.Fields.Item("PASSWORD").Value
					intCompPass = StrComp(strPassword, StrPass)

				If intCompPass <> 0 Or strUser = "" Or strPass = "" Then
					passcheck = 0
				Else
					Session("svLOGINID")	    = rsLogin.Fields.Item("LOGINID").Value
					Session("svFirstName")	    = rsLogin.Fields.Item("FIRSTNAME").Value
					Session("svLastName")	    = rsLogin.Fields.Item("LASTNAME").Value
					Session("svUsername")	    = rsLogin.Fields.Item("FULLNAME").Value
					Session("svEmail")		    = rsLogin.Fields.Item("EMAIL").Value
					Session("svName")		    = rsLogin.Fields.Item("FULLNAME").Value
					Session("svPROVIDERNAME")   = rsLogin.Fields.Item("PROVIDERNAME").Value
					Session("svPROVIDERID")     = rsLogin.Fields.Item("PROVIDERID").Value
					Session("svACCESS")         = rsLogin.Fields.Item("ACCESSID").Value
					Session("svLastLoggedIn")   = rsLogin.Fields.Item("DATECREATED").Value
					passcheck 				    = 1
				End If
			End If
		Else
			passcheck = 0
		End If
		
		If passcheck = 1 Then
            Call SaveLoginHistoryMD(Session("svLOGINID"))
        End If

		If passcheck = 0 Then
			passcheck_text = passcheck_text & "Your username/password combination is not recoginsed."
		Else
			If Session("svLOGINID") <> "" Then
			    passcheck_text = passcheck_text & "You have successfully logged into the CRMS for " & SITE_NAME
			    Response.Redirect "FindaClient.asp"
			Else
			   passcheck = 0 
			   passcheck_text = passcheck_text & "your browser is running in Private mode!"
			End If
		End If

		passcheck_text = passcheck_text & "</div>"

	Call CloseRs(rsLogin)
	Call CloseDB()

End Function


Function EmailOut(strEMAIL)

	' presumption only 1 email address present if no asumption then we must alter cod eto send all or stop re-registers

	Dim SiteLocation
	Dim strRecipient
	Dim strPassword
	Dim strEmailUsername
	Dim strUserEmail
	Dim strFullName

	ErrorCode = 1
	passcheck = 0

	SQL = "SELECT CD.EMAIL, C.PASSWORD, C.USERNAME, C.FIRSTNAME+SPACE(1)+C.SURNAME AS FULLNAME " &_
		  "FROM   CUSTOMERDETAILS CD " &_
		  "INNER JOIN CUSTOMERID C ON C.CUSTOMERID = CD.CUSTOMERID " &_
		  "WHERE CD.EMAIL = '" + Replace(strEMAIL, "'", "''") + "'" 

	Call OpenDB()
	Call OpenRs(rsEmail,SQL)

		If Not rsEmail.EOF Then					
			SiteLocation = SiteURL
			strRecipient = rsEmail(0)
			strPassword = rsEmail(1)
			strUserName = rsEmail(2)
			strFullName = rsEmail(3)
			strEmailUsername = strRecipient				
			passcheck = 1	
		Else	
			passcheck = 0		
		End If
	
	Call CloseRs(rsEmail)
	Call CloseDB()
		
	if passcheck = 0 then
		ErrorCode = 1
		passcheck = "<div style='padding:10px;'>Your username is not recoginsed.</div>"
	else
		ErrorCode = 0
		passcheck = ""
	end if

if ErrorCode <> 1 then

TBL = ""
TBL = TBL & "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 width=""100%"">" & vbCrLf
TBL = TBL & "	<tr>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" height=""1""></td>" & vbCrLf
TBL = TBL & "	</tr>" & vbCrLf
TBL = TBL & "	<tr>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" width=""1""></td>" & vbCrLf
TBL = TBL & "		<td width=""3""></td>" & vbCrLf
TBL = TBL & "		<td>" & vbCrLf
TBL = TBL & "			<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td width=""145"" height=""63""></td>" & vbCrLf
TBL = TBL & "					<td></td>" & vbCrLf
TBL = TBL & "					<td align=""right"" valign=""bottom""><img src="""&SiteLocation&"images/"&WebLogo&""" width="""&WebLogoW&""" height="""&WebLogoH&"""></td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "			</table>" & vbCrLf
TBL = TBL & "		</td>" & vbCrLf
TBL = TBL & "		<td width=""3"" height=""65""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=""#7F7F7F"" width=""1"" height=""65""></td>" & vbCrLf
TBL = TBL & "	</tr>" & vbCrLf
TBL = TBL & "	<tr>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1"" height=""5""></td>" & vbCrLf
TBL = TBL & "		<td width=""3"" height=""5""></td>" & vbCrLf
TBL = TBL & "		<td height=""5""></td>" & vbCrLf
TBL = TBL & "		<td width=""3"" height=""5""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1"" height=""5""></td>" & vbCrLf
TBL = TBL & "	</tr>" & vbCrLf
TBL = TBL & "	<tr>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1""></td>" & vbCrLf
TBL = TBL & "		<td width=""3""></td>" & vbCrLf
TBL = TBL & "		<td>" & vbCrLf
TBL = TBL & "			<table width=""100%"" border=0 cellpadding=0 cellspacing=0>" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td height=""1"" class=""dash""></td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td height=""50""></td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td bgcolor=""#cc0000"" width=""214"" height=""29"" style=""color:white"">Your Login Details</td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td height=""50""> <br/>" & vbCrLf
TBL = TBL & "					<p>Hi "&strFullName&",</p>" & vbCrLf
TBL = TBL & "					<p>Your login details to "&SITE_NAME&" are as follows:<br/>" & vbCrLf
TBL = TBL & "					<br/>" & vbCrLf
TBL = TBL & "					<p>Username : " &strUsername&"</p>" & vbCrLf
TBL = TBL & "					<p>Password : " &strPassword&"</p>" & vbCrLf
TBL = TBL & "					</td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td height=""1"" class=""dash""></td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "				<tr>" & vbCrLf
TBL = TBL & "					<td height=""50"" valign=""top""><br/>" & vbCrLf
TBL = TBL & "					If you experience any difficulties please reply to this email with details of the problem" & vbCrLf
TBL = TBL & "					<p>or call one of the "&SITE_NAME&" team on "&FeePhone&".</p>" & vbCrLf
TBL = TBL & "					<p>The information transmitted is intended only for the person or entity to which it is addressed and may contain confidential</p>" & vbCrLf
TBL = TBL & "					<p>and/or privileged material. Although this message and any attachments are believed to be free of any virus or other defect</p>" & vbCrLf
TBL = TBL & "					<p>that might affect any computer system into which it is received and opened it is the responsibility of the recipient to ensure</p>" & vbCrLf
TBL = TBL & "					<p>that it is virus free and no responsibility is accepted for any loss or damage in any way arising from its use.</p>" & vbCrLf
TBL = TBL & "					<p>Powered by <a href=""http://www.reidmark.com"" target=""_blank"">Reidmark ebusiness systems</a></p>" & vbCrLf
TBL = TBL & "					</td>" & vbCrLf
TBL = TBL & "				</tr>" & vbCrLf
TBL = TBL & "			</table>" & vbCrLf
TBL = TBL & "		</td>" & vbCrLf
TBL = TBL & "		<td width=""3""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1""></td>" & vbCrLf
TBL = TBL & "	</tr>" & vbCrLf
TBL = TBL & "	<tr>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1"" height=""5""></td>" & vbCrLf
TBL = TBL & "		<td width=""3"" height=""5""></td>" & vbCrLf
TBL = TBL & "		<td height=""5""></td>" & vbCrLf
TBL = TBL & "		<td width=""3"" height=""5""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1"" height=""5""></td>" & vbCrLf
TBL = TBL & "	</tr>" & vbCrLf
TBL = TBL & "	<tr>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""3"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""3"" height=""1""></td>" & vbCrLf
TBL = TBL & "		<td bgcolor=#7F7F7F width=""1"" height=""1""></td>" & vbCrLf
TBL = TBL & "	</tr>" & vbCrLf
TBL = TBL & "</TABLE>" & vbCrLf

HTML = "<!DOCTYPE HTML PUBLIC ""-//IETF//DTD HTML//EN"">" & vbCrLf
HTML = HTML & "<html>" & vbCrLf
HTML = HTML & "<head>" & vbCrLf
HTML = HTML & "<title>"&nextstep_name&"</title>" & vbCrLf
HTML = HTML & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">" & vbCrLf
HTML = HTML & "<link rel=""stylesheet"" href="""&SiteURL&"/includes/css/nextstep.css"" type=""text/css"">" & vbCrLf
HTML = HTML & "</head>" & vbCrLf
HTML = HTML & "<body bgcolor=""#FFFFFF"">" & vbCrLf
HTML = HTML & TBL &  vbCrLf
HTML = HTML & "</body>" &  vbCrLf
HTML = HTML & "</html>"

		' toname, toemail, fromname, fromemail, subject, textbody, htmlbody
		If SendEMail(strUserName, strRecipient, constReplyToName, constReplyToEmail, SITE_NAME&" : Login Details", "", HTML) Then
			passcheck = passcheck & "<div style='padding:10px;'>Your email has been successfully sent. Thank you.</div>"
		Else
			passcheck = passcheck & "<div style='padding:10px;'>There was an error sending your email. Please try again.</div>"
		End If

End if

End Function

'-- PASSWORD RECOVERY

'-- EMAIL FUNCTIONS

Function SendEmail(strToName, strToEmail, strFromName, strFromEmail, strSubject, strTextBody, strHTMLBody)

  On Error Resume Next                              ' Don't stop on error, we'll check this later
  Dim iMsg 
  Dim iConf 
  Dim Flds 
  'Const cdoSendUsingPort = 2                       ' Use smtp over the network
  Const cdoSendUsingPort = 1                        ' Use the local smtp service
  
  set iMsg = CreateObject("CDO.Message")	        ' Calls CDO message COM object
  set iConf = CreateObject("CDO.Configuration") 	' Calls CDO configuration COM object

  Set Flds = iConf.Fields

  With Flds
    .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort
	.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\inetpub\mailroot\pickup"  'verify that this path is correct
    '.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.yourservername.com"
    '.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 20
    
	' use the following ONLY if your smtp server requires authentication
    '.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
    '.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "your@email.com"
    '.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "password"

    .Update 'updates CDO's configuration database
  End With

  ' apply the arguments you passed in to the message
  With iMsg
    Set .Configuration = iConf
    .To = strToName & " <" & strToEmail & ">"
    .From = strFromName & " <" & strFromEmail & ">"
    .Subject = strSubject
    ' send plain text or html
    If Len(strTextBody) > 0 Then
      .TextBody = strTextBody
    Else
      .HTMLBody = strHTMLBody
    End If
    .Send	'commands CDO to send the message
  End With 

  ' clean up
  Set iMsg = Nothing
  Set iConf = Nothing
  Set Flds = Nothing
  
  ' check for error & set boolean value
  If Err.Number <> 0 Then
    retError = False
  Else
    retError = True
  End If

  On Error GoTo 0

  ' return boolean value
  SendEMail = retError

End Function

'-- EMAIL FUNCTIONS

'-- CMS FUNCTIONS

Function RenameTemplatePage(OldName, NewName)

	GP_curPath = Request.ServerVariables("PATH_INFO")
	GP_curPath = "/"
	GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
	
	If Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
		GP_curPath = GP_curPath & "/"
	End If 

	FullPath = Trim(Server.mappath(GP_curPath)) 

	Set fso = CreateObject("Scripting.FileSystemObject")
	
	FileName = "\" & NewName
	
	'We only need to check if a filename exists if the user has entered a new filename
	DoWeNeedToCheckFileName = StrComp(OldName,NewName,1)
	
	If DoWeNeedToCheckFileName = -1 Or DoWeNeedToCheckFileName = 1 Then	
		If (fso.FileExists(FullPath & FileName)) Then
			GP_FileExist = True
		End If 		  
		If GP_FileExist then		 
			ErrorMessage = " - This filename already exists"
			ErrorValue = 1
			FileName = TempFileName		
		Else		
			ErrorValue = 0
			Set fso = CreateObject("Scripting.FileSystemObject") 
				fso.MoveFile FullPath & "\" & OldName, FullPath & "\" & NewName
				Set fso = Nothing						
		End If		
	End If
	
End Function


Function CreatePageTemplate(PAGE_NAME,MENUID)

response.write PAGE_NAME & "<br>"
'Dim PAGE_NAME
'	PAGE_NAME = "Robert"
	
Dim Template

	'IF MENUID = 49 THEN
	'	
	'	Template = "<!-- ##include virtual=""includes/Templates/LibraryTemplate.asp"" -->"
	'
	'ELSEIF MENUID = 130 THEN
	'
	'	Template = "<!-- ##include virtual=""includes/Templates/SkillsForJobsTemplate.asp"" -->"
	'
	'ELSE	
	'	
	'	Template = "<!-- ##include virtual=""includes/Templates/SiteTemplate.asp"" -->"
	'
	'END IF
	
Set fso = CreateObject("Scripting.FileSystemObject")

	GP_curPath = Request.ServerVariables("PATH_INFO")
	GP_curPath = "/"
	GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
	
	if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
		GP_curPath = GP_curPath & "/"
	end if 

	FullPath = Trim(Server.mappath(GP_curPath)) 

	FileName = "\" & PAGE_NAME
	Extension = ".asp"
response.write FileName & "<br>"
	If (fso.FileExists(FullPath & FileName & Extension)) Then
		GP_FileExist = true
	End If   
	if GP_FileExist then
		'Begin_Name_Num = 0
		'while GP_FileExist    
		'	Begin_Name_Num = Begin_Name_Num + 1
		'	TempFileName = FileName & "_" & Begin_Name_Num
		'	GP_FileExist = fso.FileExists(FullPath & TempFileName & Extension)
		'wend  
		ErrorMessage = " - This filename already exists"
		ErrorValue = 1
		response.write ErrorValue & "<br>"
		'FileName = TempFileName
	end if

	'Response.Write FullPath & FileName & Extension & "<BR>"
'response.end
	Set MyFile= fso.CreateTextFile(FullPath & FileName & Extension,True)
		MyFile.Write Replace(Template,"##","#") & VbCrLf
	Set MyFile = Nothing


End Function

'-- CMS FUNCTIONS

'-- MEMBERS DIRECTORY FUNCTIONS

Function display_menu(arrlink, arrtitle, selected, unselected, menuitem)

'	strmenu = "<table width=""140"">"
'
'	for i = 0 to ubound(arrlink)
'
'		If CINT(menuitem) = CINT(i) then
'			css = unselected
'		Else
'			css = selected
'		End If
'
'		strmenu = strmenu & "<tr>"
'		strmenu = strmenu & "<td class="& css &" height=""25"">"
'		strmenu = strmenu & "<a href=" & arrlink(i) & ">&nbsp; "
'		strmenu = strmenu & ""& arrtitle(i) &""
'		strmenu = strmenu & "</a></td>"
'		strmenu = strmenu & "</tr>"
'
'	next
'
'	strmenu = strmenu & "</table>"
'
'	display_menu = strmenu

	strmenu = "<ul style=""padding:0px;margin:0px;"">"

	for i = 0 to ubound(arrlink)
		strmenu = strmenu & "<li "
		If menuitem = i then
		    strmenu = strmenu & "class=""" & selected & """"
		Else
		    strmenu = strmenu & "class=""" & unselected & """"
		End If
		strmenu = strmenu & ">"
		strmenu = strmenu & "<a href=""" & server.HTMLEncode(arrlink(i)) & """>"
		strmenu = strmenu & ""& server.HTMLEncode(arrtitle(i)) &  ""
		strmenu = strmenu & "</a></li>"
	next
	strmenu = strmenu & "</ul>"

	display_menu = strmenu

End Function


Function displayprovidermenu(menuid,logintype)

	redim arrlink(4)
	redim arrtitle(4)

	If logintype = "2" Then

		redim arrlink(4)
		redim arrtitle(4)

		arrlink(0) = "NewProvider.asp"
		arrlink(1) = "NewLogin.asp"
		arrlink(2) = "AmendLogin.asp"
		arrlink(3) = "AmendBarriers.asp"
		arrlink(4) = "MembersLogout.asp"

		arrtitle(0) = "Create New Provider"
		arrtitle(1) = "Add New Log In"
		arrtitle(2) = "View/Amend Log In"
		arrtitle(3) = "View/Amend Barriers"
		arrtitle(4) = "Log out"

	ElseIf logintype = "3" Or logintype = "4" Then

		redim arrlink(3)
		redim arrtitle(3)

		arrlink(0) = "NewLogin.asp"
		arrlink(1) = "AmendLogin.asp"
		arrlink(2) = "AmendBarriers.asp"
		arrlink(3) = "MembersLogout.asp"

		arrtitle(0) = "Add New Log In"
		arrtitle(1) = "View/Amend Log In"
		arrtitle(2) = "View/Amend Barriers"
		arrtitle(3) = "Log out"

	Else

		redim arrlink(2)
		redim arrtitle(2)

		arrlink(0) = "AmendLogin.asp"
		arrlink(1) = "AmendBarriers.asp"
		arrlink(2) = "MembersLogout.asp"

		arrtitle(0) = "View/Amend Org"
		arrtitle(1) = "View/Amend Barriers"
		arrtitle(2) = "Log out"

	End If


	strmenu = display_menu(arrlink, arrtitle, "selectedRow", "unselectedRow", menuid)

	displayprovidermenu = strmenu

End Function


Function displaycontactsubmenu(menuid,logintype)

	Dim arrlink(3)
	Dim arrtitle(3)

		arrlink(0) = "SiteContactDetails.asp"
		arrlink(1) = "SiteGettingThere.asp"
		arrlink(2) = "SiteAccess&Facilities.asp"
		arrlink(3) = "SiteAppointments.asp"

		arrtitle(0) = "Contact Details"
		arrtitle(1) = "Getting There"
		arrtitle(2) = "Access & Facilities"
		arrtitle(3) = "Appointments"

	strmenu = display_contactmenu(arrlink, arrtitle, "selectedRow", "unselectedRow", menuid)

	displaycontactsubmenu = strmenu

End Function


Function IsChecked(strFieldName)
  Dim blnChecked

  If Request.Form(strFieldName).Count > 0 Then
    blnChecked = True
  Else
    blnChecked = False
  End If
  
  IsChecked = blnChecked
End Function


Function SaveBarrier(OrgID)

    passcheck = 0

    If IsChecked("checkbox") = True Then

    	Set conn=Server.CreateObject("ADODB.Connection")
		    conn.Open DSN_CONNECTION_STRING
	    Set Rs = Server.CreateObject("ADODB.Command")
 	    Set Rs.activeconnection = conn
		    Rs.commandtype 		= 4
		    Rs.commandtext 		= "MD_BARRIER_INSERT"
		    Rs.parameters(1) 	= OrgID
		    Rs.parameters(2) 	= Request.Form("checkbox")
		    Rs.execute

        passcheck = 1
        passcheck_text = "<div style='padding:10px;'>You have successfully saved the " & Request.Form("checkbox").Count &" selected option(s)</div>"

    Else

        passcheck = 0 
		passcheck_text = "<div style='padding:10px;'>Please Select at least 1 Option</div>"

    End If

End Function


'-- MENUS - FROM OLD SITE
Function displaycontactmenu(menuid,logintype)

	Dim arrlink(6)
	Dim arrtitle(6)

		arrlink(0) = "ContactDetails.asp"
		arrlink(1) = "GettingThere.asp"
		arrlink(2) = "Access&Facilities.asp"
		arrlink(3) = "Premises.asp"
		arrlink(4) = "ServicesOnOffer.asp"
		arrlink(5) = "EligibleClients.asp"
		arrlink(6) = "Charges.asp"
		
		arrtitle(0) = "Contact Details"
		arrtitle(1) = "Getting There"
		arrtitle(2) = "Access & Facilities"
		arrtitle(3) = "Premises & Sites"
		arrtitle(4) = "Services on Offer"
		arrtitle(5) = "Eligible Clients"
		arrtitle(6) = "Charges"
	
	strmenu = display_contactmenu(arrlink, arrtitle, "selectedRow", "unselectedRow", menuid)

	displaycontactmenu = strmenu

End Function


Function display_contactmenu(arrlink, arrtitle, selected, unselected, menuitem)

	strmenu = "<ul>"

	for i = 0 to ubound(arrlink)
		strmenu = strmenu & "<li "
		If menuitem = i then
		    strmenu = strmenu & "class=""" & selected & """"
		Else
		    strmenu = strmenu & "class=""" & unselected & """"
		End If
		strmenu = strmenu & ">"
		strmenu = strmenu & "<a href=""" & server.HTMLEncode(arrlink(i)) & """>"
		strmenu = strmenu & ""& server.HTMLEncode(arrtitle(i)) &  ""
		strmenu = strmenu & "</a></li>"
	next
	strmenu = strmenu & "</ul>"

	display_contactmenu = strmenu

End Function


Function display_list(rs,str_ulclass,str_liclass,str_fieldname,strNA)
		If not rs.EOF Then
		    strlist = "<ul class=""" & str_ulclass & """>"
		    Do Until rs.EOF
		        strlist = strlist & "<li class=""" & str_liclass & """>"
		        strlist = strlist & ""& server.HTMLEncode(rs(str_fieldname)) &  ""
		        strlist = strlist & "</li>"
		    rs.MoveNext
		    Loop
		    strmenu = strmenu & "</ul>"
		else
		    strlist = strNA
		end if
	display_list = strlist
End Function

Sub SetPortalLevel(MainSite)
	If MainSite = True then
		response.cookies("siteid") = request.cookies("mainsiteid")
	End If
End Sub

'-- MENUS - FROM OLD SITE

Function RecordReferralViewing(CUSTOMER_REFERENCE, REFERRALID, LOGINID, RVIEWDATE)

    'Response.Write CUSTOMER_REFERENCE
    'Response.Write "<br/>"
    'Response.Write REFERRALID
    'Response.Write "<br/>"
    'Response.Write LOGINID
    'Response.Write "<br/>"
    'Response.Write RVIEWDATE

    SQL = "UPDATE CRMS_REFERRAL SET RVIEWDATE = '" & RVIEWDATE & "', RVIEWBY=" & LOGINID & " WHERE REFERRALID = " & REFERRALID
    Call OpenDB()
    conn.Execute(SQL)
    'Call OpenRs(rs,SQL)
    'Call CloseRs(rs)
    Call CloseDB()

End Function

'-- MEMBERS DIRECTORY FUNCTIONS
%> 