<!-- #include virtual="includes/Connections/db_connection.asp" -->
<%
Dim Rs, Conn, SQL, strSQL

Function OpenDB()
 	Set Conn = Server.CreateObject("ADODB.Connection")
  		Conn.Open DSN_CONNECTION_STRING 
End Function

Function CloseDB()
 		Conn.Close
 	Set Conn = Nothing
End Function
 
Function OpenRs(RecSet, iSQL)
 	Set RecSet = Server.CreateObject("ADODB.Recordset")
 	'If Request.ServerVariables ("REMOTE_ADDR") = "90.152.41.83" Then
 	'    Response.Write iSQL
 	'End If
  		RecSet.Open iSQL, Conn, 0, 1
End Function

Function CloseRs(RecSet)
 		RecSet.Close
 	Set RecSet = Nothing
End Function

' strSELECT									- String for select box to be returned to
' iSelectName								- The Name of the Select List
' iTABLE									- String for TABLE NAME
' iCOLUMNS									- String for COLUMNS
' iORDERBY									- String for ORDERBY CLAUSE	
' PS										- ADDD A PLEASE SELECT OPTION AT START?
' isSelected								- SELECT AN ITEM FROM THE DATA: 1 = yes, 0 = no
' iStyle									- Any Style You wish to apply to the Select List
' iClass									- Any Class you wish to apply to the Select List
' iEvent									- Any event such as onclick or other valid select tag

Function BuildSelect_SP(ByRef lst, iSelectName, rsSet,PS, isSelected, iStyle, iClass, iEvent)

	lst = "<select name=""" & iSelectName & """ id=""" & iSelectName & """ "

	If NOT isNull(iClass) Then
		lst = lst & " class=""" & iClass & """ "
	End If
	If NOT isNull(iStyle) Then
		lst = lst & " style=""" & iStyle & """ "
	End If
	If NOT isNull(iEvent) Then
		lst = lst & iEvent
	End If
		lst = lst & ">"

	If (NOT isNull(PS)) Then
			lst = lst & "<option value="""">" & PS & "</option>"
	End If

	If NOT isNull(isSelected) Then
		While (NOT rsSet.EOF)
				lstid = rsSet(0)
			optionSelected = ""
			If (CStr(lstid) = CStr(isSelected)) Then
				optionSelected = " selected"
			End If
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
			rsSet.MoveNext()
		Wend
	Else		
		While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
		Wend
	End If		

	Call CloseRs(rsSet)

	lst = lst & "</select>"

End Function

Function BuildSelect(ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS, isSelected, iStyle, iClass, iEvent)
		lst = "<select name=""" & iSelectName & """ id=""" & iSelectName & """ "
	If NOT isNull(iClass) then
		lst = lst & " class=""" & iClass & """ "
	End If
	If NOT isNull(iStyle) then
		lst = lst & " style=""" & iStyle & """ "
	End If
	If NOT isNull(iEvent) then
		lst = lst & iEvent
	End If
		lst = lst & ">"

	Dim strSQL
		strSQL = "SELECT " & iCOLUMNS & " FROM " & iTABLE & " ORDER BY " & iORDERBY
	If (NOT isNull(PS)) Then
			lst = lst & "<option value="""">" & PS & "</option>"
	End If
	
	Dim rsSet
	Call OpenRs (rsSet, strSQL)

	if NOT isNull(isSelected) then
		While (NOT rsSet.EOF)
				lstid = rsSet(0)
			optionSelected = ""
			if (CStr(lstid) = CStr(isSelected)) then
				optionSelected = " selected"
			end if
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
			rsSet.MoveNext()
		Wend
	else		
		While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
		Wend
	end if		

	CloseRs(rsSet)
	
		lst = lst & "</select>"
End Function

Function CheckFolderExists(sFolderName)
Dim FileSystemObject
Set FileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	If (FileSystemObject.FolderExists(sFolderName)) Then
		CheckFolderExists = True
	Else
		CheckFolderExists = False
	End If
Set FileSystemObject = Nothing
End Function



Function CheckFileExists(sFileName)
Dim FileSystemObject
Set FileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	If (FileSystemObject.FileExists(sFileName)) Then
		Set PermissionChecker = Server.CreateObject("MSWC.PermissionChecker")
			If PermissionChecker.HasAccess(sFileName) Then
				CheckFileExists = True 
			End If

		Set PermissionChecker = nothing
	Else
		CheckFileExists = False
	End If
Set FileSystemObject = Nothing
End Function

Function RW(strOutText)
	Response.Write strOutText
End Function

Function RW(strOutText)
	Response.Write strOutText
End Function

Function Debug()
If Request.Form <> "" Then
	For Each Item In Request.Form
		rw "" & Item & " : " & Request(Item) & "<br/>" & vbcrlf
	Next
 End If 

 If Request.QueryString <> "" Then
	For Each Item In Request.QueryString
		rw "" & Item & " : " & Request(Item) & "<br/>" & vbcrlf
	Next
 End If
End Function


Function Login(strUsername, strPassword)

passcheck = 0

SQL = "SELECT CustomerID.CustomerID, Firstname, Surname, Email, Username, Password, Firstname+Space(1)+Surname As FullName " &_
	  "FROM   CUSTOMERID, CUSTOMERDETAILS " &_
	  "WHERE  CUSTOMERID.CUSTOMERID = CUSTOMERDETAILS.CUSTOMERID AND " &_
	  "	USERNAME = '" + Replace(strUsername, "'", "''") + "'  AND " &_ 
	  "	PASSWORD = '" + Replace(strPassword, "'", "''") + "'"

Call OpenDB()
Call OpenRs(rsLogin,SQL)

	If Not rsLogin.EOF Then	
		If strUsername <> "xyz" Then
			Dim strUser, strPass, intCompPass
				strUser = rsLogin.Fields.Item("Username").Value
				strPass = rsLogin.Fields.Item("Password").Value
				intCompPass = StrComp(strPassword, StrPass)

			If intCompPass <> 0 Or strUser = "" Or strPass = "" Then
				passcheck = 0
			Else
					Session("svCustomerID")	=	rsLogin.Fields.Item("CustomerID").Value
					Session("svFirstName")	=	rsLogin.Fields.Item("Firstname").Value
					Session("svLastName")	=	rsLogin.Fields.Item("Surname").Value
					Session("svEmail")		=	rsLogin.Fields.Item("Email").Value
					Session("svName")		=	rsLogin.Fields.Item("FullName").Value
					Session("svUsername")	=	strUser
					passcheck 				=	1
			End If
		End If
	Else
		passcheck = 0
	End If

	If passcheck = 0 Then
		rw "<div style='padding:10px;'>Your username/password combination is not recoginsed.</div>"
	Else
		rw  "<div style='padding:10px;'>You have successfully logged into " & nextstep_name &".</div>"
		Response.Redirect "/home.asp?customerid="&Session("svCustomerID")
	End If

Call CloseRs(rsLogin)
Call CloseDB()

End Function

Function nulltest(str_isnull)
    If IsNull(str_isnull) OR str_isnull = "" Then
	    str_isnull = null
    End If
    nulltest = str_isnull
End Function

Function NullNestTF(str_isnull)
    If IsNull(str_isnull) OR str_isnull = "" Then
	    NullNestTF = True
	Else
	    NullNestTF = False
    End If
End Function
%>