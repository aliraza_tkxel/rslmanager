<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
Dim Stage
    Stage = Request("Stage")

    If Stage = "" Then Stage = 1 End If

    Select Case Stage
        Case 1
        StageText = "Select your Priorities"
        Case 2
        StageText = "Prioritise your Selections"
        Case 3
        StageText = "Our Recommendations"
    End Select
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

.goinghigh tr:hover
{
	background-color:#cc0000;
	color:white;
}

.goinghigh tr:hover a
{
	background-color:#cc0000;
	color:white;
}

.goinghigh a
{
	color:#cc0000;
}


table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:5px; padding-bottom:5px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

/*
tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}
*/

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

td.whiteout
{
    background:#ffffff
}

-->
</style>


<script language="javascript" type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script language="javascript" type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script language="javascript" type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script language="javascript" type="text/javascript" src="/ClientScripting/formValidation.js"></script>

<script language="javascript" type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script language="javascript" type="text/javascript">

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}


/*
function buildQueryString2(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}
*/

function buildQueryString(theFormName) {
  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)

            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function checkbox_checker()
{
// set var checkbox_choices to zero
var checkbox_choices = 0;
// Loop from zero to the one minus the number of checkbox button selections
for (counter = 0; counter < document.Form1.checkbox.length; counter++)
{
// If a checkbox has been selected it will return true
// (If not it will return false)
if (document.Form1.checkbox[counter].checked)
{ checkbox_choices = checkbox_choices + 1; }
}
if (checkbox_choices > 3 )
{
// If there were more than three selections made display an alert box
return (false);
}
if (checkbox_choices < 1)
{
// If there were less then selections made display an alert box
return (false);
}
// If three were selected then display an alert box stating input was OK
return (true);
}

var FormFields = new Array()

function CheckPostcode(obj)
{

    FormFields = new Array("txt_POSTCODE|Postcode|POSTCODE|N");
    if (!checkForm('errordiv')) return;

    if (document.getElementById('Form1').elements['txt_POSTCODE'].value == "")
    {
        if (!(checkbox_checker()))
        {
            alert('Please select at least 1 item that you deem a priority but no more than 3')
        }
        else
        {
            LoadStep(2,'errordiv')
        }
    }
    else
    {
        //xmlhttp.open("GET", "svr/check_pc.asp?txt_Postcode="+ document.getElementById('Form1').elements['txt_POSTCODE'].value,true);
        //xmlhttp.onreadystatechange=function()
        //{
          //if (xmlhttp.readyState==4)
          //{
	          //if (xmlhttp.status==200)
	          //{
			        //if (xmlhttp.responseText == "0")
			        //{
				    //    alert("Please enter a valid Postcode for <%=SITE_NAME%>.");
				    //    targetError('txt_POSTCODE','red');
				    //    return false;
			        //}
			        //else
			        //{
			            if (!(checkbox_checker()))
			            {
			                alert('Please select at least 1 item that you deem a priority but no more than 3')
			            }
			            else
			            {
			                LoadStep(2,'errordiv')
			            }
			        //}
		        //}
	        //}
        //}
    //xmlhttp.send(null)
    }
}

function BuildXMLReport(str_url,elId,elED){

	//set the loading parameter
    document.getElementById(elED).innerHTML = "<div style='float:left; padding:10px;'>Please wait. Loading...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById(elED).className = "no_error"
	//call xmlhttp functions
	var strURL = str_url
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  +
									   str_url + "' could not be found.");
							   document.getElementById(elED).innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById(elED).innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 ||
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById(elED).innerHTML = ""
							   }
							   // Call the desired result function
							   else {
									   document.getElementById(elId).innerHTML = strResponse
									   document.getElementById(elED).innerHTML = ""
									   document.getElementById(elED).className = ""
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}


function go(url,elId,pId)
{
    BuildXMLReport('rje.asp'+ url + '&siteid='+pId,'OrgDetails','errordiv');
}

function CustomSelect(aItem1)
{
    var isSelectValidationError = false
    var myString=aItem1
    var mySplitResult = myString.split(", ")
    var SelectedValues = ""
    for(i = 0; i < mySplitResult.length; i++)
    {
        SelectedValues += document.getElementById('ASSESSMENT_'+mySplitResult[i]).value + " "
	    if (document.getElementById('ASSESSMENT_'+mySplitResult[i]).value == "")
	    {
	        isSelectValidationError = true
	    }
    }
    if (isSelectValidationError == true)
    {
        alert("Please select a Priority for the chosen options")
    }
    else
    {
       var temp = SelectedValues
       var array2 = temp.split(" ") ;
       var array1= new Array;
       for(var i = 0; i < array2.length; i++)
       {
           var xx = true;
           var ArrayVal = array2[i];
           for(var j = i+1; j < array2.length; j++)
           {
             if(array2[j] == ArrayVal)
              xx = false;
              if (xx == false)
              {
                isSelectValidationError = true
                alert("Please ensure that all priorities are ordered with out duplication");
                break;
              }
           }
       	}
        if (isSelectValidationError == false)
        {
            AllSelections(aItem1)
        }
    }
}

function AllSelections(SelectedValues)
{
    var temp = SelectedValues
    var mySplitResult = temp.split(", ")
    document.getElementById('PriorityList').value = ""
    for(i = 0; i < mySplitResult.length; i++)
    {
        //document.getElementById('Priority'+(i+1)).value = (i+1)+'-'+mySplitResult[i];
        //document.getElementById('Priority'+(i+1)).value = document.getElementById('ASSESSMENT_'+mySplitResult[i]).selectedIndex+'-'+ mySplitResult[i]
        document.getElementById('PriorityList').value += document.getElementById('ASSESSMENT_'+mySplitResult[i]).selectedIndex+'-'+ mySplitResult[i] + '|'
    }
    document.getElementById('PriorityList').value = document.getElementById('PriorityList').value.slice(0, -1)

    var temp2 = document.getElementById('PriorityList').value
    var mySplitResult2 = temp2.split("|")
    var mySplitResult3
    for(k = 0; k < mySplitResult2.length; k++)
    {
        mySplitResult3 = mySplitResult2[k].split("-")
        document.getElementById('Priority'+mySplitResult3[0]).value = mySplitResult3[0]+'-'+mySplitResult3[1]
    }

    //SavePriorities()

    LoadStep(3,'errordiv')

//var lis = document.getElementsByTagName("SELECT");
	//for (var i=0; i<lis.length; i++)
	//{
	//alert(i)
	//}
}

function LoadStepAlt(StepNo,elED)
{
    switch (StepNo)
    {
        case 1 : url = "SelfAssessment-S1.asp"; cvb = "Step 1 : Select your Priorities"; break;
        case 2 : url = "SelfAssessment-S2.asp"; cvb = "Step 2 : Prioritise your Selections"; break;
        case 3 : url = "SelfAssessment-S3.asp"; cvb = "Our Recommendations"; break;
    }
    //console.log(url+ buildQueryString('Form1'));
    BuildXMLReport(url+ buildQueryString('Form1'),'OrgDetails','errordiv');
    document.getElementById("ffg").innerHTML = cvb;
}


function LoadStep(StepNo,elED)
{
    if (StepNo == 1)
    {
        //document.getElementById("Priority1").value == ""; document.getElementById("Priority2").value == ""; document.getElementById("Priority3").value == "";
    }
    switch (StepNo)
    {
        case 1 : url = "SelfAssessment-S1.asp"; cvb = "Step 1 : Select your Priorities"; break;
        case 2 : url = "SelfAssessment-S2.asp"; cvb = "Step 2 : Prioritise your Selections"; break;
        case 3 : url = "SelfAssessment-S3.asp"; cvb = "Our Recommendations"; break;
    }
    //console.log(url+ buildQueryString('Form1'));
    BuildXMLReport(url+ buildQueryString('Form1'),'OrgDetails','errordiv');
    document.getElementById("ffg").innerHTML = cvb;
}


function blar(val,priority)
{
    if (priority != "")
    {
        document.getElementById('Priority'+priority).value == "";
        document.getElementById('Priority'+priority).value = priority+'-'+val;
    }
}


function SaveRecomendations()
{
    var chk = document.Form1.CheckboxSite
    for (i=0;i<chk.length;i++)
    chk[i].checked = true;
    BuildXMLReport('svr/Recommendations_save.asp'+ buildQueryString('Form1'),'OrgDetails','errordiv');
}


function RemoveSite(ID)
{
    elDelete('SITEID');

    var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "SITEID");
        input.setAttribute("id", "SITEID");
        input.setAttribute("value", ID);
        document.getElementById("Form1").appendChild(input);

    BuildXMLReport('svr/RemoveSite.asp'+ buildQueryString('Form1'),'OrgDetails','errordiv');
    //console.log('svr/RemoveSite.asp'+ buildQueryString('Form1'))
}


function elDelete(elementId)
{
  var label=document.getElementById(elementId);
  if (label != null)
  document.getElementById("Form1").removeChild(label);
}


function BUILDME()
{
    LoadStep(3,'errordiv')
}

var plus = new Image();
    plus.src = "/images/plusImage.gif";
var minus = new Image();
    minus.src = "/images/minusImage.gif";

function toggleImg(imgName)
{
	document.images[imgName].src = (document.images[imgName].src==plus.src) ? minus.src:plus.src;
	return false;
}

function Toggle(refByName)
{
    if (document.getElementById("Div_"+refByName).style.display == "block")
    {
	    document.getElementById("Div_"+refByName).style.display = "none";
	}
    else
    {
	    document.getElementById("Div_"+refByName).style.display = "block";
	}
	toggleImg('IMG_'+refByName)
}

</script>

    <style type="text/css">
        #map
        {
	        height: 1px;
	        width: 1px;
	        display:block;
        }
    </style>

    <script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script type="text/javascript" language="javascript" src="/ClientScripting/gmap.js"></script>

    <script type="text/javascript" language="javascript">

    function PCode(){
        if (document.getElementById("txt_POSTCODE").value != "")
	    {
	        return isUKPostCode2 ("txt_POSTCODE", "You must enter a valid postcode")
	    }
	    else {
	        document.getElementById("map").style.display = "none";
	        document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
	    }
    }

    function isUKPostCode2 (itemName, errName){
	    elVal = document.getElementById(itemName).value;
	    elVal = TrimAll(elVal.toUpperCase());
	    result = elVal.match(/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) [0-9][ABD-HJLNP-UW-Z]{2})$/g);
	    if (!result) {
		    targetError(itemName,"red");
		    document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
		    //console.log(errName);
		    document.getElementById("map").style.display = "none"
		    return false;
		    }
	    else {
		    elVal = String(elVal.replace(/\s/g,""));
		    elValLen = elVal.length;
		    elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		    document.getElementById(itemName).value = elVal;
		    targetError(itemName,"black");
		    //console.log(elVal);
            //document.getElementById("map").style.display = "block";
		    usePointFromPostcode(document.getElementById('txt_POSTCODE').value, setLatLng);
		    return true;
		    }
	    }

    function TrimAll(str) {
	    return LTrimAll(RTrimAll(str));
	    }

    function LTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	    return str.substring(i,str.length);
	    }

    function RTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	    return str.substring(0,i+1);
	    }

// function showPanel(fieldName) {
// var fieldNameElement = document.getElementById("field_name");
// while(fieldNameElement.childNodes.length >= 1) {
//   fieldNameElement.removeChild(fieldNameElement.firstChild);
// }
// fieldNameElement.appendChild(fieldNameElement.ownerDocument.createTextNode(fieldName));
//}

//var p2 = document.getElementsByTagName('p')[1];
//p2.parentNode.removeChild(p2);

//var p2 = document.getElementById("dvtxt_POSTCODE").firstChild;
//p2.parentNode.removeChild(p2);

    function targetError(iItem, iColor){
        if (iColor == 'red'){
           if (document.createTextNode){
                if (document.getElementById("dv"+iItem).childNodes.length >= 1){
                    document.getElementById("dv"+iItem).removeChild(document.getElementById("dv"+iItem).firstChild)
                }
            var mytext=document.createTextNode("Sorry! This is not a valid postcode")
            document.getElementById("dv"+iItem).appendChild(mytext)
            }
        }
        else {
            var label=document.getElementById("dv"+iItem);
            if (label != null)
            label.innerHTML = ""
        }
	document.getElementById(iItem).style.color = iColor
	document.getElementById(iItem).focus();
	}

    function EnableBtn() {
        document.getElementById("Next").disabled = false;
    }

    </script>
</head>
<body lang="en" onload="LoadStep(1,'errordiv')">
<script type="text/javascript" src="/ClientScripting/wz_tooltip.js"></script>
<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>"> <!-- onsubmit="return checkMyForm(this)" -->
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%> - <span id="ffg">Step <%=Stage%> : <%=StageText %></span></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%'=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <!-- ERRORS/PAGE LOAD -->
                    <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>>
                        <%=passcheck_text%>
                    </div>
                    <!-- DYNAMIC PAGE LOAD -->
                    <div id="OrgDetails" style="float:left;width:100%;"></div>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif" width="10px" height="10px" name="FVS_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif" width="10px" height="10px" name="FVW_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<input type="hidden" name="PriorityList" id="PriorityList" value="" />
<input type="hidden" id="hiddenLng" name="hiddenLng" value="<%=Session("svhiddenLng")%>" />
<input type="hidden" id="hiddenLat" name="hiddenLat" value="<%=Session("svhiddenLat")%>" />
</form>
</body>
</html>