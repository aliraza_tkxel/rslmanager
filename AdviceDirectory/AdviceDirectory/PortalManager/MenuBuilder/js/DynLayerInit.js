	var DynInitialised = false
	function init() {
		mylayer = new DynLayer("mylayerDiv")
		mylayer.slideInit()
		DynInitialised = true
	}
	
	var TipShowing = false
	function showTip(am_i_update) {
		if (DynInitialised) {
			if (am_i_update) hideSelects();
			mylayer.moveTo(10,-200)
			mylayer.slideTo(10,10 + document.body.scrollTop ,12,20)
			TipShowing	= true
		}
	}
	
	function hideTip(am_i_update) {
		clearTimeout();
		mylayer.slideActive = false;
		mylayer.slideTo(10,-200,12,20);
		TipShowing = false;
		// IF COMING FROM UPDATE THEN HIDE SELECTS
		if (am_i_update) window.setTimeout("showSelects()", 500);
	}

	function QuickHideTip() {
		clearTimeout()
		mylayer.slideActive = false
		mylayer.moveTo(10,-500)
		TipShowing = false
	}
	
	function slideLeft(){
		if (TipShowing){
			mylayer.slideTo(10,10 + document.body.scrollTop ,12,20)			
			}
		}
	
	function setTimeoutRePosition(){
		if (TipShowing)
			setTimeout("slideLeft()", 200)
		}