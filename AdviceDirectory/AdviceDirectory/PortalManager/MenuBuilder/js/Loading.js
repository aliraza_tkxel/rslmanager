var TOPLOADER;
var BOTLOADER;
var LoaderStatus_Top = "";
var LoaderStatus_Bot = "";
var LOADINGSIZE_TOP = 0
var LOADINGSIZE_BOT = 0
var LOADING_TEXT_DOTS = new Array(".", "..", "...", "....", ".....")

function STOPLOADER(which){
	if (which == "TOP") {
		try {
			clearTimeout(TOPLOADER);
			LoaderStatus_Top = "";
			LOADINGTEXT_TOP.innerHTML = "";
			TOP_DIV_LOADER.style.display = "none"
			TOP_DIV.style.display = "block"
			}
		catch (e) {
			temp = 1
			}
		}
	else if (which == "BOTTOM") {
		try {	
			clearTimeout(BOTLOADER);
			LoaderStatus_Bot = "";
			LOADINGTEXT_BOTTOM.innerHTML = "";
			BOTTOM_DIV_LOADER.style.display = "none"
			BOTTOM_DIV.style.display = "block"
			}
		catch (e) {
			temp =1
			}
		}
	}

function STARTLOADER(which) {
	if (which == "TOP") {
		clearTimeout(TOPLOADER);
		LoaderStatus_Top = "working"
		CALLLOADER("TOP")
		TOP_DIV.style.display = "none"
		TOP_DIV_LOADER.style.display = "block"
		}
	else if (which == "BOTTOM") {
		clearTimeout(BOTLOADER);
		LoaderStatus_Bot = "working"
		CALLLOADER("BOTTOM")
		BOTTOM_DIV.style.display = "none"
		BOTTOM_DIV_LOADER.style.display = "block"
		}
	}

function CALLLOADER(which) {
	if (which == "TOP") {
		if (LoaderStatus_Top == "working"){
			LOADINGSIZE_TOP++;
			if (LOADINGSIZE_TOP == 5) LOADINGSIZE_TOP = 0;
			LOADINGTEXT_TOP.innerHTML = "Please Wait, Page is Loading" + LOADING_TEXT_DOTS[LOADINGSIZE_TOP] + "";
			TOPLOADER = setTimeout("CALLLOADER('TOP')",300);
			}
		else 
			LOADINGTEXT_TOP.innerHTML = "";
	}
	else if (which == "BOTTOM") {
		if (LoaderStatus_Bot == "working"){
			LOADINGSIZE_BOT++;
			if (LOADINGSIZE_BOT == 5) LOADINGSIZE_BOT = 0;
			LOADINGTEXT_BOTTOM.innerHTML = "Please Wait, Page is Loading" + LOADING_TEXT_DOTS[LOADINGSIZE_BOT] + "";
			BOTLOADER = setTimeout("CALLLOADER('BOTTOM')",300);
			}
		else 
			LOADINGTEXT_BOTTOM.innerHTML = "";
		}
	}	
	

	
