function printHidden(url) {
  document.body.insertAdjacentHTML("beforeEnd", 
    "<iframe name=printHiddenFrame width=0 height=0></iframe>");
  doc = printHiddenFrame.document;
  doc.open();
  doc.write(
    "<frameset onload='parent.onprintHiddenFrame()' rows=\"100%\">" +
    "<frame name=printMe src=\""+url+"\">" +
    "</frameset>");
  doc.close();
}

function onprintHiddenFrame() {
  function onfinish() {
    printHiddenFrame.outerHTML = "";
    if ( window.onprintcomplete ) window.onprintcomplete();
  }
  printFrame(printHiddenFrame.printMe, onfinish);
}
