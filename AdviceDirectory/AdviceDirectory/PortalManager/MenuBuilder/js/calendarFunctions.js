var yyDatevar ='YYnull';
var yyDiv=null;var YYLang='de';
var dom= new Array(12);
dom[0]=31;dom[1]=28;dom[2]=31;dom[3]=30;dom[4]=31;dom[5]=30;dom[6]=31;dom[7]=31;dom[8]=30;dom[9]=31;dom[10]=30;dom[11]=31;
var YYstrm= new Array(12);
YYstrm[0]='January';YYstrm[1]='February';YYstrm[2]='March';YYstrm[3]='April';YYstrm[4]='May';YYstrm[5]='June';YYstrm[6]='July';
YYstrm[7]='August'; YYstrm[8]='September';YYstrm[9]='October';YYstrm[10]='November';YYstrm[11]='December';


function ChangeCalCol(ref,col,col2){
	mydoc = document.all;
	elref = mydoc[ref];
	elref.style.backgroundColor = col;
	elref.style.color = col2;	
}
   
function YYcalclose(YYwhat){//v4.0
  if (YYwhat>=0){
    var yyTag = YYwhat - yyW + 2;
    if ((yyTag > 0)&&(yyTag <= dom[yyDiv.m])){
      var d=yyTag;
      if (YYLang=='de'){YYstrdatum = d+'/'+eval(yyDiv.m+1)+'/'+yyDiv.year;} //13.4.1968
      if (YYLang=='com'){YYstrdatum = YYstrm[yyDiv.m].substring(0,3) +' '+d+', '+yyDiv.year;}
      if (YYLang=='av'){YYstrdatum = d+'/'+YYstrm[yyDiv.m].substring(0,3)+'/'+yyDiv.year;}
      yyDatevar.value=YYstrdatum;
    }
  }
  if (document.layers){yyDiv.visibility = "hide"; showHide('show');}
  if (document.all||document.getElementById){yyDiv.style.visibility = "hidden"; showHide('show');}
}

function YYgoYear(YY){//v4.0
//Also modified by ALI Z
//  var newYear = eval(yyDiv.year) + YY;
//  yyDiv.year = newYear.toString(10);
  yyDiv.year = parseInt(document.getElementById("theYear").value);
  if (YY==0){} else {YYsetMonth(yyDiv.m,yyDiv.year)}
  setTimeout('YYcaldraw(yyDiv.d,yyDiv.m,yyDiv.year)',(document.layers)?'300':'1');
}

function YYsetMonth(YYm, YYy){//v4.0
  var startDate = new Date();
  startDate.setMonth(YYm);   startDate.setFullYear(YYy);   startDate.setDate(1);
  yyW = startDate.getDay();
  if (yyW==0){yyW=7}
  var daSchalt = yyDiv.year % 4;
  if (daSchalt==0){dom[1]=29}else {dom[1]=28}
}

function YYgoMonth(){//v4.0
//Also modified by ALI Z
//   yyDiv.m=yyDiv.m+YY;
	yyDiv.m = parseInt(document.getElementById("theMonth").value);

   if (yyDiv.m<0){yyDiv.m+=12;YYgoYear(-1)}
     else {if (yyDiv.m>11){yyDiv.m=yyDiv.m-12;YYgoYear(1)}
       else{setTimeout('YYcaldraw(yyDiv.d,yyDiv.m,yyDiv.year)',(document.layers)?'300':'1')}
     }
   YYsetMonth(yyDiv.m,yyDiv.year);
}

function YYcaldraw(ycd,ycm,ycy){//v4.0
  // writing the calendar table
  var yyfnt="<font style=\' font-size:10px\' color='"+yyDiv.yyTextcolor+"' face=\'Arial, sans-serif\'>";
  var myTR = "<tr bgcolor=\'"+yyDiv.yyBgcolor+"\'>";
  var yyatag="<a href='#' style=\"color: "+yyDiv.yyTextcolor+"; text-decoration: none\" ";
  if (document.layers||document.all||document.getElementById){
   var myMonth = YYstrm[ycm];
   var mytxt="<table border=\'0\' cellspacing=\'1\' cellpadding=\'3\' width=\'210\' style=\'background-color:white;\'>";


//This bit changed by Ali Z, so if it doesn't work you know why....
//-----------------------------------------------------------------
//   mytxt+=myTR+"<td colspan='7'>"+yyfnt+yyatag+"'YYgoMonth(-1)'>&lt;&lt;&nbsp;</a>";
//   mytxt+=myMonth;
//   mytxt+=yyatag+"'YYgoMonth(1)'>&nbsp;&gt;&gt;</a>&nbsp;&nbsp;";

   mytxt+=myTR+"<td colspan='7' valign=top>"+yyfnt+"<select name=theMonth id=theMonth onchange='YYgoMonth()' class='iagManagerTable'>";

   for (j=0;j<12;j++){
   	   if (j == yyDiv.m)
		   mytxt+="<option value=\""+j+"\" selected>" + YYstrm[j]+ "</option>";
       else	   		
	  	   mytxt+="<option value=\""+j+"\">" + YYstrm[j]+ "</option>";
   	   }
   mytxt+="</select>&nbsp;&nbsp;";


  mytxt+="<select name=theYear ID=theYear onchange='YYgoYear()' class='iagManagerTable'>";
   
//   mytxt+=yyatag+"'YYgoMonth(1)'>&nbsp;&gt;&gt;</a>&nbsp;&nbsp;";
//   mytxt+=yyatag+"'YYgoYear(-1)'>&lt;&lt;&nbsp;</a>&nbsp;"+ycy+"&nbsp;";
//   mytxt+=yyatag+"'YYgoYear(1)'>&nbsp;&gt;&gt;</a>&nbsp;&nbsp;";


   for (i=1901;i<2050;i++){
   	if (i == yyDiv.year)
   		mytxt+="<option value='"+i+"' selected>"+i+"</option>";
	else
		mytxt+="<option value='"+i+"'>"+i+"</option>";		
	}
	mytxt+="</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	
//------------------------------------------------------------------
//End of Ali modifications
                              
   mytxt+=yyatag+"onClick='YYcalclose()' title='close'><b>[ X ]</b></a></font></td></tr>"+myTR;
   mytxt+="<td>"+yyfnt+"MO</font></td><td>"+yyfnt+"TU</font></td><td>"+yyfnt+"WE</font></td><td>"+yyfnt+"TH</font></td>";
   mytxt+="<td>"+yyfnt+"FR</font></td><td>"+yyfnt+"<font color=beige>SA</font></td><td>"+yyfnt+"<font color=beige>SU</font></font></td></tr>"+myTR;
   for (var i=0;i<=41;i++){
	if ((i > (dom[ycm]+yyW-2))||(i < yyW-1))
		mytxt+="<td>"+yyfnt+"&nbsp;</font></td>";
	else {
		myStr = i-yyW+2;	
		mytxt+="<td ID=\'TD"+i+"\'onmouseover=\'ChangeCalCol(\"TD"+i+"\",\"beige\",\"blue\")\' onmouseout=\'ChangeCalCol(\"TD"+i+"\",\"" +yyDiv.yyBgcolor+ "\",\"" +yyDiv.yyTextcolor+ "\")\' style='cursor:hand; color:"+yyDiv.yyTextcolor+"; font-family:Arial, Verdana; font-size:10px' onClick=\'YYcalclose("+i+")\' title='"+myMonth+" "+myStr+", "+ycy+"'>"+ myStr + "</font></td>";
		}

     if ((i==6) || (i==13) || (i==20) || (i==27) || (i==34)) { mytxt+="</tr>" + myTR }
	 else if (i == 41) { mytxt+="</tr>"}
   }
   mytxt+="</table>";
 }
 if (document.layers){
   with (yyDiv.document){
     open('text/html');
     write(mytxt);
     close();
   }
 }  // end of ns4
 else if (document.all||document.getElementById){
   yyDiv.innerHTML=mytxt;
 } // end of ie4x / dom
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function YYsetDate(){//v4.0
   var myDate = new Date();
//Enhancements by ALI Z
//-----------------------------------------------------------------------------
//Auto go to date in the box
   if (yyDatevar.value != ""){
   	  theString = yyDatevar.value;
      theBits = theString.split("/");
	  myDate.setFullYear(theBits[2],theBits[1]-1,theBits[0]);
	  }
//------------------------------------------------------------------------------   
   yyDiv.year=myDate.getFullYear();   
//   if ((myDate.getFullYear() > 86)&&(myDate.getFullYear() <= 99)) { yyDiv.year= '19' + myDate.getYear() }
//   if ((myDate.getFullYear() > 99)&&(myDate.getFullYear() < 1900)) { yyDiv.year= (1900 + myDate.getYear())+''; }
//   if (myDate.getFullYear() <= 86){ yyDiv.year= '20' + myDate.getYear() }//2000!!
   yyDiv.m =  myDate.getMonth();
   yyDiv.d = myDate.getDate();
   var w = myDate.getDay();

   YYsetMonth(yyDiv.m,yyDiv.year);
   setTimeout('YYcaldraw(yyDiv.d,yyDiv.m,yyDiv.year)',(document.layers)?'300':'1');  
//   YYgoYear(0);
}

function YY_Calendar(YYwhat,YYleft, YYtop,YYformat, YYtextcolor, YYbgcolor){//v4.0
  yyDiv= MM_findObj('Calendar1');
  showHide();	// HIDE ALL LISTBOXES
  yyDiv.yyTextcolor = YYtextcolor;
  yyDiv.yyBgcolor = YYbgcolor;
  yyDatevar = MM_findObj(YYwhat);
  YYsetDate();
  if (document.layers){
    yyDiv.left = YYleft;
    yyDiv.top = YYtop;
    yyDiv.visibility ="show";
  }
  if (document.all){
    yyDiv.style.pixelLeft = YYleft;
    yyDiv.style.pixelTop = YYtop;
    yyDiv.style.visibility = "visible";
  }else
  if (document.getElementById){
    yyDiv.style.left = YYleft;
    yyDiv.style.top = YYtop;
    yyDiv.style.visibility = "visible";
  }
  YYLang=YYformat;
}

//--- HIDE ALL LISTBOXES ----START//
function showHide(theSelectVis)
{ 

var selects = document.all.tags("select");

if (selects.length){

    var result = ''; 
    if(theSelectVis)
    {
        for (var i=0; i<selects.length; i++)
        {
            if(selects[i].wasVisible==true)
                result += selects[i].style.visibility = "visible";
        }
    } else {
        for (var i=0; i<selects.length; i++)
        {
            if(selects[i].style.visibility!="hidden")
            {
                selects[i].wasVisible=true;
                result += selects[i].style.visibility = "hidden";
            } else {
                selects[i].wasVisible=false;
            }
        }
    }
}

}
//--- HIDE ALL LISTBOXES ----END//