var currentSpanElement	= ""; //Needed to track current span element
var menuArray = new Array(); //Tracks what divs are showing so it knows what to hide when a new span is clicked
var otherElements = new Array();		//tracks other spans when hierachy of divs is highlighted
var offsetMenuX	= 148;		//X Distance new menu will be from parent menu
var offsetMenuY	= 2;		//Y Distance increase of new menu compared to parent span
var startDistanceX = -10;       //Sets how far off onMouseOver start element we should go
var startDistanceY = 5;		//Sets how far off onMouseOver start element we should go
var menuOn= false;		//Only one set of menus can be displayed at once
var started= false;		//Used to track if we just started the menus
var clickStart= true;		//Allow an onClick event to start the menu or not here
var clickX= 0;			//Location X of event to start menu
var clickY= 0;			//Location Y of event to start menu
var selectCount	= 0;
var select	= "";
var appletCount	= 0;		//If we have applets, track the # so we can temporarily hide them
var applets= "";		//Tracks the document.all.tags("applet") collection so we can reference it once rather than twice
var openedblock = false;
var whichRowEntered;

function startMenu(menu,thisItem,level, theRow) {	//menu = menu to display,thisItem=coordinates of item to use,level=current depth of menus
	whichRowEntered = theRow;
	if (menuOn == true) {
		window.event.cancelBubble = true;
		hAD();
		return;												//Only allow one menu to be activated at a time
	} else {
		select = document.all.tags("select");
		selectCount = select.length;
		if (selectCount > 0) {
			for (i=0;i<selectCount;i++) {
				select.item(i).style.visibility = "hidden";
			}
		}
		applets = document.all.tags("applet");
		appletCount = applets.length;
		if (appletCount > 0) {
			for (i=0;i<appletCount;i++) {
				applets.item(i).style.visibility = "hidden";
			}
		}
		menuOn = true;			
		started = true;		//Lets us know we're coming in for the 1st time
		clickX = event.clientX;
		clickY = event.clientY;
		if (clickStart) window.event.cancelBubble = true;
		currentSpanElement = "StandardDiv";		
		Ch(menu,thisItem,level);
	}	
}

function Ch(menu,thisItem,level) {									//menu = menu to display,thisItem=name of span item to use,level=current depth of menus
	if (currentSpanElement != thisItem.id && started != true) {							//Only hit this if they changed span elements	
		if (!openedblock)
			eval("document.all('" + currentSpanElement + "').className = 'Of'");
		openedblock = false;
		thisItem.className = 'On';			
		currentSpanElement = thisItem.id;	//Track where the last mouseover came from
	}
	
	if (menu != "") {
		eMenu = eval("document.all('" + menu + "')");			
		eItem = eval("document.all('" + thisItem.id + "')");	//Used for x,y coordinates
		hD2(level, currentSpanElement.toString());
		theLength = menuArray.length;
		menuArray[theLength] = menu;		//Tracks open menus	
		otherElements[theLength] = thisItem.id.toString();
		for (X=1; X<menuArray.length; X++){
			eval("document.all('" + otherElements[X] + "').className = 'On'");		
			}		
		
		openedblock = true;
		eMenu.style.display="block";
	
		var positionX =  eItem.parentElement.offsetLeft + offsetMenuX;		
		var positionY =   eItem.parentElement.offsetTop + eItem.offsetTop + offsetMenuY;
				
		if (started) {
			positionX =	clickX + startDistanceX	+ document.body.scrollLeft	//eItem.offsetLeft + startDistanceX;
			positionY =	clickY + startDistanceY	+ document.body.scrollTop //+ startDistanceY;
		}

		OSW = eMenu.offsetWidth;
		OSH = eMenu.offsetHeight;
		//If screen isn't wide enough to fit menu, bump menu back to the left some
		if ((positionX + OSW - document.body.scrollLeft) >= document.body.clientWidth) {
			//positionX -= (positionX + eMenu.offsetWidth - document.body.scrollLeft - document.body.clientWidth + (10 * menuArray.length));
			//positionY += 15;
			positionX -= 280;
		}
		//If the menu is too far to the left to display, bump it to the right some
		if ((positionX + OSW) <= OSW)
			positionX += (OSW * 1.3);
		//If the menu is too far down, bump the menu up to the bottom equals the body clientHeight property
		if ((positionY +OSH - document.body.scrollTop) >= document.body.clientHeight)
			positionY -= positionY + OSH - document.body.clientHeight - document.body.scrollTop;

		eMenu.style.left = positionX;
		eMenu.style.top = positionY;
		
	}
	started = false;												//After 1st menu, turn of started variable
}

function hD2(currentLevel,thisSpan) {
		for (var i=currentLevel;i<menuArray.length;i++) {
			var arrayString = new String(menuArray[i]);
			if (arrayString == "undefined") continue;
			eval("document.all('" + menuArray[i] + "').style.display='none'");
			if (thisSpan != otherElements[i])
				eval("document.all('" + otherElements[i] + "').className='Of'");			
		}
			menuArray.length = currentLevel;
			otherElements.length = currentLevel;
}

function hD(currentLevel) {
		for (var i=currentLevel;i<menuArray.length;i++) {
			var arrayString = new String(menuArray[i]);
			if (arrayString == "undefined") continue;
			eval("document.all('" + menuArray[i] + "').style.display='none'");
			eval("document.all('" + otherElements[i] + "').className='Of'");			
		}
			menuArray.length = currentLevel;
			otherElements.length = currentLevel;
}

function hAD() {
	if (menuOn == true) {	//Don't loop through document elements if they clicked a hyperlink since it wastes time
		for (var i=0;i<menuArray.length;i++) {
			var arrayString = new String(menuArray[i]);
			if (arrayString == "undefined") continue;
			document.all(menuArray[i]).style.display = "none";
			document.all(menuArray[i]).style.left = 0;
			document.all(menuArray[i]).style.top = 0;
		}
		for (X=1; X<otherElements.length; X++){
			tempItem = eval("document.all('" + otherElements[X] + "')");
			tempItem.className= "Of";
			}		
		if (currentSpanElement != "") {	//No currentSpanElement if they haven't mousedOver any span element
			eItem = eval("document.all('" + currentSpanElement + "')");
			eItem.className = "Of";	
			menuArray = new Array();
			otherElements = new Array();	
			currentSpanElement = "";		
		}
		if (selectCount > 0) {
			for (i=0;i<selectCount;i++) {
				select.item(i).style.visibility = "visible";
			}
			selectCount = 0;
			select = "";
		}
		if (appletCount > 0) {
			for (i=0;i<appletCount;i++) {
				applets.item(i).style.visibility = "visible";
			}
			appletCount = 0;
			applets = "";
		}
	}
	menuOn = false;		//Menus off, so set this to false
}


function RT(){
	event.cancelBubble = true;
	}

function SnD(iValue,iName){
	document.getElementById("projectid" + whichRowEntered).value = iValue;
	document.getElementById("projectname" + whichRowEntered).innerHTML = iName;	
	checkLine(whichRowEntered);
	}

	
document.onclick = hAD;
