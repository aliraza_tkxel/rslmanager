<!--#include virtual="autologout.asp" -->
<HTML>
<HEAD>
<TITLE>iag Finance </TITLE>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>iag My Job</TITLE>
<link rel="stylesheet" href="/css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<!--#include file="menu/iagFINANCE.INC"-->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include file="menu/BodyTop.html" -->
      <table width="100%" border="0" cellspacing="5" cellpadding="0" >
        <tr class="iagManagerSmallBlk">
          <td width="35%"><br><img src="../myImages/HoldingGraphics/finances.gif" width="295" height="283"></td>
          <td width="65%" valign="top">
            <p class="iagManagerSmallBlk"><br><br>The Finance Module drives the allocation 
              of resources, the establishment of budgets and spending plans, monitors 
              and controls actual spending and expense allocations and delivers 
              essential MI on the progress and impact of each item of expenditure.</p>
            <p>Working alongside all other modules the Finance Module ensures:</p>
            <ul>
              <li> Standard approaches to the fund creation and budget allocation 
                procedures</li>
              <li> Standard purchase ordering processes and authorisation for 
                recording and monitoring</li>
              <li>Automatic allocation of purchases to funds and budgets</li>
              <li>Recording of customer information identified to specific projects, 
                budgets and funds</li>
              <li>Real time reporting on planned and actual expenditure for individual 
                or collective funds</li>
              <li>Real time reporting on individual budgets within specified funds</li>
              <li>Constant management information on financial performance of 
                the Partnership </li>
            </ul>
          </td>
        </tr>
      </table>
<!--#include file="../include/BodyBottom.html" -->
</BODY>
</HTML>