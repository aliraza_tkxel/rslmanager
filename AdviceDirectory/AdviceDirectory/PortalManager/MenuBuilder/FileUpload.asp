<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
dim intCodeId, LevelID

intCodeId = request("codeid")
'response.write intCodeId
LevelID = request("LevelID")
'response.write LevelID
%>
<html>
<head>
<title>File Upload</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/iagManager.css" type="text/css">
</head>
<script LANGUAGE="JavaScript">
function Uploadfile(){
	if(document.thisForm.txt_name.value == '')
		{
			alert("You must enter a name for the file you are uploading.")
			return false
		}
	if(document.thisForm.Upload.value == '')
		{
			alert("You must enter a name for the file you are uploading.")
			return false
		}

	thisForm.action = "ServerSide/UploadThefile_srv.asp";
	thisForm.submit();
}
</script>
<body bgcolor="#FFFFFF" text="#000000" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<form name="thisForm" enctype="multipart/form-data" method=POST>
<table width="500" class="iagManagerSmallBlk">
  <tr>
    <td class="iagManagerSmallBlk">Name:</td>
    <td><input type="text" name="txt_name" class="iagContentButton"></td>
  </tr>
  <tr>
    <td class="iagManagerSmallBlk">File:</td>
    <td><input type="file" name="Upload" class="iagContentButton"></td>
  </tr>
   <tr>
    <td class="iagManagerSmallBlk"><input type="hidden" name="codeid" value=<%=intCodeId%>>
	<input type="hidden" name="LevelID" value=<%=LevelID%>></td>
    <td><input type="button" value="ADD" onclick="Uploadfile()" class="iagContentButton"></td>
  </tr>
</table>
</form>
</body>
</html>
