<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"

%>
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<%
%>
<html>
<head>
<link rel="StyleSheet" href="css/dtree.css" type="text/css" />
<script type="text/javascript" src="js/dtree.js"></script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #FF9933; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #FF9933; scrollbar-shadow-color: #FF9933; scrollbar-highlight-color: #FF9933; scrollbar-darkshadow-color: #FF9933;">
<div class="dtree" style='height:100%'> 
  <script type="text/javascript">
		<!--
		d = new dTree('d');
		d.add(0,-1,'Menu Builder - ACTIVE MENUS');
<%
dim rstChild, rstCodes, rstBudgets, fundi, codei, budgeti, headi

strSQL = "SHAPE 	{ "&_
	"SELECT NP_MENUID, NP_MENUDESC FROM NEXTSTEP_PORTAL_MENU WHERE ACTIVE = 1 ORDER BY NP_MENUDESC "&_
	"}  "&_
	"APPEND	( "&_
			"( "&_
			"SHAPE 	{ "&_
				"SELECT NP_LEVEL1ID, NP_LEVEL1DESC, NP_MENUID FROM NEXTSTEP_PORTAL_MENU_LEVEL1 ORDER BY NP_LEVEL1DESC "&_
				"} "&_		
				"APPEND	( "&_
						"( "&_
						"SHAPE 	{ "&_
							"SELECT NP_LEVEL2ID, NP_LEVEL2DESC, NP_LEVEL1ID FROM NEXTSTEP_PORTAL_MENU_LEVEL2 ORDER BY NP_LEVEL2DESC "&_
							"} "&_
							"APPEND	( "&_
									"{ "&_
									"SELECT NP_LEVEL3ID, NP_LEVEL3DESC, NP_LEVEL2ID FROM NEXTSTEP_PORTAL_MENU_LEVEL3 ORDER BY NP_LEVEL3DESC "&_
									"} "&_
							"AS BUDGET RELATE NP_LEVEL2ID TO NP_LEVEL2ID "&_
						") "&_
					") "&_
				"AS CODES RELATE NP_LEVEL1ID TO NP_LEVEL1ID "&_
			") "&_
		") "&_
	"AS HEADS RELATE NP_MENUID TO NP_MENUID "&_
	") "


' Open original recordset
Set rst = Server.CreateObject("ADODB.Recordset")
rst.Open strSQL, DSN_DATASHAPING_CONNECTION_STRING

fundi = 1
currenti = 5
headparent = 0
codeparent = 0
budgetparent = 0

Do While Not rst.EOF

	Response.Write "d.add(" & currenti & ",0,'" & rst("NP_MENUDESC") & "','serverFund.asp?fundid=" & rst("NP_MENUID") & "&FUND_ACT=LOAD','','FundBuilder',false,'imgBook.gif');" & vbCrLf
	currenti = currenti + 1
	
    ' Set object to child recordset and iterate through
    Set rstChild = rst("heads").Value
    if not rstChild.EOF then
		headparent = currenti - 1
        Do While Not rstChild.EOF
		
		//'for each x in rstChild.Fields 
		//'	Response.Write ":" & x.name & ":"
		//'next  
//HeadName = Level2DESC
//HeadID = Level2ID

			Response.Write "d.add(" & currenti & "," & headparent & ",'" & rstChild("NP_LEVEL1DESC") & "','serverHead.asp?headid=" & rstChild("NP_LEVEL1ID") & "&HEAD_ACT=LOAD','','FundBuilder');" & vbCrLf
			currenti = currenti + 1			
			
			Set rstCodes = rstChild("codes").Value
			if not rstCodes.EOF then
				codeparent = currenti - 1			
				Do While Not rstCodes.EOF
				
				//CodeNo= Level3DESC
				//CodeID= Level3ID				
				
					Response.Write "d.add(" & currenti & "," & codeparent & ",'" & rstCodes("NP_LEVEL2DESC") & "','serverCode.asp?codeid=" & rstCodes("NP_LEVEL2ID") & "&CODE_ACT=LOAD','" & rstCodes("NP_LEVEL2DESC") & "','FundBuilder');" & vbCrLf
					currenti = currenti + 1

					Set rstBudgets = rstCodes("Budget").Value
					if not rstBudgets.EOF then
						budgetparent = currenti - 1
						Do While Not rstBudgets.EOF
						
						//allocationname = Level4DESC
						//BudgetID = Level4ID					
						
							Response.Write "d.add(" & currenti & "," & budgetparent & ",'" & rstBudgets("NP_LEVEL3DESC") & "','serverBudget.asp?budgetid=" & rstBudgets("NP_LEVEL3ID") & "&BUDGET_ACT=LOAD','','FundBuilder');" & vbCrLf
							currenti = currenti + 1
				
							rstBudgets.MoveNext
						Loop							 							
						
						Response.Write "d.add(" & currenti & "," & budgetparent & ",'<font color=blue>New Menu (Level 4)</font>','serverBudget.asp?codeid=" & rstCodes("NP_LEVEL2ID") & "&fundid=" & rst("NP_MENUID") & "&BUDGET_ACT=LOADFUNDDATA','','FundBuilder');" & vbCrLf
						currenti = currenti + 1						
					else
						budgetparent = currenti - 1
						Response.Write "d.add(" & currenti & "," & budgetparent & ",'<font color=blue>New Menu (Level 4)</font>','serverBudget.asp?codeid=" & rstCodes("NP_LEVEL2ID") & "&fundid=" & rst("NP_MENUID") & "&BUDGET_ACT=LOADFUNDDATA','','FundBuilder');" & vbCrLf
						currenti = currenti + 1	
					end if		

					rstCodes.MoveNext
				Loop				
				
				Response.Write "d.add(" & currenti & "," & codeparent & ",'<font color=blue>New Menu (Level 3)</font>','serverCode.asp?headid=" & rstChild("NP_LEVEL1ID") & "&CODE_ACT=LOADFUNDDATA','','FundBuilder');"
				currenti = currenti + 1				
			else
				codeparent = currenti - 1
				Response.Write "d.add(" & currenti & "," & codeparent & ",'<font color=blue>New Menu (Level 4)</font>','serverCode.asp?headid=" & rstChild("NP_LEVEL1ID") & "&CODE_ACT=LOADFUNDDATA','','FundBuilder');"
				currenti = currenti + 1	
			end if
			
            rstChild.MoveNext
        Loop
		Response.Write "d.add(" & currenti & "," & headparent & ",'<font color=blue>New Menu (Level 2)</font>','serverHead.asp?fundid=" & rst("NP_MENUID") & "&HEAD_ACT=LOADFUNDDATA','','FundBuilder');"
		currenti = currenti + 1		
    else
		headparent = currenti - 1
		Response.Write "d.add(" & currenti & "," & headparent & ",'<font color=blue>New Menu (Level 2)</font>','serverHead.asp?fundid=" & rst("NP_MENUID") & "&HEAD_ACT=LOADFUNDDATA','','FundBuilder');"
		currenti = currenti + 1	
	end if
    rst.MoveNext
Loop
Response.Write "d.add(" & currenti & ",0,'<font color=blue>New Menu (Level 1)</font>','javascript:parent.NewItem(1)');"

//'for each x in rstChild.Fields 
//'Response.Write ":" & x.name & ":"
//'next 


Function AddChild (strSQL)
    dim blnChild, rst2
    Set rst2 = Server.CreateObject("ADODB.Recordset")
    rst2.open strSQL, DSN_DATASHAPING_CONNECTION_STRING
        Response.write(rst2("Name") & "<br>")
        blnChild = rst2("Child")
        rst2.close
        set rst2 = nothing

    if blnChild then
        AddChild(strSQL)
    end if
End Function
%>		
//		d.add(1244,0,'Recycle Bin','javascript:window.alert(43242);','','','','trash.gif');
		d.draw();
		//-->
	</script>
</div>
</body>
</html>
