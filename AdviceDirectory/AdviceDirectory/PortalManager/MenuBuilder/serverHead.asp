<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include file="ADOVBS.INC" -->
<%
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function
	
	Dim treeText
	'Dim Conn
	'Dim Rs
	Dim fundid, headname, active, headid
	
Function OpenRecordSet()
	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open DSN_CONNECTION_STRING
End Function

Function CloseConnections()
	Conn.Close
	Set Conn = Nothing
End Function

'	On Error Resume Next

Function GetFormFields ()
	headname = PCase(Request.Form("headname"))
	if headname = "" then headname = null end if	
	active = Request.Form("headactive")
	if active = "" then active = 0 end if	
	fundid = Request.Form("fundid")
	if fundid = "" then fundid = 0 end if		
End Function

Function NewRecord ()

	GetFormFields()
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL1", Conn, adopenstatic, adLockOptimistic
		Rs.AddNew
		Rs("NP_LEVEL1DESC") = headname
		Rs("NP_MENUID") = fundid
		Rs("ACTIVE") = active 	
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function


Function UpdateRecord (theID)

	GetFormFields()
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT NP_LEVEL1DESC,ACTIVE FROM NEXTSTEP_PORTAL_MENU_LEVEL1 WHERE NP_LEVEL1ID = " & theID & ";",Conn,,adLockOptimistic,adCmdText
		Rs("NP_LEVEL1DESC") = headname
		Rs("ACTIVE") = active 
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function


Function GetData(theID)

	Dim StrEventQuery
		strEventQuery = "SELECT NP_MENUID AS FUNDID, NP_LEVEL1DESC AS headname, ACTIVE " &_
						"FROM 	NEXTSTEP_PORTAL_MENU_LEVEL1 " &_
						"WHERE 	NP_LEVEL1ID = " & theID

	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set Rs = Conn.Execute(strEventQuery)
	
		headname = Rs("HeadName")
		fundid = Rs("fundid")
		active = Rs("ACTIVE")

		Rs.Close
	Set Rs = Nothing
	
End Function


Function DisplayMiniTree(theID)

	Dim StrEventQuery		
		strEventQuery =	"SELECT L0.NP_MENUDESC AS fundname, L1.NP_LEVEL1DESC AS headname " &_
						"FROM 	NEXTSTEP_PORTAL_MENU_LEVEL1 L1 " &_
						"		LEFT JOIN NEXTSTEP_PORTAL_MENU L0 ON L0.NP_MENUID = L1.NP_MENUID " &_
						"WHERE L1.NP_LEVEL1ID = " & theID

	Set Rs4 = Server.CreateObject("ADODB.Recordset")						
	Set Rs4 = Conn.Execute(strEventQuery)
	
		treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Update Menu</u></b></td></tr><tr><td>&nbsp;</td></tr>"
		treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("fundname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
		treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function


Function DisplayMiniTree2(theID)

	Dim StrEventQuery
		strEventQuery = "SELECT NP_MENUDESC AS FUNDNAME FROM NEXTSTEP_PORTAL_MENU WHERE (NP_MENUID = " & theID & ")"
		
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
	
		treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Add New Menu</u></b></td></tr><tr><td>&nbsp;</td></tr>"
		treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("fundname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<font color=red>...</font></td></tr>"
		treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

Function DelRecord (theID)

	Dim StrEventQuery
		strEventQuery = "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL2 WHERE NP_LEVEL1ID = " & theID
		
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)	
		
		if (Rs4.EOF) then
			Set Rs = Server.CreateObject("ADODB.Recordset")
			Set Rs = Conn.Execute ("DELETE FROM NEXTSTEP_PORTAL_MENU_LEVEL1 WHERE NP_LEVEL1ID = " & theID & ";")
			treeText = "Menu deleted successfully."		
		else
			treeText = "Sorry, cannot delete the selected Menu as it is has a child referential integrity constraint. You can set the Menu in-active instead."
		end if
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

ACTION_TO_TAKE = Request("HEAD_ACT")
headID = Request("headid")


OpenRecordSet()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LOADFUNDDATA") Then
	fundid = Request("fundid")	
	DisplayMiniTree2(fundid)	
ElseIf (ACTION_TO_TAKE = "LOAD") Then
	GetData(headID)
	DisplayMiniTree(headID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(headID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(headID)
End If

CloseConnections()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
<% if ACTION_TO_TAKE = "ADD" Then %>
		parent.refreshSideBar();
		parent.setText("New Menu added successfully.",1);			
<% Elseif ACTION_TO_TAKE = "UPDATE" Then %>
		parent.refreshSideBar();
		parent.setText("Menu updated successfully.",1);	
<% Elseif ACTION_TO_TAKE = "LOADFUNDDATA" Then %>
		parent.ResetDiv('HEAD');
		parent.setCheckingArray('HEAD');
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("HEAD",1);				
		parent.thisForm.fundid.value = "<%=fundid%>";
		parent.thisForm.HEAD_ACT.value = "ADD";
		parent.Head.style.display = "block";
<% Elseif ACTION_TO_TAKE = "DELETE" Then %>
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);	
<% Elseif ACTION_TO_TAKE = "LOAD" Then %>
		parent.NewItem(2);
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("HEAD",2);									
		parent.thisForm.headname.value = "<%=headname%>";	
		parent.thisForm.fundid.value = "<%=fundid%>";
		parent.thisForm.headid.value = "<%=headID%>";
		if (<%=active%> == 1)
			{
			parent.thisForm.headactive[0].checked = true;
			}
		else
			{
			parent.thisForm.headactive[1].checked = true;
			}				
		parent.thisForm.HEAD_ACT.value = "UPDATE";
		parent.Head.style.display = "block";
		parent.setCheckingArray('HEAD');
<% End if %>
	}
</script>
</body>
</html>