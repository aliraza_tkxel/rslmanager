<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include file="ADOVBS.INC" -->
<%
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function
	
	Dim treeText
	'Dim Conn
	'Dim Rs
	Dim codeid, codename, codedesc, active, codecreated
	
Function OpenRecordSet()
	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open DSN_CONNECTION_STRING
End Function

Function CloseConnections()
	Conn.Close
	Set Conn = Nothing
End Function

'	On Error Resume Next

Function GetFormFields ()

	codename = PCase(Request.Form("codename"))
	RESPONSE.WRITE "<br>HHH<BR>"& codename &"<BR>DDD<br>"
	if codename = "" then codename = null end if
	active = Request.Form("codeactive")
	if active = "" then active = 0 end if
			
End Function


Function NewRecord ()

	GetFormFields()	
	
	headid = Request.Form("headid")
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL2", Conn, adopenstatic, adLockOptimistic
		Rs.AddNew		
		Rs("NP_LEVEL2DESC") = codename
		Rs("NP_LEVEL1ID") = headid
		Rs("ACTIVE") = active 		
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function


Function UpdateRecord (theID)
	
	GetFormFields()
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL2 WHERE NP_LEVEL2ID = " & theID & ";",Conn,,adLockOptimistic,adCmdText
		Rs("NP_LEVEL2DESC") = codename
		Rs("ACTIVE") = active 
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function

Dim CONTENTPRESENT, CONTENT_ACT, CONTENT, strUploads
Function GetData(theID)

	Dim StrEventQuery
		strEventQuery = "SELECT L2.NP_LEVEL2DESC, L2.ACTIVE, LB.CONTENT AS CONTENT, IsNULL(LB.NP_LEVEL2ID,0) AS CONTENTPRESENT  " &_
						" FROM NEXTSTEP_PORTAL_MENU_LEVEL2 L2 " &_
						" LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL1 L1 ON L1.NP_LEVEL1ID = L2.NP_LEVEL1ID " &_
						" LEFT JOIN NEXTSTEP_LIBRARY LB ON LB.NP_LEVEL2ID = L2.NP_LEVEL2ID " &_
						" WHERE (L2.NP_LEVEL2ID = " & theID & ")"
		
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set Rs = Conn.Execute(strEventQuery)
	response.write strEventQuery
	
	OpenDB()	
	Call BuildSelect(Str_Upload,"sel_Upload","Upload WHERE Level2ID = " & theID,"UploadID,UploadName","UploadName","Please Select",Null,Null,NULL," style='width:115px' tabindex='2' class='iagManagerSmallBlk' ")																
	'CloseDB()
		
		codename 	= Rs("NP_LEVEL2DESC")
		active 		= Rs("ACTIVE")
		CONTENT 	= Rs("CONTENT")
		CONTENTPRESENT = Rs("CONTENTPRESENT")
		'string to hold select box e.g. strUploads
		
		strUploads = Str_Upload
		If CONTENTPRESENT = 0 Then 
			CONTENT_ACT = "ADD" 
			'CONTENT = ""
		Else 
			CONTENT_ACT = "UPDATE"
			CONTENT = Replace((CONTENT), Chr(13), "<br>")
		End If 
	
		Rs.Close
	Set Rs = Nothing
	
End Function	

Function DisplayMiniTree(theID)

	Dim StrEventQuery
		strEventQuery =	"SELECT L0.NP_MENUDESC AS FUNDNAME, L1.NP_LEVEL1DESC AS headname, L2.NP_LEVEL2DESC AS codedesc " &_
						"FROM	NEXTSTEP_PORTAL_MENU_LEVEL2 L2 " &_
						"	LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL1 L1 ON L1.NP_LEVEL1ID = L2.NP_LEVEL1ID " &_
						"	LEFT JOIN NEXTSTEP_PORTAL_MENU L0 ON L0.NP_MENUID = L1.NP_MENUID " &_
						"WHERE 	L2.NP_LEVEL2ID = " & theID
		

	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
	
		treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Update Menu</u></b></td></tr><tr><td>&nbsp;</td></tr>"
		treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("fundname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("codedesc") & "</td></tr>"
		treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

Function DisplayMiniTree2(theID)

	Dim StrEventQuery				
		strEventQuery =	"SELECT L0.NP_MENUDESC AS FUNDNAME, L1.NP_LEVEL1DESC AS HEADNAME " &_
						"FROM 	NEXTSTEP_PORTAL_MENU_LEVEL1 L1 " &_
						"	LEFT JOIN NEXTSTEP_PORTAL_MENU L0 ON L0.NP_MENUID = L1.NP_MENUID " &_
						"WHERE 	L1.NP_LEVEL1ID = " & theID
						
	Set Rs4 = Server.CreateObject("ADODB.Recordset")						
	Set Rs4 = Conn.Execute(strEventQuery)
	
		treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Add New Menu</u></b></td></tr><tr><td>&nbsp;</td></tr>"
		treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("fundname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<font color=red>...</font></td></tr>"
		treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

Function DelRecord (theID)

	Dim StrEventQuery
		strEventQuery = "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL3 WHERE NP_LEVEL2ID = " & theID
		
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)	
		
		if (Rs4.EOF) then
			Set Rs = Server.CreateObject("ADODB.Recordset")
			Set Rs = Conn.Execute ("DELETE FROM NEXTSTEP_PORTAL_MENU_LEVEL2 WHERE NP_LEVEL2ID = " & theID & ";")
			treeText = "Menu deleted successfully."		
		else
			treeText = "Sorry, cannot delete the selected Menu as it is has a child referential integrity constraint. You can set the Menu in-active instead."
		end if
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function
	
ACTION_TO_TAKE = Request("CODE_ACT")
RESPONSE.WRITE ACTION_TO_TAKE
codeID = Request("codeID")

OpenRecordSet()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LOADFUNDDATA") Then
	headid = Request("headid")
	DisplayMiniTree2(headid)
ElseIf (ACTION_TO_TAKE = "LOAD") Then
	GetData(codeID)
	DisplayMiniTree(codeID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(codeID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(codeID)
End If

CloseConnections()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<DIV id="idContent" contenteditable style="width:100%; height:400px; background-color:white; font-face:Arial; padding:3; border: 1px solid threedface; overflow=auto;"><%=CONTENT%></DIV>
<DIV id="UploadDIV" class="iagManagerSmallBlk"><%=strUploads%></DIV>

<script language=javascript defer>
function returnData(){
<% if ACTION_TO_TAKE = "ADD" Then %>
		parent.refreshSideBar();
		parent.setText("New Menu added successfully.",1);		
<% Elseif ACTION_TO_TAKE = "UPDATE" Then %>
		parent.refreshSideBar();
		parent.setText("Menu has been updated successfully.",1);			
<% Elseif ACTION_TO_TAKE = "LOADFUNDDATA" Then %>
		parent.ResetDiv('CODE');
		parent.setText("<%=treeText%>");		
		parent.setCheckingArray('CODE');
		parent.showDeleteButton("CODE",1);		
		parent.thisForm.headid.value = "<%=headid%>";
		parent.thisForm.CODE_ACT.value = "ADD";
		parent.Code.style.display = "block";
<% Elseif ACTION_TO_TAKE = "DELETE" Then %>		
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);	
<% Elseif ACTION_TO_TAKE = "LOAD" Then %>
		
		parent.setText("<%=treeText%>");					
		parent.NewItem(3);
		parent.showDeleteButton("CODE",2);		
		parent.thisForm.codeid.value = "<%=codeid%>";
		parent.thisForm.codename.value = "<%=codename%>";
		parent.UploadDIV.innerHTML = UploadDIV.innerHTML
		parent.thisForm.CODE_ACT.value = "UPDATE";
	
		
		parent.thisForm.CONTENT_ACT.value = "<%=CONTENT_ACT%>";
		parent.idContent.outerHTML = idContent.outerHTML;		
		parent.setCheckingArray('CODE');
		if (<%=active%> == 1)
			{
			parent.thisForm.codeactive[0].checked = true;
			}
		else
			{
			parent.thisForm.codeactive[1].checked = true;
			}
<% End if %>
	}
</script>
</body>
</html>