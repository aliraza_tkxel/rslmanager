<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include file="ADOVBS.INC" -->
<%
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText	
	'Dim Conn
	'Dim Rs
	Dim fundid, fundname, active
	
Function OpenRecordSet()
	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open DSN_CONNECTION_STRING
End Function

Function CloseConnections()
	Conn.Close
	Set Conn = Nothing
End Function

'	On Error Resume Next

Function GetFormFields ()
	
	fundname = PCase(Request.Form("fundname"))
	if fundname = "" then fundname = null end if	
	
	active = Request.Form("fundactive")
	if active = "" then active = 0 end if	
		
End Function


Function NewRecord()

	GetFormFields()
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT NP_MENUDESC, ACTIVE FROM NEXTSTEP_PORTAL_MENU", Conn, adopenstatic, adLockOptimistic
		Rs.AddNew	
		Rs("NP_MENUDESC") = fundname 
		Rs("ACTIVE") = active 
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function


Function UpdateRecord (theID)

	GetFormFields()

	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT NP_MENUDESC, ACTIVE FROM NEXTSTEP_PORTAL_MENU WHERE NP_MENUID = " & theID & ";",Conn,,adLockOptimistic,adCmdText
		Rs("NP_MENUDESC") = fundname 
		Rs("ACTIVE") = active 
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function


Function GetData(theID)

	Dim StrEventQuery
		strEventQuery = "SELECT NP_MENUDESC, ACTIVE " &_
						"FROM NEXTSTEP_PORTAL_MENU " &_
						"WHERE (NP_MENUID = " & theID & ")"
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set Rs = Conn.Execute(strEventQuery)

		fundname = Rs("NP_MENUDESC")
		active = Rs("ACTIVE")
			
		Rs.Close
	Set Rs = Nothing
	
End Function
	

Function DelRecord (theID)

	Dim StrEventQuery
		strEventQuery = "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL1 WHERE NP_MENUID = " & theID
		
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
		
		if (Rs4.EOF) then
			Set Rs = Server.CreateObject("ADODB.Recordset")
			Set Rs = Conn.Execute ("DELETE FROM NEXTSTEP_PORTAL_MENU WHERE NP_MENUID = " & theID & ";")
			treeText = "Menu deleted successfully."		
		else
			treeText = "Sorry, cannot delete the selected Menu as it is has a child referential integrity constraint. You can set the Menu in-active instead."
		end if
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

ACTION_TO_TAKE = Request("FUND_ACT")
fundID = Request("fundid")

OpenRecordSet()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LOAD") Then
	GetData(fundID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(fundID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(fundID)
End If

CloseConnections()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
<% if ACTION_TO_TAKE = "ADD" Then %>
		parent.refreshSideBar();
		parent.setText("New Menu added successfully.",1);		
<% Elseif ACTION_TO_TAKE = "UPDATE" Then %>
		parent.refreshSideBar();
		parent.setText("Menu has been updated successfully.",1);			
<% Elseif ACTION_TO_TAKE = "DELETE" Then %>
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);
<% Elseif ACTION_TO_TAKE = "LOAD" Then %>
		parent.NewItem(1);
		parent.showDeleteButton("FUND",2);		
		parent.setText("<table class='iagManagerSmallBlk' width=380px><tr><td><b><u>Update Menu</u></b></td></tr></table>");			
		parent.thisForm.fundname.value = "<%=fundname%>";
		parent.thisForm.fundid.value = "<%=fundid%>";		
		parent.thisForm.FUND_ACT.value = "UPDATE";
		if (<%=active%> == 1)
			{
			parent.thisForm.fundactive[0].checked = true;
			}
		else
			{
			parent.thisForm.fundactive[1].checked = true;
			}		
		parent.setCheckingArray('FUND');
<% End if %>
	}
</script>
</body>
</html>