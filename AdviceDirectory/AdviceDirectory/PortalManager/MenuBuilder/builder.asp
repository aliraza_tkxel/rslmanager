<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	 
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<%
	

'Retrieve all first and last names from the userdate table
dim Fullname
dim strNameOptions
dim OptionValue

	strQuery3 = "SELECT UserFName, UserSName, WUserid FROM userdata where SupID = 215 AND Active=1 ORDER BY UserFName,UserSName "

Set objConn3 = Server.CreateObject("ADODB.Connection")
	objConn3.Open DSN_CONNECTION_STRING
Set objRs3 = objConn3.Execute(strQuery3)

	strNameOptions = "<OPTION VALUE="""">Please Select</OPTION>"
	strNameOptions = strNameOptions & "<OPTION VALUE=""ALL"">All Users</OPTION>"
	strFullname = ""
	OptionValue = 0

	Do While Not objRs3.EOF
		Fullname = objRs3("UserFName") & " " &  objRs3("UserSName")
		strNameOptions = strNameOptions & "<OPTION VALUE=" & objRs3("WUserid") & ">" & Fullname & "</OPTION>"
		OptionValue = OptionValue + 1
	objRs3.MoveNext
	Loop
	
	objRs3.close
Set objRs3 = Nothing

SQL = "SELECT * FROM UPLOAD "

Set objUpload = Server.CreateObject("ADODB.Connection")
	objUpload.Open DSN_CONNECTION_STRING
Set objRs4 = objUpload.Execute(SQL)
%>
<HTML>
<HEAD>
<title>iag Portal Menu Builder</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/iagManager.css" type="text/css">
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<STYLE TYPE="text/css">
<!--
TABLE#tblCoolbar 
	{ 
	background-color:beige; padding:1px; color:menutext; 
	border-width:1px; border-style:solid; 
	border-color:beigehighlight beigeshadow beigeshadow beigehighlight;
	}
.cbtn
	{
	height:18;
	BORDER-LEFT: threedface 1px solid;
	BORDER-RIGHT: threedface 1px solid;
	BORDER-TOP: threedface 1px solid;
	BORDER-BOTTOM: threedface 1px solid; 
	cursor: pointer;
	}
.txtbtn {font-family:tahoma; font-size:70%; color:menutext;}
//-->
</STYLE>
<STYLE TYPE="TEXT/CSS">
<!--
.Btn {font-family: Arial, Helvetica, sans-serif; font-size: 8pt; font-style: normal; background-color: threedface; border: 1px #F4F3F7 ridge; color: #FFFFFF; cursor: hand}
TABLE.REPORTING_TABLE {font-family:tahoma; font-size:10px; font-weight:normal;}
.CO{background-color:#EEEEEE;COLOR:#000000}
TABLE.REPORTING_TABLE td{font-family:tahoma; font-size:10px; font-weight:normal;}
.noOverFlow (text-overflow:absolute; overflow:hidden;)
//-->
</STYLE>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="js/calendarFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/formValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../includes/ClientScripting/js/General.js"></SCRIPT>
<script language=javascript>
var FormFields 

function refreshSideBar(){
	thisForm.action = "serverTree.asp";
	thisForm.target = "theSideBar";
	thisForm.submit();
	}

function setText(txt,hideallboolean){
TheText.innerHTML = txt;
if (hideallboolean == 1) {
	Budget.style.display = "none";
	Code.style.display = "none";
	Head.style.display = "none";
	Fund.style.display = "none";
	}
TheText.style.display = "block";
}

function DoCancel(){
	setText("Please select an item you would like to edit...",1);
}
	
function ResetDiv(what){
Head.style.display = "none";
Fund.style.display = "none";
Budget.style.display = "none";
Code.style.display = "none";
Content.style.display = "none";
NEXTSTEP_DIV_CONTACTS.style.display = 'none';
NEXTSTEP_DIV_LINKS.style.display = 'none';

if (what == "HEAD")
	temp = new Array("headname");
else if (what == "BUDGET"){
	temp = new Array("budgetname");
	}
else if (what == "CODE")
	temp = new Array("codename");
else if (what == "FUND")
	temp = new Array("fundname", "fundid");
	doc = document.all;
	for (i=0; i<temp.length; i++)
		doc[temp[i]].value = "";
	}

function setCheckingArray(what){
if (what == "HEAD") {
	FormFields = new Array("headname|Menu Name|TEXT|Y");		
	}
else if (what == "BUDGET"){
	FormFields = new Array("budgetname|Menu Name|TEXT|Y");	
	}
else if (what == "CODE"){
	FormFields = new Array("codename|Menu Name|TEXT|Y");	
	}
else if (what == "FUND"){
	FormFields = new Array("fundname|Menu Name|TEXT|Y");	
	}
else if (what == "CONTACTS"){
	//FormFields = new Array("fundname|Menu Name|TEXT|Y");	
	FormFields = new Array("txt_CONTACT_NAME|Contact Name|TEXT|Y", "txt_CONTACT_ADDRESS|Contact Address|TEXT|Y", "txt_CONTACT_POSTCODE|Contact Postcode|TEXT|Y", "txt_CONTACT_TEL|Contact Tel|TEXT|Y", "txt_CONTACT_FAX|Contact Fax|TEXT|Y", "txt_CONTACT_EMAIL|Contact Email|TEXT|Y");
	}
}

	
function NewItem(what, item1){
if (what == 2){
	ResetDiv('HEAD');
	setCheckingArray('HEAD');
	Head.style.display = "block";
	thisForm.fundid.value = item1;
	thisForm.HEAD_ACT.value = "ADD";	
	}
else if (what == 4){
	ResetDiv('BUDGET');
	setCheckingArray('BUDGET');	
	Budget.style.display = "block";
	thisForm.codeid.value = item1;
	thisForm.BUDGET_ACT.value = "ADD";	
	}
else if (what == 3){
	ResetDiv('CODE');
	setCheckingArray('CODE');	
	Code.style.display = "block";
	thisForm.headid.value = item1;	
	thisForm.CODE_ACT.value = "ADD";	
	}
else if (what == 1){
	showDeleteButton("FUND",1);
	ResetDiv('FUND');
	setCheckingArray('FUND');
	setText("<table class='iagManagerSmallBlk' width=380px><tr><td><b><u>Add New Menu</u></b></td></tr></table>")	
	Fund.style.display = "block";
	thisForm.FUND_ACT.value = "ADD";
	}
}

function BtnStatus(what){
if (what == "CONTACT_NEW") {
	document.getElementById("CONTACT_DELETE").style.display = "none";
	temp = new Array("txt_CONTACTID", "txt_CONTACT_NAME", "txt_CONTACT_ADDRESS", "txt_CONTACT_POSTCODE", "txt_CONTACT_TEL", "txt_CONTACT_FAX", "txt_CONTACT_EMAIL");
	doc = document.all;
	for (i=0; i<temp.length; i++)
		doc[temp[i]].value = "";
	doc.CONTACTS_ACT.value = "New";	
	}
}

function showDeleteButton(what, theStatus){
doc = document.all;

//NEXTSTEP_DIV_CONTACTS.style.display = "none";
//NEXTSTEP_DIV_LINKS.style.display = "none";

if (what == "HEAD") {
	if (theStatus == 1) 
		HEAD_DELETE.style.display = "none";
	else
		HEAD_DELETE.style.display = "block";
	}
else if (what == "BUDGET"){
	if (theStatus == 1) {
			BUDGET_DELETE.style.display = "none";
			NEXTSTEP_LIB.style.display = "none";
		}
	else {
			BUDGET_DELETE.style.display = "block";
			NEXTSTEP_LIB.style.display = "block";		
		}
	}
else if (what == "CODE"){
	if (theStatus == 1) 
		CODE_DELETE.style.display = "none";
	else
		CODE_DELETE.style.display = "block";
	}
else if (what == "FUND"){
	if (theStatus == 1) 
		FUND_DELETE.style.display = "none";
	else
		FUND_DELETE.style.display = "block";
	}
}

function DoDelete(what){
if (what == "HEAD") {
	thisForm.action = "serverHead.asp";
	thisForm.HEAD_ACT.value = "DELETE";		
	}
else if (what == "BUDGET"){
	thisForm.action = "serverBudget.asp";
	thisForm.BUDGET_ACT.value = "DELETE";		
	}
else if (what == "CODE"){
	thisForm.action = "serverCode.asp";
	thisForm.CODE_ACT.value = "DELETE";	
	}
else if (what == "FUND"){
	thisForm.action = "serverFund.asp";
	thisForm.FUND_ACT.value = "DELETE";	
	}
else if (what == "CONTACTS"){
	thisForm.action = "serverContacts.asp";
	thisForm.CONTACTS_ACT.value = "DELETE";	
	}
thisForm.target = "FundBuilder";	
thisForm.submit();
}

function DoSave(what){
if (what == "HEAD") {
	thisForm.action = "serverHead.asp";	
	}
else if (what == "BUDGET"){
	thisForm.action = "serverBudget.asp";
	}
else if (what == "CODE"){
	thisForm.action = "serverCode.asp";
	}
else if (what == "FUND"){
	thisForm.action = "serverFund.asp";
	}
else if (what == "CONTACTS"){
	setCheckingArray(what)
	thisForm.action = "serverContacts.asp";
	}
	//alert(FormFields)
	if (checkForm()) {
		thisForm.target = "FundBuilder";	
		thisForm.submit();
		}
}

function DoReset(what){
if (what == "HEAD")
	temp = new Array("headname");
else if (what == "BUDGET")
	temp = new Array("budgetname");
else if (what == "CODE")
	temp = new Array("codename");
else if (what == "FUND")
	temp = new Array("fundname");
	doc = document.all;
	for (i=0; i<temp.length; i++)
		doc[temp[i]].value = "";
}
	
function ContentEdit(budgetid) {
var MyAction = PCase(document.getElementById("CONTENT_ACT").value)
	document.getElementById("idContent").value = ""
	document.getElementById("inpCONTENT").value = ""
	document.getElementById("TheText").innerHTML=document.getElementById("TheText").innerHTML.replace(/Update Menu/g,MyAction+" Content");
	document.getElementById("ContentID").value = budgetid
	Budget.style.display = 'none'; 
	NEXTSTEP_DIV_LINKS.style.display = 'none'; 
	NEXTSTEP_DIV_CONTACTS.style.display = 'none';
	Content.style.display = 'block'; 

}

function ContentCodeEdit(codeid) {
var MyAction = PCase(document.getElementById("CONTENT_ACT").value)
	document.getElementById("idContent").value = ""
	document.getElementById("inpCONTENT").value = ""
	document.getElementById("TheText").innerHTML=document.getElementById("TheText").innerHTML.replace(/Update Menu/g,MyAction+" Content");
	document.getElementById("ContentID").value = codeid
	Code.style.display = 'none'; 
	NEXTSTEP_DIV_LINKS.style.display = 'none'; 
	NEXTSTEP_DIV_CONTACTS.style.display = 'none';
	Content.style.display = 'block'; 
}

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
  xmlhttp = new XMLHttpRequest();
}


function buildQueryString(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}

function JumpPage(str_url){
var iPage = document.getElementById("QuickJumpPage").value
var iPageSize = document.getElementById("PageSize").value
	if (iPage != "" && !isNaN(iPage))
		PageMe(str_url)
	else
		document.getElementById("QuickJumpPage").value = "" 
}

function PageMe(str_url){

displayDiv('LoadingText','block',1)

var strPage = str_url
var strURL = ''+str_url+ '&d='+new Date().valueOf()+''

	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strPage + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							   //alert(strResponse)
							   document.getElementById("LoadingText").innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   	alert("JavaScript Error");

							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');
									   //alert(strResponse)
									   if (strResponse == 0){									   
									   GetMe('/Menubuilder/Serverside/contacts_form_svr.asp?RsID=1')
									   document.getElementById("NEXTSTEP_DIV_CONTACTS").style.display = 'block'								
									   //GetMe("/MenuBuilder/serverside/contacts_form_svr.asp?RsID=" + RsID)
									   }
									   else {									   
									   document.getElementById("NEXTSTEP_DIV_CONTACTS").style.display = 'block'
									   document.getElementById("NEXTSTEP_DIV_CONTACTS").innerHTML = strResponse
							           }
									   displayDiv('LoadingText','none',0)

							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}

function GetMe(str_url){

displayDiv('LoadingText','block',1)

var strPage = str_url
var strURL = ''+str_url+ '&d='+new Date().valueOf()+''

	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
	
	
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  + 
									   strPage + "' could not be found.");
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   //handleErr(strResponse);
							  // alert(strResponse)
							   document.getElementById("LoadingText").innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   	alert("JavaScript Error");

							   }
							   // Call the desired result function
							   else {
									   //eval(strResultFunc + '(strResponse);');
									   //alert(strResponse)
									   document.getElementById("NEXTSTEP_DIV_CONTACTS_FORM").style.display = 'block'
									   document.getElementById("NEXTSTEP_DIV_CONTACTS_FORM").innerHTML = strResponse
							           displayDiv('LoadingText','none',0)
							   }
							   break;
			   }
	   }
	  
 	}
 xmlhttp.send(null) 
  
}

function EditItem(budgetid, itemtype) {

	ResetDiv('BUDGET');	
	PageMe("/MenuBuilder/serverside/contacts_svr.asp" + buildQueryString('thisForm'))
	//thisForm.action = "/MenuBuilder/serverside/contacts_svr.asp"
	//thisForm.target = "FundBuilder";	
	//thisForm.submit();
}

function getData(RsID,TblName)
{
GetMe("/MenuBuilder/serverside/contacts_form_svr.asp?RsID=" + RsID)
}
	
function displayDiv(which,display,loading) {

if (loading = 1){
	document.getElementById(which).innerHTML = "<table cellspacing=0 cellpadding=0 style='border-collapse:collapase;' height=100%' width='100%' border='0'><tr><td valign='middle' align='center'><img src='/myImages/Loading_IAG.gif'></td></tr></table>"
}
else {
}
	 	temp = document.getElementById(which);
		temp.style.display = display ; 
}		
</script>
<script LANGUAGE="JavaScript">
function button_over(eButton)
	{
	eButton.style.backgroundColor = "#B5BDD6";
	eButton.style.borderColor = "darkblue darkblue darkblue darkblue";
	}
function button_out(eButton)
	{
	eButton.style.backgroundColor = "beige";
	eButton.style.borderColor = "threedface";
	}
function button_down(eButton)
	{
	eButton.style.backgroundColor = "#8494B5";
	eButton.style.borderColor = "darkblue darkblue darkblue darkblue";
	}
function button_up(eButton)
	{
	eButton.style.backgroundColor = "#B5BDD6";
	eButton.style.borderColor = "darkblue darkblue darkblue darkblue";
	eButton = null; 
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var isHTMLMode=false

function document.onreadystatechange()
	{
  	idContent.document.designMode="On"
	}
function cmdExec(cmd,opt) 
	{
  	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
  	idContent.document.execCommand(cmd,"",opt);idContent.focus();
	}
function setMode(bMode)
	{
	var sTmp;
  	isHTMLMode = bMode;
  	if (isHTMLMode){sTmp=idContent.document.body.innerHTML;idContent.document.body.innerText=sTmp;} 
	else {sTmp=idContent.document.body.innerText;idContent.document.body.innerHTML=sTmp;}
  	idContent.focus();
	}
function createLink()
	{
	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
	cmdExec("CreateLink");
	}

function Save() 
	{
	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
  //	var sImgTag = idContent.document.body.all.tags("IMG");
  //var oImg;
  //	for (var i = sImgTag.length - 1; i >= 0; i--) 
  //  {
  //  	oImg = sImgTag[i];
  //  	alert("Add your code to Upload local image file here. Image Inserted : " + oImg.src );
  //}
  	//alert("Add your code to Save Document here");
	SaveChat()
  //	alert("Your Document : " + idContent.document.body.innerHTML);
	}
	
function SaveChat() {
		   document.getElementById("inpCONTENT").value = document.getElementById("idContent").innerHTML;
		if (document.getElementById("inpCONTENT").value == "")
			alert("Please enter 'Content'")
		else
		{
			thisForm.target = "FundBuilder";
			thisForm.action = "ServerSide/Builder_svr.asp";
			thisForm.submit();
		}
	}
	
function UploadFile(){
var strURL = "FileUpload.asp?codeid=" + document.thisForm.codeid.value + "&LevelID=" + document.thisForm.hid_Level2.value
//alert(strURL)
window.open (strURL,"mywindow","menubar=0,resizable=0,width=350,height=150"); 
}
function UploadLevel3File(){
var strURL = "FileUpload.asp?codeid=" + document.thisForm.budgetid.value + "&LevelID=" + document.thisForm.hid_Level3.value
//alert(strURL)
window.open (strURL,"mywindow","menubar=0,resizable=0,width=350,height=150"); 
}

function DeleteFile(level){
	if (level == 2)
		{
		if(document.thisForm.sel_Upload.value == '') 
			{
				alert("Please select a document you wish to delete from the drop down.")
				return false
			}
				thisForm.target = "FundBuilder";
				thisForm.action = "ServerSide/DeleteTheFile.asp?leveltype=" + level;
				thisForm.submit();
		}
	else
		{
		if(document.thisForm.sel_Uploadbudget.value == '') 
			{
				alert("Please select a document you wish to delete from the drop down.")
				return false
			}
				thisForm.target = "FundBuilder";
				thisForm.action = "ServerSide/DeleteTheFile.asp?leveltype=" + level;
				thisForm.submit();
		}
}

function DeleteContent(level){
	
			var where_to= confirm("Are you sure you wish to delete the content for this menu?");
 			if (where_to== true)
				{
				thisForm.target = "FundBuilder";
				thisForm.action = "ServerSide/DeleteContent_srv.asp?leveltype=" + level;
				thisForm.submit();
				}
}
</script>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0 HEIGHT=100% BORDERCOLOR=#FFFFFF class="iagManagerSmallBlk">
  <TR> 
    <TD HEIGHT=50px COLSPAN=2 width=772 style="background:#F93;color:white;font-size:medium;padding:0.5em;">nextstep Portal Menu Builderss</TD>
  </TR>
  <TR> 
    <TD HEIGHT=100% STYLE='BORDER-LEFT:1PX SOLID #FF9933' width=300> 
      <table cellspacing=0 cellpadding=0 height=100% width=100%>
        <tr> 
          <td style='border-top:1px solid #FFFFFF;border-right:1px solid #FF9933;' height=100% width=100% valign=top nowrap> 
            <iframe name=theSideBar src=serverTree.asp id=theSideBar height=100% frameborder=none bordercolor=#FFFFFF></iframe> 
          </td>
        </tr>
        <tr> 
          <TD height=1 BGCOLOR=#FFFFFF align=right class="iagManagerSmallWht" valign=middle></TD>
        </tr>
        <tr> 
          <TD height=28 BGCOLOR=#FF9933 align=right class="iagManagerSmallWht" valign=middle><b>&nbsp;</b></TD>
        </tr>
      </table>
    </TD>
    <td HEIGHT=100% width=476> 
      <form name="thisForm" method="post">
        <table cellspacing=0 cellpadding=0 height=100% width=100%>
          <tr> 
            <td width=100% colspan=2 STYLE='BORDER-RIGHT:1PX SOLID #FF9933' align=center valign=top class="iagManagerSmallBlk"> 
              <br>
              <div id=everything>	  
			  <font color=blue><div id=TheText></div></font>
			  <div id="NEXTSTEP_DIV_LINKS" style="display:none">Links</div>
			  <div id="LoadingText"></div> 
			  <div id="NEXTSTEP_DIV_CONTACTS" style="display:none">
			  	<table width="100%" height="100%" cellspacing=0 cellpadding=0 class="iagManagerSmallBlk" style="border-top:1px solid #FF9933; border-bottom:1px solid #FF9933;background-color:beige">
					<tr>
						<td align="left" valign="top">
							<table width="100%" border="0">
								<tr><td colspan="5">&nbsp;</td></tr>
								<tr VALIGN=TOP> 
									<td width="10"></td>
									<td width="150" align="left" valign="top">
									<div id="NEXTSTEP_DIV_CONTACTS_LIST">
										<table width="150"><tr><td>&nbsp;</td></tr></table>
									</div>
									</td>
									<td width="10"></td>
									<td style="width:100%; background-color:white; font-face:Arial; padding:3; border: 1px solid threedface">
									<div id="NEXTSTEP_DIV_CONTACTS_FORM" style="display:none"></div></td>
									<td width="10"></td>
								</TR>
							</table>
						</td>
					</tr>
				</table>
			  </div>
			  <div id="Content" style='display:none'>			  	
	              <table width="100%" cellspacing=0 cellpadding=0 class="iagManagerSmallBlk" style="border-bottom:1px solid #FF9933;background-color:beige">
                    <tr VALIGN=TOP> 
                      <td style='border-top:1px solid #FF9933' colspan=3>&nbsp;</td>
                    </TR>
                    <tr style='height:5px'> 
                      <td></td>
                    </TR>
                    <tr VALIGN=TOP> 
                      <td width=70></td>
                      <td align="center"> 
					  <table id="tblCoolbar" width=392 cellpadding="0" cellspacing="0">
                          <tr valign="middle"> 
                            <td colspan=16> <select onChange="cmdExec('formatBlock',this[this.selectedIndex].value);this.selectedIndex=0">
                                <option selected>Style</option>
                                <option value="Normal">Normal</option>
                                <option value="Heading 1">Heading 1</option>
                                <option value="Heading 2">Heading 2</option>
                                <option value="Heading 3">Heading 3</option>
                                <option value="Heading 4">Heading 4</option>
                                <option value="Heading 5">Heading 5</option>
                                <option value="Address">Address</option>
                                <option value="Formatted">Formatted</option>
                                <option value="Definition Term">Definition Term</option>
                              </select> <select onChange="cmdExec('fontname',this[this.selectedIndex].value);">
                                <option selected>Font</option>
                                <option value="Arial">Arial</option>
                                <option value="Arial Black">Arial Black</option>
                                <option value="Arial Narrow">Arial Narrow</option>
                                <option value="Comic Sans MS">Comic Sans MS</option>
                                <option value="Courier New">Courier New</option>
                                <option value="System">System</option>
                                <option value="Tahoma">Tahoma</option>
                                <option value="Times New Roman">Times New Roman</option>
                                <option value="Verdana">Verdana</option>
                                <option value="Wingdings">Wingdings</option>
                              </select> <select onChange="cmdExec('fontsize',this[this.selectedIndex].value);">
                                <option selected>Size</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="10">10</option>
                                <option value="12">12</option>
                                <option value="14">14</option>
                              </select> </td>
                          </tr>
                          <tr> 
                            <td><div class="cbtn" onClick="cmdExec('cut')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="1" vspace=1 align=absmiddle src="images/Editor/Cut.gif" alt="Cut"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('copy')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="1" vspace=1 align=absmiddle src="images/Editor/Copy.gif" alt="Copy"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('paste')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="1" vspace=1 align=absmiddle src="images/Editor/Paste.gif" alt="Paste"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('bold')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="1" vspace=1 align=absmiddle src="images/Editor/Bold.gif" alt="Bold"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('italic')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="1" vspace=1 align=absmiddle src="images/Editor/Italic.gif" alt="Italic"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('underline')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="1" vspace=1 align=absmiddle src="images/Editor/Under.gif" alt="Underline"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('insertorderedlist')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="2" vspace=1 align=absmiddle src="images/Editor/numlist.GIF" alt="Ordered List"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('insertunorderedlist')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="2" vspace=1 align=absmiddle src="images/Editor/bullist.GIF" alt="Unordered List"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('outdent')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="2" vspace=1 align=absmiddle src="images/Editor/deindent.gif" alt="Decrease Indent"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('indent')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="2" vspace=1 align=absmiddle src="images/Editor/inindent.gif" alt="Increase Indent"> 
                              </div></td>
                            <td><div class="cbtn" onClick="cmdExec('createLink')" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="2" vspace=1 align=absmiddle src="images/Editor/Link.gif" alt="Link"> 
                              </div></td>
                            <td><div class="cbtn" onClick="Save()" onMouseOver="button_over(this);" onMouseOut="button_out(this);" onMouseDown="button_down(this);" onMouseUp="button_up(this);"> 
                                <img hspace="2" vspace=1 align=absmiddle src="images/Editor/Save.gif" alt="Save"><font class="txtbtn">Save&nbsp;&nbsp;</font></div></td>
                            <td width=200></td>
                          </tr>
                        </table>
                        <table width="100%" class="iagManagerSmallBlk" height="300">
                          <tr> 
                            <td> <DIV id="idContent" contenteditable style="width:100%; height:400px; background-color:white; font-face:Arial; padding:3; border: 1px solid threedface; overflow=auto;"></DIV></td>
                          </tr>
                        </table></TD>
                      <td width=70></td>
                    </TR>
                    <tr VALIGN=TOP> 
                      <td></td>
                      <td align="center">
					  	<textarea  name="inpCONTENT" id="inpCONTENT" style="display:none"></textarea>
                        <input type="hidden" name="ContentID">
						<input type="hidden" name="CONTENT_ACT">						
                      </TD>
                      <td></td>
                    </TR>
                    <tr VALIGN=TOP style="display:none">
                      <td></td>
                      <td> 
                        <input type="button" class="iagContentButtonSmall" name="btnAmend" value="Amend" onClick="OpenChat()" > 
                        <input type="button" class="iagContentButtonSmall" name="btnSave"  value="Save"  onClick="SaveChat()" style="display:none"> 
                      </TD>
                      <td></td>
                    </TR>
                    <tr VALIGN=TOP> 
                      <td width=70>&nbsp;</td>
                      <td>&nbsp; </td>
                    </TR>
                    <tr style='height:5px'> 
                      <td></td>
                    </TR>
                  </TABLE>	
			  </div>
                <div id=Fund style='display:none'> 
                  <table class="iagManagerSmallblk">
                    <tr> 
                      <td>Menu Name</td>
                      <td> 
                        <input type="text" name="fundname" style="width:260px" maxlength="200" class="iagManagerSmallBlk" onBlur="checkItem()"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>Menu Active</td>
                      <td class="iagManagerSmallBlk">Yes 
                        <input type=radio name="fundactive" value="1" class="iagManagerSmallBlk" checked> 
                        &nbsp;&nbsp;No 
                        <input type=radio name="fundactive" value="0" class="iagManagerSmallBlk"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
					  			<input type="hidden" name="FUND_ACT"> 
                        		<input type="hidden" name="fundid"> 
								<input type="button" name="saveButton" value=' Save ' class="iagContentButton" onClick="DoSave('FUND')"> 
                         	<input type="button" name="resetButton" value=' Reset ' class="iagContentButton" onClick="DoReset('FUND')" STYLE="display:none"> 
                        &nbsp; 	<input type="button" name="cancelButton" value=' Cancel ' class="iagContentButton" onClick="DoCancel()"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> <div id=FUND_DELETE>
                          <input type=button name="FDG" value=' Delete ' class="iagContentButton" onClick="DoDelete('FUND')">
                        </div></td>
                    </tr>
                  </table>
                </div>
                <div id=Head style='display:none'> 
                  <table class="iagManagerSmallblk">
                    <tr> 
                      <td>Menu Name</td>
                      <td> 
                        <input type="text" name="headname" style="width:260px" maxlength="200" class="iagManagerSmallBlk" onBlur="checkItem()"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>Menu Active</td>
                      <td class="iagManagerSmallBlk">Yes 
                        <input type="radio" name="headactive" value="1" class="iagManagerSmallBlk" checked> 
                        &nbsp;&nbsp;No 
                        <input type="radio" name="headactive" value="0" class="iagManagerSmallBlk"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
					  			<input type="hidden" name="HEAD_ACT"> 
                        		<input type="hidden" name="headid"> 
								<input type="button" name="saveButton" value=' Save ' class="iagContentButton" onClick="DoSave('HEAD')"> 
                         	<input type="button" name="resetButton" value=' Reset ' class="iagContentButton" onClick="DoReset('HEAD')" STYLE="display:none"> 
                        &nbsp; 	<input type="button" name="cancelButton" value=' Cancel ' class="iagContentButton" onClick="DoCancel()"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> <div id=HEAD_DELETE>
                          <input type=button name="HDG" value=' Delete ' class="iagContentButton" onClick="DoDelete('HEAD')">
                        </div></td>
                    </tr>
                  </table>
                </div>
                <div id=Code style='display:none'> 
                  <table class="iagManagerSmallblk">
                    <tr> 
                      <td>Menu Name</td>
                      <td> 
                        <input type="text" name="codename" style="width:260px" maxlength="200" class="iagManagerSmallBlk" onBlur="checkItem()"> 
                      </td>
                    </tr>
					<tr> 
                      <td>Menu Uploads</td>
                      <td class="iagManagerSmallBlk">
                        <DIV id="UploadDIV"></DIV>
                      	<input type="hidden" name="hid_Level2" value=2 class="iagManagerSmallBlk">
					  </td>
                    </tr>
					<tr> 
                      <td></td>
                      <td class="iagManagerSmallBlk">
                       <INPUT type="button" value="ADD" onClick="UploadFile()" class="iagContentButton">
                        &nbsp; 
                        <input type="button" value="DEL" onClick="DeleteFile(2)" class="iagContentButton" name="button">
                      </td>
                    </tr>
                    <tr> 
                      <td>Menu Active</td>
                      <td class="iagManagerSmallBlk">Yes 
                        <input type="radio" name="codeactive" value="1" class="iagManagerSmallBlk" checked> 
                        &nbsp;&nbsp;No 
                        <input type="radio" name="codeactive" value="0" class="iagManagerSmallBlk"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
                        <input type="hidden" name="CODE_ACT">
                        <input type="hidden" name="codeid">
                        <input type="button" name="saveButton" value=' Save ' class="iagContentButton" onClick="DoSave('CODE')"> 
                        	<input type="button" name="resetButton" value=' Reset ' class="iagContentButton" onClick="DoReset('CODE')" STYLE="display:none"> 
                        &nbsp;	<input type="button" name="cancelButton" value=' Cancel ' class="iagContentButton" onClick="DoCancel()"> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> <div id=CODE_DELETE>
					  <input type="button" name="CODE_CONT" value='Content' onClick="document.thisForm.hid_level.value=0;ContentCodeEdit(document.thisForm.codeid.value)" class="iagContentButton">
                       <input type="button" name="BDG_CONT" value='Delete Content' onClick="DeleteContent(2)" class="iagContentButton">
					      <input type=button name="CDG" value=' Delete ' class="iagContentButton" onClick="DoDelete('CODE')">
                        </div></td>
                    </tr>
                  </table>
                </div>
                <div id=Budget style='display:none'>
				    <table class="iagManagerSmallblk">
                    <tr> 
                      <td>Menu Name</td>
                      <td> <input type="text" name="budgetname" style="width:260px" maxlength="200" class="iagManagerSmallBlk" onBlur="checkItem()"> 
                      </td>
                    </tr>
					<tr> 
                      <td>Menu Uploads</td>
                      <td class="iagManagerSmallBlk">
                        <DIV id="UploadLevel3DIV"></DIV>
                      	<input type="hidden" name="hid_Level3" value=3 class="iagManagerSmallBlk">
					  </td>
                    </tr>
					<tr> 
                      <td></td>
                      <td class="iagManagerSmallBlk">
                       <INPUT type="button" value="ADD" onClick="UploadLevel3File()" class="iagContentButton">&nbsp;
					   <INPUT type="button" value="DEL" onClick="DeleteFile(3)" class="iagContentButton">
					  </td>
                    </tr>
                    <tr> 
                      <td> Menu Active</td>
                      <td class="iagManagerSmallBlk">Yes 
                        <input type="radio" name="budgetactive" value="1" class="iagManagerSmallBlk" checked> 
                        &nbsp;&nbsp;No 
                        <input type="radio" name="budgetactive" value="0" class="iagManagerSmallBlk"> 
                      </td>
                    </tr>                   
                    <tr> 
                      <td>&nbsp;</td>
                      <td align="right"> 
					  	<table>
							<TR>								
                            <TD align="right"> 							
                              <input type="button" name="saveButton" value=' Save ' class="iagContentButton" onClick="DoSave('BUDGET')">
							  <input type="button" name="resetButton" value=' Reset ' class="iagContentButton" onClick="DoReset('BUDGET')" STYLE="display:none">
							  &nbsp;<input type="button" name="cancelButton" value=' Cancel ' class="iagContentButton" onClick="DoCancel()">
							  </td>
							  <td>							  
							  <div id="BUDGET_DELETE">&nbsp;<input type="button" name="BDG" value=' Delete ' class="iagContentButton" onClick="DoDelete('BUDGET')"></div>
							  <input type="hidden" name="BUDGET_ACT"> <input type="hidden" name="budgetid">	
                            </TD>
							</TR>
							<TR>
								<TD colspan="2" align="right">
									<div id="NEXTSTEP_LIB" style="display:none">
										<table border="0" cellpadding="0" cellspacing="0">
											<TR>								
                            					<TD align="right">
												
												</td>											
											</tr>
											<tr><td height="5"></td></tr>
											<tr align="right">	
												<td>
												<input type="button" name="BDG_CONT" value='Content' onClick="document.thisForm.hid_level.value=1;ContentEdit(document.thisForm.budgetid.value)" class="iagContentButton">
												<input type="button" name="BDG_CONT" value='Delete Content' onClick="DeleteContent(3)" class="iagContentButton">
												&nbsp;<input type="button" name="BDG_CONTACTS" value='Contacts' onClick="EditItem(document.thisForm.budgetid.value,2)" class="iagContentButton">			
												&nbsp;<input type="button" name="BDG_LINKS" value='Links' onClick="EditItem(document.thisForm.budgetid.value,3)" class="iagContentButton">
									 			</td>
											</tr>
										</table>
									 </div>
								</TD>
							</TR>
						</table> 
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> </td>
                    </tr>
                  </table>
				</div>
              </div>
            </td>
          </tr>
          <tr> 
            <TD HEIGHT=44 width=100% align=right>&nbsp;<input type="hidden" name="hid_level"></TD>
            <td rowspan=2><IMG SRC="/Images/My113.gif" WIDTH=72 HEIGHT=72></td>
          </tr>
          <TR> 
            <TD height=28 BGCOLOR=#FF9933 align=right class="iagManagerSmallWht" valign=middle><b>iagManager is a Reidmark eBusiness System</b></TD>
          </TR>
        </table>
      </form>
    </TD>
  </TR>
</TABLE>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<iframe name="FundBuilder" width="1" height="1" style="display:block;"></iframe> 
</body>
</html>