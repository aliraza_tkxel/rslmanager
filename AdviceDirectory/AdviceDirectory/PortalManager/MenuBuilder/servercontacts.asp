	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 'option explicit %>
<%
' VARIABLES NOT DEFINED IN INCLUDE FILE 
Dim MM_ContactManager_STRING 
Dim FinanceBuilderConnectionString
Dim lstid
Dim optionSelected
Dim RowClickcolumn
Dim THE_TABLE_HIGH_LIGHT_COLOUR
Dim PotentialOrderBy
Dim i
Dim strSortArray
Dim item
Dim matched
Dim j
Dim RowCodeToEvaluate
Dim TempArray
Dim TDFront, TDEnd, TDFunctionArray, TDString
Dim intNumChars
Dim CurrentTitle, CurrentASC, CurrentDESC
Dim TABLE_WIDTH_DEFAULT
Dim TopString, SubQueryStringm, Top_Data, SubQueryString
Dim JavaJump, TheJavascriptFunction
Dim strImage, strDisplay
%>
<!--#include virtual="include/Functions/LibFunctions.asp" -->
<!--#include virtual="include/Functions/TableBuilder_HTTPReqObject.asp" -->
<% Session.LCID = 2057 %>

<%
	Function RW(strOutText)
		Response.Write strOutText
	End Function
	
	Function DebugRquests()
	If Request.Form <> "" Then
		For Each Item In Request.Form
			rw "" & Item & " : " & Request(Item) & "<BR>" & vbcrlf
		Next
	 End If 
	
	 If Request.QueryString <> "" Then
		For Each Item In Request.QueryString
			rw "" & Item & " : " & Request(Item) & "<BR>" & vbcrlf
		Next
	 End If  
	End Function
		
	'Call DebugRquests()
	
		Dim CONTACT_NAME,CONTACT_ADDRESS,CONTACT_POSTCODE,CONTACT_TEL,CONTACT_FAX,CONTACT_EMAIL,CONTACTID
	
	Function GetFormData()
		CONTACT_NAME = Request("txt_CONTACT_NAME")				
		if CONTACT_NAME = "" then CONTACT_NAME = null end if
		CONTACT_ADDRESS = Request("txt_CONTACT_ADDRESS")				
		if CONTACT_ADDRESS = "" then CONTACT_ADDRESS = null end if			
		CONTACT_POSTCODE = Request("txt_CONTACT_POSTCODE")				
		if CONTACT_POSTCODE = "" then CONTACT_POSTCODE = null end if	
		CONTACT_TEL = Request("txt_CONTACT_TEL")				
		if CONTACT_TEL = "" then CONTACT_TEL = null end if	
		CONTACT_FAX = Request("txt_CONTACT_FAX")				
		if CONTACT_FAX = "" then CONTACT_FAX = null end if	
		CONTACT_EMAIL = Request("txt_CONTACT_EMAIL")				
		if CONTACT_EMAIL = "" then CONTACT_EMAIL = null end if
		CONTACTID = Request("txt_CONTACTID")				
		if CONTACTID = "" then CONTACTID = null end if
	End Function
	
	Function loadAllData()
		'Response.Redirect("Serverside/Contacts_svr.asp?budgetid="&thePage)
		rw "<script language=javascript defer> parent.PageMe(""/MenuBuilder/Serverside/Contacts_svr.asp?budgetid="&thePage & """)</script>"
	End Function	 
	
	Function AddRecord()
	Call GetFormData()
	Conn.Execute ("INSERT INTO NEXTSTEP_CONTACTS " &_
				  "		(CONTACTNAME,CONTACTADDRESS,CONTACTTEL,CONTACTFAX,CONTACTEMAIL,NP_LEVEL3ID)  " &_
				  "	VALUES  " &_
				  "		('"&CONTACT_NAME&"','"&CONTACT_ADDRESS&"','"&CONTACT_TEL&"','"&CONTACT_FAX&"','"&CONTACT_EMAIL&"',"&thePage&")")	
	Call loadAllData()
	End Function
	
	Function DelRecord()
	Call GetFormData()
	Conn.Execute ("DELETE FROM NEXTSTEP_CONTACTS WHERE CONTACTID = " & CONTACTID & ";")
	Call loadAllData()
	End Function
	
	Dim theAction 	
		theAction = Request("CONTACTS_ACT")

	Dim thePage
		thePage = Request("budgetid")

Call OpenDB()			
If (theAction = "New") Then
	AddRecord()
ElseIf (theAction = "DELETE") Then
	DelRecord()
End If
Call CloseDB()
%>