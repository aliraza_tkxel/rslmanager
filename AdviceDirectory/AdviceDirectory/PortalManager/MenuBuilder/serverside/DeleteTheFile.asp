<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!--#include file="Upload.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<%

Dim delID, filename, strURL, strFilePath, theID, budgetid, leveltype
Dim strUploadEvent, fs, strDeleteEvent

delID 		= request("sel_Upload") ' The unique record ID
delbudgetID = request("sel_Uploadbudget")
theID 		= Request("codeID") ' The Level 2 ID
budgetid 	= Request("budgetid") ' The Level 3 ID
leveltype 	= Request("leveltype")

openDB()
if leveltype = 2 then

		strUploadEvent = "SELECT * FROM UPLOAD WHERE UploadID = " & delID
	
		Set rsUpload = Server.CreateObject("ADODB.Recordset")
		Set rsUpload = Conn.Execute(strUploadEvent)
	
		filename = rsUpload("FileName")
		strURL = "/Uploads/" & filename

		strFilePath = Server.MapPath(strURL)

		Set fs=Server.CreateObject("Scripting.FileSystemObject") 
		if fs.FileExists(strFilePath) then
		  fs.DeleteFile(strFilePath)
		end if
		set fs=nothing

			strDeleteEvent = "DELETE FROM UPLOAD WHERE UploadID = " & delID
			
		Set rsDelete = Server.CreateObject("ADODB.Recordset")
		Set rsDelete = Conn.Execute(strDeleteEvent)

		Call BuildSelect(Str_Upload,"sel_Upload","Upload WHERE Level2ID = " & theID,"UploadID,UploadName","UploadName","Please Select",Null,Null,NULL," style='width:115px' tabindex='2' class='iagManagerSmallBlk' ")
		strUploads = Str_Upload
	
else

		strUploadEvent = "SELECT * FROM UPLOAD WHERE UploadID = " & delbudgetID
	
		Set rsUpload = Server.CreateObject("ADODB.Recordset")
		Set rsUpload = Conn.Execute(strUploadEvent)
		
	
		filename = rsUpload("FileName")
		strURL = "/Uploads/" & filename

		strFilePath = Server.MapPath(strURL)

		Set fs=Server.CreateObject("Scripting.FileSystemObject") 
		if fs.FileExists(strFilePath) then
		  fs.DeleteFile(strFilePath)
		end if
		set fs=nothing

			strDeleteEvent = "DELETE FROM UPLOAD WHERE UploadID = " & delbudgetID
			
		Set rsDelete = Server.CreateObject("ADODB.Recordset")
		Set rsDelete = Conn.Execute(strDeleteEvent)

		Call BuildSelect(Str_Upload,"sel_Uploadbudget","Upload WHERE Level3ID = " & budgetid,"UploadID,UploadName","UploadName","Please Select",Null,Null,NULL," style='width:115px' tabindex='2' class='iagManagerSmallBlk' ")
		strUploads = Str_Upload
	
end if
CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<DIV id="UploadDIV" class="iagManagerSmallBlk"><%=strUploads%></DIV>
<DIV id="UploadLevel3DIV" class="iagManagerSmallBlk"><%=strUploads%></DIV>
<script language="javascript">
function returnData(){
	if (<%=leveltype%> == 2)
		{
		parent.UploadDIV.innerHTML = UploadDIV.innerHTML
		}
	else
		{
		parent.UploadLevel3DIV.innerHTML = UploadLevel3DIV.innerHTML
		}
}
	
</script>
</body>
</html>
