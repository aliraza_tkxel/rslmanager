<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 'option explicit %>
<%
' VARIABLES NOT DEFINED IN INCLUDE FILE 
Dim MM_ContactManager_STRING 
Dim FinanceBuilderConnectionString
Dim lstid
Dim optionSelected
Dim RowClickcolumn
Dim THE_TABLE_HIGH_LIGHT_COLOUR
Dim PotentialOrderBy
Dim i
Dim strSortArray
Dim item
Dim matched
Dim j
Dim RowCodeToEvaluate
Dim TempArray
Dim TDFront, TDEnd, TDFunctionArray, TDString
Dim intNumChars
Dim CurrentTitle, CurrentASC, CurrentDESC
Dim TABLE_WIDTH_DEFAULT
Dim TopString, SubQueryStringm, Top_Data, SubQueryString
Dim JavaJump, TheJavascriptFunction
Dim strImage, strDisplay
%>
<!--#include virtual="include/Functions/LibFunctions.asp" -->
<!--#include virtual="include/Functions/TableBuilder_Portal_HTTPReqObject.asp" -->
<% Session.LCID = 2057 %>
<%
	'Call DebugRquests()
 	Function IsInteger(ByVal strValue)
        ' Allows only positive integers that are greater then 0 (Change[1-9] to [0-9] if you want to have 0 leading number
        Dim RegInteger
		Set RegInteger = New RegExp   
		RegInteger.Pattern = "^-[0-9]+$|^[0-9]+$"
		IsInteger = RegInteger.Test(strValue) 
    End Function
	
	Call OpenDB()
	
	Dim RsID
	Dim rsD 															' Declare Recordset
	Dim rsProject														' Declare Recordset
	Dim lstProject,lstOrganisation,lstImpactMeasure						' Declare Variable for Listbox(s)
	
		RsID = Replace(REQUEST("budgetid"),"'","''")

		SQLFilter = ""
		DisplayText = ""
	
	if (IsInteger(RsID)) then
		RsID = CInt(RsID)
		SQLFilter = SQLFilter & " NP_LEVEL3ID = " & RsID & " "
	else
		SQLFilter = SQLFilter & " NP_LEVEL3ID = -1 "
	end if

	Call CloseDB()

	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim ColumnLength 
		ColumnLength = 0
	ReDim TableTitles    (ColumnLength)	 'USED BY CODE
	ReDim DatabaseFields (ColumnLength)	 'USED BY CODE
	ReDim ColumnWidths   (ColumnLength)	 'USED BY CODE
	ReDim Legends		 (ColumnLength)	 'USED BY CODE
	ReDim TDSTUFF        (ColumnLength)	 'USED BY CODE
	ReDim TDPrepared	 (ColumnLength)	 'USED BY CODE
	ReDim LEGENDTDPrepared	 (ColumnLength)	 'USED BY CODE
	ReDim LegendName	 (ColumnLength)	 'USED BY CODE
	ReDim ColData        (ColumnLength)	 'Syntax	[column title] | [database field] | [display length(px)] 
	ReDim SortASC        (ColumnLength)	 'All Items must be included, if a sort is not reuired for the field then put ""
	ReDim SortDESC       (ColumnLength)	 'All Array sizes must match
	ReDim TDFunc         (ColumnLength)	 'stores any functions that will be applied
	Dim SubQuery			 'used to get the name of the parent object
	Dim SubQueryTitle		 'the title of the parent object
	
	Dim Legend
		Legend = true
		
		BtnExport = ""
	
	ColData (0) = "Name:|LINKNAME|150|Y|Link Name"
	SortASC (0)	= "LINKNAME ASC"
	SortDESC(0) = "LINKNAME DESC"
	TDSTUFF	(0)  = " ""OnClick=""""getData(""& rsSet(""LINKID"") & "",'LINKS')"""" style='color:blue;cursor:pointer' title=""& rsSet(""LINKNAME"") & "" "" "
	TDFunc  (0) = ""
	
	PageName = "serverside/links_svr.asp"
	EmptyText = "Report has no data or filter returned zero results."
	DefaultOrderBy = SortASC(0)
	RowClickColumn = "" 

	THE_TABLE_HIGH_LIGHT_COLOUR = "#0BAAD4"
	Maximum_Table_Length = "150px"
	
	Dim orderBy
	Dim OrderByMatched
		
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		
	Call SetSort()
	
	SQLCODE = 	"SELECT LINKID, " &_ 
				"		LINKNAME " &_
				"FROM NEXTSTEP_LINKS " &_
				"WHERE " & SQLFilter &_ 
				"ORDER BY " + Replace(orderBy, "'", "''") + ""
	
	' rw SQLCODE
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
	
	if intRecordCount > 0 then

%>
<table width="100%" height="100%" cellspacing=0 cellpadding=0 class="iagManagerSmallBlk" style="border-top:1px solid #FF9933; border-bottom:1px solid #FF9933;background-color:beige">
	<tr>
		<td align="left" valign="top">
			<table width="100%" border="0">
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
				<tr VALIGN=TOP> 
					<td width="10"></td>
					<td width="<%=Maximum_Table_Length%>" align="left" valign="top">
						<div id="NEXTSTEP_DIV_CONTACTS_LIST" style="width:<%=Maximum_Table_Length%>">
							<table width="150">
								<tr>
									<td><%=TheFullTable%></td>
								</tr>
							</table>
						</div>
					</td>
					<td width="10"></td>
					<td align="left" valign="top" style="width:100%; background-color:white; font-face:Arial; padding:3; border: 1px solid threedface">
					<div id="NEXTSTEP_DIV_CONTACTS_FORM" style="display:none"></div>&nbsp;</td>
					<td width="10"></td>
				</TR>
			</table>
		</td>
	</tr>
</table>
<% Else %>
0
<% End If %>