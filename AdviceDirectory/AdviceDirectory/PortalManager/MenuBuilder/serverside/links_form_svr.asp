<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 'option explicit %>
<%
' VARIABLES NOT DEFINED IN INCLUDE FILE 
Dim MM_ContactManager_STRING 
Dim FinanceBuilderConnectionString
Dim lstid
Dim optionSelected
Dim RowClickcolumn
Dim THE_TABLE_HIGH_LIGHT_COLOUR
Dim PotentialOrderBy
Dim i
Dim strSortArray
Dim item
Dim matched
Dim j
Dim RowCodeToEvaluate
Dim TempArray
Dim TDFront, TDEnd, TDFunctionArray, TDString
Dim intNumChars
Dim CurrentTitle, CurrentASC, CurrentDESC
Dim TABLE_WIDTH_DEFAULT
Dim TopString, SubQueryStringm, Top_Data, SubQueryString
Dim JavaJump, TheJavascriptFunction
Dim strImage, strDisplay
%>
<!--#include virtual="include/Functions/LibFunctions.asp" -->
<!--#include virtual="include/Functions/TableBuilder_Portal_HTTPReqObject.asp" -->
<% Session.LCID = 2057 %>
<% 
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 
%>
<%
	'Call DebugRquests()
 	Function IsInteger(ByVal strValue)
        ' Allows only positive integers that are greater then 0 (Change[1-9] to [0-9] if you want to have 0 leading number
        Dim RegInteger
		Set RegInteger = New RegExp   
		RegInteger.Pattern = "^-[0-9]+$|^[0-9]+$"
		IsInteger = RegInteger.Test(strValue) 
    End Function	
	
	Dim RsID
	Dim rsD 															' Declare Recordset
		RsID = Replace(REQUEST("RsID"),"'","''")

		SQLFilter = ""

	if (IsInteger(RsID)) then
		RsID = CInt(RsID)
		SQLFilter = SQLFilter & " LINKID = " & RsID & " "
	else
		SQLFilter = SQLFilter & " LINKID = -1 "
	end if


Dim LINKID
Dim LINKNAME
Dim LINKURL
Dim LINKDESCRIPTION

Dim LINKS_ACT
	LINKS_ACT = "New"
Dim LINK_DELETE_STYLE
	LINK_DELETE_STYLE = "none"

SQL =   "SELECT * " &_
		"FROM NEXTSTEP_LINKS " &_
		"WHERE " & SQLFilter 
		
Call OpenDB()	
Call OpenRs(rsLinks,SQL)
If NOT rsLinks.EOF Then
	LINKID = rsLinks(0)
	LINKNAME = rsLinks(1)
	LINKURL = rsLinks(2)
	LINKDESCRIPTION = rsLinks(3)	
	LINKS_ACT = "UPDATE"
	LINK_DELETE_STYLE = "block"
End IF
Call CloseRs(rsLinks)
Call CloseDB()
%>
<table width="100%" cellspacing="0" cellpadding="0" class="REPORTING_TABLE" STYLE="BORDER-COLLAPSE:COLLAPSE" BORDER="1" BORDERCOLOR="#c3c3c3">
  <tr> 
    <td class="CO" colspan="2" style="text-transform:uppercase; font-weight:bold; height:22px">&nbsp;Link Information</td>
  </tr>
  <tr> 
    <td class="CO" width=80>&nbsp;<STRONG>Name</STRONG></td>
    <td><input type="text" name="txt_LINK_NAME" accesskey="N" tabindex="1" / value="<%=LINKNAME%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr> 
    <td class="CO">&nbsp;<STRONG>Alt</STRONG></td>
    <td><input type="text" name="txt_LINK_ALT" accesskey="A" tabindex="1" / value="<%=LINKALT%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr> 
    <td class="CO">&nbsp;<STRONG>Url</STRONG></td>
    <td><input type="text" name="txt_LINK_URL" accesskey="P" tabindex="1" / value="<%=LINKURL%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'>
      <a href="#" onClick="CreateLink('txt_LINK_URL')"><img src="../MenuBuilder/images/Editor/Link.gif" alt="Link" hspace="2" vspace=1 border="0" align=absmiddle></a> 
    </td>
  </tr>
  <tr> 
    <td class="CO">&nbsp;<STRONG>Description</STRONG></td>
    <td><input type="text" name="txt_LINK_DESCRIPTION" accesskey="T" tabindex="1" / value="<%=LINKDESCRIPTION%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr> 
    <td class="CO" style="text-transform:uppercase; font-weight:bold; height:22px">&nbsp;</td>
    <td> 
      <table>
        <tr>
          <td><input type="Button" name="LINK_NEW" value="NEW" accesskey="S" tabindex="7" / class="Btn" onClick="BtnStatus('LINK_NEW')" style="display:<%=LINK_DELETE_STYLE%>" title="New Link"></td>
          <td width="100"><input type="Button" name="LINK_SAVE" value="SAVE" accesskey="S" tabindex="7" / class="Btn" onClick="DoSave('LINKS')" title="Save Link"></td>
          <td><input type="Button" name="LINK_DELETE" value="DELETE" accesskey="S" tabindex="7" / class="Btn" onClick="DoDelete('LINKS')" style="display:<%=LINK_DELETE_STYLE%>" title="Delete Link"></td>
        </tr>
      </table>
      <input type="hidden" name="txt_LINKID" value="<%=LINKID%>"> <input type="hidden" name="LINKS_ACT" value="<%=LINKS_ACT%>"> 
    </td>
  </tr>
</table>
