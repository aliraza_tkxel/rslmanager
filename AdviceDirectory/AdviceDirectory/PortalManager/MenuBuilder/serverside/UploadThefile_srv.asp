<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/Connections/db_connection.asp" -->
<!--#include file="Upload.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<%
	Call OpenDB()

	'THIS PART DOES THE FILE UPLOAD FOR EACH FILE PRESENTED.
	Set Uploader = New FileUploader
	Uploader.Upload()		
    byteCount = Request.TotalBytes     
    RequestBin = Request.BinaryRead(byteCount)
	dim strName
	strName 		= Uploader.Form("txt_name")
	strPath 		= Uploader.Form("Upload")
	codeid 			= Uploader.Form("codeid")
	LevelID 		= Uploader.Form("LevelID")
	
	'THIS PART SAVES EACH FILE WHICH WAS UPLOADED TO DISK
	If Uploader.Files.Count <> 0 Then
		'MAKE SURE THE DIRECTORY EXISTS
		'CREATE DIRECTORY FOR ORGANISATION IF IT DOES NOT EXIST.
		'FolderRoot = "/Doc/" & Uploader.Form("sel_ORG")
		'Set DIRSO = Server.CreateObject("Scripting.FileSystemObject")
		'If NOT DIRSO.FolderExists(Server.Mappath(FolderRoot)) then
		'	Set F = DIRSO.CreateFolder(Server.Mappath(FolderRoot))
		'	Set F = Nothing	
		'End If
		'Set DIRSO = Nothing
		
		TheLevel = LevelID
		
		if TheLevel = "3" then
			Level3ID = codeid
			Level2ID = null
		else
			Level2ID = codeid
			Level3ID = null
		end if	
		
		FolderRoot = "/Uploads/" 
		For Each File In Uploader.Files.Items			
			File.SaveToDisk FolderRoot
			
			SQL = "INSERT INTO UPLOAD (UploadName, Level2ID, Level3ID, FileName) VALUES " &_
					"('" & strName & "','" & Level2ID & "','" & Level3ID & "','" & File.FileName & "')" 
			Conn.Execute SQL
			response.write SQL
		Next
	End If
		
	Call CloseDB()

	Set Uploader = Nothing

%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">

<script language="javascript">
function returnData(){
	if (<%=TheLevel%> == 2)
		{
		window.opener.parent.FundBuilder.location.href = "../serverCode.asp?codeid=" + <% = codeid %> + "&CODE_ACT=LOAD";
		window.close();
		}
	else
		{
		window.opener.parent.FundBuilder.location.href = "../serverBudget.asp?budgetID=" + <% = codeid %> + "&BUDGET_ACT=LOAD";
		window.close();
		}	
}
	
</script>
</body>
</html>