<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 'option explicit %>
<%
' VARIABLES NOT DEFINED IN INCLUDE FILE 
Dim MM_ContactManager_STRING 
Dim FinanceBuilderConnectionString
Dim lstid
Dim optionSelected
Dim RowClickcolumn
Dim THE_TABLE_HIGH_LIGHT_COLOUR
Dim PotentialOrderBy
Dim i
Dim strSortArray
Dim item
Dim matched
Dim j
Dim RowCodeToEvaluate
Dim TempArray
Dim TDFront, TDEnd, TDFunctionArray, TDString
Dim intNumChars
Dim CurrentTitle, CurrentASC, CurrentDESC
Dim TABLE_WIDTH_DEFAULT
Dim TopString, SubQueryStringm, Top_Data, SubQueryString
Dim JavaJump, TheJavascriptFunction
Dim strImage, strDisplay
%>
<!--#include virtual="include/Functions/LibFunctions.asp" -->
<!--#include virtual="include/Functions/TableBuilder_HTTPReqObject.asp" -->
<% Session.LCID = 2057 %>
<%
	'Call DebugRquests()
 	Function IsInteger(ByVal strValue)
        ' Allows only positive integers that are greater then 0 (Change[1-9] to [0-9] if you want to have 0 leading number
        Dim RegInteger
		Set RegInteger = New RegExp   
		RegInteger.Pattern = "^-[0-9]+$|^[0-9]+$"
		IsInteger = RegInteger.Test(strValue) 
    End Function	
	
	Dim RsID
	Dim rsD 															' Declare Recordset
		RsID = Replace(REQUEST("RsID"),"'","''")

		SQLFilter = ""

	if (IsInteger(RsID)) then
		RsID = CInt(RsID)
		SQLFilter = SQLFilter & " CONTACTID = " & RsID & " "
	else
		SQLFilter = SQLFilter & " CONTACTID = -1 "
	end if


Dim CONTACTID
Dim CONTACTNAME
Dim CONTACTADDRESS
Dim CONTACTPOSTCODE
Dim CONTACTTEL
Dim CONTACTFAX
Dim CONTACTEMAIL
Dim CONTACTURL

Dim CONTACTS_ACT
	CONTACTS_ACT = "New"
Dim CONTACT_DELETE_STYLE
	CONTACT_DELETE_STYLE = "none"

SQL =   "SELECT * " &_
		"FROM NEXTSTEP_CONTACTS " &_
		"WHERE " & SQLFilter 
			
Call OpenDB()	
Call OpenRs(rsContacts,SQL)
If NOT rsContacts.EOF Then
	CONTACTID = rsContacts(0)
	CONTACTNAME = rsContacts(1)
	CONTACTADDRESS = rsContacts(2)
	CONTACTPOSTCODE = rsContacts(3) 
	CONTACTTEL = rsContacts(4)
	CONTACTFAX = rsContacts(5)
	CONTACTEMAIL = rsContacts(6)
	CONTACTURL = rsContacts(7)	
	CONTACTS_ACT = "UPDATE"
	CONTACT_DELETE_STYLE = "block"
End IF
Call CloseRs(rsContacts)
Call CloseDB()
%>
<table width="100%" cellspacing="0" cellpadding="0" class="REPORTING_TABLE" STYLE="BORDER-COLLAPSE:COLLAPSE" BORDER="1" BORDERCOLOR="#c3c3c3">
<tr>
    <td class="CO" colspan="2" style="text-transform:uppercase; font-weight:bold; height:22px">&nbsp;Contact Information</td>
  </tr>
  <tr>
    <td class="CO" width=80>&nbsp;<STRONG>Name</STRONG></td>
    <td><input type="text" name="txt_CONTACT_NAME" accesskey="N" tabindex="1" / value="<%=CONTACTNAME%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr>
    <td class="CO">&nbsp;<STRONG>Address</STRONG></td>
    <td><input type="text" name="txt_CONTACT_ADDRESS" accesskey="A" tabindex="1" / value="<%=CONTACTADDRESS%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr>
    <td class="CO">&nbsp;<STRONG>Postcode</STRONG></td>
    <td><input type="text" name="txt_CONTACT_POSTCODE" accesskey="P" tabindex="1" / value="<%=CONTACTPOSTCODE%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr>
    <td class="CO">&nbsp;<STRONG>Telephone</STRONG></td>
    <td><input type="text" name="txt_CONTACT_TEL" accesskey="T" tabindex="1" / value="<%=CONTACTTEL%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr>
    <td class="CO">&nbsp;<STRONG>Fax</STRONG></td>
    <td><input type="text" name="txt_CONTACT_FAX" accesskey="F" tabindex="1" / value="<%=CONTACTFAX%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr>
    <td class="CO">&nbsp;<STRONG>Email</STRONG></td>
    <td><input type="text" name="txt_CONTACT_EMAIL" accesskey="E" tabindex="1" / value="<%=CONTACTEMAIL%>" class="iagManagerSmallBlk" style='background-color:#FFFFFF;border-collapse:collapase;border:1px solid #FFFFFF;color:#999999'></td>
  </tr>
  <tr>
    <td class="CO" style="text-transform:uppercase; font-weight:bold; height:22px">&nbsp;</td>
    <td>
        <input type="Button" name="CONTACT_NEW" value="NEW" accesskey="S" tabindex="7" / class="Btn" OnClick="BtnStatus('CONTACT_NEW')"> <input type="Button" name="CONTACT_SAVE" value="SAVE" accesskey="S" tabindex="7" / class="Btn" onclick="DoSave('CONTACTS')">
        <input type="Button" name="CONTACT_DELETE" value="DELETE" accesskey="S" tabindex="7" / class="Btn" OnClick="DoDelete('CONTACTS')" style="display:<%=CONTACT_DELETE_STYLE%>"><input type="hidden" name="txt_CONTACTID" value="<%=CONTACTID%>"><input type="hidden" name="CONTACTS_ACT" value="<%=CONTACTS_ACT%>"></td>
  </tr>
</table>
