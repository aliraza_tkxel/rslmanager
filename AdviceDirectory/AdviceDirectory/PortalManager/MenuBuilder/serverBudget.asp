<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/connections/db_connection.asp" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include file="ADOVBS.INC" -->
<%
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText
	'Dim Conn
	'Dim Rs
	Dim fundid, codeid,  budgetname, active, budgetid
		active = 1
	
Function OpenRecordSet()
	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open DSN_CONNECTION_STRING
End Function

Function CloseConnections()
	Conn.Close
	Set Conn = Nothing
End Function

'	On Error Resume Next

Function GetFormFields ()
	budgetname = PCase(Request.Form("budgetname"))
	if budgetname = "" then budgetname = null end if	
	active = Request.Form("budgetactive")
	if active = "" then active = 0 end if	
	fundid = Request.Form("fundid")
	if fundid = "" then fundid = 0 end if
	codeid = Request.Form("codeid")
	if codeid = "" then codeid = 0 end if
End Function

Function NewRecord ()

	GetFormFields()
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL3", Conn, adopenstatic, adLockOptimistic
		Rs.AddNew
		Rs("NP_LEVEL3DESC") = budgetname		
		Rs("NP_LEVEL2ID") = codeid
		Rs("ACTIVE") = active 		
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function

Function UpdateRecord (theID)

	GetFormFields()
	
	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT * FROM NEXTSTEP_PORTAL_MENU_LEVEL3 WHERE NP_LEVEL3ID = " & theID & ";",Conn,,adLockOptimistic,adCmdText	
		Rs("NP_LEVEL3DESC") = budgetname
		Rs("ACTIVE") = active 				
		Rs.Update
		Rs.Close
	Set Rs = Nothing
	
End Function

Dim CONTENTPRESENT, CONTENT_ACT, CONTENT, strUploads
Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
		strEventQuery =	"SELECT L3.ACTIVE AS ACTIVE, NP_LEVEL3DESC AS allocationname, NP_MENUID AS FUNDID, L1.NP_LEVEL1ID AS HEADID, L2.NP_LEVEL2ID AS CODEID, IsNULL(LB.NP_LEVEL3ID,0) AS CONTENTPRESENT, LB.CONTENT AS CONTENT " &_
						"FROM NEXTSTEP_PORTAL_MENU_LEVEL3 L3 " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL2 L2 ON L2.NP_LEVEL2ID = L3.NP_LEVEL2ID " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL1 L1 ON L1.NP_LEVEL1ID = L2.NP_LEVEL1ID " &_
						"LEFT JOIN NEXTSTEP_LIBRARY LB ON LB.NP_LEVEL3ID = L3.NP_LEVEL3ID " &_
						"WHERE (L3.NP_LEVEL3ID = " & theID & ")"			
		
	Set Rs = Conn.Execute(strEventQuery)
		'response.write strEventQuery
	OpenDB()	
	Call BuildSelect(Str_Upload,"sel_Uploadbudget","Upload WHERE Level3ID = " & theID,"UploadID,UploadName","UploadName","Please Select",Null,Null,NULL," style='width:115px' tabindex='2' class='iagManagerSmallBlk' ")	
		
		strUploads = Str_Upload
		
		budgetname = Rs("AllocationName")
		headid = Rs("headid")
		fundid = Rs("fundid")
		codeid = Rs("codeid")	
		active = Rs("active")
		
		CONTENT = Rs("CONTENT")
		CONTENTPRESENT = Rs("CONTENTPRESENT")
		
		If CONTENTPRESENT = 0 Then 
			CONTENT_ACT = "ADD" 
			CONTENT = ""
		Else 
			CONTENT_ACT = "UPDATE"
			CONTENT = Replace((CONTENT), Chr(13), "<br>")
		End If 
	
		Rs.Close
	Set Rs = Nothing
	
End Function


Function DisplayMiniTree(theID)

	Dim StrEventQuery
		strEventQuery = "SELECT NP_LEVEL3DESC AS ALLOCATIONNAME, L0.NP_MENUDESC AS FUNDNAME, L1.NP_LEVEL1DESC AS HEADNAME, L2.NP_LEVEL2DESC AS CODEDESC " &_
						"FROM NEXTSTEP_PORTAL_MENU_LEVEL3 L3 " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL2 L2 ON L2.NP_LEVEL2ID = L3.NP_LEVEL2ID " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL1 L1 ON L1.NP_LEVEL1ID = L2.NP_LEVEL1ID " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU L0 ON L0.NP_MENUID = L1.NP_MENUID " &_
						"WHERE (L3.NP_LEVEL3ID = " & theID & ")"
						
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
		
		treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Update Menu</u></b></td></tr><tr><td>&nbsp;</td></tr>"
		treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("fundname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("codedesc") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("allocationname") & "</td></tr>"
		treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

Function DisplayMiniTree2(theID)
	
	Dim StrEventQuery
		strEventQuery = "SELECT L0.NP_MENUDESC AS FUNDNAME, L1.NP_LEVEL1DESC AS HEADNAME, L2.NP_LEVEL2DESC AS CODEDESC " &_
						"FROM NEXTSTEP_PORTAL_MENU_LEVEL2 L2 " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU_LEVEL1 L1 ON L1.NP_LEVEL1ID = L2.NP_LEVEL1ID " &_
						"LEFT JOIN NEXTSTEP_PORTAL_MENU L0 ON L0.NP_MENUID = L1.NP_MENUID " &_
						"WHERE (L2.NP_LEVEL2ID = " & theID & ")"

	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
		
		treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Add New Menu</u></b></td></tr><tr><td>&nbsp;</td></tr>"
		treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("fundname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("codedesc") & "</td></tr>"
		treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<font color=red>...</font></td></tr>"
		treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
		
		Rs4.close()
	Set Rs4 = Nothing
	
End Function

Function DelRecord (theID)
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Set Rs = Conn.Execute ("DELETE FROM NEXTSTEP_PORTAL_MENU_LEVEL3 WHERE NP_LEVEL3ID = " & theID & ";")
		treeText = "Menu deleted successfully."		
End Function

Function loadAllData()
	Response.Redirect("ZZServerCustomerData.asp")
End Function
	
ACTION_TO_TAKE = Request("BUDGET_ACT")
budgetID = Request("budgetid")


OpenRecordSet()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LOADFUNDDATA") Then
	codeid = Request("codeid")
	fundid = Request("fundid")	
	DisplayMiniTree2(codeid)	
ElseIf (ACTION_TO_TAKE = "LOAD") Then
	GetData(budgetID)
	DisplayMiniTree(budgetID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(budgetID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(budgetID)
End If

CloseConnections()
%>
<html>
<head>
<title>Menu Builder</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">

<DIV id="idContent" contenteditable style="width:100%; height:400px; background-color:white; font-face:Arial; padding:3; border: 1px solid threedface; overflow=auto;"><%=CONTENT%></DIV>
<DIV id="UploadLevel3DIV" class="iagManagerSmallBlk"><%=strUploads%></DIV>
<script language=javascript defer>
function returnData(){
	if ("<%=ACTION_TO_TAKE%>" == "ADD"){
		parent.refreshSideBar();
		parent.setText("New Menu added successfully.",1);			
		}
	else if ("<%=ACTION_TO_TAKE%>" == "UPDATE"){
		parent.refreshSideBar();
		parent.setText("Menu updated successfully.",1);	
		}
	else if ("<%=ACTION_TO_TAKE%>" == "LOADFUNDDATA"){
		parent.ResetDiv('BUDGET');
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("BUDGET",1);
		parent.setCheckingArray('BUDGET');
		parent.thisForm.fundid.value = "<%=fundid%>";
		parent.thisForm.codeid.value = "<%=codeid%>";		
		parent.thisForm.budgetid.value = "<%=budgetid%>";				
		parent.thisForm.BUDGET_ACT.value = "ADD";
		parent.Budget.style.display = "block";
	}
	else if ("<%=ACTION_TO_TAKE%>" == "DELETE"){
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);	
		}
	else if ("<%=ACTION_TO_TAKE%>" == "LOAD"){
		parent.NewItem(4);
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("BUDGET",2);							
		parent.thisForm.budgetname.value = "<%=budgetname%>";
		parent.thisForm.budgetid.value = "<%=budgetid%>";							
		parent.thisForm.fundid.value = "<%=fundid%>";
		parent.thisForm.codeid.value = "<%=codeID%>";
		parent.UploadLevel3DIV.innerHTML = UploadLevel3DIV.innerHTML
		if (<%=active%> == 1)
			{
			parent.thisForm.budgetactive[0].checked = true;
			}
		else
			{
			parent.thisForm.budgetactive[1].checked = true;
			}				
		parent.thisForm.BUDGET_ACT.value = "UPDATE";
		parent.thisForm.CONTENT_ACT.value = "<%=CONTENT_ACT%>";
		parent.idContent.outerHTML = idContent.outerHTML;
		parent.UploadLevel3DIV.innerHTML = UploadLevel3DIV.innerHTML
		parent.Budget.style.display = "block";
		parent.setCheckingArray('BUDGET');	
		}
	}
</script>
</body>
</html>