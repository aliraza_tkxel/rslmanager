<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
	Dim menuid, levelid
	    menuid 	= request("menuid")
	    levelid	= request("level1id")

Call OpenDB()

	SQL =   "SELECT M.MENUTITLE, M.PARENTMENUID, M.ACTIVE, M.EXTERNALURL, P.PAGEID, P.PAGE_TITLE, P.PAGE_NAME, SA.PAGE_NAME AS JOBSPAGENAME, P.FILE_NAME, P.PAGE_CONTENT, P.TEMPLATE " &_
			"	FROM CMS_MENUS M " &_
			"	LEFT JOIN MENUTOPAGELINKAGE MPL ON MPL.MENUID = M.MENUID " &_
			"	LEFT JOIN CMS_PAGES P ON P.PAGEID = MPL.PAGEID " &_
			"	LEFT JOIN TBL_SKILLSFORJOBS_AREA SA ON SA.MENUID = M.MENUID " &_
			"	WHERE M.MENUID = " & MENUID

	DontDeleteFile = 1
	ActiveLink = 1

	Call OpenRs(rsPageContent, SQL)	
		If NOT rsPageContent.EOF Then
			'do until rsPageContent.EOF
				MENUTITLE			= rsPageContent("MENUTITLE")
				PARENTMENUID		= rsPageContent("PARENTMENUID")
				Active				= rsPageContent("Active")
				ExternalURL			= rsPageContent("ExternalURL")
				PAGEID				= rsPageContent("PAGEID")
				PAGE_TITLE			= rsPageContent("PAGE_TITLE")
				PAGE_NAME			= rsPageContent("PAGE_NAME")
				PAGE_CONTENT		= rsPageContent("PAGE_CONTENT")
				TEMPLATE			= rsPageContent("TEMPLATE")
				JOBSPAGENAME		= rsPageContent("JOBSPAGENAME")
				FileName			= rsPageContent("FILE_NAME")

					If isnull(ExternalURL) Then
						ActiveLink = 1
					Else
						ActiveLink = 2
					End If

					If isnull(PAGE_NAME) Then
						DontDeleteFile = 1
					Else
						DontDeleteFile = 2
					End If

			'rsPageContent.movenext
			'loop					
		End If

	Call CloseRS(rsPageContent)

		If Active = 1 then
			ActiveYes = "checked"
		else 
			ActiveNo = "checked"
		end if

if levelid = 130 then
	SQL =   " SELECT S.AREAID, S.MENUID, S.STRAREA, S.STRCOMMENTS1, S.STRCOMMENTS2, S.STRCOMMENTS3, " &_
		    "   S.STRCOMMENTS4, S.STRCOMMENTS5, S.STRCOMMENTS6, S.STRCOMMENTS7, S.HEADING1, S.HEADING2, " &_
		    "   S.HEADING3, S.HEADING4, S.HEADING5, S.HEADING6, T.MENUTITLE " &_
		    " FROM TBL_SKILLSFORJOBS_AREA s " &_
		    " INNER JOIN CMS_MENUS T ON T.MENUID = S.MENUID " &_
		    " WHERE T.MENUID = " & menuid
'response.write sql
	Call OpenRs(rsCategories, SQL)
		If NOT rsCategories.EOF Then
			'do until rsCategories.EOF
				AREAID			= rsCategories("AREAID")
				MENUID			= rsCategories("MENUID")
				AREA			= rsCategories("STRAREA")
				Heading1		= rsCategories("heading1")
				Heading2		= rsCategories("heading2")
				Heading3		= rsCategories("heading3")
				Heading4		= rsCategories("heading4")
				Heading5		= rsCategories("heading5")
				Heading6		= rsCategories("heading6")
				Comments1		= rsCategories("strComments1")
				Comments2		= rsCategories("strComments2")
				Comments3		= rsCategories("strComments3")
				Comments4		= rsCategories("strComments4")
				Comments5		= rsCategories("strComments5")
				Comments6		= rsCategories("strComments6")
				Comments7		= rsCategories("strComments7")
				menutitle		= rsCategories("menutitle")

				'response.write 	 PAGE_TITLE
				'	RESPONSE.WRITE PAGEID
			'rsCategories.movenext
			'loop
		End If
	Call CloseRS(rsCategories)

		SQL =   "SELECT L.STRLINK, L.STRHREF " &_
				"	FROM TBL_SKILLSFORJOBS_LINK L " &_
				"	INNER JOIN TBL_SKILLSFORJOBS_LINKPATH LP ON LP.LINKID = L.LINKID " &_
				"	WHERE LP.JOBID = " & AREAID

	Call OpenRs(rsLinks, SQL)
		If NOT rsLinks.EOF Then
	 		do until rsLinks.EOF
				strLink		= rsLinks("strLink")
				strHref		= rsLinks("strHref")
 				datelist = datelist & "<tr valign=""top""> "
				datelist = datelist & "<td>" & strLink & "</td>"
				datelist = datelist & "</tr>"
			rsLinks.movenext
			loop
		End If
	Call CloseRS(rsLinks)
end if

Call CloseDB()

If TEMPLATE = 1 Then
	radsite = "checked"
Else 
	radsite = ""
End If
If TEMPLATE = 2 Then
	radLibrary = "checked"
Else 
	radLibrary = ""
End If
If TEMPLATE = 3 Then
	radNone = "checked"
Else 
	radNone = ""
End If
%>
<!-- #include file="fckeditor.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="nextstep.css" type="text/css" />
<script type="text/javascript" language="javascript" src="/js/WindowResizer.js"></script>
</head>
<script type="text/javascript" language="javascript">
xresnow=600;
yresnow=600;
</script>
<script type="text/javascript" language="javascript">

mw=0;
function LaunchWindowSize(location){
//	task manager login
//	x=542;
//	y=397;
//	future forum login
	x=680; //width
	y=478; //height

	xresnow = x;
	yresnow = y;
	if (navigator.appVersion.indexOf("Mac")!=-1 && document.all) {
			xresnow-=0;
			yresnow-=0;
	}
	xl = (screen.availWidth/2)-(xresnow/2);
	tt = (screen.availHeight/2)-(yresnow/2);
  if (document.all) mw=window.open(location,'TaskManagerP','scrollbars=yes,width='+xresnow+',height='+yresnow+',left='+xl+',status=yes,top='+tt);
  else {
    if (parseFloat(navigator.appVersion)<4.75)
      mw=window.open(location,'TaskManagerP','left='+xl+',top='+tt+',width='+x+',height='+y+',toolbar=0,status=0,location=0');
    else {
      mw=window.open(location,'TaskManagerP','scrollbars=yes,toolbar=0,status=0,location=0');
      setTimeout("mwFix("+xl+","+tt+",x,y)",10);
    }
  }
}
mw=0;
function LaunchWindow(location){
//	task manager login
//	x=542;
//	y=397;
//	future forum login
	x=835; //width
	y=405; //height

	xresnow = x;
	yresnow = y;
	if (navigator.appVersion.indexOf("Mac")!=-1 && document.all) {
			xresnow-=0;
			yresnow-=0;
	}
	xl = (screen.availWidth/2)-(xresnow/2);
	tt = (screen.availHeight/2)-(yresnow/2);
  if (document.all) mw=window.open(location,'TaskManagerP','scrollbars=yes,width='+xresnow+',height='+yresnow+',left='+xl+',status=yes,top='+tt);
  else {
    if (parseFloat(navigator.appVersion)<4.75)
      mw=window.open(location,'TaskManagerP','left='+xl+',top='+tt+',width='+x+',height='+y+',toolbar=0,status=0,location=0');
    else {
      mw=window.open(location,'TaskManagerP','scrollbars=yes,toolbar=0,status=0,location=0');
      setTimeout("mwFix("+xl+","+tt+",x,y)",10);
    }
  }
}

function mwFix(ll,tt,ww,hh) {
  ow=window.outerWidth-window.innerWidth;
  oh=window.outerHeight-window.innerHeight;
  mw.resizeTo(ww-ow,hh);
  mw.moveTo(ll,tt);
}


function UpdateContent(){
	thisForm.action = "UpdateContent_svr.asp";
	thisForm.method = "Post";
	thisForm.target = "CONTENTFrame";
	thisForm.submit();
	}

function UpdateCategoryContent(){
	thisForm.action = "UpdateCategoryContent_svr.asp";
	thisForm.method = "Post";
	thisForm.target = "CONTENTFrame";
	thisForm.submit();
	}

function UpdateMenuText()
	{
			/*
			elVal = document.getElementById("txtLinkURL").value
			var url_match = /https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?/;
			result = url_match.test(TrimAll(elVal))
			if (!result) 
				{
					alert("You must input a valid URL with http:// at the start");
					return false;	
				}
			else 
				{
				*/
					thisForm.action = "Serverside/UpdateMenu_svr.asp";
					thisForm.method = "Post";
					thisForm.target = "CONTENTFrame";
					thisForm.submit();		
				/*
				}
				*/
	}

function LTrimAll(str)
{
	if (str==null){return str;}
	for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	return str.substring(i,str.length);
}

function RTrimAll(str)
{
	if (str==null){return str;}
	for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	return str.substring(0,i+1);
}

function TrimAll(str)
{
	return LTrimAll(RTrimAll(str));
}

function DeleteMenuItem()
{
    var where_to= confirm("Are you sure you wish to delete this menu item and all its related content");
    if (where_to== true)
    {
        thisForm.action = "DeleteMenuItem_svr.asp";
        thisForm.method = "Post";
        thisForm.target = "CONTENTFrame";
        thisForm.submit();
    }
}

function DeleteSkillsMenu()
{
    var where_to= confirm("Are you sure you wish to delete this menu item and all its related content");
	if (where_to== true)
	{
	    thisForm.action = "Serverside/DeleteSkillsMenu_svr.asp";
	    thisForm.method = "Post";
	    thisForm.target = "CONTENTFrame";
	    thisForm.submit();
	}
}

function refreshSideBar2()
{
	parent.window.frames["TreeFrame"].location.reload();
	parent.refreshSideBar4();
}
		
function refreshSideBar()
{
	parent.window.frames["TreeFrame"].location.reload();
	parent.location.href="cms2.asp"
}
	
function test()
{
	//alert("test")
	var oEditor = FCKeditorAPI.GetInstance('FCKeditor2') ;
	oEditor.InsertHtml('<div style="width:100%;height:50px;background-color:#EEEEEE"></div>')
}	
</script>
<body bgcolor="#FFFFFF" text="#000000">
<form name="thisForm" action="">
<% IF levelid = 130 THEN %>
<table align="center">
<tr>
<td><input type="text" name="Catname" style="width:350" value="<%=menutitle%>" /></td>
</tr>
<tr>
<td><%
Dim sBasePath
sBasePath = LCASE(Request.ServerVariables("PATH_INFO"))
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )
FlagHeight = 2
Dim oFCKeditor, testtesxt, FlagHeight
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = Comments1
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor2"
		%>
</td>
</tr>
<tr>
<td><input type="text" name="Heading1" style="width:350" value="<%=Heading1%>" /></td>
</tr>
<tr>
<td><%
sBasePath = LCASE(Request.ServerVariables("PATH_INFO"))
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )
FlagHeight = 3
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = Comments2
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor3"

%>
</td>
</tr>
<tr>
<td><input type="text" name="Heading2" style="width:350" value="<%=Heading2%>" /></td>
</tr>
<tr>
<td><%

sBasePath = LCASE(Request.ServerVariables("PATH_INFO"))
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )
FlagHeight = 3
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = Comments3
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor4"

%>
</td>
</tr>
<tr>
<td><input type="text" name="Heading3" style="width:350" value="<%=Heading3%>" /></td>
</tr>
<tr>
<td><%

sBasePath = LCASE(Request.ServerVariables("PATH_INFO"))
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )
FlagHeight = 3
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = Comments4
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor5"

%>
</td>
</tr>
<% IF NOT AREAID = 18 THEN %>
<tr>
<td><input type="text" name="Heading4" style="width:350" value="<%=Heading4%>" /></td>
</tr>
<tr>
<td><%

sBasePath = LCASE(Request.ServerVariables("PATH_INFO"))
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )
FlagHeight = 3
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = Comments5
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor6"

%>
</td>
</tr>
<tr>
<td><input type="text" name="Heading5" style="width:350" value="<%=Heading5%>" /></td>
</tr>
<tr>
<td><%

sBasePath = Request.ServerVariables("PATH_INFO")
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )
FlagHeight = 3
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = Comments6
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor7"

%>
</td>
</tr>
<tr>
	<td style="border:solid" bgcolor="beige">
		<table width="480" style="background-color:beige">
			<tr>
				<td><b>Links:</b></td>
			</tr>
			<% 
				'response.write strLink
				response.write datelist
			%>
			<tr>
				<td align="right"><input type="button" value="Edit Links" onclick="javascript:LaunchWindowSize('CategoryLinks.asp?Areaid=<%=AREAID%>&jobid=<%=MENUID%>')" /></td>
			</tr>
		</table> 
	</td>
</tr>
<% END IF %>
<tr>
	<td><input type="button" name="UpdateButton" value="Save Changes" onclick="UpdateCategoryContent()" /> <input type="button" name="DeleteSkillsButton" value="Delete" onclick="DeleteSkillsMenu()" /></td>
</tr>
<tr>
	<td><input type="hidden" name="hid_PAGEID" value="<%=PAGEID%>" /><input type="hidden" name="hid_PAGENAME" value="<%=JOBSPAGENAME%>" /><input type="hidden" name="hid_MENUID" value="<%=menuid%>" /><input type="hidden" name="hid_AREAID" value="<%=areaid%>" /></td>
</tr>
</table>
<% ELSEif DontDeleteFile = 1 then%>
	<table border="1" align="center" width="450">
	<tr><td>
	<table style="background-color:beige" width="450">
		<tr>
			<td colspan="2"><b>There is no page or content associated with this menu title</b></td>
		
		</tr>
		<tr>
			<td width="60">Menu Text:</td>
			<td><input type="text" name="txtMenuTitle" value="<%=MENUTITLE%>" style="width:330" /></td>
		</tr>
		<tr>
			<td>Active:</td>
			<td> Yes: 
			  <input type="radio" name="radActive" value="1" <%=ActiveYes%> /> No: <input type="radio" name="radActive" value="0" <%=ActiveNo%> /></td>
		</tr>
		<% if ActiveLink = 2 then %>
		<tr>
			<td>Link URL:</td>
			<td><input type="TEXT" name="txtLinkURL" style="width:380" value="<%=ExternalURL%>" /></td>
		</tr>
		<% end if %>
		<tr>
			<td></td>
			<td align="right"><input type="button"  name="Deletebutton" value="Delete Menu Item" onclick="DeleteMenuItem()" /> <input type="button" name="UpdateMenuButton" value="Update" onclick="UpdateMenuText()" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="hidden" name="hid_DontDelete" value="<%=DontDeleteFile%>" /><input type="hidden" name="hid_MENUID" value="<%=menuid%>" /><input type="hidden" name="hid_PAGEID" value="<%=PAGEID%>" /></td>
		</tr>
	</table>
	</td>
	</tr>
	</table>
<% ELSE %>
<table border="1" align="center">
	<tr>
		<td>
			<table style="background-color:beige">
				<tr>
					<td colspan="2"><b>PAGE CONTENT</b></td>
				</tr>
				<tr>
				   	<td>Menu Text:</td>
					<td><input type="text" name="txtMenuTitle" value="<%=MENUTITLE%>" style="width:300" /></td>
				</tr>
				<tr>
					<td colspan="2" class="ErrorDivText"><div id="AssignHistoryDIV" class="ErrorDivText"></div></td>
				</tr>
				<tr>
				   	<td>File Name:</td>
					<td><input type="text" name="txtFileName" value="<%=FileName%>" style="width:300" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="text" name="txtPageName" value="<%=PAGE_NAME%>.asp" style="width:250" readonly="readonly" /> (readonly)</td>
				</tr>
				<tr>
					<td>Page Title:</td>
					<td><input type="text" name="txtPageTitle" value="<%=PAGE_TITLE%>" style="width:300" /></td>
				</tr>
				<tr>
					<td>Active:</td>
					<td> Yes: 
			        <input type="radio" name="radActive" value="1" <%=ActiveYes%> /> No: <input type="radio" name="radActive" value="0" <%=ActiveNo%> /></td>
				</tr>
				<tr>
					<td></td>
					<td>
					<input type="hidden" name="hid_PAGEID" value="<%=PAGEID%>" />
					<input type="hidden" name="hid_MENUID" value="<%=menuid%>" />
					<input type="hidden" name="hid_AREAID" value="<%=areaid%>" />
					<input type="hidden" name="hid_DontDelete" value="<%=DontDeleteFile%>" />
					<input type="hidden" name="hid_FileName" value="<%=FileName%>" />
					</td>
				</tr>
						<%
' Automatically calculates the editor base path based on the _samples directory.
' This is usefull only for these samples. A real application should use something like this:
' oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
'Dim sBasePath
sBasePath = LCASE(Request.ServerVariables("PATH_INFO"))
sBasePath = Left( sBasePath, InStrRev( sBasePath, "/portalmanager" ) )

'Dim oFCKeditor, testtesxt
Set oFCKeditor = New FCKeditor

oFCKeditor.BasePath	= sBasePath
testtesxt = PAGE_CONTENT
//oFCKeditor.Value	= "This is some <strong>sample text</strong>. You are using <a href=""http://www.fckeditor.net/"">FCKeditor</a>."
oFCKeditor.Value	= testtesxt
oFCKeditor.Create "FCKeditor1"

%>
				<tr>
					<td></td>
					<td align="right">
					<input type="button" name="UpdateButton" value="Update" onclick="UpdateContent()" style="cursor:pointer" /> 
					<input type="button"  name="Deletebutton" value="Delete" onclick="DeleteMenuItem()" style="cursor:pointer" disabled="disabled" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<% END IF %>
<iframe name="CONTENTFrame" id="CONTENTFrame" style="display:none"></iframe>
</form>
</body>
</html>