<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%


	Dim menutitle, ParentID, PageName, MENUID

	menutitle 	= request("txtMenu")
	ParentID 	= request("ParentID")
	MENUID	 	= request("MENUID")
	PageName	= request("txtMenu")
	Active		= request("radActive")
	'isLink		= request("radisLink")
	isExternal	= request("radisExternal")
	LinkURL		= request("txtLinkURL")
	TheFileName	= request("txtFileName")
	menutitle = Replace(menutitle,"'","")
	
	if LinkURL = "" then
		LinkURL = NULL
	end if
	'response.Write Active & "<br>"
	'response.Write isLink & "<br>"
	'response.Write isExternal
	'response.End
	IF MENUID = 49 THEN
	
		TEMPLATE = 2
	
	ELSE
	
		TEMPLATE = 1
	
	END IF
	
	//Function to remove bad characters (spaces and commors etc) from the pagename in order to be used for the filename
	// e.g 'Language Support' will be converted into 'LanguageSupport.asp'
	RegExpTrimAll_Version2(TheFileName)
	
		if not isExternal = 0 then
		
			CALL CreatePageTemplate(newstring,MENUID)
			
		end if

	if not ErrorValue = 1 then
	
		Call OpenDB()
		
			//Stored Procedure that inserts the name of the menu item into _tbl_menus.
			//AND to insert a row into _tbl_pages to link to the menu
		
				Dim cmd, param
			
			
				if MENUID = 130 then
		
					Set cmd=server.CreateObject("ADODB.Command")
					With cmd
					  .CommandType=adcmdstoredproc
					  .CommandText = "stp_AddSkillsForJobsMenu"
					  set .ActiveConnection=conn
					  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
					  .parameters.append param
					  set param = .createparameter("@MENUTITLE", adVarChar, adParamInput, 100, menutitle)
					  .parameters.append param
					  set param = .createparameter("@PARENTID", adInteger, adParamInput, 0, ParentID)
					  .parameters.append param
					  set param = .createparameter("@PAGENAME", adVarChar, adParamInput, 100, newstring)
					  .parameters.append param
					  set param = .createparameter("@ACTIVE", adInteger, adParamInput, 0, Active)
					  .parameters.append param
					  set param = .createparameter("@ISINTERNAL", adInteger, adParamInput, 0, isExternal)
					  .parameters.append param
					  set param = .createparameter("@URL", adVarChar, adParamInput, 200, LinkURL)
					  .parameters.append param
					  .execute ,,adexecutenorecords
					end with
			
			
				else
			
			
			
					Set cmd=server.CreateObject("ADODB.Command")
					With cmd
					  .CommandType=adcmdstoredproc
					  .CommandText = "CMS_MENU_ITEM_ADD"
					  set .ActiveConnection=conn
					  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
					  .parameters.append param
					  set param = .createparameter("@MENUTITLE", adVarChar, adParamInput, 100, menutitle)
					  .parameters.append param
					  set param = .createparameter("@PARENTID", adInteger, adParamInput, 0, ParentID)
					  .parameters.append param
					  set param = .createparameter("@PAGENAME", adVarChar, adParamInput, 100, newstring)
					  .parameters.append param
					  set param = .createparameter("@FILENAME", adVarChar, adParamInput, 100, newstring)
					  .parameters.append param
					  set param = .createparameter("@TEMPLATE", adInteger, adParamInput, 0, TEMPLATE)
					  .parameters.append param
					  set param = .createparameter("@ACTIVE", adInteger, adParamInput, 0, Active)
					  .parameters.append param
					  set param = .createparameter("@ISINTERNAL", adInteger, adParamInput, 0, isExternal)
					  .parameters.append param
					  set param = .createparameter("@URL", adVarChar, adParamInput, 200, LinkURL)
					  .parameters.append param
					  .execute ,,adexecutenorecords
					end with
					
				end if
			
			call CloseDB()
		
	end if
%>
<HTML>
<HEAD>
</HEAD>
<BODY onLoad="returnData()">
<script language="javascript">
function returnData()
{
	if('<%=ErrorValue%>' == 1)
		{
			parent.document.getElementById('AssignHistoryDIV').innerHTML = '<%=ErrorMessage%>'
			return false;
		}
	parent.refreshSideBar();	
}	
</script>
</BODY>
</HTML>