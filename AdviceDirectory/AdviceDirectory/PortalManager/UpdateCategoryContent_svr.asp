<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
		
	Dim Heading1, Heading2, MENUID

	Comments1 	= request("FCKeditor2")
	Comments2 	= request("FCKeditor3")
	Comments3 	= request("FCKeditor4")
	Comments4 	= request("FCKeditor5")
	Comments5 	= request("FCKeditor6")
	Comments6 	= request("FCKeditor7")
	Heading1 	= request("Heading1")
	Heading2 	= request("Heading2")
	Heading3 	= request("Heading3")
	Heading4 	= request("Heading4")
	Heading5 	= request("Heading5")
	MENUTITLE 	= request("Catname")
	MENUID 		= request("hid_MENUID")
	OldPageName = request("hid_PAGENAME")
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' The Intro page doesnt have these fields so we set the variables to NULL using the IstheStringNull function
	Comments5 	= IstheStringNull(Comments5)
	Comments6 	= IstheStringNull(Comments6)
	Heading4 	= IstheStringNull(Heading4)
	Heading5 	= IstheStringNull(Heading5)
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Function to remove bad characters (spaces and commors etc) from the pagename in order to be used for the filename
	' e.g 'Language Support' will be converted into 'LanguageSupport.asp'
	RegExpTrimAll_Version2(MENUTITLE)
	OldPageName = OldPageName & ".asp"
	NewPageName = newstring & ".asp"
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Function that renames the old page to the new page name we just created
	Call RenameTemplatePage(OldPageName,NewPageName)
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Call OpenDB()
		Dim cmd, param

			'Stored procedure that updates all the headings and comments fields in TBL_SKILLSFORJOBS_AREA
			' and updates the menu title in _TBL_MENUS

			Set cmd=server.CreateObject("ADODB.Command")
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "stp_UpdateCategoryContent"
			  set .ActiveConnection=conn
			  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param
			  set param = .createparameter("@Comments1", adVarChar, adParamInput, 1000, Comments1)
			  .parameters.append param
			  set param = .createparameter("@Comments2", adVarChar, adParamInput, 1000, Comments2)
			  .parameters.append param
			  set param = .createparameter("@Comments3", adVarChar, adParamInput, 1000, Comments3)
			  .parameters.append param
			  set param = .createparameter("@Comments4", adVarChar, adParamInput, 1000, Comments4)
			  .parameters.append param
			  set param = .createparameter("@Comments5", adVarChar, adParamInput, 1000, Comments5)
			  .parameters.append param
			  set param = .createparameter("@Comments6", adVarChar, adParamInput, 1000, Comments6)
			  .parameters.append param
			  set param = .createparameter("@Heading1", adVarChar, adParamInput, 1000, Heading1)
			  .parameters.append param
			  set param = .createparameter("@Heading2", adVarChar, adParamInput, 1000, Heading2)
			  .parameters.append param
			  set param = .createparameter("@Heading3", adVarChar, adParamInput, 1000, Heading3)
			  .parameters.append param
			  set param = .createparameter("@Heading4", adVarChar, adParamInput, 1000, Heading4)
			  .parameters.append param
			  set param = .createparameter("@Heading5", adVarChar, adParamInput, 1000, Heading5)
			  .parameters.append param
			  set param = .createparameter("@MENUTITLE", adVarWChar, adParamInput, 100, MENUTITLE)
			  .parameters.append param
			  set param = .createparameter("@MENUID", adInteger, adParamInput, 0, MENUID)
			  .parameters.append param
			  set param = .createparameter("@PAGE_NAME", adVarChar, adParamInput, 100, newstring)
 		 		.parameters.append param
			  set param = .createparameter("@strArea", adVarChar, adParamInput, 100, MENUTITLE)
 		 		.parameters.append param	
			  .execute ,,adexecutenorecords
			end with

	call CloseDB()
%>
<HTML>
<HEAD>
</HEAD>
<BODY onLoad="returnData()">
<script language="javascript">
function returnData()
{
	parent.refreshSideBar();	
}
</script>
</BODY>
</HTML>