<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	'Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
	Call OpenDB()
	
		AREAID = request("AREAID")

		SQL =   "SELECT L.LINKID, L.STRLINK, L.STRHREF FROM " &_
				" TBL_SKILLSFORJOBS_LINK L " &_
				"	INNER JOIN TBL_SKILLSFORJOBS_LINKPATH LP ON LP.LINKID = L.LINKID " &_
				"	WHERE LP.JOBID = " & AREAID
	Call OpenRs(rsLINK, SQL)		
		If NOT rsLINK.EOF Then			 			
			do until rsLINK.EOF
				STRLINK = rsLINK("STRLINK")
				LINKID = rsLINK("LINKID")
		
				rid = rid & rsLINK("LINKID")
					
			rsLINK.movenext
			
				If NOT rsLINK.EOF Then	
					rid = rid & ","
				end if
			loop					
		End If		
	Call CloseRS(rsLINK)
			
		Call BuildSelect(STR_Contacts,"sel_LINKS","TBL_SKILLSFORJOBS_LINK","LINKID,STRLINK","STRLINK","Please Select",Null,"width=350","EntryFields"," tabindex='6' onchange='SetLinkDetails()' ")
	
	Call CloseDB()
	
%>
<HTML>
<HEAD>
<TITLE>Skills for Jobs links</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="Javascript">
<!--
	
	function AddGroup_base(strselect,strtextbox,strtextarea){
		
		ref = document.all;
		temp2 = ref[strselect].value;
		if (temp2 == "") return true;
		temp = ref[strtextbox].value + "";
		// if first entry in text area
		if (temp == "") {
			ref[strtextbox].value = "" + temp2 + "";
			populategroups_base(strselect,strtextbox,strtextarea);
			return true;
			}
		thearray = temp.split(",");
		
		for (i=0; i<thearray.length; i++){
			temp3 = "" + temp2 + "";
			// if already in there
			if (temp3 == thearray[i]){
				return true;
				}
			}
		ref[strtextbox].value += "," + temp3;
		populategroups_base(strselect,strtextbox,strtextarea);
	}
	
		// if action = 1 then clear partners, otherwise clear priority groups
	function clearall_base(strselect,strtextbox,strtextarea){
		
		ref = document.all;

		ref[strtextbox].value = "";
		populategroups_base(strselect,strtextbox,strtextarea);

	}
	
	
	function RemoveGroup_base(strselect,strtextbox,strtextarea){

		ref = document.all;
			temp2 = ref[strselect].value;
			if (temp2 == "") return true;	
			temp = ref[strtextbox].value + "";
			if (temp == "") {
				populategroups_base(strselect,strtextbox,strtextarea)	
				return true;
				}
			thearray = temp.split(",");
			for (i=0; i<thearray.length; i++){
				temp3 = "" + temp2 + "";
				if (temp3 == thearray[i]){
					newstring = ""
					for (j=0; j<thearray.length;j++){
						if (j != i){
							if (newstring != "")
								newstring += "," + thearray[j] + "";
							else
								newstring += "" + thearray[j] + "";
							}
						}
					ref[strtextbox].value = newstring;
					populategroups_base(strselect,strtextbox,strtextarea)
					return true;
					}
				}
				
	}	
	function populategroups_base(strselect,strtextbox,strtextarea){

		ref = document.all;
		ref[strselect].disabled = true;
		mystring = ""
		ref[strselect].options[ref[strselect].selectedIndex].text;
		temp = ref[strtextbox].value + "";
		if (temp == "") {
			ref[strtextarea].value = "";	
			ref[strselect].disabled = false;	
			return true;
			}
		thearray = temp.split(",");
		mystring = new Array();
		for (i=0; i<thearray.length; i++){
			ref[strselect].value = thearray[i];
			if (ref[strselect].value == "")
				mystring[i] = "Unknown Group";
			else
				mystring[i] = ref[strselect].options[ref[strselect].selectedIndex].text;
			}
		mystring = mystring.sort();
		mystring = mystring + "";
		mystring = mystring.replace(/\,/g, ",  ");	
		ref[strtextarea].value = mystring;
		ref[strselect].value = "";
		ref[strselect].disabled = false;
		
	}

	function Upload()
		{		
	
			thisForm.submit();
		
		}
	
	function SubmitURL(itemName, errName)
	{		
			if(document.LinkForm.txtLinkName.value == '')
		{
			alert("You must enter a Link name")
			return false;
		}
		if(document.LinkForm.txtLinkURL.value == '')
		{
			alert("You must enter a URL prefixed with http://")
			return false;
		}
		
		
		elVal = document.getElementById("txtLinkURL").value
		var url_match = /https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?/;
		result = url_match.test(TrimAll(elVal))
	if (!result) 
		{
			alert("You must input a valid URL with http:// at the start");
			return false;	
		}
	else 
		{
			//alert("ok")	//return true;
			
			LinkForm.action = "Serverside/UpdateURL_svr.asp";
			LinkForm.method = "Post";
			LinkForm.target = "LINKFRAME";
			LinkForm.submit();
		
		}
			
	}
	
	
	function LTrimAll(str) {
		if (str==null){return str;}
		for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
		return str.substring(i,str.length);
	}
	
	function RTrimAll(str) {
		if (str==null){return str;}
		for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
		return str.substring(0,i+1);
	}
	
	function TrimAll(str)
	{
		return LTrimAll(RTrimAll(str));
	}
	
	function isURL (itemName, errName){
		elVal = document.getElementById("TXT_LINK").value
		var url_match = /https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?/;
		result = url_match.test(TrimAll(elVal))
	if (!result) {
		alert("You must input a valid URL for '" + result + "',\nCorrect format.");
		return false;	
		}
	else {
		alert("ok")
		return true;
		}
	}
	
	
	function ReloadParent(){
		opener.location.reload();
		//parent.location.reload();
		}
	
	function CreateNewLink()
	{		
		if(document.NewLinkForm.txtNewLinkName.value == '')
		{
			alert("You must enter a Link name")
			return false;
		}
		if(document.NewLinkForm.txtNewLinkURL.value == '')
		{
			alert("You must enter a URL with http:// at the start")
			return false;
		}
		
			elVal = document.getElementById("txtNewLinkURL").value
			var url_match = /https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?/;
			result = url_match.test(TrimAll(elVal))
		if (!result) 
			{
				alert("You must input a valid URL with http:// at the start");
				return false;	
			}
		else 
			{
			
		    NewLinkForm.action = "Serverside/CreateNewLink_svr.asp";
			NewLinkForm.method = "Post";
			NewLinkForm.target = "NEWLINKFRAME";
			NewLinkForm.submit();
			
			}
		
	}
	
	function DeleteLink()
		{
		
			var where_to= confirm("Are you sure you wish to permanently delete this link?");
 			if (where_to== true)
				{
					LinkForm.action = "Serverside/DeleteLink_svr.asp";
					LinkForm.method = "Post";
					LinkForm.target = "LINKFRAME";
					LinkForm.submit();
				}
		}
	
	function SwitchDivs()
		{
		LinkForm.style.display 			= "none"
		NewLinkForm.style.display 		= "block"
		}
	
	function refreshSideBar(){
		window.location.href = "../CategoryLinks.ASP?AREAID=" + <%=AREAID%>
		//location.href="CategoryLinks.asp"
		}
	
	function SetLinkDetails(thing){
	
    var lb = document.getElementById('sel_LINKS');
    var selected = lb.selectedIndex


     //alert(lb.options[selected].text)
     //alert(lb.options[selected].value)
	document.LinkForm.txtLinkName.value = lb.options[selected].text
	document.LinkForm.Hid_urlID.value = lb.options[selected].value
	document.LinkForm.DeleteLinkBtn.disabled = false
	document.LinkForm.SaveLinkBtn.disabled = false
	
    LinkForm.action = "Serverside/SelectURL_svr.asp";
	LinkForm.method = "Post";
	LinkForm.target = "LINKFRAME";
	LinkForm.submit();
	}
	
//-->
</SCRIPT> 

<STYLE type="text/css">
<!--
	.pad-both{padding-left:20px;padding-right:13px}
	.pad-left{padding-left:20px}
	td{font-family:arial;font-size:12px}
	input{border:1px dotted silver}
.style1 {
	font-size: 14px;
	font-weight: bold;
}
-->
</STYLE>
</HEAD>

<BODY BGCOLOR=#cc0000 LEFTMARGIN=20 TOPMARGIN=20 MARGINWIDTH=20 MARGINHEIGHT=20 ONLOAD="window.focus();populategroups_base('sel_LINKS','Hid_LINKS','ContactListArea')">
<div id="HTMLLayer"> 
<TABLE WIDTH=400PX HEIGHT=354PX BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR><TD>
	<TABLE STYLE='BORDER:1PX SOLID #000066' WIDTH=100%  BGCOLOR=#FFFFFF CELLPADDING=0 CELLSPACING=0>
		<tr><td height=20px valign=bottom>&nbsp;
		</td></tr>
		<TR><TD>
		<TABLE width=100%><TR><TD width=8px>
			
		</td><TD >
			<span class="style1">Job Link Management </span></TD>
		</TR></TABLE>
		</TD></TR>
		<TR><TD style='padding-left:60px'>
		</TD>
		</TR>
		<TR><TD align="center" bgcolor="#000000"><IMG SRC="img/spacer.gif" WIDTH=500 HEIGHT=1 ALT=""></TD></TR>
		<TR><TD class="pad-left" height=280px valign=top>
			<FORM name="thisForm" method="POST" action="ServerSide/CategoryLinks_svr.asp">
				<table width="509" class="iagManagerSmallBlk" border=0 cellpadding="0" cellspacing="0">
                	<tr> 
                    	<td width="122" height=15 colspan=2></td>
                     
                    </tr>
					<tr>
					<tr> 
                    	<td width="140" height=25>Job List:</td>
                     	<td width=201 valign="top"> <%=STR_Contacts %> </td>
                    </tr>
                    <tr> 
                        <td width="122" valign="top"> 
                        	<input type=button name="grpAdd_bar2" onClick="AddGroup_base('sel_LINKS','Hid_LINKS','ContactListArea')" value=' Add ' class="EntryButton" style="margin-top:10px;width:80px" >
                        	<input type=button name="grpRemove_bar3" onClick="RemoveGroup_base('sel_LINKS','Hid_LINKS','ContactListArea')" value=' Remove ' class="EntryButton" style="margin-top:10px;width:80px">
                        	<input type=button name="grpRemove_bar22" onClick="clearall_base('sel_LINKS','Hid_LINKS','ContactListArea')" value=' Clear' class="EntryButton" style="margin-top:10px;width:80px">
                        </td>
                        <td width=201> 
							<INPUT TYPE="hidden" NAME="HID_AREAID" VALUE="<%=AREAID%>">
                            <input type="hidden" readonly name="Hid_LINKS" value="<%=rid%>">
							
					
							
                            <textarea name="ContactListArea" readonly rows=8 cols=25 style="width:400px" class="EntryFields"></textarea>
                        </td>
                    </tr>
           		</table>
				</FORM>
				<input type="Button" name="Submit" value="Create a New Link" class="EntryButton" onClick="SwitchDivs()">
				<form name="NewLinkForm" style="display:none">
				<table width="600">
				<tr>
				<td></td>
				<td></td>
				</tr>
				<tr>
				<td>Link name:</td>
				<td><input type="text" name="txtNewLinkName" style="width:530"></td>
				</tr>
				<tr>
				<td>URL:</td>
				<td><input type="text" name="txtNewLinkURL" style="width:530"></td>
				</tr>
				<tr>
				<td></td>
				<td align="right"><input type="Button" name="NewLinkBtn" value="Create Link" class="EntryButton" onClick="CreateNewLink('LINK','LINK')"></td>
				</tr>
				</table>
				<iframe name=NEWLINKFRAME id=NEWLINKFRAME style='display:NONE'></iframe>
				</form>
				<form name="LinkForm">
				<TABLE width="600">
				<TR>
						<TD></TD>
						<TD><input type="hidden" readonly name="Hid_urlID"><INPUT TYPE="hidden" NAME="HID_AREAID2" VALUE="<%=AREAID%>"></TD>
					</TR>
					<TR>
						<TD>Link name:</TD>
						<TD><input type="text" name="txtLinkName" style="width:530"></TD>
					</TR>
					<TR>
						<TD>URL:</TD>
						<TD><input type="text" name="txtLinkURL" style="width:530"></TD>
					</TR>
					<TR>
						<TD></TD>
					  <TD align="right"><input type="Button" name="DeleteLinkBtn" value="Delete Link" class="EntryButton" onClick="DeleteLink()" disabled="disabled"> <input type="Button" name="SaveLinkBtn" value="Update" class="EntryButton" onClick="SubmitURL('LINK','LINK')" disabled="disabled"></TD>
					</TR>
				</TABLE>
				 
				<iframe name=LINKFRAME id=LINKFRAME style='display:none'></iframe>
			</form>
	</TD></TR>
	<TR><TD bgcolor="#000000"><IMG SRC="/img/spacer.gif" WIDTH=498 HEIGHT=1 ALT=""></TD></TR>
	<TR>
    	<TD HEIGHT=40px style='padding-RIGHT:50px' ALIGN=RIGHT> 
        	<input type="button" name="Close" value=" Close " class="EntryButton" onClick="javascript:ReloadParent();window.close()"> <input type="Button" name="Submit" value="Save Links" class="EntryButton" style="margin-top:10px;width:80px" onClick="Upload()">
             
		</TD>
    </TR>
	</TABLE>
</TD><TD WIDTH=7></TD></TR>
<TR><TD><IMG SRC="/images/LoginPage/im_bottomshadow.gif" WIDTH=500 HEIGHT=7 ALT=""></TD></TR>
</TABLE>
<iframe name="Finance_frame" width="300" height="300" id="Finance_frame" style='display:none'></iframe>
</div>
</BODY>
</HTML>