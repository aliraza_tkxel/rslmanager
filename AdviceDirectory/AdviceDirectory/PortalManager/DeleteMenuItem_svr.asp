<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include virtual="/portalmanager/LoginCheck.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%

	Dim MENUID, PageName

	MENUID	 			= request("hid_MENUID")
	PAGEID	 			= request("hid_PAGEID")
	PageName	 		= request("txtPageName")
	DontDelete	 		= request("hid_DontDelete")

	'Only run the delete file function if the DontDelete flag = 2
	If not DontDelete = 1 Then
	    DeleteFile(PageName)
	End If

	Call OpenDB()

		Dim cmd, param
		Set cmd=server.CreateObject("ADODB.Command")
		With cmd
		  .CommandType=adcmdstoredproc
		  .CommandText = "stp_DELETEMENUITEM"
		  Set .ActiveConnection=conn
		  Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  .parameters.append param
		  Set param = .createparameter("@MENUID", adInteger, adParamInput, 0, MENUID)
		  .parameters.append param
		  .execute ,,adexecutenorecords
		End With

		If Not DontDelete = 1 Then
			Dim cmd2, param2
			Set cmd2=server.CreateObject("ADODB.Command")
			With cmd2
			  .CommandType=adcmdstoredproc
			  .CommandText = "stp_DELETEPAGE"
			  Set .ActiveConnection=conn
			  Set param2 = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param2
			  Set param2 = .createparameter("@PAGEID", adInteger, adParamInput, 0, PAGEID)
			  .parameters.append param2
			  .execute ,,adexecutenorecords
			End With
		End If

	Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
</head>
<body onload="returnData()">
<script type="text/javascript" language="javascript">
function returnData()
{
	parent.refreshSideBar();
}	
</script>
</body>
</html>