<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
		
	Dim MENUID, PAGEID, CONTENT, sForm, PageTitle, OldPageName, NewPageName, FileName

	MENUID	 		= request("hid_MENUID")
	PAGEID	 		= request("hid_PAGEID")
	PageTitle	 	= request("txtPageTitle")
	OldPageName		= request("txtPageName")
	NewPageName		= request("txtMenuTitle")
	Active			= request("radActive")
	Parent			= request("hid_PARENT")
	FileName		= request("txtFileName")
	OldFileName		= request("hid_FileName")

	'The menubuilder breaks if ' is included in menu title
	NewPageName = Replace(NewPageName,"'","")
	'Set the menu text to whatever value has just been passed from txtMenuTitle
	MENUPAGE_NAME = NewPageName	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Function to remove bad characters (spaces and commors etc) from the pagename in order to be used for the filename
	' e.g 'Language Support' will be converted into 'LanguageSupport.asp'
	RegExpTrimAll_Version2(FileName)
	NewPageName = newstring & ".asp"
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	response.write NewPageName & "<br>"
	response.write OldFileName & "<br>"
	OldFileName = OldFileName & ".asp"
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Function that renames the old page to the new page name we just created
	Call RenameTemplatePage(OldFileName,NewPageName)
 	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
 	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Content variable is set to whatever has just been entered into the FCK editor
	CONTENT = request("FCKeditor1")
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	If NOT ErrorValue = 1 Then	
	    Call OpenDB()
		    Dim cmd, param
		    'Stored procedure that updates the menu title in _TBL_MENUS and page content, title and page name in _TBL_PAGES
		    Set cmd=server.CreateObject("ADODB.Command")
		    With cmd
		      .CommandType=adcmdstoredproc
		      .CommandText = "CMS_CONTENT_UPDATE"
		    Set .ActiveConnection=conn
		    Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  	    .parameters.append param
		    Set param = .createparameter("@CONTENT", adVarChar, adParamInput, 2147483647, CONTENT)
		  	    .parameters.append param
		    Set param = .createparameter("@MENUID", adInteger, adParamInput, 0, MENUID)
  		  	    .parameters.append param
		    Set param = .createparameter("@PAGEID", adInteger, adParamInput, 0, PAGEID)
		  	    .parameters.append param
		    Set param = .createparameter("@PAGE_TITLE", adVarChar, adParamInput, 100, PageTitle)
		  	    .parameters.append param
		    Set param = .createparameter("@PAGE_NAME", adVarChar, adParamInput, 100, newstring)
		  	    .parameters.append param
		    Set param = .createparameter("@PAGE_NAME", adVarChar, adParamInput, 100, newstring)
 		 	    .parameters.append param
		    Set param = .createparameter("@MENUPAGE_NAME", adVarChar, adParamInput, 100, MENUPAGE_NAME)
			    .parameters.append param
		    Set param = .createparameter("@ACTIVE", adInteger, adParamInput, 0, Active)
		  	    .parameters.append param	
		      .execute ,,adexecutenorecords
		    End With
	    Call CloseDB()	
	End If
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> CMS</title>
</head>
<body onload="returnData()">
<script language="javascript" type="text/javascript">
function returnData()
{
	if('<%=ErrorValue%>' == 1)
	{
		parent.document.getElementById('AssignHistoryDIV').innerHTML = '<%=ErrorMessage%>'
		return false;
	}
	parent.refreshSideBar();
}
</script>
</body>
</html>