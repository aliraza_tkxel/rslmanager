<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="nextstep.css" type="text/css">
</head>
<script type="text/javascript">
	function Switch(itemName, errName)
	{
		if(document.thisForm.txtMenu.value == '')
		{
			alert("You must enter a menu title")
			return false;
		}
		
		if(thisForm.radisExternal[0].checked == false)
		{
		if(document.thisForm.txtFileName.value == '')
		{
			alert("You must enter a file name")
			return false;
		}
		}
		
		if(thisForm.radisExternal[0].checked == true)
		{
		
			elVal = document.getElementById("txtLinkURL").value
			var url_match = /https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?/;
			result = url_match.test(TrimAll(elVal))
			if (!result) 
				{
					alert("You must input a valid URL with http:// at the start");
					return false;	
				}
			else 
				{
					
					
				}
		}
		thisForm.submit();
	}
	
	function LTrimAll(str) {
		if (str==null){return str;}
		for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
		return str.substring(i,str.length);
	}
	
	function RTrimAll(str) {
		if (str==null){return str;}
		for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
		return str.substring(0,i+1);
	}
	
	function TrimAll(str)
	{
		return LTrimAll(RTrimAll(str));
	}
	
	function OpenURL()
	{
		LinkURLrow.style.display 	= "block"
	}
	
	function CloseURL()
	{
		LinkURLrow.style.display 	= "none"
	}
	
function refreshSideBar()
{
	parent.window.frames["TreeFrame"].location.reload();
}
		
</script>
<body bgcolor="#FFFFFF" text="#000000">
<form name="thisForm" method="POST" action="AddNewMenu_svr.asp" target="ricframe">
<table border=1 width=300 bgcolor="beige" align="center">
	<tr>
		<td>
			<table bgcolor="beige">
			<tr>
			<td colspan=2><b>NEW MENU ITEM</b></td>
			</tr>
			<tr>
			<td>Menu Text:</td>
			<td><input type="text" name="txtMenu" style="width:200"></td>
			</tr>
			<tr>
				<td colspan="2" class="ErrorDivText"><div id="AssignHistoryDIV" class="ErrorDivText"></div></td>
			</tr>
			<tr>
				<td>File Name:</td>
				<td><input type="text" name="txtFileName" style="width:200"></td>
			</tr>
			<tr>
			<td>Active:</td>
			<td> Yes: 
			  <input type="radio" name="radActive" value="1"> No: <input type="radio" name="radActive" value="0" checked></td>
			</tr>
			<!--<tr>
			<td>isLink:</td>
			<td> Yes: 
			  <input type="radio" name="radisLink" value="1"> No: <input type="radio" name="radisLink" value="0" checked></td>
			</tr>-->
			<tr>
			<td valign="bottom">Is Link External:</td>
			<td> Yes: 
			  <input type="radio" name="radisExternal" value="0" onClick="OpenURL()"> No: <input type="radio" name="radisExternal" onClick="CloseURL()" value="1" checked></td>
			</tr>
			<tr id="LinkURLrow" style="display:none">
			<td valign="bottom">Link URL:</td>
			<td><input type="TEXT" name="txtLinkURL" style="width:200"></td>
			</tr>
			<tr>
			<td></td>
			<td><input type="HIDDEN" name="ParentID" value="<%=request("MENUID")%>" ><input type="HIDDEN" name="MENUID" value="<%=request("level1id")%>" ></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><input type="button" name="btnSubmit" value="Submit" onClick="Switch('LINK','LINK')"></td>
			</tr>
		  </table>	
		</td>
	</tr>
</table>
<iframe name="ricframe" style="display:none"></iframe>
</form>
</body>
</html>
