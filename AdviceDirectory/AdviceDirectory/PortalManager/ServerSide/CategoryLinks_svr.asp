<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
	'Session.LCID = 2057
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Call OpenDB()

	JOBID 			= request("HID_AREAID")
	
	
	//str_LINKID is a comma seperated string
	str_LINKID	= request("Hid_LINKS")
	

	//Stored procedure to INSERT the jobId and the IDs of each LINK that was added to the textarea
	
	Dim cmd, param

	Set cmd=server.CreateObject("ADODB.Command")
	With cmd
	  .CommandType=adcmdstoredproc
	  .CommandText = "stp_InsertJobLinkID"
	  set .ActiveConnection=conn
	  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
	  .parameters.append param
	  set param = .createparameter("@LinkID", adVarChar, adParamInput, 50, str_LINKID)
	  .parameters.append param
	  set param = .createparameter("@JobID", adInteger, adParamInput, 0, JOBID)
	  .parameters.append param
	  .execute ,,adexecutenorecords
	end with


Call CloseDB()
//Response.Redirect "../CategoryLinks.asp?AREAID=" & JOBID 

%>								
<HTML>
<HEAD>
</HEAD>
<BODY onLoad="returnData()">
<script language="javascript">
function returnData(){
	//parent.refreshSideBar();
	window.location.href = "../CategoryLinks.ASP?AREAID=" + <%=JOBID%>
		
}
	
</script>
</BODY>
</HTML>
