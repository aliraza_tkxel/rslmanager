<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%

Call OpenDB()
		
		LinkName 	= request("txtLinkName")
		LinkURL 	= request("txtLinkURL")
		LINKID 		= request("Hid_urlID")
		AREA 		= request("HID_AREAID2")
		
		Dim cmd, param

		Set cmd=server.CreateObject("ADODB.Command")
		With cmd
		  .CommandType=adcmdstoredproc
		  .CommandText = "stp_UpdateLinkNameURL"
		  set .ActiveConnection=conn
		  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  .parameters.append param
		  set param = .createparameter("@strLink", adVarChar, adParamInput, 200, LinkName)
		  .parameters.append param
		  set param = .createparameter("@strHref", adVarChar, adParamInput, 200, LinkURL)
		  .parameters.append param
		  set param = .createparameter("@LinkID", adInteger, adParamInput, 0, LINKID)
		  .parameters.append param
		  .execute ,,adexecutenorecords
		end with
		
	
	Call CloseDB()
%>
<HTML>
<HEAD>
</HEAD>
<BODY onLoad="returnData()">
<script language="javascript">
function returnData(){
	parent.refreshSideBar();
//window.location.href = "../CategoryLinks.ASP?AREAID=" + <%=AREA%>
		
}
	
</script>
</BODY>
</HTML>
