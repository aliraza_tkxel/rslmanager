<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%			
	Dim MENUID, PageName

	MENUID	 			= request("hid_MENUID")
	AreaID	 			= request("hid_AREAID")
	PageName	 		= request("hid_PAGENAME")

	PageName = PageName & ".asp"
	
	'DeleteFile function located in PortalFunctions.asp. Passes the pagename with '.asp' concatenated on to it	
	DeleteFile(PageName)

	Call OpenDB()
		Dim cmd, param
			
			'Stored procedure that Deletes the menu from _tbl_menus, TBL_SKILLSFORJOBS_AREA 
			'and any link references in TBL_SKILLSFORJOBS_LINKPATH
			
			Set cmd=server.CreateObject("ADODB.Command")
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "stp_DeleteSkillsForJobsMenu"
			  set .ActiveConnection=conn
			  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param
			  set param = .createparameter("@MENUID", adInteger, adParamInput, 0, MENUID)
			  .parameters.append param
			  set param = .createparameter("@AREAID", adInteger, adParamInput, 0, AreaID)
			  .parameters.append param
			  .execute ,,adexecutenorecords
			end with

	call CloseDB()
%>
<HTML>
<HEAD>
</HEAD>
<BODY onLoad="returnData()">
<script language="javascript">
function returnData(){
	//Function located on Content.asp
	parent.refreshSideBar2();

}
</script>
</BODY>
</HTML>