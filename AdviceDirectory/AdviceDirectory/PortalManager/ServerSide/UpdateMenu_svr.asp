<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%

Call OpenDB()
		
		menutitle 		= request("txtMenuTitle")
		MenuID	 		= request("hid_MENUID")
		PageID	 		= request("hid_PAGEID")
		Active			= request("radActive")
		LinkURL			= request("txtLinkURL")
		WhichUpdate		= request("hid_DontDelete")
		
		If LinkURL = "" THEN 
		LinkURL = NULL
		END IF 
		
		
	response.write menutitle & "<br>"
	response.write MenuID & "<br>"
	response.write PageID & "<br>"
	response.write WhichUpdate
			'response.end
		Dim cmd, param
		
		'If there isnt a row in the page table linking to this menu item then run this sproc
		if WhichUpdate = 1 then
		
			Set cmd=server.CreateObject("ADODB.Command")
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "CMS_UPDATE_MENU_TEXT"
			  set .ActiveConnection=conn
			  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param
			  set param = .createparameter("@menutitle", adVarWChar, adParamInput, 100, menutitle)
			  .parameters.append param
			  set param = .createparameter("@MENUID", adInteger, adParamInput, 0, MenuID)
			  .parameters.append param
			  set param = .createparameter("@ACTIVE", adInteger, adParamInput, 0, Active)
			  .parameters.append param
			  set param = .createparameter("@ExternalURL", adVarChar, adParamInput, 1000, LinkURL)
			  .parameters.append param
			  .execute ,,adexecutenorecords
			end with
		
		'if there is a row in the page table then run this sproc
		else
		
			Set cmd=server.CreateObject("ADODB.Command")
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "stp_UpdateDuplicateMenu"
			  set .ActiveConnection=conn
			  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param
			  set param = .createparameter("@MENUTITLE", adVarWChar, adParamInput, 100, menutitle)
			  .parameters.append param
			  set param = .createparameter("@PAGEID", adInteger, adParamInput, 0, PageID)
			  .parameters.append param
			  set param = .createparameter("@Active", adInteger, adParamInput, 0, Active)
			  .parameters.append param
			  set param = .createparameter("@EXTERNALURL", adVarChar, adParamInput, 1000, LinkURL)
			  .parameters.append param
			  .execute ,,adexecutenorecords
			end with
		
		end if
	
	Call CloseDB()
%>
<HTML>
<HEAD>
</HEAD>
<BODY onLoad="returnData()">
<script language="javascript">
function returnData(){
	parent.refreshSideBar2();
//window.location.href = "../CategoryLinks.ASP?AREAID=" + AREA
		
}
	
</script>
</BODY>
</HTML>
