<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!--#include virtual="/includes/globals.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> CMS Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="/css/reidmark.css" type="text/css" />
<script type="text/javascript">
<!--
	function newImage(arg) {
		if (document.images) {
			rslt = new Image();
			rslt.src = arg;
			return rslt;
		}
	}
	
	function changeImages() {
		if (document.images && (preloadFlag == true)) {
			for (var i=0; i<changeImages.arguments.length; i+=2) {
				document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
			}
		}
	}
	
	var preloadFlag = false;
	function preloadImages() {
		if (document.images) {
			im_enter_over = newImage("/images/LoginPage/im_enter-over.gif");
			preloadFlag = true;
		}
	}

	function Forgot(){
		Login_div.style.display = "none"
		Email_div.style.display = "block"
		Forgot_div.style.display = "none"
		TryAgain_div.style.display = "block"
	}
	
	function TryAgain(){
		Login_div.style.display = "block"
		Email_div.style.display = "none"
		Forgot_div.style.display = "block"
		TryAgain_div.style.display = "none"
	}

	function Login(){
	
		thisForm.target = "iFormFrame";
		thisForm.method = "Post";
		thisForm.action = "Serverside/Login_svr.asp";
		thisForm.submit();

	}
	
	function SubmitEmail(){
		nextForm.target = "iFormFrame";
		nextForm.method = "Post";
		nextForm.action = "/Serverside/LogEmail_srv.asp";
		nextForm.submit();
		alert("Thank you. Your login details will be emailed to you shortly.")
	}

	function keyDown() {
		if (event.keyCode == 13) {
			if (Login_div.style.display == "block") 
				Login();
			else
				SubmitEmail();
		}
	}

	document.onkeydown = keyDown	
// -->
</script>
<script type="text/javascript" language="javascript" src="/js/WindowResizer.js"></script>
<script type="text/javascript" language="javascript">
xresnow=542;
yresnow=397;
</script>
<!-- End Preload Script -->
<style type="text/css">
<!--
	.pad-left{padding-left:20px}
	td{font-family:arial;font-size:12px}
	
-->
</style>
</head>
<body bgcolor="#cc0000" onload="preloadImages();">
<div id="HTMLLayer"> 
  <table width="500" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td> 
      <table style="border:1px solid #000066" width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="/images/LoginPage/im_dashedline.gif" width="498" height="1" alt="" /></td>
          </tr>
          <tr>
            <td height="100%" valign="top"> <br/> 
            <form name="thisForm" action="">
                <div id="Login_div" style="display:block"> 
                  <table width="370" align="center">
                    <tr> 
                      <td colspan="2">
                      <strong>Please enter your designated username and password</strong></td>
                    </tr>
                    <tr> 
                      <td width="100">User Name:</td>
                      <td> 
                        <input type="text" name="txtName" id="txtName" size="40" /> 
                      </td>
                    </tr>
                    <tr> 
                      <td width="100">Password:</td>
                      <td> 
                        <input type="password" name="txtPassword" id="txtPassword" size="40" /> 
                      </td>
                    </tr>
                    <tr> 
                      <td colspan="2" bgcolor="#ffffff" align="right">
                        <input type="button" name="UpdateButton" value="Login" title="Submit Button : Login" onclick="Login()" style="cursor:pointer" /> 
                      </td>
                    </tr>
                    <tr> 
                      <td colspan="2"> 
                        <div id="MessageDiv">&nbsp;</div>
                      </td>
                    </tr>
                  </table>
                </div>
              </form>
              <form name="nextForm" action="">
                <div id="Email_div" style="display:none"> 
                  <table width="370" align="center">
                    <tr> 
                      <td colspan="2" style="font-family:arial"> 
                        <strong> Please enter your email and your password will be sent to you</strong> 
                      </td>
                    </tr>
                    <tr> 
                      <td width="120">User Email:</td>
                      <td> 
                        <input type="text" name="txtEmail" size="40" /> 
                      </td>
                    </tr>
                    <tr> 
                      <td colspan="2" align="right"> <a 
					onmouseover="changeImages('im_submit', '/images/LoginPage/im_submit-over.gif'); return true;"
					onmouseout="changeImages('im_submit', '/images/LoginPage/im_submit.gif'); return true;"> 
                        <img name="im_submit" src="/images/LoginPage/im_submit.gif" onclick='SubmitEmail()' width="46" height="15" border="0" alt="Submit" style="cursor:pointer" /></a></td>
                    </tr>
                  </table>
                </div>
              </form>
			 </td>
          </tr>
          <tr>
            <td><img src="/images/LoginPage/im_dashedline.gif" width="498" height="1" alt="" /></td>
          </tr>
          <tr>
            <td height="70" style="padding-left:60px"> 
                <div id="Forgot_div"></div>
                <div id="TryAgain_div" style="display:none">
                    <a href="javascript:TryAgain()"><u><i>Click here to try to login again</i></u></a>
                </div>
            </td>
          </tr>
        </table>
        </td>
      <td width="7">
        </td>
    </tr>
    <tr>
      <td>
        </td>
    </tr>
  </table>
</div>
<iframe name="iFormFrame" id="iFormFrame" style="display:none"></iframe>
</body>
</html>