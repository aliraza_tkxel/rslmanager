<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!--#include virtual="/portalmanager/LoginCheck.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> CMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="StyleSheet" href="dtree.css" type="text/css" />
<style type="text/css">
<!--
.style1 
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9pt;
}
-->
</style>

<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
function refreshSideBar()
{
	document.thisForm.action = "cms.asp";
	document.thisForm.target = "TreeFrame";
	document.thisForm.submit();
}

function refreshSideBar4()
{
	window.location.href = "../cms2.asp"
}
//--><!]]>
</script>
</head>
<body bgcolor="#cc0000">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<form name="thisForm" action="" target="TreeFrame">
				<table style="border:1px solid #000066" width="100%" height="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
							<a href="Logout.asp" class="style1">Logout</a>
					  		<iframe name="TreeFrame" id="TreeFrame" src="cms.asp" height="100%" width="390" frameborder="0" style="display:block"></iframe>
						</td>
						<td width="100%">
							<iframe name="ContentFrame" id="ContentFrame" height="100%" width="100%" frameborder="0" style="display:block"></iframe>
						</td>
					</tr>
				</table>
			</form>
		  </td>
		<td width="7"><img src="/images/LoginPage/im_rightdshadow.gif" width="7" height="100%" alt="" /></td>
	</tr>
	<tr>
	    <td><img src="/images/LoginPage/im_bottomshadow.gif" width="100%" height="7" alt="" /></td>
	</tr>
</table>
<iframe name="hidden_FRAME<%=TimeStamp%>" style="display:block"></iframe>
</body>
</html>