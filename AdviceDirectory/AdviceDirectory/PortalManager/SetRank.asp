<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 
	 
	Response.Buffer = false 
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
	Dim ParentID
		ParentID = request("MENUid") 

	Dim cmd, param

	Set cnSQL = Server.CreateObject("ADODB.Connection")
		cnSQL.Open DSN_CONNECTION_STRING

	set rsAssign = server.CreateObject("ADODB.Recordset")
	Set cmd=server.CreateObject("ADODB.Command")
	With cmd
	  .CommandType=adcmdstoredproc
	  .CommandText = "stp_SelectMenuRanking"
	  set .ActiveConnection=cnSQL
	  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
	  .parameters.append param
	  set param = .createparameter("@ParentMenuID", adInteger, adParamInput, 0, ParentID)
	  .parameters.append param
	  set rsAssign = .execute
	end with
	
	JobID = JobID & "<select name=""selto"" id=""selto"" size=""3"" style=""width:300;HEIGHT:200"" multiple>"
	if NOT rsAssign.EOF then	
		Do until rsAssign.eof 
			title = rsAssign("MenuTitle")
			MenuID = rsAssign("MenuID")
			title = Replace(title,"'"," ")		
			JobID = JobID & "<option value=""1""> "  & title & " </option>"
			TestThing = TestThing & "<option value=""" & rsAssign("MenuID") & """> "  & title & " </option>"
			rsAssign.MoveNext
		Loop
		JobID = JobID & "</select>"
	end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="nextstep.css" type="text/css" />
</head>
<script language="javascript" type="text/javascript">

var optcount = 0

function refreshSideBar()
{
    parent.window.frames["TreeFrame"].location.reload();
    parent.location.href="cms2.asp"
}

function Upload()
{	
		if (document.getElementById('sel_WORKER').value == "")
			{
			alert("Please select a name from the dropdown")
			return false;
			}
		thisForm.target = "TimeSpent_frame";
		thisForm.method = "Post";
		thisForm.action = "ServerSide/Priority_svr.asp";
		thisForm.submit();
}

function sortlist() {
var lb = document.getElementById('selto');
arrTexts = new Array();

for(i=0; i<lb.length; i++)  {
  arrTexts[i] = lb.options[i].text;
}

arrTexts.sort();

for(i=0; i<lb.length; i++)  {
  lb.options[i].text = arrTexts[i];
  lb.options[i].value = arrTexts[i];
}
}

function up()
{
    var lb = document.getElementById('selto');
    var selected = lb.selectedIndex

    varval = 0

    if (selected == -1)
    {
        alert("You have not selected anything!")
        varval = 1
    }
    if (selected == 0)
    {
        alert("You are a the top of the list!")
        varval = 1
    }

    if (varval == 0)
    {
        tmptext = lb.options[selected].text
        tmpvalue = lb.options[selected].value

        lb.options[selected].text = lb.options[selected - 1].text
        lb.options[selected].value = lb.options[selected - 1].value
        lb.options[selected - 1].text = tmptext 
        lb.options[selected - 1].value = tmpvalue
        lb.selectedIndex = selected - 1
    }
}

function down()
{
    var lb = document.getElementById('selto');
    var selected = lb.selectedIndex
    var varval = 0

    if (selected == -1)
    {
        alert("You have not selected anything!")
        varval = 1
    }
    if (selected == lb.length-1)
    {
        alert("You are a the bottom of the list!")
        varval = 1
    }
    if (varval == 0)
    {
        tmptext = lb.options[selected].text
        tmpvalue = lb.options[selected].value
        lb.options[selected].text = lb.options[selected + 1].text
        lb.options[selected].value = lb.options[selected + 1].value
        lb.options[selected + 1].text = tmptext 
        lb.options[selected + 1].value = tmpvalue
        lb.selectedIndex = selected + 1
     }
}

function SetRank()
{
    x = document.thisForm
	rank()
	thisForm.target = "TimeSpent_frame";
	//thisForm.target = "_blank";
	thisForm.method = "POST";
	thisForm.action = "SetRank_svr.asp";
	//thisForm.action = "PTEST.asp";
	thisForm.submit();
}

function rank()
{
	x = document.thisForm
	//alert(x.selto.options.length)
	for(var i=0;i<x.selto.length;i++)
		x.selto[i].selected=true;
		//alert(x.selfrom.value)
}
</script>
<body bgcolor="#FFFFFF" text="#000000">
    <form name="thisForm" action="">
        <table border="1" bgcolor="beige" align="center">
            <tr>
                <td>
                    <table bgcolor="beige" cellpadding="2" width="300" align="center">
                        <tr>
                            <td colspan="2"><b>Menu Ranking</b></td>
                        </tr>
                        <tr>
                            <td colspan="2">Use the Up and Down buttons to order the menu titles how you want then click the Rank button to save.</td>
                        </tr>
                        <tr>
                            <td width="250">
                                <select name="selto" id="selto" size="3" style="width:400;HEIGHT:200" multiple="multiple">
                                    <%=TestThing%>
                                </select>
                            </td>
                            <td align="center" width="100">
                                <input id="Upbtn" type="button" onclick="up()"  value="UP" style="width:44" class="EntryButton" /><br/><br/>
                                <input id="Downbtn" type="button" onclick="down()"  value="DOWN" class="EntryButton" /><br/><br/>
                                <input id="Button1" type="button" onclick="SetRank()"  value="RANK" style="width:44" class="EntryButton" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <iframe style="display:none" name="TimeSpent_frame"></iframe>
    </form>
</body>
</html>