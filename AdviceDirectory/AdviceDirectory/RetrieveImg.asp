<%@LANGUAGE="VBSCRIPT" %>
<%
	'
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Call Debug()
Dim PHOTOGRAPH

    SITEID = Request("SITEID")
    If SITEID = "" Or IsNull(SITEID) Then SITEID = -1 End If

  	SQL = "SELECT PHOTOGRAPH " &_
  	        "FROM TBL_MD_PROVIDER P " &_
	        "INNER JOIN TBL_MD_PROVIDER_DETAIL PD ON p.PROVIDERID = PD.PROVIDERID " &_
	        "INNER JOIN TBL_MD_PROVIDER_SITE S ON p.PROVIDERID = p.PROVIDERID " &_
	        "WHERE S.SITEID = " & SITEID
	Call OpenDB()
	Call OpenRs(rsClient, SQL)
		If NOT (rsClient.EOF) Then
			PHOTOGRAPH = rsClient("PHOTOGRAPH")
		End If
	Call CloseRS(rsClient)
	Call CloseDB()
%>
<%
If IsNull(PHOTOGRAPH) Then
    rw "spacer.gif"
Else
    Response.ContentType = "image/gif"
    Response.Clear
    Response.BinaryWrite(PHOTOGRAPH)
    Response.End()
End If
%>