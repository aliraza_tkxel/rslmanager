﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<!-- #include virtual="/membersarea/includes/processsearch.asp" -->
<%
    'Dim PAGE_TITLE
    'Dim TAB_MENU
	'Dim PAGE_DESCRIPTION
	'Dim PAGE_KEYWORDS
	'Dim LOGIN_TYPE
	'Dim PROVIDERID

	    siteid = request.cookies("siteid")

	    If NullNestTF(siteid) = True Then
	        Response.Redirect "/SessionTimeOut.asp"
	    End If

	Response.Cookies("pg") = request("pg")

	Call OpenDB()

		'Dim cmd, param

			Set cmd=server.CreateObject("ADODB.Command")
			Set rsContactDetails =server.CreateObject("ADODB.Recordset")
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "stp_MD_Select_Site_Details"
			  set .ActiveConnection=conn
			  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param
			  set param = .createparameter("@SiteID", adInteger, adParamInput, 0, siteid)
			  .parameters.append param
			  set rsContactDetails = .execute
			end with

		If NOT rsContactDetails.EOF Then
			SiteName 		= rsContactDetails("SiteName")
			ProviderSummary = rsContactDetails("ProviderSummary")
			AddressLine1 	= rsContactDetails("AddressLine1")
			AddressLine2 	= rsContactDetails("AddressLine2")
			AddressLine3 	= rsContactDetails("AddressLine3")
			AddressLine4 	= rsContactDetails("AddressLine4")
			PostCode 		= rsContactDetails("PostCode")
			Telephone 		= rsContactDetails("Telephone")
			Fax 			= rsContactDetails("Fax")
			ClientContact 	= rsContactDetails("ClientContact")
			Email 			= rsContactDetails("Email")
			Email           = "<a href=""mailto:" & email & """>" & email & "</a>"
			URL             = rsContactDetails("URL")
			URL             = "<a href=""" & URL & """ target=""_new"">" & URL & "</a>"
			OpeningHours 	= rsContactDetails("OpeningHours")
		End If

	Call CloseDB()
%>
<%
	'Calls function that sets up the tab menu
	TAB_MENU = displaycontactmenu(0,LOGIN_TYPE)
	MainSite = true
    PAGE_TITLE = "Contact Details"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="Link1" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--


//-->
</style>

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=SiteName%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
		<%
		    If Session("svLOGINID") <> "" Then
		        Call listCatDIR(404, "navmenu" , "CRMS")
		    Else
		        Call listCat(10, "navmenu")
		    End If
		%>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; margin-right:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform">
                    <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck%></div>
	                    <div>
                            <div class="left">
                                <div id="portal_submenu" style="width:144px;">
                                    <!-- #include virtual="/membersarea/includes/backtosearch.asp" --><br />
                                    <!-- #include virtual="/membersarea/includes/sitephoto.asp" --><br />
                                    <%=TAB_MENU%>
                                </div>
                            </div>
                            <div class="right">
                                <div>
                                     <div class="membersheader">Organisation Summary</div>
                                     <div class="membersbody">
                                        <span><%=ProviderSummary%></span><br />
                                     </div>
                                    <div class="membersheader">Contact Details</div>
                                    <div class="membersbody">
                                        <span><%=SiteName%></span><br />
		                                <span><%=AddressLine1%></span><br />
		                                <span><%=AddressLine2%></span><br />
		                                <span><%=AddressLine3%></span><br />
		                                <span><%=AddressLine4%></span><br />
		                                <span><%=PostCode%></span><br />
                                    </div>
                                    <div class="membersbody">
                                        <span class="lblDetails">Tel:</span> <%=Telephone%><br />
		                                <span class="lblDetails">Fax:</span> <%=Fax%><br />
		                                <span class="lblDetails">Contact:</span><%=ClientContact%><br />
		                                <span class="lblDetails">Email:</span><%=Email%><br />
		                                <span class="lblDetails">Website:</span><%=URL%><br />
                                    </div>
                                    <div class="membersheader">Opening Times</div>
                                    <div class="membersbody">
                                       <%=OpeningHours%>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>