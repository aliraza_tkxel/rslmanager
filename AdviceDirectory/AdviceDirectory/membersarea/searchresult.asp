<%@  language="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  START OF DISTANCE CALCULATION                                 :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

'http://www.zipcodeworld.com/samples/distance.asp.html
'http://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Validation

const pi = 3.14159265358979323846

Function distance(lat1, lon1, lat2, lon2, unit)
  Dim theta, dist
  theta = lon1 - lon2
  dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
  'response.write "dist = " & dist & "<br>"
  dist = acos(dist)
  dist = rad2deg(dist)
  'response.write "dist = " & dist & "<br>"
  distance = dist * 60 * 1.1515
  Select Case lcase(unit)
    Case "k"
      distance = distance * 1.609344
    Case "n"
      distance = distance * 0.8684
  End Select
End Function 


'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function get the arccos function using arctan function   :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function acos(rad)
  If abs(rad) <> 1 Then
    acos = pi/2 - atn(rad / sqr(1 - rad * rad))
  ElseIf rad = -1 Then
    acos = pi
  End If
End function


'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function converts decimal degrees to radians             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function deg2rad(deg)
	deg2rad = cdbl(deg * pi / 180)
End Function

'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function converts radians to decimal degrees             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function rad2deg(rad)
	rad2deg = cdbl(rad * 180 / pi)
End Function

'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  END OF DISTANCE CALCULATION                                   :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Function isreq_null(str)
	If isnull(str) Or str = "" Then
		str = Null
	End If
	isreq_null = str
End Function

Dim PCodeLatitude
Dim PCodeLongitude
    PCodeLatitude = Request("hiddenLat")
    PCodeLongitude = Request("hiddenLng")

Call OpenDB()

Dim page
Dim maxrows
    ' ALTER THIS TO GET THE PAGING TO ALTER THE PREV AND NEXT ACCORDING TO THE NUMBER OF ROWS REQUIRED TO BE RETURNED
    maxrows = 10
Dim cmd
Dim param


Call Debug()

If request("hid_isForm") <> "" Then
	page = 0
Else
	page = CInt(request("page"))
End if

    OutcodeCount = 1
    GetDistances = False

If Request("txtPostcode") <> "" Then
'rw "IF1"
    GetDistances = True
End If

If OutcodeCount > 0 Then
'rw "IF2" & "<br/>"
If Request("hid_isForm") <> "" Then
'rw "IF3" & "<br/>"
	'We put ALL the results of the search in a temporary table so we are not
	'using up credits on the postcode search for everypage
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
        .CommandType=adcmdstoredproc
        .CommandText = "STP_SEARCH_TMPRESULT"
        Set .ActiveConnection=conn
        Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
            .parameters.append param
        Set param = .createparameter("@ORGNAME", adVarChar, adParamInput, 250, isreq_null(request("orgname")) )
            .parameters.append param
        Set param = .createparameter("@ORGTYPE", adInteger, adParamInput, 0, isreq_null(request("selProviderType")) )
            .parameters.append param
        Set param = .createparameter("@SERVICETYPE", adInteger, adParamInput, 0, isreq_null(request("selServiceType")))
            .parameters.append param
        Set param = .createparameter("@SERVICES", adVarChar, adParamInput, 250, isreq_null(request("chkServiceList")))
            .parameters.append param
        Set param = .createparameter("@ELIGIBLE", adVarChar, adParamInput, 250, isreq_null(request("chkTargetList")))
            .parameters.append param
        Set param = .createparameter("@AREA", adVarChar, adParamInput, 250, isreq_null(request("chkAreaList")))
            .parameters.append param
        Set param = .createparameter("@LANGUAGE", adInteger, adParamInput, 0, isreq_null(request("selLang")))
            .parameters.append param
        Set param = .createparameter("@SPECIALNEEDS", adInteger, adParamInput, 0, isreq_null(request("selAccess")))
            .parameters.append param
        Set param = .createparameter("@SESSIONID", adVarChar, adParamInputOutput, 250, "")
            .parameters.append param
            .execute ,,adexecutenorecords

'rw  "STP_SEARCH_TMPRESULT" 
'    & isreq_null(request("orgname"))            & "," 
'    & isreq_null(request("selProviderType"))    & "," 
'    & isreq_null(request("selServiceType"))     & "," 
'    & isreq_null(request("chkServiceList"))     & "," 
'    & isreq_null(request("chkTargetList"))      & "," 
'    & isreq_null(request("chkAreaList"))        & "," 
'    & isreq_null(request("selLang")))           & "," 
'    & isreq_null(request("selAccess"))

	Session("searchid") = .Parameters("@SESSIONID").Value

	End With

	If NOT Request("txtPostcode") = "" Then
'rw "IF4" & "<br/>"

	'We assign a temporary ID into the session

	If GetDistances = True Then
'rw "IF5" & "<br/>"
			Dim rstStores
			Dim dblDistance

			Call openDB()
'rw "STP_GET_TMPSEARCH " & Session("searchid") & "<br/>"
			'We grab the sites returned in the search from the temp table
			set rstStores= server.CreateObject("adodb.recordset")
			Set cmd=server.CreateObject("ADODB.Command")
			    rstStores.ActiveConnection = conn
			    rstStores.locktype=adlockoptimistic
			    rstStores.cursortype = adopenstatic
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "STP_GET_TMPSEARCH"
			  set .ActiveConnection=conn
				  	Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
  					.parameters.append param
  					Set param = .createparameter("@TMPSEACHID", adVarChar, adParamInput, 250, Session("searchid") )
  					.parameters.append param
			end with
			rstStores.Open cmd

			'We loop through them and assign them a distance from the submitted postcode
			'Work out the distances and store them in the recordset
			while not rstStores.EOF
'				'Work out the distance between the customer and the store
'				If dblCustomerEasting = "" Then
'				    dblDistance = "0"
'				Else
'				    dblDistance = Distance(dblCustomerEasting, rstStores.Fields("easting"), dblCustomerNorthing, rstStores.Fields("northing"))
'				    'Convert the distance in metres to miles
'				    dblDistance = dblDistance / 1609
'				    'Save the distance
'				End If
'				    rstStores.Fields("distance") = dblDistance
                    rstStores.Fields("distance") = distance(PCodeLatitude, PCodeLongitude, rstStores.Fields("lat"), rstStores.Fields("lon"), "m")
				    rstStores.Update
				rstStores.MoveNext
			wend

			'Apply the filter and order the response
			'rstStores.Filter = "distance <= 10"  'Use a filter like this to limit the radius of the search
			'rstStores.Sort = "distance"


		'In order for the paging to work we do this function to assign a rowresult id in the order
		'of distance - perhaps a temporary table would have been simpler - but it works and is a little more efficient
		'You do this once rather than create a temp table for every page

		'''''''''''''''''''''''''''
		end if
		'''''''''''''''''''''''''''
'rw "STP_SEARCH_REORDER_BY_DISTANCE" & Session("searchid") & "<br/>"
	'Do this --------------------------------
		Set cmd=server.CreateObject("ADODB.Command")
		With cmd
		  .CommandType=adcmdstoredproc
		  .CommandText = "STP_SEARCH_REORDER_BY_DISTANCE"
		  set .ActiveConnection=conn
		  Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  .parameters.append param
		  Set param = .createparameter("@TMPID", adVarChar, adParamInput, 250, Session("searchid"))
		  .parameters.append param
		  .execute ,,adexecutenorecords
		end with

	end if


end if

If IsNull(Session("searchid")) Or Session("searchid") = "" Then
'rw "IF6" & "<br/>"
Else
'rw "IF7" & "<br/>"
    'Get the page of the result set
    'Response.Write "Actual Search ID: " & Session("searchid") & "<BR>"
    'Do this --------------------------------
    Set rsPage = server.CreateObject("adodb.recordset")
    Set cmd = server.CreateObject("ADODB.Command")
        With cmd
            .CommandType=adcmdstoredproc
            .CommandText = "STP_SEARCH_PROVIDER_PAGED_FHNY"
            Set .ActiveConnection=conn
            Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .parameters.append param
            Set param = .createparameter("@TMPSEARCHID", adVarChar, adParamInput, 250, Session("searchid"))
                .parameters.append param
            Set param = .createparameter("@STARTROWINDEX", adInteger, adParamInput, 0, page)
                .parameters.append param
            Set param = .createparameter("@MAXIMUMROWS", adInteger, adParamInput, 0, maxrows)
                .parameters.append param
            Set param = .createparameter("@TOTALROWS", adInteger, adParamInputOutput, 0, 0)
                .parameters.append param
            Set rsPage = .execute 
        End With

    strResult = "<table width=""100%"">" &_
    "<tr class=""search_header_row"">" &_
    "<td valign=""top"" class=""search_header_cell"">Click a provider for more details</td>" &_
    "<td class=""search_header_cell"">Distance</td>" &_
    "</tr>"

    If rsPage.EOF Then
	    strResult = strResult & "<tr class=""search_body_row""><td valign=""top"" class=""search_body_cell"" colspan=""3"">There were no results</td></tr>"
    Else
	    Do until rsPage.EOF
	        ProviderSummary = rsPage("ProviderSummary")
	        ProviderSummary = Replace(ProviderSummary,"    "," ")
	        ProviderSummary = Replace(ProviderSummary,"   "," ")
	        ProviderSummary = Replace(ProviderSummary,"  "," ")
	        ProviderSummary = Replace(ProviderSummary,"'","\")
	        ProviderSummary = Replace(ProviderSummary,"?","\")
	        ProviderSummary = Replace(ProviderSummary,"-"," ")
	        ProviderSummary = replace(ProviderSummary, chr(13), " ")
            ProviderSummary = replace(ProviderSummary, chr(10), " ")
            ProviderSummary = makesafe(ProviderSummary)

		    If NOT isnumeric(rsPage("Distance")) Then
			    strDistance = "N/A" 
		    Else
			    strDistance = FormatNumber(rsPage("Distance"),2) & "m"
		    End If

		    strProvider = ""
		    If trim(rsPage("ProviderName") ) = trim(rsPage("SiteName") ) Then
			    strProvider = rsPage("ProviderName")
		    Else
			    strProvider = rsPage("ProviderName") & ", " & rsPage("SiteName")
		    End If
		    strResult = strResult & "<tr class=""search_body_row"">" & _
		    "<td width=""100%""><a class=""genlink"" onmouseover=""Tip('"&ProviderSummary&"', TITLE, 'Organisation Summary', TITLEBGCOLOR, '#cc0000', BORDERCOLOR, '#cc0000', SHADOW, true, SHADOWWIDTH, 7)"" onmouseout=""UnTip()"" href=""/membersarea/contactdetails.asp?pg=FindHelpNearToYou&currentpage=" & page& "&providerid=" & rsPage("ProviderID") & "&siteid=" & rsPage("SiteID") &  """>" & strProvider & "</a></td>"  & _
		    "<td width=""50px"">" & strDistance & "</td>" & _
		    "</tr>" 
	    rsPage.MoveNext
	    Loop
	        rsPage.Close
	    set rsPage = Nothing
	    totalrows = cmd.Parameters("@TOTALROWS").Value
	    strResult = strResult & "<tr class=""search_footer_row""><td  valign=""top""  align=""right"" class=""search_footer_cell"" colspan=""3"">"
        If CInt(page) > CInt(0) Then
		    strResult = strResult & "<a target=""searchresult"" class=""genlink"" href=""/membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	    End If
	    If CInt(page) + CInt(maxrows) < CInt(totalrows) Then
		    strResult =strResult & "<a target=""searchresult"" class=""genlink""  href=""/membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	    End If
    End if
	    strResult = strResult & "</table>"

End If

    Call CloseDB()

end if

strResult = strResult
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Untitled Document</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" type="text/css" href="/css/Members_FrontEnd.css" />

    <script language="javascript" type="text/javascript">
        function searchresult()
        {
	        //var outcode = <%=OutcodeCount%>
	        //if (outcode > 0)
	        //{
		        parent.document.getElementById('searchresult').innerHTML = document.getElementById('searchresult').innerHTML;
	            parent.DisableBtn("BtnPCcodeSubmit")
	        //}
	        //else
	        //{
		    //    alert("You must enter a postcode which is a valid")
	        //}
        }
    </script>

</head>
<body onload="searchresult()">
    <div id="searchresult">
        <%=strResult%>
        <div style="float:right; color:black; font-style:italic">* All distances are as the crow flies and shown in miles</div><br />
    </div>
</body>
</html>
