<%
SITEID = request.cookies("siteid")

If NullNestTF(SITEID) = True Then
    Response.Redirect "/SessionTimeOut.asp"
End If

Call Opendb()

Set cmd=server.CreateObject("ADODB.Command")
With cmd
  .CommandType=adcmdstoredproc
  .CommandText = "STP_MD_HASSITEPHOTO"
  set .ActiveConnection=conn
  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
  .parameters.append param
  set param = .createparameter("@SITEID", adInteger, adParamInput, 0, SITEID)
  .parameters.append param
  set param = .createparameter("@HASPHOTO", adInteger, adParamInputOutput, 0,1)
  .parameters.append param
  .execute ,,adexecutenorecords
  hasphoto = .Parameters("@HASPHOTO").Value 
end with

Call CloseDB()

	If CInt(hasphoto) = 1 Then
		strphoto = "/MDAdmin/getsitephoto.aspx?siteid=" & SITEID 
	Else
		strPhoto = "/images/tmpsite.gif"
	End If
%>
<img alt="Site image" src="<%=strphoto%>" alt="" />