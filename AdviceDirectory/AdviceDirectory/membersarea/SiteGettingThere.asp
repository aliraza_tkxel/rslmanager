﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->

<%
	Dim PAGE_TITLE
    Dim TAB_MENU
	Dim LOGIN_TYPE
	Dim PROVIDERID

	    LOGIN_TYPE =request.cookies("LoginType")
	    PROVIDERID = request.cookies("OrgID")
	    siteid = request.cookies("siteid")

	If NullNestTF(siteid) = True Then
	    Response.Redirect "/SessionTimeOut.asp"
	End If

	Call OpenDB()

		Dim cmd, param

			Set cmd=server.CreateObject("ADODB.Command")
			Set rsContactDetails =server.CreateObject("ADODB.Recordset")
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "stp_MD_Select_Site_Details"
			  set .ActiveConnection=conn
			  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			  .parameters.append param
			  set param = .createparameter("@SiteID", adInteger, adParamInput, 0, siteid)
			  .parameters.append param
			  set rsContactDetails = .execute
			end with

		If NOT rsContactDetails.EOF Then
		    SiteName 		= rsContactDetails("SiteName")
			ParkingDetails 	= rsContactDetails("ParkingDetails")
			NearestBus 		= rsContactDetails("NearestBus")
			NearestTrain 	= rsContactDetails("NearestTrain")
			NearestTram 	= rsContactDetails("NearestTram")
			Postcode	 	= rsContactDetails("Postcode")
			Postcode = Replace(Postcode," ","%20")
		End If

	Call CloseDB()
%>
<%
	'Calls function that sets up the tab menu
	TAB_MENU = displaycontactsubmenu(1,LOGIN_TYPE)
	PAGE_TITLE = "Getting There"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="Link1" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--


//-->
</style>

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);

//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=SiteName%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; margin-right:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform">
                    <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck%></div>
	                
	                    <div class="container2">
                            <div class="left">
                                <div id="portal_submenu">
                                    <!-- #include virtual="/membersarea/includes/backtomain.asp" --><br />
                                    <!-- #include virtual="/membersarea/includes/sitephoto.asp" --><br />
                                    <%=TAB_MENU%>
                                </div>
                            </div>
                            <div class="right">
                                <div id="portal_content">                                    
                                    <div class="membersheader">Getting There</div>
                                    <div class="membersbody">
                                        <span class="lblDetails">Parking Details:</span><%=ParkingDetails%>
                                    </div>
                                    <div class="membersbody">
                                        <span class="lblDetails">Nearest Bus:</span><%=NearestBus%>
                                    </div>
                                    <div class="membersbody">
                                        <span class="lblDetails">Nearest Train:</span><%=NearestTrain%><br />
                                    </div>
                                    <div class="membersbody">
                                       <span class="lblDetails">Nearest Tram:</span><%=NearestTram %><br />
                                    </div>
                                    <div class="membersbody">
                                        <span class="lblDetails">Directions:</span><a target="_blank" href="http://www.multimap.com/map/browse.cgi?client=public&search_result=&db=pc&keepicon=true&lang=&pc=<%=PostCode%>&advanced=&client=public&addr2=&quicksearch=l2%205ru&addr3=&addr1=">View map</a> and directions using multimap<br />
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>