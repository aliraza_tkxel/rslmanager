<%@LANGUAGE="VBSCRIPT" %>

<%
    Response.Buffer = false
    Response.Expires=-1000
    Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  START OF DISTANCE CALCULATION                                 :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

'http://www.zipcodeworld.com/samples/distance.asp.html
'http://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Validation

const pi = 3.14159265358979323846

Function distance(lat1, lon1, lat2, lon2, unit)
  Dim theta, dist
  theta = lon1 - lon2
  dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
  'response.write "dist = " & dist & "<br>"
  dist = acos(dist)
  dist = rad2deg(dist)
  'response.write "dist = " & dist & "<br>"
  distance = dist * 60 * 1.1515
  Select Case lcase(unit)
    Case "k"
      distance = distance * 1.609344
    Case "n"
      distance = distance * 0.8684
  End Select
End Function 


'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function get the arccos function using arctan function   :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function acos(rad)
  If abs(rad) <> 1 Then
    acos = pi/2 - atn(rad / sqr(1 - rad * rad))
  ElseIf rad = -1 Then
    acos = pi
  End If
End function


'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function converts decimal degrees to radians             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function deg2rad(deg)
	deg2rad = cdbl(deg * pi / 180)
End Function

'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  this function converts radians to decimal degrees             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function rad2deg(rad)
	rad2deg = cdbl(rad * 180 / pi)
End Function

'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  END OF DISTANCE CALCULATION                                   :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Function isreq_null(str)
	If isnull(str) Or str = "" Then
		str = Null
	End If
	isreq_null = str
End Function

Dim PCodeLatitude
Dim PCodeLongitude
    PCodeLatitude = Request("hiddenLat")
    PCodeLongitude = Request("hiddenLng")

Dim page
Dim maxrows
    ' ALTER THIS TO GET THE PAGING TO ALTER THE PREV AND NEXT ACCORDING TO THE NUMBER OF ROWS REQUIRED TO BE RETURNED
    maxrows = 3
Dim cmd
Dim param

Call OpenDB()

If request("hid_isForm") <> "" Then
	page = 0
Else
	page = CInt(request("page"))
End if

'    OutcodeCount = 1
'    GetDistances = False

'If Request("txt_POSTCODE") <> "" AND Request("hid_IsNewSearch") = 1 Then
'    GetDistances = True
'End If

'If OutcodeCount > 0 Then

If Request("hid_IsNewSearch") = "1" Then
	'We put ALL the results of the search in a temporary table so we are not
	'using up credits on the postcode search for everypage
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
        .CommandType=adcmdstoredproc
        .CommandText = "STP_SEARCH_TMPRESULT"
        Set .ActiveConnection=conn
        Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
            .parameters.append param
        Set param = .createparameter("@ORGNAME", adVarChar, adParamInput, 250, isreq_null(request("orgname")) )
            .parameters.append param
        Set param = .createparameter("@ORGTYPE", adInteger, adParamInput, 0, isreq_null(request("selProviderType")) )
            .parameters.append param
        Set param = .createparameter("@SERVICETYPE", adInteger, adParamInput, 0, isreq_null(request("selServiceType")))
            .parameters.append param
        Set param = .createparameter("@SERVICES", adVarChar, adParamInput, 250, isreq_null(request("chkServiceList")))
            .parameters.append param
        Set param = .createparameter("@ELIGIBLE", adVarChar, adParamInput, 250, isreq_null(request("chkTargetList")))
            .parameters.append param
        Set param = .createparameter("@AREA", adVarChar, adParamInput, 250, isreq_null(request("chkAreaList")))
            .parameters.append param
        Set param = .createparameter("@LANGUAGE", adInteger, adParamInput, 0, isreq_null(request("selLang")))
            .parameters.append param
        Set param = .createparameter("@SPECIALNEEDS", adInteger, adParamInput, 0, isreq_null(request("selAccess")))
            .parameters.append param
        Set param = .createparameter("@SESSIONID", adVarChar, adParamInputOutput, 250, "")
            .parameters.append param
            .execute ,,adexecutenorecords

	Session("searchid") = .Parameters("@SESSIONID").Value
	
	'rw Session("searchid") & "<br/>"

	End With

	If NOT Request("txt_POSTCODE") = "" Then

	'''''''''This is Postcode Anywhere code '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	'We assign a temporary ID into the session

'	If GetDistances = True Then
			Dim CONNECTION_STRING 
			    CONNECTION_STRING = DSN_CONNECTION_STRING
			Dim rstStores
			Dim dblDistance
					
			Call openDB()
			
			'We grab the sites returned in the search from the temp table
			set rstStores= server.CreateObject("adodb.recordset")
			Set cmd=server.CreateObject("ADODB.Command")
			    rstStores.ActiveConnection = conn			
			    rstStores.locktype=adlockoptimistic
			    rstStores.cursortype = adopenstatic		
			With cmd
			  .CommandType=adcmdstoredproc
			  .CommandText = "STP_GET_TMPSEARCH"
			  set .ActiveConnection=conn				
				  	Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
  					.parameters.append param
  					Set param = .createparameter("@TMPSEACHID", adVarChar, adParamInput, 250, Session("searchid") )
  					.parameters.append param				
			end with
			rstStores.Open cmd

			'We loop through them and assign them a distance from the submitted postcode	
			'Work out the distances and store them in the recordset
			while not rstStores.EOF
                    rstStores.Fields("distance") = distance(PCodeLatitude, PCodeLongitude, rstStores.Fields("lat"), rstStores.Fields("lon"), "m")
				    rstStores.Update
				rstStores.MoveNext
			wend

			'Apply the filter and order the response
			'rstStores.Filter = "distance <= 10"  'Use a filter like this to limit the radius of the search
			'rstStores.Sort = "distance"
		
		
		'In order for the paging to work we do this function to assign a rowresult id in the order
		'of distance - perhaps a temporary table would have been simpler - but it works and is a little more efficient
		'You do this once rather than create a temp table for every page
		
		'''''''''''''''''''''''''''
'		end if
		'''''''''''''''''''''''''''
		
	'Do this --------------------------------
		Set cmd=server.CreateObject("ADODB.Command")
		With cmd
		  .CommandType=adcmdstoredproc
		  .CommandText = "STP_SEARCH_REORDER_BY_DISTANCE"
		  set .ActiveConnection=conn
		  Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  .parameters.append param
		  Set param = .createparameter("@TMPID", adVarChar, adParamInput, 250, Session("searchid"))
		  .parameters.append param
		  .execute ,,adexecutenorecords
		end with

	end if
	
	
'end if

If IsNull(Session("searchid")) Or Session("searchid") = "" Then
    '-- DO WHAT CHRIS HU ?
    rw "DO WHAT CHRIS HU ?"
Else
    '-- PROC AND SEARCH
    'rw "PROC AND SEARCH"
    Dim My_String2
    Dim My_Array2
 
       My_String2=Request("Priority1")
    If My_String2 <> "" Then
        My_Array2=split(My_String2,"-")  
        Call CreateSessionVariables (My_Array2(1), My_Array2(0))
        Call BuildSearchresult(My_Array2(1), "Priority 1", 1, "block")
    End If

       My_String2=Request("Priority2")
    If My_String2 <> "" Then
        My_Array2=split(My_String2,"-")  
        Call CreateSessionVariables (My_Array2(1), My_Array2(0))
        Call BuildSearchresult(My_Array2(1), "Priority 2", 2, "none")  
    End If

       My_String2=Request("Priority3")
    If My_String2 <> "" Then
        My_Array2=split(My_String2,"-")  
        Call CreateSessionVariables (My_Array2(1), My_Array2(0))
        Call BuildSearchresult(My_Array2(1), "Priority 3", 3, "none")
    End If

End If

    Call CloseDB()
    
    else
    
   ' NO NEW SESSION. OLD ONE STILL ACTIVE.

 
       My_String2=Request("Priority1")
    If My_String2 <> "" Then
        My_Array2=split(My_String2,"-")  
        Call CreateSessionVariables (My_Array2(1), My_Array2(0))
        Call BuildSearchresult(My_Array2(1), "Priority 1", 1, "block")
    End If

       My_String2=Request("Priority2")
    If My_String2 <> "" Then
        My_Array2=split(My_String2,"-")  
        Call CreateSessionVariables (My_Array2(1), My_Array2(0))
        Call BuildSearchresult(My_Array2(1), "Priority 2", 2, "none")  
    End If

       My_String2=Request("Priority3")
    If My_String2 <> "" Then
        My_Array2=split(My_String2,"-")  
        Call CreateSessionVariables (My_Array2(1), My_Array2(0))
        Call BuildSearchresult(My_Array2(1), "Priority 3", 3, "none")
    End If
   

end if
%>
<%
 Function CreateSessionVariables(Selection, Priority)

'rw "SELECTION:" & Selection & "<br/>"
'rw "PRIORITY:" & Priority & "<br/>"

    Select Case Priority
        Case 1
            Session("svPriortity1") = Selection
        Case 2
            Session("svPriortity2") = Selection
        Case 3
            Session("svPriortity3") = Selection
    End Select

End Function


Function BuildSearchresult(item, strPriorityText, PriorityOrder, display)

Dim CID
    CID = Session("svCustomerID")
    
    If CID = "" Or IsNull(CID) then CID = "Null" End If

Dim inProviderID

Dim Loopit
    Loopit = 1

'RW "STP_SEARCH_PROVIDER_PAGED" & "<br/>"
'RW Session("searchid") & "<br/>"
'Call Debug()

    Set rsPage= server.CreateObject("adodb.recordset")
    Set cmd=server.CreateObject("ADODB.Command")
        With cmd
            .CommandType=adcmdstoredproc
            .CommandText = "STP_SEARCH_PROVIDER_PAGED"
            Set .ActiveConnection=conn
            Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .parameters.append param
            Set param = .createparameter("@TMPSEARCHID", adVarChar, adParamInput, 250, Session("searchid"))
                .parameters.append param
            Set param = .createparameter("@STARTROWINDEX", adInteger, adParamInput, 0, page)
                .parameters.append param
            Set param = .createparameter("@MAXIMUMROWS", adInteger, adParamInput, 0, maxrows)
                .parameters.append param
            Set param = .createparameter("@TOTALROWS", adInteger, adParamInputOutput, 0, 0)
                .parameters.append param
            Set param = .createparameter("@SELECTEDPRIORITY", adInteger, adParamInput, 0, item)
                .parameters.append param
            Set rsPage = .execute 
        End With

If rsPage.EOF Then

SQL = "SELECT DESCRIPTION FROM ASSESSMENT_OPTIONS WHERE ID = " & item
Call OpenRs(rs,SQL) 
If NOT rs.EOF then
    AO = rs(0)
End If
Call CloseRs(rs)


            strResult = strResult & "<br/><b>Priority " & Priority & " ("&AO&") " & "</b><a href=""JavaScript:Toggle('"&PriorityOrder&"')""><img src=""/images/minusImage.gif"" width=""20"" height=""20"" border=""0"" align=""absmiddle"" name=""IMG_"&PriorityOrder&""" /></a>" &_
                                "<div style=""display:"&display&";"" id=""Div_"&PriorityOrder&""">" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
                                "<thead>" & vbCrLf &_
                                    "<tr class="""">" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Distance</th>" & vbCrLf &_
                                        " <th>&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult = strResult & "<tr>" &_
	    "<td valign=""top"" colspan=""6"">There were no results</td>" &_
	"</tr>" 
Else


If PriorityOrder = 1 Then
    img = "minusImage"
Else
    img = "plusImage"
End If
        strResult = strResult & "<br/><b>Priority " & Priority & " ("& rsPage("BARRIER") &") " & "</b><a href=""JavaScript:Toggle('"&PriorityOrder&"')""><img src=""/images/"&img&".gif"" width=""20"" height=""20"" border=""0"" align=""absmiddle"" name=""IMG_"&PriorityOrder&""" /></a>" &_
                                "<div style=""display:"&display&";"" id=""Div_"&PriorityOrder&""">" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"" class=""goinghigh"">" & vbCrLf &_
                                "<thead>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Distance</th>" & vbCrLf &_
                                        " <th>&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"

	    Do until rsPage.EOF

	        ProviderSummary = rsPage("ProviderSummary")
	        ProviderSummary = Replace(ProviderSummary,"    "," ")
	        ProviderSummary = Replace(ProviderSummary,"   "," ")
	        ProviderSummary = Replace(ProviderSummary,"  "," ")
	        ProviderSummary = Replace(ProviderSummary,"'","\")
	        ProviderSummary = Replace(ProviderSummary,"�","\")
	        ProviderSummary = Replace(ProviderSummary,"-"," ")
	        ProviderSummary = replace(ProviderSummary, chr(13), " ")
            ProviderSummary = replace(ProviderSummary, chr(10), " ")            
            ProviderSummary = makesafe(ProviderSummary)

		    If not isnumeric(rsPage("Distance")) Then
			    strDistance = "N/A" 
		    Else
			    strDistance = FormatNumber(rsPage("Distance"),2) & "m"
		    End If

		    strProvider = ""
		    If trim(rsPage("ProviderName") ) = trim(rsPage("SiteName") ) Then
			    strProvider = rsPage("ProviderName")
		    Else
			    strProvider = rsPage("ProviderName") & ", " & rsPage("SiteName")
		    End If

		    inProviderID = rsPage("ProviderID")

            ADDRESSLINE1 = rsPage("ADDRESSLINE1")
            EMAIL = rsPage("EMAIL")
		    If Isnull(EMAIL) or EMAIL = "" Then EMAIL = NULL Else EMAIL = "<a href=""mailto:"&EMAIL&""" title=""email:"&strProvider&"""/>"&EMAIL&"</a>" End If
		    TELEPHONE = rsPage("TELEPHONE")

		    strResult = strResult & "<tr>" & vbCrLf &_
		    "<td><a onmouseover=""Tip('"&ProviderSummary&"', TITLE, 'Organisation Summary', TITLEBGCOLOR, '#cc0000', BORDERCOLOR, '#cc0000', SHADOW, true, SHADOWWIDTH, 7)"" onmouseout=""UnTip()"" href=""/membersarea/contactdetails.asp?UT=0&pg=SelfAssessment&currentpage=0&providerid="&inProviderID&"&siteid="&rsPage("SITEID")&""">" & strProvider & "</a></td>" & vbCrLf &_
		    "<td>" & ADDRESSLINE1 & "</td>" & vbCrLf &_
		    "<td>" & TELEPHONE & "</td>" & vbCrLf &_
		    "<td>" & EMAIL & "</td>" & vbCrLf &_
		    "<td>" & strDistance & "<input type=""checkbox"" name=""CheckboxSite"" value=""" & rsPage("SITEID") & "_" & item & "-" & PriorityOrder & """ style=""visibility:hidden"" / ></td>" & vbCrLf &_
		    "<td class=""whiteout"">" & vbCrLf &_
		    "<input class=""submit"" type=""button"" name=""Remove"" id=""Remove"" value="" Remove "" title=""Button : Remove Our Recommendation"" style=""width:auto"" onclick=""RemoveSite("&rsPage("SITEID")&")""/>" & vbCrLf &_    
		    "</td>" & vbCrLf &_
		    "</tr>"
		    Loopit = Loopit + 1
'-- SITE , PRIORITY ITEM E.G. HOUSING, PRIORITY ORDER (SPECIFIED BY USER)
	        rsPage.MoveNext
	    Loop

	    rsPage.Close
	set rsPage = Nothing

	totalrows = cmd.Parameters("@TOTALROWS").Value	
	
	strResult = strResult & "</tbody>" & vbCrLf &_
	"<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""6"">"

    'if cint(page) > cint(0) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	'end if	
	'if cint(page) + cint(maxrows) < cint(totalrows) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	'end if

End if

    strResult = strResult & "</tfoot>" & vbCrLf &_
	                        "</table></div>"
	RW strResult & "<br/>"
    
    'Next
    
    strResult = ""

'end if

'Call CloseDB()

End Function
 
%>
<%
Session("svP1") = ""
Session("svP2") = ""
Session("svP3") = ""
Session("svP1") = Request("Priority1")
Session("svP2") = Request("Priority2")
Session("svP3") = Request("Priority3")
Session("Chk")  = Request("checkbox")
Session("pcode_result") = Request("txt_POSTCODE")
Session("PriorityList") = Request("PriorityList")
%>
<%=strResult%>
<div class="row" style="padding-top:1em;">
    <span class="label"> 
        <input class="submit" type="button" name="Back_2" id="Back_2" value=" Back " title="Submit Button : Back" style="width:auto" onclick="LoadStep(2,'errordiv')"/>
    </span>
    <span class="formw">
        <input class="submit" type="button" name="Save" id="Save" value=" Save " title="Submit Button : Next" style="width:auto; display:none" onclick="SaveRecomendations()"/>
    </span>
</div>
<input type="hidden" name="checkbox" id="checkbox" value="<%=Request("checkbox")%>" />
<input type="hidden" name="Priority1" id="Priority1" value="<%=Session("svP1")%>" />
<input type="hidden" name="Priority2" id="Priority2" value="<%=Session("svP2")%>" />
<input type="hidden" name="Priority3" id="Priority3" value="<%=Session("svP3")%>" />
<input type="hidden" name="txt_POSTCODE" id="txt_POSTCODE" value="<%=Session("pcode")%>" />
<input type="hidden" name="Stage" id="Stage" value="3" style="width:auto" />
<%'=Session("pcode_result")%>
<div style="float:right; clear:left; color:black; font-style:italic">* All distances are as the crow flies and shown in miles</div>