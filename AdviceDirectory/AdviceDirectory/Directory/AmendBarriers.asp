﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!-- #include virtual="/includes/ssl.asp" -->
<% 
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/functions/MembersLoginCheck.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Function DebugRquests()
        If Request.Form <> "" Then
            For Each Item In Request.Form
                rw "" & Item & " : " & Request(Item) & "<br/>" & vbcrlf
            Next
        End If
        If Request.QueryString <> "" Then
            For Each Item In Request.QueryString
                rw "" & Item & " : " & Request(Item) & "<br/>" & vbcrlf
            Next
        End If
    End Function

    Dim PAGE_TITLE
    Dim TAB_MENU
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS
	Dim LOGIN_TYPE
	Dim PROVIDERID

	LOGIN_TYPE = session("LoginType")
	PROVIDERID = Request("sel_PROVIDER")

	If NullNestTF(PROVIDERID) = True Then
	    PROVIDERID = -1
	Else
	    PROVIDERID = PROVIDERID
	End If

	'Call DebugRquests()
	'--- RULES
	'-- IF USERTYPE = ADMINISTRATOR THEN CAN ACCESS ALL ORGANISATIONS AND AMEND BARRIERS FOR THEM
	'-- ELSE LIMIT THE RESULTS TO THOSE BELONGING TO A SINGLE ORGANISATION
	
	If LOGIN_TYPE = 2 Then
	    'BARRIER_PROVIDERS = null
	    'BARRIER_TEXT = "Please Select"
	Else
	    'BARRIER_PROVIDERS = " WHERE PROVIDERID = " & Session("OrgID")
	    'BARRIER_TEXT = null
	    'PROVIDERID = Session("OrgID")
	End If

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = true
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

    Call OpenDB()
	Dim LOGINID
        LOGINID = nulltest(Session("UserID"))

        strSelect_Text = "Please Select"

	Set rsProvider = server.CreateObject("ADODB.Recordset")
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "CRMS_ORGANISATIONS"
      set .ActiveConnection=conn
      set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
      .parameters.append param
      Set rsProvider = .execute 
    end with

    Call BuildSelect_SP(STR_PROVIDER,"sel_PROVIDER",rsProvider,strSelect_Text,ProviderID,"width:auto",null,"tabindex='1' onchange='MySelectSubmit()'")
    Call CloseDB()

	'Call OpenDB()
	'Call BuildSelect(STR_PROVIDER,"sel_PROVIDER","TBL_MD_PROVIDER" & BARRIER_PROVIDERS,"ProviderID,ProviderName","ProviderName",BARRIER_TEXT,PROVIDERID,"width:242px","EntryFields","tabindex='6' onchange='MySelectSubmit()' ")
	'Call CloseDB()

	'Calls function that sets up the tab menu
	If (LOGIN_TYPE = 1) Then
	    TAB_MENU = displayprovidermenu(1,LOGIN_TYPE)
	ElseIf ((LOGIN_TYPE = 3) Or (LOGIN_TYPE = 4)) Then
	    TAB_MENU = displayprovidermenu(2,LOGIN_TYPE)
	Else
	    TAB_MENU = displayprovidermenu(3,LOGIN_TYPE)
	End if
%>
<%
If Request.Form("Save") <> "" Then

    Dim passcheck 		' USER OK/NOT OK TO LOGIN
    Dim passcheck_text
        passcheck = ""

    Call SaveBarrier(PROVIDERID)

    If passcheck = 0 Then
        ErrorClass = " class=""er"""
    Else
        ErrorClass = " class=""no_error"""
    End If

End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/includes/ClientScripting/js/FormValidation_XHTML.js"></script>
<script type="text/javascript" language="javascript" src="/includes/ClientScripting/js/General_XHTML.js"></script>

<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);

function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}

function MySelectSubmit()
{
document.myform.submit();
}

//--><!]]>
</script>
<style type="text/css">
#M_Container2
{
	margin-left:10px;
	margin-right:10px;
	border:0px solid pink;
}
</style>
</head>
<body lang="en">
<form name="myform" action="AmendBarriers.asp" method="post">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container" style="border: solid 0px green">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->		
  	</div>
  	<div id="M_Container2">
  	    <!-- Start Left -->
	    <div id="NavigationLft">
		    <!-- Start Navigation -->
 		    <div id="primary-nav">
			    <% Call listCat(10, "navmenu") %>
		    </div>
		    <!-- End Navigation -->
	    </div>
	    <!-- End Left -->
	    <div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<!-- "maincontent" margin-left:10px; margin-right:10px; -->
		<div id="maincontent" style="background-attachment: fixed; background: white url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<!-- style="margin-left:-10px;" -->
			<div><%=PAGE_CONTENT%></div>
			<br />
			    <div id="nsform">
                    <div class="container2">
                        <div class="left">
                            <div id="portal_submenu">
                                <%=TAB_MENU%>
                            </div>
                        </div>
                        <div class="right" style="border:solid 0px blue">
                            <div id="portal_content" style="border:solid 0px red">
                                <div id="errordiv" <%=ErrorClass%>><%=passcheck_text%></div>
                                <div class="membersheader">Barriers</div>
                                    <div style="float:left; background-color:#003366; padding-left:1em; width:100%">
                                    <span style="float:left; margin:5px; color:White">Organisation</span>
                                    <span style="float:left; margin:5px;"><%=STR_PROVIDER%></span>
                                    </div>
                                <div style="float:left; width: 100%" class="membersbody">
                                <%
                                set rsBooks = Server.CreateObject("ADODB.Recordset")
                                    rsBooks.ActiveConnection = DSN_CONNECTION_STRING
                                    rsBooks.Source = "SELECT ASSESSMENT_OPTIONS.ID, ASSESSMENT_OPTIONS.[DESCRIPTION], TBL_MD_PROVIDER_ASSESSMENTOPTION.ID AS selected FROM TBL_MD_PROVIDER_ASSESSMENTOPTION RIGHT OUTER JOIN ASSESSMENT_OPTIONS ON ASSESSMENT_OPTIONS.ID = TBL_MD_PROVIDER_ASSESSMENTOPTION.ID AND TBL_MD_PROVIDER_ASSESSMENTOPTION.PROVIDERID = " & PROVIDERID & " WHERE ACTIVE = 1"
                                    rsBooks.CursorType = 0
                                    rsBooks.CursorLocation = 2
                                    rsBooks.LockType = 3
                                    rsBooks.Open()
                                %>
                                <!-- <table border="1" cellpadding="0" cellspacing="0">//-->
                                <%
                                Dim numberColumns
                                Dim startrw
                                Dim endrw
                                Dim numrows
                                    startrw = 0
                                    endrw = Repeat1__index
                                    numberColumns = 2
                                    'numrows = 2
                                    'while((numrows <> 0) AND (Not rsBooks.EOF))
                                while Not rsBooks.EOF
	                                startrw = endrw + 1
	                                endrw = endrw + numberColumns
                                %>
                                <!-- <tr align="center" valign="top"> //-->
                                <div style="float:left; border: solid 0px green; width:100%; margin-bottom:1em">
                                <%
                                While ((startrw <= endrw) AND (Not rsBooks.EOF))
                                %>
		                                 <!-- <td> //--> 
                                <div style="float:left; border: solid 0px green; width:40%; margin-left:1em">
                                    <span class="formw" style="white-space:nowrap; float:left; border: solid 0px red"> <label for="rdo_ASSESSMENT_<%=rsBooks("ID")%>"><%=rsBooks("DESCRIPTION")%>:</label> </span>
                                    <span class="label" style="float:right; border: solid 0px green">
                                        <input 
                                            style="width:auto"
                                            type="checkbox" 
                                            name="checkbox" 
                                            <%If rsBooks("selected") <> "" Then Response.Write "Checked=""Checked""" End If %>
                                            value="<%=rsBooks("ID")%>" />
                                    </span>
                                </div>
		                                  <!--</td>//-->
		                                  <%
	                                startrw = startrw + 1
	                                rsBooks.MoveNext()
	                                Wend
	                                %>
		                                <!--</tr>//-->
		                                </div>
		                                <%
                                 numrows=numrows-1
                                 Wend
                                 %>
	                      <!-- </table> //-->
	                      <br />
	                      <div style="float:left; clear:both">
	                      <span class="formw" style="float:left">&nbsp;</span>
	                      <span class="label" style="float:left">
	                        <input style="cursor:pointer" type="button" value="Select All" onclick="checkAll(document.myform.checkbox)" />
	                        <input style="cursor:pointer" type="button" value="Deselect All" onclick="uncheckAll(document.myform.checkbox)" />
	                        <input style="cursor:pointer" id="Save" name="Save" type="submit" value="Save" />
	                      </span>
	                      </div>
	                      <% If rsBooks.EOF And rsBooks.BOF Then %>
                            Nothing is avaiable for selection.
	                      <% End If ' end rsBooks.EOF And rsBooks.BOF %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	 	</div>
	 	<!-- End Content -->
	</div>
  	</div>
	<!-- Start Content Container -->
	
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</form>
<iframe name="iFormFrame" id="iFormFrame" style="display:none"></iframe>
</body>
</html>