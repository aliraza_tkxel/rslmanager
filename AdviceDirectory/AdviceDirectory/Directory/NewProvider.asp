﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/functions/MembersLoginCheck.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim TAB_MENU
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS
	Dim LOGIN_TYPE
	Dim PROVIDERID

	LOGIN_TYPE = session("LoginType")
	PROVIDERID = session("OrgID")

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = true
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	'Calls function that sets up the tab menu
	TAB_MENU = displayprovidermenu(0,LOGIN_TYPE)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/includes/ClientScripting/js/FormValidation_XHTML.js"></script>
<script type="text/javascript" src="/includes/ClientScripting/js/General_XHTML.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; margin-right:10px; background-attachment: fixed; background: white url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			    <div id="nsform">
			    <div class="container2">
                    <div class="left">
                        <div id="portal_submenu">
                            <%=TAB_MENU%>
                        </div>
                    </div>
                    <div class="right">
                        <div id="portal_content">
                            <div id="errordiv" <%=ErrorClass%>><%=passcheck_text%></div>
                            <div class="membersheader">Organisation</div>
                            <div class="membersbody">

                                <form method="post" id="Form1" action="svr/NewProvider_svr.asp" target="iFormFrame">
                                    <fieldset>
                                    <legend>Login to this site</legend>
                                                    <div class="row">
                                                        <span class="label"> 
                                                            <label for="txt_OrgName" accesskey="O" style="cursor:pointer">
                                                                <span style="text-decoration:underline">O</span>rganisation Name:
                                                            </label>
                                                        </span>
                                                        <span class="formw">
                                                            <input 
                                                                class="border-white" 
                                                                onblur="this.className='border-white'" 
                                                                onfocus="this.className='border-red'" 
                                                                type="text" 
                                                                size="40" 
                                                                name="txt_OrgName" 
                                                                id="txt_OrgName" 
                                                                maxlength="100" 
                                                                value="" 
                                                                tabindex="1" 
                                                                required="Y" 
                                                                validate="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/" validateMsg="Organisation Name" />
                                                            <img src="/js/FVS.gif" name="img_OrgName" id="img_OrgName" width="15px" height="15px" alt="" />
                                                        </span>
                                                   </div>
                                                   <div class="row">
                                                       <span class="label"> 
                                                            <label for="chk_OrgStatus" accesskey="A" style="cursor:pointer">
                                                                <span style="text-decoration:underline">A</span>ctive:
                                                            </label>
                                                        </span>
                                                        <span class="formw">
                                                            <input 
                                                                type="hidden" 
                                                                value="<%=LOGIN_TYPE%>" 
                                                                name="hid_Logintype" 
                                                                id="hid_Logintype" />
                                                            <input 
                                                                class="submit" 
                                                                type="checkbox" 
                                                                name="chk_OrgStatus" 
                                                                id="chk_OrgStatus" 
                                                                value="1"
                                                                checked="checked" 
                                                                title="Checkbox : Organisation Active" 
                                                                style="width:auto"/>
                                                        </span>
                                                   </div>
                                                   <div class="row">
                                                        <span class="label"> &nbsp; </span>
                                                        <span class="formw">
                                                            <input 
                                                                class="submit" 
                                                                type="submit" 
                                                                name="Save" 
                                                                id="Save" 
                                                                value=" Save " 
                                                                title="Submit Button : Save" 
                                                                style="width:auto"/>
                                                        </span>
                                                   </div>
                                    </fieldset>
                                    </form>

                            </div>
                        <br />
                        <div id="divOrgString"></div>
                        </div>
                    </div>
                </div>
	 	    </div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<iframe name="iFormFrame" id="iFormFrame" style="display:none"></iframe>
</body>
</html>