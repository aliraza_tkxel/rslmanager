﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/functions/MembersLoginCheck.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
	Dim LOGIN_TYPE
	Dim PROVIDERID

	LOGIN_TYPE = session("LoginType")
	PROVIDERID = session("OrgID")

	If request("Action") = 1 Then
		Action = 1
		MenID = 2
		if LOGIN_TYPE = 3 then MenID = 1 end if
	Else
		Action = 0
		MenID = 1
		if LOGIN_TYPE = 3 then MenID = 0 end if
	End If

	LoginID = request("LoginID")
	If LoginID = "" Then
		'LoginID = 0
		LOGINID = nulltest(Session("UserID"))      
	End If

	If LoginID = "" Then
		buttonclass = "hidediv"
	Else
		buttonclass = "shovdiv"
	End If

    Dim PAGE_TITLE
    Dim TAB_MENU
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = true
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))


	Call OpenDB()

	Dim encLib
	Set encLib = Server.CreateObject("EncLib.EncDec")

	If NOT LoginID = "" Then

	    Dim cmd, param

		    Set cmd=server.CreateObject("ADODB.Command")
		    Set rsLogin =server.CreateObject("ADODB.Recordset")
		    With cmd
		      .CommandType=adcmdstoredproc
		      .CommandText = "stp_SelectLoginsfromID"
		      Set .ActiveConnection=conn
		      Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		      .parameters.append param
		      Set param = .createparameter("@LoginID", adInteger, adParamInput, 0, LoginID)
		      .parameters.append param
		      Set rsLogin = .execute
		    End With

	    Loginstatus = "Checked"

	    If NOT rsLogin.EOF Then

		    Do until rsLogin.eof

		        Firstname 		= rsLogin("FirstName")
		        Lastname 		= rsLogin("Lastname")
		        Email 			= rsLogin("Email")
		        USERNAME		= rsLogin("USERNAME")
		        Password 		= encLib.decryptpwd(rsLogin("Password"))
		        LEVELOFACCESS 	= rsLogin("AccessID")
		        Loginstatus 	= rsLogin("LoginStatus")
		        ProviderID 		= rsLogin("ProviderID")

		        If Loginstatus  Then
			        Loginstatus = "Checked"
		        Else
			        Loginstatus = ""
		        End If

		        rsLogin.MOVENEXT
		    Loop

	    End If

    End If


    strSelect_Text = "Please Select"

	If LOGIN_TYPE = "2" Then
		'strSQL_Provider = "TBL_MD_PROVIDER"
		'strSelect_Text = "Please Select"
		strSQL_LoginType = "TBL_MD_LOGIN_ACCESS"
	Else
	    If LOGIN_TYPE = 4 then
	        strSQL_LoginType = "TBL_MD_LOGIN_ACCESS WHERE ACCESSID IN(1,3,4)"
	    End If
	    If LOGIN_TYPE = 3 then
	        strSQL_LoginType = "TBL_MD_LOGIN_ACCESS WHERE ACCESSID IN(1,3)"
	    End If
	    If LOGIN_TYPE = 1 then
	        strSQL_LoginType = "TBL_MD_LOGIN_ACCESS WHERE ACCESSID IN(1)"
	    End If
		'strSQL_Provider = "TBL_MD_PROVIDER WHERE PROVIDERID = " & PROVIDERID
		'strSQL_LoginType = "TBL_MD_LOGIN_ACCESS WHERE ACCESSID = 1"		
		'strSelect_Text = null
	End If


	'Dim LOGINID
    '    LOGINID = nulltest(Session("UserID"))

	Set rsProvider = server.CreateObject("ADODB.Recordset")
    Set cmd=server.CreateObject("ADODB.Command")
    With cmd
      .CommandType=adcmdstoredproc
      .CommandText = "CRMS_ORGANISATIONS"
      set .ActiveConnection=conn
      set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
      .parameters.append param
      Set rsProvider = .execute 
    end with

    Call OpenDB()
    Call BuildSelect_SP(STR_PROVIDER,"sel_PROVIDER",rsProvider,strSelect_Text,ProviderID,"width:auto",null,"tabindex='1'")
    'Call CloseDB()

	Dim STR_PROVIDER
	Dim STR_LEVELOFACCESS
	'Call BuildSelect(STR_PROVIDER,"sel_PROVIDER",strSQL_Provider,"ProviderID,ProviderName","ProviderName",strSelect_Text,ProviderID,Null,"EntryFields","tabindex='1'")
	Call BuildSelect(STR_LEVELOFACCESS,"sel_LEVELOFACCESS",strSQL_LoginType,"AccessID,AccessType","AccessType",strSelect_Text ,Levelofaccess,Null,"EntryFields","tabindex='7'")

	Call CloseDB()

	'Calls function that sets up the tab menu
	If (LOGIN_TYPE = 1) Then
	    TAB_MENU = displayprovidermenu(0,LOGIN_TYPE)
	Else
	    TAB_MENU = displayprovidermenu(MenID,LOGIN_TYPE)
	End if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/includes/ClientScripting/js/FormValidation_XHTML.js"></script>
<script type="text/javascript" src="/includes/ClientScripting/js/General_XHTML.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function Upload(Action)
{
	if (Action == 1)
	{
		document.Form1.action="svr/NewLogin_svr.asp?action=" + Action
		document.Form1.submit();
	}
	else if (Action == 2)
	{
		var answer = confirm("You have chosen to delete this log in account")
		if (answer)
		{
		    document.Form1.action="svr/NewLogin_svr.asp?action=" + Action
			document.Form1.submit();
		}
		else
		{
		    //do nothing
		}
	}
}	
//--><!]]>
</script>

<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; margin-right:10px; background-attachment: fixed; background: white url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			    <div id="nsform">
			    <div class="container2">
                    <div class="left">
                        <div id="portal_submenu">
                            <%=TAB_MENU%>
                        </div>
                    </div>
                    <div class="right">
                        <div id="portal_content">
                            <div id="errordiv" <%=ErrorClass%>><%=passcheck_text%></div>
                            <div class="membersheader">Login</div>
                            <div class="membersbody">

                                <form method="post" name="Form1" id="Form1" action="svr/MembersLogin_svr.asp" target="iFormFrame">
	                                <fieldset>
		                            <legend>Create a new login to the Members' Directory</legend>
                                    <div id="Frm" style="float:left; width:48%;">
                                        <div class="row">
					                        <span class="label">
					                            <label for="sel_PROVIDER" accesskey="O" style="cursor:pointer">
					                                <span style="text-decoration:underline">O</span>rganisation Name:
					                            </label>
					                        </span>
					                        <span class="formw">
					                            <%=STR_PROVIDER%>
					                        </span>
					                    </div>
					                    <div class="row">
                                            <span class="label"> 
                                                <label for="txt_FIRSTNAME" accesskey="F" style="cursor:pointer">
                                                    <span style="text-decoration:underline">F</span>irst Name:
                                                </label> 
                                                </span>
                                            <span class="formw">
                                                <input 
                                                    class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    type="text" 
                                                    size="40" 
                                                    name="txt_FIRSTNAME" 
                                                    id="txt_FIRSTNAME" 
                                                    maxlength="100" 
                                                    value="<%=FIRSTNAME%>" 
                                                    tabindex="2"
                                                    required="Y" 
                                                    validate="/([a-zA-Z0-9,\s]{4,500})$/" 
                                                    validateMsg="Firstname" />
                                                <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" alt="" />
                                            </span>
                                       </div>
					                    <div class="row">
                                            <span class="label"> 
                                                <label for="txt_SURNAME" accesskey="S" style="cursor:pointer">
                                                    <span style="text-decoration:underline">S</span>urname:
                                                </label>
                                            </span>
                                            <span class="formw">
                                                <input 
                                                    class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    type="text" 
                                                    size="40" 
                                                    name="txt_SURNAME" 
                                                    id="txt_SURNAME" 
                                                    maxlength="100" 
                                                    value="<%=LASTNAME%>" 
                                                    tabindex="3" 
                                                    required="Y" 
                                                    validate="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/" validateMsg="Surname" />
                                                <img src="/js/FVS.gif" name="img_SURNAME" id="img_SURNAME" width="15px" height="15px" alt="" />
                                            </span>
                                       </div>
					                    <div class="row">
                                            <span class="label"> 
                                                <label for="txt_EMAIL" accesskey="E" style="cursor:pointer">
                                                    <span style="text-decoration:underline">E</span>mail:
                                                </label>
                                            </span>
                                            <span class="formw">
                                                <input 
                                                    class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    type="text" 
                                                    size="40" 
                                                    name="txt_EMAIL" 
                                                    id="txt_EMAIL" 
                                                    maxlength="250" 
                                                    value="<%=EMAIL%>" 
                                                    tabindex="4" 
                                                    required="Y" 
                                                    validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" 
                                                    validateMsg="Email" />
                                                <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                            </span>
                                       </div>
                                        <div class="row">
                                            <span class="label" style="white-space:nowrap;">
                                                <label for="txt_USERNAME" accesskey="U">
                                                    <span style="text-decoration:underline; cursor:pointer">U</span>sername:
                                                </label>
                                            </span>
                                            <span class="formw">
                                                <input 
                                                    class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    size="40" type="text" 
                                                    name="txt_USERNAME" 
                                                    id="txt_USERNAME" 
                                                    maxlength="250" 
                                                    value="<%=USERNAME%>" required="Y" 
                                                    validate="/[a-zA-Z0-9_\@\.\-]{6,70}$/"
                                                    validateMsg="Username :- Must be 6 - 70 characters in length. Letters and Numbers are permitted. Spaces and most special characters are not permitted." />
                                                <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" />
                                            </span>
                                        </div>
					                    <div class="row">
                                            <span class="label"> 
                                                <label for="txt_PASSWORD" accesskey="P" style="cursor:pointer">
                                                    <span style="text-decoration:underline">P</span>assword:
                                                </label>
                                            </span>
                                            <span class="formw">
                                                <input 
                                                    class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
                                                    type="password" 
                                                    size="40" 
                                                    name="txt_PASSWORD" 
                                                    id="txt_PASSWORD" 
                                                    maxlength="10" 
                                                    value="<%=Password%>"
                                                    tabindex="5"  
                                                    required="Y" 
                                                    validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                                    validateMsg="Password - Password must be between 6 and 10 Characters and contain at least 1 number." />
                                                <img src="/js/FVS.gif" name="img_password" id="img_password" width="15px" height="15px" alt="" />
                                            </span>
                                        </div>
					                    <div class="row">
					                        <span class="label"> 
                                                <label for="txt_CONFIRMPASSWORD" accesskey="C" style="cursor:pointer">
                                                    <span style="text-decoration:underline">C</span>onfirm Password:
                                                </label>
                                            </span>
						                    <span class="formw">
						                        <input 
						                            class="border-white" 
                                                    onblur="this.className='border-white'" 
                                                    onfocus="this.className='border-red'" 
						                            type="password" 
						                            size="40" 
						                            name="txt_CONFIRMPASSWORD" 
						                            id="txt_CONFIRMPASSWORD"
						                            maxlength="10"
						                            value="<%=Password%>"
						                            tabindex="6"  
						                            required="Y" 
                                                    validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                                    validateMsg="Password - Password must be between 6 and 10 Characters and contain at least 1 number." />
						                        <img src="/js/FVS.gif" name="img_CONFIRMPASSWORD" id="img_CONFIRMPASSWORD" width="15px" height="15px" alt="" />
						                    </span>
					                    </div>
					                    <div class="row">
                                            <span class="label"> 
                                                <label for="sel_LEVELOFACCESS" accesskey="L" style="cursor:pointer">
                                                    <span style="text-decoration:underline">L</span>evel of Access:
                                                </label>
                                            </span>
                                            <span class="formw">
                                                <%=STR_LEVELOFACCESS%>
                                                <img src="/js/FVS.gif" name="img_LEVELOFACCESS" id="img_LEVELOFACCESS" width="15px" height="15px" alt="" />
                                            </span>
                                       </div>
					                    <div class="row">
					                        <span class="label">
					                            <label for="chk_LoginStatus" accesskey="A" style="cursor:pointer">
					                                <span style="text-decoration:underline">A</span>ctive:
					                            </label>
					                        </span>
					                        <span class="formw">
					                            <input 
					                                type="checkbox" 
					                                name="chk_LoginStatus" 
					                                id="chk_LoginStatus" 
					                                value="1" <%=Loginstatus%> 
					                                tabindex="8" 
					                                style="width: auto;" />
					                        </span>
					                    </div>
					                    <div class="row">
					                        <span class="label">
                					            &nbsp;
					                        </span>
					                        <span class="formw">
						                            <input 
						                                class="<%=buttonclass%>" 
						                                type="button" 
						                                name="Delete" 
						                                id="Delete" 
						                                value="Delete" 
						                                tabindex="9"
						                                style="margin-right:5px; text-align: center; width: auto; cursor: pointer;" onclick="Upload(2)"/>
						                            <input 
						                                class="submit" 
						                                type="button" 
						                                name="Save" 
						                                id="Save" 
						                                value="Save" 
						                                tabindex="10"
						                                style="width:auto" 
						                                onclick="Upload(1)" />
					                        </span>
					                    </div>
                                    <input type="hidden" name="hid_Action" id="hid_Action" value="<%=Action%>" />
			                        <input type="hidden" name="hid_LoginID" id="hid_LoginID" value="<%=LoginID%>" />
                                    </div>
		                        </fieldset>
                                </form>

                            </div>
                        <br />
                        <div id="divOrgString"></div>
                        </div>
                    </div>
                </div>
	 	    </div>
	 	<!-- End Content -->
	 	</div>
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<iframe name="iFormFrame" id="iFormFrame" style="display:none"></iframe>
</body>
</html>