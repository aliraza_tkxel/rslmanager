<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Encrypting password
Dim encLib
Set encLib = Server.createobject("EncLib.EncDec")

Dim strUName
	strUName = request("txt_username")
Dim strPwd
	strPwd = encLib.encryptpwd(request("txt_password"))
	
Dim Pass

Call OpenDB()
	Dim cmd
	Dim param
	Set cmd=server.CreateObject("ADODB.Command")
	Set rsLogin =server.CreateObject("ADODB.Recordset")
	With cmd
	  .CommandType=adcmdstoredproc
	  .CommandText = "stp_MD_Login"
	  set .ActiveConnection=conn
	  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
	  .parameters.append param
	  set param = .createparameter("@USERNAME", adVarChar, adParamInput, 100, strUName)
	  .parameters.append param
	  set rsLogin = .execute
	End With

	If NOT rsLogin.EOF Then
			Password = rsLogin("PASSWORD")
			'If password is good then go to Job Queue page
			If Password = strPwd Then
				Pass = "Yes"
				session("UserID") 		= rsLogin("LOGINID")
				session("LoginType")	= rsLogin("ACCESSID")
				session("OrgID")		= rsLogin("PROVIDERID")
				'Session("Name") 		= rsLogin("FIRSTNAME") + "&nbsp;" + rsLogin("SURNAME")
				If session("LoginType") = 1 Then
					GoToPage = "../AmendLogin.asp"
				Else
				    GoToPage = "../NewLogin.asp"
				End If
			Else
				Pass = "No"
			End If
		Else
			Pass = "No"
	End If
Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
</head>
<script language="javascript" type="text/javascript">
function LoginFail()
{
	if ("<%=Pass%>" == "No")
	{
		parent.document.getElementById('errordiv').innerHTML = "<div style='padding:10px;'><font color='red'>Your Login and/or Password do not match our records</font></div>"
		parent.document.getElementById("errordiv").className = "er"
		setTimeout("parent.errordiv.innerHTML = ''",  2000);
		setTimeout("parent.errordiv.className = ''",  2000);
	}
	else
	{
		parent.document.getElementById('errordiv').innerHTML = "<div style='padding:10px;'>Entering Members Area</div>"
		parent.document.getElementById("errordiv").className = ""
		parent.location.href = "<%=GoToPage%>"
	}
}
</script>
<body onload="LoginFail()">
</body>
</html>