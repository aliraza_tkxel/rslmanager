<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%

Call OpenDB()

Dim OrgName, OrgStatus
Redim arrform(2)
Dim strStatus

Function GetForm()
	OrgName 	= request("txt_OrgName")
	OrgStatus 	= request("chk_OrgStatus")
	If OrgStatus = "" Then
		OrgStatus = 0
	Else 
		OrgStatus = 1
	End If
End function

Function ValidateForm()
	'Sets an array of error messages to be displayed to the user if they dont enter certain fields
	i = 0
	if OrgName = "" then
		arrform(i) = "The organisation field cannot be empty."
		i = i + 1
	end if
	if len(OrgName) > 100 then
		arrform(i) = "Must be less."
		i = i + 1
	end if

	Dim cmd
	Dim param

	Set cmd=server.CreateObject("ADODB.Command")
	With cmd
	  .CommandType=adcmdstoredproc
	  .CommandText = "MD_CHECKORGEXISTS"
	  set .ActiveConnection=conn
	  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
	  .parameters.append param
	  set param = .createparameter("@ProviderName", adVarChar, adParamInput, 150, OrgName)
	  .parameters.append param
	  set param = .createparameter("@COUNT", adInteger, adParamInputOutput, 0, 0)
	  .parameters.append param
	  .execute ,,adexecutenorecords
	  OrgNameExists = .parameters("@COUNT")
	end with

	if OrgNameExists > 0 then
		arrform(i) = "You cannot enter an Organisation name that is already on the system"
	i = i + 1
	end if

	'if OrgStatus = 0 then
	'	arrform(i) = "You must tick the org status."
	'	i = i + 1
	'end if

	redim preserve arrform(i-1)
	ValidateForm = i
End function

Function Displayvalidation(arrform)
	'sets the array of messages into html bullet points
	str = "<ul style='margin-left:1em'>"
	    for i = 0 to ubound(arrform)
		    str = str & "<li>" & arrform(i) & "</li>"
	    next
	str = str & "</ul>"
	Displayvalidation = str
End function

'set variables function
Call GetForm()
validatecount = ValidateForm()
'if the form fields are valid then insert into database
If validatecount = 0 Then
	Dim cmd
	Dim param
	'Stored procedure to INSERT new provider details into TBL_PROVIDER table
	Set cmd=server.CreateObject("ADODB.Command")
	With cmd
	  .CommandType=adcmdstoredproc
	  .CommandText = "MD_PROVIDER_INSERT"
	  Set .ActiveConnection=conn
	  Set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
	  .parameters.append param
	  Set param = .createparameter("@ProviderName", adVarChar, adParamInput, 100, OrgName)
	  .parameters.append param
	  Set param = .createparameter("@OrgStatus", adInteger, adParamInput, 0, OrgStatus)
	  .parameters.append param
	  .execute ,,adexecutenorecords
	End With
	strStatus="The New Provider has been added to the system."
	countdirect = 1
Else
	'display and html formatted list error messages (fields that have not been filled)
	strStatus = Displayvalidation(arrform)
	countdirect = 0
End If

Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
</head>
<body onload="returnData()">
<script language="javascript" type="text/javascript">
function returnData()
{
    varstatus = "<%=strStatus%>"
    parent.document.getElementById('errordiv').innerHTML = varstatus
    parent.document.getElementById("errordiv").className = "er"

    if(<%=countdirect%> == 1)
	{
	    parent.window.location.href = "../NEWLOGIN.ASP" 
	}
}
</script>
</body>
</html>