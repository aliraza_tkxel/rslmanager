<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%

Call OpenDB()
		
	Dim ProviderID, Firstname, Lastname, Email, Username, Password, ConfirmPass, LevelofAccess, LoginStatus, Action, LoginID, SaveDel, MoveVariable 
	Redim arrform(6)
	Dim strStatus

	Function GetForm()
		ProviderID 		= request("sel_PROVIDER")
		Firstname 		= request("txt_FIRSTNAME")
		Lastname 		= request("txt_SURNAME")
		Email 			= request("txt_EMAIL")
		Username 		= request("txt_USERNAME")
		Password		= request("txt_PASSWORD")
		ConfirmPass		= request("txt_CONFIRMPASSWORD")
		LevelofAccess 	= request("sel_LEVELOFACCESS")
		LoginStatus 	= request("chk_LoginStatus")
		Action  		= request("hid_Action")
		LoginID 		= request("hid_LoginID")
		SaveDel 		= request("Action")

		If LoginStatus = "" Then
			LoginStatus = 0
		Else 
			LoginStatus = 1	
		End If

	End function
		
		Function ValidateForm()
			
			'Sets an array of error messages to be displayed to the user if they dont enter certain fields
			
			i = 0
		
			if Firstname = "" then
				arrform(i) = "The field First Name cannot be empty."
				i = i + 1
			end if
			
			if len(Firstname) > 100 then
				arrform(i) = "Must be less."
				i = i + 1
			end if	
			
			if Lastname = "" then
				arrform(i) = "The field Surname cannot be empty."
				i = i + 1
			end if
			
			if Email = "" then
				arrform(i) = "The field Email cannot be empty."
				i = i + 1
			end if
			
			if Email <> "" AND isValidEmail(Email) = False then
				arrform(i) = "Email - Email is not a valid email address."
				i = i + 1
			end if
			
			if Username = "" then
				arrform(i) = "The field Username cannot be empty."
				i = i + 1
			end if
			
			If Username <> "" AND isUsername(Username) = False Then
				arrform(i) = "Username :- Must be 6 - 70 characters in length. Letters and Numbers are permitted. Spaces and most special characters are not permitted."
				i = i + 1
			end if
			
			if Password = "" then
				arrform(i) = "The field Password cannot be empty."
				i = i + 1
			end if
			
			If Password <> "" AND isPassword(Password) = False Then
				arrform(i) = "Password - Password must be between 6 and 10 Characters and contain at least 1 number.."
				i = i + 1
			end if
				
			result = StrComp(Password, ConfirmPass)
			if not result = 0 then
				arrform(i) = "The field Confirm Password does not match the field Password"
				i = i + 1
			end if	 

			if LevelofAccess = "" then
				arrform(i) = "You must select a level of access"
			    i = i + 1			
			end if


			Dim cmd, param
			
			Set cmd=server.CreateObject("ADODB.Command")
			With cmd
			.CommandType=adcmdstoredproc
			.CommandText = "MD_CHECKLOGIN"
			set .ActiveConnection=conn
			set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
			.parameters.append param
			set param = .createparameter("@USERNAME", adVarChar, adParamInput, 50, Username)
			.parameters.append param
			set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LoginID)
			.parameters.append param
			set param = .createparameter("@ISVALID_COUNT", adInteger, adParamInputOutput, 0, 0)
			.parameters.append param
			.execute ,,adexecutenorecords
			isUserNameValid = .parameters("@ISVALID_COUNT")
			end with

			if isUserNameValid > 0 then
				arrform(i) = "You cannot enter a Username that is already on the system"
			i = i + 1			
			end if

			redim preserve arrform(i-1)
			ValidateForm = i
		
		End function
		
		Function Displayvalidation(arrform)		
			'sets the array of messages into html bullet points
			str = "There are errors with the following:"		
			str =  str & "<ul style='margin-left:1em'>"			
			    for i = 0 to ubound(arrform)
				    str = str & "<li>" & arrform(i) & "</li>"
			    next			
			str = str & "</ul>"			
			Displayvalidation =  str		
		End function
		
		Function InsertForm()
			
			if validatecount = 0 then
			
				'Password encryption
				dim encLib
				set encLib = Server.CreateObject("EncLib.EncDec")
				
				Dim cmd, param
	
				Set cmd=server.CreateObject("ADODB.Command")
				With cmd
				  .CommandType=adcmdstoredproc
				  .CommandText = "stp_NewLogin"
				  set .ActiveConnection=conn
				  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
				  .parameters.append param
				  set param = .createparameter("@Firstname", adVarChar, adParamInput, 100, Firstname)
				  .parameters.append param
				  set param = .createparameter("@Lastname", adVarChar, adParamInput, 100, Lastname)
				  .parameters.append param
				  set param = .createparameter("@Email", adVarChar, adParamInput, 100, Email)
				  .parameters.append param
				  set param = .createparameter("@Username", adVarChar, adParamInput, 250, Username)
				  .parameters.append param
				  set param = .createparameter("@Password", adVarChar, adParamInput, 100, encLib.encryptpwd(Password))
				  .parameters.append param
				  set param = .createparameter("@LevelofAccess", adInteger, adParamInput, 0, LevelofAccess)
				  .parameters.append param
				  set param = .createparameter("@LoginStatus", adInteger, adParamInput, 0, LoginStatus)
				  .parameters.append param
				  set param = .createparameter("@ProviderID", adInteger, adParamInput, 0, ProviderID)
				  .parameters.append param
				  .execute ,,adexecutenorecords
				end with
			
				strStatus = "The New Login details have been added to the system."
				
			else
				'display and html formatted list error messages (fields that have not been filled)
				strStatus = Displayvalidation(arrform)
			end if
			
		End function
		
		Function UpdateForm()
			'Password encryption
			dim encLib
			set encLib = Server.CreateObject("EncLib.EncDec")
		
			if validatecount = 0 then
				
				Dim cmd, param	
			
					Set cmd=server.CreateObject("ADODB.Command")
					With cmd
					  .CommandType=adcmdstoredproc
					  .CommandText = "stp_MD_UpdateLogins"
					  set .ActiveConnection=conn
					  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
					  .parameters.append param
					  set param = .createparameter("@FIRSTNAME", adVarChar, adParamInput, 100, Firstname)
					  .parameters.append param
					  set param = .createparameter("@LASTNAME", adVarChar, adParamInput, 100, Lastname)
					  .parameters.append param
					  set param = .createparameter("@EMAIL", adVarChar, adParamInput, 100, Email)
					  .parameters.append param
					  set param = .createparameter("@USERNAME", adVarChar, adParamInput, 250, Username)
					  .parameters.append param
					  set param = .createparameter("@PASSWORD", adVarChar, adParamInput, 100, encLib.encryptpwd(Password))
					  .parameters.append param
					  set param = .createparameter("@LEVELOFACCESS", adInteger, adParamInput, 0, LevelofAccess)
					  .parameters.append param
					  set param = .createparameter("@LOGINSTATUS", adInteger, adParamInput, 0, LoginStatus)
					  .parameters.append param
					  set param = .createparameter("@PROVIDERID", adInteger, adParamInput, 0, ProviderID)
					  .parameters.append param
					  set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LoginID)
					  .parameters.append param
					  .execute ,,adexecutenorecords
					end with
				
				strStatus="The Login details have been updated."
				
			else
				'display and html formatted list error messages (fields that have not been filled)
				strStatus = Displayvalidation(arrform)
			end if
		
		End function
		
		Function DeleteLogin()
		
			Dim cmd, param
	
				Set cmd=server.CreateObject("ADODB.Command")
				With cmd
				  .CommandType=adcmdstoredproc
				  .CommandText = "stp_MD_DeleteLogins"
				  set .ActiveConnection=conn
				  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
				  .parameters.append param
				  set param = .createparameter("@LoginID", adInteger, adParamInput, 0, LoginID)
				  .parameters.append param
				  .execute ,,adexecutenorecords
				end with

				strStatus="The Login details have been deleted."
				MoveVariable = 1
		End function

		'set variables function
		Call GetForm()

		If SaveDel = 2 Then
			DeleteLogin()
		Else
			validatecount = ValidateForm()
		    'if the form fields are valid then insert into database
			If Action = 0 Then
					InsertForm()
			Else
					UpdateForm()
			End If
		End If

	Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
</head>
<body onload="returnData()">
<script language="javascript" type="text/javascript">
function returnData()
{
	varstatus = "<div style='padding:10px;'><%=strStatus%></div>"
	parent.document.getElementById("errordiv").innerHTML = varstatus
	parent.document.getElementById("errordiv").className = "er"
	//setTimeout("parent.errordiv.innerHTML = ''",  2000);
    //setTimeout("parent.errordiv.className = ''",  2000);

	if ("<%=MoveVariable%>" == 1)
	{
		parent.window.location.href="../AmendLogin.asp";
	}
}
</script>
</body>
</html>