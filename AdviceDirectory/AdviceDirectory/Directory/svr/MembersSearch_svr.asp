<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Call OpenDB()

    Dim LOGINID
        LOGINID = nulltest(Session("UserID"))

    Dim PROVIDERID
        PROVIDERID = session("OrgID")
    Dim SELECTED_LOCALAUTHORITYAREAID
        SELECTED_LOCALAUTHORITYAREAID = nulltest(Request("sel_LOCALAUTHORITYAREA"))

	orgname = request("txt_OrgName")
	LOGIN_TYPE = request("hid_Logintype")

	If orgname = "" Then
		orgname = Null
	Else
		orgname = request("txt_OrgName")
	End If

	'If Login Type is an administrator (LOGIN_TYPE = "2") then they should be able to see all organisations
	'if it is = 1 then they are a provider and should only see their own organisation
	'If LOGIN_TYPE = "2" Then
	'	PROVIDERID = Null
	'Else
	'	PROVIDERID = Clng(PROVIDERID)
	'End If

	Dim cmd, param

		Set cmd=server.CreateObject("ADODB.Command")
		Set rsOrg =server.CreateObject("ADODB.Recordset") 
		With cmd
		  .CommandType=adcmdstoredproc
		  .CommandText = "MD_SELECTPROVIDER"
		  set .ActiveConnection=conn
		  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  	.parameters.append param
		  set param = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
		  	.parameters.append param
		  set param = .createparameter("@LOCALAUTHORITYAREAID", adInteger, adParamInput, 0, SELECTED_LOCALAUTHORITYAREAID)
		  	.parameters.append param		  	
		  set param = .createparameter("@PROVIDERNAME", adVarChar, adParamInput, 150, orgname)
 			.parameters.append param
		  set rsOrg = .execute
		end with

	'rw ProviderName
	Dim cmd2, param2

		Set cmd2=server.CreateObject("ADODB.Command")
		Set rsLogin =server.CreateObject("ADODB.Recordset")
		    rsLogin.locktype=adlockoptimistic
		    rsLogin.cursortype = adOpenStatic
		With cmd2
		  .CommandType = adCmdStoredProc
		  .CommandText = "MD_SELECTLOGINS_NEW"
		  set .ActiveConnection=conn
		  	set param2 = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
		  	.parameters.append param2
		   	set param2 = .createparameter("@ProviderID", adInteger, adParamInput, 0, null)
  			.parameters.append param2
  			set param2 = .createparameter("@PROVIDERNAME", adVarChar, adParamInput, 150, orgname)
  			.parameters.append param2
  			  set param2 = .createparameter("@LOGINID", adInteger, adParamInput, 0, LOGINID)
		  	.parameters.append param2
		  'set rsLogin = .execute
		end with

	rsLogin.CursorLocation = adUseClient
	rsLogin.Open cmd2

	orglist = CreateString(rsOrg, rsLogin)

	Function CreateString(rsOrg, rsLogin)

	If NOT rsOrg.EOF Then

		loginname = "<table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"">"
		loginname = loginname & "<tr>"
		loginname = loginname & "<td width=""67%"" class=""membersheader""><b>Organisation Name:</b></td>"
		loginname = loginname & "<td width=""33%"" class=""membersheader""><b>Status:</b></td>"
		loginname = loginname & "</tr>"

		Do until rsOrg.eof

			orgid = rsOrg("ProviderID")
			OrgStatus = rsOrg(2)

			If OrgStatus Then
				OrgStatus = "Active"
			Else
				OrgStatus = "Inactive"
			End If

            ' NO FILTER RESULTS IN ALL LOGINS DISPLAYED
            '
            '-- RESULT SET MULTIPLIED
            'rsLogin.filter = "ProviderID=28"
            '-- FILTERS TO ORG LOGGED IN
			rsLogin.filter = "ProviderID=" & orgid
			'-- FILTERS TO ORG IN ABOVE SP
			'rsLogin.filter = "ProviderID=" & rsOrg("ProviderID")

			ProviderName =  replace(rsOrg("ProviderName"),"'","\'")

				loginname = loginname & "<tr>" & vbCrLf &_
				"<td class=""membersbody""><a href=""#"" onclick=""ShowList(" & orgid & ");return false;""><img src=""images/treeview.gif"" alt="""" /></a> <a href=""AmendOrganisation.asp?OrgID=" & orgid & """>" & ProviderName & "</a></td>" & vbCrLf &_
				"<td class=""membersbody"">" & OrgStatus & "</td>" & vbCrLf &_
				"</tr>"	& vbCrLf &_
				"<tr><td colspan=2>" & vbCrLf &_
			    "<table width=""100%"" id=""" & orgid & """ style=""display:none"">" & vbCrLf
			    If rsLogin.eof Then
			        loginname = loginname & "<tr><td width=""100%"" class=""membersbody"">No Login Data Found</td></tr>" & vbCrLf
			    Else
			        loginname = loginname & "<tr>" & vbCrLf &_
			        "<td width=""33%"" class=""membersheader""><b>Adviser:</b></td>" & vbCrLf &_
			        "<td width=""33%"" class=""membersheader""><b>Level of Access:</b></td>" & vbCrLf &_
			        "<td width=""33%"" class=""membersheader""><b>Active:</b></td>"	& vbCrLf &_
			        "</tr>" & vbCrLf
			    End If
			Do until rsLogin.eof

				LoginStatus = rsLogin("LoginStatus")

				If LoginStatus Then
					LoginStatus = "Active"
				Else
					LoginStatus = "Inactive"
				End If

				'if user is a provider then do not allow them to access/edit user details
				'if session("LoginType") = 1 then
				'	AmendLink = "#"
				'else
					AmendLink = "NewLogin.asp?Action=1&LoginID=" & rsLogin("LoginID") & ""
				'end if

				loginname = loginname & "<tr>" '& vbCrLf
				loginname = loginname & "<td width=""30%"" class=""membersbody""><a href=" & AmendLink & ">" '& vbCrLf
				loginname = loginname & "" & replace(rsLogin("FirstName") & " " & rsLogin("LastName"),"'","\'")
				loginname = loginname & "</a></td>" '& vbCrLf
				loginname = loginname & "<td width=""33%"" class=""membersbody"">" & rsLogin("ACCESSTYPE") & "</td>" '& vbCrLf
				loginname = loginname & "<td width=""33%"" class=""membersbody"">" & LoginStatus & "</td>" '& vbCrLf
				loginname = loginname & "</tr>"	' & vbCrLf

			rsLogin.MOVENEXT
			Loop
				loginname = loginname & "</table>"	' & vbCrLf
				loginname = loginname & "</td></tr>"

		rsOrg.MOVENEXT
		Loop

		loginname = loginname & "</table>"

	End If

	CreateString = loginname

	End function

	Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
</head>
<body onload="returnData()">
<script language="javascript" type="text/javascript">
function returnData()
{
	parent.document.getElementById("divOrgString").innerHTML = document.getElementById("divOrgString").innerHTML //orglist;
}
</script>
<div id="divOrgString">
<%
'rw "MD_SELECTPROVIDER" & LOGINID & "," & ""
%>
<%=orglist%>
</div>
</body>
</html>