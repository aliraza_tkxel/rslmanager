﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%	
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
If (Session("svCustomerID") = "" OR IsNull(Session("svCustomerID")) OR Session("svCustomerID") = -1) Then
    Response.Redirect("Login.asp?Session=0")
End If
%>
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
Function SavePRIORITIES2()

    Dim myStrPL
    Dim PRIORITIES
    Dim ASSESSMENTOPTIONS

        myStrPL = Request("PriorityList")

    If myStrPL <> "" Then
        MyArray = split(myStrPL,"|")
        PRIORITIES = ""
        ASSESSMENTOPTIONS = ""
        For Each item In MyArray
            MyArray2 = split(item,"-")
            PRIORITIES = PRIORITIES & MyArray2(0) & ","
            ASSESSMENTOPTIONS = ASSESSMENTOPTIONS & MyArray2(1) & ","
        Next
    End If

        PRIORITIES = Left(PRIORITIES,Len(PRIORITIES)-1) 
        ASSESSMENTOPTIONS = Left(ASSESSMENTOPTIONS,Len(ASSESSMENTOPTIONS)-1)

    passcheck = 0
	passcheck_text = "<div style='padding:10px; padding-right:0px'>"
	
Call OpenDB()
    Conn.execute "EXEC CRMS_CURRENTPRIORITIES_UPDATE " & Session("svCustomerID") & ",null,'" & ASSESSMENTOPTIONS & "','" & PRIORITIES & "'"
Call CloseDB()

    passcheck_text = passcheck_text & "The priorities of the " & UBound(MyArray)+1 & " main areas of concern have been updated to the new priority order."
    passcheck_text = passcheck_text & "</div>"
    passcheck = 1

End Function

If Request.Form("action") = "Update" Then

Dim passcheck 		' USER OK/NOT OK TO LOGIN
Dim passcheck_text
	passcheck = ""
	
    Call SavePRIORITIES2()

    ' SET ERROR FLAG - CSS STYLE SHEET USED
    If passcheck = 0 Then
        ErrorClass = " class=""er"""
    Else
        ErrorClass = " class=""no_error"""
    End If

End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:5px; padding-bottom:5px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

-->
</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script type="text/javascript" language="javascript">

function CustomSelect(aItem1)
{
    var isSelectValidationError = false
    var myString=aItem1
    var mySplitResult = myString.split(",")
    var SelectedValues = ""
    for(i = 0; i < mySplitResult.length; i++)
    {
        SelectedValues += document.getElementById('ASSESSMENT_'+mySplitResult[i]).value + " "
	    if (document.getElementById('ASSESSMENT_'+mySplitResult[i]).value == "")
	    {
	        isSelectValidationError = true
	    }
    }
    if (isSelectValidationError == true)
    {
        alert("Please select a Priority for the chosen options")
    }
    else
    {
       var temp = SelectedValues
       var array2 = temp.split(" ") ;
       var array1= new Array;
       for(var i = 0; i < array2.length; i++)
       {
           var xx = true;
           var ArrayVal = array2[i];
           for(var j = i+1; j < array2.length; j++)
           {
             if(array2[j] == ArrayVal) 
              xx = false;
              if (xx == false)
              {
                isSelectValidationError = true
                alert("Please ensure that all priorities are ordered with out duplication");
                break;
              }
           }
       	}
        if (isSelectValidationError == false)
        {
            AllSelections(aItem1)
        }
    }
}


function AllSelections(SelectedValues)
{
    var temp = SelectedValues
    var mySplitResult = temp.split(",")
    document.getElementById('PriorityList').value = ""
    for(i = 0; i < mySplitResult.length; i++)
    {
       document.getElementById('PriorityList').value += document.getElementById('ASSESSMENT_'+mySplitResult[i]).selectedIndex+'-'+ mySplitResult[i] + '|'
    }
    document.getElementById('PriorityList').value = document.getElementById('PriorityList').value.slice(0, -1)

    var temp2 = document.getElementById('PriorityList').value
    var mySplitResult2 = temp2.split("|")
    var mySplitResult3
    for(k = 0; k < mySplitResult2.length; k++)
    {
        mySplitResult3 = mySplitResult2[k].split("-")
        document.getElementById('Priority'+mySplitResult3[0]).value = mySplitResult3[0]+'-'+mySplitResult3[1]    
    }  
    SavePriorities()
}


function blar(val,priority)
{
    if (priority != "")
    {
        document.getElementById('Priority'+priority).value == "";
        document.getElementById('Priority'+priority).value = priority+'-'+val;
    }
}


function SavePriorities()
{
    document.getElementById('action').value = 'Update'
    document.Form1.submit();
}

</script>
</head>
<body lang="en">
<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>"> <!-- onsubmit="return checkMyForm(this)" -->
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                     <!-- ERRORS/PAGE LOAD -->
                     <div id="errordiv" style="float:left; width:100%;" <%=ErrorClass%>>
                            <%=passcheck_text%>
                    </div>
                    <!-- DYNAMIC PAGE LOAD -->         
<%

    SQL = "SELECT COUNT(1) " &_ 
    "FROM ASSESSMENT_OPTIONS AO " &_ 
    "INNER JOIN CUSTOMERID_PRIORITIES CP ON CP.ASSESSMENTOPTIONID = AO.ID " &_ 
    "INNER JOIN ( " &_
    "SELECT MAX(HISTORYID) AS HISTORYID, CUSTOMERID " &_
    "FROM  CUSTOMERID_PRIORITIES " &_
    "GROUP BY CUSTOMERID " &_
    ") HCP ON HCP.HISTORYID = CP.HISTORYID AND HCP.CUSTOMERID = CP.CUSTOMERID " &_
    "WHERE CP.CUSTOMERID = " & Session("svCustomerID")
    Call OpenDB()
    Call OpenRs (rs,SQL)
        Dim ArrayLength 
            ArrayLength = rs(0)
    Call CloseRs(rs)
    Call CloseDB()
%>
<% If ArrayLength >= 1 Then  

    ReDim Preserve ArrayID(ArrayLength)
    ReDim Preserve ArrayDescription(ArrayLength)
    ReDim Preserve ArrayPriority(ArrayLength)
    
    SQL = "SELECT AO.ID, AO.DESCRIPTION, CP.PRIORITY " &_ 
    "FROM ASSESSMENT_OPTIONS AO " &_ 
    "INNER JOIN CUSTOMERID_PRIORITIES CP ON CP.ASSESSMENTOPTIONID = AO.ID " &_ 
    "INNER JOIN ( " &_
    "SELECT MAX(HISTORYID) AS HISTORYID, CUSTOMERID " &_
    "FROM  CUSTOMERID_PRIORITIES " &_
    "GROUP BY CUSTOMERID " &_
    ") HCP ON HCP.HISTORYID = CP.HISTORYID AND HCP.CUSTOMERID = CP.CUSTOMERID " &_
    "WHERE CP.CUSTOMERID = " & Session("svCustomerID") & " " &_
    "ORDER BY CP.HISTORYID DESC, CP.PRIORITY, CP.ID, ID "
    Call OpenDB()
    Call OpenRs (rsPostCode,SQL)
        If NOT rsPostCode.EOF Then
        i = 1
        rw "From the " & ArrayLength & " area(s) you have selected, please number them in order of priority:"
        rw "<div style=""padding-left:10px"">"
        Dim ASSESSMENTOPTIONS
            ASSESSMENTOPTIONS = ""
            while not rsPostCode.eof            
                ArrayID(i) = rsPostCode("ID")
                ArrayDescription(i) = rsPostCode("DESCRIPTION")
                ArrayPriority(i) = rsPostCode("PRIORITY")
                ASSESSMENTOPTIONS = ASSESSMENTOPTIONS + Cstr(ArrayID(i)) + ","
                
              %>                       
                <div class="row" style="padding-top:1em; padding-left:1em">
                    <span class="formw" style="width:auto">
                        <select name="ASSESSMENT_<%=ArrayID(i)%>" id="ASSESSMENT_<%=ArrayID(i)%>" style="width:auto" onchange="blar(<%=ArrayID(i)%>,this.value)">
                            <option value=""> </option>
                                <%
                                For j=1 to ArrayLength
                                    rw "<option value="""&j&""""
                                    If j = CInt(ArrayPriority(i)) Then 
                                        rw "selected=""selected"">"
                                    Else
                                        rw ">"
                                    End If
                                    rw "" & j & "</option>"
                                Next
                                %>
                        </select>
                    </span>
                    <span class="label">
                        <%=ArrayDescription(i)%>
                    </span>
                </div>
                <%
          	    rsPostCode.MoveNext()
          	    i = i + 1
            Wend
            
            ASSESSMENTOPTIONS = Left(ASSESSMENTOPTIONS,Len(ASSESSMENTOPTIONS)-1)
            %>
                    <div class="row" style="padding-top:1em;">
                        <span class="label"> 
                            <input class="submit" type="button" name="BtnUpdate" id="BtnUpdate" value=" Update " title="Submit Button : Update" style="width:auto" onclick="CustomSelect('<%=ASSESSMENTOPTIONS%>')"/>
                        </span>
                    </div>
            <%
        Else
            '
        End If
    Call CloseRs(rsPostCode)
    Call CloseDB()
%>               
                    
                    <%
                    For k=1 to ArrayLength
                    SessionNumber = "svP"+CStr(k)
                    %>
                    <!-- <br />Priority<%=k%> //-->
                    <input type="hidden" name="Priority<%=k%>" id="Priority<%=k%>" value="<%=Session(SessionNumber)%>" />
                    <%
                    Next
                    %>
                    <input type="hidden" name="PriorityList" id="PriorityList" value="" />
                    <input type="hidden" name="action" id="action" value="" />
                    <!-- DYNAMIC PAGE LOAD -->
                    <% Else %>
                        You have no 'Current Priorities' assigned to the Self Assessment selections. Please complete a <a href="/SelfAssessment.asp">Self Assessment</a>.
                    <% End If %>
                    </div>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" alt="" style="visibility:hidden" />
</form>
</body>
</html>