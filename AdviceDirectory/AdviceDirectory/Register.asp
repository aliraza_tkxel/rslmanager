﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>

<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
PartNo = 1

Call OpenDB()
Call BuildSelect(STR_HEAR,"sel_HEAR","HEAR_ABOUT_US","ID,DESCRIPTION","DESCRIPTION","Please Select",Session("RF_HEAR"),Null,Null,"  tabindex=""8"" required=""N"" validate=""/([0-9])$/""  validateMsg=""Hear about us"" ") 
Call CloseDB()

'Call Debug()

If Request.Form("Next") <> "" Then

    Session("RF_FIRSTNAME") = Request.Form("txt_FIRSTNAME")
    Session("RF_SURNAME") = Request.Form("txt_SURNAME")
    Session("RF_DOB") = Request.Form("txt_DOB")
    Session("RF_HOUSENUMBER") = Request.Form("txt_HOUSENUMBER")
    Session("RF_POSTCODE") = Request.Form("txt_POSTCODE")
    Session("RF_EMAIL") = Request.Form("txt_EMAIL")
    Session("RF_TELEPHONE") = Request.Form("txt_TELEPHONE")
    Session("RF_MOBILE") = Request.Form("txt_MOBILE")
    Session("RF_HEAR") = Request.Form("sel_HEAR")
    Session("RF_HEAR_OTHER") = Request.Form("txt_OTHER")

    Response.Redirect ("/Register_part2.asp")
End If

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
<script type="text/javascript">

function dd(frm)
{
   if ( (document.getElementById("sel_HEAR").options[document.getElementById("sel_HEAR").selectedIndex].text == 'Other') && ( document.getElementById("txt_OTHER").value == "") )
    {
        alert("There are errors with the following form fields.\nOther (Please Specify)");
        return false;
    }
    else
    {
        return checkMyForm(frm);
    }
}

</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="Div1" style="text-align:right; background-color: #cc0000; color:White; border:solid 1px #cc0000; border-bottom:0px; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <h3>Step <%=PartNo%> of 2</h3>
                </div>
                <div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <div id="errordiv" style="float:left; width:100%" <%=ErrorClass%>>
                            <%=passcheck_text%>
                    </div>
                        <form method="post" name="Form1" id="Form1" action="<%=strIncludeFile%>" onsubmit="return dd(this);"><!--return checkMyForm(this)//-->
                            <div id="Part1" style="float:left; width:100%">
	                            <fieldset>
		                            <legend>Register Part 1</legend>
			                            <p class="H4">
			                                To Create your own Greater Manchester Advancment Network account, please complete the following details:
			                            </p>
			                            <div id="Frm" style="float:left; width:48%;">
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_FIRSTNAME" accesskey="F">
                                                        <span style="text-decoration:underline; cursor:pointer">F</span>irstname:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_FIRSTNAME")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="txt_FIRSTNAME" 
                                                        id="txt_FIRSTNAME" 
                                                        maxlength="40" 
                                                        tabindex="1" 
                                                        required="Y" 
                                                        validate="/([a-zA-Z0-9,\s]{1,40})$/" 
                                                        validateMsg="Firstname" />
                                                    <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_SURNAME" accesskey="S">
                                                        <span style="text-decoration:underline; cursor:pointer">S</span>urname:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_SURNAME")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" size="40" 
                                                        name="txt_SURNAME" 
                                                        id="txt_SURNAME" 
                                                        maxlength="40" 
                                                        tabindex="2" 
                                                        required="Y" 
                                                        validate="/([a-zA-Z0-9,\s]{1,40})$/" 
                                                        validateMsg="Surname" />
                                                    <img src="/js/FVS.gif" name="img_SURNAME" id="img_SURNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_DOB" accesskey="D">
                                                        <span style="text-decoration:underline; cursor:pointer">D</span>oB (dd/mm/yyyy):
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_DOB")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="txt_DOB" 
                                                        id="txt_DOB" 
                                                        maxlength="10" 
                                                        value="<%=DOB%>" 
                                                        tabindex="3" 
                                                        required="Y"                                                    
                                                        validate="/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/"
                                                        validateMsg="Date of Birth (dd/mm/yyyy)" />
                                                         <!--   Previous Regular Expression
                                                                Dates prior to 1900 were permitted.
                                                                validate="/^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4}|[0-9]{2})$/" 
                                                        //-->
                                                    <img src="/js/FVS.gif" name="img_DOB" id="img_DOB" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_HOUSENUMBER" accesskey="H">
                                                        <span style="text-decoration:underline; cursor:pointer">H</span>ouse Number:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_HOUSENUMBER")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" size="40" 
                                                        name="txt_HOUSENUMBER" 
                                                        id="txt_HOUSENUMBER" 
                                                        tabindex="4" 
                                                        maxlength="10" 
                                                        required="N" 
                                                        validate="/([a-zA-Z0-9,\s]{1,20})$/" 
                                                        validateMsg="House Number" />
                                                    <img src="/js/FVS.gif" name="img_HOUSENUMBER" id="img_HOUSENUMBER" width="15px" height="15px" alt="" />
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_POSTCODE" accesskey="P">
                                                        <span style="text-decoration:underline; cursor:pointer">P</span>ostcode:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_POSTCODE")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="txt_POSTCODE" 
                                                        id="txt_POSTCODE" 
                                                        maxlength="8" 
                                                        tabindex="5" 
                                                        required="Y" 
                                                        validate="/^[A-Za-z]{1,2}\d{1,2}[A-Za-z]? \d[A-Za-z]{2}$/" 
                                                        validateMsg="Postcode" />
                                                    <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                           </div>
                                           <div class="row">
                                                <span class="label">
                                                    <label for="txt_EMAIL" accesskey="E">
                                                        <span style="text-decoration:underline; cursor:pointer">E</span>mail:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_EMAIL")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="txt_EMAIL" 
                                                        id="txt_EMAIL" 
                                                        maxlength="250" 
                                                        tabindex="6" 
                                                        required="N" 
                                                        validate="/^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/" 
                                                        validateMsg="Email" />
                                                    <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" alt="" />
                                                </span>
                                           </div>
                                           <div class="row">
                                                <span class="label">
                                                    <label for="txt_TELEPHONE" accesskey="T">
                                                        <span style="text-decoration:underline; cursor:pointer">T</span>elephone:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_TELEPHONE")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="txt_TELEPHONE" 
                                                        id="txt_TELEPHONE" 
                                                        maxlength="20" 
                                                        tabindex="7" 
                                                        required="N" 
                                                        validate="/^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/" validateMsg="Telephone" />
                                                    <img src="/js/FVS.gif" name="img_TELEPHONE" id="img_TELEPHONE" width="15px" height="15px" alt="" />
                                                </span>
                                           </div>
                                           <div class="row">
                                                <span class="label">
                                                    <label for="txt_MOBILE" accesskey="M">
                                                        <span style="text-decoration:underline; cursor:pointer">M</span>obile:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_MOBILE")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="txt_MOBILE" 
                                                        id="txt_MOBILE" 
                                                        maxlength="20" 
                                                        tabindex="7" 
                                                        required="N" 
                                                        validate="/^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/" validateMsg="Mobile" />
                                                    <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" alt="" />
                                                </span>
                                           </div>
                                           <div class="row">
                                                <span class="label">
                                                    <label for="sel_HEAR" accesskey="W">
                                                        <span style="text-decoration:underline; cursor:pointer">W</span>here did you hear about us?:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <%=STR_HEAR%>
                                                    <img src="/js/FVS.gif" name="img_HEAR" id="img_HEAR" width="15px" height="15px" alt="" />
                                                </span>
                                           </div>
                                           <div class="row">
                                                <span class="label">
                                                    <label for="txt_OTHER" accesskey="O">
                                                        <span style="text-decoration:underline; cursor:pointer">O</span>ther (Please Specify):
                                                    </label>
                                                 </span>
                                                <span class="formw"> 
                                                 <input 
                                                        value="<%=Session("RF_OTHER")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" size="40" 
                                                        name="txt_OTHER" 
                                                        id="txt_OTHER" 
                                                        maxlength="40" 
                                                        tabindex="8" 
                                                        required="N" 
                                                        validate="/([a-zA-Z0-9,\s]{1,40})$/" 
                                                        validateMsg="Other (Please Specify)" />
                                                        <img src="/js/FVS.gif" name="img_OTHER" id="img_OTHER" width="15px" height="15px" alt="" />
                                                </span>
                                            </div>
                                           <div class="row">
                                                <span class="label">&nbsp;</span>
                                                <span class="formw">
                                                    <input class="submit" 
                                                            type="submit" 
                                                            name="Next" 
                                                            id="Next" 
                                                            value=" Next " 
                                                            title="Submit Button : Next" 
                                                            tabindex="9" 
                                                            style="width:auto"/>
                                                </span>
                                           </div>
                                        </div>
		                            </fieldset>
		                        </div>
                              </form>
                              <div style="clear:left; padding-top:10px;">Don't have an email address? Click here <a href="/NoEmailAccount.asp" title="Don't have an email address? Click here">Click here</a></div>
                            </div>
                    <br />
                    <p>
                        Already got a Username and Password? <a href="/Login.asp" title="Already got a Username and Password? Login now:">Login</a> now.
                    </p>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>