﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%	
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
If (Session("svCustomerID") = "" OR IsNull(Session("svCustomerID")) OR Session("svCustomerID") = -1) Then
    Response.Redirect("Login.asp?Session=0")
End If
%>
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
'Uses pythag to work out the distance between 2 points
Function Distance (e1, e2, n1, n2)
	Distance = ((e1-e2)^2 + (n1-n2)^2)^0.5
End Function

Function CurrentContacts(CustomerID, Priority, display)

'   SQL = " SELECT CP.SESSIONID, AO.DESCRIPTION AS BARRIER, CP.PRIORITY, P.PROVIDERID, P.PROVIDERNAME, S.SITEID, S.SITENAME, S.ADDRESSLINE1, S.TELEPHONE, S.EMAIL, R.EASTING, R.NORTHING, '' AS DISTANCE, R.CUSTOMERID " &_
'       " FROM dbo.TBL_MD_TMPSEARCH_RESULT_SAVED RS " &_
'       " INNER JOIN TBL_MD_TMPSEARCH_RESULT R ON R.SITEID = RS.SITEID AND R.SESSIONID = RS.SESSIONID " &_
'       " RIGHT OUTER JOIN CUSTOMERID_PRIORITIES CP ON CP.SESSIONID = R.SESSIONID AND CP.ASSESSMENTOPTIONID = RS.ASSESSMENTOPTIONID " &_
'	   "     LEFT JOIN TBL_MD_PROVIDER P ON P.PROVIDERID = R.PROVIDERID " &_
'	   "     LEFT JOIN TBL_MD_PROVIDER_DETAIL PD ON R.PROVIDERID = PD.PROVIDERID " &_
'	   "     LEFT JOIN TBL_MD_PROVIDER_SITE S ON S.SITEID = R.SITEID " &_
'       " INNER JOIN ASSESSMENT_OPTIONS AO ON AO.ID = CP.ASSESSMENTOPTIONID " &_
'       " WHERE CP.CUSTOMERID = " & CustomerID & " AND CP.PRIORITY = " & Priority & " " &_
'       " ORDER BY CP.PRIORITY"
       
 SQL = " SELECT IsNull(PD.PROVIDERSUMMARY,'Awaiting Information') as PROVIDERSUMMARY, CP.SESSIONID, AO.ID AS AOID, AO.DESCRIPTION AS BARRIER, CP.PRIORITY, P.PROVIDERID, P.PROVIDERNAME, S.SITEID, S.SITENAME, S.ADDRESSLINE1, S.TELEPHONE, S.EMAIL, R.EASTING, R.NORTHING, '' AS DISTANCE, R.CUSTOMERID " &_
       " FROM dbo.TBL_MD_TMPSEARCH_RESULT_SAVED RS " &_
       " INNER JOIN TBL_MD_TMPSEARCH_RESULT R ON R.SITEID = RS.SITEID AND R.SESSIONID = RS.SESSIONID " &_
       " RIGHT OUTER JOIN " &_
       " ( " &_
       "  	SELECT SESSIONID, ASSESSMENTOPTIONID, HISTORYID, PRIORITY, CUSTOMERID " &_
	   "	FROM CUSTOMERID_PRIORITIES OUT_CP " &_
	   "	WHERE HISTORYID = ( " &_
	   "				SELECT MAX(HISTORYID) AS HISTORYID " &_
	   "				FROM CUSTOMERID_PRIORITIES IN_CP " &_
	   "				WHERE OUT_CP.SESSIONID = IN_CP.SESSIONID " &_
	   "				GROUP BY IN_CP.SESSIONID " &_
	   "				) " &_
       " ) " &_
       " CP ON CP.SESSIONID = R.SESSIONID AND CP.ASSESSMENTOPTIONID = RS.ASSESSMENTOPTIONID  " &_
	   "     LEFT JOIN TBL_MD_PROVIDER P ON P.PROVIDERID = R.PROVIDERID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER_DETAIL PD ON R.PROVIDERID = PD.PROVIDERID " &_
	   "     LEFT JOIN TBL_MD_PROVIDER_SITE S ON S.SITEID = R.SITEID " &_
       " INNER JOIN ASSESSMENT_OPTIONS AO ON AO.ID = CP.ASSESSMENTOPTIONID " &_
       " WHERE CP.CUSTOMERID = " & CustomerID & " AND CP.PRIORITY = " & Priority & " " &_
       " ORDER BY CP.PRIORITY"

Call OpenDB()
Call OpenRs(rsPage,SQL)
If rsPage.EOF Then

            strResult = strResult & "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""5"" style=""background-color:white; color:black; padding-left:0px"">&nbsp;</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Distance</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
	strResult = strResult & "<tr>" &_
	    "<td valign=""top"" colspan=""5"">There were no results</td>" &_
	"</tr>" 
Else
        strResult = strResult & "<br/><b>Priority " & Priority & " ("& rsPage("BARRIER") &") " & "</b><a href=""JavaScript:Toggle('"&Priority&"')""><img src=""/images/plusImage.gif"" width=""20"" height=""20"" border=""0"" align=""absmiddle"" name=""IMG_"&Priority&""" /></a>" &_
                                "<div style=""display:"&display&";"" id=""Div_"&Priority&""">" &_
                                "<table cellspacing=""0"" cellpadding=""2"" width=""100%"" style=""border-collapse:collapse"" border=""0"">" & vbCrLf &_
        					         "<thead>" & vbCrLf &_
                                     "<tr>" & vbCrLf &_
                                        " <th colspan=""5"" style=""background-color:white; color:black; padding-left:0px""></th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                      "<tr class="""">" & vbCrLf &_
                                        " <th>Organisation</th>" & vbCrLf &_
                                        " <th>Address</th>" & vbCrLf &_
                                        " <th>Telephone</th>" & vbCrLf &_
                                        " <th>Email</th>" & vbCrLf &_
                                        " <th>Distance</th>" & vbCrLf &_
                                     "</tr>" & vbCrLf &_
                                 "</thead>" & vbCrLf &_
                                  "<tbody>"
   If IsNull(rsPage("ProviderID")) Then
	    strResult = strResult & "<tr>" &_
	        "<td valign=""top"" colspan=""5"">There were no results</td>" &_
	    "</tr>"
	Else
'i = 0
	    Do until rsPage.EOF
	    'i = i + 1
	        dblCustomerEasting = "362196"
	        dblCustomerNorthing = "404438"
	        dblDistance = Distance(dblCustomerEasting, rsPage("easting"), dblCustomerNorthing, rsPage("northing"))
			'Convert the distance in metres to miles
			dblDistance = dblDistance / 1609 

            dblDistance = rsPage("Distance")
		    If not isnumeric(dblDistance) Then
			    strDistance = "N/A" 
		    Else
			    strDistance = FormatNumber(dblDistance,2) & " miles"
		    End If
		    ProviderSummary =  rsPage("ProviderSummary")

		    strProvider = ""
		    If trim(rsPage("ProviderName") ) = trim(rsPage("SiteName") ) Then
			    strProvider = rsPage("ProviderName")
		    Else
			    strProvider = rsPage("ProviderName") & ", " & rsPage("SiteName")
		    End If

            ADDRESSLINE1 = rsPage("ADDRESSLINE1")
		    EMAIL = rsPage("EMAIL")
		    If Isnull(EMAIL) or EMAIL = "" Then EMAIL = NULL Else EMAIL = "<a href=""mailto:"&EMAIL&""" title=""email:"&strProvider&""">"&EMAIL&"</a>" End If
		    TELEPHONE = rsPage("TELEPHONE")

		    strResult = strResult & "<tr>" & vbCrLf &_
		    "<td><a onmouseover=""Tip('"&ProviderSummary&"', TITLE, 'Organisation Summary', TITLEBGCOLOR, '#cc0000', BORDERCOLOR, '#cc0000', SHADOW, true, SHADOWWIDTH, 7)"" onmouseout=""UnTip()"" onclick=""go(buildQueryString('Form1'),'OrgDetails',"&rsPage("SITEID")&")"" title="""&strProvider&""">" & strProvider & "</a></td>" & vbCrLf &_
		    "<td>" & ADDRESSLINE1 & "</td>" & vbCrLf &_
		    "<td>" & TELEPHONE & "</td>" & vbCrLf &_
		    "<td>" & EMAIL & "</td>" & vbCrLf &_
		    "<td>" & strDistance & "</td>" & vbCrLf &_
		    "</tr>" 
'-- SITE , PRIORITY ITEM E.G. HOUSING, PRIORITY ORDER (SPECIFIED BY USER)

	        rsPage.MoveNext
	        'rw i
	    Loop

	    End If

	    rsPage.Close
	set rsPage = Nothing

	'totalrows = cmd.Parameters("@TOTALROWS").Value	

	strResult = strResult & "</tbody>" & vbCrLf
	
	strResult = strResult & "<tfoot>" & vbCrLf &_
	"<tr>" & vbCrLf &_
	"<td valign=""top"" align=""right"" colspan=""5"">"

    'if cint(page) > cint(0) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page - maxrows & """>Previous Page</a>   &nbsp;" 
	'end if	
	'if cint(page) + cint(maxrows) < cint(totalrows) then
	'	strResult = strResult & "<a href=""membersarea/searchresult.asp?currentpage=" & page& "&page=" & page + maxrows & """>Next Page</a></td></tr>"
	'end if

End if

    strResult = strResult & "</td></tr></tfoot>" & vbCrLf &_
	                        "</table></div>"
	'strResult = strResult & "</table></div><br/>"
   
    'Next
    CurrentContacts = strResult
    'strResult = ""

'end if

'Call CloseDB()

End Function


  SQL = "SELECT COUNT(1) " &_ 
        "FROM ASSESSMENT_OPTIONS AO " &_ 
        "INNER JOIN CUSTOMERID_PRIORITIES CP ON CP.ASSESSMENTOPTIONID = AO.ID " &_ 
        "INNER JOIN ( " &_
        "SELECT MAX(HISTORYID) AS HISTORYID, CUSTOMERID " &_
        "FROM  CUSTOMERID_PRIORITIES " &_
        "GROUP BY CUSTOMERID " &_
        ") HCP ON HCP.HISTORYID = CP.HISTORYID AND HCP.CUSTOMERID = CP.CUSTOMERID " &_
        "WHERE CP.CUSTOMERID = " & Session("svCustomerID")
    Call OpenDB()
    Call OpenRs (rs,SQL)
        Dim ArrayLength 
            ArrayLength = rs(0)
    Call CloseRs(rs)
    Call CloseDB()

Dim ResultString
    ResultString = ""

For j=1 to ArrayLength
    If j = 1 Then
        ResultString = CurrentContacts(Session("svCustomerID"),j, "block")
    Else
        ResultString = ResultString & CurrentContacts(Session("svCustomerID"),j, "none")
    End If
Next
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

/* =links */

table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:5px; padding-bottom:5px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}

tbody tr:hover a
{
	background-color:#cc0000;
	color:white;
}

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

-->
</style>

<script type="text/javascript" language="javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript" language="javascript" src="/ClientScripting/formValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script language="javascript" type="text/javascript">

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}


/*
function buildQueryString2(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}
*/

function buildQueryString(theFormName) {

  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            
            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function checkbox_checker()
{
// set var checkbox_choices to zero
var checkbox_choices = 0;
// Loop from zero to the one minus the number of checkbox button selections
for (counter = 0; counter < document.Form1.checkbox.length; counter++)
{
// If a checkbox has been selected it will return true
// (If not it will return false)
if (document.Form1.checkbox[counter].checked)
{ checkbox_choices = checkbox_choices + 1; }
}
if (checkbox_choices > 3 )
{
// If there were more than three selections made display an alert box 
return (false);
}
if (checkbox_choices < 3)
{
// If there were less then selections made display an alert box 
return (false);
}
// If three were selected then display an alert box stating input was OK
return (true);
}

var FormFields = new Array()

function CheckPostcode(obj)
{

    FormFields = new Array("txt_POSTCODE|Postcode|POSTCODE|N");
    if (!checkForm('errordiv')) return;

    if (document.getElementById('Form1').elements['txt_POSTCODE'].value == "")
    {
        if (!(checkbox_checker()))
        {
            alert('Please select 3 items that you deem a priority')
        }
        else
        {
            LoadStep(2,'errordiv')
        }
    }
    else
    {
        xmlhttp.open("GET", "svr/check_pc.asp?txt_Postcode="+ document.getElementById('Form1').elements['txt_POSTCODE'].value,true);
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4)
          {
	          if (xmlhttp.status==200)
	          {
			        if (xmlhttp.responseText == "0") 
			        {
				        alert("Please enter a valid Postcode for <%=SITE_NAME%>.");
				        targetError('txt_POSTCODE','red');
				        return false;
			        }
			        else
			        {
			            if (!(checkbox_checker()))
			            {
			                alert('Please select 3 items that you deem a priority')
			            }
			            else
			            {
			                LoadStep(2,'errordiv')
			            }
			        }
		        }
	        }
        }
    xmlhttp.send(null)
    }   
}

function BuildXMLReport(str_url,elId,elED){

	//set the loading parameter
    document.getElementById(elED).innerHTML = "<div style='float:left; padding:10px;'>Please wait. Loading...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById(elED).className = "no_error"
	//call xmlhttp functions
	var strURL = str_url
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  +
									   str_url + "' could not be found.");
							   document.getElementById(elED).innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById(elED).innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById(elED).innerHTML = ""
							   }
							   // Call the desired result function
							   else {
									   document.getElementById(elId).innerHTML = strResponse
									   document.getElementById(elED).innerHTML = ""
									   document.getElementById(elED).className = ""
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}


function go(url,elId,pId)
{
    BuildXMLReport('rje.asp'+ url + 'siteid='+pId,'OrgDetails','errordiv');
}


function LoadStep(StepNo,elED)
{
    BuildXMLReport('/svr/CurrentContacts.asp','OrgDetails','errordiv');
}

</script>

<script type="text/javascript" language="javascript">

var plus = new Image();
    plus.src = "/images/plusImage.gif";
var minus = new Image();
    minus.src = "/images/minusImage.gif";

function toggleImg(imgName)
{
	document.images[imgName].src = (document.images[imgName].src==plus.src) ? minus.src:plus.src;
	return false;
}
		
function Toggle(refByName)
{
    if (document.getElementById("Div_"+refByName).style.display == "block")
    {
	    document.getElementById("Div_"+refByName).style.display = "none";
	}
    else
    {
	    document.getElementById("Div_"+refByName).style.display = "block";
	}
	toggleImg('IMG_'+refByName)
}

</script>

</head>
<body lang="en" onload="toggleImg('IMG_1')">
<script type="text/javascript" src="/ClientScripting/wz_tooltip.js"></script>
<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>"> <!-- onsubmit="return checkMyForm(this)" -->
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	</div>
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <!-- ERRORS/PAGE LOAD -->
                    <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>>
                        <%=passcheck_text%>
                    </div>
                    <!-- ERRORS/PAGE LOAD -->
                    <!-- DYNAMIC PAGE LOAD -->
                    <div id="OrgDetails" style="float:left;width:100%;">
                        <%=ResultString%>
                    </div>
                    <!-- DYNAMIC PAGE LOAD -->
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif"  width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif"  width="10px" height="10px" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" alt="" style="visibility:hidden" />
</form>
</body>
</html>