﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	'Response.Buffer = false
	'Response.Expires=-1000
	'Response.CacheControl="no-cache"
%>
<!-- #include virtual="includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

  			SITE_CONTENT = "Register"

	Call PageContent(SITE_CONTENT)
%>

<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
PartNo = 2

Call OpenDB()
Call BuildSelect(STR_SECURITY_QUESTION,"sel_SECURITY_QUESTION","SECURITY_QUESTION","ID,DESCRIPTION","DESCRIPTION","Please Select",Session("RF_SECURITY_QUESTION"),Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Security Question"" ")
Call CloseDB()

If Request.Form("Back") <> "" Then
    Response.Redirect "/Register.asp"
End If

If Request.Form("Save") <> "" Then

    Session("RF_USERNAME") = Session("RF_EMAIL")
    Session("RF_PASSWORD") = Request.Form("txt_PASSWORD")
    Session("RF_RETYPEPASSWORD") = Request.Form("txt_RETYPEPASSWORD")
    Session("RF_SECURITY_QUESTION") = Request.Form("sel_SECURITY_QUESTION")
    Session("RF_ANSWER") = Request.Form("txt_ANSWER")
    Session("RF_PASSWORD") = Request.Form("txt_PASSWORD")

Dim JS_OUT
Dim txt
Dim STR_CLASS

    txt = ""
    STR_CLASS = ""
    'txt_DOB = "06/11/1976"

Call OpenDB()
Call CheckRegistered()
Call CloseDB()

IF JS_OUT = 1 Then
    txt = "<div style='padding:10px;'>Your details match a user already registered within the Network. For Further information please Freephone "& FreePhone &".</div>"
    STR_CLASS = " class=""er"""
ElseIf JS_OUT = 2 Then
    txt = "<div style='padding:10px;'>Some of the requested information appears to be missing.</div>"
    STR_CLASS = " class=""er"""
ElseIf JS_OUT = 3 Then
     txt = "<div style='padding:10px;'>Your details have been added to the Greater Manchester Advancement Network. You may now <a href=""/login.asp"">login</a>.</div>"
     STR_CLASS = " class=""no_error"""
     SendEmail__()
     
    Session("RF_USERNAME") = null
    Session("RF_PASSWORD") = null
    Session("RF_RETYPEPASSWORD") = null
    Session("RF_SECURITY_QUESTION") = null
    Session("RF_ANSWER") = null
    Session("RF_PASSWORD") = null
    Session("RF_FIRSTNAME") = null
    Session("RF_DOB") = null
    Session("RF_SURNAME")= null
    Session("RF_HOUSENUMBER")= null
    Session("RF_POSTCODE")= null
    Session("RF_EMAIL")= null
    Session("RF_TELEPHONE")= null
    Session("RF_MOBILE")= null
    Session("RF_HEAR")= null
    Session("RF_HEAR_OTHER")= null
     
End If
	
    'Session("RF_FIRSTNAME") = Request.Form("txt_FIRSTNAME")
    'Session("RF_SURNAME") = Request.Form("txt_FIRSTNAME")
    'Session("RF_HOUSENUMBER") = Request.Form("txt_HOUSENUMBER")
    'Session("RF_POSTCODE") = Request.Form("txt_POSTCODE")
    'Session("RF_EMAIL") = Request.Form("txt_EMAIL")
    'Session("RF_TELEPHONE") = Request.Form("txt_TELEPHONE")
    'Session("RF_HEAR") = Request.Form("sel_HEAR")

    'strUserName = "d"
    'strRecipient = "robert.egan@reidmark.com"
    
    'rw "strUserName " & strUserName & "<br/>"
    'rw "strRecipient " & strRecipient & "<br/>"
    'rw "constReplyToName " &  constReplyToName & "<br/>"
    'rw "constReplyToEmail " &  constReplyToEmail & "<br/>"
    'rw "SITE_NAME " &  SITE_NAME &" : Login Details" & "<br/>"
    'HTML = "hi" & "<br/>"
    'rw HTML
    'Response.End()
    
Function SendEmail__()
   
    strToName = "robert egan"
    strToEmail = "robert.egan@reidmark.com"
    strFromName = "robert egan"
    strFromEmail = "robert.egan@reidmark.com"
    strSubject = "Thans"
    strTextBody = "thanks"
    strHTMLBody = null
    
    On Error Resume Next ' don't stop on error, we'll check this later
    
    Dim iMsg 
    Dim iConf 
    Dim Flds 
    'Const cdoSendUsingPort = 2 ' send msg using smtp over the network
    Const cdoSendUsingPort = 1 'tells cdo we're using the local smtp service
  
    set iMsg = CreateObject("CDO.Message")	'calls CDO message COM object
    set iConf = CreateObject("CDO.Configuration") 	'calls CDO configuration COM object

    Set Flds = iConf.Fields

    With Flds
    .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\inetpub\mailroot\pickup"  'verify that this path is correct
    '.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.yourservername.com"
    '.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 20

    ' use the following ONLY if your smtp server requires authentication
    '.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
    '.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "your@email.com"
    '.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "password"

    .Update 'updates CDO's configuration database
    End With

    ' apply the arguments you passed in to the message
    With iMsg
    Set .Configuration = iConf
    .To = strToName & " <" & strToEmail & ">"
    .From = strFromName & " <" & strFromEmail & ">"
    .Subject = strSubject
    ' send plain text or html
    If Len(strTextBody) > 0 Then
      .TextBody = strTextBody
    Else
      .HTMLBody = strHTMLBody
    End If
    .Send	'commands CDO to send the message
    End With  

    ' clean up
    Set iMsg = Nothing
    Set iConf = Nothing
    Set Flds = Nothing
    
End Function
    
    
    'Call SendEmail__()
        'If SendEMail(strUserName,  strRecipient, constReplyToName, constReplyToEmail, SITE_NAME&" : Register", "-", HTML) Then
			'passcheck = passcheck & "Your email has been successfully sent. Thank you."
		'Else
			'passcheck = passcheck & "There was an error sending your email. Please try again."
		'End If
    'rw passcheck
    'Response.Redirect "/Register_part2.asp"   
End If

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function wopenExternal(url, name, w, h)
{
  w += 32;
  h += 96;
  
  wleft = (screen.width - w) / 2;
  wtop = (screen.height - h) / 2;
  var win = window.open(url,
    name,
    'width=' + w + ', height=' + h + ', ' +
    'left=' + wleft + ', top=' + wtop + ', ' +
    'location=no, menubar=no, ' +
    'status=no, toolbar=no, scrollbars=yes, resizable=no');
}
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="Div1" style="text-align:right; background-color: #cc0000; color:White; border:solid 1px #cc0000; border-bottom:0px; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <span style="padding-top:1em; float:left"></span>
                    <h3><span style="padding-top:1em;">Step <%=PartNo%> of 2</span></h3>
                </div>
                <div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <div id="errordiv" <%=STR_CLASS%>><%=txt%></div> 
                        <form method="post" id="Form1" action="Register_part2.asp" onsubmit="return checkMyForm(this);">
                            <div id="Part1">
	                            <fieldset>
		                            <legend>Register Part 1</legend>
			                            <p class="H4">
			                                To Create your own Greater Manchester Advancment Network account, please complete the following details:
			                            </p>
			                            <div id="Frm" style="float:left; width:48%;">
                                            <div class="row">
                                                <span class="label">
                                                    <label for="TXT_USERNAME" accesskey="U">
                                                        <span style="text-decoration:underline; cursor:pointer">U</span>sername:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_EMAIL")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="text" 
                                                        size="40" 
                                                        name="TXT_USERNAME" 
                                                        id="TXT_USERNAME" 
                                                        maxlength="100" 
                                                        required="Y" 
                                                        validate="/[a-zA-Z0-9_\@\.\-]{6,70}$/"
                                                        validateMsg="Username" />
                                                    <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_PASSWORD" accesskey="P">
                                                        <span style="text-decoration:underline; cursor:pointer">P</span>assword:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_PASSWORD")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="password" 
                                                        size="40" 
                                                        name="txt_PASSWORD" 
                                                        id="txt_PASSWORD" 
                                                        maxlength="10" 
                                                        required="Y" 
                                                        validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                                        validateMsg="Password - Password must be between 6 and 10 Characters and contain at least 1 number." />
                                                    <img src="/js/FVS.gif" name="img_PASSWORD" id="img_PASSWORD" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_RETYPEPASSWORD" accesskey="R">
                                                        <span style="text-decoration:underline; cursor:pointer">R</span>etype Password:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input 
                                                        value="<%=Session("RF_RETYPEPASSWORD")%>" 
                                                        class="border-white" 
                                                        onblur="this.className='border-white'" 
                                                        onfocus="this.className='border-red'" 
                                                        type="password" 
                                                        size="40" 
                                                        name="txt_RETYPEPASSWORD" 
                                                        id="txt_RETYPEPASSWORD" 
                                                        maxlength="10" 
                                                        required="Y" 
                                                        validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                                        validateMsg="Retype Password" />
                                                    <img src="/js/FVS.gif" name="img_RETYPEPASSWORD" id="img1" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="sel_SECURITY_QUESTION" accesskey="S"> 
                                                        <span style="text-decoration:underline; cursor:pointer">S</span>ecurity Question:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <%=STR_SECURITY_QUESTION%>
                                                    <img src="/js/FVS.gif" name="img_SECURITY_QUESTION" id="img_SECURITY_QUESTION" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="txt_ANSWER" accesskey="R" style="cursor:pointer">
                                                        <span style="text-decoration:underline">A</span>nswer:
                                                    </label>
                                                </span>
                                                <span class="formw">
                                                    <input value="<%=Session("RF_ANSWER")%>" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="40" name="txt_ANSWER" id="txt_ANSWER" maxlength="10" value="" required="Y" validate="/([a-zA-Z0-9,\s]{4,500})$/" validateMsg="Answer" />
                                                    <img src="/js/FVS.gif" name="img_ANSWER" id="img2" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">
                                                    <label for="sel_DPA_AWARE" accesskey="D" style="cursor:pointer">
                                                        <span style="text-decoration:underline">T</span>erms and Conditions:
                                                    </label>
                                                </span>
                                                <span class="formw" style="white-space:nowrap">
                                                    <input class="border-white" type="checkbox" name="DPA_AWARE" id="DPA_AWARE" style="width:auto" onclick="clickFunction()" />
                                                    <input type="hidden" value="" id="hid_DPA_AWARE" name="hid_DPA_AWARE" required="Y" validate="/[1]/" validateMsg="Terms and Conditions" /> <img src="/js/FVS.gif" name="img_DPA_AWARE" id="img_DPA_AWARE" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                                </span>
                                            </div>
                                            <div class="row">
                                                <span class="label">&nbsp;</span>
                                                <span class="formw">Tick the box to confirm you have read and agree to the following <a href="JavaScript:wopenExternal('pPrivacy.asp','Privacy',650,500)">terms</a>.</span>
                                            </div>
                                            <div class="row">
                                                <span class="label">&nbsp;</span>
                                                <span class="formw">
                                                    <input class="submit" type="button" onclick="javascript:location.href='Register.asp'" return false;" name="Back" id="Back" value=" Back " title="Submit Button : Back" style="width:auto; margin-right:10px"/><input class="submit" type="submit" name="Save" id="Save" value=" Save " title="Submit Button : Save" style="width:auto"/>
                                                </span>
                                            </div>
                                        </div>
		                            </fieldset>
		                        </div>
                            </form>
                        </div>
                    <br />
                    <p>
                        Already got a Username and Password? <a href="/Login.asp" title="Already got a Username and Password? Login now:">Login</a> now.
                    </p>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>