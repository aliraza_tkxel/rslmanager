﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
    If Customerid <> "" Then
        ' ALL IS OK
    Else
        Response.Redirect("Login.asp?Session=0")
    End If

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\!|\.|\?|\;|\,|\:|/"
  			GP_curPath = Trim(regEx.Replace(GP_curPath, ""))

	Call PageContent(GP_curPath)
%>

<%
Dim strIncludeFile
	strIncludeFile = "/"&GP_curPath&".asp"
%>
<%
If Customerid = "" Then Customerid = -1 End If

Dim STR_SECURITYQUESTION

Dim SECURITYQUESTION
Dim SECURITYANSWER
Dim USERNAME
Dim PASSWORD

Function LoadRecord()
Call OpenDB()
SQL = "SELECT * FROM DBO.CUSTOMERID C INNER JOIN DBO.CUSTOMERDETAILS CD ON CD.CUSTOMERID = C.CUSTOMERID WHERE C.CUSTOMERID = " & Customerid
Call OpenRs(rsDetails,SQL)
    If NOT rsDetails.EOF Then   
        SECURITYANSWER = rsDetails("ANSWER")
        SECURITYQUESTION = rsDetails("QUESTION")
        USERNAME = rsDetails("USERNAME")
	    PASSWORD = rsDetails("PASSWORD")
    End If
Call CloseRs(rsDetails)
Call BuildSelect(STR_SECURITYQUESTION,"sel_SECURITYQUESTION","SECURITY_QUESTION","ID,DESCRIPTION","DESCRIPTION","Please Select",SECURITYQUESTION,Null,Null," required=""Y"" validate=""/([0-9])$/""  validateMsg=""Security Question"" ") 
Call CloseDB()
End Function

Function EditRecord()

On Error Resume Next

SECURITYQUESTION = nulltest(Request.Form("sel_SECURITYQUESTION"))
SECURITYANSWER = nulltest(Request.Form("txt_SECURITYANSWER"))
USERNAME = nulltest(Request.Form("txt_USERNAME"))
PASSWORD = nulltest(Request.Form("txt_PASSWORD"))

Set objConn	= Server.CreateObject("ADODB.Connection")
    	set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "PU_CLIENT_LOGINDETAILS_UPDATE"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@SECURITYQUESTION", adInteger, adParamInput, 0, SECURITYQUESTION)
                .Parameters.Append .createparameter("@SECURITYANSWER", adVarWChar, adParamInput, 300, SECURITYANSWER)
                .Parameters.Append .createparameter("@USERNAME", adVarWChar, adParamInput, 500, USERNAME)
                .Parameters.Append .createparameter("@PASSWORD", adVarWChar, adParamInput, 100, PASSWORD)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMERID)
                'Execute the function
			    .execute ,,adexecutenorecords
		    End With
	    objConn.Close
    Set objConn = Nothing

    ' check for error & set boolean value
    If Err.Number <> 0 Then
        retError = False
    Else
        retError = True
    End If

  On Error GoTo 0

  ' return boolean value
  EditRecord = retError

End Function

If Request.Form("Edit") <> "" Then

  
Call EditRecord()

Dim ErrorClass
Dim passcheck 		' EMAIL SENT YES/NO (REASON WHY)
	passcheck = ""
		
	If EditRecord() Then
		passcheck_text = passcheck_text & "<div style='padding:10px;'>Login Details have been updated.</div>"
		ErrorClass = " class=""no_error"""
	Else
		passcheck_text = passcheck_text & "<div style='padding:10px;'>There was an error updating Login Details. Please try again.</div>"
		ErrorClass = " class=""er"""
	End If

End If

Call LoadRecord()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<script type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>
</head>
<body lang="en">
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCat(10, "navmenu") %>
		</div>
		<!-- End Navigation -->
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;"><%=PAGE_CONTENT%></div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <div id="errordiv" style="margin-left:5px;" <%=ErrorClass%>><%=passcheck_text%></div>
                    <form method="post" id="Form1" action="<%=strIncludeFile%>" onsubmit="return checkMyForm(this);">
                        <fieldset>
	                        <legend>Login to this site</legend>
	                        <div id="Frm" style="float:left; width:100%;">
	                            <div class="row">
	                                <div style="float:left; width:100%; background-color:#cc0000; color:white; padding-bottom:1em; padding-top:1em; margin-bottom:1em;">
	                                    <div style="float:left; margin-left:1em">
	                                        Login Details
	                                    </div>
	                                    <div style="float:right; margin-right:1em">
	                                        <input class="submit" type="button" accesskey="B" name="Back" title="Button : My Details" value="Back" onclick="Javascript:window.location.href='MyDetails.asp'" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        <label for="sel_SECURITYQUESTION" accesskey="S">
                                            <span style="text-decoration:underline; cursor:pointer">S</span>ecurity Question:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <%=STR_SECURITYQUESTION %>
                                        <img src="/js/FVS.gif" name="img_SECURITYQUESTION" id="img_SECURITYQUESTION" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        <label for="txt_SECURITYANSWER" accesskey="S">
                                            <span style="text-decoration:underline; cursor:pointer">S</span>ecurity Answer:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" size="40" type="text" name="txt_SECURITYANSWER" id="txt_SECURITYANSWER" maxlength="300" value="<%=SECURITYANSWER%>" required="Y" validate="/([a-zA-Z0-9,\s]{4,300})$/" validateMsg="Security Answer" />
                                        <img src="/js/FVS.gif" name="img_SECURITYANSWER" id="img_SECURITYANSWER" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        <label for="txt_USERNAME" accesskey="U">
                                            <span style="text-decoration:underline; cursor:pointer">U</span>sername:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input 
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            size="40" type="text" 
                                            name="txt_USERNAME" 
                                            id="txt_USERNAME" 
                                            maxlength="250" 
                                            value="<%=USERNAME%>" 
                                            required="Y" 
                                            validate="/[a-zA-Z0-9_\@\.\-]{6,70}$/"
                                            validateMsg="Username :- Must be 6 - 70 characters in length. Letters and Numbers are permitted. Spaces and most special characters are not permitted." />
                                        <img src="/js/FVS.gif" name="img_USERNAME" id="img_USERNAME" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        <label for="txt_PASSWORD" accesskey="P">
                                            <span style="text-decoration:underline; cursor:pointer">P</span>assword:
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            size="40" type="password" 
                                            name="txt_PASSWORD" 
                                            id="txt_PASSWORD" 
                                            maxlength="50" 
                                            value="<%=PASSWORD%>" 
                                            required="Y" 
                                            validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                            validateMsg="Password - Password must be between 6 and 10 Characters and contain at least 1 number." />
                                        <img src="/js/FVS.gif" name="img_PASSWORD" id="img_PASSWORD" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                               </div>
                                <div class="row">
                                    <span class="label" style="white-space:nowrap;">
                                        <label for="txt_RETYPEPASSWORD" accesskey="P">
                                            <span style="text-decoration:underline; cursor:pointer">P</span>assword (re-type):
                                        </label>
                                    </span>
                                    <span class="formw">
                                        <input 
                                            class="border-white" 
                                            onblur="this.className='border-white'" 
                                            onfocus="this.className='border-red'" 
                                            size="40" 
                                            type="password" 
                                            name="txt_RETYPEPASSWORD" 
                                            id="txt_RETYPEPASSWORD" 
                                            maxlength="50" 
                                            value="" 
                                            required="Y" 
                                            validate="/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,10})$/" 
                                            validateMsg="Password (re-type) - Password must be between 6 and 10 Characters and contain at least 1 number." />
                                        <img src="/js/FVS.gif" name="img_RETYPEPASSWORD" id="img_RETYPEPASSWORD" width="15px" height="15px" alt="" /> <span style="color:red">*</span>
                                    </span>
                               </div>
                               <div class="row">
                                    <span class="label">&nbsp;</span>
                                    <span class="formw">
                                        <input class="submit" type="submit" name="Edit" id="BtnEditLD" value=" Edit " title="Submit Button : Edit Login details" style="width:auto" />
                                    </span>
                               </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
</body>
</html>