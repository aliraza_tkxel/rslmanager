<%@LANGUAGE="VBSCRIPT" %>
<%
	'
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Call Debug()
Dim SITENAME
Dim PROVIDERNAME
Dim PHOTOGRAPH
Dim ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3
Dim TELEPHONE
Dim FAX
Dim EMAIL
Dim CLIENTCONTACT
Dim OPENINGHOURS
Dim OPENINGTIMES
Dim SITEID

ADDRESS = ""

    SITEID = Request("SITEID")
    If SITEID = "" Or IsNull(SITEID) Then SITEID = -1 End If

  	SQL = "SELECT SITENAME, PROVIDERNAME, PHOTOGRAPH, S.ADDRESSLINE1, S.ADDRESSLINE2, S.ADDRESSLINE3, S.ADDRESSLINE4, S.TELEPHONE, S.FAX, S.EMAIL, S.CLIENTCONTACT, S.OPENINGHOURS, S.OPENINGTIMES " &_ 
  	        "FROM TBL_MD_PROVIDER P " &_
	        "INNER JOIN TBL_MD_PROVIDER_DETAIL PD ON p.PROVIDERID = PD.PROVIDERID " &_
	        "INNER JOIN TBL_MD_PROVIDER_SITE S ON p.PROVIDERID = p.PROVIDERID " &_
	        "WHERE S.SITEID = " & SITEID

	Call OpenDB()
	Call OpenRs(rsClient, SQL)
		If NOT (rsClient.EOF) Then
			SITENAME	= rsClient("SITENAME")
			PROVIDERNAME	= rsClient("PROVIDERNAME")
			PHOTOGRAPH = rsClient("PHOTOGRAPH")
			ADDRESSLINE1 = rsClient("ADDRESSLINE1")
			ADDRESSLINE2 = rsClient("ADDRESSLINE2")
			ADDRESSLINE3 = rsClient("ADDRESSLINE3")					
			TELEPHONE = rsClient("TELEPHONE")
			FAX = rsClient("FAX")
			EMAIL = rsClient("EMAIL")
			CLIENTCONTACT = rsClient("CLIENTCONTACT")
			OPENINGHOURS = rsClient("OPENINGHOURS")
			OPENINGTIMES = rsClient("OPENINGTIMES")

			If Isnull(EMAIL) or EMAIL = "" Then EMAIL = NULL Else EMAIL = "<a href=""mailto:"&EMAIL&""" title=""email:"&PROVIDERNAME&"," &SITENAME& """/>"&EMAIL&"</a>" End If
			If ADDRESSLINE1 <> "" Then ADDRESS = ADDRESSLINE1 End If
			If ADDRESSLINE2 <> "" Then ADDRESS = ADDRESS & ", " & ADDRESSLINE2 End If
			If ADDRESSLINE2 <> "" Then ADDRESS = ADDRESS & ", " & ADDRESSLINE3  End If

		End If
	Call CloseRS(rsClient)
	Call CloseDB()
%>
<div style="float:left; border:solid 0px #cc0000; padding:1em; width:100%;">
    <div style="float:left; text-align:left; margin-left:10px; width:48%;">
         <div style="float:left;"> 
            <% If IsNull(PHOTOGRAPH) Then %>
                <img src="/images/tmpsite.gif" alt="" /> 
            <% Else %>                  
                <img src="RetrieveImg.asp?SiteID=<%=SITEID%>" alt="" />
            <% End If %>
            </div>
        <div style="float:left; padding-left:1em">
        <%
        rw "<b>" & PROVIDERNAME & "</b>" & "<br/><br/>"
        rw "<div style=""padding-bottom:1em"">" & SITENAME & "</div>"
        rw "<div style=""padding-bottom:1em"">" & ADDRESS & "</div>"
        rw "<div style=""padding-bottom:1em"">Telephone: " & TELEPHONE & "</div>"
        rw "<div style=""padding-bottom:1em"">Fax: " & TELEPHONE & "</div>"
        rw "<div style=""padding-bottom:1em"">Email: " & EMAIL & "</div>"
        rw "<div style=""padding-bottom:1em"">Contact Name: " & CLIENTCONTACT & "</div>"
        rw "<div style=""padding-bottom:1em"">Opening Hours: " & OPENINGHOURS & "</div>"
        %>
        </div>
    </div>
    <div style="float:right; text-align:left; width:48%;">
    <input style="margin-bottom:1em; cursor:pointer" class="submit" type="button" name="BtnPrint" id="BtnPrint" value=" Print " title="Submit Button : Print these details" style="width:auto" onclick="window.print()"/> <br />
    <input style="margin-bottom:1em; cursor:pointer" class="submit" type="button" name="BtnEmail" id="BtnEmail" value=" Email " title="Submit Button : Email these details to a specified email address" style="width:auto" onclick="alert('Email Functionality')"/> <br />
    <% 
        If ( Session("svCustomerID") = "" OR IsNull(Session("svCustomerID")) ) Then 
    %>
    <input style="margin-bottom:1em; cursor:pointer" class="submit" type="button" name="BtnRegister" id="BtnRegister" value=" Register " title="Submit Button : Register with <%=SITE_NAME%> " style="width:auto" onclick="window.location.href='/register.asp'"/> <br /> 
    <%
        End If
    %>
    </div>
    <div style="clear:both">
        <div class="row" style="padding-top:1em;">
            <span class="label">
                <input class="submit" type="button" name="Back_3" id="Back_3" value=" Back " title="Submit Button : Back" style="width:auto" onclick="LoadStep(3,'errordiv')"/>
            </span>
        </div>
    </div>
</div>
<input type="hidden" name="checkbox" value="<%=Request("checkbox")%>" />
<input type="hidden" name="Priority1" id="Priority1" value ="<%=Request("Priority1")%>" />
<input type="hidden" name="Priority2" id="Priority2" value ="<%=Request("Priority2")%>" />
<input type="hidden" name="Priority3" id="Priority3" value ="<%=Request("Priority3")%>" />
<input type="hidden" name="Stage" id="Stage" value="3" style="width:auto" />