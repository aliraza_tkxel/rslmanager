<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'Call Debug()

Dim My_String
Dim My_Array
    My_String=Request("checkbox")
    My_Array=split(My_String,",")

    Checkbox = Request("checkbox")
    Session("Checkbox") = Request("checkbox")

    IsNewSearch = 0

    If ( (Session("pcode_result") <> Request("txt_POSTCODE")) OR (Checkbox <> Session("Chk")) ) Then
        IsNewSearch = 1
    End If

    If Request("txt_POSTCODE") = "" Then
        Session("pcode") = null
    Else
        Session("pcode") = Request("txt_POSTCODE")
    End If

    isform = "g"

    For i=0 to UBound(My_Array)
        SessionNumber = "svChk"+CStr(i+1)
	    Session(SessionNumber) = CInt(My_Array(i))
    Next

    ReDim Preserve ArrayID(UBound(My_Array))
    ReDim Preserve ArrayDescription(UBound(My_Array))

    For i=0 to UBound(ArrayID)
       ArrayID(i) = CInt(ArrayID(i))
       'rw "Barrier Item" & ArrayID(i) & "<br />"
    Next

    Dim ArrayIDValues
        ArrayIDValues = ""
    SQL = "SELECT ID, DESCRIPTION FROM ASSESSMENT_OPTIONS WHERE ACTIVE = 1 AND ID IN("&Checkbox&")"
    Call OpenDB()
    Call OpenRs (rsPostCode,SQL)
        If NOT rsPostCode.EOF Then
        i = 0
        rw "From the " & UBound(My_Array)+1 & " area(s) you have selected, please number them in order of priority:"
            while not rsPostCode.eof
                ArrayID(i) = rsPostCode("ID")
                ArrayIDValues = ArrayIDValues & ArrayID(i)
                ArrayDescription(i) = rsPostCode("DESCRIPTION")
                'rw "ASSESSMENT_" & ArrayID(i) & "<br />"
                'rw CInt(Session("svPriortity1"))
              %>              
                <div class="row" style="padding-top:1em; padding-left:1em">
                    <span class="formw" style="width:auto">
                    <!-- 
                    1<%=Session("svPriortity1") %>, <%=ArrayID(i) %><br />
                    2<%=Session("svPriortity2") %>, <%=ArrayID(i) %><br />
                    3<%=Session("svPriortity2") %>, <%=ArrayID(i) %><br />
                    -->
                        <select name="ASSESSMENT_<%=ArrayID(i)%>" id="ASSESSMENT_<%=ArrayID(i)%>" style="width:auto" onchange="blar(<%=ArrayID(i)%>,this.value)">
                            <option value=""> </option>
                                <%
                                For j=1 to UBound(My_Array)+1
                                    PriortityNumber = "svPriortity"+CStr(j)
                                    rw "<option value="""&j&""""
                                    If CInt(Session(PriortityNumber)) = CInt(ArrayID(i)) Then 
                                        rw "selected=""selected"">"
                                    Else
                                        rw ">"
                                    End If
                                    rw "" & j & "</option>"
                                Next
                                %>
                        </select>
                    </span>
                    <span class="label">
                        <%=ArrayDescription(i)%>
                    </span>
                </div>
                <%
          	    rsPostCode.MoveNext()
          	    i = i + 1
            Wend
        Else
            '
        End If
    Call CloseRs(rsPostCode)
    Call CloseDB()
%>
<div class="row" style="padding-top:1em;">
    <span class="label"> 
        <input class="submit" type="button" name="Back_1" id="Back_1" value=" Back " title="Submit Button : Back" style="width:auto" onclick="LoadStepAlt(1,'errordiv')"/>
    </span>
    <span class="formw">
        <input class="submit" type="button" name="Next_2" id="Next_2" value=" Next " title="Submit Button : Next" style="width:auto" onclick="CustomSelect(document.getElementById('checkbox').value)"/><%'=ArrayIDValues %>
    </span>
</div>
<input type="hidden" name="checkbox" id="checkbox" value="<%=Request("checkbox")%>" />
<%
For k=0 to UBound(My_Array)+1
SessionNumber = "svP"+CStr(k)
%>
<!-- <br />Priority<%=k%> //-->
<input type="hidden" name="Priority<%=k%>" id="Priority<%=k%>" value="<%=Session(SessionNumber)%>" />
<%
Next
%>
txt_POSTCODE : <input type="hidden" name="txt_POSTCODE" id="txt_POSTCODE" value="<%=Session("pcode")%>" /><br />
Stage : <input type="hidden" name="Stage" id="Stage" value="2" style="width:auto" /><br />
hid_isForm : <input type="hidden" name="hid_isForm" value="<%=isform%>" /><br />
hid_IsNewSearch : <input type="hidden" name="hid_IsNewSearch" value="<%=IsNewSearch%>" /><br />
<%=Session("searchid") %>