﻿<%@LANGUAGE="VBSCRIPT" %>
<!-- #include virtual="/includes/ssl.asp" -->
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
'-- LOGIN CHECK

    Dim isLoggedIn
        isLoggedIn = True
    Dim LOGINID
        LOGINID = nulltest(Session("svLOGINID"))

    If (NullNestTF(LOGINID) = True) Then
        isLoggedIn = False
    Else
        If NOT IsNumeric(LOGINID) Then
            isLoggedIn = False
        End If
    End If

    If isLoggedIn = False Then
        Response.Redirect("/CRMS/index.asp?Session=0")
    End If

'-- LOGIN CHECK
%>
<%
Dim TimeStamp
    TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

    Dim PAGE_TITLE
    Dim PAGE_CONTENT
	Dim PAGE_DESCRIPTION
	Dim PAGE_KEYWORDS

    Dim GP_curPath
		GP_curPath = Request.ServerVariables("SCRIPT_NAME")

	 Dim regEx, Match, Matches
 	 Set regEx = New RegExp
	  	 regEx.Global = True
	  	 regEx.IgnoreCase = True
	   	 regEx.Pattern = "([a-zA-Z]:(\\w+)*\\[a-zA-Z0_9]+)?.asp"
  			gGP_curPath = Trim(regEx.Replace(GP_curPath, ""))
		regEx.Pattern = "\/index|\!|\.|\?|\;|\,|\:|/"
 			GP_curPath = Trim(regEx.Replace(gGP_curPath, ""))

	Call PageContent(GP_curPath)
%>
<%
Dim strIncludeFile
	strIncludeFile = ""&gGP_curPath&".asp"
%>
<%

Dim Stage
    Stage = Request("Stage")
    If Stage = "" Then Stage = 1 End If

    Select Case Stage
        Case 1
        StageText = "Select your Priorities"
        Case 2
        StageText = "Prioritise your Selections"
        Case 3
        StageText = "Our Recommendations"
    End Select
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=SITE_NAME%> : <%=PAGE_TITLE%></title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="Title" content="<%=nextstep_name%>"/>
<meta name="Description" content="<%=PAGE_DESCRIPTION%>"/>
<meta name="Keywords" content="<%=PAGE_KEYWORDS%>"/>
<meta name="Author" content="<%=nextstep_name%>"/>
<meta http-equiv="EXPIRES" content="+30 days"/>
<meta http-equiv="content-LANGUAGE" content="English"/>
<meta name="ROBOTS" content="index,follow"/>
<meta name="REVISIT-AFTER" content="15 days"/>
<meta name="ROBOTS" content="ALL"/>
<meta name="DSTRIBUTION" content="global"/>
<link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
<link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />

<style type="text/css">
<!--

.goinghigh tr:hover
{
	background-color:#cc0000;
	color:white;
}

.goinghigh tr:hover a
{
	background-color:#cc0000;
	color:white;
}

.goinghigh a
{
	color:#cc0000;
}


table a
{
	color:#950000;
	text-decoration:none;
}

table a:link {}

table a:visited
{
	font-weight:normal;
	color:#666;
}

table a:hover
{
	/*border-bottom: 1px dashed #bbb;*/
}

/* =head =foot */

thead th
{
	background:#cc0000;
	padding-top:10px;
	padding-left:10px;
	padding-bottom:10px;
	text-align:left;
	color:#fff;
	font-size: 12px;
	border-bottom:1px solid white;
}

thead td
{
	background:#cc0000;
	text-align:left;
	color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	line-height: 2.15em;
	font-weight:bold;
	border-bottom:1px solid white;
}

tfoot th, tfoot td
{
	background:#ffffff;
	color:#cc0000;
	border-top:1px dotted #cc0000;
}

tfoot td
{
	text-align:right
}

/* =body */

tbody th, tbody td
{
	border-top:1px dotted #cc0000;
	padding-left:10px;
	padding-top:5px; padding-bottom:5px;
}

tbody th
{
	white-space: nowrap;
}

tbody th a
{
	color:#333;
}

/*
tbody tr:hover
{
	background:gray;
	color:#ffffff;
	cursor:pointer;
}
*/

tbody tr.red td
{
	background:#ffffff;
	color:#003366;
	cursor:auto;
	height:25px;
}
tbody tr.red:hover
{
	background:#ffffff;
	color:#000000;
	cursor:auto;
}

tbody tr.empty:hover
{
	background:#ffffff;
	color:#ffffff;
	cursor:auto;
}

td.whiteout
{
    background:#ffffff
}

-->
</style>

<style type="text/css" media="screen"> 
 
.black_overlay
{
	display: none;
	position: absolute;
	top:0px; left:0px;
	width: 100%; height: 100%;
	background-color: #BDBDBD;
	z-index:1001;
	overflow: hidden;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
 
.white_content
{
	display: none;
	position: absolute;
	top: 25%; left: 30%;
	width: 40%;	height: 195px;
	padding: 5px;
	border: 1px solid #cc0000;
	background-color: white;
	z-index:1002;
	overflow: hidden;
}
 
</style> 

<script language="javascript" type="text/javascript" src="/ClientScripting/styleswitcher.js"></script>
<script language="javascript" type="text/javascript" src="/ClientScripting/stylestorage.js"></script>
<script language="javascript" type="text/javascript" src="/ClientScripting/cssForm.js"></script>
<script language="javascript" type="text/javascript" src="/ClientScripting/formValidation.js"></script>

<script language="javascript" type="text/javascript">


function Cancel()
{
    LightBox();
    document.getElementById("light").style.height = "50px"
    document.getElementById("light_response").innerHTML = ""
    document.getElementById("light_error").innerHTML = ""
}


function Close()
{
   Cancel()
}


function LightBox()
{
    var thearray= new Array("light","fade");
    for(i=0; i<thearray.length; i++)
    {
        toggle(thearray[i])
    }
}


function toggle(id)
{
	var state = document.getElementById(id).style.display;
	if (state == 'block')
	{
		document.getElementById(id).style.display = 'none';
	} 
	else
	{
		document.getElementById(id).style.display = 'block';
	}
}

function Disclaimer()
{
    LightBox();
    setTimeout((function() {BuildXMLReport('Disclaimer.asp','light_response','light_error',50,128)}), 1000);
}


</script>
<script language="javascript" type="text/javascript">
<!--//--><![CDATA[//><!--
navHover = function() {
	var lis = document.getElementById("navmenu").getElementsByTagName("LI");
	for (var i=0; i<lis.length; i++) {
		lis[i].onmouseover=function() {
			this.className+=" iehover";
		}
		lis[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" iehover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", navHover);
//--><!]]>
</script>

<script language="javascript" type="text/javascript">

var xmlhttp=false;
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}


/*
function buildQueryString2(theFormName) {
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
      qs+=(qs=='')?'?':'&'
      qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
      }
    }
  return qs
}
*/

function buildQueryString(theFormName) {
  var currentTime = new Date()
  theForm = document.forms[theFormName];
  var qs = ''
  for (e=0;e<theForm.elements.length;e++) {
    if (theForm.elements[e].name!='') {
	  if (theForm.elements[e].type=='checkbox') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  if (theForm.elements[e].type=='radio') {
        if (theForm.elements[e].checked) {
		  qs+=(qs=='')?'?':'&'
		  qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
		  }
		}
	  else {
	        //qs+=(qs=='')?'?':'&'
            //qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            
            if ((theForm.elements[e].type!='checkbox') && (theForm.elements[e].type!='radio'))
            {
                qs+=(qs=='')?'?':'&'
                qs+=theForm.elements[e].name+'='+escape(theForm.elements[e].value)
            }
	    }
      }
    }
    qs+=(qs=='')?'?':'&' + 'dt='+currentTime
  return qs
}


function checkbox_checker()
{
// set var checkbox_choices to zero
var checkbox_choices = 0;
// Loop from zero to the one minus the number of checkbox button selections
for (counter = 0; counter < document.Form1.checkbox.length; counter++)
{
// If a checkbox has been selected it will return true
// (If not it will return false)
if (document.Form1.checkbox[counter].checked)
{ checkbox_choices = checkbox_choices + 1; }
}
if (checkbox_choices > 3 )
{
// If there were more than three selections made display an alert box 
return (false);
}
if (checkbox_choices < 1)
{
// If there were less then selections made display an alert box 
return (false);
}
// If three were selected then display an alert box stating input was OK
return (true);
}

var FormFields = new Array()

function CheckPostcode(obj)
{

    FormFields = new Array("txt_POSTCODE|Postcode|POSTCODE|N");
    if (!checkForm('errordiv')) return;

    if (document.getElementById('Form1').elements['txt_POSTCODE'].value == "")
    {
        if (!(checkbox_checker()))
        {
            alert('Please select at least 1 item that you deem a priority but no more than 3')
        }
        else
        {
            LoadStep(2,'errordiv')
        }
    }
    else
    {
        xmlhttp.open("GET", "svr/check_pc.asp?txt_Postcode="+ document.getElementById('Form1').elements['txt_POSTCODE'].value,true);
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4)
          {
	          if (xmlhttp.status==200)
	          {
			        if (xmlhttp.responseText == "0")
			        {
				        alert("Please enter a valid Postcode for <%=SITE_NAME%>.");
				        targetError('txt_POSTCODE','red');
				        return false;
			        }
			        else
			        {
			            if (!(checkbox_checker()))
			            {
			                alert('Please select at least 1 item that you deem a priority but no more than 3')
			            }
			            else
			            {
			                LoadStep(2,'errordiv')
			            }
			        }
		        }
	        }
        }
    xmlhttp.send(null)
    }
}

function BuildXMLReport(str_url,elId,elED){

	//set the loading parameter
    document.getElementById(elED).innerHTML = "<div style='float:left; padding:10px;'>Please wait. Loading...</div><div style='float:right;padding:10px;'><img src='/images/XMLLoader.gif'></div>"
    document.getElementById(elED).className = "no_error"
	//call xmlhttp functions
	var strURL = str_url
	xmlhttp.open("GET",strURL,true);
 	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState == 4) {
			   strResponse = xmlhttp.responseText;
			   switch (xmlhttp.status) {
					   // Page-not-found error
					   case 404:
							   alert("Error: Not Found. The requested URL '"  +
									   str_url + "' could not be found.");
							   document.getElementById(elED).innerHTML = ""
							   break;
					   // Display results in a full window for server-side errors
					   case 500:
							   alert("Error: Server Error. An unexpected errror 500 has occurred.");
							   document.getElementById(elED).innerHTML = strResponse
							   break;
					   default:
							   // Call JS alert for custom error or debug messages
							   if (strResponse.indexOf('Error:') > -1 || 
									   strResponse.indexOf('Debug:') > -1) {
									   alert("Error: A JavaScript Error has been encountered. " + strResponse);
									   document.getElementById(elED).innerHTML = ""
							   }
							   // Call the desired result function
							   else {
									   document.getElementById(elId).innerHTML = strResponse
									   document.getElementById(elED).innerHTML = ""
									   document.getElementById(elED).className = ""
									   //try {
									//	   alert('hum!')
									//	   }
									//	catch(e){
									//		}
							   }
							   break;
			  }
	     }
 	}
    xmlhttp.send(null)
}


function go(url,elId,pId)
{
    BuildXMLReport('rje.asp'+ url + '&siteid='+pId,'OrgDetails','errordiv');
}

function CustomSelect(aItem1)
{
    var isSelectValidationError = false
    var myString=aItem1
    var mySplitResult = myString.split(", ")
    var SelectedValues = ""
    for(i = 0; i < mySplitResult.length; i++)
    {
        SelectedValues += document.getElementById('ASSESSMENT_'+mySplitResult[i]).value + " "
	    if (document.getElementById('ASSESSMENT_'+mySplitResult[i]).value == "")
	    {
	        isSelectValidationError = true
	    }
    }
    if (isSelectValidationError == true)
    {
        alert("Please select a Priority for the chosen options")
    }
    else
    {
       var temp = SelectedValues
       var array2 = temp.split(" ") ;
       var array1= new Array;
       for(var i = 0; i < array2.length; i++)
       {
           var xx = true;
           var ArrayVal = array2[i];
           for(var j = i+1; j < array2.length; j++)
           {
             if(array2[j] == ArrayVal) 
              xx = false;
              if (xx == false)
              {
                isSelectValidationError = true
                alert("Please ensure that all priorities are ordered with out duplication");
                break;
              }
           }
       	}
        if (isSelectValidationError == false)
        {
            AllSelections(aItem1)
        }
    }
}

function AllSelections(SelectedValues)
{
    var temp = SelectedValues
    var mySplitResult = temp.split(", ")
    document.getElementById('PriorityList').value = ""
    for(i = 0; i < mySplitResult.length; i++)
    {
        //document.getElementById('Priority'+(i+1)).value = (i+1)+'-'+mySplitResult[i];        
        //document.getElementById('Priority'+(i+1)).value = document.getElementById('ASSESSMENT_'+mySplitResult[i]).selectedIndex+'-'+ mySplitResult[i]
        document.getElementById('PriorityList').value += document.getElementById('ASSESSMENT_'+mySplitResult[i]).selectedIndex+'-'+ mySplitResult[i] + '|'
    }
    document.getElementById('PriorityList').value = document.getElementById('PriorityList').value.slice(0, -1)

    var temp2 = document.getElementById('PriorityList').value
    var mySplitResult2 = temp2.split("|")
    var mySplitResult3
    for(k = 0; k < mySplitResult2.length; k++)
    {
        mySplitResult3 = mySplitResult2[k].split("-")
        document.getElementById('Priority'+mySplitResult3[0]).value = mySplitResult3[0]+'-'+mySplitResult3[1]    
    }

    //SavePriorities()

    LoadStep(3,'errordiv')
    
//var lis = document.getElementsByTagName("SELECT");
	//for (var i=0; i<lis.length; i++)
	//{
	//alert(i)
	//}
}

function LoadStepAlt(StepNo,elED)
{
    switch (StepNo)
    {
        case 1 : url = "SelfAssessment-S1.asp"; cvb = "Step 1 : Select your Priorities"; break;
        case 2 : url = "SelfAssessment-S2.asp"; cvb = "Step 2 : Prioritise your Selections"; break;
        case 3 : url = "SelfAssessment-S3.asp"; cvb = "Our Recommendations"; break;
    }
    BuildXMLReport(url+ buildQueryString('Form1'),'OrgDetails','errordiv');
    document.getElementById("ffg").innerHTML = cvb;
}


function LoadStep(StepNo,elED)
{
    if (StepNo == 1)
    {
        //document.getElementById("Priority1").value == ""; document.getElementById("Priority2").value == ""; document.getElementById("Priority3").value == "";
    }
    switch (StepNo)
    { 
        case 1 : url = "SelfAssessment-S1.asp"; cvb = "Step 1 : Select your Priorities"; break;
        case 2 : url = "SelfAssessment-S2.asp"; cvb = "Step 2 : Prioritise your Selections"; break;
        case 3 : url = "SelfAssessment-S3.asp"; cvb = "Our Recommendations"; break;
    }
    BuildXMLReport(url+ buildQueryString('Form1'),'OrgDetails','errordiv');
    document.getElementById("ffg").innerHTML = cvb;
}


function blar(val,priority)
{
    if (priority != "")
    {
        document.getElementById('Priority'+priority).value == "";
        document.getElementById('Priority'+priority).value = priority+'-'+val;
    }
}


function SaveRecomendations()
{
    var chk = document.Form1.CheckboxSite
    for (i=0;i<chk.length;i++)
    chk[i].checked = true;
    BuildXMLReport('svr/Recommendations_save.asp'+ buildQueryString('Form1'),'OrgDetails','errordiv');
}


function RemoveSite(ID)
{
    elDelete('SITEID');

    var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "SITEID");
        input.setAttribute("id", "SITEID");
        input.setAttribute("value", ID);
        document.getElementById("Form1").appendChild(input);

    BuildXMLReport('svr/RemoveSite.asp'+ buildQueryString('Form1'),'OrgDetails','errordiv');
}


function elDelete(elementId)
{
  var label=document.getElementById(elementId);	
  if (label != null)
  document.getElementById("Form1").removeChild(label);
}


function BUILDME()
{
    LoadStep(3,'errordiv')
}

var plus = new Image();
    plus.src = "/images/plusImage.gif";
var minus = new Image();
    minus.src = "/images/minusImage.gif";

function toggleImg(imgName)
{
	document.images[imgName].src = (document.images[imgName].src==plus.src) ? minus.src:plus.src;
	return false;
}

function Toggle(refByName)
{
    if (document.getElementById("Div_"+refByName).style.display == "block")
    {
	    document.getElementById("Div_"+refByName).style.display = "none";
	}
    else
    {
	    document.getElementById("Div_"+refByName).style.display = "block";
	}
	toggleImg('IMG_'+refByName)
}

</script>
</head>
<body lang="en" onload="LoadStep(1,'errordiv'); Disclaimer()">
<script type="text/javascript" src="/ClientScripting/wz_tooltip.js"></script>
<div id="fade" class="black_overlay"></div>
<div id="light" class="white_content">
    <div id="light_error" style="float:left;width:100%;"></div>
    <div id="light_response" style="float:left;width:100%;"></div>
</div>
<form method="post" id="Form1" name="Form1" action="<%=strIncludeFile%>"> <!-- onsubmit="return checkMyForm(this)" -->
<p id="access"><a href="#content" accesskey="g">Go to page content</a></p>
<!-- Start Container //-->
<div id="group" style="border: solid 0px red">
  <!-- #include virtual="/includes/Header.asp" -->
	<div id="M_Container">
		<!-- Start Page Title //-->
		<div id="MiddleMC">
		  <h1><%=PAGE_TITLE%> - <span id="ffg">Step <%=Stage%> : <%=StageText %></span></h1>
		</div>
		<!-- End Page Title //-->
  	</div>
	<!-- Start Left -->
	<div id="navigation">
		<!-- Start Navigation -->
 		<div id="primary-nav">
			<% Call listCatDIR(404, "navmenu" , "CRMS") %>
		</div>
		<!-- End Navigation -->
	<!-- End Left -->
	<!-- Start Content Container -->
	<div id="content" style="border: solid 0px red">
		<!-- Start Content -->
		<div id="maincontent" style="margin-left:10px; background-attachment: fixed; background: #FFFFFF url('/images/im_dashedline.gif') repeat-x; background-position: left top;">
			<div style="margin-left:-10px;">
			    <%=PAGE_CONTENT%>
			</div>
			<br />
			<div style="margin-left:-10px;">
			    <div id="nsform" style="float:left; width:96%; border:solid 1px #cc0000; padding-top:1em; padding-bottom:1em; padding-right:1em; padding-left:1em">
                    <!-- ERRORS/PAGE LOAD -->
                    <div id="errordiv" style="float:left;width:100%;" <%=ErrorClass%>>
                        <%=passcheck_text%>
                    </div>
                    <!-- DYNAMIC PAGE LOAD -->
                    <div id="OrgDetails" style="float:left;width:100%;"></div>
                </div>
			</div>
	 	</div>
	 	<!-- End Content -->
	</div>
	<!-- End Content Container -->
	<!-- Start Navigation //-->
	 <div id="footer">
		<ul>
			<% Call listCat2(41, "ulFooter") %>
		</ul>
	</div>
	<!-- End Navigation -->
</div>
<!-- End Container //-->
<img src="/js/img/FVER.gif" width="10px" height="10px" name="FVER_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVEB.gif" width="10px" height="10px" name="FVEB_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVS.gif" width="10px" height="10px" name="FVS_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVW.gif" width="10px" height="10px" name="FVW_Image" alt="" style="visibility:hidden" />
<img src="/js/img/FVTG.gif" width="10px" height="10px" name="FVTG_Image" alt="" style="visibility:hidden" />
<input type="hidden" name="PriorityList" id="PriorityList" value="" />
</form>
</body>
</html>