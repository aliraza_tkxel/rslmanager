<%@LANGUAGE="VBSCRIPT" %>
<%
	Response.Buffer = false
	Response.Expires=-1000
	Response.CacheControl="no-cache"
%>
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
Customerid = NullTest(Request("CID"))
If Customerid = "" Then Customerid = -1 End If

POSTCODE = Session("pcode")
'If POSTCODE = "" Then
'        SQL = "SELECT POSTCODE FROM CUSTOMERDETAILS WHERE CUSTOMERID = " & Customerid & ""
'        Call OpenDB()
'        Call OpenRs (rsPostCode,SQL)
'            If NOT rsPostCode.EOF Then
'	            POSTCODE = rsPostCode(0)
'            Else
'                POSTCODE = Session("pcode")
'            End If
'       Call CloseRs(rsPostCode)
'        Call CloseDB()
'End Ifs


Dim My_String
Dim My_Array
    My_String = Request("checkbox")

If My_String <> "" Then
    My_Array = split(My_String,",")
    ReDim Preserve My_Array(3)
    'rw "h-1-h"
Else
    'rw "h-2-h"
    ' GET ITEM FROM DB
        Set objConn	= Server.CreateObject("ADODB.Connection")
    	Set objCmd	= Server.CreateObject("ADODB.Command")
    		objConn.Open DSN_CONNECTION_STRING
            With objCmd
                .ActiveConnection = objConn 'You can also just specify a connection string here
                .CommandText = "CRMS_CURRENTPRIORITIES_DELIMITED"
                .CommandType = adCmdStoredProc 'Requires the adovbs.inc file or typelib meta tag
                .CommandTimeout = 0
                'Add Input Parameters
                .Parameters.Append .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
                .Parameters.Append .createparameter("@CUSTOMERID", adInteger, adParamInput, 0, CUSTOMERID)
               'Execute the function
			    '.execute ,,adexecutenorecords
    				Set rsBU = .Execute
					If NOT rsBU.EOF Then
					'rw "h-3-h"
					    If rsBU(0) <> "0" Then
					        'rw "h-4-h"
					        My_Array = split(rsBU(0),",")
					        My_String = "Array"
					    Else
					        If Session("Checkbox") <> "" Then
					            My_String = Session("Checkbox")
					            My_Array = split(My_String,", ")
					        Else
                                My_String = ""
                            End If
                        End If
					Else
					    'rw "h-6-h"
					    ReDim Preserve My_Array(3)
                        My_Array(0) = CInt(Session("svChk1"))
                        My_Array(1) = CInt(Session("svChk2"))
                        My_Array(2) = CInt(Session("svChk3"))
					    'Item1 = CInt(Session("svChk1"))
                        'Item2 = CInt(Session("svChk2"))
                        'Item3 = CInt(Session("svChk3")) 
					End If
 		    End With
	    objConn.Close
    Set objConn = Nothing 
End If
%>
<div id="OrgDetails" style="float:left;width:100%;">
                    <fieldset>
		                <legend>Personal Assessment</legend>
<div id="STEP">
			                <div id="Frm" style="float:left; width:100%;">
			                    <p class="H4">
			                        Please enter your postcode and select up to 3 of the areas you feel are your priority for action at the moment:
			                    </p>
			                    <div id="_1" style="float:left; width:48%; padding-bottom:1em;">
                                    <div class="row">
                                        <span class="label"> <label for="txt_POSTCODE" accesskey="P"><span style="text-decoration:underline; cursor:pointer">P</span>ostcode:</label> </span>
                                        <span class="formw">
                                            <input style="width:auto" class="border-white" onblur="this.className='border-white'" onfocus="this.className='border-red'" type="text" size="8" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="8" value="<%=POSTCODE%>" tabindex="1" required="Y" validate="/^[A-Za-z]{1,2}\d{1,2}[A-Za-z]? \d[A-Za-z]{2}$/" validateMsg="Postcode" />
                                            <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" alt="" />
                                        </span>
                                   </div>
                                </div>

<div id="_2" style="float:left; width:100%; clear:left; border-top: 1px solid #cc0000; padding-top:1em;">
<%
set rsBooks = Server.CreateObject("ADODB.Recordset")
    rsBooks.ActiveConnection = DSN_CONNECTION_STRING
    rsBooks.Source = "SELECT ID, DESCRIPTION FROM ASSESSMENT_OPTIONS WHERE ACTIVE = 1"
    rsBooks.CursorType = 0
    rsBooks.CursorLocation = 2
    rsBooks.LockType = 3
    rsBooks.Open()
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<%
Dim numberColumns
Dim startrw
Dim endrw
Dim numrows
    startrw = 0
    endrw = Repeat1__index
    numberColumns = 2
    'numrows = 2
    'while((numrows <> 0) AND (Not rsBooks.EOF))
while Not rsBooks.EOF
	startrw = endrw + 1
	endrw = endrw + numberColumns
%>
<tr align="center" valign="top" class="" style="background-color:White; cursor:default">
<%
While ((startrw <= endrw) AND (Not rsBooks.EOF))
%>
		  <td> 
			<div class="row">
    <span class="formw" style="white-space:nowrap"> <label for="rdo_ASSESSMENT_<%=rsBooks("ID")%>"><%=rsBooks("DESCRIPTION")%>:</label> </span>
    <span class="label">
        <input
            style="width:auto"
            class="border-white"
            onblur="this.className='border-white'"
            onfocus="this.className='border-red'"
            type="checkbox"
            name="checkbox"
            value="<%=rsBooks("ID")%>" 
            <%
            If My_String <> "" Then
                For i=0 to UBound(My_Array) 
                    If CInt(My_Array(i)) = CInt(rsBooks("ID")) Then 
                        rw "checked"
                    End If
                Next
            End If
             %>
             />
    </span>
</div>
		  </td>
		  <%
	startrw = startrw + 1
	rsBooks.MoveNext()
	Wend
	%>
		</tr>
		<%
 numrows=numrows-1
 Wend
 %>
	  </table>
	  <br/>
	  <% If rsBooks.EOF And rsBooks.BOF Then %>
	       No self assessment priorities are avaiable for selection.
	  <% End If ' end rsBooks.EOF And rsBooks.BOF %>
</div> 
                                            <div class="row">
                                                <span class="label"> </span>
                                                <span class="formw">
                                                    <input class="submit" type="button" name="Next" value=" Next " title="Submit Button : Next" style="width:auto" onclick="CheckPostcode(this)"/>
                                                </span>
                                           </div>
                            </div>
</div>		                </fieldset>
<input type="hidden" name="Stage" id="Stage" value="<%=Stage%>" style="width:auto" />
<%
if My_String = "" then
    For k=1 to 3
        rw "<input type='hidden' name='Priority"&k&"' id='Priority"&k&"' value='' />"
    Next
else
    For k=1 to UBound(My_Array)
        SessionNumber = "svP"+CStr(k)
        rw "<input type='hidden' name='Priority"&k&"' id='Priority"&k&"' value='"&Session(SessionNumber)&"' />"
    Next
%>
<!-- <br />Priority<%=k%> //-->
<%
end if
%>
</div>