<div><h4><b>Please note:</b></h4></div>
<div style="border:solid 1px #cc0000; height:145px; padding:1em;">
    <fieldset>
        <legend>Adviser Alert:</legend>
         It is not possible to save the results of the clients self assessment when using this link. 
         You must first create a client record, and then carry out a self assessment to save the results.
         <div class="row">
            <span class="label" style="width:1%">&nbsp;</span>
            <span class="formw" style="width:99%">
                   <input class="submit" type="button" name="BtnCancel" id="BtnCancel" value=" Ok " title="Submit Button : Acknowledge Statement " tabindex="1" style="width:auto; float:right" onclick="Cancel()"/>
            </span>
        </div>
    </fieldset>
</div>