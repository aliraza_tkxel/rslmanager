<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252" LCID = 2057 %>
<%
    If Response.Buffer Then
    '   Response.Clear
        Response.Status = "500 Internal Server Error"
        Response.ContentType = "text/html"
        Response.Expires = 0
    End If
%>
<!-- #include virtual="/includes/ssl.asp" -->
<!-- #include virtual="/includes/globals.asp" -->
<!-- #include virtual="includes/adovbs.inc" -->
<!-- #include virtual="includes/functions/LibFunctions.asp" -->
<!-- #include virtual="includes/functions/PortalFunctions.asp" -->
<%
	'IF NO SESSION VARIABLE THEN TIMEOUT HAS EXPIRED. SO REDIRECT TO LOGOUT PAGE.
	'UserID_193 = Session("svUserID")
	'If (UserID_193 = "") Then
	'	'Response.Redirect "/index.asp?TIMEDOUT=1"
    'End If
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="page-enter" content="revealtrans(duration=0.000,transition=5)" />
<meta http-equiv="page-exit" content="revealtrans(duration=0.000,transition=5)" />
<title>ANP Error Page</title>
<link rel="stylesheet" href="/css/lsc.css" type="text/css" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body bgcolor="#FFFFFF">
<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
    <tr>
        <td height="100%" colspan="2" valign="top">
<%
'On Error Resume Next
'Referencing the error object
Set objError = Server.getLastError()
    strNumber = objError.AspCode
    strSource = objError.Category
    strPage = objError.File 
    strDesc = objError.Description
    strCode = Server.HTMLEncode(objError.Source)
    If strCode = "" Then strCode = "No code available"
    strLine = ObjError.Line
    strASPDesc = ObjError.ASPDescription
      
   'You get the entire context of the page that had the error.
   'Review the server variables to see if you would want to store more information.
    strRemoteAddr = Request.ServerVariables("REMOTE_ADDR")
    strRemoteHost = Request.ServerVariables("REMOTE_HOST")
    strLocalAddr = Request.ServerVariables("LOCAL_ADDR")
    IagType = "ANP <font color=""#cc0000"">PORTAL</font>"	

    UserName = ""
    UserPass = ""
    UserEmail = ""

Call OpenDB()
Call OpenRs(rsUserDetails,"SELECT * FROM CustomerID WHERE CustomerID = 1")
If (NOT rsUserDetails.EOF) Then
	UserName = rsUserDetails("LOGINUSER")
End If
Call CloseRs(rsUserDetails)
Call CloseDB()

Dim MailToUser, ErrorType
'    TheIP = Request.ServerVariables("REMOTE_ADDR")
'ErrorType = "INTERNAL DEVELOPER "
'Select Case TheIP
	'Case "194.129.129.95" MailToUser = "scott@reidmark.com"
	'Case "194.129.129.218" MailToUser = "zanfar.ali@reidmark.com"
	'Case "194.129.129.222" MailToUser = "paul@reidmark.com"
	'Case "194.129.129.28" MailToUser = "julian@reidmark.com"
	'Case "194.129.129.94" MailToUser = "rob@reidmark.com"
	'Case "194.129.129.167" MailToUser = "akhlaq@reidmark.com"	
	'Case "194.129.129.231" MailToUser = "jimmy@reidmark.com"		
	'Case Else 
		MailToUser = "robert.egan@reidmark.com"
		ErrorType = "REAL "
'End Select

	Dim iMsg
	Set iMsg = CreateObject("CDO.Message") 'calls CDO message COM object
    Dim iConf
	Set iConf = CreateObject("CDO.Configuration") 'calls CDO configuration COM object
	Dim Flds
	Set Flds = iConf.Fields
		Flds("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1 'tells cdo we're using the local smtp service
		Flds("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\inetpub\mailroot\pickup" 'verify that this path is correct
		Flds.Update 'updates CDO's configuration database
	Set iMsg.Configuration = iConf 'sets the configuration for the message
		iMsg.To = MailToUser
		iMsg.From = "error@reidmark.com"
		iMsg.Subject = "Portal Error"

	    HTML = ""
	    HTML = "<html><head><title>ANP PORTAL Error</title>" 
	    HTML = HTML & "<style><!--TD{border-top:1px dotted silver} .EmailFont {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-style: normal;line-height: normal;font-weight: normal;color: #000000;text-decoration: none;}--></style>"
	    HTML = HTML & "</head><body bgcolor='#ffffff' cellspacing='0' cellpadding='0' border='0' height='100%' bordercolor='#ffffff'>" 
	    HTML = HTML & "<div width='100%' style='background-color=#BDBDBD;padding:10px;text-align:right;border:1px solid black'><img src=""https://greatermanchester-anp.org.uk/images/Greater_Manchester_Advancement_Network.gif""></div><br>"
	    HTML = HTML &  "<table width='100%' cellspacing='0' cellpadding='3' border='0' style='border-collapse:collapse' class='EmailFont'>"
	    HTML = HTML &  "<tr><td colspan='2' style='font-size:14px;border-top:none'><b><u>Main Error Details</u></b></td></tr>"	
	    HTML = HTML &  "<tr><td width='160'><font color='blue'>System:</font></td><td>"& IagType &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>User Name & ID:</font></td><td>" & Session("FIRSTNAME") & " " & Session("LASTNAME") & ", ["& Session("USERID") &"]</td></tr>"		
	    HTML = HTML &  "<tr><td><font color='blue'>User Login Name:</font></td><td>"& UserName &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>User Email:</font></td><td>"& UserEmail &"</td></tr>"			
	    HTML = HTML &  "<tr><td><font color='blue'>Request Objects:</font></td><td>"& Session("MadError") &"</td></tr>"			
	    HTML = HTML &  "<tr><td><font color='blue'>Organisation / Type:</font></td><td>"& Session("ORGNAME") & " " & Session("TYPENAME") & "</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Date/Time:</font></td><td>"& now & "</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Error Number:</font></td><td>"& strNumber &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Source:</font></td><td>"& strSource &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>File:</font></td><td>"& strPage &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Querystring:</font></td><td>"& request.QueryString &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>POST Data:</font></td><td>"& request.Form &"</td></tr>"
	    HTML = HTML &  "<tr valign='top'><td><font color='blue'>Description:</font></td><td>" & strDesc & ". " & strASPDesc &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Code:</font></td><td>"& strcode &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Line:</font></td><td>"& strLine &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Remote Address:</font></td><td>"& strRemoteAddr &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Remote Host:</font></td><td>"& strRemoteHost &"</td></tr>"
	    HTML = HTML &  "<tr><td><font color='blue'>Local Address:</font></td><td>"& strLocalAddr &"</td></tr>"
	    HTML = HTML &  "</table>"
	    HTML = HTML & "<br/>"
	    HTML = HTML &  "<table width='100%' cellspacing='0' cellpadding='3' border='0' style='border-collapse:collapse' class='EmailFont'>"
	    HTML = HTML &  "<tr><td colspan='2' style='font-size:14px;border-top:none'><b><u>Extended Server Variable Information</u></b></td></tr>"		
	    HTML = HTML &  "<tr><td width='160'><b>Server Variable Name</b></td><td><b>Server Variable Value</b></td></tr>"		
	    For Each x In Request.ServerVariables
	      HTML = HTML &  "<tr><td><font color='blue'>" & x & ":</font></td><td>" & Request.ServerVariables(x) & "</td></tr>"
	    Next
	    HTML = HTML &  "</table>"	
	    HTML = HTML &  "</body></html>"
	    body = HTML

	iMsg.HTMLBody = body
	iMsg.Send 'commands CDO to send the message
	set iMsg = Nothing
	
	'rw HTML

   'Your basic ADO stuff.  This is generic code.  I would use a stored procedure to do this on SQL Server.
   set rsError = Server.CreateObject("ADODB.Recordset")
   set EConn = Server.CreateObject("ADODB.Connection")   
   'SQL Server Connection, make sure you put in your information!
    EConn.ConnectionString = DSN_CONNECTION_STRING
    EConn.open
    rsError.Open "G_ERRORMESSAGES", EConn, 2, 3, &H0002  
   'Adding Record
    rsError.AddNew
    'The datetime is set on the backend as a default value.
    rsError("er_number") = strNumber
    rsError("er_source") = strSource
    rsError("er_page") = strPage
    rsError("er_desc") = strDesc + ". " + strASPDesc
	'rsError("LUMP_CODE") = HTML
    rsError("er_code") = strCode
    rsError("er_line") = strLine
    rsError("er_remote_addr") = strRemoteAddr
    rsError("er_remote_host") = strRemotehost
    rsError("er_local_addr") = strLocalAddr
    rsError.Update   
   %>
<br/>
    <table width="90%" align="center" cellspacing="0" cellpadding="3" border="1" bordercolor="gray" style="border-collapse:collapse">
	  <tr>
      <td colspan="2" align="center" bgcolor="black" style='color:white'><b>An error has occured, if this problem persists 
      please contact ANP Portal Support on</b> <a href="mailto:robert.egan@reidmark.com" style='color:white'><b>robert.egan@reidmark.com</b></a></td>
      </tr>
      <tr> 
        <td width="200">Error Number:</td>
        <td><%=strNumber%></td>
      </tr>
      <tr> 
        <td width="200">Source:</td>
        <td><%=strSource%></td>
      </tr>
      <tr> 
        <td width="200">File:</td>
        <td><%=strPage%></td>
      </tr>
      <tr valign="top"> 
        <td width="200">Description:</td>
        <td><%=strDesc + ". " + strASPDesc%></td>
      </tr>
      <tr> 
        <td width="200">Code:</td>
        <td><%=strcode%></td>
      </tr>
      <tr> 
        <td width="200">Line:</td>
        <td><%=strLine%></td>
      </tr>
      <tr> 
        <td width="200">Remote Address:</td>
        <td><%=strRemoteAddr%></td>
      </tr>
      <tr> 
        <td width="200">Remote Host:</td>
        <td><%=strRemoteHost%></td>
      </tr>
      <tr> 
        <td width="200">Local Address:</td>
        <td><%=strLocalAddr%></td>
      </tr>

    </table>
</table>	
<%  
   'Kill them their objects
   set rsError = nothing
   set EConn = nothing
   set objError = nothing
   Response.Write Err.description
   Response.End
%>
</body>
</html>