<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="page-enter" content="revealtrans(duration=0.000,transition=5)" />
    <meta http-equiv="page-exit" content="revealtrans(duration=0.000,transition=5)" />
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <title>Google</title>
    <style type="text/css">
        html { height: 100% }
        body { height: 100%; margin: 0; padding: 0 }
        #map_canvas { height: 100% }
        #map
        {
	        height: 500px;
	        width: 500px;
        }
    </style>

    <script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="gmap.js" type="text/javascript"></script>

</head>
<body>
    <input type="text" id="postcode" size="10" />
    <input type="submit" value="Place Marker" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, placeMarkerAtPoint)" />
    <input type="submit" value="Center Map" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, setCenterToPoint)" />
    <input type="submit" value="Show Lat/Lng" onclick="javascript:usePointFromPostcode(document.getElementById('postcode').value, showPointLatLng)" />
    <div id="map">
    </div>
</body>
</html>
