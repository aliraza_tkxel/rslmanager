Imports Microsoft.VisualBasic
Imports System.Web.Mail

Public Class ProviderEmailNotifier

    Public Sub sendPCChangeNotification(ByRef po As Object, ByVal ps As Boolean)
        Dim provider As MDApplication.Provider
        Dim site As MDApplication.ProviderSite
        Dim cid As Integer
        If ps Then
            provider = DirectCast(po, MDApplication.Provider)
            cid = provider.getID
        Else
            site = DirectCast(po, MDApplication.ProviderSite)
            cid = site.getPID
        End If
        Dim userAdapter As New ProvidersMainTableAdapters.QRY_USER_PROVIDER_ADMINTableAdapter
        Dim userTable As ProvidersMain.QRY_USER_PROVIDER_ADMINDataTable = userAdapter.GetDataByPID(cid)
        For Each row As ProvidersMain.QRY_USER_PROVIDER_ADMINRow In userTable
            If row.Email = "rashid.khan@reidmark.com" Then
                Dim msg As New MailMessage
                msg.From = "support@reidmark.com"
                msg.To = row.Email
                msg.BodyFormat = MailFormat.Html
                msg.Subject = "Provider Address Modified"
                Dim body As String = "<html>"
                body += "Dear " + row.FirstName + ",<br /><br />"
                body += "During last amendment operation on Provider <b>" + row.ProviderName
                body += "</b> Following Address is modified.<br /><br />"
                body += "<table><tr><td>Organisation Name:</td><td><b>" + row.ProviderName + "</b></td></tr>"
                If ps Then
                    body += "<tr><td>Address:</td><td>" + provider.getAddress1 + "</td></tr>"
                    body += "<tr><td></td><td>" + provider.getAddress2 + "</td></tr>"
                    body += "<tr><td></td><td>" + provider.getAddress3 + "</td></tr>"
                    body += "<tr><td></td><td>" + provider.getAddress4 + "</td></tr>"
                    body += "<tr><td></td><td>" + provider.getPostcode + "</td></tr></table>"
                Else
                    body += "<tr><td>Site Address:</td><td>" + site.getAddress1 + "</td></tr>"
                    body += "<tr><td></td><td>" + site.getAddress2 + "</td></tr>"
                    body += "<tr><td></td><td>" + site.getAddress3 + "</td></tr>"
                    body += "<tr><td></td><td>" + site.getAddress4 + "</td></tr>"
                    body += "<tr><td></td><td>" + site.getPostcode + "</td></tr></table>"
                End If
                body += "<br /><br />Please contact us if you need any information about this message."
                body += "<br /><br />Best Regards,"
                body += "<br /><br />Reidmark Support"
                body += "</html>"
                msg.Body = body
                SmtpMail.Send(msg)
            End If
        Next
        'SmtpMail.Send("rashid@abc.com", "rashid.khan@reidmark.com", "Test", "Test Message")
    End Sub
End Class
