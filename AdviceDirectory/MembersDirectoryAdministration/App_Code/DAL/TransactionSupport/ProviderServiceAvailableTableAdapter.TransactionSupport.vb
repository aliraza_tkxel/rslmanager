Imports System.Data
Imports System.Data.SqlClient

Namespace ProvidersMainTableAdapters

	''' <summary>
	''' Adding support for transaction processing 
	''' </summary>
	''' <remarks></remarks>

	Partial Public Class ProviderServiceAvailableTableAdapter

		''' <summary>
		''' Local SqlTransaction referance
		''' </summary>
		''' <remarks></remarks>

		Private _transaction As SqlTransaction

		''' <summary>
		''' Property enabling access to Local SqlTransaction
		''' </summary>
		''' <value></value>
		''' <returns>
		''' SqlTransaction object
		''' </returns>
		''' <remarks></remarks>
		Private Property Transaction() As SqlTransaction
			Get
				Return Me._transaction
			End Get
			Set(ByVal Value As SqlTransaction)
				Me._transaction = Value
			End Set
		End Property

		''' <summary>
		''' Method implementing Initialising a Transaction
		''' on current TableAdapter
		''' </summary>
		''' <remarks></remarks>

		Public Sub BeginTransaction()
			' Open the connection, if needed
			If Me.Connection.State <> ConnectionState.Open Then
				Me.Connection.Open()
			End If

			' Create the transaction and assign it to the Transaction property
			Me.Transaction = Me.Connection.BeginTransaction()

			' Attach the transaction to the Adapters
			For Each command As SqlCommand In Me.CommandCollection
				command.Transaction = Me.Transaction
			Next

			Me.Adapter.InsertCommand.Transaction = Me.Transaction
			Me.Adapter.UpdateCommand.Transaction = Me.Transaction
			Me.Adapter.DeleteCommand.Transaction = Me.Transaction
		End Sub

		''' <summary>
		''' Method implementing Transaction completion for
		''' TableAdapter
		''' </summary>
		''' <remarks></remarks>

		Public Sub CommitTransaction()
			' Commit the transaction
			Me.Transaction.Commit()

			' Close the connection
			Me.Connection.Close()
		End Sub

		''' <summary>
		''' Method implementing Transaction rollback
		''' procedure for TableAdapter
		''' </summary>
		''' <remarks></remarks>

		Public Sub RollbackTransaction()
			' Rollback the transaction
			Me.Transaction.Rollback()

			' Close the connection
			Me.Connection.Close()
		End Sub

	End Class
End Namespace

