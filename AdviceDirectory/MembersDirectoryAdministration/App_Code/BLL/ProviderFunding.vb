Imports Microsoft.VisualBasic


Namespace MDApplication

	''' <summary>
	''' ProviderFunding class hold information about funding source
	''' details of provider.
	''' </summary>
	''' <remarks></remarks>

	Public Class ProviderFunding

		''' <summary>
		''' funding source
		''' </summary>
		''' <remarks></remarks>

		Private source As Integer

		''' <summary>
		''' expiry date of funding
		''' </summary>
		''' <remarks></remarks>

		Private expire As Date

		''' <summary>
		''' setting source id and expiry date through constructor
		''' </summary>
		''' <param name="s">Integer</param>
		''' <param name="e">Date</param>
		''' <remarks></remarks>

		Public Sub New(ByVal s As Integer, ByVal e As Date)
			Me.source = s
			Me.expire = e
		End Sub

		''' <summary>
		''' getting source id
		''' </summary>
		''' <returns>Integer</returns>
		''' <remarks></remarks>

		Public Function getSource() As Integer
			Return Me.source
		End Function

		''' <summary>
		''' getting expiry date
		''' </summary>
		''' <returns>Date</returns>
		''' <remarks></remarks>

		Public Function getExpire() As Date
			Return Me.expire
		End Function

	End Class

End Namespace
