Imports Microsoft.VisualBasic
Imports ProvidersMainTableAdapters

Namespace MDApplication

	''' <summary>
	'''	This class holds information related to Manchester Portal Members 
	''' Providers extended from ProviderSiteContact having common fields related to
	''' contact details.
	''' </summary>
	''' <remarks>
	''' Any changes in database fields of TBL_PROVIDER_DETAIL must be updated
	''' in this class.
	''' </remarks>

	Public Class Provider
		Inherits ProviderSiteContact

		''' <summary>
		''' This blocks declares all related fields for Provider.
		''' </summary>
		''' <remarks></remarks>
		Private url As String
		Private webadmin As String
		Private areas As New ArrayList()
		Private typeid As Integer
        Private servicetype As New ArrayList()

        'Martix (Q4)
		Private matrix As Boolean
		Private matrixaward As Nullable(Of Date)
        Private matrixdue As Nullable(Of Date)

        'Ofsted (Q4)
        Private Ofsted As Boolean
        Private OfstedInspection As Nullable(Of Date)
        Private OfstedDue As Nullable(Of Date)

        'Customer Service Excellence Standard Accredited (Q4)
        Private ExcellenceAccredited As Boolean
        Private ExcellenceAccreditedDate As Nullable(Of Date)
        Private ExcellenceAccreditedDueDate As Nullable(Of Date)

        'Other Accreditation (Q4)
        Private OtherAccreditation As Boolean
        Private OtherAccreditationDate As Nullable(Of Date)
        Private OtherAccreditationDueDate As Nullable(Of Date)
        Private OtherAccreditationSpecified As String

		Private charges As Boolean
		Private chargesdetail As String
		Private serviceavailable As New ArrayList()
		Private targeted As Boolean
		Private target As New ArrayList()
		Private clientgroupwards As String
		Private SERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER As String
		Private SERVICESAVAILABLE_SPECIALIST_OTHER As String
		Private othertarget As String
		Private specialist As Boolean
		Private specialistdetail As String
		Private premises As Boolean
		Private site As New ArrayList()
		Private funding As New ArrayList()
		Private futureiag As String
		Private anyothercomment As String
		Private fundingdetail As String
		Private useorgsite As Boolean
		Private ProviderSummary As String

		''' <summary>
		''' This objects declaration block is instantiating all Table Adapter
		''' classes used for updating provider information to physical database.
		''' </summary>
		''' <remarks></remarks>

		Private providersTableAdapter As New ProvidersTableAdapter
		Private providersDetailTableAdapter As New ProviderDetailsTableAdapter
		Private providersAreaTableAdapter As New ProviderAreaTableAdapter
		Private providersServiceAvailableTableAdapter As New ProviderServiceAvailableTableAdapter
		Private providersServiceTypeTableAdapter As New ProviderServiceTypeTableAdapter
		Private providersTypeTableAdapter As New ProviderTypeTableAdapter
		Private providersTargetTableAdapter As New ProviderTargetTableAdapter
		Private providersFundingSourceTableAdapter As New ProviderFundingSourceTableAdapter
		Private providersSiteTableAdapter As New ProviderSitesTableAdapter

		''' <summary>
		''' Object contructor
		''' </summary>
		''' <remarks>
		''' Every ArrayList declared should be cleared at startup.
		''' </remarks>

		Public Sub New()
			areas.Clear()
			servicetype.Clear()
			serviceavailable.Clear()
			target.Clear()
			site.Clear()
			funding.Clear()
		End Sub

		''' <summary>
		''' fuction returning array of inttegers containing all area id's
		''' of current ptovider.
		''' </summary>
		''' <returns>
		''' Integer() consists of AreaID
		''' </returns>
		''' <remarks></remarks>

		Public Function getAreas() As Integer()
			Return CType(areas.ToArray(GetType(Integer)), Integer())
		End Function

		''' <summary>
		''' Function to add area id to Areas ArrayList
		''' </summary>
		''' <param name="areaid">
		''' areaid to add into List
		''' </param>
		''' <remarks>
		''' while adding new ids duplication must be checked.
		''' </remarks>

		Public Sub addAreas(ByVal areaid As Integer)
			areas.Add(areaid)
		End Sub

		''' <summary>
		''' reset area list to empty.
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetArea()
			areas.Clear()
		End Sub

		''' <summary>
		''' Integer array of service type id of curent provider.
		''' </summary>
		''' <returns>
		''' Integer() consists of servicetype id
		''' </returns>
		''' <remarks></remarks>

		Public Function getServiceType() As Integer()
			Return CType(servicetype.ToArray(GetType(Integer)), Integer())
		End Function

		''' <summary>
		''' adding service type id to list.
		''' </summary>
		''' <param name="st">
		''' servivice type id as Integer
		''' </param>
		''' <remarks>
		''' duplication must be checked.
		''' </remarks>

		Public Sub addServiceType(ByVal st As Integer)
			servicetype.Add(st)
		End Sub

		''' <summary>
		''' reset servicetype list to empty
		''' </summary>
		''' <remarks>
		''' </remarks>

		Public Sub resetServiceType()
			servicetype.Clear()
		End Sub

		''' <summary>
		''' service available ids of current provider
		''' </summary>
		''' <returns>
		''' Integer() consist of service available ids.
		''' </returns>
		''' <remarks></remarks>

		Public Function getServiceAvailable() As Integer()
			Return CType(serviceavailable.ToArray(GetType(Integer)), Integer())
		End Function

		''' <summary>
		''' addition of service available id of current provider
		''' </summary>
		''' <param name="sa">
		''' service available id as Integer
		''' </param>
		''' <remarks></remarks>

		Public Sub addServiceAvailable(ByVal sa As Integer)
			serviceavailable.Add(sa)
		End Sub

		''' <summary>
		''' reset service available id list to empty.
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetServiceAvailable()
			serviceavailable.Clear()
		End Sub

		''' <summary>
		''' target groups ids of current provider
		''' </summary>
		''' <returns>
		''' Integer() consists of Target Id's
		''' </returns>
		''' <remarks></remarks>

		Public Function getTarget() As Integer()
			Return CType(target.ToArray(GetType(Integer)), Integer())
		End Function


		''' <summary>
		''' adding target group ids to list.
		''' </summary>
		''' <param name="t">target id as Integer</param>
		''' <remarks></remarks>

		Public Sub addTarget(ByVal t As Integer)
			target.Add(t)
		End Sub

		''' <summary>
		''' reset target group list to empty
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetTarget()
			target.Clear()
		End Sub

		''' <summary>
		''' provides Array containing ProviderSite objects.
		''' </summary>
		''' <returns>
		''' ProviderSite() consisting details of each site registered with
		''' current provider.
		''' </returns>
		''' <remarks></remarks>

		Public Function getSite() As ProviderSite()
			Return CType(site.ToArray(GetType(ProviderSite)), ProviderSite())
		End Function

		''' <summary>
		''' adding a site object to current provider.
		''' </summary>
		''' <param name="st">
		''' ProviderSite object
		''' </param>
		''' <remarks>
		''' site object must be instantiate before adding.
		''' </remarks>

		Public Sub addSite(ByVal st As ProviderSite)
			site.Add(st)
		End Sub

		''' <summary>
		''' reset ProviderSite List to empty.
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetSite()
			site.Clear()
		End Sub

		''' <summary>
		''' Array of ProviderFunding objects
		''' </summary>
		''' <returns>
		''' ProviderFunding() consists of funding details of provider.
		''' </returns>
		''' <remarks></remarks>

		Public Function getProviderFunding() As ProviderFunding()
			Return CType(funding.ToArray(GetType(ProviderFunding)), ProviderFunding())
		End Function

		''' <summary>
		''' adding ProviderFunding object to ArrayList
		''' </summary>
		''' <param name="pf">
		''' ProviderFunding object
		''' </param>
		''' <remarks>
		''' providerfunding object must be instantiate before adding.
		''' </remarks>

		Public Sub addProviderFunding(ByVal pf As ProviderFunding)
			funding.Add(pf)
		End Sub

		''' <summary>
		''' reset ProviderFunding List to empty.
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetProviderFunding()
			funding.Clear()
		End Sub

		''' <summary>
		''' setting provider web url
		''' </summary>
		''' <param name="value">
		''' String url
		''' </param>
		''' <remarks></remarks>

		Public Sub setUrl(ByVal value As String)
			Me.url = value
		End Sub

		''' <summary>
		''' getting provider web url
		''' </summary>
		''' <returns>
		''' string url
		''' </returns>
		''' <remarks></remarks>

		Public Function getUrl() As String
			Return Me.url
		End Function

		''' <summary>
        ''' getting provider Summary
        ''' </summary>
        ''' <param name="value">
        ''' String Summary
        ''' </param>
        ''' <remarks></remarks>

        Public Sub setProviderSummary(ByVal value As String)
            Me.ProviderSummary = value
        End Sub

        ''' <summary>
        ''' getting provider Summary
        ''' </summary>
        ''' <returns>
        ''' string Summary
        ''' </returns>
        ''' <remarks></remarks>

        Public Function getProviderSummary() As String
            Return Me.ProviderSummary
        End Function


		''' <summary>
		''' setting website admin name of provider
		''' </summary>
		''' <param name="value">
		''' String webadmin
		''' </param>
		''' <remarks></remarks>

		Public Sub setWebAdmin(ByVal value As String)
			Me.webadmin = value
		End Sub

		''' <summary>
		''' getting provider website admin name
		''' </summary>
		''' <returns>
		''' String webadmin
		''' </returns>
		''' <remarks></remarks>

		Public Function getWebAdmin() As String
			Return Me.webadmin
		End Function

		''' <summary>
		''' setting provider type id
		''' </summary>
		''' <param name="value">
		''' Integer typeid
		''' </param>
		''' <remarks></remarks>

		Public Sub setTypeID(ByVal value As Integer)
			Me.typeid = value
		End Sub

		''' <summary>
		''' getting provider typeid
		''' </summary>
		''' <returns>
		''' Integer typeid
		''' </returns>
		''' <remarks></remarks>

		Public Function getTypeID() As Integer
			Return Me.typeid
		End Function

		''' <summary>
		''' setting matrix accreditation flag.
		''' </summary>
		''' <param name="value">
		''' Boolean flag
		''' </param>
		''' <remarks></remarks>

		Public Sub setMatrix(ByVal value As Boolean)
			Me.matrix = value
		End Sub

		''' <summary>
		''' getting matrix accreditaion flag.
		''' </summary>
		''' <returns>
		''' Boolean flag
		''' </returns>
		''' <remarks></remarks>

		Public Function getMatrix() As Boolean
			Return Me.matrix
		End Function

		''' <summary>
		''' setting matrix award date of provider
		''' </summary>
		''' <param name="value">
		''' Date awarddate
		''' </param>
		''' <remarks></remarks>
		''' 

		Public Sub setMatrixAward(ByVal value As Date)
			Me.matrixaward = value
		End Sub

		''' <summary>
		''' getting matrix award date of provider
		''' </summary>
		''' <returns>
		''' Date awarddate
		''' </returns>
		''' <remarks></remarks>

		Public Function getMatrixAward() As Date
			Return Me.matrixaward
		End Function

		''' <summary>
		''' setting matrix expiry date of provider.
		''' </summary>
		''' <param name="value">
		''' Date duedate
		''' </param>
		''' <remarks></remarks>

		Public Sub setMatrixDue(ByVal value As Date)
			Me.matrixdue = value
		End Sub

		''' <summary>
		''' getting matrix expiry date of provider.
		''' </summary>
		''' <returns>
		''' Date duedate
		''' </returns>
		''' <remarks></remarks>

		Public Function getMatrixDue() As Date
			Return Me.matrixdue
		End Function


        Public Sub setOfsted(ByVal value As Boolean)
            Me.Ofsted = value
        End Sub

        Public Function getOfsted() As Boolean
            Return Me.Ofsted
        End Function

        Public Sub setOfstedInspection(ByVal value As Date)
            Me.OfstedInspection = value
        End Sub

        Public Function getOfstedInspection() As Date
            Return Me.OfstedInspection
        End Function

        Public Sub setOfstedDue(ByVal value As Date)
            Me.OfstedDue = value
        End Sub

        Public Function getOfstedDue() As Date
            Return Me.OfstedDue
        End Function

        Public Sub setExcellenceAccredited(ByVal value As Boolean)
            Me.ExcellenceAccredited = value
        End Sub

        Public Function getExcellenceAccredited() As Boolean
            Return Me.ExcellenceAccredited
        End Function

        Public Sub setExcellenceAccreditedDate(ByVal value As Date)
            Me.ExcellenceAccreditedDate = value
        End Sub

        Public Function getExcellenceAccreditedDate() As Date
            Return Me.ExcellenceAccreditedDate
        End Function

        Public Sub setExcellenceAccreditedDueDate(ByVal value As Date)
            Me.ExcellenceAccreditedDueDate = value
        End Sub

        Public Function getExcellenceAccreditedDueDate() As Date
            Return Me.ExcellenceAccreditedDueDate
        End Function

        Public Sub setOtherAccreditation(ByVal value As Boolean)
            Me.OtherAccreditation = value
        End Sub

        Public Function getOtherAccreditation() As Boolean
            Return Me.OtherAccreditation
        End Function

        Public Sub setOtherAccreditationDate(ByVal value As Date)
            Me.OtherAccreditationDate = value
        End Sub

        Public Function getOtherAccreditationDate() As Date
            Return Me.OtherAccreditationDate
        End Function

        Public Sub setOtherAccreditationDueDate(ByVal value As Date)
            Me.OtherAccreditationDueDate = value
        End Sub

        Public Function getOtherAccreditationDueDate() As Date
            Return Me.OtherAccreditationDueDate
        End Function

        Public Sub setOtherAccreditationSpecified(ByVal value As String)
            Me.OtherAccreditationSpecified = value
        End Sub

        Public Function getOtherAccreditationSpecified() As String
            Return Me.OtherAccreditationSpecified
        End Function

		''' <summary>
		''' setting chargable services flag of provider
		''' </summary>
		''' <param name="value">
		''' Boolean chargable
		''' </param>
		''' <remarks></remarks>

		Public Sub setCharges(ByVal value As Boolean)
			Me.charges = value
		End Sub

		''' <summary>
		''' getting chargable services flag of provider
		''' </summary>
		''' <returns>
		''' Boolean chargable
		''' </returns>
		''' <remarks></remarks>

		Public Function getCharges() As Boolean
			Return Me.charges
		End Function

		''' <summary>
		''' setting chargable service detail of provider
		''' </summary>
		''' <param name="value">
		''' String detail
		''' </param>
		''' <remarks></remarks>

		Public Sub setChargesDetail(ByVal value As String)
			Me.chargesdetail = value
		End Sub

		''' <summary>
		''' getting chargable service detail of provider
		''' </summary>
		''' <returns>
		''' String detail
		''' </returns>
		''' <remarks></remarks>

		Public Function getChargesDetail() As String
			Return Me.chargesdetail
		End Function

		''' <summary>
		''' setting target group flag of provider.
		''' </summary>
		''' <param name="value">
		''' Boolean targeted
		''' </param>
		''' <remarks></remarks>

		Public Sub setTargeted(ByVal value As Boolean)
			Me.targeted = value
		End Sub

		''' <summary>
		''' getting target group flag of provider.
		''' </summary>
		''' <returns>
		''' Boolean targeted
		''' </returns>
		''' <remarks></remarks>

		Public Function getTargeted() As Boolean
			Return Me.targeted
		End Function

		''' <summary>
		''' setting client groups ward detail if specific ward is targeted
		''' </summary>
		''' <param name="value">
		''' String ward detail
		''' </param>
		''' <remarks></remarks>

		Public Sub setClietGroupWards(ByVal value As String)
			Me.clientgroupwards = value
		End Sub

		''' <summary>
		''' getting client groups ward detail if specific ward is targeted
		''' </summary>
		''' <returns>
		''' String ward detail
		''' </returns>
		''' <remarks></remarks>

		Public Function getClientGroupWards() As String
			Return Me.clientgroupwards
		End Function



		''' <summary>
		''' setting any other target group detials of provider.
		''' </summary>
		''' <param name="value">
		''' String targetdetail
		''' </param>
		''' <remarks></remarks>

		Public Sub setOtherTarget(ByVal value As String)
			Me.othertarget = value
		End Sub

		''' <summary>
		''' getting any other target group detials of provider.
		''' </summary>
		''' <returns>
		''' String targetdetail
		''' </returns>
		''' <remarks></remarks>

		Public Function getOtherTarget() As String
			Return Me.othertarget
		End Function


        ''' <summary>
        ''' setting any other Services Available - Learning Skills .
        ''' </summary>
        ''' <param name="value">
        ''' String OtherLearningSkillsDetail
        ''' </param>
        ''' <remarks></remarks>

        Public Sub setSERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER(ByVal value As String)
            Me.SERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER = value
        End Sub

        ''' <summary>
        ''' getting any other Services Available - Learning Skills .
        ''' </summary>
        ''' <returns>
        ''' String OtherLearningSkillsDetail
        ''' </returns>
        ''' <remarks></remarks>

        Public Function getSERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER() As String
            Return Me.SERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER
        End Function

        ''' <summary>
        ''' setting any other Services Available - Learning Skills .
        ''' </summary>
        ''' <param name="value">
        ''' String OtherLearningSkillsDetail
        ''' </param>
        ''' <remarks></remarks>

        Public Sub setSERVICESAVAILABLE_SPECIALIST_OTHER(ByVal value As String)
            Me.SERVICESAVAILABLE_SPECIALIST_OTHER = value
        End Sub

        ''' <summary>
        ''' getting any other Services Available - Learning Skills .
        ''' </summary>
        ''' <returns>
        ''' String OtherLearningSkillsDetail
        ''' </returns>
        ''' <remarks></remarks>

        Public Function getSERVICESAVAILABLE_SPECIALIST_OTHER() As String
            Return Me.SERVICESAVAILABLE_SPECIALIST_OTHER
        End Function

		''' <summary>
		''' setting specialist services flag
		''' </summary>
		''' <param name="value">
		''' Boolean specialist
		''' </param>
		''' <remarks></remarks>

		Public Sub setSpecialist(ByVal value As Boolean)
			Me.specialist = value
		End Sub

		''' <summary>
		''' getting specialist services flag
		''' </summary>
		''' <returns>
		''' Boolean specialist
		''' </returns>
		''' <remarks></remarks>

		Public Function getSpecialist() As Boolean
			Return Me.specialist
		End Function

		''' <summary>
		''' setting specialist service detail
		''' </summary>
		''' <param name="value">
		''' String specialist service detail
		''' </param>
		''' <remarks></remarks>

		Public Sub setSpecialistDetail(ByVal value As String)
			Me.specialistdetail = value
		End Sub

		''' <summary>
		''' getting specialist service detail
		''' </summary>
		''' <returns>
		''' String specialist service detail
		''' </returns>
		''' <remarks></remarks>

		Public Function getSpecialistDetails() As String
			Return Me.specialistdetail
		End Function

		''' <summary>
		''' setting flag for provider having more than one sites
		''' </summary>
		''' <param name="value">
		''' Boolean moresites
		''' </param>
		''' <remarks></remarks>

		Public Sub setPremises(ByVal value As Boolean)
			Me.premises = value
		End Sub

		''' <summary>
		''' getting flag for provider having more than one sites
		''' </summary>
		''' <returns>
		''' Boolean moresites
		''' </returns>
		''' <remarks></remarks>

		Public Function getPremises() As Boolean
			Return Me.premises
		End Function

		''' <summary>
		''' setting future iag development details
		''' </summary>
		''' <param name="value">
		''' String iag details
		''' </param>
		''' <remarks></remarks>

		Public Sub setFutureIAG(ByVal value As String)
			Me.futureiag = value
		End Sub

		''' <summary>
		''' getting future iag development details
		''' </summary>
		''' <returns>
		''' String iag details
		''' </returns>
		''' <remarks></remarks>

		Public Function getFutureIAG() As String
			Return Me.futureiag
		End Function

		''' <summary>
		''' setting specific funding source detail
		''' </summary>
		''' <param name="value">
		''' String detail
		''' </param>
		''' <remarks></remarks>

		Public Sub setFundingDetail(ByVal value As String)
			Me.fundingdetail = value
		End Sub

		''' <summary>
		''' getting specific funding source detail
		''' </summary>
		''' <returns>
		''' String detail
		''' </returns>
		''' <remarks></remarks>

		Public Function getFundingDetail() As String
			Return Me.fundingdetail
		End Function


		''' <summary>
		''' setting any other comments detail
		''' </summary>
		''' <param name="value">
		''' String details
		''' </param>
		''' <remarks></remarks>

		Public Sub setAnyOtherComments(ByVal value As String)
			Me.anyothercomment = value
		End Sub

		''' <summary>
		''' getting any other comments detail
		''' </summary>
		''' <returns>
		''' String details
		''' </returns>
		''' <remarks></remarks>

		Public Function getAnyOtherComments() As String
			Return Me.anyothercomment
		End Function

		''' <summary>
		''' setting flag indicating use of provider contact fields as first site.
		''' </summary>
		''' <param name="value">
		''' Boolean
		''' </param>
		''' <remarks></remarks>

		Public Sub setUseOrgSite(ByVal value As Boolean)
			Me.useorgsite = value
		End Sub

		''' <summary>
		''' getting flag indicating use of provider contact fields as first site.
		''' </summary>
		''' <returns>
		''' Boolean
		''' </returns>
		''' <remarks></remarks>

		Public Function getUseOrgSite() As Boolean
			Return Me.useorgsite
		End Function

		Public Function getProviderName() As String
			Return Me.name
		End Function


		''' <summary>
		''' update current provider state to database tables
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' </remarks>


        Public Sub updateProviderDBTable()
            Dim cflag As Boolean = False
            'providersTableAdapter.BeginTransaction()
            Try
                Dim add As Boolean
                Dim providerTable As ProvidersMain.ProviderDetailsDataTable = providersDetailTableAdapter.GetAllProvidersDetailByID(Integer.Parse(id))
                Dim providerTableRow As ProvidersMain.ProviderDetailsRow

                If providerTable.Count = 0 Then
                    providerTable = New ProvidersMain.ProviderDetailsDataTable
                    providerTableRow = providerTable.NewProviderDetailsRow
                    add = True
                Else
                    add = False
                    providerTableRow = providerTable(0)
                End If
                providerTableRow.ProviderID = Me.id
                providerTableRow.ProviderName = Me.name
                providerTableRow.ProviderSummary = Me.ProviderSummary
                providerTableRow.AddressLine1 = Me.address1
                providerTableRow.AddressLine2 = Me.address2
                providerTableRow.AddressLine3 = Me.address3
                providerTableRow.AddressLine4 = Me.address4
                If Not add Then
                    If providerTableRow.Postcode <> Me.postcode Then
                        cflag = True
                    End If
                End If
                providerTableRow.Postcode = Me.postcode
                providerTableRow.lat = Me.lattitude
                providerTableRow.lon = Me.longitude
                providerTableRow.Telephone = Me.telephone
                providerTableRow.Fax = Me.fax
                providerTableRow.URL = Me.url
                providerTableRow.Email = Me.email
                providerTableRow.ClientContact = Me.contact
                providerTableRow.WebAdministrator = Me.webadmin

                'Matrix (Q4)
                providerTableRow.MatrixAccrediation = Me.matrix
                If Me.matrix Then
                    providerTableRow.MatrixDate = Me.matrixaward
                    providerTableRow.MatrixDueDate = Me.matrixdue
                End If

                'Ofsted (Q4)
                providerTableRow.Ofsted = Me.Ofsted
                If Me.Ofsted Then
                    providerTableRow.OfstedInspection = Me.OfstedInspection
                    providerTableRow.OfstedDueDate = Me.OfstedDue
                End If

                'Customer Service Excellence Standard Accredited (Q4)
                providerTableRow.ExcellenceAccredited = Me.ExcellenceAccredited
                If Me.ExcellenceAccredited Then
                    providerTableRow.ExcellenceAccreditedDate = Me.ExcellenceAccreditedDate
                    providerTableRow.ExcellenceAccreditedDueDate = Me.ExcellenceAccreditedDueDate
                End If

                'Other Accreditation (Q4)
                providerTableRow.OtherAccreditation = Me.OtherAccreditation
                If Me.OtherAccreditation Then
                    providerTableRow.OtherAccreditationDate = Me.OtherAccreditationDate
                    providerTableRow.OtherAccreditationDueDate = Me.OtherAccreditationDueDate
                    providerTableRow.OtherAccreditationSpecified = Me.OtherAccreditationSpecified
                End If

                providerTableRow.Chargeable = Me.charges
                If Me.charges Then
                    providerTableRow.ChargeableService = Me.chargesdetail
                End If

                If providerTableRow.IsOtherSpecialistNull Then

                End If
                providerTableRow.OtherLearningSkills = Me.SERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER
                providerTableRow.OtherSpecialist = Me.SERVICESAVAILABLE_SPECIALIST_OTHER

                providerTableRow.TargetClient = Me.targeted
                providerTableRow.TargetWards = Me.clientgroupwards
                providerTableRow.SpecialistService = Me.specialist
                If Me.specialist Then
                    providerTableRow.SpecialistDetail = Me.specialistdetail
                End If
                If Me.targeted Then
                    providerTableRow.OtherEligibleClients = Me.othertarget
                End If
                providerTableRow.MoreSites = Me.premises
                providerTableRow.UseMainSite = Me.useorgsite
                providerTableRow.FundingSpecifics = Me.fundingdetail
                providerTableRow.FutureIAGDev = Me.futureiag
                providerTableRow.OtherInformation = Me.anyothercomment
                If add Then
                    providerTable.AddProviderDetailsRow(providerTableRow)
                End If
                providersDetailTableAdapter.Update(providerTable)
                If cflag Then
                    Dim pNotify As New ProviderEmailNotifier
                    pNotify.sendPCChangeNotification(Me, True)
                End If
                'providersDetailTableAdapter.CommitTransaction()
            Catch ex As Exception
                'If providersTableAdapter.Connection.State = Data.ConnectionState.Open Then
                'providersTableAdapter.RollbackTransaction()
                'End If
            End Try
        End Sub

		''' <summary>
		''' Section 1 of entry form updation to database.
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' </remarks>

		Public Sub updateSection1toDB()
			Try
				If Me.areas.Count > 0 Then
					'providersAreaTableAdapter.BeginTransaction()
					Dim providerAreaTable As ProvidersMain.ProviderAreaDataTable = providersAreaTableAdapter.GetAllProviderAreaByID(id)
					Dim providerAreaRow As ProvidersMain.ProviderAreaRow
					If providerAreaTable.Count > 0 Then
						For Each arearow As ProvidersMain.ProviderAreaRow In providerAreaTable
							providersAreaTableAdapter.Delete(arearow.ProviderID, arearow.AreaID)
						Next
					End If
					providerAreaTable = New ProvidersMain.ProviderAreaDataTable
					Dim ac As Integer
					For ac = 0 To Me.areas.Count - 1
						providerAreaRow = providerAreaTable.NewProviderAreaRow
						providerAreaRow.ProviderID = id
						providerAreaRow.AreaID = Me.areas.Item(ac)
						providerAreaTable.AddProviderAreaRow(providerAreaRow)
					Next
					providersAreaTableAdapter.Update(providerAreaTable)
					'providersAreaTableAdapter.CommitTransaction()
				End If
			Catch ex As Exception
				If providersAreaTableAdapter.Connection.State = Data.ConnectionState.Open Then
					'providersAreaTableAdapter.RollbackTransaction()
				End If
			End Try
		End Sub

		''' <summary>
		''' Section 2 of entry form updation to database.
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' </remarks>

		Public Sub updateSection2toDB()
			Try
				'providersTypeTableAdapter.BeginTransaction()
				Dim providerTypeTable As ProvidersMain.ProviderTypeDataTable = providersTypeTableAdapter.GetProviderTypeByID(id)
				Dim providerTypeRow As ProvidersMain.ProviderTypeRow
				If providerTypeTable.Count > 0 Then
					For Each typerow As ProvidersMain.ProviderTypeRow In providerTypeTable
						providersTypeTableAdapter.Delete(typerow.ProviderID, typerow.TypeID)
					Next
				End If
				providerTypeTable = New ProvidersMain.ProviderTypeDataTable
				providerTypeRow = providerTypeTable.NewProviderTypeRow
				providerTypeRow.ProviderID = id
				providerTypeRow.TypeID = Me.typeid
				providerTypeTable.AddProviderTypeRow(providerTypeRow)
				providersTypeTableAdapter.Update(providerTypeTable)
				'providersTypeTableAdapter.CommitTransaction()
				If Me.servicetype.Count > 0 Then
					'providersServiceTypeTableAdapter.BeginTransaction()
					Dim providerServiceTypeTable As ProvidersMain.ProviderServiceTypeDataTable = providersServiceTypeTableAdapter.GetProviderServiceTypeByID(id)
					Dim providerServiceTypeRow As ProvidersMain.ProviderServiceTypeRow
					If providerServiceTypeTable.Count > 0 Then
						For Each typerow As ProvidersMain.ProviderServiceTypeRow In providerServiceTypeTable
							providersServiceTypeTableAdapter.Delete(typerow.ProviderID, typerow.TypeID)
						Next
					End If
					providerServiceTypeTable = New ProvidersMain.ProviderServiceTypeDataTable
					Dim st As Integer
					For st = 0 To Me.servicetype.Count - 1
						providerServiceTypeRow = providerServiceTypeTable.NewProviderServiceTypeRow
						providerServiceTypeRow.ProviderID = id
						providerServiceTypeRow.TypeID = Me.servicetype.Item(st)
						providerServiceTypeTable.AddProviderServiceTypeRow(providerServiceTypeRow)
					Next
					providersServiceTypeTableAdapter.Update(providerServiceTypeTable)
					'providersServiceTypeTableAdapter.CommitTransaction()
				End If
			Catch ex As Exception
			'If providersServiceTypeTableAdapter.Connection.State = Data.ConnectionState.Open Then
			'	providersServiceTypeTableAdapter.RollbackTransaction()
			'End If
			'If providersTypeTableAdapter.Connection.State = Data.ConnectionState.Open Then
			'	providersTypeTableAdapter.RollbackTransaction()
			'End If
			End Try
		End Sub

		''' <summary>
		''' Section 3 of Entry form updation to database.
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' </remarks>

		Public Sub updateSection3toDB()
			Try
				If Me.serviceavailable.Count > 0 Then
					'providersServiceAvailableTableAdapter.BeginTransaction()
					Dim providerServiceAvailableTable As ProvidersMain.ProviderServiceAvailableDataTable = providersServiceAvailableTableAdapter.GetProviderServiceAvaliableByID(id)
					Dim providerServiceAvailableRow As ProvidersMain.ProviderServiceAvailableRow
					If providerServiceAvailableTable.Count > 0 Then
						For Each typerow As ProvidersMain.ProviderServiceAvailableRow In providerServiceAvailableTable
							providersServiceAvailableTableAdapter.Delete(typerow.ProviderID, typerow.ServiceID)
						Next
					End If
					providerServiceAvailableTable = New ProvidersMain.ProviderServiceAvailableDataTable
					Dim st As Integer
					For st = 0 To Me.serviceavailable.Count - 1
						providerServiceAvailableRow = providerServiceAvailableTable.NewProviderServiceAvailableRow
						providerServiceAvailableRow.ProviderID = id
						providerServiceAvailableRow.ServiceID = Me.serviceavailable.Item(st)
						providerServiceAvailableTable.AddProviderServiceAvailableRow(providerServiceAvailableRow)
					Next
					providersServiceAvailableTableAdapter.Update(providerServiceAvailableTable)
					'providersServiceAvailableTableAdapter.CommitTransaction()
				End If
			Catch ex As Exception
                'If providersServiceAvailableTableAdapter.Connection.State = Data.ConnectionState.Open Then
                '	providersServiceAvailableTableAdapter.RollbackTransaction()
                'End If
			End Try

		End Sub

		''' <summary>
		''' Section 4 of entry form updating to database.
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' </remarks>

		Public Sub updateSection4toDB()
			If Me.targeted Then
				Try
					If Me.target.Count > 0 Then
						'providersTargetTableAdapter.BeginTransaction()
						Dim providerTargetTable As ProvidersMain.ProviderTargetDataTable = providersTargetTableAdapter.GetAllByID(id)
						Dim providerTargetRow As ProvidersMain.ProviderTargetRow
						If providerTargetTable.Count > 0 Then
							For Each row As ProvidersMain.ProviderTargetRow In providerTargetTable
								providersTargetTableAdapter.Delete(row.ProviderID, row.TargetID)
							Next
						End If
						providerTargetTable = New ProvidersMain.ProviderTargetDataTable
						Dim st As Integer
						For st = 0 To Me.target.Count - 1
							providerTargetRow = providerTargetTable.NewProviderTargetRow
							providerTargetRow.ProviderID = id
							providerTargetRow.TargetID = Me.target.Item(st)
							providerTargetTable.AddProviderTargetRow(providerTargetRow)
						Next
						providersTargetTableAdapter.Update(providerTargetTable)
						'providersTargetTableAdapter.CommitTransaction()
					End If
				Catch ex As Exception
					'If providersTargetTableAdapter.Connection.State = Data.ConnectionState.Open Then
						'providersTargetTableAdapter.RollbackTransaction()
					'End If
				End Try
			End If
		End Sub

		''' <summary>
		''' Section 6 of entry form updation to database
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' </remarks>

		Public Sub updateSection6toDB()
			If Me.funding.Count > 0 Then
				Try
					'providersFundingSourceTableAdapter.BeginTransaction()
					Dim providerFundingTable As ProvidersMain.ProviderFundingSourceDataTable = providersFundingSourceTableAdapter.GetAllProviderFundingSourceByID(id)
					Dim providerFundingRow As ProvidersMain.ProviderFundingSourceRow
					If providerFundingTable.Count > 0 Then
						For Each row As ProvidersMain.ProviderFundingSourceRow In providerFundingTable
							providersFundingSourceTableAdapter.Delete(row.ProviderID, row.SourceID)
						Next
					End If
					providerFundingTable = New ProvidersMain.ProviderFundingSourceDataTable
					Dim st As Integer
					For st = 0 To Me.funding.Count - 1
						Dim fundingObj As ProviderFunding = CType(Me.funding.Item(st), ProviderFunding)
						providerFundingRow = providerFundingTable.NewProviderFundingSourceRow
						providerFundingRow.ProviderID = id
						providerFundingRow.SourceID = fundingObj.getSource
						providerFundingRow.EndDate = fundingObj.getExpire
						providerFundingTable.AddProviderFundingSourceRow(providerFundingRow)
					Next
					providersFundingSourceTableAdapter.Update(providerFundingTable)
					'providersFundingSourceTableAdapter.CommitTransaction()
				Catch ex As Exception
                    'If providersFundingSourceTableAdapter.Connection.State = Data.ConnectionState.Open Then
                    '	providersFundingSourceTableAdapter.RollbackTransaction()
                    'End If
				End Try
			End If
		End Sub

		''' <summary>
		''' ProviderSites in ArrayList updation to database using ProviderSite
		''' public method.
		''' </summary>
		''' <remarks>
		''' FillProviderFromDB must be called before commiting to database
		''' in order to prevent existing values inside database to be lost.
		''' Site details must be updated after all provider sections are
		''' successfully updated to database.
		''' </remarks>

		Public Sub updateSitetoDB()
			Dim i As Integer
			For i = 0 To Me.site.Count - 1
				CType(Me.site.Item(i), ProviderSite).updateSitetoDB(Me)
			Next
		End Sub

		''' <summary>
		''' deletion of provider from database
		''' </summary>
		''' <remarks>
		''' method assumes refrential integrity constraints in database are
		''' implemented as on delete cascade for all related records of
		''' provider.
		''' </remarks>

		Public Sub deleteProviderfromDB()
			'providersTableAdapter.BeginTransaction()
			providersTableAdapter.Delete(Me.id)
			'providersTableAdapter.CommitTransaction()
		End Sub

		''' <summary>
		''' filling provider attribute values from databse as well as
		''' related details lists.
		''' </summary>
		''' <remarks>
		''' This function must be called after setting provider id.
		''' </remarks>

		Public Sub fillProviderFromDB()
			Dim providerTable As ProvidersMain.ProvidersDataTable = providersTableAdapter.GetByID(Integer.Parse(id))
			For Each providerrow As ProvidersMain.ProvidersRow In providerTable
				Me.name = providerrow.ProviderName
			Next

			Dim providerDetailTable As ProvidersMain.ProviderDetailsDataTable = providersDetailTableAdapter.GetAllProvidersDetailByID(id)
			For Each providerdetailrow As ProvidersMain.ProviderDetailsRow In providerDetailTable
				If Not providerdetailrow.IsProviderSummaryNull() Then
					Me.ProviderSummary = providerdetailrow.ProviderSummary
				End If
				Me.address1 = providerdetailrow.AddressLine1
				If Not providerdetailrow.IsAddressLine2Null() Then
					Me.address2 = providerdetailrow.AddressLine2
				End If
				If Not providerdetailrow.IsAddressLine3Null() Then
					Me.address3 = providerdetailrow.AddressLine3
				End If
				If Not providerdetailrow.IsAddressLine4Null() Then
					Me.address4 = providerdetailrow.AddressLine4
				End If
                Me.postcode = providerdetailrow.Postcode
                Me.lattitude = providerdetailrow.lat
                Me.longitude = providerdetailrow.lon
				If Not providerdetailrow.IsTelephoneNull() Then
					Me.telephone = providerdetailrow.Telephone
				End If
				If Not providerdetailrow.IsFaxNull() Then
					Me.fax = providerdetailrow.Fax
				End If
				If Not providerdetailrow.IsURLNull() Then
					Me.url = providerdetailrow.URL
				End If
				If Not providerdetailrow.IsEmailNull() Then
					Me.email = providerdetailrow.Email
				End If
				Me.contact = providerdetailrow.ClientContact
				Me.webadmin = providerdetailrow.WebAdministrator

			'Matrix (Q4)
				Me.matrix = providerdetailrow.MatrixAccrediation
				If providerdetailrow.MatrixAccrediation Then
					Me.matrixaward = providerdetailrow.MatrixDate
					Me.matrixdue = providerdetailrow.MatrixDueDate
				End If

                'Ofsted (Q4)
                Me.Ofsted = providerdetailrow.Ofsted
                If providerdetailrow.Ofsted Then
                    Me.OfstedInspection = providerdetailrow.OfstedInspection
                    Me.OfstedDue = providerdetailrow.OfstedDueDate
                End If

                'Customer Service Excellence Standard Accredited (Q4)
                Me.ExcellenceAccredited = providerdetailrow.ExcellenceAccredited
                If providerdetailrow.ExcellenceAccredited Then
                    Me.ExcellenceAccreditedDate = providerdetailrow.ExcellenceAccreditedDate
                    Me.ExcellenceAccreditedDueDate = providerdetailrow.ExcellenceAccreditedDueDate
                End If

                'Other Accreditation (Q4)
                Me.OtherAccreditation = providerdetailrow.OtherAccreditation
                If providerdetailrow.OtherAccreditation Then
                    Me.OtherAccreditationDate = providerdetailrow.OtherAccreditationDate
                    Me.OtherAccreditationDueDate = providerdetailrow.OtherAccreditationDueDate
                    Me.OtherAccreditationSpecified = providerdetailrow.OtherAccreditationSpecified
                End If

				Me.charges = providerdetailrow.Chargeable
				If providerdetailrow.Chargeable Then
					Me.chargesdetail = providerdetailrow.ChargeableService
                End If

				Me.targeted = providerdetailrow.TargetClient
				If Me.targeted Then
					setProviderTargetFields()
					If Not providerdetailrow.IsOtherEligibleClientsNull() Then
						Me.othertarget = providerdetailrow.OtherEligibleClients
					End If
                End If

                If Not providerdetailrow.IsOtherLearningSkillsNull() Then
                    Me.SERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER = providerdetailrow.OtherLearningSkills
                End If

                If Not providerdetailrow.IsOtherSpecialistNull() Then
                    Me.SERVICESAVAILABLE_SPECIALIST_OTHER = providerdetailrow.OtherSpecialist
                End If

                Me.specialist = providerdetailrow.SpecialistService
                If Me.specialist Then
                    If Not providerdetailrow.IsSpecialistDetailNull Then
                        Me.specialistdetail = providerdetailrow.SpecialistDetail
                    End If
                End If
                If Not providerdetailrow.IsTargetWardsNull Then
                    Me.clientgroupwards = providerdetailrow.TargetWards
                End If
                Me.premises = providerdetailrow.MoreSites
                Me.useorgsite = providerdetailrow.UseMainSite
                If Not providerdetailrow.IsFutureIAGDevNull Then
                    Me.futureiag = providerdetailrow.FutureIAGDev
                End If
                If Not providerdetailrow.IsOtherInformationNull Then
                    Me.anyothercomment = providerdetailrow.OtherInformation
                End If
                If Not providerdetailrow.IsFundingSpecificsNull Then
                    Me.fundingdetail = providerdetailrow.FundingSpecifics
                End If
            Next
			Dim site As ProviderSite
			Me.resetSite()
			Dim providerSiteTable As ProvidersMain.ProviderSitesDataTable = providersSiteTableAdapter.GetProviderSitesByID(id)
			Dim cnt As Boolean = True
			For Each providersiterow As ProvidersMain.ProviderSitesRow In providerSiteTable
				site = New ProviderSite(Me)
				site.setID(providersiterow.SiteID)
				site.setPID(id)
				site.setFixed(cnt)
				site.fillSiteFromDB()
				Me.addSite(site)
				cnt = False
			Next
			Me.resetArea()
			Dim providerAreaTable As ProvidersMain.ProviderAreaDataTable = providersAreaTableAdapter.GetAllProviderAreaByID(id)
			For Each providerarearow As ProvidersMain.ProviderAreaRow In providerAreaTable
				Me.addAreas(providerarearow.AreaID)
			Next
			Dim providerTypeTable As ProvidersMain.ProviderTypeDataTable = providersTypeTableAdapter.GetProviderTypeByID(id)
			For Each providertyperow As ProvidersMain.ProviderTypeRow In providerTypeTable
				Me.typeid = providertyperow.TypeID
			Next
			Me.resetServiceType()
			Dim providerServiceTypeTable As ProvidersMain.ProviderServiceTypeDataTable = providersServiceTypeTableAdapter.GetProviderServiceTypeByID(id)
			For Each providerservicetyperow As ProvidersMain.ProviderServiceTypeRow In providerServiceTypeTable
				Me.addServiceType(providerservicetyperow.TypeID)
			Next
			Me.resetServiceAvailable()
			Dim providerServiceAvailableTable As ProvidersMain.ProviderServiceAvailableDataTable = providersServiceAvailableTableAdapter.GetProviderServiceAvaliableByID(id)
			For Each providerserviceavailablerow As ProvidersMain.ProviderServiceAvailableRow In providerServiceAvailableTable
				Me.addServiceAvailable(providerserviceavailablerow.ServiceID)
			Next
			Me.resetProviderFunding()
			Dim providerFundingTable As ProvidersMain.ProviderFundingSourceDataTable = providersFundingSourceTableAdapter.GetAllProviderFundingSourceByID(id)
			For Each providerfundingtablerow As ProvidersMain.ProviderFundingSourceRow In providerFundingTable
				Me.addProviderFunding(New MDApplication.ProviderFunding(providerfundingtablerow.SourceID, providerfundingtablerow.EndDate))
			Next
		End Sub

		''' <summary>
		''' filling provider target group list from database
		''' </summary>
		''' <remarks>
		''' method internally called from fill method.
		''' </remarks>

		Private Sub setProviderTargetFields()
			Me.resetTarget()
			Dim providerTargetTable As ProvidersMain.ProviderTargetDataTable = providersTargetTableAdapter.GetAllByID(id)
			For Each providertargetrow As ProvidersMain.ProviderTargetRow In providerTargetTable
				Me.addTarget(providertargetrow.TargetID)
			Next
		End Sub

	End Class

End Namespace

