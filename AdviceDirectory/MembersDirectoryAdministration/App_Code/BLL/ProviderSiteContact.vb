Imports Microsoft.VisualBasic

Namespace MDApplication

	Public Class ProviderSiteContact

		Protected id As Integer
		Protected name As String
		Protected address1 As String
		Protected address2 As String
		Protected address3 As String
		Protected address4 As String
        Protected postcode As String
        Protected lattitude As Double
        Protected longitude As Double
		Protected telephone As String
		Protected fax As String
		Protected email As String
		Protected contact As String

		''' <summary>
		''' 
		''' </summary>
		''' <param name="id"></param>
		''' <remarks></remarks>

		Public Sub setID(ByVal id As Integer)
			Me.id = id
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getID() As Integer
			Return Me.id
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="name"></param>
		''' <remarks></remarks>

		Public Sub setName(ByVal name As String)
			Me.name = name
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getName() As String
			Return Me.name
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="address1"></param>
		''' <remarks></remarks>

		Public Sub setAddress1(ByVal address1 As String)
			Me.address1 = address1
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getAddress1() As String
			Return Me.address1
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="address2"></param>
		''' <remarks></remarks>

		Public Sub setAddress2(ByVal address2 As String)
			Me.address2 = address2
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getAddress2() As String
			Return Me.address2
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="address3"></param>
		''' <remarks></remarks>

		Public Sub setAddress3(ByVal address3 As String)
			Me.address3 = address3
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getAddress3() As String
			Return Me.address3
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="address4"></param>
		''' <remarks></remarks>

		Public Sub setAddress4(ByVal address4 As String)
			Me.address4 = address4
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getAddress4() As String
			Return Me.address4
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setPostcode(ByVal value As String)
			Me.postcode = value
		End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function getPostcode() As String
            Return Me.postcode
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="value"></param>
        ''' <remarks></remarks>

        Public Sub setLattitude(ByVal value As Double)
            Me.lattitude = value
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function getLattitude() As Double
            Return Me.lattitude
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="value"></param>
        ''' <remarks></remarks>

        Public Sub setLongitude(ByVal value As Double)
            Me.longitude = value
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function getLongitude() As Double
            Return Me.longitude
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="value"></param>
        ''' <remarks></remarks>

		Public Sub setTelephone(ByVal value As String)
			Me.telephone = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getTelephone() As String
			Return Me.telephone
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setFax(ByVal value As String)
			Me.fax = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getFax() As String
			Return Me.fax
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setEmail(ByVal value As String)
			Me.email = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getEmail() As String
			Return Me.email
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setContact(ByVal value As String)
			Me.contact = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getContact() As String
			Return Me.contact
		End Function

	End Class

End Namespace
