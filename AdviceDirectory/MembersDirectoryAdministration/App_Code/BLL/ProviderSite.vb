Imports Microsoft.VisualBasic
Imports ProvidersMainTableAdapters

Namespace MDApplication

	''' <summary>
	''' Class holds information and methods related to sites of each provider
	''' extended from ProviderSiteContact having common fields related to
	''' contact details.
	''' </summary>
	''' <remarks>
	''' Any changes in database fields of TBL_PROVIDER_SITE must be updated
	''' in this class.
	''' </remarks>

	Public Class ProviderSite
		Inherits ProviderSiteContact

		''' <summary>
		''' This blocks declares all related fields for ProviderSite.
		''' </summary>
		''' <remarks></remarks>

		Private pid As Integer
		Private datetimeofopening As String
		Private serviceid As Integer
		Private nearesttram As String
		Private nearesttrain As String
		Private nearestbus As String
		Private parking As Boolean
		Private parkingdetails As String
		Private directions As String
		Private appointmentcontact As String
		Private openinghours As String
		Private closelunch As Boolean
		Private lunchtime As String
		Private dropin As Boolean
		Private appointmentonly As Boolean
		Private telephonehelp As Boolean
		Private telehelpdetails As String
		Private webhelp As Boolean
		Private webhelpdetails As String
		Private access As New ArrayList()
		Private facilities As New ArrayList()
		Private otherfacilities As String
		Private languages As New ArrayList()
		Private otherlanguages As String
		Private specialist As String
		Private fixed As Boolean
        Private photograph As Byte()

        Private pobj As MDApplication.Provider

		''' <summary>
		''' This objects declaration block is instantiating all Table Adapter
		''' classes used for updating site information to physical database.
		''' </summary>
		''' <remarks></remarks>

		Private providerSiteTableAdapter As New ProviderSitesTableAdapter
		Private providerSiteFacilitiesTableAdapter As New ProviderSiteFacilitiesTableAdapter
		Private providerSiteAccessTableAdapter As New ProviderSiteAccessTableAdapter
		Private providerSiteLanguagesTableAdapter As New ProviderSiteLanguagesTableAdapter

		''' <summary>
		''' Object contructor
		''' </summary>
		''' <remarks>
		''' Every ArrayList declared should be cleared at startup.
		''' </remarks>

        Public Sub New(ByRef p As MDApplication.Provider)
            access.Clear()
            facilities.Clear()
            languages.Clear()
            Me.pobj = p
        End Sub

		''' <summary>
		''' getting ArrayList of Access IDs of current site.
		''' </summary>
		''' <returns>Integer()</returns>
		''' <remarks></remarks>

		Public Function getAccess() As Integer()
			Return CType(access.ToArray(GetType(Integer)), Integer())
		End Function

		''' <summary>
		''' adding access id to site access list
		''' </summary>
		''' <param name="value">Integer</param>
		''' <remarks></remarks>

		Public Sub addAccess(ByVal value As Integer)
			access.Add(value)
		End Sub

		''' <summary>
		''' reseting access list to empty
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetAccess()
			access.Clear()
		End Sub

		''' <summary>
		''' getting ArrayList of Facilities IDs of current site.
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getFacilities() As Integer()
			Return CType(facilities.ToArray(GetType(Integer)), Integer())
		End Function

		''' <summary>
		''' adding facility id to Facilities List
		''' </summary>
		''' <param name="value">Integer</param>
		''' <remarks></remarks>

		Public Sub addFacilities(ByVal value As Integer)
			facilities.Add(value)
		End Sub

		''' <summary>
		''' reseting Facilities list to empty
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetFacilities()
			facilities.Clear()
		End Sub

		''' <summary>
		''' getting ArrayList of Languages IDs of current site.
		''' </summary>
		''' <returns>Integer()</returns>
		''' <remarks></remarks>

		Public Function getLanguages() As Integer()
			Return CType(languages.ToArray(GetType(Integer)), Integer())
		End Function

		''' <summary>
		''' adding language id to language list
		''' </summary>
		''' <param name="value">Integer</param>
		''' <remarks></remarks>

		Public Sub addLanguages(ByVal value As Integer)
			languages.Add(value)
		End Sub

		''' <summary>
		''' reseting language list to empty
		''' </summary>
		''' <remarks></remarks>

		Public Sub resetLanguages()
			languages.Clear()
		End Sub

		''' <summary>
		''' getting provider id of site
		''' </summary>
		''' <returns>Integer</returns>
		''' <remarks></remarks>

		Public Function getPID() As Integer
			Return Me.pid
		End Function

		''' <summary>
		''' setting provider id
		''' </summary>
		''' <param name="value">Integer</param>
		''' <remarks></remarks>

		Public Sub setPID(ByVal value As Integer)
			Me.pid = value
		End Sub


		''' <summary>
		''' getting date and time of site opening detail
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getDateTimeofOpening() As String
			Return Me.datetimeofopening
		End Function

		''' <summary>
		''' setting date and time of site opening detail
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setDateTimeofOpening(ByVal value As String)
			Me.datetimeofopening = value
		End Sub

		''' <summary>
		''' getting service id of site
		''' </summary>
		''' <returns>Integer</returns>
		''' <remarks></remarks>

		Public Function getServiceID() As Integer
			Return Me.serviceid
		End Function

		''' <summary>
		''' setting service id of site
		''' </summary>
		''' <param name="value">Integer</param>
		''' <remarks></remarks>

		Public Sub setServiceID(ByVal value As Integer)
			Me.serviceid = value
		End Sub

		''' <summary>
		''' getting nearest tram station
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getNearestTram() As String
			Return Me.nearesttram
		End Function

		''' <summary>
		''' setting nearest tram station
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setNearestTram(ByVal value As String)
			Me.nearesttram = value
		End Sub

		''' <summary>
		''' getting nearest train station
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getNearestTrain() As String
			Return Me.nearesttrain
		End Function

		''' <summary>
		''' setting nearest train station
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setNearestTrain(ByVal value As String)
			Me.nearesttrain = value
		End Sub

		''' <summary>
		''' getting nearest bus station
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getNearestbus() As String
			Return Me.nearestbus
		End Function

		''' <summary>
		''' setting nearest bus station
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setNearestBus(ByVal value As String)
			Me.nearestbus = value
		End Sub

		''' <summary>
		''' getting parking available flag
		''' </summary>
		''' <returns>Boolean</returns>
		''' <remarks></remarks>

		Public Function getParking() As Boolean
			Return Me.parking
		End Function

		''' <summary>
		''' setting parking available flag
		''' </summary>
		''' <param name="value">Boolean</param>
		''' <remarks></remarks>

		Public Sub setParking(ByVal value As Boolean)
			Me.parking = value
		End Sub

		''' <summary>
		''' getting parking details
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getParkingDetails() As String
			Return Me.parkingdetails
		End Function

		''' <summary>
		''' setting parking details
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setParkingDetails(ByVal value As String)
			Me.parkingdetails = value
		End Sub

		''' <summary>
		''' getting directions detail
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getDirections() As String
			Return Me.directions
		End Function

		''' <summary>
		''' setting directions detail
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setDirections(ByVal value As String)
			Me.directions = value
		End Sub

		''' <summary>
		''' getting contact number for appointment
		''' </summary>
		''' <returns>String</returns>
		''' <remarks></remarks>

		Public Function getAppointmentContact() As String
			Return Me.appointmentcontact
		End Function

		''' <summary>
		''' setting contact number for appointment
		''' </summary>
		''' <param name="value">String</param>
		''' <remarks></remarks>

		Public Sub setAppointmentContact(ByVal value As String)
			Me.appointmentcontact = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getOpeningHours() As String
			Return Me.openinghours
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setOpeningHours(ByVal value As String)
			Me.openinghours = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getLunchClose() As Boolean
			Return Me.closelunch
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setLunchClose(ByVal value As Boolean)
			Me.closelunch = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getLunchTime() As String
			Return Me.lunchtime
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setLunchTime(ByVal value As String)
			Me.lunchtime = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getDropin() As Boolean
			Return Me.dropin
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setDropin(ByVal value As Boolean)
			Me.dropin = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getAppointmentsOnly() As Boolean
			Return Me.appointmentonly
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setAppointmentsOnly(ByVal value As Boolean)
			Me.appointmentonly = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getTelephoneHelp() As Boolean
			Return Me.telephonehelp
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setTelephoneHelp(ByVal value As Boolean)
			Me.telephonehelp = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getTeleHelpDetail() As String
			Return Me.telehelpdetails
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setTeleHelpDetail(ByVal value As String)
			Me.telehelpdetails = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getWebHelp() As Boolean
			Return Me.webhelp
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setWebHelp(ByVal value As Boolean)
			Me.webhelp = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getWebHelpDetail() As String
			Return Me.webhelpdetails
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setWebHelpDetail(ByVal value As String)
			Me.webhelpdetails = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getOtherFacilities() As String
			Return Me.otherfacilities
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setOtherFacilities(ByVal value As String)
			Me.otherfacilities = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getOtherLanguages() As String
			Return Me.otherlanguages
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setOtherLanguages(ByVal value As String)
			Me.otherlanguages = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getSpecialist() As String
			Return Me.specialist
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setSpecialist(ByVal value As String)
			Me.specialist = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>
		''' 
		Public Function getFixed() As Boolean
			Return Me.fixed
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setFixed(ByVal value As Boolean)
			Me.fixed = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <param name="value"></param>
		''' <remarks></remarks>

		Public Sub setPhotograph(ByVal value As Byte())
			Me.photograph = value
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>

		Public Function getPhotograph() As Byte()
			Return Me.photograph
		End Function

		''' <summary>
		''' 
		''' </summary>
		''' <remarks></remarks>

		Public Sub updateSitetoDB(ByVal sender As Object)
            If TypeOf (sender) Is MDApplication.Provider Then
                Dim cflag As Boolean = False
                'providerSiteTableAdapter.BeginTransaction()
                Try
                    Dim add As Boolean
                    Dim providerSiteTable As ProvidersMain.ProviderSitesDataTable = providerSiteTableAdapter.GetSiteBySID(id)
                    Dim providerSiteTableRow As ProvidersMain.ProviderSitesRow
                    If providerSiteTable.Count = 0 Then
                        providerSiteTable = New ProvidersMain.ProviderSitesDataTable
                        providerSiteTableRow = providerSiteTable.NewProviderSitesRow
                        add = True
                    Else
                        add = False
                        providerSiteTableRow = providerSiteTable(0)
                    End If
                    providerSiteTableRow.ProviderID = CType(sender, MDApplication.Provider).getID
                    providerSiteTableRow.SiteName = Me.name
                    providerSiteTableRow.AddressLine1 = Me.address1
                    providerSiteTableRow.AddressLine2 = Me.address2
                    providerSiteTableRow.AddressLine3 = Me.address3
                    providerSiteTableRow.AddressLine4 = Me.address4
                    If Not add Then
                        If providerSiteTableRow.PostCode <> Me.postcode Then
                            cflag = True
                        End If
                    End If
                    providerSiteTableRow.PostCode = Me.postcode
                    '-- START
                    '-- GOOGLE API DATA
                    providerSiteTableRow.lat = Me.lattitude
                    providerSiteTableRow.lon = Me.longitude
                    '-- END
                    providerSiteTableRow.Telephone = Me.telephone
                    providerSiteTableRow.Fax = Me.fax
                    providerSiteTableRow.ClientContact = Me.contact
                    providerSiteTableRow.Email = Me.email
                    providerSiteTableRow.OpeningTimes = Me.datetimeofopening
                    providerSiteTableRow.ServiceID = Me.serviceid
                    providerSiteTableRow.NearestTram = Me.nearesttram
                    providerSiteTableRow.NearestTrain = Me.nearesttrain
                    providerSiteTableRow.NearestBus = Me.nearestbus
                    providerSiteTableRow.Parking = Me.parking
                    providerSiteTableRow.ParkingDetails = Me.parkingdetails
                    providerSiteTableRow.Directions = Me.directions
                    providerSiteTableRow.AppointmentPhone = Me.appointmentcontact
                    providerSiteTableRow.OpeningHours = Me.openinghours
                    providerSiteTableRow.LunchtimeClose = Me.closelunch
                    providerSiteTableRow.Lunchtime = Me.lunchtime
                    providerSiteTableRow.DropIn = Me.dropin
                    providerSiteTableRow.Appointment = Me.appointmentonly
                    providerSiteTableRow.TelephoneHelp = Me.telephonehelp
                    providerSiteTableRow.HelpLineDetails = Me.telehelpdetails
                    providerSiteTableRow.WebHelp = Me.webhelp
                    providerSiteTableRow.WebHelpDetails = Me.webhelpdetails
                    providerSiteTableRow.FacilitiesDetails = Me.otherfacilities
                    providerSiteTableRow.OtherLanguages = Me.otherlanguages
                    providerSiteTableRow.SpecialistWorkshops = Me.specialist
                    'If Not Me.photograph Is Nothing Then
                    'Dim p As Integer
                    'For p = 0 To Me.photograph.Length - 1
                    providerSiteTableRow.Photograph = Me.photograph
                    'Next
                    '
                    'End If
                    If add Then
                        providerSiteTable.AddProviderSitesRow(providerSiteTableRow)
                    End If
                    providerSiteTableAdapter.Update(providerSiteTable)
                    If cflag Then
                        Dim pNotify As New ProviderEmailNotifier
                        pNotify.sendPCChangeNotification(Me, False)
                    End If
                    '   providerSiteTableAdapter.CommitTransaction()

                    If Me.id = 0 Then
                        Dim tproviderSiteTable As ProvidersMain.ProviderSitesDataTable = providerSiteTableAdapter.GetProviderSitesByID(Me.pid)
                        Dim tproviderSiteTableRow As ProvidersMain.ProviderSitesRow = tproviderSiteTable(0)
                        Me.id = tproviderSiteTableRow.SiteID
                    End If

                    '  providerSiteAccessTableAdapter.BeginTransaction()
                    Dim providerSiteAccessTable As ProvidersMain.ProviderSiteAccessDataTable = providerSiteAccessTableAdapter.GetSiteAccessByID(id)
                    Dim providerSiteAccessRow As ProvidersMain.ProviderSiteAccessRow
                    If providerSiteAccessTable.Count > 0 Then
                        For Each typerow As ProvidersMain.ProviderSiteAccessRow In providerSiteAccessTable
                            providerSiteAccessTableAdapter.Delete(typerow.SiteID, typerow.AccessID)
                        Next
                    End If
                    providerSiteAccessTable = New ProvidersMain.ProviderSiteAccessDataTable
                    Dim st As Integer
                    For st = 0 To Me.access.Count - 1
                        providerSiteAccessRow = providerSiteAccessTable.NewProviderSiteAccessRow
                        providerSiteAccessRow.SiteID = id
                        providerSiteAccessRow.AccessID = Me.access.Item(st)
                        providerSiteAccessTable.AddProviderSiteAccessRow(providerSiteAccessRow)
                    Next
                    providerSiteAccessTableAdapter.Update(providerSiteAccessTable)
                    'providerSiteAccessTableAdapter.CommitTransaction()

                    'providerSiteFacilitiesTableAdapter.BeginTransaction()
                    Dim providerSiteFacilitiesTable As ProvidersMain.ProviderSiteFacilitiesDataTable = providerSiteFacilitiesTableAdapter.GetSiteFacilitiesByID(id)
                    Dim providerSiteFacilityRow As ProvidersMain.ProviderSiteFacilitiesRow
                    If providerSiteFacilitiesTable.Count > 0 Then
                        For Each typerow As ProvidersMain.ProviderSiteFacilitiesRow In providerSiteFacilitiesTable
                            providerSiteFacilitiesTableAdapter.Delete(typerow.SiteID, typerow.FacilityID)
                        Next
                    End If
                    providerSiteFacilitiesTable = New ProvidersMain.ProviderSiteFacilitiesDataTable
                    For st = 0 To Me.facilities.Count - 1
                        providerSiteFacilityRow = providerSiteFacilitiesTable.NewProviderSiteFacilitiesRow
                        providerSiteFacilityRow.SiteID = id
                        providerSiteFacilityRow.FacilityID = Me.facilities.Item(st)
                        providerSiteFacilitiesTable.AddProviderSiteFacilitiesRow(providerSiteFacilityRow)
                    Next
                    providerSiteFacilitiesTableAdapter.Update(providerSiteFacilitiesTable)
                    'providerSiteFacilitiesTableAdapter.CommitTransaction()

                    'providerSiteLanguagesTableAdapter.BeginTransaction()
                    Dim providerSiteLanguageTable As ProvidersMain.ProviderSiteLanguagesDataTable = providerSiteLanguagesTableAdapter.GetSiteLanguagesByID(id)
                    Dim providerSiteLanguageRow As ProvidersMain.ProviderSiteLanguagesRow
                    If providerSiteLanguageTable.Count > 0 Then
                        For Each typerow As ProvidersMain.ProviderSiteLanguagesRow In providerSiteLanguageTable
                            providerSiteLanguagesTableAdapter.Delete(typerow.SiteID, typerow.LanguageID)
                        Next
                    End If
                    providerSiteLanguageTable = New ProvidersMain.ProviderSiteLanguagesDataTable
                    For st = 0 To Me.languages.Count - 1
                        providerSiteLanguageRow = providerSiteLanguageTable.NewProviderSiteLanguagesRow
                        providerSiteLanguageRow.SiteID = id
                        providerSiteLanguageRow.LanguageID = Me.languages.Item(st)
                        providerSiteLanguageTable.AddProviderSiteLanguagesRow(providerSiteLanguageRow)
                    Next
                    providerSiteLanguagesTableAdapter.Update(providerSiteLanguageTable)
                    'providerSiteLanguagesTableAdapter.CommitTransaction()
                Catch ex As Exception
                    'If providerSiteLanguagesTableAdapter.Connection.State = Data.ConnectionState.Open Then
                    '    providerSiteLanguagesTableAdapter.RollbackTransaction()
                    'End If
                    'If providerSiteFacilitiesTableAdapter.Connection.State = Data.ConnectionState.Open Then
                    '    providerSiteFacilitiesTableAdapter.RollbackTransaction()
                    'End If
                    'If providerSiteAccessTableAdapter.Connection.State = Data.ConnectionState.Open Then
                    '    providerSiteAccessTableAdapter.RollbackTransaction()
                    'End If
                    'If providerSiteTableAdapter.Connection.State = Data.ConnectionState.Open Then
                    '    providerSiteTableAdapter.RollbackTransaction()
                    'End If
                End Try
            End If
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <remarks></remarks>

		Public Sub deleteSitefromDB()
			Try
				providerSiteTableAdapter.BeginTransaction()
				providerSiteTableAdapter.Delete(Me.id)
				providerSiteTableAdapter.CommitTransaction()
			Catch ex As Exception
				If providerSiteTableAdapter.Connection.State = Data.ConnectionState.Open Then
					providerSiteTableAdapter.RollbackTransaction()
				End If
			End Try
		End Sub

		''' <summary>
		''' 
		''' </summary>
		''' <remarks></remarks>

		Public Sub fillSiteFromDB()
			Dim providerSiteTable As ProvidersMain.ProviderSitesDataTable = providerSiteTableAdapter.GetSiteBySID(id)
			For Each providersitesrow As ProvidersMain.ProviderSitesRow In providerSiteTable
				Me.name = providersitesrow.SiteName
				Me.address1 = providersitesrow.AddressLine1
				Me.serviceid = providersitesrow.ServiceID
				If Not providersitesrow.IsAddressLine2Null() Then
					Me.address2 = providersitesrow.AddressLine2
				End If
				If Not providersitesrow.IsAddressLine3Null() Then
					Me.address3 = providersitesrow.AddressLine3
				End If
				If Not providersitesrow.IsAddressLine4Null() Then
					Me.address4 = providersitesrow.AddressLine4
				End If
                Me.postcode = providersitesrow.PostCode
                '-- START
                '-- GOOGLE API DATA
                Me.lattitude = providersitesrow.lat
                Me.longitude = providersitesrow.lon
                '-- END
				If Not providersitesrow.IsTelephoneNull() Then
					Me.telephone = providersitesrow.Telephone
				End If
				If Not providersitesrow.IsFaxNull() Then
					Me.fax = providersitesrow.Fax
				End If
				If Not providersitesrow.IsClientContactNull() Then
					Me.contact = providersitesrow.ClientContact
				End If
				If Not providersitesrow.IsEmailNull() Then
					Me.email = providersitesrow.Email
				End If
				If Not providersitesrow.IsOpeningTimesNull() Then
					Me.datetimeofopening = providersitesrow.OpeningTimes
				End If
				If Not providersitesrow.IsNearestTramNull() Then
					Me.nearesttram = providersitesrow.NearestTram
				End If
				If Not providersitesrow.IsNearestTrainNull() Then
					Me.nearesttrain = providersitesrow.NearestTrain
				End If
				If Not providersitesrow.IsNearestBusNull() Then
					Me.nearestbus = providersitesrow.NearestBus
				End If
				Me.parking = providersitesrow.Parking
				If providersitesrow.Parking Then
					If Not providersitesrow.IsParkingDetailsNull() Then
						Me.parkingdetails = providersitesrow.ParkingDetails
					End If
				End If
				If Not providersitesrow.IsDirectionsNull() Then
					Me.directions = providersitesrow.Directions
				End If
				If Not providersitesrow.IsAppointmentPhoneNull() Then
					Me.appointmentcontact = providersitesrow.AppointmentPhone
				End If
				If Not providersitesrow.IsOpeningHoursNull() Then
					Me.openinghours = providersitesrow.OpeningHours
				End If
				Me.closelunch = providersitesrow.LunchtimeClose
				If providersitesrow.LunchtimeClose Then
					If Not providersitesrow.IsLunchtimeNull() Then
						Me.lunchtime = providersitesrow.Lunchtime
					End If
				End If

				Me.dropin = providersitesrow.DropIn
				Me.appointmentonly = providersitesrow.Appointment
				Me.telephonehelp = providersitesrow.TelephoneHelp
				If providersitesrow.TelephoneHelp Then
					If Not providersitesrow.IsHelpLineDetailsNull() Then
						Me.telehelpdetails = providersitesrow.HelpLineDetails
					End If
				End If
				Me.webhelp = providersitesrow.WebHelp
				If providersitesrow.WebHelp Then
					If Not providersitesrow.IsWebHelpDetailsNull() Then
						Me.webhelpdetails = providersitesrow.WebHelpDetails
					End If
				End If
				Me.resetAccess()
				Dim siteAccessTable As ProvidersMain.ProviderSiteAccessDataTable = providerSiteAccessTableAdapter.GetSiteAccessByID(id)
				If siteAccessTable.Rows.Count > 0 Then
					For Each siteaccessrow As ProvidersMain.ProviderSiteAccessRow In siteAccessTable
						Me.addAccess(siteaccessrow.AccessID)
					Next
				End If
				Me.resetFacilities()
				Dim siteFacilitiesTable As ProvidersMain.ProviderSiteFacilitiesDataTable = providerSiteFacilitiesTableAdapter.GetSiteFacilitiesByID(id)
				If siteFacilitiesTable.Rows.Count > 0 Then
					For Each sitefacilitiesrow As ProvidersMain.ProviderSiteFacilitiesRow In siteFacilitiesTable
						Me.addFacilities(sitefacilitiesrow.FacilityID)
					Next
				End If
				If Not providersitesrow.IsFacilitiesDetailsNull() Then
					Me.otherfacilities = providersitesrow.FacilitiesDetails
				End If
				Me.resetLanguages()
				Dim siteLanguagesTable As ProvidersMain.ProviderSiteLanguagesDataTable = providerSiteLanguagesTableAdapter.GetSiteLanguagesByID(id)
				If siteLanguagesTable.Rows.Count > 0 Then
					For Each sitelanguagesrow As ProvidersMain.ProviderSiteLanguagesRow In siteLanguagesTable
						Me.addLanguages(sitelanguagesrow.LanguageID)
					Next
				End If
				If Not providersitesrow.IsOtherLanguagesNull() Then
					Me.otherlanguages = providersitesrow.OtherLanguages
				End If
				If Not providersitesrow.IsSpecialistWorkshopsNull() Then
					Me.specialist = providersitesrow.SpecialistWorkshops
				End If
				If Not providersitesrow.IsPhotographNull Then
					'Dim p As Integer
					'For p = 0 To providersitesrow.Photograph.Length - 1
					Me.photograph = providersitesrow.Photograph
					'Next
				End If
			Next
		End Sub

	End Class

End Namespace
