<%@ Page Language="VB" AutoEventWireup="false" CodeFile="amendprovider.aspx.vb" Inherits="addamendprovider" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add Amend Provider</title>
    <link rel="stylesheet" href="StyleSheet.css" />
    <link rel="stylesheet" href="MembersArea.css" />
    <style type="text/css">
        #map
        {
	        height: 1px;
	        width: 1px;
	        display:none;
        }
    </style>

    <script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="Google/gmap.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

    function PCode(){
        if (document.getElementById("txtPostcode").value != "")
	    {
	        return isUKPostCode ("txtPostcode", "You must enter a valid postcode")
	    }
	    else {
	        document.getElementById("map").style.display = "none";
	        document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
	    }
    }

    function isUKPostCode (itemName, errName){
	    elVal = document.getElementById(itemName).value;
	    elVal = TrimAll(elVal.toUpperCase());
	    result = elVal.match(/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) [0-9][ABD-HJLNP-UW-Z]{2})$/g);
	    if (!result) {
		    targetError(itemName,"red");
		    document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
		    document.getElementById("map").style.display = "none"
		    return false;
		    }
	    else {
		    elVal = String(elVal.replace(/\s/g,""));
		    elValLen = elVal.length;
		    elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		    document.getElementById(itemName).value = elVal;
		    targetError(itemName,"black");
		    //document.getElementById("map").style.display = "block";
		    usePointFromPostcode(document.getElementById('txtPostcode').value, setLatLng);
		    return true;
		    }
	    }

    function TrimAll(str) {
	    return LTrimAll(RTrimAll(str));
	    }

    function LTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	    return str.substring(i,str.length);
	    }

    function RTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	    return str.substring(0,i+1);
	    }

    function targetError(iItem, iColor){
	    document.getElementById(iItem).style.color = iColor
	    document.getElementById(iItem).focus();
	    }

    </script>

</head>
<body class="gbody">
	<script type ="text/javascript" language ="javascript" src="calendarFunctions.js"></script>
    <form id="wfrmAddAmendProvider" runat="server">
    <div class="panel-css">
		<asp:SqlDataSource ID="dsServicesType" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT [TypeID], [TypeName] FROM [TBL_MD_SERVICETYPE]" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsTarget" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT TargetName, TargetID FROM TBL_MD_TARGET" EnableViewState="False">
		</asp:SqlDataSource>
		<asp:SqlDataSource ID="dsOrgType" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT TypeID, TypeName FROM TBL_MD_TYPE" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsArea" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT AreaID, AreaName FROM TBL_MD_AREA" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsServiceAvailable_LearningSkills" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT ServiceID, ServiceName FROM TBL_MD_SERVICEAVAILABLE where ServiceTypeID=1" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsServiceAvailable_General" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT ServiceID, ServiceName FROM TBL_MD_SERVICEAVAILABLE where ServiceTypeID=2" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsServiceAvailable_Specialist" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT ServiceID, ServiceName FROM TBL_MD_SERVICEAVAILABLE where ServiceTypeID=3" EnableViewState="False"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsFunding" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT SourceID, SourceName FROM TBL_MD_FUNDINGSOURCE" EnableViewState="False">
		</asp:SqlDataSource>
		<asp:Panel ID="panHeader" runat="server" CssClass="panel" Width="98%">
			<table style="width: 100%">
				<tr>
					<td colspan="3" style="width: 70%; text-align: left">
						<asp:Label ID="lblOrgName" runat="server" Font-Bold="True"></asp:Label>&nbsp;</td>
					<td style="text-align: right; width: 30%;">
						<asp:Button ID="btnCancel" runat="server" ForeColor="Red" Text="Cancel Updates" CssClass="inputbutton" />&nbsp;</td>
				</tr>
				<tr>
					<td class="ten" colspan="4" style="height: 37px">
						<hr />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="panErrors" runat="server" CssClass="panel" Width="90%">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="4">
						<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Page Validation Error See * for Detail"
							ValidationGroup="Section1" />
						<asp:ValidationSummary ID="ValidationSummary2" runat="server" HeaderText="Page Validation Error See * for Detail"
							ValidationGroup="Section2" />
						<asp:ValidationSummary ID="ValidationSummary3" runat="server" HeaderText="Page Validation Error See * for Detail"
							ValidationGroup="Section3" />
						<asp:ValidationSummary ID="ValidationSummary4" runat="server" HeaderText="Page Validation Error See * for Detail"
							ValidationGroup="Section4" />
						<asp:ValidationSummary ID="ValidationSummary5" runat="server" HeaderText="Page Validation Error See * for Detail"
							ValidationGroup="Section5" />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="panSection1" runat="server" CssClass="panel" Width="90%">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="2">
						<strong>1) Organisation Details</strong></td>
					<td class="forty">
					</td>
					<td class="ten" style="vertical-align: top; width: 10%;">
					</td>
				</tr>
				<tr>
				<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label13" runat="server" Text="Summary of Organisation :"></asp:Label>
						<span style="font-size: 8pt; color: #993333">(please provide a brief summary explaining yout organisations service using no more than 100 characters)</span>
					</td>
					<td class="forty">
						<asp:TextBox 
						    ID="txtProviderSummary" 
						    runat="server"
						    Height="100px" 
						    width="100%" 
						    MaxLength="1000" 
						    Enabled="True" 
						    EnableViewState="False" 
						    CssClass="datebox" 
						    TextMode="MultiLine">
						</asp:TextBox>
					</td>
					<td class="ten" style="vertical-align: top; width: 10%;">
							</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label1" runat="server" Text="Name of Organisation :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtOrgName" runat="server" MaxLength="100" Enabled="True" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
							</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label2" runat="server" Text="Address Line 1 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress1" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						<asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1"
							Display="Dynamic" ErrorMessage="Address Line 1 is Mandatory" ValidationGroup="Section1">*</asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label3" runat="server" Text="Address Line 2 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress2" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label4" runat="server" Text="Address Line 3 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress3" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label5" runat="server" Text="Address Line 4 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress4" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label6" runat="server" Text="Postcode :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtPostcode" runat="server" MaxLength="8" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<asp:HiddenField ID="hiddenLat" runat="server" EnableViewState="False"></asp:HiddenField>
                        <asp:HiddenField ID="hiddenLng" runat="server" EnableViewState="False"></asp:HiddenField>
						<div id="map"></div>
                    </td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						<asp:RegularExpressionValidator ID="revPostCode" runat="server" ControlToValidate="txtPostcode"
							Display="Dynamic" ErrorMessage="Postcode is not in valid UK format" ValidationExpression="^[A-Za-z]{1,2}[0-9A-Za-z]{1,2}[ ]?[0-9]{0,1}[A-Za-z]{2}$"
							ValidationGroup="Section1">*</asp:RegularExpressionValidator>
						<asp:RequiredFieldValidator ID="rfvPostcode" runat="server" ControlToValidate="txtPostcode"
							Display="Dynamic" ErrorMessage="Postcode is Mandatory" ValidationGroup="Section1">*</asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label7" runat="server" Text="Telephone Number :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtTelephone" runat="server" MaxLength="20" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						<asp:RegularExpressionValidator ID="revTelephone" runat="server" ControlToValidate="txtTelephone"
							Display="Dynamic" ErrorMessage="Telephone number format error" ValidationExpression="(\s*\(?0\d{4}\)?(\s*|-)\d{3}(\s*|-)\d{3}\s*)|(\s*\(?0\d{3}\)?(\s*|-)\d{3}(\s*|-)\d{4}\s*)|(\s*(7|8)(\d{7}|\d{3}(\-|\s{1})\d{4})\s*) "
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label8" runat="server" Text="Fax Number :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtFax" runat="server" MaxLength="20" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						<asp:RegularExpressionValidator ID="revFax" runat="server" ControlToValidate="txtFax"
							Display="Dynamic" ErrorMessage="Fax number format error" ValidationExpression="(\s*\(?0\d{4}\)?(\s*|-)\d{3}(\s*|-)\d{3}\s*)|(\s*\(?0\d{3}\)?(\s*|-)\d{3}(\s*|-)\d{4}\s*)|(\s*(7|8)(\d{7}|\d{3}(\-|\s{1})\d{4})\s*) "
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label9" runat="server" Text="Website Address :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtURL" runat="server" MaxLength="100" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						<asp:RegularExpressionValidator ID="revUrl" runat="server" ControlToValidate="txtURL"
							Display="Dynamic" ErrorMessage="URL format error" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label10" runat="server" Text="E-Mail Address :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtEmail" runat="server" MaxLength="100" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						<asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
							Display="Dynamic" ErrorMessage="Email format error" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label11" runat="server" Text="Client Contact :"></asp:Label>
						<span style="font-size: 8pt; color: #993333">(please provide details of a named individual
							who clients can contact direct)</span></td>
					<td class="forty">
						<asp:TextBox ID="txtContactName" runat="server" MaxLength="80" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label12" runat="server" Text="Website Administrator :"></asp:Label>
						<span style="font-size: 8pt; color: #993333">(please provide details of a named individual
							who will be responsible for keeping your organisation's website information up-to-date)</span></td>
					<td class="forty">
						<asp:TextBox ID="txtWebAdmin" runat="server" MaxLength="80" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="vertical-align: top; width: 10%;">
						</td>
				</tr>
			</table>
			<br />
			<table style="width:100%">
				<tr>
					<td style="width: 10%">
					</td>
					<td style="width: 60%">
				Area in which Services Offered:</td>
					<td style="width: 30%">
						<asp:CustomValidator ID="cvServices" runat="server" Display="Dynamic" ErrorMessage="At least one service area must be selected" ValidateEmptyText="True" ValidationGroup="Section1">*</asp:CustomValidator></td>
				</tr>
				<tr>
				<td style="width: 10%">
				</td>
					<td style="width: 60%; text-align: right">
						<asp:CheckBoxList ID="cbArea" runat="server" TextAlign="Left" CellPadding="5" DataSourceID="dsArea" DataTextField="AreaName" DataValueField="AreaID" RepeatColumns="2" Width="100%" EnableViewState="False">
						</asp:CheckBoxList></td>
					<td style="width: 30%">
						</td>
				</tr>
			</table>
			<p class="btnnext">
				<asp:Button ID="btnSave1" runat="server" Text="Save & Next" ValidationGroup="Section1" CssClass="inputbutton" />&nbsp;</p>
		</asp:Panel>
		<asp:Panel ID="panSection2" runat="server" CssClass="panel" Width="90%" Visible="False">
			<table style="width: 100%" id="tblOrgType" runat="server">
				<tr>
					<td>
						<strong>2.Type of Organisation (please select one box only)</strong>
					    <asp:CustomValidator ID="cvOrgType" runat="server" 
					        ControlToValidate="rbOrgType"
						    Display="Dynamic" 
						    ErrorMessage="Must select one organisation type" 
						    ValidateEmptyText="True" 
						    ValidationGroup="Section2">*</asp:CustomValidator>
					</td>
				</tr>
				<tr>					
					<td style="text-align: right;">
					    <asp:RadioButtonList 
					        ID="rbOrgType" 
					        runat="server" 
					        RepeatColumns="2" 
					        TextAlign="Left" 
					        CellPadding="5" 
					        DataSourceID="dsOrgType" 
					        DataTextField="TypeName" 
					        DataValueField="TypeID" 
					        Width="100%" 
					        EnableViewState="False" 
					        CausesValidation="True">
				        </asp:RadioButtonList>
					</td>	
				</tr>
			</table>				
			<br />
			<table style="width:100%" id="tblServiceType" runat="server">
				<tr>
					<td class="fifty" style="height: 21px">
						<strong>3) Type of Service (please select all that apply)</strong>
						<asp:CustomValidator 
						    ID="cvServiceType" 
						    runat="server" 
						    ErrorMessage="At least one service type must be selected" 
						    Display="Dynamic" 
						    ValidationGroup="Section2">*</asp:CustomValidator>
					</td>
				</tr>
				<tr>
					<td class="ninty" style="text-align: right">
						<asp:CheckBoxList 
						    ID="cbServiceType" 
						    runat="server" 
						    CellPadding="5" 
						    TextAlign="Left" 
						    DataSourceID="dsServicesType" 
						    DataTextField="TypeName" 
						    DataValueField="TypeID" 
						    Width="100%" 
						    EnableViewState="False">
						</asp:CheckBoxList>
					</td>
				</tr>
			</table>
			<br />
			<table style="width:100%">
				<tr>
					<td class="fifty" colspan="2">
						<strong>4) Quality Assurance Accreditation</strong><div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:109px; z-index:20; visibility: hidden'></div></td>
				</tr>
				<tr>
					<td style="width: 50%; vertical-align:middle">
						Matrix Accredited :</td>
					<td class="fifty">
						<asp:RadioButtonList ID="rbMatrix" runat="server" RepeatDirection="Horizontal" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date awarded :</td>
					<td class="fifty">
						<asp:TextBox ID="txtMatrixDate" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick ="YY_Calendar('txtMatrixDate',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<%--<input type="image" name="ibtnDateAwarded" title="Click to choose from calender" src="icon_cal.gif" style="border-width:0px;" onclick ="NewCal('ibtnDateAwarded','ddmmyyyy');" id="Image1"/>--%>
						<asp:CustomValidator ID="cvMatrixAward" runat="server"
							Display="Dynamic" ErrorMessage="Matrix award date required or not valid" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date re-assessment is due :</td>
					<td class="fifty">
						<asp:TextBox ID="txtMatrixDue" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<%--<a href="javascript:;" onclick ="YY_Calendar('txtMatrixDue',260,360,'de','#FFFFFF','#FDBF83','YY_calendar1')">Pick Date</a>--%>
						<a href="javascript:;" onclick="YY_Calendar('txtMatrixDue',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<%--<input type="image" name="ibtnDateDue" title="Click to choose from calender" src="icon_cal.gif" style="border-width:0px;" onclick = "NewCal('ibtnDateDue','ddmmyyyy');"/>--%>
						<asp:CustomValidator ID="cvMatrixDue" runat="server"
							Display="Dynamic" ErrorMessage="Matrix due date required or not valid" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 50%;; vertical-align:middle">
						Ofsted Inspected:</td>
					<td class="fifty">
						<asp:RadioButtonList ID="rbOfsted" runat="server" RepeatDirection="Horizontal" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date awarded :</td>
					<td class="fifty">
						<asp:TextBox ID="txtOfstedDate" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick="YY_Calendar('txtOfstedDate',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<asp:CustomValidator ID="cvOfstedDate" runat="server"
							Display="Dynamic" ErrorMessage="Ofsted inspection date required or not valid" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date re-inspection is due :</td>
					<td class="fifty">
						<asp:TextBox ID="txtOfstedDue" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick="YY_Calendar('txtOfstedDue',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<asp:CustomValidator ID="cvOfstedInspectionDue" runat="server"
							Display="Dynamic" ErrorMessage="Ofsted re-inspection date required or not valid" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 50%;; vertical-align:middle">
						Customer Service Excellence Standard Accredited:</td>
					<td class="fifty">
						<asp:RadioButtonList ID="rbExcellenceAccredited" runat="server" RepeatDirection="Horizontal" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date awarded :</td>
					<td class="fifty">
						<asp:TextBox ID="txtExcellenceAccreditedDate" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick="YY_Calendar('txtExcellenceAccreditedDate',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<asp:CustomValidator ID="cvExcellenceAccreditedDate" runat="server"
							Display="Dynamic" ErrorMessage="Customer Service Excellence Standard Accredited date required or not valid" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date re-inspection is due :</td>
					<td class="fifty">
						<asp:TextBox ID="txtExcellenceAccreditedDueDate" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick="YY_Calendar('txtExcellenceAccreditedDueDate',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<asp:CustomValidator ID="cvExcellenceAccreditedDueDate" runat="server"
							Display="Dynamic" ErrorMessage="Customer Service Excellence re-inspection date required or not valid" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 50%; vertical-align:middle">Other Accreditation:</td>
					<td class="fifty">
						<asp:RadioButtonList ID="rbOtherAccreditation" runat="server" RepeatDirection="Horizontal" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date awarded :</td>
					<td class="fifty">
						<asp:TextBox ID="txtOtherAccreditationDate" 
						    runat="server" 
						    MaxLength="10" 
						    Width="100px" 
						    Wrap="False" 
						    EnableViewState="False" 
						    CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick="YY_Calendar('txtOtherAccreditationDate',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<asp:CustomValidator 
						    ID="cvOtherAccreditationDate" 
						    runat="server"
							Display="Dynamic" 
							ErrorMessage="Other Accreditation date required or not valid" 
							ValidationGroup="Section2">*</asp:CustomValidator>
						</td>
				</tr>
				<tr>
					<td style="width: 50%">
						Please state date re-assessment is due :</td>
					<td class="fifty">
						<asp:TextBox ID="txtOtherAccreditationDueDate" runat="server" MaxLength="10" Width="100px" Wrap="False" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<a href="javascript:;" onclick="YY_Calendar('txtOtherAccreditationDueDate',260,360,'de','#FFFFFF','#0099CC','Calendar1')"><img src="icon_cal.gif" alt="Pick Date" style="vertical-align: text-bottom"/></a>
						<asp:CustomValidator 
						    ID="cvOtherAccreditationDueDate" 
						    runat="server"
							Display="Dynamic" 
							ErrorMessage="Other Accreditation re-assessment date required or not valid" 
							ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
				    <td colspan="2">
				            Please Specify (other)
							<asp:CustomValidator 
							    ID="cvOtherAccreditationSpecified" 
							    runat="server" 
							    Display="Dynamic" 
							    ErrorMessage="Please Specify 'Other Accreditation'"
								ValidationGroup="Section2">*</asp:CustomValidator>
							<asp:TextBox 
							    ID="txtOtherAccreditationSpecified" 
							    runat="server" 
							    EnableViewState="False" 
							    Height="100px"
							    MaxLength="1000" 
							    TextMode="MultiLine" 
							    Width="100%" 
							    CssClass="datebox">
							</asp:TextBox>
				    </td>
				</tr>
			</table>
			<br />
			<table style="width:100%">
				<tr>
					<td colspan="2" style="height: 21px">
						<strong>5) Charges</strong></td>
				</tr>
				<tr>
					<td style="height: 21px" colspan="2">
						Information and advice should be provided free of charge to all client groups in
						the Greater Manchester Area. Are there any other services for which you charge clients?
						(e.g. Guidance, Psychometric testing)</td>
				</tr>
				<tr>
					<td style="height: 21px; width: 80%;">
						<asp:RadioButtonList ID="rbCharges" runat="server" RepeatDirection="Horizontal" TextAlign="Left" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList>If charges are made please provide details of which clients are charged, a menu of
service fees and any subsidies for special client groups.</td>
					<td style="height: 21px">
						<asp:CustomValidator ID="cvCharges" runat="server" ErrorMessage="Charges details required" ControlToValidate="txtChargesDetail" Display="Dynamic" ValidationGroup="Section2">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td colspan="2" style="height: 21px">
						<asp:TextBox ID="txtChargesDetail" runat="server" Height="100px" MaxLength="1000"
							TextMode="MultiLine" Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
				</tr>
			</table>
			<p class="btnnext">
				<asp:Button ID="btnSection2Prev" runat="server" Text="Save & Prev." ValidationGroup="Section2" CssClass="inputbutton" />
				<asp:Button ID="btnSave2" runat="server" Text="Save & Next" ValidationGroup="Section2" CssClass="inputbutton" /></p>
		</asp:Panel>
		<asp:Panel ID="panSection6" runat="server" CssClass="panel" Width="90%" Visible="False">
			<table style="width: 100%" id="tblServiceAvailable" runat="server">
				<tr>
					<td class="ten">
						<strong>6) Services Available (please mark all relevant boxes)</strong>
						<asp:CustomValidator 
						    ID="cvServiceAvailable" 
						    runat="server" 
						    Display="Dynamic" 
						    ErrorMessage="At least one avaiable service must be selected" 
						    ValidationGroup="Section3">*</asp:CustomValidator>
					</td>
				</tr>
				<tr>
				    <td><strong>Learning & Skills</strong></td>
				</tr>
				<tr>
					<td style="width: 100%; text-align: right;">
						<asp:CheckBoxList 
						    ID="cbServiceAvailable_LS" 
						    RepeatColumns="2" 
						    runat="server" 
						    CellPadding="5" 
						    TextAlign="Left" 
						    DataSourceID="dsServiceAvailable_LearningSkills" 
						    DataTextField="ServiceName" 
						    DataValueField="ServiceID" 
						    Width="100%" 
						    EnableViewState="False">
						</asp:CheckBoxList>
					</td>
				</tr>
				<tr>
				    <td>
				            Please Specify (other)
							<asp:CustomValidator 
							    ID="cvLEARNINGSKILLSOTHER" 
							    runat="server" 
							    Display="Dynamic" 
							    ErrorMessage="Please Specify 'FE - HE - Other'"
								ValidationGroup="Section3">*</asp:CustomValidator>
							<asp:TextBox ID="txtLEARNINGSKILLSOTHER" 
							    runat="server" 
							    EnableViewState="False" 
							    Height="100px"
							    MaxLength="1000" 
							    TextMode="MultiLine" 
							    Width="100%" 
							    CssClass="datebox">
							</asp:TextBox>
				    </td>
				</tr>
				<tr>
				    <td><strong>General</strong></td>
				</tr>				
				<tr>
					<td style="width: 100%; text-align: right;">
						<asp:CheckBoxList 
						    ID="cbServiceAvailable_G" 
						    RepeatColumns="2" 
						    runat="server" 
						    CellPadding="5" 
						    TextAlign="Left" 
						    DataSourceID="dsServiceAvailable_General" 
						    DataTextField="ServiceName" 
						    DataValueField="ServiceID" 
						    Width="100%" 
						    EnableViewState="False">
						</asp:CheckBoxList>
					</td>
				</tr>
				<tr>
				    <td><strong>Specialist</strong></td>
				</tr>
				<tr>
					<td style="width: 100%; text-align: right;">
						<asp:CheckBoxList 
						    ID="cbServiceAvailable_S" 
						    RepeatColumns="2" 
						    runat="server" 
						    CellPadding="5" 
						    TextAlign="Left" 
						    DataSourceID="dsServiceAvailable_Specialist" 
						    DataTextField="ServiceName" 
						    DataValueField="ServiceID" 
						    Width="100%" 
						    EnableViewState="False">
						</asp:CheckBoxList>
					</td>
				</tr>
				<tr>
				    <td>
				            Please Specify (other)
							<asp:CustomValidator 
							    ID="cvSPECIALISTOTHER" 
							    runat="server" 
							    Display="Dynamic" 
							    ErrorMessage="Please Specify 'Other'"
								ValidationGroup="Section3">*</asp:CustomValidator>
							<asp:TextBox 
							    ID="txtSPECIALISTOTHER" 
							    runat="server" 
							    EnableViewState="False" 
							    Height="100px"
							    MaxLength="1000" 
							    TextMode="MultiLine" 
							    Width="100%" 
							    CssClass="datebox">
							</asp:TextBox>
				    </td>
				</tr>
			</table>
			<br />
			<p class="btnnext">
				<asp:Button ID="btnSection6Prev" runat="server" Text="Save & Prev." ValidationGroup="Section3" CssClass="inputbutton" />
				<asp:Button ID="btnSave6" runat="server" Text="Save & Next" ValidationGroup="Section3" CssClass="inputbutton" />
			</p>
		</asp:Panel>
		<asp:Panel ID="panSection7" runat="server" CssClass="panel" Width="90%" Visible="False">
			<table style="width: 100%">
				<tr>
					<td class="ten">
						<strong>7a). Is your service targeted at specific client groups?</strong>
						<asp:CustomValidator 
						    ID="cvTarget" 
						    runat="server" 
						    ErrorMessage="Target groups must be selected" 
						    Display="Dynamic" 
						    ValidationGroup="Section4">*</asp:CustomValidator>
					</td>
				</tr>
				<tr>
					<td style="height: 21px;">
						<asp:RadioButtonList 
						    ID="rbTarget" 
						    runat="server" 
						    RepeatDirection="Horizontal" 
						    TextAlign="Left" 
						    CellPadding="5" 
						    EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
			</table>
			<table style="width: 100%" id="tblTarget" runat="server">
				<tr>
					<td class="ten">
						<strong>If yes please complete the rest of this section below (mark all that apply).</strong>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">
						<table width="100%">
							<tr>
								<td style="width: 50%; text-align: left;">
									<strong>Target Groups</strong>
								</td>
							</tr>
							<tr>
								<td style="width: 50%">
									<asp:CheckBoxList 
									    ID="cbTargetGroup" 
									    RepeatColumns="2" 
									    runat="server" 
									    DataSourceID="dsTarget" 
									    DataTextField="TargetName"
										DataValueField="TargetID" 
										TextAlign="Left" 
										CellPadding="5" 
										Width="100%" 
										EnableViewState="False">
									</asp:CheckBoxList>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="3" style="height: 20px">
						<strong>Service Targeted at Specific District/ Ward
							<asp:CustomValidator 
							    ID="cvTargetWards" 
							    runat="server" 
							    Display="Dynamic" 
							    ErrorMessage="Please provide districts/wards detail"
								ValidationGroup="Section4">*</asp:CustomValidator>
							</strong>
						</td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						<asp:TextBox ID="txtTargetWard" runat="server" EnableViewState="False" Height="100px"
							MaxLength="1000" TextMode="MultiLine" Width="100%" CssClass="datebox"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						<strong>7b). Is your service targeted at specific client groups (e.g. offenders, BME communities, etc.)
							<asp:CustomValidator ID="cvSpecialist" runat="server" Display="Dynamic" ErrorMessage="Specialist service details must be filled"
								ValidationGroup="Section4">*</asp:CustomValidator></strong></td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						<asp:RadioButtonList ID="rbSpecialist" runat="server" RepeatDirection="Horizontal" TextAlign="Left" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						<strong>If yes please give detail below</strong></td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						<asp:TextBox ID="txtSpecialistDetail" runat="server" EnableViewState="False" Height="100px"
							MaxLength="1000" TextMode="MultiLine" Width="100%" CssClass="datebox"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						<strong>Other - please provide information on any other eligible clients</strong></td>
				</tr>
				<tr>
					<td style="width: 100%; height: 21px;">
						<asp:TextBox ID="txtOtherTarget" runat="server" Height="100px" MaxLength="1000" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
				</tr>
			</table>
			<br />
			<p class="btnnext">
				<asp:Button ID="btnSection7Prev" runat="server" Text="Save & Prev." ValidationGroup="Section4" CssClass="inputbutton" />
				<asp:Button ID="btnSave7" runat="server" Text="Save & Next" ValidationGroup="Section4" CssClass="inputbutton" /></p>
		</asp:Panel>
		<asp:Panel ID="panSection8" runat="server" CssClass="panel" Width="410px" Visible="False">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="3">
						<strong>8) Premises</strong></td>
				</tr>
				<tr>
					<td style="width: 100%;">
						Do you provide services at more than one site?<asp:CustomValidator ID="cvPremises"
							runat="server" Display="Dynamic" ErrorMessage="Must select to fill main first site" ValidationGroup="Premises">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="width: 100%">
						<asp:RadioButtonList ID="rbSites" runat="server" RepeatDirection="Horizontal" CellPadding="5" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList><br />
						<asp:CheckBox ID="cbSameSite" runat="server" ForeColor="Red" Text="Use Organisation Details for First Site" EnableViewState="False" /></td>
				</tr>
			</table>
			<br />
			<p class="btnnext">
				<asp:Button ID="btnSection8Prev" runat="server" Text="Save & Prev." ValidationGroup="Premises" CssClass="inputbutton" />
				<asp:Button ID="btnSave8" runat="server" Text="Save & Next" ValidationGroup="Premises" CssClass="inputbutton" /></p>
		</asp:Panel>
		<asp:Panel ID="panSection9" runat="server" CssClass="panel" Width="90%" Visible="False">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="4" style="width: 80%; text-align: left;">
						<strong>9) Funding</strong><div id='Calendar2' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:109px; z-index:20; visibility: hidden'></div></td>
				</tr>
				<tr>
					<td style="width: 80%; text-align: left;" colspan="2">
						Please provide details of where funding for your service is received from and the
						end date for funding (as appropriate)<asp:CustomValidator ID="cvFunding" runat="server"
							ErrorMessage="Funding source date must be valid" Display="Dynamic" ValidationGroup="Section5">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; width: 80%; text-align: right">
						<span style="text-decoration: underline">Funding Sources </span>
					</td>
					<td style="width: 40%">
						<span style="text-decoration: underline">Date funding due to end</span></td>
				</tr>
				<tr>
					<td style="width: 80%; text-align: right">
						<asp:CheckBoxList ID="cbFunding" runat="server" CellPadding="5" DataSourceID="dsFunding"
							DataTextField="SourceName" DataValueField="SourceID" TextAlign="Left" EnableViewState="False">
						</asp:CheckBoxList>&nbsp;
					</td>
					<td style="width: 40%">
						<table style="width: 100%" id="tblFundingEnd" runat="server">
							<tr>
								<td>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width: 80%; text-align: left">
						Funding source specific details.
						<asp:CustomValidator ID="cvFundingSpec" runat="server"
							Display="Dynamic" ErrorMessage="Specific funding source details required" ValidationGroup="Section5">*</asp:CustomValidator></td>
					<td style="width: 20%">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: left; width: 80%;">
						<asp:TextBox 
						    ID="txtFunding" 
						    runat="server" 
						    EnableViewState="False" 
						    Height="100px"
							MaxLength="1000" 
							TextMode="MultiLine" 
							Width="100%" 
							CssClass="datebox">
						</asp:TextBox>
					</td>
				</tr>
			</table>
			<br />
			<p class="btnnext">
				<asp:Button ID="btnSection9Prev" runat="server" Text="Save & Prev." ValidationGroup="Section5" CssClass="inputbutton" />
				<asp:Button ID="btnSave9" runat="server" Text="Save & Next" ValidationGroup="Section5" CssClass="inputbutton" /></p>
		</asp:Panel>
		<div id="divScript" runat="server">
		</div>
		<asp:Panel ID="panSection10" runat="server" CssClass="panel" Width="90%" Visible="False">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="3">
						<strong>10) Future Developments</strong></td>
				</tr>
				<tr>
					<td class="ten" colspan="3">
						Please provide details of any future developments which may affect your current levels of service provision (e.g. relocation, site closure, merger etc.)</td>
				</tr>
				<tr>
					<td style="width: 100%;">
						<asp:TextBox 
						    ID="txtFutureIAG" 
						    runat="server" 
						    Height="100px" 
						    MaxLength="1000" 
						    TextMode="MultiLine"
							Width="100%" 
							EnableViewState="False" 
							CssClass="datebox">
						</asp:TextBox>
					</td>
				</tr>
				<tr>
					<td style="width: 100%">
						<strong>11) Any other information
						</strong>
					</td>
				</tr>
				<tr>
					<td style="width: 100%">
						Please state any other information not requested which you feel relevant to your service provision and its entry on the Advice Directory.</td>
				</tr>
				<tr>
					<td style="width: 100%">
						<asp:TextBox 
						    Height="100px" 
						    ID="txtAnyOther" 
						    MaxLength="1000" 
						    runat="server" 
						    TextMode="MultiLine" 
						    Width="100%" 
						    EnableViewState="False" 
						    CssClass="datebox" />
					</td>
				</tr>
			</table>
			<br />
			<p class="btnnext">
				<asp:Button ID="btnSection10Prev" runat="server" Text="Save & Prev." CssClass="inputbutton" />
				&nbsp;<asp:Button ID="btnSaveSubmit" runat="server" Text="Save & Submit" CssClass="inputbutton" /></p>
		</asp:Panel>
		&nbsp;
    </div>
    </form>
</body>
</html>