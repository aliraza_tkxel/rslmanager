<%@ Page Language="VB" AutoEventWireup="true" CodeFile="viewprovider.aspx.vb" Inherits="viewprovider" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Provider View</title>
    <link rel="stylesheet" type="text/css" title="default" id="default" href="/css/100FontSize.css" />
    <link rel="alternate stylesheet" type="text/css" title="style1" href="/css/150FontSize.css" />
    <link rel="alternate stylesheet" type="text/css" title="style2" href="/css/200FontSize.css" />
    <link rel="alternate stylesheet" type="text/css" title="style3" href="/css/style3.css" />
</head>
<body style="background-color:#EEEEEE">
 		<asp:Panel ID="panmain" runat="server" CssClass="panel-css">
 		<form id="wfrmViewProvider" runat="server">
			<table style="width: 98%" border="0">
				<tr>
					<td colspan="2">
						<asp:Label ID="lblOrgName" runat="server" Font-Bold="True"></asp:Label></td>
					<td style="text-align: right;">
						<asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="inputbutton" />
						<asp:Button ID="btnAmend" runat="server" Text="Amend" CssClass="inputbutton" /></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 21px">
						<hr />
					</td>
				</tr>
				<tr>
					<td style="height: 21px" colspan="2">
						<strong>1. Organisation Details</strong></td>
					<td> </td>
				</tr>
				<tr>
					<td colspan="3">
						<table style="width: 100%">
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Organisation Summary :</td>
								<td style="width: 50%">
									<asp:Label ID="lblOrgSummary" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Organisation Name :</td>
								<td style="width: 50%">
									<asp:Label ID="lblOname" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Address :</td>
								<td style="width: 50%">
									<asp:Label ID="lblAddress1" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
								</td>
								<td style="width: 50%">
									<asp:Label ID="lblAddress2" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
								</td>
								<td style="width: 50%">
									<asp:Label ID="lblAddress3" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
								</td>
								<td style="width: 50%">
									<asp:Label ID="lblAddress4" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Postcode :</td>
								<td style="width: 50%">
									<asp:Label ID="lblPostcode" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Telephone :</td>
								<td style="width: 50%">
									<asp:Label ID="lblTelephone" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Fax :</td>
								<td style="width: 50%">
									<asp:Label ID="lblFax" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Website URL :</td>
								<td style="width: 50%">
									<asp:Label ID="lblUrl" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									E Mail :</td>
								<td style="width: 50%">
									<asp:Label ID="lblEmail" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 40%">
									Client Contact :</td>
								<td style="width: 50%">
									<asp:Label ID="lblClient" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%; height: 21px;">
								</td>
								<td style="width: 40%; height: 21px;">
									Website Administrator :</td>
								<td style="width: 50%; height: 21px;">
									<asp:Label ID="lblWebAdmin" runat="server"></asp:Label></td>
							</tr>
							<tr>
								<td style="width: 10%" valign="top">
								</td>
								<td style="width: 40%" valign="top">
									Area in which services are offered :</td>
								<td style="width: 50%" valign="top">
									<table style="width: 100%" id="tblServiceArea" runat="server">
										
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>2. Type of Organisation</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					    <table style="width: 100%" id="tblOrgType" runat="server">
						    <tr>
							    <td style="width: 10%"> </td>
							    <td style="width: 90%">	</td>
						    </tr>
					    </table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>3. Type of Service</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3"><table style="width: 100%" id="tblServiceType" runat="server">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 90%">
								
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>4. Quality Assurance Accreditation</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					<table style="width: 100%" id="tblQuality" runat="server">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 90%">
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height: 21px;">
						<strong>5. Charges</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="height: 21px">
					<table style="width: 100%" id="tblCharges" runat="server">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 90%">
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>6. Services Available</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					<table style="width: 100%" id="tblServiceAvaiable" runat="server">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 90%">
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>7. Client Groups Eligible to Access Services</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table style="width: 100%" id="tblTarget" runat="server">
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 90%">
								</td>
							</tr>
						</table>
						<table style="width: 100%" id="tblOtherTarget">
							<tr>
								<td style="width: 10%">
								</td>
								<td style="width: 90%">
									<asp:Label ID="lblOtherTarget" runat="server"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height: 21px;">
						<strong>8. Premises &amp; Sites</strong></td>
					<td style="height: 21px;">
					</td>
				</tr>
				<tr>
					<td colspan="3"><table style="width: 100%" runat = "server" id="tblProviderSite">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 40%">
							</td>
							<td style="width: 50%">
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>9. Funding Sources</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					    <table style="width: 100%" id="tblFunding" runat="server">
						    <tr>
							    <td style="width: 10%">
							    </td>
							    <td style="width: 60%">
							    </td>
							    <td style="width: 30%">
							    </td>
						    </tr>
					    </table>
					    <table style="width: 100%" id="tblFundingDetail">
						    <tr>
							    <td style="width: 10%">
							    </td>
							    <td style="width: 60%">
								    <asp:Label ID="lblFundingDetail" runat="server"></asp:Label></td>
							    <td style="width: 30%">
							    </td>
						    </tr>
					    </table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>10. Future IAG Developments</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3"><table style="width: 100%" id="tblIAG" runat="server">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 90%">
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>11. Any Other Information</strong></td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					<table style="width: 100%" id="tblAnyOther" runat="server">
						<tr>
							<td style="width: 10%">
							</td>
							<td style="width: 90%">
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>			 
        </form>
    </asp:Panel>
</body>
</html>