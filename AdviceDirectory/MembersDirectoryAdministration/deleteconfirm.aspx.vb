''' <summary>
''' class for code behind for deleteconfirm.aspx
''' </summary>
''' <remarks></remarks>
Partial Class deleteconfirm
	Inherits MSDN.SessionPage

	Private confirmpage As String

	''' <summary>
	''' page load method to reset display UI
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>
	''' display is build by reading context items passed by calling page.
	''' </remarks>

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			If Context.Items("confirm") Is Nothing Then
				lblMessage.Text = "Error is dialog"
				btnOK.Enabled = False
				btnCancel.Enabled = False
			Else
				hfURL.Value = Context.Items("confirmpage")
				lblMessage.Text = Context.Items("confirmtext")
			End If
		End If
	End Sub

	''' <summary>
	''' ok button event implementaion
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>
	''' context item for confirm is set to true and transfered to refering URL
	''' </remarks>

	Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
		Context.Items.Add("confirm", True)
		Context.Items.Add("confirmresult", True)
		Server.Transfer(hfURL.Value)
	End Sub

	''' <summary>
	''' cancel button event implementaion
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>
	''' context item for confirm is set to false and transfered to refering URL
	''' </remarks>

	Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		Context.Items.Add("confirm", True)
		Context.Items.Add("confirmresult", False)
		Server.Transfer(hfURL.Value)
	End Sub
End Class
