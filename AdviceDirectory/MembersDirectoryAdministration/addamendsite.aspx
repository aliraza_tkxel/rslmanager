<%@ Page Language="VB" AutoEventWireup="false" CodeFile="addamendsite.aspx.vb" Inherits="addamendsite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add Amend Provider Site</title>
    <link rel ="Stylesheet" href = "StyleSheet.css" />
    <link rel="stylesheet" href="MembersArea.css" />
    <style type="text/css">
        #map
        {
	        height: 1px;
	        width: 1px;
	        display:none;
        }
    </style>

    <script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=AIzaSyBfAwpN1S3CD4PRLJOA-WF4mBMASfan4JU"
        type="text/javascript"></script>

    <script src="Google/gmap.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

    function PCode(){
        if (document.getElementById("txtPostcode").value != "")
	    {
	        return isUKPostCode ("txtPostcode", "You must enter a valid postcode")
	    }
	    else {
	        document.getElementById("map").style.display = "none";
	        document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
	    }
    }

    function isUKPostCode (itemName, errName){
	    elVal = document.getElementById(itemName).value;
	    elVal = TrimAll(elVal.toUpperCase());
	    result = elVal.match(/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) [0-9][ABD-HJLNP-UW-Z]{2})$/g);
	    if (!result) {
		    targetError(itemName,"red");
		    document.getElementById("hiddenLat").value = "";
	        document.getElementById("hiddenLng").value = "";
		    document.getElementById("map").style.display = "none"
		    return false;
		    }
	    else {
		    elVal = String(elVal.replace(/\s/g,""));
		    elValLen = elVal.length;
		    elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		    document.getElementById(itemName).value = elVal;
		    targetError(itemName,"black");
		    //document.getElementById("map").style.display = "block";
		    usePointFromPostcode(document.getElementById('txtPostcode').value, setLatLng);
		    return true;
		    }
	    }

    function TrimAll(str) {
	    return LTrimAll(RTrimAll(str));
	    }

    function LTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	    return str.substring(i,str.length);
	    }

    function RTrimAll(str) {
	    if (str==null){return str;}
	    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	    return str.substring(0,i+1);
	    }

    function targetError(iItem, iColor){
	    document.getElementById(iItem).style.color = iColor
	    document.getElementById(iItem).focus();
	    }

    </script>

</head>
<body class="gbody">
    <form id="wfrmProviderSite" runat="server">
    <div class="panel-css">
		<asp:SqlDataSource ID="dsSiteServices" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT [TypeID], [TypeName] FROM [TBL_MD_PROVIDER_SITE_SERVICETYPE]" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsSiteAccess" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT AccessID, Description FROM TBL_MD_ACCESS" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsSiteLanguages" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT LanguageID, LanguageName FROM TBL_MD_LANGUAGES" EnableViewState="False"></asp:SqlDataSource>
		<asp:SqlDataSource ID="dsSiteFacilities" runat="server" ConnectionString="<%$ ConnectionStrings:nextstep Greater ManchesterConnectionString %>"
			SelectCommand="SELECT FacilityID, Description FROM TBL_MD_FACILITIES" EnableViewState="False"></asp:SqlDataSource>
		<asp:Panel ID="panHeader" runat="server" CssClass="panel" Width="98%">
			<table style="width: 100%">
				<tr>
					<td class="forty" colspan="3" style="width: 50%; text-align: left">
						<asp:Label ID="lblOrgName" runat="server" Font-Bold="True"></asp:Label>&nbsp;</td>
					<td class="ten" style="text-align: right; width: 50%;">
						&nbsp;<asp:Button ID="btnDelete" runat="server" ForeColor="Red" Text="Delete" CssClass="inputbutton" />&nbsp;
					</td>
				</tr>
				<tr>
					<td class="ten" colspan="4">
						<hr />
					</td>
				</tr>
				</table>
		</asp:Panel>
		<asp:Panel ID="panerrors" runat="server" CssClass="panel" Width="98%">
			<table style="width: 100%">
				<tr>
					<td>
						<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Validation Failed see * for details"
							ValidationGroup="Section1" />
						<asp:ValidationSummary ID="ValidationSummary2" runat="server" HeaderText="Validation Failed see * for details"
							ValidationGroup="Section2" />
						<asp:ValidationSummary ID="ValidationSummary3" runat="server" HeaderText="Validation Failed see * for details"
							ValidationGroup="Section3" />
						<asp:ValidationSummary ID="ValidationSummary4" runat="server" HeaderText="Validation Failed see * for details"
							ValidationGroup="Section4" />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="panFooter" runat="server" CssClass="panel" Width="98%">
			<table style="width: 100%">
				<tr>
					<td style="text-align: center">
						<asp:Button ID="btnFirstSite" runat="server" Enabled="False" Text="<< First Site" CssClass="inputbutton" Width="95px" />
						<asp:Button ID="btnPrevSite" runat="server" Enabled="False" Text="< Prev. Site" CssClass="inputbutton" Width="95px" />
						<asp:Button ID="btnNextSite" runat="server" Enabled="False" Text="Next Site >" CssClass="inputbutton" Width="95px" />
						<asp:Button ID="btnLastSite" runat="server" Enabled="False" Text="Last Site >>" CssClass="inputbutton" Width="95px" /></td>
				</tr>
				<tr>
					<td>
						<hr />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="panSection8a" runat="server" CssClass="panel" Width="90%">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="3">
						<strong>1) Site Details</strong></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label1" runat="server" Text="Name of Site :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtOrgName" runat="server" MaxLength="100" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
						<asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtOrgName"
							Display="Dynamic" ErrorMessage="Site name is Mandatory" ValidationGroup="Section1">*</asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label2" runat="server" Text="Address Line 1 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress1" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
						<asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1"
							Display="Dynamic" ErrorMessage="Address Line1 is Mandatory" ValidationGroup="Section1">*</asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label3" runat="server" Text="Address Line 2 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress2" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label4" runat="server" Text="Address Line 3 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress3" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label5" runat="server" Text="Address Line 4 :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtAddress4" runat="server" MaxLength="30" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label6" runat="server" Text="Postcode :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtPostcode" runat="server" MaxLength="8" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						<asp:HiddenField ID="hiddenLat" runat="server" EnableViewState="False"></asp:HiddenField>
                        <asp:HiddenField ID="hiddenLng" runat="server" EnableViewState="False"></asp:HiddenField>						
						<div id="map"></div>
					</td>
					<td class="ten">
						<asp:RequiredFieldValidator ID="rfvPostcode" runat="server" ControlToValidate="txtPostcode"
							Display="Dynamic" ErrorMessage="Postcode is Mandatory" ValidationGroup="Section1">*</asp:RequiredFieldValidator>
						<asp:RegularExpressionValidator ID="revPostcode" runat="server" ControlToValidate="txtPostcode"
							ErrorMessage="Postcode is not valid UK format" ValidationExpression="^[A-Za-z]{1,2}[0-9A-Za-z]{1,2}[ ]?[0-9]{0,1}[A-Za-z]{2}$"
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label7" runat="server" Text="Telephone Number :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtTelephone" runat="server" MaxLength="20" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
						<asp:RegularExpressionValidator ID="revTelephone" runat="server" ControlToValidate="txtTelephone"
							Display="Dynamic" ErrorMessage="Telephone number format error" ValidationExpression="(\s*\(?0\d{4}\)?(\s*|-)\d{3}(\s*|-)\d{3}\s*)|(\s*\(?0\d{3}\)?(\s*|-)\d{3}(\s*|-)\d{4}\s*)|(\s*(7|8)(\d{7}|\d{3}(\-|\s{1})\d{4})\s*) "
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label8" runat="server" Text="Fax Number :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtFax" runat="server" MaxLength="20" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
						<asp:RegularExpressionValidator ID="revFax" runat="server" ControlToValidate="txtFax"
							Display="Dynamic" ErrorMessage="Fax number format error" ValidationExpression="(\s*\(?0\d{4}\)?(\s*|-)\d{3}(\s*|-)\d{3}\s*)|(\s*\(?0\d{3}\)?(\s*|-)\d{3}(\s*|-)\d{4}\s*)|(\s*(7|8)(\d{7}|\d{3}(\-|\s{1})\d{4})\s*) "
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label11" runat="server" Text="Client Contact :"></asp:Label>
						<span style="font-size: 8pt; color: #993333">(please provide details of a named individual
							at this site)</span></td>
					<td class="forty">
						<asp:TextBox ID="txtContactName" runat="server" MaxLength="80" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label10" runat="server" Text="E-Mail Address :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtEmail" runat="server" MaxLength="100" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
						<asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
							Display="Dynamic" ErrorMessage="Email format error" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
							ValidationGroup="Section1">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:Label ID="Label9" runat="server" Text="Date / Times of Opening :"></asp:Label></td>
					<td class="forty">
						<asp:TextBox ID="txtDateTime" runat="server" Height="50px" MaxLength="1000" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>				
			</table>
			<p class="btnnext">
				<asp:Button ID="btnSection8Prev" runat="server" Text="Save & Prev." ValidationGroup="Section1" CssClass="inputbutton" />
				<asp:Button ID="btnSave8" runat="server" Text="Save & Next" ValidationGroup="Section1" CssClass="inputbutton" /></p>
		</asp:Panel>
		<asp:Panel ID="panSection8b" runat="server" CssClass="panel" Visible="False" Width="90%">
			<table style="width: 100%" border="0">
				<tr>
					<td colspan="4">
						<strong>2) Nearest station or stop</strong></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						Tram :</td>
					<td>
						<asp:TextBox 
						    ID="txtTram" 
						    runat="server" 
						    MaxLength="80" 
						    EnableViewState="False" 
						    CssClass="datebox">
						</asp:TextBox>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						Train :</td>
					<td>
						<asp:TextBox 
						    ID="txtTrain" 
						    runat="server" 
						    MaxLength="80" 
						    EnableViewState="False" 
						    CssClass="datebox">
					    </asp:TextBox>
					</td>
					<td>&nbsp;</td>	
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						Bus :</td>
					<td>
						<asp:TextBox ID="txtBus" runat="server" MaxLength="80" EnableViewState="False" CssClass="datebox"></asp:TextBox>
						</td>
		            <td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						Parking Facilities :</td>
					<td>
						<asp:RadioButtonList ID="rbParking" runat="server" CellPadding="5" RepeatDirection="Horizontal"
							TextAlign="Left" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList>
						</td>
			        <td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						If Yes, please give details. (<span style="color: #ff0000">Max. 1000 Char.</span>)</td>
					<td>
						<asp:CustomValidator ID="cvParking" runat="server" ControlToValidate="rbParking"
							Display="Dynamic" ErrorMessage="Please provide parking details" ValidationGroup="Section2">*</asp:CustomValidator>
					</td>
					</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<asp:TextBox 
						    ID="txtParking" 
						    runat="server" 
						    Height="100px" 
						    MaxLength="1000" 
						    TextMode="MultiLine"
							Width="100%" 
							EnableViewState="False" 
							CssClass="datebox">
						</asp:TextBox>
					</td>
				    <td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						Directions : (please use common landmarks).( <span style="color: #ff0000">Max 2000 Char.</span>)
						</td>
			        <td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<asp:TextBox ID="txtDirections" runat="server" Height="100px" MaxLength="2000" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox>
							</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<p class="btnnext">
				<asp:Button ID="btnSection8bPrev" runat="server" Text="Save & Prev." ValidationGroup="Section2" CssClass="inputbutton" />
				<asp:Button ID="btnSave8b" runat="server" Text="Save & Next" ValidationGroup="Section2" CssClass="inputbutton" /></p>
		</asp:Panel>
		<asp:Panel ID="panSection8c" runat="server" CssClass="panel" Visible="False" Width="90%">
			<table style="width: 100%">
				<tr>
					<td class="forty" colspan="3">
						<strong>3) Arrangements for booking appointments</strong></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						Contact number for appointments :</td>
					<td class="forty">
						<asp:TextBox ID="txtContactApp" runat="server" MaxLength="20" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
						<asp:RegularExpressionValidator ID="revContact" runat="server" ControlToValidate="txtContactApp"
							Display="Dynamic" ErrorMessage="Telephone number format error" ValidationExpression="(\s*\(?0\d{4}\)?(\s*|-)\d{3}(\s*|-)\d{3}\s*)|(\s*\(?0\d{3}\)?(\s*|-)\d{3}(\s*|-)\d{4}\s*)|(\s*(7|8)(\d{7}|\d{3}(\-|\s{1})\d{4})\s*) "
							ValidationGroup="Section3">*</asp:RegularExpressionValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						Opening Hours :</td>
					<td class="forty">
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						<asp:TextBox ID="txtOpeningHours" runat="server" Height="100px" MaxLength="1000"
							TextMode="MultiLine" Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						Closed for lunch?</td>
					<td class="forty">
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						If Yes, please state times.</td>
					<td class="forty">
						<asp:RadioButtonList ID="rbLunch" runat="server" CellPadding="5" RepeatDirection="Horizontal"
							TextAlign="Left" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
					<td class="ten">
						<asp:CustomValidator ID="cvLunch" runat="server" ControlToValidate="rbLunch" Display="Dynamic"
							ErrorMessage="Please provide lunch timings" ValidationGroup="Section3">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						<asp:TextBox ID="txtLunchTime" runat="server" Height="100px" MaxLength="1000" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						<asp:CheckBox ID="cbDropin" runat="server" Text="Drop-in" EnableViewState="False" /></td>
					<td class="forty">
						<asp:CheckBox ID="cbAppointment" runat="server" Text="Appointment Only" EnableViewState="False" /></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						Telephone Helpline :</td>
					<td class="forty">
						<asp:RadioButtonList ID="rbTeleHelp" runat="server" CellPadding="5" RepeatDirection="Horizontal"
							TextAlign="Left" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						If Yes, please provide details. (<span style="color: #ff0000">Max 300 Char</span>)</td>
					<td class="ten">
						<asp:CustomValidator ID="cvTelephone" runat="server" ControlToValidate="rbTeleHelp"
							Display="Dynamic" ErrorMessage="Please provide telephone help detail" ValidationGroup="Section3">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						<asp:TextBox ID="txtTeleHelp" runat="server" Height="50px" MaxLength="300" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						Website Help :</td>
					<td class="forty">
						<asp:RadioButtonList ID="rbWebHelp" runat="server" CellPadding="5" RepeatDirection="Horizontal"
							TextAlign="Left" EnableViewState="False">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						If Yes, please provide details. (<span style="color: #ff0000">Max 300 Char</span>)</td>
					<td class="ten">
						<asp:CustomValidator ID="cvWebhelp" runat="server" ControlToValidate="rbWebHelp"
							Display="Dynamic" ErrorMessage="Please provide webhelp detail" ValidationGroup="Section3">*</asp:CustomValidator></td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						<asp:TextBox ID="txtWebHelp" runat="server" Height="50px" MaxLength="300" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten">
					</td>
				</tr>
			</table>
			<p class="btnnext">
				<asp:Button ID="btnSection8cPrev" runat="server" Text="Save & Prev." ValidationGroup="Section3" CssClass="inputbutton" />
				<asp:Button ID="btnSave8c" runat="server" Text="Save & Next" ValidationGroup="Section3" CssClass="inputbutton" /></p>
		</asp:Panel>
		<asp:Panel ID="panSection8d" runat="server" CssClass="panel" Visible="False" Width="90%">
			<table style="width: 100%">
				<tr>
					<td class="ten" colspan="3">
						<strong>4) Special Needs</strong></td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						ACCESS</td>
					<td class="forty">
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2" style="text-align: right">
						<asp:CheckBoxList 
					        ID="cbAccess" 
					        runat="server" 
					        CellPadding="5" 
					        TextAlign="Left" 
					        DataSourceID="dsSiteAccess" 
					        DataTextField="Description" 
					        DataValueField="AccessID" 
					        EnableViewState="False">
						</asp:CheckBoxList>
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty">
						FACILITIES</td>
					<td class="forty">
					</td>
					<td class="ten">
						<asp:CustomValidator 
						    ID="cvFacilities" 
						    runat="server" 
						    Display="Dynamic" 
						    ErrorMessage="Select at least one facility"
							ValidationGroup="Section4">*
						</asp:CustomValidator>
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2" style="text-align: right">
						<asp:CheckBoxList 
						    ID="cbFacilities" 
						    runat="server" 
						    CellPadding="5" 
						    TextAlign="Left" 
						    DataSourceID="dsSiteFacilities" 
						    DataTextField="Description" 
						    DataValueField="FacilityID" 
						    EnableViewState="False">
						</asp:CheckBoxList>
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						Any other special needs services please specify.</td>
					<td class="ten">
						<asp:CustomValidator 
						    ID="cvSpecial" 
						    runat="server" 
						    Display="Dynamic" 
						    ErrorMessage="Please provide other facility detail"
							ValidationGroup="Section4">*
						</asp:CustomValidator>
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						<asp:TextBox ID="txtFacilities" 
						    runat="server" 
						    Height="100px" 
						    MaxLength="1000" 
						    TextMode="MultiLine"
							Width="100%" 
							EnableViewState="False" 
							CssClass="datebox">
						</asp:TextBox>
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						LANGUAGES</td>
					<td class="ten">
						<asp:CustomValidator 
						    ID="cvLanguage" 
						    runat="server" 
						    Display="Dynamic" 
						    ErrorMessage="Please select a language"
							ValidationGroup="Section4">*
						</asp:CustomValidator>
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2" style="text-align: right">
						<asp:CheckBoxList 
						    ID="cbLanguages" 
						    runat="server" 
						    CellPadding="5" 
						    TextAlign="Left" 
						    DataSourceID="dsSiteLanguages" 
						    DataTextField="LanguageName" 
						    DataValueField="LanguageID" 
						    EnableViewState="False" 
						    RepeatColumns="2">
						</asp:CheckBoxList>
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						Other Languages (please specify).</td>
					<td class="ten">
						<asp:CustomValidator
						    ID="cvLanguages" 
						    runat="server" 
						    Display="Dynamic" 						  
						    ErrorMessage="Please provide other language detail"
							ValidationGroup="Section4">*
						</asp:CustomValidator>
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2">
						<asp:TextBox 
						    ID="txtLanguages" 
						    runat="server" 
						    Height="100px" 
						    MaxLength="1000" 
						    TextMode="MultiLine"
							Width="100%" 
							EnableViewState="False" 
							CssClass="datebox">
						</asp:TextBox>
					</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td colspan="2">
						Specialist workshops/events/group work/based in community venues any other support (please specify).</td>
					<td class="ten">
					</td>
				</tr>
				<tr>
					<td class="ten" style="height: 110px">
					</td>
					<td class="forty" colspan="2" style="height: 110px">
						<asp:TextBox ID="txtSpecialist" runat="server" Height="100px" MaxLength="1000" TextMode="MultiLine"
							Width="100%" EnableViewState="False" CssClass="datebox"></asp:TextBox></td>
					<td class="ten" style="height: 110px">
					</td>
				</tr>
				<tr>
					<td class="ten">
					</td>
					<td class="forty" colspan="2" style="text-align: right">
						<table id="tblFileUpload" runat="server" style="width: 100%" enableviewstate="false">
							<tr>
								<td style="width: 30%; text-align: left;">
						Site Photograph:</td>
								<td style="width: 30%; text-align: left;">
						<asp:FileUpload ID="fuSite" runat="server" EnableViewState="False" CssClass="inputbutton" />
								</td>
							</tr>
							<tr>
								<td style="width: 30%; text-align: left;">
									<span style="font-size: 8pt; color: #993333">Suggested file format is JPG having 144x144
										pixel size.</span></td>
								<td style="width: 30%; text-align: right;">
								</td>
							</tr>
						</table>
                        <br />
                        <asp:Button ID="btnDeleteImage" runat="server" CssClass="inputbutton" ForeColor="Red"
                            Text="Delete Image" Visible="False" /></td>
					<td class="ten">
					</td>
				</tr>
			</table>
			<p class="btnnext">
				<asp:Button ID="btnSection8dPrev" runat="server" Text="Save & Prev." ValidationGroup="Section4" CssClass="inputbutton" />
						<asp:Button ID="btnSaveAdd" runat="server" ForeColor="Green" Text="Save & Add Site" Enabled="False" ValidationGroup="Section4" CssClass="inputbutton" />
				<asp:Button ID="btnSave8d" runat="server" Text="Save & Next" ValidationGroup="Section4" CssClass="inputbutton" /></p>
		</asp:Panel>
    </div>
    </form>
</body>
</html>