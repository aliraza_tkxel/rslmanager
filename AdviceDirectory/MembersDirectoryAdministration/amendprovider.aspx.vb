''' <summary>
''' Code behind class for amendprovider.aspx
''' </summary>
''' <remarks>
''' Class is inherited from MSDN.SessionPage which is custome assembly in SessionUtility.dll
''' which is extended from System.Web.UI.Page 
''' SessionPage class provides functionality to share session between ASP and ASP.NET by utilising
''' SessionManager.dll on ASP side.
''' ASPSession is visible on all extending class that holds session string value to share with ASP Pages
''' To utilise this funtionality ASP Session must be turned off in IIS.
''' </remarks>

Partial Class addamendprovider
	Inherits MSDN.SessionPage

	''' <summary>
	''' variable to hold provider id
	''' </summary>
	''' <remarks></remarks>

	Private pid As Integer

	''' <summary>
	''' Instance of MDApplication.Provider class that holds data structure and methods related
	''' to provider detalis
	''' </summary>
	''' <remarks></remarks>

	Private providerObj As New MDApplication.Provider

	''' <summary>
	''' section 1 save button implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
		saveSection1()
		If IsValid Then
			panSection1.Visible = False
			panSection2.Visible = True
		End If
	End Sub

	''' <summary>
	''' method to save forms section 1 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

    Private Sub saveSection1()

        providerObj.setID(Session("pid"))


        'CK Amendment Set the name from textbox rather than label so
        'we can alter the name
        'providerObj.setName(lblOrgName.Text)
        providerObj.setName(txtOrgName.Text)

        providerObj.setProviderSummary(txtProviderSummary.Text)
        providerObj.setAddress1(txtAddress1.Text)
        providerObj.setAddress2(txtAddress2.Text)
        providerObj.setAddress3(txtAddress3.Text)
        providerObj.setAddress4(txtAddress4.Text)
        providerObj.setPostcode(txtPostcode.Text)
        providerObj.setLattitude(hiddenLat.Value)
        providerObj.setLongitude(hiddenLng.Value)
        providerObj.setTelephone(txtTelephone.Text)
        providerObj.setFax(txtFax.Text)
        providerObj.setUrl(txtURL.Text)
        providerObj.setEmail(txtEmail.Text)
        providerObj.setContact(txtContactName.Text)
        providerObj.setWebAdmin(txtWebAdmin.Text)
        providerObj.resetArea()
        Dim i As Integer
        For i = 0 To cbArea.Items.Count - 1
            If cbArea.Items(i).Selected Then
                providerObj.addAreas(cbArea.Items(i).Value)
            End If
        Next
        Session.Add("provider", providerObj)
    End Sub

	''' <summary>
	''' method to save forms section 2 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection2()
		If Not rbOrgType.SelectedValue = Nothing Then
			providerObj.setTypeID(rbOrgType.SelectedValue)
		End If
		providerObj.resetServiceType()
		Dim i As Integer
		For i = 0 To cbServiceType.Items.Count - 1
			If cbServiceType.Items(i).Selected Then
				providerObj.addServiceType(cbServiceType.Items(i).Value)
			End If
        Next
        'Matrix (Q4)
		providerObj.setMatrix(rbMatrix.SelectedValue)
		If rbMatrix.SelectedValue Then
			Try
				If txtMatrixDate.Text <> "" Then
					providerObj.setMatrixAward(Date.Parse(txtMatrixDate.Text))
				Else
					providerObj.setMatrixAward(Nothing)
				End If
				If txtMatrixDue.Text <> "" Then
					providerObj.setMatrixDue(Date.Parse(txtMatrixDue.Text))
				Else
					providerObj.setMatrixDue(Nothing)
				End If
			Catch fex As FormatException
				Console.Write(fex.Message)
			End Try
        End If
        'Ofsted (Q4)
        providerObj.setOfsted(rbOfsted.SelectedValue)
        If rbOfsted.SelectedValue Then
            Try
                If txtOfstedDate.Text <> "" Then
                    providerObj.setOfstedInspection(Date.Parse(txtOfstedDate.Text))
                Else
                    providerObj.setOfstedInspection(Nothing)
                End If
                If txtOfstedDue.Text <> "" Then
                    providerObj.setOfstedDue(Date.Parse(txtOfstedDue.Text))
                Else
                    providerObj.setOfstedDue(Nothing)
                End If
            Catch fex As FormatException
                Console.Write(fex.Message)
            End Try
        End If
        'Customer Service Excellence Standard Accredited (Q4)
        providerObj.setExcellenceAccredited(rbExcellenceAccredited.SelectedValue)
        If rbExcellenceAccredited.SelectedValue Then
            Try
                If txtExcellenceAccreditedDate.Text <> "" Then
                    providerObj.setExcellenceAccreditedDate(Date.Parse(txtExcellenceAccreditedDate.Text))
                Else
                    providerObj.setExcellenceAccreditedDate(Nothing)
                End If
                If txtExcellenceAccreditedDueDate.Text <> "" Then
                    providerObj.setExcellenceAccreditedDueDate(Date.Parse(txtExcellenceAccreditedDueDate.Text))
                Else
                    providerObj.setExcellenceAccreditedDueDate(Nothing)
                End If
            Catch fex As FormatException
                Console.Write(fex.Message)
            End Try
        End If
        'Other Accreditation (Q4)
        providerObj.setOtherAccreditation(rbOtherAccreditation.SelectedValue)
        If rbOtherAccreditation.SelectedValue Then
            Try
                If txtOtherAccreditationDate.Text <> "" Then
                    providerObj.setOtherAccreditationDate(Date.Parse(txtOtherAccreditationDate.Text))
                    providerObj.setOtherAccreditationSpecified(txtOtherAccreditationSpecified.Text)
                Else
                    providerObj.setOtherAccreditationDate(Nothing)
                    providerObj.setOtherAccreditationSpecified(Nothing)
                End If

                If txtOtherAccreditationDueDate.Text <> "" Then
                    providerObj.setOtherAccreditationDueDate(Date.Parse(txtOtherAccreditationDueDate.Text))
                Else
                    providerObj.setOtherAccreditationDueDate(Nothing)
                End If

            Catch fex As FormatException
                Console.Write(fex.Message)
            End Try
        End If

		providerObj.setCharges(rbCharges.SelectedValue)
		If rbCharges.SelectedValue Then
			providerObj.setChargesDetail(txtChargesDetail.Text)
		End If
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' method to save forms section 3 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection3()
		providerObj.resetServiceAvailable()
		Dim i As Integer
        For i = 0 To cbServiceAvailable_LS.Items.Count - 1
            If cbServiceAvailable_LS.Items(i).Selected Then
                providerObj.addServiceAvailable(cbServiceAvailable_LS.Items(i).Value)
            End If
        Next
        For i = 0 To cbServiceAvailable_G.Items.Count - 1
            If cbServiceAvailable_G.Items(i).Selected Then
                providerObj.addServiceAvailable(cbServiceAvailable_G.Items(i).Value)
            End If
        Next
        For i = 0 To cbServiceAvailable_S.Items.Count - 1
            If cbServiceAvailable_S.Items(i).Selected Then
                providerObj.addServiceAvailable(cbServiceAvailable_S.Items(i).Value)
            End If
        Next
        providerObj.setSERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER(txtLEARNINGSKILLSOTHER.Text)
        providerObj.setSERVICESAVAILABLE_SPECIALIST_OTHER(txtSPECIALISTOTHER.Text)
        Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' method to save forms section 4 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Public Sub saveSection4()
		providerObj.setTargeted(rbTarget.SelectedValue)
		If rbTarget.SelectedValue Then
			providerObj.resetTarget()
			Dim i As Integer
			For i = 0 To cbTargetGroup.Items.Count - 1
				If cbTargetGroup.Items(i).Selected Then
					providerObj.addTarget(cbTargetGroup.Items(i).Value)
				End If
			Next
			providerObj.setClietGroupWards(txtTargetWard.Text)
			providerObj.setSpecialist(rbSpecialist.SelectedValue)
			providerObj.setSpecialistDetail(txtSpecialistDetail.Text)
			providerObj.setOtherTarget(txtOtherTarget.Text)
		End If
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' method to save forms section 5 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Public Sub saveSection5()
		providerObj.setPremises(rbSites.SelectedValue)
		providerObj.setUseOrgSite(cbSameSite.Checked)
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' method to save forms section 6 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Public Sub saveSection6()
		providerObj.resetProviderFunding()
		Dim i As Integer
		For i = 0 To cbFunding.Items.Count - 1
			If cbFunding.Items(i).Selected Then
				Try
					Dim s As String = CType(Request.Form("fundingend" + i.ToString), String)
					providerObj.addProviderFunding(New MDApplication.ProviderFunding(cbFunding.Items(i).Value, Date.Parse(s)))
				Catch fex As FormatException
					Console.Write(fex.Message)
				End Try
			End If
		Next
		providerObj.setFundingDetail(txtFunding.Text)
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' method to save forms section 7 values to provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection7()
		providerObj.setFutureIAG(txtFutureIAG.Text)
		providerObj.setAnyOtherComments(txtAnyOther.Text)
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' method to fill form values from provider object data structure
	''' </summary>
	''' <remarks></remarks>

	Private Sub fillSections()
		pid = Session("pid")
		lblOrgName.Text = providerObj.getName()
		txtOrgName.Text = providerObj.getName()
		txtProviderSummary.Text = providerObj.getProviderSummary()
		txtAddress1.Text = providerObj.getAddress1()
		txtAddress2.Text = providerObj.getAddress2()
		txtAddress3.Text = providerObj.getAddress3()
		txtAddress4.Text = providerObj.getAddress4()
        txtPostcode.Text = providerObj.getPostcode()
        '-- START
        '-- Google API result is pulled from the DB
        hiddenLat.Value = providerObj.getLattitude()
        hiddenLng.Value = providerObj.getLongitude()
        '-- END
		txtTelephone.Text = providerObj.getTelephone()
		txtFax.Text = providerObj.getFax()
		txtURL.Text = providerObj.getUrl()
		txtEmail.Text = providerObj.getEmail()
		txtContactName.Text = providerObj.getContact()
		txtWebAdmin.Text = providerObj.getWebAdmin()
		Dim i As Integer, j As Integer
		Dim pa As Integer() = providerObj.getAreas()
		For i = 0 To cbArea.Items.Count - 1
			For j = 0 To pa.Length - 1
				If pa(j) = cbArea.Items(i).Value Then
					cbArea.Items(i).Selected = True
					Exit For
				End If
			Next
		Next

		For i = 0 To rbOrgType.Items.Count - 1
			If rbOrgType.Items(i).Value = providerObj.getTypeID() Then
				rbOrgType.Items(i).Selected = True
			End If
		Next
		Dim ps As Integer() = providerObj.getServiceType()
		For i = 0 To cbServiceType.Items.Count - 1
			For j = 0 To ps.Length - 1
				If ps(j) = cbServiceType.Items(i).Value Then
					cbServiceType.Items(i).Selected = True
					Exit For
				End If
			Next
        Next
        'Matix (Q4)
		If providerObj.getMatrix() Then
			rbMatrix.SelectedValue = 1
			txtMatrixDate.Text = providerObj.getMatrixAward().ToShortDateString()
			txtMatrixDue.Text = providerObj.getMatrixDue().ToShortDateString()
		Else
			rbMatrix.SelectedValue = 0
        End If
        'Ofsted (Q4)
        If providerObj.getOfsted() Then
            rbOfsted.SelectedValue = 1
            txtOfstedDate.Text = providerObj.getOfstedInspection().ToShortDateString()
            txtOfstedDue.Text = providerObj.getOfstedDue().ToShortDateString()
        Else
            rbOfsted.SelectedValue = 0
        End If
        'Other Accreditation (Q4)
        If providerObj.getOtherAccreditation() Then
            rbOtherAccreditation.SelectedValue = 1
            txtOtherAccreditationDate.Text = providerObj.getOtherAccreditationDate.ToShortDateString()
            txtOtherAccreditationDueDate.Text = providerObj.getOtherAccreditationDueDate.ToShortDateString()
            txtOtherAccreditationSpecified.Text = providerObj.getOtherAccreditationSpecified()
        Else
            rbOtherAccreditation.SelectedValue = 0
            txtOtherAccreditationSpecified.Text = Nothing
        End If
		If providerObj.getCharges() Then
			rbCharges.SelectedValue = 1
			txtChargesDetail.Text = providerObj.getChargesDetail()
		Else
			rbCharges.SelectedValue = 0
		End If

        Dim psa_G As Integer() = providerObj.getServiceAvailable()
        For i = 0 To cbServiceAvailable_G.Items.Count - 1
            For j = 0 To psa_G.Length - 1
                If psa_G(j) = cbServiceAvailable_G.Items(i).Value Then
                    cbServiceAvailable_G.Items(i).Selected = True
                    Exit For
                End If
            Next
        Next

        Dim psa_LS As Integer() = providerObj.getServiceAvailable()
        For i = 0 To cbServiceAvailable_LS.Items.Count - 1
            For j = 0 To psa_LS.Length - 1
                If psa_LS(j) = cbServiceAvailable_LS.Items(i).Value Then
                    cbServiceAvailable_LS.Items(i).Selected = True
                    Exit For
                End If
            Next
        Next

        Dim psa_S As Integer() = providerObj.getServiceAvailable()
        For i = 0 To cbServiceAvailable_S.Items.Count - 1
            For j = 0 To psa_LS.Length - 1
                If psa_S(j) = cbServiceAvailable_S.Items(i).Value Then
                    cbServiceAvailable_S.Items(i).Selected = True
                    Exit For
                End If
            Next
        Next

        If providerObj.getSERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER <> "" Then
            txtLEARNINGSKILLSOTHER.Text = providerObj.getSERVICESAVAILABLE_LEARNINGANDSKILLS_OTHER
        End If

        If providerObj.getSERVICESAVAILABLE_SPECIALIST_OTHER <> "" Then
            txtSPECIALISTOTHER.Text = providerObj.getSERVICESAVAILABLE_SPECIALIST_OTHER
        End If

        If providerObj.getTargeted() Then
            rbTarget.SelectedValue = 1
            Dim pt As Integer() = providerObj.getTarget()
            For i = 0 To cbTargetGroup.Items.Count - 1
                For j = 0 To pt.Length - 1
                    If pt(j) = cbTargetGroup.Items(i).Value Then
                        cbTargetGroup.Items(i).Selected = True
                        Exit For
                    End If
                Next
            Next
            txtOtherTarget.Text = providerObj.getOtherTarget()
        Else
            rbTarget.SelectedValue = 0
        End If
        txtTargetWard.Text = providerObj.getClientGroupWards()
        If providerObj.getSpecialist() Then
            rbSpecialist.SelectedValue = 1
            txtSpecialistDetail.Text = providerObj.getSpecialistDetails
        Else
            rbSpecialist.SelectedValue = 0
        End If

        If providerObj.getPremises() Then
            rbSites.SelectedValue = 1
        Else
            rbSites.SelectedValue = 0
        End If
        cbSameSite.Checked = providerObj.getUseOrgSite()
        divScript.InnerHtml = ""
        divScript.InnerHtml += "<script type='text/javascript' language='JavaScript'>"
        Dim pfs As MDApplication.ProviderFunding() = providerObj.getProviderFunding()
        For i = 0 To cbFunding.Items.Count - 1
            For j = 0 To pfs.Length - 1
                If pfs(j).getSource = cbFunding.Items(i).Value Then
                    cbFunding.Items(i).Selected = True
                    If panSection9.Visible Then
                        divScript.InnerHtml += "document.getElementById('fundingend" + i.ToString() + "').value='" + pfs(j).getExpire().ToShortDateString() + "';"
                    End If
                    Exit For
                End If
            Next
        Next
        divScript.InnerHtml += "</script>"
        txtFunding.Text = providerObj.getFundingDetail()
        txtFutureIAG.Text = providerObj.getFutureIAG()
        txtAnyOther.Text = providerObj.getAnyOtherComments()
    End Sub

	''' <summary>
	''' section 2 previous button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection2Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection2Prev.Click
		saveSection2()
		If IsValid Then
			panSection2.Visible = False
			panSection1.Visible = True
		End If
	End Sub

	''' <summary>
	''' section 2 forward button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave2.Click
		saveSection2()
		If IsValid Then
			panSection2.Visible = False
			panSection6.Visible = True
		End If
	End Sub

	''' <summary>
	''' section 3 previous button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection6Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection6Prev.Click
		saveSection3()
		If IsValid Then
			panSection6.Visible = False
			panSection2.Visible = True
		End If
	End Sub

	''' <summary>
	''' section 3 forward button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave6.Click
		saveSection3()
		If IsValid Then
			panSection6.Visible = False
			panSection7.Visible = True
		End If
	End Sub

	''' <summary>
	''' section 4 previous button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection7Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection7Prev.Click
		saveSection4()
		If IsValid Then
			panSection7.Visible = False
			panSection6.Visible = True
		End If
	End Sub

	''' <summary>
	''' section 5 previous button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection8Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection8Prev.Click
		panSection8.Visible = False
		panSection7.Visible = True
		saveSection5()
	End Sub

	''' <summary>
	''' section 5 forward button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave8.Click
		If IsValid Then
			providerObj.setUseOrgSite(cbSameSite.Checked)
			Context.Items.Add("forward", True)
			Session.Add("provider", providerObj)
            Server.Transfer("~/addamendsite.aspx", True)
		End If
	End Sub

	''' <summary>
	''' section 6 previous button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection9Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection9Prev.Click
		saveSection6()
		If IsValid Then
			Context.Items.Add("forward", False)
			Session.Add("provider", providerObj)
			'Context.Items.Add("pid", pid)
			Server.Transfer("~/addamendsite.aspx", True)
		End If
	End Sub

	''' <summary>
	''' section 6 forward button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave9.Click
		saveSection6()
		If IsValid Then
			panSection9.Visible = False
			panSection10.Visible = True
		End If
	End Sub

	''' <summary>
	''' section 7 previous button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection10Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection10Prev.Click
		panSection10.Visible = False
		panSection9.Visible = True
		saveSection7()
	End Sub

	'Protected Sub rbMatrix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbMatrix.SelectedIndexChanged
	'	If rbMatrix.SelectedValue Then
	'		txtMatrixDate.Enabled = True
	'		txtMatrixDue.Enabled = True
	'	Else
	'		txtMatrixDate.Enabled = False
	'		txtMatrixDue.Enabled = False
	'	End If
	'End Sub

	'Protected Sub rbCharges_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCharges.SelectedIndexChanged
	'	If rbCharges.SelectedValue Then
	'		txtChargesDetail.Enabled = True
	'	Else
	'		txtChargesDetail.Enabled = False
	'	End If
	'End Sub

	''' <summary>
	''' Page load event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        txtPostcode.Attributes.Add("onchange", "PCode();")
        'Session.Add("pid", 11)
        restoreProviderState()
        If Not IsPostBack Then
            'If ASPSession("UserID") Is Nothing Or ASPSession("UserID") = "" Then
            '	Session.Clear()
            '	Response.Redirect("/MembersLogin.asp?TimedOut=1")
            'End If
            If Context.Items("transfer") Then
                If Context.Items("forward") Then
                    panSection1.Visible = False
                    panSection9.Visible = True
                Else
                    If Context.Items("havesites") Then
                        panSection1.Visible = False
                        panSection7.Visible = True
                    Else
                        panSection1.Visible = False
                        panSection8.Visible = True
                    End If
                End If
            Else
                panSection1.Visible = True
                If Not Session("pid") Is Nothing Then
                    pid = Session("pid")
                    'Dim providers As New ProvidersMainTableAdapters.ProvidersTableAdapter
                    'Dim providersTable As ProvidersMain.ProvidersDataTable = providers.GetByID(pid)
                    'For Each providerrow As ProvidersMain.ProvidersRow In providersTable
                    providerObj.setID(pid)
                    providerObj.fillProviderFromDB()
                    Session.Add("provider", providerObj)
                    lblOrgName.Text = providerObj.getName
                    'Next
                Else
                    Response.Redirect("default.aspx")
                End If
            End If
        End If
        pid = Session("pid")
    End Sub

	''' <summary>
	''' Provider funding source form fields creation.
	''' </summary>
	''' <remarks>
	''' calander button alignment is set through css rules defined in stylesheet
	''' class funding cell
	''' </remarks>

	Private Sub fillFunding()
		Dim i As Integer
		For i = 0 To cbFunding.Items.Count - 1
			Dim row As New HtmlTableRow
			Dim cell As New HtmlTableCell
			cell.Attributes.Add("class", "fundingcell")
			cell.InnerHtml = "<input id='fundingend" + i.ToString + "' name='fundingend" + i.ToString + "' maxlength='10' size='6' type='text'/>"
			row.Cells.Add(cell)
			cell = New HtmlTableCell
			cell.Attributes.Add("class", "fundingcell")
			cell.InnerHtml = "<a href=""javascript:;"" onclick=""YY_Calendar('fundingend" + i.ToString + "',260,360,'de','#FFFFFF','#0099CC','Calendar2')""><img src='icon_cal.gif' alt='Pick Date' /></a>"
			row.Cells.Add(cell)
			tblFundingEnd.Rows.Add(row)
		Next
	End Sub

	''' <summary>
	''' section 4 forward button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>context is transfered to addamensite.aspx</remarks>

	Protected Sub btnSave7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave7.Click
		saveSection4()
		If IsValid Then
			If providerObj.getSite.Length > 0 Then
				Context.Items.Add("forward", True)
				Session.Add("provider", providerObj)
				'Context.Items.Add("pid", pid)
				Server.Transfer("~/addamendsite.aspx", True)
			Else
				panSection7.Visible = False
				panSection8.Visible = True
			End If
		End If
	End Sub

	''' <summary>
	''' refill from session state after loading page
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub Page_SaveStateComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SaveStateComplete
		restoreProviderState()
		fillFunding()
		'If Not Session("provider") Is Nothing Then
		fillSections()
		'End If
	End Sub

	''' <summary>
	''' resoring provider object from session state
	''' </summary>
	''' <remarks></remarks>

	Private Sub restoreProviderState()
		If Not (Session("provider") Is Nothing) Then
			providerObj = CType(Session("provider"), MDApplication.Provider)
		End If
	End Sub

	''' <summary>
	''' Final save and submit button click implementation.
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>provider object is physically commited to Database</remarks>

	Protected Sub btnSaveSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSubmit.Click
		saveSection7()
		providerObj.updateProviderDBTable()
		providerObj.updateSection1toDB()
		providerObj.updateSection2toDB()
		providerObj.updateSection3toDB()
		providerObj.updateSection4toDB()
		providerObj.updateSection6toDB()
		providerObj.updateSitetoDB()
    		Response.Redirect("~/PostcodeAnywhere/populate_result.asp?providerid=" + pid.ToString & "&pc=" & txtPostcode.Text)

        	Response.Redirect("~/viewprovider.aspx?providerid=" + pid.ToString)
	End Sub

	''' <summary>
	''' validation method for matrix award date
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvMatrixAward_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvMatrixAward.ServerValidate
		If rbMatrix.SelectedValue Then
			Try
				Dim td As Date
				td = Date.Parse(txtMatrixDate.Text)
				If td > Date.Today Then
					args.IsValid = False
				Else
					args.IsValid = True
				End If
			Catch ex As Exception
				If txtMatrixDate.Text = "" And rbMatrix.SelectedValue = 0 Then
					args.IsValid = True
				Else
					args.IsValid = False
				End If
			End Try
		End If
	End Sub

	''' <summary>
	''' validation method for matrix due date
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvMatrixDue_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvMatrixDue.ServerValidate
		If rbMatrix.SelectedValue Then
			Try
				Dim td As Date
				Dim ad As Date
				td = Date.Parse(txtMatrixDue.Text)
				ad = Date.Parse(txtMatrixDate.Text)
				If td < Date.Today Or td <= ad Then
					args.IsValid = False
				Else
					args.IsValid = True
				End If
			Catch ex As Exception
				If txtMatrixDue.Text = "" And rbMatrix.SelectedValue = 0 Then
					args.IsValid = True
				Else
					args.IsValid = False
				End If
			End Try
		End If
	End Sub

    ''' <summary>
    ''' validation method for Ofsted Inspection date
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvOfstedDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvOfstedDate.ServerValidate
        If rbOfsted.SelectedValue Then
            Try
                Dim td As Date
                td = Date.Parse(txtOfstedDate.Text)
                If td > Date.Today Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Catch ex As Exception
                If txtOfstedDate.Text = "" And rbOfsted.SelectedValue = 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' validation method for Ofsted Re-Inspection Date
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvOfstedInspectionDue_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvOfstedInspectionDue.ServerValidate
        If rbOfsted.SelectedValue Then
            Try
                Dim td As Date
                Dim ad As Date
                td = Date.Parse(txtOfstedDue.Text)
                ad = Date.Parse(txtOfstedDate.Text)
                If td < Date.Today Or td <= ad Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Catch ex As Exception
                If txtOfstedDue.Text = "" And rbOfsted.SelectedValue = 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' VALIDATION METHOD
    ''' Customer Service Excellence Standard Accredited (Q4)
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvExcellenceAccreditedDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvExcellenceAccreditedDate.ServerValidate
        If rbExcellenceAccredited.SelectedValue Then
            Try
                Dim td As Date
                td = Date.Parse(txtExcellenceAccreditedDate.Text)
                If td > Date.Today Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Catch ex As Exception
                If txtExcellenceAccreditedDate.Text = "" And rbExcellenceAccredited.SelectedValue = 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' VALIDATION METHOD
    ''' Customer Service Excellence Standard Accredited (Q4)
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvExcellenceAccreditedDueDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvExcellenceAccreditedDueDate.ServerValidate
        If rbExcellenceAccredited.SelectedValue Then
            Try
                Dim td As Date
                Dim ad As Date
                td = Date.Parse(txtExcellenceAccreditedDueDate.Text)
                ad = Date.Parse(txtExcellenceAccreditedDate.Text)
                If td < Date.Today Or td <= ad Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Catch ex As Exception
                If txtExcellenceAccreditedDueDate.Text = "" And rbExcellenceAccredited.SelectedValue = 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' VALIDATION METHOD
    ''' Other Accreditation (Q4)
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvOtherAccreditationDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvOtherAccreditationDate.ServerValidate
        If rbOtherAccreditation.SelectedValue Then
            Try
                Dim td As Date
                td = Date.Parse(txtOtherAccreditationDate.Text)
                If td > Date.Today Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Catch ex As Exception
                If txtOtherAccreditationDate.Text = "" And rbOtherAccreditation.SelectedValue = 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' VALIDATION METHOD
    ''' Other Accreditation (Q4)
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvOtherAccreditationDueDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvOtherAccreditationDueDate.ServerValidate
        If rbOtherAccreditation.SelectedValue Then
            Try
                Dim td As Date
                Dim ad As Date
                td = Date.Parse(txtOtherAccreditationDueDate.Text)
                ad = Date.Parse(txtOtherAccreditationDate.Text)
                If td < Date.Today Or td <= ad Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Catch ex As Exception
                If txtOtherAccreditationDueDate.Text = "" And rbOtherAccreditation.SelectedValue = 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End Try
        End If
    End Sub

	''' <summary>
	''' validation method for provider type selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvOrgType_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvOrgType.ServerValidate
		Dim i As Integer
		Dim flag As Boolean
		For i = 0 To rbOrgType.Items.Count - 1
			If rbOrgType.Items(i).Selected Then
				flag = True
			End If
		Next
		If flag Then
			args.IsValid = True
		Else
			args.IsValid = False
		End If
	End Sub

	''' <summary>
	''' validation method for provider area selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvServices_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvServices.ServerValidate
		Dim i As Integer
		Dim flag As Boolean
		For i = 0 To cbArea.Items.Count - 1
			If cbArea.Items(i).Selected Then
				flag = True
			End If
		Next
		If flag Then
			args.IsValid = True
		Else
			args.IsValid = False
		End If
	End Sub

	''' <summary>
	''' validation method for provider service type selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvServiceType_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvServiceType.ServerValidate
		Dim i As Integer
		Dim flag As Boolean
		For i = 0 To cbServiceType.Items.Count - 1
			If cbServiceType.Items(i).Selected Then
				flag = True
			End If
		Next
		If flag Then
			args.IsValid = True
		Else
			args.IsValid = False
		End If
	End Sub

	''' <summary>
	''' validation method for provider charges selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvCharges_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvCharges.ServerValidate
		If rbCharges.SelectedValue Then
			If txtChargesDetail.Text = "" Then
				args.IsValid = False
			Else
				args.IsValid = True
			End If
		End If
	End Sub

	''' <summary>
	''' validation method for provider service available selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvServiceAvailable_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvServiceAvailable.ServerValidate
		Dim i As Integer
        Dim flag_LS As Boolean
        Dim flag_G As Boolean
        Dim flag_S As Boolean

        For i = 0 To cbServiceAvailable_LS.Items.Count - 1
            If cbServiceAvailable_LS.Items(i).Selected Then
                flag_LS = True
            End If
        Next
        For i = 0 To cbServiceAvailable_G.Items.Count - 1
            If cbServiceAvailable_G.Items(i).Selected Then
                flag_G = True
            End If
        Next
        For i = 0 To cbServiceAvailable_S.Items.Count - 1
            If cbServiceAvailable_S.Items(i).Selected Then
                flag_S = True
            End If
        Next

        If ((flag_LS = True) Or (flag_G = True) Or (flag_S = True)) Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
	End Sub

	''' <summary>
	''' validation method for provider target group selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvTarget_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTarget.ServerValidate
		If rbTarget.SelectedValue Then
			Dim i As Integer
			Dim flag As Boolean
			For i = 0 To cbTargetGroup.Items.Count - 1
				If cbTargetGroup.Items(i).Selected Then
					flag = True
				End If
			Next
			If flag Then
				args.IsValid = True
			Else
				args.IsValid = False
			End If
		End If
	End Sub

	''' <summary>
	''' validation method for provider funding source 
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks>disabled method</remarks>

	Protected Sub cvFunding_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvFunding.ServerValidate
		'Dim i As Integer
		'Dim flag As Boolean
		'For i = 0 To cbFunding.Items.Count - 1
		'	If cbFunding.Items(i).Selected Then
		'		flag = True
		'	End If
		'Next
		'If flag Then
		'	args.IsValid = True
		'Else
		'	args.IsValid = False
		'End If
	End Sub

	''' <summary>
	''' validation method for provider specific funding selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvFundingSpec_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvFundingSpec.ServerValidate
		Dim i As Integer
		Dim flag As Boolean = True
		For i = 0 To cbFunding.Items.Count - 1
			If cbFunding.Items(i).Selected And cbFunding.Items(i).Text.ToUpper.Contains("PLEASE SPECIFY BELOW") Then
				If txtFunding.Text = "" Then
					flag = False
				End If
			End If
		Next
		If flag Then
			args.IsValid = True
		Else
			args.IsValid = False
		End If
	End Sub

	''' <summary>
	''' cancel update button click implementation
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		Session.Remove("provider")
  
		Response.Redirect("~/viewprovider.aspx?providerid=" + pid.ToString)
	End Sub

	''' <summary>
	''' validation method for provider site entry selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvPremises_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvPremises.ServerValidate
		If rbSites.SelectedValue = 0 Then
			If cbSameSite.Checked Then
				args.IsValid = True
			Else
				args.IsValid = False
			End If
		Else
			args.IsValid = True
		End If
	End Sub

	''' <summary>
	''' validation method for provider target group specific ward/district selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvTargetWards_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTargetWards.ServerValidate
		Dim i As Integer
		Dim flag As Boolean = True
		For i = 0 To cbTargetGroup.Items.Count - 1
			If cbTargetGroup.Items(i).Selected And cbTargetGroup.Items(i).Text.ToUpper.Contains("PEOPLE FROM SPECIFIC DISTRICTS") Then
				If txtTargetWard.Text = "" Then
					flag = False
				End If
			End If
		Next
		If flag Then
			args.IsValid = True
		Else
			args.IsValid = False
		End If
    End Sub

    ''' <summary>
    ''' validation method for Services Available - Specialist
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub cvSPECIALISTOTHER_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvSPECIALISTOTHER.ServerValidate
        Dim i As Integer
        Dim flag As Boolean = True
        For i = 0 To cbServiceAvailable_S.Items.Count - 1
            If cbServiceAvailable_S.Items(i).Selected And cbServiceAvailable_S.Items(i).Text.ToUpper.Contains("OTHER") Then
                If txtSPECIALISTOTHER.Text = "" Then
                    flag = False
                End If
            End If
        Next
        If flag Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    ''' <summary>
    ''' validation method for Services Available - Learning and Skills
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub cvLEARNINGSKILLSOTHER_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvLEARNINGSKILLSOTHER.ServerValidate
        Dim i As Integer
        Dim flag As Boolean = True
        For i = 0 To cbServiceAvailable_LS.Items.Count - 1
            If cbServiceAvailable_LS.Items(i).Selected And cbServiceAvailable_LS.Items(i).Text.ToUpper.Contains("OTHER") Then
                If txtLEARNINGSKILLSOTHER.Text = "" Then
                    flag = False
                End If
            End If
        Next
        If flag Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

	''' <summary>
	''' validation method for provider target group specialist service selection
	''' </summary>
	''' <param name="source"></param>
	''' <param name="args"></param>
	''' <remarks></remarks>

	Protected Sub cvSpecialist_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvSpecialist.ServerValidate
		If rbSpecialist.SelectedValue Then
			If txtSpecialistDetail.Text = "" Then
				args.IsValid = False
			Else
				args.IsValid = True
			End If
		End If
    End Sub

    ''' <summary>
    ''' validation method 
    ''' Other Accrediation (Q4)
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvOtherAccreditationSpecified_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvOtherAccreditationSpecified.ServerValidate
        If rbOtherAccreditation.SelectedValue Then
            If txtOtherAccreditationSpecified.Text = "" Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        End If
    End Sub


End Class
