''' <summary>
''' Class implenting code behind for getsitephoto.aspx
''' </summary>
''' <remarks></remarks>
Partial Class getsitephoto
	Inherits MSDN.SessionPage

	''' <summary>
	''' Page load method to determine how picture content is to be sent.
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>
	''' queystring may pass site id to get site photo or instruct class to obtain temporary
	''' image stream from session.
	''' </remarks>

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			If Request.QueryString("showimg") Is Nothing Then
				Dim sid As Integer
				sid = Integer.Parse(Request.QueryString("siteid"))
				Response.ContentType = "image/jpeg"
				Dim photoTableAdapter As New ProviderSiteTableAdapters.ProviderSitePhotoTableAdapter
				Dim photoTable As ProviderSite.ProviderSitePhotoDataTable = photoTableAdapter.GetSitePhotoByID(sid)
				Dim photoRow As ProviderSite.ProviderSitePhotoRow = photoTable(0)
				Response.BinaryWrite(photoRow.Photograph)
				Response.Flush()
			      Response.Close()
			ElseIf Request.QueryString("siteid") Is Nothing Then
				Dim img As Byte() = Session(Request.QueryString("showimg"))
				Response.ContentType = "image/jpeg"
				Response.BinaryWrite(img)
				Response.Flush()
			      Response.Close()
			End If
		Catch ex As Exception
			Response.Write("Image Error")
		End Try

	End Sub
End Class
