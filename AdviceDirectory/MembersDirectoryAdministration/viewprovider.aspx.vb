Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports ProviderDetailTableAdapters
Imports ProviderSiteTableAdapters

''' <summary>
''' Code behind class for amendprovider.aspx
''' </summary>
''' <remarks>
''' Class is inherited from MSDN.SessionPage which is custome assembly in SessionUtility.dll
''' which is extended from System.Web.UI.Page 
''' SessionPage class provides functionality to share session between ASP and ASP.NET by utilising
''' SessionManager.dll on ASP side.
''' ASPSession is visible on all extending class that holds session string value to share with ASP Pages
''' To utilise this funtionality ASP Session must be turned off in IIS.
''' 
''' Page accept one querystring providerid and filters
''' Providers according to id and display detail view.
''' </remarks>

Partial Public Class viewprovider
	Inherits MSDN.SessionPage

	''' <summary>
	''' Inner class for custom htmltablecell
	''' </summary>
	''' <remarks>
	''' </remarks>

	Private Class ValTableCell
		Inherits HtmlTableCell

		''' <summary>
		''' Inner Class Constructor
		''' </summary>
		''' <remarks></remarks>

		Public Sub New()
			MyBase.Width = "90%"
			MyBase.VAlign = "top"
		End Sub
	End Class

	''' <summary>
	''' Inner class for custom htmltablecell
	''' </summary>
	''' <remarks>
	''' </remarks>

	Private Class EmptyTableCell
		Inherits HtmlTableCell

		''' <summary>
		''' Inner Class Constructor
		''' </summary>
		''' <remarks></remarks>

		Public Sub New()
			MyBase.Width = "10%"
			MyBase.VAlign = "top"
		End Sub
	End Class

	''' <summary>
	''' Inner class to hold sites related table data
	''' </summary>
	''' <remarks>
	''' </remarks>

	Private Class SiteTableView

		''' <summary>
		''' ArrayList holding HTMLTableRow for site details
		''' </summary>
		''' <remarks>
		''' Must be clear while initialised
		''' </remarks>

		Private trarr As New ArrayList()

		''' <summary>
		''' class Conctructor
		''' </summary>
		''' <remarks>ArrayList set to Empty</remarks>

		Public Sub New()
			trarr.Clear()
		End Sub

		''' <summary>
		''' Method to support addition of table row to ArrayList
		''' </summary>
		''' <param name="label">Parameter for setting lable text of cell item</param>
		''' <param name="value">Parameter for setting lable value in seprate cell</param>
		''' <remarks></remarks>

		Public Sub addRow(ByVal label As String, ByVal value As String)
			Dim tr As New HtmlTableRow()
			tr.Cells.Add(New EmptyTableCell())
			Dim tdl As New HtmlTableCell()
			tdl.Width = "40%"
			tdl.VAlign = "top"
			tdl.InnerHtml = label
			Dim tdv As New HtmlTableCell()
			tdv.Width = "50%"
			tdv.VAlign = "top"
			tdv.InnerHtml = value
			tr.Cells.Add(tdl)
			tr.Cells.Add(tdv)
			trarr.Add(tr)
		End Sub

		''' <summary>
		''' Method to generate array of HTMLTableRow from ArrayList
		''' </summary>
		''' <remarks></remarks>
		''' <returns>HtmlTableRow()</returns>

		Public Function getRows() As HtmlTableRow()
			Return DirectCast(trarr.ToArray(GetType(HtmlTableRow)), HtmlTableRow())
		End Function

	End Class


	''' <summary>
	''' Holding value of providerID
	''' </summary>
	''' <remarks>
	''' </remarks>

	Private pid As String = ""

	''' <summary>
	''' Generic common declaration of table row for re use within code
	''' </summary>
	''' <remarks>
	''' must always be set to new when used
	''' </remarks>

	Private tr As New HtmlTableRow()

	''' <summary>
	''' Generic common declaration of table cell for re use within code
	''' </summary>
	''' <remarks>
	''' must always be set to new when used
	''' </remarks>

	Private td As HtmlTableCell = New ValTableCell()

	''' <summary>
	''' ASPX Page Load method 
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>Setting Initial state of page with change due to any postback</remarks>

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session.Timeout = 20
        pid = Request.QueryString("providerid")

		If Not pid Is Nothing Then
            If ASPSession("UserID") Is Nothing Or ASPSession("UserID") = "" Then
                'Response.Redirect("default.aspx")
            Else
                If ASPSession("LoginType") = "3" Or ASPSession("LoginType") = "4" Then
                    btnDelete.Enabled = False
                    'btnAmend.Enabled = False
                Else
                    If ASPSession("LoginType") = "2" Then
                        btnDelete.Enabled = True
                        'btnAmend.Enabled = True
                    Else
                        btnDelete.Enabled = False
                        btnAmend.Enabled = False
                    End If
                End If
            End If

			setProviderDetailsView()
			setProviderAreaView()
			setProviderTypeView()
			setProviderServiceTypeView()
			setProviderServiceAvailableView()
			setProviderSiteView()
			setProviderFundingView()
			'btnAmend.Visible = True
			'btnDelete.Visible = True
		Else
			Response.Redirect("default.aspx")
		End If
		If Not Context.Items("confirm") Is Nothing Then
			If Context.Items("confirmresult") Then
				deleteProvider()
				Response.Redirect("default.aspx")
			End If
		End If
	End Sub

	''' <summary>
	''' Method to display provider detail on aspx page
	''' </summary>
	''' <remarks>
	''' Most of fields must be checked for null values before diplaying
	''' as provider main table may have information that is not present
	''' in provider detail table.
	''' </remarks>

	Private Sub setProviderDetailsView()
		Dim providerDetail As New ProviderDetailsTableAdapter()
		Dim providerDetailTable As ProviderDetail.ProviderDetailsDataTable = providerDetail.GetProviderbyID(Integer.Parse(pid))
		If providerDetailTable.Count > 0 Then
			For Each providerdetailrow As ProviderDetail.ProviderDetailsRow In providerDetailTable
				If providerdetailrow.IsProviderSummaryNull = False Then
					lblOrgSummary.Text = providerdetailrow.ProviderSummary
				Else
					lblOrgSummary.Text = ""
				End If
				lblOname.Text = providerdetailrow.ProviderName
				lblOrgName.Text = providerdetailrow.ProviderName
				If Not providerdetailrow.IsAddressLine1Null() Then
					lblAddress1.Text = providerdetailrow.AddressLine1
				End If
				If Not providerdetailrow.IsAddressLine2Null Then
					lblAddress2.Text = providerdetailrow.AddressLine2
				End If
				If Not providerdetailrow.IsAddressLine3Null() Then
					lblAddress3.Text = providerdetailrow.AddressLine3
				End If
				If Not providerdetailrow.IsAddressLine4Null() Then
					lblAddress4.Text = providerdetailrow.AddressLine4
				End If
				If Not providerdetailrow.IsPostcodeNull Then
					lblPostcode.Text = providerdetailrow.Postcode
				End If
				If Not providerdetailrow.IsTelephoneNull() Then
					lblTelephone.Text = providerdetailrow.Telephone
				End If
				If Not providerdetailrow.IsFaxNull() Then
					lblFax.Text = providerdetailrow.Fax
				End If
				If Not providerdetailrow.IsURLNull() Then
					lblUrl.Text = providerdetailrow.URL
				End If
				If Not providerdetailrow.IsEmailNull() Then
					lblEmail.Text = providerdetailrow.Email
				End If
				If Not providerdetailrow.IsClientContactNull Then
					lblClient.Text = providerdetailrow.ClientContact
				End If
				If Not providerdetailrow.IsWebAdministratorNull Then
					lblWebAdmin.Text = providerdetailrow.WebAdministrator
				End If
				If Not providerdetailrow.IsOtherEligibleClientsNull Then
					lblOtherTarget.Text = "Other Eligible Clients:<br />" + providerdetailrow.OtherEligibleClients
				End If
				'Matrix (Q4)
				If Not providerdetailrow.IsMatrixAccrediationNull Then
					If providerdetailrow.MatrixAccrediation Then
						tr = New HtmlTableRow()
						td = New ValTableCell()
						td.InnerText = "Matrix Accredited : Yes"
						tr.Cells.Add(New EmptyTableCell())
						tr.Cells.Add(td)
						tblQuality.Rows.Add(tr)
						tr = New HtmlTableRow()
						td = New ValTableCell()
						tr.Cells.Add(New EmptyTableCell())
						td.InnerText = "Date Awarded : " + providerdetailrow.MatrixDate.ToShortDateString()
						tr.Cells.Add(td)
						tblQuality.Rows.Add(tr)
						tr = New HtmlTableRow()
						td = New ValTableCell()
						tr.Cells.Add(New EmptyTableCell())
						td.InnerText = "Re-Assessment Due Date : " + providerdetailrow.MatrixDueDate.ToShortDateString()
						tr.Cells.Add(td)
						tblQuality.Rows.Add(tr)
					Else
						tr = New HtmlTableRow()
						td = New ValTableCell()
						tr.Cells.Add(New EmptyTableCell())
						td.InnerText = "Matrix Accredited : No"
						tr.Cells.Add(td)
						tblQuality.Rows.Add(tr)
					End If
                End If
                'Ofsted (Q4)
                If Not providerdetailrow.IsOfstedNull Then
                    If providerdetailrow.Ofsted Then
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        td.InnerText = "Ofsted inspection : Yes"
                        tr.Cells.Add(New EmptyTableCell())
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Date Ofsted inspected : " + providerdetailrow.OfstedInspection.ToShortDateString()
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Ofsted re-inspection due : " + providerdetailrow.OfstedDueDate.ToShortDateString()
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                    Else
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Ofsted Inspection : No"
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                    End If
                End If
                'Customer Service Excellence Standard Accredited (Q4)
                If Not providerdetailrow.IsExcellenceAccreditedNull Then
                    If providerdetailrow.ExcellenceAccredited Then
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        td.InnerText = "Customer Service Excellence Standard Accredited : Yes"
                        tr.Cells.Add(New EmptyTableCell())
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Date awarded : " + providerdetailrow.ExcellenceAccreditedDate.ToShortDateString()
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Date re-assessment is due: " + providerdetailrow.ExcellenceAccreditedDueDate.ToShortDateString()
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                    Else
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Customer Service Excellence Standard Accredited : No"
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                    End If
                End If
                ' Other Accreditation (Q4)
                If Not providerdetailrow.IsOtherAccreditationNull Then
                    If providerdetailrow.OtherAccreditation Then
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        td.InnerText = "Other Accreditation : Yes"
                        tr.Cells.Add(New EmptyTableCell())
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Date awarded : " + providerdetailrow.OtherAccreditationDate.ToShortDateString()
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Date re-assessment is due : " + providerdetailrow.OtherAccreditationDueDate.ToShortDateString()
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                        tr = New HtmlTableRow()
                        tr.Cells.Add(New EmptyTableCell())
                        td = New ValTableCell()
                        td.InnerHtml = "<p>Other Accreditation Specified</p><p>" + providerdetailrow.OtherAccreditationSpecified + "</p>"
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                    Else
                        tr = New HtmlTableRow()
                        td = New ValTableCell()
                        tr.Cells.Add(New EmptyTableCell())
                        td.InnerText = "Other Accreditation : No"
                        tr.Cells.Add(td)
                        tblQuality.Rows.Add(tr)
                    End If
                End If

				If Not providerdetailrow.IsChargeableNull Then
					If providerdetailrow.Chargeable Then
						tr = New HtmlTableRow()
						td = New ValTableCell()
						tr.Cells.Add(New EmptyTableCell())
						td.InnerText = "Chargeable Services: Yes"
						tblCharges.Rows.Add(tr)
						tr = New HtmlTableRow()
						tr.Cells.Add(New EmptyTableCell())
						td = New ValTableCell()
						td.InnerHtml = "<p>Charges Detail</p><p>" + providerdetailrow.ChargeableService + "</p>"
						tr.Cells.Add(td)
						tblCharges.Rows.Add(tr)
					Else
						tr = New HtmlTableRow()
						td = New ValTableCell()
						tr.Cells.Add(New EmptyTableCell())
						td.InnerText = "Chargeable Services : No"
						tr.Cells.Add(td)
						tblCharges.Rows.Add(tr)
					End If
				End If
				If Not providerdetailrow.IsTargetClientNull Then
					If providerdetailrow.TargetClient Then
						setProviderTargetView()
					Else
						tr = New HtmlTableRow()
						tr.Cells.Add(New EmptyTableCell())
						td = New ValTableCell()
						td.InnerText = "Service targeted at specific client groups : No"
						tr.Cells.Add(td)
						tblTarget.Rows.Add(tr)
					End If
				End If
				tr = New HtmlTableRow()
				td = New HtmlTableCell()
				td.InnerHtml = "<br />"
				tr.Cells.Add(td)
				tblTarget.Rows.Add(tr)
				If Not providerdetailrow.IsTargetWardsNull Then
					tr = New HtmlTableRow()
					tr.Cells.Add(New EmptyTableCell())
					td = New ValTableCell()
					td.InnerText = "Specific Targeted Districts/Wards : " + providerdetailrow.TargetWards
					tr.Cells.Add(td)
					tblTarget.Rows.Add(tr)
				End If
				If Not providerdetailrow.IsSpecialistServiceNull Then
					If providerdetailrow.SpecialistService Then
						tr = New HtmlTableRow()
						tr.Cells.Add(New EmptyTableCell())
						td = New ValTableCell()
						td.InnerHtml = "Provide Specialist Services to specific client group : Yes <br />Specialist Services Details :<br />" + providerdetailrow.SpecialistDetail
						tr.Cells.Add(td)
						tblTarget.Rows.Add(tr)
					Else
						tr = New HtmlTableRow()
						tr.Cells.Add(New EmptyTableCell())
						td = New ValTableCell()
						td.InnerText = "Provide Specialist Services to specific client group : No"
						tr.Cells.Add(td)
						tblTarget.Rows.Add(tr)
					End If
				End If
				If Not providerdetailrow.IsFundingSpecificsNull Then
					lblFundingDetail.Text = "Other Funding Details :" + providerdetailrow.FundingSpecifics
				End If
				tr = New HtmlTableRow()
				tr.Cells.Add(New EmptyTableCell())
				td = New ValTableCell()
				If Not providerdetailrow.IsFutureIAGDevNull Then
					td.InnerText = providerdetailrow.FutureIAGDev
				End If
				tr.Cells.Add(td)
				tblIAG.Rows.Add(tr)
				tr = New HtmlTableRow()
				tr.Cells.Add(New EmptyTableCell())
				td = New ValTableCell()
				If Not providerdetailrow.IsOtherInformationNull Then
					td.InnerText = providerdetailrow.OtherInformation
				End If
				tr.Cells.Add(td)
				tblAnyOther.Rows.Add(tr)
			Next
		Else
			Response.Redirect("default.aspx")
		End If
	End Sub

	''' <summary>
	''' Method to display provider registered area into page
	''' </summary>
	''' <remarks></remarks>

	Private Sub setProviderAreaView()
		Dim providerArea As New ProviderAreaTableAdapter()
		Dim providerAreaTable As ProviderDetail.ProviderAreaDataTable = providerArea.GetAreaByID(Integer.Parse(pid))
		For Each providerarearow As ProviderDetail.ProviderAreaRow In providerAreaTable
			Dim tr As New HtmlTableRow()
			td = New HtmlTableCell()
			td.InnerText = providerarearow.AreaName
			tr.Cells.Add(td)
			tblServiceArea.Rows.Add(tr)
		Next
	End Sub

	''' <summary>
	''' Method to display provider registered type into page
	''' </summary>
	''' <remarks></remarks>

	Private Sub setProviderTypeView()
		Dim providerType As New ProviderTypeTableAdapter()
		Dim providerTypeTable As ProviderDetail.ProviderTypeDataTable = providerType.GetTypebyID(Integer.Parse(pid))
		For Each providertyperow As ProviderDetail.ProviderTypeRow In providerTypeTable
			tr = New HtmlTableRow()
			td = New ValTableCell()
			tr.Cells.Add(New EmptyTableCell())
			td.InnerText = providertyperow.TypeName
			tr.Cells.Add(td)
			tblOrgType.Rows.Add(tr)
		Next
	End Sub

	''' <summary>
	''' Method to display provider registered service type into page
	''' </summary>
	''' <remarks></remarks>

	Private Sub setProviderServiceTypeView()
		Dim providerServiceType As New ProviderServiceTypeTableAdapter()
		Dim providerServiceTypeTable As ProviderDetail.ProviderServiceTypeDataTable = providerServiceType.GetServiceTypebyID(Integer.Parse(pid))
		For Each providerservicetyperow As ProviderDetail.ProviderServiceTypeRow In providerServiceTypeTable
			tr = New HtmlTableRow()
			tr.Cells.Add(New EmptyTableCell())
			td = New ValTableCell()
			td.InnerText = providerservicetyperow.TypeName
			tr.Cells.Add(td)
			tblServiceType.Rows.Add(tr)
		Next
	End Sub

	''' <summary>
	''' Method to display provider registered service available into page
	''' </summary>
	''' <remarks></remarks>

	Private Sub setProviderServiceAvailableView()
		Dim providerServiceAvailable As New ProviderServiceAvailableTableAdapter()
		Dim providerServiceAvailableTable As ProviderDetail.ProviderServiceAvailableDataTable = providerServiceAvailable.GetServiceAvailablebyID(Integer.Parse(pid))
		For Each providerserviceavailablerow As ProviderDetail.ProviderServiceAvailableRow In providerServiceAvailableTable
			tr = New HtmlTableRow()
			tr.Cells.Add(New EmptyTableCell())
			td = New ValTableCell()
			td.InnerText = providerserviceavailablerow.ServiceName
			tr.Cells.Add(td)
			tblServiceAvaiable.Rows.Add(tr)
		Next
	End Sub

	''' <summary>
	''' Method to display provider registered target group if any into page
	''' </summary>
	''' <remarks></remarks>

	Private Sub setProviderTargetView()
		Dim providerTarget As New ProviderTargetTableAdapter()
		Dim providerTargetTable As ProviderDetail.ProviderTargetDataTable = providerTarget.GetTargetbyID(Integer.Parse(pid))
		If providerTargetTable.Rows.Count > 0 Then
			tr = New HtmlTableRow()
			tr.Cells.Add(New EmptyTableCell())
			td = New ValTableCell()
			td.InnerHtml = "Service targeted at specific client groups : Yes <br /><br /><u>Targeted Clients</u>"
			tr.Cells.Add(td)
			tblTarget.Rows.Add(tr)
			For Each providertargetrow As ProviderDetail.ProviderTargetRow In providerTargetTable
				tr = New HtmlTableRow()
				tr.Cells.Add(New EmptyTableCell())
				td = New ValTableCell()
				td.InnerText = providertargetrow.TargetName
				tr.Cells.Add(td)
				tblTarget.Rows.Add(tr)
			Next
		End If
	End Sub

	''' <summary>
	''' Method to generate Site row inner class object and display sites related rows
	''' into page.
	''' </summary>
	''' <remarks>
	''' First Sites row class object is filled with sites values and then those rows are added
	''' to main site table embeded in aspx page
	''' </remarks>

	Private Sub setProviderSiteView()
		Dim providerSites As New ProviderSitesTableAdapter()
		Dim providerSitesTable As ProviderSite.ProviderSitesDataTable = providerSites.GetSitebyID(Integer.Parse(pid))
		Dim i As Integer = 1
		Dim siteid As Integer = 0
		For Each providersitesrow As ProviderSite.ProviderSitesRow In providerSitesTable
			siteid = providersitesrow.SiteID
			Dim sitesTable As New SiteTableView()
			sitesTable.addRow("<b><u>SITE " + CStr(i) + "</u></b>", "")
			If Not providersitesrow.IsSiteNameNull() Then
				sitesTable.addRow("Site Name :", providersitesrow.SiteName)
			End If
			sitesTable.addRow("Address :", providersitesrow.AddressLine1)
			If Not providersitesrow.IsAddressLine2Null() Then
				sitesTable.addRow("", providersitesrow.AddressLine2)
			End If
			If Not providersitesrow.IsAddressLine3Null() Then
				sitesTable.addRow("", providersitesrow.AddressLine3)
			End If
			If Not providersitesrow.IsAddressLine4Null() Then
				sitesTable.addRow("", providersitesrow.AddressLine4)
			End If
			sitesTable.addRow("Postcode :", providersitesrow.PostCode)
			If Not providersitesrow.IsTelephoneNull() Then
				sitesTable.addRow("Telephone :", providersitesrow.Telephone)
			End If
			If Not providersitesrow.IsFaxNull() Then
				sitesTable.addRow("Fax :", providersitesrow.Fax)
			End If
			If Not providersitesrow.IsClientContactNull() Then
				sitesTable.addRow("Contact :", providersitesrow.ClientContact)
			End If
			If Not providersitesrow.IsEmailNull() Then
				sitesTable.addRow("E Mail :", providersitesrow.Email)
			End If
			If Not providersitesrow.IsOpeningTimesNull() Then
				sitesTable.addRow("Date / Times of Opening :", providersitesrow.OpeningTimes)
			End If
			Dim siteService As New SiteServiceTypeTableAdapter
			Dim siteServiceTable As ProviderSite.SiteServiceTypeDataTable = siteService.GetServiceTypebyID(siteid)
			If siteServiceTable.Rows.Count > 0 Then
				Dim servicetypestr As String = ""
				For Each siteservicerow As ProviderSite.SiteServiceTypeRow In siteServiceTable
					servicetypestr = siteservicerow.TypeName
				Next
				sitesTable.addRow("Tier of Service : ", servicetypestr)
			End If

			'sitesTable.addRow("Facilities :", "")

			sitesTable.addRow("<u>Nearest Station or Stop</u>", "")
			If Not providersitesrow.IsNearestTramNull() Then
				sitesTable.addRow("Tram :", providersitesrow.NearestTram)
			End If
			If Not providersitesrow.IsNearestTrainNull() Then
				sitesTable.addRow("Train :", providersitesrow.NearestTrain)
			End If
			If Not providersitesrow.IsNearestBusNull() Then
				sitesTable.addRow("Bus :", providersitesrow.NearestBus)
			End If
			If providersitesrow.Parking Then
				sitesTable.addRow("Parking Facilities :", "Yes")
				If Not providersitesrow.IsParkingDetailsNull() Then
					sitesTable.addRow("Parking Details :", providersitesrow.ParkingDetails)
				End If
			Else
				sitesTable.addRow("Parking Facilities :", "No")
			End If
			If Not providersitesrow.IsDirectionsNull() Then
				sitesTable.addRow("Directions :", providersitesrow.Directions)
			End If
			If Not providersitesrow.IsAppointmentPhoneNull() Then
				sitesTable.addRow("Contact for Appointments :", providersitesrow.AppointmentPhone)
			End If
			If Not providersitesrow.IsOpeningHoursNull() Then
				sitesTable.addRow("Opening Hours :", providersitesrow.OpeningHours)
			End If
			If providersitesrow.LunchtimeClose Then
				sitesTable.addRow("Closed for Lunch?", "Yes")
				If Not providersitesrow.IsLunchtimeNull() Then
					sitesTable.addRow("Lunch Time :", providersitesrow.Lunchtime)
				End If
			Else
				sitesTable.addRow("Closed for Lunch?", "No")
			End If

			sitesTable.addRow("Drop In :", IIf(providersitesrow.DropIn, "Yes", "No"))
			sitesTable.addRow("Appointment Only :", IIf(providersitesrow.Appointment, "Yes", "No"))
			If providersitesrow.TelephoneHelp Then
				sitesTable.addRow("Telephone Helpline :", "Yes")
				If Not providersitesrow.IsHelpLineDetailsNull() Then
					sitesTable.addRow("Helpline Details :", providersitesrow.HelpLineDetails)
				End If
			Else
				sitesTable.addRow("Telephone Helpline :", "No")
			End If
			If providersitesrow.WebHelp Then
				sitesTable.addRow("Website Help :", "Yes")
				If Not providersitesrow.IsWebHelpDetailsNull() Then
					sitesTable.addRow("Website Help Details :", providersitesrow.WebHelpDetails)
				End If
			Else
				sitesTable.addRow("Website Help :", "No")
			End If

			Dim siteAccess As New SiteAccessTableAdapter()
			Dim siteAccessTable As ProviderSite.SiteAccessDataTable = siteAccess.GetAccessbyID(siteid)
			If siteAccessTable.Rows.Count > 0 Then
				Dim accessstr As String = "<ul>"
				For Each siteaccessrow As ProviderSite.SiteAccessRow In siteAccessTable
					accessstr += "<li>" + siteaccessrow.Description + "</li>"
				Next
				accessstr += "</ul>"
				sitesTable.addRow("Site Access :", accessstr)
			End If

			Dim siteFacilities As New SiteFacilitiesTableAdapter()
			Dim siteFacilitiesTable As ProviderSite.SiteFacilitiesDataTable = siteFacilities.GetFacilitybyID(siteid)
			If siteFacilitiesTable.Rows.Count > 0 Then
				Dim facilitiesstr As String = "<ul>"
				For Each sitefacilitiesrow As ProviderSite.SiteFacilitiesRow In siteFacilitiesTable
					facilitiesstr += "<li>" + sitefacilitiesrow.Description + "</li>"
				Next
				facilitiesstr += "</ul>"
				sitesTable.addRow("Extra Facilities :", facilitiesstr)
			End If
			If Not providersitesrow.IsFacilitiesDetailsNull() Then
				sitesTable.addRow("Other Facilities Details :", providersitesrow.FacilitiesDetails)
			End If

			Dim siteLanguages As New SiteLanguagesTableAdapter()
			Dim siteLanguagesTable As ProviderSite.SiteLanguagesDataTable = siteLanguages.GetLanguagebyID(siteid)
			If siteLanguagesTable.Rows.Count > 0 Then
				Dim languagesstr As String = "<ul>"
				For Each sitelanguagesrow As ProviderSite.SiteLanguagesRow In siteLanguagesTable
					languagesstr += "<li>" + sitelanguagesrow.LanguageName + "</li>"
				Next
				languagesstr += "</ul>"
				sitesTable.addRow("Languages :", languagesstr)
			End If

			If Not providersitesrow.IsOtherLanguagesNull() Then
				sitesTable.addRow("Other Languages :", providersitesrow.OtherLanguages)
			End If
			If Not providersitesrow.IsSpecialistWorkshopsNull() Then
				sitesTable.addRow("Specialist Workshops:", providersitesrow.SpecialistWorkshops)
			End If
			If Not providersitesrow.IsPhotographNull Then
				sitesTable.addRow("Photograph : <br /><span style=""font-size: 8pt; color: #993333"">Suggested file format is JPG having 144x144 pixel size.</span>", "<img class=""siteimage"" src=""getsitephoto.aspx?siteid=" + providersitesrow.SiteID.ToString + """ />")
			End If
			Dim siterows As HtmlTableRow() = sitesTable.getRows()
			For j As Integer = 0 To siterows.Length - 1
				tblProviderSite.Rows.Add(siterows(j))
			Next
			i += 1
		Next
	End Sub

	''' <summary>
	''' Method to display provider funding source detail into page
	''' </summary>
	''' <remarks>
	''' </remarks>

	Private Sub setProviderFundingView()
		Dim providerFunding As New ProviderFundingTableAdapter()
		Dim providerFundingTable As ProviderDetail.ProviderFundingDataTable = providerFunding.GetFundingbyID(Integer.Parse(pid))
		If providerFundingTable.Rows.Count > 0 Then
			tr = New HtmlTableRow()
			tr.Cells.Add(New EmptyTableCell())
			td = New HtmlTableCell()
			td.Width = "60%"
			td.InnerHtml = "<u>Source Name</u>"
			tr.Cells.Add(td)
			td = New HtmlTableCell()
			td.Width = "30%"
			td.InnerHtml = "<u>Funding Due to End</u>"
			tr.Cells.Add(td)
			tblFunding.Rows.Add(tr)
			For Each providerfundingrow As ProviderDetail.ProviderFundingRow In providerFundingTable
				tr = New HtmlTableRow()
				tr.Cells.Add(New EmptyTableCell())
				td = New HtmlTableCell()
				td.Width = "60%"
				td.InnerHtml = providerfundingrow.SourceName
				tr.Cells.Add(td)
				td = New HtmlTableCell()
				td.Width = "30%"
				td.InnerText = providerfundingrow.EndDate.ToShortDateString()
				tr.Cells.Add(td)
				tblFunding.Rows.Add(tr)
			Next
		End If
	End Sub

	''' <summary>
	''' Method to implement delete button functionality
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>
	''' Confirmation is obtained through passing context item to deleteconfirm.aspx
	''' and result is monitored on Page Load method where if confirm context is true
	''' delete method is called.
	''' </remarks>

	Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Context.Items.Add("confirm", True)
		Context.Items.Add("confirmpage", "~/viewprovider.aspx?providerid=" + pid.ToString)
		Context.Items.Add("confirmtext", "Are you Sure to Delete this Provider?" + _
		vbCrLf + "This action will delete all related details of provider!!")
		Server.Transfer("~/deleteconfirm.aspx")
	End Sub

	''' <summary>
	''' Delete method to delete provider after obtaining confirmation.
	''' </summary>
	''' <remarks>
	''' this method is called from page load method and assmes all relationship
	''' constraints are setup for delete related records.
	''' </remarks>

	Private Sub deleteProvider()
		Dim providerObj As New MDApplication.Provider
		providerObj.setID(pid)
		providerObj.deleteProviderfromDB()
	End Sub

	''' <summary>
	''' method to implement Amend button functionality.
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks>
	''' amend method create session variable to hold provider id and
	''' redirects to amendprovider.aspx
	''' </remarks>

	Protected Sub btnAmend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAmend.Click
		Session.Clear()
		Session.Add("pid", Integer.Parse(pid))
		Response.Redirect("~/amendprovider.aspx")
	End Sub

End Class