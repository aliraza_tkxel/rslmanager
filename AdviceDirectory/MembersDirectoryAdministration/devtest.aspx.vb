
Partial Class devtest
    Inherits MSDN.SessionPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ASPSession("hello") = "Hello World"
        Response.Write(ASPSession("hello"))
        Response.Write(ASPSession("mysession"))
        Response.Write("LoginType" + (ASPSession("LoginType")))
        Response.Write("UserID" + (ASPSession("UserID")))
    End Sub
End Class
