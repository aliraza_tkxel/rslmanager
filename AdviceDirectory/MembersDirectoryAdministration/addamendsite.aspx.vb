''' <summary>
''' Code behind class for addamendsite.aspx
''' </summary>
''' <remarks>
''' Class is inherited from MSDN.SessionPage which is custome assembly in SessionUtility.dll
''' which is extended from System.Web.UI.Page 
''' SessionPage class provides functionality to share session between ASP and ASP.NET by utilising
''' SessionManager.dll on ASP side.
''' ASPSession is visible on all extending class that holds session string value to share with ASP Pages
''' To utilise this funtionality ASP Session must be turned off in IIS.
''' </remarks>

Partial Class addamendsite
	Inherits MSDN.SessionPage

	''' <summary>
	''' Variable declaration
	''' </summary>
	''' <remarks></remarks>

	Private pid As Integer
	Private siteindex As Integer
	Private isnew As Boolean

	''' <summary>
	''' Object variable to hold instance of Provider Class
	''' </summary>
	''' <remarks></remarks>

	Private providerObj As MDApplication.Provider

	''' <summary>
	''' object variable to hold instanse of ProviderSite class
	''' </summary>
	''' <remarks></remarks>

	Private siteObj As MDApplication.ProviderSite
	
	''' <summary>
	''' Event action for section 1 save and next button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave8.Click
		saveSection1()
		If IsValid Then
			panSection8a.Visible = False
			panSection8b.Visible = True
		End If
	End Sub

	''' <summary>
	''' Event action for section 2 save and previous button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection8bPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection8bPrev.Click
		saveSection2()
		If IsValid Then
			panSection8b.Visible = False
			panSection8a.Visible = True
		End If
	End Sub

	''' <summary>
	''' Event action for section 2 save and next button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave8b_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave8b.Click
		saveSection2()
		If IsValid Then
			panSection8b.Visible = False
			panSection8c.Visible = True
		End If
	End Sub

	''' <summary>
	''' Event action for section 3 save and previous button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection8cPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection8cPrev.Click
		saveSection3()
		If IsValid Then
			panSection8c.Visible = False
			panSection8b.Visible = True
		End If
	End Sub

	''' <summary>
	''' Event action for section 3 save and next button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave8c_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave8c.Click
		saveSection3()
		If IsValid Then
			panSection8c.Visible = False
			panSection8d.Visible = True
			btnSaveAdd.Enabled = True
		End If
	End Sub

	''' <summary>
	''' Event action for section 4 save and previous button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection8dPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection8dPrev.Click
		saveSection4()
		If IsValid Then
			panSection8d.Visible = False
			panSection8c.Visible = True
			btnSaveAdd.Enabled = False
		End If
	End Sub

	''' <summary>
	''' Event action for section 4 save and next button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSave8d_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave8d.Click
		saveSection4()
		If IsValid Then
			Context.Items.Add("forward", True)
			Context.Items.Add("transfer", True)
			Server.Transfer("~/amendprovider.aspx", True)
		End If
	End Sub

	''' <summary>
	''' Page load event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtPostcode.Attributes.Add("onchange", "PCode();")
        restoreProviderState()
        If Not IsPostBack Then
            If Not providerObj Is Nothing Then
                pid = Session("pid")
                lblOrgName.Text = providerObj.getName
                If providerObj.getSite.Length <= 0 Then
                    isnew = True
                    If providerObj.getUseOrgSite Then
                        useMainSite()
                    Else
                        initSite()
                    End If
                Else
                    isnew = False
                End If
                If Session("siteindex") Is Nothing Then
                    siteindex = 0
                Else
                    siteindex = Session("siteindex")
                End If
            Else
                If ASPSession("UserID") Is Nothing Or ASPSession("UserID") = "" Then
                    Session.Clear()
                    Response.Redirect("default.aspx")
                Else
                    Response.Redirect("~/viewprovider.aspx?providerid=" + ASPSession("OrgID"))
                End If
            End If
            If Context.Items("forward") Then
                panSection8a.Visible = True
            Else
                panSection8a.Visible = False
                panSection8d.Visible = True
                btnSaveAdd.Enabled = True
            End If
        Else
            siteindex = Session("siteindex")
            isnew = Session("sitenew")
        End If
        If isnew Or providerObj.getSite.Length = 1 Then
            btnFirstSite.Enabled = False
            btnNextSite.Enabled = False
            btnPrevSite.Enabled = False
            btnLastSite.Enabled = False
        Else
            btnFirstSite.Enabled = True
            btnNextSite.Enabled = True
            btnPrevSite.Enabled = True
            btnLastSite.Enabled = True
        End If
        If providerObj.getSite.Length <= 1 Then
            btnDelete.Enabled = False
        Else
            btnDelete.Enabled = True
        End If
        Session.Add("sitenew", isnew)
        Session.Add("siteindex", siteindex)
        Session.Add("provider", providerObj)
        If Not Context.Items("confirm") Is Nothing Then
            If Context.Items("confirmresult") Then
                deleteSite()
            End If
        End If
    End Sub

	''' <summary>
	''' Method to fill provider contact common values to first added site.
	''' </summary>
	''' <remarks>
	''' Method is only called when there is no site registered and
	''' usemainorg flag is true in Provider class.
	''' </remarks>

	Private Sub useMainSite()
        siteObj = New MDApplication.ProviderSite(providerObj)
		siteObj.setFixed(True)
		siteObj.setPID(providerObj.getID)
		siteObj.setName(providerObj.getName)
		siteObj.setAddress1(providerObj.getAddress1)
		siteObj.setAddress2(providerObj.getAddress2)
		siteObj.setAddress3(providerObj.getAddress3)
		siteObj.setAddress4(providerObj.getAddress4)
        siteObj.setPostcode(providerObj.getPostcode)
        '-- START
        '-- GOOGLE MAPS API RESULT
        siteObj.setLattitude(providerObj.getLattitude)
        siteObj.setLongitude(providerObj.getLongitude)
        '-- END
		siteObj.setTelephone(providerObj.getTelephone)
		siteObj.setContact(providerObj.getContact)
		siteObj.setFax(providerObj.getFax)
		siteObj.setEmail(providerObj.getEmail)
		providerObj.addSite(siteObj)
    End Sub

    ''' <summary>
    ''' Method to initilize first added site.
    ''' </summary>
    ''' <remarks>
    ''' Method is only called when there is no site registered and
    ''' usemainorg flag is false in Provider class.
    ''' </remarks>

    Private Sub initSite()
        siteObj = New MDApplication.ProviderSite(providerObj)
        siteObj.setFixed(True)
        siteObj.setPID(providerObj.getID)
        providerObj.addSite(siteObj)
    End Sub

	''' <summary>
	''' Method to fill all sections values from class to form fields
	''' </summary>
	''' <remarks></remarks>

	Private Sub fillSections()
		If providerObj.getSite.Length <> 0 Then
			siteObj = providerObj.getSite(siteindex)
			'If siteObj.getFixed Then
			'	btnDelete.Enabled = False
			'Else
			'	btnDelete.Enabled = True
			'End If
			txtOrgName.Text = siteObj.getName
			txtAddress1.Text = siteObj.getAddress1
			txtAddress2.Text = siteObj.getAddress2
			txtAddress3.Text = siteObj.getAddress3
			txtAddress4.Text = siteObj.getAddress4
            txtPostcode.Text = siteObj.getPostcode
            '-- START
            '-- GOOGLE API RESULT
            hiddenLat.Value = siteObj.getLattitude
            hiddenLng.Value = siteObj.getLongitude
            '-- END
			txtTelephone.Text = siteObj.getTelephone
			txtFax.Text = siteObj.getFax
			txtContactName.Text = siteObj.getContact
			txtEmail.Text = siteObj.getEmail
			txtDateTime.Text = siteObj.getDateTimeofOpening
            'If siteObj.getServiceID <> 0 Then
            'rbServices.SelectedValue = siteObj.getServiceID.ToString
            'End If
            txtTram.Text = siteObj.getNearestTram
            txtTrain.Text = siteObj.getNearestTrain
            txtBus.Text = siteObj.getNearestbus
            If siteObj.getParking Then
                rbParking.SelectedValue = 1
            Else
                rbParking.SelectedValue = 0
            End If

            If siteObj.getParking Then
                txtParking.Text = siteObj.getParkingDetails
            End If
            txtDirections.Text = siteObj.getDirections
            txtContactApp.Text = siteObj.getAppointmentContact
            txtOpeningHours.Text = siteObj.getOpeningHours

            If siteObj.getLunchClose Then
                rbLunch.SelectedValue = 1
                txtLunchTime.Text = siteObj.getLunchTime
            Else
                rbLunch.SelectedValue = 0
            End If
            cbDropin.Checked = siteObj.getDropin
            cbAppointment.Checked = siteObj.getAppointmentsOnly
            If siteObj.getTelephoneHelp Then
                rbTeleHelp.SelectedValue = 1
                txtTeleHelp.Text = siteObj.getTeleHelpDetail
            Else
                rbTeleHelp.SelectedValue = 0
            End If
            If siteObj.getWebHelp Then
                rbWebHelp.SelectedValue = 1
                txtWebHelp.Text = siteObj.getWebHelpDetail
            Else
                rbWebHelp.SelectedValue = 0
            End If
            Dim i As Integer, j As Integer
            Dim pa As Integer() = siteObj.getAccess
            For i = 0 To cbAccess.Items.Count - 1
                cbAccess.Items(i).Selected = False
                For j = 0 To pa.Length - 1
                    If pa(j) = cbAccess.Items(i).Value Then
                        cbAccess.Items(i).Selected = True
                        Exit For
                    End If
                Next
            Next

            Dim ps As Integer() = siteObj.getFacilities
            For i = 0 To cbFacilities.Items.Count - 1
                cbFacilities.Items(i).Selected = False
                For j = 0 To ps.Length - 1
                    If ps(j) = cbFacilities.Items(i).Value Then
                        cbFacilities.Items(i).Selected = True
                        Exit For
                    End If
                Next
            Next

            txtFacilities.Text = siteObj.getOtherFacilities

            Dim pl As Integer() = siteObj.getLanguages
            For i = 0 To cbLanguages.Items.Count - 1
                cbLanguages.Items(i).Selected = False
                For j = 0 To pl.Length - 1
                    If pl(j) = cbLanguages.Items(i).Value Then
                        cbLanguages.Items(i).Selected = True
                        Exit For
                    End If
                Next
            Next
            txtLanguages.Text = siteObj.getOtherLanguages
            txtSpecialist.Text = siteObj.getSpecialist
            If Not siteObj.getPhotograph Is Nothing Then
                If siteObj.getPhotograph.Length > 0 Then
                    Session.Add("siteid" + siteObj.getID.ToString, siteObj.getPhotograph)
                    tblFileUpload.Rows(1).Cells(1).InnerHtml = "<img class=""siteimage"" src=""getsitephoto.aspx?showimg=siteid" + siteObj.getID.ToString + """ /><br />"
                    btnDeleteImage.Visible = True
                End If
            Else
                btnDeleteImage.Visible = False
            End If
        End If
	End Sub

	''' <summary>
	''' Method to save current section 1 form values to current site object
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection1()
		siteObj = providerObj.getSite(siteindex)
		siteObj.setName(txtOrgName.Text)
		siteObj.setAddress1(txtAddress1.Text)
		siteObj.setAddress2(txtAddress2.Text)
		siteObj.setAddress3(txtAddress3.Text)
		siteObj.setAddress4(txtAddress4.Text)
        siteObj.setPostcode(txtPostcode.Text)
        siteObj.setLattitude(hiddenLat.Value)
        siteObj.setLongitude(hiddenLng.Value)
		siteObj.setTelephone(txtTelephone.Text)
		siteObj.setFax(txtFax.Text)
		siteObj.setContact(txtContactName.Text)
		siteObj.setEmail(txtEmail.Text)
		siteObj.setDateTimeofOpening(txtDateTime.Text)
        'If rbServices.SelectedIndex <> -1 Then
        'siteObj.setServiceID(rbServices.SelectedValue)
        'End If
        providerObj.getSite(siteindex) = siteObj
        Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' Method to save current section 2 form values to current site object
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection2()
		siteObj = providerObj.getSite(siteindex)
		siteObj.setNearestTram(txtTram.Text)
		siteObj.setNearestTrain(txtTrain.Text)
		siteObj.setNearestBus(txtBus.Text)
		siteObj.setParking(rbParking.SelectedValue)
		If siteObj.getParking Then
			siteObj.setParkingDetails(txtParking.Text)
		End If
		siteObj.setDirections(txtDirections.Text)
		providerObj.getSite(siteindex) = siteObj
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' Method to save current section 3 form values to current site object
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection3()
		siteObj = providerObj.getSite(siteindex)
		siteObj.setAppointmentContact(txtContactApp.Text)
		siteObj.setOpeningHours(txtOpeningHours.Text)
		siteObj.setLunchClose(rbLunch.SelectedValue)
		If siteObj.getLunchClose Then
			siteObj.setLunchTime(txtLunchTime.Text)
		End If
		siteObj.setDropin(cbDropin.Checked)
		siteObj.setAppointmentsOnly(cbAppointment.Checked)
		siteObj.setTelephoneHelp(rbTeleHelp.SelectedValue)
		If siteObj.getTelephoneHelp Then
			siteObj.setTeleHelpDetail(txtTeleHelp.Text)
		End If
		siteObj.setWebHelp(rbWebHelp.SelectedValue)
		If siteObj.getWebHelp Then
			siteObj.setWebHelpDetail(txtWebHelp.Text)
		End If
		providerObj.getSite(siteindex) = siteObj
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' Method to save current section 4 form values to current site object
	''' </summary>
	''' <remarks></remarks>

	Private Sub saveSection4()
		siteObj = providerObj.getSite(siteindex)
		siteObj.resetAccess()
		Dim i As Integer
		For i = 0 To cbAccess.Items.Count - 1
			If cbAccess.Items(i).Selected Then
				siteObj.addAccess(cbAccess.Items(i).Value)
			End If
		Next
		siteObj.resetFacilities()
		For i = 0 To cbFacilities.Items.Count - 1
			If cbFacilities.Items(i).Selected Then
				siteObj.addFacilities(cbFacilities.Items(i).Value)
			End If
		Next
		siteObj.setOtherFacilities(txtFacilities.Text)
		siteObj.resetLanguages()
		For i = 0 To cbLanguages.Items.Count - 1
			If cbLanguages.Items(i).Selected Then
				siteObj.addLanguages(cbLanguages.Items(i).Value)
			End If
		Next
		siteObj.setOtherLanguages(txtLanguages.Text)
		siteObj.setSpecialist(txtSpecialist.Text)

		If Not fuSite.PostedFile Is Nothing Then
			If fuSite.PostedFile.FileName.Trim.Length > 0 And fuSite.PostedFile.ContentLength > 0 Then
				Dim imgStream As IO.Stream = fuSite.PostedFile.InputStream()
				Dim imgLen As Integer = fuSite.PostedFile.ContentLength
				Dim imgContentType As String = fuSite.PostedFile.ContentType
				Dim imgName As String = fuSite.PostedFile.FileName.Substring(fuSite.PostedFile.FileName.LastIndexOf("\") + 1)
				Dim imgBinaryData(imgLen) As Byte
				Dim n As Int32 = imgStream.Read(imgBinaryData, 0, imgLen)
				siteObj.setPhotograph(createThumnail(imgStream, 144, 144))
			End If
        End If
		providerObj.getSite(siteindex) = siteObj
		Session.Add("provider", providerObj)
	End Sub

	''' <summary>
	''' Method to resize uploaded image
	''' </summary>
	''' <param name="ImageStream">byte stearm of uploaded file as IO.Stream</param>
	''' <param name="tWidth">image width to resize as Double</param>
	''' <param name="tHeight">image height to resize as Double</param>
	''' <returns>Byte() of image data</returns>
	''' <remarks></remarks>

	Private Function createThumnail(ByVal ImageStream As IO.Stream, ByVal tWidth As Double, ByVal tHeight As Double) As Byte()
		Dim g As System.Drawing.Image = System.Drawing.Image.FromStream(ImageStream)
		Dim thumbSize As New Drawing.Size()
		thumbSize = NewthumbSize(g.Width, g.Height, tWidth, tHeight)
		Dim imgOutput As New Drawing.Bitmap(g, thumbSize.Width, thumbSize.Height)
		Dim imgStream As New IO.MemoryStream()
		Dim thisFormat As Drawing.Imaging.ImageFormat = g.RawFormat
		imgOutput.Save(imgStream, thisFormat)
		Dim imgbin(imgStream.Length) As Byte
		imgStream.Position = 0
		Dim n As Int32 = imgStream.Read(imgbin, 0, imgbin.Length)
		g.Dispose()
		imgOutput.Dispose()
		Return imgbin
	End Function

	''' <summary>
	''' sub function of createthumbnail for resizing
	''' </summary>
	''' <param name="currentwidth"></param>
	''' <param name="currentheight"></param>
	''' <param name="newWidth"></param>
	''' <param name="newHeight"></param>
	''' <returns>Size structure of Drwaing object</returns>
	''' <remarks></remarks>

	Function NewthumbSize(ByVal currentwidth As Double, ByVal currentheight As Double, ByVal newWidth As Double, ByVal newHeight As Double) As Drawing.Size
		' Calculate the Size of the New image 
		Dim tempMultiplier As Double

		If currentheight > currentwidth Then ' portrait 
			tempMultiplier = newHeight / currentheight
		Else
			tempMultiplier = newWidth / currentwidth
		End If

		Dim NewSize As New Drawing.Size(CInt(currentwidth * tempMultiplier), CInt(currentheight * tempMultiplier))
		Return NewSize
	End Function

	''' <summary>
	''' Event action for section 1 save and previous button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSection8Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSection8Prev.Click
		saveSection1()
		If IsValid Then
			If providerObj.getSite.Length > 0 Then
				Context.Items.Add("havesites", True)
				Context.Items.Add("forward", False)
				Context.Items.Add("transfer", True)
				Server.Transfer("~/amendprovider.aspx", True)
			Else
				Context.Items.Add("forward", False)
				Context.Items.Add("transfer", True)
				Server.Transfer("~/amendprovider.aspx", True)
			End If
		End If
	End Sub

	''' <summary>
	''' method to restore provider object from session state
	''' </summary>
	''' <remarks></remarks>

	Private Sub restoreProviderState()
		If Not (Session("provider") Is Nothing) Then
			providerObj = CType(Session("provider"), MDApplication.Provider)
		End If
	End Sub

	''' <summary>
	''' restoring values from session after page completly loaded
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub Page_SaveStateComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SaveStateComplete
		restoreProviderState()
		fillSections()
	End Sub

	''' <summary>
	''' Site navigation move first button event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnFirstSite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFirstSite.Click
		siteindex = 0
		Session.Add("siteindex", siteindex)
		fillSections()
	End Sub

	''' <summary>
	''' Site navigation move last button event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnLastSite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLastSite.Click
		siteindex = providerObj.getSite.Length - 1
		Session.Add("siteindex", siteindex)
		fillSections()
	End Sub

	''' <summary>
	''' Site navigation move previous button event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnPrevSite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevSite.Click
		siteindex -= 1
		If siteindex < 0 Then
			siteindex = 0
		End If
		Session.Add("siteindex", siteindex)
		fillSections()
	End Sub

	''' <summary>
	''' Site navigation move next button event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnNextSite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNextSite.Click
		siteindex += 1
		If siteindex >= providerObj.getSite.Length Then
			siteindex = providerObj.getSite.Length - 1
		End If
		Session.Add("siteindex", siteindex)
		fillSections()
	End Sub

	''' <summary>
	''' Save and Add new site button event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnSaveAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click
		saveSection4()
		If IsValid Then
            siteObj = New MDApplication.ProviderSite(providerObj)
			'siteObj.setID(providerObj.getSite.Length)
			siteObj.setPID(providerObj.getID)
			providerObj.addSite(siteObj)
			isnew = True
			panSection8d.Visible = False
			panSection8a.Visible = True
			siteindex = providerObj.getSite.Length - 1
			Session.Add("sitenew", isnew)
			Session.Add("siteindex", siteindex)
			Session.Add("provider", providerObj)
			btnSaveAdd.Enabled = False
			btnFirstSite.Enabled = False
			btnNextSite.Enabled = False
			btnPrevSite.Enabled = False
			btnLastSite.Enabled = False
			fillSections()
			'btnCancelUpdate.Enabled = True
			'btnCancelUpdate.Visible = True
		End If
	End Sub


	''' <summary>
	''' Delete site button event method
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>

	Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		'Dim result As MsgBoxResult
		Session.Add("siteindex", siteindex)
		Context.Items.Add("confirm", True)
		Context.Items.Add("confirmpage", "~/addamendsite.aspx")
		Context.Items.Add("confirmtext", "Are you Sure to Delete this Site?")
		Server.Transfer("~/deleteconfirm.aspx")
		'result = MsgBox("Are you Sure to Delete this Site?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Delete Confirmation")

		'If result = MsgBoxResult.Yes Then
		'	Dim tsites() As MDApplication.ProviderSite
		'	siteObj = providerObj.getSite(siteindex)
		'	tsites = providerObj.getSite
		'	providerObj.resetSite()
		'	Dim i As Integer
		'	For i = 0 To tsites.Length - 1
		'		If tsites(i).getID <> siteObj.getID Then
		'			providerObj.addSite(CType(tsites(i), MDApplication.ProviderSite))
		'		End If
		'	Next
		'	siteObj.deleteSitefromDB()
		'	siteindex = 0
		'	isnew = False
		'	Session.Add("sitenew", isnew)
		'	Session.Add("siteindex", siteindex)
		'	Session.Add("provider", providerObj)
		'	btnSaveAdd.Enabled = False
		'	btnCancelUpdate.Enabled = False
		'	btnCancelUpdate.Visible = False
		'	btnFirstSite.Enabled = True
		'	btnNextSite.Enabled = True
		'	btnPrevSite.Enabled = True
		'	btnLastSite.Enabled = True
		'	fillSections()
		'End If
	End Sub

	''' <summary>
	''' Method to delete current visible site from database
	''' </summary>
	''' <remarks></remarks>

	Private Sub deleteSite()
		Dim tsites() As MDApplication.ProviderSite
		siteindex = Session("siteindex")
		siteObj = providerObj.getSite(siteindex)
		tsites = providerObj.getSite
		providerObj.resetSite()
		Dim i As Integer
		For i = 0 To tsites.Length - 1
            If tsites(i).getAddress1 <> siteObj.getAddress1 Then
                providerObj.addSite(CType(tsites(i), MDApplication.ProviderSite))
            End If
		Next
		siteObj.deleteSitefromDB()
        siteindex = 0
        If providerObj.getSite.Length = 0 Then
            isnew = True
        Else
            isnew = False
        End If
		Session.Add("sitenew", isnew)
		Session.Add("siteindex", siteindex)
		Session.Add("provider", providerObj)
		btnSaveAdd.Enabled = False
		btnFirstSite.Enabled = True
		btnNextSite.Enabled = True
		btnPrevSite.Enabled = True
		btnLastSite.Enabled = True
		fillSections()
		panSection8a.Visible = True
		panSection8d.Visible = False
	End Sub




    ''' <summary>
    ''' validation method for parking radio button and parking details
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvParking_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvParking.ServerValidate
        If rbParking.SelectedValue = 1 And txtParking.Text = "" Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    ''' <summary>
    ''' validation method for lunchtime close selection and timings
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvLunch_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvLunch.ServerValidate
        If rbLunch.SelectedValue = 1 And txtLunchTime.Text = "" Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    ''' <summary>
    ''' validation method for telephone help selection
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvTelephone_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTelephone.ServerValidate
        If rbTeleHelp.SelectedValue = 1 And txtTeleHelp.Text = "" Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    ''' <summary>
    ''' validation method for website help selection
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvWebhelp_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvWebhelp.ServerValidate
        If rbWebHelp.SelectedValue = 1 And txtWebHelp.Text = "" Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    ''' <summary>
    ''' validation method for site facilities selection
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvFacilities_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvFacilities.ServerValidate
        'Dim i As Integer
        'Dim flag As Boolean
        'For i = 0 To cbFacilities.Items.Count - 1
        '	If cbFacilities.Items(i).Selected Then
        '		flag = True
        '	End If
        'Next
        'If flag Then
        '	args.IsValid = True
        'Else
        '	args.IsValid = False
        'End If
    End Sub

    ''' <summary>
    ''' validation method for other site facilities detail
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvSpecial_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvSpecial.ServerValidate
        Dim i As Integer
        Dim flag As Boolean = True
        For i = 0 To cbFacilities.Items.Count - 1
            If cbFacilities.Items(i).Selected And cbFacilities.Items(i).Text.ToUpper = "OTHER" Then
                If txtFacilities.Text = "" Then
                    flag = False
                End If
            End If
        Next
        If flag Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    ''' <summary>
    ''' validation method for other laguages selection detail
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvLanguages_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvLanguages.ServerValidate
        Dim i As Integer
        Dim flag As Boolean = True
        For i = 0 To cbLanguages.Items.Count - 1
            If cbLanguages.Items(i).Selected And cbLanguages.Items(i).Text.ToUpper.Contains("OTHER") Then
                If txtLanguages.Text = "" Then
                    flag = False
                End If
            End If
        Next
        If flag Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    ''' <summary>
    ''' validation method for languages selection
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>

    Protected Sub cvLanguage_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvLanguage.ServerValidate
        Dim flag As Boolean = True
        Dim i As Integer
        For i = 0 To cbFacilities.Items.Count - 1
            If cbFacilities.Items(i).Selected And cbFacilities.Items(i).Text.ToUpper.Contains("LANGUAGE") Then
                flag = False
            End If
        Next
        If Not flag Then
            For i = 0 To cbLanguages.Items.Count - 1
                If cbLanguages.Items(i).Selected Then
                    flag = True
                End If
            Next
        End If
        args.IsValid = flag
    End Sub

    Protected Sub btnDeleteImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteImage.Click
        siteObj = providerObj.getSite(siteindex)
        siteObj.setPhotograph(Nothing)
        providerObj.getSite(siteindex) = siteObj
        Session.Add("provider", providerObj)
        fillSections()
    End Sub
End Class
