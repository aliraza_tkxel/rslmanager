<%@ Page Language="VB" AutoEventWireup="false" CodeFile="deleteconfirm.aspx.vb" Inherits="deleteconfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Delete Confirm</title>
    <link rel ="Stylesheet" href = "StyleSheet.css" />
    <link rel="stylesheet" href="MembersArea.css" />
</head>
<body class="gbody">
    <form id="wfrmDeleteConfirm" runat="server">
    <div class="orgformbg">
		<asp:Panel ID="Panel1" runat="server" CssClass="panel" Width="410px">
			<asp:Label ID="lblMessage" runat="server"></asp:Label><br />
			<br />
			&nbsp;<asp:Button ID="btnOK" runat="server" Text="YES" CssClass="inputbutton" />
			<asp:Button ID="btnCancel" runat="server" Text="NO" CssClass="inputbutton" />
			<asp:HiddenField ID="hfURL" runat="server" />
		</asp:Panel>
    
    </div>
    </form>
</body>
</html>
