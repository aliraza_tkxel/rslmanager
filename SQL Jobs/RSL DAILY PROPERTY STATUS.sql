USE [msdb]
GO

/****** Object:  Job [RSL DAILY PROPERTY STATUS]    Script Date: 01/12/2014 14:36:15 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 01/12/2014 14:36:15 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'RSL DAILY PROPERTY STATUS', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'RECORD CURRENT PROPERTY STOCK STATUS GROUPED BY ASSETTYPE, STOCKTYPE AND STATUS.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [INSERT]    Script Date: 01/12/2014 14:36:15 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'INSERT', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'INSERT INTO P_DAILYSTATUS (ASSETTYPEID, ASSETNAME, STOCKTYPE, STATUSID, STATUSNAME, AMOUNT, TOTALVALUE,DTIMESTAMP,DEVELOPMENTID)
	SELECT 	
		A.ASSETTYPEID, A.DESCRIPTION,
		P.STOCKTYPE,
		S.STATUSID, S.DESCRIPTION,
		COUNT(*) ,
		ISNULL(SUM(F.RENT) + SUM(F.SERVICES),0),
		getdate(),P.DEVELOPMENTID
		FROM P__PROPERTY P
		LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID
		INNER JOIN P_ASSETTYPE A ON A.ASSETTYPEID = P.ASSETTYPE
		INNER JOIN P_STATUS S ON S.STATUSID = P.STATUS
	GROUP 	BY P.STOCKTYPE, A.DESCRIPTION, A.ASSETTYPEID, S.DESCRIPTION, S.STATUSID,P.DEVELOPMENTID
	ORDER	BY P.STOCKTYPE, A.DESCRIPTION, A.ASSETTYPEID, S.DESCRIPTION, S.STATUSID,P.DEVELOPMENTID', 
		@database_name=N'RSLBHALive', 
		@output_file_name=N'D:\RSL Manager Data Area\SQL LOG FILES\DAILYSTATUS.LOG', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'NIGHTLY', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20050112, 
		@active_end_date=99991231, 
		@active_start_time=40000, 
		@active_end_time=235959, 
		@schedule_uid=N'fbcdd1c7-d446-4c63-bb40-edd19c00a70b'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

