<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
' *** Edit Operations: declare variables

MM_editAction = CStr(Request("URL"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Delete Record: declare variables

if (CStr(Request("MM_delete")) <> "" And CStr(Request("MM_recordId")) <> "") Then

  MM_editConnection = RSL_CONNECTION_STRING
  MM_editTable = "dbo.G_NEWS"
  MM_editColumn = "NewsID"
  MM_recordId = "" + Request.Form("MM_recordId") + ""
  MM_editRedirectUrl = "NewsItems_Delete.asp?Delete=True"

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If
  
End If
%>
<%
' *** Delete Record: construct a sql delete statement and execute it

If (CStr(Request("MM_delete")) <> "" And CStr(Request("MM_recordId")) <> "") Then

  ' create the sql delete statement
  MM_editQuery = "delete from " & MM_editTable & " where " & MM_editColumn & " = " & MM_recordId

  If (Not MM_abortEdit) Then
    ' execute the delete
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    If (MM_editRedirectUrl <> "") Then
      Response.Redirect(MM_editRedirectUrl)
    End If
  End If

End If
%>
<%
Dim rsNews__varNewsID
rsNews__varNewsID = "0"
if (Request("ID") <> "") then rsNews__varNewsID = Request("ID")
%>
<%
OpenDB()
sql = "SELECT TITLE, NEWSIMAGE, CONTENT  FROM dbo.G_NEWS  WHERE NewsID = " + Replace(rsNews__varNewsID, "'", "''") + ""
Call OpenRs(rsNews, sql)
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager News --> News --> Delete News</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="10">&nbsp;</td>
                      <td width="506" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td colspan="3" valign="top"><br>
            <span class="iagManagerSmallBlk">Please check this is the correct 
            news item before you delete it.<br>
            <b>NOTE:</b> This action cannot be undone.<br>
            <br>
            </span><span class="RSLBlack"><strong><%=(rsNews.Fields.Item("Title").Value)%></strong></span><br> 
                              <br /> <span class="RSLBlack"><%=(rsNews.Fields.Item("Content").Value)%></span> 
                              <br /> </td>
                          </tr>
                        </table></td>
                      <td width="26"> <p>&nbsp;</p></td>
                      <td width="225" align="right" valign="top"><br>
						<%If rsNews.Fields.Item("NewsImage").Value <> "" Then %>
							<img src="NewsImages/<%=(rsNews.Fields.Item("NewsImage").Value)%>" width=145px height=135px>
						<% Else %>
							<img src='/myImages/news_image.gif' width=145px height=135px>
						<% End If %>
<br><br> 
						<form name="NewsDelete" method="POST" action="<%=MM_editAction%>">
						<input type="submit" name="Submit" value="Delete" class="RSLButton">
						<input type="hidden" name="ID" value="<%=rsNews__varNewsID%>">
						<input type="hidden" name="MM_delete" value="true">
						<input type="hidden" name="MM_recordId" value="<%= rsNews__varNewsID %>">
						</form>									
                      </td>
                      <td width="19" align="right">&nbsp;</td>
                    </tr>
                  </table>
                  
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
CloseRs(rsNews)
%>