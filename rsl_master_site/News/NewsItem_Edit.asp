<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
' *** Edit Operations: declare variables

MM_editAction = CStr(Request("URL"))
'If (Request.QueryString <> "") Then
  'MM_editAction = MM_editAction & "?" & Request.QueryString
'End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Update Record: set variables

If (CStr(Request("MM_update")) <> "" And CStr(Request("MM_recordId")) <> "") Then

  MM_editConnection = RSL_CONNECTION_STRING
  MM_editTable = "dbo.G_NEWS"
  MM_editColumn = "NewsID"
  MM_recordId = "" + Request.Form("MM_recordId") + ""
  MM_editRedirectUrl = "NewsItem_Edit.asp?Amend=1"
  MM_fieldsStr  = "text|value"
  MM_columnsStr = "Content|',none,''"
'on error resume next
'Response.Write Request.Form("text")

  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  
  ' set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(Request.Form(MM_fields(i)))
  Next

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If

End If
%>
<%
' *** Update Record: construct a sql update statement and execute it

If (CStr(Request("MM_update")) <> "" And CStr(Request("MM_recordId")) <> "") Then

  ' create the sql update statement
  MM_editQuery = "update " & MM_editTable & " set "
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_editQuery = MM_editQuery & ","
    End If
    MM_editQuery = MM_editQuery & MM_columns(i) & " = " & FormVal
  Next
  MM_editQuery = MM_editQuery & " where " & MM_editColumn & " = " & MM_recordId

  If (Not MM_abortEdit) Then
    ' execute the update
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    If (MM_editRedirectUrl <> "") Then
      Response.Redirect(MM_editRedirectUrl)
    End If
  End If

End If
%>
<%
Dim rsNewsEdit__VarNewsID
rsNewsEdit__VarNewsID = "0"
if (Request.QueryString("MaxID") <> "") then rsNewsEdit__VarNewsID = Request.QueryString("MaxID")
%>
<%
set rsNewsEdit = Server.CreateObject("ADODB.Recordset")
rsNewsEdit.ActiveConnection = RSL_CONNECTION_STRING
rsNewsEdit.Source = "SELECT * FROM dbo.G_NEWS WHERE NewsID = " + Replace(rsNewsEdit__VarNewsID, "'", "''") + ""
rsNewsEdit.CursorType = 0
rsNewsEdit.CursorLocation = 2
rsNewsEdit.LockType = 3
rsNewsEdit.Open()
rsNewsEdit_numRows = 0
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager News --> News --> Edit News Story</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="730" border="1" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                          <tr> 
                            <td colspan="2" valign="top" class="iagManagerSmallBlk"> 
                              <% If Request.QueryString("Amend") = "" Then %>
                              <object id="richedit" data="../RTE_files/richedit.html" width="100%" height="400" type="text/x-scriptlet" VIEWASTEXT>
                              </object> 
                              <form ACTION="<%=MM_editAction%>" id="theForm" name="theForm" method="POST">
                                <textarea name="text" style="display:none" rows="1" cols="20"><%=(rsNewsEdit.Fields.Item("Content").Value)%></textarea>
                                <input type="hidden" name="MM_update" value="true">
                                <input type="hidden" name="MM_recordId" value="<%= rsNewsEdit.Fields.Item("NewsID").Value %>">
            </form>
                              <script language="JavaScript" event="onload" for="window">
richedit.options = "history=yes;source=yes";
richedit.docHtml = theForm.text.innerText;
</script>
                              <script language="JavaScript" event="onscriptletevent(name, eventData)" for="richedit">
if (name == "post") {
if(confirm("This Document is about to be submitted Are you sure you have finished editing?")){
theForm.text.value = eventData;
theForm.submit();
}
}
</script>
                              <% End IF %>
                              <% If Request.QueryString("Amend") <> "" Then %>
                              <br>&nbsp;&nbsp;The Story has been successfully updated. 
                              <% End IF %>
                            </td>
                          </tr>
                        </table>
                      
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
rsNewsEdit.Close()
%>