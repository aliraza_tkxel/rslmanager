<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->

<%
NewsType = Replace(Request("newsType"), "'", "''")
NewsOption = ""

If NewsType <> "" Then
	If NewsType = "All" then
		SQL = "SELECT NEWSID, TITLE, DATECREATED2 = CONVERT(VARCHAR, DATECREATED,103), DATEEXPIRES2 = CONVERT(VARCHAR, DATEEXPIRES, 103), HASEXPIRED = CASE WHEN (DATEEXPIRES >= GETDATE()) THEN '0' ELSE '1' END FROM DBO.G_NEWS ORDER BY HASEXPIRED,DATECREATED, DATEEXPIRES, TITLE"
		NewsOption = "'RSL Manager'"		
	Else
		SQL = "SELECT NEWSID, TITLE, DATECREATED2 = CONVERT(VARCHAR, DATECREATED,103), DATEEXPIRES2 = CONVERT(VARCHAR, DATEEXPIRES, 103), HASEXPIRED = CASE WHEN (DATEEXPIRES >= GETDATE()) THEN '0' ELSE '1' END FROM DBO.G_NEWS WHERE NEWSFOR LIKE '%," & NewsType & ",%' ORDER BY HASEXPIRED, DATECREATED, DATEEXPIRES, TITLE"
		NewsOption = "'" & Request("optionname") & "'"
	End if

OpenDB()
Call OpenRs(rsNewsItem, SQL)

rsNewsItem_numRows = 0

Dim Repeat1__numRows
Repeat1__numRows = 10
Dim Repeat1__index
Repeat1__index = 0
rsNewsItem_numRows = rsNewsItem_numRows + Repeat1__numRows

'  *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

' set the record count
rsNewsItem_total = rsNewsItem.RecordCount

' set the number of rows displayed on this page
If (rsNewsItem_numRows < 0) Then
  rsNewsItem_numRows = rsNewsItem_total
Elseif (rsNewsItem_numRows = 0) Then
  rsNewsItem_numRows = 1
End If

' set the first and last displayed record
rsNewsItem_first = 1
rsNewsItem_last  = rsNewsItem_first + rsNewsItem_numRows - 1

' if we have the correct record count, check the other stats
If (rsNewsItem_total <> -1) Then
  If (rsNewsItem_first > rsNewsItem_total) Then rsNewsItem_first = rsNewsItem_total
  If (rsNewsItem_last > rsNewsItem_total) Then rsNewsItem_last = rsNewsItem_total
  If (rsNewsItem_numRows > rsNewsItem_total) Then rsNewsItem_numRows = rsNewsItem_total
End If

' *** Move To Record and Go To Record: declare variables

Set MM_rs    = rsNewsItem
MM_rsCount   = rsNewsItem_total
MM_size      = rsNewsItem_numRows
MM_uniqueCol = ""
MM_paramName = ""
MM_offset = 0
MM_atTotal = false
MM_paramIsDefined = false
If (MM_paramName <> "") Then
  MM_paramIsDefined = (Request.QueryString(MM_paramName) <> "")
End If

' *** Move To Record: handle 'index' or 'offset' parameter

if (Not MM_paramIsDefined And MM_rsCount <> 0) then

  ' use index parameter if defined, otherwise use offset parameter
  r = Request.QueryString("index")
  If r = "" Then r = Request.QueryString("offset")
  If r <> "" Then MM_offset = Int(r)

  ' if we have a record count, check if we are past the end of the recordset
  If (MM_rsCount <> -1) Then
    If (MM_offset >= MM_rsCount Or MM_offset = -1) Then  ' past end or move last
      If ((MM_rsCount Mod MM_size) > 0) Then         ' last page not a full repeat region
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' move the cursor to the selected record
  i = 0
  While ((Not MM_rs.EOF) And (i < MM_offset Or MM_offset = -1))
    MM_rs.MoveNext
    i = i + 1
  Wend
  If (MM_rs.EOF) Then MM_offset = i  ' set MM_offset to the last possible record

End If

' *** Move To Record: if we dont know the record count, check the display range

If (MM_rsCount = -1) Then

  ' walk to the end of the display range for this page
  i = MM_offset
  While (Not MM_rs.EOF And (MM_size < 0 Or i < MM_offset + MM_size))
    MM_rs.MoveNext
    i = i + 1
  Wend

  ' if we walked off the end of the recordset, set MM_rsCount and MM_size
  If (MM_rs.EOF) Then
    MM_rsCount = i
    If (MM_size < 0 Or MM_size > MM_rsCount) Then MM_size = MM_rsCount
  End If

  ' if we walked off the end, set the offset based on page size
  If (MM_rs.EOF And Not MM_paramIsDefined) Then
    If (MM_offset > MM_rsCount - MM_size Or MM_offset = -1) Then
      If ((MM_rsCount Mod MM_size) > 0) Then
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' reset the cursor to the beginning
  If (MM_rs.CursorType > 0) Then
    MM_rs.MoveFirst
  Else
    MM_rs.Requery
  End If

  ' move the cursor to the selected record
  i = 0
  While (Not MM_rs.EOF And i < MM_offset)
    MM_rs.MoveNext
    i = i + 1
  Wend
End If

' *** Move To Record: update recordset stats

' set the first and last displayed record
rsNewsItem_first = MM_offset + 1
rsNewsItem_last  = MM_offset + MM_size
If (MM_rsCount <> -1) Then
  If (rsNewsItem_first > MM_rsCount) Then rsNewsItem_first = MM_rsCount
  If (rsNewsItem_last > MM_rsCount) Then rsNewsItem_last = MM_rsCount
End If

' set the boolean used by hide region to check if we are on the last record
MM_atTotal = (MM_rsCount <> -1 And MM_offset + MM_size >= MM_rsCount)

' *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

' create the list of parameters which should not be maintained
MM_removeList = "&index="
If (MM_paramName <> "") Then MM_removeList = MM_removeList & "&" & MM_paramName & "="
MM_keepURL="":MM_keepForm="":MM_keepBoth="":MM_keepNone=""

' add the URL parameters to the MM_keepURL string
For Each Item In Request.QueryString
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepURL = MM_keepURL & NextItem & Server.URLencode(Request.QueryString(Item))
  End If
Next

' add the Form variables to the MM_keepForm string
For Each Item In Request.Form
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepForm = MM_keepForm & NextItem & Server.URLencode(Request.Form(Item))
  End If
Next

' create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL & MM_keepForm
if (MM_keepBoth <> "") Then MM_keepBoth = Right(MM_keepBoth, Len(MM_keepBoth) - 1)
if (MM_keepURL <> "")  Then MM_keepURL  = Right(MM_keepURL, Len(MM_keepURL) - 1)
if (MM_keepForm <> "") Then MM_keepForm = Right(MM_keepForm, Len(MM_keepForm) - 1)

' a utility function used for adding additional parameters to these strings
Function MM_joinChar(firstItem)
  If (firstItem <> "") Then
    MM_joinChar = "&"
  Else
    MM_joinChar = ""
  End If
End Function

' *** Move To Record: set the strings for the first, last, next, and previous links

MM_keepMove = MM_keepBoth
MM_moveParam = "index"

' if the page has a repeated region, remove 'offset' from the maintained parameters
If (MM_size > 0) Then
  MM_moveParam = "offset"
  If (MM_keepMove <> "") Then
    params = Split(MM_keepMove, "&")
    MM_keepMove = ""
    For i = 0 To UBound(params)
      nextItem = Left(params(i), InStr(params(i),"=") - 1)
      If (StrComp(nextItem,MM_moveParam,1) <> 0) Then
        MM_keepMove = MM_keepMove & "&" & params(i)
      End If
    Next
    If (MM_keepMove <> "") Then
      MM_keepMove = Right(MM_keepMove, Len(MM_keepMove) - 1)
    End If
  End If
End If

' set the strings for the move to links
If (MM_keepMove <> "") Then MM_keepMove = MM_keepMove & "&"
urlStr = Request.ServerVariables("URL") & "?" & MM_keepMove & MM_moveParam & "="
MM_moveFirst = urlStr & "0"
MM_moveLast  = urlStr & "-1"
MM_moveNext  = urlStr & Cstr(MM_offset + MM_size)
prev = MM_offset - MM_size
If (prev < 0) Then prev = 0
MM_movePrev  = urlStr & Cstr(prev)

End if
%>
<%
SQL = "SELECT TEAMCODE, TEAMNAME FROM G_TEAMCODES WHERE NEWSALLOWED = 1"
OpenDB()
Call OpenRs(rsTeams, SQL)
strTeams = ""
while not rsTeams.EOF
	strTeams = strTeams & "<option value='" & rsTeams("TEAMCODE") & "'>" & rsTeams("TEAMNAME") & "</option>"
	rsTeams.moveNext
wend
CloseRs(rsTeams)
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager News --> News --> Amend News</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script LANGUAGE="JavaScript">
	function newsItemGo(){
		form1.optionname.value  = document.form1.newsType.options[document.form1.newsType.selectedIndex].text;
		form1.action = "NewsItems_Amend.asp"
		form1.submit()
	}
</SCRIPT>
</HEAD>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<br>
    <table width="371" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td width="13" class="RSLBlack">&nbsp;</td>
        <td width="351" class="RSLBlack">Please select below what type 
          of news you wish to edit.</td>
        <td width="7" class="RSLBlack">&nbsp;</td>
      </tr>
      <tr> 
        <td colspan="3" height="5"></td>
      </tr>
      <tr> 
        <td width="13">&nbsp;</td>
        <td width="351"> 
          <form name="form1" method="post" action=""><input type="hidden" name="optionname">
            <select name="newsType" onChange="newsItemGo()" class="textbox200">
              <option>Select a news type</option>
              <option value="All">ALL</option>
			  <%=strTeams%>
            </select>
          </form>
        </td>
        <td width="7">&nbsp;</td>
      </tr>
    </table>
    <br>
    <% If NewsType <> "" Then %>
    <% If Not rsNewsItem.EOF Or Not rsNewsItem.BOF Then %>
<span class="RSLBlack">&nbsp;&nbsp;&nbsp;Please click on the <%= NewsOption %> news article you wish to <font color=red>AMEND</font><br>
<br>
</span> 
<table border="0" cellspacing="0" cellpadding="0">
  <tr class="RSLBlack"> 
    <td width="10">&nbsp;</td>
    <td width="306" style='border-bottom:1px solid black'><b>News 
      Headline</b></td>
    <td width=100px style='border-bottom:1px solid black'>&nbsp;<b>Date Created</b></td>
    <td width=100px style='border-bottom:1px solid black'>&nbsp;<b>Date Expires</b></td>	
    <td width=100px style='border-bottom:1px solid black'>&nbsp;<b>Expired</b></td>	
  </tr>
  <tr> 
    <td width="10"></td>
    <td height="4" ></td>
    <td width="10"></td>
  </tr>
  <% 
While ((Repeat1__numRows <> 0) AND (NOT rsNewsItem.EOF)) 
linecolour = "black"
hasexpired = "No"
if (rsNewsItem.Fields.Item("hasexpired").Value = 1) then
	linecolour = "red"
	hasexpired = "YES"
end if
%>
  <tr class="RSLBlack" style='color:<%=linecolour%>'> 
    <td width="10">&nbsp;</td>
    <td width="306"><a href="NewsItems_Amendb.asp?ID=<%=(rsNewsItem.Fields.Item("NewsID").Value)%>"><%=(rsNewsItem.Fields.Item("Title").Value)%></a> </td>
    <td>&nbsp;<%=(rsNewsItem.Fields.Item("DateCreated2").Value)%></td>
    <td>&nbsp;<%=(rsNewsItem.Fields.Item("DateExpires2").Value)%></td>	
    <td>&nbsp;<%=hasexpired%></td>		
  </tr>
  <tr> 
    <td width="10"></td>
    <td colspan="2" height="3" ></td>
  </tr>
  <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  rsNewsItem.MoveNext()
Wend
%>
  <tr> 
    <td width="10"></td>
    <td height="1" ></td>
    <td width="10"></td>
  </tr>
  <tr> 
    <td width="10"></td>
    <td colspan="2" height="3" ></td>
  </tr>
  <tr> 
    <td width="10"></td>
    <td colspan="4" height="1" bgcolor="#000000"></td>
  </tr>
  <tr> 
    <td width="10">&nbsp;</td>
    <td colspan="4" align=center height=20> 
      <table width="458" border="0" cellspacing="0" cellpadding="0">
        <tr class="RSLBlack"> 
          <td width="221"> 
            <div align="right">&lt;&lt; <A HREF="<%=MM_movePrev%>"><font color=blue>Prevous Items</font></a></div>
          </td>
          <td width="23">&nbsp;</td>
          <td width="214"><A HREF="<%=MM_moveNext%>"><font color=blue>Next 
            Items</font></a> &gt;&gt;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="10"></td>
    <td colspan="4" height="1" bgcolor="#000000"></td>
  </tr>
  <tr> 
    <td width="10">&nbsp;</td>
    <td width="306" >&nbsp;</td>
    <td width="76" class="RSLBlack">&nbsp;</td>
  </tr>
</table>
<% Else %>
<span><strong>&nbsp;&nbsp;&nbsp;No news articles exist for the 
selected option <br>
<br>
</strong></span> 
<% End If ' end Not rsNewsItem.EOF Or NOT rsNewsItem.BOF %> 
<% End If  %>

<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
If NewsType <> "" Then
	rsNewsItem.Close()
End if
%>
