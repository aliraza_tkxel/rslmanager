<%@ Language=VBScript %>
<!-- #include file="aspRoutines.asp" -->
<%On Error Resume Next%>
<html>
	<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script language="javascript" src="functions.js"></script>
	</head>
	<body bgcolor="#ffffff">
<%
On Error Resume Next
dim iTotal, sLeftIndent
dim bLoaded
Dim objDocument
Dim sOpenFolders
	
iTotal = 0
sLeftIndent = ""

bLoaded = fnLoadXMLData(objDocument)

if (bLoaded = true) then
	'Start building the menu table structure%>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<form name="frmMenu" action="menu.asp" method="post"><%
				sOpenFolders = Request.Form("hdnOpenFolders")
				
				'This subroutine generates the HTML for the menu based on
				'data loaded into the XML object
				DisplayNode objDocument.childNodes, iTotal, sLeftIndent, sOpenFolders%>
				<input type="hidden" name="hdnOpenFolders" value="<%=sOpenFolders%>">
				</form>
			</td>
		</tr>
	</table>
<%
end if
%>
<SCRIPT LANGUAGE="Javascript">
	<!--
		//These two arrays work with each other to identify the menu element that should
		//be hidden or made visible.  There is a one-to-one relationship between
		//the rows of each array.  For example arClickedElementID(0) contains the
		//ID to access the element ID stored in arAffectedMenuItemID(0).
			
		//Note: The value of the ASP variable iTotal used below represents the
		//total number of items and subitems in the menu.  The value is set by
		//reference in the DisplayNode() subroutine call
		var arClickedElementID = new Array(<% for i = 1 to iTotal %> "<%=i%>"<%if i < iTotal then%>,<%end if%> <%next%>);
		var arAffectedMenuItemID = new Array(<% for i = 1 to iTotal %> "<%=i+1%>"<%if i < iTotal then%>,<%end if%> <%next%>);
	//-->
</SCRIPT>
<%
'Release memory reserved by XML object
Set objDocument	= Nothing

if err <> 0 then
	'We got an error, so display the message
	Response.Write err.description
end if
%>
	</body>
</html>