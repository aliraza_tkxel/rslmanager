<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Response.ContentType = "text/xml"
	
	Set objDom = Server.CreateObject("Microsoft.XMLDOM")
	objDom.preserveWhiteSpace = True

	FLDID = Request("FLDID")
	IF (FLDID = "") THEN FLDID = -1
	SQL = "SELECT * FROM DOC_FOLDER WHERE PARENTFOLDER = " & FLDID & " ORDER BY FOLDERNAME"
	
	Call OpenDB()
	Call OpenRs(rsHE, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot
		While NOT rsHE.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsHE("FOLDERNAME")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the src attribute to the field node ***
		   Set objcolName = objDom.createAttribute("src")
		   objcolName.Text = "TREELOAD.asp?FLDID=" & rsHE("FOLDERID")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the action attribute to the field node ***
		   Set objcolName = objDom.createAttribute("action")
		   objcolName.Text = "Javascript:GO('FOLDER.asp?FLDID=" & rsHE("FOLDERID") & "&FL_A=L')"
		   objRow.SetAttributeNode(objColName)

		'   '*** Append the icon attribute to the field node ***
		 '  Set objcolName = objDom.createAttribute("icon")
		  ' objcolName.Text = "img2.gif"
		   'objRow.SetAttributeNode(objColName)
		'
		 '  '*** Append the openIcon attribute to the field node ***
		  ' Set objcolName = objDom.createAttribute("openIcon")
		   'objcolName.Text = "img2.gif"
	'	   o'bjRow.SetAttributeNode(objColName)
'
		   objRoot.appendChild objRow
	
			rsHE.moveNext()	
		Wend

    'FINALLY ADD THE NEW OPTION AS FOLLOWS
    Set objRow = objDom.createElement("tree")

    '*** Append the text attribute to the field node ***
    Set objcolName = objDom.createAttribute("text")
    objcolName.Text = "New Folder"
    objRow.SetAttributeNode(objColName)

    '*** Append the action attribute to the field node ***
    Set objcolName = objDom.createAttribute("action")
    objcolName.Text = "Javascript:GO('FOLDER.asp?FLDID=" & FLDID & "&FL_A=LFD')"
    objRow.SetAttributeNode(objColName)

    objRoot.appendChild objRow
	
	Call CloseRs(rsHE)
	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 
	

%>
