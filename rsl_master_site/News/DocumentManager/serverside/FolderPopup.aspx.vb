Imports System.Data.SqlClient
Imports dalDocumentManager
Imports dalDocumentManagerTableAdapters

Partial Class News_DocumentManager_serverside_DocumentManager
    Inherits MSDN.SessionPage

    Private rootNode As TreeNode

    ''' <summary>
    ''' loading page and checking user login.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID_193 As String = ASPSession("UserID")
        If (UserID_193 = "") Then
            Response.Redirect("/login/login.aspx?TIMEDOUT=1")
            'Response.Redirect "/Default.asp?TIMEDOUT=1"
        End If
    End Sub

    ''' <summary>
    ''' for documents manager in admin area on intranet
    ''' </summary>
    ''' <param name="startNode"></param>
    ''' <param name="Folder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Private Function AddElements(ByVal startNode As TreeNode, ByVal Folder As Integer) As TreeNode
        Try
            Dim nodeAdapter As New dalDocumentManagerTableAdapters.DOC_FOLDERTableAdapter
            Dim nodeTable As dalDocumentManager.DOC_FOLDERDataTable = nodeAdapter.GetDataByNodeID(Folder)
            Dim fileAdapter As New dalDocumentManagerTableAdapters.DOC_FILESTableAdapter
            Dim fileTable As dalDocumentManager.DOC_FILESDataTable = fileAdapter.GetData(Folder, ASPSession("TeamCode"), Integer.Parse(ASPSession("USERID")))
            'Response.Write(fileTable.Count.ToString)
            'Response.Write(nodeTable.Count.ToString)
            For Each sd As dalDocumentManager.DOC_FOLDERRow In nodeTable

                'Dim folderElem As TreeNode = New TreeNode(sd.FOLDERNAME, sd.FOLDERID.ToString, "images/folder.gif", str, "theRightSide")

                Dim folderElem As TreeNode = New TreeNode(sd.FOLDERNAME, sd.FOLDERID.ToString, "images/folder.gif")
                'startNode.ChildNodes.Add(AddElements(folderElem, sd.FOLDERID))
                folderElem.SelectAction = TreeNodeSelectAction.Select
                folderElem.PopulateOnDemand = True
                startNode.ChildNodes.Add(folderElem)
            Next
            'For Each fi As dalDocumentManager.DOC_FILESRow In fileTable
            '    Dim str As String = "DOCUMENT_AMEND.asp?DOCID=" + fi.DOCUMENTID.ToString + "&ACTION=AMEND"
            '    Dim fileElem As TreeNode = New TreeNode(fi.DOCNAME, fi.DOCUMENTID.ToString, "images/file.gif", str, "theRightSide")
            '    fileElem.SelectAction = TreeNodeSelectAction.Select
            '    fileElem.PopulateOnDemand = False
            '    startNode.ChildNodes.Add(fileElem)
            'Next
            'If startNode.Depth > 0 Then
            '    Dim tdn As TreeNode = New TreeNode("New Document", "-3", "images/file_new.gif", "DOCUMENT.asp?FLDID=" + Folder.ToString + "&ACTION=CREATE", "theRightSide")
            '    tdn.SelectAction = TreeNodeSelectAction.Select
            '    tdn.PopulateOnDemand = False
            '    startNode.ChildNodes.Add(tdn)
            'End If
            'Dim tfn As TreeNode = New TreeNode("New Folder", "-2", "images/folder_new.gif", "FOLDER.asp?FLDID=" + Folder.ToString + "&ACTION=CREATE", "theRightSide")
            'tfn.SelectAction = TreeNodeSelectAction.Select
            'tfn.PopulateOnDemand = False
            'startNode.ChildNodes.Add(tfn)
            Return startNode
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        End Try
    End Function


    Sub PopulateNode(ByVal source As Object, ByVal e As TreeNodeEventArgs)
        Dim tn As TreeNode = e.Node
       
        AddElements(tn, Integer.Parse(tn.Value))
    End Sub

    Protected Sub Select_Change(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvMain.SelectedNodeChanged
        Dim tv As TreeView = CType(sender, TreeView)
        lblFolderDestination.Text = tv.SelectedNode.Text
        ViewState("FolderId") = tv.SelectedNode.Value

    End Sub

    Protected Sub btnMove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMove.Click
        If lblFolderDestination.Text = "" Then
            ClientScript.RegisterStartupScript(Page.GetType, "selectFolder", "alert("" Please select a folder."");", True)
        Else
            Dim strParam As String = "'" + lblFolderDestination.Text + "'"
            strParam += "," + ViewState("FolderId")
            strParam += "," + Request.QueryString("DOCID")
            strParam += "," + Request.QueryString("FLDID")

            Dim strFunc As String
            strFunc = "MoveFolder(" + strParam + ");"
            Page.ClientScript.RegisterStartupScript(Page.GetType, "moveFolder", strFunc, True)
        End If

    End Sub

End Class
