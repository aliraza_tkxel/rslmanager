<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Response.ContentType = "text/xml"
	
	Set objDom = Server.CreateObject("Microsoft.XMLDOM")
	objDom.preserveWhiteSpace = True

	FLDID = Request("FLDID")
	IF (FLDID = "") THEN FLDID = -1
	SQL = "SELECT * FROM DOC_FOLDER WHERE PARENTFOLDER = " & FLDID & " ORDER BY FOLDERNAME"
	
	Call OpenDB()
	Call OpenRs(rsHE, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot
		
		While NOT rsHE.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsHE("FOLDERNAME")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the src attribute to the field node ***
		   Set objcolName = objDom.createAttribute("src")
		   objcolName.Text = "TREELOAD.asp?FLDID=" & rsHE("FOLDERID")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the action attribute to the field node ***
		   Set objcolName = objDom.createAttribute("action")
		   objcolName.Text = "Javascript:GO('FOLDER.asp?FLDID=" & rsHE("FOLDERID") & "&ACTION=UPDATE')"
		   objRow.SetAttributeNode(objColName)

		 '*** Append the icon attribute to the field node ***
		 	' Set objcolName = objDom.createAttribute("icon")
		  	' objcolName.Text = "img2.gif"
		   	' objRow.SetAttributeNode(objColName)
		
		 '*** Append the openIcon attribute to the field node ***
		  	' Set objcolName = objDom.createAttribute("openIcon")
		   	' objcolName.Text = "img2.gif"
			' o'bjRow.SetAttributeNode(objColName)

		   objRoot.appendChild objRow
	
			rsHE.moveNext()	
		Wend
	Call CloseRs(rsHE)
	
	SQL = 	"SELECT DOCUMENTID, DOCNAME " &_
			"FROM CONVERTCOMMA_RECORD_TO_MANY_RECORD() D " &_
			"	LEFT JOIN G_TEAMCODES TC on D.DOCFOR  COLLATE Latin1_General_BIN = TC.TEAMCODE  COLLATE Latin1_General_BIN " &_
			"	LEFT JOIN E_TEAM T ON T.TEAMID = TC.TEAMID " &_
			" WHERE FOLDERID = " & FLDID & " AND ((TC.TEAMCODE LIKE '%" & Session("TeamCode") & "%') OR (D.CREATEDBY = " & SESSION("USERID") & " AND D.DOCUMENTID = (SELECT MAX(DOCUMENTID) FROM DOC_DOCUMENT))) " &_
			" GROUP BY DOCUMENTID, DOCNAME " &_
			" ORDER BY DOCNAME "
			
	Call OpenRs(rsDoc, SQL)

	While NOT rsDoc.EOF
	
		Set objRow = objDom.createElement("tree")
	
		'*** Append the text attribute to the field node ***
		Set objcolName = objDom.createAttribute("text")
		objcolName.Text =rsDoc("DOCNAME")
		objRow.SetAttributeNode(objColName)
	
		'*** Append the action attribute to the field node ***
		Set objcolName = objDom.createAttribute("action")
		objcolName.Text = "Javascript:GO('DOCUMENT_AMEND.asp?DOCID=" & rsDoc("DOCUMENTID") & "&ACTION=AMEND')"
		objRow.SetAttributeNode(objColName)
	
		objRoot.appendChild objRow

			
	rsDoc.moveNext()	
	Wend
		
	Call CloseRs(rsDoc)
		

    'FINALLY ADD THE NEW OPTION (NEW FOLDER) AS FOLLOWS
    Set objRow = objDom.createElement("tree")

    '*** Append the text attribute to the field node ***
    Set objcolName = objDom.createAttribute("text")
    objcolName.Text = "New Folder" 
    objRow.SetAttributeNode(objColName)

    '*** Append the action attribute to the field node ***
    Set objcolName = objDom.createAttribute("action")
    objcolName.Text = "Javascript:GO('FOLDER.asp?FLDID=" & FLDID & "&ACTION=CREATE')"
    objRow.SetAttributeNode(objColName)
	
	   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)

    objRoot.appendChild objRow
	
	 'FINALLY ADD THE NEW OPTION (NEW DOCUMENT )AS FOLLOWS
    Set objRow = objDom.createElement("tree")

    '*** Append the text attribute to the field node ***
    Set objcolName = objDom.createAttribute("text")
    objcolName.Text = "New Document"
    objRow.SetAttributeNode(objColName)

    '*** Append the action attribute to the field node ***
    Set objcolName = objDom.createAttribute("action")
    objcolName.Text = "Javascript:GO('DOCUMENT.asp?FLDID=" & FLDID & "&ACTION=CREATE')"
    objRow.SetAttributeNode(objColName)

	
	 '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)

	  objRoot.appendChild objRow
	
	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 
	

%>
