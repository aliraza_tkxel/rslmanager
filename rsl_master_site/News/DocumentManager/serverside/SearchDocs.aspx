<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SearchDocs.aspx.vb" Inherits="News_DocumentManager_serverside_DocumentManager"
    Trace="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Manager</title>
    <link rel="stylesheet" href="../../../css/RSL.css" type="text/css" />
    <link rel="stylesheet" href="../../../net/StyleSheet.css" type="text/css" />
    <link rel="stylesheet" href="../../../BHAIntranet/CSS/General/MandReads.css" type="text/css" />

    <script type="text/javascript" language="javascript">
     
    </script>

    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
-->
</style>
</head>
<body bgcolor="#ffffff">
    <form id="frmMain" runat="server">
        <div id="searchContainer">
            <asp:Label ID="lblErrors" runat="server">
            </asp:Label>
            <strong>Search:</strong><br />
            <br />
            <table>
                <tr>
                    <td style="width: 100px">
                        Folder Name:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtFolderName" CssClass="textbox200" runat="server" Width="283px"></asp:TextBox></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Document Title:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtDocumentTitle" CssClass="textbox200" runat="server" Width="283px"></asp:TextBox></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Keywords:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtKeywords" CssClass="textbox200" runat="server" Width="283px"></asp:TextBox></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Creation Date</td>
                    <td style="width: 63px">
                        From:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtCreationDateFrom" CssClass="textbox" runat="server"></asp:TextBox></td>
                    <td style="width: 71px">
                        To:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtCreationDateTo" CssClass="textbox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Expiry Date</td>
                    <td style="width: 63px">
                        From:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtExpiryDateFrom" CssClass="textbox" runat="server"></asp:TextBox></td>
                    <td style="width: 71px">
                        To:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtExpiryDateTo" CssClass="textbox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 63px">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 71px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 63px">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 71px">
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="btnSearch" runat="server" CssClass="RSLButton" Text="Search...  "
                            Width="88px" /></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 63px">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 71px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
            </table>
            <br />
            <asp:ObjectDataSource ID="odsSearchDocs" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="DocSearchRefined" TypeName="dalMandatoryReadTableAdapters.I_MANDATORY_READTableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtFolderName" Name="FOLDERNAME" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtDocumentTitle" Name="DOCTITLE" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtKeywords" Name="KEYWORDS" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtCreationDateFrom"  Name="CREATIONDATEFROM" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="txtCreationDateTo" Name="CREATIONDATETO" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="txtExpiryDateFrom" Name="EXPIRYDATEFROM" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="txtExpiryDateTo" Name="EXPIRYDATETO" PropertyName="Text"
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:GridView CssClass="gvwMand" ShowHeader="False" ID="gvwDoc" AutoGenerateColumns="False"
                runat="server" AllowPaging="True" PageSize="3" >
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <br />
                            <div class="docimage">
                                <asp:Image ID="imgEmp" ImageUrl='<%# docType(eval("DOCFILE")) %>' runat="server" />
                            </div>
                            <div class="doccontent">
                                <div class="doctitle">
                                    <asp:Label ID="lblNameText" runat="server" Text="Document" />:
                                    <asp:Label ID="lblTitleText" runat="server" Text='<%# eval("DOCNAME") %>' />
                                </div>
                                <div class="docdetails">
                                    <asp:Label ID="lblTeam" runat="server" Text="View Doc:" />
                                    <asp:LinkButton ID="lnkDoc" OnClick="Searchlink_Click" CommandArgument='<%# eval("DOCUMENTID") %>' runat="server">Click here</asp:LinkButton>                            
                                    <br />
                                    <asp:Label ID="lblDate" runat="server" Text="Date Submitted:" />
                                    <asp:Label ID="lblDateText" runat="server" Text='<%# FormatDate(eval("DATECREATED")) %>' />
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </ItemTemplate>                        
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                No documents found.
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
