<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<% 
Dim TheAction,DOCUMENTID,DocumentVerified

Function GetPostedData()
    TheAction = Request("hid_Action")
 End Function

'Function gets the posted data value
Function UploadFormRequest(name)
   if(name<>"") Then
    if Request(name)<>"" then
        UploadFormRequest = Request(name)
    end if      
   end if 
End Function
Function AmendedFileName(FilePath)
    Dim FilePart
    
    FilePart=Split(filePath, "\")
    AmendedFileName=FilePart(UBound(FilePart))
End Function

Function sendEmail()
    if UploadFormRequest("chkMandatory") = 1  then

        sql=   "SELECT WORKEMAIL FROM dbo.E_CONTACT C " &_
            "INNER JOIN dbo.E_JOBDETAILS J ON J.EMPLOYEEID=C.EMPLOYEEID " &_
            "AND  J.ACTIVE=1 AND J.TEAM IN ( " &_
            "SELECT t.TEAMID FROM E_TEAM T " &_
            "INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID " &_
            "WHERE T.ACTIVE = 1 AND WORKEMAIL IS NOT NULL " &_
            "AND tc.teamcode IN (" & UploadFormRequest("hid_TEAMCODES") & "))"
        
        Call OpenRS(rsEmail, SQL)
        If Not rsEmail.EOF Then
  
            'Do until the end of all records
            Do Until rsEmail.EOF
                if strRecipiants<>"" then
                    strRecipiants=strRecipiants & ", " & rsEmail("WORKEMAIL")
                else
                    strRecipiants = rsEmail("WORKEMAIL")
                End if
                rsEmail.MoveNext
            Loop
   
        End If 
    Call CloseRs(rsEmail)
 

    Dim iMsg
    Set iMsg = CreateObject("CDO.Message")
    Dim iConf
    Set iConf = CreateObject("CDO.Configuration")

    Dim Flds
    Set Flds = iConf.Fields
    Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

    emailbody = "A mandatory read document <b>" &  UploadFormRequest("txt_DOCNAME") & "</b> has been uploaded into document directory. Please log on to RSLmanager to access the document. <br/>"
                
    Set iMsg.Configuration = iConf
    iMsg.To = strRecipiants
    iMsg.From = "DocumentManager@broadlandgroup.org"
    iMsg.Subject = "New Mandatory Read Document"
    iMsg.HTMLBody = emailbody
    'iMsg.TextBody = emailbody
    iMsg.Send
 end if
End Function

Sub InsertRecord()
    
  MM_editConnection = RSL_CONNECTION_STRING
  MM_editTable = "dbo.DOC_DOCUMENT"
  MM_editRedirectUrl = ""
  MM_fieldsStr  = "hid_FOLDERID|value|sel_TYPE|value|txt_DOCNAME|value|txt_DATEEXPIRES|value|hid_DOCFOR|value|hid_USERID|value|chkMandatory|value|txtKeywords|value"
  MM_columnsStr = "FOLDERID|',none,''|TYPEID|',none,''|DOCNAME|',none,''|DATEEXPIRES|',none,''|DOCFOR|',none,''|CREATEDBY|',none,''|MANDATORY|,none,0|KEYWORDS|',none,''"

  'Create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")

 
   
  
  ' Set the form values
   For i = LBound(MM_fields) To UBound(MM_fields) Step 2
      MM_fields(i+1) = UploadFormRequest(MM_fields(i))
   Next
  
  MM_editQuery = ""
  MM_tableValues = ""
  MM_dbValues = ""
  DOCUMENTID=""
  
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End if
    MM_tableValues = MM_tableValues & MM_columns(i)
    MM_dbValues = MM_dbValues & FormVal
  Next
  
    ' this part adds an amend item  for the FILE NAME so that if the filename is changed it is shown in the DB
  if UploadFormRequest("rslfile") <> "" then
	  if AmendedFileName(UploadFormRequest("rslfile")) <> "" then 
		AdditionalAdd_COL = ",DOCFILE"
		AdditionalAdd_FIELD = ",'" & AmendedFileName(UploadFormRequest("rslfile")) & "'"
	  Else
		AdditionalAdd_COL = ",DOCFILE"
		AdditionalAdd_FIELD = ",'" & UploadFormRequest("rslfile") & "'"
	  End If
  End If

  MM_editQuery = "SET NOCOUNT ON;insert into " & MM_editTable & " (" & MM_tableValues & AdditionalAdd_COL & ") values (" & MM_dbValues & AdditionalAdd_FIELD & "); SELECT SCOPE_IDENTITY() AS DOCUMENTID;"
  

  ' execute the insert
    set MM_editCmd = Conn.Execute(MM_editQuery)
    DOCUMENTID = MM_editCmd.fields("DOCUMENTID").value
    Call sendEmail()
  
End Sub
Sub AmendRecord()

  MM_editConnection = RSL_CONNECTION_STRING
  MM_editTable = "dbo.DOC_DOCUMENT"
  MM_editColumn = "DOCUMENTID"
  MM_recordId = "" + UploadFormRequest("MM_recordId") + ""
  DocumentID = UploadFormRequest("MM_recordId")
  MM_editRedirectUrl = "Document_Amend.asp?MaxID=" + MM_recordId
 
  'if a new document has been uploaded then we need to change the file path if not , dont
  MM_fieldsStr  = "sel_TYPE|value|txt_DOCNAME|value|txt_DATEEXPIRES|value|hid_DOCFOR|value|hid_USERID|value|chkMandatory|value|hid_DATEMODIFIED|VALUE"
  MM_columnsStr = "TYPEID|',none,''|DOCNAME|',none,''|DATEEXPIRES|',none,''|DOCFOR|',none,''|MODIFIEDBY|',none,''|MANDATORY|,none,0|DATEMODIFIED|',none,''"


  'create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  
  'set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(UploadFormRequest(MM_fields(i)))
  Next
  
  MM_editQuery = ""
  MM_tableValues = ""
  MM_dbValues = ""
  
  ' create the sql update statement
  MM_editQuery = "update " & MM_editTable & " set "
  
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    
    If (Delim = "none") Then Delim = ""
    
    AltVal = MM_typeArray(1)
    
    If (AltVal = "none") Then AltVal = ""
    
    EmptyVal = MM_typeArray(2)
    
    If (EmptyVal = "none") Then EmptyVal = ""
    
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_editQuery = MM_editQuery & ","
    End If
    MM_editQuery = MM_editQuery & MM_columns(i) & " = " & FormVal
  Next

  'this part adds an amend item of for the FILE NAME so that if the filename is changed it is shown in the DB
  if UploadFormRequest("rslfile") <> "" then
	  if AmendedFileName(UploadFormRequest("rslfile")) <> "" then 
		AdditionalEdit = ", DOCFILE = '" & AmendedFileName(UploadFormRequest("rslfile")) & "'"
	  Else
		AdditionalEdit = ", DOCFILE = '" & UploadFormRequest("rslfile") & "'"
	  End If
  End If

  'this is the SQL STRING that is dynamically created
  MM_editQuery = MM_editQuery & AdditionalEdit & " where " & MM_editColumn & " = " & MM_recordId
 
  'execute the insert
  set MM_editCmd = Conn.Execute(MM_editQuery)
  DOCUMENTID=""
  response.Write (MM_recordId)
  DOCUMENTID=MM_recordId

End Sub

GetPostedData()
OpenDB()
Select Case TheAction 
	Case "INSERT"   InsertRecord()
	Case "AMEND" AmendRecord()
End Select
CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Document Insert-Amend</title>
    <script language="javascript" type="text/javascript">
        function sendData() {
            if ("<%=TheAction%>" == "INSERT" || "<%=TheAction%>" == "AMEND") {
                parent.Add_Document("<%=DOCUMENTID%>")
                parent.parent.theDocBar.ReloadMe();

                //Set the page message
               
                    parent.document.getElementById("pMessage").style.display = "block";
                    parent.document.getElementById("tblForm").style.display = "none";
                
            }
            return;

        }
    </script>
</head>
<body onload="sendData();">
</body>
</html>