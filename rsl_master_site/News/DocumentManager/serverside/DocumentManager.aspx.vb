Imports System.Data.SqlClient

Partial Class News_DocumentManager_serverside_DocumentManager
    Inherits MSDN.SessionPage

    Private rootNode As TreeNode

    ''' <summary>
    ''' loading page and checking user login.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID_193 As String = ASPSession("UserID")
        If (UserID_193 = "") Then
            Response.Redirect("/login/login.aspx?TIMEDOUT=1")
            'Response.Redirect "/Default.asp?TIMEDOUT=1"
        End If
    End Sub

    ''' <summary>
    ''' for documents manager in admin area on intranet
    ''' </summary>
    ''' <param name="startNode"></param>
    ''' <param name="Folder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Private Function AddElements(ByVal startNode As TreeNode, ByVal Folder As Integer) As TreeNode
        Try
            Dim nodeAdapter As New dalDocumentManagerTableAdapters.DOC_FOLDERTableAdapter
            Dim nodeTable As dalDocumentManager.DOC_FOLDERDataTable = nodeAdapter.GetDataByNodeID(Folder)
            Dim fileAdapter As New dalDocumentManagerTableAdapters.DOC_FILESTableAdapter
            Dim fileTable As dalDocumentManager.DOC_FILESDataTable = fileAdapter.GetData(Folder, ASPSession("TeamCode"), Integer.Parse(ASPSession("USERID")))
            'Response.Write(fileTable.Count.ToString)
            'Response.Write(nodeTable.Count.ToString)
            For Each sd As dalDocumentManager.DOC_FOLDERRow In nodeTable
                Dim str As String = "FOLDER.asp?FLDID=" + sd.FOLDERID.ToString + "&ACTION=UPDATE"
                Dim folderElem As TreeNode = New TreeNode(sd.FOLDERNAME, sd.FOLDERID.ToString, "images/folder.gif", str, "theRightSide")
                'startNode.ChildNodes.Add(AddElements(folderElem, sd.FOLDERID))
                folderElem.SelectAction = TreeNodeSelectAction.Select
                folderElem.PopulateOnDemand = True
                startNode.ChildNodes.Add(folderElem)
            Next
            For Each fi As dalDocumentManager.DOC_FILESRow In fileTable
                Dim str As String = "DOCUMENT_AMEND.asp?DOCID=" + fi.DOCUMENTID.ToString + "&ACTION=AMEND"
                Dim fileElem As TreeNode = New TreeNode(fi.DOCNAME, fi.DOCUMENTID.ToString, "images/file.gif", str, "theRightSide")
                fileElem.SelectAction = TreeNodeSelectAction.Select
                fileElem.PopulateOnDemand = False
                startNode.ChildNodes.Add(fileElem)
            Next
            If startNode.Depth > 0 Then
                Dim tdn As TreeNode = New TreeNode("New Document", "-3", "images/file_new.gif", "DOCUMENT.asp?FLDID=" + Folder.ToString + "&ACTION=CREATE", "theRightSide")
                tdn.SelectAction = TreeNodeSelectAction.Select
                tdn.PopulateOnDemand = False
                startNode.ChildNodes.Add(tdn)
            End If
            Dim tfn As TreeNode = New TreeNode("New Folder", "-2", "images/folder_new.gif", "FOLDER.asp?FLDID=" + Folder.ToString + "&ACTION=CREATE", "theRightSide")
            tfn.SelectAction = TreeNodeSelectAction.Select
            tfn.PopulateOnDemand = False
            startNode.ChildNodes.Add(tfn)
            Return startNode
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' for documents directory view on intranet
    ''' </summary>
    ''' <param name="startNode"></param>
    ''' <param name="Folder"></param>
    ''' <param name="fname"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Private Function AddElementsDocs(ByVal startNode As TreeNode, ByVal Folder As Integer, ByVal fname As String) As TreeNode
        Try
            Dim nodeAdapter As New dalDocumentManagerTableAdapters.DOC_FOLDERTableAdapter
            Dim nodeTable As dalDocumentManager.DOC_FOLDERDataTable = nodeAdapter.GetDataByNodeID(Folder)
            Dim fileAdapter As New dalDocumentManagerTableAdapters.DOC_FILESTableAdapter
            Dim fileTable As dalDocumentManager.DOC_FILESDataTable = fileAdapter.GetData(Folder, ASPSession("TeamCode"), Integer.Parse(ASPSession("USERID")))
            'Response.Write(fileTable.Count.ToString)
            'Response.Write(nodeTable.Count.ToString)
            For Each sd As dalDocumentManager.DOC_FOLDERRow In nodeTable
                'Dim str As String = "FOLDER.asp?FLDID=" + sd.FOLDERID.ToString + "&ACTION=UPDATE"
                Dim folderElem As TreeNode = New TreeNode(sd.FOLDERNAME, sd.FOLDERID.ToString, "images/folder.gif")
                'startNode.ChildNodes.Add(AddElements(folderElem, sd.FOLDERID))
                folderElem.SelectAction = TreeNodeSelectAction.Expand
                folderElem.PopulateOnDemand = True
                startNode.ChildNodes.Add(folderElem)
            Next
            For Each fi As dalDocumentManager.DOC_FILESRow In fileTable
                Dim str As String = "/NEWS/DOCUMENTMANAGER/DOCUMENTS/" + fname + "/" + fi.DOCFILE
                Dim fileElem As TreeNode = New TreeNode(fi.DOCNAME, fi.DOCUMENTID.ToString, "images/file.gif", str, "_blank")
                fileElem.SelectAction = TreeNodeSelectAction.Select
                fileElem.PopulateOnDemand = False
                startNode.ChildNodes.Add(fileElem)
            Next
            Return startNode
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' get executed as event when node try to fill elements on fly.
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub PopulateNode(ByVal source As Object, ByVal e As TreeNodeEventArgs)
        Dim tn As TreeNode = e.Node
        If Not Request.QueryString("dir") Is Nothing Then
            AddElementsDocs(tn, Integer.Parse(tn.Value), tn.Text)
        Else
            AddElements(tn, Integer.Parse(tn.Value))
        End If
    End Sub

    Protected Sub tvMain_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvMain.SelectedNodeChanged

    End Sub
End Class
