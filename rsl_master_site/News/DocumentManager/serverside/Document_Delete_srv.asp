<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
DIM RQ_DOCID,DeletionMessage
	RQ_DOCID = REQUEST("hid_DocID")

    Call OpenDB()
        
        'DELETE THE DOCUMENT FROM THE DATABASE
	    SQL = "DELETE FROM DOC_DOCUMENT WHERE DOCUMENTID = " & RQ_DOCID
	    conn.execute(SQL)
	    
	Call CloseDB()
	
	DeletionMessage = "Document Deleted Successfully"

%>
<html xmlns="http://www.w3.org/1999/xhtml"  >
    <head>  
        <title>Document Delete</title> 
        <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
        <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
        <link href="/css/RSL.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            <!--
                .style1 
                {	
                    color: #003399;
	                font-weight: bold;
                }
            -->
        </style>
    
        <script type="text/javascript"   language="javascript">
	        parent.theDocBar.ReloadMe()
        </script>
    </head>
    <body >
        <table width="437" border="0" cellspacing="1" cellpadding="1" class="RSLBLack">
            <tr>
                <td colspan="2" style=" white-space:nowrap">        <br/>
                    <table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width:1px;height:1px;" ></td>
                            <td style="width:220px;height:1px; background-color:#669900"></td>
                            <td style="width:1px;height:1px;"></td>
                        </tr>
                        <tr>
                            <td style="width:1px; background-color:#669900;"></td>
                            <td  style="width:1px;height:23px; background-color:#FFFFFF;" >
                                <table width="285" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="width:5px;" >&nbsp;</td>
                                        <td><%=DeletionMessage%></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:1px; background-color:#669900;"></td>
                        </tr>
                        <tr>
                            <td style="width:1px;height:1px;"></td>
                            <td style="width:220px;height:1px; background-color:#669900;" ></td>
                            <td style="width:1px;height:1px;" ></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width:84px;"></td>
            </tr>
        </table>
    </body>
</html>

