<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
bypasssecurityaccess = true 
server.ScriptTimeout = 1000000
%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
' *** Edit Operations: (Modified for File Upload) declare variables
MM_editAction = CStr(Request.ServerVariables("URL")) 'MM_editAction = CStr(Request("URL"))
%>

<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Building page first load
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	OpenDB()
	Dim rqDocumentID
	' GET THE INFORMATION FOR THE DOCUMENT YOU ARE AMENDING
	rqDocumentID = request("DOCID")
	IF NOT rqDocumentID <> "" THEN rqDocumentID = -1 END IF
		
	SQL = 	"SELECT D.*, DT.DESCRIPTION AS DOCTYPEDESC, E.FIRSTNAME + ' ' + E.LASTNAME AS CREATEDBYWHO , E2.FIRSTNAME + ' ' + E2.LASTNAME AS MODIFIEDBYWHO " &_
			"FROM DOC_DOCUMENT D  " &_
			"	LEFT JOIN G_DOCUMENTTYPE DT ON DT.DOCUMENTTYPEID = D.TYPEID  " &_
			"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = D.CREATEDBY  " &_
			"	LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = D.MODIFIEDBY  " &_
			"WHERE DOCUMENTID = " & rqDocumentID
	
	Call OpenRs(rsDoc, SQL)
	

		doc_DOCUMENTID 	= rsDoc("DOCUMENTID")
		doc_DOCNAME 	= rsDoc("DOCNAME")
		doc_DATEEXPIRES = rsDoc("DATEEXPIRES")
		doc_TYPE 		= rsDoc("TYPEID")
		doc_DOCFOR 		= rsDoc("DOCFOR")
		doc_FOLDERID	= rsDoc("FOLDERID")
		doc_TYPEDESC	= rsDoc("DOCTYPEDESC")
		doc_DOCFILE		= rsDoc("DOCFILE")
		doc_CREATIONDATE= rsDoc("DATECREATED")
		doc_CREATEDBY	= rsDoc("CREATEDBYWHO")
		doc_CREATEDBYID	= rsDoc("CREATEDBY")
		doc_KEYWORDS    = rsDoc("KEYWORDS")
		doc_MAND        = rsDoc("MANDATORY")
			    
		if doc_MAND = true then
		str_Mand = "checked"
		end if
			
		if cInt(doc_CREATEDBYID) = cInt(Session("USERID")) then 
		doc_DELETEPERMISSION = 1
		End If
		doc_MODIFIEDDATE= rsDoc("DATEMODIFIED")
		doc_MODIFIEDDBY	= rsDoc("MODIFIEDBYWHO")
		
		IF doc_FOLDERID <> "" THEN	theFolderPath = GetFolderPath() END IF
		
	Call CloseRs(rsDoc)
	
	'GET THE LIST OF DEFAULT USERS WHO CAN ACCESS THE 
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'CANDELETEALLDOCS' AND DEFAULTVALUE = " &  cInt(Session("USERID"))   
	Call OpenRs(rsDocDefault, SQL)
	If not rsDocDefault.EOF then
	    doc_DELETEPERMISSION = 1
	end if
	CloseRs(rsDocDefault)
	
	'GET THE LIST OF DOC TYPES
	SQL = "SELECT DOCUMENTTYPEID, DESCRIPTION FROM G_DOCUMENTTYPE ORDER BY DESCRIPTION ASC"
	
	Call OpenRs(rsDocType, SQL)
	strDocType = ""
	while not rsDocType.EOF
		IF rsDocType("DOCUMENTTYPEID") = doc_TYPE THEN
			setSelected = "selected"
		ELSE
			setSelected = ""
		END IF
		strDocType = strDocType & "<option value='" & rsDocType("DOCUMENTTYPEID") & "' " & setSelected & ">" & rsDocType("DESCRIPTION") & "</option>"
		rsDocType.moveNext
	wend
	CloseRs(rsDocType)
	
	' GET THE LIST OF TEAMS (checks are activated in javascript function onload)
	SQL = 	"SELECT TC.TEAMCODE , T.TEAMNAME FROM e_TEAM T " &_
			"	INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE ACTIVE=1"
	Call OpenRS(rsTeams, SQL)
	TeamStr = ""

  If Not rsTeams.EOF Then
            TeamStr = TeamStr & "<table width=400>"
            LinkCount = 0
            
            ' Do until the end of all records
            Do Until rsTeams.EOF
         
             'change 5 to any number of columns you like 
            If LinkCount Mod 2 = 0 Then
            
               If LinkCount <> 0 Then TeamStr = TeamStr & "</tr>"
                  TeamStr = TeamStr &"<TR><TD>" & rsTeams("TEAMNAME") & "&nbsp;</TD><TD>:&nbsp;</TD><TD><input type=""checkbox"" id=""DocType"" value=""" & rsTeams("TEAMCODE") & """></TD>"
               Else
                  TeamStr = TeamStr &"<TD>" & rsTeams("TEAMNAME") & "&nbsp;</TD><TD>:&nbsp;</TD><TD><input type=""checkbox"" id=""DocType"" value=""" & rsTeams("TEAMCODE") & """></TD>"
               End If

               LinkCount = LinkCount + 1
               
            'loop till end of records
            rsTeams.MoveNext
            Loop
            
            TeamStr = TeamStr & "</tr></table>"
         Else 

            'Write no records in there are no records
            Response.Write"Sorry, no teams were found!"
         
         End If
	
	
	CloseRs(rsTeams)
	
	Function GetFolderPath()

		FolderID= doc_FOLDERID
		SQL = "SELECT FOLDERNAME, PARENTFOLDER FROM DOC_FOLDER WHERE FOLDERID = " & FolderID
		
		Call OpenRs(rsFolder, SQL)
		
		FolderName = "/" & rsFolder("FOLDERNAME") 
		FolderID = rsFolder("PARENTFOLDER")
		Call CloseRs(rsFolder)
		
		While not PathStart = "END"
		
			SQL = "SELECT PARENTFOLDER,FOLDERNAME FROM DOC_FOLDER WHERE FOLDERID = " & FolderID
			Call OpenRs(rsFolderPath, SQL)
			
			IF NOT rsFolderPath.EOF THEN
				FolderName = "/" & rsFolderPath("FOLDERNAME") & FolderName
				IF rsFolderPath("PARENTFOLDER") <> -1 THEN
					FolderID = rsFolderPath("PARENTFOLDER")
				ELSE
					PathStart =  "END"
				END if
			ELSE
				PathStart = "END"
			END IF
			
			Call CloseRs(rsFolderPath)
			
		Wend
				GetFolderPath =  FolderName
	End Function
CloseDB()

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' End of building page first load
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style1 {
	color: #003399;
	font-weight: bold;
}
-->
</style>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/menu.js"></SCRIPT>
<script language="JavaScript">
function getFileExtension(filePath) { //v1.0
  fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/')+1,filePath.length) : filePath.substring(filePath.lastIndexOf('\\')+1,filePath.length));
  return fileName.substring(fileName.lastIndexOf('.')+1,fileName.length);
}

function checkFileUpload(form,extensions) { //v1.0

  document.MM_returnValue = true;
  if (extensions && extensions != '') {
    for (var i = 0; i<form.elements.length; i++) {
	  
      field = form.elements[i];
	 
      if (field.type.toUpperCase() != 'FILE') continue;
      if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
        ManualError('img_DOCUMENTFILE','This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.',0);
       
		document.MM_returnValue = false;field.focus();
		return false;
		break;
  } } }
	ManualError('img_DOCUMENTFILE','',3);
	return true;
}
</script>
<script language="JavaScript">
<!--
var FormFields = new Array()
FormFields[0] = "txt_DOCNAME|Doc Name|TEXT|Y"						
FormFields[1] = "txt_DATEEXPIRES|Expiry Date|DATE|Y"						
FormFields[2] = "hid_DOCFOR|Share Doc|TEXT|Y"

function BtnSubmit(){
	
	temp = document.getElementsByName("DocType")
	
	strNews = ""
	if (temp.length){
		for (i=0; i<temp.length; i++){
			if (temp[i].checked == true)
				strNews += "," + temp[i].value
			}
		}
	if (strNews != "") strNews += ","
	
	document.getElementById("hid_DOCFOR").value = strNews
	result = checkFileUpload(UserNews,'doc;txt;xls;pdf;jpeg;jpg;gif;bmp;ppt;docx;xlsx;pptx');
	if (!checkForm()) return false;	
	if (!result) return false;
	go();
	}

function go(){	
    UserNews.hid_Action.value="AMEND";
    UserNews.target = "ServerFrame"; 
	UserNews.action = "Document_srv.asp";
	UserNews.submit();	
	}

function SelectAll(newStatus) {
	temp = document.getElementsByName("DocType")
	if (temp.length){
		for (i=0; i<temp.length; i++){
			temp[i].checked = newStatus
			}	
		}
	}
	
function SetChecks(){
	Data = "<%=doc_DOCFOR%>"
	if (Data != "") {
		DataArray = Data.split(",")
		checkRef = document.getElementsByName("DocType")
		
		for (i=1; i<Data.length-1; i++){
			if (checkRef.length){
				for (j=0; j<checkRef.length; j++){
					if (checkRef[j].value == DataArray[i])
						checkRef[j].checked = true;
					}
				}
			}
		}
	}
	
function ShowHideDivs(cycle){

	if (cycle == 1){
		div_details.style.display = "none"
		div_amend.style.display = "block"
		}
	else if (cycle == 2){
		div_details.style.display = "block"
		div_amend.style.display = "none"
		}

}

function DeleteDocument(){

	<% if doc_DELETEPERMISSION = 1 then %>
	if (confirm("Are you sure you wish to delete this document?  If YES click 'OK', if NO click 'Cancel'")){
		UserNews.hid_DocID.value = "<%=doc_DOCUMENTID%>"
		UserNews.action = "Document_delete_srv.asp?hid_DocID=<%=doc_DOCUMENTID%>";
		UserNews.submit();	
	}
	<% else %>
	
	alert("Only the creator of this document is allowed to delete this document.")
	
	<% End If %>

}

function MoveDocument(){

    var strUrlPrms = "DOCID=<%=doc_DOCUMENTID%>&FLDID=<%=doc_FOLDERID%>&ACTION=MOVETOFOLDER";   
    window.open("FolderPopup.aspx?" + strUrlPrms, "_blank","TOP=200, LEFT=400, scrollbars=yes, height=590, width=460, status=NO, resizable= Yes");		
}
function getmimetype(docfile)
{
    var mimetype,filetype,splitnametype
    
    splitnametype=docfile.split(".")
    
    //filetype=docfile.substring(docfile.length-3,docfile.length)
    filetype=splitnametype[1]
    
    switch (filetype)
    {
        case 'xls':
            mimetype='application/vnd.ms-excel'
            break;
        case 'doc':    
            mimetype='application/msword'
            break;
        case 'ppt':
            mimetype='application/vnd.ms-powerpoint'    
            break;
        case 'gif':    
            mimetype='image/gif'
            break;
        case 'jpg':    
            mimetype='image/jpeg'
            break;
        case 'pdf':
            mimetype='application/pdf'
            break;
        case 'docx':
            mimetype='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            break;
        case 'xlsx':
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            break;
        case 'pptx':
            mimetype='application/vnd.openxmlformats-officedocument.presentationml.presentation'        
            break;
        default:
            mimetype=''
   }
    
   return  mimetype;
       
}

function View_Doc()
{
    var mimetype=getmimetype("<%=doc_DOCFILE%>");
    var strUrlPrms = "ff=DOCUMENTFILE&fn=DOCFILE&tn=DOC_DOCUMENT&cs=connRSL&cr=DocumentId=<%=doc_DOCUMENTID%>&mm="+mimetype;
    window.open("/DBFileUpload/uploader.aspx?" + strUrlPrms, "_blank","TOP=200, LEFT=400, scrollbars=yes, height=590, width=460, status=NO, resizable= Yes");		
}
function Add_Document(docid)
{
  UserNews.encoding = "multipart/form-data"; 
  UserNews.target = "ServerFrame";
  UserNews.keycriteria.value="DOCUMENTID="+docid;
  UserNews.action = "/DBFileUpload/uploader.aspx";
  UserNews.submit();
}    
//-->
</script>
</head>
<body onLoad="SetChecks()">
<form METHOD="POST" name="UserNews" id="UserNews">
<div id="div_RIGHTSIDE" name="div_RIGHTSIDE" style="display:block " >
  <table border="0" cellspacing="0" cellpadding="0" width=100%>
	<tr> 
	  <td> 
		<p id="pMessage" style="display:none;margin-left:10px;padding:5px 5px 5px 5px;border:solid 1px #669900;"  >Your Document has been Amended.</p>
		 <!-- action="<%=MM_editAction%>" //-->
	      <table id="tblForm" width="449" border="0" cellspacing="0" style="display:block;" cellpadding="0">
            <tr>
              <td width="10">&nbsp;</td>
              <td><table width="437" border="0" cellspacing="1" cellpadding="1" class="RSLBLack">
                <tr>
                  <td colspan="3" nowrap><br>
                      <br>
                      <table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="448" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><span class="style1">Document</span> : <%=doc_DOCNAME%></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                  <td colspan="2" nowrap>&nbsp;</td>
                  <td width="39">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3" nowrap><table width="422" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="108">View Document </td>
                        <!--<td width="59"><a href="/NEWS/DOCUMENTMANAGER/DOCUMENTS<%=theFolderPath%>/<%=doc_DOCFILE%>" target="_blank"><img src="/BoardMembers/images/im_rightbutton.gif" width="11" height="11" border="0" onclick="View_Doc();"></a></td>-->
                        <td width="59"><img alt=""  src="/BoardMembers/images/im_rightbutton.gif" style="border:0;width:11px;height:11px; cursor:pointer;"  onclick="View_Doc();"/></td>
                        <td width="127">Delete Document </td>
                        <td width="128">
							<img src="/BoardMembers/images/trash.gif" width="13" height="12" style="cursor:hand" onClick="DeleteDocument()">
                         	<input type="hidden" name="hid_DocID" id="hid_DocID" value="9">
						</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td colspan="3">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="108">Amend Details </td>
                        <td width="59"><img src="/BoardMembers/images/im_down.gif" width="11" height="11" style="cursor:hand" onClick="ShowHideDivs(1)"></td>
                        <td width="127">Move Document </td>
                        <td width="128">
							<img src="/BoardMembers/images/im_rightbutton.gif" width="13" height="12" style="cursor:hand" onClick="MoveDocument()">
                         	<input type="hidden" name="hid_MoveDocID" id="hid_MoveDocID" value="9">
						</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="68" nowrap>&nbsp;</td>
                  <td width="16">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan=2></td>
                  </tr>
              </table>
			  <div name="div_details" id="div_details" style="display:block">
                <table width="437" border="0" cellspacing="1" cellpadding="1" class="RSLBLack">
                  <tr>
                    <td colspan="5" nowrap><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="448" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><span class="style1"> Details </span></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="2" nowrap>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width="257">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="8" nowrap>&nbsp;</td>
                    <td width="107" nowrap><strong>Doc Title </strong></td>
                    <td width="14">:&nbsp;</td>
                    <td><%=doc_DOCNAME%></td>
                    <td width="35"></td>
                  </tr>
                  <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td width="8">&nbsp;</td>
                    <td><strong>Expiry Date </strong></td>
                    <td>:&nbsp;</td>
                    <td><%=doc_DATEEXPIRES%></td>
                    <td></td>
                  </tr>
				    <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td width="8" valign=top>&nbsp;</td>
                    <td valign=top><strong>Type</strong></td>
                    <td valign=top>:</td>
                    <td><%=doc_TYPEDESC%></td>
                    <td valign=top>&nbsp;</td>
                  </tr>
                   <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td valign=top>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5" valign=top><table  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="1" bgcolor="#669900"></td>
                        <td width="448" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="5">&nbsp;</td>
                              <td><span class="style1"> Modification History </span></td>
                            </tr>
                        </table></td>
                        <td width="1" bgcolor="#669900"></td>
                      </tr>
                      <tr>
                        <td width="1" height="1"></td>
                        <td width="220" height="1" bgcolor="#669900"></td>
                        <td width="1" height="1"></td>
                      </tr>
                    </table></td>
                    
                  </tr>
                  <tr>
                    <td valign=top>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="8" valign=top>&nbsp;</td>
                    <td valign=top><strong>Creation Date </strong></td>
                    <td valign=top>:</td>
                    <td><%=doc_CREATIONDATE%></td>
                    <td valign=top>&nbsp;</td>
                  </tr>    <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td width="8" valign=top>&nbsp;</td>
                    <td valign=top><strong>Created By</strong></td>
                    <td valign=top>:</td>
                    <td><%=doc_CREATEDBY%></td>
                    <td valign=top>&nbsp;</td>
                  </tr>    <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td width="8" valign=top>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign=top>&nbsp;</td>
                  </tr>
                   <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td width="8" valign=top>&nbsp;</td>
                    <td valign=top><strong>Last Modified Date </strong></td>
                    <td valign=top>:</td>
                    <td><%=doc_MODIFIEDDATE%></td>
                    <td valign=top>&nbsp;</td>
                  </tr>   <tr>
                    <td height="3" colspan="5">
					</td>
                    </tr>
                  <tr>
                    <td width="8" valign=top>&nbsp;</td>
                    <td valign=top><strong>Last Modified </strong><strong>By</strong></td>
                    <td valign=top>:</td>
                    <td><%=doc_MODIFIEDDBY%></td>
                    <td valign=top>&nbsp;</td>
                  </tr>                </table>
				</div>
			  <div name="div_amend" id="div_amend" style="display:none">
                <table width="437" border="0" cellspacing="1" cellpadding="1" class="RSLBLack">
                  <tr>
                    <td colspan="4" nowrap><table  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="1" bgcolor="#669900"></td>
                        <td width="448" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="5">&nbsp;</td>
                              <td><span class="style1"> Amend Details (back to details <img src="/BoardMembers/images/im_up.gif" width="11" height="11" onClick="ShowHideDivs(2)" style="cursor:hand">) </span></td>
                            </tr>
                        </table></td>
                        <td width="1" bgcolor="#669900"></td>
                      </tr>
                      <tr>
                        <td width="1" height="1"></td>
                        <td width="220" height="1" bgcolor="#669900"></td>
                        <td width="1" height="1"></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td nowrap>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width="339">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="68" nowrap>Doc Title </td>
                    <td width="16">:&nbsp;</td>
                    <td><input name="txt_DOCNAME" type="text"  class="textbox200" style='width:250px' value="<%=doc_DOCNAME%>">
                    </td>
                    <td width="39"><image src="/js/FVS.gif" name="img_DOCNAME" width="15px" height="15px" border="0"></td>
                  </tr>
                  <tr>
                    <td height="3" colspan="4">
					</td>
                    </tr>
                  <tr>
                    <td>Expiry Date </td>
                    <td>:&nbsp;</td>
                    <td><input name="txt_DATEEXPIRES" type="text" maxlength="10" class="textbox200" style='width:250px' value="<%=doc_DATEEXPIRES%>">
                    </td>
                    <td><image src="/js/FVS.gif" name="img_DATEEXPIRES" width="15px" height="15px" border="0"></td>
                  </tr>
				                    <tr>
                    <td height="3" colspan="4">
					</td>
                    </tr>
                  <tr>
                    <td > Doc Path </td>
                    <td>:&nbsp;</td>
                    <td><input type="file" size=34 name="rslfile" class="RSLButton" style='background-color:white;color:black'>
                    </td>
                    <td><image src="/js/FVS.gif" name="img_DOCUMENTFILE" width="15px" height="15px" border="0"></td>
                  </tr>
				                    <tr>
                    <td height="3" colspan="4">
					</td>
                    </tr>
                  <tr>
                    <td valign=top>Type</td>
                    <td valign=top>:</td>
                    <td><select name="sel_TYPE"  id="sel_TYPE"  class="textbox100">
                        <%=strDocType%>
                      </select>
                        <image src="/js/FVS.gif" name="img_TYPE" width="15px" height="15px" border="0"> </td>
                    <td valign=top>&nbsp;</td>
                  </tr>
                                   <tr>
                    <td height="3" colspan="4">
					</td>
                    </tr>
                    
                                     <tr>
                  <td valign=top>Mandatory</td>
                  <td valign=top>:</td>
                  <td>
                      <input id="chkMandatory" <% = str_MAND %> name="chkMandatory" value="1" type="checkbox" />
                      <image src="/js/FVS.gif" name="img_TYPE" width="15px" height="15px" border="0"> </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                
                 <tr>
                  <td height="3" colspan="4"></td>
                </tr>
                
                  <tr>
                  <td valign=top>Keywords</td>
                  <td valign=top>:</td>
                  <td>
                      <textarea id="txtKeywords" name="txtKeywords" cols="20" rows="2"><% = doc_KEYWORDS %></textarea>
                     
                      <image src="/js/FVS.gif" name="img_TYPE" width="15px" height="15px" border="0"> </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                
                
                <tr>
                  <td height="3" colspan="4"></td>
                </tr>   
                    
                    
                    
                  <tr>
                    <td valign=top><strong>Access</strong></td>
                    <td colspan="3" valign=top><image src="/js/FVS.gif" name="img_DOCFOR" width="15px" height="15px" border="0"></td>
                    </tr> 
                  <tr>
                    <td colspan="4" valign=top><table width="432" border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#DFEACA">
                          <td height="5" colspan="3">
						  </td>
                          </tr>
                        <tr bgcolor="#DFEACA">
                          <td>&nbsp;</td>
                          <td> Select all teams you wish to share the documents with ...</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr bgcolor="#DFEACA">
                          <td width="10">&nbsp;</td>
                          <td><%=TeamStr%> </td>
                          <td width="10">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#DFEACA">
                          <td height="5" colspan="3">
						  </td>
                          </tr>
                      </table></td>
                    </tr>
                  <tr>
                    <td colspan="4" valign=top><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="17%"><input type="button" value="Select All" onClick="SelectAll(true)" class="RSLButton" style='font-size:9px'></td>
                        <td width="22%"><input type="button" value="Deselect All" onClick="SelectAll(false)" class="RSLButton" style='font-size:9px'></td>
                        <td width="14%"><input name="hid_FOLDERPATH" type="hidden" value="<%=theFolderPath%>"/>
                          <input name="hid_DATEMODIFIED" type="hidden" value="<%rw date & " " & time%>"/>
                          <input name="hid_DOCFOR" type="hidden" value=""/>
                          <input name="hid_DOCID" type="hidden" value="<%=rqDocumentID%>"/>
                          <input name="hid_USERID" type="hidden" value="<%=Session("UserID")%>"/>
                          <input type="hidden" name="MM_insert" value="true"/>
                          <input type="hidden" name="MM_recordId" value="<%= rqDocumentID %>"/>
                          </td>
                          <td width="47%" align="right"><input type="button" name="Submit" value="Upload Document" onClick="BtnSubmit()" class="RSLButton"></td>
                        </tr>
                    </table></td>
                    </tr>
                </table>
			  </div></td>
            </tr>
          </table>
		</td>
	</tr>
  </table>
  <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</div>
  <!--Start of hidden fields for document upload tool-->
  <input type="hidden" name="hid_Action" />
  <input type="hidden" name="keycriteria" value="" />
  <input type="hidden" name="updatefields" value="" />
  <input type="hidden" name="filefield" value="DOCUMENTFILE" />
  <input type="hidden" name="tablename" value="DOC_DOCUMENT" />
  <input type="hidden" name="connectionstring" value="connRSL" />
 <!--End of hidden fields for document upload tool-->
</form>
<iframe src="/secureframe.asp" name="ServerFrame" style="display:none"></iframe>
</body>
</html>
