Imports System.Data.SqlClient

Partial Class News_DocumentManager_serverside_DocumentManager
    Inherits MSDN.SessionPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID_193 As String = ASPSession("UserID")
        If (UserID_193 = "") Then
            Response.Redirect("/login/login.aspx?TIMEDOUT=1")
            'Response.Redirect "/Default.asp?TIMEDOUT=1"
        End If


        If Not Page.IsPostBack Then

        End If
    End Sub

    Public Function DocType(ByVal strFileName As Object) As String
        If IsDBNull(strFileName) Then
            Return "N/A"
        Else
            Return filehelper.GetDocImage(strFileName)
        End If

    End Function

    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dddd, dd MMMM yyyy")
        End If

    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Try
        If validateFilters() Then
            gvwDoc.DataSource = odsSearchDocs
            gvwDoc.DataBind()
        End If

        'Catch ex As Exception
        'ErrorHelper.LogError(ex, True)
        'End Try

    End Sub

    Private Function validateFilters() As Boolean
        ' validate date filter fields        

        Dim errors As New ErrorCollection
        If Not String.IsNullOrEmpty(txtCreationDateFrom.Text) Then
            If Not valhelper.validateDate(txtCreationDateFrom.Text) Then
                errors.Add("Enter a valid from creation date")
            End If
        End If

        If Not String.IsNullOrEmpty(txtCreationDateTo.Text) Then
            If Not valhelper.validateDate(txtCreationDateTo.Text) Then
                errors.Add("Enter a valid to creation date")
            End If
        End If

        If Not String.IsNullOrEmpty(txtExpiryDateFrom.Text) Then
            If Not valhelper.validateDate(txtExpiryDateFrom.Text) Then
                errors.Add("Enter a valid from expiry date")
            End If
        End If

        If Not String.IsNullOrEmpty(txtExpiryDateTo.Text) Then
            If Not valhelper.validateDate(txtExpiryDateTo.Text) Then
                errors.Add("Enter a valid to expiry date")
            End If
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            lblErrors.Text = strError
            lblErrors.ForeColor = Drawing.Color.Red
            Return False
        Else
            lblErrors.Text = ""
        End If

        Return True
    End Function

    Protected Sub gvwDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwDoc.PageIndexChanging
        gvwDoc.PageIndex = e.NewPageIndex
        BindDocs()
    End Sub

    Private Sub BindDocs()
        gvwDoc.DataSource = odsSearchDocs
        gvwDoc.DataBind()
    End Sub

    Protected Sub odsSearchDocs_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsSearchDocs.Selecting
        ' stuff extra parameters into sproc
        e.InputParameters("EMPID") = Integer.Parse(ASPSession("USERID"))
        e.InputParameters("TEAMCODE") = ASPSession("TeamCode")
    End Sub
    Protected Sub SearchLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton
        lnk = CType(sender, LinkButton)

        If lnk IsNot Nothing Then

            Dim taDocs As New dalDocumentManagerTableAdapters.DOC_DOCUMENTTableAdapter
            Dim docTab As dalDocumentManager.DOC_DOCUMENTDataTable = taDocs.GetData(Integer.Parse(lnk.CommandArgument))
            Dim docRow As dalDocumentManager.DOC_DOCUMENTRow
            If docTab.Rows.Count > 0 Then
                docRow = docTab(0)
            Else

            End If

            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & docRow.DOCFILE)
            Response.AddHeader("Content-Length", docRow.DOCUMENTFILE.Length)
            Response.ContentType = "application/doc"
            Response.BinaryWrite(docRow.DOCUMENTFILE)
            Response.End()
            Response.Flush()

        End If

    End Sub
End Class
