<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
bypasssecurityaccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->

<%

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Start of declare and request vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	 
	Dim rqFolderID, rqAction, CREATEDBY,CREATIONDATE
	Dim TitleMessage,GeneralName, setFolderName, RELOAD ,FolderPath
	Dim ShowDeleteFolder
	Dim rqMoveFolder, rqMoveDocID, rqMoveFolderId
	Dim disableFolder 
	disableFolder = "hello"
	
	rqFolderID 			= Request("FLDID")
	rqAction 			= Request("Action")
	rqAction_Process 	= Request("Action_process")
	rqAction_Process2 	= Request("Action_process2")
	rqMoveDocID         = Request("DOCID")
	rqMoveFolder        = Request("MOVETO")
	rqMoveFolderId      = Request("MOVEFLDID")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End of declare and request vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	disableFolder = ""
	
	' When the page is requested for the first time to amend/add/delete a folder
	OPENDB()
	If rqAction <> "" then 
	
		Select Case rqAction
			case "CREATE" 
				TitleMessage 	= "<b>Create New Folder</b>"
				GeneralName 	= "Create New"
			
			case "UPDATE"
				TitleMessage 	= "<b>Amend Folder</b>"
				GeneralName 	= "Amend"						
				GetFolderDetails()
								
		    case "DELETE"			    
				TitleMessage = "<b>Delete Folder</b> - (Please note that all files have to be removed under this folder first)"
				GeneralName = "Delete"
	    		GetFolderDetails()
	    		
	    	case "MOVETOFOLDER"
	    	    TitleMessage = "<b>Move To Folder</b>"
	    	    GeneralName = "Move"
	    	    setFolderName = rqMoveFolder
	    	    disableFolder = "disabled"
	    	  	    	    
			case else
				TitleMessage = "There is a small error on this page please contact the RSLsupport team. Thankyou"
				GeneralName = "HIDEFORM"
				
		End Select
	
	' When the page is submitted from its self to perform amend/add/delete 
	ElseIf rqAction_Process <> "" then

        if rqAction_Process2 = "Delete" then
            DeleteFolder()
        else        
		    Select Case rqAction_Process
			    case "Create New"
				   CreateFolder()
			    case "Amend"
				    AmendFolder()				    
				case "Move"		  				    
				    MoveFolder()
				
			    case else
				    TitleMessage = "There is a small error on this page please contact the RSLsupport team. Thankyou"
				    GeneralName = "Error"
		    End Select
		 end if
			
	' Catch the error
	Else
		TitleMessage = "There is a small error on this page please contact the RSLsupport team. Thankyou"
	End If
	CLOSEDB()
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Start of Functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	Function GetFolderDetails()
	
	    ' test if ok to show folder
		ShowDeleteFolder = IsFolderEmpty()
		
		
		' Get The folder Name of the folder you wish to amend
		SQL =   "SELECT F.* , F.DATESTAMP , E.FIRSTNAME + ' ' + E.LASTNAME AS CREATEDBYWHO " &_
				"FROM DOC_FOLDER F " &_
				"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = F.CREATEDBY " &_
				"WHERE F.FOLDERID = " & rqFolderID
		
		Call OpenRs(rsFolder, SQL)
			
			createdby = rsFolder("CREATEDBYWHO")
			CREATIONDATE =  rsFolder("DATESTAMP")
			setFolderName = rsFolder("FOLDERNAME")
		Call CloseRs(rsFolder)
	
	End Function
	
	Function CreateFolder()
					
			' Create folder in the database 
			SQL = "INSERT INTO DOC_FOLDER (FOLDERNAME,PARENTFOLDER,CREATEDBY) VALUES ('" & rTrim(lTrim(Request.Form("txt_FolderName"))) & "', " & Request.Form("hid_FOLDERID") & ", " & Request.Form("hid_USERID") & ")"
			Conn.Execute (SQL)
			TitleMessage = "The folder has been created sucessfully"
			RELOAD = "TRUE"
			GeneralName = "HIDEFORM"
		
	End Function

	
	Function AmendFolder()
		
		
		If NOT  Request.Form("txt_FolderName") =  Request.Form("hid_FolderName") then
		
					
			' Get The folder Name of the folder you wish to amend
			SQL = "UPDATE DOC_FOLDER SET FOLDERNAME = '" & Request.Form("txt_FolderName") & "' WHERE FOLDERID = " & Request.Form("hid_FolderID")
			Conn.Execute (SQL)
	
			TitleMessage = "The folder has been Amended sucessfully"
			GeneralName = "HIDEFORM"
			RELOAD = "TRUE"
		else
			TitleMessage 	= "<BR><b>You have not changed the name of this file.</b>"
			setFolderName 	= Request.Form("txt_FolderName")
	 		setFolderName	= Request.Form("hid_FolderName")
	  		rqFolderID		= Request.Form("hid_FolderID")
	  		GeneralName		= Request.Form("Action_Process")
		End If
	End Function

    function MoveFolder()
                           
        Dim docID
        Dim docFilename, docFullFilename, docDestinationPath
        Dim docFolderID
	    docID = Request("hid_MoveDocID")

        ' get the filename
	    SQL = "SELECT * FROM DOC_DOCUMENT WHERE DOCUMENTID = " & docID
	    OpenDB()

	    Call OpenRs(rsDoc, SQL)
	    if not rsDoc.eof then

			docFilename = rsDoc("DOCFILE")
			docFolderID = rsDoc("FOLDERID")
			            
            if cstr(docFolderID) <> Request.Form("hid_MoveFolderID") and docFolderID <> "" then
        
                ' move file within the database			        
		        SQL = "UPDATE DOC_DOCUMENT SET FOLDERID = "  & Request.Form("hid_MoveFolderID") & " WHERE DOCUMENTID = " & docID		        
		        conn.execute(SQL)		        
    		    		    
		        TitleMessage = "The document has been moved sucessfully"
		        GeneralName = "HIDEFORM"
		        RELOAD = "TRUE"
		    else
		        if docFolderID = "" then
		    	    TitleMessage 	= "<BR><b>Please select a folder name to move the document to.</b>"
		    	else		    	    
		    	    TitleMessage 	= "<BR><b>Please select a different folder name than the source folder.</b>"
		    	end if
		    	GeneralName = "HIDEFORM"
		        RELOAD = "TRUE"		        
		    end if
        end if   

    end function
	
	Function DeleteFolder()
		
		rqFolderID = Request.Form("hid_FolderID")
	
		' delete folder from DB
		SQL = "DELETE FROM DOC_FOLDER WHERE FOLDERID = " & rqFolderID
		Conn.Execute (SQL)
		
		TitleMessage = "The folder has been Deleted sucessfully"
		GeneralName = "HIDEFORM"
		RELOAD = "TRUE"
		Action_Process2 = ""
	End Function
	
			
	Function IsFolderEmpty()
	    
	    Dim FilesCount,SubFolderCount	    
	    
	    FilesCount=0
	    SubFolderCount=0
	    FolderId=0
	    
	    FolderId = rqFolderID
	    
		If(FolderId<>0) then	
		
		    'Get the subfolder count 
		    SQL = "SELECT COUNT(FOLDERID) AS FOLDERCOUNT FROM DOC_FOLDER WHERE PARENTFOLDER = " & FolderID
		    
		    Call OpenRs(rsSubFolder, SQL)
		    If not rsSubFolder.eof then
			    SubFolderCount = rsSubFolder("FOLDERCOUNT")
		    end if
		    Call CloseRs(rsSubFolder)
		    
		    'Get the files count in the folder
		    SQL = "SELECT COUNT(DOCUMENTID) AS FILESCOUNT FROM DOC_DOCUMENT WHERE FOLDERID= " & FolderID
		   
		    Call OpenRs(rsfiles, SQL)
		    if not rsfiles.eof then
			    FilesCount = rsfiles("FILESCOUNT")
		    end if
		    Call CloseRs(rsfiles)
		
		End if
		
		If(FilesCount<>0 or SubFolderCount<>0) Then
		    IsFolderEmpty = false
		Else    
		    IsFolderEmpty = true
		End if
	    
	End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style1 {	color: #003399;
	font-weight: bold;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">

	function SubmitForm(){

	if (RSLFORM.txt_FolderName.value == ""){
		alert("Please enter a Folder Name")
	
		return false
		}
	RSLFORM.submit()
	
	}
	
	function ConfirmDeleteFolder()
	{        
	    if ( confirm("Delete this folder?"))
	    {
	        RSLFORM.Action_Process2.value = "Delete"	    
	        RSLFORM.submit()
	    }
	}
	
	function CancelMove()
	{   	            
        window.location.href = "DOCUMENT_AMEND.asp?DOCID=" + RSLFORM.hid_MoveDocID.value + "&ACTION=AMEND";        
	}	
	
	function ConfirmMoveToFolder()
	{        
	    //if ( RSLFORM.hid_MoveFolderID.value == "" || RSLFORM.txt_FolderName.value == "" )
	    if ( RSLFORM.txt_FolderName.value == "" )
	    {
	        alert("Please select a folder name.");
	    }
	    else
	    {
	        // don't prompt for now...
	        //if ( confirm("Move document to this folder?") )
	        //{	        
            RSLFORM.Action_Process2.value = "Move"	    
            RSLFORM.submit()
	        //}
	    }	    
	}
	
	<% if RELOAD = "TRUE" then %>
	
		parent.theDocBar.ReloadMe()

	<% End If %>

</script>
</head>
<body >
<div id="div_RIGHTSIDE" name="div_RIGHTSIDE" style="display:block ">
  <table width="449" border="0" cellspacing="0" cellpadding="0">
  <form name="RSLFORM" method="POST" ACTION="Folder.asp">
    <tr>
      <td width="16">&nbsp;</td>
      <td width="122">&nbsp;</td>
      <td width="262">&nbsp;</td>
      <td width="27">&nbsp;</td>
      <td width="22">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="4"><table  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1" height="1"></td>
            <td width="220" height="1" bgcolor="#669900"></td>
            <td width="1" height="1"></td>
          </tr>
          <tr>
            <td width="1" bgcolor="#669900"></td>
            <td width="448" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5">&nbsp;</td>
                  <td><span class="style1"> <%=TitleMessage%> </span></td>
                </tr>
            </table></td>
            <td width="1" bgcolor="#669900"></td>
          </tr>
          <tr>
            <td width="1" height="1"></td>
            <td width="220" height="1" bgcolor="#669900"></td>
            <td width="1" height="1"></td>
          </tr>
        </table></td>
      </tr>
	<%If Not GeneralName = "HIDEFORM" then %>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Folder Name </td>
      <td>
      <input type="text" name="txt_FolderName" class="textbox200" value="<%=setFolderName%>" "<%=disableFolder%>">      
      </td>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>
		 <input name="hid_FolderName" type="hidden" value="<%=setFolderName%>"> 
	  	<input name="hid_FolderID" type="hidden" id="hid_FolderID" value="<%=rqFolderID%>">
		<input name="hid_USERID" type="hidden" id="hid_USERID" value="<%=session("userid")%>">
		<input name="hid_MoveDocID" type="hidden" id="hid_MoveDocID" value="<%=rqMoveDocID%>">
		<input name="hid_MoveFolderID" type="hidden" id="hid_MoveFolderID" value="<%=rqMoveFolderID%>">
	  	<input name="Action_Process" type="hidden" id="Action_Process" value="<%=GeneralName%>">
	  	<input name="Action_Process2" type="hidden" id="Action_Process2" value="">
	  </td>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>
      <%if rqAction <> "MOVETOFOLDER" then %>
      <input type="button" name="Submit" value="<%=GeneralName%> Folder" class="RSLButton" onClick="SubmitForm()">
      <% else %>
      <input type="button" name="Submit" value="Cancel" class="RSLButton" onClick="CancelMove()">
      <% End If %>
            
      <%If ShowDeleteFolder then %><input type="button" name="Submit" value="Delete Folder" class="RSLButton" onClick="ConfirmDeleteFolder()">
      <% End If %> 
      
      <%If rqAction = "MOVETOFOLDER" then %>
      <input type="button" name="Submit" value="Confirm Move" class="RSLButton" onClick="ConfirmMoveToFolder()">
      <% End If %> 
      </td>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td></td>
      <td>&nbsp;</td>
    </tr>
  
  	<% if NOT rqAction = "CREATE" AND NOT rqAction = "MOVETOFOLDER" then %>
    <tr>
      <td>&nbsp;</td>
      <td colspan="4"><table  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="1" bgcolor="#669900"></td>
          <td width="448" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5">&nbsp;</td>
                <td><span class="style1"> Folder Details</span></td>
              </tr>
          </table></td>
          <td width="1" bgcolor="#669900"></td>
        </tr>
        <tr>
          <td width="1" height="1"></td>
          <td width="220" height="1" bgcolor="#669900"></td>
          <td width="1" height="1"></td>
        </tr>
      </table></td>
      </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Created By : </strong></td>
      <td><%=createdby%></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Creation Date : </strong></td>
      <td><%=creationdate%></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<% End If 
	 End If %>
	</FORM>
  </table>
</div>
</body>
</html>
