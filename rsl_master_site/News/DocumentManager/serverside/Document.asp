<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
bypasssecurityaccess = true
server.ScriptTimeout = 1000000
 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Declare and request vars
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	Dim rqFolderID, rqAction,rqMM_insert
	Dim TitleMessage,GeneralName, FolderName, RELOAD
	Dim NewDOCUMENTID
	Dim TeamStr,strDocType

	rqFolderID 			= Request("FLDID")
	rqAction 			= Request("Action")

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' End of decalare and request vars
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Building page first load
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Gets the list of Teams
Sub BuildTeamCheckBox()

    'Get the Team Code
    SQL = "SELECT TC.TEAMCODE, T.TEAMNAME FROM E_TEAM T " &_
		  "	INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID " &_
	      " WHERE T.ACTIVE = 1 ORDER BY T.TEAMNAME"

    Call OpenRS(rsTeams, SQL)

    'Start of table creation
    TeamStr = TeamStr & "<table width=""400"">"

    If Not rsTeams.EOF Then
        LinkCount = 0

        'Do until the end of all records
        Do Until rsTeams.EOF

          'change 5 to any number of columns you like
          If LinkCount Mod 2 = 0 Then
             If LinkCount <> 0 Then TeamStr = TeamStr & "</tr>"
             TeamStr = TeamStr &"<tr><td>" & rsTeams("TEAMNAME") & "&nbsp;</td><td>:&nbsp;</td><td><input type=""checkbox"" id=""DocType"" name=""DocType"" value=""" & rsTeams("TEAMCODE") & """ /></td>"
          Else
             TeamStr = TeamStr &"<td>" & rsTeams("TEAMNAME") & "&nbsp;</td><td>:&nbsp;</td><td><input type=""checkbox"" id=""DocType"" name=""DocType"" value=""" & rsTeams("TEAMCODE") & """ /></td>"
          End If

          LinkCount = LinkCount + 1

          'loop till end of records
          rsTeams.MoveNext
        Loop

            TeamStr = TeamStr & "</tr>"
    Else
     'There are no records
      TeamStr = TeamStr & "<tr><td>Sorry, no records were found!</td></tr>"
    End If

    'End of table creation
    TeamStr = TeamStr & "</table>"
End Sub

'Get the document type for drop down
Sub BuildDocTypeSelection()

    SQL = "SELECT DOCUMENTTYPEID, DESCRIPTION FROM G_DOCUMENTTYPE ORDER BY DESCRIPTION ASC"
    Call OpenRs(rsDocType, SQL)
    strDocType = ""
    while not rsDocType.EOF
	    strDocType = strDocType & "<option value='" & rsDocType("DOCUMENTTYPEID") & "'>" & rsDocType("DESCRIPTION") & "</option>"
	    rsDocType.moveNext
    wend
    Call CloseRs(rsDocType)

End Sub

'Call the functions when the page loads
Call OpenDB()
    BuildTeamCheckBox()
    BuildDocTypeSelection()
Call CloseDB()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' End of building page first load
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>
<html xmlns="http://www.w3.org/1999/xhtml"  >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=10" />
        <title>Document Insert</title>
        <link href="/css/RSL.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">
            body 
            {
	            margin-left: 0px;
	            margin-top: 0px;
	            scrollbar-3dlight-color: #CCCCCC; 
	            scrollbar-arrow-color: #839FAD; 
	            scrollbar-base-color: #F7F8F8;
             }
            .style1 
             {	
                color: #003399;
	            font-weight: bold;
             }
          </style>
          <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
          <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
          <script type="text/javascript" language="JAVASCRIPT" src="/js/FormValidation.js"></script>
          <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
          <script type="text/javascript" language="JavaScript">
                function getFileExtension(filePath) 
                { //v1.0
                    fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/')+1,filePath.length) : filePath.substring(filePath.lastIndexOf('\\')+1,filePath.length));
                    return fileName.substring(fileName.lastIndexOf('.')+1,fileName.length);
                }
                
                function checkFileUpload(form,extensions) 
                { //v1.0
                    var MAX_FILE_UPLOAD_SIZE = 20971520;  // MAX SIZE 20 MB LIMIT
                    var fileSize = document.getElementById('rslfile').files[0].size;
                    if (fileSize > MAX_FILE_UPLOAD_SIZE) {
                        alert('File size should be less than 20MB');
                        return false;
                    }


                    document.MM_returnValue = true;
                    if (extensions && extensions != '') 
                    {
                        for (var i = 0; i<form.elements.length; i++) 
                        {
	                      field = form.elements[i];
	                      if (field.type.toUpperCase() != 'FILE') continue;
                          if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) 
                          {
                            ManualError('img_DOCUMENTFILE','This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.',0);
       	                    document.MM_returnValue = false;field.focus();
		                    return false;
		                    break;
                            } 
                         } 
                     }
	                 ManualError('img_DOCUMENTFILE','',3);
	                 return true;
                 }
            </script>
            
            <script type="text/javascript" language="JavaScript">
            <!--
                var FormFields = new Array()
                FormFields[0] = "txt_DOCNAME|Doc Name|TEXT|Y"						
                FormFields[1] = "txt_DATEEXPIRES|Expiry Date|DATE|Y"						
                FormFields[2] = "hid_DOCFOR|Share Doc|TEXT|Y"

                function BtnSubmit()
                {
	            	temp = document.getElementsByName("DocType")
	            	strNews = ""
	            	strTeamCodes=""
	                if (temp.length)
	                {
		                for (i=0; i<temp.length; i++)
		                {
		                    if (temp[i].checked == true) 
                                {
		                            strNews += "," + temp[i].value
		                            strTeamCodes += "'" + temp[i].value + "',"
		                        }
			            }
		            }
	                if (strNews != "") strNews += ","

	                document.getElementById("hid_DOCFOR").value = strNews
	                document.getElementById("hid_TEAMCODES").value = strTeamCodes.substr(0, strTeamCodes.length - 1)
	                result = checkFileUpload(UserNews,'doc;txt;xls;pdf;jpeg;jpg;gif;bmp;ppt;docx;xlsx;pptx');
	                
	                if (!checkForm()) return false;	
	                if (!result) return false;
	                go();
	            }

                function go()
                {
                    document.getElementById("hid_Action").value = "INSERT";
                    document.UserNews.target = "ServerFrame";
                    document.UserNews.action = "Document_srv.asp";
                    document.UserNews.submit();	
	            }

                function SelectAll(newStatus) {
                    temp = document.getElementsByName("DocType")
	                if (temp.length)
	                {
		                for (i=0; i<temp.length; i++)
		                {
			                temp[i].checked = newStatus
			            }	
		            }
                }
	          
		        function Add_Document(docid)
	            {
	                document.UserNews.encoding = "multipart/form-data";
	                document.UserNews.target = "ServerFrame";
	                document.UserNews.keycriteria.value = "DOCUMENTID=" + docid;
	                document.UserNews.action = "/DBFileUpload/uploader.aspx";
	                document.UserNews.submit();
                }    
            //-->
            </script>
    </head>
    <body>
        <form name="UserNews" id="UserNews" method="post" action="">
        <div id="div_RIGHTSIDE" style="display:block ">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
	           <tr> 
	              <td>
	                  
	                  <!--Start of Message after document insert-->
                       <p id="pMessage" style="display:none;margin-left:10px;padding:5px 5px 5px 5px;border:solid 1px #669900;width:20%;" >Your Document has been added.</p>
	                  <!--End of Message after document insert-->
	                    
		               <!--Start of form creation for insert doc-->          
                       <table id="tblForm" width="449" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td>
                                    <table width="437" border="0" cellspacing="1" cellpadding="1" class="RSLBLack">
                                    <tr>
                                        <td colspan="4" nowrap="nowrap">
                                            <table  border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="1" height="1"></td>
                                                    <td width="220" height="1" bgcolor="#669900"></td>
                                                    <td width="1" height="1"></td>
                                                 </tr>
                                                 <tr>
                                                     <td width="1" bgcolor="#669900"></td>
                                                     <td width="448" height="23" bgcolor="#FFFFFF">
                                                        <table width="285" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="5">&nbsp;</td>
                                                                <td><span class="style1">New Document</span> </td>
                                                            </tr>
                                                        </table>
                                                     </td>
                                                     <td width="1" bgcolor="#669900"></td>
                                                  </tr>
                                                  <tr>
                                                        <td width="1" height="1"></td>
                                                        <td width="220" height="1" bgcolor="#669900"></td>
                                                        <td width="1" height="1"></td>
                                                  </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" nowrap="nowrap">To add a document, please enter or select values for the details <br/> requested below.</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td width="339">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="68" nowrap="nowrap">Doc Title </td>
                                        <td width="16">:&nbsp;</td>
                                        <td><input name="txt_DOCNAME" id="txt_DOCNAME" type="text" class="textbox200" style='width:250px' value="<%=doc_DOCNAME%>" /></td>
                                        <td width="39"><img src="/js/FVS.gif" name="img_DOCNAME" id="img_DOCNAME" width="15px" height="15px" border="0" alt="" /></td>
                                    </tr>
                                    <tr>
                                        <td height="3" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td>Expiry Date </td>
                                        <td>:&nbsp;</td>
                                        <td><input name="txt_DATEEXPIRES" id="txt_DATEEXPIRES" type="text" maxlength="10" class="textbox200" style='width:250px' value="<%=doc_DATEEXPIRES%>" /></td>
                                        <td><img src="/js/FVS.gif" name="img_DATEEXPIRES" id="img_DATEEXPIRES" width="15px" height="15px" border="0" alt="" /></td>
                                    </tr>
                                    <tr>
                                        <td height="3" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td > Doc Path </td>
                                        <td>:&nbsp;</td>
                                        <td><input type="file" name="rslfile" id="rslfile" class="RSLButton" style='background-color:white;color:black' /></td>
                                        <td><img src="/js/FVS.gif" name="img_DOCUMENTFILE" id="img_DOCUMENTFILE" width="15px" height="15px" border="0" alt="" /></td>
                                    </tr>
                                    <tr>
                                         <td height="3" colspan="4"></td>
                                    </tr>   
                                    <tr>
                                        <td valign="top">Type</td>
                                        <td valign="top">:</td>
                                        <td>
                                            <select name="sel_TYPE"  id="sel_TYPE"  class="textbox100">
                                              <%=strDocType%>
                                            </select>
                                            <img src="/js/FVS.gif" name="img_TYPE" id="img_TYPE" width="15px" height="15px" border="0" alt="" /> 
                                        </td>
                                        <td valign="top">&nbsp;</td>
                                     </tr>
                                     <tr>
                                        <td height="3" colspan="4"></td>
                                     </tr>
                                     <tr>
                                         <td valign="top">Mandatory</td>
                                         <td valign="top">:</td>
                                        <td>   
                                            <input id="chkMandatory" name="chkMandatory" value="1" type="checkbox" />
                                         </td>
                                        <td valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="3" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">Keywords</td>
                                        <td valign="top">:</td>
                                        <td>       
                                            <textarea id="txtKeywords" name="txtKeywords" cols="20" rows="2"></textarea>
                                            <img src="/js/FVS.gif" name="img_TYPE" id="img_TYPE" width="15px" height="15px" border="0" alt="" /> 
                                         </td>
                                         <td valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td height="3" colspan="4"></td>
                                    </tr>              
                                    <tr>
                                      <td valign="top"><strong>Access</strong></td>
                                      <td colspan="3" valign="top"><img src="/js/FVS.gif" name="img_DOCFOR" id="img_DOCFOR" width="15px" height="15px" border="0" alt="" /></td>
                                    </tr>
                                    <tr>
                                          <td colspan="4" valign="top">
                                             <table width="432" border="0" cellspacing="0" cellpadding="0">
                                                <tr bgcolor="#DFEACA">
                                                    <td height="5" colspan="3"></td>
                                                </tr>
                                                <tr bgcolor="#DFEACA">
                                                    <td>&nbsp;</td>
                                                    <td> Select all teams you wish to share the documents with ...</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr bgcolor="#DFEACA">
                                                    <td width="10">&nbsp;</td>
                                                    <td><%=TeamStr%> </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                                <tr bgcolor="#DFEACA">
                                                    <td height="5" colspan="3"></td>
                                                </tr>
                                            </table>
                                         </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" valign="top">
                                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="18%"><input type="button" value="Select All" onclick="SelectAll(true)" class="RSLButton" style='font-size:9px' /></td>
                                                    <td width="21%"><input type="button" value="Deselect All" onclick="SelectAll(false)" class="RSLButton" style='font-size:9px' /></td>
                                                    <td width="14%">
                                                         <input name="hid_FOLDERPATH" id="hid_FOLDERPATH" type="hidden" value="<%=theFolderPath%>" />
                                                         <input name="hid_DOCFOR" id="hid_DOCFOR" type="hidden" value="" />
                                                         <input name="hid_TEAMCODES" id="hid_TEAMCODES" type="hidden" value="" />
                                                         <input name="hid_FOLDERID" id="hid_FOLDERID" type="hidden" value="<%=rqFolderID%>" />
                                                         <input name="hid_USERID" id="hid_USERID" type="hidden" value="<%=Session("UserID")%>" />
                                                    </td>
                                                    <td width="47%" align="right">
                                                        <input type="button" name="Submit" id="Submit" value="Upload Document" onclick="BtnSubmit()" class="RSLButton" />
                                                    </td>
                                                </tr>
                                            </table>
                                         </td>
                                    </tr>
                                </table>
                             </td>
                           </tr>
                      </table>
                      <!--End of form creation for insert doc-->
                      
                      <!--Start of hidden fields for document upload tool-->
                      <input type="hidden" name="hid_Action" id="hid_Action" />
                      <input type="hidden" name="keycriteria" id="keycriteria" value="" />
                      <input type="hidden" name="updatefields" id="updatefields" value="" />
                      <input type="hidden" name="filefield" id="filefield" value="DOCUMENTFILE" />
                      <input type="hidden" name="filename" id="filename" value="DOCFILE" />
                      <input type="hidden" name="tablename" id="tablename" value="DOC_DOCUMENT" />
                      <input type="hidden" name="connectionstring" id="connectionstring" value="connRSL" />
                     <!--End of hidden fields for document upload tool-->
		             
	               </td>
	            </tr>
            </table>
            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
        </div>
        </form>
        <iframe src="/secureframe.asp" name="ServerFrame" id="ServerFrame" height="400" width="400" style="display:none"></iframe>
    </body>
</html>
