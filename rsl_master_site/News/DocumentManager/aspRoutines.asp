<%
'This function loads XML data into an instance
'of an MSXML object.  objDocument is an empty variable that will return
'a populated XML document object.  bLoadSubItems is a Boolean that indicates 
'whether we want our "Load on Demand" folder loaded with subitems. If 
'the data is loaded successfully, the function returns TRUE, otherwise 
'it returns FALSE.
Function fnLoadXMLData(byRef objDocument)
  Dim bResult
  bResult = True
  'Create instance of XML document object that we can manipulate
  Set objDocument = Server.CreateObject("MSXML2.FreeThreadedDOMDocument.3.0")
  If objDocument Is Nothing Then
    Response.Write "objDocument object not created<br>"
    bResult = False
  Else
    If Err Then
      Response.Write "XML Document Object Creation Error - <BR>"
      Response.write Err.Description & "<BR>"
      bResult = False
    Else
      On Error resume Next
      'Load up the XML document into the XML object
      objDocument.async = False
      bLoaded = objDocument.Load(Server.MapPath("menuitems.xml"))
      If err <> 0 Then
        Response.Write err.Description & "<BR>"
        bResult = False
        err = 0
      End If
    End If
  End If
  fnLoadXMLData = bResult
End Function

'This subroutine is the workhorse of our menu page.  It is responsible for 
'traversing the XML tree to display each menu item.  The routine calls itself
'recursively and generates an HTML page containing javascript that handles
'showing/hiding menu items.
'[Parameters]
'--------------------
'objNodes :	The XML object containing our menu data
'iElement :	Passed by reference, this value increments twice each time we add
'			a new menu item.  The first time it increments, the value is to identify the 
'			menu item.  The value is immediately incremented and this time is used to
'			identify the element that will be shown/hidden
'sLeftIndent :	Passed by reference, this string accumulates the <td> and <img> tags necessary
'				to display empty space and dotted lines to the left of the menu item as
'				the item gets indented in the list.
'sOpenFolders :	This string contains values that tell the subroutine
'					which folders should be displayed as "open" by default.
Sub DisplayNode(ByVal objNodes, ByRef iElement, ByRef sLeftIndent, ByRef sOpenFolders)
  On Error Resume Next
  Dim oNode, sAttrValue, sNodeType, sURL
  Dim NODE_ELEMENT
  dim sTempLeft, bHasChildren, bIsLast, bIsRoot, bShowOpen
  Dim strLoadOnDemand
  
  NODE_ELEMENT = 1
  iElement = iElement + 1
  For Each oNode In objNodes
    'Find out if current node has children
    bHasChildren = oNode.hasChildNodes
    'Find out if the current node is the last member
    'in the list or not
    If Not(oNode.nextSibling Is Nothing) then
      bIsLast = False
    Else
      bIsLast = True
    End If
    'Ignore NODE_TEXT node types
    If oNode.nodeType = NODE_ELEMENT Then
      sNodeType = oNode.nodeName
      sAttrValue = oNode.getAttribute("name")	'Get the display value of the current node
      sURL = Server.URLEncode(oNode.getAttribute("path"))
      'Find out if this is the root of the tree
      If (sNodeType = "root") Then
        bIsRoot = True
      Else
        bIsRoot = False
      End If
      If (sNodeType = "document") Then
%><table border=0 cellspacing=0 cellpadding=0><tr><%
        'Display the proper indentation formatting
        Response.write sLeftIndent
        'Now display the document node
%><td height="16"><img src=images/<%=fnChooseIcon(bIsLast, bIsRoot, sNodeType, bHasChildren, True)%> width=31 height=16 border=0></td><td><img src=images/pixel.gif width=2 height=1></td><td nowrap class=node><img src=images/pixel.gif width=2 height=1><a href="/navigation/content.asp?document=<%=sURL%>" target=basefrm onClick=objPreviousLink=fnSelectItem(this,objPreviousLink)><%=sAttrValue%></a><img src=images/pixel.gif width=2 height=1></td></tr></table><%
      'Otherwise this is a folder
      Else
        strLoadOnDemand = oNode.getAttribute("loadOnDemand")
        'We set the LoadOnDemand value for the History folder
        'because we want it to be populated only when clicked by the user
        If (strLoadOnDemand = "yes") Then
          sMode = "LoadOnDemand"
        Else
          sMode = ""
        End If
%><table border=0 cellspacing=0 cellpadding=0><tr><%
        'Check if we are building the tree for the first time
        If (Request.Form = "") Then
          If (sNodeType = "root") Then
            bShowOpen = True 'We want the root folder open by default
            sOpenFolders = sOpenFolders & "," & iElement
          Else
            bShowOpen = False
          End If
        Else
          'Read the hidden field to determine if the current folder
          'should be displayed as Open
          bShowOpen = fnGetFolderStatus(iElement, sOpenFolders)
        End If
        Response.write sLeftIndent  'Display the proper indentation formatting
        'Now display the folder
%><td height="16"><img onclick="doChangeTree(this, arClickedElementID, arAffectedMenuItemID);" class=LEVEL<%=iElement%> src=images/<%=fnChooseIcon(bIsLast, bIsRoot, sNodeType, bHasChildren, bShowOpen)%> id=<%=iElement%> width=31 height=16 border=0 name="<%=sMode%>"></td><td><img src=images/pixel.gif width=2 height=1></td><td nowrap class=node><img src=images/pixel.gif width=2 height=1><a href="/navigation/content.asp?folder=<%=sURL%>" target=basefrm onClick="objPreviousLink=fnSelectItem(this,objPreviousLink)"><%=sAttrValue%></a><img src=images/pixel.gif width=2 height=1></td></tr></table><%
        'Increment the element ID
        iElement = iElement + 1
        'After displaying the folder, let's see
        'if it contains any submenu items
        If bHasChildren Then
%><table border=0 cellspacing=0 cellpadding=0><tr class=LEVEL<%=iElement%> id=<%=iElement%> style=display:<%If bShowOpen=False Then%>none<%End If%>><td><%
          'First store the indentation code
          sTempLeft = sLeftIndent
          'We don't want to indent the first node on our tree
          'so only generate indent code if this not the root menu item
          If (iElement > 1) Then
            sLeftIndent = fnBuildLeftIndent(oNode, bIsLast, sLeftIndent)
          End If
          'Call this subroutine again to process the submenu item
          DisplayNode oNode.childNodes, iElement, sLeftIndent, sOpenFolders
          'We're popping the stack, so reset the value of sLeftIndent
          'to what it was before we went into the DisplayNode() subroutine above
sLeftIndent = sTempLeft%></td></tr></table><%
        End If
      End If
    End If
  Next
  'Display any error messages encountered while executing this subroutine
  If Err <> 0 Then
    Response.Write Err.description & "<br>"
  End If
End Sub

'This function returns the appropriate icon to be displayed in the menu.  It decides
'which icon to return based on the parameters that are passed in.
'[Parameters]
'--------------------
'bIsLast :		TRUE/FALSE - is this the last child in the current list?
'bIsRoot :		TRUE/FALSE - is this the root node of the tree?
'sNodeType :	String containing "document", "folder", or "root".
'bHasChildren :	TRUE/FALSE - specifies if the current item has any children
'bShowOpen :	TRUE/FALSE - specifies if we want the folder open or closed icon displayed
function fnChooseIcon(byval bIsLast, byval bIsRoot, byval sNodeType, byval bHasChildren, byval bShowOpen)
	dim sIcon
	
	sIcon = ""
	
	if (sNodeType = "document") then  
		if (bIsLast = false) then
			sIcon = "docjoin.gif"  'This is not the last document in list, so use JOIN graphic
		else
			sIcon = "doc.gif"  'This is the last document on the list so use the DOC angle graphic
		end if
	else 
		if (bIsRoot = true) then
			'Root item requires special icon
			if (bShowOpen = true) then
				sIcon = "minusonly.gif"
			else
				sIcon = "plusonly.gif"
			end if
		elseif  (bHasChildren = true) then
			'Folder has children, so use default folder open icon
			if (bShowOpen = true) then
				sIcon = "folderopen.gif"
			else
				sIcon = "folderclosed.gif"
			end if
		elseif (bHasChildren = false) then
			'Folder does NOT have children, so first check
			'what order it is in the list
			if (bIsLast = false) then
				'Not the last member, so use an empty folder with a line join graphic
				sIcon = "folderclosedjoin-empty.gif"	
			else
				'Is the last member, so use an empty folder with a line angle graphic
				sIcon = "folderclosed-empty.gif"
			end if
		end if
	end if
	
	fnChooseIcon = sIcon
end function

'This function builds the html code necessary for indenting the menu
'item.  This includes any graphics that may be necessary for showing
'a continuation of a dotted line.  The new string is returned by the function.
'[Parameters]
'--------------------
'oNode :	Object reference for a node
'bIsLast :	TRUE/FALSE - Is this the last child in the list?
'sLeftIndent :	String containing the html code for indenting the item
function fnBuildLeftIndent(byval oNode, byval bIsLast, byVal sLeftIndent)
	
	'Check to see if this node is the last on the 
	'list or if it has more siblings.  We set up our indent
	'accordingly.  We need to set this up before displaying
	'any of the node's children.
	if (bIsLast = false) then
		'This node is not the last on the list, so we need to create
		'an indent that contains a dotted line.
		sLeftIndent = sLeftIndent & "<td><img src=images/line.gif width=18 height=16></td>"
	else
		'Otherwise it is the last on the list so we just need to
		'display a blank space
		sLeftIndent = sLeftIndent & "<td><img src=images/pixel.gif width=20 height=1 border=0></td>"
	end if
	
	fnBuildLeftIndent = sLeftIndent
end function

'Determines whether a folder needs to be displayed as Open or not.
'If the value of the iID is present in the hidden
'field that contains the IDs of all open folders, then we know 
'it should be Open.  Returns TRUE if folder should be open,
'otherwise FALSE is returned
function fnGetFolderStatus(iID, sOpenFolders)
	dim bReturn, sValue
	
	bReturn = false
	
	if (instr(sOpenFolders, CStr(iID))) then
		bReturn = true
	end if
	
	fnGetFolderStatus = bReturn
end function
%>