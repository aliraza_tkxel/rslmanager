<%@Language="VBScript"%>
<%Option Explicit%>
<%Response.Buffer = True%>
<%
Server.ScriptTimeOut = 60
On Error Resume Next
  
Dim strRootFolder
Dim intLenRootFolder
Dim objFSO
Dim strXmlFile
Dim strVbCrLf
Dim strVbTab
Dim numTree
Dim objFile
  
strVbCrLf = VbCrLf
strVbTab = VbTab
numTree = 0
strRootFolder = Request.ServerVariables("APPL_PHYSICAL_PATH")
intLenRootFolder = Len(strRootFolder)
strXmlFile = "<?xml version=""1.0"" ?>" & strVbCrLf _
  & "<root name=""Site Root"" path=""/"">" & strVbCrLf
  
'-- create an instance of the FSO object
Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
'-- open file for writing, create if not there, and open as text
Set objFile = objFSO.CreateTextFile(strRootFolder & "/navigation/menuitems.xml", True, False)
'-- write root node
objFile.Write(strXmlFile)
  
Call TraverseSite(strRootFolder,numTree)
  
Sub TraverseSite(strFolder,thisTree)
  '-- reset the string which gets written to the XML file
  strXmlFile = ""
  '-- declare our variables in the sub
  Dim objFolder, objSubFolder, objSubFile, i
  Dim objThisFolder, objthisFile, strURL, strFolderURL
  
  '-- grab the folder passed in the sub
  Set objFolder = objFSO.GetFolder(strFolder)
  '-- grab its subfolders
  Set objSubFolder = objFolder.SubFolders
  '-- grab its files
  Set objSubFile = objFolder.Files
  
  '-- properly nest the nodes for a nice display
  For i = 1 To thisTree
    strXmlFile = strXmlFile & strVbTab
  Next
  '-- get the folder path excluding the application root folder
  strFolderURL = Mid(objFolder.Path, intLenRootFolder, Len(objFolder.Path))
  '-- turn the physical path into a relative path
  strFolderURL = Replace(strFolderURL, "\", "/")
  '-- append node to the output unless it is the root folder
  If thisTree > 0 Then
    strXmlFile = strXmlFile & "<folder name=""" _
      & objFolder.Name & """ path=""" & strFolderURL & "/""" _
      & " loadOnDemand=""" & fnLoadOnDemand(strFolderURL) & """>" & strVbCrLf
    objFile.Write(strXmlFile)
  End If
  '-- reset the string output again
  strXmlFile = ""
  '-- if there are subfolders under it then run sub again for each one
  If Not IsEmpty(objSubFolder) Then
    For Each objThisFolder in objSubFolder
      Call TraverseSite(objThisFolder.Path,thisTree + 1)
    Next
  End If
  '-- if there are files in the folder
  If Not IsEmpty(objSubFile) Then
    For Each objthisFile in objSubFile
      '-- properly nest the nodes for a nice display
      For i = 0 To thisTree + 1
        strXmlFile = strXmlFile & strVbTab
      Next
      '-- get the virtual path
      strURL = Mid(objthisFile.Path, intLenRootFolder, Len(objthisFile.Path))
      strURL = Replace(strURL, "\", "/")
      '-- create output string
      strXmlFile = strXmlFile & "<document name=""" _
        & objthisFile.Name & """ path="""	& strURL & """/>" & strVbCrLf
    Next
    '-- write output to XML file
    objFile.Write(strXmlFile)
    strXmlFile = ""
  End If
  For i = 1 To thisTree
    strXmlFile = strXmlFile & strVbTab
  Next
  '-- close the folder node
  If thisTree > 0 Then
    strXmlFile = strXmlFile & "</folder>" & strVbCrLf
  End If
  objFile.Write(strXmlFile)
  strXmlFile = ""
  '-- show the progress of the code as it goes through folders
  Response.Write("Folder <font color=""#FF0000"">" _
    & strFolderURL & "</font> written!<br>")
  Response.Flush
End Sub
  
Function fnLoadOnDemand(folderPath)
  If folderPath = "/navigation" Then
    fnLoadOnDemand = "yes"
  Else
    fnLoadOnDemand = "no"
  End If
End Function
  
'--write end root node
objFile.Write("</root>")
objFile.close
Set objFile = Nothing
Set objFSO = Nothing
%>