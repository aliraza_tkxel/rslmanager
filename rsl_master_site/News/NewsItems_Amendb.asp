<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Sub BuildUploadRequest(RequestBin,UploadDirectory,storeType,sizeLimit,nameConflict)
  'Get the boundary
  PosBeg = 1
  PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
  if PosEnd = 0 then
    Response.Write "<b>Form was submitted with no ENCTYPE=""multipart/form-data""</b><br>"
    Response.Write "Please correct the form attributes and try again."
    Response.End
  end if
  'Check ADO Version
	set checkADOConn = Server.CreateObject("ADODB.Connection")
	adoVersion = CSng(checkADOConn.Version)
	set checkADOConn = Nothing
	if adoVersion < 2.5 then
    Response.Write "<b>You don't have ADO 2.5 installed on the server.</b><br>"
    Response.Write "The File Upload extension needs ADO 2.5 or greater to run properly.<br>"
    Response.Write "You can download the latest MDAC (ADO is included) from <a href=""www.microsoft.com/data"">www.microsoft.com/data</a><br>"
    Response.End
	end if		
  'Check content length if needed
	Length = CLng(Request.ServerVariables("HTTP_Content_Length")) 'Get Content-Length header
	If "" & sizeLimit <> "" Then
    sizeLimit = CLng(sizeLimit)
    If Length > sizeLimit Then
      Request.BinaryRead (Length)
      Response.Write "Upload size " & FormatNumber(Length, 0) & "B exceeds limit of " & FormatNumber(sizeLimit, 0) & "B"
      Response.End
    End If
  End If
  boundary = MidB(RequestBin,PosBeg,PosEnd-PosBeg)
  boundaryPos = InstrB(1,RequestBin,boundary)
  'Get all data inside the boundaries
  Do until (boundaryPos=InstrB(RequestBin,boundary & getByteString("--")))
    'Members variable of objects are put in a dictionary object
    Dim UploadControl
    Set UploadControl = CreateObject("Scripting.Dictionary")
    'Get an object name
    Pos = InstrB(BoundaryPos,RequestBin,getByteString("Content-Disposition"))
    Pos = InstrB(Pos,RequestBin,getByteString("name="))
    PosBeg = Pos+6
    PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(34)))
    Name = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
    PosFile = InstrB(BoundaryPos,RequestBin,getByteString("filename="))
    PosBound = InstrB(PosEnd,RequestBin,boundary)
    'Test if object is of file type
    If  PosFile<>0 AND (PosFile<PosBound) Then
      'Get Filename, content-type and content of file
      PosBeg = PosFile + 10
      PosEnd =  InstrB(PosBeg,RequestBin,getByteString(chr(34)))
      FileName = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      FileName = Mid(FileName,InStrRev(FileName,"\")+1)
      'Add filename to dictionary object
      UploadControl.Add "FileName", FileName
      Pos = InstrB(PosEnd,RequestBin,getByteString("Content-Type:"))
      PosBeg = Pos+14
      PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
      'Add content-type to dictionary object
      ContentType = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      UploadControl.Add "ContentType",ContentType
      'Get content of object
      PosBeg = PosEnd+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = FileName
      ValueBeg = PosBeg-1
      ValueLen = PosEnd-Posbeg
    Else
      'Get content of object
      Pos = InstrB(Pos,RequestBin,getByteString(chr(13)))
      PosBeg = Pos+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      ValueBeg = 0
      ValueEnd = 0
    End If
    'Add content to dictionary object
    UploadControl.Add "Value" , Value	
    UploadControl.Add "ValueBeg" , ValueBeg
    UploadControl.Add "ValueLen" , ValueLen	
    'Add dictionary object to main dictionary
    UploadRequest.Add name, UploadControl	
    'Loop to next object
    BoundaryPos=InstrB(BoundaryPos+LenB(boundary),RequestBin,boundary)
  Loop

  if (UploadRequest.Item("hid_DEFAULT_NEWSIMAGE").Item("Value") = "NEW" ) then 

  GP_keys = UploadRequest.Keys
  for GP_i = 0 to UploadRequest.Count - 1
    GP_curKey = GP_keys(GP_i)
    'Save all uploaded files
    if UploadRequest.Item(GP_curKey).Item("FileName") <> "" then
      GP_value = UploadRequest.Item(GP_curKey).Item("Value")
      GP_valueBeg = UploadRequest.Item(GP_curKey).Item("ValueBeg")
      GP_valueLen = UploadRequest.Item(GP_curKey).Item("ValueLen")

      if GP_valueLen = 0 then
        Response.Write "<B>An error has occured saving uploaded file!</B><br><br>"
        Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br>"
        Response.Write "File does not exists or is empty.<br>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
	  	  response.End
	    end if
      
      'Create a Stream instance
      Dim GP_strm1, GP_strm2
      Set GP_strm1 = Server.CreateObject("ADODB.Stream")
      Set GP_strm2 = Server.CreateObject("ADODB.Stream")
      
      'Open the stream
      GP_strm1.Open
      GP_strm1.Type = 1 'Binary
      GP_strm2.Open
      GP_strm2.Type = 1 'Binary
        
      GP_strm1.Write RequestBin
      GP_strm1.Position = GP_ValueBeg
      GP_strm1.CopyTo GP_strm2,GP_ValueLen
    
      'Create and Write to a File
      GP_curPath = Request.ServerVariables("PATH_INFO")
      GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
      if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
        GP_curPath = GP_curPath & "/"
      end if 
      GP_CurFileName = UploadRequest.Item(GP_curKey).Item("FileName")
      GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & GP_CurFileName
      'Check if the file alreadu exist
      GP_FileExist = false
      Set fso = CreateObject("Scripting.FileSystemObject")
      If (fso.FileExists(GP_FullFileName)) Then
        GP_FileExist = true
      End If      
      if nameConflict = "error" and GP_FileExist then
        Response.Write "<B>File already exists!</B><br><br>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
				GP_strm1.Close
				GP_strm2.Close
	  	  response.End
      end if
      if ((nameConflict = "over" or nameConflict = "uniq") and GP_FileExist) or (NOT GP_FileExist) then
        if nameConflict = "uniq" and GP_FileExist then
          Begin_Name_Num = 0
          while GP_FileExist    
            Begin_Name_Num = Begin_Name_Num + 1
            GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            GP_FileExist = fso.FileExists(GP_FullFileName)
          wend  
          UploadRequest.Item(GP_curKey).Item("FileName") = fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
					UploadRequest.Item(GP_curKey).Item("Value") = UploadRequest.Item(GP_curKey).Item("FileName")
        end if
        on error resume next
        GP_strm2.SaveToFile GP_FullFileName,2
        if err then
          Response.Write "<B>An error has occured saving uploaded file!</B><br><br>"
          Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br>"
          Response.Write "Maybe the destination directory does not exist, or you don't have write permission.<br>"
          Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
    		  err.clear
  				GP_strm1.Close
  				GP_strm2.Close
  	  	  response.End
  	    end if
  			GP_strm1.Close
  			GP_strm2.Close
  			if storeType = "path" then
  				UploadRequest.Item(GP_curKey).Item("Value") = GP_curPath & UploadRequest.Item(GP_curKey).Item("Value")
  			end if
        on error goto 0
      end if
    end if
  next

  end if

End Sub

'String to byte string conversion
Function getByteString(StringStr)
  For i = 1 to Len(StringStr)
 	  char = Mid(StringStr,i,1)
	  getByteString = getByteString & chrB(AscB(char))
  Next
End Function

'Byte string to string conversion
Function getString(StringBin)
  getString =""
  For intCount = 1 to LenB(StringBin)
	  getString = getString & chr(AscB(MidB(StringBin,intCount,1))) 
  Next
End Function

Function UploadFormRequest(name)
  on error resume next
  if UploadRequest.Item(name) then
    UploadFormRequest = UploadRequest.Item(name).Item("Value")
  end if  
End Function

If (CStr(Request.QueryString("GP_upload")) <> "") Then
  GP_redirectPage = ""
  If (GP_redirectPage = "") Then
    GP_redirectPage = CStr(Request.ServerVariables("URL"))
  end if
    
  RequestBin = Request.BinaryRead(Request.TotalBytes)
  Dim UploadRequest
  Set UploadRequest = CreateObject("Scripting.Dictionary")  
  BuildUploadRequest RequestBin, "NewsImages", "file", "", "uniq"
	  
  '*** GP NO REDIRECT
end if  
if UploadQueryString <> "" then
  UploadQueryString = UploadQueryString & "&GP_upload=true"
else  
  UploadQueryString = "GP_upload=true"
end if  

%>
<%
' *** Edit Operations: (Modified for File Upload) declare variables

MM_editAction = CStr(Request.ServerVariables("URL")) 'MM_editAction = CStr(Request("URL"))
If (UploadQueryString <> "") Then
  MM_editAction = MM_editAction & "?" & UploadQueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: (Modified for File Upload) set variables

If (CStr(UploadFormRequest("MM_insert")) <> "") Then

  MM_editConnection = RSL_CONNECTION_STRING
  MM_editTable = "dbo.G_NEWS"
  MM_editColumn = "NewsID"
  MM_recordId = "" + UploadFormRequest("MM_recordId") + ""
  MM_editRedirectUrl = "PartnershipNewsItem_Edit.asp?MaxID=" + MM_recordId
  if (UploadFormRequest("hid_DEFAULT_NEWSIMAGE") = "OLD") then
	  MM_fieldsStr  = "txt_TITLE|value|txt_DATEEXPIRES|value|hid_NEWSFOR|value|hid_USERID|value|hid_DATEMODIFIED|value"
	  MM_columnsStr = "TITLE|',none,''|DATEEXPIRES|',none,''|NEWSFOR|',none,''|MODIFIEDBY|',none,''|DATEMODIFIED|',none,''"
  elseif (UploadFormRequest("hid_DEFAULT_NEWSIMAGE") = "DEF") then
	  MM_fieldsStr  = "txt_TITLE|value|txt_DATEEXPIRES|value|GET_NOTHING|value|hid_NEWSFOR|value|hid_USERID|value|hid_DATEMODIFIED|value"
	  MM_columnsStr = "TITLE|',none,''|DATEEXPIRES|',none,''|NEWSIMAGE|',none,''|NEWSFOR|',none,''|MODIFIEDBY|',none,''|DATEMODIFIED|',none,''"
  else
	  MM_fieldsStr  = "txt_TITLE|value|txt_DATEEXPIRES|value|NEWSIMAGE|value|hid_NEWSFOR|value|hid_USERID|value|hid_DATEMODIFIED|value"
	  MM_columnsStr = "TITLE|',none,''|DATEEXPIRES|',none,''|NEWSIMAGE|',none,''|NEWSFOR|',none,''|MODIFIEDBY|',none,''|DATEMODIFIED|',none,''"
  end if

  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  
  ' set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(UploadFormRequest(MM_fields(i)))
  Next

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And UploadQueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And UploadQueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & UploadQueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & UploadQueryString
    End If
  End If

End If
%>
<%
' *** Insert Record: (Modified for File Upload) construct a sql insert statement and execute it

If (CStr(UploadFormRequest("MM_insert")) <> "") Then

  ' create the sql update statement
  MM_editQuery = "update " & MM_editTable & " set "
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_editQuery = MM_editQuery & ","
    End If
    MM_editQuery = MM_editQuery & MM_columns(i) & " = " & FormVal
  Next
  MM_editQuery = MM_editQuery & " where " & MM_editColumn & " = " & MM_recordId
  
  If (Not MM_abortEdit) Then
    ' execute the insert
	OpenDB()
    set MM_editCmd = Conn.Execute(MM_editQuery)
	Response.Redirect("NewsItem_Edit.asp?MaxID=" & MM_recordId)
	CloseDB()
	
  End If

End If
%>
<%
Dim NewsItemID
NewsItemID = "0"
if (Request("ID") <> "") then NewsItemID = Replace(Request("ID"), "'", "''")
%>
<%
OpenDB()
SQL = "SELECT TEAMCODE, TEAMNAME FROM G_TEAMCODES WHERE NEWSALLOWED = 1"
Call OpenRS(rsTeams, SQL)
TeamStr = ""
WHILE NOT rsTeams.EOF
	TeamStr = TeamStr & "<TR><TD>" & rsTeams("TEAMNAME") & "&nbsp;</TD><TD>:&nbsp;</TD><TD><input type=""checkbox"" id=""NewsType"" value=""" & rsTeams("TEAMCODE") & """></TD></TR>"
	rsTeams.moveNext
WEND
CloseRs(rsTeams)

SQL = "SELECT TITLE, NEWSIMAGE, NEWSFOR, DATEEXPIRES = CONVERT(VARCHAR, DATEEXPIRES, 103) FROM G_NEWS WHERE NEWSID = " & NewsItemID
Call OpenRs(rsNews, SQL)
if (not rsNews.EOF) then
	title = rsNews("TITLE")
	newsfor = rsNews("NEWSFOR")
	dateexpires = rsNews("DATEEXPIRES")
	newsimage = rsNews("NEWSIMAGE")
	initialvalue = "DEF"
	defaultselected = ";background-color:beige"
	initialstatus = "READONLY"
	if (newsimage <> "") then	
		initialcolour = ";background-color:beige"
		initialvalue = "OLD"
		defaultselected	= ""
	end if
end if
CloseRs(rsNews)
CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager News --> News --> Amend News</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/menu.js"></SCRIPT>
<script language="JavaScript">
function getFileExtension(filePath) { //v1.0
  fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/')+1,filePath.length) : filePath.substring(filePath.lastIndexOf('\\')+1,filePath.length));
  return fileName.substring(fileName.lastIndexOf('.')+1,fileName.length);
}

function checkFileUpload(form,extensions) { //v1.0
  document.MM_returnValue = true;
  if (extensions && extensions != '') {
    for (var i = 0; i<form.elements.length; i++) {
      field = form.elements[i];
      if (field.type.toUpperCase() != 'FILE') continue;
      if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
        ManualError('img_NEWSIMAGE','This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.',0);
        document.MM_returnValue = false;field.focus();
		return false;
		break;
  } } }
	ManualError('img_NEWSIMAGE','',3);
	return true;
}
</script>
<script language="JavaScript">
<!--
var FormFields = new Array()
FormFields[0] = "txt_TITLE|Title|TEXT|Y"						
FormFields[1] = "txt_DATEEXPIRES|Expiry Date|DATE|Y"						
FormFields[2] = "hid_NEWSFOR|Share News|TEXT|Y"

function BtnSubmit(){
	temp = document.getElementsByName("NewsType")
	strNews = ""
	if (temp.length){
		for (i=0; i<temp.length; i++){
			if (temp[i].checked == true)
				strNews += "," + temp[i].value
			}
		}
	if (strNews != "") strNews += ","
	document.getElementById("hid_NEWSFOR").value = strNews

	if (document.UserNews.hid_DEFAULT_NEWSIMAGE.value == "DEF" || document.UserNews.hid_DEFAULT_NEWSIMAGE.value == "OLD")
		result = true
	else
		result = checkFileUpload(UserNews,'gif;jpg;jpeg');

	if (!checkForm()) return false;	
	if (!result) return false;
	go();
	}

function DoPreview() {
  var filename = document.getElementById("NEWSIMAGE").value;
  var Img = new Image();
  imagestat = document.getElementById("hid_DEFAULT_NEWSIMAGE").value
  if (imagestat == "OLD"){
    document.images["NewsPreview"].src = "NewsImages/<%=newsimage%>";
	}
  else if (imagestat == "DEF"){
    document.images["NewsPreview"].src = "/myImages/news_image.gif";
	}
  else {  
	  if (filename == "") {
		Img.src = filename;
		<% if (newsimage = "") then %>
		document.images["NewsPreview"].src = "/myImages/news_image.gif";
		<% else %>
		document.images["NewsPreview"].src = "NewsImages/<%=newsimage%>";
		<% end if %>	
		}
	  else {
		Img.src = filename;
		document.images["NewsPreview"].src = Img.src;
		}
	 }
  }

function SetChecks(){
	Data = "<%=newsfor%>"
	if (Data != "") {
		DataArray = Data.split(",")
		checkRef = document.getElementsByName("NewsType")

		for (i=1; i<Data.length-1; i++){
			if (checkRef.length){
				for (j=0; j<checkRef.length; j++){
					if (checkRef[j].value == DataArray[i])
						checkRef[j].checked = true;
					}
				}
			}
		}
	}
	
function go(){	
	UserNews.action = "<%=MM_editAction%>";
	UserNews.submit();	
	}

function SelectAll(newStatus) {
	temp = document.getElementsByName("NewsType")
	if (temp.length){
		for (i=0; i<temp.length; i++){
			temp[i].checked = newStatus
			}	
		}
	}

function NewImageUp(){
	document.getElementById("New").style.backgroundColor = "beige"
	document.getElementById("hid_DEFAULT_NEWSIMAGE").value = "NEW"			
<% if (newsimage <> "") then %>
	document.getElementById("Upl").style.backgroundColor = ""		
<% end if %>	
	document.getElementById("Def").style.backgroundColor = ""			
	document.UserNews.NEWSIMAGE.readOnly = false
	DoPreview()
	}

function DoDefaultImage(){
<% if (newsimage <> "") then %>
	document.getElementById("Upl").style.backgroundColor = ""		
<% end if %>	
	document.getElementById("New").style.backgroundColor = ""		
	document.getElementById("hid_DEFAULT_NEWSIMAGE").value = "DEF"			
	document.getElementById("Def").style.backgroundColor = "beige"		
	document.UserNews.NEWSIMAGE.readOnly = true
	DoPreview()	
	}

function UploadedImage(){
	document.getElementById("Def").style.backgroundColor = ""
	document.getElementById("New").style.backgroundColor = ""	
	document.getElementById("hid_DEFAULT_NEWSIMAGE").value = "OLD"			
	document.getElementById("Upl").style.backgroundColor = "beige"		
	document.UserNews.NEWSIMAGE.readOnly = true
	DoPreview()	
	}

//-->
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages();SetChecks()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
  <table border="0" cellspacing="0" cellpadding="0" width=100%>
	<tr> 
	  <td> 
		<% IF Request.QueryString("Add") = "Success" Then %>
		Your News Story has been added to the Whiteboard 
		<%Else %>
		<form METHOD="POST" name="UserNews" action="<%=MM_editAction%>" ENCTYPE="multipart/form-data">
		  <!-- action="<%=MM_editAction%>" //-->
		  <table width="100%" border="0" cellspacing="1" cellpadding="1" class="RSLBLack">
			<tr>
			  <td colspan=5><br>
              To amend a news item, please update the values for the news article 
              below.<br>
				<font color=red><b>Please Note:</b></font> All images 
				must be 145px by 135px as they will be reshaped to this 
				size automatically, thus causing blurring for images that 
				are not the same size.<br>
				<br>
			  </td>
			</tr>
			<tr> 
            <td width="100px" nowrap>News Headline</td><td>:&nbsp;</td>
			  <td> 
				<input name="txt_TITLE" type="text" maxlength="30" class="textbox200" style='width:324px' value="<%=title%>">
			  </td>
     			<TD><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>
				<td rowspan=5 valign=top align=center width=100%>
				<% if (newsimage = "") then %>
					<img name="NewsPreview" src="/myImages/news_image.gif" width="145" height="135" border="1" bordercolor="#133E71" />
				<% else %>		
					<img name="NewsPreview" src="NewsImages/<%=newsimage%>" width="145" height="135" border="1" bordercolor="#133E71" />				
				<% end if %>
			</td>										  
				
			</tr>
			<tr> 
			  <td>Expiry Date </td><td>:&nbsp;</td>
			  <td> 
				<input name="txt_DATEEXPIRES" type="text" maxlength="10" class="textbox200" style='width:324px'' value="<%=dateexpires%>">
			  </td>
        		<TD><image src="/js/FVS.gif" name="img_DATEEXPIRES" width="15px" height="15px" border="0"></TD>										  
			</tr>
			<tr> 
            <td >News Image </td><td>:&nbsp;</td>
			  <td> 
				<input type="file" size=40 name="NEWSIMAGE" class="RSLButton" style='background-color:white;color:black' onchange="DoPreview()" <%=initialstatus%>>
			  </td>
     			<TD><image src="/js/FVS.gif" name="img_NEWSIMAGE" width="15px" height="15px" border="0"></TD>										  
			</tr>
			<tr><td colspan=2></td>
			<td>
				<input type="button" onclick="NewImageUp()" value="New Image" name="New" class="RSLButton" style='font-size:8px'>				
				<input type="button" onclick="DoDefaultImage()" value="Default Image" name="Def" class="RSLButton" style='font-size:8px<%=defaultselected%>'>
				<% if (newsimage <> "") then %>				
				<input type="button" onclick="UploadedImage()" value="Uploaded Image" name="Upl" class="RSLButton" style='font-size:8px<%=initialcolour%>'>				
				<% end if %>
			</td>
			<tr> 
            <td valign=top>Share News</td><td valign=top>:&nbsp;</td>
			<td>
				<table>
				<tr><td colspan=3 nowrap>Please select all teams you wish to share the news with...</td></tr>
				<%=TeamStr%>
				</table>								
			</td>
			<TD valign=top><br><br><image src="/js/FVS.gif" name="img_NEWSFOR" width="15px" height="15px" border="0"></TD>										
			</tr>
			<tr> 
			  <td colspan=2></td>
			  <td width=330px> 
				<input type="button" value="Select All" onClick="SelectAll(true)" class="RSLButton" style='font-size:8px'>
				<input type="button" value="Deselect All" onClick="SelectAll(false)" class="RSLButton" style='font-size:8px'>
			  </td>
			</tr>			
			<tr> 
			  <td colspan=2></td>
			  <td align=right width=330px> 
				  <input name="hid_DATEMODIFIED" type="hidden" value="<%=FormatDateTime(date,1)%>">
				  <input name="hid_DEFAULT_NEWSIMAGE" type="hidden" value="<%=initialvalue%>">
				  <input name="hid_NEWSFOR" type="hidden" value="">
				  <input name="hid_USERID" type="hidden" value="<%=Session("UserID")%>">
				  <input type="hidden" name="MM_insert" value="true">
	              <input type="hidden" name="MM_recordId" value="<%= NewsItemID %>">				  
				<input type="button" name="Submit" value="Update Story" onClick="BtnSubmit()" class="RSLButton">
			  </td>
			</tr>
		  </table>
		</form>
		<% End IF %>
	  </td>
	</tr>
  </table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

