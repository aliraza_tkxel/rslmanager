<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 
dim Session
set Session = server.CreateObject("SessionMgr.Session2")
%>
<!-- #include virtual="Connections/db_connection.asp" -->
<% 

	'THIS PART REMOVES A LOGGED IN USER FROM THE APPLICATION OBJECT
	If Application("ActiveUserList") <> "" Then 
		intUsers = 0
		strActiveUserList = Application("ActiveUserList")
		strActiveUserList = Left(strActiveUserList, Len(strActiveUserList) - 3)
	
		aActiveUsers = Split(strActiveUserList, "|||")
	
		strActiveUserInformation = Application("ActiveUserInformation")
		strActiveUserInformation = Left(strActiveUserInformation, Len(strActiveUserInformation) - 3)
	
		aActiveInformation = Split(strActiveUserInformation, "|||")	
		SQL_LOGOUT = ""
		For ix = 0 To UBound(aActiveUsers)
			If Instr(1, aActiveUsers(ix), Session.SessionID) > 0 Then
				SQL_LOGOUT = SQL_LOGOUT & "UPDATE LOGIN_HISTORY SET LOGOUTTIME = GETDATE(), LOGOUTTYPE = 1 WHERE SESSIONID = " & Session.SessionID & " AND LOGOUTTIME IS NULL;"
				aActiveUsers(ix) = "XXXX"
				aActiveInformation(ix) = "XXXX"			
			Else
				intUsers = intUsers + 1	
			End If 
		Next

		if (SQL_LOGOUT <> "") Then
			Set LogConnection = Server.CreateObject("ADODB.Connection")
			LogConnection.Open RSL_CONNECTION_STRING 
			'LogConnection.Execute SQL_LOGOUT				
			LogConnection.Close
			Set LogConnection = Nothing
		end if
				
		strActiveUserList = Join(aActiveUsers, "|||") & "|||"
		strActiveUserList = Replace(strActiveUserList, "XXXX|||", "")
	
		strActiveUserInformation = Join(aActiveInformation, "|||") & "|||"
		strActiveUserInformation = Replace(strActiveUserInformation, "XXXX|||", "")	
	
		Application.Lock
		Application("ActiveUserList") = strActiveUserList
		Application("ActiveUserInformation") = strActiveUserInformation			
		Application("ActiveUsers") = intUsers	
		Application.UnLock
	end if

	'Session.Abandon() 
	'Response.Redirect "/login/login.aspx?LOGGEDOUT=1"
	Response.Redirect "/BHAIntranet/login.aspx?status=signout"
%>