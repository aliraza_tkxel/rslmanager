<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpaceUsed.aspx.vb" Inherits="SpaceUsed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>
            Space Used</h1>
        <p>
            This page shows how to list and sort information about all of the user tables in
            a database.</p>
        <p>
            <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Rows" DataFormatString="{0:#,##0}" HeaderText="Rows" HtmlEncode="False"
                        SortExpression="Rows">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Data_Int" DataFormatString="{0:#,##0} KB" HeaderText="Data Size"
                        HtmlEncode="False" SortExpression="Data_Int">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Index_Size_Int" DataFormatString="{0:#,##0} KB" HeaderText="Index Size"
                        HtmlEncode="False" SortExpression="Index_Size_Int">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" connectionString="Data Source=(local);Initial Catalog=RSLBHALive;Integrated Security=true" providerName="System.Data.SqlClient"
                SelectCommand="TableSpaceUsed" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </p>
    
    </div>
    </form>
</body>
</html>
