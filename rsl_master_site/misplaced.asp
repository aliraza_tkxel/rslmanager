<%@  language="VBSCRIPT" codepage="1252" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>RSL Manager</title>
    <link href="BHAIntranet/CSS/style.css" rel="stylesheet" type="text/css" />
</head>
<script language="JavaScript" src="/js/preloader.js" type="text/javascript"></script>
<body bgcolor="#FFFFFF" onload="preloadImages()" marginheight="0" leftmargin="10"
    topmargin="10" marginwidth="0">
    <img src="bhaintranet/images/bg_page.png" width="2312" height="1540" class="bgimg" />
    <div id="wrapper">
        <div>
            <img src="bhaintranet/images/logored.png" />
        </div>
        <div class="logo-rsl">
            <img src="bhaintranet/images/rsllogo.png" />
        </div>
        <div class="rsl-login">
            <div class="fpass">
                <p>
                    <strong>Forgotten your password?</strong>
                </p>
                <p>
                    Please enter your designated email and your password will be sent to you.
                </p>
                <br />
            </div>
            <form name="loginform" target="serverFrame" method="post" action="sendpwd.asp">
            <div>
                <input type="text" class="txtfield email" value="Email" name="Email" id="Email"
                    onblur="if (this.value == '') {this.value = 'Email';}" onfocus="if (this.value == 'Email') {this.value = '';}" />
            </div>
            <div class="btn-area">
                <input type="submit" class="btn" value="Submit" name="send" />
            </div>
            </form>            
            <div class="login-again">
                <div id="errorDiv" name="errorDiv">
                    <br>
                    <strong><a href="default.asp">Try to login again</a></strong>
                </div>
            </div>
        </div>
        <div class="rsl-login">
            <div class="footer">
                Broadland Housing Association, NCFC, Jarrold Stand, Carrow Road, Norwich, NR1 1HU<br />
                Tel: 01603 750200 Fax: 01603 750222 Email: enq@broadlandhousing.org
            </div>
        </div>
    </div>
    <iframe src="/secureframe.asp" name="serverFrame" width="300px" height="200px" style='display: none'>
    </iframe>
</body>
</html>
