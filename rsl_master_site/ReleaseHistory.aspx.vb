Imports System.IO

Partial Class ReleaseHistory
    Inherits System.Web.UI.Page

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load
        If Not Page.IsPostBack Then
            Dim di As New DirectoryInfo(Server.MapPath("~\VersionHistory"))
            gvReleaseHistory.DataSource = di.GetFiles("*.pdf")
            gvReleaseHistory.DataBind()

        End If
    End Sub


    Protected Function GetReleasePath(ByVal sPath As String) As String
        Return "VersionHistory\" + Path.GetFileName(sPath)
    End Function


End Class
