<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst_post, lst_paymenttype
	
	OpenDB()
	
	Call BuildSelect(lst_post, "sel_DIRECTPOSTITEMS", " F_DIRECTPOSTITEMS WHERE ITEMID NOT IN(SELECT ITEMID WHERE ITEMID BETWEEN 56 and 66) ", "ITEMID, ITEMNAME", "ITEMNAME", "Please Select", 0, null, "textbox200", "ONCHANGE='get_items()' disabled")
	Call BuildSelect(lst_paymenttype, "sel_", " F_DIRECTPOSTITEMS ", "ITEMID, ITEMNAME", "ITEMNAME", "Please Select", 0, null, "textbox200", "ONCHANGE='get_items()' disabled")

	' IF LOCKDOWN IS ON THEN WE CANT POST IN PREV YEARS OR FUTURE YEARS
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ACCOUNTS_LOCKDOWN' "
	Call OpenRs(rsLockdown, SQL)	
	LockdownStatus = rsLockdown("DEFAULTVALUE")
	Call CloseRs(rsLockdown)
	
	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	SQL = "EXEC GET_VALIDATION_PERIOD"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	CloseDB()	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Direct Posting</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var is_a_valid_tenancy = 0

	function SetForSave(){
		// SET APPROPRIATE FORM FIELDS
		FormFields.length = 0		
		FormFields[0] = "hid_TENANTNUMBER|TENANTNUMBER|INTEGER|N"
		FormFields[1] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
		FormFields[2] = "sel_DIRECTPOSTITEMS|DIRECTPOSTITEMS|SELECT|Y"
		FormFields[3] = "txt_CLAIMNUMBER|CLAIMNUMBER|TEXT|N"
	 	FormFields[4] = "txt_PROCESSDATE|Processing Date|DATE|Y"
		if (dates_required == 1){
			FormFields[5] = "txt_FROM|FROM|DATE|Y"
			FormFields[6] = "txt_TO|TO|DATE|Y"
			}
		else {
			FormFields[5] = "txt_FROM|FROM|DATE|N"
			FormFields[6] = "txt_TO|TO|DATE|N"
			}
		}

	function ATTACHITEM (JournalID, JournalValue){
		RSLFORM.txt_AMOUNT.value = FormatCurrency(JournalValue);	
		RSLFORM.hid_ATTACHID.value = JournalID
		RSLFORM.txt_AMOUNT.readOnly = true
		RSLFORM.Detach.style.visibility = "visible";					
		}
	
	function ATTACHBANK(BankID){
		RSLFORM.hid_ATTACHID.value = BankID	
		}

	function ATTACHCUSTOMER(CustomerID){
		RSLFORM.hid_ATTACHID.value = CustomerID	
		}
		
	function DETACHITEM(){
		RSLFORM.txt_AMOUNT.readOnly = false
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_AMOUNT.value = "";
		RSLFORM.hid_ATTACHID.value = "";															
		ref = document.getElementsByName("attach")
		if (ref.length){
			for (i=0; i<ref.length; i++){
				ref[i].checked = false
				}
			}
		}
				
	function save_form(){
		SetForSave()
		if (!checkForm()) return false;

    <% if LockdownStatus = "ON" then %>
	    var YStart = new Date("<%=YearStartDate%>")
	    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
	    var YEnd = new Date("<%=YearendDate%>")
	    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
	    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 2, YearStartDate),1) %>") 

	    if (!checkForm()) return false;
	    if  ( real_date(RSLFORM.txt_PROCESSDATE.value) < YStart || real_date(RSLFORM.txt_PROCESSDATE.value) > YEnd ) {
	        // outside current financial year
	        if ( TodayDate <= YEndPlusTime ) {
	            // inside grace period
	            if ( real_date(RSLFORM.txt_PROCESSDATE.value) < YStartPlusTime || real_date(RSLFORM.txt_PROCESSDATE.value) > YEnd) {
	                // outside of last and this financial year
                    alert("Please enter a tax date for either the previous\nor current financial year\n(i.e. between '<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>' and '<%=YearendDate%>')");
                    return false;
	            } else {
	                // date must be ok?
	            }
	        } else {
	            // we're outside the grace period and also outside this financial year
                alert("Please enter a tax date for this financial year\n(i.e. between '<%=YearStartDate%>' and '<%=YearendDate%>')");
                return false;                
		    }
	    }	
	<% end if %>

		var is_returnable = RSLFORM.sel_DIRECTPOSTITEMS.value;
		if (is_returnable == 4 || is_returnable == 5 || is_returnable == 13 || is_returnable == 16){
			if (RSLFORM.hid_ATTACHID.value == "") {
				//answer = confirm("No Journal Entry has been attached, are you sure you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
				//if (!answer) return false
				alert("No Journal Entry has been attached!\nPlease select a journal entry to reverse before continuing...")
				return false;
				}
			}

		if (is_returnable == 6){
			if (RSLFORM.hid_ATTACHID.value == "") {
				alert("You must attach a bank line before you can process a Tenant Refund (BACS).\nIf none are setup click on the appropriate customer name to set one up.")
				return false
				}
			}

		if (is_returnable == 11){
			if (RSLFORM.hid_ATTACHID.value == "") {
				alert("You must select which customer will be paid the cheque before you can process a Tenant Refund (Cheque).")
				return false
				}
			}

		tenantNumber = RSLFORM.txt_TENANTSEARCH.value
		ClaimNumber  = RSLFORM.txt_CLAIMNUMBER.value
		PostingType  = RSLFORM.sel_DIRECTPOSTITEMS[ RSLFORM.sel_DIRECTPOSTITEMS.selectedIndex].text;
		ProcessDate	 = RSLFORM.txt_PROCESSDATE.value
		
		if (RSLFORM.txt_FROM.value != ""){
			FromToString = "\n\n Date From : " + RSLFORM.txt_FROM.value + "\n\n Date To : " +  RSLFORM.txt_TO.value
			}
		else {
			FromToString = ""
			}
		
		Amount		 = RSLFORM.txt_AMOUNT.value
		alert("Tenant Number : " + tenantNumber + " \n\nClaim Number : " + ClaimNumber + " \n\nPosting Type : " + PostingType  + " \n\nProcess Date : " + ProcessDate + FromToString + " \n\nAmount : " + Amount + " \n\n\n\n Please confirm that you have checked all the submitted information is correct.")

		if (confirm("Please confirm that you have checked all the submitted information is correct.")){
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait posting...</B></FONT>";
		STATUS_DIV.style.visibility = "visible";
		RSLFORM.target = "frm_direct";
		RSLFORM.action = "serverside/direct_srv.asp";
		RSLFORM.btn_POST.disabled = true;		
		RSLFORM.submit();
		}
	}
	
	// 1 = called by tenant number 2 = callled by claim number
	function get_tenant(int_which){
	
		// SET APPROPRIATE FORMM FIELDS
		FormFields.length = 0
		if (int_which == 1)
			FormFields[0] = "txt_TENANTSEARCH|Tenant Number|INTEGER|Y"
		else 
			FormFields[0] = "txt_CLAIMNUMBER|Claim Number|TEXT|Y"
			
		if (!checkForm()) return false;
		
		ACCOUNT_DIV.innerHTML = "";
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait finding tenant...</B></FONT>";
		STATUS_DIV.style.visibility = "visible";
		RSLFORM.target = "frm_direct";
		if (int_which == 1)
			RSLFORM.action = "serverside/direct_srv.asp?action=GET";
		else
			RSLFORM.action = "serverside/direct_srv.asp?action=GETWITHHBREF";
		RSLFORM.txt_AMOUNT.readOnly = false
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_AMOUNT.value = "";											
		RSLFORM.hid_ATTACHID.value = "";													
		RSLFORM.submit();
		
	
	}
	var dates_required = 0 //used to determine whether or not to force from and to dates
	
	// retrieve items from this rent account if cheque or DD is selected 
	function get_items(){
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>&nbsp;</B></FONT>";		
		
		var is_returnable = RSLFORM.sel_DIRECTPOSTITEMS.value;
		// if chosen item is HB (2 or 3) or LA refund (1) OR DSS(7) or support payments(8,9) then shfow date boxes 
		if (is_returnable == '1' || is_returnable == '2' || is_returnable == '3' || is_returnable == '7' || is_returnable == '8' || is_returnable == '9' || is_returnable == '10'){
			dates_required = 1;
			RSLFORM.txt_FROM.style.visibility = "visible";
			RSLFORM.txt_TO.style.visibility = "visible";
			fromspan.style.visibility = "visible";
			tospan.style.visibility = "visible";
			}
		else {
			dates_required = 0;
			RSLFORM.txt_FROM.style.visibility = "hidden";
			RSLFORM.txt_TO.style.visibility = "hidden";
			fromspan.style.visibility = "hidden";
			tospan.style.visibility = "hidden";
			}

		RSLFORM.txt_FROM.value = "";
		RSLFORM.txt_TO.value = "";
		SetForSave()
		checkForm()

		RSLFORM.txt_AMOUNT.readOnly = false		
		RSLFORM.txt_AMOUNT.value = "";
		RSLFORM.hid_ATTACHID.value = "";															
		RSLFORM.Detach.style.visibility = "hidden";
		// is dd unpaid or cheque unpaid
		if ((is_returnable == 4 || is_returnable == 5 || is_returnable == 13 || is_returnable == 16) && is_a_valid_tenancy == 1){
		
			STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait retrieving items...</B></FONT>";
			STATUS_DIV.style.visibility = "visible";
			RSLFORM.target = "frm_direct";
			RSLFORM.action = "serverside/get_direct_post_items.asp";
			RSLFORM.submit();
			}
		//if Tenant Refund Bacs then get bank details
		else if ((is_returnable == 6) && is_a_valid_tenancy == 1){
			STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait checking bank details...</B></FONT>";
			STATUS_DIV.style.visibility = "visible";
			RSLFORM.target = "frm_direct";
			RSLFORM.action = "serverside/get_direct_post_bankdetails.asp";
			RSLFORM.submit();
			}
		else if ((is_returnable == 11) && is_a_valid_tenancy == 1){
			STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait getting tenant details...</B></FONT>";
			STATUS_DIV.style.visibility = "visible";
			RSLFORM.target = "frm_direct";
			RSLFORM.action = "serverside/get_direct_post_customers.asp";
			RSLFORM.submit();
			}
		else 
			ACCOUNT_DIV.innerHTML = "";
	}
			
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
		
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR>
			
      <TD>Enter tenant number to bring back correct tenant, then attach appropriate direct posting. If you choose 'DD Unpaid' or
	  		'Cheque' unpaid you can then attach the payment to a specific item. All HB entries require a date range to be entered.
	  </TD>
		</TR>
		<TR style=height:15px><TD></td></TR>
	</TABLE>
			
    <TABLE BORDER=0 width=90% CELLPADDING=2 CELLSPACING=0 align=center>
<form name = RSLFORM method=post>
        <TR> 
      <TD nowrap><B>Enter Tenant Number :</B></TD>
            <TD colspan=2><INPUT TYPE=TEXT NAME=txt_TENANTSEARCH CLASS='TEXTBOX100' MAXLENGTH=10>
				 <image src="/js/FVS.gif" name="img_TENANTSEARCH" width="15px" height="15px" border="0">
				<INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_FINDT' VALUE=' Find ' onclick='get_tenant(1)'>
			</TD>
			<TD valign=top STYLE='BORDER:1PX SOLID #133E71'  width=90% bgcolor=beige rowspan=7 height=100px>
			<div id=details>&nbsp;</div>
			</TD>
		</TR>
		<TR> 
			
      <TD nowrap><B>Enter Claim Number :</B></TD>
            <TD colspan=2><INPUT TYPE=TEXT NAME=txt_CLAIMNUMBER CLASS='TEXTBOX100' MAXLENGTH=10>
				 <image src="/js/FVS.gif" name="img_CLAIMNUMBER" width="15px" height="15px" border="0">
				<INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_FINDC' VALUE=' Find ' onclick='get_tenant(2)'></TD>
		</TR>
		<TR style=height:15px><TD colspan=3></td></TR>
		<TR> 
		  	
      <TD nowrap><B>Posting Type :</B></TD>
            <TD nowrap><%=lst_post%>
				 <image src="/js/FVS.gif" name="img_DIRECTPOSTITEMS" width="15px" height="15px" border="0">
			</TD>
		</TR>
		<TR> 
	      <TD nowrap><b>Process Date :</b></TD>
		  	<TD nowrap colspan=2>
				<INPUT TYPE=TEXT NAME=txt_PROCESSDATE CLASS='TEXTBOX100' MAXLENGTH=10>
				<image src="/js/FVS.gif" name="img_PROCESSDATE" width="15px" height="15px" border="0">
			</TD>				
		</TR>
		<TR> 
	      <TD nowrap><b><span id=fromspan style='visibility:hidden'>Date From :</span></b></TD>
		  	<TD nowrap colspan=2>
				<INPUT TYPE=TEXT NAME=txt_FROM CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:hidden'>
				<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
				&nbsp;<B><span id=tospan style='visibility:hidden'>To:</span></B>
				<INPUT TYPE=TEXT NAME=txt_TO CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:hidden'>
				<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">				
			</TD>				
		</TR>
		<TR> 
		  	<TD nowrap><B>Amount :</B></TD>
            <TD nowrap colspan=2>
				<INPUT TYPE=TEXT NAME=txt_AMOUNT CLASS='TEXTBOX100' MAXLENGTH=10>
				<image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0">
				<INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_POST' VALUE=' Post ' DISABLED ONCLICK="save_form()">
				<INPUT TYPE=hidden NAME=hid_TENANTNUMBER>
				<INPUT TYPE=hidden NAME=hid_ATTACHID>				
				 <image src="/js/FVS.gif" name="img_TENANTNUMBER" width="15px" height="15px" border="0">
				 <input type=button name=Detach value="DETACH" class="RSLButton" style='color:red;visibility:hidden' onclick="DETACHITEM()">
				</TD>
		</TR>
		<TR> 
		  	
      <TD colspan=3> Click here to redirect page after submission : 
        <input type="checkbox" name="redir" value="1">
			</TD>
			<td>			<DIV ID=STATUS_DIV STYLE='VISIBILITY:HIDDEN'><FONT STYLE='FONT-SIZE:13PX'><B>&nbsp;</B></FONT></DIV></td>
		</TR>
		<TR>
			<TD colspan=5 height=200px VALIGN=TOP>	
				<DIV ID=ACCOUNT_DIV STYLE='BORDER:1PX SOLID #133E71;background-color:beige;OVERFLOW:auto;height:100px;height:100%' class="TA">
				</DIV>
			</TD>
		</TR>
</form>
    </TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_direct width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>
