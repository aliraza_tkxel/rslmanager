<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match	
	Dim TDFunc		   (6)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
	ColData(0)  = "ID|PP|80"
	SortASC(0) 	= "PP ASC"
	SortDESC(0) = "PP DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""
		
	ColData(1)  = "Date|sdate|80"
	SortASC(1) 	= "sdate ASC"
	SortDESC(1) = "sdate DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "N/L Account|d|250"
	SortASC(2) 	= "NL_ACCOUNT.[DESC] ASC"
	SortDESC(2) = "NL_ACCOUNT.[DESC] DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Value|v|90"
	SortASC(3) 	= "NL_PREPAYMENT.[VALUE] ASC"
	SortDESC(3) = "NL_PREPAYMENT.[VALUE] DESC"	
	TDSTUFF(3)  = " "" ALIGN=RIGHT"" "
	TDFunc(3) = "FormatCurrency(|)"		

	ColData(4)  = "No: Months|NO_MONTHS|50"
	SortASC(4) 	= "NL_PREPAYMENT.NO_MONTHS ASC"
	SortDESC(4) = "NL_PREPAYMENT.NO_MONTHS DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""
	
	ColData(5)  = "Monthly Amount|MONTH_AMT|80"
	SortASC(5) 	= "NL_PREPAYMENT.MONTH_AMT ASC"
	SortDESC(5) = " NL_PREPAYMENT.MONTH_AMT DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""
	
	ColData(6)  = "|EMPTY|80"
	SortASC(6)  = ""
	SortDESC(6) = ""
	TDSTUFF(6)  = " "" "" & rsSet(""EMPTY1"") & "" "" "
	TDFunc(6) = ""

	
	PageName = "Prepayment_dtl.asp"
	EmptyText = "No Relevant Prepayment List found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

	
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""PREPAYID"") &  "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	
	SQLCODE="SELECT  NL_ACCOUNT.[DESC] as d, NL_PREPAYMENT.[VALUE] as v, NL_PREPAYMENT.NO_MONTHS, NL_PREPAYMENT.MONTH_AMT,  " &_
	"convert(varchar(12),NL_PREPAYMENT.STARTDATE,103) as sdate,  "&_
	"NL_PREPAYMENT.PREPAYID, " &_
	" CASE WHEN no_montHs<>runcount THEN "&_
	"		' style=''background-color:#FFFFFF;color:#000000;cursor:hand'' onclick=PrePaymentAmend(' + CAST(NL_PREPAYMENT.PREPAYID AS VARCHAR) + ') ' " &_
	"      ELSE " &_
	"	'' " &_
	" END AS EMPTY1," &_	
	" CASE WHEN no_montHs<>runcount THEN "&_
	"		'[<font color=blue>AMEND</font>]' " &_
	"      ELSE " &_
	"	'' " &_
	" END AS " &_
 	"EMPTY,'<b>PP</b> '+ right('000000000'+cast(PREPAYID as varchar),7) as PP " &_
	"FROM    NL_PREPAYMENT INNER JOIN  " &_
	"NL_ACCOUNT ON NL_PREPAYMENT.NOMINALACC = NL_ACCOUNT.ACCOUNTID " &_
	" ORDER BY "& orderBy

	
	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	'OpenDB()
	
	'Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "NL_ACCOUNT", "ACCOUNTID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	'CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<!-- #BeginEditable "doctitle" -->
<TITLE>RSL Manager Finance --> Prepayment List</TITLE>
<!-- #EndEditable -->
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array()

function PrePaymentAmend (PREPAYID){
	event.cancelBubble=true
	location.href = "/accounts/Prepayment.asp?ID=" + PREPAYID
	}


function SubmitPage()
	{
		

	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value  ;
	}

function load_me(txnid)
	{
	
	window.event.cancelBubble=true
	window.showModelessDialog("PrepaymentPerMonth.asp?txnid=" + txnid  + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 550px; status: No; resizable: No;")
	
	
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="/Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>