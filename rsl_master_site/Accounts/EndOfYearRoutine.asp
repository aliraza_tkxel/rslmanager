<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	
    CompanyFilterCREDIT = ""
    CompanyFilterDEBIT = ""
    rqCompany = Request("COMPANY")
	if (rqCompany <> "") then
		CompanyFilterCREDIT = " and isnull(JECL.COMPANYID,1) = '" & rqCompany & "' "
        CompanyFilterDEBIT = " and isnull(JEDL.COMPANYID,1) = '" & rqCompany & "' "
	end if
    
    Call OpenDB()
	
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:200px' onchange='FilterReport()' ")	

	Call CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Accounts </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function GO(){
	SubmitDiv.style.display = "block"
	thisform.btn_123456789.disabled = true;
	thisform.submit()
    }

   
    function FilterReport() {
 
        location.href = "EndOfYearRoutine.ASP?Company=" + document.getElementById("sel_COMPANY").value;
    }


function PopLedgerDetails(FY, TXNID, ACCID){
    window.open("NominalLedger/EndOfYearLedgerItem.asp?LID=" + ACCID + "&TXNID=" + TXNID + "&FY=" + FY + "&Company=<%=rqCompany%>"  + "&Random=" + new Date(), "_blank", "TOP=100, LEFT=200, scrollbars=yes, height=590, width=790, status=YES, resizable= Yes")		
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<div id="SubmitDiv" style='display:none;padding:10px;text-align:center;font-size:18px;background-color:white;border:1px dotted black;position:absolute;top:250;left:250;width:250;height:50;z-Index:30'><b>Please be patient...</b></div>
<form name="thisform" method="post" action="Serverside/EndOfYearRoutine_svr.asp?Company=<%=rqCompany%>">
<BR>	
<%
if (Session("EndOfYearRoutineFailure") <> "") then
	Response.Write "<b>Server Processing Error:</b><br> <font color=red>" & Session("EndOfYearRoutineFailure") & "</font><BR><BR>"
	Session("EndOfYearRoutineFailure") = ""
end if
%>
The table that follows shows summary information for any income and expense year end routines which have been ran. To get detailed information of the transaction click on the respective line. To run the routine for last year click on the 'GO' button below. <br><br><b>Note:</b> This may not be possible if you are still in the same financial year as the year the routine is to be run for.
<BR><BR>
Company : <%=lstCompany %>
    <BR><BR>
<TABLE border=1 style='border-collapse:collapse; behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor=STEELBLUE cellpadding=2>
<thead>
<TR style='background-color:#133e71;color:white;font-weight:bold'><TD width=60>TXNID</TD><TD width=90>DATE</TD><TD WIDTH=250>DESCRIPTION</TD><TD width=110 align=center>DEBIT</TD><TD width=110 align=center>CREDIT</TD></TR>
</thead><tbody>
<%
SQL = "SELECT JECL.ACCOUNTID, JE.TRANSACTIONID, JECL.TXNID, JECL.TXNDATE, JECL.DESCRIPTION, 0 AS DEBIT, JECL.AMOUNT AS CREDIT FROM NL_JOURNALENTRY JE " &_ 
		"	INNER JOIN NL_JOURNALENTRYCREDITLINE JECL ON JECL.TXNID = JE.TXNID " & CompanyFilterCREDIT &_ 
		"WHERE JE.TRANSACTIONTYPE = 27 AND JECL.ACCOUNTID = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'RETAINED_EARNINGS_ACCOUNT" & rqCompany & "') " &_
		"UNION ALL " &_
		"SELECT JEDL.ACCOUNTID, JE.TRANSACTIONID, JEDL.TXNID, JEDL.TXNDATE, JEDL.DESCRIPTION, JEDL.AMOUNT AS DEBIT, 0 AS CREDIT FROM NL_JOURNALENTRY JE " &_
		"	INNER JOIN NL_JOURNALENTRYDEBITLINE JEDL ON JEDL.TXNID = JE.TXNID "  & CompanyFilterDEBIT &_
		"WHERE JE.TRANSACTIONTYPE = 27 AND JEDL.ACCOUNTID = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'RETAINED_EARNINGS_ACCOUNT" & rqCompany & "') " &_
		"ORDER BY JE.TRANSACTIONID ASC"
    Response.Write SQL
LastRunYear = -1
Call OpenDB()
Call OpenRs(rsYE, SQL)
RowCount = 0
while NOT rsYE.EOF
	Response.Write "<TR onclick=""PopLedgerDetails(" & rsYE("TRANSACTIONID") & "," & rsYE("TXNID") & ", " & rsYE("ACCOUNTID")& ")""><TD>" & rsYE("TXNID") & "</td><td>" & rsYE("TXNDATE") & "</td><td>" & rsYE("DESCRIPTION") & "</td><td align=right>" & FormatNumber(rsYE("DEBIT"),2) & "</td><td align=right>" & FormatNumber(rsYE("CREDIT"),2) & "</td></tr>"
	RowCount = RowCount + 1
	'THIS WILL BE THE LAST FINANCIAL YEAR THE PROCEDURE AS RUN FOR.
	LastRunYear = CLng(rsYE("TRANSACTIONID"))
	rsYE.moveNext
wend
Call CloseRs(rsYE)

if (RowCount = 0) then
	Response.Write "<TR><TD colspan=5 align=center>No Year End Routines Created</td></tr>"
end if

SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'CURRENT_INCOME_EXPENSE_YEAREND_ROUTINE" & rqCompany & "'"
Call OpenRs(rsT, SQL)
if (NOT rsT.EOF) then
	CurrentRunYear = CLng(rsT("DEFAULTVALUE"))
else
	CurrentRunYear = -1
end if
Call CloseRs(rsT)

'check whether the current run will be greater than the last run finacial year
if (CurrentRunYear > LastRunYear) then
	'validate the financial year and if the range exists for the id
	if (CurrentRunYear <> -1) then
		SQL = "SELECT * FROM F_FISCALYEARS WHERE YRANGE = " & CurrentRunYear
		Call OpenRs(rsFY, SQL)
		if (NOT rsFY.EOF) then
			StartDate = CDate(rsFY("YSTART"))
			EndDate = CDate(rsFY("YEND"))
		else
			CurrentRunYear = -1
		end if
		Call CloseRs(rsFY)
	end if
else
	CurrentRunYear = -2
end if

Call CloseDB()

'IF THE VARIABLE IS EQUAL TO -2 THEN THE CURRENT RUN FINANCIAL YEAR IS INCORRECT.
if CurrentRunYear = -2 then
	RunText = "The current financial year is not greater than the last financial year run of [" & LastRunYear & "]. Please contact the software vendor."
'IF THE VARIABLE IS EQUAL TO -1 THEN SOME ERROR HAS OCCURRED AND THE PROCEDURE CANNOT BE RUN
elseif CurrentRunYear = -1 then
	RunText = "Unable to establish which year, the year end routine should be run for. Please contact the software vendor."
'IF THE DATE IS GREATER THAN THE END DATE, THEN THE YEAR HAS ENDED AND THE ROUTINE CAN BE RUN.
elseif Date > EndDate then
	RunText = "Run the year end routine for income and expenses for the financial year " & FormatDateTime(StartDate,2) & " to " & FormatDateTime(EndDate,2) & " <input type=button name=""btn_123456789"" onclick=""GO()"" value="" GO "" class=""RSLButton""> "	
'OTHERWISE THE ROUTINE CANNOT BE RUN BECUASE THE YEAR END HAS NOT BEEN REACHED
else
	RunText = "The financial year from " & FormatDateTime(StartDate,2) & " to " & FormatDateTime(EndDate,2) & " is still current. Therefore the year end routine cannot be run."
end if
%>
</TBODY>
</TABLE>
<BR>
<%=RunText%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

