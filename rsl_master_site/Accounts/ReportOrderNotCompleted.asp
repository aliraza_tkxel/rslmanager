<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% Server.ScriptTimeout=300 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CONST_PAGESIZE
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance
	Dim viewItemSelectedAll,viewItemSelected1,viewItemSelected2,viewItemSelected3, StrSrch
	dim Cost, AllCount
	dim BollFlag, lstSuppliers, lstStatus, supplier, sstatus,Rule1_text,spotype,statusdisabled 
	BollFlag=false

	dim FY
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If RequesT("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("sel_SUPPLIER") = "" Then supplier = -1 Else supplier = Request("sel_SUPPLIER") End If
	If Request("sel_POTYPE") = "" Then 
	    spotype = -1 
	    statusdisabled="disabled"  
	Else 
	    spotype = Request("sel_POTYPE") 
	    if(spotype=2) Then
	        statusdisabled=""
	    else
	        statusdisabled="disabled"    
	    End if
	End If
	If Request("sel_STATUS") = "" Then 
	    sstatus = -1 
	Else 
	   if(statusdisabled="disabled") Then
	        sstatus = -1 
	     else
	        sstatus = Request("sel_STATUS")    
	    End if
	End If
	
	//response.write sel_FISCALYEAR
	if Request("sel_FISCALYEAR")="" then
		FY=0
		if Request("sel_PERIOD") ="" then
			FY=0
		else
			FY=cint(Request("sel_PERIOD"))
		end if
	else
		FY=cint(Request("sel_FISCALYEAR"))
	end if

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany = "") then
		rqCompany = 1
	end if

	'response.write FY
	Call OpenDB()
	Call BuildSelect(lstFY, "sel_FISCALYEAR", "F_FISCALYEARS where Yrange>5", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", FY, NULL, "textbox200", " tabindex=1 STYLE='WIDTH:170' ")			  
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "All", supplier, NULL, "textbox200", " style='width:240px'")	
	Call BuildSelect(lstPOTYPE, "sel_POTYPE", "F_POTYPE", "POTYPEID, POTYPENAME", "POTYPENAME ", "All", spotype, NULL, "textbox200", " style='width:170px' OnChange=""GetRepairStatus();"" ")	
	Call BuildSelect(lstStatus, "sel_STATUS", "C_STATUS WHERE ITEMID = 1", "ITEMSTATUSID, DESCRIPTION", "DESCRIPTION ", "All", sstatus, NULL, "textbox200", " style='width:210px' "+ statusdisabled)	
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:170px'")	
   
    call closeDB()
		
	PageName = "ReportOrderNotCompleted.asp"
	MaxRowSpan = 6
	OpenDB()
	get_tenant_balance()
	get_Sum()
	CloseDB()

	function get_sum()
		dim rsSet
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.ActiveConnection.CommandTimeout = 900

		strSQL="EXEC P_ORDERNOTCOMPLETD_REPORT 2," & FY & "," & supplier & "," & sstatus & "," & spotype & "," & rqCompany 
		'rw strSQL
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.CursorLocation = 3
		rsSet.Open()
		Cost=0
		if not rsSet.eof then
			Cost=rsSet("Cost")
			AllCount = rsSet("TheCount")
		end if
		rsSet.close()
		Set rsSet = Nothing
	end function

	Function get_tenant_balance()
	
			Dim orderBy, strSQL, rsSet, intRecord ,dbcmd, Icount
			'orderBy = "rslt.cname"
			'if (Request("CC_Sort") <> "") then orderBy = " " & Request("CC_Sort") end if
			intRecord = 0
			str_data = ""
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.ActiveConnection.CommandTimeout = 900
			strSQL="P_ORDERNOTCOMPLETD_REPORT  1," & FY & "," & supplier & "," & sstatus & "," & spotype & "," & rqCompany 
			'rw strSQL
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			'intPageCount = Icount
			intPageCount= rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			
			
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			
			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
			
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then

				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			
			If intRecordCount > 0 Then
				' Display the record that you are starting on and the record
				' that you are finishing on for this page by writing out the
				' values in the intStart and the intFinish variables.
				str_data = str_data & "<TBODY CLASS='CAPS'>"
					' Iterate through the recordset until we reach the end of the page
					' or the last record in the recordset.
					For intRecord = 1 to rsSet.PageSize
						

						'	str_data = str_data & "<TR>" &_
						'		"<TD>" & rsSet("PIDATE") & "</TD>" &_
						'		"<TD>" & rsSet("ITEMNAME") & "</TD>" &_
						'		"<TD>" & rsSet("PO") & "</TD>" &_
						'		"<TD ALIGN=CENTER>" & rsSet("PITYPE") & "</TD>" &_
						'		"<TD align=left> " & rsSet("Supplier") & "</TD>" &_
						'		"<TD align=left> " & rsSet("status") & "</TD>" &_
						'		"<TD align=Right>" &  rsSet("GROSS_COST") & "</TD>" &_
						'		"<TD align=right>" & rsSet("OrderCost") & "</TD>" &_
						'		"<TD align=centeR>" & rsSet("chkBox") & "</TD>" &_									
						'		"</TR>"

						RowType = rsSet("ORDERITEMID")
						if (RowType = -1) then
							str_data = str_data & "<TR style='background-color:silver;color:#133e71;line-height:18px'>" &_
								"<TD>" & rsSet("PO") & "</TD>" &_
								"<TD COLSPAN=2>" & rsSet("Supplier") & "</td>" &_
								"<TD>" & rsSet("POTYPE") & "</TD>" &_
								"<TD align=right><b>" & FormatNumber(rsSet("OrderCost"),2) & "</b></TD>" &_
								"<TD align=center>&nbsp;</TD>" &_									
								"</TR>"
						else
						
						RepairStatus = rsSet("STATUS")
						' RULE AROUND YEAR END 
						' IF A REPAIR THAT HAS A STATUS OF 'IN PROGRESS' HAS A COMPLETION ENTERED IN THE NEXT FINANCIAL YEAR BUT WITH
    					' A DATE FOR THE SELECTED FINANCIAL YEAR THEN WE FLAG THIS AS 'AWAITING APPROVAL' IN THE 'IN PROGRESS LIST'
					
						if (rsSet("COMPLETED_IN_NEXT_FISCAL_YEAR") = 1) then
						        RepairStatus = "<font color=blue>Awaiting Approval **</font>"
						        Rule1_text = "<tr><td colspan=2><font color=blue>** Repair that was 'In Progress' at end of selected fiscal year but completion date has been back dated.</font></td></tr>"
						end if 	
						' END RULE
							str_data = str_data & "<TR>" &_
								"<TD>&nbsp;&nbsp;" & rsSet("PIDATE") & "</TD>" &_
								"<TD>" & rsSet("TAXDATE") & "</TD>" &_								
								"<TD>" & rsSet("ITEMNAME") & "</TD>" &_
								"<TD>&nbsp;&nbsp;" & RepairStatus & "</TD>" &_
								"<TD align=Right>" &  FormatNumber(rsSet("ITEMCOST"),2) & "</TD>" &_
								"<TD align=center><INPUT TYPE=HIDDEN NAME='HID_" & rsSet("ORDERITEMID") & "' id='HID_" & rsSet("ORDERITEMID") & "'  VALUE='" & rsSet("ITEMCOST") & "'>" & rsSet("chkBox") & "</TD>" &_									
								"</TR>"						
						end if
						count = count + 1
						rsSet.movenext()
						If rsSet.EOF Then Exit for 

					Next
					str_data = str_data & "</TBODY>"
	
					'ensure table height is consistent with any amount of records
					fill_gaps()
					
					' links
					str_data = str_data &_
					"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & "  ALIGN=CENTER>" &_
					"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
					"<A HREF = '" & PageName & "?sel_STATUS="&REQUEST("sel_STATUS")&"&sel_SUPPLIER="&REQUEST("sel_SUPPLIER")&"&page=1&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & FY & "&sel_POTYPE=" & REQUEST("sel_POTYPE") &"'><b><font color=BLUE>First</font></b></a> "  &_
					"<A HREF = '" & PageName & "?sel_STATUS="&REQUEST("sel_STATUS")&"&sel_SUPPLIER="&REQUEST("sel_SUPPLIER")&"&page=" & prevpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & FY  & "&sel_POTYPE=" & REQUEST("sel_POTYPE") &"'><b><font color=BLUE>Prev</font></b></a>"  &_
					" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
					" <A HREF='" & PageName & "?sel_STATUS="&REQUEST("sel_STATUS")&"&sel_SUPPLIER="&REQUEST("sel_SUPPLIER")&"&page=" & nextpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & FY & "&sel_POTYPE=" & REQUEST("sel_POTYPE") &"'><b><font color=BLUE>Next</font></b></a>"  &_ 
					" <A HREF='" & PageName & "?sel_STATUS="&REQUEST("sel_STATUS")&"&sel_SUPPLIER="&REQUEST("sel_SUPPLIER")&"&page=" & intPageCount & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & FY  & "&sel_POTYPE=" & REQUEST("sel_POTYPE") &"'><b><font color=BLUE>Last</font></b></a>"  &_
					"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
					"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
					"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>No records found</TD></TR>" 
				count = 1
				fill_gaps()
			End If
				
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Create Year End Accrual</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function FormatCurrencyComma(Figure){
		NewFigure = FormatCurrency(Math.abs(Figure))
		NegativeCurrency = false
		if (parseFloat(Figure,10) < 0) NegativeCurrency = true
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		if (NegativeCurrency) NewFigure = "-" + NewFigure.toString()
		return NewFigure
	}

	var FormFields = new Array()
	FormFields[0] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	function selectAllChecked()
	{
	var myCollection=document.getElementsByName("CHKORDERID") 
		checkedAllBox = RSLFORM.chkAll.checked
		for (i=0;i<myCollection.length;i++)
		{
			if( checkedAllBox) {
			myCollection[i].checked =true;
			}			
			else{
			myCollection[i].checked =false; 			
				}
		}
	}
	
	function CreateAccrul(){
	
		var chkSelCount=0
		var TotalCheckValue = 0
		checkedAllBox = false;
		var myCollection=document.getElementsByName("CHKORDERID") 
		checkedAllBox = RSLFORM.chkAll.checked
		for (i=0;i<myCollection.length;i++)
		{
			if(myCollection[i].checked ==true || checkedAllBox) {
				TotalCheckValue += parseFloat(document.getElementById("HID_" + myCollection[i].value).value)
					if (!checkedAllBox)
					{
					chkSelCount=chkSelCount+1
					}
				}
		}
		TotalCheckValue = "�" + FormatCurrencyComma(TotalCheckValue)
		
		
		if (checkedAllBox && chkSelCount != 0){
			alert("You have individual items checked plus the 'Accrue All Items' checkbox selected.\nYou can only do one action.\nPlease amend appropriately to continue.")
			return false;
			}
		if (!checkedAllBox && chkSelCount==0)
		{
			alert("Please select at least one item before creating an accrual.")
			return false
		}

		if (checkedAllBox){
			if (RSLFORM.AllCount.value == 0){
				alert("There are no items available to accrue for the selected filters.")
				return false;
				}
			answer = confirm("You are about to do an 'All Item Accrual' for:\n\nPeriod: " + RSLFORM.sel_FISCALYEAR.options[RSLFORM.sel_FISCALYEAR.selectedIndex].text + ".\nSupplier: " + RSLFORM.sel_SUPPLIER.options[RSLFORM.sel_SUPPLIER.selectedIndex].text + ".\nStatus: " + RSLFORM.sel_STATUS.options[RSLFORM.sel_STATUS.selectedIndex].text + ".\n\nDo you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
			if (!answer) return false
			TotalCheckValue = RSLFORM.AllItemValue.value;
			}
		else {
			answer = confirm("You are about to do an individual item accrual for [" + chkSelCount + "] items.\n\nDo you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
			if (!answer) return false
			}
			
		answer = confirm("The total value of the accrual will be:\n\n ACCRUAL VALUE " + TotalCheckValue + ".\n\nDo you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
		if (!answer) return false
		
		RSLFORM.action="ServerSide/AutomaticAccrulsCreation.asp"
		RSLFORM.submit()
	}
	
	function JumpPage(){
		
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
            location.href = "ReportOrderNotCompleted.asp?page=" + iPage + "&sel_PERIOD=" + document.getElementById("sel_FISCALYEAR").value + "&sel_SUPPLIER=" + document.getElementById("sel_SUPPLIER").value + "&sel_POTYPE=" + document.getElementById("sel_POTYPE").value + "&sel_STATUS=" + document.getElementById("sel_STATUS").value + "&company="+document.getElementById("sel_COMPANY").value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function resubmit(){
		
		if (!checkForm()) return false;
        location.href = "ReportOrderNotCompleted.asp?page=1&sel_PERIOD=" + document.getElementById("sel_FISCALYEAR").value + "&sel_SUPPLIER=" + document.getElementById("sel_SUPPLIER").value + "&sel_POTYPE=" + document.getElementById("sel_POTYPE").value + "&sel_STATUS=" + document.getElementById("sel_STATUS").value + "&company=" + document.getElementById("sel_COMPANY").value +"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value
	}

	function openHelp(){
	
		window.open("popups/yearendaccrualhelp.asp", "","height=473,width=600");
	
	}
	function GetRepairStatus(){
	    if(!(document.RSLFORM.sel_POTYPE.options[document.RSLFORM.sel_POTYPE.selectedIndex].value==2)) 
	    {
	         document.RSLFORM.sel_STATUS.disabled=true;
	         document.RSLFORM.sel_STATUS.selectedIndex=0;        
	    }
	    else    
	        document.RSLFORM.sel_STATUS.disabled=false;
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name="RSLFORM" method="post">
	<table width=750 CELLPADDING=1 CELLSPACING=2 BORDER=0>
		<tr>
			<td><span style="font-weight:bold;padding-right:2px;" >Financial Year :</span><%=lstFY%></td>
			<td></td>
			<td >
			   
			</td>
		</tr>
        <tr>
			<td><span style="font-weight:bold;padding-right:30px;" >Company :</span><%=lstcOMPANY%></td>
			<td><span style="font-weight:bold;" >Supplier : </span><%=lstSuppliers%></td>
			<td >
			    <span style="font-weight:bold;padding-right:20px;" >Pagesize :</span>
			    <input type="text" class="textbox" id="txt_PAGESIZE" name="txt_PAGESIZE" maxlength="3" size="5" tabindex="3" value="<%=CONST_PAGESIZE%>"/>
			    <image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0" />
			</td>
		</tr>
		<tr>
		    <td><span style="font-weight:bold;padding-right:35px;" >PO Type :</span><%=lstPOTYPE%></td>
			<td><span style="font-weight:bold;" >Repair Status : </span><%=lstStatus%></td> 
			<td><b>Accrue All Items :</b><input type="checkbox" name="chkAll" value="1" onchange="selectAllChecked()"></td>
		</tr>
		<tr>
		    <td>&nbsp;</td>
			<td colspan="2"   align="right">
			    <input title='Refresh Accrual List' type="button" name="btn" value="Refresh" class="RSLButton" onclick="resubmit()"/>
			    &nbsp;
			    <input title='Open Help Page' type="button" name="btnHelp" value="Help" onclick="openHelp()" class="RSLButton"/>
			    &nbsp;
				<input title='Commit Accruals' type="button" name="createAccruls" value="CREATE" onclick="CreateAccrul()" class="RSLButton" style='BACKGROUND-COLOR:RED;COLOR:WHITE'/>
			</td>
		</tr>
		<%= Rule1_text%>

	</table>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=0 STYLE="BORDER-COLLAPSE:COLLAPSE" BORDER=1>
	<TR style='background-color:#133e71;color:white;line-height:18px;text-transform:uppercase'> 
		<TD width=120>&nbsp;<B>Order Date</B></TD>
		<TD width=100>&nbsp;<B>Tax Date</B></TD>		
		<TD WIDTH=300>&nbsp;<B>Item Name</B></TD>
		<TD WIDTH=120PX ALIGN=Left>&nbsp;<B>Status</B></TD>
		<TD WIDTH=100PX ALIGN=CENTER>&nbsp;<B>Item Cost</B></TD>
		<TD WIDTH=40PX >&nbsp;<B>Accrue</B></TD>
	</TR>
    <%=str_data%> 
    <TR style='background-color:#133e71;color:white;line-height:18px'>
     <TD COLSPAN=4 ALIGN=RIGHT bgcolor="" ><b>GROSS TOTAL </b></TD>
     <TD ALIGN=RIGHT><b><% =formatcurrency(Cost)%></b></TD><TD></TD>
	</TR>
  </TABLE>
  <input type="hidden" value="<%= formatcurrency(Cost)%>" name="AllItemValue">
  <input type="hidden" value="<%=AllCount%>" name="AllCount">
  <input type="hidden" value="<%= FY%>" name="Yrange">
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
						