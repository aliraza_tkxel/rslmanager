<%
	'INTODUCED THE YRANGE VARIABLE, SO THAT THE OPENING BALANCE FOR THE RESPECTIVE 
	'FISCAL YEAR IS RETURNED
	Function get_opening_balance(ACCID, YR, Company)

		Call get_lastyearend()
		
		if (Cint(YR) > Cint(LYE_FY)) then
			SQL_STARTDATE = LYE_STARTDATE
			SQL_ENDDATE = DateAdd("d", -1, PS)
			SQL = "SELECT (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0))  AS BALANCE " &_
					 "FROM NL_ACCOUNT TOPLEVEL " &_
					 "	LEFT JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = TOPLEVEL.ACCOUNTTYPE " &_
					 "	LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&SQL_ENDDATE&" 23:59' AND isnull(COMPANYID,1) = " & Company & " GROUP BY ACCOUNTID) DBNOCHILD " &_
					 "		  ON DBNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
					 "	LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&SQL_ENDDATE&" 23:59' AND isnull(COMPANYID,1)  = " & Company & "  GROUP BY ACCOUNTID) CRNOCHILD " &_
					 " 		  ON CRNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
					 "	LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&SQL_ENDDATE&" 23:59' AND isnull(COMPANYID,1)  = " & Company & "  GROUP BY PARENTREFLISTID) DBCHILD " &_
					 "		  ON DBCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
					 "	LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&SQL_ENDDATE&" 23:59' AND isnull(COMPANYID,1)  = " & Company & "  GROUP BY PARENTREFLISTID) CRCHILD " &_
					 "		  ON CRCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
					 "WHERE	TOPLEVEL.ACCOUNTID = " & ACCID
			
		else
			' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
			SQL = 	"SELECT ISNULL(SUM(B.BALANCE),0) AS BALANCE " &_
					"FROM 	NL_ACCOUNT A " &_
					"		LEFT JOIN NL_OPENINGBALANCES B ON B.ACCOUNTID = A.ACCOUNTID " &_
					"WHERE	(A.ACCOUNTID = " & ACCID & " OR A.PARENTREFLISTID = " & ACCID & ") " &_
					"AND YRANGE = " & YR
		end if

		Call OpenRS(rsSetOB, SQL)
		If Not rsSetOB.EOF Then ob = rsSetOB("BALANCE") Else ob = 0 End If
		get_opening_balance = ob
		CloseRs(rsSetOB)
		
	End Function

	' IPAGE	:	PAGENUMBER
	' SD	:	STARTDATE OF SQL -- FOR PART MONTH CALLS 
	' ED	:	ENDDATE OF SQL
	' ACCID	:	ACCOUNTID OF REQUIRED NOMINAL
	' QUERYTYPE EITHER 1 = FULL YEAR VIEW OR 2 = MONTH VIEW
	' VERSION 2.0 REWRITE. PREVIOUS VERSION USED DOUBLE TOP WITH UNION ALL, 
	' BUT THE UNION WAS REMOVING ROWS. ALSO STANDARDZED SORT ORDER, SO THAT IT IS UNIQUE.
	Function get_balance(IPAGE, SD, ED, ACCID, PSIZE, QUERYTYPE, Company)
		
		Dim b
		if (iPage < 1) then iPage = 1
		TOP_SQL = " TOP " & (PSIZE * (iPage-1)) & " " 
		'Response.Write TOP_SQL
		' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
		SQL = "SELECT ISNULL(SUM(DEBIT) - SUM(CREDIT),0) AS BALANCE FROM ( " &_
					"SELECT " & TOP_SQL & " J.TXNDATE, DEBIT, CREDIT AS CREDIT FROM NL_JOURNALENTRY J " &_ 
					"LEFT JOIN NL_TRANSACTIONTYPE T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE " &_ 
					"INNER JOIN ( " &_ 
					"SELECT LINEID, 1 AS TABLEORDER, TXNID, ISNULL(AMOUNT,0) AS DEBIT, 0 AS CREDIT, NLA1.ACCOUNTID, NLA1.PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE JEDL " &_ 
					"	INNER JOIN NL_ACCOUNT NLA1 ON NLA1.ACCOUNTID = JEDL.ACCOUNTID AND (NLA1.ACCOUNTID = " & ACCID & " OR NLA1.PARENTREFLISTID = " & ACCID & ") AND isnull(COMPANYID,1)  = " & Company & "  " &_
					"UNION ALL " &_ 
					"SELECT LINEID, 0 AS TABLEORDER, TXNID, 0 AS DEBIT, ISNULL(AMOUNT,0) AS CREDIT, NLA2.ACCOUNTID, NLA2.PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE JECL " &_ 
					"	INNER JOIN NL_ACCOUNT NLA2 ON NLA2.ACCOUNTID = JECL.ACCOUNTID AND (NLA2.ACCOUNTID = " & ACCID & " OR NLA2.PARENTREFLISTID = " & ACCID & ") AND isnull(COMPANYID,1)  = " & Company & "  " &_ 
					") U ON J.TXNID = U.TXNID " &_
					"WHERE J.TXNDATE >= '" & SD & "' AND J.TXNDATE <= '" & ED & "' AND TRANSACTIONTYPE <> 9  AND isnull(j.COMPANYID,1)  = " & Company & " " &_
					"ORDER BY J.TXNDATE, J.TXNID, LINEID, TABLEORDER) DD"
		Call OpenRS(rsSetGB, SQL)
		If Not rsSetGB.EOF Then b = rsSetGB("BALANCE") Else b = 0 End If
		get_balance = b 	' BALANCE + OPENING BALANCE + PARTMONTH BALANCE
		CloseRs(rsSetGB)
		
	End Function
	
	' GET ACCOUNT DETAILS
	' REQUIRES VARIABLES ACCOUNTNUMBER, FULLNAME, ACCOUNTTYPE TO BE DECLARED ON CALLING PAGE
	Function get_account_details(ACCID)
	
		' GET ACCOUNT DETAILS
		SQL = "SELECT 	ISNULL(A.FULLNAME,'N/A') AS FULLNAME, " &_
				"		ISNULL(A.ACCOUNTNUMBER,'N/A') AS ACCOUNTNUMBER, " &_
				"		ISNULL(T.DESCRIPTION,'N/A') AS AC_TYPE " &_
				"FROM	NL_ACCOUNT A LEFT JOIN NL_ACCOUNTTYPE T ON A.ACCOUNTTYPE = T.ACCOUNTTYPEID " &_
				"WHERE	A.ACCOUNTID = " & ACCID

		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			AccountNumber = rsSet("ACCOUNTNUMBER")
			FullName = rsSet("FULLNAME")
			AccountType = rsSet("AC_TYPE")
		End If
		CloseRs(rsSet)
		
	End Function 
	
	' GET THE START AND END DATE FOR THE CURRENT FISCAL YEAR
	' MUST DECLARE STARTDATE
	' MUST DECLARE ENDDATE
	Function get_year(YR)

		if (YR = "" or isNull(YR)) then
			today = FormatDateTime(Date, 1)
			SQL = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
		else
			SQL = "SELECT YRange, YStart, YEnd FROM dbo.F_FiscalYears WHERE YRange = " & YR 
		end if

		Call OpenRs(rsFYear, SQL)
		if (NOT rsFYear.EOF) then
			FY = rsFYear("YRange")
			StartDate = rsFYear("YStart")
			EndDate = rsFYear("YEnd")		
		end if
		Call CloseRs(rsFYear)
	
		PS = CDate(FormatDateTime(StartDate,1))
		PE = CDate(FormatDateTime(Enddate,1))
	
	End function
	
	Function get_lastyearend()
		SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'NOMINALLASTOPENINGBALANCES'"
		Call OpenRs(rsLAST, SQL)
		if (NOT rsLAST.EOF) then
			LYE_FY = rsLAST("DEFAULTVALUE")
			SQL = "SELECT * FROM F_FISCALYEARS WHERE YRANGE = " & LYE_FY
			Call OpenRs(rsLYE, SQL)
			LYE_STARTDATE = CDate(FormatDateTime(rsLYE("YSTART"),1))
			LYE_ENDDATE = CDate(FormatDateTime(rsLYE("YEND"),1))
			Call CloseRs(rsLYE)
		else
			LYE_FY = -1
		end if
		Call CloseRs(rsLAST)
	End Function
	
	Function build_fiscal_month_select(ByRef lst, selectedMonth, isYTD, YR)
	
		'SQL = "SELECT * FROM F_FISCALYEARS WHERE YSTART <= '" & date & "' AND YEND >= '" & date & "'"
		SQL = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YRANGE = " & YR

		Call OpenRs(rsPeriod, SQL)
		PS = CDate(FormatDateTime(rsPeriod("YSTART"),1))
		PE = CDate(FormatDateTime(rsPeriod("YEND"),1))
		Call CloseRs(rsPeriod)

		YearStart = Year(PS)
		YearEnd = Year(PE)
		MonthStart = Month(PS)
		MonthEnd = Month(PE)
		
		'A MONTH COUNT OF THE TOTAL INCLUSIVE MONTHS BETWEEN THE PERIOD THAT IS CURRENTLY SELECTED.
		MonthC = DateDiff("m", CDate("1 " & MonthName(MonthStart) & " " & YearStart),  CDate("1 " & MonthName(MonthEnd) & " " & YearEnd)) + 1
		If cint(isYTD) = 1 Then lst = "<OPTION VALUE=''>Ytd</OPTION>" Else lst = "" End If 
		for i=1 to MonthC
			If cint(MonthStart) = cint(selectedMonth) then isSelected = "selected" Else isSelected = "" End If
			lst = lst & "<OPTION VALUE=""" & MonthStart & "_" & YearStart & """ "&isSelected&">" & MonthName(MonthStart) & " " & YearStart & "</OPTION>"
			MonthStart = MonthStart + 1
			if (MonthStart = 13) then
				MonthStart = 1
				YearStart = YearStart + 1
			end if
		next
	
	End Function

	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function

	Function stripHTML(strHTML)
	'Strips the HTML tags from strHTML
	
	  Dim objRegExp, strOutput
	  Set objRegExp = New Regexp
	
	  objRegExp.IgnoreCase = True
	  objRegExp.Global = True
	  objRegExp.Pattern = "<(.|\n)+?>"
	
	  'Replace all HTML tag matches with the empty string
	  strOutput = objRegExp.Replace(strHTML, "")
	  
	  'Replace all < and > with &lt; and &gt;
	  strOutput = Replace(strOutput, "<", "&lt;")
	  strOutput = Replace(strOutput, ">", "&gt;")
	  
	  stripHTML = strOutput    'Return the value of strOutput
	
	  Set objRegExp = Nothing
	End Function
    
    Function get_balance_byTXNID(IPAGE, SD, ED, ACCID, PSIZE, TXNID)
                                
                                Dim b
                                if (iPage < 1) then iPage = 1
                                TOP_SQL = (PSIZE * (iPage-1))
                                'Response.Write "<br/>TOP_SQL" & TOP_SQL & "<br/><br/>"
                                ' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
                                SQL = "EXEC NL_LEDGERDETAILS_ITEM_BALANCE " & TXNID & "," & TOP_SQL
                                Call OpenRS(rsSetGB, SQL)
                                If Not rsSetGB.EOF Then b = rsSetGB("BALANCE") Else b = 0 End If
                                get_balance_byTXNID = b           ' BALANCE + OPENING BALANCE + PARTMONTH BALANCE
                                Call CloseRs(rsSetGB)
                                
End Function
	
%>