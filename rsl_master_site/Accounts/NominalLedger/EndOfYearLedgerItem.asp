<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="INCLUDES/functions.asp" -->
<%
	'THIS IS DIFFERENT TO THE ORIGINAL LEDGER ITEM AS FOLLOWS:
	'NO BACK BUTTON AS IT IS SEPARATE FROM THE NOMINAL
	'THE ACCOUNTS ARE DISPLAYED INITIALLY INSTEAD OF THE DESCRIPTION.


	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance, FY
	Dim AccountNumber, FullName, AccountType, PS, PE, amitop
	Dim startdate, enddate, openingbalance, searchstring, searchstringSQL
	
	Call OpenDB()

	FY = Request("FY")
	get_year(FY)					' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	get_account_details(Request("LID"))		' GET THE ACCOUNT HEADER INFORMATION
	
	' BUILD PAGED REPORT
	Function get_item()
		
		TXNID = Request("TXNID")
		Dim strSQL, rsSet, intRecord 

		str_data = ""
		SQL = "SELECT J.TXNID, J.TXNDATE, T.DESCRIPTION, U.DEBIT, U.CREDIT, U.EDITSEQUENCE, U.DESCRIPTION AS ITEMDESC, U.MEMO, U.FULLNAME, U.ACCOUNTNUMBER " &_  
				"FROM NL_JOURNALENTRY J " &_ 
				"LEFT JOIN NL_TRANSACTIONTYPE T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE " &_ 
				"INNER JOIN ( " &_
				"	SELECT LINEID, 1 AS TABLEORDER, TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, EDITSEQUENCE, JEDL.DESCRIPTION, JEDL.MEMO, NLA1.FULLNAME, NLA1.ACCOUNTNUMBER " &_
				"		FROM NL_JOURNALENTRYDEBITLINE JEDL " &_ 
				"			INNER JOIN NL_ACCOUNT NLA1 ON NLA1.ACCOUNTID = JEDL.ACCOUNTID " &_ 
				"		WHERE JEDL.TXNID = " & TXNID & " " &_
				"	UNION ALL " &_ 
				"	SELECT LINEID, 0 AS TABLEORDER, TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, EDITSEQUENCE, JECL.DESCRIPTION, JECL.MEMO, NLA2.FULLNAME, NLA2.ACCOUNTNUMBER  " &_  
				"		FROM NL_JOURNALENTRYCREDITLINE JECL " &_ 
				"			INNER JOIN NL_ACCOUNT NLA2 ON NLA2.ACCOUNTID = JECL.ACCOUNTID " &_
				"		WHERE JECL.TXNID = " & TXNID & " " &_
				") U ON J.TXNID = U.TXNID " &_ 
				"WHERE J.TXNid = " & TXNID & " " &_
				"ORDER BY J.TXNDATE, J.TXNID, EDITSEQUENCE, TABLEORDER DESC"
		Set rsSet = Conn.Execute(SQL)
		count = 0	

		Response.Write "<TBODY CLASS='CAPS'>"
		credittotal = 0
		debittotal = 0
		while NOT rsSet.EOF
			ititle = stripHTML("A/C : " & rsSet("ACCOUNTNUMBER") & " " & rsSet("FULLNAME") & "&#13;" & rsSet("MEMO"))
			ititle2 = stripHTML(rsSet("ITEMDESC") & "&#13;" & rsSet("MEMO"))			
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
			str_data = "<TR VALIGN=TOP STYLE='CURSOR:HAND"&color&"'>" &_
				"<TD >&nbsp;" & rsSet("TXNDATE") & "</TD>" &_
				"<TD NOWRAP>" & rsSet("DESCRIPTION") & "</TD>" &_
				"<TD id=""Col1"" style='display:none' TITLE=""" & ititle & """>" & rsSet("ITEMDESC") & "</TD>" &_
				"<TD id=""Col2"" TITLE=""" & ititle2 & """><b>" & rsSet("ACCOUNTNUMBER") & "</b> " & rsSet("FULLNAME") & "</TD>" 				
			' SHOW DEBIT
			If rsSet("DEBIT") = 0 Then 
				str_data = str_data & "<TD NOWRAP></TD>" 
			Else
				debittotal = debittotal + cdbl(rsSet("DEBIT"))
				str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:RED'>"&FormatNumber(rsSet("DEBIT"),2)&"</TD>" 
			End If
			' SHOW CREDIT
			If rsSet("CREDIT") = 0 Then 
				str_data = str_data & "<TD NOWRAP></TD>" 
			Else
				credittotal = credittotal + cdbl(rsSet("CREDIT"))
				str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:BLUE'>"&FormatNumber(rsSet("CREDIT"),2)&"</TD>" 
			End If

			str_data = str_data & "</TR>"
			Response.Write str_data
			count = count + 1
			rsSet.movenext()
		wend

		if (count = 1) then
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if		
			Response.Write "<TR style='" & color & "'><TD COLSPAN=5 align=center><B>No journal entries found.</B></TD></TR>"
			count = count + 1 
		end if
		
		' END BALANCE
		Response.Write "<TR style='background-color:thistle'><TD align=right COLSPAN=3><B> TOTAL: </B></TD>" &_
		"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(debittotal,2),2)&"</B></TD><TD ALIGN=RIGHT><B>"&FormatNumber(Round(credittotal,2),2)&"</B></TD></TR></TBODY>" 
		
		rsSet.close()
		Set rsSet = Nothing
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt, innercount
		cnt = 0
		innercount = count
		tr_num = CONST_PAGESIZE - count
		while (cnt <= tr_num)
			if innercount mod 2 = 0 then color = "STYLE='background-color:beige'" else color = "" end if
			Response.Write "<TR "&color&"><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD>" &_
								  "<TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>"
			cnt = cnt + 1
			innercount = innercount + 1
		wend		
	End Function
	

%>
<html>
<head>
<title>Ledger Item - Transaction <%=Request("TXNID")%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function Swap(iCol){
	HideCols = "col2"
	ShowCols = "col1"
	if (iCol == 2) {
		HideCols = "col1"
		ShowCols = "col2"
		}
	ref = document.getElementsByName(HideCols)
	if (ref.length){
		for (i=0; i<ref.length; i++)
			ref[i].style.display = "none"
		}
	ref = document.getElementsByName(ShowCols)
	if (ref.length){
		for (i=0; i<ref.length; i++)
			ref[i].style.display = "block"
		}		
	}
		
</SCRIPT>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="white" text="#000000" onload="window.focus()" class='ta'>
<form name = RSLFORM method=post> 
<TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=1 align=center>
	<TR>
		<TD style='border:1px solid black;background-color:thistle;color:white;height:15' width=100 valign=middle><b>&nbsp;Account :</b></TD><TD valign=middle >&nbsp;<%=AccountNumber %> - <%=FullName%>&nbsp;[<font color=blue><%=AccountType%></font>]<BR></TD>
		<TD ALIGN=RIGHT rowspan=2 valign=top>
			<input type="BUTTON" name="btnClose" value="Close" class=rslbutton onclick="window.close()" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
		</TD>
	</TR>
	<TR>
		<TD style='border:1px solid black;background-color:thistle;color:white;height:15' width=100 valign=middle><b>&nbsp;Period :</b></TD><TD valign=middle>&nbsp;<%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD>
	</TR>
</TABLE>
<br>
<TABLE align=center WIDTH=750 CELLPADDING=1 CELLSPACING=0 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=thistle>
		<THEAD><TR STYLE='HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71'><TD COLSPAN=5 CLASS='TABLE_HEAD'></TD></TR>
		<TR> 
		<TD STYLE='WIDTH:80px' NOWRAP CLASS='TABLE_HEAD'>&nbsp;<B>Date</B> </TD>
		  <TD STYLE='WIDTH:35px' NOWRAP CLASS='TABLE_HEAD'> <B>Type</B> </TD>
		  <TD CLASS='TABLE_HEAD' WIDTH=460> <B><a href=# onclick="Swap(1)"><font color=blue style='text-decoration:none'>Description</font></a> / <a href=# onclick="Swap(2)"><font color=blue style='text-decoration:none'>Account</font></a></B> </TD>
		  <TD STYLE='WIDTH:90px' NOWRAP CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Debit</B> </TD>
		  <TD STYLE='WIDTH:90px' NOWRAP CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Credit</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
<%
get_item()
Call CloseDB()
%>
	</TABLE>
</FORM>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
