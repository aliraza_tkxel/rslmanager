<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/functions.asp" -->
<%
	StartZ = Timer 
	CONST NOTYEARTODATE = 1
	
	Dim STARTDATE, ENDDATE, PS, PE, FY, credit_total, debit_total, lstBoxFrom, lstBoxTo, selFrom, selTo
	Dim strHousing, strOther, strCurrent, strLiabilities, pl_period, pl_ytd, strCreditor, 	creditors, ytd_creditors
	Dim total_net_equity, net_fixed_assets, current_assets, current_liabilites, operating_surplus
	Dim ytd_total_net_equity, ytd_net_fixed_assets, ytd_current_assets, ytd_current_liabilites, strRest
	Dim BalanceSheet(120,8)
	Dim ArrayBluePrint(20)
	Dim ArrayBluePrintStart(20)
	Dim ArrayBluePrintEnd(20)
	Dim YearRange, HeadingCounter		
	
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
	Call OpenDB()
	Call get_year(Request("FY"))
	Call get_lastyearend()
	Call BuildSelect(lstFY, "FY", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", FY, NULL, "textbox200", " tabindex=1 onchange=""SubmitForm()"" ")	
	'LYE_FY - THIS CONTAINS THE LAST FISCAL YEAR FOR WHICH THEIR IS OPENING BALANCES
	'IF FY >= LYE_FY THEN USE THE STARTDATE OBTAINED FROM THIS OTHERWISE USE THE ORIGINAL START DATE
	if (CInt(FY) > Cint(LYE_FY)) then
		SQL_STARTDATE = LYE_STARTDATE
	Else
		SQL_STARTDATE = PS	
	End If
	
	ENDDATE = Request("TOMONTH")
	
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	If ENDDATE = "" Then 
		ENDDATE = PE
	Else
		TempArrayTo = Split(ENDDATE,"_")
		DmonthTo = TempArrayTo(0)
		DyearTo = TempArrayTo(1)
		ENDDATE = Cdate("1 " & MonthName(DmonthTo) & " " & DYearTo)
		ENDDATE = Cdate(LastDayOfMonth(ENDDATE) & " " & MonthName(DmonthTo) & " " & DYearTo)
	End If

	Call build_monster()
	
	Call build_fiscal_month_select(lstBoxTo, DmonthTo, NOTYEARTODATE, FY)
	Call build_str(strHousing, "Housing Properties", total_net_equity, ytd_total_net_equity)
	Call build_str(strOther, "Other Fixed Assets", net_fixed_assets, ytd_net_fixed_assets)
	Call build_str(strCurrent, "Current Assets", current_assets, ytd_current_assets)
	Call build_str(strLiabilities, "Current Liabilities", current_liabilites, ytd_current_liabilites)
	Call build_str(strRest, "Capital and Reserve", other, ytd_other)
	Call build_str(strCreditor, "Creditors: Due After 1 year", creditors, ytd_creditors)

	Call get_P_and_L()

	Function build_monster()
	
		SQL = "SELECT SIGNAGE, ACCOUNTNAME, SUM(PERIOD) AS PERIOD, SUM(YTD) AS YTD, SUBHEADING, HEADING, ORDERID FROM ( "&_
				"SELECT BS.SIGNAGE, BS.ACCOUNTNAME, " &_
				"		SUM(ISNULL(PERIOD_DB.DEBIT,0)) - SUM(ISNULL(PERIOD_CR.CREDIT,0)) PERIOD, " &_
				"		SUM(ISNULL(YTD_DB.DEBIT,0)) - SUM(ISNULL(YTD_CR.CREDIT,0)) YTD, " &_
				"		S.DESCRIPTION AS SUBHEADING, G.DESCRIPTION AS HEADING, " &_
				"		BS.ORDERID " &_
				"FROM	NL_BALANCESHEET BS " &_
				"		INNER JOIN  NL_ACCOUNT A ON A.ACCOUNTID = BS.ACCOUNTID " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM 	NL_JOURNALENTRYDEBITLINE D INNER JOIN NL_JOURNALENTRY J ON D.TXNID = J.TXNID  " &_
				"					WHERE D.TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND D.TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  PERIOD_DB  ON PERIOD_DB.ACCOUNTID = A.ACCOUNTID " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE C INNER JOIN NL_JOURNALENTRY J ON C.TXNID = J.TXNID " &_
				"					WHERE C.TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND C.TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  PERIOD_CR  ON PERIOD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYDEBITLINE "  &_
				"					WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  YTD_DB  ON YTD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE " &_
				"					WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  YTD_CR  ON YTD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		INNER JOIN NL_BALANCESHEETSUBGROUPING S ON BS.SUBGROUPID = S.SUBGROUPID " &_
				"		INNER JOIN NL_BALANCESHEETGROUPING G ON BS.GROUPID = G.GROUPID " &_
				"WHERE	BS.ACTIVE = 1 " &_
				"GROUP		BY BS.ORDERID, BS.ACCOUNTNAME, G.DESCRIPTION, S.DESCRIPTION, BS.SIGNAGE " &_
				"UNION ALL " &_
				"SELECT 	BS.SIGNAGE, BS.ACCOUNTNAME, " &_
				"		SUM(ISNULL(PERIOD_DB.DEBIT,0)) - SUM(ISNULL(PERIOD_CR.CREDIT,0)) PERIOD, " &_
				"		SUM(ISNULL(YTD_DB.DEBIT,0)) - SUM(ISNULL(YTD_CR.CREDIT,0)) YTD, " &_
				"		S.DESCRIPTION AS SUBHEADING, G.DESCRIPTION AS HEADING, " &_
				"		BS.ORDERID " &_
				"FROM	NL_BALANCESHEET BS " &_
				"		INNER JOIN  NL_ACCOUNT A ON A.PARENTREFLISTID = BS.ACCOUNTID " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM 	NL_JOURNALENTRYDEBITLINE D INNER JOIN NL_JOURNALENTRY J ON D.TXNID = J.TXNID  " &_
				"					WHERE D.TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND D.TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  PERIOD_DB  ON PERIOD_DB.ACCOUNTID = A.ACCOUNTID " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE C INNER JOIN NL_JOURNALENTRY J ON C.TXNID = J.TXNID " &_
				"					WHERE C.TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND C.TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  PERIOD_CR  ON PERIOD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYDEBITLINE "  &_
				"					WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  YTD_DB  ON YTD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE " &_
				"					WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' " &_
				"					GROUP BY ACCOUNTID)  YTD_CR  ON YTD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		INNER JOIN NL_BALANCESHEETSUBGROUPING S ON BS.SUBGROUPID = S.SUBGROUPID " &_
				"		INNER JOIN NL_BALANCESHEETGROUPING G ON BS.GROUPID = G.GROUPID " &_
				"WHERE	BS.ACTIVE = 1 " &_
				"GROUP		BY BS.ORDERID, BS.ACCOUNTNAME, G.DESCRIPTION, S.DESCRIPTION, BS.SIGNAGE " &_
				") T " &_
				"GROUP BY SIGNAGE, ACCOUNTNAME, SUBHEADING, HEADING, ORDERID " &_
				"ORDER BY ORDERID " 
				
				PrevHeading = ""
				RowCounter = 1
				HeadingCounter = 0
				Call OpenRs(rsBS, SQL)
				while NOT rsBS.EOF
					NewHeading = rsBS("HEADING")
					if (NewHeading <> PrevHeading) then
						PrevHeading = NewHeading
						if (HeadingCounter <> 0) then 
							ArrayBluePrintEnd(HeadingCounter) = RowCounter-1
						end if
						HeadingCounter = HeadingCounter + 1							
						ArrayBluePrint(HeadingCounter) = NewHeading
						ArrayBluePrintStart(HeadingCounter) = RowCounter
					end if
					BalanceSheet(RowCounter,1) = NewHeading
					BalanceSheet(RowCounter,2) = rsBS("SUBHEADING")
					BalanceSheet(RowCounter,3) = rsBS("SIGNAGE")
					BalanceSheet(RowCounter,4) = rsBS("ACCOUNTNAME")
					BalanceSheet(RowCounter,5) = rsBS("PERIOD")
					BalanceSheet(RowCounter,6) = rsBS("YTD")
					BalanceSheet(RowCounter,7) = rsBS("ORDERID")
'					BalanceSheet(RowCounter,8) = rsBS("OB")					
					
					RowCounter = RowCounter + 1																																			
					rsBS.moveNext()
				wend
				if (HeadingCounter <> 0) then 
					ArrayBluePrintEnd(HeadingCounter) = RowCounter-1
				end if
		End Function
	
	' THIS FUNCTION IS DISGUSTING I APOLOGISE IN ADVANCE, IT JUST GREW AND GREW AND GREW
	' THE REASON IS THE COMPLEXITIES INVOLVED IN THE CALCLUATIONS COS SOMETIMES INCOME/COSTS WILL BE REPRESENTED 
	' AS POSITIVE AND OTHER TIMES AS A NEGATIVE NUMBER ??????????????????????????????????????????????????? PP
	' There is no excuse, when you read this consider yourself sacked... Mark Carney
	Function build_str(ByRef str, grouping, ByRef total, ByRef ytd_total)
	
		Dim subgroup_total, ytd_subgroup_total, group_total, current_subgroup_heading, str_detail
		Dim current_group_heading
		subgroup_total = 0 
		ytd_subgroup_total = 0
		group_total = 0
		ytd_total = 0
		total = 0
		str = ""
		str_detail = ""
		current_group_heading = ""
		current_subgroup_heading = ""
		
		Matched = false
		For i=0 to HeadingCounter
			if grouping = ArrayBluePrint(i) then
				Matched = true
				StartLoopCounter = ArrayBluePrintStart(i)
				EndLoopCounter = ArrayBluePrintEnd(i)
				Exit For
			end if
		Next
		
		if Matched = true Then		
			current_subgroup_heading 	= BalanceSheet(StartLoopCounter, 2)
			current_group_heading 		=  BalanceSheet(StartLoopCounter, 1)
			str = str & "<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5><B><U>"&current_group_heading&"</U></B></span></TD></TR>"
		end If
		if Matched = true Then				
		For zC = StartLoopCounter to EndLoopCounter

			If current_subgroup_heading = BalanceSheet(zC, 2) Then		' IF SAME SUBGROUPING
			' WRITE HIDDEN DETAIL LINE AND UPDATE TOTALS
				subgroup_total = subgroup_total + BalanceSheet(zC, 5)
				ytd_subgroup_total = ytd_subgroup_total + BalanceSheet(zC, 6)
				ytd_group_total = ytd_group_total + BalanceSheet(zC, 6)

				str_detail = str_detail & "<TR STYLE='DISPLAY:NONE' id='"&BalanceSheet(zC, 2)&"'>" &_
							"<TD>&nbsp;&nbsp;&nbsp;<i>" & BalanceSheet(zC, 4) & "</I></TD>"
							if cdbl(BalanceSheet(zC, 5)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><i>" & FormatSign(FormatNumber(ABS(BalanceSheet(zC, 5)),2),BalanceSheet(zC, 5)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
							if cdbl(BalanceSheet(zC, 6)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><i>" & FormatSign(FormatNumber(ABS(BalanceSheet(zC, 6)),2),BalanceSheet(zC, 6)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
			Else															' IF NEW SUBGROUPING
				str = str & "<TR STYLE='CURSOR:HAND' ONCLICK=""toggle('"&current_subgroup_heading&"')"">" &_
							"<TD>&nbsp;" & current_subgroup_heading & "</TD>"
							if cdbl(subgroup_total) = 0 Then
								str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(subgroup_total),2),subgroup_total) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
							if cdbl(ytd_subgroup_total) = 0 Then
								str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>" & str_detail
							Else
								str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(ytd_subgroup_total),2),ytd_subgroup_total) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>" & str_detail
							End If
				str_detail = ""
				str_detail = str_detail & "<TR STYLE='DISPLAY:NONE' id='"&BalanceSheet(zC, 2)&"'>" &_
							"<TD>&nbsp;&nbsp;&nbsp;<i>" & BalanceSheet(zC, 4) & "</I></TD>" 
							if cdbl(BalanceSheet(zC, 5)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><I>" & FormatSign(FormatNumber(ABS(BalanceSheet(zC, 5)),2),BalanceSheet(zC, 5)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
							if cdbl(BalanceSheet(zC, 6)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><I>" & FormatSign(FormatNumber(ABS(BalanceSheet(zC, 6)),2),BalanceSheet(zC, 6)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
				subgroup_total = 0
				subgroup_total = subgroup_total + BalanceSheet(zC, 5)
				YTD_subgroup_total = 0
				ytd_subgroup_total = ytd_subgroup_total + BalanceSheet(zC, 6)
			End If
			If current_group_heading <> BalanceSheet(zC, 1) Then
				str = str & "<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5><B><U>"&BalanceSheet(zC, 1)&"</U></B></span></TD></TR>"
			eND iF
			current_subgroup_heading 	= BalanceSheet(zC, 2)
			current_group_heading 		= BalanceSheet(zC, 1)
			total = total + BalanceSheet(zC, 5)
			ytd_total = ytd_total + BalanceSheet(zC, 6)
			sign = BalanceSheet(zC, 3)
			
		Next
			' DISPLAY FINAL ROW IN SET COMPLETE WITH HIDDEN DETAIL ROWS
			str = str & "<TR STYLE='CURSOR:HAND' ONCLICK=""toggle('"&current_subgroup_heading&"')"">" &_
				"<TD>&nbsp;" & current_subgroup_heading & "</TD>"
				if CDBL(subgroup_total) = 0 Then
					str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
				Else
					str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(subgroup_total),2),subgroup_total) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
				End If
				if CDBL(ytd_subgroup_total) = 0 Then
					str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>" & str_detail
				Else
					str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(ytd_subgroup_total),2),ytd_subgroup_total) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>" & str_detail
				End If 
		End if
		
	
	End Function
	
	' 1 = negative , 2 = positive
	Function FormatSign(strNumber, intNumber)
	
		If IsNull(strNumber) Then
			FormatSign = "0"
			Exit Function
		Else
			If cdbl(intNumber) < 0 Then
				FormatSign = "(" & strNumber & ")"
				Exit Function
			ElseIf cdbl(intNumber) > 0 Then
				FormatSign = strNumber
				Exit Function
			Else
				If cint(strNumber) < 0 Then
					FormatSign = "(" & strNumber & ")"
					Exit Function
				Else
					FormatSign = strNumber
					Exit Function
				End If
			End If
			
		End If
		
	End Function
	
	' THIS FUNCTIOHN GETS THE VALUES FROM THE PROFIT AD LOSS PAGE AND HENCE SHOULD ALWAYS MATCH THAT VALUE
	Function get_P_and_L()
	
		SQL = "SELECT SUM(PERIOD) AS PERIOD, SUM(YTD) AS YTD FROM ( " &_
				"SELECT 	SUM(ISNULL(PERIOD_DB.DEBIT,0)) - SUM(ISNULL(PERIOD_CR.CREDIT,0)) PERIOD, " &_
				"		SUM(ISNULL(YTD_DB.DEBIT,0)) - SUM(ISNULL(YTD_CR.CREDIT,0)) YTD " &_
				"FROM	NL_PROFITANDLOSS PL " &_
				"		INNER JOIN  NL_ACCOUNT A ON A.ACCOUNTID = PL.ACCOUNTID " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYDEBITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  PERIOD_DB  ON PERIOD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYCREDITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  PERIOD_CR  ON PERIOD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYDEBITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59'  " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&PE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  YTD_DB  ON YTD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYCREDITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59'  " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&PE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  YTD_CR  ON YTD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"			INNER JOIN NL_PROFITANDLOSSSUBGROUPING S ON PL.SUBGROUPID = S.SUBGROUPID " &_
				"			INNER JOIN NL_PROFITANDLOSSGROUPING G ON PL.GROUPID = G.GROUPID " &_
				"WHERE	PL.ACTIVE = 1 " &_
				"UNION ALL " &_
				"SELECT 	SUM(ISNULL(PERIOD_DB.DEBIT,0)) - SUM(ISNULL(PERIOD_CR.CREDIT,0)) PERIOD, " &_
				"		SUM(ISNULL(YTD_DB.DEBIT,0)) - SUM(ISNULL(YTD_CR.CREDIT,0)) YTD " &_
				"FROM	NL_PROFITANDLOSS PL " &_
				"		INNER JOIN  NL_ACCOUNT A ON A.PARENTREFLISTID = PL.ACCOUNTID " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYDEBITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  PERIOD_DB  ON PERIOD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYCREDITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  PERIOD_CR  ON PERIOD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYDEBITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59'  " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&PE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  YTD_DB  ON YTD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"			FROM NL_JOURNALENTRYCREDITLINE " &_
				"			WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59'  " &_
				"			AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&PS&" 00:00' AND TXNDATE <= '"&PE&" 23:59' ) " &_								
				"			GROUP BY ACCOUNTID)  YTD_CR  ON YTD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"			INNER JOIN NL_PROFITANDLOSSSUBGROUPING S ON PL.SUBGROUPID = S.SUBGROUPID " &_
				"			INNER JOIN NL_PROFITANDLOSSGROUPING G ON PL.GROUPID = G.GROUPID " &_
				"WHERE	PL.ACTIVE = 1 " &_
				") T "				
				
		
		'rw SQL & "<br><BR>"
		Call OpenRs(rsSet, SQL)	
		if not rsSet.EOF Then 
			pl_period = rsSet(0)
			pl_ytd = rsSet(1)
		else
			pl_period = 0
			pl_ytd = 0
		End If	
		CloseRs(rsSet)
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<link rel="stylesheet" href="/js/Tree/dtree.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Tree/Tree_TwoCols.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<script type="text/javascript">
<!--
		
	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"

	function convertDate(strDate) {
		strDate = new String(strDate);
		arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		arrSplit = strDate.split("/");
		return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
		}

	function SubmitForm(){
		document.getElementById("BTN_GO").disabled = true
		THISFORM.submit()
		}
		
	function click_go(){
		location.href = "BALANCESHEET_body.ASP?FY=<%=FY%>&TOMONTH="+document.getElementById("selTo").value
		}

	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}

	// OPEN OR CLOSE ALL ROWS DEPENDENT ON STSATUS OF GLOBAL VARIABLES BELOW
	var current_rowStatus = new String("none")
	var next_rowStatus = new String("block")
	function toggleAll(){
		
		var coll = document.all.tags("TR");
			if (coll!=null)
				for (i=0; i<coll.length; i++) {
					if (coll[i].style.display == current_rowStatus)
						coll[i].style.display = next_rowStatus;
					}
		if (current_rowStatus == "none"){
			current_rowStatus = new String("block")
			next_rowStatus = new String("none")
			}
		else {
			current_rowStatus = new String("none")
			next_rowStatus = new String("block")
			}
	}
//-->
</script>
<STYLE TYPE="TEXT/CSS">
#TABLE1 TD {BORDER-BOTTOM:1PX DOUBLE SILVER	}
</STYLE>
<div style='position:absolute;top:60;left:430;z-Index:100'>
<%
Response.Write "Report Generated in : " & FormatNumber(Timer - StartZ,3) & " seconds"
%>
</div>
<TABLE CELLPADDING=1 CELLSPACING=2 ALIGN=CENTER>
	<TR><TD style='font-size:16px' ALIGN=CENTER><b><U>Balance Sheet</U></b></TD></TR>
	<TR><TD  ALIGN=CENTER><%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD></TR>
	<TR><TD  ALIGN=CENTER><%=now%></TD></TR>
	<TR style='height:8px'><TD></TD></TR>
</TABLE>
<TABLE align=CENTER WIDTH=95% CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:0PX SOLID BLACK;border-collapse:collapse' border=0>
<FORM NAME="THISFORM" METHOD="POST" ACTION="BalanceSheet.asp">
	<TR>
		<TD><SPAN STYLE='CURSOR:HAND' onclick='toggleAll()'><U><B>Open/Close</b></U></SPAN></TD>
		<TD ALIGN=RIGHT COLSPAN=4>
			<B>Period</b>&nbsp;&nbsp;&nbsp;
			<%=lstFY%>&nbsp;&nbsp;&nbsp;	
			<b>Month (To)</b>&nbsp;&nbsp;
			<SELECT NAME='selTo' class='textbox'><%=lstBoxTo%></SELECT>
			&nbsp;&nbsp;
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()">
		</TD>
	</TR>
</FORM>
</TABLE>
<TABLE ID="TABLE1" align=CENTER WIDTH=95% CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:0PX SOLID BLACK;border-collapse:collapse' border=0>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR>
			<TR> 
			<TD STYLE='WIDTH:200PX'>&nbsp;</TD>
		  	<TD ALIGN=RIGHT><B>Period</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
		  	<TD ALIGN=RIGHT><B>Year To Date</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
		</TR>
		<TR STYLE='HEIGHT:50PX'><TD COLSPAN=5 VALIGN=MIDDLE><span style='font-size:14px'><B><U>Assets</U></B></span></TD></TR>
		<%=strHousing%>
		<TR bgcolor='WHITESMOKE'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Total Net Equity</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(total_net_equity),2),total_net_equity)%></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(ytd_total_net_equity),2),ytd_total_net_equity)%></b></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<%=strOther%>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Net Fixed Assets</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(net_fixed_assets),2),net_fixed_assets) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_net_fixed_assets),2),ytd_net_fixed_assets) %></b></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<%=strCurrent%>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Current Assets</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(current_assets),2),current_assets) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_current_assets),2),ytd_current_assets) %></b></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<%=strLiabilities%>
		<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5></TD></TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Current Liabilities</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(current_liabilites),2),current_liabilites) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_current_liabilites),2),ytd_current_liabilites) %></b></TD>
		</TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Total Assets</B></TD>
			<% 
			   total_assets = total_net_equity + net_fixed_assets + current_assets + current_liabilites
			   'The mega bug in this page only took me three hours to discover. kind regards zanfar.
			   'ytd_total_assets = ytd_total_net_equity + ytd_net_fixed_assets + ytd_current_assets + current_liabilites
			   ytd_total_assets = ytd_total_net_equity + ytd_net_fixed_assets + ytd_current_assets + ytd_current_liabilites			   
			%>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(total_assets),2),total_assets) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_total_assets),2),ytd_total_assets) %></b></TD>
		</TR>
		<%=strCreditor%>
		<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5></TD></TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Creditors: Due After 1 year</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(creditors),2),creditors) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_creditors),2),ytd_creditors) %></b></TD>
		</TR>
		<%=strRest%>
		<TR>
			<TD>&nbsp;Surplus to Date</TD>
			<TD ALIGN=RIGHT><%=FormatSign(FormatNumber(ABS(pl_period),2),pl_period)%></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><%=FormatSign(FormatNumber(ABS(pl_ytd),2),pl_ytd)%></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
		</TR>
		<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5></TD></TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Capital and Reserve</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(other + pl_period),2),other + pl_period) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_other + pl_ytd),2),ytd_other + pl_ytd) %></b></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Total Liabilities and Capital</B></TD>
			<% 
			   total_lc = creditors + other + pl_period
			   ytd_total_lc = ytd_creditors + ytd_other + pl_ytd
			%>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ABS(total_lc),2),total_lc) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(abs(ytd_total_lc),2),ytd_total_lc) %></b></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR>
</TABLE>	
</BODY>
</HTML>

