<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/functions.asp" -->
<%
	StartZ = Timer 
	Dim STARTDATE, ENDDATE, FStart, Fend, credit_total, debit_total, lstBox, selValue
	Dim strIncome, strCosts, strLoans, strDepreciation, PE, PS, FY
	Dim operating_income_total, operating_costs_total, loan_interest_total, depreciation_total, operating_surplus
	Dim ytd_operating_income_total, ytd_operating_costs_total, ytd_loan_interest_total, ytd_depreciation_total
	Dim ProfitandLoss(320,8)
	Dim ArrayBluePrint(20)
	Dim ArrayBluePrintStart(20)
	Dim ArrayBluePrintEnd(20)
	Dim YearRange, HeadingCounter		

	Call OpenDB()
	selValue = Request("START")
	Call get_year(Request("FY"))
	Call BuildSelect(lstFY, "FY", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", FY, NULL, "textbox200", " tabindex=1 onchange=""SubmitForm()"" ")	

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = PS
		ENDDATE = PE
		FStart = PS
		FEnd  = PE
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		FStart = PS
		FEnd = ENDDATE
	End If

	Call build_fiscal_month_select(lstBox, Dmonth, 1, FY)

	Call build_monster()	

	Call build_str(strIncome, "Rental", operating_income_total, ytd_operating_income_total)
	Call build_str(strIncome, "Other Income", operating_income_total, ytd_operating_income_total)	
	Call build_str(strCosts, "Pay", operating_costs_total, ytd_operating_costs_total)
	Call build_str(strCosts, "Maintenance And Repairs", operating_costs_total, ytd_operating_costs_total)
	Call build_str(strCosts, "Other Estates Costs", operating_costs_total, ytd_operating_costs_total)
	Call build_str(strCosts, "Administration Costs", operating_costs_total, ytd_operating_costs_total)			
	Call build_str(strLoans, "Interest", loan_interest_total, ytd_loan_interest_total)
	Call build_str(strDepreciation, "Depreciation", depreciation_total, ytd_depreciation_total)

	Function build_monster()
	
			SQL = "SELECT SIGNAGE, ACCOUNTNAME, SUM(PERIOD) AS PERIOD, SUM(YTD) AS YTD, SUBHEADING, HEADING, ORDERID FROM ( " &_
				"SELECT 	PL.SIGNAGE, PL.ACCOUNTNAME, " &_
				"		SUM(ISNULL(PERIOD_DB.DEBIT,0)) - SUM(ISNULL(PERIOD_CR.CREDIT,0)) PERIOD, " &_
				"		SUM(ISNULL(YTD_DB.DEBIT,0)) - SUM(ISNULL(YTD_CR.CREDIT,0)) YTD, " &_
				"		S.DESCRIPTION AS SUBHEADING, " &_
				"		G.DESCRIPTION AS HEADING, PL.ORDERID " &_
				"FROM	NL_PROFITANDLOSS PL " &_
				"		INNER JOIN  NL_ACCOUNT A ON A.ACCOUNTID = PL.ACCOUNTID " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM 	NL_JOURNALENTRYDEBITLINE " &_
				"					WHERE TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  PERIOD_DB  ON PERIOD_DB.ACCOUNTID = A.ACCOUNTID " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE " &_
				"					WHERE TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  PERIOD_CR  ON PERIOD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYDEBITLINE "  &_
				"					WHERE TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  YTD_DB  ON YTD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE " &_
				"					WHERE TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  YTD_CR  ON YTD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		INNER JOIN NL_PROFITANDLOSSSUBGROUPING S ON PL.SUBGROUPID = S.SUBGROUPID " &_
				"		INNER JOIN NL_PROFITANDLOSSGROUPING G ON PL.GROUPID = G.GROUPID " &_
				"WHERE	PL.ACTIVE = 1 " &_
				"GROUP		BY PL.ORDERID, PL.ACCOUNTNAME, S.DESCRIPTION, G.DESCRIPTION, PL.SIGNAGE " &_
				"UNION ALL " &_
				"SELECT 	PL.SIGNAGE, PL.ACCOUNTNAME, " &_
				"		SUM(ISNULL(PERIOD_DB.DEBIT,0)) - SUM(ISNULL(PERIOD_CR.CREDIT,0)) PERIOD, " &_
				"		SUM(ISNULL(YTD_DB.DEBIT,0)) - SUM(ISNULL(YTD_CR.CREDIT,0)) YTD, " &_
				"		S.DESCRIPTION AS SUBHEADING, " &_
				"		G.DESCRIPTION AS HEADING, PL.ORDERID " &_
				"FROM	NL_PROFITANDLOSS PL " &_
				"		INNER JOIN  NL_ACCOUNT A ON A.PARENTREFLISTID = PL.ACCOUNTID " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM 	NL_JOURNALENTRYDEBITLINE " &_
				"					WHERE TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  PERIOD_DB  ON PERIOD_DB.ACCOUNTID = A.ACCOUNTID " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE " &_
				"					WHERE TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  PERIOD_CR  ON PERIOD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN  (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYDEBITLINE "  &_
				"					WHERE TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  YTD_DB  ON YTD_DB.ACCOUNTID = A.ACCOUNTID  " &_
				"		LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID " &_
				"					FROM NL_JOURNALENTRYCREDITLINE " &_
				"					WHERE TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' " &_
				"					AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"&FStart&" 00:00' AND TXNDATE <= '"&FEnd&" 23:59' ) " &_				
				"					GROUP BY ACCOUNTID)  YTD_CR  ON YTD_CR.ACCOUNTID = A.ACCOUNTID  " &_
				"		INNER JOIN NL_PROFITANDLOSSSUBGROUPING S ON PL.SUBGROUPID = S.SUBGROUPID " &_
				"		INNER JOIN NL_PROFITANDLOSSGROUPING G ON PL.GROUPID = G.GROUPID " &_
				"WHERE	PL.ACTIVE = 1 " &_
				"GROUP		BY PL.ORDERID, PL.ACCOUNTNAME, S.DESCRIPTION, G.DESCRIPTION, PL.SIGNAGE " &_
				") T " &_
				"GROUP BY SIGNAGE, ACCOUNTNAME, SUBHEADING, HEADING, ORDERID " &_
				"ORDER BY ORDERID "						

				PrevHeading = ""
				RowCounter = 1
				HeadingCounter = 0
				Call OpenRs(rsBS, SQL)
				while NOT rsBS.EOF
					NewHeading = rsBS("HEADING")
					if (NewHeading <> PrevHeading) then
						PrevHeading = NewHeading
						if (HeadingCounter <> 0) then 
							ArrayBluePrintEnd(HeadingCounter) = RowCounter-1
						end if
						HeadingCounter = HeadingCounter + 1							
						ArrayBluePrint(HeadingCounter) = NewHeading
						ArrayBluePrintStart(HeadingCounter) = RowCounter
					end if
					ProfitandLoss(RowCounter,1) = NewHeading
					ProfitandLoss(RowCounter,2) = rsBS("SUBHEADING")
					ProfitandLoss(RowCounter,3) = rsBS("SIGNAGE")
					ProfitandLoss(RowCounter,4) = rsBS("ACCOUNTNAME")
					ProfitandLoss(RowCounter,5) = rsBS("PERIOD")
					ProfitandLoss(RowCounter,6) = rsBS("YTD")
					ProfitandLoss(RowCounter,7) = rsBS("ORDERID")
					
					RowCounter = RowCounter + 1																																			
					rsBS.moveNext()
				wend
				if (HeadingCounter <> 0) then 
					ArrayBluePrintEnd(HeadingCounter) = RowCounter-1
				end if
		End Function

	' THIS FUNCTION IS DISGUSTING I APOLOGISE IN ADVANCE, IT JUST GREW AND GREW AND GREW
	' THE REASON IS THE COMPLEXITIES INVOLVED IN THE CALCLUATIONS COS SOMETIMES INCOME/COSTS WILL BE REPRESENTED 
	' AS POSITIVE AND OTHER TIMES AS A NEGATIVE NUMBER ??????????????????????????????????????????????????? PP
	Function build_str(ByRef str, grouping, ByRef total, ByRef ytd_total)
	
		Dim subgroup_total, group_total, current_subgroup_heading, str_detail
		Dim current_group_heading
		subgroup_total = 0 
		'group_total = 0
		'ytd_total = 0
		'total = 0
		'str = ""
		str_detail = ""
		current_group_heading = ""
		current_subgroup_heading = ""
		
		Matched = false
		For i=0 to HeadingCounter
			if grouping = ArrayBluePrint(i) then
				Matched = true
				StartLoopCounter = ArrayBluePrintStart(i)
				EndLoopCounter = ArrayBluePrintEnd(i)
				Exit For
			end if
		Next
		
		if Matched = true Then		
			current_subgroup_heading 	= ProfitandLoss(StartLoopCounter, 2)
			current_group_heading 		=  ProfitandLoss(StartLoopCounter, 1)
			str = str & "<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5><B><U>"&current_group_heading&"</U></B></span></TD></TR>"
		end If
		if Matched = true Then				
		For zC = StartLoopCounter to EndLoopCounter

			If current_subgroup_heading = ProfitandLoss(zC, 2) Then		' IF SAME SUBGROUPING
			' WRITE HIDDEN DETAIL LINE AND UPDATE TOTALS
				subgroup_total = subgroup_total + ProfitandLoss(zC, 5)
				group_total = group_total + ProfitandLoss(zC, 5)
				ytd_subgroup_total = ytd_subgroup_total + ProfitandLoss(zC, 6)
				ytd_group_total = ytd_group_total + ProfitandLoss(zC, 6)

				str_detail = str_detail & "<TR STYLE='DISPLAY:NONE' id='"&ProfitandLoss(zC, 2)&"'>" &_
							"<TD>&nbsp;&nbsp;&nbsp;<i>" & ProfitandLoss(zC, 4) & "</I></TD>"
							if cdbl(ProfitandLoss(zC, 5)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><i>" & FormatSign(FormatNumber(ABS(ProfitandLoss(zC, 5)),2),ProfitandLoss(zC, 3)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
							if cdbl(ProfitandLoss(zC, 6)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><i>" & FormatSign(FormatNumber(ABS(ProfitandLoss(zC, 6)),2),ProfitandLoss(zC, 3)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
			Else															' IF NEW SUBGROUPING
				str = str & "<TR STYLE='CURSOR:HAND' ONCLICK=""toggle('"&current_subgroup_heading&"')"">" &_
							"<TD>&nbsp;" & current_subgroup_heading & "</TD>"
							if cdbl(subgroup_total) = 0 Then
								str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(subgroup_total),2),sign) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
							if cdbl(ytd_subgroup_total) = 0 Then
								str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>" & str_detail
							Else
								str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(ytd_subgroup_total),2),sign) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>" & str_detail
							End If
				str_detail = ""
				str_detail = str_detail & "<TR STYLE='DISPLAY:NONE' id='"&ProfitandLoss(zC, 2)&"'>" &_
							"<TD>&nbsp;&nbsp;&nbsp;<i>" & ProfitandLoss(zC, 4) & "</I></TD>" 
							if cdbl(ProfitandLoss(zC, 5)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><I>" & FormatSign(FormatNumber(ABS(ProfitandLoss(zC, 5)),2),ProfitandLoss(zC, 3)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
							if cdbl(ProfitandLoss(zC, 6)) = 0 Then
								str_detail = str_detail & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							Else
								str_detail = str_detail & "<TD ALIGN=RIGHT><I>" & FormatSign(FormatNumber(ABS(ProfitandLoss(zC, 6)),2),ProfitandLoss(zC, 3)) & "</i></TD><TD ALIGN=RIGHT>&nbsp;</TD>"
							End If
				subgroup_total = 0
				subgroup_total = subgroup_total + ProfitandLoss(zC, 5)
				YTD_subgroup_total = 0
				ytd_subgroup_total = ytd_subgroup_total + ProfitandLoss(zC, 6)
			End If
			If current_group_heading <> ProfitandLoss(zC, 1) Then
				str = str & "<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5><B><U>"&ProfitandLoss(zC, 2)&"</U></B></span></TD></TR>"
			eND iF
			current_subgroup_heading 	= ProfitandLoss(zC, 2)
			current_group_heading 		= ProfitandLoss(zC, 1)
			total = total + ProfitandLoss(zC, 5)
			ytd_total = ytd_total + ProfitandLoss(zC, 6)
			sign = ProfitandLoss(zC, 3)
		Next
			' DISPLAY FINAL ROW IN SET COMPLETE WITH HIDDEN DETAIL ROWS
			str = str & "<TR STYLE='CURSOR:HAND' ONCLICK=""toggle('"&current_subgroup_heading&"')"">" &_
				"<TD>&nbsp;" & current_subgroup_heading & "</TD>"
				if cdbl(subgroup_total) = 0 Then
					str = str & "<TD ALIGN=RIGHT>-</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
				Else
					str = str & "<TD ALIGN=RIGHT>" & FormatSign(FormatNumber(ABS(subgroup_total),2),sign) & "</TD><TD ALIGN=RIGHT>&nbsp;</TD>"
				End If

				str = str & "<TD ALIGN=RIGHT>&nbsp;</TD><TD ALIGN=RIGHT>&nbsp;</TD></TR>" &	str_detail
		End if
	
	End Function
	
	' 1 = negative , 2 = positive
	Function FormatSign(strNumber, sign)
	
		If IsNull(strNumber) Then
			FormatSign = "0"
			Exit Function
		Else
			If cint(sign) = 1 Then
				FormatSign = "(" & strNumber & ")"
				Exit Function
			ElseIf cint(sign) = 2 Then
				FormatSign = strNumber
				Exit Function
			Else
				If cint(strNumber) < 0 Then
					FormatSign = "(" & strNumber & ")"
					Exit Function
				Else
					FormatSign = strNumber
					Exit Function
				End If
			End If
			
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<link rel="stylesheet" href="/js/Tree/dtree.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>

<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Tree/Tree_TwoCols.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF  MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<script type="text/javascript">
<!--
		
	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"

	function convertDate(strDate) {
		strDate = new String(strDate);
		arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		arrSplit = strDate.split("/");
		return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
		}

	function SubmitForm(){
		document.getElementById("BTN_GO").disabled = true
		THISFORM.submit()
		}
	
	function click_go(){
		location.href = "IncomeAndExpenditure_Body.asp?FY=<%=FY%>&START="+document.getElementById("selMonth").value
		}

	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}

	// OPEN OR CLOSE ALL ROWS DEPENDENT ON STSATUS OF GLOBAL VARIABLES BELOW
	var current_rowStatus = new String("none")
	var next_rowStatus = new String("block")
	function toggleAll(){
		
		var coll = document.all.tags("TR");
			if (coll!=null)
				for (i=0; i<coll.length; i++) {
					if (coll[i].style.display == current_rowStatus)
						coll[i].style.display = next_rowStatus;
					}
		if (current_rowStatus == "none"){
			current_rowStatus = new String("block")
			next_rowStatus = new String("none")
			}
		else {
			current_rowStatus = new String("none")
			next_rowStatus = new String("block")
			}
	}

//-->
</script>
<STYLE TYPE="TEXT/CSS">
#TABLE1 TD {BORDER-BOTTOM:1PX DOUBLE SILVER	}
</STYLE>
<div style='position:absolute;top:80;left:430;z-Index:100'>
<%
Response.Write "Report Generated in : " & FormatNumber(Timer - StartZ,3) & " seconds"
%>
</div><br><br>
<TABLE CELLPADDING=1 CELLSPACING=2 ALIGN=CENTER>
	<TR><TD style='font-size:16px' ALIGN=CENTER><b><U>Operating Income and Costs</U></b></TD></TR>
	<TR><TD  ALIGN=CENTER><%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD></TR>
	<TR><TD  ALIGN=CENTER><%=now%></TD></TR>
	<TR style='height:8px'><TD></TD></TR>
</TABLE>
<TABLE align=CENTER WIDTH=95% CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:0PX SOLID BLACK;border-collapse:collapse' border=0>
<FORM NAME="THISFORM" METHOD="POST" ACTION="incomeandexpenditure_body.asp">
	<TR>
		<TD><SPAN STYLE='CURSOR:HAND' onclick='toggleAll()'><U><B>Open/Close</b></U></SPAN></TD>
		<TD ALIGN=RIGHT>
			<B>Period</b>&nbsp;&nbsp;&nbsp;
			<%=lstFY%>&nbsp;&nbsp;&nbsp;			
			<B>Month</b>&nbsp;&nbsp;&nbsp;
			<SELECT NAME='selMonth' class='textbox' value="<%=selValue%>"><%=lstBox%></SELECT>
			&nbsp;&nbsp;	
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()">
		</TD>
	</TR>
</FORM>
</TABLE>
<TABLE ID="TABLE1" align=CENTER WIDTH=95% CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:0PX SOLID BLACK;border-collapse:collapse' border=0>
		<THEAD>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR>
			<TR> 
			<TD STYLE='WIDTH:150PX'>&nbsp;</TD>
		  	<TD ALIGN=RIGHT><B>Period</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
		  	<TD ALIGN=RIGHT><B>Year To Date</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
		</TR>
		<TR STYLE='HEIGHT:50PX'><TD COLSPAN=5 VALIGN=MIDDLE><span style='font-size:14px'><B><U>Income</U></B></span></TD></TR>
		<%=strIncome%>
		<TR bgcolor='WHITESMOKE'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Operating Income</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<% If cdbl(operating_income_total) > 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(operating_income_total))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(operating_income_total))%></b></TD>
			<% End If %>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<% If cdbl(ytd_operating_income_total) > 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(ytd_operating_income_total))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(ytd_operating_income_total))%></b></TD>
			<% End If %>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<TR STYLE='HEIGHT:50PX'><TD COLSPAN=5 VALIGN=MIDDLE><span style='font-size:14px'><B><U>Costs</U></B></span></TD></TR>
		<%=strCosts%>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Operating Costs</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(operating_costs_total),1) %></b></TD>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<TD ALIGN=RIGHT><b><%=FormatSign(FormatNumber(ytd_operating_costs_total),1) %></b></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Operating Surplus</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<%   operating_surplus = abs(operating_income_total)-operating_costs_total
				If cdbl(operating_surplus) < 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(operating_surplus))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(operating_surplus))%></b></TD>
			<% End If %>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<%   ytd_operating_surplus = abs(ytd_operating_income_total)-ytd_operating_costs_total
				If cdbl(ytd_operating_surplus) < 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(ytd_operating_surplus))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(ytd_operating_surplus))%></b></TD>
			<% End If %>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<TR STYLE='HEIGHT:50PX'><TD COLSPAN=5 VALIGN=MIDDLE><span style='font-size:14px'><B><U>Interest</U></B></span></TD></TR>
		<%=strLoans%>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Net Surplus</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<%   net_surplus = abs(operating_surplus)-loan_interest_total
				If cdbl(net_surplus) < 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(net_surplus))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(net_surplus))%></b></TD>
			<% End If %>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<%   ytd_net_surplus = abs(ytd_operating_surplus)-ytd_loan_interest_total
				If cdbl(ytd_net_surplus) < 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(ytd_net_surplus))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(ytd_net_surplus))%></b></TD>
			<% End If %>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
		<TR STYLE='HEIGHT:50PX'><TD COLSPAN=5 VALIGN=MIDDLE><span style='font-size:14px'><B><U>Depreciation</U></B></span></TD></TR>
		<%=strDepreciation%>
		<TR STYLE='HEIGHT:30PX'><TD COLSPAN=5></TD></TR>
		<TR bgcolor='whitesmoke'>
			<TD VALIGN=MIDDLE ALIGN=RIGHT><B>Revenue Surplus</B></TD>
			<TD ALIGN=RIGHT>&nbsp;</TD>
			<%   revenue_surplus = net_surplus- abs(depreciation_total)
			If cdbl(revenue_surplus) < 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(revenue_surplus))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(revenue_surplus))%></b></TD>
			<% End If %>
		  	<TD ALIGN=RIGHT>&nbsp;</TD>
			<%   ytd_revenue_surplus = ytd_net_surplus- abs(ytd_depreciation_total)
			If cdbl(ytd_revenue_surplus) < 0 Then %>
				<TD ALIGN=RIGHT>(<b><%=FormatNumber(abs(ytd_revenue_surplus))%></b>)</TD>
			<% Else %>
				<TD ALIGN=RIGHT><b><%=FormatNumber(abs(ytd_revenue_surplus))%></b></TD>
			<% End If %>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>

</TABLE>	
</BODY>
</HTML>

