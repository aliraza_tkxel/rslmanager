<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/Includes/Functions/URLQueryReturn.asp" -->
<%
	Dim yRaange
	set rsFYear = Server.CreateObject("ADODB.Recordset")
	    rsFYear.ActiveConnection = RSL_CONNECTION_STRING
	    If (Request("sel_PERIOD") = "") Then
		    rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YSTART <= GETDATE() AND YEND >= GETDATE()"
	    Else
		    yRaange=Request("sel_PERIOD")
		    rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YRANGE = " & REQUEST("sel_PERIOD")
	    End If
	rsFYear.CursorType = 0
	rsFYear.CursorLocation = 2
	rsFYear.LockType = 3
	rsFYear.Open()
	If (NOT rsFYear.EOF) Then
		YearRange = rsFYear("YRange")
		StartDate = rsFYear("YStart")
		EndDate = rsFYear("YEnd")
	End If
	rsFYear.close

	PS = CDate(FormatDateTime(StartDate,1))
	PE = CDate(FormatDateTime(Enddate,1))


	Dim CONST_PAGESIZE

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance
	Dim viewItemSelectedAll,viewItemSelected1,viewItemSelected2,viewItemSelected3, StrSrch
	dim InvoiveAmtSum,paymentSum, OpeningBalanceSum, outstandingSum
	dim BollFlag
	BollFlag = false
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	viewItemSelectedAll=""
	viewItemSelected1=""
	viewItemSelected2=""
	viewItemSelected3=""


	If Request("SEARCHNAME") = "" Then
		StrSrch = 0
	Else
		StrSrch = Request("SEARCHNAME")
	End If
	If StrSrch = 0 Then
		viewItemSelectedAll = "SELECTED"
	End If

	If StrSrch = 1 Then
		viewItemSelected1 = "SELECTED"
	End If
	If StrSrch = 2 Then
		viewItemSelected2 = "SELECTED"
	End If
	If StrSrch = 3 Then
		viewItemSelected3 = "SELECTED"
	End If

	PageName = "AgedDebtors.asp"
	MaxRowSpan = 6

	Call OpenDB()
	Call get_tenant_balance()
	Call get_Sum()
	Call CloseDB()

	Function get_sum()
        strSQL="exec f_get_agedbalance_report 1," & YearRange
		Dim rsSet
		Set rsSet = Server.CreateObject("ADODB.Recordset")
		    rsSet.ActiveConnection = RSL_CONNECTION_STRING
		    rsSet.Source = strSQL
		    rsSet.CursorType = 2
		    rsSet.CursorLocation = 3
		    rsSet.Open()
		    If Not rsSet.eof Then
			    InvoiveAmtSum = rsSet("InvoiveAmtSum")
			    paymentSum = rsSet("paymentSum")
			    OpeningBalanceSum = rsSet("OpeningBalanceSum")
			    outstandingSum = rsSet("outstandingSum")
		    End If
		    rsSet.close()
		Set rsSet = Nothing
	End Function



	Function get_tenant_balance()

			Dim orderBy, strSQL, rsSet, intRecord ,dbcmd, Icount

			intRecord = 0
			str_data = ""
            strSQL = "exec f_get_agedbalance_report 0," & YearRange
			set rsSet = Server.CreateObject("ADODB.Recordset")
			    rsSet.ActiveConnection = RSL_CONNECTION_STRING
			    rsSet.Source = strSQL
			    rsSet.CursorType = 3
			    rsSet.CursorLocation = 3
			    rsSet.Open()

            orderBy = "[cname] ASC"
			If (Request("CC_Sort") <> "") Then orderBy = " " & Request("CC_Sort") End If

            rsSet.Sort = orderBy

			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			'intPageCount = Icount
			intPageCount= rsSet.PageCount
			intRecordCount = rsSet.RecordCount

			' Sort pages
			intpage = CInt(Request("page"))

			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If

			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If

			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.

			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If

			' Make sure that the recordset is not empty.  If it is not, then set the
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then

				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If

			count = 0

			If intRecordCount > 0 Then
				' Display the record that you are starting on and the record
				' that you are finishing on for this page by writing out the
				' values in the intStart and the intFinish variables.
				str_data = str_data & "<tbody class=""CAPS"">"
					' Iterate through the recordset until we reach the end of the page
					' or the last record in the recordset.
					For intRecord = 1 to rsSet.PageSize
						str_data = str_data & "<tr>" &_
										"<td>" & rsSet("cat") & "</td>" &_
										"<td>" &  rsSet("cname") & "</td>" &_
										"<td align=""right""> " & FORMATNUMBER(rsSet("OpeningBalance"),2) & "</td>" &_
										"<td align=""right""> " & FORMATNUMBER(rsSet("InvoiveAmt"),2) & "</td>" &_
										"<td align=""right"">" & FORMATNUMBER(rsSet("payment"),2) & "</td>" &_
										"<td align=""right"">" & FORMATNUMBER(rsSet("outstanding"),2) & "</td>" &_
										"</tr>"
						count = count + 1
						rsSet.movenext()
						If rsSet.EOF Then Exit for

					Next
					str_data = str_data & "</tbody>"
	
					'ensure table height is consistent with any amount of records
					Call fill_gaps()

					' links
					str_data = str_data &_
					"<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
					"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">"  &_
					"<a href='" & PageName & "?page=1&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=""blue"">First</font></b></a> "  &_
					"<a href= '" & PageName & "?page=" & prevpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange  & "'><b><font color=""blue"">Prev</font></b></a>"  &_
					" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
					" <a href='" & PageName & "?page=" & nextpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
					" <a href='" & PageName & "?page=" & intPageCount & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange  & "'><b><font color=""blue"">Last</font></b></a>"  &_
					"</td><td align=""right"" width=""100"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value='' size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"">&nbsp;"  &_
					"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px"">"  &_
					"</td></tr></thead></table></td></tr></tfoot>"
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">No records found</td></tr>" 
				count = 1
				Call fill_gaps()
			End If

			rsSet.close()
			Set rsSet = Nothing

		End function


		' pads table out to keep the height consistent
		Function fill_gaps()

			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td></tr>"
				cnt = cnt + 1
			wend

		End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --&gt; Supplier Balances</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

	var FormFields = new Array()
	    FormFields[0] = "txt_PAGESIZE|Page Size|INTEGER|Y"

	function JumpPage(){
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage)) {
			location.href = "AgedDebtors.ASP?page="+iPage+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value
            }
		else {
			document.getElementById("QuickJumpPage").value = ""
            }
		}

	function click_go(){
		if (!checkForm()) return false;
		location.href = "AgedDebtors.ASP?page=1&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value
		}

    </script>
</head>
<body onload="initSwipeMenu(2);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" cellpadding="1" cellspacing="2" style="border-collapse: COLLAPSE;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="7">
        <thead>
            <tr>
                <td colspan="6" class='TABLE_HEAD'>
                    <b>FISCAL YEAR</b>:
                    <select name="sel_PERIOD" id="sel_PERIOD" class="textbox" style='width: 170PX'>
                        <%
		                Call OpenDB()
		                SQL = "SELECT * FROM F_FISCALYEARS ORDER BY YRANGE"
		                Call OpenRs(rsTheYears, SQL)
		                While (NOT rsTheYears.EOF)
			                isSelected = ""
			                If (CStr(YearRange) = CStr(rsTheYears.Fields.Item("YRANGE").Value)) Then
				                isSelected = " selected"
			                End If
                        %>
                        <option value="<%=(rsTheYears.Fields.Item("YRANGE").Value)%>" <%=isSelected%>>
                            <%=(rsTheYears.Fields.Item("YSTART").Value) & " - " & (rsTheYears.Fields.Item("YEND").Value)%></option>
                        <%
	  		                rsTheYears.MoveNext()
		                Wend
                        %>
                    </select><!-- &nbsp;&nbsp;<b>View By</b>
   <SELECT name="viewby" CLASS="textbox">
   	<option value="0" <%= viewItemSelectedAll%> >All</option>
   	<option value="1" <%= viewItemSelected1%>>Employe</option>
   	<option value="2" <%= viewItemSelected2%>>Supplier</option>
   	<option value="3" <%= viewItemSelected3%>>Tenent</option>
   </select>-->
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pagesize&nbsp;
                    <input type="text" class="textbox" name="txt_PAGESIZE" id="txt_PAGESIZE" maxlength="3" size="5" tabindex="3"
                        value="<%=CONST_PAGESIZE%>" />
                    <img src="/js/FVS.gif" name="img_PAGESIZE" id="img_PAGESIZE" width="15px" height="15px"
                        border="0" alt="" />
                    <input type="button" id="BTN_GO" name="BTN_GO" value=" GO " class="rslbutton" onclick="click_go()" />
                </td>
            </tr>
            <tr>
                <td width="150px">
                    &nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("cat asc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                        style="text-decoration: none">
                        <img src="../../../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0"
                            alt="Sort Ascending" /></a>&nbsp;<b>Category</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("cat desc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                                style="text-decoration: none"><img src="../../../myImages/sort_arrow_down.gif" width="11"
                                    height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td width="450px">
                    &nbsp; <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("cname asc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                        style="text-decoration: none">
                        <img src="../../../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0"
                            alt="Sort Ascending" /></a>&nbsp;<b>Name</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("cname desc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                                style="text-decoration: none"><img src="../../../myImages/sort_arrow_down.gif" width="11"
                                    height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td width="100px" align="right">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("OpeningBalance asc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                        style="text-decoration: none">
                        <img src="../../../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0"
                            alt="Sort Ascending" /></a>&nbsp;<b>Opening Balance</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("OpeningBalance desc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                                style="text-decoration: none"><img src="../../../myImages/sort_arrow_down.gif" width="11"
                                    height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td width="100px" align="right">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("InvoiveAmt asc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                        style="text-decoration: none">
                        <img src="../../../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0"
                            alt="Sort Ascending" /></a>&nbsp;<b>Invoice Amount</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("InvoiveAmt desc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                                style="text-decoration: none"><img src="../../../myImages/sort_arrow_down.gif" width="11"
                                    height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td width="100px" align="right">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("payment asc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                        style="text-decoration: none">
                        <img src="../../../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0"
                            alt="Sort Ascending" /></a>&nbsp;<b>Payments & Credit Notes</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("payment desc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                                style="text-decoration: none"><img src="../../../myImages/sort_arrow_down.gif" width="11"
                                    height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td width="100px" align="right">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("outstanding asc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                        style="text-decoration: none">
                        <img src="../../../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0"
                            alt="Sort Ascending" /></a>&nbsp;<b>Balance</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("outstanding desc")%>&PAGESIZE=<%=CONST_PAGESIZE %>"
                                style="text-decoration: none"><img src="../../../myImages/sort_arrow_down.gif" width="11"
                                    height="12" border="0" alt="Sort Descending" /></a>
                </td>
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="10" align="center" style="border-bottom: 2px solid #133e71">
            </td>
        </tr>
        <%=str_data%>
        <tr style="background-color: white">
            <td style="background-color: white; color: black" colspan="2" align="right">
                <b>Total : </b>
            </td>
            <td style="background-color: white; color: black" align="right">
                <b>
                    <%=formatcurrency(OpeningBalanceSum)%>
                </b>
            </td>
            <td style="background-color: white; color: black" align="right">
                <b>
                    <%=formatcurrency(InvoiveAmtSum)%>
                </b>
            </td>
            <td style="background-color: white; color: black" align="right">
                <b>
                    <%=formatcurrency(paymentSum)%>
                </b>
            </td>
            <td style="background-color: white; color: black" align="right">
                <b>
                    <%=formatcurrency(outstandingSum)%>
                </b>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
