<%
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=trialBalance_xls.xls"
%>

<% ByPassSecurityAccess = true %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/functions.asp" -->

<%
    Dim startdate, credit_total, debit_total, str_data, count
    Dim FY, PS, PE,  SQL_STARTDATE, ENDDATE
    Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
    Call OpenDB()
    Call get_year(Request("FY"))
    Call get_lastyearend()

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")

	if (rqCompany = "") then rqCompany = "1"
    Company = rqCompany
	if (rqCompany <> "") then
	    CompanyFilterAC = " AND isnull(ac.COMPANYID,1) = '" & rqCompany & "' "
        CompanyFilterNL = " AND isnull(COMPANYID,1)  = '" & rqCompany & "' "
     CompanyFilterX = " AND isnull(x.COMPANYID,1)  = '" & rqCompany & "' "
        CompanyFilterTAC = " AND isnull(tac.COMPANYID,1) = '" & rqCompany & "' "
	end if

    'LYE_FY - THIS CONTAINS THE LAST FISCAL YEAR FOR WHICH THEIR IS OPENING BALANCES
    'IF FY >= LYE_FY THEN USE THE STARTDATE OBTAINED FROM THIS OTHERWISE USE THE ORIGINAL START DATE
    if (CInt(FY) > Cint(LYE_FY)) then
	    SQL_STARTDATE = LYE_STARTDATE
    Else
	    SQL_STARTDATE = PS	
    End If
    
    If Request("END") <> "" Then enddate = Request("END") Else enddate = PE End If

	Call GetVariables()
	Call getAccounts()

	Function GetVariables()

		FY = Request.QueryString("FY")
        PS = CDate(FormatDateTime(Request.QueryString("START"),1))
		PE = CDate(FormatDateTime(Request.QueryString("END"),1))

	End Function

	Function getAccounts()

		Dim strSQL, rsLEVEL1, rsLEVEL2

        'year end routine filter
	    'need to remove the year end routine journal entires for the current year you are in so that the state of 
	    'the nominals can be seen before it was run.
	    YearEndRoutineFilter = " AND TXNID NOT IN (SELECT TXNID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 AND TXNDATE >= '"& PS &" 00:00' AND TXNDATE <= '"& PE &" 23:59' " & CompanyFilterNL & " ) " 
		
		str_data = ""
		strSQL = 		"SHAPE {SELECT 	ISNULL(TOPLEVEL.ACCOUNTID,-1) AS ACCOUNTID, " &_
			             " 					ISNULL(TOPLEVEL.ACCOUNTNUMBER,'N/A') AS ACCOUNTNUMBER, " &_
			             "					LTRIM(ISNULL(TOPLEVEL.ACCOUNTNUMBER,'') + ' ' + ISNULL(TOPLEVEL.FULLNAME,'')) AS LEVEL1NAME, " &_
			             "					ISNULL(AT.DESCRIPTION,'N/A') AS ACCOUNTTYPE, " &_
			             "					ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0) AS DEBIT1, " &_
			             "					ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0) AS CREDIT1, " &_
			             "					CASE WHEN (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) > 0 THEN " &_
			             "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) ELSE 0 END AS DEBIT, " &_	
			             "					CASE WHEN (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) < 0 THEN " &_
			             "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) ELSE 0 END AS CREDIT " &_	
			             "					FROM 	NL_ACCOUNT TOPLEVEL " &_
                         "						INNER JOIN NL_ACCOUNT_TO_COMPANY TAC ON TAC.ACCOUNTID = TOPLEVEL.ACCOUNTID " & CompanyFilterTAC &_
			             "					LEFT JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = TOPLEVEL.ACCOUNTTYPE " &_
			             "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " & YearEndRoutineFilter & CompanyFilterNL & " GROUP BY ACCOUNTID) DBNOCHILD " &_
			             "					  ON DBNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
			             "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " & YearEndRoutineFilter & CompanyFilterNL & " GROUP BY ACCOUNTID) CRNOCHILD " &_
			             "				 		  ON CRNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
			             "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID INNER JOIN NL_ACCOUNT_TO_COMPANY AC ON AC.ACCOUNTID = A.ACCOUNTID " & CompanyFilterAC & " WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " & YearEndRoutineFilter & CompanyFilterX & " GROUP BY PARENTREFLISTID) DBCHILD " &_
			             "						ON DBCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
			             "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID INNER JOIN NL_ACCOUNT_TO_COMPANY AC ON AC.ACCOUNTID = A.ACCOUNTID " & CompanyFilterAC & "  WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " & YearEndRoutineFilter & CompanyFilterX & " GROUP BY PARENTREFLISTID) CRCHILD " &_
			             "						  ON CRCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
			 			 "WHERE			isnull(TOPLEVEL.PARENTREFLISTID,0) = 0 " &_
			             "ORDER 	BY ACCOUNTNUMBER} " &_
			            " APPEND((SHAPE {SELECT 	A.PARENTREFLISTID, A.ACCOUNTID, A.ACCOUNTNUMBER, A.ACCOUNTNUMBER + ' ' + A.NAME AS LEVEL2NAME, " &_
			            "							AT.DESCRIPTION AS ACCOUNTTYPE, " &_
			            "							ISNULL(DR.AMOUNT,0) AS DEBIT1, ISNULL(CR.AMOUNT,0) AS CREDIT1, " &_
			            "							CASE WHEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) > 0 THEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) ELSE 0 END AS DEBIT, " &_
			            "							CASE WHEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) < 0 THEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) ELSE 0 END AS CREDIT " &_		
			            "				FROM 	NL_ACCOUNT A " &_
    		            "						INNER JOIN NL_ACCOUNT_TO_COMPANY AC ON AC.ACCOUNTID = A.ACCOUNTID " & CompanyFilterAC &_
			            "						INNER JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = A.ACCOUNTTYPE " &_
			            "						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " & YearEndRoutineFilter & CompanyFilterNL & " GROUP BY ACCOUNTID) CR ON CR.ACCOUNTID = A.ACCOUNTID " &_
			            "						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' " & YearEndRoutineFilter  & CompanyFilterNL &  " GROUP BY ACCOUNTID) DR ON DR.ACCOUNTID = A.ACCOUNTID " &_
			            "				WHERE	PARENTREFLISTID IS NOT NULL ORDER BY ACCOUNTNUMBER}) " &_
			            " AS LEVEL2 RELATE ACCOUNTID TO PARENTREFLISTID)"

        RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING
	    ' SQL NEEDS OPTIMISING  
		set rsLEVEL1 = Server.CreateObject("ADODB.Recordset")
		rsLEVEL1.ActiveConnection = RSL_DATASHAPE_CONNECTION_STRING
		rsLEVEL1.Source = strSQL
		rsLEVEL1.CursorType = 3
		rsLEVEL1.CursorLocation = 3
		rsLEVEL1.Open()
        
        credit_total = 0
	    debit_total = 0
		str_data = str_data &  "<tbody>"
        
        Do While Not rsLEVEL1.EOF
		
            str_data = str_data & 	"<tr>" &_
                                        "<td>" & rsLEVEL1("LEVEL1NAME") & "</td>"

                                        If rsLEVEL1("DEBIT") = 0 Then
			                                str_data = str_data &  "<td>&nbsp;</td>"
		                                Else
			                                debit_total = debit_total + ABS(rsLEVEL1("DEBIT"))
                                            str_data = str_data &  "<td> "& FormatNumber(ABS(rsLEVEL1("DEBIT"))) & " </td>"
		                                End If

                                        If rsLEVEL1("CREDIT") = 0 Then
			                                str_data = str_data &  "<td>&nbsp;</td>"
		                                Else
			                                credit_total = credit_total + ABS(rsLEVEL1("CREDIT"))
                                            str_data = str_data &  "<td> "& FormatNumber(ABS(rsLEVEL1("CREDIT"))) & " </td>"
		                                End If

					str_data = str_data & "</tr>"

		    ' Set object to child recordset and iterate through
		    Set rsLEVEL2 = rsLEVEL1("LEVEL2").Value
		    if not rsLEVEL2.EOF then
			    Do While Not rsLEVEL2.EOF
					
                    str_data = str_data & 	"<tr>" &_
                                        "<td><i>&nbsp;&nbsp;&nbsp;" & rsLEVEL2("LEVEL2NAME") & "</i></td>"

                                        If rsLEVEL2("DEBIT") = 0 Then
			                                str_data = str_data &  "<td>&nbsp;</td>"
		                                Else
                                            str_data = str_data &  "<td><i>" & FormatNumber(ABS(rsLEVEL2("DEBIT"))) & "</i></td>"
		                                End If

                                        If rsLEVEL2("CREDIT") = 0 Then
			                                str_data = str_data &  "<td>&nbsp;</td>"
		                                Else
                                            str_data = str_data &  "<td><i>" & FormatNumber(ABS(rsLEVEL2("CREDIT"))) & "</i></td>"
		                                End If

					str_data = str_data & "</tr>"
				    rsLEVEL2.MoveNext
			    Loop
		    end if
	
    	    count=count+1
            rsLEVEL1.MoveNext()

	    Loop
    
        rsLEVEL1.Close()

       str_data = str_data & " <TR><TD COLSPAN=3>&nbsp;</TD></TR>"
	   str_data = str_data & " <TR><TD></TD><TD ALIGN=RIGHT><b>&pound;" & FormatNumber(abs(debit_total)) & "</b></TD><TD ALIGN=RIGHT><B>&pound;" & FormatNumber(abs(credit_total)) & "</b></TD></TR>"

        str_data = str_data &  "</tbody>"
		
	End function
	
%>
<html>
<head>
    <title>RSL Manager Business</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body bgcolor="#ffffff">
    <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                Account
            </td>
            <td>
                Dedit
            </td>
            <td>
               Credit
            </td>
        </tr>
        <%= str_data %>
    </table>
</body>
</html>
