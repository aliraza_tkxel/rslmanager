<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim tdate, ref, desc, memo, txnid, debits, credits,createdby, reversalDate
	txnid = Request("TXNID")
	debits = 0
	credits = 0 
    
	if txnid = "" Then txnid = -1 End If
	OpenDB()
	strSQL = "SELECT J.TXNDATE, J.REFNUMBER, UN.DESCRIPTION, COALESCE(CONVERT(nvarchar(30), GJ.GJ_REVERSALDATE, 103),'-') as GJ_REVERSALDATE, UN.FilePath, " &_
		"A.ACCOUNTNUMBER + ' ' + A.NAME AS ACCOUNTNAME, UN.DEBIT, UN.CREDIT, UN.MEMO,E.FIRSTNAME+' '+E.LASTNAME AS FULLNAME " &_
		"FROM 	NL_JOURNALENTRY J " &_
		"	 INNER JOIN (SELECT *, AMOUNT AS DEBIT, 0 AS CREDIT, File_Url as FilePath FROM NL_JOURNALENTRYDEBITLINE WHERE TXNID = " & txnid &_
		"		UNION ALL " &_
		"	SELECT *, 0 AS DEBIT, AMOUNT AS DEBIT, File_Url as FilePath FROM NL_JOURNALENTRYCREDITLINE WHERE TXNID = " & txnid & " )  UN ON J.TXNID = UN.TXNID " &_
		"	INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = UN.ACCOUNTID " &_
        " INNER JOIN NL_GENERALJOURNAL GJ ON GJ.GJID=J.TRANSACTIONID "&_
        " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=GJ.USERID  "&_
		"ORDER BY EDITSEQUENCE"
	Call OpenRs(rsSet, strSQL)
	if not rsSet.EOF Then
		tdate = rsSet("TXNDATE")
		ref = rsSet("REFNUMBER")
        createdby=rsSet("FULLNAME")
        reversalDate = rsSet("GJ_REVERSALDATE")
		'desc = rsSet("DESCRIPTION")
		'memo = rsSet("MEMO")
	End If
%>
<HTML>
<HEAD>
<title>RSL Manager - Accounts</title> <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1"> 
<link rel="stylesheet" href="/css/RSL.css" type="text/css"> <META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" /> 
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" /> 
<style type="text/css" media="print">
    #btn_print{ visibility:hidden; }
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT> <SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT> 
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT> <SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT> 
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT> 
<SCRIPT LANGUAGE=JAVASCRIPT>
    function Print_GJ() {
        window.print();
       }
</SCRIPT> 

<BODY BGCOLOR=#FFFFFF ONLOAD="preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=700px align=center BORDER=0> 
<tr bgcolor='whitesmoke'> 
	<td colspan=3 STYLE='BORDER-bottom:1PX SOLID BLACK'>
        <b>General Journal Posting</b>
    </td>
 </tr> 
<tr> 
    <td WIDTH=100PX><b>Date</b> </td>
    <td><%=tdate%></td>
    <!--<td><b>Memo</b></td>-->
    <td>&nbsp;</td>
</tr> 
<tr> 
    <td WIDTH=100PX><b>Reversal Date</b> </td>
    <td><%=reversalDate%></td>
    <td>&nbsp;</td>
</tr> 
<tr> 
    <td><b>Reference</b> </td>
    <TD><%=ref%></TD>
    <!--<TD ROWSPAN="10" VALIGN=TOP><%=memo%></TD>-->
    <td style="text-align:right;padding-right:15px;"><input id="btn_print"  name="btn_print" type="button" value=" Print "  class="rslbutton" onclick="Print_GJ();"/></td>
</tr> 
<!--<tr> 
    <TD><b>Details</b> </TD>
    <TD><%=desc%></td>
</tr>-->
<tr>
    <td><b>Created By</b></td>
    <td><%=createdby%></td>
    <td>&nbsp;</td>
</tr>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td colspan=2>&nbsp;</td></tr>

</TABLE><br>
<TABLE align=center VALIGN=TOP height=200px STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' cellspacing=0 cellpadding=3 width=700px id="ItemTable" slcolor='' hlcolor="silver"> 
<THEAD>
<TR STYLE='HEIGHT:10PX' bgcolor=whitesmoke> 
	<TD WIDTH=150px STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>Account</b></TD>
    <TD WIDTH=75px ALIGN=RIGHT STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>Debit</b></TD>
	<TD WIDTH=75px ALIGN=RIGHT STYLE='BORDER-BOTTOM:1PX SOLID BLACK;padding-right:20px;'><b>Credit</b></TD>
	<TD WIDTH=200px% STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>Detail</b></TD>
    <TD WIDTH=200px% STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>Memo</b></TD>
	<TD WIDTH=20px STYLE='BORDER-BOTTOM:1PX SOLID BLACK'></TD>
</TR> 
</THEAD>
<TBODY>
	<% while not rsSet.EOF %>
	<tr>
		<TD WIDTH=150px><%=rsSet("ACCOUNTNAME")%></TD>
		<TD WIDTH=75px align=right><%=FormatNumber(rsSet("DEBIT"))%></TD>
		<TD WIDTH=75px align=right style='padding-right:20px;'><%=FormatNumber(rsSet("CREDIT"))%></TD>
        <TD WIDTH=200px><%=rsSet("DESCRIPTION")%></TD>
        <TD WIDTH=200px><%=rsSet("MEMO")%></TD>
        <% If IsNull(rsSet("FilePath")) = False Then %>
        <td>
            <img src="../../../IMAGES/IconAttachment.gif" onclick="window.open('/GJ_Images/<%=rsSet("FilePath")%>');">
        </td>
        <% End If %>
     </tr>
		
		<% 
		debits = debits  + rsSet("DEBIT")
		credits = credits  + rsSet("CREDIT")
		rsSet.MoveNExt() 
		%>
	<% Wend
	CloseRs(rsSet)%>
</TBODY>
<TFOOT>	<TR><TD HEIGHT=100%></TD></TR></TFOOT>
</TABLE>
<TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=700px align=center> 
<TR bgcolor=whitesmoke ALIGN=RIGHT>
	<TD WIDTH=200px style='border:none'>&nbsp;</TD>
	<TD WIDTH=75px align=right><b><%=FormatNumber(debits)%></b></TD>
	<TD WIDTH=75px nowrap align=right><b><%=FormatNumber(credits)%></b></TD>
    <TD WIDTH=190px nowrap></TD>
    <TD WIDTH=200px nowrap></TD>
</TR> 
</TABLE>
</BODY>
</HTML>
