<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="INCLUDES/functions.asp" -->
<%
	Dim startdate, enddate, AccountNumber, FULLNAME, AccountType, OpeningBalance, balance, count
	Dim dbsum, crsum
	Dim PS, PE, FY, Company
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE	
	OpenDB()
	
	FY = Request("FY")
    Company = Request("company")
	get_year(FY)		' RESIDES IN INCLUDE FILE
	get_account_details(Request("LID"))
	OpeningBalance = get_opening_balance(Request("LID"), FY, Company)
	
	YearStart = Year(PS)
	YearEnd = Year(PE)
	MonthStart = Month(PS)
	MonthEnd = Month(PE)
	
	'A MONTH COUNT OF THE TOTAL INCLUSIVE MONTHS BETWEEN THE PERIOD THAT IS CURRENTLY SELECTED.
	MonthCount = DateDiff("m", CDate("1 " & MonthName(MonthStart) & " " & YearStart),  CDate("1 " & MonthName(MonthEnd) & " " & YearEnd)) + 1
	
	MonthCount = 12
	Redim Debits(12)
	Redim Credits(12)
	Redim Total(12)		
	for i=1 to 12
		Debits(i) = 0
		Credits(i) = 0
		Total(i) = 0
	next
	
	Set Rs = Server.Createobject("ADODB.Recordset")
	Set dbcmd = Server.Createobject("ADODB.Command")

	dbcmd.ActiveConnection = RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "NL_DEBITS_MONTHLY_BY_ACCOUNT"
'	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = Request("LID")
	dbcmd.Parameters(2) = FormatDateTime(StartDate,1) & " 00:00"
	dbcmd.Parameters(3) = FormatDateTime(Enddate,1) & " 23:59"
    dbcmd.Parameters(4) = Company
	Set Rs = dbcmd.execute

	While NOT Rs.EOF
		Debits(Rs("IMONTH")) = Rs("AMOUNT")
		Rs.moveNext
	Wend	
	Rs.close()
	Set Rs = Nothing
	Set dbcmd = Nothing

	Set Rs = Server.Createobject("ADODB.Recordset")
	Set dbcmd = Server.Createobject("ADODB.Command")

	dbcmd.ActiveConnection = RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "NL_CREDITS_MONTHLY_BY_ACCOUNT"
'	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = Request("LID")
	dbcmd.Parameters(2) = FormatDateTime(StartDate,1) & " 00:00"
	dbcmd.Parameters(3) = FormatDateTime(Enddate,1) & " 23:59"		
    dbcmd.Parameters(4) = Company
	Set Rs = dbcmd.execute

	While NOT Rs.EOF
		Credits(Rs("IMONTH")) = Rs("AMOUNT")
		Rs.moveNext
	Wend	
	Rs.close()
	Set Rs = Nothing
	Set dbcmd = Nothing
	
		
%>
<html>
<head>
<title>Ledger Summary - <%=FullName%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JavaScript">

	function load_details(dtmFrom, dtmTo){
	
		location.href = "LEDGERDETAILS.ASP?START="+dtmFrom+"&END="+dtmTo+"&LID=<%=Request("LID")%>&FY=<%=FY%>&Company=<%=Company%>"

	}
	

</SCRIPT>

<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000" onload="window.focus()" class='ta'>
<TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=1 align=center>
	<TR>
		<TD style='border:1px solid black;background-color:thistle;color:white;height:15' width=100 valign=middle><b>&nbsp;Account :</b></TD><TD valign=middle>&nbsp;<%=AccountNumber %> - <%=FullName%>&nbsp;[<font color=blue><%=AccountType%></font>]&nbsp;</TD>
		<TD ALIGN=RIGHT rowspan=2 valign=top>
			<input type="BUTTON" name="btnClose" value="Full List" class=rslbutton onclick="javascript: location.href ='LedgerDetails.asp?LID=<%=Request("LID")%>&AN=<%=Request("AN")%>&AMITOP=<%=amitop%>&FY=<%=FY%>&company=<%=Company%>'" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
			<input type="BUTTON" name="btnClose" value="Close" class=rslbutton onclick="window.close()" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
		</TD>
	</TR>
	<TR>
		<TD style='border:1px solid black;background-color:thistle;color:white;height:15' width=100 valign=middle><b>&nbsp;Period :</b></TD><TD valign=middle>&nbsp;<%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD>
	</TR>
</TABLE>
<br>
<TABLE align=center WIDTH=750 CELLPADDING=1 CELLSPACING02 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=thistle>
	<THEAD>
		<TR><TD colspan=3 align=right><B>Opening Balance:&nbsp;</B></TD><TD align=right><B><%= FormatCurrency(OpeningBalance)%></b></TD></TR>
		<TR> 
		<TD STYLE='WIDTH:165px' CLASS='TABLE_HEAD'>&nbsp;<B>Date</B> </TD>
		  <TD STYLE='WIDTH:165px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Debit</B> </TD>
		  <TD STYLE='WIDTH:165px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Credit</B> </TD>
		  <TD STYLE='WIDTH:165px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Balance</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD  CLASS='TABLE_HEAD' COLSPAN=4 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<TR style='background-color:thistle'><TD COLSPAN=4>&nbsp;</TD></TR>
<%
	Balance = OpeningBalance
	count = 0
	dbsum = 0
	crsum = 0
	for i=1 to MonthCount
		if count mod 2 = 0 then color = ";background-color:beige'" else color = "" end if
		VAL = MonthStart & "_" & YearStart
		CurrBal = Debits(MonthStart) - Credits(MonthStart)
		dbsum = dbsum + Debits(MonthStart)
		crsum = crsum + Credits(MonthStart)
		Balance = Balance + CurrBal
		param1 = DateAdd("m", count, PS)
		param2 = DateAdd("d", -1, DateAdd("m", (count+1), PS))
		Response.Write "<TR STYLE='CURSOR:HAND"&color&"' ONCLICK=""load_details('"&param1&"','"&param2&"')""><TD>" & MonthName(MonthStart) & " " & YearStart & "</TD><TD align=right>" & FormatCurrency(Debits(MonthStart)) & "</TD><TD align=right>" & FormatCurrency(Credits(MonthStart)) & "</TD><TD align=right>" & FormatCurrency(Balance) & "</TD></TR>"
		MonthStart = MonthStart + 1
		if (MonthStart = 13) then
			MonthStart = 1
			YearStart = YearStart + 1
		end if
		count = count + 1
	next

%>
<TR style='background-color:thistle'>
	<TD STYLE='BORDER-bottom:2PX SOLID #133E71' align=right>&nbsp;</TD>
	<TD STYLE='BORDER-bottom:2PX SOLID #133E71' align=right><B><%= FormatCurrency(dbsum)%></B></TD>
	<TD STYLE='BORDER-bottom:2PX SOLID #133E71' align=right><B><%= FormatCurrency(crsum)%></B></TD>
	<TD STYLE='BORDER-bottom:2PX SOLID #133E71' align=right><B><%= FormatCurrency(Balance - CDbl(OpeningBalance))%></B></TD>
</TR>
<TR style='background-color:thistle'>
	<TD colspan=3 align=right><B>End Balance:</B></TD>
	<TD align=right><B><%= FormatCurrency(Balance)%></B></TD>
</TR>
</TABLE>
</body>
</html>
