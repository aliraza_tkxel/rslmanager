<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->


<%
	CONST CONST_PAGESIZE = 20
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc	   (4)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "THISTLE"
		
	ColData(0)  = "Date Created|CreatedDate|120"
	SortASC(0)  = "J.TXNID ASC"
	SortDESC(0) = "J.TXNID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0)   = ""

    ColData(1)  = "Journal Date|TDATE|120"
	SortASC(1)  = "J.TXNDATE ASC"
	SortDESC(1) = "J.TXNDATE DESC"
	TDSTUFF(1)  = ""
	TDFunc(1)   = ""
    
	ColData(2)  = "Created By|FULLNAME|150"
	SortASC(2)  = "E.FIRSTNAME+' '+E.LASTNAME ASC"
	SortDESC(2) = "E.FIRSTNAME+' '+E.LASTNAME DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2)   = ""	

	ColData(3)  = "Reference|REF|150"
	SortASC(3)  = "J.REFNUMBER ASC"
	SortDESC(3) = "J.REFNUMBER DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3)   = ""		

	'ColData(2)  = "Description|DD|300"
	'SortASC(2)  = "D.DESCRIPTION ASC"
	'SortDESC(2) = "D.DESCRIPTION DESC"	
	'TDSTUFF(2)  = " "" TITLE = '' "" "
	'TDFunc(2)   = ""		

	ColData(4)  = "Amount|AMOUNT|100"
	SortASC(4)  = "D.DEBITS ASC"
	SortDESC(4) = "D.DEBITS DESC"	
	TDSTUFF(4)  = " "" ALIGN=RIGHT"" "
	TDFunc(4)   = "FormatCurrency(|)"		
	
	PageName = "generaljournallist.asp"
	EmptyText = "No Relevant General Journals found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" TITLE="""""" & rsSet(""MEMO"") & """""" ONCLICK=""""load_me("" & rsSet(""TXNID"") & "")"""" """ 
    RowClickColumn = " ""  ONCLICK=""""load_me("" & rsSet(""TXNID"") & "")"""" """ 

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " and isnull(j.COMPANYID,1) = '" & rqCompany & "' "
	end if

	Dim orderBy
	OrderByMatched = 0
	orderBy = DefaultOrderBy
	Call SetSort()
	
	OpenDB()

   	 rqReference   = RTrim(Ltrim(Replace(Request("txt_Reference"),"'","''")))
	' Key Word serach on the description
   	 If rqReference<> "" then

         	 	' Split the original string into an array
      	 	myRefData = Split(replace(rqReference,"'","''"), " ")
       	 	SQL_Filter = SQL_Filter &  " AND ("
       	 	For j = 0 To UBound(myRefData)
       	 		 if not j = 0 then  SQL_Filter = SQL_Filter & " OR " end if
       	  	 	 SQL_Filter = SQL_Filter &  "J.REFNUMBER   like '%" & myRefData(j) & "%'"
      		 Next
      		  SQL_Filter = SQL_Filter &  " ) "
        
   	 End if

   	 'rqDescription   = RTrim(Ltrim(Replace(Request("txt_Description"),"'","''")))
	' Key Word serach on the description
   	 'If rqDescription <> "" then

         	 	' Split the original string into an array
      '	 	myData = Split(replace(rqDescription,"'","''"), " ")
      ' 	 	SQL_Filter = SQL_Filter &  " AND ("
      ' 	 	For i = 0 To UBound(myData)
       '	 		 if not i = 0 then  SQL_Filter = SQL_Filter & " OR " end if
       '	  	 	 SQL_Filter = SQL_Filter &  " D.DESCRIPTION like '%" & myData(i) & "%'"
      '		 Next
      '		  SQL_Filter = SQL_Filter &  " ) "
        
   	 'End if
   
    ' build the sql statement to filter by fiscal year
	rqFiscalYear = ""
	rqFiscalYear = Request("sel_FISCALYEAR")
	if ( rqFiscalYear <> "") then
	    SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & rqFiscalYear

	    Call OpenRs(rsFiscalYear , SQL)
	    
	    if (NOT rsFiscalYear.EOF) then   
		    SQL_Filter = SQL_Filter & " AND (J.TXNDATE >= '" & FormatDateTime(rsFiscalYear("YSTART"),1) & "' and J.TXNDATE<= '" & FormatDateTime(rsFiscalYear("YEND"),1) & "')"
		end if
	    CloseRs(rsFiscalYear)

	end if	

    ' build the fiscal dropdown now we have collected the passed variable
    Call BuildSelect(lstFiscalYear, "sel_FISCALYEAR", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, Yend, 103)", "CONVERT(VARCHAR, YSTART, 103)+ ' - ' + CONVERT(VARCHAR, Yend, 103)", "Please Select...", rqFiscalYear, NULL, "textbox200", " TABINDEX='1' style='width:180'")	
    Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:150px'")	
   
    CloseDB()
    
    
	'SQLCODE	=	"SELECT	J.TXNID, J.TXNDATE AS TDATE, J.REFNUMBER AS REF, D.DEBITS AS AMOUNT --, D.MEMO,D.DESCRIPTION AS DD " &_
	'		"FROM 	NL_JOURNALENTRY J " &_
	'		"	INNER JOIN ( "&_
    '        "                SELECT TXNID, TXNDATE, SUM(AMOUNT) AS DEBITS--, DESCRIPTION, MEMO " &_
	'		"	             FROM NL_JOURNALENTRYDEBITLINE GROUP BY TXNID, TXNDATE--, DESCRIPTION, MEMO " &_
    '        "               )  D ON D.TXNID = J.TXNID " &_
	'		"	--INNER JOIN (SELECT TXNID, TXNDATE, SUM(AMOUNT) AS CREDITS, DESCRIPTION, MEMO " &_
	'		"	--        FROM NL_JOURNALENTRYCREDITLINE GROUP BY TXNID, TXNDATE, DESCRIPTION, MEMO) C ON C.TXNID = J.TXNID " &_
	'		"WHERE	J.TRANSACTIONTYPE = 13 " & SQL_Filter & " ORDER BY " & orderBy

   SQLCODE	=	"SELECT	J.TXNID, J.TXNDATE AS TDATE, J.REFNUMBER AS REF, D.DEBITS AS AMOUNT,E.FIRSTNAME+' '+E.LASTNAME AS FULLNAME,J.TimeCreated AS CreatedDate " &_
			" FROM 	NL_JOURNALENTRY J " &_
			"	INNER JOIN ( "&_
            "                SELECT TXNID, TXNDATE, SUM(AMOUNT) AS DEBITS " &_
			"	             FROM NL_JOURNALENTRYDEBITLINE GROUP BY TXNID, TXNDATE " &_
            "               )  D ON D.TXNID = J.TXNID " &_
            " INNER JOIN NL_GENERALJOURNAL GJ ON GJ.GJID=J.TRANSACTIONID "&_
            " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=GJ.USERID  "&_
			"WHERE	(J.TRANSACTIONTYPE = 13 OR J.TRANSACTIONTYPE = (SELECT TRANSACTIONTYPEID FROM NL_TRANSACTIONTYPE WHERE DESCRIPTION='GJR' ) ) " & SQL_Filter & CompanyFilter & " ORDER BY " & orderBy
  'response.Write(SQLCODE)
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
   ' response.Write(SQLCODE)
   'response.End

	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Balance Transfer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
    function load_me(TXN_ID)
    {
        if (window.showModelessDialog) {        // Internet Explorer
            window.showModelessDialog("Popups/viewGJ.asp?TXNID=" + TXN_ID + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
        }
        else {
            window.open("Popups/viewGJ.asp?TXNID=" + TXN_ID + "&Random=" + new Date(), "", "width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
        }
    }
		
    function SubmitPage(){
    thisForm.action = "<%=PageName%>"
    thisForm.target = ""
    thisForm.submit()
}

function ExportList() {
    thisForm.action = "exportjournallist.asp";
   // thisForm.target = "_blank";
    thisForm.submit();
}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name="thisForm" method="POST">
<table class="RSLBlack">
 <tr>
    <td><b>Fiscal Years: </b></td>
    <td><b>Reference:</b></td>
     <td><b>Company:</b></td>
    <!--<td colspan=2><b>Description </b>(simple key word search):</td>--> 
      
    <td></td>
    <td></td>
 </tr>


 <tr>
    <td><%=lstfiscalyear%></td>
    <td><input id="txt_REFERENCE"  name="txt_REFERENCE" type="text" TABINDEX="2"  class="textbox"  value="<%=rqREFERENCE%>"/></td>
   <td><%=lstCompany%></td>
     <!--<td><input id="txt_DESCRIPTION"  name="txt_DESCRIPTION" type="text" TABINDEX="3"  class="textbox"  value="<%=rqDESCRIPTION%>"/></td>-->
    <td><input type=button class="RSLButton" value=" Apply Filter " TABINDEX="4"  onclick="SubmitPage()"></td>
    <td width="35%"></td>
        <td align="right"><input name="btnExport" type="button" value="Export" class="RSLButton" onclick="ExportList()" /></td>
   </tr> 
</table>
<input type="hidden" name="sqlFilter" value="<%=SQL_Filter%>" />
<input type="hidden" name="orderBy" value="<%=orderBy%>" />
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>

</BODY>
</HTML>