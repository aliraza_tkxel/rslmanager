<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="INCLUDES/functions.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance, FY
	Dim AccountNumber, FullName, AccountType, PS, PE, amitop
	Dim startdate, enddate, openingbalance, searchstring, searchstringSQL
	
	Call OpenDB()

	FY = Request("FY")
	Call get_year(FY)					        ' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	Call get_account_details(Request("LID"))	' GET THE ACCOUNT HEADER INFORMATION
    
    Company = Request("Company")

	' BUILD PAGED REPORT
	Function get_item()

		TXNID = Request("TXNID")
		Dim strSQL, rsSet, intRecord

		str_data = ""
	SQL = "SELECT ACCOUNTID, J.TXNID, CAST(FLOOR( CAST( J.TIMECREATED AS FLOAT ) )	AS DATETIME ) AS TIMECREATED , T.DESCRIPTION, U.DEBIT, U.CREDIT, "&_ 	
        "U.EDITSEQUENCE,  Case When U.DESCRIPTION LIKE '%Payment Card%' Then CONVERT(VARCHAR,RJ.TENANCYID)+':' + PaymentCard.DESCRIPTION " &_
		"ELSE ISNULL(U.DESCRIPTION, ' ') END AS ITEMDESC, U.MEMO, U.FULLNAME, U.ACCOUNTNUMBER, File_Url " &_  
				"FROM NL_JOURNALENTRY J " &_ 
                " INNER JOIN NL_GENERALJOURNAL GJ ON GJ.GJID=J.TRANSACTIONID " &_
				"LEFT JOIN NL_TRANSACTIONTYPE T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE " &_ 
				"INNER JOIN ( " &_
				"	SELECT NLA1.ACCOUNTID AS ACCOUNTID, LINEID, 1 AS TABLEORDER, TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, EDITSEQUENCE, JEDL.DESCRIPTION, JEDL.MEMO, NLA1.FULLNAME, NLA1.ACCOUNTNUMBER, JEDL.File_Url " &_
				"		FROM NL_JOURNALENTRYDEBITLINE JEDL " &_ 
				"			INNER JOIN NL_ACCOUNT NLA1 ON NLA1.ACCOUNTID = JEDL.ACCOUNTID " &_ 
				"		WHERE JEDL.TXNID = " & TXNID & " " &_
				"	UNION ALL " &_ 
				"	SELECT NLA2.ACCOUNTID AS ACCOUNTID, LINEID, 0 AS TABLEORDER, TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, EDITSEQUENCE, JECL.DESCRIPTION, JECL.MEMO, NLA2.FULLNAME, NLA2.ACCOUNTNUMBER, JECL.File_Url  " &_  
				"		FROM NL_JOURNALENTRYCREDITLINE JECL " &_ 
				"			INNER JOIN NL_ACCOUNT NLA2 ON NLA2.ACCOUNTID = JECL.ACCOUNTID " &_
				"		WHERE JECL.TXNID = " & TXNID & " " &_
				") U ON J.TXNID = U.TXNID " &_ 
				"LEFT JOIN F_RENTJOURNAL RJ ON J.TRANSACTIONID = RJ.JOURNALID " &_
				"LEFT JOIN F_PAYMENTTYPE P ON RJ.PAYMENTTYPE = P.PAYMENTTYPEID " &_
				"OUTER APPLY (Select F_PAYMENTCARDCODES.DESCRIPTION,F_PAYMENTCARDCODES.CODEID,F_PAYMENTCARDCODES.FileExtensions from  F_PAYMENTCARDDATA  " &_
				"INNER JOIN F_PAYMENTCARDFILES ON F_PAYMENTCARDDATA.FILEID =F_PAYMENTCARDFILES.FILEID " &_
				"INNER JOIN F_PAYMENTCARDCODES ON  F_PAYMENTCARDFILES.PAYMENTCODE = F_PAYMENTCARDCODES.CODEID " &_
				"Where F_PAYMENTCARDDATA.JOURNALID = RJ.JOURNALID  " &_
				"AND '.' + RIGHT(F_PAYMENTCARDFILES.[FILENAME], Len(F_PAYMENTCARDFILES.[FILENAME]) - Charindex('.', F_PAYMENTCARDFILES.[FILENAME])) =  " &_ 
				"F_PAYMENTCARDCODES.FileExtensions " &_
				")PaymentCard  " &_
				"WHERE J.TXNid = " & TXNID & " " &_
				"ORDER BY J.TIMECREATED, J.TXNID, EDITSEQUENCE, TABLEORDER DESC"
		Set rsSet = Conn.Execute(SQL)
		count = 0

		Response.Write "<tbody class='CAPS'>"
		credittotal = 0
		debittotal = 0
		while NOT rsSet.EOF
			ititle = stripHTML("A/C : " & rsSet("ACCOUNTNUMBER") & " " & rsSet("FULLNAME") & "&#13;" & rsSet("MEMO"))
			ititle2 = stripHTML(rsSet("ITEMDESC") & "&#13;" & rsSet("MEMO"))
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if

            If ( (rsSet("DESCRIPTION")  = "RD" and rsSet("ACCOUNTID") = 14) OR (rsSet("DESCRIPTION")  = "VR" And (rsSet("ACCOUNTID") = 193 or rsSet("ACCOUNTID") = 201)) ) Then
                blar = "onclick=""XLSMe()"" "
            End If

			str_data = "<tr valign=top style='cursor:pointer"&color&"'>" &_
				"<td>&nbsp;" & rsSet("TIMECREATED") & "</td>" &_
				"<td nowrap=""nowrap"">" & rsSet("DESCRIPTION") & "</td>" &_
				"<td name=""Col1"" id =""Col1"" title=""" & ititle & """>" & rsSet("ITEMDESC") & "</td>" &_
				"<td name=""Col2"" id=""Col2"" style='display:none' title=""" & ititle2 & """><b>" & rsSet("ACCOUNTNUMBER") & "</b> " & rsSet("FULLNAME") & "</td>" 				
			' SHOW DEBIT
			If rsSet("DEBIT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
				debittotal = debittotal + cdbl(rsSet("DEBIT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"" "&blar&" style=""color:red"">"&FormatNumber(rsSet("DEBIT"),2)&"</td>" 
			End If
			' SHOW CREDIT
			If rsSet("CREDIT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
				credittotal = credittotal + cdbl(rsSet("CREDIT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"" "&blar&" style=""color:blue"">"&FormatNumber(rsSet("CREDIT"),2)&"</td>"
			End If
        
            If IsNull(rsSet("File_Url")) = False Then
                docPath = rsSet("File_Url")
                str_data = str_data & "<td> " &_
                    "<img src=""../../IMAGES/IconAttachment.gif"" onclick=""window.open('Browser/docOpen.asp?docPath=" & docPath &"')"";> </td>"

            End If
			str_data = str_data & "</tr>"
			Response.Write str_data
			count = count + 1
			rsSet.movenext()
		wend

		if (count = 1) then
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
			Response.Write "<tr style='" & color & "'><td colspan=""6"" align=""center""><b>no journal entries found.</b></td></tr>"
			count = count + 1
		end if

		' END BALANCE
		Response.Write "<tr style='background-color:thistle'><td align=""right"" colspan=""3""><b> total: </b></td>" &_
		"<td align=""right""><b>"&FormatNumber(Round(debittotal,2),2)&"</b></td><td align=""right""><b>"&FormatNumber(Round(credittotal,2),2)&"</b></td></tr></tbody>" 

		rsSet.close()
		Set rsSet = Nothing
	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt, innercount
		cnt = 0
		innercount = count
		tr_num = CONST_PAGESIZE - count
		while (cnt <= tr_num)
			if innercount mod 2 = 0 then color = "style='background-color:beige'" else color = "" end if
			Response.Write "<tr "&color&"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" &_
								  "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
			cnt = cnt + 1
			innercount = innercount + 1
		wend
	End Function
%>
<html>
<head>
    <title>Ledger Item - Transaction
        <%=Request("TXNID")%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
    <script language="javascript" type="text/javascript" src="/js/CrossBrowser/ImageFormValidation-1.0.1.js"></script>
    <script language="JavaScript" type="text/javascript">
        function Swap(iCol) 
        {
       
            HideCols = "Col2";
            ShowCols = "Col1";

            if (iCol == 2)
            {
                HideCols = "Col1";
                ShowCols = "Col2";
            }
            ref = document.getElementsByName(HideCols)
            //ref = document.getElementById(HideCols)
            if (ref.length)
            {
                for (i = 0; i < ref.length; i++)
                {
                    ref[i].style.display = "none";
                }
            }

            //ref = document.getElementById(HideCols)
            ref = document.getElementsByName(ShowCols)
            if (ref.length)
            {
                for (i = 0; i < ref.length; i++)
                {
                    ref[i].style.display = "block";
                }
            }
           
        }

        var FormFields = new Array();
            FormFields[0] = "hid_XNID|XNID|Number|Y";
        function XLSMe() {
            if (!checkForm()) return false;
            //location.href = "LedgerItemDetail.asp?TXNID=" + document.getElementById("hid_XNID").value + "&LID=<%=Request("LID")%>&FY=<%=FY%>";
            location.href = "LedgerItemDetail.asp?TXNID=" + document.getElementById("hid_XNID").value + "&LID=<%=Request("LID")%>&START=<%=Request("STARTDATE")%>&END=<%=Request("ENDDATE")%>&PAGESIZE=<%=Request("pagesize")%>&page=<%=Request("page")%>&FY=<%=Request("FY")%>&SEARCHSTRING=<%=Request("SEARCHSTRING")%>";
        }

    </script>
</head>
<body bgcolor="white" text="#000000" onload="window.focus()" class='ta'>
    <form name="RSLFORM" method="post" action="">
    <input type="hidden" name="hid_XNID" id="hid_XNID" value="<%=Request("TXNID")%>" />
    <img name="img_XNID" id="img_XNID" src="/js/FVS.gif" width="15" height="15" alt="" />
    <table width="750" cellpadding="1" cellspacing="1" align="center">
        <tr>
            <td style='border: 1px solid black; background-color: thistle; color: white; height: 15'
                width="100" valign="middle">
                <b>&nbsp;Account :</b>
            </td>
            <td valign="middle">
                &nbsp;<%=AccountNumber %>
                -
                <%=FullName%>&nbsp;[<font color="blue"><%=AccountType%></font>]<br/>
            </td>
            <td align="right" rowspan="2" valign="top">
                <input type="button" name="btnBack" value=" Back " class="rslbutton" onclick="location.href ='LedgerDetails.asp?LID=<%=Request("LID")%>&START=<%=Request("STARTDATE")%>&END=<%=Request("ENDDATE")%>&PAGESIZE=<%=Request("pagesize")%>&page=<%=Request("page")%>&FY=<%=Request("FY")%>&SEARCHSTRING=<%=Request("SEARCHSTRING")%>&Company=<%=Company%>'"
                    style='background-color: silver; color: white; border: solid 1px black' />
                <input type="button" name="btnClose" value="Close" class="rslbutton" onclick="window.close()"
                    style='background-color: silver; color: white; border: solid 1px black' />
            </td>
        </tr>
        <tr>
            <td style='border: 1px solid black; background-color: thistle; color: white; height: 15'
                width="100" valign="middle">
                <b>&nbsp;Period :</b>
            </td>
            <td valign="middle">
                &nbsp;<%=FormatDateTime(StartDate,1)%>
                -
                <%=FormatDateTime(EndDate,1)%>
            </td>
        </tr>
    </table>
    <br/>
    <table align="center" width="750" cellpadding="1" cellspacing="0" style='border: solid 1px black;
        behavior: url(/Includes/Tables/tablehl.htc); border-collapse: collapse' slcolor=''
        border="1" hlcolor="thistle">
        <thead>
            <tr style='height: 3PX; border-bottom: 1px solid #133E71'>
                <td colspan="6" class='TABLE_HEAD'>
                </td>
            </tr>
            <tr>
                <td style='width: 80px' nowrap="nowrap" class='TABLE_HEAD'>
                    &nbsp;<b>Date</b>
                </td>
                <td style='width: 35px' nowrap="nowrap" class='TABLE_HEAD'>
                    <b>Type</b>
                </td>
                <td class='TABLE_HEAD' width="460">
                    <b><a href="#" onclick="Swap(1)"><font color="blue" style='text-decoration: none'>Description</font></a>
                        / <a href="#" onclick="Swap(2)"><font color="blue" style='text-decoration: none'>Account</font></a></b>
                </td>
                <td style='width: 90px' nowrap="nowrap" class='TABLE_HEAD' align="center">
                    <b>Debit</b>
                </td>
                <td style='width: 90px' nowrap="nowrap" class='TABLE_HEAD' align="center">
                    <b>Credit</b>
                </td>
                <td style="width: 20px" nowrap="nowrap" class="TABLE_HEAD" align="center"></td>
            </tr>
            <tr style='height: 3px'>
                <td class='table_head' colspan="6" align="center" style='border-bottom: 2px solid #133e71'>
                </td>
            </tr>
        </thead>
        <%
Call get_item()
Call CloseDB()
        %>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
