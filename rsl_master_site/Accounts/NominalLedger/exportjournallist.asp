<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpenDB()
    'if(Len(Request("orderBy"))>0) then
     '    orderBy = Request("orderBy")
    'else
        orderBy = "  J.TXNDATE ASC"
    'end if
    SQLCODE	=	"SELECT	J.TXNID, J.TXNDATE AS TDATE, J.REFNUMBER AS REF, D.DEBITS AS AMOUNT,E.FIRSTNAME+' '+E.LASTNAME AS FULLNAME,J.TimeCreated AS CreatedDate " &_
			" FROM 	NL_JOURNALENTRY J " &_
			"	INNER JOIN ( "&_
            "                SELECT TXNID, TXNDATE, SUM(AMOUNT) AS DEBITS " &_
			"	             FROM NL_JOURNALENTRYDEBITLINE GROUP BY TXNID, TXNDATE " &_
            "               )  D ON D.TXNID = J.TXNID " &_
            " INNER JOIN NL_GENERALJOURNAL GJ ON GJ.GJID=J.TRANSACTIONID "&_
            " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=GJ.USERID  "&_
			"WHERE	J.TRANSACTIONTYPE = 13 "  & Request("sqlFilter") & " ORDER BY " & orderBy
   ' response.Write(SQLCODE)
   Call OpenRs(rsJournalList , SQLCODE)

	
%>
<HTML>
<Head>
</Head>
<BODY BGCOLOR="#FFFFFF" MARGINHEIGHT="0" LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">

<% 
   Response.ContentType = "application/vnd.ms-excel"
   Response.AddHeader "Content-Disposition", "attachment; filename=JournalList.xls"
%>
<table width="40%" border="1px">
<tr>
<td align="center"><span style="font-weight:bold;">Date Created</span></td>
<td align="center"><span style="font-weight:bold;">Journal Date</span></td>
<td align="center"><span style="font-weight:bold;">Created By</span></td>
<td align="center"><span style="font-weight:bold;">Reference</span></td>
<td align="center"><span style="font-weight:bold;">Amount</span></td>
</tr>
<% while (Not rsJournalList.EOF) %>
<tr align="center">
<td><%=rsJournalList("CreatedDate") %></td>
<td><%=rsJournalList("TDATE") %></td>
<td><%=rsJournalList("FULLNAME") %></td>
<td><%=rsJournalList("REF") %></td>
<td><%=rsJournalList("Amount") %></td>

</tr>
<%
 rsJournalList.Movenext
Wend %>
</table>
</BODY>
</HTML>