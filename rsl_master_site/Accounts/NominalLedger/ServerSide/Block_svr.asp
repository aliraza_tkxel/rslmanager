<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	SchemeId = Request.Form("sel_Schemes")
    callbackFunc = Request.form("IACTION")
    isCascade = Request.form("IS_CASCADE")
    selectedBlock = Request.form("SELECTED_BLOCK")

	if (SchemeId = "") then
		SchemeId = -1
	end if

	OpenDB()		
    Dim lstBlocks
	Call BuildSelect(lstBlocks, "sel_Blocks", "P_BLOCK WHERE SCHEMEID = " & SchemeId, "BLOCKID, BLOCKNAME ", "BLOCKNAME", "Please Select", NULL, "WIDTH:150PX", "textbox200","onchange='PopulatePropertiesList()'")	
	CloseDB()
		
%>
<html>
<head></head>
<script language=javascript>
    function ReturnData() {

        parent.THISFORM.sel_Blocks.outerHTML = BlocksData.innerHTML;

        var callbackFuncName = '<%=callbackFunc%>';
        if (callbackFuncName == "populate_properties")
        {
            var cascade = '<%=isCascade%>';
            var block = '<%=selectedBlock%>';

            if (cascade == 'true' && block && block > 0)
            {
                parent.document.getElementById('sel_Blocks').value = block;
            }
            parent.PopulatePropertiesList();
        }

    }
</script>
<body onload="ReturnData()">
<div id="BlocksData"><%=lstBlocks%></div>
</body>
</html>