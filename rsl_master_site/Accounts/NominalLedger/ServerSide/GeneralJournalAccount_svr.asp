<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim gjId, tname , userId
	
	gjId = Request.QueryString("gjId")
	tname = Request.QueryString("tname")
	userId = Session("UserID")
	
	OpenDB()	
	
	' CALL STORED PROCEDURE F_INSERT_GJ_Template
	Set comm = Server.CreateObject("ADODB.Command")
	comm.commandtext = "F_INSERT_GJ_Template"
	comm.commandtype = 4
	Set comm.activeconnection = Conn
	comm.parameters(1) = CLng(gjId)
	comm.parameters(2) = tname
	comm.parameters(3) = CInt(userId)
	comm.execute
	
	status = comm.parameters(0)
    Set comm = Nothing
	
	CloseDB()

	Response.Redirect "/ACCOUNTS/NOMINALLEDGER/generaljournallist.asp"

%>
