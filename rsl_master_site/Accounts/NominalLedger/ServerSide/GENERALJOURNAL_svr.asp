<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim str_date, str_ref, str_details, int_account,  str_memo, isIncSC
	Dim general_journal_id, editseq, amount, cmdText, txnid	
	Dim schemeId ,blockId,propertyId, companyid, isTemplate
    Dim length
    length = 0
	schemeId = Request.Form("sel_Schemes")
	blockId =  Request.Form("sel_Blocks")
	propertyId = Request.Form("sel_property")
    companyid = Request.Form("sel_company")
    isTemplate = Request.QueryString("isTemplate")

	editseq = 0
	OpenDB()	
	
	str_date 	= Request.Form("txt_DATE")
	str_isIncSC = Request.Form("txt_ServiceCharge")
	
    str_ReversalDate 	= Request.Form("txt_ReversalDATE")
	'str_ref 	= Replace(Request.Form("txt_REF"), "'", "''")
    Dim formattedReversalDate
    
    
   if (str_isIncSC = "on") then
        isIncSC = "1"
        else
		isIncSC = "NULL"
    end if	


    if (str_ReversalDate = "") then
        formattedReversalDate = "NULL"
    else
        formattedReversalDate = "'" & FormatDateTime(str_ReversalDate) & "'"
    end if	
    'JG-716R
    'Generating Unique Reference Number
    strSQL = "Select TOP(1) TXNID,REFNUMBER from NL_JOURNALENTRY where REFNUMBER is not null AND TRANSACTIONTYPE=13 ORDER BY TIMECREATED Desc"
	Call OpenRs(rsGJ, strSQL)
	tempRefNumber= CStr(rsGJ("REFNUMBER"))
	    
    if((StrComp(mid(tempRefNumber,1,1),"G") = 0 ) And (StrComp(mid(tempRefNumber,2,1),"J") = 0 And (StrComp(mid(tempRefNumber,3,1),"-") = 0) )) then
        length = len(rsGJ("REFNUMBER"))-3
        str_ref = "GJ-" & int( mid( rsGJ("REFNUMBER"), 4, length) )  + 1
    else
	    	str_ref = "GJ-500"  
    end if
	Call CloseRs(rsGJ) 
    
    'response.Write(str_ref)
    'response.end()

	'str_details 	= Replace(Request.Form("txt_DETAILS"), "'", "''")
	'str_memo	= Replace(Request.Form("txt_MEMO"), "'", "''")

	OpenDB()
	
	' INSERT ROW IUNTO NL_GENERALJOURNAL
	strSQL = "SET NOCOUNT ON;" &_	
		"INSERT INTO NL_GENERALJOURNAL " &_
		"(GJDATE, USERID, GJ_REVERSALDATE, companyid,isIncSC) VALUES ('" & FormatDateTime(str_date) & "', " & SESSION("USERID") & ", " & formattedReversalDate & ", " & companyid & ", " & isIncSC & " )" &_
		";SELECT SCOPE_IDENTITY() AS GJID"
    
	Call OpenRs(rsGJ, strSQL)
	general_journal_id = rsGJ("GJID")
	Call CloseRs(rsGJ) 
	
	' CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ENTRY
	Set comm = Server.CreateObject("ADODB.Command")
	comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
	comm.commandtype = 4
	Set comm.activeconnection = Conn
	comm.parameters(1) = 13
	comm.parameters(2) = Cint(general_journal_id)
	comm.parameters(3) = now 
	comm.parameters(4) = FormatDateTime(str_date)
	comm.parameters(5) = str_ref
	comm.parameters(6) = null	
	if (not (IsNull(schemeId)or schemeId="")) Then
	    comm.parameters(7) = Cint(schemeId)
	end if
	if (not (IsNull(blockId) or blockId="")) Then
	    comm.parameters(8) = Cint(blockId)
	end if
	if (not (IsNull(propertyId) or propertyId="")) Then
	    comm.parameters(9) = propertyId
	end if
	 comm.parameters(10) = companyid
	
	comm.execute
	
	txnid = comm.parameters(0)
	rw txnid & "<BR>"
	
	' CREATE JOURNALENTRY DETAIL LINES
	for each item in Request.Form("ID_ROW")	

		editseq = editseq + 1
		Data = Request.Form("iData" & item)
		
		DataArray = Split(Data, "||<>||")
		if Cdbl(DataArray(0)) > 0 Then 
			cmdText = "F_INSERT_NL_JOURNALENTRYDEBITLINE" 
			amount = DataArray(0)
		Else 
			cmdText = "F_INSERT_NL_JOURNALENTRYCREDITLINE" 
			amount = DataArray(1)
		End If
		
		comm.commandtext = cmdText
		comm.parameters(1) = Clng(txnid)
		comm.parameters(2) = Cint(DataArray(2))
		comm.parameters(3) = now 
		comm.parameters(4) = Cint(editseq)
		comm.parameters(5) = FormatDateTime(str_date)
		comm.parameters(6) = amount
		comm.parameters(7) = str_ref & " " & DataArray(3) 'str_details
		comm.parameters(8) = str_ref & " " & DataArray(4)'str_memo

        if Cdbl(DataArray(0)) > 0 Then ' Debit line
            comm.parameters(9) =  null ' line id
            comm.parameters(10) = null 'tracker id
			
			if (not (IsNull(DataArray(5)) or DataArray(5)="")) Then ' cost center
				comm.parameters(11) = Cint(DataArray(5))
			else
				comm.parameters(11) = null
			end if
		
			if (not (IsNull(DataArray(6)) or DataArray(6)="")) Then ' head
				comm.parameters(12) = Cint(DataArray(6))
			else
				comm.parameters(12) = null
			end if
			
			if (not (IsNull(DataArray(7)) or DataArray(7)="")) Then ' expense
				comm.parameters(13) = Cint(DataArray(7))
			else
				comm.parameters(13) = null
			end if
			
			if (not (IsNull(DataArray(8)) or DataArray(8)="")) Then ' expense
				comm.parameters(14) = Cint(DataArray(8))
			else
				comm.parameters(14) = null
			end if
			comm.parameters(15)= DataArray(9)
			if(not (IsNull(DataArray(10)) or DataArray(10)="")) Then
				comm.parameters(16)= DataArray(10)
			else
				comm.parameters(16) = null
			end if
            if (not (IsNull(DataArray(12)) or DataArray(12)="")) Then
                comm.parameters(17) = DataArray(12)
            else
                comm.parameters(17) = null
            end if
		Else                           ' Credit line
            comm.parameters(9) =  null ' line id
				
			if (not (IsNull(DataArray(5)) or DataArray(5)="")) Then ' cost center
				comm.parameters(10) = Cint(DataArray(5))
			else
				comm.parameters(10) = null
			end if
		
			if (not (IsNull(DataArray(6)) or DataArray(6)="")) Then
				comm.parameters(11) = Cint(DataArray(6))
			else
				comm.parameters(11) = null
			end if
			
			if (not (IsNull(DataArray(7)) or DataArray(7)="")) Then
				comm.parameters(12) = Cint(DataArray(7))
			else
				comm.parameters(12) = null
			end if
			
			if (not (IsNull(DataArray(8)) or DataArray(8)="")) Then ' Company
				comm.parameters(13) = Cint(DataArray(8))
			else
				comm.parameters(13) = null
			end if
			comm.parameters(14)= DataArray(9)
			
			if(not (IsNull(DataArray(10)) or DataArray(10)="")) Then
				comm.parameters(15)= DataArray(10)
			else
				comm.parameters(15) = null
			end if
            if (not (IsNull(DataArray(12)) or DataArray(12)="")) Then
                comm.parameters(16) = DataArray(12)
            else
                comm.parameters(16) = null
            end if
		End If

		comm.execute

	next

    if ( Not(str_ReversalDate = "") ) THEN
        if ( DateDiff("d",FormatDateTime(str_ReversalDate),FormatDateTime(Date)) >= 0) THEN
            comm.commandtext = "AC_RevertGeneralJournal"
            comm.parameters(1) = Cint(general_journal_id)
            comm.execute
        End if
    End if
    Set comm = Nothing
	
	CloseDB()

    if (isTemplate = "") then
        Response.Redirect "/ACCOUNTS/NOMINALLEDGER/generaljournallist.asp"
    end if	
	
%>

<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>General Journal Server</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function CheckTemplate()
        {
            var isTempl = '<%=isTemplate%>';
            var gjId = '<%=txnid%>';
            
            if (isTempl)
            {
                parent.OpenTemplateNamePopup(gjId);
            }
            
        }

    </script>
</head>
<body onload="CheckTemplate();">
</body>
</html>