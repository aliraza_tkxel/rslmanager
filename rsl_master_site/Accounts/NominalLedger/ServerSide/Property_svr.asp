<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
    BlockId = Request.Form("sel_Blocks")
    SchemeId = Request.Form("sel_Schemes")
    isCascade = Request.form("IS_CASCADE")
    selectedProperty = Request.form("SELECTED_PROPERTY")

	if (BlockId = "") then
		BlockId = -1
	end if

    if (SchemeId = "") then
		SchemeId = -1
	end if

	OpenDB()		
    Dim lstProprty
	Call BuildSelect(lstProprty, "sel_property", "P__PROPERTY WHERE BLOCKID = " & BlockId & " and schemeId = "& SchemeId , "PROPERTYID,HOUSENUMBER+' '+ADDRESS1 AS Addresss", "ADDRESS1 ", "Please Select", NULL, "WIDTH:150PX", "textbox200","")	
	CloseDB()
		
%>
<html>
<head></head>
<script language=javascript>
    function ReturnData() {
        parent.THISFORM.sel_property.outerHTML = PropertyData.innerHTML;

        var cascade = '<%=isCascade%>';
        var property = "'<%=selectedProperty%>'";

        if (cascade == 'true' && property) {
            parent.SetProperty();
        }
    }
</script>
<body onload="ReturnData()">
<div id="PropertyData"><%=lstProprty%></div>
</body>
</html>