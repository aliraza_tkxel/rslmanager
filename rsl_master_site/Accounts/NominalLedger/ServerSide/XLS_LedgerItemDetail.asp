<%
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
%>
<!--#include file="../INCLUDES/functions.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance
	Dim AccountNumber, FullName, AccountType, amitop
	Dim startdate, enddate, openingbalance, searchstring, searchstringSQL

	Dim PS, PE, FY
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
    Dim TXNID

	Call OpenDB()

	FY = Request("FY")
	get_year(FY)					        ' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	get_account_details(Request("LID"))		' GET THE ACCOUNT HEADER INFORMATION
    TXNID = Request("TXNID")
	
	If Request("START") <> "" Then startdate = replace(Request("START"),"@","/") Else startdate = PS End If
	If Request("END") <> "" Then enddate =  replace(Request("END"),"@","/") Else enddate = PE End If
	'If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 25 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("SEARCHSTRING") = "" Then SearchFactor = "" Else SearchFactor = "%" & Replace(Request("SEARCHSTRING"), "'", "''") & "%" End If

    CONST_PAGESIZE = 50000

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	' BUILD PAGED REPORT
	Function get_breakdown()

		AccountID = Request("LID")
		Dim strSQL, rsSet, intRecord

		str_data = ""
        'rw "EXEC NL_LEDGERDETAILS_ITEM " & CInt(intPage) & ", " & CONST_PAGESIZE & "," & TXNID & " "
        'Response.End()
		set rsPager = Conn.execute("EXEC NL_LEDGERDETAILS_ITEM " & CInt(intPage) & ", " & CONST_PAGESIZE & "," & TXNID & " ")
        intPageCount = rsPager("TOTALPAGES")
		intRecordCount = rsPager("TOTALROWS")
		intPage = rsPager("CURRENTPAGE")

		set rsSet = rsPager.NextRecordset()
		Set rsPager = Nothing

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		Response.Write "<tbody class='CAPS'>"

        ' CALCULATE BALANCE FOR TOP OF PAGE
		'VERSION 2  REWRITE (ALI)
		'SEPARATED THE CALLS TO OPENING BALANCE, PAGE BALANCE AND MONTH BALANCE.
		'INCLUDE FUNCTIONS ALSO MODIFIED.
		'if (SearchFactor = "") then
			    'Opening_Balance = get_opening_balance(AccountID, FY)
				page_balance = get_balance_byTXNID(intPage, startdate, enddate, AccountID, CONST_PAGESIZE, TXNID)
			    'balance = Opening_Balance + page_balance
                running_balance = page_balance
			    'running_balance = balance
			' ENTER OPENING BALANCE
		'else
		'	Opening_Balance = 0
		'	page_balance = 0
		'	running_balance = 0
		'end if

        'rw Opening_Balance & "<br/>"
        'rw page_balance & "<br/>"
        'rw balance & "<br/>"
        'rw running_balance & "<br/>"

        credittotal = 0
		debittotal = 0
		count = 1

        Response.Write "<tr style='background-color:thistle'><td colspan=""4""><b>Brought Forward</b></td>" &_
			"<td align='right'><b>"&FormatNumber(Round(page_balance,2)*-1,2)&"</b></td></tr>" 

		while NOT rsSet.EOF
			str_data = "<tr valign=""top"">" &_
				"<td>&nbsp;" & rsSet("TIMECREATED") & "</td>" &_
				"<td nowrap=""nowrap"">" & rsSet("DESCRIPTION") & "</td>" &_
				"<td title=""" & rsSet("DESCRIPTION") & " For Tenancy Reference: "&rsSet("TENANCYID")&""">" & rsSet("TENANCYID") & "</td>"
				' SHOW DEBIT
			If rsSet("AMOUNT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
                debittotal = debittotal + cdbl(rsSet("AMOUNT"))
                'running_balance = running_balance + cdbl(rsSet("AMOUNT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"">"&FormatNumber(rsSet("AMOUNT"),2)&"</td>"
			End If
			' SHOW CREDIT
			If rsSet("AMOUNT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
                credittotal = credittotal + cdbl(rsSet("AMOUNT"))
                'running_balance = running_balance + cdbl(rsSet("AMOUNT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"">"&FormatNumber(rsSet("AMOUNT"),2)&"</td>"
			End If

			' WRITE COLUMNS TO PAGE
			Response.Write str_data & "</tr>"
			count = count + 1
			rsSet.movenext()
		wend

        running_balance = (running_balance - credittotal)
        running_balance = running_balance *(-1)

		if (count = 1) then
			Response.Write "<tr><td colspan=""5"" align=""center""><b>No journal entries found.</b></td></tr>"
			count = count + 1
		end if
		
		'ensure table height is consistent with any amount of records
		'Call fill_gaps()

		' END BALANCE
		Response.Write "<tr><td align=""right"" colspan=""3""><b> Total: </b></td>" &_
		"<td align=""right""><b>"&FormatNumber(Round(debittotal,2),2)&"</b></td><td align=""right""><b>"&FormatNumber(Round(credittotal,2),2)&"</b></td></tr>" 
        ' END BALANCE
		Response.Write "<tr><td colspan=""4""><b></b></td>" &_
		"<td align=""right""><b>"&FormatNumber(Round(running_balance,2),2)&"</b></td></tr>"
        Response.Write "</tbody>"

		' LINKS
		
		rsSet.close()
		Set rsSet = Nothing
	End function

	' pads table out to keep the height consistent
	Function fill_gaps()
		Dim tr_num, cnt, innercount
		cnt = 0
		innercount = count
		tr_num = CONST_PAGESIZE - count
		while (cnt <= tr_num)
			if innercount mod 2 = 0 then color = "style='background-color:beige'" else color = "" end if
			Response.Write "<tr "&color&"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" &_
								  "<td>&nbsp;</td><td>&nbsp;</td></tr>"
			cnt = cnt + 1
			innercount = innercount + 1
		wend
	End Function
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Ledger Details -
        <%=FullName%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <table align="center" width="750" cellpadding="1" cellspacing="0" border="0">
        <thead>
            <tr align="left">
                <td class='NO-BORDER' colspan="6">
                    <div align="left">
                        <b><font size="4">Export of Nominal Ledger</font></b><br />
                        <br />
                        <%
                            Dim todaysDate, todaysTime
				                todaysDate=Date()
                                todaysTime=time()
                                rw "Exported By : " & Session("FirstName") & " " & Session("LastName") & "<br/>"
				                rw "Date of Export : " & todaysDate & "<br/>"
                                rw "Time of Export : " & todaysTime & "<br/><br/>"
                                rw "Account : " & AccountNumber & " - " & FullName & " ["&AccountType&"]" & "<br/>"
                                rw "Dates : From " & startdate & " To " & enddate & "<br/>"
                                rw "Search Criteria : " & Replace(Request("SEARCHSTRING"), "'", "''") & "<br/><br/>"
                        %>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width: 80px' nowrap="nowrap">
                    <b>Date</b>
                </td>
                <td style='width: 35px' nowrap="nowrap">
                    <b>Type</b>
                </td>
                <td width="360">
                    <b>Description</b>
                </td>
                <td style='width: 90px' nowrap="nowrap" align="center">
                    <b>Debit</b>
                </td>
                <td style='width: 90px' nowrap="nowrap" align="center">
                    <b>Credit</b>
                </td>
            </tr>
        </thead>
        <%
        Call get_breakdown()
        Call CloseDB()
        %>
    </table>
</body>
</html>
