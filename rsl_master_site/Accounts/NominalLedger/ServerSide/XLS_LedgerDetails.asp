<%
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_EXPORT.asp" -->
<!--#include file="../Includes/functions.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance
	Dim AccountNumber, FullName, AccountType, amitop
	Dim startdate, enddate, openingbalance, searchstring, searchstringSQL

	Dim PS, PE, FY, Company
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE

	Call OpenDB()



	FY = Request("FY")
    Company = Request("Company")
	Call get_year(FY)					        ' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	Call get_account_details(Request("LID"))	' GET THE ACCOUNT HEADER INFORMATION

	If Request("START") <> "" Then startdate = replace(Request("START"),"@","/") Else startdate = PS End If
	If Request("END") <> "" Then enddate =  replace(Request("END"),"@","/") Else enddate = PE End If
	'If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 25 Else CONST_PAGESIZE = Request("PAGESIZE") End If    
	If Request("SEARCHSTRING") = "" Then SearchFactor = "" Else SearchFactor = "%" & Replace(Request("SEARCHSTRING"), "'", "''") & "%" End If
    If Request("SCHEMESTRING") = "" Then SchemeFactor = "" Else SchemeFactor = "" & Replace(Request("SCHEMESTRING"), "'", "''") & "" End If

    CONST_PAGESIZE = 50000

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
    
	' BUILD PAGED REPORT
	Function get_breakdown()

		AccountID = Request("LID")
		Dim strSQL, rsSet, intRecord 

		str_data = ""
        
	    set rsPager = Conn.execute("EXEC NL_LEDGERDETAILS " & CInt(intPage) & ", " & CONST_PAGESIZE & ", '" & FormatDateTime(STARTDATE,1)&  "', '" & FormatDateTime(enddate,1) & "', " & AccountID & ", '" & SearchFactor & "', '" & SchemeFactor & "'  , " & Company )
       
       
		intPageCount = rsPager("TOTALPAGES")
		intRecordCount = rsPager("TOTALROWS")
		intPage = rsPager("CURRENTPAGE")

		set rsSet = rsPager.NextRecordset()
		Set rsPager = Nothing
		
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' CALCULATE BALANCE FOR TOP OF PAGE
		'VERSION 2  REWRITE (ALI)
		'SEPARATED THE CALLS TO OPENING BALANCE, PAGE BALANCE AND MONTH BALANCE.
		'INCLUDE FUNCTIONS ALSO MODIFIED.
		if (SearchFactor = "") then
			Opening_Balance = get_opening_balance(AccountID, FY, company)
			If CDATE(PS) = CDATE(startdate) Then
				page_balance = get_balance(intPage, startdate, enddate, AccountID, CONST_PAGESIZE, 1, company)
				month_balance = 0
			Else
				page_balance = get_balance(intPage, startdate, enddate, AccountID, CONST_PAGESIZE, 2,company)
				month_balance = get_balance(1000000, PS, DateAdd("d", -1, startdate), AccountID, CONST_PAGESIZE, 2, company)
			End If
			balance = Opening_Balance + month_balance + page_balance
			running_balance = balance
			' ENTER OPENING BALANCE
		else
			Opening_Balance = 0
			page_balance = 0
			running_balance = 0
		end if

		Response.Write "<tbody class='CAPS'>"
            Response.Write "<tr><td colspan=""4""></td><td style=""background-color:thistle""><b>Brought Forward</b></td>" &_
			"<td align='right'><b>"&FormatNumber(Round(balance,2),2)&"</b></td></tr>" 

		count = 1

		while NOT rsSet.EOF
			str_data = "<tr valign=""top"">" &_
				"<td>&nbsp;" & rsSet("TXNDATE") & "</td>" &_
				"<td nowrap=""nowrap"">" & rsSet("DESCRIPTION") & "</td>" &_
				"<td title=""" & rsSet("MEMO") & """>" & rsSet("ITEMDESC") & "</td>"&_
                "<td>" & rsSet("MEMO") & "</td>"
				' SHOW DEBIT
			If rsSet("DEBIT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
				running_balance = running_balance + cdbl(rsSet("DEBIT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"" style=""color:red"">"&FormatNumber(rsSet("DEBIT"),2)&"</td>" 
			End If
			' SHOW CREDIT
			If rsSet("CREDIT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
				running_balance = running_balance - cdbl(rsSet("CREDIT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"" style=""color:blue"">"&FormatNumber(rsSet("CREDIT"),2)&"</td>" 
			End If

			' SHOW RUNNING BALANCE
			str_data = str_data & "<td nowrap=""nowrap"" align=""right"">"&FormatNumber(round(running_balance,2),2)&"</td></tr>"
			Response.Write str_data
			count = count + 1
			rsSet.movenext()
		wend

		if (count = 1) then
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
			Response.Write "<tr style='" & color & "'><td colspan=""6"" align=""center""><b>No journal entries found.</b></td></tr>"
			count = count + 1 
		end if
		
		' END BALANCE
		Response.Write "<tr><td colspan=""4""></td><td style=""background-color:thistle""><b>Total</b></td>" &_
		"<td align=""right""><b>"&FormatNumber(Round(running_balance,2),2)&"</b></td></tr></tbody>"

		' LINKS


		rsSet.close()
		Set rsSet = Nothing
	End function

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Ledger Details -
        <%=FullName%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <table align="center" width="750" cellpadding="1" cellspacing="0" border="0">
        <thead>
            <tr align="left">
                <td class='NO-BORDER' colspan="7">
                    <div align="left">
                        <b><font size="4">Export of BHG Nominal Ledger</font></b><br />
                        <br />
                        <%
                            Dim todaysDate, todaysTime
				                todaysDate=Date()
                                todaysTime=time()
                                rw "Exported By : " & Session("FirstName") & " " & Session("LastName") & "<br/>"
				                rw "Date of Export : " & todaysDate & "<br/>"
                                rw "Time of Export : " & todaysTime & "<br/><br/>"
                                rw "Account : " & AccountNumber & " - " & FullName & " ["&AccountType&"]" & "<br/>"
                                rw "Dates : From " & startdate & " To " & enddate & "<br/>"
                                rw "Search Criteria : " & Replace(Request("SEARCHSTRING"), "'", "''") & "<br/><br/>"
                        %>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width: 80px' nowrap="nowrap">
                    <b>Date</b>
                </td>
                <td style='width: 35px' nowrap="nowrap">
                    <b>Type</b>
                </td>
                <td width="360">
                    <b>Description</b>
                </td>
                <td width="360">
                    <b>Memo</b>
                </td>
                <td style='width: 90px' nowrap="nowrap" align="center">
                    <b>Debit</b>
                </td>
                <td style='width: 90px' nowrap="nowrap" align="center">
                    <b>Credit</b>
                </td>
                <td style='width: 100px' nowrap="nowrap" align="center">
                    <b>Balance</b>
                </td>
            </tr>
        </thead>
        <%

        Call get_breakdown()
        Call CloseDB()
        %>
    </table>
</body>
</html>
