<%@  language="VBSCRIPT" codepage="1252" %>
<% bypasssecurityaccess = True %>
<% Response.Buffer = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstAccounts
	OpenDB()
	' LIMIT NOMINALS TO ONLY NON PARENT ACCOUNTS AND TYPE OF ASSET LIABILITY INCOME AND EXPENSE
	Tsql = "NL_ACCOUNT A INNER JOIN NL_Account_to_company CCA ON CCA.accountid = A.accountid  WHERE CCA.COMPANYID = 1 AND A.ACCOUNTTYPE IN (3, 6, 7, 10, 11, 9, 12, 8, 13,14)  " &_
			" AND (A.ACCOUNTNUMBER not IN ('1600', '2400') ) "
	Call BuildSelect(lstAccounts, "sel_ACCOUNT", Tsql, "A.ACCOUNTID, A.ACCOUNTNUMBER + ' ' + A.NAME AS NAME", "A.ACCOUNTNUMBER", "Please Select...", NULL, "WIDTH:1PX; display:none", "", "tabindex=4")
	 'ByRef lst, iSelectName, SP_NAME, PS, isSelected, iStyle, iClass, iEvent
	Dim lstSchemes 
	Call BuildSelect_SP_DROPDOWN(lstSchemes, "sel_Schemes", "DF_GetAllSchemes ", "Please Select", "","WIDTH:150PX", "textbox200", "onchange='PopulateBlocksWithCallback()'")
	
    Dim blocksList 
  Call BuildSelect(blocksList, "sel_Blocks", "P_BLOCK WHERE SCHEMEID =-1", "BLOCKID, BLOCKNAME ", "BLOCKNAME", "Please Select", blocksList, "WIDTH:150PX", "textbox200","onchange='PopulatePropertiesList()'")
    
    Dim propertyList 
  Call BuildSelect(propertyList, "sel_property", "P__PROPERTY WHERE BLOCKID =0", "PROPERTYID,HOUSENUMBER+' '+ADDRESS1 AS Addresss ", "ADDRESS1 ", "Please Select", propertyList, "WIDTH:150PX", "textbox200","")

    Call GetCurrentYear()
    FY = GetCurrent_YRange
    DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
		"WHERE CC.COSTCENTREID NOT IN (22,23) AND CC.COMPANYID = 1 " &_ 
		"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"
    Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL,NULL , "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")
    
    Call BuildSelect(lstCompany, "sel_COMPANY", "G_Company", "CompanyId, Description", "CompanyId", NULL, NULL, NULL, "textbox200", " style='width:150px' onchange='Select_OnchangeNominalCodes();Select_OnchangeCostCentre()'  tabindex=5")

    Dim lstTemplates
	Call BuildSelect(lstTemplates, "sel_Templates",  "NL_GeneralJournalTemplate WHERE ISACTIVE = 1", "TemplateId, TemplateName", "TemplateName", "Please Select...", NULL, "WIDTH:1PX; display:none", "", "tabindex=4")

    'Getting User name
	strSQL = "Select FIRSTNAME +' '+LASTNAME as Username from E__EMPLOYEE Where EmployeeId =" &SESSION("USERID") 
	Call OpenRs(rsEmp, strSQL)
	user_name = rsEmp("Username")
	Call CloseRs(rsEmp) 
    
    'GETTING TOTAL RECORD
	strCountSQL = "SELECT COUNT(*) TOTAL FROM NL_GeneralJournalTemplate WHERE ISACTIVE = 1"
	Call OpenRs(rsTotal, strCountSQL)
	totalRecords = rsTotal("TOTAL")
	Call CloseRs(rsTotal) 
     
    CloseDB()
	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	
 
%>
<html>
<head>
    <title>RSL Manager - Accounts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <style type="text/css">
        #confrmTextDiv {
            padding: 0px 5px 0px 5px;
            font-size: 11px;
        }

        #confrmButtonDiv {
            padding: 0px 10px 10px 10px;
        }

        #confrmHeaderDiv {
            background-color: Black;
            color: White;
            height: 20px;
            padding: 10px 10px 5px 10px;
            font-size: 12px;
        }

        #id_confrmdiv {
            display: none;
            background-color: #FFF;
            border: 1px solid #000;
            position: fixed;
            width: 400px;
            left: 30%;
            top: 30%;
            margin-left: -150px;
            box-sizing: border-box;
            text-align: center;
        }

            #id_confrmdiv button {
                background-color: #FFF;
                display: inline-block;
                border: 2px solid #899EB7;
                padding: 1px;
                text-align: center;
                width: 70px;
                cursor: pointer;
                color: #133E70;
            }

            #id_confrmdiv .button:hover {
                background-color: #ddd;
            }

        #confirmBox .message {
            text-align: left;
            margin-bottom: 8px;
        }

        /* Template Name popup */
        #template-name-popup-background
        {
            display: none;
            background: rgba(0,0,0,0.5);
            position: fixed;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            z-index: 1000;
        }

        #template-name-popup {        
            background-color: #FFF;
            border: 1px solid #000;
            position: fixed;
            width: 423px;
            padding-left: 30px;
            padding-right: 30px;
            left: 30%;
            top: 30%;
            margin-left: -150px;
            box-sizing: border-box;
        }

        #template-name-popup button {
                background-color: #FFF;
                display: inline-block;
                border: 1px solid #000;
                padding: 3px 15px 3px 15px;
                text-align: center;
                cursor: pointer;
                color: #000;
            }

       #template-name-header {                        
            height: 20px;
            padding: 10px 10px 5px 10px;
            font-size: 13px;
            text-align:center;
        }

        #template-name-footer {
            padding: 0px 10px 10px 10px;
            text-align: center;
        }

        #template-name-body {
            text-align:center;
        }

        /* General Journal Template List Styling section */

        #popup-background {
            display: none;
            background: rgba(0,0,0,0.5);
            position: fixed;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            z-index: 1000;
        }


        #template-list-popup {
            background-color: #FFF;
            border: 1px solid #000;
            position: fixed;
            width: 600px;
            left: 30%;
            top: 30%;
            margin-left: -150px;
            box-sizing: border-box;
        }

            #template-list-popup button {
                background-color: #FFF;
                display: inline-block;
                border: 1px solid #000;
                padding: 1px;
                text-align: center;
                width: 70px;
                cursor: pointer;
                color: #000;
            }

            #template-list-popup .button:hover {
                background-color: #ddd;
            }

        #template-list-header {
            background-color: Black;
            color: White;
            height: 20px;
            padding: 10px 10px 5px 10px;
            font-size: 12px;
        }

        #template-list-footer {
            padding: 0px 10px 10px 10px;
            text-align: right;
        }

        #tbl-template-list {
            border-spacing: 0;
            width: 100%;
            padding: 5px;
        }

            #tbl-template-list th {
                border-bottom: 1px solid #000;
                text-align: left;
                font-size: 11px;
            }

            #tbl-template-list tbody tr {
                height: 28px;
            }
    </style>
</head>
<script language="JavaScript" src="/js/preloader.js"></script>
<script language="JavaScript" src="/js/general.js"></script>
<script language="JavaScript" src="/js/menu.js"></script>
<script language="JavaScript" src="/js/FormValidation.js"></script>
<script language="JavaScript" src="/js/calendarFunctions.js"></script>
<script language="JavaScript" src="/js/financial.js"></script>
<script language="JAVASCRIPT">

    var UploadFileName = "";
    var OrignalFileName = "";
    var FormFields = new Array()
    FormFields[0] = "txt_DATE|Date|DATE|Y"
    //FormFields[1] = "txt_REF|Reference|TEXT|N"
    FormFields[1] = "txt_DETAILS|Details|TEXT|N"
    FormFields[2] = "txt_ACCOUNT|Account|TEXT|Y"
    FormFields[3] = "txt_AMOUNT|Amount|CURRENCY|Y"
    FormFields[4] = "txt_CreatedDate|CreatedDate|TEXT|N"
    FormFields[5] = "txt_ReversalDATE|ReversalDate|DATE|N"
    FormFields[6] = "txt_PropertyApportionment|PropertyApportionment|CURRENCY|N"
    FormFields[7] = "txt_ServiceCharge|INCSC|BIT|Y"
    FormFields[8] = "txt_MEMO|MEMO|TEXT|Y"
    //FormFields[6] = "dr_cr|PaymentType|TEXT|N"

    var RowCounter = 0;
    var balance = 0;
    var debit_balance = 0;
    var credit_balance = 0;
    var total = 0;
    var blank = "";

    function PopulateBlocks() {
        document.THISFORM.target = "GJ_FRM"
        document.THISFORM.action = "ServerSide/Block_svr.asp"
        document.THISFORM.submit()
    }

    function PopulateBlocksWithCallback() {
        document.THISFORM.IACTION.value = "populate_properties";
        document.THISFORM.target = "GJ_FRM"
        document.THISFORM.action = "ServerSide/Block_svr.asp"
        document.THISFORM.submit()
    }

    function PopulatePropertiesList() {
        document.THISFORM.target = "GJ_FRM"
        document.THISFORM.action = "ServerSide/Property_svr.asp"
        document.THISFORM.submit()
    }

    function SetProperty() {
        var propertyId = document.THISFORM.SELECTED_PROPERTY.value;
        document.getElementById('sel_property').value = propertyId;

        document.THISFORM.SELECTED_PROPERTY.value = "";
        document.THISFORM.SELECTED_BLOCK.value = "";
        document.THISFORM.IS_CASCADE.value = "";

    }

    function SetExpenditure() {
   
        var expenditureId = document.THISFORM.SELECTED_EXPENDITURE.value;
        if (expenditureId && expenditureId > 0)
        {
            document.getElementById('sel_EXPENDITURE').value = expenditureId;
        }

        document.THISFORM.SELECTED_HEAD.value = "";
        document.THISFORM.SELECTED_EXPENDITURE.value = "";
        document.THISFORM.IS_CASCADE.value = "";
    }

    function PopulateListBox(values, thetext, which) {
        values = values.replace(/\"/g, "");
        thetext = thetext.replace(/\"/g, "");
        values = values.split(";;");
        thetext = thetext.split(";;");

        if (which == -1)
            var selectlist = document.forms.THISFORM.sel_COSTCENTRE;
        else {

            if (which == 2)
                var selectlist = document.forms.THISFORM.sel_HEAD;
            else if (which == 10)
                var selectlist = document.forms.THISFORM.sel_ACCOUNT;
            else
                var selectlist = document.forms.THISFORM.sel_EXPENDITURE;

        }

        selectlist.length = 0;

        for (i = 0; i < thetext.length; i++) {
            var oOption = document.createElement("OPTION");

            oOption.text = thetext[i];
            oOption.value = values[i];
            selectlist.options[selectlist.length] = oOption;
        }
    }

    function GetTemplateDetail(templateId) {
        document.THISFORM.IACTION.value = templateId;
        document.THISFORM.action = "../ServerSide/GeneralJournalTemplateDetail_svr.asp";
        document.THISFORM.target = "PURCHASEFRAME2<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }

    function GetTemplateList() {
        document.THISFORM.IACTION.value = "get_template_list";
        document.THISFORM.action = "../ServerSide/GeneralJournalTemplate_svr.asp";
        document.THISFORM.target = "PURCHASEFRAME2<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }

    function Select_OnchangeNominalCodes() {
        document.THISFORM.IACTION.value = "getnominalcodes_AUTO";
        document.THISFORM.action = "../../Finance/ServerSide/GetNominalCodes.asp";
        document.THISFORM.target = "PURCHASEFRAME2<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }

    function Select_OnchangeCostCentre() {
        //document.getElementById("txt_EMLIMITS").value = "0.00";						
        //document.getElementById("txt_EXPBALANCE").value = "0.00";
        //PopulateListBox("", "Waiting for Data...", -1);

        document.THISFORM.IACTION.value = "getcostcentre";
        document.THISFORM.action = "../../Finance/ServerSide/GetCostCentres.asp";
        document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }

    function Select_OnchangeFund() {
        //document.getElementById("txt_EMLIMITS").value = "0.00";
        //document.getElementById("txt_EXPBALANCE").value = "0.00";
        PopulateListBox("", "Waiting for Data...", 2);
        PopulateListBox("", "Please Select a Head...", 1);
        document.THISFORM.IACTION.value = "gethead";
        document.THISFORM.action = "../../Finance/ServerSide/GetHeads.asp";
        document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }


    function Select_OnchangeHead() {
        //document.getElementById("txt_EXPBALANCE").value = "0.00";
        //document.getElementById("txt_EMLIMITS").value = "0.00";
        PopulateListBox("", "Waiting for Data...", 1);
        document.THISFORM.IACTION.value = "change";
        document.THISFORM.action = "../../Finance/ServerSide/GetExpenditures.asp";
        document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }
    function AddRow(insertPos) {
        if (!validateReversalDate()) return false;
        if (!validateExpenditure()) return false;

        if (!checkForm()) return false
        AddDataRow(insertPos, -1);
    }

    function isTemplateNameUnique() {
        var templateName = document.getElementById('txtTemplateName').value.toLowerCase();        
        var templates = document.getElementById('sel_Templates').options;
        var isFound = false;

        for (var i = 0; i < templates.length; i++) {
            var tempOptionsText = templates[i].innerHTML.toLowerCase();
            if (tempOptionsText == templateName)
            {
                isFound = true;
                break;
            }
        }

        if(isFound == true)
        {
            alert("Template name already exist, please enter different template name")
            return false;
        }
        return true;
    }

    function AddDataRow(insertPos, uploadedFileNumber) {
        RowCounter++
        var debit, credit, propertyApportionment, incSC
        //reference = document.getElementById("txt_REF").value
        detail = document.getElementById("txt_DETAILS").value
        memo = document.getElementById("txt_MEMO").value

        incSC = document.getElementById("txt_ServiceCharge").checked
        if (incSC == false) {
            incSC = 0;
        }
        else {
            incSC = 1;
        }
        propertyApportionment = document.getElementById("txt_PropertyApportionment").value

        var selectedText = document.getElementById("txt_ACCOUNT").value;
        // Now set dropdown selected option to 'Admin'.
        $('#sel_ACCOUNT option').map(function () {
            if ($(this).text() == selectedText) return this;
        }).attr('selected', 'selected');

        if (document.getElementById("sel_ACCOUNT").value == "") {
            alert("Nominal Account has incorrect value set")
            return false
        }

        //alert(document.getElementById("sel_ACCOUNT").value)

        costCentre = document.getElementById("sel_COSTCENTRE").value
        head = document.getElementById("sel_HEAD").value
        expense = document.getElementById("sel_EXPENDITURE").value
        accountname = document.getElementById("txt_ACCOUNT").value //document.getElementById("sel_ACCOUNT").options[document.getElementById("sel_ACCOUNT").selectedIndex].text
        accountid = document.getElementById("sel_ACCOUNT").value
        //accountname = document.getElementById("sel_ACCOUNT").selectedIndex
        CompanyId = document.getElementById("sel_COMPANY").value

        if (THISFORM.dr_cr[0].checked == true) {
            debit = document.getElementById("txt_AMOUNT").value
            credit = 0
            isDebit = 1;
        }
        else {
            credit = document.getElementById("txt_AMOUNT").value
            debit = 0
            isDebit = 0;
        }

        oTable = document.getElementById("ItemTable");

        for (i = 0; i < oTable.rows.length; i++) {
            // alert(oTable.rows);
            if (oTable.rows.item(i).id == "EMPTYLINE") {
                oTable.deleteRow(i)
                break;
            }
        }
        oRow = oTable.insertRow(insertPos)
        oRow.id = "TR" + RowCounter
        oRow.onclick = AmendRow
        oRow.style.cursor = "hand"

        DATA = "<input type=\"hidden\" id=\"ID_ROW\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" id=\"iDATA" + RowCounter + "\" name=\"iDATA" + RowCounter + "\" value=\"" + debit + "||<>||" + credit + "||<>||" + accountid + "||<>||" + detail + "||<>||" + memo + "||<>||" + costCentre + "||<>||" + head + "||<>||" + expense + "||<>||" + CompanyId + "||<>||" + incSC + "||<>||" + propertyApportionment + "||<>||" + isDebit + "||<>||" + UploadFileName + "||<>||" + OrignalFileName + "\">"

        AddCell(oRow, accountname + DATA, "", "", "")
        AddCell(oRow, blank, blank, "", "")
        AddCell(oRow, FormatCurrencyComma(debit), debit, "right", "")
        AddCell(oRow, FormatCurrencyComma(credit), credit, "right", "")

        if (UploadFileName) {
            docPath = "/GJ_Images/" + UploadFileName;
            ClipImage = "<img src='../../IMAGES/IconAttachment.gif' onclick=OpenClipImage('" + docPath + "');>"
             //ClipImage = "<img src='../../IMAGES/IconAttachment.gif' onclick=window.open('" + docPath + "');>"
             AddCell(oRow, ClipImage, "", "center", "#FFFFFF");
             UploadFileName = "";
             OrignalFileName = "";
             var anchorFile = document.getElementById("aUploadFile");
             anchorFile.setAttribute("style", "display:none");
        }
        else {
            AddCell(oRow, "<td></td>", "", "center", "#FFFFFF");
        }

        DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + debit + "," + credit + ")\">"
        AddCell(oRow, DelImage, "", "center", "#FFFFFF");
        SetTotals(debit, credit)
        ResetData();
    }

    function OpenClipImage(docPath)
    {
        debugger;
        event.stopPropagation();
        window.open(docPath);
    }

    function ResetData() {
        //document.getElementById("txt_REF").value = ""
        document.getElementById("txt_DETAILS").value = "";
        document.getElementById("txt_ACCOUNT").value = "";
        document.getElementById("sel_ACCOUNT").value = "";
        ResetCostCentreDropdowns();
        document.getElementById("txt_AMOUNT").value = "";
        document.getElementById("txt_MEMO").value = "";
        document.getElementById("AmendButton").style.display = "none";
        document.getElementById("AddButton").style.display = "block";
        document.getElementById("txt_PropertyApportionment").value = "";
        var anchorFile = document.getElementById("aUploadFile");
        anchorFile.setAttribute("style", "display:none");
        //document.getElementById("txt_ServiceCharge").checked = false;
    }

    function DisplayTemplateListPopup(rowData) {

        document.getElementById('template-list-body').innerHTML = "";
        document.getElementById('template-list-body').innerHTML = rowData;

        document.getElementById('popup-background').style.display = "block";
        document.getElementById('btn-cancel').onclick = function () {
            document.getElementById('popup-background').style.display = "none";
        };

    }

    function OpenTemplateNamePopup(gjId) {

        document.getElementById('txtTemplateName').value = "";
        document.getElementById('hdnGjId').value = gjId;
        document.getElementById('template-name-popup-background').style.display = "block";

        document.getElementById('btnCancelTemplate').onclick = function () {
            location.href = "/ACCOUNTS/NOMINALLEDGER/generaljournallist.asp"
        };

        document.getElementById('btnSaveTemplate').onclick = function () {
            var templateName = document.getElementById('txtTemplateName').value;
            if(templateName.trim().length == 0)
            {
                alert("Please enter template name");
                return false;
            }

            if (isTemplateNameUnique() == false)
            {
                return false;
            }

            document.getElementById('template-name-popup-background').style.display = "none";
            var gId = document.getElementById('hdnGjId').value;
            var templateName = document.getElementById('txtTemplateName').value;

            var queryString = "?gjId=" + gId + "&tname=" + templateName
            THISFORM.target = ""
            THISFORM.method = "POST"
            THISFORM.action = "ServerSide/GeneralJournalAccount_svr.asp" + queryString
            THISFORM.submit();            
        };

    }

    function LoadGeneralJournaltemplate(template) {

        console.log(template);

        document.getElementById('popup-background').style.display = "none";
        ResetGeneralJournalFormForTemplate();
        ResetAccountList();
        PopulateGeneralJournalTemplateData(template);
        PopulateAccountJournalTemplateData(template);
    }

    function CheckTemplateCount() {
        var templateCount = "<%=totalRecords%>";
        var maxTemplateCount = 50;
        if (templateCount >= maxTemplateCount)
        {
            alert("Maximum 50 templates can be saved. Please remove template from Template Management Area to add more.");
            return false;
        }
        return true;        
    }

    function PopulateAccountJournalTemplateData(template) {

        var accountTemplateArray = template.accountTemplateRows;
        oTable = document.getElementById("ItemTable");
        insertPos = -1;

        for (var j = 0; j < accountTemplateArray.length; j++) {
            var template = accountTemplateArray[j];

            oRow = oTable.insertRow(insertPos)
            oRow.id = "TR" + template.Counter
            oRow.onclick = AmendRow
            oRow.style.cursor = "hand"

            DATA = "<input type=\"hidden\" id=\"ID_ROW\" name=\"ID_ROW\" value=\"" + template.Counter + "\"><input type=\"hidden\" id=\"iDATA" + template.Counter + "\" name=\"iDATA" + template.Counter + "\" value=\"" + template.Debit + "||<>||" + template.Credit + "||<>||" + template.AccountId + "||<>||" + template.Detail + "||<>||" + template.Memo + "||<>||" + template.CostCentreId + "||<>||" + template.HeadId + "||<>||" + template.ExpenditureId + "||<>||" + template.CompanyId + "||<>||" + template.IncSC + "||<>||" + template.PropertyApportionment + "||<>||" + template.IsDebit + "||<>||" + UploadFileName + "||<>||" + OrignalFileName + "\">"
            AddCell(oRow, template.AccountNumber + " " + template.FullName + DATA, "", "", "")
            AddCell(oRow, blank, blank, "", "")
            AddCell(oRow, FormatCurrencyComma(template.Debit), template.Debit, "right", "")
            AddCell(oRow, FormatCurrencyComma(template.Credit), template.Credit, "right", "")
            AddCell(oRow, "<td></td>", "", "center", "#FFFFFF");
            DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + template.Counter + "," + template.Debit + "," + template.Credit + ")\">"
            AddCell(oRow, DelImage, "", "center", "#FFFFFF");
            SetTotals(template.Debit, template.Credit)

        }

        RowCounter = 0;
        if (accountTemplateArray.length > 0)
        {
            RowCounter = accountTemplateArray.length
        }

    }

    function PopulateGeneralJournalTemplateData(template) {
        document.getElementById("sel_COMPANY").value = template.companyId;
        if (template.schemeId && template.schemeId > 0) {
            document.getElementById("sel_Schemes").value = template.schemeId;
        }
        document.THISFORM.SELECTED_BLOCK.value = template.blockId;
        document.THISFORM.SELECTED_PROPERTY.value = template.propertyId;
        document.THISFORM.IS_CASCADE.value = "true";
        PopulateBlocksWithCallback();

    }

    function ResetGeneralJournalFormForTemplate() {
        document.getElementById("txt_DETAILS").value = "";
        document.getElementById("sel_COMPANY").selectedIndex = "0";
        document.getElementById("txt_ACCOUNT").value = "";
        document.getElementById("txt_AMOUNT").value = "";
        document.getElementById("dr_cr").checked = true;
        document.getElementById("txt_PropertyApportionment").value = "";

        document.getElementById("sel_Schemes").selectedIndex = "0";
        PopulateBlocksWithCallback();

        ResetCostCentreDropdowns();

        document.getElementById("txt_MEMO").value = "";

    }

    function ResetCostCentreDropdowns()
    {
        document.getElementById("sel_COSTCENTRE").selectedIndex = "0";
        document.THISFORM.SELECTED_HEAD.value = "";
        document.THISFORM.SELECTED_EXPENDITURE.value = "";
        document.THISFORM.IS_CASCADE.value = "true";
        Select_OnchangeFund();
    }

    function ResetAccountList() {

        while (document.getElementById('ItemTable').rows.length > 1) {
            document.getElementById("ItemTable").deleteRow(1);
        }

        balance = 0;
        debit_balance = 0;
        credit_balance = 0;

        document.getElementById("hd_BALANCE").value = FormatCurrencyComma(balance);
        document.getElementById("hd_CREDITS").value = parseFloat(credit_balance);
        document.getElementById("hd_DEBITS").value = parseFloat(debit_balance);

        document.getElementById("i_BALANCE").innerHTML = FormatCurrencyComma(balance);
        document.getElementById("i_DEBITS").innerHTML = FormatCurrencyComma(debit_balance);
        document.getElementById("i_CREDITS").innerHTML = FormatCurrencyComma(credit_balance);

    }

    function SelectElement(id, valueToSelect) {
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }

    function AmendRow() {

        event.cancelBubble = true
        Ref = this.id
        RowNumber = Ref.substring(2, Ref.length)

        StoredData = document.getElementById("iDATA" + RowNumber).value;
        StoredArray = StoredData.split("||<>||")

        var costcentreId = StoredArray[5];
        var headId = StoredArray[6];
        var expenditureId = StoredArray[7];

        if (costcentreId && costcentreId > 0) {
            document.getElementById("sel_COSTCENTRE").value = costcentreId;
        } else {
            document.getElementById("sel_COSTCENTRE").selectedIndex = "0";
        }
        document.THISFORM.SELECTED_HEAD.value = headId;
        document.THISFORM.SELECTED_EXPENDITURE.value = expenditureId;
        document.THISFORM.IS_CASCADE.value = "true";
        Select_OnchangeFund();

        document.getElementById("txt_MEMO").value = StoredArray[4]
        document.getElementById("txt_DETAILS").value = StoredArray[3]

        //document.getElementById("txt_REF").value = StoredArray[3]
        document.getElementById("sel_ACCOUNT").value = StoredArray[2]
        var accountsSelect = document.getElementById("sel_ACCOUNT");
        var accountText = accountsSelect.options[accountsSelect.selectedIndex].text;
        document.getElementById("txt_ACCOUNT").value = accountText

        if (StoredArray[9] == "0") {
            document.getElementById("txt_ServiceCharge").checked = false;
        }
        else {
            document.getElementById("txt_ServiceCharge").checked = true;
        }
        document.getElementById("txt_PropertyApportionment").value = FormatCurrencyComma(StoredArray[10])
        var isDebit = StoredArray[11]
        //for (i=0; i<StoredArray.length; i++)
        // 	alert(StoredArray[i])
        if (isDebit > 0) {
            document.getElementById("txt_AMOUNT").value = FormatCurrencyComma(StoredArray[0]);
            THISFORM.dr_cr[0].checked = true;
        }
        else {
            document.getElementById("txt_AMOUNT").value = FormatCurrencyComma(StoredArray[1]);
            THISFORM.dr_cr[1].checked = true;
        }

        UploadFileName = StoredArray[12]
        if (UploadFileName) {
            OrignalFileName = StoredArray[13];
            var anchorFile = document.getElementById("aUploadFile");
            anchorFile.href = anchorFile.href + UploadFileName;
            anchorFile.setAttribute("style", "display:inline");
            anchorFile.innerText = OrignalFileName;
        }

        document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[0] + "||<>||" + StoredArray[1]
        document.getElementById("AddButton").style.display = "none"
        document.getElementById("AmendButton").style.display = "block"
    }

    function UpdateRow() {

        if (!validateReversalDate()) return false;
        if (!validateExpenditure()) return false;
        if (!checkForm()) return false

        sTable = document.getElementById("ItemTable")
        RowData = document.getElementById("UPDATEID").value
        RowArray = RowData.split("||<>||")
        MatchRow = RowArray[0]
        for (i = 0; i < sTable.rows.length; i++) {
            if (sTable.rows.item(i).id == "TR" + MatchRow) {
                sTable.deleteRow(i)
                SetTotals(-RowArray[1], -RowArray[2])
                break;
            }
        }
        AddRow(i)
    }

    // COMPUTE THE TOTAL FOR THE DEBIT TOTAL, THE CREDIT TOTAL AND THE OVERALL BALANCE
    function SetTotals(debitvalue, creditvalue) {

        balance -= parseFloat(creditvalue);
        balance += parseFloat(debitvalue);
        debit_balance += parseFloat(debitvalue);
        credit_balance += parseFloat(creditvalue);

        document.getElementById("hd_BALANCE").value = FormatCurrencyComma(balance);
        document.getElementById("hd_CREDITS").value = parseFloat(credit_balance);
        document.getElementById("hd_DEBITS").value = parseFloat(debit_balance);

        document.getElementById("i_BALANCE").innerHTML = FormatCurrencyComma(balance);
        document.getElementById("i_DEBITS").innerHTML = FormatCurrencyComma(debit_balance);
        document.getElementById("i_CREDITS").innerHTML = FormatCurrencyComma(credit_balance);
    }

    function AddCell(iRow, iData, iTitle, iAlign, iColor) {
        oCell = iRow.insertCell()
        oCell.innerHTML = iData
        if (iTitle != "") oCell.title = iTitle
        if (iAlign != "") oCell.style.textAlign = iAlign
        if (iColor != "") oCell.style.backgroundColor = iColor
    }

    function DeleteRow(RowID, debit, credit) {

        event.cancelBubble = true;

        oTable = document.getElementById("ItemTable");
        //alert(oTable);
        for (i = 0; i < oTable.rows.length; i++) {
            if (oTable.rows.item(i).id == "TR" + RowID) {
                oTable.deleteRow(i)
                SetTotals(-debit, -credit)
                break;
            }
        }
        if (oTable.rows.length == 1) {
            oRow = oTable.insertRow()
            oRow.id = "EMPTYLINE"
            oCell = oRow.insertCell()
            oCell.colSpan = 7
            oCell.innerHTML = "Please enter an item from above"
            oCell.style.textAlign = "center"
        }
        ResetData()
    }

    function SaveJournal(isTemplate) {
        /*if (! parseFloat(FormatCurrency(balance)) == 0 ){
        alert("Balance must equal zero");
        return false;
        }*/
        if (parseFloat(document.getElementById("hd_DEBITS").value) == 0 && parseFloat(document.getElementById("hd_CREDITS").value) == 0) {
            alert("Please enter some items before recording a general journal.")
            return false;
        }

        if (parseFloat(document.getElementById("hd_BALANCE").value) != 0) {
            alert("Please make sure Debit and Credit amounts are equal.")
            return false;
        }


        var queryString = "";
        if(isTemplate)
        {
            if (CheckTemplateCount() == false) return false;
            queryString = "?isTemplate=1"
        }

        ResetData()
        FormFields[0] = "txt_DATE|Date|DATE|N"
        //FormFields[1] = "txt_REF|Reference|TEXT|N"
        FormFields[1] = "txt_DETAILS|Details|TEXT|N"
        FormFields[2] = "txt_ACCOUNT|Account|TEXT|N"
        FormFields[3] = "txt_AMOUNT|Amount|CURRENCY|N"
        FormFields[4] = "txt_CreatedDate|CreatedDate|TEXT|N"
        FormFields[5] = "txt_ReversalDATE|ReversalDate|DATE|N"
        FormFields[6] = "txt_PropertyApportionment|PropertyApportionment|CURRENCY|N"
        FormFields[7] = "txt_ServiceCharge|INCSC|BIT|N"
        FormFields[8] = "txt_MEMO|MEMO|TEXT|N"

        if (!checkForm()) return false
		if(isTemplate)
        {
            THISFORM.target = "GJ_FRM"
        }
		else
		{
			THISFORM.target = ""
		}
        THISFORM.action = "ServerSide/generaljournal_svr.asp" + queryString
        THISFORM.submit();
    }

    // ONLY ALLOW POSITIVE VALUES TO PASS THROUGH		
    function make_postive(iVal) {

        if (iVal != "" && !isNaN(iVal))
            if (iVal < 0)
                document.getElementById("txt_AMOUNT").value = "";
    }
    function make_postive_PA(iVal) {

        if (iVal != "" && !isNaN(iVal))
            if (iVal < 0) {
                document.getElementById("txt_PropertyApportionment").value = "";
            }
    }
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function checkErrors() {
        var foo = getParameterByName('error');
        if (foo) {
            alert("Duplicate Reference Number Error - Try again please !");
        }
    }

    function confirmDialog() {

        document.getElementById('id_confrmdiv').style.display = "block";
        document.getElementById('id_truebtn').onclick = function () {
            document.getElementById('id_confrmdiv').style.display = "none";
            return true;
        };

        document.getElementById('id_falsebtn').onclick = function () {
            document.getElementById('id_confrmdiv').style.display = "none";
            document.getElementById("txt_ReversalDATE").value = "";
        };
    }

    function syncReversalDate() {

        //we need to ensure that Reversal date is set to 1st of next month by default//
        txt_DATE = document.getElementById("txt_DATE").value;
        if (txt_DATE == "") {
            document.getElementById("txt_ReversalDATE").value = "";
            return true;
        }

        arrSplit = txt_DATE.split("/");
        var txt_NewReversalDATE = "";
        var date = month = year = "";
        date = "1";

        if ((+arrSplit[1]) == 12)
            year = 1 + (+arrSplit[2]);
        else
            year = arrSplit[2];

        month = (1 + (+arrSplit[1])) % 12;
        if (month == 0) {
            month = 12;
        }
        txt_NewReversalDATE = txt_NewReversalDATE.concat(date, "/", month, "/", year);
        document.getElementById("txt_ReversalDATE").value = txt_NewReversalDATE;
        if (txt_DATE != "" && confirmDialog())
            return true;
        else
            return false;
    }

    function validateReversalDate() {

        // we need to ensure that Reversal DATE is not less than Journal Date //
        txt_DATE = document.getElementById("txt_DATE").value;
        arrSplit = txt_DATE.split("/");
        txt_DATE = new Date(arrSplit[2], arrSplit[1] - 1, arrSplit[0]);

        txt_NewReversalDATE = document.getElementById("txt_ReversalDATE").value;
        arrSplit = txt_NewReversalDATE.split("/");
        txt_NewReversalDATE = new Date(arrSplit[2], arrSplit[1] - 1, arrSplit[0]);

        if (txt_NewReversalDATE <= txt_DATE) {
            alert("Please enter a Reversal Date greater than Journal Date!");
            return false;
        }

        return true;
    }

    function validateExpenditure() {
        if ((document.getElementById("sel_COSTCENTRE").value != "" && document.getElementById("sel_COSTCENTRE").value > 0) &&
            (document.getElementById("sel_EXPENDITURE").value == "" || document.getElementById("sel_EXPENDITURE").value <= 0)) {
            alert("Please select Expenditure!");
            return false;
        }
        return true;
    }

    function afterUploadFileEvent(orignalFileName, uFileName) {
        debugger;
        OrignalFileName = orignalFileName;
        UploadFileName = uFileName;
        var anchorFile = document.getElementById("aUploadFile");
        anchorFile.href = anchorFile.href + UploadFileName;
        anchorFile.setAttribute("style", "display:inline");
        anchorFile.innerText = orignalFileName;
    }

</script>
<link rel="stylesheet" href="jquery-ui.css">
<link rel="stylesheet" href="https://jqueryui.com/autocomplete/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>


  $( function() {
    var availableTags = [

      <%
      OpenDB()
      Dim AccountList
        'Getting User name
 
      strSQL = "SELECT HE.ACCOUNTNUMBER + ' ' + HE.NAME AS Account  FROM NL_Account HE " &_
			    "INNER JOIN NL_Account_to_company CCA ON CCA.accountid = he.accountid " &_ 
			    "WHERE cca.companyid = 1 AND HE.ACCOUNTTYPE IN (3, 6, 7, 10, 11, 9, 12, 8, 13,14) AND (HE.ACCOUNTNUMBER not IN ('1600', '2400') ) ORDER BY HE.AccountNumber"
    		
	    Call OpenRs(rsAccount, strSQL)
        while (Not rsAccount.EOF)
	    
        
      %>
      "<%=rsAccount("Account") %>",
      <% 
      rsAccount.Movenext
    Wend
    Call CloseRs(rsAccount)
      CloseDB()
      %>
    ];
    $( "#txt_ACCOUNT" ).autocomplete({
      source: availableTags
    });

  } );
</script>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(0);preloadImages();checkErrors();" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0" class='TA'>

    <div id="id_confrmdiv">
        <div id="confrmHeaderDiv">
            <span>
                <strong>Are You Sure?</strong>
            </span>
        </div>
        <br>
        <div id="confrmTextDiv" valign="top">
            <span float:center>As you have recorded a Reversal Date, the general journal will be reversed on the date you have entered.
            </span>
        </div>
        <br>
        <div valign="bottom" id="confrmButtonDiv">
            <button id="id_falsebtn">Cancel</button>
            &emsp;&emsp;
            <button id="id_truebtn">Confirm</button>
        </div>
    </div>

    <div id="popup-background">
        <div id="template-list-popup">
            <div id="template-list-header">
                <span>
                    <strong>Select a Template</strong>
                </span>
            </div>
            <br>
            <div style="max-height: 260px; overflow: auto;">
                <table id="tbl-template-list">
                    <thead>
                        <tr>
                            <th>Template Name:</th>
                            <th>Saved:</th>
                            <th>By:</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="template-list-body">
                    </tbody>

                </table>
            </div>
            <br>
            <div valign="bottom" id="template-list-footer">
                <button id="btn-cancel">Cancel</button>
            </div>
        </div>
    </div>

    <div id="template-name-popup-background">
        <div id="template-name-popup">
            <div id="template-name-header">
                <span>
                    <strong>Your journal has been posted successfully!</strong>
                </span>
            </div>
            <br>
            <div id="template-name-body" valign="top">
                <span float:center style="font-size:12px;"> To save this journal as a template you must give it a unique name for future reference:
                </span>
                <br />
                <br />
                <input type="text" id="txtTemplateName" maxlength="50" style="width:250px; font-style:italic; text-align:center;"  placeholder="Enter the template name here" />
                <input type="hidden" id="hdnGjId" value="" />
            </div>
            <br>
            <div valign="bottom" id="template-name-footer">
                <button id="btnCancelTemplate">Do Not Save Template</button>
                &emsp;&emsp;
                <button id="btnSaveTemplate">Save Template</button>
            </div>
        </div>
    </div>

    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <iframe src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style='display: none'></iframe>
    <iframe src="/secureframe.asp" name="PURCHASEFRAME2<%=TIMESTAMP%>" style='display: none'></iframe>
    <form name="THISFORM" action="" method="post">
        <br>
        <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="3" width="95%" align="center" border="0">
            <tr bgcolor='whitesmoke'>
                <td colspan="6" style='border-bottom: 1PX SOLID BLACK'><b>General 
Journal Tool</b></td>
                <td></td>
            </tr>
            <tr>
                <td>Inc In S/Chge </td>
                <td>
                    <input type="checkbox" name="txt_ServiceCharge" id="txt_ServiceCharge" />
                    <img src="/js/FVS.gif" width="15" height="15" id="img_ServiceCharge" name="img_ServiceCharge">
                </td>
                <td>Property Apportionment </td>
                <td>
                    <input id="txt_PropertyApportionment" name="txt_PropertyApportionment" type="text" class="TEXTBOX200" tabindex="5" maxlength="10" size="50" style='width: 150PX' onblur='make_postive_PA(this.value)'>
                    <img src="/js/FVS.gif" width="15" height="15" id="img_PropertyApportionment" name="img_PropertyApportionment">
                </td>
            </tr>
            <tr>
                <td width="100PX">Journal Date </td>
                <td>
                    <input name="txt_DATE" id="txt_DATE" type="text" class="TEXTBOX200" size="50" onblur="syncReversalDate()" maxlength="50" tabindex="1" value="" style="width: 150PX"><img src="/js/FVS.gif" width="15" height="15" id="img_DATE" name="img_DATE">
                    <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_DATE',240,165,'de','#FFFFFF','#133E71')" tabindex="1">
                        <img alt="calender_image" src="../images/calendar.png" width="20" height="20" style="vertical-align: middle;"></a>


                    <div id="Calendar1" style="position: absolute; width: 200px; height: 109px; z-index: 20; visibility: hidden; background-color: white;">
                    </div>

                </td>

                <td width="100PX">Scheme: </td>
                <td><%=lstSchemes%></td>

            </tr>
            <tr>
                <td width="100PX">Reversal Date </td>
                <td>
                    <input name="txt_ReversalDATE" id="txt_ReversalDATE" type="text" class="TEXTBOX200" size="50" onblur="validateReversalDate()" maxlength="50" tabindex="1" value="" style='width: 150PX'><img src="/js/FVS.gif" width="15" height="15" id="img_ReversalDATE" name="img_ReversalDATE">
                    <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_ReversalDATE',240,165,'de','#FFFFFF','#133E71')" tabindex="1">
                        <img alt="calender_image" src="../images/calendar.png" width="20" height="20" style="vertical-align: middle;"></a>


                    <div id="Calendar2" style="position: absolute; width: 200px; height: 109px; z-index: 20; visibility: hidden; background-color: white;">
                    </div>

                </td>

                <td>Blocks:</td>
                <td><%=blocksList%></td>

            </tr>
            <tr>
                <td width="100PX">Date Created</td>
                <td>
                    <input id="txt_CreatedDate" name="txt_CreatedDate" type="text" class="TEXTBOX200" size="50" maxlength="50" tabindex="1" value="<%=date%>" style='width: 150PX' readonly><img src="/js/FVS.gif" width="15" height="15" id="img_CreatedDate" name="img_CreatedDate"></td>
                <td>Properties:</td>
                <td><%=propertyList%></td>
            </tr>
            <tr>
                <td>Details </td>
                <td>
                    <input id="txt_DETAILS" name="txt_DETAILS" type="text" class="TEXTBOX200" tabindex="3" maxlength="50" size="50" style='width: 150PX'>
                    <img src="/js/FVS.gif" width="15" height="15" id="img_DETAILS" name="img_DETAILS" title="">
                </td>
                <td>Cost Centre :
                </td>
                <td>
                    <%=lstCostCentres%>
                </td>
                <!-- TODO: -->


            </tr>
            <tr>
                <td>Company </td>
                <td>
                    <%=lstCompany%>

                </td>
                <td>Head :
                </td>
                <td>
                    <select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()"
                        tabindex="4">
                        <option value="">Please Select a Cost Centre...</option>
                    </select>
                </td>


            </tr>
            <tr>
                <td>Nominal Account  <%=lstAccounts%></td>
                <td>
                    <div class="ui-widget" id="accountdiv">
                        <input id="txt_ACCOUNT" class="TEXTBOX200" style='width: 150PX'>
                        <img src="/js/FVS.gif" width="15" height="15" id="img_ACCOUNT" name="img_ACCOUNT">
                    </div>

                </td>
                <td>Expenditure :
                </td>
                <td>
                    <select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" tabindex="4">
                        <option value="">Please select a Head...</option>
                    </select>
                </td>

            </tr>
            <tr>

                <td>Amount </td>
                <td>
                    <input id="txt_AMOUNT" name="txt_AMOUNT" type="text" class="TEXTBOX200" tabindex="5" maxlength="10" size="50" style='width: 150PX' onblur='make_postive(this.value)'>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_AMOUNT" id="img_AMOUNT">
                </td>
                <td>Reason <span style="color: red; vertical-align: middle;">*</span> :</td>
                <td rowspan="4" valign="TOP">
                    <textarea tabindex="6" name="txt_MEMO" id="txt_MEMO" rows="7" class="TEXTBOX200" style='overflow: hidden; border: 1px solid #133E71; width: 198px;'></textarea>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_MEMO" id="img_MEMO">
                </td>
            </tr>


            <tr>
                <td>Submitted By</td>
                <td><%=user_name %></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <table>
                        <tr>
                            <td>Debit
                                <input type="radio" id="dr_cr" name="dr_cr" value="1" checked></td>
                            <td>Credit
                                <input type="radio" id="dr_cr" name="dr_cr" value="1"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>

            <tr>
                <td>Attach a File</td>
                <td nowrap>
                    <input type="button" value=" Browse " id="btnBrowse" onclick="window.open('Popups/SaveFileForGJ.asp', '_blank', 'height=150, width=400, status=YES, resizable= Yes')">

                    <a id="aUploadFile" href="/GJ_Images/"
                        target="_blank" style="display: none">View File</a>

                </td>

            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>

            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td align="RIGHT">
                    <input type="HIDDEN" name="UPDATEID" id="UPDATEID">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td nowrap>
                                <input type="button" name="TemplateButton" value=" Select a Template " class="RSLButton" onclick="GetTemplateList()" tabindex="6">&nbsp;</td>
                            <td nowrap>
                                <input type="button" name="ResetButton" value=" RESET " class="RSLButton" onclick="ResetData()" tabindex="6">&nbsp;</td>
                            <td>
                                <input type="button" id="AddButton" name="AddButton" value=" ADD " class="RSLButton" onclick="AddRow(-1)" tabindex="6"></td>
                            <td>
                                <input type="button" id="AmendButton" name="AmendButton" value=" AMEND " class="RSLButton" onclick="UpdateRow()" style="display: none" tabindex="6"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <table border="0" align="center" valign="TOP" style='border: 1PX SOLID BLACK; behavior: url(/Includes/Tables/tablehl.htc)' cellspacing="0" cellpadding="3" width="95%" id="ItemTable" slcolor='' hlcolor="silver">
            <thead>
                <tr style='height: 10PX'>
                    <td width="50%" style='border-bottom: 1PX SOLID BLACK'><b>Account</b></td>
                    <td width="15%" style='border-bottom: 1PX SOLID BLACK'>&nbsp;</td>
                    <td width="15%" style='border-bottom: 1PX SOLID BLACK' align="RIGHT"><b>Debit</b></td>
                    <td width="15%" style='border-bottom: 1PX SOLID BLACK' align="RIGHT"><b>Credit</b></td>
                    <td style='border-bottom: 1PX SOLID BLACK'>&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                <tr id="EMPTYLINE">
                    <td colspan="5" align="center">Please enter an item from above</td>
                </tr>
            </tbody>
        </table>
        <table style='border: 1PX SOLID BLACK; border-top: none' cellspacing="0" cellpadding="2" width="95%" align="center">
            <tr bgcolor="whitesmoke" align="RIGHT">
                <td width="50%" style='border: none'>&nbsp;</td>
                <td width="15%" bgcolor="white" nowrap>
                    <b>
                        <input type="hidden" name="IACTION" id="IACTION" />
                        <input type="hidden" name="EXPENDITURE_LEFT_LIST" id="EXPENDITURE_LEFT_LIST" />
                        <input type="hidden" name="EMPLOYEE_LIMIT_LIST" id="EMPLOYEE_LIMIT_LIST" />
                        <input type="hidden" name="hid_TOTALCCLEFT" id="hid_TOTALCCLEFT" />
                        <input type="hidden" id="hd_BALANCE" value="0">
                        <input type="hidden" name="SELECTED_BLOCK" id="SELECTED_BLOCK" />
                        <input type="hidden" name="SELECTED_PROPERTY" id="SELECTED_PROPERTY" />
                        <input type="hidden" name="SELECTED_HEAD" id="SELECTED_HEAD" />
                        <input type="hidden" name="SELECTED_EXPENDITURE" id="SELECTED_EXPENDITURE" />
                        <input type="hidden" name="IS_CASCADE" id="IS_CASCADE" />

                        <div id="i_BALANCE">0.00</div>
                    </b>
                </td>
                <td width="15%" bgcolor="white">
                    <b>
                        <input type="hidden" id="hd_DEBITS" value="0">
                        <div id="i_DEBITS">0.00</div>
                    </b>
                </td>
                <td width="15%" nowrap bgcolor="white">
                    <b>
                        <input type="hidden" id="hd_CREDITS" value="0">
                        <div id="i_CREDITS">0.00</div>
                    </b>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br>
        <table cellspacing="0" cellpadding="2" width="755">
            <tr align="RIGHT">
                <td width="754" align="right" nowrap>
                    <input type="button" value=" Post &amp; Save as a Template " class="RSLButton" onclick="SaveJournal(1)">
                    &nbsp;
	<input type="button" value=" Post " class="RSLButton" onclick="SaveJournal()">&nbsp;&nbsp;&nbsp;</td>
            </tr>
        </table>
    </form>
    <iframe src="/secureframe.asp" name='GJ_FRM' id='GJ_FRM' style='width: 400:height:100; display: none'></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <div style="display:none;"><%=lstTemplates%></div>
</body>
</html>

