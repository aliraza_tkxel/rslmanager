<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="INCLUDES/functions.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance
	Dim AccountNumber, FullName, AccountType, amitop
	Dim startdate, enddate, openingbalance, searchstring, searchstringSQL

	Dim PS, PE, FY
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
    Dim TXNID

	Call OpenDB()

	FY = Request("FY")
	get_year(FY)					        ' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	get_account_details(Request("LID"))		' GET THE ACCOUNT HEADER INFORMATION
    TXNID = Request("TXNID")
	
	If Request("START") <> "" Then startdate = Request("START") Else startdate = PS End If
	If Request("END") <> "" Then enddate = Request("END")Else enddate = PE End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 25 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("SEARCHSTRING") = "" Then SearchFactor = "" Else SearchFactor = "%" & Replace(Request("SEARCHSTRING"), "'", "''") & "%" End If

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	' BUILD PAGED REPORT
	Function get_breakdown()

		AccountID = Request("LID")
		Dim strSQL, rsSet, intRecord

		str_data = ""
        'rw "EXEC NL_LEDGERDETAILS_ITEM " & CInt(intPage) & ", " & CONST_PAGESIZE & "," & TXNID & " "
        'Response.End()
		set rsPager = Conn.execute("EXEC NL_LEDGERDETAILS_ITEM " & CInt(intPage) & ", " & CONST_PAGESIZE & "," & TXNID & " ")
        intPageCount = rsPager("TOTALPAGES")
		intRecordCount = rsPager("TOTALROWS")
		intPage = rsPager("CURRENTPAGE")

		set rsSet = rsPager.NextRecordset()
		Set rsPager = Nothing

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		Response.Write "<tbody class='CAPS'>"

        ' CALCULATE BALANCE FOR TOP OF PAGE
		'VERSION 2  REWRITE (ALI)
		'SEPARATED THE CALLS TO OPENING BALANCE, PAGE BALANCE AND MONTH BALANCE.
		'INCLUDE FUNCTIONS ALSO MODIFIED.
		'if (SearchFactor = "") then
			    'Opening_Balance = get_opening_balance(AccountID, FY)
				page_balance = get_balance_byTXNID(intPage, startdate, enddate, AccountID, CONST_PAGESIZE, TXNID)
                'balance = Opening_Balance + page_balance
                running_balance = page_balance
			    'running_balance = balance
			' ENTER OPENING BALANCE
		'else
		'	Opening_Balance = 0
		'	page_balance = 0
		'	running_balance = 0
		'end if

        'rw Opening_Balance & "<br/>"
        'rw page_balance & "<br/>"
        'rw balance & "<br/>"
        'rw running_balance & "<br/>"

        credittotal = 0
		debittotal = 0
		count = 1

        Response.Write "<tr style='background-color:thistle'><td colspan=""4""><b>Brought Forward</b></td>" &_
			"<td align='right'><b>"&FormatNumber(Round(page_balance,2)*-1,2)&"</b></td></tr>" 

		while NOT rsSet.EOF
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
			str_data = "<tr valign=""top"" style=""cursor:pointer"&color&""" >" &_
				"<td>&nbsp;" & rsSet("TIMECREATED") & "</td>" &_
				"<td nowrap=""nowrap"">" & rsSet("DESCRIPTION") & "</td>" &_
				"<td title=""" & rsSet("DESCRIPTION") & " For Reference: "&rsSet("TENANCYID")&""">" & rsSet("TENANCYID") & "</td>"
				' SHOW DEBIT
			If rsSet("AMOUNT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
                debittotal = debittotal + cdbl(rsSet("AMOUNT"))
                'running_balance = running_balance + cdbl(rsSet("AMOUNT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"" style=""color:red"">"&FormatNumber(rsSet("AMOUNT"),2)&"</td>"
			End If
			' SHOW CREDIT
			If rsSet("AMOUNT") = 0 Then
				str_data = str_data & "<td nowrap=""nowrap""></td>"
			Else
                credittotal = credittotal + cdbl(rsSet("AMOUNT"))
                'running_balance = running_balance + cdbl(rsSet("AMOUNT"))
				str_data = str_data & "<td align=""right"" nowrap=""nowrap"" style=""color:blue"">"&FormatNumber(rsSet("AMOUNT"),2)&"</td>"
			End If

			' WRITE COLUMNS TO PAGE
			Response.Write str_data & "</tr>"
			count = count + 1
			rsSet.movenext()
		wend

        running_balance = (running_balance - credittotal)
        running_balance = running_balance * (-1)

		if (count = 1) then
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
			Response.Write "<tr style='" & color & "'><td colspan=""5"" align=""center""><b>No journal entries found.</b></td></tr>"
			count = count + 1
		end if
		
		'ensure table height is consistent with any amount of records
		Call fill_gaps()

		' END BALANCE
		Response.Write "<tr style='background-color:thistle'><td align=""right"" colspan=""3""><b> Total: </b></td>" &_
		"<td align=""right""><b>"&FormatNumber(Round(debittotal,2),2)&"</b></td><td align=""right""><b>"&FormatNumber(Round(credittotal,2),2)&"</b></td></tr>" 
        ' END BALANCE
		Response.Write "<tr style=""background-color:thistle""><td colspan=""4""><b></b></td>" &_
		"<td align=""right""><b>"&FormatNumber(Round(running_balance,2),2)&"</b></td></tr>"
        Response.Write "</tbody>"

		' LINKS
		Response.Write "<tfoot><tr><td colspan='5' style='border-top:2px solid #133e71' align='center'>" &_
		"<table cellspacing='0' cellpadding='0' width='100%'><thead><tr><td width='100'></td><td align='center'>" &_
		"<a href='LedgerItemDetail.asp?TXNID="&Request("TXNID")&"&SEARCHSTRING="&Request("SEARCHSTRING")&"&page=1&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color='blue'>First</font></b></a>&nbsp;" &_
		"<a href='LedgerItemDetail.asp?TXNID="&Request("TXNID")&"&SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color='blue'>Prev</font></b></a>" &_
		" Page <b><font color='red'>" & intpage & "</font></b> of " & intPageCount & ". Records: " & CONST_PAGESIZE * (intpage - 1) + 1 & "  to " & (intPage-1)*CONST_PAGESIZE+(count-1) &	" of " & intRecordCount &_
		"&nbsp;<a href='LedgerItemDetail.asp?TXNID="&Request("TXNID")&"&SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color='blue'>Next</font></b></a>&nbsp;" &_
		"<a href='LedgerItemDetail.asp?TXNID="&Request("TXNID")&"&SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & intPageCount & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color='blue'>Last</font></b></a>" &_
		"</td><td align='right' width='100'>Page:&nbsp;<input type='text' name='QuickJumpPage' id=""QuickJumpPage"" value='' size='2' maxlength='3' class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;<input type='button' name='btnGo' id='btnGo' value='GO' class='rslbutton' onclick='JumpPage()'>"  &_
		"</td></tr></thead></table></td></tr></tfoot>"

		rsSet.close()
		Set rsSet = Nothing
	End function

	' pads table out to keep the height consistent
	Function fill_gaps()
		Dim tr_num, cnt, innercount
		cnt = 0
		innercount = count
		tr_num = CONST_PAGESIZE - count
		while (cnt <= tr_num)
			if innercount mod 2 = 0 then color = "style='background-color:beige'" else color = "" end if
			Response.Write "<tr "&color&"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" &_
								  "<td>&nbsp;</td><td>&nbsp;</td></tr>"
			cnt = cnt + 1
			innercount = innercount + 1
		wend
	End Function
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Ledger Details - <%=FullName%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
</head>
<script language="JavaScript" type="text/javascript" src="/js/preloader.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/menu.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>
<script language="JavaScript" type="text/javascript">

var FormFields = new Array();
    FormFields[0] = "txt_FROM|FROM|DATE|Y"
    FormFields[1] = "txt_TO|TO|DATE|Y"
    FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"

function JumpPage()
{
	iPage = document.getElementById("QuickJumpPage").value
	if (iPage != "" && !isNaN(iPage))
		location.href = "LedgerItemDetail.asp?TXNID=<%=TXNID%>&SEARCHSTRING="+document.getElementById("txt_LIKE").value+"&page="+iPage+"&START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value+"&LID=<%=Request("LID")%>&FY=<%=FY%>" 
	else
		document.getElementById("QuickJumpPage").value = ""
}

function convertDate(strDate)
{
	strDate = new String(strDate);
	arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	arrSplit = strDate.split("/");
	return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
}

function click_go()
{
	if (!checkForm()) return false;	
	DateError = false
	if (convertDate(document.getElementById("txt_TO").value) > convertDate("<%=PE%>") )
    {
		ManualError("img_TO", "The TO date must be less than or equal to '<%=PE%>'.", 0)
		DateError = true
	}
	if (convertDate(document.getElementById("txt_FROM").value) < convertDate("<%=PS%>") )
    {
		ManualError("img_FROM", "The FROM date must be greater than or equal to '<%=PS%>'.", 0)
		DateError = true
	}
	if (DateError == true) return false;
	location.href = "LedgerItemDetail.asp?TXNID=<%=TXNID%>&SEARCHSTRING="+document.getElementById("txt_LIKE").value+"&page=1&START="+document.getElementById("txt_FROM").value+"&END="+document.getElementById("txt_TO").value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&FY=<%=FY%>&LID=<%=Request("LID")%>"
}

function XLSMe()
{
	if (!checkForm()) return false;
	window.open("ServerSide/XLS_LedgerItemDetail.asp?TXNID=<%=TXNID%>&SEARCHSTRING="+document.getElementById("txt_LIKE").value+"&START=" + document.RSLFORM.txt_FROM.value + "&END=" + document.RSLFORM.txt_TO.value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&FY=<%=FY%>");
}

</script>
<body bgcolor="white" text="#000000" onload="window.focus()" class="ta">
<form name="RSLFORM" method="post" action=""> 
<table width="750" cellpadding="1" cellspacing="1" align="center">
	<tr>
		<td style="border:1px solid black;background-color:thistle;color:white;height:15" width="100" valign="middle"><b>&nbsp;Account :</b></td>
		<td valign="middle">&nbsp;<%=AccountNumber %> - <%=FullName%>&nbsp;[<font color="blue"><%=AccountType%></font>]<br/></td>
		<td align="right" rowspan="2" valign="top">
		    <input type="button" name="btnExport" value="Export" class="rslbutton" onclick="XLSMe()" style="BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1px BLACK" />
            <input type="button" name="btnBack" value="Back" class="rslbutton" onclick="location.href='LedgerItem.asp?page=<%=intPage%>&pagesize<%=CONST_PAGESIZE%>&startdate=<%=startdate%>&enddate=<%=enddate%>&LID=<%=Request("LID")%>&FY=<%=FY%>&TXNID=<%=TXNID%>&SEARCHSTRING=<%=SEARCHSTRING%>'" style="BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1px BLACK" />
			<input type="button" name="btnClose" value="Close" class="rslbutton" onclick="window.close()" style="BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1px BLACK" />
        </td>
	</tr>
	<tr>
		<td style="border:1px solid black;background-color:thistle;color:white;height:15" width="100" valign="middle"><b>&nbsp;Period :</b></td>
		<td valign="middle">&nbsp;<%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></td>
	</tr>
</table>
<br/>
<table align="center" width="750" cellpadding="1" cellspacing="0" style='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc); border-collapse:collapse' slcolor='' border="1" hlcolor="thistle">
	<thead>
		<tr style="height:3px;border-bottom:1px solid #133E71">
		    <td colspan="5" class="table_HEAD"></td>
		</tr>
		<tr>
			<td align="right" colspan="5" class="table_HEAD">
			<!-- <span title='Enter any keyword/reference code to search for'><b>&nbsp;Search&nbsp;&nbsp; -->
			<input type="hidden" class="textbox" name="txt_LIKE" id="txt_LIKE" maxlength="12" size="12" tabindex="3" value='<%=Request("SEARCHSTRING")%>' />
			<img src="/js/FVS.gif" name="img_LIKE" id="img_LIKE" width="15px" height="15px" border="0" alt="" /></b></span>
			<!-- &nbsp;&nbsp;Dates:&nbsp;&nbsp;<b>From&nbsp;&nbsp; -->
			<input type="hidden" class="textbox" name="txt_FROM" id="txt_FROM" maxlength="12" size="12" tabindex="3" value='<%=STARTDATE%>' />
			<img src="/js/FVS.gif" name="img_FROM" id="img_FROM" width="15px" height="15px" border="0" alt="" />
			<!-- &nbsp;&nbsp;To&nbsp;&nbsp; -->
			<input type="hidden" class="textbox" name="txt_TO" id="txt_TO" maxlength="12" size="12" tabindex="3" value='<%=ENDDATE%>' />
			<img src="/js/FVS.gif" name="img_TO" id="img_TO" width="15px" height="15px" border="0" alt="" />
            &nbsp;&nbsp;Pagesize&nbsp;
			<input type="text" class="textbox" name="txt_PAGESIZE" id="txt_PAGESIZE" maxlength="4" size="6" tabindex="3" value="<%=CONST_PAGESIZE%>" />
			<img src="/js/FVS.gif" name="img_PAGESIZE" id="img_PAGESIZE" width="15px" height="15px" border="0" alt="" />
			&nbsp;&nbsp;
			<input type="button" id="BTN_GO" name="BTN_GO" value="Proceed" class="rslbutton" onclick="click_go()" /></b>&nbsp;&nbsp;
			</td>
		</tr>
		<tr> 
		  <td class='table_HEAD' style='width:80px' nowrap="nowrap">&nbsp;<b>Date</b> </td>
		  <td class='table_HEAD' style='width:35px' nowrap="nowrap"> <b>Type</b> </td>
		  <td class='table_HEAD' width="360"> <b>Tenancy/Property Reference</b> </td>
		  <td class='table_HEAD' style='width:90px' nowrap="nowrap" align="center"> <b>Debit</b> </td>
		  <td class='table_HEAD' style='width:90px' nowrap="nowrap" align="center"> <b>Credit</b> </td>
		</tr>
		<tr style='height:3px'>	
		    <td class='table_HEAD' colspan="5" align="center" style="border-bottom:2px solid #133E71"></td>
		</tr>
	</thead>
<%
Call get_breakdown()
Call CloseDB()
%>
	</table>
</form>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>