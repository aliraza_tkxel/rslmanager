<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Accounts </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="14">&nbsp;</td>
    <td> 
      <table width="100%" border="0" cellspacing="5" cellpadding="0" class="rslmanager">
        <tr> 
        <tr> 
          <td><b>Accounts</b></td>
          <td>&nbsp;</td>
        </tr>
          <td width="35%"><img src="../myImages/HoldingGraphics/Accounts.gif" width="295" height="283"></td>
          <td width="65%" valign="top"> 
            <p><br>
              The Accounts Module drives the allocation 
              of resources, the establishment of budgets and spending plans, monitors 
              and controls actual spending and expense allocations and delivers 
              essential MI on the progress and impact of each item of expenditure.</p>
            <p>Working alongside all other modules the Accounts Module ensures:</p>
            <ul>
              <li> Standard approaches to the fund creation and budget allocation 
                procedures</li>
              <li> Standard purchase ordering processes and authorisation for 
                recording and monitoring</li>
              <li>Automatic allocation of purchases to funds and budgets</li>
              <li>Recording of customer information identified to specific projects, 
                budgets and funds</li>
              <li>Real time reporting on planned and actual expenditure for individual 
                or collective funds</li>
              <li>Real time reporting on individual budgets within specified funds</li>
              <li>Constant management information on financial performance of 
                the Partnership </li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="14">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

