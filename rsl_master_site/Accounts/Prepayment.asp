<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>


<%
openDB()
Tsql = "NL_ACCOUNT A WHERE NOT EXISTS (SELECT * FROM NL_ACCOUNT WHERE PARENTREFLISTID = A.ACCOUNTID) "
Call BuildSelect(lstAccounts, "sel_ACCOUNT", Tsql, "A.ACCOUNTID, A.ACCOUNTNUMBER + ' ' + A.NAME AS NAME", "A.NAME", "Please Select...", NULL, "WIDTH:350PX", "textbox200", "tabindex=0")
Call BuildSelect(lstPrePayAccounts, "sel_PREPAYACCOUNT", "NL_ACCOUNT a where ACCOUNTID=372", "A.ACCOUNTID, A.ACCOUNTNUMBER + ' ' + A.NAME AS NAME", "A.NAME", "Please Select...", NULL, "WIDTH:350PX", "textbox200", "tabindex=1")
fiscalyear = Request("FiscalYear")
if (NOT isNumeric(fiscalyear)) then
	fiscalyear = ""
end if

set rsFYear = Server.CreateObject("ADODB.Recordset")
rsFYear.ActiveConnection = RSL_CONNECTION_STRING
if (fiscalyear = "" or isNull(fiscalyear)) then
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
else
	rsFYear.Source = "SELECT YRange, YStart, YEnd FROM dbo.F_FiscalYears WHERE YRange = " & fiscalyear 
end if
rsFYear.CursorType = 0
rsFYear.CursorLocation = 2
rsFYear.LockType = 3
rsFYear.Open()
if (rsFYear.EOF) Then
	rsFYear.close()
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
	rsFYear.Open()
End if
rsFYear_numRows = 0
fiscalyear = rsFYear("YRange")
yearstart = rsFYear("YStart")
yearend = rsFYear("YEnd")


MonthST = Month(CDate(yearstart))
MonthEN = Month(CDate(yearend))
rsFYear.close()
		
%>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/formValidation.js"></SCRIPT>

<script LANGUAGE="VBScript">
	function DateOverLap()
		dim d
		d=dateAdd("m",frm.sel_slMonts.selectedIndex,frm.txt_StartDATE.value)


		if dateDiff("m",  frm.txt_FEndDate.value,d ) > 1 or datediff("m",frm.txt_FStartDate.value,frm.txt_StartDATE.value)  <0 then
			msgbox "Start date must be with in the financial period"
			DateOverLap=false
		
		else
			DateOverLap=true
		end if


	end function
</SCRIPT>

<script LANGUAGE="JavaScript">

var FormFields = new Array()
var PrePayId=0
function locControls(status)
{
	frm.sel_ACCOUNT.readonly=status
	frm.sel_PREPAYACCOUNT.readonly=status
	frm.txt_value.readonly=status
	frm.txt_MNTHAMT.readonly=status
	frm.txt_details.readonly=status
	
}
function Sumbitpage()
{
	
	
	FormFields.length = 0
	FormFields[0] = "sel_ACCOUNT|Nominal Account|SELECT|Y"
	FormFields[1] = "sel_PREPAYACCOUNT|Prepayment Account|SELECT|Y"
	FormFields[2] = "txt_value|Value|CURRENCY|Y"
	FormFields[3] = "sel_slMonts|Months|SELECT|Y"
	FormFields[4] = "txt_MNTHAMT|Monthly Amount|CURRENCY|Y"
	FormFields[5] = "txt_details|Details|TEXT|Y"
	FormFields[6] = "txt_StartDATE|Start Date|DATE|Y"
	
	//if (DateOverLap()==false) return false
	
	if (parseFloat(frm.txt_value.value)<parseFloat(frm.txt_MNTHAMT.value))
	{
		alert("Monthy amount must be less than value")
		frm.txt_MNTHAMT.focus()
		return false
	}
	if (!checkForm()) 
		{
			return false
		}
	frm.submit()
	return true
}
function Get_Data(txnid,cnt)
	{
		
		
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var url="/Includes/xmlServer/xmlsrv.asp?id=" + txnid + "&task=PREPAYGETDATA";
		var nodeMap
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		xmlobj.async = false;
		
		var t=xmlobj.selectSingleNode("//rs:data/z:row");
		frm.hdn_ACCOUNT.value=t.getAttribute("NOMINALACC")
		frm.sel_ACCOUNT.value=t.getAttribute("NLDESC")
		frm.sel_PREPAYACCOUNT.value='Prepayments'
		frm.txt_value.value=t.getAttribute("VALUE")
		frm.sel_slMonts.value=t.getAttribute("NO_MONTHS")
		frm.txt_MNTHAMT.value=t.getAttribute("MONTH_AMT")
		frm.txt_details.value=t.getAttribute("DETAILS")
		frm.txt_StartDATE.value=t.getAttribute("STARTDATE")
		frm.txt_PONUMBER.value=t.getAttribute("PONUMBER")
		frm.hdn_ORDERITEMID.value=t.getAttribute("TRANSACTIONID")
		PrePayId=t.getAttribute("PREPAYID")
		frm.sButton.value="Update"
		frm.hdn_ID.value=PrePayId
		
		
	}
	
function Find_PO()
{
	FormFields[0] = "txt_PONUMBER|Purchase Order|INT|Y"
	if (!checkForm()) return false
	var xh=new ActiveXObject("Microsoft.XMLHTTP");
	var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
	var poNumber=frm.txt_PONUMBER.value
	var orderid=parseInt(poNumber)
	
	var url="/Includes/xmlServer/xmlsrv.asp?id=" + orderid + "&task=PREPAYGETDATAPO";
	var nodeMap
	xh.open("POST",url,false);
	xh.send();
	xmlobj=xh.responseXML;
	xmlobj.async = false;
	
	var l=xmlobj.selectSingleNode("//rs:data");
	
	if (  l.childNodes.length > 2 )
	{
		alert("Prepayment can not be set for Purchase orders with more than one item")
		return false
	}
	if ( parseInt(xmlobj.selectSingleNode("//rs:data/z:row").getAttribute("ORDERID"))==0   )
	{
		alert("Purchase orders does not exist")
		return false	
	}
	var t=xmlobj.selectSingleNode("//rs:data/z:row");
	
	
	frm.hdn_ACCOUNT.value=t.getAttribute("NOMINALACC")
	frm.sel_ACCOUNT.value=t.getAttribute("NLDESC")
	frm.txt_value.value=t.getAttribute("VALUE")
	frm.txt_details.value=t.getAttribute("DETAILS")
	frm.txt_StartDATE.value=t.getAttribute("STARTDATE")
	frm.hdn_ORDERITEMID.value=t.getAttribute("TRANSACTIONID")
	//locControls (true)
}
function No_Change()
{
	if (frm.sel_slMonts.value != 0)
	frm.txt_MNTHAMT.value=FormatCurrency(parseFloat(frm.txt_value.value)/frm.sel_slMonts.value,2)
}

</script>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages();<% if request("id") > 0 Then %> Get_Data(<%=request("id")%>) <% End If %>" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  


	
	
	
<TABLE WIDTH=480 BORDER=0 CELLPADDING=0 CELLSPACING=0 height=26>
  <TR>
			
		  
    <TD ROWSPAN=2 width="133"><img src="Images/tab_prepayment102.GIF" width="100" height="23" border=0></TD>
			
    <TD width="461"><IMG SRC="images/spacer.gif" WIDTH=292 HEIGHT=19></TD>
		</TR>
		<TR>
			
    <TD BGCOLOR=#133E71 width="400"><IMG SRC="images/spacer.gif" WIDTH=392 HEIGHT=1></TD>
		</TR>
		<tr> 
			<td colspan=2 style="border-left:1px solid #133e71; border-right:1px solid #133e71"> <img src="images/spacer.gif" width=53 height=6></td>
		</tr>	
	</TABLE>
	 	
	
	  <table cellspacing=0 WIDTH=400 height=220PX cellpadding=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71'>
	 
        <form name=frm method=post action=serverSide/PrePaymentServer.asp>
		 <tr> 
            
      <td nowrap width="117" ><b>Purchase Order:</b> </td>
            
      <td width="449" >  <input type=text name="txt_PONUMBER" maxlength=20 size=15 class="textbox200" tabindex=2 style='width:100'> &nbsp;&nbsp;&nbsp; 
            <input type=button name=poFind class="RSLButton" value=" Find " onClick="return Find_PO()"></td>
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_PONUMBER"></td>
          </tr>
          <tr> 
            
      <td nowrap width="117" ><b>Nominal Account:</b> </td>
            
      <td width="449" > <input type=text name="sel_ACCOUNT" maxlength=120 class="textbox200" tabindex=2 style='width:350' readOnly> 
	  <input type=hidden name="hdn_ACCOUNT" maxlength=120 class="textbox200" tabindex=2 style='width:10' >
	  	</td>
            
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_ACCOUNT"></td>
          </tr>
          <tr> 
            
      <td width="117" ><b>Prepayment Account:</b></td>
            
      <td width="449"><input type=text name="sel_PREPAYACCOUNT" maxlength=120 class="textbox200" tabindex=2 style='width:350' readOnly value="Prepayments"> </td>
            
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_PREPAYACCOUNT"></td>
          </tr>
		  <tr>
		  	<td colspan="2">
			
			</td>
		  </tr>
          <tr> 
            
      <td nowrap width="117" ><b>Value:</b></td>
            
      <td width="449"> 
        <input type=text name="txt_value" maxlength=20 size=15 class="textbox200" tabindex=2 style='width:72' readOnly>
		<input type=hidden name="txt_FStartDate" maxlength=20 size=15 class="textbox200" tabindex=2 style='width:72'  value=<%=yearstart %>>
		<input type=hidden name="txt_FEndDate" maxlength=20 size=15 class="textbox200" tabindex=2 style='width:72'  value=<%=yearend %>>
			</td>
			
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_value"></td>
          </tr>
          
          <tr> 
            
      <td title="Date" width="117" ><b>Months:</b></td>
            
      <td width="449">
	  	<select name="sel_slMonts" class='textbox200' tabindex=3 style='width:72' onChange="No_Change()">
			<option value="0">Please Select</option>
			<%
				for i=1 to 12
					response.write("<option value=" & i &">" & i & "</option>")
				next 
				
			%>
		</select>
       
			</td>
			
      	<td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_slMonts"></td>
          </tr>
        <tr>
		
		  <td width="117"> <b>Monthly Amount: </b> </td>
				
		  <td width="449"> 
	
			<input type=text name="txt_MNTHAMT" maxlength=20 size=15 class="textbox200" tabindex=4 style='width:72'>
				</td>
				
		  <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_MNTHAMT"></td>
		</tr>
	  <tr>
		<tr>
		
		  <td width="117"> <b>Start Date: </b> </td>
				
		  <td width="449"> 
	
			<input type=text name="txt_StartDATE" maxlength=20 size=15 class="textbox200" tabindex=4 style='width:72' value="<%=FormatDateTime(Date,2)%>">
			</td>
				
		  <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_StartDATE"></td>
		</tr>
	  <tr>
		  <td width="117"> <b>Details: </b> </td>
				
		  <td width="449"> 
	
			<textarea name="txt_details" maxlength=200 size=35 class="textbox200" Style="width:350; height=50"></textarea>
				</td>
				
		  <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_details"></td>
	 </tr>
		 
		
			<tr>
			
      <td width="117"> </td>
			
      <td width="449"> 
        <input type=reset name=reset class="RSLButton" value=Reset>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type=button name=sButton class="RSLButton" value=Submit onClick="return Sumbitpage()">
			</td>
		</tr>
		<input type="hidden" name="hdn_ID"  value="0">
		<input type="hidden" name="hdn_ORDERITEMID"  value="0">
		
        </form>
      </table>

    </body>

<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<%

%>