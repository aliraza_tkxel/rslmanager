<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Call OpenDB()
Call BuildSelect(lstBANK, "sel_Bank", "nl_account where isbankable=1", "ACCOUNTID, [DESC]", "ACCOUNTNUMBER", "Please Select", NULL, NULL, "textbox200", "datasrc=""#Srvxml"" datafld=""Bank"" STYLE='WIDTH:170' tabindex=0")
Call BuildSelect(lstCompany, "sel_Company", "G_Company", "COMPANYID, DESCRIPTION", "COMPANYID", null, rqCompany, NULL, "textbox200", "datasrc=""#Srvxml"" datafld=""Company"" style='width:150px'")	
 
OptionString = ""
SQL = "SELECT SCHEMEID AS  DEVELOPMENTID, SCHEMENAME FROM P_SCHEME ORDER BY SCHEMENAME"
Call OpenRs(rsSCH, SQL)
while NOT rsSCH.EOF
	OptionString = OptionString & "<OPTION VALUE=""" & rsSCH("DEVELOPMENTID") & """>" & rsSCH("SCHEMENAME") & "</OPTION>"
	rsSCH.moveNext
wend
Call CloseRs(rsSCH)

'!!!WARNING YOU MUST MAKE SURE THAT THE BANK ACCOUNTS ARE NOT SELECTABLE BY THE NOMINAL ACCOUNT LIST OTHERWISE BAD STUIFF WILL HAPPEN IN BANK REC AREA
Call BuildSelect(lstPMethod, "sel_PMethod", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN(8,5,3,2,9)", "PAYMENTTYPEID, [DESCRIPTION]", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", "datasrc=""#Srvxml"" datafld=""PMethod"" STYLE='WIDTH:100' tabindex=2")
Tsql = "NL_ACCOUNT A WHERE NOT EXISTS (SELECT * FROM NL_ACCOUNT WHERE PARENTREFLISTID = A.ACCOUNTID)and isbankable=0 "
Call BuildSelect(lstAccounts, "sel_ACCOUNT", Tsql, "A.ACCOUNTID, A.ACCOUNTNUMBER + ' ' + A.NAME AS NAME", "A.ACCOUNTNUMBER", "Please Select...", NULL, "WIDTH:250PX", "textbox200", "datasrc=""#Srvxml"" datafld=""NLAcc"" tabindex=7")

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED 
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	SQL = "EXEC GET_VALIDATION_PERIOD 17"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

%>
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=5" />
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>

<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<script language="JavaScript">
	var FormFields = new Array()
	var index=0
	var firstRowDeleted=0
	var ammedRowid,TabRowId
	
	function ResetRowid()
	{
		document.getElementById("hdn_rowid").value=0
	}
	
	function setTotal()
	{
		var d =xmlTable.XMLDocument.documentElement.childNodes.length
		//var t=pttyCash.XMLDocument.documentElement.selectSingleNode("//records")
		var grs,grossTot=0
		var nCost=0;
		var NetAmt
		for (i=0; i< d;i++)
		{
			NetAmt=xmlTable.XMLDocument.documentElement.childNodes.item(i).selectSingleNode("netAmt").text
			NetAmt=NetAmt.replace(/\,/g,"")
			
			nCost=nCost+  parseFloat(NetAmt)
			grs=parseFloat(NetAmt) + parseFloat(NetAmt)/100*20
			grossTot=grs + grossTot
		}
		
		document.getElementById("netTOT").innerHTML=FormatCurrencyComma(nCost)
		//document.getElementById("netGRSS").innerHTML=FormatCurrencyComma(grossTot)
	}
	
	function ResetNL()
	{
		Srvxml.XMLDocument.documentElement.selectSingleNode("NLAcc").text=""
		Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text=""
		Srvxml.XMLDocument.documentElement.selectSingleNode("GrssAmt").text=""
		Srvxml.XMLDocument.documentElement.selectSingleNode("Detail").text=""
	}
	
	function ResetData()
	{
		Srvxml.XMLDocument.documentElement.selectSingleNode("NLAcc").text=""
		Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text=""
		Srvxml.XMLDocument.documentElement.selectSingleNode("GrssAmt").text=""
		Srvxml.XMLDocument.documentElement.selectSingleNode("Detail").text=""
		frm.AddButton.value=" ADD "
	}
	
	function CreatePayment()
	{
		setChecking(2)
		
		
		// MAKE SURE ORDER DATE IS IN CURRENT FISCAL YEAR
		var YStart = new Date("<%=YearStartDate%>")
		var YEnd = new Date("<%=YearendDate%>")
		if ( (real_date(frm.txt_DATE.value) < YStart) || (real_date(frm.txt_DATE.value) > YEnd) ){

			//alert("Please enter a date Between '<%=YearStartDate%>' and '<%=YearendDate%>'");
            alert("The month is now locked down and entries can only be made for the current month");
            return false;
		}
		// END ------------------------------------------
//return false
		if (!checkForm()) return false		
		//FIXED BY ZANFAR -- HAD TO REMOVE THE COMMA FROM NETOT OTHERWISE THE THING WOULDNT WORK.
		if (parseFloat(document.getElementById("txt_AMT").value.replace(/\,/g, "")) <= 0)
			{
					alert("The control amount must be greater than zero.\nPlease amend the value to continue.")
					return false	
			}		
		if (parseFloat(document.getElementById("netTOT").innerHTML.replace(/\,/g, "")) != parseFloat(document.getElementById("txt_AMT").value.replace(/\,/g, "")))
			{
					alert("The totals of the item amounts must be equal to control amount.\nPlease add/amend/delete the items appropriately to continue.")
					return false	
			}
			
		var url,srchString
		var i;		
		var xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var d =xml2Post.XMLDocument.documentElement.childNodes.length
		if (d>0) // Check at least one record in xml2Post xml
		{
			
			for(i=0;i<d;i++)
			{
                srchString = "//record[@rowid=" + i + "]/Company"
                xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text = document.getElementById("sel_Company").value
                srchString = "//record[@rowid=" + i + "]/Bank"
				xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text=document.getElementById("sel_Bank").value
				srchString="//record[@rowid=" + i + "]/Date"
				xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text=document.getElementById("txt_DATE").value
				srchString="//record[@rowid=" + i + "]/PMethod"
				xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text=document.getElementById("sel_PMethod").value
				srchString="//record[@rowid=" + i + "]/BSlip"
				xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text=document.getElementById("txt_BSLP").value
				srchString="//record[@rowid=" + i + "]/From"
				xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text=document.getElementById("txt_FROM").value
				srchString="//record[@rowid=" + i + "]/development"
				xml2Post.XMLDocument.documentElement.selectSingleNode(srchString).text=document.getElementById("sel_Development").value
	
			}
			
			if ( frm.AddButton.value =" ADD ") // First Insert
			{
					url="/Includes/xmlServer/xmlsrv.asp?id=0" + "&task=MISPAYMENTSAVE";
			}
			
			xmlHttp.open("POST", url, false);
			xmlHttp.setRequestHeader( "Content-Type","text/xml")
			xmlHttp.send(xml2Post.XMLDocument);
			xmlobj=xmlHttp.responseXML
			var msg=xmlobj.xml
			
			alert("Transaction Reference Number: " + xmlobj.selectSingleNode("output").text)
			location.href="MislaniousIncome_dtl.asp"
		}
	}

	function real_date(str_date){
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");
		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		}	
		
	function setGrossAmt()
	{
		var gAmt=parseFloat(document.getElementById("txt_NETAMT").value)+ parseFloat(document.getElementById("txt_NETAMT").value)/100*20
		document.getElementById("txt_GAMT").value=FormatCurrencyComma(gAmt)
		Srvxml.XMLDocument.documentElement.selectSingleNode("//GrssAmt").text=FormatCurrencyComma(document.getElementById("txt_GAMT").value)
	}

	function setChecking(DataSet)
	{
		if (DataSet == 1) {
			FormFields.length = 0
			FormFields[0] = "sel_Bank|Bank|SELECT|Y"
			FormFields[1] = "txt_DATE|Date|DATE|Y"
			FormFields[2] = "sel_PMethod|Payment Method|SELECT|Y"
			FormFields[3] = "txt_FROM|From|TEXT|Y"
			FormFields[4] = "txt_AMT|Amount|CURRENCY|Y"
			FormFields[5] = "sel_ACCOUNT|Nominal Account|SELECT|Y"
            FormFields[6] = "txt_NETAMT|Net Amount|CURRENCY|Y"
           // FormFields[7] = "sel_COMPANY|Company|SELECT|Y"
			}
		else if (DataSet == 2) {
			FormFields.length = 0
			FormFields[0] = "sel_Bank|Bank|SELECT|Y"
			FormFields[1] = "txt_DATE|Date|DATE|Y"
			FormFields[2] = "sel_PMethod|Payment Method|SELECT|Y"
			FormFields[3] = "txt_FROM|From|TEXT|Y"
            FormFields[4] = "txt_AMT|Amount|CURRENCY|Y"
           // FormFields[5] = "sel_COMPANY|Company|SELECT|Y"
			}
	}

	function LockLeftHadside(status)
	{
        frm.sel_COMPANY.disabled = status
        frm.sel_Bank.disabled = status
		frm.sel_PMethod.disabled=status
		frm.txt_FROM.disabled=status
		frm.txt_DATE.disabled=status
		frm.txt_BSLP.disabled=status
		frm.txt_AMT.disabled=status
	}

	function AddRow(ref)
	{
		setChecking(1)
		if (!checkForm()) return false
		if (parseFloat(document.getElementById("txt_NETAMT").value) <= 0) {
			alert("The Net amount must be greater than zero.\nPlease amend the value to continue.")
			return false;
			}
						
		var srchStr,Grsamt
		
		if (ref.value==" ADD ")
		{	
			if (firstRowDeleted==0)
			{
				var srchStr="//record[@rid=-1]" 
				var d =xmlTable.XMLDocument.documentElement.selectSingleNode(srchStr)
				xmlTable.XMLDocument.documentElement.removeChild(d)
				firstRowDeleted=1
			}
			var insRecord = xmlAddItem.XMLDocument.documentElement.setAttribute("rid",index)
			xmlAddItem.XMLDocument.documentElement.selectSingleNode("//Account").text=document.getElementById("sel_ACCOUNT").options[document.getElementById("sel_ACCOUNT").selectedIndex].text
			xmlAddItem.XMLDocument.documentElement.selectSingleNode("//netAmt").text=FormatCurrencyComma(document.getElementById("txt_NETAMT").value)
			xmlAddItem.XMLDocument.documentElement.selectSingleNode("//GrossAmt").text=FormatCurrencyComma(document.getElementById("txt_GAMT").value)
			xmlAddItem.XMLDocument.documentElement.selectSingleNode("//rowid").text=document.getElementById("hdn_rowid").value
			insRecord = xmlAddItem.XMLDocument.documentElement.cloneNode(true);
			xmlTable.XMLDocument.documentElement.appendChild(insRecord);
			// Add data to xml2Post xml
			//var insRecord = Srvxml.XMLDocument.documentElement.setAttribute("rid",index)
			var insRecord = Srvxml.XMLDocument.documentElement.setAttribute("rowid",document.getElementById("hdn_rowid").value)
			insRecord = Srvxml.XMLDocument.documentElement.cloneNode(true);
			xml2Post.XMLDocument.documentElement.appendChild(insRecord);
			document.getElementById("hdn_rowid").value=parseInt(document.getElementById("hdn_rowid").value)+1
			index=index+1
			ResetNL()
		}

		if (ref.value=="AMEND")
		{
			srchStr="//record[@rowid=" + ammedRowid + "]/NLAcc"
			xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text=Srvxml.XMLDocument.documentElement.selectSingleNode("NLAcc").text
			srchStr="//record[@rid=" + TabRowId + "]/Account"
			
			xmlTable.XMLDocument.documentElement.selectSingleNode(srchStr).text=frm.sel_ACCOUNT.options[frm.sel_ACCOUNT.selectedIndex].text
			
			srchStr="//record[@rowid=" + ammedRowid + "]/Net"
			xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text=Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text
			srchStr="//record[@rid=" + TabRowId + "]/netAmt"
			xmlTable.XMLDocument.documentElement.selectSingleNode(srchStr).text=FormatCurrencyComma(document.getElementById("txt_NETAMT").value)
			Grsamt=parseFloat(Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text)
			frm.txt_GAMT.value=Grsamt+Grsamt/100*20
			srchStr="//record[@rid=" + TabRowId + "]/GrossAmt"
			xmlTable.XMLDocument.documentElement.selectSingleNode(srchStr).text=FormatCurrencyComma(document.getElementById("txt_GAMT").value)

			srchStr="//record[@rowid=" + ammedRowid + "]/GrssAmt"
			xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text=parseFloat(document.getElementById("txt_NETAMT").value) +parseFloat(document.getElementById("txt_NETAMT").value) /100*20

			srchStr="//record[@rowid=" + ammedRowid + "]/Detail"
			xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text=Srvxml.XMLDocument.documentElement.selectSingleNode("Detail").text
			frm.AddButton.value=" ADD "
			ResetNL()
		}
		setTotal()
	}
	
	function SetAttribute(ref,Action)
	{
			ref.setAttribute("Iamclicked", 1)
			var myCollection=document.getElementsByName(ref.name) 
			var srchStr,d,strReset,RowId
			if (firstRowDeleted==1)
			{
			if (myCollection.length)
			{
				for (i=0;i<myCollection.length;i++)
				{
					if (myCollection[i].getAttribute("Iamclicked") == 1)	
					{
						ref.removeAttribute("Iamclicked")
						if (Action==1)//Delete Row
						{
							if (firstRowDeleted==1)
							{
								/*Remove the selected record*/
								srchStr="//record[@rid=" + i + "]"
								attrSrch="//record[@rid=" + i + "]/rowid"
								d =xmlTable.XMLDocument.documentElement.selectSingleNode(srchStr)
								RowId=parseInt(xmlTable.XMLDocument.documentElement.selectSingleNode(attrSrch).text)
								xmlTable.XMLDocument.documentElement.removeChild(d)
								/* Reset rid's of those recoreds after the currently deleted one*/
								index=index-1
								for(j=i+1;j<=myCollection.length;j++)
								{
									strReset="//record[@rid=" + j + "]"
									d =xmlTable.XMLDocument.documentElement.selectSingleNode(strReset)
									d.setAttribute("rid",j-1)
								}
								srchStr="//record[@rowid=" + RowId + "]/deleted"
								d =xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text=1
								setTotal()
							}
						}
						if (Action==2)//Amend Row
						{
								var Grsamt
								srchStr="//record[@rid=" + i + "]/rowid"
								RowId=parseInt(xmlTable.XMLDocument.documentElement.selectSingleNode(srchStr).text)
								ammedRowid=RowId
								TabRowId=i
								
								srchStr="//record[@rowid=" + RowId + "]/NLAcc"
								
								document.getElementsByName("sel_ACCOUNT").value=parseInt(xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text)
								Srvxml.XMLDocument.documentElement.selectSingleNode("NLAcc").text=parseInt(xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text)

								srchStr="//record[@rowid=" + RowId + "]/Net"
								document.getElementsByName("txt_NETAMT").value=FormatCurrency(parseFloat(xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text))
								Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text=FormatCurrency(parseFloat(xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text))
								Grsamt=parseFloat(Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text)
								frm.txt_GAMT.value=Grsamt+Grsamt/100*20

								srchStr="//record[@rowid=" + RowId + "]/Detail"
								document.getElementsByName("txt_detail").value=xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text
								Srvxml.XMLDocument.documentElement.selectSingleNode("Detail").text=xml2Post.XMLDocument.documentElement.selectSingleNode(srchStr).text
								frm.AddButton.value="AMEND"
						}
					}
				}
			}
		}		
		if (xmlTable.XMLDocument.documentElement.childNodes.length==0)
		{
			var insRecord = FirstRow.XMLDocument.documentElement.cloneNode(true);
			xmlTable.XMLDocument.documentElement.appendChild(insRecord);
			firstRowDeleted=0
			Srvxml.XMLDocument.documentElement.selectSingleNode("NLAcc").text=""
			Srvxml.XMLDocument.documentElement.selectSingleNode("Net").text=""
			Srvxml.XMLDocument.documentElement.selectSingleNode("Detail").text=""
			frm.AddButton.value=" ADD "
		}
	}
</script>

<BODY BGCOLOR=#FFFFFF  ONLOAD="initSwipeMenu(4);preloadImages();ResetRowid()" onUnload="macGo()"  MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">

<xml id="FirstRow">
	<record rid="-1">
		<Account> Please add items..</Account>
		<netAmt></netAmt>
		<GrossAmt></GrossAmt>
		<rowid></rowid>
	</record>
</xml>
<xml id="xmlAddItem">
		<record rid="-1">
			<Account></Account>
			<netAmt></netAmt>
			<GrossAmt></GrossAmt>
			<rowid></rowid>
		</record>
</xml>
<xml id="xmlTable">
	<root>
		<record rid="-1">
			<Account> Please add items..</Account>
			<netAmt></netAmt>
			<GrossAmt></GrossAmt>
			<rowid>0</rowid>
		</record>
	</root>
</xml>
<xml id="Srvxml">
	<record rowid="0">
        <Company></Company>
		<Bank></Bank>
		<Date><%=formatDateTime(Date,2)%></Date>
		<PMethod></PMethod>
		<BSlip></BSlip>
		<From></From>
		<NLAcc></NLAcc>
		<Net></Net>
		<GrssAmt></GrssAmt>
		<Detail></Detail>
		<deleted>0</deleted>
		<development>-1</development>
	</record>
</xml>
<xml id="xml2Post">
	<root></root>
</xml>
<!--#include virtual="Includes/Tops/BodyTop.asp" --> 
<FORM NAME="frm">
<CENTER>
<TABLE style='width=750' cellspacing=0 cellpadding=0>
	<TR><TD STYLE='FONT-SIZE:14PX'><b>MISCELLANEOUS PAYMENT</b></TD></TR>
	<TR>
      <TD> 
		<!-- BANK DETAILS TABLE -----START------->
        <table cellspacing=0 cellpadding=3  style='BORDER:1PX SOLID BLACK;height=117'>
          <tr bgcolor='steelblue' style='color:white'><TD colspan=6><b>BANK DETAILS</b></TD></tr>
          <tr> 
            <td style='WIDTH:50'>Company</td>
            <td style='WIDTH:170'><%=lstCompany%></td>
            <td style='WIDTH:16'></td>
            <td style='WIDTH:50'></td>
            <td style='WIDTH:70'></td>
            <td style='WIDTH:16'></td>
          </tr>
            <tr> 
            <td style='WIDTH:50'>Bank</td>
            <td style='WIDTH:170' ><%=lstBANK%></td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_Bank"></td>
            <td style='WIDTH:50'>Date</td>
            <td style='WIDTH:70'> 
              <input name="txt_DATE" type="text" datasrc="#Srvxml" datafld="Date"  class="TEXTBOX200" tabindex=1 style='WIDTH:70' size=10 maxlength=10 value="<%=FormatDateTime(Date,2)%>">
            </td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_DATE"></td>
          </tr>
          <tr> 
            <td style='WIDTH:50'>Payment method</td>
            <td style='WIDTH:170'><%=lstPMethod%></td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_PMethod"></td>
            <td style='WIDTH:50'>Cheque / Slip No.</td>
            <td style='WIDTH:70'> 
              <input  name="txt_BSLP" type="text" class="TEXTBOX200"  datasrc="#Srvxml" datafld="BSlip"tabindex=3 style='WIDTH:70' size=10 >
            </td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_BSLP"></td>
          </tr>
          <tr> 
            <td style='WIDTH:50'>From</td>
            <td > 
              <input name="txt_FROM" type="text" class="TEXTBOX200" tabindex=4  datasrc="#Srvxml" datafld="From" style='WIDTH:170' size=10 >
            </td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_FROM"></td>
            <td style='WIDTH:50'>Control Amount</td>
            <td style='WIDTH:70'> 
              <input name="txt_AMT" type="text" class="TEXTBOX200" tabindex=5 style='WIDTH:70' size=10 >
            </td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_AMT"></td>
          </tr>
          <tr> 
            <td style='WIDTH:50'>Scheme</td>
            <td colspan=4> 
			 <SELECT NAME="sel_Development" ID="sel_Development" class="textbox200" datasrc="#Srvxml" datafld="development" STYLE='WIDTH:310' tabindex=6>
			 	<OPTION VALUE="-1">Please Select</OPTION>
				<%=OptionString%>
			 </SELECT>
              <input name="hdn_rowid" type="hidden" class="TEXTBOX200"  style='WIDTH:10'   value="0">
            
          
            <input name="hdn_AMT" type="hidden" class="TEXTBOX200" tabindex=5 style='WIDTH:70' size=10 >
			</td>
            <td style='WIDTH:16'><img src="/js/FVS.gif" width=15 height=15 name="img_Development"></td>
          </tr>
        </table>
        <!-- BANK DETAILS TABLE -----END------->
      </TD>
	  <TD>
	  <!-- NOMINAL DETAILS TABLE -----START------->
		<TABLE  cellspacing=0 cellpadding=3  style='BORDER:1PX SOLID BLACK;height=116'>
             <tr bgcolor='steelblue' style='WIDTH:500;color:white'><TD colspan=6><b>NOMINAL DETAILS</b></TD></tr>
			 <TR>
				<TD style='WIDTH:50'>Nominal account</TD><TD style='WIDTH:250'><%=lstAccounts%></TD>
				<TD><img src="/js/FVS.gif" width=15 height=15 name="img_ACCOUNT"></TD>
			</TR>
			<TR>
				<TD style='WIDTH:50'>Net amount</TD><TD style='WIDTH:250'><input  name="txt_NETAMT" type="text" class="TEXTBOX200" tabindex=8 style='WIDTH:100' size=10 datasrc="#Srvxml" datafld="Net" onBlur="setGrossAmt()"></TD>
				<TD><img src="/js/FVS.gif" width=15 height=15 name="img_NETAMT"></TD>
			</TR>
			
			<TR>
				<TD style='WIDTH:50'>Details</TD><TD style='WIDTH:250'><input name="txt_detail" type="text" class="TEXTBOX200" tabindex=9 style='WIDTH:250' size=10 datasrc="#Srvxml" datafld="Detail" ></TD>
				<TD></TD>
			</TR>
			<TR>
				<TD style='WIDTH:50'></TD><TD style='WIDTH:250'><input name="txt_GAMT" type="hidden" class="TEXTBOX200" tabindex=100 style='WIDTH:100' size=10 datasrc="#Srvxml" datafld="GrssAmt" readonly></TD>
				<TD></TD>
			</TR>
			<tr>
				<td colspan=2 align=right>
					<input type=button Name="ResetButton" value=" RESET " class="RSLButton" onclick="ResetData()">&nbsp;
					<input type=button Name="AddButton" value=" ADD " class="RSLButton" onclick="AddRow(this)">
				</td>
			</tr>
		</TABLE>
		<!-- NOMINAL DETAILS TABLE -----end------->
	  </TD>
	</TR>
</TABLE>
<br>
<table height=40 cellspacing=0 cellpadding=0>
	<tr>
		<td valign=top bordercolor="steelblue"> 
          <!-- START OF XML TABLE WHICH DISPLAYS ITEMS -->
          <table  datasrc="#xmlTable" ID="tblDisplay" cellspacing="0" cellpadding=3 width='750' STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor="whitesmoke">
				 <thead>
					<tr bgcolor='steelblue' style='color:white'>
						<td style='WIDTH:500' ><b>NOMINAL ACCOUNT</b></td>
						<td STYLE='WIDTH:100' align=RIGHT><b>NET AMOUNT</b></td>
						<td STYLE='WIDTH:15;'>&nbsp;</td>
					</tr>
				 </thead>
				 <tbody>
					<tr style='cursor:hand' valign=middle>
						<td style='WIDTH:500'> <input type="text" class="TEXTBOX200"  datafld="Account" name="tbAccs" onclick="SetAttribute(this,2)"  style='WIDTH:500;border:none;cursor:hand;background-color:transparent;' readonly></td>
						<td style='WIDTH:90' align=RIGHT><input type="text" class="TEXTBOX200" datafld="netAmt" name="tbNet" onclick="SetAttribute(this,2)"  style='text-Align:right;WIDTH:100;border:none;cursor:hand;background-color:transparent;' readonly></td>
						<td style='WIDTH:15;background-color:white' >
							<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick="SetAttribute(this,1)" name="img_DEL">
						</td>
					</tr>
				 </tbody> 
			</table>
			<!-- END OF XML TABLE WHICH DISPLAYS ITEMS -->
		</td>
	</tr>
</table>
<!-- START OF TOTALLING TABLE -->
<table width='750' cellspacing=0 cellpadding=3 style='BORDER-left:1PX SOLID BLACK;BORDER-right:1PX SOLID BLACK;BORDER-bottom:1PX SOLID BLACK;' >
	<tr bgcolor='steelblue' style='color:white'>
	  <td style='WIDTH:500' align="right" width="600"><b>Total :</b></td>
	  <td style='WIDTH:90' align=RIGHT ><b><div id="netTOT">0.00</div></b></td>
	  <td style='WIDTH:15' align=left >&nbsp;</td>
	</tr>
</table>
<table width=750>
	<tr>
		<td aligh="right"><p align="right"><input  type="button" name="btnSave" value=" CREATE PAYMENT " class="RSLButton" onClick="return CreatePayment()"></p></td>
	</tr>
</table>
<!-- END OF TOTALLING TABLE -->
</CENTER>
</FORM>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
