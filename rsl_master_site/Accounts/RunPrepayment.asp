<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<HTML>
<HEAD>
<%
	dim dbcmd
	set dbcmd= server.createobject("ADODB.Command")
	
	dim msgTxt,Req
	req=request("task")
	
	if req="" then 
		req="no"
	end if
	msgTxt="Run PrePayment posting for the following items?."
	
	if req="yes" then
		dbcmd.ActiveConnection=RSL_CONNECTION_STRING
		dbcmd.CommandType = 4
		dbcmd.CommandText = "F_RUN_PREPAYMENT_NEW"
		dbcmd.execute
		msgTxt="Prepayment posting completed successfully."
	end if


	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match	
	Dim TDFunc		   (3)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
		
	ColData(0)  = "Start Date|STARTDATE|80"
	SortASC(0) 	= "STARTDATE ASC"
	SortDESC(0) = "STARTDATE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "N/L Account|NlAcc|250"
	SortASC(1) 	= "NL_ACCOUNT.[DESC] ASC"
	SortDESC(1) = "NL_ACCOUNT.[DESC] DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Monthly Amount|v|80"
	SortASC(2) 	= "NL_ACCRUALS.MONTH_AMT ASC"
	SortDESC(2) = " NL_ACCRUALS.MONTH_AMT DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""
			
	ColData(3)  = "No: Months Missed|MisCount|50"
	SortASC(3) 	= "MisCount ASC"
	SortDESC(3) = "MisCount DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""
	
	
	
	PageName = "RunPrepayment.asp"
	EmptyText = "No Relevant Prepayment List found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

	


	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	
	SQLCODE="select STARTDATE,PREPAYID,NOMINALACC,TRANSACTIONID,RUNCOUNT,no_months,value v,DETAILS,dbo.GetMissingPrepaycount(PREPAYID,getdate()) as MisCount,a.[DESC] as NlAcc " &_
			" from NL_PREPAYMENT p inner join NL_Account a on a.accountid=p.NOMINALACC " &_
			" where STARTDATE <= getdate() and STAGEON=0 and RUNCOUNT=0  and NEXTRUNMONTH <= month(getdate())" &_
			" union " &_
			" select STARTDATE,PREPAYID,NOMINALACC,TRANSACTIONID,RUNCOUNT,no_months,value,DETAILS , " &_
			" case when " &_
			"	dateAdd(Month,RUNCOUNT,STARTDATE)> getdate() THEN 0 " &_
			"	   when " &_
			"	dateAdd(Month,RUNCOUNT,STARTDATE)< getdate() THeN  " &_
			"		dbo.GetMissingPrepaycount(PREPAYID,getdate()) " &_
			" end as MISSCOUNT,a.[name] as NlAcc " &_
			" from NL_PREPAYMENT p inner join NL_Account a on a.accountid=p.NOMINALACC " &_
			" where  RUNCOUNT < no_months and STARTDATE <= getdate() and STAGEON=1 and NEXTRUNMONTH <= month(getdate()) " &_
			" and PREPAYID not in (select PREPAYID " &_
			" from NL_PREPAYMENT where STARTDATE <= getdate() and STAGEON=0 and RUNCOUNT=0 ) " &_
			" ORDER BY "& orderBy
	
	
	
	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<script language="JavaScript">
function post()
{
	location.href="RunPrepayment.asp?task=yes"
	document.getElementById("btnRun").disabled=true
}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" --> 
<br>
<br>
	<form name = thisForm method=post>
		<table aligh=center >
			<tr  bgcolor='whitesmoke' style='WIDTH:350;BORDER:1PX SOLID BLACK;'>
				<td style='WIDTH:350'>
					<b> Prepayment</b>
				</td>
			</tr>
			
			<tr style='WIDTH:350'>
				<td style='WIDTH:350'>
					<div id="msg">
					<%=msgTxt%>
					</div>
				</td>
			</tr>
			<tr  style='WIDTH:250'>
				<td style='WIDTH:350' aligh="center">
					
					<div id="dbtn" >
					<p align="center" <% if  req="yes" then response.write("style='visibility :hidden'") else response.write("style='visibility :visible'") end if %>>
					<input type="button" name="btnRun" value="   OK   "   class="RSLButton" onClick="post()">
					</p>
					</div>
					
					
					
				</td>
			</tr>
			<tr  bgcolor='whitesmoke' style='WIDTH:250'>
				<td style='WIDTH:350'>&nbsp;
					
				</td>
			</tr>
		</table>
		<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
			<TR>
				
			  <TD>&nbsp;</TD>
			</TR>
			<TR>
				<TD BGCOLOR=#133E71><IMG SRC="/Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
			</TR>
		</TABLE>
		<%=TheFullTable%>
		
	</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
