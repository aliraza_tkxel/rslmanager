<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)
	Dim searchSQL,searchName
	
	searchName = Replace(Request("name"),"'","''")
	searchSQL = ""
	if searchName <> "" Then
		searchSQL = " Where (E.FIRSTNAME LIKE '%" & searchName & "%' OR E.LASTNAME LIKE '%" & searchName & "%') "
	Else
		searchName = ""
	End If
	
	ColData(0)  = "Name|FULLNAME|120"
	SortASC(0) 	= "FULLNAME ASC"
	SortDESC(0) = "FULLNAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Expenditure|Expenditure|130"
	SortASC(1) 	= "expenditure asc"
	SortDESC(1) = "expenditure desc"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Head|Head|175"
	SortASC(2) 	= "head asc"
	SortDESC(2) = "head desc"
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Cost Centre|CostCentre|170"
	SortASC(3) 	= "costcentre asc"
	SortDESC(3) = "costcentre desc"
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Limit|LIMIT|80"
	SortASC(4) 	= "limit asc"
	SortDESC(4) = "limit desc"	
	TDSTUFF(4)  = " "" align=right "" "
	TDFunc(4) = "FormatCurrency(|)"		

	PageName = "LimitList.asp"
	EmptyText = "No Employee Limits set in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE ="SELECT E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, isnull(EL.LIMIT,0) as limit, EX.DESCRIPTION AS EXPENDITURE, HE.DESCRIPTION AS HEAD, CC.DESCRIPTION AS COSTCENTRE FROM F_EMPLOYEELIMITS EL " &_
				"INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = EL.EMPLOYEEID " &_
				"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = EL.EXPENDITUREID " &_
				"INNER JOIN F_HEAD HE ON EX.HEADID = HE.HEADID " &_
				"INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = HE.COSTCENTREID " &_
				"" & searchSQL & "" &_
				"ORDER BY " & orderBy
			

'rESPONSE.wRITE SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Employee Limit List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--

	function quick_find(){
	
		location.href = "LimitList.asp?name="+thisForm.findemp.value;
		
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2><IMG NAME="tab_employees" TITLE='Employees' SRC="images/tab_employee_limits.gif" WIDTH=128 HEIGHT=20 BORDER=0></TD>
		<!--TD><IMG SRC="images/spacer.gif" WIDTH=656 HEIGHT=19></TD-->
		<TD ALIGN=RIGHT>
		<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0><TR>
				<TD ALIGN=RIGHT>
					<INPUT TYPE='BUTTON' name='btn_FIND' title='Search for matches using both first and surname.' CLASS='RSLBUTTON' VALUE='Quick Find' ONCLICK='quick_find()'>
					&nbsp;<input type=text  size=18 name='findemp' class='textbox200'>
				</TD></TR>
			</TABLE>
		</TD>	
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=620 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>