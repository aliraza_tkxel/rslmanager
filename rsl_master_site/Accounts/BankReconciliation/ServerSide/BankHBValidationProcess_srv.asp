<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	'''''' page notes     ''''''
	' For the processing for this page we need to get the local authority id 
	' to match the records that have been seleccted
	'''''''''''''''''''''''''''''
	Dim bt_id , SQLFilter, insert_list, detotal_credit, detotal_debit, bank_statement, LocalAuthority
	Dim rec_type, method, TimeStamp, key_TransactionDate , key_LAuthID
	
	SQLFilter = " "
	
	' Get all the variables that are passed to this page
	
	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("STATEMENTID")
	LocalAuthority = Request("LocalAuthorityID")
	
	'Set Statis variables
	
	rec_type = 1 ''This means these items are from the rent journal
	method = 1 'Manual Validation
	TimeStamp = Now()
	
	IF insert_list <> "" Then
		OpenDB()
		InsertDebitCredit_Amount()
		CloseDB()
	End If
	
	'insert the debits and credits into table to balance the amounts	
	Function InsertDebitCredit_Amount ()
		strSQL = 	"SET NOCOUNT ON;" &_	
					"	INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
					"	VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
					"	SELECT SCOPE_IDENTITY() AS BTID;"
	
		set rsSet = Conn.Execute(strSQL)
		bt_id = rsSet.fields("BTID").value
		rsSet.close()
		set rsSet = Nothing 
		
		Insert_HB_rec_Values()
	End Function
	
	Function Insert_HB_rec_Values()
		sel_split = Split(insert_list ,",") ' split selected string

		For each key in sel_split
			'SPLIT THE KEY
			key_split 			= Split(key ,"~")
			' Assign the values to variables
			key_TransactionDate = key_split(0)
			key_LAuthID 		= key_split(1)

			SQL = "DECLARE @GROUPINGID INT; " &_
			  " INSERT INTO F_BANK_GROUPING (RANDOM) VALUES (1); SET @GROUPINGID = SCOPE_IDENTITY(); " &_
			  " INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD, GROUPINGID) " &_
			  " SELECT 	" & bt_id & ", " & bank_statement & ", " & rec_type & ", RJ.JOURNALID ,'" & TimeStamp & "'," & Session("USERID") & "," & method & ", @GROUPINGID " &_ 
						" FROM	 	F_RENTJOURNAL RJ " &_
						"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
						"			INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID " &_
						"			INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID " &_
						"			INNER JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID " &_
						"			LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = DEV.LOCALAUTHORITY " &_
						" WHERE		RJ.STATUSID <> 1  " &_
						" AND 	P.PAYMENTTYPEID IN (1,15,16) AND NOT EXISTS" &_
						"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " &_
						" AND RJ.TRANSACTIONDATE = '" & key_TransactionDate & "' AND LA.LOCALAUTHORITYID = " & key_LAuthID & " "
			Conn.Execute(SQL)
		Next
	End Function

	Response.Redirect "BankHBValidation_svr.asp?RESET=1&Date=" & Request("txt_STATEMENTDATE") & "&STATEMENTID=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO") & "&sel_LA=" & Request("sel_LA")	
%>
