<SCRIPT LANAGUAGE=JAVASCRIPT>
function SendBalances() {
	parent.RSLFORM.hid_STARTINGBALANCE.value = "<%=FormatNumber(StartingBalance,2,-1,0,0)%>"	
	parent.RSLFORM.txt_STARTINGBALANCE.value = "<%=FormatNumber(StartingBalance,2)%>"
	
	parent.RSLFORM.hid_ENDINGBALANCE.value = "<%=FormatNumber(EndingBalance,2,-1,0,0)%>"
	parent.RSLFORM.txt_ENDINGBALANCE.value = "<%=FormatNumber(EndingBalance,2)%>"

	parent.RSLFORM.hid_DEBITLEFT.value = "<%=FormatNumber(RSL_DEBITS,2,-1,0,0)%>"
	parent.RSLFORM.hid_CREDITLEFT.value = "<%=FormatNumber(RSL_CREDITS,2,-1,0,0)%>"

	parent.RSLFORM.txt_DEBITLEFT.value = "<%=FormatNumber(RSL_DEBITS,2)%>"
	parent.RSLFORM.txt_CREDITLEFT.value = "<%=FormatNumber(RSL_CREDITS,2)%>"
	
	parent.RSLFORM.hid_TOTALBALANCE.value = "<%=FormatNumber(Difference,2,-1,0,0)%>"				
	parent.RSLFORM.txt_TOTALBALANCE.value = "<%=FormatNumber(Difference,2)%>"				

	parent.RSLFORM.txt_STATEMENTDATE.value = "<%=STATEMENTDATE%>"
	parent.RSLFORM.STATEMENTID.value = "<%=statement%>"	

	parent.RSLFORM.sel_VALIDATE.disabled = false					
	}
</SCRIPT>