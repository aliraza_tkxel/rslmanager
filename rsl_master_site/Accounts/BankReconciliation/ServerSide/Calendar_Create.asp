<%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function

	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 0 'Sets the top position
	MyCalendar.Left = 0 'Sets the left position
	MyCalendar.Position = "relative" 'Relative or Absolute positioning
	MyCalendar.Height = "161" 'Sets the height
	MyCalendar.Width = "180" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = False 'Toggles the Date Selection form.
	
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='DDCSV.asp?pROCESSINGdATE=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))
		EndOfMonthDate = LastDayOfMonth(CDate("1" & "/" & TheCurrentMonth & "/" & TheCurrentYear)) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear					
	End If

	StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

	'THIS CALCULATES THE validations gone thorugh
	ACTUALSQL = "SET NOCOUNT ON;DECLARE @CURRENTACCOUNT INT; " &_
				"SET @CURRENTACCOUNT = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTID'); " &_
				"SELECT BS.BSID, DAY(STATEMENTDATE) AS THEDAY, " &_
					"IDIFF = (STARTINGBALANCE - ENDINGBALANCE) - ISNULL(TOTAL1,0) - ISNULL(TOTAL2,0) - ISNULL(TOTAL3,0) - ISNULL(TOTAL4,0) - ISNULL(TOTAL5,0) - ISNULL(TOTAL6,0) - ISNULL(TOTAL7,0) - ISNULL(TOTAL8,0) - ISNULL(TOTAL9,0) - ISNULL(TOTAL10,0), " &_
					"TOTALCOUNT = ISNULL(COUNT1,0) + ISNULL(COUNT2,0) + ISNULL(COUNT3,0) + ISNULL(COUNT4,0) + ISNULL(COUNT5,0) + ISNULL(COUNT6,0) + ISNULL(COUNT7,0) + ISNULL(COUNT8,0) + ISNULL(COUNT9,0) + ISNULL(COUNT10,0) " &_
					"FROM F_BANK_STATEMENTS BS " &_
					"LEFT JOIN " &_
						"(SELECT COUNT(JOURNALID) AS COUNT1, ISNULL(SUM(AMOUNT),0) AS TOTAL1, BSID FROM F_RENTJOURNAL RJ " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND RECTYPE = 1 " &_
						"		GROUP BY BR.BSID) RJ ON RJ.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(DATAID) AS COUNT2, ISNULL(SUM(TOTALVALUE),0) AS TOTAL2, BSID FROM F_BACSDATA BD " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND RECTYPE = 2 " &_
						"		GROUP BY BSID) INV ON INV.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(ORDERITEMID) AS COUNT3, ISNULL(SUM(GROSSCOST),0) AS TOTAL3, BSID FROM F_PURCHASEITEM PI " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE =  PI.ORDERITEMID AND RECTYPE = 3 " &_
						"		GROUP BY BSID) EXP ON EXP.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(BLTID) AS COUNT4, ISNULL(SUM(AMOUNT),0) AS TOTAL4, BSID FROM NL_BALANCETRANSFER BT " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BT.BLTID AND RECTYPE = 4  " &_
						"		WHERE ACCOUNTFRM = @CURRENTACCOUNT " &_
						"		GROUP BY BSID) BT_NEG ON BT_NEG.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(BLTID) AS COUNT5, ISNULL(SUM(-AMOUNT),0) AS TOTAL5, BSID FROM NL_BALANCETRANSFER BT " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BT.BLTID AND RECTYPE = 4  " &_
						"		WHERE ACCOUNTTO = @CURRENTACCOUNT " &_
						"		GROUP BY BSID) BT_POS ON BT_POS.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(D.pdid) AS COUNT6, ISNULL(SUM(-AMOUNT),0) AS TOTAL6, BSID FROM P_DEVLOPMENTGRANT D " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = D.PDID AND RECTYPE = 5  " &_
						"		GROUP BY BSID) GR ON GR.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(HDRID) AS COUNT7, ISNULL(SUM(-NETAMOUNT),0) AS TOTAL7, BSID FROM NL_MISCELLANEOUS_DTL DTL " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = DTL.ID AND RECTYPE = 6  " &_
						"	WHERE DTL.NLACCOUNT = @CURRENTACCOUNT " &_						
						"		GROUP BY BSID) MI_POS ON MI_POS.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(HDRID) AS COUNT8, ISNULL(SUM(-NETAMOUNT),0) AS TOTAL8, BSID FROM NL_MISCELLANEOUS_DTL DTL " &_
						" 	INNER JOIN NL_MISCELLANEOUS_HDR H ON H.ID = DTL.HDRID " &_			
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = DTL.ID AND RECTYPE = 6  " &_
						"	WHERE H.BANK = @CURRENTACCOUNT " &_						
						"		GROUP BY BSID) MI_NEG ON MI_NEG.BSID = BS.BSID " &_
					"LEFT JOIN " &_
						"(SELECT COUNT(CASHPOSTINGID) AS COUNT9, ISNULL(SUM(-AMOUNT),0) AS TOTAL9, BSID FROM F_CASHPOSTING CP " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = CP.CASHPOSTINGID AND RECTYPE = 7 " &_
						"	WHERE CP.PAYMENTTYPE IN (5,8,92,93,25) " &_
						"		GROUP BY BR.BSID) CASHCHEQUE ON CASHCHEQUE.BSID = BS.BSID " &_
					"LEFT JOIN " &_	
						"(SELECT COUNT(ORDERITEMID) AS COUNT10, ISNULL(SUM(GROSSCOST),0) AS TOTAL10, BSID FROM F_PURCHASEITEM PI " &_
						"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE =  PI.ORDERITEMID AND RECTYPE = 8 " &_
						"		GROUP BY BSID) TREIMB ON TREIMB.BSID = BS.BSID " &_
					"WHERE STATEMENTDATE >= '" & StartOfMonthDate & "' AND STATEMENTDATE <= '" & EndOfMonthDate & "'"

	Select Case Month(MyCalendar.GetDate())
		Case TheCurrentMonth

			OpenDB()
			
			'THIS SECTION DISPLAYS THE ACTUAL DIRECT DEBITS IN THE SYSTEM
			Call OpenRs(rsDays, ACTUALSQL)
			while (NOT rsDays.EOF)
				Difference = FormatNumber(rsDays("IDIFF"),2,-1,0,0)
				'Response.Write rsDays("THEDAY") & " - " & Difference & "<BR>"
				'isNull(Difference) OR 
				TotalCount = CInt(rsDays("TOTALCOUNT"))
				if (TotalCount = 0) then
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' title='No items have been validated against this day' href=""javascript:LoadDay(" & rsDays("BSID") & ")"" ><font color=white><small><b>" & rsDays("THEDAY") & " </b></small></font></a>", "orange"
				elseif (Difference = 0 AND TotalCount <> 0) then
					if (TotalCount = 1) then
						TotalText = "1 item has"
					else
						TotalText = TotalCount & " items have"
					end if
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' title='" & TotalText & " been validated and the closing balance matches.' href=""javascript:LoadDay(" & rsDays("BSID") & ")"" ><font color=white><small><b>" & rsDays("THEDAY") & " </b></small></font></a>", "green"
				else
					if (TotalCount = 1) then
						TotalText = "1 item has"
					else
						TotalText = TotalCount & " items have"
					end if
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' title='" & TotalText & " been validated and the difference is &pound;" & FormatNumber(Difference,2) & ".' href=""javascript:LoadDay(" & rsDays("BSID") & ")"" ><font color=white><small><b>" & rsDays("THEDAY") & " </b></small></font></a>", "red"				
				end if
				
				rsDays.moveNext
			wend
			CloseRs(rsDays)									
		
			CloseDB()
			
	End Select
	
	MyCalendar.Draw()
%>