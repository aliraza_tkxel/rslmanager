<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim bt_id ,SQLFilter, insert_list, detotal_credit, detotal_debit, bank_statement
	Dim rec_type, method, TimeStamp, key_PaymentStartDate
	
	SQLFilter = " "
	
	' Get all the variables that are passed to this page
	
	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("STATEMENTID")
	
	'Set Statis variables
	
	rec_type = 1 ''This means these items are from the rent journal
	method = 1 'Manual Validation
	TimeStamp = Now()
	
	if insert_list <> "" then
		OpenDB()
		InsertDebitCredit_Amount()
		CloseDB()
	End If
	
	'insert the debits and credits into table to balance the amounts
	Function InsertDebitCredit_Amount ()
		strSQL = 	"SET NOCOUNT ON;" &_	
					"	INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
					"	VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
					"	SELECT SCOPE_IDENTITY() AS BTID;"
	
		set rsSet = Conn.Execute(strSQL)
		bt_id = rsSet.fields("BTID").value
		rsSet.close()
		set rsSet = Nothing 
		
		Insert_TR_rec_Values()
	
	End Function

	
	Function Insert_TR_rec_Values()

		sel_split = Split(insert_list ,",") ' split selected string

		For each key in sel_split
			'SPLIT THE KEY

			SQL = "DECLARE @GROUPINGID INT; " &_
			  " INSERT INTO F_BANK_GROUPING (RANDOM) VALUES (1); SET @GROUPINGID = SCOPE_IDENTITY(); " &_
			  " INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD, GROUPINGID) " &_
			  " SELECT 	" & bt_id & ", " & bank_statement & ", " & rec_type & ", RJ.JOURNALID ,'" & TimeStamp & "'," & Session("USERID") & "," & method & ", @GROUPINGID " &_ 
						" FROM	 	F_RENTJOURNAL RJ " &_
						"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_						
						"			INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID " &_
						"			INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID " &_
						"			INNER JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID " &_
						"			INNER JOIN F_TENANTREFUNDS TR ON TR.JOURNALID = RJ.JOURNALID " &_
						"			INNER JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
						"			INNER JOIN F_TENANTBACSFILES TBF ON TBF.FILEID = TB.FILEID " &_					
						" WHERE		RJ.STATUSID <> 1  " &_
						" AND 	P.PAYMENTTYPEID in (19,24) AND NOT EXISTS" &_
						"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " &_
						" AND TBF.FILEID = " & key & " "
			Conn.Execute(SQL)
		next
	End Function

	Response.Redirect "BankTRValidation_svr.asp?RESET=1&Date=" & Request("txt_STATEMENTDATE") & "&STATEMENTID=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO") 
%>
