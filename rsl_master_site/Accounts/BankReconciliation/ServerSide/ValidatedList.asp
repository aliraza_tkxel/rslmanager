<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../includes/BRFunctions.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim mypage, numpages, prevpage, nextpage, numrecs, pagesize
	
	Headers = Array("REC No.", "Type", "Payment Type", "Transactions", "","Withdrawn", "Paid In")
	SortOrder = Array("", "","","", "", "", "")
	SortOrderAsc = Array("black","black","black","black","black","black","black","black","black")	
	SortOrderDesc = Array("black","black","black","black","black","black","black","black","black")	
	HeaderWidths = Array("100","160","350","50", "30", "90", "90", "")
			'
	STATEMENTID = Request("STATEMENTID")

	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
				"<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" 
				
		for i=0 to Ubound(Headers)
			tWidth = "WIDTH=""" & HeaderWidths(i) & """"
			if (tWidth = "WIDTH=""""") then tWidth = ""
			str_data = str_data & "<TD " & tWidth & " height=20><FONT STYLE='COLOR:WHITE'><B>" & Headers(i) & "</B></FONT></TD>" 
		next

		str_data = str_data & "</TR>"

				
		cnt = 0
		strSQL =	"SELECT GROUPINGID, 'Rent Journal' as TYPE, COUNT(*) AS ITEMCOUNT,  P.DESCRIPTION, " &_
					"	ISNULL(SUM(RJ.AMOUNT),0) AS AMOUNT " &_
					"	FROM F_RENTJOURNAL RJ  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1 AND BSID = " & STATEMENTID & " " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID  " &_
					"		INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID  " &_
					"		INNER JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID  " &_
					"		LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE  " &_
					"GROUP BY GROUPINGID,  P.DESCRIPTION " &_
					"UNION ALL " &_
					"SELECT GROUPINGID, 'Invoices' AS TYPE, BD.INVOICECOUNT,  " &_
					"	PT.DESCRIPTION AS PAYMENTTYPE, " &_ 
					"	-BD.TOTALVALUE AS AMOUNT " &_
					"	FROM F_BACSDATA BD  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND BR.RECTYPE = 2 AND BSID = " & STATEMENTID & " " &_
					"		INNER JOIN F_POBACS PB ON PB.DATAID = BD.DATAID  " &_
					"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = PB.INVOICEID  " &_
					"		LEFT JOIN F_PAYMENTTYPE PT ON INV.PAYMENTMETHOD = PT.PAYMENTTYPEID  " &_
					"	GROUP BY GROUPINGID, BD.INVOICECOUNT, BD.TOTALVALUE, PT.DESCRIPTION  " &_
					"ORDER BY GROUPINGID, P.DESCRIPTION " 

		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		totaldebit = 0
		totalcredit = 0

		While NOT Rs.EOF Then
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			 
			str_data = str_data & 	"<TR><TD>" & Rs("GROUPINGID") & "</TD>" &_
						"<TD>" & Rs("TYPE") & "</TD>" &_
						"<TD>" & Rs("DESCRIPTION") & "</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=center >" & Rs("ITEMCOUNT")  & "</TD>" &_
						"<TD></TD>" 

'			"<TD STYLE='border-left:1px dotted #133e71' align=center title='Click here to see the individual items for this transaction.' style='cursor:hand' onClick=""PopList('" & Rs("GROUPINGID") & "')"">" & Rs("ITEMCOUNT")  & "</TD>" &_
			amount = Rs("AMOUNT")
			if (amount < 0) then
				amount = -amount
				totaldebit = totaldebit + amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				totalcredit = totalcredit + amount
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
			str_data = str_data & "</TR>"
			
			Rs.movenext()
		Wend

		Rs.close()
		Set Rs = Nothing
		
		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=10 WIDTH=100% ALIGN=CENTER valign=top><B>No matching records exist !!&nbsp;</B></TD></TR>"
			cnt = 1
		End If

		str_data = str_data & "<TR><TD colspan=8 align=right style='border-top:1px solid black'><b>Total</b></TD>" &_
					"<TD style='border-top:1px solid black;border-left:1px dotted #133e71' align=right><b>" & FormatCurrency(totalcredit) & "</b>&nbsp;</TD>" &_
					"<TD style='border-top:1px solid black;border-left:1px dotted #133e71' align=right><b>" & FormatCurrency(totaldebit) & "</b>&nbsp;</TD></TR>"					

		
'		str_data = str_data & "<tr bgcolor=black>" &_
'			"	<td colspan=10 align=center height=22 valign=middle>" &_
'			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
'			"			<tfoot>" &_
'			"				<tr style='color:white' bgcolor=black>" &_
'			"					<td width=550px align=right>" &_
'			"						<a class='RSLWhite' target=""BankFrame" & TIMESTAMP & """ href=""ServerSide/ValidatedList.asp?TIMESTAMP=" & TIMESTAMP & "&page=1&STATEMENTID=" & STATEMENTID & """><b>FIRST</b></a>" &_
'			"						<a class='RSLWhite' target=""BankFrame" & TIMESTAMP & """ href=""ServerSide/ValidatedList.asp?TIMESTAMP=" & TIMESTAMP & "&page=" & prevpage & "&STATEMENTID=" & STATEMENTID & """><b>PREV</b></a>" &_
'			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
'			"						<a class='RSLWhite' target=""BankFrame" & TIMESTAMP & """ href=""ServerSide/ValidatedList.asp?TIMESTAMP=" & TIMESTAMP & "&page=" & nextpage & "&STATEMENTID=" & STATEMENTID & """><b>NEXT</b></a>" &_
'			"						<a class='RSLWhite' target=""BankFrame" & TIMESTAMP & """ href=""ServerSide/ValidatedList.asp?TIMESTAMP=" & TIMESTAMP & "&page=" & numpages & "&STATEMENTID=" & STATEMENTID & """><b>LAST</b></a>" &_
'			"					</td>" &_
'			"				    <td align=right>" &_
'			"						Page:" &_
'			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
'			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) TravelTo('page=' + document.getElementById('page').value + '&" & Server.HTMLEncode(TheNextPage) & "'); else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
'			"					</td>" &_
'			"				</tr>" &_
'			"			</tfoot>" &_
'			"		</table>" &_
'			"	</td>" &_
'			"</tr>"
'

	End function
	
%>
<html>
<head><title>Reconciled List</title></head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language=javascript>
<!--
function CloseWindow(){
	window.close()
	}
	
function doPrint(){  
	 if(self.print){
	   	 document.getElementById("PrintButton").style.display = "none";
	   	 document.getElementById("CloseButton").style.display = "none";		 
		 self.print();  
		 self.close();  
		 return False;  
 		}  
	 else if (navigator.appName.indexOf(' Microsoft') != -1){  
		 VBPrint();  
		 self.close();  
		 }  
 	else{  
	 alert("To print this document, you will need to\nclick the Right Mouse Button and select\n' Print'");  
 	}  
 }  
//-->  	
</script>
<body leftmargin="0" topmargin="0" style='padding:10px'>
<object id="WBControl" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object> 
<script language="VBScript">
<!--  
 Sub VBPrint() On Error Resume next  
	 WBControl.ExecWB 6,1  
 End Sub  
//-->
</script>  

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td>
  	Below are the transaction that were validated against the bank statement for the date '<%=request("STATEMENTDATE")%>'. <br><br>Total Lines : <b><%=cnt%></b> with a total value of <b><%=FormatCurrency(abs(totaldebit-totalcredit))%></b>&nbsp;&nbsp;(<%=DebitCreditCode%>)</td>
</tr>
<tr> 
  <td>&nbsp;</td>
</tr>
<tr> 
  <td><%=str_data%></td>
</tr>
<tr> 
  <td>&nbsp;</td>
</tr>
<tr> 
  <td align="right"> 
	<input type="button" value="Print" class="RSLButton" onClick="doPrint()" id="PrintButton">
	<input type="button" value="Close" class="RSLButton" onClick="CloseWindow()" id="CloseButton">&nbsp;
  </td>
</tr>
</table>
</body>
</html>