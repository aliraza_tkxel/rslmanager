<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="Calendar.asp" -->
<!--#include file="../includes/BRFunctions.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim mypage, numpages, prevpage, nextpage, numrecs, pagesize,mypagesize	

	Headers = Array("Date", "Reference", "Bank", "Bank Slip", "Count", "Withdrawn","Paid In", "&nbsp;")
	SortOrder = Array("HDR.MISDATE", "HDR.ID", "NL1.FULLNAME",  "BSLIP", "THECOUNT", "THEVALUE", "NEGATIVETHEVALUE", "")
	SortOrderAsc = Array("black","black","black","black","black","black","black","black","black")	
	SortOrderDesc = Array("black","black","black","black","black","black","black","black","black")		
	HeaderWidths = Array("100", "110", "180", "100", "100", "100", "100", "")
			
	detotal_debit = Request("detotal_debit")
	if detotal_debit = "" Then detotal_debit = 0 end if

	detotal_credit = Request("detotal_credit")
	if detotal_credit = "" Then detotal_credit = 0 end if

	selected = Request("idlist") // get selected checkboxes into an array
	selected_data = "," & Replace(selected, " ", "") & ","


	todate = Request("txt_TO")
	if todate = "" Then 
		to_sql = "" 
	else
		to_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,HDR.MISDATE,103),103) <= '" & FormatDateTime(todate,1) & "' "
	end if

	fromdate = Request("txt_FROM")
	if fromdate = "" Then 
		from_sql = "" 
	else
		from_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,HDR.MISDATE,103),103) >= '" & FormatDateTime(fromdate,1) & "' "
	end if

    companyid = Request("sel_COMPANY")
	if companyid = "" Then 
		companyid_sql = "" 
	else
		companyid_sql = " AND ISNULL(HDR.COMPANYID,1) = '" & companyid & "' "
	end if

	orderBy = Request("orderBy")
	Call SetSort()
	
	TheNextPage_NoOrder = "txt_FROM=" & fromdate & "&txt_TO=" & todate
	TheNextPage = "orderBy=" & orderBy & "&" & TheNextPage_NoOrder
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CLng(mypage)
		end if
		
		Call TopBit()
				
		cnt = 0
		strSQL = 	"SET NOCOUNT ON;DECLARE @CURRENTACCOUNT INT; " &_
					"SET @CURRENTACCOUNT = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTID'); " &_
					"SELECT HDR.ID, COUNT(*) AS THECOUNT, HDR.MISDATE, NL1.FULLNAME, HDR.ACCFROM, HDR.BSLIP, " &_
					"	THEVALUE = CASE WHEN NL1.ACCOUNTID =  @CURRENTACCOUNT THEN -SUM(DTL.NETAMOUNT) " &_
					"	ELSE SUM(DTL.NETAMOUNT) END, " &_
					"	NEGATIVETHEVALUE = CASE WHEN NL1.ACCOUNTID =  @CURRENTACCOUNT THEN -SUM(DTL.NETAMOUNT) " &_
					"	ELSE SUM(DTL.NETAMOUNT) END " &_
					"FROM NL_MISCELLANEOUS_HDR HDR " &_
					"	INNER JOIN NL_MISCELLANEOUS_DTL DTL ON DTL.HDRID = HDR.ID " &_                            
					"	INNER JOIN NL_ACCOUNT NL1 ON NL1.ACCOUNTID = HDR.BANK " &_
					"	INNER JOIN NL_ACCOUNT NL2 ON NL2.ACCOUNTID = DTL.NLACCOUNT " &_
					"WHERE (NL1.ACCOUNTID = @CURRENTACCOUNT OR NL2.ACCOUNTID = @CURRENTACCOUNT) " &_
					"	AND NOT EXISTS (SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 6 AND RECCODE = DTL.ID) " &_
					"	" & from_sql & to_sql & companyid_sql & " " &_
					"GROUP BY HDR.ID,  NL1.FULLNAME, HDR.ACCFROM, NL1.ACCOUNTID, HDR.MISDATE, HDR.BSLIP " &_
					"ORDER		BY " & orderBy & ""
		response.write strSQL			
		if mypage = 0 then mypage = 1 end if
		
		if (Request("pagesize")="") then 
			mypagesize = 12
		else
			mypagesize = CLng(Request("pagesize"))
		end if 
		
		pagesize = mypagesize
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		Set regEx = New RegExp 

		
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if check box has been previously selected
			isselected = ""
			trstyle = ""
			MI_ID = Rs("ID")
			ListSearch = "," & MI_ID & ","
			Comparison = InStr(1, selected_data, ListSearch, 0)
			If NOT isNULL(Comparison) AND Comparison > 0  Then
					isselected = " checked "
					trstyle = " style='background-color:steelblue;color:white'"
			End If
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1

			accountfrom = Rs("fullname")
			accountfromtitle = ""
			if (Not isNull(Rs("fullname"))) then
				if (Len(Rs("fullname")) > 20) then
					accountfrom = left(Rs("fullname"),17) + "..."
					accountfromtitle = Rs("fullname")
				end if
			end if

			str_data = str_data & 	"<TR " & trstyle & "><TD>" & Rs("MISDATE") & "</TD>" &_
						"<TD>" &  MIIncome(Rs("ID")) & "</TD>" &_						
						"<TD title=""" & accountfromtitle & """>" &  accountfrom & "</TD>" &_
						"<TD title=""" & Rs("ACCFROM") & """>" & Rs("BSLIP") & "</TD>" &_						
						"<TD STYLE='border-left:1px dotted #133e71' align=center title='Click here to see the individual items for this transaction.' style='cursor:hand' onClick=""PopList('" & MI_ID & "',0)"">" & Rs("THECOUNT") & "</TD>" 												
			amount = Rs("THEVALUE")
			if (amount < 0) then
				iType = 1
				amount = -amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				iType = 0						
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & MI_ID & "' value='" & MI_ID & "' onclick='do_sum("& MI_ID &"," & FormatNumber(amount,2,-1,0,0)& "," & iType & ")'></TD>" &_
						"</TR>"
			
			Rs.movenext()
			End If
		Next

		Rs.close()
		Set Rs = Nothing
		
		Call BottomBit(5)

	End function
	
	if (Request("RESET") = 1) then
%>	
	<!--#include file="Balance_Server.asp" -->
<%
	end if	
%>
<html>
<head>
	<!-- #include file="Balance_Server_JS.asp" -->
<script language=javascript>
function ReturnData(){
	<% if (Request("RESET") = 1) then %>
		parent.CalendarHolder.innerHTML = LoaderDivCalendar.innerHTML				
		parent.reset_client_totals()
		SendBalances()		
	<% end if %>
	parent.MainDiv.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
</head>
<body onload="ReturnData()">
<% if (Request("RESET") = 1) then %>
	<DIV id="LoaderDivCalendar">
	<!--#include file="Calendar_Create.asp" -->
	</DIV>
<% end if %>	
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>