<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match	
	Dim TDFunc		   (6)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
	ColData(0)  = "Ref|REF|80"
	SortASC(0) 	= "REF ASC"
	SortDESC(0) = "REF DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "MIIncome(|)"
	
	ColData(1)  = "Bank|Bank|200"
	SortASC(1) 	= "NL_ACCOUNT_1.[DESC] ASC"
	SortDESC(1) = "NL_ACCOUNT_1.[DESC] DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "Type|Paymenttype|90"
	SortASC(2) 	= "F_PAYMENTTYPE.DESCRIPTION  ASC"
	SortDESC(2) = "F_PAYMENTTYPE.DESCRIPTION  DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Income|INCOME|80"
	SortASC(3) 	= "INCOMESORT ASC"
	SortDESC(3) = "INCOMESORT DESC"	
	TDSTUFF(3)  = " "" ALIGN=RIGHT STYLE='COLOR:BLUE'"" "
	TDFunc(3) = ""		

	ColData(4)  = "Payment|PAYMENT|80"
	SortASC(4) 	= "PAYMENTSORT ASC"
	SortDESC(4) = "PAYMENTSORT DESC"	
	TDSTUFF(4)  = " "" ALIGN=RIGHT STYLE='COLOR:RED'"" "
	TDFunc(4) = ""
	
	ColData(5)  = "Items|tcount|70"
	SortASC(5) 	= "tcount ASC"
	SortDESC(5) = "tcount DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""
	
	ColData(6)  = "Date|TXNDATE|70"
	SortASC(6) 	= "NL_JOURNALENTRY.TXNDATE ASC"
	SortDESC(6) = "NL_JOURNALENTRY.TXNDATE DESC"	
	TDSTUFF(6)  = ""
	TDFunc(6) = ""
	
    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " where isnull(NL_Miscellaneous_HDR.COMPANYID,1) = '" & rqCompany & "' "
	end if
	
	PageName = "MislaniousIncome_dtl.asp"
	EmptyText = "No Relevant Miscellaneous Receipt found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""id"") &  "","" & rsSet(""TYPE"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	
	SQLCODE="SELECT   NL_Miscellaneous_HDR.ID as REF,  NL_ACCOUNT_1.[DESC] AS Bank, F_PAYMENTTYPE.DESCRIPTION AS Paymenttype, " &_
				"case WHEN SUM(NL_Miscellaneous_dtl.NetAmount) >= 0 then SUM(NL_Miscellaneous_dtl.NetAmount) else NULL end AS INCOMESORT, " &_
				"case WHEN SUM(NL_Miscellaneous_dtl.NetAmount) < 0 then -SUM(NL_Miscellaneous_dtl.NetAmount) else NULL end AS PAYMENTSORT, " &_			
				"case WHEN SUM(NL_Miscellaneous_dtl.NetAmount) >= 0 then '�' + convert(varchar, SUM(NL_Miscellaneous_dtl.NetAmount),1) else NULL end AS INCOME, " &_
				"case WHEN SUM(NL_Miscellaneous_dtl.NetAmount) < 0 then '�' + convert(varchar, -SUM(NL_Miscellaneous_dtl.NetAmount),1) else NULL end AS PAYMENT, " &_			
				"case WHEN SUM(NL_Miscellaneous_dtl.NetAmount) >= 0 then 1 else 2 end AS TYPE, " &_
                " COUNT(NL_Miscellaneous_HDR.ID) AS tcount, NL_Miscellaneous_HDR.ID, NL_JOURNALENTRY.TXNDATE " &_
			" FROM         NL_ACCOUNT NL_ACCOUNT_1 " &_
                      " INNER JOIN NL_Miscellaneous_HDR ON NL_ACCOUNT_1.ACCOUNTID = NL_Miscellaneous_HDR.Bank " &_
                      " INNER JOIN F_PAYMENTTYPE ON NL_Miscellaneous_HDR.PMethod = F_PAYMENTTYPE.PAYMENTTYPEID  " &_
                      " INNER JOIN NL_ACCOUNT NL_ACCOUNT_2  " &_
                      " INNER JOIN NL_Miscellaneous_dtl ON NL_ACCOUNT_2.ACCOUNTID = NL_Miscellaneous_dtl.NlAccount ON NL_Miscellaneous_HDR.ID = NL_Miscellaneous_dtl.hdrid" &_
                      " INNER JOIN NL_JOURNALENTRY ON NL_Miscellaneous_HDR.TXNID = NL_JOURNALENTRY.TXNID " & CompanyFilter &_
			" GROUP BY NL_ACCOUNT_1.[DESC], F_PAYMENTTYPE.[DESCRIPTION ], NL_Miscellaneous_HDR.ID, NL_JOURNALENTRY.TXNDATE " &_
			"ORDER BY "& orderBy
	
	
	
	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

    
	Call OpenDB()
	 Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:150px'")	
    Call CloseDB()


%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Miscellaneous Income/Payments</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()

    function SubmitPage() {
        var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
        
        location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_COMPANY=" + Company;

    }

	function load_me(txnid, type){
		window.showModelessDialog("Popups/MIS.asp?txnid=" + txnid  + "&type=" + type + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
		}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <tr>
            <td>
                <b>Company: </b>  <%=lstCompany%>   <input type="button" class="RSLButton" value=" Update Search " title="Update Search" onclick="SubmitPage()" />
           
            </td>
          
        </tr>
      
               
    
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="../Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>