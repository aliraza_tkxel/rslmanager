<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim tdate, ref, desc, memo, txnid, debits, credits
	txnid = Request("TXNID")
	netSum = 0
	GrsSum = 0
	if txnid = "" Then txnid = -1 End If
	OpenDB()
	strSQL = "SELECT   NL_ACCOUNT_1.[DESC] AS Bank, F_PAYMENTTYPE.DESCRIPTION AS Paymenttype, NL_ACCOUNT_2.[DESC] AS Nominal, " &_
             "         NL_Miscellaneous_dtl.NetAmount, NL_Miscellaneous_dtl.Gross, NL_Miscellaneous_dtl.NlAccount, NL_Miscellaneous_HDR.AccFrom, " &_
             "         NL_Miscellaneous_HDR.BSlip, NL_Miscellaneous_dtl.Details, NL_JOURNALENTRY.TXNDATE,'MIS'+ right( '00000000' + cast(NL_Miscellaneous_HDR.ID as varchar(10)),8) as REF " &_
			 "FROM     NL_ACCOUNT NL_ACCOUNT_1 INNER JOIN " &_
             "         NL_Miscellaneous_HDR ON NL_ACCOUNT_1.ACCOUNTID = NL_Miscellaneous_HDR.Bank INNER JOIN " &_
             "         F_PAYMENTTYPE ON NL_Miscellaneous_HDR.PMethod = F_PAYMENTTYPE.PAYMENTTYPEID INNER JOIN " &_
             "         NL_ACCOUNT NL_ACCOUNT_2 INNER JOIN " &_
             "         NL_Miscellaneous_dtl ON NL_ACCOUNT_2.ACCOUNTID = NL_Miscellaneous_dtl.NlAccount ON " &_
             "         NL_Miscellaneous_HDR.ID = NL_Miscellaneous_dtl.hdrid INNER JOIN " &_
             "         NL_JOURNALENTRY ON NL_Miscellaneous_HDR.TXNID = NL_JOURNALENTRY.TXNID " &_
            "WHERE     NL_Miscellaneous_HDR.ID = "  + txnid
			 
	Call OpenRs(rsSet, strSQL)
	if not rsSet.EOF Then
		tdate = rsSet("TXNDATE")
		ref = rsSet("REF")
		desc = rsSet("Bank")
		memo = rsSet("AccFrom")
	End If
%>
<HTML>
<HEAD>
<title>RSL Manager - Accounts</title> <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1"> 
<link rel="stylesheet" href="/css/RSL.css" type="text/css"> <META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" /> 
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" /> 
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT> <SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT> 
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT> <SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT> 
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT> <SCRIPT LANGUAGE=JAVASCRIPT>

			
</SCRIPT> 
<BODY BGCOLOR=#FFFFFF ONLOAD="preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=700px align=center BORDER=0> 
<TR bgcolor='whitesmoke'> 
	<TD colspan=3 STYLE='BORDER-bottom:1PX SOLID BLACK'><b>Miscellaneous Receipt</b></TD></TR> 
	<TR> <TD WIDTH=100PX><b>Date</b> </TD><TD><%=tdate%></TD><TD><b>Memo</b></TD></TR> 
	<TR> <TD><b>Reference</b> </TD><TD><%=ref%></TD><TD ROWSPAN="10" VALIGN=TOP>
		<%=memo%></TD></TR> 
	<TR> <TD><b>Details</b> </TD><TD><%=desc%></td></tr>
	<tr><td colspan=2>&nbsp;</td></tr><tr><td colspan=2>&nbsp;</td></tr><tr><td colspan=2>&nbsp;</td></tr><tr><td colspan=2>&nbsp;</td></tr>

</TABLE><br>


<TABLE align=center VALIGN=TOP height=200px STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' cellspacing=0 cellpadding=3 width=700px id="ItemTable" slcolor='' hlcolor="silver"> 
<THEAD>
<TR STYLE='HEIGHT:10PX' bgcolor=whitesmoke> 
	<TD WIDTH=300px STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>Nominal Account</b></TD>
	<TD WIDTH=100px% STYLE='BORDER-BOTTOM:1PX SOLID BLACK'>&nbsp;</TD>
	<TD WIDTH=100px ALIGN=RIGHT STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>Net Amount</b></TD>
	<TD WIDTH=100px ALIGN=RIGHT STYLE='BORDER-BOTTOM:1PX SOLID BLACK'><b>&nbsp;</b></TD>
</TR> 
</THEAD>
<TBODY>
	<% while not rsSet.EOF %>
	
		<TD WIDTH=400px><%=rsSet("Nominal")%></TD>
		<TD WIDTH=100px>&nbsp;</TD>
		<TD WIDTH=100px align=right><%=FormatNumber(rsSet("NetAmount"))%></TD>
		<TD WIDTH=100px align=right>&nbsp;</TD></TR>
		
		<% 
		netSum = netSum  + rsSet("NetAmount")
		'GrsSum = GrsSum  + rsSet("Gross")
		rsSet.MoveNExt() 
		%>
	<% Wend
	CloseRs(rsSet)%>
</TBODY>
<TFOOT>	<TR><TD HEIGHT=100%></TD></TR></TFOOT>
</TABLE>
<TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=700px align=center> 
<TR bgcolor=whitesmoke ALIGN=RIGHT>
	<TD WIDTH=50% style='border:none'>&nbsp;</TD>
	<TD WIDTH=15% nowrap></TD>
	<TD WIDTH=15% align=right><b><%=FormatNumber(netSum)%></b></TD>
	<TD WIDTH=15% nowrap align=right>&nbsp;</TD>
</TR> 
</TABLE>
</BODY>
</HTML>

