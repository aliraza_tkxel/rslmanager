<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()

	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DIAGNOSTICSPASSWORD'"
	Call OpenRs(rsPA, SQL)
	if (NOT rsPA.EOF) then
		iPassword = rsPA("DEFAULTVALUE")
	else
		iPassword = "PASSWORDFAILURE"
	end if
	Call CloseRs(rsPA)

	Call BuildSelect(lst_Modules, "sel_MODULE", "RSL_CHECK_MODULE ", "MODULEID, MODULENAME", "MODULENAME", "PLEASE SELECT", Request("sel_MODULE"), "width:200px", "textbox", " onchange=""DoChecks()"" ")
	Call CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Checks </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function DoChecks(){
	RSLFORM.submit()
	}

function ToggleTable(i){
	if (document.getElementById("TABLE" + i).style.display == "none")
		document.getElementById("TABLE" + i).style.display = "block"
	else
		document.getElementById("TABLE" + i).style.display = "none"		
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table border="0" cellspacing="0" cellpadding="5">
<FORM NAME="RSLFORM" method="POST">
  <tr> 
    <td ><b>Module:</b> </td><td><%=lst_Modules%></td>
	<td>&nbsp;&nbsp;&nbsp;Security Password</td><td><input type=password value="********" name="txt_PASS" class="textbox200"></td>
	<td><input type=submit class="RSLButton" value=" SUBMIT "></td>
  </tr>
 </FORM>
</table>
<%
if (iPassword <> "PASSWORDFAILURE"  AND (Request("txt_PASS") = iPassword OR Session("SUPREMEBEINGACCESS") = "yes")) then
Session("SUPREMEBEINGACCESS") = "yes"
if (Request("sel_MODULE") <> "") then
	Call OpenDB()
	SQL = "SELECT * FROM RSL_CHECKS WHERE MODULEID = " & Request("sel_MODULE") & " ORDER By ORDERBY"
	Call OpenRs(Rs, SQL)
	if (NOT Rs.EOF) then
	iTableCounter = 0
	while NOT Rs.EOF
		iTableCounter = iTableCounter + 1
		Response.Write "<div align=center style='border-top:1px gray dotted'>&nbsp;</div>"
		Response.Write "<SPAN><U><b>" & Rs("NAME") & "</b></U></SPAN><br>" & Rs("DESCRIPTION") & ".<BR>"
		if (NOT isNull(Rs("COMMENT"))) then
			Response.Write "<font color=red><b> NOTE:</b></font> &nbsp;" & Rs("COMMENT")
		end if

		SQL = Rs("SQLCHECK")
		ResultType = Rs("SHOULDEQUAL")
		if (ResultType = "NORECORDS") then
			Call OpenRs(Rs2, SQL)
			if (NOT Rs2.EOF) then
				if (Rs("WARNINGTYPE") = 1) then
					Response.Write "<div><font color=red style='font-size:14px'><img src=""/myImages/info.gif"" width=15 height=16>&nbsp;&nbsp;<b>FAILED</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>'" & Rs("SHOULDEQUAL") & "'</font>.</b> &nbsp;&nbsp;&nbsp; <span  onclick=""ToggleTable(" & iTableCounter & ")"" style='cursor:hand'><font color=blue><i>Toggle Results</i></font></span></div>"			
				else
					Response.Write "<div><font color=BLUE style='font-size:14px'><b>WARNING</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>'" & Rs("SHOULDEQUAL") & "'</font>.</b> &nbsp;&nbsp;&nbsp; <span  onclick=""ToggleTable(" & iTableCounter & ")"" style='cursor:hand'><font color=blue><i>Toggle Results</i></font></span></div>"			
				end if
				FieldCount = Rs2.fields.count
				Response.Write "<div id=""TABLE" & iTableCounter& """ style='display:none'><TABLE style='border-collapse:collapse;color:red;' border=1><TR>"
				for i=0 to FieldCount-1
					Response.Write "<TD align=center><b>" & Rs2.fields(i).name & "</b></TD>"
				next
				Response.Write "</TR>"				
				while NOT Rs2.EOF
					Response.Write "<TR>"
					for i=0 to FieldCount-1
						Response.Write "<TD>" & Rs2(i) & "</TD>"
					next
					Response.Write "</TR>"
					Rs2.movenext
				wend
				Response.Write "</TABLE></div>"
			else
				Response.Write "<div><font color=green style='font-size:14px'><b>PASSED</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>'" & Rs("SHOULDEQUAL") & "'</font>.</b></div>"
			end if
		else
			Call OpenRs(Rs2, SQL)
			if (NOT Rs2.EOF) then
				Result = Rs2(0)
				ResultArray = Split(Rs("SHOULDEQUAL"), "|")
				'build compare string and text string
				EvalString = ""
				EvalText = ""
				for i=0 to Ubound(ResultArray)
					if (i = 0) then
						EvalString = " " & Result & ResultArray(i)
						EvalText = " '" & ResultArray(i) & "'"
					else
						EvalString = EvalString & " AND " & Result & ResultArray(i)						
						EvalText = EvalText & " AND '" & ResultArray(i) & "'"						
					end if
				next
				if ( Eval(EvalString) ) then
					Response.Write "<div><font color=green style='font-size:14px'><b>PASSED</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>" & EvalText & "</font>.</b> &nbsp;&nbsp;&nbsp; <font color=purple>Result = " & Result & "</font></div>"			
				else
					if (Rs("WARNINGTYPE") = 1) then
						Response.Write "<div><font color=red style='font-size:14px'><img src=""/myImages/info.gif"" width=15 height=16>&nbsp;&nbsp;<b>FAILED</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>" & EvalText & "</font>.</b> &nbsp;&nbsp;&nbsp; <font color=purple>Result = " & Result & "</font> </div>"			
					else
						Response.Write "<div><font color=blue style='font-size:14px'><b>WARNING</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>" & EvalText & "</font>.</b> &nbsp;&nbsp;&nbsp; <font color=purple>Result = " & Result & "</font> </div>"			
					end if					
				end if
			else
				Response.Write "<div><font color=red style='font-size:14px'><img src=""/myImages/alertw.gif"" width=20 height=20><b>QUERY FAILURE</b></font> &nbsp;&nbsp;&nbsp; <font color=purple>Answer should be <b>'" & Rs("SHOULDEQUAL") & "'</font>.</b></div>"
			end if
		end if
		Rs.moveNext
	wend
	Call CloseRs(Rs)
	Call CloseDB()
	else
		Response.Write "<div align=center style='border-top:1px gray dotted'><br><b>No Checks Found</b></div>"
	end if
end if
else
	Response.Write "<div align=center style='border-top:1px gray dotted'><br><b>ACCESS IS DENIED</b></div>"
end if
%>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

