<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Function YearEndAccrual(strWord)
		YearEndAccrual = "<b>YE</b> " & characterPad(strWord,"0", "l", 6)
	End Function

	Dim CONST_PAGESIZE
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance
	Dim viewItemSelectedAll,viewItemSelected1,viewItemSelected2,viewItemSelected3, StrSrch
	dim Cost
	dim BollFlag, lstSuppliers, lstStatus, supplier, sstatus
	BollFlag=false

	dim FY
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If RequesT("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	
	YACCRID = Request("YACCRID")
	IF (YACCRID = "") THEN YACCRID = -1
	IPREVPAGE = Request("IPREVPAGE")
	PREV_SORT = Request("PREV_SORT")
		
	PageName = "YearEndAccrualPO_dtl.asp"
	MaxRowSpan = 5
	OpenDB()
	get_tenant_balance()
	get_Sum()
	CloseDB()

	function get_sum()
		dim rsSet
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		strSQL="EXEC P_ORDERNOTCOMPLETD_ACCRUED 2," & YACCRID
		'rw strSQL
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.CursorLocation = 3
		rsSet.Open()
		Cost=0
		if not rsSet.eof then
			Cost=rsSet("Cost")
		end if
		rsSet.close()
		Set rsSet = Nothing
	end function

	Function get_tenant_balance()
	
			Dim orderBy, strSQL, rsSet, intRecord ,dbcmd, Icount
			'orderBy = "rslt.cname"
			'if (Request("CC_Sort") <> "") then orderBy = " " & Request("CC_Sort") end if
			intRecord = 0
			str_data = ""
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			strSQL="P_ORDERNOTCOMPLETD_ACCRUED 1," & YACCRID
			'rw strSQL
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			'intPageCount = Icount
			intPageCount= rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			
			
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			
			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
			
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then

				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			
			If intRecordCount > 0 Then
				' Display the record that you are starting on and the record
				' that you are finishing on for this page by writing out the
				' values in the intStart and the intFinish variables.
				str_data = str_data & "<TBODY CLASS='CAPS'>"
					' Iterate through the recordset until we reach the end of the page
					' or the last record in the recordset.
					For intRecord = 1 to rsSet.PageSize

						RowType = rsSet("ORDERITEMID")
						if (RowType = -1) then
							str_data = str_data & "<TR style='background-color:silver;color:#133e71;'>" &_
								"<TD>" & rsSet("PO") & "</TD>" &_
								"<TD COLSPAN=2>" & rsSet("Supplier") & "</td>" &_
								"<TD>" & rsSet("POTYPE") & "</TD>" &_
								"<TD align=right><b>" & FormatNumber(rsSet("OrderCost"),2) & "</b></TD>" &_
								"</TR>"
						else
							str_data = str_data & "<TR>" &_
								"<TD>&nbsp;&nbsp;" & rsSet("PIDATE") & "</TD>" &_
								"<TD>" & rsSet("TAXDATE") & "</TD>" &_								
								"<TD>" & rsSet("ITEMNAME") & "</TD>" &_
								"<TD>&nbsp;&nbsp;" & rsSet("STATUS") & "</TD>" &_
								"<TD align=Right>" &  FormatNumber(rsSet("ITEMCOST"),2) & "</TD>" &_
								"</TR>"						
						end if
						count = count + 1
						rsSet.movenext()
						If rsSet.EOF Then Exit for 

					Next
					str_data = str_data & "</TBODY>"
	
					'ensure table height is consistent with any amount of records
					fill_gaps()
					
					' links
					str_data = str_data &_
					"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & "  ALIGN=CENTER>" &_
					"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
					"<A HREF = '" & PageName & "?YACCRID="&YACCRID&"&PREV_SORT="&PREV_SORT&"&IPREVPAGE="&IPREVPAGE&"&page=1&PAGESIZE="&REQUEST("PAGESIZE")&"'><b><font color=BLUE>First</font></b></a> "  &_
					"<A HREF = '" & PageName & "?YACCRID="&YACCRID&"&PREV_SORT="&PREV_SORT&"&IPREVPAGE="&IPREVPAGE&"&page=" & prevpage & "&PAGESIZE="&REQUEST("PAGESIZE")&"'><b><font color=BLUE>Prev</font></b></a>"  &_
					" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
					" <A HREF='" & PageName & "?YACCRID="&YACCRID&"&PREV_SORT="&PREV_SORT&"&IPREVPAGE="&IPREVPAGE&"&page=" & nextpage & "&PAGESIZE="&REQUEST("PAGESIZE")&"'><b><font color=BLUE>Next</font></b></a>"  &_ 
					" <A HREF='" & PageName & "?YACCRID="&YACCRID&"&PREV_SORT="&PREV_SORT&"&IPREVPAGE="&IPREVPAGE&"&page=" & intPageCount & "&PAGESIZE="&REQUEST("PAGESIZE")&"'><b><font color=BLUE>Last</font></b></a>"  &_
					"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
					"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
					"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>No records found</TD></TR>" 
				count = 1
				fill_gaps()
			End If
				
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Year End Accrual Details</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()
	FormFields[0] = "txt_PAGESIZE|Page Size|INTEGER|Y"

	function resubmit(){
		if (!checkForm()) return false;
		location.href = "YearEndAccrualPO_dtl.asp?page=1&PREV_SORT=<%=PREV_SORT%>&IPREVPAGE=<%=IPREVPAGE%>&YACCRID=<%=YACCRID%>&PAGESIZE="+document.getElementById("txt_PAGESIZE").value
		}

	function JumpPage(){
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "YearEndAccrualPO_dtl.asp?page=" + iPage + "&PREV_SORT=<%=PREV_SORT%>&IPREVPAGE=<%=IPREVPAGE%>&YACCRID=<%=YACCRID%>&PAGESIZE="+document.getElementById("txt_PAGESIZE").value
		else
			document.getElementById("QuickJumpPage").value = "" 
		}
	
	function GoBack(){
		location.href = "YearEndAccruals_dtl.asp?page=<%=IPREVPAGE%>&CC_SORT=<%=PREV_SORT%>"
		}	
// -->		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name="RSLFORM" method="post">
	<table width=750 CELLPADDING=1 CELLSPACING=2 BORDER=0>
		<tr>
			<TD>
			<input title='Go Back to Main List' type="button" name="btn" value="BACK" class="RSLButton" Onclick="GoBack()">
			&nbsp;Reference: <%=YearEndAccrual(YACCRID)%>
			</TD>
			<TD align=right><b>Pagesize :</b>&nbsp;&nbsp;<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
			<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
			<input title='Refresh Accrual List' type="button" name="btn" value="Refresh" class="RSLButton" Onclick="resubmit()">
			</td>
		</tr>
	</table>
  <TABLE WIDTH=750 CELLPADDING=2 CELLSPACING=0 STYLE="BORDER-COLLAPSE:COLLAPSE" BORDER=1>
	<TR style='background-color:#133e71;color:white;line-height:18px;text-transform:uppercase'> 
		<TD width=120>&nbsp;<B>Order Date</B></TD>
		<TD width=100>&nbsp;<B>Tax Date</B></TD>		
		<TD WIDTH=300>&nbsp;<B>Item Name</B></TD>
		<TD WIDTH=120PX ALIGN=Left>&nbsp;<B>Status</B></TD>
		<TD WIDTH=100PX ALIGN=CENTER>&nbsp;<B>Item Cost</B></TD>
	</TR>
    <%=str_data%> 
    <TR style='background-color:#133e71;color:white;line-height:18px'>
     <TD COLSPAN=4 ALIGN=RIGHT bgcolor="" ><b>GROSS TOTAL </b></TD>
     <TD ALIGN=RIGHT><b><% =formatcurrency(Cost)%></b></TD>
	</TR>
  </TABLE>
</FORM>  
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
						