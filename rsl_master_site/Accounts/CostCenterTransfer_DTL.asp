<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Date|BDATE|100"
	SortASC(0) 	= "BDATE ASC"
	SortDESC(0) = "BDATE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "From CostCentre|FROMCC|250"
	SortASC(1) 	= "FROMCC ASC"
	SortDESC(1) = "FROMCC DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "To CostCentre|TOCC|250"
	SortASC(2) 	= "TOCC ASC"
	SortDESC(2) = "TOCC DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Amount|Amount|120"
	SortASC(3) 	= "Total ASC"
	SortDESC(3) = "Total DESC"	
	TDSTUFF(3)  = " "" ALIGN=RIGHT"" "
	TDFunc(3) = "FormatCurrency(|)"		

	ColData(4)  = "Fiscal Year|PhyYear|80"
	SortASC(4) 	= "PhyYear ASC"
	SortDESC(4) = "PhyYear DESC"
	TDSTUFF(4)  = ""
	TDFunc(4) = ""

	ColData(5)  = "|EMPTY|70"
	SortASC(5) 	= ""
	SortDESC(5) = ""
	TDSTUFF(5)  = """ style='background-color:white'"" "
	TDFunc(5) = ""
	
	PageName = "CostCenterTransfer_dtl.asp"
	EmptyText = "No Relevant transfer found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	
	RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""CCTranId"")  & "")"""" """ 

	
	

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	

	If Session("TeamCode") = "EXE" Then

	SQLCODE="SELECT cast(Year(YStart) as varchar) + ' - ' + cast(year(YEnd) as varchar) as PhyYear,EMPTY = CASE WHEN APPROVED = 0 THEN '<input type=checkbox onClick=chkClick() name=chk value=""' + cast(CCTranID AS VARCHAR) + '"">' ELSE '<font color=red>Approved</font>' END, CCTranId,F_BUDGETRTRANSFER.AMOUNT AS AMOUNT, CONVERT(VARCHAR, F_BUDGETRTRANSFER.BDDATE, 103) AS BDATE, " &_
            "F_COSTCENTRE.DESCRIPTION AS FROMCC, F_COSTCENTRE_1.DESCRIPTION AS TOCC " &_
			"FROM       F_BUDGETRTRANSFER INNER JOIN " &_
			"F_FISCALYEARS FY on F_BUDGETRTRANSFER.PHYYEAR=FY.YRange inner join " &_
			"		   F_COSTCENTRE ON F_BUDGETRTRANSFER.FRMCOSETCENTRE = F_COSTCENTRE.COSTCENTREID INNER JOIN " &_
			"		   F_COSTCENTRE F_COSTCENTRE_1 ON F_BUDGETRTRANSFER.TOCOSTCENTRE = F_COSTCENTRE_1.COSTCENTREID " &_
			"ORDER BY " & orderBy
	ELSE
	SQLCODE="SELECT cast(Year(YStart) as varchar) + ' - ' + cast(year(YEnd) as varchar) as PhyYear,EMPTY = CASE WHEN APPROVED = 0 THEN '<font color=blue>W/A</font>' ELSE '<font color=red>Approved</font>' END, CCTranId,F_BUDGETRTRANSFER.AMOUNT AS AMOUNT, CONVERT(VARCHAR, F_BUDGETRTRANSFER.BDDATE, 103) AS BDATE, " &_
            "F_COSTCENTRE.DESCRIPTION AS FROMCC, F_COSTCENTRE_1.DESCRIPTION AS TOCC " &_
			"FROM       F_BUDGETRTRANSFER INNER JOIN " &_
			"F_FISCALYEARS FY on F_BUDGETRTRANSFER.PHYYEAR=FY.YRange inner join  " &_
			"		   F_COSTCENTRE ON F_BUDGETRTRANSFER.FRMCOSETCENTRE = F_COSTCENTRE.COSTCENTREID INNER JOIN " &_
			"		   F_COSTCENTRE F_COSTCENTRE_1 ON F_BUDGETRTRANSFER.TOCOSTCENTRE = F_COSTCENTRE_1.COSTCENTREID " &_
			"ORDER BY " & orderBy
	END IF	
	
	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Budget Virement</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--

var FormFields = new Array()

function chkClick()
{
event.cancelBubble=true
}
function load_me(CCTranId){
	//location.href="CostCenterTransfer.asp?CCTranId=" + CCTranId 
	//window.showModelessDialog("RemitanceSlip_dtl.asp?SupplierID=" + SupplierID  + "&PROCESSDATE=" + ProcessDate + "&PAYMENTTYPEID=" + PaymentType + "&Random=" + new Date(),1,"dialogHeight: 600px; dialogWidth:790px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;");
	//,,"dialogHeight: 200px; dialogWidth: 250px; dialogTop: 23px; dialogLeft: 1076px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;");
	
	}



function SubmitPage()
	{
		thisForm.action="ServerSide/CostCentreTransfer_Aprove.asp"
		thisForm.submit()
		document.getElementById("SMIT").disabled = true
			
		

	//location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value  + "&sup_dt='" + thisForm.txt_DT.value + "'"
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = "thisForm" method="post">
<%
dim DivTxt
DivTxt="<DIV ID=""idDIV"""

If Session("TeamCode") = "EXE" Then

	DivTxt=DivTxt & "style=""display:inline"" "
else
	DivTxt=DivTxt & "style=""display:none"" "
end if
DivTxt=DivTxt & ">"
%>
<%=DivTxt%>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	
	<TR>
		<TD ALIGN=RIGHT><INPUT TYPE=button NAME=SMIT VALUE="Approve" class="RSLButton" onClick="SubmitPage()"></TD>
	<TR>
</TABLE>
</DIV>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>