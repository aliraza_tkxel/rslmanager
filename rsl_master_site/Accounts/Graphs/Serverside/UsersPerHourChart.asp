<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Set cd = CreateObject("ChartDirector.API")
	'The data for the bar chart

	Dim data (23)
	Dim labels (23)
	for i=0 to 23
		data(i) = 0
		labels(i) = i
	next

	Call OpenDB()
	if (Request("TYPE") = 1) then
		SQL = "EXEC LOGIN_AVERAGEUSERSPERHOUR_WEEKDAYS"
	else
		SQL = "EXEC LOGIN_AVERAGEUSERSPERHOUR_WEEKENDS"
	end if	
	Call OpenRs(rsL, SQL)
	while NOT rsL.EOF
		data(rsL("HOURID")) = FormatNumber(rsL("AVERAGEPEOPLEPERHOUR"),2,-1,0,0)
		rsL.moveNext
	wend
	Call CloseRs(rsL)
	Call CloseDB()
	
	'Create a XYChart object of size 250 x 250 pixels
	Set c = cd.XYChart(600, 220)
	'Set the plotarea at (30, 20) and of size 200 x 200 pixels
	Call c.setPlotArea(50, 30, 500, 150)
	
	'Add a bar chart layer using the given data
	if (Request("TYPE") = 1) then	
		Call c.addBarLayer(data).set3D()
	else
		Call c.addBarLayer(data, &Hff00).set3D()
	end if	
	
	'Add a title to the y axis
	Call c.yAxis().setTitle("Average Users")
	
	'Add a title to the x axis
	Call c.xAxis().setTitle("Hour of the Day")
	
	'Set the x axis labels using the given labels
	Call c.xAxis().setLabels(labels)

	'Add a title to the chart
	if (Request("TYPE") = 1) then
		Call c.addTitle("Average Users per Hour (Weekdays)")
	else
		Call c.addTitle("Average Users per Hour (Weekends)")
	end if	

	'output the chart
	Response.ContentType = "image/png"
	Response.BinaryWrite c.makeChart2(cd.GIF)
	Response.End
%>

