<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
Company = Request("Company")
if (Company="") then Company = "1"
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Finance - Finance Builder</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/formValidation.js"></SCRIPT>
<script language=javascript>
	var FormFields 
	var Diff

	function refreshSideBar(){
		thisForm.action = "ServerSide/FinanceBuilderDev_svr.asp";
		thisForm.target = "theSideBar";
		thisForm.submit();
		}

	function setText(txt,hideallboolean){
		TheText.innerHTML = txt;
		if (hideallboolean == 1) {
			Budget.style.display = "none";
			Head.style.display = "none";
			CostCentre.style.display = "none";
			}
		TheText.style.display = "block";
		}
	
	function DoCancel(){
		setText("Please select an item you would like to edit...",1);
	}
		
	function ResetDiv(what){
		temp = new Array("txt_CostCentreDescription", "CCID", "txt_AccountableBody", "txt_Department", "txt_DateStart", "txt_DateEnd");
		doc = document.all;
		for (i=0; i<temp.length; i++)
			doc[temp[i]].value = "";
		}
	
	function setCheckingArray(what){
		FormFields = new Array("txt_CostCentreDescription|Cost Centre|TEXT|Y", "txt_AccountableBody|Budget Holder|TEXT|Y", "txt_Department|Department|TEXT|Y", "txt_DateStart|Date Commence|DATE|Y", "txt_DateEnd|Date Ceases|DATE|Y");	
	}
	
	function DoSave(what){
		thisForm.action = "ServerSide/CC_DEV.asp";
		if (checkForm()) {
			thisForm.target = "FB_DEV_4466";	
			thisForm.submit();
			}
		}
	
	function DoReset(what){
		temp = new Array("txt_CostCentreDescription", "txt_AccountableBody", "txt_Department", "txt_DateStart", "txt_DateEnd");
		doc = document.all;
		for (i=0; i<temp.length; i++)
			doc[temp[i]].value = "";
		}
	
	function SetEmployeeLimits(){
		window.showModalDialog("Popups/SetEmployeeLimits.asp?EXID=" + thisForm.EXID.value + "&rand=" + new Date(), "Expenditures", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
		}

	function EditExpend(){
		var theURL = "Popups/AmendScheme.asp?HDID=" + document.thisForm.HDID.value + "&NODE="+ document.thisForm.NODE.value + "&rand=" + new Date() ;
		window.showModalDialog(theURL, "Expenditures", "dialogHeight: 515px; dialogWidth: 435px; status: No; resizable: No;");
		}
		
	function getOB(){
		//fix by ali, otherwise the opening balnce would crash if text was sent over...
		FormFields = new Array("txt_OPBalance|Opening Balance|CURRENCY|Y")
		if (!checkForm()) return false;
		
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var t=thisForm.txt_OPBalance.value
		if (t=="") t=0
		var url="/Includes/xmlServer/xmlsrv.asp?id=" + thisForm.txt_nominalcode.value + "&task=DVB"+ "&nop=" + t + "&HDID=" + thisForm.HDID.value;
		
		var nodeMap
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		xmlobj.async = false;
		
		var t=xmlobj.selectSingleNode("//rs:data/z:row");
		var balance=t.getAttribute("BALANCE")
		
		if (balance < 0) {
			alert("Balance will exceed Opening Balance for associated Nominal Account")
			thisForm.txt_OPBalance.value=thisForm.hdn_OPBalance.value
			}
		else {
			//fix by ali otherwise when the item was updated twice and the secondf one 
			//failed the value would be reset to the initial loadup value
			thisForm.hdn_OPBalance.value = thisForm.txt_OPBalance.value
			}			
		}
</script>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopSpecial.asp" -->
  
  <TR> 
    <TD HEIGHT=100% STYLE='BORDER-LEFT:1PX SOLID #7E729E' width=300> 
      <table cellspacing=0 cellpadding=0 height=100% width=100%>
        <tr> 
          <td style='border-top:1px solid #FFFFFF;border-right:1px solid #7E729E;' height=100% width=100% valign=top nowrap> 
            <iframe  name=theSideBar src="ServerSide/FinanceBuilderDev_svr.asp?Company=<%=Company%>" id=theSideBar height=100% frameborder=none bordercolor=#FFFFFF></iframe> 
          </td>
        </tr>
        <tr> 
          <TD height=1 BGCOLOR=#FFFFFF align=right valign=middle></TD>
        </tr>
        <tr> 
          <TD height=28 BGCOLOR=#133e71 align=right valign=middle><b>&nbsp;</b></TD>
        </tr>
      </table>
    </TD>
    <td HEIGHT=100% width=476> 
      <form name=thisForm method=post>
        <table cellspacing=0 cellpadding=0 height=100% width=100%>
          <tr> 
            <td width=100% colspan=2 STYLE='BORDER-RIGHT:1PX SOLID #7E729E' align=center valign=top class="RSLBlack"> 
              <br>
			  <input type="hidden" name="NODE" value="">
              <div id=everything style='height:370px;'> <font color=blue> 
                <div id=TheText></div>
                </font> 
                <div id=CostCentre style='display:none'> 
				
                  <table class="RSLBlack">
                    <tr> 
                      <td>Cost Centre</td>
                      <td> 
                        <input type=text name=txt_CostCentreDescription style='width:260px' maxlength=30 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_CostCentreDescription"></td>
                    </tr>
                    <tr> 
                      <td>Department</td>
                      <td> 
                        <input type=text name=txt_Department style='width:260px' maxlength=45 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_Department"></td>					  
                    </tr>
                    <td>Budget Holder</td>
                      <td> 
                        <input type=text name=txt_AccountableBody style='width:260px' maxlength=45 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_AccountableBody"></td>					  
                    </tr>
                    <tr> 
                      <td>Date Commence</td>
                      <td> 
                        <input type=text name=txt_DateStart style='width:260px' maxlength=10 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_DateStart"></td>					  
                    </tr>
                    <tr> 
                      <td>Date Ceases</td>
                      <td> 
                        <input type=text name=txt_DateEnd style='width:260px' maxlength=10 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_DateEnd"></td>					  
                    </tr>
                    <tr> 
                      <td>Cost Centre Active</td>
                      <td class="RSLBlack">Yes 
                        <input type=radio name=rdo_CostCentreActive value=1 class="RSLBlack" checked>
                        &nbsp;&nbsp;No 
                        <input type=radio name=rdo_CostCentreActive value=0 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_CostCentreActive	"></td>					  					  					  
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
                        <input type=hidden name=CC_A>
                        <input type=hidden name=CCID>
                        <input type=button name=saveButton value=' Save ' class="RSLButton" onclick="DoSave('COSTCENTRE')">
                        &nbsp; 
                        <input type=button name=cancelButton value=' Cancel ' class="RSLButton" onclick="DoCancel()">
                      </td>
                    </tr>
                  </table>
                </div>
                <div id=Head style='display:none'>
                <table class="RSLBlack" style='border-collapse:collapse' cellpadding=2>
                  <tr bgcolor=beige> 
                    <td style='border-top:1px solid black;border-left:1px solid black;'>Development 
                      Name</td>
                    <td style='border-top:1px solid black;border-right:1px solid black;'> 
                      <input type=text name=txt_HeadDescription style='width:210px;Border:1px solid black; ' maxlength=30 class="RSLBlack" readonly>
                    </td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_HeadDescription"></td>
                  </tr>
				 <tr bgcolor=beige>
					<td style='border-left:1px solid black;'>Contract Start</td>
					<td style='border-right:1px solid black;'>
                      <input type=text name=txt_ContractStart style='width:210px;Border:1px solid black;' maxlength=12 class="RSLBlack" readonly> 
					</td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_dtStart"></td>
                 </tr>
				 <tr bgcolor=beige>
					<td style='border-left:1px solid black;'>Anticipated Completion</td>
					<td style='border-right:1px solid black;'>
                      <input type=text name=txt_AnticipatedCompletion style='width:210px;Border:1px solid black;' maxlength=12 class="RSLBlack" readonly > 
					</td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_antendDate"></td>
                 </tr bgcolor=beige>
				 <tr bgcolor=beige>
					<td style='border-bottom:1px solid black;border-left:1px solid black;'>Actual Completion</td>
					<td style='border-right:1px solid black;border-bottom:1px solid black;'>
                      <input type=text name=txt_ActualCompletion style='width:210px;Border:1px solid black;' maxlength=12 class="RSLBlack" readonly > 
					</td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_endDate"></td>
                 </tr bgcolor=beige>
				 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><u><b>Development Summary</b></u></td>
                  </tr>
                  <tr> 
                    <td>Total Estimated Cost</td>
                    <td> 
                      <input type=text name=txt_headallocation maxlength=10 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right' readonly> 
                    </td>
                  </tr>
				  <tr><td colspan=2><hr height=1 style='border:1px dotted black'></td></tr>
                  <tr> 
                    <td>Opening Balance</td>
                    <td colspan=1 align=right> 
                      <input type=text name=HeadOpeningBalance maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right' readonly>
                    </td>
                  </tr>
                  <tr> 
                    <td>Total Spent/Committed</td>
                    <td colspan=1 align=right> 
                      <input type=text readonly name=TotalSpent maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                    </td>
                  </tr>
                  <tr> 
                    <td>Grants Received</td>
                    <td colspan=1 align=right> 
                      <input type=text readonly name=GrantsReceived maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                    </td>
                  </tr>
                  <tr> 
                    <td><b>Balance</b></td>
                    <td colspan=1 align=right> 
                      <input type=text readonly name=HeadBalance maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                    </td>
                  </tr>
				  <tr><td colspan=2><hr height=3 style='border:1px dotted black'></td></tr>				  
                  <tr> 
                    <td>Money Left</td>
                    <td colspan=1 align=right> 
                      <input type=text readonly name=MoneyLeft maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                    </td>
                  </tr>
				  <tr><td colspan=3><div id="ActiveDiv">&nbsp;</div></td></tr>
                  <tr> 
                    <td>&nbsp;</td>
                    <td align=right colspan=1> 
                      <input type=button name=AmendExpen value='Amend' class="RSLButton" onClick="EditExpend()">
                      <input type=hidden name=HDID>
                    </td>
                  </tr>
                </table>
                </div>
                <div id=Budget style='display:none'> 
                  <table class="RSLBlack" style='border-collapse:collapse' cellpadding=2>
                    <tr bgcolor=beige> 
                      <td style='border-left:1px solid black;border-top:1px solid black'>Expenditure Name</td>
                      <td style='border-right:1px solid black;border-top:1px solid black'> 
                        <input type=text name=txt_expenditurename style='width:210px;border:1px solid black' maxlength=30 class="RSLBlack" readonly>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_expenditurename"></td>					  					  
                    </tr>
					<tr bgcolor=beige> 
                      <td style='border-left:1px solid black;'>Opening Balance</td>
                      <td style='border-right:1px solid black;'> 
                        <input type=text name=txt_OPBalance style='width:210px;color:blue;' maxlength=10 class="RSLBlack" onBlur=getOB()>
						<input type=hidden name=hdn_OPBalance>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_OPBalance"></td>					  
                    </tr>
                    <tr bgcolor=beige> 
                      <td style='border-left:1px solid black;'>Expenditure Allocation</td>
                      <td style='border-right:1px solid black;'> 
                        <input type=text name=txt_expenditureallocation style='width:210px;border:1px solid black' maxlength=10 class="RSLBlack" readonly>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_expenditureallocation"></td>					  
                    </tr>
                    <tr bgcolor=beige> 
                      <td style='border-left:1px solid black;'>Total Spent</td>
                      <td style='border-right:1px solid black;'> 
                        <input type=text name=txt_expTotalSpent style='width:210px;border:1px solid black' maxlength=10 class="RSLBlack" readonly>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_expTotalSpent"></td>					  
                    </tr>
                    <tr bgcolor=beige> 
                      <td style='border-left:1px solid black;border-bottom:1px solid black;'>Expenditure Active</td>
                      <td style='border-right:1px solid black;border-bottom:1px solid black;'> 
                        <input type=text name=budgetactive style='width:210px;border:1px solid black' maxlength=10 class="RSLBlack" readonly>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_expTotalSpent"></td>					  
                    </tr>
                    <tr style='display:none'> 
                      <td>Date Created</td>
                      <td> 
                        <input type=text readonly name=budgetcreated maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px'>
                      </td>
                    </tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td><u><b>Accounts Interfacing</b></u></td></tr>					
                    <tr> 
                      <td>Nominal Code</td>
                      <td> 
                        <input type=text name=txt_nominalcode READONLY class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
						<img src="/js/FVS.gif" height=15 width=15 name="img_nominalcode">
                      </td>
                    </tr>
                    <tr> 
                      <td>Control Account</td>
                      <td> 
                        <input type=text name=txt_controlcode READONLY class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
						<img src="/js/FVS.gif" height=15 width=15 name="img_controlcode">
                      </td>
                    </tr>					
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
						<!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->					  
                        <input type=hidden name=Bmintotal>					  					  
                        <input type=hidden name=EX_A>
                        <input type=hidden name=EXID>
						<input type=hidden name=hid_nominalcode>
						<input type=hidden name=hid_controlcode>
						<input type=hidden name=txt_totValue style='width:100px;text-Align:right;Border:1px solid black;' maxlength=10 class="RSLBlack" readonly > 
                          <input type=button name="BDGEE" value=' Set Limits ' class="RSLButton" onClick="SetEmployeeLimits()">						
						<img src="/js/FVS.gif" height=15 width=15 name="img_dummy">						
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </td>
          </tr>
          <tr> 
            <TD HEIGHT=44 width=100% align=right>&nbsp;</TD>
            <td rowspan=2><IMG SRC="/myImages/My113.gif" WIDTH=72 HEIGHT=72></td>
          </tr>
          <TR> 
            <TD height=28 BGCOLOR=#133e71 align=right class="RSLWhite" valign=middle><b>rslManager 
              is a Reidmark eBusiness System</b></TD>
          </TR>
        </table>
      </form>
    </TD>
  </TR>
</TABLE>
<iframe src="/secureframe.asp"  name=FB_DEV_4466 style='display:none;width:800px;height:500px'></iframe> 
</body>
</html>

