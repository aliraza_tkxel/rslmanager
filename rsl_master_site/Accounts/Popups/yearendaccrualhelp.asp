<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<html>
<head>
<title>Accrual Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body  bgcolor=beige text="#000000" style='padding-left:5px'>
<table HEIGHT=100% WIDTH=100% cellspacing=5 valign=top border=0>
  <tr>
    <td valign=top> 
      <TABLE CELLPADDING=1 CELLSPACING=2 BORDER=0 valign=top> 
        <tr><td><u style='font-size:15px'><b>Year End Accruals</b></u></td></tr>
        <tr style='height:10px'><td></td></tr>
          <td> 
            This page enables all purchase orders that have not been accounted for in the nominal ledger to be accrued as at the end of the financial year.<BR><BR>
			Rows returned are firstly based on the following criteria : 
			<UL>
				<LI>All unreconciled items with an order date which exists within the previous financial year.
				<LI>All reconciled items with an order date which exists within the previous financial year
					and also a taxdate in the current financial year.
			</UL>
          </td> 
        </tr>
         <tr>
		  <td> 
		  	Additional filters may be applied :
			<UL>
				<LI>Filter by supplier.
				<LI>Filter by repair item status.
			</UL>
          </td>
        </tr>
		<tr>
			<td>Once filters have been applied click the 'Refresh' button to refresh the result set. </td>
		</tr>  
        <tr>  <td> 
		  	The result set shows the date of the order as taken from the purchase order, the item name as taken
			from the purchase item (for automated repair purchase orders this will be the repair description),
			the purchase order number the item is associated with, the type of the purchase item: either R for
			repair or G for General, supplier, current status of item (only for repairs), and finally the cost of
			the purchase item. These rows are grouped by purchase order.<BR><BR>
			These items can be selectively accrued by clicking individual checkboxes or accrued as a whole by
			clicking the select box titled 'Accrue All Items'. If 'Accrue All Items' is selected then the accrual 
			will take place only for the result set returned based on any filters applied <I>not</I> for the full
			set of accruable items in the system. To create the Acrrual simply click 'Create'.		
          </td>
        </tr>    
        <tr>  <td> 
		  When the accrual has taken place all items will be grouped by expenditure and inserted into the nominal.
		  The system will credit 2455 'Sundry Creditors' for the full acrrual amount and debit the individual expense
		  accounts with the grouped amount. You will then be redirected to the accruals listing page. 
          </td>
        </tr>    
		</table>
    </td>
  </tr>
 </table>
</body>
</html>
 