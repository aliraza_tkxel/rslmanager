<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	OpenDB()

	SQL = "SELECT E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, isnull(LIMIT,0) as limit, EMPLOYEELIMITID FROM F_EMPLOYEELIMITS EL INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = EL.EMPLOYEEID WHERE EXPENDITUREID = " & Request("EXID") & " ORDER BY FULLNAME"
	Call OpenRs(rsEm, SQL)
	
	if (NOT rsEm.EOF) then
		TableString = ""
		while NOT rsEM.EOF 
			TableString = TableString & "<TR><TD>" & rsEm("FULLNAME") & "</TD><TD width=110px align=right>" & FormatCurrency(rsEm("LIMIT")) & "</td><TD align=center BGCOLOR=#ffffff width=30px style='cursor:hand' nowrap onclick=""DeleteMe(" & rsEm("EMPLOYEELIMITID")& ")""><font color=red>DEL</font></TD></TR>"
			rsEm.movenext
		wend
		TableString = TableString & "<TFOOT><TR><TD colspan=3 align=center height='100%'></TD></TR></TFOOT>"
	else
		TableString = "<TR><TD colspan=3 align=center>No Employee Limits Set</TD></TR>"
		TableString = TableString & "<tfoot><TR><TD colspan=3 align=center height='100%'></TD></TR></tfoot>"		
	end if
		
	CloseDB()
%>
<html>
<head>
<title>Expenditure Limits</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript src="/js/formValidation.js"></script>
<script language=javascript>
var ItemArray = new Array(<%=ItemArray%>)

var FormFields = new Array()
FormFields[0] = "sel_TEAMS|Team|SELECT|Y"
FormFields[1] = "sel_ASSIGNTO|Employee|SELECT|Y"
FormFields[2] = "txt_LIMIT|Limit|CURRENCY|Y"

function CreateLimit(){
	if (!checkForm()) 
	{
	   return false
	}
	document.getElementById("EmployeeId").value  = document.getElementById("sel_ASSIGNTO").value 
	RSLFORM.hid_ACTION.value = "NEW"
	RSLFORM.action = "../ServerSide/EmployeeLimits_svr.asp"
	RSLFORM.target = "ServerLimitFrame"
	RSLFORM.submit()
	}

function DeleteMe(T_ID){
	RSLFORM.hid_ACTION.value = "DELETE"
	RSLFORM.hid_EMPLOYEELIMITID.value = T_ID	
	RSLFORM.action = "../ServerSide/EmployeeLimits_svr.asp"
	RSLFORM.target = "ServerLimitFrame"
	RSLFORM.submit()
	}

function GetEmployees(){
	if (RSLFORM.sel_TEAMS.value != "") {
		RSLFORM.action = "../../MyTasks/ServerSide/GetEmployees.asp"
		RSLFORM.target = "ServerLimitFrame"
		RSLFORM.submit()
		}
	else {
		RSLFORM.sel_ASSIGNTO.outerHTML = "<select name='sel_ASSIGNTO' id='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX'><option value=''>Please select an Employee</option></select>"
		}
	}	
</script>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000">
<table HEIGHT=100% WIDTH=100% cellspacing=5>
  <tr>
    <td> 
      <TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
        <tr> 
          <td valign="top" height="20"> 
            <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
              <TR> 
                <TD ROWSPAN=2 width="79"><img src="../Images/set%2Blimits.gif" width="115" height="20"></TD>
                <TD HEIGHT=19></TD>
              </TR>
              <TR> 
                <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <tr> 
          <td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
            <table>
              <FORM name="RSLFORM" method="POST">			
              <tr>
                  <td colspan=3>To add an employee limit first find the employee, 
                    then enter a valid amount the '<b>Limit</b>' field and then click 
                    on '<b>ADD</b>'. To update a previously stored limit just 
                    select the user and put a new amount in the '<b>Limit</b>' field 
                    to update the Employee Limit respectively. 
                <tr>
					
                  <td><b>Team :</b></td>
					<td><select name="sel_TEAMS" id="sel_TEAMS" class="textbox200" onchange="GetEmployees()" STYLE='WIDTH:258PX'>
						  <option value="">Please Select</option>
							<%
							Dim rsTeams
							OpenDB()
							SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM WHERE TEAMID <> 1 ORDER BY TEAMNAME"
							Call OpenRs(rsTeams, SQL)							
							While (NOT rsTeams.EOF)
							%>
							<option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
							<%
							  rsTeams.MoveNext()
							Wend
							If (rsTeams.CursorType > 0) Then
							  rsTeams.MoveFirst
							Else
							  rsTeams.Requery
							End If
							CloseRs(rsTeams)
							CloseDB()
							%>
						  <option value='NOT'>No Team</option>
					</select></td>
					<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
				</tr>
				<tr>
					
                  <td nowrap><b>Employee :</b></td>
					<td><select name="sel_ASSIGNTO" id= "sel_ASSIGNTO" class="textbox200" STYLE='WIDTH:258PX'>
						  <option value="">Please select an Employee</option>
					</select>
                  </td>
					<TD><image src="/js/FVS.gif" name="img_ASSIGNTO" width="15px" height="15px" border="0"></TD>							
				</tr>
				<tr>
                  <td nowrap><b>Limit : </b></td>
					<td><input type="text" class="textbox200" style='width:258px' name="txt_LIMIT" id="txt_LIMIT"></td>
					<TD><image src="/js/FVS.gif" name="img_LIMIT" width="15px" height="15px" border="0"></TD>							
				</tr>
              <tr>
              

                  <td colspan=2 align=right> 
                    <input name="hid_EMPLOYEELIMITID" type="hidden" value="">
                    <input name="hid_EXPENDITUREID" type="hidden" value="<%=Request("EXID")%>">
                    <input name="hid_TASK" type="hidden" value="1">
                    <input name="hid_ACTION" type="hidden" value="1">
                     <input name="EmployeeId" id ="EmployeeId" type="hidden" value="">
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                    <input type="button" onClick="window.close()" value=" CLOSE " class="RSLButton">
                    <input type="button" onclick="CreateLimit()" value=" ADD " class="RSLButton">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
              </tr>
              <tr>
                <td colspan=3><b>Employees with a purchase limit set for selected expenditure...</b></td>
              </tr>
              <tr>
                <td colspan=3 style='border-top:1px solid #133e71;'> 
                  <div id="MainDiv" style='overflow:auto;height:340;width:384' class="TA"> 
					<table cellspacing=0 cellpadding=3 style='border-collapse:collapse;behavior:url(../../Includes/Tables/tablehl.htc);' slcolor='' hlcolor='STEELBLUE' border=1 width=100% height=100%>
					<%=TableString%>
					</table>
                  </div>
                </td>
              </tr>
              </FORM>			  
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<iframe  src="/secureframe.asp" name="ServerLimitFrame" style='display:none'></iframe>
</body>
</html>
