<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	OpenDB()
	Call GetCurrentYear()
	FY = GetCurrent_YRange
   
    Company = Request("Company")
    if (Company="") then Company = "1"
	
	Dim lstApprovedBy
	DISTINCT_SQL = "F_COSTCENTRE CC " &_
			"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
			"WHERE EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND CC.COMPANYID = " &  company & " AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"

	Call BuildSelect(lstCostCentres, "sel_CostCentre", DISTINCT_SQL, " CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' style='width:265px'")
	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENT", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_developmentid, NULL, "textbox200", " style='width:265px'" )
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:265px'")
	
	CloseDB()
%>
<html>
<head>
<title>Expenditure Limits</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript src="/js/formValidation.js"></script>
<script language=javascript>

	var ItemArray = new Array(<%=ItemArray%>)

	var FormFields = new Array()
	FormFields[0] = "sel_TEAMS|Team|SELECT|Y"
	FormFields[1] = "sel_ASSIGNTO|Employee|SELECT|Y"
	FormFields[2] = "txt_LIMIT|Limit|CURRENCY|Y"

	function CreateLimit(){

		if (RSLFORM.sel_ASSIGNTO.value != ""){
		
			RSLFORM.hid_ACTION.value = "NEW"
			RSLFORM.hid_EXPENDITUREID.value = RSLFORM.sel_Expenditure.value
			RSLFORM.action = "../Serverside/EmployeeLimitList_svr.asp"
			RSLFORM.target = "ServerLimitFrame"	
			RSLFORM.submit()
			
			}
		else alert("You have not selected an employee. Please select one from the Employee select box.")
			
		}

	function DeleteMe(T_ID, E_ID, DESC, LIMIT){

		RSLFORM.hid_ACTION.value = "DELETE"
		RSLFORM.hid_EMPLOYEELIMITID.value = T_ID
		RSLFORM.hid_EXPENDITUREID.value = E_ID
		RSLFORM.hid_ExpenditureName.value = DESC
		RSLFORM.hid_LIMIT.value = LIMIT
		RSLFORM.action = "../Serverside/EmployeeLimitList_svr.asp"
		RSLFORM.target = "ServerLimitFrame"
		RSLFORM.submit()
		
		}

	function GetEmployees(){
	
		if (RSLFORM.sel_TEAMS.value != "") {
			RSLFORM.action = "../../MyTasks/Serverside/GetEmployeesForExpenditures.asp";
			RSLFORM.target = "ServerLimitFrame";
			RSLFORM.submit();
			}
		else {
			RSLFORM.sel_ASSIGNTO.outerHTML = "<select name='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX'><option value=''>Please select a team</option></select>";
			}
		}
	
	function Select_OnchangeFund(){
		PopulateListBox("", "Waiting for Data...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		RSLFORM.IACTION.value = "gethead";
		RSLFORM.action = "../../Finance/ServerSide/GetHeads.asp";
		RSLFORM.target = "ServerLimitFrame";
		RSLFORM.submit();
		}
	
	function Select_OnchangeHead(){
		PopulateListBox("", "Waiting for Data...", 1);	
		RSLFORM.IACTION.value = "change";
		RSLFORM.action = "../../Finance/ServerSide/GetExpenditures.asp";
		RSLFORM.target = "ServerLimitFrame";
		RSLFORM.submit();
		}

	function PopulateListBox(values, thetext, which){
	
	values = values.replace(/\"/g, "");
	thetext = thetext.replace(/\"/g, "");
	values = values.split(";;");
	thetext = thetext.split(";;");
		
		if (which == 2)
			var selectlist = document.forms.RSLFORM.sel_HEAD;
		else 
			var selectlist = document.forms.RSLFORM.sel_Expenditure;	
			selectlist.length = 0;
	
			for (i=0; i<thetext.length;i++){
				var oOption = document.createElement("OPTION");
	
				oOption.text=thetext[i];
				oOption.value=values[i];
				selectlist.options[selectlist.length]= oOption;
				}
	}
	
	
	function GenerateExpenditures(){
		RSLFORM.hid_ACTION.value = "GenExp"
		RSLFORM.action = "../Serverside/EmployeeLimitList_svr.asp"
		RSLFORM.target = "ServerLimitFrame2"
		RSLFORM.submit()
		}
		
	function DisplayLimit(){
		RSLFORM.hid_ExpenditureName.value = RSLFORM.sel_Expenditure.options(RSLFORM.sel_Expenditure.selectedIndex).text
		RSLFORM.theAddButton.disabled = false
	}
	
</script>
</head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000">
<table HEIGHT=100% WIDTH=100% cellspacing=5>
  <tr> 
    <td> 
      <TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
        <tr> 
          <td valign="top" height="20"> 
            <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
              <TR> 
                <TD ROWSPAN=2 width="79"><img src="../Images/set%2Blimits.gif" width="115" height="20"></TD>
                <TD HEIGHT=19></TD>
              </TR>
              <TR> 
                <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <tr> 
          <td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
            <table>
              <FORM name="THISFORM" method="POST">
			  	<input type=hidden name="EXPENDITURE_LEFT_LIST">
			  	<input type=hidden name="EMPLOYEE_LIMIT_LIST">
                <input type="hidden" name="hid_TOTALCCLEFT">								
			  </FORM>
			  <FORM name="RSLFORM" method="POST">
                <tr> 
                  <td colspan=3>To add an employee limit first find the employee, 
                    then enter a valid amount the '<b>Limit</b>' field and then 
                    click on '<b>ADD</b>'. To update a previously stored limit 
                    just select the user and put a new amount in the '<b>Limit</b>' 
                    field to update the Employee Limit respectively. 
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td><b>Team :</b></td>
                  <td> 
                    <select name="sel_TEAMS" class="textbox200" onChange="GetEmployees()" STYLE='WIDTH:258PX'>
                      <option value="">Please Select</option>
                      <%
							Dim rsTeams
							OpenDB()
							SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM WHERE TEAMID <> 1 ORDER BY TEAMNAME"
							Call OpenRs(rsTeams, SQL)							
							While (NOT rsTeams.EOF)
							%>
                      <option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
                      <%
							  rsTeams.MoveNext()
							Wend
							If (rsTeams.CursorType > 0) Then
							  rsTeams.MoveFirst
							Else
							  rsTeams.Requery
							End If
							CloseRs(rsTeams)
							CloseDB()
							%>
                      <option value='NOT'>No Team</option>
                    </select>
                  </td>
                  <TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>
                </tr>
                <tr> 
                  <td nowrap><b>Employee :</b></td>
                  <td> 
                    <select name="sel_ASSIGNTO" class="textbox200" STYLE='WIDTH:258PX' >
                      <option value="">Please select a team</option>
                    </select>
                    </td>
                  <TD><image src="/js/FVS.gif" name="img_ASSIGNTO" width="15px" height="15px" border="0"></TD>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td>Cost Centre :</td>
                  <td><%=lstCostCentres%> </td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td>Head :</td>
                  <td> 
                    <select id="sel_HEAD" name="sel_HEAD" class="RSLBlack" style='width:265px' onChange="GenerateExpenditures();Select_OnchangeHead();">
                      <option value="">Please Select a Cost Centre...</option>
                    </select>
                  </td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td>Expenditure :</td>
                  <td> 
                    <select id="sel_Expenditure" name="sel_Expenditure" class="RSLBlack" style='width:265px' onChange="DisplayLimit()">
                      <option value="">Please select a Head...</option>
                    </select>
					<input type="hidden" name="hid_ExpenditureName" value="">
                  </td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr ID="LimitRow"> 
                  <td nowrap><b>Limit : </b></td>
                  <td> 
                    <input type="text" class="textbox200" style='width:258px' name="txt_LIMIT">
                  </td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td colspan=2 align=right>
                    <input name="IACTION" type='hidden' value="change">
                    <input name="hid_EMPLOYEELIMITID" type="hidden" value="">
                    <input name="hid_EXPENDITUREID" type="hidden" value="">
					<input name="hid_LIMIT" type="hidden" value="">
                    <input name="hid_TASK" type="hidden" value="1">
                    <input name="hid_ACTION" type="hidden" value="1">
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                    <input type="button" onClick="window.close()" value=" CLOSE " class="RSLButton">
                    <input name="theAddButton" type="button" onclick="CreateLimit()" value=" ADD " class="RSLButton" disabled>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                </tr>
                <tr> 
                  <td colspan=3><b>Employees with a purchase limit set for selected 
                    expenditure...</b></td>
                </tr>
                <tr> 
                  <td colspan=3 style='border-top:1px solid #133e71;'> 
                    <div id="MainDiv" style='overflow:auto;height:230;width:384' class="TA"> 
                      <table cellspacing=0 cellpadding=3 style='border-collapse:collapse;behavior:url(../../Includes/Tables/tablehl.htc);' slcolor='' hlcolor='STEELBLUE' border=1 width=100% height=100%>
                        <tr><td align=center>Select an employee to view the list of selected expenditures.</td></tr>
						<tfoot>
						<tr><td height=100%></td></tr>
						</tfoot>
                      </table>
                    </div>
                  </td>
                </tr>
              </FORM>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<iframe  src="/secureframe.asp" name="ServerLimitFrame" style='display:none'></iframe>
<iframe  src="/secureframe.asp" name="ServerLimitFrame2" style='display:none'></iframe> 
</body>
</html>
