<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Transferred|TDate|120"
	SortASC(0) 	= "TRANSFERDATE ASC"
	SortDESC(0) = "TRANSFERDATE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "From Account|From_Account|300"
	SortASC(1) 	= "NL_ACCOUNT.FULLNAME ASC"
	SortDESC(1) = "NL_ACCOUNT.FULLNAME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "To Account|To_Account|300"
	SortASC(2) 	= "NL_ACCOUNT_1.FULLNAME ASC"
	SortDESC(2) = "NL_ACCOUNT_1.FULLNAME DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Amount|Amount|100"
	SortASC(3) 	= "Amount ASC"
	SortDESC(3) = "Amount DESC"	
	TDSTUFF(3)  = " "" ALIGN=RIGHT"" "
	TDFunc(3) = "FormatCurrency(|)"		
	
	ColData(4)  = "Reff:|Reff|300"
	SortASC(4) 	= "Reff ASC"
	SortDESC(4) = "Reff DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""
			
	
	
	PageName = "BalanceTransferDTL.asp"
	EmptyText = "No Relevant Balance Transfer found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND NL_BALANCETRANSFER.COMPANYID = '" & rqCompany & "' "
	end if
	
	'RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""BLTID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	Accid=request("Accid")
	if Accid="" then
		SupFilter = "WHERE  1=1 "
	else
		SupFilter = "WHERE  NL_BALANCETRANSFER.ACCOUNTFRM=" & Accid
	end if
	'SupFilter = "WHERE  1=1 "
	
	'if (Request("sel_SUPPLIER") <> "") then
	'	SupFilter =SupFilter & " and FINV.SUPPLIERID = '" & Request("sel_SUPPLIER") & "' "
	'end if
	
	'if ((Request("sup_dt") <> "''" )and  (len(Request("sup_dt") ) <> 0)) then
	
	'	SupFilter = SupFilter & " and PBACS.PROCESSDATE = " & Request("sup_dt") 
	'end if

	
	

	SQLCODE="SELECT  NL_BALANCETRANSFER.BLTID,  NL_BALANCETRANSFER.AMOUNT AS Amount, NL_ACCOUNT.NAME AS From_Account, NL_ACCOUNT_1.NAME AS To_Account, " &_
            " NL_BALANCETRANSFER.TRANSFERDATE AS TDate, NL_BALANCETRANSFER.REFF AS Reff " &_
			" FROM  NL_BALANCETRANSFER  INNER JOIN " &_
            " NL_ACCOUNT ON NL_BALANCETRANSFER.ACCOUNTFRM = NL_ACCOUNT.ACCOUNTID INNER JOIN " &_
            " NL_ACCOUNT NL_ACCOUNT_1 ON NL_BALANCETRANSFER.ACCOUNTTO = NL_ACCOUNT_1.ACCOUNTID "  & SupFilter & CompanyFilter &_
			" ORDER BY " & orderBy


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
	Call OpenDB()
	 Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:150px'")	
     Call CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Balance Transfer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array()

function SubmitPage()
	{
		
    var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;

    location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Company=" + Company  ;
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<table class="RSLBlack"><tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><%=lstCompany%>
</td>

<td><img src="/js/FVS.gif" width="15" height="15" name="img_DT"></td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()" id=button1 name=button1>
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>