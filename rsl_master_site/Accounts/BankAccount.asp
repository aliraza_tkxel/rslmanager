<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="NominalLedger/Includes/functions.asp" -->
<!--#include file="Includes/functions.asp" -->
<!--#include file="Includes/depositfunctions.asp" -->
<%
	StartZ = Timer 
	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size
	Dim nopounds
	Dim searchSQL,searchName
	Dim lstAccounts, balance
	Dim open_what, lstBankAccounts, pettycash_account_id, current_account_id

	Dim PS, PE, FY
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE,CompanyFilter
		
	open_what = Request("OPENDIV")
	if open_what = "" then open_what = 0 End If
	
	OpenDB()
    
    CompanyFilter = ""
    rqCompany = Request("COMPANYFilter")
	if (rqCompany <> "") then
		CompanyFilter = " where b.COMPANYID = '" & rqCompany & "' "
	end if

	FY = Request("FY")  
	Call get_year(FY)
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	pettycash_account_id = get_account_nos("PETTYCASH")
	current_account_id = get_account_nos("BANKCURRENTACCOUNTID")
	getAccounts()
	
	' WE NEED TO CURRENT BALANCE ON ALL BANK ACCOUNTS. FIRST WE NEED TO DETERMINE WHICH KIND
	' OF ACCOUNT WE ARE DEALING WITH. THIS IN TURN DETERMINES WHICH TABLE WE LOOK IN FOR OUR BALANCE
	Function get_cb_balance(NOMID)
		
		If CINT(NOMID) = CINT(current_account_id) Then
			get_cb_balance = get_cashbook_opening_balance(FY) + get_cashbook_balance(5000, PS, PE, 999)
			'get_cb_balance = 0
		ElseIf NOMID = pettycash_account_id Then
			SQL = "SELECT BALANCE FROM NL_OPENINGBALANCES WHERE ACCOUNTID = " & Cint(Request(""))
		Else	' DEPOSIT ACCOUNT		
			get_cb_balance = get_deposit_cashbook_ob(NOMID, FY) + get_deposit_cashbook_balance(NOMID, 5000, PS, PE, 999)
		End If
		
	End Function
	
	Function get_st_balance(NOMID)
		
		If CINT(NOMID) = CINT(current_account_id) Then
			get_st_balance = get_statement_opening_balance(FY) + get_statement_balance(5000, PS, PE, 999)
			'get_st_balance = 0
		ElseIf NOMID = pettycash_account_id Then
			SQL = "SELECT BALANCE FROM NL_OPENINGBALANCES WHERE ACCOUNTID = " & Cint(Request(""))
		Else		
			get_st_balance = get_deposit_statement_ob(NOMID) + get_deposit_statement_balance(NOMID, 5000, PS, PE, 999)
	End If
		
	End Function
	' retrives team details from database and builds 'paged' report
	Function getAccounts()
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT	NOMINALACCOUNT, ACCOUNTID, ACCOUNTTYPE, ISNULL(B.ACCOUNTNAME, 'N/A') AS ACCOUNTNAME, "&_
					"		ISNULL(B.BANK, 'N/A') AS BANK, " &_
					"		ISNULL(B.ACCOUNTNO, ' ') AS ACCOUNTNO, ISNULL(OPENINGBALANCE,0) AS OB " &_
					"FROM 	NL_BANKACCOUNT B " & CompanyFilter
							
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
				
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0	
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
			For intRecord = 1 to rsSet.PageSize
				If Cint(rsSet("ACCOUNTTYPE")) = 1 Then
					account_balance = balance
				ELse
					account_balance = 0
				End If
				account_name = rsSet("ACCOUNTNAME")
				str_data = str_data & "<TR STYLE='CURSOR:HAND'>" &_
					"<TD ONCLICK=""load_account(" & rsSet("ACCOUNTID") & ")"">" & rsSet("BANK") & "</TD>" &_
					"<TD ONCLICK=""load_account(" & rsSet("ACCOUNTID") & ")"">" & rsSet("ACCOUNTNAME") & "</TD>" &_										
					"<TD ONCLICK=""load_statement("&Cint(rsSet("NOMINALACCOUNT"))&")"" ALIGN=RIGHT STYLE='color:silver;CURSOR:HAND'><i>Coming Soon</i></TD>" &_
					"<TD ONCLICK=""load_cashbook("&Cint(rsSet("NOMINALACCOUNT"))&")"" ALIGN=RIGHT STYLE='COLOR:BLUE;CURSOR:HAND'>" & FormatCurrency(get_cb_balance(rsSet("NOMINALACCOUNT")),2) &  " </TD>" &_
					"</TR>" 

					'THIS LINE REPLACED BY ME TEMPORARILY
					'"<TD ONCLICK=""load_statement("&Cint(rsSet("NOMINALACCOUNT"))&")"" ALIGN=RIGHT STYLE='COLOR:BLUE;CURSOR:HAND'>" & FormatCurrency(get_st_balance(rsSet("NOMINALACCOUNT")),2) &  " </TD>" &_
					
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=8 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
			"<A HREF = 'BANKACCOUNT.asp?FY="&FY&"&page=1'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'BANKACCOUNT.asp?FY="&FY&"&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='BANKACCOUNT.asp?FY="&FY&"&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='BANKACCOUNT.asp?FY="&FY&"&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			
		End If
		
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=6 ALIGN=CENTER><B>No Bank Accounts exist within the system !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=7 ALIGN=CENTER STYLE='BACKGROUND-COLOR:WHITE'>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	End Function
	
	Function get_account_nos(str_defaultname)
	
		SQL = " SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = '" & str_defaultname & "'"
		Call OpenRs(rsSet, SQL)
		If not rsSet.EOF Then
			get_account_nos = rsSet(0)
		Else
			get_account_nos = -1
		End If	
		
	End Function
	
	strBankSQL = " NL_ACCOUNT A WHERE  ISBANKABLE = 1 AND NOT EXISTS " &_
				" (SELECT * FROM NL_BANKACCOUNT WHERE NOMINALACCOUNT = A.ACCOUNTID) " 
	strAccountSQL  = " NL_BANKACCOUNTTYPE  A WHERE  NOT EXISTS " &_
						" (SELECT * FROM NL_BANKACCOUNT WHERE ACCOUNTTYPE = A.BANKACCOUNTTYPEID AND BANKACCOUNTTYPEID IN (1,3)) "			
				
	Call BuildSelect(lstAccounts, "sel_ACCOUNTTYPE", strAccountSQL, "BANKACCOUNTTYPEID, BANKACCOUNTTYPE", "BANKACCOUNTTYPE", "Please Select", NULL, NULL, "textbox200", " TABINDEX=1")	
	Call BuildSelect(lstBankAccounts, "sel_NOMINALACCOUNT", strBankSQL, "ACCOUNTID, ACCOUNTNUMBER + ' ' + NAME AS ACCNAME", "ACCOUNTNUMBER", "Please Select", NULL, NULL, "textbox200", " TABINDEX=1 ")	
	Call BuildSelect(lstFY, "sel_FY", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", FY, NULL, "textbox200", " tabindex=1 onchange=""ChangeYear()"" ")
   Call BuildSelect(lstCompany, "sel_COMPANYID", "G_Company", "CompanyId, Description", "CompanyId", null, l_companyid, NULL, "textbox200", "  ")
    Call BuildSelect(lstCompanyFilter, "sel_COMPANYFilter", "G_Company", "CompanyId, Description", "CompanyId", "Please Select Company", rqCompany, NULL, "textbox200", " onchange=""ChangeYear()""  ")

	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --&gt; Bank Accounts</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

    var FormFields = new Array();
    FormFields[0] = "sel_ACCOUNTTYPE|Account Type|SELECT|Y"
    FormFields[1] = "txt_ACCOUNTNAME|Account Name|TEXT|Y"
    FormFields[2] = "txt_BANK|Bank|TEXT|N"
    FormFields[3] = "txt_ADD1|Address 1|TEXT|N"
    FormFields[4] = "txt_ADD2|Address 2|TEXT|N"
    FormFields[5] = "txt_TOWN|Town|TEXT|N"
    FormFields[6] = "txt_COUNTY|County|TEXT|N"
    FormFields[7] = "txt_POSTCODE|Postcode|POSTCODE|N"
    FormFields[8] = "txt_ACCOUNTNO|Account Number|INTEGER|N"
    FormFields[9] = "txt_SORTCODE|Sort Code|INTEGER|N"
    FormFields[10] = "txt_OPENINGBALANCE|Opening Balance|CURRENCY|Y"
    FormFields[11] = "sel_NOMINALACCOUNT|Associated Nominal Account|SELECT|Y"

    // called by save and amend buttons
    function click_save(str_action) {
        if (!checkForm()) return false;
        RSLFORM.target = "";
        RSLFORM.hid_ACTION.value = str_action
        RSLFORM.action = "serverside/bankaccount_srv.asp"
        RSLFORM.submit();
    }

    function ChangeYear() {
        RSLFORM.target = "";
        RSLFORM.BTN_SAVE.disabled = true
        RSLFORM.BTN_AMEND.disabled = true
        RSLFORM.action = "BankAccount.asp?FY=" + document.getElementById("sel_FY").value + "&CompanyFilter=" + document.getElementById("sel_COMPANYFilter").value
        RSLFORM.submit();
    }



    // called by delete button
    function del_account(int_id) {

        event.cancelBubble = true
        var answer
        answer = window.confirm("Are you sure you wish to delete this account?")

        // if yes then send to server page to delete
        if (answer) {
            RSLFORM.target = "frm_bank_account";
            RSLFORM.hid_ACTION.value = "DELETE"
            RSLFORM.hid_ACCOUNTID.value = int_id;
            RSLFORM.action = "serverside/bankaccount_srv.asp"
            RSLFORM.submit();
        }

    }

    // called when user clicks on a row
    function load_account(int_id) {
        RSLFORM.target = "frm_bank_account";
        RSLFORM.hid_ACTION.value = "LOAD"
        RSLFORM.hid_ACCOUNTID.value = int_id;
        RSLFORM.action = "serverside/bankaccount_srv.asp?FY=<%=FY%>"
        RSLFORM.submit();
    }

    // swaps save button for amend button and vice versa
    // called by server side page
    function swap_button(int_which_one) { //   1 = show amend, 2 = show save

        if (int_which_one == 2) {
            document.getElementById("BTN_SAVE").style.display = "block"
            document.getElementById("BTN_AMEND").style.display = "none"
        }
        else {
            document.getElementById("BTN_SAVE").style.display = "none"
            document.getElementById("BTN_AMEND").style.display = "block"
        }
    }

    // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
    function swap_div(int_which_one) {
        if (int_which_one == 1) {
            new_div.style.display = "none";
            table_div.style.display = "block";
            document.getElementById("tab_dev").src = 'img/account-open.gif';
            document.getElementById("tab_dev_team").src = 'img/new-previous.gif';
            document.getElementById("tab_amend_dev").src = 'img/blank.gif';
        }
        else if (int_which_one == 2) {
            clear()
            new_div.style.display = "block";
            table_div.style.display = "none";
            document.getElementById("tab_dev").src = 'img/account-closed.gif';
            document.getElementById("tab_dev_team").src = 'img/new-open.gif';
            document.getElementById("tab_amend_dev").src = 'img/blank.gif';
            swap_button(2)
        }
        else {
            new_div.style.display = "block";
            table_div.style.display = "none";
            document.getElementById("tab_dev").src = 'img/account-closed.gif';
            document.getElementById("tab_dev_team").src = 'img/new-closed.gif';
            document.getElementById("tab_amend_dev").src = 'img/amend-open.gif';
            swap_button(3)
        }
        checkForm()
    }

    // clear form fields
    function clear() {
        RSLFORM.reset();
    }

    function JumpPage() {
        iPage = document.getElementById("QuickJumpPage").value
        if (iPage != "" && !isNaN(iPage))
            location.href = "bankaccount.asp?FY=<%=FY%>&page=" + iPage
        else
            document.getElementById("QuickJumpPage").value = ""
    }

    function quick_find() {
        location.href = "bankaccount.asp?name=" + RSLFORM.findemp.value;
    }

    function sortcode() {

        var s1, s2, s3, st
        s1 = RSLFORM.txt_SORTCODE_1.value;
        s2 = RSLFORM.txt_SORTCODE_2.value;
        s3 = RSLFORM.txt_SORTCODE_3.value;

        st = "" + s1.toString() + "" + s2.toString() + "" + s3.toString()
        RSLFORM.txt_SORTCODE.value = st;
    }

    // SPLIT SORTCODE AND ENTER INTO APPROPRIATE BOXES
    function split_sortcode() {

        var st, s1, s2, s3
        st = new String(RSLFORM.txt_SORTCODE.value);

        s1 = st.substr(0, 2);
        s2 = st.substr(2, 2);
        s3 = st.substr(4, 2);
        RSLFORM.txt_SORTCODE_1.value = s1;
        RSLFORM.txt_SORTCODE_2.value = s2;
        RSLFORM.txt_SORTCODE_3.value = s3;
    }

    function load_statement(accountID) {
        return false;
        if (accountID == <%=current_account_id %>)
        location.href = "statement.asp?FY=<%=FY%>"
		else if (accountID == <%=pettycash_account_id %>)
        location.href = "pettycashstatement.asp?FY=<%=FY%>"
		else
        location.href = "depositstatement.asp?FY=<%=FY%>&ACCOUNTID=" + accountID
    }

    function load_cashbook(accountID) {

        if (accountID == <%=current_account_id %>)
        location.href = "cashbook.asp?FY=<%=FY%>"
		else if (accountID == <%=pettycash_account_id %>){
            alert("The Petty Cash Cashbook will be coming soon.")
            return false
            location.href = "pettycash_cashbook.asp?FY=<%=FY%>"
        }
		else
        location.href = "depositcashbook.asp?FY=<%=FY%>&ACCOUNTID=" + accountID
    }

    function open_any_div(divID) {

        if (divID != 0)
            swap_div(divID);
    }



</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();open_any_div(<%=open_what%>)" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=RSLFORM method=post> 
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR> 
	  <TD ></TD>
	  <TD ></TD>
	  <TD ></TD>
	  <TD height=19 align=right nowrap>Company:&nbsp;<%=lstCompanyFilter%></td>
	</TR>
    <TR> 
	  <TD ROWSPAN=2> <IMG NAME="tab_dev" ONCLICK='swap_button(1);swap_div(1)' style='cursor:hand' SRC="img/account-open.gif" WIDTH=73 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_dev_team" ONCLICK='swap_button(2);swap_div(2)' style='cursor:hand' SRC="img/new-previous.gif" WIDTH=60 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_amend_dev" SRC="img/blank.gif" WIDTH=74 HEIGHT=20 BORDER=0></TD>
	  <TD height=19 align=right nowrap>Financial Year:&nbsp;<%=lstFY%></td>
	</TR>
	<TR> 
	  <TD BGCOLOR=#133E71 > <IMG SRC="images/spacer.gif" WIDTH=543 HEIGHT=1></TD>
	</TR>
</TABLE>
  <!-- End ImageReady Slices -->
    <DIV ID=new_div STYLE='DISPLAY:NONE'> 
    <table width=750 style="BORDER-RIGHT: SOLID 1PX #133E71" cellpadding=1 cellspacing=2 border=0 class="TAB_TABLE">
      <TR STYLE='HEIGHT:10PX'>
        <TD></TD>
      </TR>
      <tr> 
        <td>Account Type:</td>
        <td> <%=lstAccounts%> <image src="/js/FVS.gif" name="img_ACCOUNTTYPE" width="15px" height="15px" border="0"></td>
        <td> Address 1:</td>
        <td> 
          <input type="textbox" class="textbox200" name="txt_ADD1" maxlength=50 size=30 tabindex=9>
          <image src="/js/FVS.gif" name="img_ADD1" width="15px" height="15px" border="0"></td>
      </tr>
      <tr> 
        <td>Nominal Account :</td>
        <td><%=lstBankAccounts%> 
			<image src="/js/FVS.gif" name="img_NOMINALACCOUNT" width="15px" height="15px" border="0">
		</td>
        <td>Address 2:</td>
        <td> 
          <input type="textbox" class="textbox200" name="txt_ADD2" maxlength=50 size=30 tabindex=10>
          <image src="/js/FVS.gif" name="img_ADD2" width="15px" height="15px" border="0"></td>
      </tr>
      <tr>
        <td> Account Name</td>
        <td><input type="textbox" class="textbox200" name="txt_ACCOUNTNAME" maxlength=100 size=30 tabindex=2>
          <image src="/js/FVS.gif" name="img_ACCOUNTNAME" width="15px" height="15px" border="0">
		   </td>
        <td>Town:</td>
        <td>
          <input type="textbox" class="textbox200" name="txt_TOWN" maxlength=50 size=30 tabindex=11>
		  <image src="/js/FVS.gif" name="img_TOWN" width="15px" height="15px" border="0">
        </td>
      </tr>
      <tr> 
        <td>Bank:</td>
        <td> 
          <input type="textbox" class="textbox200" name="txt_BANK" maxlength=100 size=30 tabindex=3>
          <image src="/js/FVS.gif" name="img_BANK" width="15px" height="15px" border="0"></td>
        <td>County:</td>
        <td>
          <input type="textbox" class="textbox200" name="txt_COUNTY" maxlength=100 size=30 tabindex=12>
		  <image src="/js/FVS.gif" name="img_COUNTY" width="15px" height="15px" border="0">
        </td>
      </tr>
      <tr> 
        <td> Account No:</td>
        <td> 
          <input type="textbox" class="textbox200" name="txt_ACCOUNTNO" maxlength=8 size=30 tabindex=4>
          <image src="/js/FVS.gif" name="img_ACCOUNTNO" width="15px" height="15px" border="0"></td>
        <td>Postcode:</td>
        <td>
          <input type="textbox" class="textbox200" name="txt_POSTCODE" maxlength=100 size=30 tabindex=13>
		  <image src="/js/FVS.gif" name="img_POSTCODE" width="15px" height="15px" border="0">
          </td>
      </tr>
      <tr> 
        <td> Sort Code:</td>
        <TD nowrap> 
          <INPUT TYPE=TEXT NAME='txt_SORTCODE_1' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX' tabindex="5">
          &nbsp;- 
          <INPUT TYPE=TEXT NAME='txt_SORTCODE_2' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX' tabindex="6">
          &nbsp;- 
          <INPUT TYPE=TEXT NAME='txt_SORTCODE_3' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX' tabindex="7">
          <INPUT TYPE=HIDDEN NAME='txt_SORTCODE'>
          <image src="/js/FVS.gif" name="img_SORTCODE" width="15px" height="15px" border="0"> 
        </TD>
        <td>Company:</td>
        <td> <%=lstCompany %></td>
      </tr>
      <tr> 
        <td>Opening Balance</td>
        <td>
          <input type="textbox" class="textbox200" name="txt_OPENINGBALANCE" maxlength=100 size=30 tabindex=8 readonly>
          <image src="/js/FVS.gif" name="img_OPENINGBALANCE" width="15px" height="15px" border="0"></td>
        <td>&nbsp;</td>
        <td> 
		  <input type="hidden" name="hid_FY" value="<%=FY%>">
          <input type="button" id="BTN_SAVE" name"BTN_SAVE" value="Save" class=rslbutton onClick="click_save('NEW')" style='display:block' tabindex="14">
          <input type="button" id="BTN_AMEND" name='BTN_AMEND' value="Amend" class=rslbutton onClick="{ click_save('AMEND'); }" style='display:none' tabindex="15">
        </td>
      </tr>
      <tr> 
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </DIV>
<DIV ID=table_div>
	<TABLE WIDTH=750 CLASS="TAB_TABLE" CELLPADDING=1 CELLSPACING=2 STYLE="behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=1 hlcolor=THISTLE>
		<THEAD><TR STYLE="HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71"><TD COLSPAN=5 CLASS='TABLE_HEAD'></TD></TR>
		<TR> 
		  <TD WIDTH=180PX CLASS='TABLE_HEAD'> <B>Bank</B> </TD>
  		  <TD WIDTH=180PX CLASS='TABLE_HEAD'> <B>Account Name</B> </TD>		  
 		  <TD WIDTH=150PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Statement</B> </TD>
		  <TD WIDTH=150PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Casbook</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<%=str_data%>
	</TABLE>
</DIV>
<INPUT TYPE='HIDDEN' NAME='hid_ACCOUNTID'>
<INPUT TYPE='HIDDEN' NAME='hid_ACTION'>
</form>
<div style='position:absolute;top:80;left:430;z-Index:100'>
<%

%>
</div>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_bank_account width=600px height=400px style='display:none'></iframe>
</BODY>
</HTML>

