<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	Function YearEndAccrual(strWord)
		YearEndAccrual = "<b>YE</b> " & characterPad(strWord,"0", "l", 6)
	End Function

	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match	
	Dim TDFunc	   (3)
	
	RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""YENDACCRLID"")  & "")"""" """ 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
	ColData(0)  = "Reference|YENDACCRLID|120"
	SortASC(0) 	= "YENDACCRLID ASC"
	SortDESC(0) = "YENDACCRLID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "YearEndAccrual(|)"
	
	ColData(1)  = "Date|sdate|180"
	SortASC(1) 	= "sdate ASC"
	SortDESC(1) = "sdate DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "Amount|v|100"
	SortASC(2)  = "ye.AMOUNT ASC"
	SortDESC(2) = "ye.AMOUNT DESC"	
	TDSTUFF(2)  = " "" ALIGN=RIGHT"""
	TDFunc(2) = "FormatCurrency(|)"		

	ColData(3)  = "Created By|empname|250"
	SortASC(3) = "empname ASC"
	SortDESC(3) = "empname DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		
	
	PageName = "YearEndAccruals_dtl.asp"
	EmptyText = "No Relevant Year End Accruals List found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""ACCRUALID"") &  "")"""" """ 
	
	Dim orderBy
	OrderByMatched = 0
	orderBy = DefaultOrderBy
	Call SetSort()

    CompanyFilter = ""
    rqCompany = Request("COMPANY")
	if (rqCompany <> "") then
        CompanySQL = " where isnull(ye.COMPANYID,1) = '" & rqCompany & "' "
    end if

	SQLCODE=" SELECT '' as EMPTY,YENDACCRLID, convert(varchar(12),ye.ACCDATE,103) as sdate, isnull(ye.AMOUNT,0) as v, e.FIRSTNAME + ' ' + e.LASTNAME empname " &_
        		" FROM    NL_YEARENDACCRAL  ye INNER JOIN AC_LOGINS ac on ye.USERUD=ac.EMPLOYEEID " &_
			" inner join E__EMPLOYEE e on e.EMPLOYEEID=ac.EMPLOYEEID " & CompanySQL  &_
        		" ORDER BY "& orderBy	
	'RW SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

    OpenDB()
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " onchange='click_go()' style='width:150px'")	
    CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<!-- #BeginEditable "doctitle" -->
<TITLE>RSL Manager Finance --> Year End Accruals List</TITLE>
<!-- #EndEditable -->
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
    var FormFields = new Array()

    function load_me(YACCRID) {

        location.href = "YearEndAccrualPO_dtl.asp?YACCRID=" + YACCRID + "&IPREVPAGE=<%=intpage%>&PREV_SORT=<%=orderBy%>"
    }

    function click_go() {

        location.href = "YearEndAccruals_dtl.asp?Company=" + document.getElementById("sel_COMPANY").value;
    }
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <tr> 
    <TD><b>Company</b> : <%=lstCompany %></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="/Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>