<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()

Call GetCurrentYear()
FY = GetCurrent_YRange

Dim lstApprovedBy
DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
		"WHERE CC.COSTCENTREID NOT IN (11) " &_ 
		"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"


Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")

CloseDB()

TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager - Budget Virement</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	var FormFields = new Array()
	
	function PopulateListBox(values, thetext, which){
		if (which == 3)
			var selectlist = document.forms.THISFORM.sel_EXPENDITURE3;
		else if (which == 4)
			var selectlist = document.forms.THISFORM.sel_HEAD2;
		else if (which == 2){
			var selectlist = document.forms.THISFORM.sel_HEAD;
			PopulateListBox(values, thetext, 4)
			}
		else 
			var selectlist = document.forms.THISFORM.sel_EXPENDITURE1;	

		values = values.replace(/\"/g, "");
		thetext = thetext.replace(/\"/g, "");
		values = values.split(";;");
		thetext = thetext.split(";;");
		selectlist.length = 0;

		for (i=0; i<thetext.length;i++){
			var oOption = document.createElement("OPTION");

			oOption.text=thetext[i];
			oOption.value=values[i];
			selectlist.options[selectlist.length]= oOption;
			}
		}
		
	function Select_OnchangeFund(){
		document.getElementById("txt_EXPBALANCE1").value = "0.00";
		document.getElementById("txt_EXPALLOC1").value = "0.00";		

		document.getElementById("txt_EXPALLOC3").value = "0.00";				
		document.getElementById("txt_EXPBALANCE3").value = "0.00";		
		PopulateListBox("", "Waiting for Data...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		PopulateListBox("", "Please Select a Head...", 3);		
		THISFORM.IACTION.value = "gethead";
		THISFORM.action = "/Finance/ServerSide/GetHeads.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		UpdateFigures()
		}
	
	function Select_OnchangeHead(VAL){
		document.getElementById("txt_EXPBALANCE" + VAL).value = "0.00";
		document.getElementById("txt_EXPALLOC" + VAL).value = "0.00";		
		PopulateListBox("", "Waiting for Data...", VAL);	
		THISFORM.IACTION.value = "change";
		THISFORM.action = "ServerSide/GetExpendituresForVirement.asp?WHICHBOX=" + VAL;
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		UpdateFigures()		
		}

	function SetPurchaseLimits(VAL){
		eIndex = document.getElementById("sel_EXPENDITURE" + VAL).selectedIndex;
		eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST" + VAL).value;
		eAmounts = eAmounts.split(";;");
		eEMLIMITS = document.getElementById("EXPENDITURE_LIST" + VAL).value;
		eEMLIMITS = eEMLIMITS.split(";;");
	
		document.getElementById("txt_EXPBALANCE" + VAL).value = FormatCurrencyComma(eAmounts[eIndex]);
		document.getElementById("txt_EXPALLOC" + VAL).value = FormatCurrencyComma(eEMLIMITS[eIndex]);	
		UpdateFigures()
		}

	function UpdateFigures(){
		FormFields.length = 1
		FormFields[0] = "txt_TRANSFERFIGURE|Transfer Amount|CURRENCY|Y"
		if (!checkForm()) 
			TheValue = 0
		else
			TheValue = parseFloat(THISFORM.txt_TRANSFERFIGURE.value)

		THISFORM.txt_EXPALLOC1_N.value = FormatCurrencyComma(parseFloat(THISFORM.txt_EXPALLOC1.value.replace(",", "")) - TheValue)
		THISFORM.txt_EXPBALANCE1_N.value = FormatCurrencyComma(parseFloat(THISFORM.txt_EXPBALANCE1.value.replace(",", "")) - TheValue)

		THISFORM.txt_EXPALLOC3_N.value = FormatCurrencyComma(parseFloat(THISFORM.txt_EXPALLOC3.value.replace(",", "")) + TheValue)
		THISFORM.txt_EXPBALANCE3_N.value = FormatCurrencyComma(parseFloat(THISFORM.txt_EXPBALANCE3.value.replace(",", "")) + TheValue)		
		}

	function SaveTransaction(){
		FormFields[0] = "txt_TRANSFERFIGURE|Transfer Amount|CURRENCY|Y"
		FormFields[1] = "sel_EXPENDITURE1|From Expenditure|SELECT|Y"
		FormFields[2] = "sel_EXPENDITURE3|To Expenditure|SELECT|Y"
		if (!checkForm()) return false
		if (parseFloat(THISFORM.txt_TRANSFERFIGURE.value.replace(",", "")) <= 0) {
			alert("The transfer amount must be a positive figure.\nPlease re-enter the value.")
			return false;
			}
		if (parseFloat(THISFORM.txt_EXPBALANCE1_N.value.replace(",", "")) < 0) {
			alert("The amount you have set to be transferred is more then the budget left in the selected Expenditure : " + THISFORM.sel_EXPENDITURE1.options[THISFORM.sel_EXPENDITURE1.selectedIndex].text + ".\nThe maximum you can transfer is �" + THISFORM.txt_EXPBALANCE1.value + ".\n\nPlease lower the value and try again.")
			return false;
			}
		THISFORM.action = "ServerSide/BudgetVirement_svr.asp"
		THISFORM.target = ""
		THISFORM.submit()
		document.getElementById("SaveButton").disabled = true
		}									
</SCRIPT>		
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<iframe  src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style='display:none'></iframe> 
&nbsp;<b>PERIOD : <%=FormatDateTime(GetCurrent_StartDate,1)%> - <%=FormatDateTime(GetCurrent_EndDate,1)%></b>
<TABLE>
<FORM NAME=THISFORM method=post>
<TR><TD>

<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3>
	<TR bgcolor=#133E71 style='color:white'> 
		<TD colspan=9><b>VIREMENT FROM:</b></TD>
	</TR>
	<TR>
		<TD>&nbsp;Cost Centre : </TD><TD><%=lstCostCentres%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_COSTCENTRE"></td>	
	</TR>
	<TR>
		<TD>&nbsp;Head : </TD><TD>
							<select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead(1)" tabindex=4>
                            <option value="">Please Select a Cost Centre...</option>
                          </select>
						  </TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_HEAD"></td>	
	</TR>
	<TR>
		<TD>&nbsp;Expenditure : </TD><TD>                        
						<select id="sel_EXPENDITURE1" name="sel_EXPENDITURE1" class="TEXTBOX200" onchange="SetPurchaseLimits(1)" tabindex=4>
                          <option value="">Please select a Head...</option>
                        </select>
						</TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_EXPENDITURE1"></td>	
	</TR>
	<TR>
		<TD>&nbsp;Total Allocated : </TD><TD>
		<input type="text" name="txt_EXPALLOC1" class="textbox200" value="0.00" tabindex=-1 readonly>
		</TD><td></td>	
	</TR>
	<TR>
		<TD>&nbsp;Expenditure Left : </TD><TD>
		<input type="text" name="txt_EXPBALANCE1" class="textbox200" value="0.00" tabindex=-1 readonly>
		<INPUT TYPE=HIDDEN NAME="IACTION">
		<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LEFT_LIST1">
		<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LIST1">					
		</TD><td></td>	
	</TR>
</table>

</TD><TD VALIGN=BOTTOM>

<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3>
	<TR>
		<TD>&nbsp;New Allocation : </TD><TD>
		<input type="text" name="txt_EXPALLOC1_N" class="textbox200" tabindex=-1 readonly style='color:red'>
		</TD><td></td>	
	</TR>
	<TR>
		<TD>&nbsp;Expenditure Left : </TD><TD>
		<input type="text" name="txt_EXPBALANCE1_N" class="textbox200" tabindex=-1 readonly style='color:red'>
		</TD><td></td>	
	</TR>
</TABLE>

</TD></TR></TABLE>
<BR>
<table><tr><td>

<table STYLE='BORDER:1PX SOLID BLACK;'  cellspacing=0 cellpadding=3>
<TR bgcolor=#133E71 style='color:white'> 
	<TD colspan=9><b>TRANSFER AMOUNT:</b></TD>
</TR>
<tr><td><input type=text name="txt_TRANSFERFIGURE" tabindex=4 value="" class="TEXTBOX200" onblur="UpdateFigures()"></td><td><IMG SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15 NAME="img_TRANSFERFIGURE"></TD></TR>
</TABLE>

</td><td width=330 nowrap>&nbsp;

</td></tr></table>
<BR>
<TABLE><TR><TD>

<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3>
	<TR bgcolor=#133E71 style='color:white'> 
		<TD colspan=9><b>VIREMENT TO:</b></TD>
	</TR>
	<TR>
		<TD>&nbsp;</td>	
	</TR>
	<TR>
		<TD>&nbsp;Head : </TD><TD>
							<select id="sel_HEAD2" name="sel_HEAD2" class="TEXTBOX200" onchange="Select_OnchangeHead(3)" tabindex=4>
                            <option value="">Please Select a Cost Centre...</option>
                          </select>
						  </TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_HEAD2"></td>	
	</TR>
	<TR>
		<TD>&nbsp;Expenditure : </TD><TD>                        
						<select id="sel_EXPENDITURE3" name="sel_EXPENDITURE3" class="TEXTBOX200" onchange="SetPurchaseLimits(3)" tabindex=4>
                          <option value="">Please select a Head...</option>
                        </select>
						</TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_EXPENDITURE3"></td>	
	</TR>
	<TR>
		<TD>&nbsp;Total Allocated : </TD><TD>
		<input type="text" name="txt_EXPALLOC3" class="textbox200" value="0.00" tabindex=-1 readonly>
		</TD><td></td>	
	</TR>
	<TR>
		<TD>&nbsp;Expenditure Left : </TD><TD>
		<input type="text" name="txt_EXPBALANCE3" class="textbox200" value="0.00" tabindex=-1 readonly>
		<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LEFT_LIST3">
		<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LIST3">					
		</TD><td></td>	
	</TR>
</table>

</TD><TD VALIGN=BOTTOM>

<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3>
	<TR>
		<TD>&nbsp;New Allocation : </TD><TD>
		<input type="text" name="txt_EXPALLOC3_N" class="textbox200" tabindex=-1 readonly style='color:red'>
		</TD><td></td>	
	</TR>
	<TR>
		<TD>&nbsp;Expenditure Left : </TD><TD>
		<input type="text" name="txt_EXPBALANCE3_N" class="textbox200" tabindex=-1 readonly style='color:red'>
		</TD><td></td>	
	</TR>
</TABLE>

</TD></TR>
<TR><TD colspan=2 align=right><input type=button name="SaveButton" class="RSLButton" onclick="SaveTransaction()" value=" Save "></TD></TR>
</FORM>
</TABLE>
	
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

