<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Call OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DIAGNOSTICSPASSWORD'"
Call OpenRs(rsPA, SQL)
if (NOT rsPA.EOF) then
	iPassword = rsPA("DEFAULTVALUE")
else
	iPassword = "PASSWORDFAILURE"
end if
Call CloseRs(rsPA)
Call CloseDB()

if (iPassword <> "PASSWORDFAILURE"  AND (Request("txt_PASS") = iPassword OR Session("SUPREMEBEINGACCESS") = "yes")) then
	Session("SUPREMEBEINGACCESS") = "yes"
	strUserInformation = Application("ActiveUserInformation")
	strUserList = Application("ActiveUserList")

	if (strUserList = "") then
		TableString = "<TR><TD colspan=8 align=center>No users currently logged on</TD></TR>"
	else
		strActiveUserList = Application("ActiveUserList")
		strActiveUserList = Left(strActiveUserList, Len(strActiveUserList) - 3)
	
		aActiveUsers = Split(strActiveUserList, "|||")
	
		strActiveUserInformation = Application("ActiveUserInformation")
		strActiveUserInformation = Left(strActiveUserInformation, Len(strActiveUserInformation) - 3)
	
		aActiveInformation = Split(strActiveUserInformation, "|||")
		
		TableString = ""
		Count = 0
		
		Call OpenDB()
		
		for i=0 to Ubound(aActiveUsers)
			AA = Split(aActiveUsers(i), ":")
			BB = Split(aActiveInformation(i), "::")
			
			if (NOT isNull(BB(1)) AND isNumeric(BB(1)) ) then
				ClientID = BB(1)
			else
				ClientID = -1
			end if 
			SQL = "SELECT O.NAME, E.FIRSTNAME, E.LASTNAME, T.TEAMNAME FROM E__EMPLOYEE E " &_ 
					"LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_ 
					"LEFT JOIN E_TEAM T ON JD.TEAM = T.TEAMID " &_ 
					"LEFT JOIN S_ORGANISATION O ON O.ORGID = E.ORGID WHERE E.EMPLOYEEID = " & ClientID
			Call OpenRs(rsInfo, SQL)
			if (NOT rsInfo.EOF) then
				Org = rsInfo("NAME")
				if (Not isNull(Org)) then
					Org = ", " & Org
				end if
				Firstname = rsInfo("FIRSTNAME")
				Lastname = rsInfo("LASTNAME")
				Team = rsInfo("TEAMNAME")
				Count = Count + 1
			else
				Org = ""
				Firstname = "UNKNOWN"
				Lastname = ""
				Team = "UNKNOWN"
			end if
			Call CloseRs(rsInfo)
			TableString = TableString & "<TR><TD>" & BB(1) & "</TD><TD>" & AA(0) & "</TD><TD>" & AA(1) & ":" & AA(2) & ":" & AA(3) & "</TD><TD>" & Firstname & " " & Lastname & Org & "</TD><TD>" & BB(2) & "</TD><TD>" & Team & "</TD></TR>" & chr(13)
		next
		
		Call CloseDB()
	end if
end if
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager --> Logged Users </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<table border="0" cellspacing="0" cellpadding="5">
<FORM NAME="RSLFORM" method="POST">
  <tr> 
	<td>&nbsp;&nbsp;&nbsp;<b>Security Password</b></td><td><input type=password value="********" name="txt_PASS" class="textbox200"></td>
	<td><input type=submit class="RSLButton" value=" SUBMIT + REFRESH "></td>
  </tr>
 </FORM>
</table>
<%
if (iPassword <> "PASSWORDFAILURE"  AND (Request("txt_PASS") = iPassword OR Session("SUPREMEBEINGACCESS") = "yes")) then
%>
<TABLE WIDTH=750 BORDER=1 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE'>
<TR CLASS='TABLE_HEAD' style='BACKGROUND-COLOR:#133e71;COLOR:WHITE;FONT-WEIGHT:BOLD'>
<TD>UserID</TD>
<TD>Session ID</TD>
<TD>Last Accessed</TD>
<TD>Name</TD>
<TD>IP</TD>
<TD>Team</TD>
</TR>
<%=TableString%>
<TR><TD colspan=6 align=center><b>Total Count: <%=Count%> Users</b></TD></TR>
</TABLE>
<%
else
	Response.Write "<div align=center style='border-top:1px gray dotted'><br><b>ACCESS IS DENIED</b></div>"
end if
%>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>