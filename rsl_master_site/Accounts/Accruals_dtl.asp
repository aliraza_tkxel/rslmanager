<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	
	
	

	
	
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc	   (5)
	
	
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
	ColData(0)  = "Date|sdate|80"
	SortASC(0) 	= "sdate ASC"
	SortDESC(0) = "sdate DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "N/L Account|d|250"
	SortASC(1) 	= "NL_ACCOUNT.[DESC] ASC"
	SortDESC(1) = "NL_ACCOUNT.[DESC] DESC"	
	TDSTUFF(1)  = " ""title='""&rsSet(""DETAILS"")& ""' "" "
	TDFunc(1) = ""		

	ColData(2)  = "Value|v|90"
	SortASC(2) 	= "NL_ACCRUALS.[VALUE] ASC"
	SortDESC(2) = "NL_ACCRUALS.[VALUE] DESC"	
	TDSTUFF(2)  = " "" ALIGN=RIGHT"" "
	TDFunc(2) = "FormatCurrency(|)"		

	ColData(3)  = "No: Months|NO_MONTHS|50"
	SortASC(3) 	= "NL_ACCRUALS.NO_MONTHS ASC"
	SortDESC(3) = "NL_ACCRUALS.NO_MONTHS DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""
	
	ColData(4)  = "Monthly Amount|MONTH_AMT|80"
	SortASC(4) 	= "NL_ACCRUALS.MONTH_AMT ASC"
	SortDESC(4) = " NL_ACCRUALS.MONTH_AMT DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""
	
	ColData(5)  = "|EMPTY|80"
	SortASC(5) 	= ""
	SortDESC(5) = ""
	TDSTUFF(5)  = " "" style='background-color:#FFFFFF;color:#000000;cursor:hand' onclick=""""PrePaymentAmend("" & rsSet(""ACCRUALID"") & "")"""">[<font color=blue>AMEND</font>]<b "" "
	
	
	
	PageName = "ccruals_dtl.asp"
	EmptyText = "No Relevant Accruals List found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""ACCRUALID"") &  "")"""" """ 

	
	Dim orderBy
	OrderByMatched = 0
	orderBy = DefaultOrderBy
	Call SetSort()



	
	SQLCODE=" SELECT  NL_ACCOUNT.AccountNumber + ' - '+ NL_ACCOUNT.[DESC]  as d, NL_ACCRUALS.[VALUE] as v, NL_ACCRUALS.NO_MONTHS, NL_ACCRUALS.MONTH_AMT, " &_
	" convert(varchar(12),NL_ACCRUALS.STARTDATE,103) as sdate, " &_
		" NL_ACCRUALS.ACCRUALID, '' AS EMPTY,NL_ACCRUALS.DETAILS " &_
		" FROM    NL_ACCRUALS INNER JOIN " &_
	" NL_ACCOUNT ON NL_ACCRUALS.NOMINALACC = NL_ACCOUNT.ACCOUNTID "  &_
	" and NL_ACCRUALS.RUNCOUNT<NL_ACCRUALS.no_months " &_
		" ORDER BY "& orderBy
	
	
	
	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	'OpenDB()
	
	'Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "NL_ACCOUNT", "ACCOUNTID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	'CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<!-- #BeginEditable "doctitle" -->
<TITLE>RSL Manager Finance --> Accruals List</TITLE>
<!-- #EndEditable -->
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array()

function PrePaymentAmend (ACCRUAL_ID){
	event.cancelBubble=true
	
	location.href = "/accounts/Accruals.asp?ID=" + ACCRUAL_ID
	}
function refreshList()
{
 	var Dispid=0
	var myCollection=document.getElementsByName("DisplayOption")
	if  (myCollection[0].checked ==true) Dispid=0;
	if  (myCollection[1].checked ==true) Dispid=1;
	
	
	location.href = "/accounts/Accruals_dtl.asp?dispID=" + Dispid
}
function SubmitPage(){
	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value  ;
	}

function load_me(txnid) {
	event.cancelBubble=true
	window.showModelessDialog("AccuralsPerMonth.asp?txnid=" + txnid  + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 550px; status: No; resizable: No;")
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<!--<TR>
		
      <TD><b> Accruals</b> 
        <input type="radio" value="0" name="DisplayOption" onClick="refreshList()" <%=OPtion1%> >
        &nbsp;&nbsp;&nbsp;&nbsp;<b>Year End Accruals</b> 
        <input type="radio" value="1" name="DisplayOption" onClick="refreshList()" <%=OPtion2%>></TD>
	</TR>
	<TR>-->
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="/Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>