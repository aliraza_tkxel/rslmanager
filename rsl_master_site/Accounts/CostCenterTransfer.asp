<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<%

dim CCTranId
CCTranId=request("CCTranId")
if CCTranId = "" then
	CCTranId=0
end if
dim DISTINCT_SQL
dim Xml 
dim dbrs1,strsql1
OpenDB()


Call GetCurrentYear()
FY = GetCurrent_YRange


DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
		"WHERE CC.COSTCENTREID NOT IN (11,13,22,23) " &_ 
		"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"





Count = 0
SQL = "SELECT CC.DESCRIPTION, CC.COSTCENTREID, ISNULL(COSTCENTREALLOCATION,0) - ISNULL(SUM(ALLOCATED),0) AS BUDGETLEFT FROM F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION FCA ON CC.COSTCENTREID=FCA.COSTCENTREID " &_
		"LEFT JOIN ( " &_
		"SELECT SUM(FHA.HEADALLOCATION) AS ALLOCATED, FH.COSTCENTREID FROM F_HEAD FH INNER JOIN F_HEAD_ALLOCATION FHA ON FH.HEADID=FHA.HEADID  WHERE FHA.ACTIVE = 1 GROUP BY FH.COSTCENTREID   " &_
		"UNION ALL " &_
		"SELECT SUM(AMOUNT), FRMCOSETCENTRE FROM F_BUDGETRTRANSFER BT WHERE APPROVED = 0 GROUP BY FRMCOSETCENTRE " &_
		") HD ON HD.COSTCENTREID = CC.COSTCENTREID " &_		
		"WHERE CC.COSTCENTREID <> 11 AND (FCA.ACTIVE = 1) " &_
		"AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND " &_
		"GROUP BY CC.DESCRIPTION, CC.COSTCENTREID, COSTCENTREALLOCATION ORDER BY CC.DESCRIPTION"
CCArray = ""
CCValue = ""

PHY_SQL="F_FISCALYEARS where year(getdate())-1<=year(YStart) "

Call BuildSelect(lstPhyYear, "sel_PHYYEAR", PHY_SQL, "YRange, convert(varchar(12),YStart,103) + ' - ' + Convert(varchar(12),YEnd,103) as PhyYear", "PhyYear", "Please Select...", NULL, NULL, "textbox200","datasrc=""#cct"" datafld=""phyYear"" onchange=""SetCostCenter()"" ")
'Call BuildSelect(lstCostCentresfrom, "sel_COSTCENTREFROM", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200","datasrc=""#cct"" datafld=""fromcc"" onchange=""SetRemaining()"" ")
'Call BuildSelect(lstCostCentresto, "sel_COSTCENTRETO", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200","datasrc=""#cct"" datafld=""tocc""")

Call OpenRs(rsCC, SQL)
while NOT rsCC.EOF
	if (Count = 0) then
		CCArray = rsCC("COSTCENTREID")
		CCValue = FormatNumber(rsCC("BUDGETLEFT"),2,-1,0,0)
		Count = 1
	else
		CCArray = CCArray & "," & rsCC("COSTCENTREID")
		CCValue = CCValue & "," & FormatNumber(rsCC("BUDGETLEFT"),2,-1,0,0)
	end if
	rsCC.moveNext
wend
Call CloseRs(rsCC)
if (CCArray = "") then
	CCArray = "0"
	CCValue = "0"
end if
CloseDB()


Xml="<xml id=""cct"">" & _
  "<records>" & _
    "<phyYear></phyYear>" &_
 	"<BDDate>" & FormatDateTime(Date,2)& "</BDDate>" & _
 	"<fromcc></fromcc>" & _
 	"<amount></amount>" & _
 	"<tocc></tocc>" & _
 	"<notes></notes>" & _
	"<cctranid>0</cctranid>" & _
 	"</records></xml>" 




 

set dbrs1=server.createobject("ADODB.Recordset")
if CCTranId > 0 then
	strsql1="select phyYear,FrmCosetCentre, Amount, ToCostCentre, Note, BDDate from F_BUDGETRTRANSFER where CCTranId=" & CCTranId
	openDB()
	call openrs(dbrs1,strsql1)
	Xml="<xml id=""cct"">" & _
		"<records>" & _
			"<phyYear>"& dbrs1("phyYear")& "</phyYear>" &_
			"<BDDate>"& dbrs1("BDDate") &"</BDDate>" & _
			"<fromcc>"& dbrs1("FrmCosetCentre") &"</fromcc>" & _
 			"<amount>"& dbrs1("Amount") &"</amount>" & _
 			"<tocc>"& dbrs1("ToCostCentre") &"</tocc>" & _
 			"<notes>"& dbrs1("Note") &"</notes>" & _
			"<cctranid>" & CCTranId & "</cctranid>" & _
 			"</records></xml>" 

	closedb()
	response.write Xml
END IF
IF CCTranId=0 THEN
	response.write Xml
end if


%>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>

<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<script LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></script>
<script LANGUAGE="JavaScript">
var CCArray = new Array(<%=CCArray%>)
var CCValue = new Array(<%=CCValue%>)

var FormFields = new Array()
function SetCostCenter()
{
	var PhyYear=-1
	if (document.getElementById("sel_PHYYEAR").value!= "")
	{
		PhyYear=document.getElementById("sel_PHYYEAR").value
	
	
		url="/Includes/xmlServer/xmlsrv.asp?id="+ PhyYear + "&task=BUDGETCC";
		
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		
		
		var nodeMap
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		xmlobj.async = false;
		var t=xmlobj.selectSingleNode("//rs:data")
		var f=t.childNodes;
				
		document.getElementById("sel_COSTCENTREFROM").options.length=0;
		document.getElementById("sel_COSTCENTRETO").options.length=0;
		document.getElementById("sel_COSTCENTREFROM").options[document.getElementById("sel_COSTCENTREFROM").options.length] = new Option("Please Select", "-1")
		document.getElementById("sel_COSTCENTRETO").options[document.getElementById("sel_COSTCENTRETO").options.length] = new Option("Please Select", "-1")
		for (var i=0; i<f.length; i++) { 
			var oOption = document.createElement("OPTION");
			document.getElementById("sel_COSTCENTREFROM").options[document.getElementById("sel_COSTCENTREFROM").options.length] = new Option(f[i].getAttribute("DESCRIPTION"), f[i].getAttribute("COSTCENTREID"))
			document.getElementById("sel_COSTCENTRETO").options[document.getElementById("sel_COSTCENTRETO").options.length] = new Option(f[i].getAttribute("DESCRIPTION"), f[i].getAttribute("COSTCENTREID"))
			} 
	}
	if (document.getElementById("sel_PHYYEAR").value== "")
	{
		document.getElementById("sel_COSTCENTREFROM").options.length=0;
		document.getElementById("sel_COSTCENTRETO").options.length=0;
		document.getElementById("sel_COSTCENTREFROM").options[document.getElementById("sel_COSTCENTREFROM").options.length] = new Option("Please Select", "-1")
		document.getElementById("sel_COSTCENTRETO").options[document.getElementById("sel_COSTCENTRETO").options.length] = new Option("Please Select", "-1")

	}
	
}
function SetRemaining()
	{
		if (document.getElementById("sel_COSTCENTREFROM").value!= -1)
		{
			url="/Includes/xmlServer/xmlsrv.asp?id="+ document.getElementById("sel_COSTCENTREFROM").value + "&task=BUDGETTransferrable";
			var xh=new ActiveXObject("Microsoft.XMLHTTP");
			var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
			
			
			var nodeMap
			xh.open("POST",url,false);
			xh.send();
			xmlobj=xh.responseXML;
			xmlobj.async = false;
			var t=xmlobj.selectSingleNode("//rs:data")
			var f=t.childNodes;
			if (t.childNodes.length>0)
				document.getElementById("txt_REMAINING").value=xmlobj.selectSingleNode("//rs:data/z:row").getAttribute("BUDGETLEFT")
		}
		if (document.getElementById("sel_COSTCENTREFROM").value!= -1)
		{
			document.getElementById("txt_REMAINING").value="00.00"
		}
	}
	
function SaveData()
{
	FormFields[0] = "txt_date|Date|DATE|Y"
	FormFields[1] = "sel_COSTCENTREFROM|From CostCentre|SELECT|Y"
	FormFields[2] = "txt_amt|Amount|CURRENCY|Y"
	FormFields[3] = "sel_COSTCENTRETO|To CostCentre|SELECT|Y"
	FormFields[4] = "sel_PHYYEAR|Physical Year|SELECT|Y"
	if (!checkForm()) return false
	if (document.getElementById("sel_COSTCENTREFROM").value== document.getElementById("sel_COSTCENTRETO").value)
	{
		alert("You cannot transfer to the same Cost Centre.\nPlease change the To Cost Centre and try again.")
		return false;
	}

	if (parseFloat(document.getElementById("txt_amt").value) <= 0)
	{
		alert("You must enter a positive amount to transfer.\nPlease change the figure and try again.")
		return false;
	}

	if (parseFloat(document.getElementById("txt_amt").value) > parseFloat(document.getElementById("txt_REMAINING").value))
	{
		alert("You cannot transfer more than the 'Transferrable' value for the selected Cost Centre.")
		return false;
	}
	
	var xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
	var url
	if (document.getElementById("sButton").value=="Save")
		url="/Includes/xmlServer/xmlsrv.asp?id=0" + "&task=BUDGETTRANSINSERT";
	else
		url="/Includes/xmlServer/xmlsrv.asp?id=0" + "&task=BUDGETUPDATE";
		
	document.getElementById("sButton").disabled = true
	xmlHttp.open("POST", url, false);
	xmlHttp.setRequestHeader( "Content-Type","text/xml")
	xmlHttp.send(cct.XMLDocument);
	xmlobj=xmlHttp.responseXML
	
	alert("Cost Centre Transfer completed successfully.")
	location.href="CostCenterTransfer_DTL.asp"
	

}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" --> 
<TABLE WIDTH=592 BORDER=0 CELLPADDING=0 CELLSPACING=0 height=26>
  <TR>
			
		  
    <TD ROWSPAN=2 width="133"><img src="Images/tab_budgetvirement.GIF" width="133" height="23" border=0></TD>
			
    <TD width="461"><IMG SRC="images/spacer.gif" WIDTH=200 HEIGHT=19></TD>
		</TR>
		<TR>
			
    <TD BGCOLOR=#133E71 width="400"><IMG SRC="images/spacer.gif" WIDTH=200 HEIGHT=1></TD>
		</TR>
		<tr> 
			<td colspan=2 style="border-left:1px solid #133e71; border-right:1px solid #133e71"> <img src="images/spacer.gif" width=53 height=6></td>
		</tr>	
	</TABLE>
	 	
	
	  <table cellspacing=0 WIDTH=400 height=220PX cellpadding=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71'>
	 
        <form name="frm" method="post">
		<tr> 
            
      		<td nowrap width="117" ><b>&nbsp;&nbsp;&nbsp;Fiscal Year: </b> </td>
     		<td >
		        <%=lstPhyYear%>
      		</td>
	        <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_PHYYEAR"></td>
        </tr>
       	<tr> 
            
      		<td nowrap width="117" ><b>&nbsp;&nbsp;&nbsp;Date: </b> </td>
     		<td >
		        <input type=text name="txt_date"  maxlength=20 size=15 class="textbox200" style='width:100' datasrc="#cct" datafld="BDDate">
      		</td>
	        <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_date"></td>
        </tr>
          <tr> 
            
      <td ><b> &nbsp;&nbsp;&nbsp;From CostCentre</b></td>
            
      <td width="449"> 
	  	<select name="sel_COSTCENTREFROM" class='textbox200' datasrc="#cct" datafld="fromcc" onchange="SetRemaining()">
			
		</select>
		 &nbsp;&nbsp;Transferrable &nbsp; <input style='text-align:right' type=text class="textbox" readonly name="txt_REMAINING"></td>
            
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTREFROM"></td>
          </tr>
		  <tr>
		  	<td colspan="2">
			
			</td>
		  </tr>
          <tr> 
            
      <td nowrap  ><b>&nbsp;&nbsp;&nbsp;Amount</b></td>
            
      <td> 
        <input type=text name="txt_amt" maxlength=20 size=15 class="textbox200" style='width:100' datasrc="#cct" datafld="amount">
			</td>
			
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_amt"></td>
          </tr>
          
          <tr> 
            
      <td title="Date"  ><b>&nbsp;&nbsp;&nbsp;To CostCentre</b></td>
            
      <td> 
	  	<select name="sel_COSTCENTRETO" class='textbox200' datasrc="#cct" datafld="tocc">
			
		</select>
		</td>
			
      <td width="15"><img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTRETO"></td>
          </tr>
          
          <tr>
			
      <td > <b>&nbsp;&nbsp;&nbsp;Note: </b> </td>
			
      <td  colspan=2> 
        
        <textarea name="txt_details" maxlength=200 size=35 class="textbox200" Style="width:450; height=50" datasrc="#cct" datafld="notes">
				</textarea>
			</td>
			
      <td ><img src="/js/FVS.gif" width="15" height="15" name="img_details"></td>
		 </tr>
		<tr>
			<tr>
			
      <td > </td>
			
      <td > 
        <input type=reset name=reset class="RSLButton" value=Reset>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<% if CCTranId > 0 then 
					response.write "<input type=button name=sButton class=""RSLButton"" value= ""Update"" onClick=""return SaveData()"">"
				   else
				   	response.write "<input type=button name=sButton class=""RSLButton"" value= ""Save"" onClick=""return SaveData()"">"
				   end if 
				%>
			</td>
		</tr>
        </form>
      </table>

<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<%

%>
</BODY>
</HTML>
