<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, optionvalues2, optiontext2
	Dim intReturn, theamounts, ExpenditureLeft, EXPID, HeadID, CCID
	
	strWhat = Request.form("IACTION")

	If strWhat = "change" Then 
		rePop() 
	End If

	Function rePop()
		HeadID = -1
		if (Request("WHICHBOX") = 1) then
			if(Request.Form("sel_HEAD") <> "") then HeadID = Request.Form("sel_HEAD")		
		else
			if(Request.Form("sel_HEAD2") <> "") then HeadID = Request.Form("sel_HEAD2")		
		end if		

		if (HeadID = -1) then
			optionvalues = ""
			theamounts = "0"
			ExpenditureLeft = "0"			
			optiontext = "Please Select a Head..."			
			Exit Function
		end if
		
		OpenDB()

		Call GetCurrentYear()
		FY = GetCurrent_YRange
				
		SQL = "SELECT DESCRIPTION, ISNULL(EXA.EXPENDITUREALLOCATION,0) AS EXPALL, EX.EXPENDITUREID, (isNull(EXPENDITUREALLOCATION,0) - isNull(GROSSCOST,0)) as REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.ACTIVE = 1 AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) " &_				
				"left JOIN ( "&_
					"SELECT SUM(GROSSCOST) as GROSSCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEITEM where F_PURCHASEITEM.ACTIVE = 1 "&_
					"AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_					
					"GROUP BY EXPENDITUREID) PI on PI.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (HEADID = " & HeadID & ") "&_
				"ORDER BY DESCRIPTION"
				
		Call OpenRs(rsExpenditure, SQL)		
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		ExpenditureLeft = "0"		
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			theamounts = theamounts & ";;" & rsExpenditure.Fields.Item("REMAINING").Value
			ExpenditureLeft = ExpenditureLeft & ";;" & rsExpenditure.Fields.Item("EXPALL").Value			
			
			  count = count + 1
			  rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			ExpenditureLeft = "0"
		End If

		CloseRs(rsExpenditure)
		CloseDB()		
	End Function
	
%>

<html>
<head>
<title>Expenditure Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload=loaded()>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/ImageFormValidation.js"></SCRIPT>
<script language=javascript>

	function loaded(){
		if ('<%=strWhat%>' == 'change'){
			parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", <%=Request("WHICHBOX")%>);
			parent.THISFORM.EXPENDITURE_LEFT_LIST<%=Request("WHICHBOX")%>.value = "<%=theamounts%>";
			parent.THISFORM.EXPENDITURE_LIST<%=Request("WHICHBOX")%>.value = "<%=ExpenditureLeft%>";			
			}
		}
		
</script>

</body>
</html>

