<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
    Dim callbackFunc
    callbackFunc = Request.form("IACTION")

TableString = ""

SQL = "SELECT TOP(50)	TemplateId " &_
		",ISNULL(TemplateName,'-') AS TemplateName " &_
		",ISNULL(CONVERT(NVARCHAR(10),CREATEDDATE, 103),'-') AS Saved " &_
		",ISNULL(SUBSTRING(E.FIRSTNAME, 1, 1) + ' ' + E.LASTNAME,'-') AS [By] " &_
        "FROM	NL_GeneralJournalTemplate NGT " &_
		"        LEFT JOIN E__EMPLOYEE E ON NGT.CreatedBy = E.EMPLOYEEID " &_
        "WHERE	NGT.ISACTIVE = 1 " &_
        "ORDER BY CREATEDDATE DESC "

Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	While (NOT rsLoader.EOF)
		TableString = TableString & "<tr><td>" & rsLoader("TemplateName") & "</td><td>" & rsLoader("Saved") & "</td><td>" & rsLoader("By") & "</td><td><button onclick='GetTemplateDetail(" & rsLoader("TemplateId") & ")'>Select</button></td></tr>"
		rsLoader.moveNext
	wend
Else
	TableString = "<tr><td colspan='4' align='center'>No template found</td></tr>"
End If
Call CloseRs(rsLoader)
Call CloseDB()
%>

<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>General Journal Template</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function GetGeneralJournalTemplateList()
        {
            var callback = "<%=callbackFunc%>";
            if (callback == "get_template_list")
            {
                var templateListRows = "<%=TableString%>";
                parent.DisplayTemplateListPopup(templateListRows);
            }
        }

    </script>
</head>
<body onload="GetGeneralJournalTemplateList();">
</body>
</html>