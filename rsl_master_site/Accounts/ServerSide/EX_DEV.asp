<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText, ExpenditurePurchases
	Dim ProjectUsers, TimeCost, TimeMatched, fundid, HDID,  expname, startdate
	Dim expallocation, active, datecreated, EXID, totalallocated, totalfund, totalremaining, allocated
	Dim nominalcode, controlcode ,OpenBalance1
	
Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT DESCRIPTION, isnull(EXA.EXPENDITUREALLOCATION,0) as EXPENDITUREALLOCATION, " &_
					"QBDEBITCODE, QBCONTROLCODE, isnull(OPENBALANCE,0) as OPENBALANCE, EX.headid, EXA.active " &_
					"FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR IS NULL WHERE EX.EXPENDITUREID = " & theID & ""
	Set Rs = Conn.Execute(strEventQuery)

	expname 		= Rs("DESCRIPTION")
	expallocation 	= Rs("EXPENDITUREALLOCATION")
	nominalcode		= Rs("QBDEBITCODE")
	controlcode		= Rs("QBCONTROLCODE")
	OpenBalance1	= FormatNumber(Rs("OPENBALANCE"),2,-1,0,0)
	HDID = Rs("HEADID")
	active = Rs("ACTIVE")
	Rs.Close
	Set Rs = Nothing
	
	Set Rs5 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALPURCHASES FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND EXPENDITUREID = " & theID & " "
	Set Rs5 = Conn.Execute(strEventQuery)
	ExpenditurePurchases = Rs5("TOTALPURCHASES")
	Rs5.Close
	Set Rs5 = Nothing
		
End Function	

Function DisplayMiniTree(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT EX.DESCRIPTION AS EXPNAME, CC.DESCRIPTION AS COSTCENTRENAME, HD.DESCRIPTION AS HEADNAME FROM F_EXPENDITURE EX left join F_HEAD HD on HD.headid = EX.headid left join F_COSTCENTRE CC on CC.COSTCENTREID = HD.COSTCENTREID WHERE (EXPENDITUREID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='RSLBlack' width=370px><tr><td><b><u>Update Expenditure</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("HEADNAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("EXPNAME") & "</td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

ACTION_TO_TAKE = Request("EX_A")
EXID = Request("EXID")

OpenDB()

If (ACTION_TO_TAKE = "L") Then
	GetData(EXID)
	DisplayMiniTree(EXID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
	if ("<%=ACTION_TO_TAKE%>" == "L"){
		parent.setText("<%=treeText%>");
		parent.thisForm.txt_expenditureallocation.value = "<%=FormatCurrency(expallocation)%>";
		parent.thisForm.txt_expTotalSpent.value = "<%=FormatCurrency(ExpenditurePurchases)%>";		
		parent.thisForm.txt_expenditurename.value = "<%=expname%>";
		parent.thisForm.EXID.value = "<%=EXID%>";						
		parent.thisForm.budgetcreated.value = "<%=startdate%>";		
		parent.thisForm.HDID.value = "<%=HDID%>";
		parent.thisForm.txt_nominalcode.value = "<%=nominalcode%>";
		parent.thisForm.txt_controlcode.value = "<%=controlcode%>";
		parent.thisForm.hid_nominalcode.value = "<%=nominalcode%>";
		parent.thisForm.hid_controlcode.value = "<%=controlcode%>";
		parent.thisForm.txt_OPBalance.value ="<%=OpenBalance1%>"
		parent.thisForm.hdn_OPBalance.value ="<%=OpenBalance1%>"
		if ("<%=active%>" == "True")
			parent.thisForm.budgetactive.value = "YES";
		else
			parent.thisForm.budgetactive.value = "NO";
		parent.thisForm.EX_A.value = "UPDATE";
		parent.Head.style.display = "none";
		parent.CostCentre.style.display = "none";
		parent.Budget.style.display = "block";
		}
	}
</script>
</body>
</html>
