<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
'''''''''
'' Page Note: This is a copy of EmployeeLimit_srv.asp, changed to meet rash new functionality
'' 8/june/2004 - No longer returns the Employees, but now returns the expenditures
''''''''''

Dim ID
Dim DataFields   (2)
Dim DataTypes    (2)
Dim ElementTypes (2)
Dim FormValues   (2)
Dim FormFields   (2)
UpdateID	  = "hid_EMPLOYEELIMITID"
ExpenditureDesc = Request.Form("sel_Expenditure")
ExpenditureID = Request.Form("hid_EXPENDITUREID")
ExpenditureName = Request.Form("hid_ExpenditureName")
EmpLimit = Request.Form("hid_LIMIT")					'This is for delete only purpose
ExpenditureLimit = Request.Form("txt_LIMIT")
AssignTo = Request("sel_ASSIGNTO")
if (AssignTo = "") then AssignTo = -1
FormFields(0) = "cde_EMPLOYEEID|" & Request("sel_ASSIGNTO")
FormFields(1) = "txt_LIMIT|TEXT"
FormFields(2) = "hid_EXPENDITUREID|TEXT"


Function NewRecord ()
		
		If ExpenditureDesc <> "" And AssignTo <> "" Then
			
			'Multiple Deletion ofexpenditures
			SQL = "DELETE F_EMPLOYEELIMITS " &_
				"FROM F_EMPLOYEELIMITS EL " &_
				"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = EL.EXPENDITUREID " &_
				"INNER JOIN F_HEAD HE ON HE.HEADID = EX.HEADID " &_
				"WHERE (HE.COSTCENTREID = 9) AND (EX.DESCRIPTION = '" & ExpenditureDesc & "') " &_
				"AND EL.EMPLOYEEID = " & AssignTo & " "
			rESPONSE.wRITE sql & "<br><br>"
			Call OpenRs(rsExpendList, SQL)
			
		SQL1 = "DELETE from F_DEVLIMITS where EMPLOYEEID = " & Request("sel_ASSIGNTO") & " AND DESCRIPTION = '" & ExpenditureName &  "'"
		Conn.Execute SQL1, recaffected
		
		SQL2 = "INSERT INTO  F_DEVLIMITS (EMPLOYEEID,DESCRIPTION,LIMIT) VALUES (" & Request("sel_ASSIGNTO") & ",'" & ExpenditureName & "'," & ExpenditureLimit & ")"
		Conn.Execute SQL2, recaffected
		
			'Multiple Inserts ofexpenditures		
			SQL = "INSERT INTO F_EMPLOYEELIMITS " &_
				"SELECT " & AssignTo & " AS THEUSER, EX.EXPENDITUREID, " & Request.Form("txt_LIMIT") & " AS THELIMIT " &_
				"FROM F_EXPENDITURE EX " &_
				"INNER JOIN F_HEAD HE ON HE.HEADID = EX.HEADID " &_
				"WHERE (HE.COSTCENTREID = 9) AND (EX.DESCRIPTION = '" & ExpenditureDesc & "') "
			rw sql & "<br><br>"

			Call OpenRs(rsExpendList, SQL)
							
		End If
		
		LoadRecord()
		
	End Function
	
	Function AmendRecord()
	
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		SQL = "UPDATE F_EMPLOYEELIMITS " & strSQL & " WHERE EMPLOYEELIMITID = " & ID
		Conn.Execute SQL, recaffected
		
		SQL = "UPDATE F_DEVLIMITS SET DESCRIPTION = '" & ExpenditureName & "', LIMIT = " & ExpenditureLimit & " WHERE EMPLOYEEID = " & Request("sel_ASSIGNTO")
		Conn.Execute SQL, recaffected
	
	End Function
	
	Function DeleteRecord(ActionType)
		ID = Request.Form(UpdateID)
		rw "-----" & ActionType & "<br><br>"
		SQL = "delete f_employeelimits " &_
				"from f_employeelimits el  " &_
				"	inner join f_expenditure ex on ex.expenditureid = el.expenditureid " &_
				"where employeeid = " & AssignTo & " and ex.description = '" &  ExpenditureName & "' and el.limit = " & EmpLimit
		rw SQL & "<br><br>"
		Conn.Execute SQL, recaffected
		SQL1 = "DELETE from F_DEVLIMITS where EMPLOYEEID = " & Request("sel_ASSIGNTO") & " AND DESCRIPTION = '" & ExpenditureName &  "'"
		rw SQL1 & "<br><br>"
		Conn.Execute SQL1, recaffected

		LoadRecord()	
	End Function


	' 8th June 2004 - Returns all the expenditures records that employees have
	' limits on, to the server frame in SetEmployeeLimts.asp
	Function LoadRecord()
		
		'select the records that relate to the employee
		SQL = "SELECT DISTINCT F.DESCRIPTION AS descrip , isnull(LIMIT,0) as limit, EMPLOYEELIMITID, F.EXPENDITUREID FROM F_EMPLOYEELIMITS EL INNER JOIN F_EXPENDITURE F ON F.EXPENDITUREID = EL.EXPENDITUREID INNER JOIN F_HEAD HE ON HE.HEADID = F.HEADID AND HE.COSTCENTREID = 9 WHERE EL.EMPLOYEEID = " & AssignTo & "ORDER BY F.DESCRIPTION"
		Call OpenRs(rsEm, SQL)
		response.write  SQL
		'generate the html
		if (NOT rsEm.EOF) then
			
			TableString = ""
			tempDestinct = ""
			while NOT rsEM.EOF 
				if NOT tempDestinct = rsEm("descrip") then
				TableString = TableString & "<TR><TD>" & rsEm("descrip") & "</TD><TD width=110px align=right>" & FormatCurrency(rsEm("LIMIT")) & "</td><TD align=center BGCOLOR=#ffffff width=30px nowrap style='cursor:hand' onclick=""DeleteMe(" & rsEm("EMPLOYEELIMITID")& "," & rsEm("EMPLOYEELIMITID")& ",'" & rsEm("descrip")&  "'," & rsEm("limit")& ")""><font color=red>DEL</font></TD></TR>"
				End If
				tempDestinct = rsEm("descrip")
				rsEm.movenext
			wend
			TableString = TableString & "<TFOOT><TR><TD colspan=3 align=center height='100%'></TD></TR></TFOOT>"
		else
		TableString = "<TR><TD colspan=3 align=center>No Development Expenditures Assigned</TD></TR>"
			TableString = TableString & "<TFOOT><TR><TD colspan=3 align=center height='100%'></TD></TR></TFOOT>"		
		end if
		
	End Function


//Function GO()
	//CloseDB()
	'Response.Redirect "../CRM.asp?CustomerID=" & ID
//End Function

TableString = ""

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "GenExp"   LoadRecord()	
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord(1)
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head>
</head>
<script language="javascript">
function ReturnData(){
	parent.MainDiv.innerHTML = MainDiv.innerHTML
	
	}
</script>
<body onload="ReturnData()">
<div id="MainDiv">
<table cellspacing=0 cellpadding=3 style='border-collapse:collapse;behavior:url(../../Includes/Tables/tablehl.htc);' SLCOLOR='' HLCOLOR='STEELBLUE' border=1 width=100% height=100%>
<%=TableString%>
</table>
</div>
</body>
</html>