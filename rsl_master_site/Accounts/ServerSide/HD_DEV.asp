<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function
	
Dim treeText
Dim CCID, headname, headcode, headallocation, active, datecreated, HDID
Dim TotalSpent, OpeningBalance, Grants, StartDate, ActualDate, TargetDate, ActiveString
	
Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT HEA.ACTIVE, HE.COSTCENTREID, ISNULL(HEA.HeadAllocation,0) AS HeadAllocation, DESCRIPTION FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.FISCALYEAR IS NULL WHERE HE.HEADID = " & theID & ""
	Set Rs = Conn.Execute(strEventQuery)

	'THIS PART FIND THE HEAD/DEVELOPMENT DETAILS AND ALSO THE TOTAL DEVELOPMENT COST
	headname = Rs("DESCRIPTION")
	headallocation = Rs("HeadAllocation")
	CCID = Rs("COSTCENTREID")
	active = Rs("active")
	if (active = true) then
		ActiveString = "The development is <font color=blue>ACTIVE</font> : <a href='Serverside/HD_DEV_ACTIVE.asp?HDID=" & theID & "&ACTION=INACTIVE' target='FB_DEV_4466'><font color=red>Click to make In-Active</font></A>"
	else
		ActiveString = "The development is <font color=red>IN-ACTIVE</font>: <a href='Serverside/HD_DEV_ACTIVE.asp?HDID=" & theID & "&ACTION=ACTIVE' target='FB_DEV_4466'><font color=blue>Click to make Active</font></A>"
	end if

	Rs.Close
	Set Rs = Nothing
	
	'THIS PART FINDS THE TOTAL SPENT AGAINST THE DEVELOPMENT
	strEventQuery = "SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALSPENT FROM F_PURCHASEITEM PI " &_
					"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
					"INNER JOIN F_HEAD HE ON HE.HEADID = EX.HEADID AND HE.HEADID = " & theID & " " &_
					"WHERE PI.ACTIVE = 1"
	Call OpenRs(Rs4, strEventQuery)
	Set Rs4 = Conn.Execute(strEventQuery)
	TotalSpent = Rs4("TOTALSPENT")
	Call CloseRs(Rs4)
	
	'THIS GETS A SUM OF ALL THE OPENING BALANACES FOR THE CHILD ACCOUNTS
	SQL = "SELECT ISNULL(SUM(OPENBALANCE),0) AS OB FROM F_EXPENDITURE WHERE HEADID = " & theID
	Call OpenRs(rsOB, SQL)	
	OpeningBalance = rsOB("OB")
	Call CloseRs(rsOB)
	
	'THIS GETS A SUM OF ALL GRANTS ASSOCIATED WITH THE ACCOUNT
	SQL = "SELECT ISNULL(SUM(AMOUNT),0) AS GRANTS FROM P_DEVLOPMENTGRANT DG " &_
			"INNER JOIN F_DEVELOPMENT_HEAD_LINK DHL ON DHL.DEVELOPMENTID = DG.DEVCENTREID " &_
			"INNER JOIN F_HEAD HE ON HE.HEADID = DHL.HEADID " &_
			"WHERE HE.HEADID = " & theID
	Call OpenRs(rsGR, SQL)
	Grants = rsGR("GRANTS")
	Call CloseRs(rsGR)
	
	'THIS GETS THE DEVELOPMENT DATES
	SQL = "SELECT D.STARTDATE, D.TARGETDATE, D.ACTUALDATE FROM PDR_DEVELOPMENT D " &_
			"INNER JOIN F_DEVELOPMENT_HEAD_LINK DHL ON DHL.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"INNER JOIN F_HEAD HE ON HE.HEADID = DHL.HEADID AND HE.HEADID = " & theID
	Call OpenRs(rsDT, SQL)
	if (NOT rsDT.EOF) then
		StartDate = rsDT("STARTDATE")
		TargetDate = rsDT("TARGETDATE")
		ActualDate = rsDT("ActualDate")
	end if
	Call CloseRs(rsDT)
End Function	

Function DisplayMiniTree(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT HD.DESCRIPTION AS HEADNAME, CC.DESCRIPTION AS COSTCENTRENAME FROM F_HEAD HD Left join F_COSTCENTRE CC on HD.COSTCENTREID = CC.COSTCENTREID WHERE (HD.HEADID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Update Development</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

ACTION_TO_TAKE = Request("HD_A")
HDID = Request("HDID")
NODE = Request("node")

OpenDB()

If (ACTION_TO_TAKE = "L") Then
	GetData(HDID)
	DisplayMiniTree(HDID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
<% if ACTION_TO_TAKE = "L" Then %>
		parent.setText("<%=treeText%>");
		parent.thisForm.txt_headallocation.value = "<%=FormatCurrency(headallocation)%>";
		parent.thisForm.txt_HeadDescription.value = "<%=headname%>";
		parent.thisForm.txt_ContractStart.value = "<%=StartDate%>";
		parent.thisForm.txt_AnticipatedCompletion.value = "<%=TargetDate%>";
		parent.thisForm.txt_ActualCompletion.value = "<%=ActualDate%>";						
		parent.thisForm.TotalSpent.value = "<%=FormatCurrency(TotalSpent)%>";
		parent.thisForm.HeadOpeningBalance.value = "<%=FormatCurrency(OpeningBalance)%>";		
		parent.thisForm.GrantsReceived.value = "<%=FormatCurrency(Grants)%>";
		parent.thisForm.HeadBalance.value = "<%=FormatCurrency(OpeningBalance+TotalSpent-Grants)%>";		
		parent.thisForm.MoneyLeft.value = "<%=FormatCurrency(headallocation-(OpeningBalance+TotalSpent-Grants))%>";				
		parent.ActiveDiv.innerHTML = "<%=ActiveString%>";
		parent.thisForm.CCID.value = "<%=CCID%>";
		parent.thisForm.HDID.value = "<%=HDID%>";
		parent.thisForm.NODE.value = "<%=NODE%>"
		parent.CostCentre.style.display = "none";
		parent.Budget.style.display = "none";
		parent.Head.style.display = "block";
		<% if (Request("WITHRELOAD") <> "") then %>
		parent.theSideBar.ReloadMe()
		<% end if %>
<% End if %>
	}
</script>
	

</body>
</html>
