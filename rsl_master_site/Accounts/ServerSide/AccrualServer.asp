<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	dim dbcmd
	dim Reff,details,txtdate
	details= Replace(Request.Form("txt_details"), "'", "''") 
	
	if Request.Form("hdn_ID")=0 then
		
		set dbcmd= server.createobject("ADODB.Command")
		dbcmd.ActiveConnection=RSL_CONNECTION_STRING
		dbcmd.CommandType = 4
		dbcmd.CommandText = "NL_INSERT_ACCRUALS"
				
		dbcmd.Parameters.Refresh
		dbcmd.Parameters(1) = Request("sel_ACCOUNT")
		dbcmd.Parameters(2) = Request.Form("txt_value")
		dbcmd.Parameters(3) = Request.Form("sel_slMonts")
		dbcmd.Parameters(4) = Request.Form("txt_MNTHAMT")
		dbcmd.Parameters(5) = details
		dbcmd.Parameters(6) = Request.Form("txt_StartDATE")
		dbcmd.Parameters(7) = 0
		dbcmd.execute
		set dbcmd=nothing
	else
		set dbcmd= server.createobject("ADODB.Command")
		dbcmd.ActiveConnection=RSL_CONNECTION_STRING
		dbcmd.CommandType = 4
		dbcmd.CommandText = "NL_UPDATE_ACCRUALS"
		dbcmd.Parameters.Refresh
		dbcmd.Parameters(1) = Request.Form("hdn_ID")
		dbcmd.Parameters(2) = Request.Form("sel_ACCOUNT")
		dbcmd.Parameters(3) = Request.Form("txt_value")
		dbcmd.Parameters(4) = Request.Form("sel_slMonts")
		dbcmd.Parameters(5) = Request.Form("txt_MNTHAMT")
		dbcmd.Parameters(6) = details
		dbcmd.Parameters(7) = Request.Form("txt_StartDATE")
		
		dbcmd.execute
		set dbcmd=nothing
	end if
	
	Response.Redirect "../Accruals_dtl.asp"
'
%>
