<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%


Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText	
	Dim CostCentreID, CostCentre, accountablebody, approvedby, department, datestart, dateend, amount, active, minimumTotal

'	On Error Resume Next

Function GetFormFields ()
	accountablebody = Request.Form("txt_AccountableBody")
	if accountablebody = "" then accountablebody = null end if
	department = Request.Form("txt_Department")
	if department = "" then department = null end if	
	CostCentre = Request.Form("txt_CostCentreDescription")
	if CostCentre = "" then CostCentre = null end if	
	datestart = Request.Form("txt_DateStart")
	if datestart = "" then datestart = null end if	
	active = Request.Form("rdo_CostCentreActive")
	if active = "" then active = 0 end if	
	dateend = Request.Form("txt_DateEnd")
	if dateend = "" then dateend = null end if	
	dateend = CDate(dateend)
	datestart = Cdate(datestart)
	if (dateend < datestart) then
		dateend = datestart + 1
	end if
	dateend = FormatDateTime(dateend,1)
	datestart = FormatDateTime(datestart,1)
	amount = Request.Form("txt_CostCentreAllocation")
	if amount = "" then amount = null end if
End Function

Function UpdateRecord (theID)
	GetFormFields()
	
	SQL = "UPDATE F_COSTCENTRE " &_
			"SET DESCRIPTION = '" & CostCentre & "', " &_
					"DEPARTMENT = '" & department & "', "&_
					"ACCBODY = '" & accountablebody & "', "&_
					"DATESTART = '" & datestart & "', "&_
					"DATEEND = '" & dateend & "', "&_
					"USERID =" & Session("UserID") & ", "&_
					"LASTMODIFIED = '" & Date & "' " &_															
					"WHERE COSTCENTREID = " & theID
	Conn.Execute (SQL)
	
	SQL = "UPDATE F_COSTCENTRE_ALLOCATION " &_
			"SET COSTCENTREALLOCATION = 99999999999, "&_
					"ACTIVE = '" & active & "', "&_
					"MODIFIEDBY =" & Session("UserID") & ", "&_
					"MODIFIED = '" & Date & "' " &_															
					"WHERE COSTCENTREID = " & theID & " AND FISCALYEAR IS NULL "
	Conn.Execute (SQL)

End Function

Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT ISNULL(CCA.COSTCENTREALLOCATION,0) AS COSTCENTREALLOCATION, CC.COSTCENTREID, DESCRIPTION, DEPARTMENT, AccBody, APPROVEDBY, isnull(CCA.Active,0) AS ACTIVE, dateStart = CONVERT(VARCHAR,Datestart,103), dateend = CONVERT(VARCHAR,Dateend,103) FROM F_COSTCENTRE CC LEFT JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND FISCALYEAR IS NULL WHERE CC.COSTCENTREID = " & theID & ""
	Set Rs = Conn.Execute(strEventQuery)

	accountablebody = Rs("AccBody")
	department = Rs("DEPARTMENT")
	CostCentre = Rs("DESCRIPTION")
	datestart = Rs("datestart")
	active = Rs("active")
	dateend = Rs("dateend")
	amount = FormatNumber(Rs("COSTCENTREALLOCATION"),2,0,0,0)
	
	Rs.Close
	Set Rs = Nothing

	strEventQuery = "select isNull(sum(HEA.HEADALLOCATION),0) as alreadyallocated from F_HEAD HE LEFT JOIN F_HEAD_ALLOCATION HEA ON HE.HEADID = HEA.HEADID AND FISCALYEAR IS NULL WHERE COSTCENTREID = " & theID & ""
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
	minimumTotal = Rs4("alreadyallocated")
	Rs4.Close
	Set Rs4 = Nothing	
End Function	

ACTION_TO_TAKE = Request("CC_A")
CostCentreID = Request("CCID")

OpenDB()

If (ACTION_TO_TAKE = "L") Then
	GetData(CostCentreID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(CostCentreID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
<% if ACTION_TO_TAKE = "UPDATE" Then %>
		parent.refreshSideBar();
		parent.setText("Cost Centre has been updated successfully.",1);			
<% Elseif ACTION_TO_TAKE = "L" Then %>
		parent.setText("<table class='iagManagerSmallBlk' width=380px><tr><td><b><u>Update Cost Centre</u></b></td></tr></table>");			
		parent.thisForm.txt_CostCentreDescription.value = "<%=CostCentre%>";
		parent.thisForm.CCID.value = "<%=CostCentreID%>";
		parent.thisForm.txt_AccountableBody.value = "<%=accountablebody%>";
		parent.thisForm.txt_Department.value = "<%=department%>";
		parent.thisForm.txt_DateStart.value = "<%=datestart%>";
		parent.thisForm.txt_DateEnd.value = "<%=dateend%>";
		parent.thisForm.CC_A.value = "UPDATE";
		if ("<%=active%>" == "True")
			parent.thisForm.rdo_CostCentreActive[0].checked = true;
		else
			parent.thisForm.rdo_CostCentreActive[1].checked = true;		
		parent.setCheckingArray('COSTCENTRE');
		parent.Budget.style.display = "none";
		parent.Head.style.display = "none";		
		parent.CostCentre.style.display = "block";		
<% End if %>
	}
</script>
	

</body>
</html>
