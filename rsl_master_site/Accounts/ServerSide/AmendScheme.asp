<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	'REWRITTEN BY ALI ON 23 FEB 2005
	
	'''''' PAGE NOTE: '''''
	' 7/june/2004 - This page was originally a mirror of CreateScheme.asp with slight 
	' adjustments to allow amendment of expenditures
	' <<<< new note here please >>>>>
	'''''''''''''''''''''''
	
	'7/june/2004 - Get the Head ID from AmendScheme pop up
	Dim HDID
	HDID = Request.Form("HDID")
	
	OpenDB()

	CheckList = Replace(Request.Form("iChecks"), " ", "")

	'GET THE INFORMATION REQUIRED TO CREATE A NEW HEAD SCHEME	
	SchemeName = Request.Form("txt_SCHEME")
	SchemeTotal = Request.Form("txt_RunningTotal")
	CurrentDate = FormatDateTime(Date,1)

	' 7/june/2004 - Construct SQL statement to amend the details of the development
	' Agreed by Jimmy & Zanfa baby that USERID is last person to modify
	
	SQL = "UPDATE F_HEAD " & _
		  "SET F_HEAD.USERID = " & Session("USERID") & ", " &_  
		  " F_HEAD.LASTMODIFIED = '" & CurrentDate & "' "&_
		  " WHERE ((F_HEAD.HEADID= " & HDID & "))"
	Conn.Execute(SQL)

	SQL = "UPDATE F_HEAD_ALLOCATION " & _
		  "SET F_HEAD_ALLOCATION.MODIFIEDBY = " & Session("USERID") & ", " &_  
		  " F_HEAD_ALLOCATION.ACTIVE = 1, "&_
		  " F_HEAD_ALLOCATION.MODIFIED = '" & CurrentDate & "', "&_
		  " F_HEAD_ALLOCATION.HEADALLOCATION = " & SchemeTotal & " "&_
		  " WHERE ((F_HEAD_ALLOCATION.HEADID= " & HDID & "))"
	Conn.Execute(SQL)	
		
	'SPLIT THE CHECKLIST INTO AN ARRAY SO WE CAN LOOP THORUGH EACH OF THE ITEMS....	
	CheckArray = Split(CheckList, ",")

	'Declare vars for strings and counters
	Dim ExID, NoStatus, ICounter
	
	ExIDList = ""
	ICounter = 0
	
	'Find the size of the array
	theArraySize =  Ubound(CheckArray)
	 
	'Loop through array of Expend records in array
	for each ExpendIDNumber in CheckArray
	
		ExID =  Request.Form("txt_EXP" & ExpendIDNumber)
		ExDescription = Request.Form("txt_Code" & ExpendIDNumber)

		'If the record in the array does already exist in the db then AMEND the data
		if ExID <> "" then
		
				'Amend the expenditure info that match with the expend IDs			
				SQL = "UPDATE F_EXPENDITURE " & _
				  "SET F_EXPENDITURE.DESCRIPTION = '" & ExDescription & "', " &_
				  "F_EXPENDITURE.USERID = " & Session("USERID") & ", " &_
				  "F_EXPENDITURE.QBDEBITCODE = '" & Request.Form("txt_QBDEBITCODE" & ExpendIDNumber)  & "', " &_
				  "F_EXPENDITURE.QBCONTROLCODE = '" & Request.Form("txt_QBCONTROLCODE" & ExpendIDNumber)  & "', " &_				  
				  "F_EXPENDITURE.LASTMODIFIED = '" & CurrentDate & "' " &_
				  " WHERE ((F_EXPENDITURE.HEADID= " & HDID & " AND F_EXPENDITURE.EXPENDITUREID = " & ExID & " ))"
				  Conn.Execute(SQL) 

				'Amend the expenditure info that match with the expend IDs			
				SQL = "UPDATE F_EXPENDITURE_ALLOCATION " & _
				  "SET F_EXPENDITURE_ALLOCATION.EXPENDITUREALLOCATION = " & Request.Form("txt_Val" & ExpendIDNumber) & ", "  &_
				  "F_EXPENDITURE_ALLOCATION.MODIFIEDBY = " & Session("USERID") & ", " &_
				  "F_EXPENDITURE_ALLOCATION.ACTIVE = 1, " &_
				  "F_EXPENDITURE_ALLOCATION.MODIFIED = '" & CurrentDate & "' " &_
				  " WHERE (F_EXPENDITURE_ALLOCATION.EXPENDITUREID = " & ExID & " )"
				  Conn.Execute(SQL) 				   
		else
		
				' INSERT new expenditure and retrieve the ID number		
				SQL = "SET NOCOUNT ON;INSERT INTO F_EXPENDITURE (DESCRIPTION, EXPENDITUREDATE, USERID, LASTMODIFIED, HEADID, QBDEBITCODE, QBCONTROLCODE, OPENBALANCE) VALUES " &_
					 "('" & ExDescription & "', '" & CurrentDate & "', " & Session("USERID") & ", '" & CurrentDate & "', " & HDID  & ", '" & Request.Form("txt_QBDEBITCODE" & ExpendIDNumber)  & "', '" & Request.Form("txt_QBCONTROLCODE" & ExpendIDNumber) & "', 0) " &_
					 "SELECT SCOPE_IDENTITY() AS EXPID;SET NOCOUNT OFF"
					 Call OpenRs(rsInsert, SQL)
					 ExID = rsInsert("EXPID")
					 CloseRs(rsInsert)
				
				SQL = "INSERT INTO F_EXPENDITURE_ALLOCATION (EXPENDITUREID, EXPENDITUREALLOCATION, MODIFIED, MODIFIEDBY, ACTIVE) VALUES ( " &_ 
					ExID & ", " & Request.Form("txt_Val" & ExpendIDNumber) & ", '" & CurrentDate & "', " & Session("USERID") & ", 1)"
				Conn.Execute SQL		
		End IF	
		
		'If the item is the last in the array then don't put a comma on the end
		ICounter = ICounter + 1
		ExIDList = ExIDList & ExID
		If ExID <> "" then 
		If ICounter <= theArraySize then ExIDList = ExIDList & "," End If
		End IF
			
	next
	if (ExIDList = "") then
		SQL = "UPDATE F_EXPENDITURE_ALLOCATION SET ACTIVE = 0 FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR IS NULL WHERE EX.HEADID =  " & HDID & " "
	else
		SQL = "UPDATE F_EXPENDITURE_ALLOCATION SET ACTIVE = 0 FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR IS NULL WHERE EX.HEADID =  " & HDID & " AND EX.EXPENDITUREID NOT IN (" & ExIDList & ")"
	end if	
	Conn.Execute(SQL)

	CloseDB()
	
	Response.Redirect("HD_DEV.asp?HDID=" & HDID & "&HD_A=L&WITHRELOAD=1")	
%>