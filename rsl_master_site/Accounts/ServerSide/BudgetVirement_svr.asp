<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	TRANSFERAMOUNT = Replace(REQUEST("txt_TRANSFERFIGURE"), ",", "")
	FROM_EXP = REQUEST("sel_EXPENDITURE1")
	TO_EXP = REQUEST("sel_EXPENDITURE3")
			
	OpenDB()

	Call GetCurrentYear()
	FY = GetCurrent_YRange	

	SQL = "INSERT INTO F_EXPENDITURE_VIREMENT (TRANSFERAMOUNT, FISCALYEAR, USERID, FROM_EXP, TO_EXP) VALUES ( " &_
			TRANSFERAMOUNT & "," & FY & "," & SESSION("USERID") & "," & FROM_EXP & "," & TO_EXP & ")"
	Conn.Execute SQL

	'DEDUCT FROM THE INITIAL EXPENDITURE
	SQL = "UPDATE F_EXPENDITURE_ALLOCATION SET EXPENDITUREALLOCATION = EXPENDITUREALLOCATION - " & TRANSFERAMOUNT & " WHERE (FISCALYEAR = " & FY & " OR FISCALYEAR IS NULL) AND EXPENDITUREID = " & FROM_EXP
	Conn.Execute SQL

	'DEDUCT FROM THE INITIAL HEAD
	SQL = "UPDATE F_HEAD_ALLOCATION SET HEADALLOCATION = HEADALLOCATION - " & TRANSFERAMOUNT & " FROM F_EXPENDITURE EX INNER JOIN F_HEAD HE ON HE.HEADID = EX.HEADID INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND (FISCALYEAR = " & FY & " OR FISCALYEAR IS NULL) WHERE EX.EXPENDITUREID = " & FROM_EXP
	Conn.Execute SQL
	
	'ADD TO THE TARGET EXPENDITURE
	SQL = "UPDATE F_EXPENDITURE_ALLOCATION SET EXPENDITUREALLOCATION = EXPENDITUREALLOCATION + " & TRANSFERAMOUNT & " WHERE (FISCALYEAR = " & FY & " OR FISCALYEAR IS NULL) AND EXPENDITUREID = " & TO_EXP
	Conn.Execute SQL

	'ADD TO THE INITIAL HEAD
	SQL = "UPDATE F_HEAD_ALLOCATION SET HEADALLOCATION = HEADALLOCATION + " & TRANSFERAMOUNT & " FROM F_EXPENDITURE EX INNER JOIN F_HEAD HE ON HE.HEADID = EX.HEADID INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND (FISCALYEAR = " & FY & " OR FISCALYEAR IS NULL) WHERE EX.EXPENDITUREID = " & TO_EXP
	Conn.Execute SQL

	
	Response.Redirect "../BudgetVirementList.asp"		
%>