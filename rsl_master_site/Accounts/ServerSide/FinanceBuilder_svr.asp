<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'"
Call OpenRs(rsDev, SQL)
if (NOt rsDev.eOF) then
	DevID = rsDev("DEFAULTVALUE")
else
	DevID = -1
end if
CloseRs(rsDev)


CloseDB()

CompanyFilter = ""
rqCompany = Request("sel_COMPANY")
if (rqCompany = "") then rqCompany = "1"
CompanyFilter = " AND  CC.COMPANYID = '" & rqCompany & "' "
 
StatusFilter = ""
rqStatus = Request("sel_FinanceBuilderStatus")

if (rqStatus = "") then rqStatus = "1"
if (rqStatus = "1") then StatusFilter = " AND  isnull(CCA.ACTIVE,0) = " & rqStatus & " "
if (rqStatus = "2") then StatusFilter = " AND  isnull(CCA.ACTIVE,0) = 0 "


FY = Request("FY")
        
    Call OpenDB()
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", null, rqCompany, NULL, "textbox200", " onchange='SubmitPage()' style='width:170px'")	
    Call BuildSelect(lstFinanceBuilderStatus, "sel_FinanceBuilderStatus", "F_ExpenditureStatus", "ExpenditureStatusID, DESCRIPTION", "ExpenditureStatusID", NULL, rqStatus, NULL, "textbox200", " onchange='SubmitPage()' style='width:170px'")
	
    Call CloseDB()


%>
<html>
<head>
<link type="text/css" rel="stylesheet" href="/js/XTree/xtree2.css" />
<script type="text/javascript" src="/js/XTree/xtree2.js"></script>
<script type="text/javascript" src="/js/XTree/xmlextras.js"></script>
<script type="text/javascript" src="/js/XTree/xloadtree2.js"></script>
<script language=javascript>
var rti;
var CurrentSelected = ""
function GO(iPage){
    CurrentlySelected = tree.getSelected().getParent()
    var Company = document.getElementById("sel_COMPANY").value;
    var Status = document.getElementById("sel_FinanceBuilderStatus").value;
    parent.FB.location.href = iPage + "&FY=<%=FY%>&Company=" + Company + "&FinanceBuilderStatus=" + Status;
	}

    function PopLimit1() {
        var Company = document.getElementById("sel_COMPANY").value;
	    window.showModalDialog("../Popups/SetGlobalEmployeeLimits.asp?Company=" + Company, "Limits by User", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
	}
		
function ReloadMe(){
	if (CurrentlySelected != "")
		CurrentlySelected.reload();
	}

    function SubmitPage() {
        var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
        var FinanceBuilderStatus = document.getElementById("sel_FinanceBuilderStatus").options[document.getElementById("sel_FinanceBuilderStatus").selectedIndex].value;

        location.href = "FinanceBuilder_svr.asp?FY=<%=FY%>&sel_COMPANY=" + Company + "&sel_FinanceBuilderStatus=" + FinanceBuilderStatus;

	}

</script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="0" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<img name="web_folder" src="images/folder.png" width="1" height="1">
<img name="web_openfolder" src="images/openfolder.png" width="1" height="1">
<img name="web_file" src="images/file.png" width="1" height="1">
<img name="web_lminus" src="images/Lminus.png" width="1" height="1">
<img name="web_lplus" src="images/Lplus.png" width="1" height="1">
<img name="web_tminus" src="images/Tminus.png" width="1" height="1">
<img name="web_tplus" src="images/Tplus.png" width="1" height="1">
<img name="web_i" src="images/I.png" width="1" height="1">
<img name="web_l" src="images/L.png" width="1" height="1">
<img name="web_t" src="images/T.png" width="1" height="1">
<img name="web_base" src="images/base.gif" width="1" height="1">
<img name="web_img1" src="img1.gif" width="1" height="1">
<img name="web_img2" src="img2.gif" width="1" height="1">
<img name="web_img3" src="img3.gif" width="1" height="1">
<img name="web_img4" src="img4.gif" width="1" height="1">
<img name="web_img5" src="img5.gif" width="1" height="1">
<img name="web_img6" src="img6.gif" width="1" height="1">
<img name="web_img7" src="img7.gif" width="1" height="1">
<form name="thisForm" method="get" action="">

<table>
    <tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif;FONT-SIZE: 10PX;">Company:</td><td><%=lstCompany %></td></tr>
    <tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif;FONT-SIZE: 10PX;">Show:</td><td><%=lstFinanceBuilderStatus %></td></tr>
</table>

</form>
 
<div class="dtree" style='height:100%'>
<script type="text/javascript">
parent.thisForm.hdn_CompanyId.value = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
/// XP Look
webFXTreeConfig.rootIcon		= web_folder.src;
webFXTreeConfig.openRootIcon	= web_openfolder.src;
webFXTreeConfig.folderIcon		= web_folder.src;
webFXTreeConfig.openFolderIcon	= web_openfolder.src;
webFXTreeConfig.fileIcon		= web_file.src;
webFXTreeConfig.lMinusIcon		= web_lminus.src;
webFXTreeConfig.lPlusIcon		= web_lplus.src;
webFXTreeConfig.tMinusIcon		= web_tminus.src;
webFXTreeConfig.tPlusIcon		= web_tplus.src;
webFXTreeConfig.iIcon			= web_i.src;
webFXTreeConfig.lIcon			= web_l.src;
webFXTreeConfig.tIcon			= web_t.src;

var rti;
var tree = new WebFXTree("RSL Finance Builder", "", "", web_base.src, web_base.src);
tree.add(new WebFXTreeItem("Limits by User", "javascript:PopLimit1()"));
<%

SQL = "SELECT  CC.COSTCENTREID, DESCRIPTION, ISNULL(CCA.ACTIVE,0) AS ACTIVE FROM F_COSTCENTRE CC LEFT JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND FISCALYEAR = " & FY & " WHERE CC.COSTCENTREID <> " & DevID  & CompanyFilter & StatusFilter &  " ORDER BY DESCRIPTION"

    Call OpenDB()
Call OpenRs(rsCC, SQL)
while NOT rsCC.EOF
	if (rsCC("ACTIVE") = true) then
		Response.Write "tree.add(new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & """, ""TL_F.asp?TYPE=1&CCID=" & rsCC("CostCentreID")  & "&FinanceBuilderStatus=" & rqStatus & "&COMPANY=" & rqCompany & "&FY=" & FY & """, ""JavaScript:GO('CC.asp?CCID=" & rsCC("CostCentreID") & "&FinanceBuilderStatus=" & rqFinanceBuilderStatus &  "&zCOMPANY=" & rqCompany & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	else
		Response.Write "tree.add(new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & """, ""TL_F.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&FinanceBuilderStatus=" & rqStatus &  "&COMPANY=" & rqCompany & "&FY=" & FY & """, ""JavaScript:GO('CC.asp?CCID=" & rsCC("CostCentreID") & "&FinanceBuilderStatus=" & rqFinanceBuilderStatus &  "&zCOMPANY=" & rqCompany & "&CC_A=L')"", """", web_img7.src, web_img7.src));"
	end if	
	rsCC.moveNext
wend
Response.Write "tree.add(new WebFXLoadTreeItem(""New Cost Centre"", """", ""JavaScript:parent.NewItem(1)""));"	
Call CloseRs(rsCC)
Call CloseDB()
%>

tree.write()
</script>
</div>
</body>
</html>
