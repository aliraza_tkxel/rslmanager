<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
		
	OpenDB()

    rqCompany = Request("COMPANY")
	
    if not rqCompany = "" then

	    'do all the front page checks again, to make sure they are not by passed somehow.
	    SQL = "SELECT ISNULL(MAX(TRANSACTIONID),-1) AS MAXID FROM NL_JOURNALENTRY WHERE TRANSACTIONTYPE = 27 and companyid = " & rqCompany
	
	    LastRunYear = -1
	    Call OpenDB()
	    Call OpenRs(rsYE, SQL)
	    if (NOT rsYE.EOF) then
		    LastRunYear = Clng(rsYE("MAXID"))
	    end if
	    Call CloseRs(rsYE)

	    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'CURRENT_INCOME_EXPENSE_YEAREND_ROUTINE" & rqCompany & "'"
	    Call OpenRs(rsT, SQL)
	    if (NOT rsT.EOF) then
		    CurrentRunYear = CLng(rsT("DEFAULTVALUE"))
	    else
		    CurrentRunYear = -1
	    end if
	    Call CloseRs(rsT)
	
	    'check whether the current run will be greater than the last run finacial year
	    if (CurrentRunYear > LastRunYear) then
		    'validate the financial year and if the range exists for the id
		    if (CurrentRunYear <> -1) then
			    SQL = "SELECT * FROM F_FISCALYEARS WHERE YRANGE = " & CurrentRunYear
			    Call OpenRs(rsFY, SQL)
			    if (NOT rsFY.EOF) then
				    StartDate = CDate(rsFY("YSTART"))
				    EndDate = CDate(rsFY("YEND"))
			    else
				    CurrentRunYear = -1
			    end if
			    Call CloseRs(rsFY)
		    end if
	    else
		    CurrentRunYear = -2
	    end if
	

	
	    'IF THE VARIABLE IS EQUAL TO -2 THEN THE CURRENT RUN FINANCIAL YEAR IS INCORRECT.
	    if CurrentRunYear = -2 then
		    RunFailureText = "The current financial year is not greater than the last financial year run of [" & LastRunYear & "]. Please contact the software vendor."
	    'IF THE VARIABLE IS EQUAL TO -1 THEN SOME ERROR HAS OCCURRED AND THE PROCEDURE CANNOT BE RUN
	    elseif CurrentRunYear = -1 then
		    RunFailureText = "Unable to establish which year, the year end routine should be run for. Please contact the software vendor."
	    'IF THE DATE IS GREATER THAN THE END DATE, THEN THE YEAR HAS ENDED AND THE ROUTINE CAN BE RUN.
	    elseif Date > EndDate then
		    RunFailureText = ""
		
		    SQL = "EXEC NL_YEAR_END_ROUTINE_FOR_EXPENSES_AND_INCOME " & CurrentRunYear
		    Conn.Execute SQL
	
	    'OTHERWISE THE ROUTINE CANNOT BE RUN BECUASE THE YEAR END HAS NOT BEEN REACHED
	    else
		    RunFailureText = "The financial year from " & FormatDateTime(StartDate,2) & " to " & FormatDateTime(EndDate,2) & " is still current. Therefore the year end routine cannot be run."
	    end if

	    if (RunFailureText <> "") then
		    Session("EndOfYearRoutineFailure") = RunFailureText
	    end if

	    Call CloseDB()
		
	    Response.Redirect "../EndOfYearRoutine.asp"		

        end if
%>