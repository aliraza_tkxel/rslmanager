<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Response.ContentType = "text/xml"

Set objDom = Server.CreateObject("Microsoft.XMLDOM")
objDom.preserveWhiteSpace = True

Company = Request("Company")
if (Company="") then Company = "1"

if (Request("TYPE") = "1") then
	CCID = Request("CCID")
	IF (CCID = "") THEN CCID = -1

	WHICHLIST = Request("ALPHA")
	SELECT CASE WHICHLIST
		CASE "1" 	ALAPHABET_FILTER = " AND DESCRIPTION < 'A' "
		CASE "2"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'A' AND DESCRIPTION < 'D' "
		CASE "3"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'D' AND DESCRIPTION < 'G' "
		CASE "4"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'G' AND DESCRIPTION < 'J' "
		CASE "5"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'J' AND DESCRIPTION < 'M' "
		CASE "6" 	ALAPHABET_FILTER = " AND DESCRIPTION >= 'M' AND DESCRIPTION < 'P' "
		CASE "7"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'P' AND DESCRIPTION < 'S' "
		CASE "8"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'S' AND DESCRIPTION < 'V' "
		CASE "9"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'V' AND DESCRIPTION < 'Y' "
		CASE "10"	ALAPHABET_FILTER = " AND DESCRIPTION >= 'Y' "
	END SELECT
	
	SQL = "SELECT HE.HEADID, DESCRIPTION, HE.COSTCENTREID, HEA.ACTIVE FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND FISCALYEAR IS NULL " & ALAPHABET_FILTER & " INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = he.HEADID "
    SQL = SQL & " left JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID WHERE HE.COSTCENTREID = " & CCID & " and p.companyid = " & Company & " ORDER BY DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsHE, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot
	if (rsHE.EOF) then
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = "No Developments Found"
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the icon attribute to the field node ***
		   Set objcolName = objDom.createAttribute("icon")
		   objcolName.Text = "images/exclamation.16.gif"
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the openIcon attribute to the field node ***
		   Set objcolName = objDom.createAttribute("openIcon")
		   objcolName.Text = "images/exclamation.16.gif"
		   objRow.SetAttributeNode(objColName)
	
		   objRoot.appendChild objRow
		
	else 
		While NOT rsHE.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsHE("DESCRIPTION")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the src attribute to the field node ***
		   Set objcolName = objDom.createAttribute("src")
		   objcolName.Text = "TreeLoad.asp?TYPE=2&HDID=" & rsHE("HEADID")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the action attribute to the field node ***
		   Set objcolName = objDom.createAttribute("action")
		   objcolName.Text = "Javascript:GOHE('HD_DEV.asp?HDID=" & rsHE("HEADID") & "&HD_A=L','" & WHICHLIST & "')"
		   objRow.SetAttributeNode(objColName)

		   if (rsHE("ACTIVE") = true) then
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img2.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img2.gif"
			   objRow.SetAttributeNode(objColName)	
		   else
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img6.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img6.gif"
			   objRow.SetAttributeNode(objColName)
			end if
	
		   objRoot.appendChild objRow
	
			rsHE.moveNext()	
		Wend
	end if
	Call CloseRs(rsHE)
	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 
	
elseif (Request("TYPE") = "2") then

	HDID = Request("HDID")
	IF (HDID = "") THEN HDID = -1
	SQL = "SELECT EX.EXPENDITUREID, DESCRIPTION, HEADID, EXA.ACTIVE FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR IS NULL WHERE EX.HEADID = " & HDID & " ORDER BY DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsHE, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot

 	if (rsHE.EOF) then
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = "No Expenditures Found"
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the icon attribute to the field node ***
		   Set objcolName = objDom.createAttribute("icon")
		   objcolName.Text = "images/exclamation.16.gif"
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the openIcon attribute to the field node ***
		   Set objcolName = objDom.createAttribute("openIcon")
		   objcolName.Text = "images/exclamation.16.gif"
		   objRow.SetAttributeNode(objColName)
	
		   objRoot.appendChild objRow
		
	else 
		While NOT rsHE.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsHE("DESCRIPTION")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the action attribute to the field node ***
		   Set objcolName = objDom.createAttribute("action")
		   objcolName.Text = "Javascript:GO('EX_DEV.asp?EXID=" & rsHE("EXPENDITUREID") & "&EX_A=L')"
		   objRow.SetAttributeNode(objColName)
	
			if (rsHE("ACTIVE") = true) then
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)
			else
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img5.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img5.gif"
			   objRow.SetAttributeNode(objColName)

			end if	
		   objRoot.appendChild objRow
	
			rsHE.moveNext()	
		Wend
	end if
	Call CloseRs(rsHE)
	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 

end if
%>
