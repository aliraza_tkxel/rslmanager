<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

zCompany = Request("zCompany")
FinanceBuilderStatus = Request("FinanceBuilderStatus")

Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText	
	Dim CostCentreID, CostCentre, accountablebody, approvedby, department, datestart, dateend, amount, active, minimumTotal, FY, CompanyId

'	On Error Resume Next

Function GetFormFields ()
	accountablebody = Request.Form("txt_AccountableBody")
	if accountablebody = "" then accountablebody = null end if
	department = Request.Form("txt_Department")
	if department = "" then department = null end if	
	CostCentre = Request.Form("txt_CostCentreDescription")
	if CostCentre = "" then CostCentre = null end if	
	datestart = Request.Form("txt_DateStart")
	if datestart = "" then datestart = null end if	
	active = Request.Form("rdo_CostCentreActive")
	if active = "" then active = 0 end if	
	dateend = Request.Form("txt_DateEnd")
	if dateend = "" then dateend = null end if	
	dateend = CDate(dateend)
	datestart = Cdate(datestart)
	if (dateend < datestart) then
		dateend = datestart + 1
	end if
	dateend = FormatDateTime(dateend,1)
	datestart = FormatDateTime(datestart,1)
	amount = Request.Form("txt_CostCentreAllocation")
	if amount = "" then amount = null end if
    CompanyId=Request.form("hdn_CompanyId")

End Function

Function NewRecord ()
	GetFormFields()
	
	SQL = "SET NOCOUNT ON;INSERT INTO F_COSTCENTRE (DESCRIPTION, DEPARTMENT, ACCBODY, DATESTART, DATEEND, USERID, LASTMODIFIED, COMPANYID) " &_
			"VALUES ('" & CostCentre & "', " &_
					"'" & department & "', "&_
					"'" & accountablebody & "', "&_
					"'" & datestart & "', "&_
					"'" & dateend & "', "&_
					"" & Session("UserID") & ", "&_
					"getdate()," & CompanyId & ");SELECT SCOPE_IDENTITY() AS NEWID;SET NOCOUNT OFF "
	Call OpenRs(rsINS, SQL)
	CCID = rsINS("NEWID")
	Call CloseRs(rsINS)

	'THIS FUCNTION HAS BEEN SPLIT UP, SO THAT THIS PART NOW STORES THE AMOUNT, ETC.
	SQL = "INSERT INTO F_COSTCENTRE_ALLOCATION (COSTCENTREID, FISCALYEAR, COSTCENTREALLOCATION, ACTIVE, MODIFIEDBY, MODIFIED, companyid) " &_
			"VALUES ('" & CCID & "', " &_
					"'" & FY & "', "&_
					"" & amount & ", "&_
					"'" & active & "', "&_
					"" & Session("UserID") & ", "&_
					"getdate()," & CompanyId & ") "
	Conn.Execute (SQL)
End Function

Function UpdateRecord (theID)
	GetFormFields()

	SQL = "UPDATE F_COSTCENTRE " &_
			"SET DESCRIPTION = '" & CostCentre & "', " &_
					"DEPARTMENT = '" & department & "', "&_
					"ACCBODY = '" & accountablebody & "', "&_
					"DATESTART = '" & datestart & "', "&_
					"DATEEND = '" & dateend & "', "&_
					"USERID =" & Session("UserID") & ", "&_
					"LASTMODIFIED = getdate() " &_															
					"WHERE COSTCENTREID = " & theID & " AND companyid = " & CompanyId
	Conn.Execute (SQL)
	
	'BECAUSE OF THE MULTIPLE YEAR SCENARIO WE HAVE TO CHECK IF THE RECORD EXISTS
	'EACH YEAR OR NOT

	SQL = "SELECT COSTCENTREID FROM F_COSTCENTRE_ALLOCATION WHERE COSTCENTREID = " & theID & " AND companyid = " & CompanyId & "  AND FISCALYEAR = " & FY
	Call OpenRs(rsExists, SQL)
	if (rsExists.EOF) then
		SQL = "INSERT INTO F_COSTCENTRE_ALLOCATION (COSTCENTREID, FISCALYEAR, COSTCENTREALLOCATION, ACTIVE, MODIFIEDBY, MODIFIED, COMPANYID) " &_
				"VALUES ('" & theID & "', " &_
						"'" & FY & "', "&_
						"" & amount & ", "&_
						"'" & active & "', "&_
						"" & Session("UserID") & ", "&_
						"getdate()," & CompanyId & ") "
		Conn.Execute (SQL)	
	else
		SQL = "UPDATE F_COSTCENTRE_ALLOCATION " &_
				"SET COSTCENTREALLOCATION = " & amount & ", "&_
						"ACTIVE = '" & active & "', "&_
						"MODIFIEDBY =" & Session("UserID") & ", "&_
						"MODIFIED = getdate() " &_															
						"WHERE COSTCENTREID = " & theID & " AND companyid = " & CompanyId & " AND FISCALYEAR = " & FY
		Conn.Execute (SQL)
	end if
	Call CloseRs(rsExists)

End Function

Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT ISNULL(CCA.COSTCENTREALLOCATION,0) AS COSTCENTREALLOCATION, CC.COSTCENTREID, DESCRIPTION, DEPARTMENT, AccBody, APPROVEDBY, isnull(CCA.Active,0) AS ACTIVE, dateStart = CONVERT(VARCHAR,Datestart,103), dateend = CONVERT(VARCHAR,Dateend,103) FROM F_COSTCENTRE CC LEFT JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND FISCALYEAR = " & FY  & " WHERE CC.COSTCENTREID = " & theID & " and cc.CompanyId = " & zCompany
	Set Rs = Conn.Execute(strEventQuery)

	accountablebody = Rs("AccBody")
	department = Rs("DEPARTMENT")
	CostCentre = Rs("DESCRIPTION")
	datestart = Rs("datestart")
	active = Rs("active")
	dateend = Rs("dateend")
	amount = FormatNumber(Rs("COSTCENTREALLOCATION"),2,-1,0,0)
	
	Rs.Close
	Set Rs = Nothing

	strEventQuery = "select isNull(sum(HEA.HEADALLOCATION),0) as alreadyallocated from F_HEAD HE LEFT JOIN F_HEAD_ALLOCATION HEA ON HE.HEADID = HEA.HEADID AND FISCALYEAR = " & FY & "  WHERE COSTCENTREID = " & theID 
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Set Rs4 = Conn.Execute(strEventQuery)
	minimumTotal = Rs4("alreadyallocated")
	Rs4.Close
	Set Rs4 = Nothing	
End Function	

Function DelRecord (theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT COUNT(*) AS THECOUNT from F_HEAD where COSTCENTREID = " & theID
	Set Rs4 = Conn.Execute(strEventQuery)
	ActualCount = Rs4("THECOUNT")
	Rs4.close()
	Set Rs4 = Nothing
			
	if (ActualCount = 0) then
		Conn.Execute ("DELETE FROM F_COSTCENTRE WHERE COSTCENTREID = " & theID & ";")
		Conn.Execute ("DELETE FROM F_COSTCENTRE_ALLOCATION WHERE COSTCENTREID = " & theID & ";")

		treeText = "Cost Centre deleted successfully."		
	else
		treeText = "Sorry, cannot delete the selected COST CENTRE as it is has " & ActualCount & " HEADS setup underneath it. You can set the COST CENTRE in-active instead."
	end if

End Function

ACTION_TO_TAKE = Request("CC_A")
CostCentreID = Request("CCID")

'make sure we get the currrent fiscal year variable one way or the other
FY = Request("FY")
if (FY = "") then
	FY = Request("hdn_FY")
end if

Response.Write("Fiscal Year is " & FY & "<br>")

OpenDB()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "L") Then
	GetData(CostCentreID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(CostCentreID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(CostCentreID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
    function returnData() { 
<% if ACTION_TO_TAKE = "ADD" Then %>
		parent.refreshSideBar();
		parent.setText("New Cost Centre added successfully.",1);		
        <% Elseif ACTION_TO_TAKE = "UPDATE" Then %>
		parent.refreshSideBar();
		parent.setText("Cost Centre has been updated successfully.",1);			
<% Elseif ACTION_TO_TAKE = "DELETE" Then %>
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);
<% Elseif ACTION_TO_TAKE = "L" Then %>
		parent.NewItem(1);
		parent.showDeleteButton("COSTCENTRE",2);		
		parent.setText("<table class='iagManagerSmallBlk' width=380px><tr><td><b><u>Update Cost Centre</u></b></td></tr></table>");			
		parent.thisForm.txt_CostCentreAllocation.value = "<%=amount%>";
		parent.thisForm.txt_CostCentreDescription.value = "<%=CostCentre%>";
		parent.thisForm.CCID.value = "<%=CostCentreID%>";
		parent.thisForm.costcentremintotal.value = "<%=minimumTotal%>";		
		parent.thisForm.txt_AccountableBody.value = "<%=accountablebody%>";
		parent.thisForm.txt_Department.value = "<%=department%>";
		parent.thisForm.txt_DateStart.value = "<%=datestart%>";
        parent.thisForm.txt_DateEnd.value = "<%=dateend%>";
        parent.thisForm.hdn_CompanyId.value = "<%=zCompany%>";
		parent.thisForm.CC_A.value = "UPDATE";
		if ("<%=active%>" == "True")
			parent.thisForm.rdo_CostCentreActive[0].checked = true;
		else
			parent.thisForm.rdo_CostCentreActive[1].checked = true;		
		parent.setCheckingArray('COSTCENTRE');
<% End if %>
	}
</script>
	

</body>
</html>
