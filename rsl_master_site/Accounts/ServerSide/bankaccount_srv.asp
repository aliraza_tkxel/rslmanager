<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID, LoaderString
	ReDim DataFields   (11)
	ReDim DataTypes    (11)
	ReDim ElementTypes (11)
	ReDim FormValues   (11)
	ReDim FormFields   (11)
	UpdateID	  = "hid_ACCOUNTID"
	FormFields(0) = "txt_ACCOUNTNAME|TEXT"
	FormFields(1) = "txt_BANK|TEXT"
	FormFields(2) = "txt_ADD1|TEXT"
	FormFields(3) = "txt_ADD2|TEXT"
	FormFields(4) = "txt_TOWN|TEXT"
	FormFields(5) = "txt_COUNTY|TEXT"
	FormFields(6) = "txt_POSTCODE|TEXT"
	FormFields(7) = "txt_ACCOUNTNO|TEXT"
	FormFields(8) = "txt_SORTCODE|TEXT"
	FormFields(9) = "sel_ACCOUNTTYPE|SELECT"
	FormFields(10) = "sel_NOMINALACCOUNT|SELECT"
    FormFields(11) = "sel_COMPANYID|SELECT"
	OpenDB()
	
	Dim nlaccountname, nlaccountid, bankaccounttype, bankaccountid, companyid
	
	TheAction = Request("hid_Action")
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	CloseDB()

	Function NewRecord ()

		Call MakeInsert(strSQL)	
		SQL = "SET NOCOUNT ON;INSERT INTO NL_BANKACCOUNT " & strSQL & ";SELECT SCOPE_IDENTITY() AS BANKID;"
		Call OpenRs(rsNEW, SQL)
		ID = rsNEW("BANKID")
		Call CloseRs(rsNEW)
		
		SQL = "INSERT INTO NL_BANKACCOUNT_OPENINGBALANCES (ACCOUNTID, YRANGE, BALANCE) VALUES " &_
				"(" & ID & ", " & Request("hid_FY") & ", " & Request("txt_OPENINGBALANCE") & ")"
		Conn.Execute SQL				
		GO()		
	End Function
	
	Function AmendRecord()

		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		SQL = "UPDATE NL_BANKACCOUNT " & strSQL & " WHERE ACCOUNTID = " & ID
		Conn.Execute SQL, recaffected
		
		SQL = "SELECT * FROM NL_BANKACCOUNT_OPENINGBALANCES WHERE ACCOUNTID = " & ID  & " AND YRANGE = " & REQUEST("hid_FY")
		Call OpenRs(RsExists, SQL)
		if (NOT RsExists.EOF) then
			SQL = "UPDATE NL_BANKACCOUNT_OPENINGBALANCES SET BALANCE = " & Request("txt_OPENINGBALANCE") & " WHERE ACCOUNTID = " & Request.Form("sel_NOMINALACCOUNT")  & " AND YRANGE = " & REQUEST("hid_FY")
			Conn.Execute SQL
		else
			SQL = "INSERT INTO NL_BANKACCOUNT_OPENINGBALANCES (ACCOUNTID, YRANGE, BALANCE) VALUES " &_
					"(" & ID & ", " & Request("hid_FY") & ", " & Request("txt_OPENINGBALANCE") & ")"
			Conn.Execute SQL
		end if
		Call CloseRs(RsExists)
		GO()
	End Function
	
	Function DeleteRecord()
		
		ID = Request.Form(UpdateID)
		response.write "<BR> Update id is : "& ID	
		SQL = "DELETE FROM NL_BANKACCOUNT WHERE ACCOUNTID = " & ID
		Conn.Execute SQL, recaffected
		GO()	
			
	End Function
	
	Function LoadRecord()
		ID = Request.Form(UpdateID)
		ReDim Preserve FormFields(12)
		FormFields(12) = "txt_OPENINGBALANCE|SELECT"
		LoaderString = ""
		'Call OpenRs(rsLoader, "SELECT * FROM NL_BANKACCOUNT WHERE ACCOUNTID = " & ID)
		SQL = "SELECT 	ACC.ACCOUNTID AS NLACC, ACC.ACCOUNTNUMBER + ' ' + ACC.NAME AS NLNAME, " &_
				"		ISNULL(A.ACCOUNTTYPE,-1) AS ACCOUNTTYPE,  " &_
				"		A.ACCOUNTNAME, T.BANKACCOUNTTYPE, OP.BALANCE AS OPENINGBALANCE, A.NOMINALACCOUNT, " &_
				"		A.BANK, A.ACCOUNTNO, A.SORTCODE, A.ADD1, A.ADD2, A.TOWN, A.COUNTY, A.POSTCODE , isnull(A.COMPANYID,1) companyid " &_
				"FROM	NL_BANKACCOUNT A " &_
				"		INNER JOIN NL_BANKACCOUNTTYPE T ON A.ACCOUNTTYPE = T.BANKACCOUNTTYPEID " &_
				"		INNER JOIN NL_ACCOUNT ACC ON ACC.ACCOUNTID = A.NOMINALACCOUNT " &_
				"		LEFT JOIN NL_BANKACCOUNT_OPENINGBALANCES OP ON OP.ACCOUNTID  = A.ACCOUNTID AND OP.YRANGE = " & Request("FY") & " " &_
				"WHERE 	A.ACCOUNTID = "  & ID
		Call OpenRs(rsLoader, SQL)
		RW SQL
		if not (rsLoader.EOF) then
			nlaccountname = rsLoader("NLNAME")
			nlaccountid = rsLoader("NLACC")
			bankaccounttype = rsLoader("BANKACCOUNTTYPE")
			bankaccountid = rsLoader("ACCOUNTTYPE")
            companyid = rsLoader("COMPANYID")
			for i=0 to Ubound(FormFields)
				FormArray = Split(FormFields(i), "|")
				TargetItem = FormArray(0)
				LoaderString = LoaderString & "parent.RSLFORM." & TargetItem & ".value = """ & rsLoader(RIGHT(TargetItem, Len(TargetItem)-4)) & """" & VbCrLf
			next
		end if
		CloseRs(rsLoader)
	End Function
	
	Function GO()
		'Response.Redirect "../BANKACCOUNT.ASP"	
	End Function
	
	
%>
<html>
<body>
<script language=javascript>
	<% if TheAction = "LOAD" then %>
        parent.RSLFORM.sel_NOMINALACCOUNT.options[parent.RSLFORM.sel_NOMINALACCOUNT.options.length] = new Option('<%=nlaccountname%>', '<%=nlaccountid%>', true);
		<%IF CINT(bankaccountid) <> 2 Then %>
        parent.RSLFORM.sel_ACCOUNTTYPE.options[parent.RSLFORM.sel_ACCOUNTTYPE.options.length] = new Option('<%=bankaccounttype%>', '<%=bankaccountid%>', true);
	<% end if %>	
		<%=LoaderString %>
            parent.split_sortcode();
    parent.document.getElementById("sel_COMPANYID").value = '<%=companyid %>';
    parent.RSLFORM.hid_ACTION.value = "AMEND"
    parent.checkForm();
    parent.swap_div(3);
	<% 
        Else
    Response.Redirect "../BankAccount.asp?FY=" & Request("hid_FY")
    end if 
	%>

</script>
</body>
</html>