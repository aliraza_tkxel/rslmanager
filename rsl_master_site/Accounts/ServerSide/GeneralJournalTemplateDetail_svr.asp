<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

templateId = Request.form("IACTION")

Call OpenDB()

' Load General Journal Template
SQL = " SELECT	ISNULL(CompanyId,0) CompanyId , ISNULL(SchemeId,0) SchemeId, ISNULL(BlockId,0) BlockId, ISNULL(PropertyId,'') PropertyId " &_
      " FROM	    NL_GeneralJournalTemplate NGT " &_
      " WHERE	NGT.ISACTIVE = 1 AND TemplateId = " & templateId &_
      " ORDER BY CREATEDDATE DESC "


Call OpenRs(rsTemplateLoader, SQL)
If (NOT rsTemplateLoader.EOF) Then
    companyId = rsTemplateLoader("CompanyId")
    schemeId = rsTemplateLoader("SchemeId")
    blockId = rsTemplateLoader("BlockId")
    propertyId = rsTemplateLoader("PropertyId")
End If
Call CloseRs(rsTemplateLoader)



' Load Account Template List
TableString = ""

SQL = " SELECT	ROW_NUMBER() OVER(ORDER BY GAT.TemplateId ASC) AS [Counter] " &_
		" , NA.AccountNumber " &_
		" , NA.Name " &_
		" , 0.00 AS Debit " &_
		" , 0.00 AS Credit " &_
		" , GAT.AccountId " &_
		" , ISNULL(REPLACE(GAT.Details, CHAR(13) + CHAR(10), ' ')  ,'') AS Detail " &_
		" , ISNULL(REPLACE( GAT.Reason, CHAR(13) + CHAR(10), ' '),'') AS Memo " &_
		" , ISNULL(CONVERT(VARCHAR,GAT.CostCenterId),'0') AS CostCentreId " &_
		" , ISNULL(CONVERT(VARCHAR,GAT.HeadId),'0')  AS HeadId " &_
		" , ISNULL(CONVERT(VARCHAR,GAT.ExpenditureId),'0')  AS ExpenditureId " &_
		" , GT.CompanyId  " &_
		" , 0 AS IncSC " &_
		" , 0.00 AS PropertyApportionment " &_
        " , ISNULL(Convert(varchar,IsDebit),'0') AS IsDebit " &_
" FROM	NL_GeneralJournalAccountTemplate GAT " &_
" 		INNER JOIN NL_ACCOUNT NA ON GAT.AccountId = NA.AccountId " &_
" 		INNER JOIN NL_GeneralJournalTemplate GT ON GAT.GeneralJournalTemplateId = GT.TemplateId " &_
" WHERE	GAT.GeneralJournalTemplateId = " & templateId &_
" ORDER BY Sequence asc "

Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	While (NOT rsLoader.EOF)

        TemplateString = ""
        TemplateString = "{ ""Counter"" : {Counter}, ""AccountNumber"" : ""{AccountNumber}"" , ""FullName"" : ""{FullName}"" ,""Debit"" : {Debit} ,""Credit"" : {Credit} " &_
                           ",""AccountId"" : {AccountId} ,""Detail"" : ""{Detail}"" ,""Memo"" : ""{Memo}"" ,""CostCentreId"" : {CostCentreId} ,""HeadId"" : {HeadId} " &_
                            ",""ExpenditureId"" : {ExpenditureId} ,""CompanyId"" : {CompanyId} ,""IncSC"" : {IncSC} ,""PropertyApportionment"" : {PropertyApportionment} ,""IsDebit"" : {IsDebit}  },"

        TemplateString = Replace(TemplateString, "{Counter}", rsLoader("Counter"))
        TemplateString = Replace(TemplateString, "{AccountNumber}", rsLoader("AccountNumber"))
        TemplateString = Replace(TemplateString, "{FullName}", rsLoader("Name"))
        TemplateString = Replace(TemplateString, "{Debit}", rsLoader("Debit"))
        TemplateString = Replace(TemplateString, "{Credit}", rsLoader("Credit"))
        TemplateString = Replace(TemplateString, "{AccountId}", rsLoader("AccountId"))
        TemplateString = Replace(TemplateString, "{Detail}", rsLoader("Detail"))
        TemplateString = Replace(TemplateString, "{Memo}", rsLoader("Memo"))
        TemplateString = Replace(TemplateString, "{CostCentreId}", rsLoader("CostCentreId"))
        TemplateString = Replace(TemplateString, "{HeadId}", rsLoader("HeadId"))
        TemplateString = Replace(TemplateString, "{ExpenditureId}", rsLoader("ExpenditureId"))
        TemplateString = Replace(TemplateString, "{CompanyId}", rsLoader("CompanyId"))
        TemplateString = Replace(TemplateString, "{IncSC}", rsLoader("IncSC"))
        TemplateString = Replace(TemplateString, "{PropertyApportionment}", rsLoader("PropertyApportionment"))
        TemplateString = Replace(TemplateString, "{IsDebit}", rsLoader("IsDebit"))

		TableString = TableString & TemplateString
		rsLoader.moveNext
	wend

    TableString = Left(TableString, Len(TableString) - 1)
    TableString = "[" & TableString & "]"

Else
	TableString = "[]"
End If
Call CloseRs(rsLoader)


Call CloseDB()

%>

<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>General Journal Template</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function GetGeneralJournalTemplateList()
        {
            var templateListRows = '<%=TableString%>';
            console.log(templateListRows);

            var accountList = JSON.parse(templateListRows);

            var companyIdVal = "<%=companyId%>";
            var schemeIdVal = "<%=schemeId%>";
            var blockIdVal = "<%=blockId%>";
            var propertyIdVal = "<%=propertyId%>";

            var gTemplate = {};
            gTemplate.companyId = companyIdVal;
            gTemplate.schemeId = schemeIdVal;
            gTemplate.blockId = blockIdVal;
            gTemplate.propertyId = propertyIdVal;
            gTemplate.accountTemplateRows = accountList;

            parent.LoadGeneralJournaltemplate(gTemplate);
        }

    </script>
</head>
<body onload="GetGeneralJournalTemplateList();">
</body>
</html>