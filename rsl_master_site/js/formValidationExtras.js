function DisableItems(itemList){
	arrayRefs = itemList.split(",")
	for (i=0; i<arrayRefs.length; i++){
		el = FormFields[arrayRefs[i]].split("|")
		temp = el[3].split("_")
		if (temp.length > 1) return false;		
		if (el[2] == "RADIO"){
			for (j=0; j<document.getElementsByName(el[0]).length; j++){
				document.getElementsByName(el[0])[j].checked = false
				document.getElementsByName(el[0])[j].disabled = true
				}
			}
		else if (el[2] == "CHECKBOX"){
			document.getElementById(el[0]).checked = false
			document.getElementById(el[0]).disabled = true
			}
		else {
			document.getElementById(el[0]).value = ""
			document.getElementById(el[0]).disabled = true
			}
		el[3] = "I_" + el[3]
		FormFields[arrayRefs[i]] = el.join("|")			
		}
	}

function EnableItems(itemList){
	arrayRefs = itemList.split(",")
	for (i=0; i<arrayRefs.length; i++){
		el = FormFields[arrayRefs[i]].split("|")
		temp = el[3].split("_")
		if (temp.length != 2) return false;		
		if (el[2] == "RADIO"){
			for (j=0; j<document.getElementsByName(el[0]).length; j++){
				//document.getElementsByName(el[0])[j].checked = false
				document.getElementsByName(el[0])[j].disabled = false
				}
			}
		else if (el[2] == "CHECKBOX"){
			//document.getElementById(el[0]).checked = false
			document.getElementById(el[0]).disabled = false
			}
		else {
			//document.getElementById(el[0]).value = ""
			document.getElementById(el[0]).disabled = false
			}
		el[3] = temp[1]
		FormFields[arrayRefs[i]] = el.join("|")
		}
	}
	