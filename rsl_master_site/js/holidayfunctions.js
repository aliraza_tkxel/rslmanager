
    /*
    * Date Format 1.2.3
    * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
    * MIT license
    *
    * Includes enhancements by Scott Trenda <scott.trenda.net>
    * and Kris Kowal <cixar.com/~kris.kowal/>
    *
    * Accepts a date, a mask, or a date and a mask.
    * Returns a formatted version of the given date.
    * The date defaults to the current date/time.
    * The mask defaults to dateFormat.masks.default.
    */


    var dateFormat = function () {
        var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
		    val = String(val);
		    len = len || 2;
		    while (val.length < len) val = "0" + val;
		    return val;
		};

        // Regexes and supporting functions are cached through closure
        return function (date, mask, utc) {
            var dF = dateFormat;

            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                mask = date;
                date = undefined;
            }

            // Passing date through Date applies Date.parse, if necessary
            date = date ? new Date(date) : new Date;
            if (isNaN(date)) throw SyntaxError("invalid date");

            mask = String(dF.masks[mask] || mask || dF.masks["default"]);

            // Allow setting the utc argument via the mask
            if (mask.slice(0, 4) == "UTC:") {
                mask = mask.slice(4);
                utc = true;
            }

            var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			    d: d,
			    dd: pad(d),
			    ddd: dF.i18n.dayNames[D],
			    dddd: dF.i18n.dayNames[D + 7],
			    m: m + 1,
			    mm: pad(m + 1),
			    mmm: dF.i18n.monthNames[m],
			    mmmm: dF.i18n.monthNames[m + 12],
			    yy: String(y).slice(2),
			    yyyy: y,
			    h: H % 12 || 12,
			    hh: pad(H % 12 || 12),
			    H: H,
			    HH: pad(H),
			    M: M,
			    MM: pad(M),
			    s: s,
			    ss: pad(s),
			    l: pad(L, 3),
			    L: pad(L > 99 ? Math.round(L / 10) : L),
			    t: H < 12 ? "a" : "p",
			    tt: H < 12 ? "am" : "pm",
			    T: H < 12 ? "A" : "P",
			    TT: H < 12 ? "AM" : "PM",
			    Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			    o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			    S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

            return mask.replace(token, function ($0) {
                return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
            });
        };
    } ();

    // Some common format strings
    dateFormat.masks = {
        "default": "ddd mmm dd yyyy HH:MM:ss",
        shortDate: "m/d/yy",
        shortDate2: "dd/mm/yyyy",
        mediumDate: "mmm d, yyyy",
        longDate: "mmmm d, yyyy",
        fullDate: "dddd, mmmm d, yyyy",
        shortTime: "h:MM TT",
        mediumTime: "h:MM:ss TT",
        longTime: "h:MM:ss TT Z",
        isoDate: "yyyy-mm-dd",
        isoTime: "HH:MM:ss",
        isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };

    // Internationalization strings
    dateFormat.i18n = {
        dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
        monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
    };

    // For convenience...
    Date.prototype.format = function (mask, utc) {
        return dateFormat(this, mask, utc);
    };

    function JSlong_date(str_date) {
        var arrMon = new Array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var date_array = str_date.split("/");
        return new String(Math.abs(date_array[0]) + " " + arrMon[parseInt(date_array[1], 10)] + " " + date_array[2])
    }

    // RSL Code
    function bhDays() {
        //alert('bhDays')
        var bhSplit, cnt, testBHDate, high, testDate, bhCount, holStart, holEnd, tmpStartDate, totdate, i

        holStart = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));

        if (document.getElementById("radMulti").checked == true)
        {
            holEnd = new Date(Date.parse(JSlong_date(document.getElementById("txt_RETURNDATE").value)));
        }
        else
        {
            holEnd = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
        }

        var bhDates = document.getElementById("dates").value
        var bhSplit = new Array();
            bhSplit = bhDates.split(',');

        var cnt = 1
        var bhCount = 0
        i = 0

        if (document.getElementById("radMulti").checked == true) {

            var tmpStartDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
            var tmpEndDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_RETURNDATE").value)));
            var totdate = (Math.abs(holStart.getTime() - holEnd.getTime()) / 86400000)+ 2;
            
            console.log("totdate" + totdate)
            while (cnt < totdate) {
              
                //console.log("loop")
                while (i <= bhSplit.length - 1) {
                    //cccc = Math.abs(new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime() - new Date(tmpStartDate.getTime())) / 86400000
                    cccc = new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime() - tmpStartDate.getTime();
                    //console.log("Date 1 - " + new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime())
                    //console.log("Date 2 - " + new Date(tmpStartDate.getTime()))
                    //if (bhSplit[i] = "28/03/2016") {
                    //   console.log("bhSplit" + bhSplit[i])
                    //  console.log("bhSplit" + new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime())
                    // }
                    var leaveDate = new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime();
                    if (leaveDate >= tmpStartDate.getTime() && leaveDate <= tmpEndDate.getTime()) {
                        bhCount = bhCount + 1;
                        console.log("bhSplit = " + bhSplit[i])
                        console.log("leaveDate = " + leaveDate)
                        console.log("tmpStartDate = " + tmpStartDate.getTime());
                        console.log("tmpEndDate = " + tmpEndDate.getTime());
                    }

                    if (cccc == 0) {
                        //    bhCount = bhCount + 1;
                        //console.log("bhCount" + bhCount)
                        //console.log("BH" + bhSplit[i])
                    }
                    i = i + 1
                }
                //i = 0
                //tmpStartDate = new Date(tmpStartDate.getTime() + 1);
                //tmpStartDate = new Date(tmpStartDate.getTime() + (24 * 60 * 60 * 1000));
                cnt = cnt + 1

            }
            
        }
        else {
            var pattern
            var dts = new String(document.getElementById("dates").value)
                pattern = /\s*,\s*/;
            var dtsList = dts.split(pattern);
                pattern = /(\w+)\s+(\w+)/;
                byDateList = new Array;
                for (i = 0; i < dtsList.length; i++) {
                    byDateList[i] = dtsList[i].replace(pattern, "$2, $1")
                }
                for (i = 0; i < byDateList.length; i++) {
                    if (byDateList[i] == document.getElementById("txt_STARTDATE").value) {
                        bhCount = -1;
                    }
                }
            }
            console.log("bhCount return = " + bhCount)
        return bhCount
    }


    function TodaysDateFormatted() 
    {
    var months = new Array(12);
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";

    var now = new Date();
    var monthnumber = now.getMonth();
    var monthname = months[monthnumber];
    var monthday = now.getDate();
    var year = now.getYear();
    if (year < 2000) { year = year + 1900; }
    var TodaysDate = monthday + '/' + (monthnumber + 1) + '/' + year;
    TodaysDate = new Date(Date.parse(JSlong_date(TodaysDate)));
    return TodaysDate
    }


    function bhSickDays() {

        var bhSplit, cnt, testBHDate, high, testDate, bhCount, holStart, holEnd, tmpStartDate, totdate, i

        holStart = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));

        if (document.getElementById("txt_RETURNDATE").value == "") {
            holEnd = TodaysDateFormatted();
        }
        else {
            holEnd = new Date(Date.parse(JSlong_date(document.getElementById("txt_RETURNDATE").value)));
        }

        var bhDates = document.getElementById("dates").value
        var bhSplit = new Array();
        bhSplit = bhDates.split(',');

        var cnt = 1
        var bhCount = 0;
        i = 0

        var tmpStartDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
        var totdate = (Math.abs(holStart.getTime() - holEnd.getTime()) / 86400000) + 1;

        while (cnt <= totdate) {
                while (i <= bhSplit.length - 1) {
                    cccc = new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime() - tmpStartDate.getTime();
                    if (cccc == 0) {
                        bhCount = bhCount + 1;
                    }
                    i = i + 1
                }
                i = 0;
                tmpStartDate = new Date(tmpStartDate.getTime() + (24 * 60 * 60 * 1000));
                cnt = cnt + 1
            }
            return bhCount
    }


    function workDays() {

        getType()
        var BegDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
        var EndDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_RETURNDATE").value)));
        bhCountdays = bhDays()

        var non_work_days = document.getElementById("hid_non_work_days").value
        // if multi day is picked
        if (document.getElementById("radMulti").checked == true) {

            WholeWeeks = parseInt((EndDate - BegDate) / (1000 * 60 * 60 * 24 * 7).toFixed(2));

            var NoOfDays = WholeWeeks * 7
            var DateCnt = new Date(BegDate)
            DateCnt.setDate(DateCnt.getDate() + NoOfDays)
            EndDays = 0
            var loopDate = new Date();
            loopDate.setTime(DateCnt.valueOf());

            while (loopDate.valueOf() < EndDate.valueOf() + 86400000) {

                // if not Sunday/Saturday
                // Javascript Sunday is 0, Monday is 1, Saturday is 6 and so on.
                // vb Script Sunday is 1, Monday is 2, Saturday is 7 and so on.
                if ((loopDate.getDay() != "0") && (loopDate.getDay() != "6")) {
                    EndDays = EndDays + 1
                }

                loopDate.setTime(loopDate.valueOf() + 86400000);
            }

            var jsWorkDays = (WholeWeeks * 5 + EndDays);
            //console.log("jsWorkDays" + jsWorkDays)
            // remove half days from total
            if ((document.getElementById("radStartAM").checked == true) || (document.getElementById("radStartPM").checked == true)) {
                jsWorkDays = jsWorkDays - 0.5;
            }
            if ((document.getElementById("radEndAM").checked == true) || (document.getElementById("radEndPM").checked == true)) {
                jsWorkDays = jsWorkDays - 0.5;
            }
            if (jsWorkDays < 0.5) {
                document.getElementById("txt_DURATION").style.color = "red"
                document.getElementById("txt_DURATION").value = "Invalid Date Range"
                document.getElementById("btn_submit").disabled = true;
            }
            else {
                document.getElementById("txt_DURATION").style.color = "black"
                jsWorkDays = jsWorkDays - bhCountdays - non_work_days
                document.getElementById("txt_DURATION").value = jsWorkDays
                document.getElementById("btn_submit").disabled = false;
            }
            //console.log("jsWorkDays" + jsWorkDays)
            //console.log("bhCountdays" + bhCountdays)
        }
        else {
            // if single day is chosen
            if ((document.getElementById("radStartAM").checked == true) || (document.getElementById("radStartPM").checked == true)) {
                jsWorkDays = 0.5;
            }
            else {
                jsWorkDays = 1
            }
            EndDate = BegDate
            if ((BegDate.getDay() == "0") || (BegDate.getDay() == "6")) {
                document.getElementById("txt_DURATION").style.color = "red"
                document.getElementById("txt_DURATION").value = "Weekend"
                document.getElementById("btn_submit").disabled = true
                return false
            }
            if (bhCountdays >= 0) {
                document.getElementById("txt_DURATION").style.color = "black"
                jsWorkDays = jsWorkDays - bhCountdays - non_work_days
                document.getElementById("txt_DURATION").value = jsWorkDays
                document.getElementById("btn_submit").disabled = false
            }
            else {
                document.getElementById("txt_DURATION").style.color = "red"
                document.getElementById("txt_DURATION").value = "Bank Holiday"
                document.getElementById("btn_submit").disabled = true
            }
        }
    }


    function sicknessDays() {
        //GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
        document.getElementById("btn_submit").disabled = false;

        var holType
        if (document.getElementById("radStartFull").checked == true) {
            holType = "F";
        }
        else if (document.getElementById("radStartAM").checked == true) {
            holType = "M";
        }
        else {
            holType = "A";
        }
        document.getElementById("holtype").value = holType;
        var BegDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
        var months = new Array(12);
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";

        var now = new Date();
        var monthnumber = now.getMonth();
        var monthname = months[monthnumber];
        var monthday = now.getDate();
        var year = now.getYear();
        if (year < 2000) { year = year + 1900; }
        var TodaysDate = monthday + '/' + (monthnumber + 1) + '/' + year;
        TodaysDate = new Date(Date.parse(JSlong_date(TodaysDate)));
        var totdays = Math.abs(BegDate.getTime() - TodaysDate.getTime()) / 86400000

        bhCountdays = bhSickDays();

        var non_work_days = document.getElementById("hid_non_work_days").value;
        if (document.getElementById("hid_pagename").value == "pSickLeave") {
            if (document.getElementById("txt_RETURNDATE").value == "") {
                EndDate = TodaysDate;
            }
            else {
                EndDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_RETURNDATE").value)));
            }
        }
        else {
            EndDate = TodaysDate
        }
        // Javascript will not give a whole number but a float/decimal value.h
        // Need to parse the value as a Integer and get a whole number.
        // Reason : To ensure the same value is returned as was done in vb script.
        WholeWeeks = parseInt((EndDate - BegDate) / (1000 * 60 * 60 * 24 * 7).toFixed(2));
        // we are adding days by multiplying the number of weeks by 7
        var NoOfDays = WholeWeeks * 7
        var DateCnt = new Date(BegDate)
        DateCnt.setDate(DateCnt.getDate() + NoOfDays)
        EndDays = 0
        var loopDate = new Date();
        loopDate.setTime(DateCnt.valueOf());
        while (loopDate.valueOf() < EndDate.valueOf() + 86400000) {

            // if not Sunday/Saturday
            // Javascript Sunday is 0, Monday is 1, Saturday is 6 and so on.
            // vb Script Sunday is 1, Monday is 2, Saturday is 7 and so on.
            if ((loopDate.getDay() != "0") && (loopDate.getDay() != "6")) {
                EndDays = EndDays + 1
            }

            loopDate.setTime(loopDate.valueOf() + 86400000);
        }

        JSsicknessDays = (WholeWeeks * 5 + EndDays);

        if (totdays < 0) {
            document.getElementById("txt_DURATION").style.color = "red"
            document.getElementById("txt_DURATION").value = "Invalid Date"
            document.getElementById("btn_submit").disabled = true;
            return false;
        }
        // if single day is chosen
        if (totdays = 0) {
            if ((document.getElementById("radStartAM").checked == true) || (document.getElementById("radStartPM").checked == true)) {
                JSsicknessDays = 0.5
            }
            else {
                JSsicknessDays = 1
            }
            EndDate = BegDate
        }
        else {
            if ((document.getElementById("radStartAM").checked == true) || (document.getElementById("radStartPM").checked == true)) {
                JSsicknessDays = JSsicknessDays - 0.5
            }
            if (document.getElementById("hid_pagename").value == "pSickLeave") {
                if ((document.getElementById("radEndAM").checked == true) || (document.getElementById("radEndPM").checked == true)) {
                    JSsicknessDays = JSsicknessDays - 0.5
                }
            }
        }
        // if Saturday or Sunday
        if ((BegDate.getDay() == "0") || (BegDate.getDay() == "6")) {
            document.getElementById("txt_DURATION").style.color = "red"
            document.getElementById("txt_DURATION").value = "Weekend"
            document.getElementById("btn_submit").disabled = true;
            return false;
        }
        //weird
        document.getElementById("hid_pagename").value = document.getElementById("hid_pagename").value

        if (bhCountdays >= 0) {
            document.getElementById("txt_DURATION").style.color = "black"
            JSsicknessDays = JSsicknessDays - bhCountdays - non_work_days
            document.getElementById("txt_DURATION").value = JSsicknessDays
            if (JSsicknessDays > 0) {
                document.getElementById("btn_submit").disabled = false;
            }
            else if (JSsicknessDays = 0) {
                document.getElementById("txt_DURATION").style.color = "red"
                document.getElementById("txt_DURATION").value = non_work_days
                document.getElementById("btn_submit").disabled = true;
            }
            else {
                document.getElementById("txt_DURATION").style.color = "red"
                document.getElementById("txt_DURATION").value = "Invalid Date"
                document.getElementById("btn_submit").disabled = true;
            }
        }
    }

function sicknessDaysCount() {
    //GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
    document.getElementById("btn_submit").disabled = false;

    var holType, hr;
    if (document.getElementById("radStartFull").checked == true) {
        holType = "F";
    }
    else if (document.getElementById("radStartAM").checked == true) {
        holType = "M";
    }
    else {
        holType = "A";
    }
    document.getElementById("holtype").value = holType;
    var BegDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
    var months = new Array(12);
    months[0] = "January";
    months[1] = "February";
    months[2] = "March";
    months[3] = "April";
    months[4] = "May";
    months[5] = "June";
    months[6] = "July";
    months[7] = "August";
    months[8] = "September";
    months[9] = "October";
    months[10] = "November";
    months[11] = "December";

    var now = new Date();
    var monthnumber = now.getMonth();
    var monthname = months[monthnumber];
    var monthday = now.getDate();
    var year = now.getYear();
    if (year < 2000) { year = year + 1900; }
    var TodaysDate = monthday + '/' + (monthnumber + 1) + '/' + year;
    TodaysDate = new Date(Date.parse(JSlong_date(TodaysDate)));
    var totdays = Math.abs(BegDate.getTime() - TodaysDate.getTime()) / 86400000

    bhCountdays = bhSickDaysCount();

    var non_work_days = document.getElementById("hid_non_work_days").value;
    if (document.getElementById("hid_pagename").value == "iSick") {
        var edate = document.getElementById("txt_RETURNDATE").value;
        var sdate = document.getElementById("txt_STARTDATE").value;
        if (edate < sdate) {
            EndDate = new Date(sdate);
        }

        else {
            EndDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_RETURNDATE").value)));
        }
    }
    else {
        EndDate = TodaysDate
    }
    // Javascript will not give a whole number but a float/decimal value.h
    // Need to parse the value as a Integer and get a whole number.
    // Reason : To ensure the same value is returned as was done in vb script.
    WholeWeeks = parseInt((EndDate - BegDate) / (1000 * 60 * 60 * 24 * 7).toFixed(2));
    // we are adding days by multiplying the number of weeks by 7
    var NoOfDays = WholeWeeks * 7
    var DateCnt = new Date(BegDate)
    DateCnt.setDate(DateCnt.getDate() + NoOfDays)
    EndDays = 0
    var loopDate = new Date();
    loopDate.setTime(DateCnt.valueOf());
    while (loopDate.valueOf() < EndDate.valueOf() + 86400000) {

        // if not Sunday/Saturday
        // Javascript Sunday is 0, Monday is 1, Saturday is 6 and so on.
        // vb Script Sunday is 1, Monday is 2, Saturday is 7 and so on.
        if ((loopDate.getDay() != "0") && (loopDate.getDay() != "6")) {
            EndDays = EndDays + 1
        }

        loopDate.setTime(loopDate.valueOf() + 86400000);
    }

    JSsicknessDays = (WholeWeeks * 5 + EndDays);

    if (totdays < 0) {
        document.getElementById("txt_DURATION").style.color = "red"
        document.getElementById("txt_DURATION").value = "Invalid Date"
        document.getElementById("btn_submit").disabled = true;
        return false;
    }
    // if single day is chosen
    hr = 7.5;
    if (totdays = 0) {
        if ((document.getElementById("radStartAM").checked == true) || (document.getElementById("radStartPM").checked == true)) {
            JSsicknessDays = 0.5;
           // hr = 3.75;
        }
        else {
            JSsicknessDays = 1
        }
        EndDate = BegDate
    }
    else {
        if ((document.getElementById("radStartAM").checked == true) || (document.getElementById("radStartPM").checked == true)) {
            JSsicknessDays = JSsicknessDays - 0.5;
           // hr = 3.75;
        }
        if (document.getElementById("hid_pagename").value == "iSick") {
            if ((document.getElementById("radEndAM").checked == true) || (document.getElementById("radEndPM").checked == true)) {
                JSsicknessDays = JSsicknessDays - 0.5
                //hr = 3.75;
            }
        }
    }
    // if Saturday or Sunday
    if ((BegDate.getDay() == "0") || (BegDate.getDay() == "6")) {
        document.getElementById("txt_DURATION").style.color = "red"
        document.getElementById("txt_DURATION").value = "Weekend"
        document.getElementById("btn_submit").disabled = true;
        return false;
    }
    //weird
    document.getElementById("hid_pagename").value = document.getElementById("hid_pagename").value

    if (bhCountdays >= 0) {
        document.getElementById("txt_DURATION").style.color = "black"
        JSsicknessDays = JSsicknessDays - bhCountdays;
        document.getElementById("txt_DURATION").value = JSsicknessDays
        document.getElementById("txt_DURATIONHours").value = JSsicknessDays * hr
        if (JSsicknessDays > 0) {
            document.getElementById("btn_submit").disabled = false;
        }
        else if (JSsicknessDays == 0) {
            document.getElementById("txt_DURATION").style.color = "red"
            document.getElementById("txt_DURATIONHours").style.color = "red"
            document.getElementById("txt_DURATION").value = non_work_days
            document.getElementById("txt_DURATIONHours").value = non_work_days * hr
            document.getElementById("btn_submit").disabled = true;
        }
        else {
            document.getElementById("txt_DURATION").style.color = "red"
            document.getElementById("txt_DURATION").value = "Invalid Date"
            document.getElementById("btn_submit").disabled = true;
        }
    }
}
function getType() {

        var holType = "";
        // if multi day
        if (document.getElementById("radMulti").checked == true) {
            if (document.getElementById("radStartFull").checked == true) {
                holType = "F";
            }
            else if (document.getElementById("radStartAM").checked == true) {
                holType = "M";
            }
            else {
                holType = "A";
            }
            if (document.getElementById("radEndFull").checked == true) {
                holType = holType + "-F";
            }
            else if (document.getElementById("radEndAM").checked == true) {
                holType = holType + "-M";
            }
            else {
                holType = holType + "-A";
            }
        }
        else {
            if (document.getElementById("radStartFull").checked == true) {
                holType = "F";
            }
            else if (document.getElementById("radStartAM").checked == true) {
                holType = "M";
            }
            else {
                holType = "A";
            }
        }
        document.getElementById("holtype").value = holType
    }


    function getSicknessType() {

		var holType = "";
		
        if (document.getElementById("radStartFull").checked == true)
        {
            holType = "F";
        }
        else if (document.getElementById("radStartAM").checked == true)
        {
                holType = "M";
        }
        else
        {
                holType = "A";
        }


        if (document.getElementById("hid_pagename").value == "pSickLeave")
        {
            if (document.getElementById("radStartFull").checked == true)
            {
                    holType = holType + "-F";
            }
            else if (document.getElementById("radStartAM").checked == true)
            {
                    holType = holType + "-M";
            }
            else
            {
                    holType = holType + "-A";
            }
        }
		document.getElementById("holtype").value = holType
	}
function bhSickDaysCount() {

    var bhSplit, cnt, testBHDate, high, testDate, bhCount, holStart, holEnd, tmpStartDate, totdate, i

    holStart = new Date(document.getElementById("txt_STARTDATE").value);

    if (document.getElementById("txt_RETURNDATE").value == "") {
        holEnd = TodaysDateFormatted();
    }
    else {
        holEnd = new Date(document.getElementById("txt_RETURNDATE").value);
    }

    var bhDates = document.getElementById("dates").value
    var bhSplit = new Array();
    bhSplit = bhDates.split(',');

    var cnt = 1
    var bhCount = 0;
    i = 0

    var tmpStartDate = new Date(document.getElementById("txt_STARTDATE").value);
    var totdate = (Math.abs(holStart.getTime() - holEnd.getTime()) / 86400000) + 1;

    while (cnt <= totdate) {
        while (i <= bhSplit.length - 1) {
            cccc = new Date(Date.parse(JSlong_date(bhSplit[i]))).getTime() - tmpStartDate.getTime();
            if (cccc == 0) {
                bhCount = bhCount + 1;
            }
            i = i + 1
        }
        i = 0;
        tmpStartDate = new Date(tmpStartDate.getTime() + (24 * 60 * 60 * 1000));
        cnt = cnt + 1
    }
    return bhCount
}
