	function FormatCurrencyComma(Figure){
		NewFigure = FormatCurrency(Math.abs(Figure))
		NegativeCurrency = false
		if (parseFloat(Figure,10) < 0) NegativeCurrency = true
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		if (NegativeCurrency) NewFigure = "-" + NewFigure.toString()
		return NewFigure
	}

	function TotalBoth(){

		event.srcElement.style.textAlign = "right"
		SetChecking(1)		
		if (!checkForm(true)) {
			document.getElementById("txt_GROSSCOST").value = ""
			return false
			}
		document.getElementById("txt_GROSSCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))
		}

	function SetVat(){
		if (document.getElementById("sel_VATTYPE").value == 0 || document.getElementById("sel_VATTYPE").value == 3){
			document.getElementById("txt_VAT").value = "0.00"
			document.getElementById("txt_VAT").readOnly = true
			}
		else if (document.getElementById("sel_VATTYPE").value == 1){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(20)
			}
		else if (document.getElementById("sel_VATTYPE").value == 2){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(5)
			}			
		document.getElementById("txt_GROSSCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
		}

	function AddVAT(val){
		if (isNumeric("txt_NETCOST", "")){
			//VAT = FormatCurrency(parseFloat(document.getElementById("txt_NETCOST").value) /100 * val)
			VAT = new Number (document.getElementById("txt_NETCOST").value /100 * val)
			VAT = round(round(VAT,4),2)
			document.getElementById("txt_VAT").value = FormatCurrency(VAT)
			document.getElementById("txt_GROSSCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
			}
		}

	function round(number,X) {
	// rounds number to X decimal places, defaults to 2
		X = (!X ? 2 : X);
		return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
	}

	function ResetVAT(){
		if (document.getElementById("sel_VATTYPE").value == 1) AddVAT(20)
		if (document.getElementById("sel_VATTYPE").value == 2) AddVAT(5)		
		}		
