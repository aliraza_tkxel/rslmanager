$(function () {

    $("table").delegate('td.RollOver', 'mouseover mouseleave', function (e) {
        if (e.type == 'mouseover') {
            $(this).parent().addClass("hover");
        } else {
            $(this).parent().removeClass("hover");
        }

    });

});
