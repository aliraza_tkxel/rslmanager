<!-- #include virtual="Connections/db_connection.asp" -->
<%
	'CONST BANK_HOLIDAY_STRING = "09/04/2004,12/04/2004,31/05/2004,30/08/2004,27/12/2004,28/12/2004,03/01/2005,25/03/2005,28/03/2005,02/05/2005,30/05/2005,29/08/2005,26/12/2005,27/12/2005,02/01/2006,14/04/2006,17/04/2006,01/05/2006,29/05/2006,28/08/2006,25/12/2006,26/12/2006,01/01/2007,06/04/2007,09/04/2007,07/05/2007,28/05/2007,27/08/2007,25/12/2007,26/12/2007"
	employee_id = Request("employeeid")
	
	Dim BANK_HOLIDAY_STRING,noworkday, HRule
	//returns number of bank holidays in years
	Function BankHoliday()
	    
	    dim Bank_Holidays,count
	    
	    Set Conn = Server.CreateObject("ADODB.Connection")
	    Conn.ConnectionTimeout = 180
	    Conn.Open RSL_CONNECTION_STRING 
	    If employee_id <>"" then
	        Set rsSet = Server.CreateObject("ADODB.Recordset")
    	    
	        strSQL = "SELECT ISNULL(HOLIDAYRULE,0) AS HRULE FROM E_JOBDETAILS WHERE EMPLOYEEID=" & employee_id
    	   
            set rsSet = Conn.Execute(strSQL)
            if Not rsSet.EOF then
                HRule = rsSet("HRULE")
            end if
            rsSet.Close()
         end if   
        
        if (HRule = 6 or HRule = 7 or HRule = 8 or HRule = 9) then
            strSQL = "SELECT convert(varchar,BHDATE,103) as BHDATE FROM G_BANKHOLIDAYS WHERE MeridianEast=1"
        else
            strSQL = "SELECT convert(varchar,BHDATE,103) as BHDATE FROM G_BANKHOLIDAYS WHERE BHA=1"
        end if

	    set rsSet = Conn.Execute(strSQL)
	
	    count=1
	
	    While Not rsSet.EOF
	        if(count=1) then
	            Bank_Holidays=Bank_Holidays & rsSet("BHDATE")
	            count=count+1
	        else
	            Bank_Holidays=Bank_Holidays & "," & rsSet("BHDATE")
	        end if
	        rsSet.movenext()
	    Wend
	    rsSet.Close()
	    
	    BankHoliday=Bank_Holidays
	End Function
	
	BANK_HOLIDAY_STRING=BankHoliday()
	
	'Response.Write BANK_HOLIDAY_STRING
	
	
	
	// calculates number of work days between two dates and also remove bank holidays - the LAST DATE IS NOT INCLUSIVE(PP)
	Function workDays(date1, date2)     
	
   		
		//bhCountdays = bhDays()
		Dim startdate, enddate, enddays, arr_bh
		
		startdate 	= DateValue(date1)
		enddate		= DateValue(date2)			
		arr_bh = Split(BANK_HOLIDAY_STRING, ",")
			
		enddays = 0
		Do While startdate <= enddate
			If weekday(startdate) <> "1" And weekday(startdate) <> "7" Then
				enddays = enddays + 1
				// check for bank holidays
				For each bh in arr_bh
					If ( startdate = DateValue(bh) ) And ( weekday(DateValue(bh)) <> "1" And weekday(DateValue(bh)) <> "7") Then
						endays = enddays - 1
						Exit For
					End If						
				Next
			End If
			startdate = DateAdd("d", 1, startdate)
		Loop
		If weekday(enddate) <> "1" And weekday(enddate) <> "7" Then
			WorkDays = enddays - 1
		Else
			WorkDays = enddays
		End If
		'WorkDays=1000		
	End Function
    
    'noworkday=workDays("24/08/2007", "31/08/2007")
    
    'Response.Write("<br/>" & noworkday)

%>