<%
	CONST BANK_HOLIDAY_STRING = "09/04/2004,12/04/2004,31/05/2004,30/08/2004,27/12/2004,28/12/2004,03/01/2005,25/03/2005,28/03/2005,02/05/2005,30/05/2005,29/08/2005,26/12/2005,27/12/2005,02/01/2006,14/04/2006,17/04/2006,01/05/2006,29/05/2006,28/08/2006,25/12/2006,26/12/2006"
	
	// calculates number of work days between two dates and also remove bank holidays - the LAST DATE IS NOT INCLUSIVE(PP)
	Function workDays(date1, date2)     
   		
		//bhCountdays = bhDays()
		Dim startdate, enddate, enddays, arr_bh
		
		startdate 	= DateValue(date1)
		enddate		= DateValue(date2)			
		arr_bh = Split(BANK_HOLIDAY_STRING, ",")
			
		enddays = 0
		Do While startdate <= enddate
			If weekday(startdate) <> "1" And weekday(startdate) <> "7" Then
				enddays = enddays + 1
				// check for bank holidays
				For each bh in arr_bh
					If ( startdate = DateValue(bh) ) And ( weekday(DateValue(bh)) <> "1" And weekday(DateValue(bh)) <> "7") Then
						endays = enddays - 1
						Exit For
					End If						
				Next
			End If
			startdate = DateAdd("d", 1, startdate)
		Loop
		If weekday(enddate) <> "1" And weekday(enddate) <> "7" Then
			WorkDays = enddays - 1
		Else
			WorkDays = enddays
		End If
				
	End Function


%>