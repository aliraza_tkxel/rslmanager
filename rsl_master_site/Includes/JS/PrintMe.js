<object id="WBControl" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object> 
<script language="VBScript"><!--  
/////////////
// PAGE NOTE : - 
// These function allow an icon named PrinterIcon to be pressed and prints out the page
// This technique uses active X controls to print and if the users machine does not support this
// it will prompt to right click the screen.

//	* IMPORTANT NOTE * Place this include file just below the start of the body tag
/////////////

 Sub VBPrint() On Error Resume next  
	 WBControl.ExecWB 6,1  
 End Sub  


 //-->
</script>  

<script language=JavaScript><!--   
 
 function doPrint(){  

	 if(self.print){
	   
	   	 document.PrinterIcon.style.display = "none";
		 self.print();  
		 self.close();  
		 return False;  
 		}  
		
	 else if (navigator.appName.indexOf(' Microsoft') != -1){  

		 VBPrint();  
		 self.close();  
		 }  
 	else{  
	 alert("To print this document, you will need to\nclick the Right Mouse Button and select\n' Print'");  
 	}  
 }  
//-->  
 </script>  
