<%
Sub LogActiveUser
	If (Session("USERID") <> "") Then
		'Application("ActiveUsers") = 0
        'Application("ActiveUserList") = ""
		'Application("ActiveUserInformation") = ""
		'rw Application("ActiveUserInformation") & "<BR>"
		'rw Session.SessionID
		Dim strActiveUserList
		Dim intUserStart, intUserEnd
		Dim strUser
		Dim strDate
		Dim	strUserInformation

		strActiveUserList = Application("ActiveUserList")

        If Instr(1, strActiveUserList, Session.SessionID) > 0 Then
            Application.Lock
            intUserStart = Instr(1, strActiveUserList, Session.SessionID)
            intUserEnd = Instr(intUserStart, strActiveUserList, "|||")
            strUser = Mid(strActiveUserList, intUserStart, intUserEnd - intUserStart)
            strActiveUserList = Replace(strActiveUserList, strUser, Session.SessionID & ":" & Now())
            Application("ActiveUserList") = strActiveUserList
            Application.UnLock
        Else
            Application.Lock
            Application("ActiveUsers") = CInt(Application("ActiveUsers")) + 1
            Application("ActiveUserList") = Application("ActiveUserList") & Session.SessionID & ":" & Now() & "|||"
			Application("ActiveUserInformation") = Application("ActiveUserInformation") & Session.SessionID & "::" & Session("USERID") & "::" & Request.ServerVariables("REMOTE_ADDR") & "|||"
            Application.UnLock
			
			'LOG THE LOGIN TO THE DATABASE HERE.
			Set LogConnection = Server.CreateObject("ADODB.Connection")
			LogConnection.Open RSL_CONNECTION_STRING 
			SQL_LOGIN = "INSERT INTO LOGIN_HISTORY (SESSIONID, [USER], TEAM, EMPLOYEEID, HTTPINFO, REMOTE_ADDR, REMOTE_HOST, TEAMCODE) VALUES " &_
				"(" & Session.SessionID & ", '" & Replace(SESSION("FIRSTNAME"),"'","''") & " " & Replace(SESSION("LastName"),"'","''") & "', '" & Replace(SESSION("TEAMNAME"),"'","''") & "', " & SESSION("USERID") & ", '" & Request.ServerVariables("ALL_HTTP") & "', '" & Request.ServerVariables("REMOTE_ADDR") & "', '" & Request.ServerVariables("REMOTE_HOST") & "', '" & Session("TeamCode") & "')" 
			LogConnection.Execute SQL_LOGIN				
			LogConnection.Close
			Set LogConnection = Nothing
        End If
	End If
End Sub
 
Sub ActiveUserCleanup

	Dim ix
	Dim intUsers
	Dim strActiveUserList
	Dim aActiveUsers
	Dim intActiveUserCleanupTime
	Dim intActiveUserTimeout
	Dim strActiveUserInformation
	Dim aActiveInformation
	
	intActiveUserCleanupTime = 15 'In minutes, how often should the ActiveUserList be cleaned up.
	intActiveUserTimeout = 30 'In minutes, how long before a User is considered Inactive and is deleted from ActiveUserList
	
	If Application("ActiveUserList") = "" Then Exit Sub
	
	If DateDiff("n", Application("ActiveUsersLastCleanup"), Now()) > intActiveUserCleanupTime Then
	
		Application.Lock
		Application("ActiveUsersLastCleanup") = Now()
		Application.Unlock
	
		intUsers = 0
		strActiveUserList = Application("ActiveUserList")
		strActiveUserList = Left(strActiveUserList, Len(strActiveUserList) - 3)

		aActiveUsers = Split(strActiveUserList, "|||")
		
		strActiveUserInformation = Application("ActiveUserInformation")
		strActiveUserInformation = Left(strActiveUserInformation, Len(strActiveUserInformation) - 3)
	
		aActiveInformation = Split(strActiveUserInformation, "|||")
		SQL_LOGOUT = ""
		
		For ix = 0 To UBound(aActiveUsers)
			If DateDiff("n", Mid(aActiveUsers(ix), Instr(1, aActiveUsers(ix), ":") + 1, Len(aActiveUsers(ix))), Now()) > intActiveUserTimeout Then
				SQL_LOGOUT = SQL_LOGOUT & "UPDATE LOGIN_HISTORY SET LOGOUTTIME = GETDATE(), LASTPAGEACCESS = '" & FormatDateTime(Mid(aActiveUsers(ix), Instr(1, aActiveUsers(ix), ":") + 1, Len(aActiveUsers(ix))),0)& "', LOGOUTTYPE = 2 WHERE SESSIONID = " & Left(aActiveUsers(ix), Instr(1, aActiveUsers(ix), ":")-1 ) & " AND LOGOUTTIME IS NULL;"
				aActiveUsers(ix) = "XXXX"
				aActiveInformation(ix) = "XXXX"
			Else
				intUsers = intUsers + 1
			End If 
		Next

		if (SQL_LOGOUT <> "") Then
			Set LogConnection = Server.CreateObject("ADODB.Connection")
			LogConnection.Open RSL_CONNECTION_STRING 
			LogConnection.Execute SQL_LOGOUT				
			LogConnection.Close
			Set LogConnection = Nothing
		end if
				
		strActiveUserList = Join(aActiveUsers, "|||") & "|||"
		strActiveUserList = Replace(strActiveUserList, "XXXX|||", "")

		strActiveUserInformation = Join(aActiveInformation, "|||") & "|||"
		strActiveUserInformation = Replace(strActiveUserInformation, "XXXX|||", "")
	
		Application.Lock
		Application("ActiveUserList") = strActiveUserList
		Application("ActiveUserInformation") = strActiveUserInformation		
		Application("ActiveUsers") = intUsers
		Application.UnLock
	
	End If

End Sub
%>

