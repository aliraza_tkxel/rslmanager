<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim theURL
	Dim str_data, count, my_page_size
	Dim team_name
	Dim TheFullTable
	Dim ForCounter
	Dim MaxRowSpan
	Dim SubQueryValue

	Function Create_Table()
		Dim strSQL, rsSet, intRecord 

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
		'gets the array counter used for looping
		ForCounter = UBound(ColData)

		'gets the max rowspan for use later on
		MaxRowSpan = ForCounter + 1

		'builds the appropriate row code to be evaluated later
		if (RowClickcolumn = "") then
			RowCodeToEvaluate = " ""<TR STYLE='CURSOR:HAND'>"" "
		else
			RowCodeToEvaluate = " ""<TR STYLE='CURSOR:HAND' "" & Eval(RowClickColumn) & "">"" "
		end if
				
		'Prepares the TD Code and stores them in the TDPrepared array
		for i=0 to ForCounter
			TempArray = Split(Coldata(i), "|")
			TableTitles    (i) = TempArray(0)
			DatabaseFields (i) = TempArray(1)
			TDFront = ""
			TDEnd = ""
			if (TDFunc(i) <> "") then
				TDFunctionArray = Split(TDFunc(i), "|")
				TDFront = TDFunctionArray(0)
				TDEnd = TDFunctionArray(1)
			end if
			ColumnWidths   (i) = TempArray(2)
			if (TableTitles(i) = "TEXT") then
				if (TDSTUFF(i) = "") then
					TDPrepared (i) = " ""<TD>"" & " & TDFront & "DatabaseFields(j)" & TDEnd & " & ""</TD>"" "
				else
					TDPrepared (i) = " ""<TD "" & Eval(TDSTUFF(j)) & "" >"" & " & TDFront & "DatabaseFields(j)" & TDEnd & " & ""</TD>"" "
				end if				
			else
				if (TDSTUFF(i) = "") then
					TDPrepared (i) = " ""<TD>"" & " & TDFront & "rsSet(DatabaseFields(j))" & TDEnd & " & ""</TD>"" "
				else								
					TDPrepared (i) = " ""<TD "" & Eval(TDSTUFF(j)) & "" >"" & " & TDFront & "rsSet(DatabaseFields(j))" & TDEnd & " & ""</TD>"" "
				end if
			end if
		next
		
		intRecord = 0
		str_data = ""
		'Set the sql code
		strSQL = SQLCODE

		//response.write strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		if SubQuery <> "" then
			SubQueryValue = rsSet(SubQuery)
		end if

		str_data = str_data & "<TBODY CLASS=""CAPS"">" 
	
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

				str_data = str_data & Eval(RowCodeToEvaluate)
				'loop through the tds and build a single row
				for j=0 to ForCounter
					str_data = str_data & Eval(TDPrepared(j))
				next
				str_data = str_data & "</TR>" 
					
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next

			str_data = str_data & "</TBODY>" 
		End If
			
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = str_data & "<THEAD><TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>" & EmptyText & "</TD></TR>" &_
						"</THEAD>"
		End If

		'calls the top builder
		Top_Data = Build_TableTop()

		'ensure table height is consistent with any amount of records
		Call fill_gaps()

		'aDD rECORD pAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)
		' links
		str_data = str_data &_
		"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
		" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
		"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
		"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
		"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		
		'Build the jumper function
		TheJavascriptFunction = WriteJavaJumpFunction()

		'This variable will now contain everything for the table to be displayed
		TheFullTable = TheJavascriptFunction & Top_Data & str_data & "</TABLE>"
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	Function Build_TableTop()
		TDString = ""
		for i=0 to ForCounter
			CurrentTitle = TableTitles(i)
			if (CurrentTitle = "TEXT") then
				CurrentTitle = ""
			end if
			CurrentASC = ""
			if (SortASC(i) <> "") then
				if (orderBy = SortASC(i)) then
					CurrentASC = "<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortASC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_up_red.gif"" border=""0"" alt=""Current Sort Order""></a>&nbsp;"
				else
					CurrentASC = "<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortASC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_up.gif"" border=""0"" alt=""Sort Ascending""></a>&nbsp;"
				end if
			end if
			CurrentDESC = ""			
			if (SortDESC(i) <> "") then
				if (orderBy = SortDESC(i)) then
					CurrentDESC = "&nbsp;<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortDESC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_down_red.gif"" border=""0"" alt=""Current Sort Order""></a>"
				else
					CurrentDESC = "&nbsp;<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortDESC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_down.gif"" border=""0"" alt=""Sort Descending""></a>"
				end if
			end if
			TDString = TDString & "<TD CLASS='TABLE_HEAD' WIDTH=""" & ColumnWidths(i) & """>" & CurrentASC & CurrentTitle & CurrentDESC & "</TD>" 
		next
		if (isEmpty(THE_TABLE_HIGH_LIGHT_COLOUR) = true) then
			THE_TABLE_HIGH_LIGHT_COLOUR = "STEELBLUE"
		end if
		TopString = "<TABLE CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' " &_
					"STYLE='BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)' " &_
					"slcolor='' hlcolor=" & THE_TABLE_HIGH_LIGHT_COLOUR & " BORDER=1>"  &_
					"<THEAD>" 
		if SubQuery <> "" then
			SubQueryString = "<TR><TD COLSPAN=" & MaxRowSpan & " class='TABLE_HEAD'>" & SubQueryTitle & SubQueryValue & "</TD></TR>" 			
		end if
		TopString = TopString & SubQueryString & "<TR>"  &_
					TDString &_
					"</TR><TR STYLE='HEIGHT:3PX'>"  &_
					"<TD COLSPAN=" & MaxRowSpan & " ALIGN='CENTER' STYLE='BORDER-bottom:2PX SOLID #133E71' CLASS='TABLE_HEAD'></TD>"  &_
					"</TR></THEAD>" 
		
		Build_TableTop = TopString
	End Function
		
	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function

	//validates the sort criteria	
	Function SetSort()
		if (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			for i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
				if SortDESC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
			next
		end if
	End Function
	
	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
%>	