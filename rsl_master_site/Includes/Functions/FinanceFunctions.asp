<%
	Dim TRANSACTION_IDENTITY 
	TRANSACTION_IDENTITY = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "") & "_" & SESSION("USERID")
	
	Function CreatePurchaseOrder(Expenditure, Head, ItemRef, ItemDesc, PODate, DelDate, DelDetail, VatType, NetCost, VAT, QuotedCost, POStatus, POType, UserID, Supplier, Development)		
		if (DelDate <> "NULL") then
			DelDate = "'" & DelDate & "'"
		end if
		SQL = "INSERT INTO F_PURCHASEORDER(" &_
				"EXPENDITUREID, "&_	
				"HEADID, "&_	
				"ITEMREF, "&_	
				"ITEMDESC, "&_	
				"PODATE, "&_	
				"EXPDELDATE, "&_	
				"DELDETAIL, "&_	
				"VATTYPE, "&_	
				"NETCOST, "&_	
				"VAT, "&_					
				"QUOTEDCOST, "&_	
				"POSTATUS, "&_	
				"POTYPE, "&_					
				"USERID, "&_	
				"SUPPLIER, "&_	
				"DEVELOPMENT, "&_
				"ACTIVE "&_	
				" ) VALUES ( " &_
					"" & Expenditure & ", " &_
					"" & Head & ", " &_					
					"'" & ItemRef & "', " &_					
					"'" & ItemDesc & "', " &_					
					"'" & PODate & "', " &_					
					"" & DelDate & ", " &_					
					"'" & DelDetail & "', " &_					
					"" & VatType & ", " &_					
					"" & NetCost & ", " &_					
					"" & VAT & ", " &_					
					"" & QuotedCost & ", " &_
					"" & POStatus & ", " &_															
					"" & POType & ", " &_																				
					"" & UserID & ", " &_					
					"" & Supplier & ", " &_					
					"" & Development & ", " &_										
					"" & 1 & ") "
		'Response.Write SQL
		Conn.Execute(SQL)
		
	End Function

	Function AmendPurchaseOrder(OrderID, Expenditure, Head, ItemRef, ItemDesc, PODate, DelDate, DelDetail, VatType, NetCost, VAT, QuotedCost, UserID, Supplier, Development)		
	if (DelDate <> "NULL") then
			DelDate = "'" & DelDate & "'"
		end if
		SQL = "UPDATE F_PURCHASEORDER SET " &_
				"EXPENDITUREID = " & Expenditure & ", "&_	
				"HEADID = " & Head & ", "&_	
				"ITEMREF = '" & ItemRef & "', "&_	
				"ITEMDESC = '" & ItemDesc & "', "&_	
				"PODATE = '" & PODate & "', " &_	
				"EXPDELDATE = " & DelDate & ", " &_	
				"DELDETAIL = '" & DelDetail & "', " &_	
				"VATTYPE = " & VatType & ", " &_	
				"NETCOST = " & NetCost & ", " &_	
				"VAT = " & VAT & ", " &_					
				"QUOTEDCOST = " & QuotedCost & ", " &_	
				"USERID = " & UserID & ", " &_	
				"SUPPLIER = " & Supplier & ", "&_	
				"DEVELOPMENT = " & Development & ", "&_					
				"IsAmend = 1 " &_
				"WHERE ORDERID = " & OrderID
		Conn.Execute(SQL)

	End Function

	Function AmendPurchaseOrder_Limited(OrderID, ItemRef, ItemDesc, DelDetail, VatType, NetCost, VAT, QuotedCost, UserID)		
	SQL = "UPDATE F_PURCHASEORDER SET " &_
				"ITEMREF = '" & ItemRef & "', "&_	
				"ITEMDESC = '" & ItemDesc & "', "&_	
				"DELDETAIL = '" & DelDetail & "', " &_	
				"VATTYPE = " & VatType & ", " &_	
				"NETCOST = " & NetCost & ", " &_	
				"VAT = " & VAT & ", " &_					
				"QUOTEDCOST = " & QuotedCost & ", " &_	
				"IsAmend = 1, " &_
				"USERID = '" & UserID & "' " &_	
				"WHERE ORDERID = " & OrderID
		Conn.Execute(SQL)

		
	End Function

	Function ReconcilePurchaseOrder(OrderID, ReconcileDate, TaxDate, InvoiceNumber, NetCost, VATTYPE, VAT, TotalCost)
		SQL = "UPDATE F_PURCHASEORDER "&_
				"SET POSTATUS = 9, " &_
				"RECDATE = '" & ReconcileDate & "', " &_
				"TAXDATE = '" & TaxDate & "', " &_				
				"INVOICENUMBER = '" & InvoiceNumber & "', " &_
				"NETCOST = " & NetCost & ", " &_
				"VAT = " & VAT & " ," &_				
				"VATTYPE = " & VATTYPE & ", " &_								
				"QUOTEDCOST = " & TotalCost & " " &_
				"WHERE OrderID = " & OrderID
		Conn.Execute (SQL)
	End Function

	Function PartReconcile(OrderID, ReconcileDate, TaxDate, InvoiceNumber, NetCost, VATTYPE, VAT, TotalCost, Notes)
		SQL = "SELECT * FROM F_PURCHASEORDER WHERE ORDERID = " & OrderID

		Call OpenRs(rsMPO, SQL)
		IF (NOT rsMPO.EOF) THEN
			EXPENDITUREID = rsMPO("EXPENDITUREID")
			HEADID =  rsMPO("HEADID")
			'ITEMREF =  rsMPO("ITEMREF")
			ITEMREF =  "PART REC FOR " & MasterPurchaseNumber(OrderID)			
			'ITEMDESC =  rsMPO("ITEMDESC")
			ITEMDESC =  Notes
			PODATE = rsMPO("PODATE")
			EXPDELDATE = rsMPO("EXPDELDATE")
			if (EXPDELDATE = "" OR ISNULL(EXPDELDATE)) then
				EXPDELDATE = "NULL"
			else
				EXPDELDATE = "'" & FormatDateTime(EXPDELDATE,1) & "'"
			end if
			TAXDATE = rsMPO("TAXDATE")
			DELDETAIL = rsMPO("DELDETAIL")
			USERID = rsMPO("USERID")
			RECDATE = rsMPO("RECDATE")
			SUPPLIER = rsMPO("SUPPLIER")
			if (DEVELOPMENT = "" OR ISNULL(DEVELOPMENT)) then
				DEVELOPMENT = "NULL"
			else
				DEVELOPMENT = rsMPO("DEVELOPMENT")
			end if
			ACTIVE = 1
			POTYPE = rsMPO("POTYPE")
			POSTATUS = 9
			MASTERORDERID = OrderID
			RECTYPE = 4
			CloseRs(rsMPO)
			
			SQL = "INSERT INTO F_PURCHASEORDER (EXPENDITUREID, HEADID, ITEMREF, ITEMDESC, PODATE, " &_
					"EXPDELDATE, TAXDATE, DELDETAIL, VATTYPE, NETCOST, VAT, QUOTEDCOST, USERID, RECDATE, SUPPLIER, DEVELOPMENT, " &_
					"ACTIVE, INVOICENUMBER, POTYPE, POSTATUS, MASTERORDERID, RECTYPE) VALUES ( " &_
					EXPENDITUREID & ", " & HEADID & ", '" & ITEMREF & "', '" & ITEMDESC & "', '" & FormatDateTime(PODATE,1) & "', " &_
					EXPDELDATE & ", '" & TaxDate & "', '" & DELDETAIL & "', " & VATTYPE & ", " & NetCost & ", " &_
					VAT & ", " & TotalCost & ", " & Session("USERID") & ", '" & ReconcileDate & "', " & SUPPLIER & ", " &_
					DEVELOPMENT & ", " & ACTIVE & ", '" & InvoiceNumber & "', " & POTYPE & ", " & POSTATUS & ", " & MASTERORDERID &_
					", " & RECTYPE & ") "
			'Response.Write SQL
			Conn.Execute (SQL)
			'Response.Write SQL			
		END IF
			
	End Function
	
	Function CancelPurchaseOrder(OrderID, UserID)
		SQL = "UPDATE F_PURCHASEORDER SET ACTIVE = 0 , USERID = " & UserID & "WHERE ORDERID = " & OrderID
		Conn.Execute(SQL)
	End Function
	
	Function LOG_PI_ACTION (orderitemid, iAction)
		SQL = "INSERT INTO F_PURCHASEITEM_LOG (IDENTIFIER,ORDERITEMID,ORDERID,ACTIVE,PITYPE, PISTATUS,TIMESTAMP,ACTIONBY,ACTION) " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', ORDERITEMID, ORDERID, ACTIVE, PITYPE, PISTATUS, GETDATE(), " & SESSION("USERID") & ", '" & iAction & "' FROM F_PURCHASEITEM WHERE ORDERITEMID = " & orderitemid
		'response.Write(SQL)
		Conn.Execute(SQL)				
	End Function
    
    Function LOG_PI_ACTION_InvoiceUploadPage (orderitemid, iAction)
		SQL = "INSERT INTO F_PURCHASEITEM_LOG (IDENTIFIER,ORDERITEMID,ORDERID,ACTIVE,PITYPE, PISTATUS,TIMESTAMP,ACTIONBY,ACTION, ISUPLOADINVOICE) " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', ORDERITEMID, ORDERID, ACTIVE, PITYPE, PISTATUS, GETDATE(), " & SESSION("USERID") & ", '" & iAction & "', 1 FROM F_PURCHASEITEM WHERE ORDERITEMID = " & orderitemid
		Conn.Execute(SQL)				
	End Function		

	Function LOG_PI_ACTION_USING_JID (JID, iAction)
		SQL = "INSERT INTO F_PURCHASEITEM_LOG " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', PI.ORDERITEMID, PI.ORDERID, PI.ACTIVE, PI.PITYPE, PI.PISTATUS, GETDATE(), " & SESSION("USERID") & ",  '" & iAction & "' " &_
					"FROM F_PURCHASEITEM PI, P_WOTOREPAIR WTR " &_
						"WHERE PI.ORDERITEMID = WTR.ORDERITEMID AND WTR.JOURNALID = " & JID					
		Conn.Execute(SQL)				
	End Function	

    Function LOG_PI_ACTION_WITH_IMAGE (orderitemid, image_location, iAction, iWhereSQL)
		SQL = "INSERT INTO F_PURCHASEITEM_LOG (IDENTIFIER, ORDERID, ACTIVE, PITYPE, PISTATUS, TIMESTAMP, ACTIONBY, ACTION, image_url,ORDERITEMID) " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', ORDERID, ACTIVE, PITYPE, PISTATUS, GETDATE(), " & SESSION("USERID") & ", '" & iAction & "', '" & image_location & "',ORDERITEMID FROM F_PURCHASEITEM WHERE ORDERITEMID = " & orderitemid & " " & iWhereSQL
		'response.Write(SQL)
		
		Conn.Execute(SQL)				
	End Function    	

	Function LOG_PO_ACTION (orderid, iAction, iWhereSQL)
		SQL = "INSERT INTO F_PURCHASEORDER_LOG (IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION) " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', ORDERID, ACTIVE, POTYPE, POSTATUS, GETDATE(), " & SESSION("USERID") & ", '" & iAction & "' FROM F_PURCHASEORDER WHERE ORDERID = " & orderid & " " & iWhereSQL
		Conn.Execute(SQL)				
	End Function

    Function LOG_PO_ACTIONLOG_INVOICEUPLOADPAGE (orderitemid, iAction)
		SQL = "INSERT INTO F_PURCHASEITEM_LOG (IDENTIFIER,ORDERITEMID,ORDERID,ACTIVE,PITYPE, PISTATUS,TIMESTAMP,ACTIONBY,ACTION, ISUPLOADINVOICE) " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', ORDERITEMID, ORDERID, ACTIVE, PITYPE, PISTATUS, GETDATE(), " & SESSION("USERID") & ", '" & iAction & "', 1 FROM F_PURCHASEITEM WHERE ORDERITEMID = " & orderitemid
		Conn.Execute(SQL)				
	End Function

	Function LOG_PO_ACTION_WITH_IMAGE (orderid, image_location, iAction, iWhereSQL)
		SQL = "INSERT INTO F_PURCHASEORDER_LOG (IDENTIFIER, ORDERID, ACTIVE, POTYPE, POSTATUS, TIMESTAMP, ACTIONBY, ACTION, image_url) " &_
				"SELECT '" & TRANSACTION_IDENTITY & "', ORDERID, ACTIVE, POTYPE, POSTATUS, GETDATE(), " & SESSION("USERID") & ", '" & iAction & "', '" & image_location & "' FROM F_PURCHASEORDER WHERE ORDERID = " & orderid & " " & iWhereSQL
		'response.Write(SQL)
		Conn.Execute(SQL)				
	End Function    	

%>	