<%
	//post to quickbooks here

	
	Function debit_account(nominal_code, credit_amount, FQ_status)
		
		Dim nominal_code_table_listid 	
		
		// get correct reference id from accounts table for 5630
		oRecordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		
		nominal_code_table_listid = oRecordset("LISTID")	
		oRecordset.Close
		response.write "<BR> list id = : " & nominal_code_table_listid
	
		Set oRecordset = CreateObject("ADODB.Recordset")
		oRecordset.Open "SELECT * FROM JOURNALENTRYDEBITLINE WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
		
		oRecordset.AddNew()
		oRecordset.Fields("REFNUMBER").Value = "1"
		oRecordset.Fields("JOURNALDEBITLINEACCOUNTREFLISTID").Value = nominal_code_table_listid
		oRecordset.Fields("JOURNALDEBITLINEAMOUNT").Value = credit_amount
		oRecordset.Fields("JOURNALDEBITLINEMEMO").Value = "Auto RSL Debit"
		oRecordset.Fields("JournalDEBitLineEntityRefListID").Value = "A0000-1084200309"
		oRecordset.Fields("JournalDEBitLineEntityRefFullName").Value = "BHA"
		oRecordset.Fields("FQSaveToCache").Value = FQ_status	
		oRecordset.Update()
		
		
	End Function	

	Function credit_account(nominal_code, credit_amount, FQ_status)
		
		Dim nominal_code_table_listid 	
		
		// get correct reference id from accounts table for 5630
		oRecordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		
		nominal_code_table_listid = oRecordset("LISTID")	
		oRecordset.Close
		response.write "<BR> list id = : " & nominal_code_table_listid
	
		Set oRecordset = CreateObject("ADODB.Recordset")
		oRecordset.Open "SELECT * FROM JOURNALENTRYCREDITLINE WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
		
		oRecordset.AddNew()
		oRecordset.Fields("REFNUMBER").Value = "1"
		oRecordset.Fields("JOURNALCREDITLINEACCOUNTREFLISTID").Value = nominal_code_table_listid
		oRecordset.Fields("JOURNALCREDITLINEAMOUNT").Value = credit_amount
		oRecordset.Fields("JOURNALCREDITLINEMEMO").Value = "Auto RSL Credit"
		oRecordset.Fields("JournalCREDitLineEntityRefListID").Value = "A0000-1084200309"
		oRecordset.Fields("JournalCREDitLineEntityRefFullName").Value = "BHA"
		oRecordset.Fields("FQSaveToCache").Value = FQ_status	
		oRecordset.Update()
		
	End Function	

	//end of post to quickbooks
	
%>