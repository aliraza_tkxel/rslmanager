<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim theURL
	Dim str_data, count, my_page_size
	Dim team_name
	Dim TheFullTable
	Dim ForCounter
	Dim MaxRowSpan
	Dim SubQueryValue
	Dim item_source

	Function Create_Table()
		Dim strSQL, rsSet, intRecord 

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
		'gets the array counter used for looping
		ForCounter = UBound(ColData)

		'gets the max rowspan for use later on
		MaxRowSpan = ForCounter + 1 + 1

		'builds the appropriate row code to be evaluated later
		if (RowClickcolumn = "") then
			RowCodeToEvaluate = " ""<TR STYLE='CURSOR:HAND; border: 1px solid black;text-align:center;'>"" "
		else
			RowCodeToEvaluate = " ""<TR STYLE='CURSOR:HAND;border: 1px solid black;text-align:center;' "" & Eval(RowClickColumn) & "">"" "
		end if
				
		'Prepares the TD Code and stores them in the TDPrepared array
		for i=0 to ForCounter-2
			TempArray = Split(Coldata(i), "|")

			TableTitles    (i) = TempArray(0)
			DatabaseFields (i) = TempArray(1)
			TDFront = ""
			TDEnd = ""
			if (TDFunc(i) <> "") then
				TDFunctionArray = Split(TDFunc(i), "|")
				TDFront = TDFunctionArray(0)
				TDEnd = TDFunctionArray(1)
			end if
			ColumnWidths   (i) = TempArray(2)
			if (TableTitles(i) = "TEXT") then
				if (TDSTUFF(i) = "") then
					TDPrepared (i) = " ""<TD STYLE='border: 1px solid black; text-align:center;CURSOR:HAND;'>"" & " & TDFront & "DatabaseFields(j)" & TDEnd & " & ""</TD>"" "
				else
					TDPrepared (i) = " ""<TD STYLE='border: 1px solid black;text-align=center;CURSOR:HAND;'"" & Eval(TDSTUFF(j)) & "" >"" & " & TDFront & "DatabaseFields(j)" & TDEnd & " & ""</TD>"" "
				end if				
			else
				if (TDSTUFF(i) = "") then
					TDPrepared (i) = " ""<TD STYLE='border: 1px solid black;text-align=center;CURSOR:HAND;'>"" & " & TDFront & "rsSet(DatabaseFields(j))" & TDEnd & " & ""</TD>"" "
				else								
					TDPrepared (i) = " ""<TD STYLE='border: 1px solid black;text-align=center;CURSOR:HAND;' "" & Eval(TDSTUFF(j)) & "" >"" & " & TDFront & "rsSet(DatabaseFields(j))" & TDEnd & " & ""</TD>"" "
				end if
			end if
		next
		
		intRecord = 0
		str_data = ""
		'Set the sql code
		strSQL = SQLCODE
'Response.write(strSQL)
'Response.end
'Response.flush
		'response.write strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
'Response.write("3")
'Response.end
'Response.flush
			
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		if SubQuery <> "" then
			SubQueryValue = rsSet(SubQuery)
		end if

		str_data = str_data & "<TBODY CLASS=""CAPS"">" 
		
		' DETERMINE WETHER OR NOT TO SHOW ALL PURCHASE ORDERS ONLY IF TEAM IS IT
		Dim join_type
		'If Session("TeamCode") = "ITI" Then
        If Session("TeamCode") = "FINA" OR Cint(CANSEE) = 1 Then
            CANSEE = 1
			join_type = " LEFT "
		Else
            CANSEE = 0
			join_type = " INNER "
		End If

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to my_page_size
			
				PIString = ""	
				
				IF ( (CStr(CURRENTPO) = "" AND count = 0) OR CStr(CURRENTPO) = Cstr(rsSet("ORDERID"))) THEN
				
					IMAGE_STRING = "<img src='/js/tree/img/minus.gif' width=18  border=0 onclick=load_me("&rsSet("ORDERID")&")>"
					SQL =  " SELECT DISTINCT PI.ORDERITEMID,PI.ORDERID, PI.GROSSCOST, PS.POSTATUSNAME, PI.ITEMDESC, PI.ITEMNAME,EX.DESCRIPTION AS EXPENDITURE,PI.NETCOST,PI.VAT, PI.PISTATUS" &_
							" FROM F_PURCHASEITEM PI " &_
                            "LEFT JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
							"INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID " &_
							
							" WHERE PI.ORDERID = " & rsSet("ORDERID") & " AND PI.ACTIVE = 1  " &_
                         	"ORDER BY ORDERITEMID ASC"
							
					SQL="select top 1 PI.ORDERITEMID,PI.ORDERID,PI.ITEMNAME,EX.DESCRIPTION,PI.REASON,PI.CANCELBY,PI.VAT,PI.GROSSCOST,PI.NETCOST ,PIL.IMAGE_URL " &_
						"from F_PURCHASEITEM as PI INNER JOIN F_EXPENDITURE EX on Ex.EXPENDITUREID=PI.EXPENDITUREID " &_
						"INNER JOIN F_PURCHASEITEM_LOG PIL on PIL.ORDERITEMID=PI.ORDERITEMID "&_
						"where PI.ORDERID="& rsSet("ORDERID") & " order by TimeStamp desc" 
					SQL="select  PI.ORDERITEMID,PI.ORDERID, PI.ItemName,EX.Description,PI.Reason,(E.FIRSTNAME+E.LASTNAME)as CANCELBY,PI.VAT,PI.GrossCOst,PI.NETCOST , "&_
						"PIL.Image_url from F_PURCHASEITEM as PI INNER JOIN (select ORDERITEMID , max(logid) logid "&_
						"from F_PURCHASEITEM_LOG where len(image_url)>0 group by ORDERITEMID) PIMAX on PIMAX.ORDERITEMID=PI.ORDERITEMID "&_
						"INNER JOIN F_EXPENDITURE EX on Ex.EXPENDITUREID=PI.EXPENDITUREID "&_
						"INNER JOIN F_PURCHASEITEM_LOG PIL on PIMAX.logid=PIL.logid "&_
						"INNER JOIN E__EMPLOYEE E on E.EMPLOYEEID=PI.CANCELBY where PI.ORDERID = "& rsSet("ORDERID")
					
					OpenDB()
					' response.Write(SQL)
					' response.end
					' response.flush
					Call OpenRs_RecCount(rsPI, SQL)
					'For Purchase Items layout
					refNo = 1
					PIString ="<TR style='background:#4681b3;font-size:10px;color:White;font-weight:bold;text-align:center;'>"&_
					"<td valign='center' >Ref:</td>" &_
					"<td valign='center' >Item Name:</td>" &_
					"<td valign='center' >Expenditure:</td>" &_
					"<td valign='center' >REASON:</td>" &_
					"<td valign='center' >CANCEL BY:</td>" &_
					
					
					
					"<td valign='center' >NET(�):</td>" &_
					"<td valign='center' >INVOICE URL:</td>" &_
					"<td valign='center' ></td>" &_
					"</TR>"
					
					totalPIs= rsPI.RecordCount
					'Checking if all Purchase Items Approved
					SQL =  " SELECT DISTINCT PI.ORDERITEMID FROM F_PURCHASEITEM PI WHERE PI.ORDERID = " & rsSet("ORDERID") & " AND PI.ACTIVE = 1 AND PISTATUS IN ( 5,18, 4,7,8,9,10,11,12,13,14, 17,19) " '7,
					Call OpenRs_RecCount(rsCheckPI, SQL)
					approvedPIs = rsCheckPI.RecordCount
					 counter =1    
					'Getting Purchase Items for particular Purchase Order
				    While NOT rsPI.EOF
						my_page_size = my_page_size - 1
						if(refNo<=9) then
					        refNo ="00"& refNo
					    else
			                refNo ="0"&refNo
					    end if
						PIString = PIString & "<TR style='font-size:10px;text-align:center;' class='order-row' userid='"&Session("UserID")&"' orderid='"&rsPI("ORDERID")&"-"&rsPI("ORDERITEMID")&"' postatusid='18' finalpostatusname='Goods Approved' title=''>"&_
							"<TD align=center>" & refNo& "</TD> "&_
							"<TD align=center>" & rsPI("ITEMNAME") & "</TD>"&_
							"<TD align=center>" & rsPI("DESCRIPTION") & "</TD>"&_
							"<TD align=center>" & rsPI("REASON") & "</TD>"&_
							"<TD align=center>" & rsPI("CANCELBY") & "</TD>"&_
							
										
							
							"<TD align=center>" & FormatCurrency(rsPI("NETCOST")) & "</TD>"
							if IsNull(rsPI("IMAGE_URL"))THEN
							PIString = PIString &"<TD align=center>" & rsPI("IMAGE_URL") & "</TD>"
							ELSE
							PIString = PIString &"<td><img src=""../../IMAGES/IconAttachment.gif"" onclick=window.open('"&rsPI("IMAGE_URL")&"');></td>"							
							END if
							PIString = PIString & "<TD align=center></TD>"	
											
							PIString = PIString & "</TR>"
						rsPI.moveNext
					    refNo = refNo+1
                        counter = counter + 1
					  Wend
					  Call CloseRs(rsPI)
					  CloseDB()
				      If (refNo=0) then
				       PIString = ""
				      Else
				        'Getting Total of Purchase Items
				        SQL =  " SELECT Sum(PI.GROSSCOST) As TotalGrossCost,Sum(PI.NETCOST) as TotalNetCost,Sum(PI.VAT) as TotalVAT" &_
							" FROM F_PURCHASEITEM PI " &_
                         	" WHERE PI.ORDERID = " & rsSet("ORDERID") & " AND PI.ACTIVE = 1  " 
                         
					
					    OpenDB()
					    'response.Write(SQL)
					    Call OpenRs(rsTotalPI, SQL)
					    If(Not rsTotalPI.EOF) Then
					        TotalNetCost = 0
					        TotalVAT = 0
					        TotalGrossCost = 0
					        if (len (rsTotalPI("TotalNetCost"))>0) then
					          TotalNetCost = rsTotalPI("TotalNetCost")
					        end if
					        if (len (rsTotalPI("TotalVAT"))>0) then
					          TotalVAT = rsTotalPI("TotalVAT")
					        end if
					        if (len (rsTotalPI("TotalGrossCost"))>0) then
					          TotalGrossCost = rsTotalPI("TotalGrossCost")
					        end if
					         
				            PIString = PIString & "<TR style='font-size:10px;border-style:none;text-align:center;border: 1px solid black;text-align=center'>"&_
							"<TD align=right style='background:#4681b3;' colspan=5><span style='text-align=right;color:white;font-size:11px;font-weight:bold;'>Total:</span></TD>"&_
							 "<td align='center' style='font-weight:bold;'>"&FormatCurrency(TotalNetCost)&"</td>" &_
							 "<td align='center' style='font-weight:bold;'>"&FormatCurrency(TotalVAT)&"</td>" &_
							"<td align='center' style='font-weight:bold;'>"&FormatCurrency(TotalGrossCost)&"</td>" &_
							"<td align='center' style='border-right: 1px solid black;'></td>" &_
							"</TR>"
						End If	
				      End If
					
				ELSE
					IMAGE_STRING = "<img src='/js/tree/img/plus.gif' width=18 height=18 border=0 onclick=load_me("&rsSet("ORDERID")&") >"					
				END IF
				
				'response.write(count & " " & CURRENTPO & "<br>")
                'response.Write(ERowCodeToEvaluate)
				str_data = str_data & Eval(RowCodeToEvaluate) & "<TD style='border:none;BACKGROUND-COLOR:WHITE;background:url(/js/tree/img/line.gif) 2' width=20>" & IMAGE_STRING & "</TD>"
				'loop through the tds and build a single row
				for j=0 to ForCounter-2
					str_data = str_data & Eval(TDPrepared(j))
				next
				str_data = str_data & "</TR>" & PIString
					
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next

			str_data = str_data & "</TBODY>" 
		End If
			
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = str_data & "<THEAD><TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>" & EmptyText & "</TD></TR>" &_
						"</THEAD>"
		End If

		'calls the top builder
		Top_Data = Build_TableTop()

		'ensure table height is consistent with any amount of records
		'Call fill_gaps()

		'aDD rECORD pAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)
		' links
		str_data = str_data &_
		"<TFOOT><TR STYLE='border: 1px solid black;'><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
		" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
		"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
		"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
		"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		
		'Build the jumper function
		TheJavascriptFunction = WriteJavaJumpFunction()

		'This variable will now contain everything for the table to be displayed
		TheFullTable = TheJavascriptFunction & Top_Data & str_data & "</TABLE>"
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	Function Build_TableTop()
		TDString = "<TD CLASS='TABLE_HEAD' width=20>&nbsp;</TD>"
		for i=0 to ForCounter-2
			CurrentTitle = TableTitles(i)
			if (CurrentTitle = "TEXT") then
				CurrentTitle = ""
			end if
			CurrentASC = ""
			if (SortASC(i) <> "") then
				if (orderBy = SortASC(i)) then
					CurrentASC = "<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortASC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_up_red.gif"" border=""0"" alt=""Current Sort Order""></a>&nbsp;"
				else
					CurrentASC = "<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortASC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_up.gif"" border=""0"" alt=""Sort Ascending""></a>&nbsp;"
				end if
			end if
			CurrentDESC = ""			
			if (SortDESC(i) <> "") then
				if (orderBy = SortDESC(i)) then
					CurrentDESC = "&nbsp;<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortDESC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_down_red.gif"" border=""0"" alt=""Current Sort Order""></a>"
				else
					CurrentDESC = "&nbsp;<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortDESC(i)) & """ style=""text-decoration:none""><img src=""/myImages/sort_arrow_down.gif"" border=""0"" alt=""Sort Descending""></a>"
				end if
			end if
			TDString = TDString & "<TD CLASS='TABLE_HEAD' WIDTH=""" & ColumnWidths(i) & """>" & CurrentASC & CurrentTitle & CurrentDESC & "</TD>" 
		next
		if (isEmpty(THE_TABLE_HIGH_LIGHT_COLOUR) = true) then
			THE_TABLE_HIGH_LIGHT_COLOUR = "STEELBLUE"
		end if
		TopString = "<TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=0 CLASS='TAB_TABLE' " &_
					"STYLE='BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)' " &_
					"slcolor='' hlcolor=" & THE_TABLE_HIGH_LIGHT_COLOUR & " BORDER=0>"  &_
					"<THEAD>" 
		if SubQuery <> "" then
			SubQueryString = "<TR><TD COLSPAN=" & MaxRowSpan & " class='TABLE_HEAD'>" & SubQueryTitle & SubQueryValue & "</TD></TR>" 			
		end if
		TopString = TopString & SubQueryString & "<TR>"  &_
					TDString &_
					"</TR><TR STYLE='HEIGHT:3PX'>"  &_
					"<TD COLSPAN=" & MaxRowSpan & " ALIGN='CENTER' STYLE='BORDER-bottom:2PX SOLID #133E71' CLASS='TABLE_HEAD'></TD>"  &_
					"</TR></THEAD>" 
		
		Build_TableTop = TopString
	End Function
		
	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function

	//validates the sort criteria	
	Function SetSort()
		if (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			for i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
				if SortDESC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
			next
		end if
	End Function
	
	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
%>	