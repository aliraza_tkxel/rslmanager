<!--#include virtual="Includes/Functions/Base64Encryption.asp" -->
<%
Function GetFormFields ()
	For i = 0 To UBound(FormFields)
		FormSplit = Split(FormFields(i), "|")
		FormValues(i) = Request.Form(FormSplit(0))		
		DataTypes(i) = FormSplit(1)

		FormSecondSplit = Split(FormSplit(0), "_")
		ElementTypes(i) = FormSecondSplit(0)
		DataFields(i) = FormSecondSplit(1)	

		if (ElementTypes(i) = "cde") then
			'is code instead of a request object
			FormValues(i) = DataTypes(i)
		else
			if (ElementTypes(i) = "rdo") then
				If (FormValues(i) = "on") Then
					FormValues(i) = 1
				End If
			end if
			If (FormValues(i) = "") Then
				FormValues(i) = "Null"
			else 
				FormValues(i) = Replace(FormValues(i), "'", "''")
				if (DataTypes(i) = "TEXT") then
					FormValues(i) = "'" & TRIM(FormValues(i)) & "'"
				elseif (DataTypes(i) = "PATH") then
					FormValues(i) = "'" & Replace(FormValues(i), "\", "/") & "'"
				elseif (DataTypes(i) = "DATE") then
					FormValues(i) = "'" & FormatDateTime(FormValues(i), 2) & "'" 
				end if
			end if
		end if
	Next	
End Function

Function MakeInsert(ByRef InsertSQL)
	GetFormFields()
	ColumnSQL = ""
	DataSQL = ""
	For i = 0 to Ubound(FormFields)
		if (i = 0) then
		
			ColumnSQL = ColumnSQL & DataFields(i)
			if (DataFields(i) ="PASSWORD") then
			pwd = FormValues(i)
			pwd = Replace(pwd, "'", "")
				DataSQL = DataSQL & ",'" & base64_encode(pwd) & "'"
			else
				DataSQL = DataSQL & FormValues(i)
			end if 
			
		else
			ColumnSQL = ColumnSQL & "," & DataFields(i)
			if (DataFields(i) ="PASSWORD") then
			pwd = FormValues(i)
			pwd = Replace(pwd, "'", "")
				DataSQL = DataSQL & ",'" & Base64Encode(pwd)  & "'"
			else
				DataSQL = DataSQL & "," & FormValues(i)
			end if 
			
		end if
	Next
	InsertSQL = " (" & ColumnSQL & ") VALUES (" & DataSQL & ") "
End Function

Function MakeUpdate(ByRef UpdateSQL)
	GetFormFields()
	ColumnSQL = ""
	For i = 0 to Ubound(FormFields)
	
		
		if (DataFields(i) ="PASSWORD") then
			pwd = FormValues(i)
			pwd = Replace(pwd, "'", "")
								
			if (i = 0) then
			
			ColumnSQL = ColumnSQL & DataFields(i) & " = '" & Base64Encode(pwd) & "'"
			
			else
				ColumnSQL = ColumnSQL & "," & DataFields(i) & " = '" & Base64Encode(pwd) & "'"
			end if
		else
		
			if (i = 0) then
				ColumnSQL = ColumnSQL & DataFields(i) & " = " & FormValues(i)
			else
				ColumnSQL = ColumnSQL & "," & DataFields(i) & " = " & FormValues(i)
			end if
		end if 
		
	Next
	UpdateSQL = " SET " & ColumnSQL & " "
End Function
%>