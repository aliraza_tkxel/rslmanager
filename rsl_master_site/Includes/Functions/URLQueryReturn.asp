<%
'i think this function gets any request form objects so it can use them again and again when record paging
'so that they are not lost
Function theURLSET(DONTMATCH)
	Dim strSort
	Dim strTemp
	strSort=DONTMATCH
	strTemp=""
	
	strSortArray = Split(strSort, "|")
	for each item in Request.Form
		matched = false
		for j=0 to UBound(strSortArray)
			if lcase(item) = strSortArray(j) then
				matched = true
				Exit For
			end if
		next
		if (NOT matched) then
			strTemp=strTemp & item & "=" & Server.UrlEncode(Request(item)) & "&"
		end if
	next
	for each item in Request.QueryString
		matched = false
		for j=0 to UBound(strSortArray)
			if lcase(item) = strSortArray(j) then
				matched = true
				Exit For
			end if
		next
		if (NOT matched) then
			strTemp=strTemp & item & "=" & Server.UrlEncode(Request(item)) & "&"
		end if
	next
	
	theURLSET=strTemp

End Function
%>