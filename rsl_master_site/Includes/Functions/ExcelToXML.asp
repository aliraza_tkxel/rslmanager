<%
prot = "http"
https = lcase(request.ServerVariables("HTTPS"))
if https <> "off" then prot = "https"
domainname = Request.ServerVariables("SERVER_NAME")

Public Function GetXMLfromXLSFile(FilePath, SheetName,ColumnList)
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        Dim xmlhttp, DataToSend, postUrl
            DataToSend = "FilePath=" & FilePath & "&SheetName=" & SheetName & "&ColumnsList=" & ColumnList

            postUrl = prot & "://" & domainname & "/RSLExcelToXMLWebService/RSLExcelToXMLService.asmx/ExcelToXML"

            'postUrl = "http://rslbhalive.local/RSLExcelToXMLWebService/RSLExcelToXMLService.asmx/ExcelToXML"
            'postUrl = "https://dev.broadlandhousinggroup.org/RSLExcelToXMLWebService/RSLExcelToXMLService.asmx/ExcelToXML"
            'postUrl = "https://test.broadlandhousinggroup.org/RSLExcelToXMLWebService/RSLExcelToXMLService.asmx/ExcelToXML"
		    'postUrl = "https://crm.broadlandhousinggroup.org/RSLExcelToXMLWebService/RSLExcelToXMLService.asmx/ExcelToXML"

            set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
                xmlhttp.Open "POST", postUrl, false
                xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
                xmlhttp.send DataToSend
        GetXMLfromXLSFile = xmlhttp.responseText 
    Else
        GetXMLfromXLSFile = "<table>Page loaded first time and form not posted</table>"
    End If
End Function
%>
