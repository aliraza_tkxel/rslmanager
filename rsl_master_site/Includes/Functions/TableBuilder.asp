<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim theURL
	Dim str_data, count, my_page_size
	Dim team_name
	Dim TheFullTable
	Dim ForCounter
	Dim MaxRowSpan
	Dim SubQueryValue

	Function Create_Table()
		Dim strSQL, rsSet, intRecord 

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

		'gets the array counter used for looping
		ForCounter = UBound(ColData)

		'gets the max rowspan for use later on
		MaxRowSpan = ForCounter + 1

		'builds the appropriate row code to be evaluated later
		If (RowClickcolumn = "") Then
			RowCodeToEvaluate = " ""<tr style='cursor:pointer'>"" "
		Else
			RowCodeToEvaluate = " ""<tr style='cursor:pointer' "" & Eval(RowClickColumn) & "">"" "
		End If

		'Prepares the TD Code and stores them in the TDPrepared array
		For i=0 To ForCounter
			TempArray = Split(Coldata(i), "|")
            If UBound(TempArray) > 1 Then
			    TableTitles    (i) = TempArray(0)
			    DatabaseFields (i) = TempArray(1)
			    TDFront = ""
			    TDEnd = ""
			    If (TDFunc(i) <> "") Then
				    TDFunctionArray = Split(TDFunc(i), "|")
				    TDFront = TDFunctionArray(0)
				    TDEnd = TDFunctionArray(1)
			    End If
			    ColumnWidths   (i) = TempArray(2)
			    If (TableTitles(i) = "TEXT") Then
				    If (TDSTUFF(i) = "") Then
					    TDPrepared (i) = " ""<td>"" & " & TDFront & "DatabaseFields(j)" & TDEnd & " & ""</td>"" "
				    Else
					    TDPrepared (i) = " ""<td "" & Eval(TDSTUFF(j)) & "" >"" & " & TDFront & "DatabaseFields(j)" & TDEnd & " & ""</td>"" "
				    End If
			    Else
				    If (TDSTUFF(i) = "") Then
					    TDPrepared (i) = " ""<td>"" & " & TDFront & "rsSet(DatabaseFields(j))" & TDEnd & " & ""</td>"" "
				    Else
					    TDPrepared (i) = " ""<td "" & Eval(TDSTUFF(j)) & "" >"" & " & TDFront & "rsSet(DatabaseFields(j))" & TDEnd & " & ""</td>"" "
				    End If
			    End If
            End If
		Next

		intRecord = 0
		str_data = ""
		'Set the sql code
		strSQL = SQLCODE

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
        rsSet.ActiveConnection.CommandTimeout = 60 	
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		If SubQuery <> "" Then
			SubQueryValue = rsSet(SubQuery)
		End If
 		str_data = str_data & "<tbody class=""CAPS"">"

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 To rsSet.PageSize

				str_data = str_data & Eval(RowCodeToEvaluate)
				'loop through the tds and build a single row
				For j=0 To ForCounter
                    If j <= UBound(TDPrepared) Then
					    str_data = str_data & Eval(TDPrepared(j))
                    End If
				Next
				str_data = str_data & "</tr>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit For

			Next

			str_data = str_data & "</tbody>"
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = str_data & "<thead><tr><td colspan=""" & MaxRowSpan & """ align=""center"">" & EmptyText & "</td></tr>" &_
						"</thead>"
		End If

		'calls the top builder
		Top_Data = Build_TableTop()

		'ensure table height is consistent with any amount of records
		Call fill_gaps()

		'aDD rECORD pAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)
		' links
		str_data = str_data &_
		"<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
		"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">"  &_
		"<a href="" " & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1"" title=""First""><b><font color=""blue"">First</font></b></a> "  &_
		"<a href="" " & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & " "" title=""Prev""><b><font color=""blue"">Prev</font></b></a>"  &_
		" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
		" <a href="" " & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & " "" title=""Next""><b><font color=""blue"">Next</font></b></a>"  &_ 
		" <a href="" " & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & " "" title=""Last""><b><font color=""blue"">Last</font></b></a>"  &_
		"</td><td align=""right"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"" />&nbsp;"  &_
		"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" title=""GO"" style=""font-size:10px; cursor:pointer"">"  &_
		"</td></tr></thead></table></td></tr></tfoot>"

		'Build the jumper function
		TheJavascriptFunction = WriteJavaJumpFunction()

		'This variable will now contain everything for the table to be displayed
		TheFullTable = TheJavascriptFunction & Top_Data & str_data & "</table>"

		rsSet.close()
		Set rsSet = Nothing

	End function

	Function Build_TableTop()
		TDString = ""
		For i=0 To ForCounter
            If i <= UBound(TableTitles) Then
			    CurrentTitle = TableTitles(i)
			    If (CurrentTitle = "TEXT") Then
				    CurrentTitle = ""
			    End If
			    CurrentASC = ""
			    If (SortASC(i) <> "") Then
				    if (orderBy = SortASC(i)) Then
					    CurrentASC = "<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortASC(i)) & """ style=""text-decoration:none"" title=""Current Sort Order""><img src=""/myImages/sort_arrow_up_red.gif"" border=""0"" alt=""Current Sort Order""></a>&nbsp;"
				    Else			
					    CurrentASC = "<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortASC(i)) & """ style=""text-decoration:none"" title=""Sort Ascending""><img src=""/myImages/sort_arrow_up.gif"" border=""0"" alt=""Sort Ascending""></a>&nbsp;"
				    End If
			    End If
			    CurrentDESC = ""
			    If (SortDESC(i) <> "") Then
				    If (orderBy = SortDESC(i)) Then
					    CurrentDESC = "&nbsp;<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortDESC(i)) & """ style=""text-decoration:none"" title=""Current Sort Order""><img src=""/myImages/sort_arrow_down_red.gif"" border=""0"" alt=""Current Sort Order""></a>"
				    Else
					    CurrentDESC = "&nbsp;<a href=""" & PageName & "?" & theURL & "CC_Sort=" & Server.UrlEncode(SortDESC(i)) & """ style=""text-decoration:none"" title=""Sort Ascending""><img src=""/myImages/sort_arrow_down.gif"" border=""0"" alt=""Sort Descending""></a>"
				    End If
			    End If
			    TDString = TDString & "<td class=""TABLE_HEAD"" width=""" & ColumnWidths(i) & """>" & CurrentASC & CurrentTitle & CurrentDESC & "</td>" 
            End If
		Next
		If (isEmpty(THE_TABLE_HIGH_LIGHT_COLOUR) = True) Then
			THE_TABLE_HIGH_LIGHT_COLOUR = "#4682b4"
		End If
		TopString = "<table width=""750"" cellpadding=""1"" cellspacing=""2"" class=""TAB_TABLE"" " &_
					"style=""border-collapse:collapse; behavior:url(/Includes/Tables/tablehl.htc)"" " &_
					"slcolor="""" hlcolor=" & THE_TABLE_HIGH_LIGHT_COLOUR & " border=""1"">"  &_
					"<thead>"
		If SubQuery <> "" Then
			SubQueryString = "<tr><td colspan=""" & MaxRowSpan & """ class=""TABLE_HEAD"">" & SubQueryTitle & SubQueryValue & "</td></tr>"
		End If
		TopString = TopString & SubQueryString & "<tr>"  &_
					TDString &_
					"</tr><tr style=""height:3px"">"  &_
					"<td colspan=""" & MaxRowSpan & """ align=""center"" style=""border-bottom:2px solid #133e71"" class=""TABLE_HEAD""></td>"  &_
					"</tr></thead>" 

		Build_TableTop = TopString
	End Function

	'pads table out to keep the height consistent
	Function fill_gaps()
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend
	End Function

	'validates the sort criteria
	Function SetSort()
		if (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			for i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
				if SortDESC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
			next
		end if
	End Function
	
	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<script type=""text/javascript"" language=""JavaScript"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</script>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
%>	