<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	'CANCEL ANY PURCHASE ORDERS THAT EXIST...
	Function CancelPO(JID)
		//GET THE MASTER ORDERID
		SQL = "SELECT PI.ORDERID " &_
				"FROM F_PURCHASEITEM PI, P_WOTOREPAIR WTR " &_
					"WHERE PI.ORDERITEMID = WTR.ORDERITEMID AND WTR.JOURNALID = " & JID
		Call OpenRs(rsPO, SQL)
		ORDERID = rsPO("ORDERID")
		Call CloseRs(rsPO)
		
		//CANCEL THE PURCHASE ITEM
		SQL = "UPDATE F_PURCHASEITEM SET ACTIVE = 0 " &_
				"FROM F_PURCHASEITEM PI, P_WOTOREPAIR WTR " &_
					"WHERE PI.ORDERITEMID = WTR.ORDERITEMID AND WTR.JOURNALID = " & JID
		Conn.Execute(SQL)

		//THIS PART WILL CHECK IF ALL REPAIRS HAVE BEEN CANCELLED IN WHICH CASE IT WILL SET THE WORK ORDER TO BE CANCELLED ASWELL.
		//GET FULL COUNT OF ITEMS
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ORDERID = " & ORDERID
		Call OpenRs(rsCOUNT, SQL)
		TOTALCOUNT = CLng(rsCOUNT("THECOUNT"))
		Call CloseRs(rsCOUNT)

		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 0 AND ORDERID = " & ORDERID
		Call OpenRs(rsCOUNT, SQL)
		CLOSEDCOUNT = CLng(rsCOUNT("THECOUNT"))
		Call CloseRs(rsCOUNT)
		
		//if the counts match then all items have been made in active then set the whole purchase order to be in active		
		IF (CLOSEDCOUNT = TOTALCOUNT) then
			Call LOG_PO_ACTION(ORDERID, "AUTOMATED REPAIR STATUS CANCEL", "")						
			SQL = "UPDATE F_PURCHASEORDER SET ACTIVE = 0 WHERE ORDERID = " & ORDERID
			Conn.Execute SQL
			
			SQL = "UPDATE P_WORKORDER SET WOSTATUS = 5 WHERE ORDERID = " & ORDERID
			Conn.Execute SQL
		ELse
			SET_PO_STATUS(JID) 
		END IF			
		
	End Function

	'changes the status of a purchase order depending on the journalid
	Function ChangePIStatus(JID, PISTAT)
		'HAVE TO CHECK WHETHER THE PURCHASE ITEM HAS BEEN RECONCILED ALREADY OR NOT..IF YES, DO NOT ALLOW STATUS TO BE UPDATED.
		SQL = "SELECT PISTATUS FROM F_PURCHASEITEM PI INNER JOIN P_WOTOREPAIR WTR ON WTR.ORDERITEMID = PI.ORDERITEMID WHERE PI.PISTATUS < 9 AND WTR.JOURNALID = " & JID
		Call OpenRs(rsReconciled, SQL)
		if (NOT rsReconciled.EOF) then

			//LOG ACTION
			Call LOG_PI_ACTION_USING_JID(JID, "REPAIR ORDERITEM STATUS UPDATE")	
			//SET THE NEW STATUS OF THE PURCHASE ITEM
			SQL = "UPDATE F_PURCHASEITEM SET ACTIVE = 1, PISTATUS = " & PISTAT & " " &_
					"FROM F_PURCHASEITEM PI, P_WOTOREPAIR WTR " &_
						"WHERE PI.ORDERITEMID = WTR.ORDERITEMID AND WTR.JOURNALID = " & JID
			Conn.Execute(SQL)

		end if
		CloseRs(rsReconciled)
		
		SET_PO_STATUS(JID)
		
	End Function
	
	Function SET_PO_STATUS(JID)
		//GET THE MASTER ORDERID
		SQL = "SELECT PI.ORDERID " &_
				"FROM F_PURCHASEITEM PI, P_WOTOREPAIR WTR " &_
					"WHERE PI.ORDERITEMID = WTR.ORDERITEMID AND WTR.JOURNALID = " & JID
		Call OpenRs(rsPO, SQL)
		ORDERID = rsPO("ORDERID")
		Call CloseRs(rsPO)
		
		//CHECK IF ALL ITEMS HAVE BEEN WORK COMPLETED (5) OR INVOICE RECIEVED (7)
		//IF SO SET THE APPROPRIATE MASTER PO STATUS

		//GET FULL COUNT OF ITEMS
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = " & ORDERID
		Call OpenRs(rsCOUNT, SQL)
		TOTALCOUNT = CLng(rsCOUNT("THECOUNT"))
		Call CloseRs(rsCOUNT)
		
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS IN (0,3) AND ORDERID = " & ORDERID
		Call OpenRs(rsWO, SQL)
		WORKORDERED = CLng(rsWO("THECOUNT"))
		Call CloseRs(rsWO)
		if (WORKORDERED = TOTALCOUNT) then
				
				NewPOStatus = 3 'Work Ordered

				' Check here to see if all the items in the workorder are less than that of assigned

				SQL = "SELECT COUNT(*) AS ASSIGNEDJOURNALS  FROM "  &_
					"	P_WORKORDER W "  &_
					"		INNER JOIN P_WOTOREPAIR WOT ON WOT.WOID = W.WOID "  &_
					"		INNER JOIN C_JOURNAL J ON J.JOURNALID = WOT.JOURNALID "  &_
					"	WHERE W.ORDERID = " & ORDERID  & " AND J.CURRENTITEMSTATUSID IN (1,2,12) " 
				' zanfar (SENIOR DEV WANNABE) said dont put 8 in there (declined) 
				
				
				Call OpenRs(rsJournalsAssigned, SQL)
					JOURNALSASSIGNED = CLng(rsJournalsAssigned("ASSIGNEDJOURNALS"))
				Call CloseRs(rsJournalsAssigned)
				
				' If all the items in the order have status less than 2 then set work order status to assigned
				' else (which means it could be part in progress) then set to part in progress
				
				If JOURNALSASSIGNED = TOTALCOUNT THEN
					NewWOStatus = 2 'Assigned	
				ELSE
					NewWOStatus = 6 'In Progress	
				END IF
									
		else
			'CHECK IF THE PURCHASE ITEMS ARE FULLY COMPLETE
			SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS >= 5 AND ORDERID = " & ORDERID
			Call OpenRs(rsWO, SQL)
			WORKCOMPLETED = CLng(rsWO("THECOUNT"))
			Call CloseRs(rsWO)
			IF (WORKCOMPLETED = TOTALCOUNT) Then
				NewPOStatus = 5 'Work Completed

				'CHECK IF THE INVOICES HAVE BEEN RECIEVED FOR EACH PURCHASE ITEM
				SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS >= 5 AND ORDERID = " & ORDERID
				Call OpenRs(rsIO, SQL)
				INVOICESRECIEVED = CLng(rsIO("THECOUNT"))
				Call CloseRs(rsIO)
				IF (INVOICESRECIEVED = TOTALCOUNT) Then
					NewWOStatus = 11 'COMPLETED
				ELSE
					NewWOStatus = 6 'IN PROGRESS
				END IF
				
			Else
				NewPOStatus = 4 'Part Completed
				NewWOStatus = 6 'In Progress				
			End if
		End if

		SQL = "SELECT ORDERID FROM F_PURCHASEORDER WHERE POSTATUS < 7 AND ORDERID = " & ORDERID
		Call OpenRs(rsCANUPDATE, SQL)
		//if the above is empty, this means that the purchase order has not been reconciled therefore it can be updated.
		//otherwise it should not be updated as this will ruin the status of the PO.
		if (NOT rsCANUPDATE.EOF) then
			Call LOG_PO_ACTION(ORDERID, "AUTOMATED REPAIR STATUS UPDATE", "")						
			SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = " & NewPOStatus & " WHERE ORDERID = " & ORDERID
			Conn.Execute SQL	
		end if
		CloseRs(rsCANUPDATE)

		SQL = "UPDATE P_WORKORDER SET WOSTATUS = " & NewWOStatus & " WHERE ORDERID = " & ORDERID
		Conn.Execute SQL	

	End Function
%>