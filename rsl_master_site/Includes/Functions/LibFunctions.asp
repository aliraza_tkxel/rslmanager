<% 
set Session = server.CreateObject("SessionMgr.Session2")
%>
<!-- #include virtual="Live_Connections/db_connection.asp" -->
<!-- #include virtual="Includes/Functions/ActiveUsers.asp" -->
<%
	'on error resume next
	Call LogActiveUser()
	Call ActiveUserCleanup()
	'on error goto 0

	Dim Rs, Conn, SQL, strSQL
	Dim GetCurrent_EndDate, GetCurrent_StartDate, GetCurrent_YRange
	
	Function OpenDB()
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.ConnectionTimeout = 90	
		Conn.CommandTimeout = 0		'Will run commands until they finish!
		Conn.Open RSL_CONNECTION_STRING 
		
	End Function
	 
	Function CloseDB()
		Conn.Close
		Set Conn = Nothing
	End Function
	
	// Currently opens a ForwardOnly cursor, ReadOnly lock
	Function OpenRs(RecSet, iSQL)
		Set RecSet = Server.CreateObject("ADODB.Recordset")
		RecSet.Open iSQL, Conn, 0, 1                       
	End Function

	Function OpenRs_RecCount(RecSet, iSQL)
		Set RecSet = Server.CreateObject("ADODB.Recordset")
		RecSet.CursorLocation = 3
		RecSet.Open iSQL, Conn, 3, 3
	End Function
	
	// Opens a KeySet ReadOnly cursor
	Function OpenRsK(RecSet, iSQL)
		Set RecSet = Server.CreateObject("ADODB.Recordset")
		RecSet.Open iSQL, Conn, 1, 1
	End Function
	
	Function CloseRs(RecSet)
		RecSet.Close
		Set RecSet = Nothing
	End Function
	
	// strSELECT								- String for select bOX TO BE RETURNED TO
	// iSelectName								- The Name of the Select List
	// iTABLE									- String for TABLE NAME
	// iCOLUMNS									- String for COLUMNS
	// iORDERBY									- String for ORDERBY CLAUSE	
	// PS										- ADDD A PLEASE SELECT OPTION AT START?
	// isSelected								- SELECT AN ITEM FROM THE DATA: 1 = yes, 0 = no
	// iStyle									- Any Style You wish to apply to the Select List
	// iClass									- Any Class you wish to apply to the Select List
	// iEvent									- Any event such as onclick or other valid select tag
	// IsNullNumber								- Returns a value of Zero if there is a null or equal to ""
	// isManager()								- Returns 1 if they are a Manager and 0 if Not
	
	Function isManager()
		//Is Manager
		employee_id = Session("userid")
		OpenDB()
			mysql = " SELECT ISMANAGER FROM E_JOBDETAILS WHERE EMPLOYEEID = " & employee_id
			Call OpenRs(rsManager,mysql)
			isManager = rsManager(0)
		CloseDB()
	End Function
	
	Function isDirector()
		
		employee_id = Session("userid")
		OpenDB()
		mysql = " SELECT ISDIRECTOR FROM E_JOBDETAILS WHERE EMPLOYEEID = " & employee_id
		Call OpenRs(rsDirector,mysql)
		isDirector = rsDirector(0)
		CloseDB()
		
	End Function
	
	Function isLineManager(userid, manid)
		
		OpenDB()
		mysql = " SELECT * FROM E_JOBDETAILS WHERE LINEMANAGER = " & manid & " AND EMPLOYEEID = " & userid
		Call OpenRs(rsLineMan,mysql)
		if not rsLineMan.EOF Then
			isLineManager = true
		else
			isLineManager = false
		end if
		CloseDB()
		
	End Function
	
	Function IsNullNumber(ValueEntered)
			If isNull(ValueEntered) or ValueEntered = "" Then  IsNullNumber = 0 Else IsNullNumber = ValueEntered End IF
	End Function

	Function BuildSelectForSC(ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS, isSelected, iStyle, iClass, iEvent, dropDownName)
		lst = "<select name='" & iSelectName & "' id='" & iSelectName & "' "
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		lst = lst & ">"

		Dim strSQL
			strSQL = "SELECT " & iCOLUMNS & " FROM " & iTABLE & " ORDER BY " & iORDERBY

		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</option>"
		End If

		Dim rsSet
		Call OpenRs (rsSet, strSQL)
		
		If NOT isNull(isSelected) Then
            if(not rsSet.EOF) then
                lst = lst & "<option value='-1'>All " & dropDownName & " </option>"
            End If

			While (NOT rsSet.EOF)
				lstid = rsSet(0)
				optionSelected = ""
				If (CStr(lstid) = CStr(isSelected)) Then
					optionSelected = " selected"
				End If
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		Else
			if(not rsSet.EOF) then
				lst = lst & "<option value='-1'>All " & dropDownName & " </option>"
			end if
			While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		End If

		Call CloseRs(rsSet)

		lst = lst & "</select>"
	End Function
	''''''''''''''''''''''''''''''''''''''''''''''''
		Function BuildSelectVAT(ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS, isSelected, iStyle, iClass, iEvent)
		lst = "<select name='" & iSelectName & "' id='" & iSelectName & "' "
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		lst = lst & ">"

		Dim strSQL
			strSQL = "SELECT " & iCOLUMNS & " FROM " & iTABLE & " ORDER BY " & iORDERBY

		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</option>"
		End If

		Dim rsSet
		Call OpenRs (rsSet, strSQL)
		If NOT isNull(rsSet) Then
		 ' response.write("basiq")
		 ' response.end
		 ' response.flush
		lst = lst & "<option value='' selected>Please Select</option>"
		End If 
		
		If NOT isNull(isSelected) Then
			While (NOT rsSet.EOF)
				lstid = rsSet(0)
				optionSelected = ""
				If (CStr(lstid) = CStr(isSelected)) Then
					optionSelected = " selected"
				End If
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		Else
			While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		End If

		Call CloseRs(rsSet)

		lst = lst & "</select>"
	End Function
	''''''''''''''''''''''''''''''''''''''''''''''''
	Function BuildSelect(ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS, isSelected, iStyle, iClass, iEvent)
		lst = "<select name='" & iSelectName & "' id='" & iSelectName & "' "
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		lst = lst & ">"

		Dim strSQL
			strSQL = "SELECT " & iCOLUMNS & " FROM " & iTABLE & " ORDER BY " & iORDERBY

		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</option>"
		End If

		Dim rsSet
		Call OpenRs (rsSet, strSQL)

		If NOT isNull(isSelected) Then
			While (NOT rsSet.EOF)
				lstid = rsSet(0)
				optionSelected = ""
				If (CStr(lstid) = CStr(isSelected)) Then
					optionSelected = " selected"
				End If
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		Else
			While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		End If

		Call CloseRs(rsSet)

		lst = lst & "</select>"
	End Function
	
	Function BuildSelectListBox(ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS, isSelected, iStyle, iClass, iEvent)
		lst = "<SELECT size=""200"" multiple=""multiple"" NAME='" & iSelectName & "' "
		if NOT isNull(iClass) then
			lst = lst & " class='" & iClass & "' "
		end if
		if NOT isNull(iStyle) then
			lst = lst & " style='" & iStyle & "' "
		end if
		if NOT isNull(iEvent) then
			lst = lst & iEvent
		end if
		lst = lst & ">"
	
		Dim strSQL
			strSQL = "SELECT " & iCOLUMNS & " FROM " & iTABLE & " ORDER BY " & iORDERBY
	
		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</OPTION>"
		END IF
		'RW STRSQL
		Dim rsSet
		Call OpenRs (rsSet, strSQL)
	
		if NOT isNull(isSelected) then
			While (NOT rsSet.EOF)
				lstid = rsSet(0)
				optionSelected = ""
				if (CStr(lstid) = CStr(isSelected)) then
					optionSelected = " selected"
				end if
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		else		
			While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		end if		
	
		CloseRs(rsSet)
		
		lst = lst & "</SELECT>"
	End Function
	Dim StopWatch(19)
	sub StartTimer(x)
		StopWatch(x) = Timer	
	end sub
	
	function StopTimer(x)
	   EndTime = Timer
	   StopTimer = EndTime - StopWatch(x)
	end function
	
	Function characterPad(strWord,chCharacter,chLOR,intTotal)  
	 intNumChars = intTotal - len(strWord)  
	 if intNumChars > 0 then  
	 if chLOR="l" then  
	 characterPad=string(intNumChars,chCharacter) & strWord  
	 else  
	 characterPad=strWord & string(intNumChars,chCharacter)  
	 end if  
	 else  
	 characterPad = strWord  
	 end if  
	End Function  
	
	Function CustomerNumber(strWord)
		CustomerNumber = "CN" & characterPad(strWord,"0", "l", 8)
	End Function
	
    Function MyView(strWord)
		MyView = "View"
	End Function	

	Function PurchaseNumber(strWord)
		PurchaseNumber = "<b>PO</b> " & characterPad(strWord,"0", "l", 7)
	End Function	
	
	Function CreditNumber(strWord)
		CreditNumber = "<b>CN&nbsp;</b> " & characterPad(strWord,"0", "l", 7)
	End Function	
	
	Function SaleNumber(strWord)
		SaleNumber = "<b>SI</b> " & characterPad(strWord,"0", "l", 7)
	End Function	

	Function SalesCreditNoteNumber(strWord)
		SalesCreditNoteNumber = "<b>SCN</b> " & characterPad(strWord,"0", "l", 7)
	End Function

	Function WorkNumber(strWord)
		WorkNumber = "<b>WO</b> " & characterPad(strWord,"0", "l", 7)
	End Function	

	Function MasterPurchaseNumber(strWord)
		MasterPurchaseNumber = "<b>MP</b> " & characterPad(strWord,"0", "l", 7)
	End Function	

	Function MIIncome(strWord)
		MIIncome = "<b>MIS</b> " & characterPad(strWord,"0", "l", 7)
	End Function	

	Function PartPurchaseNumber(strWord)
		PartPurchaseNumber = "<b>PP</b> " & characterPad(strWord,"0", "l", 7)
	End Function	
	
	Function TenancyReference(strWord)
		TenancyReference = characterPad(strWord,"0", "l", 6)
	End Function
	
	Function SalesCustomerReference(strWord)
		SalesCustomerReference = "<b>SCU</b> " & characterPad(strWord,"0", "l", 6)
	End Function

	Function BatchNumber(strWord)
		BatchNumber = "<b>BN</b> " & characterPad(strWord,"0", "l", 7)
	End Function
	
	Function rw(theString)
		Response.write theString
	End Function	
	
	Function rq(theString)
		rq = Request(theString)	
	End Function	
	
	Function GetCurrentYear()
		today = FormatDateTime(Date, 1)
'		response.write today
		rsFYearSQL = "SELECT YRange, YStart, YEnd FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
		Call OpenRs(rsFYear, rsFYearSQL)
		if (NOT rsFYear.EOF) then
			GetCurrent_YRange = rsFYear("YRange")
			GetCurrent_StartDate = rsFYear("YStart")
			GetCurrent_EndDate = rsFYear("YEnd")		
		end if
		Call CloseRs(rsFYear)
	End Function

	Function BuildSelect_SP_DROPDOWN(ByRef lst, iSelectName, SP_NAME, PS, isSelected, iStyle, iClass, iEvent)
		
		lst = "<SELECT NAME='" & iSelectName & "' ID='" & iSelectName & "' "
		
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		
		lst = lst & ">"
		
		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</OPTION>"
		End If
		
		Dim spRs, spRsDD
		Dim rsSet
		Dim color
		Dim elStatus
		
		'rw "Conn" & Conn
		'rw "<BR>"
		'rw "SP_NAME" & SP_NAME
		'Response.End()

	set spRs = Server.CreateObject("ADODB.Command")
		spRs.ActiveConnection = Conn
		spRs.CommandText = SP_NAME
		spRs.CommandType = 4
		spRs.CommandTimeout = 0
		spRs.Prepared = true
		
	set spRsDD = spRs.Execute
	
		if NOT isNull(isSelected) then
			While (NOT spRsDD.EOF)
				lstid = spRsDD(0)
				optionSelected = ""
				if (CStr(lstid) = CStr(isSelected)) then
					optionSelected = " selected"
				end if
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & spRsDD(1) & "</option>"
				spRsDD.MoveNext()
			Wend
		else		
			While (NOT spRsDD.EOF)
				lst = lst & "<option value='" & spRsDD(0) & "'>" & spRsDD(1) & "</option>"
				spRsDD.MoveNext()
			Wend
		end if		
	
		lst = lst & "</SELECT>"

	set spRsDD = nothing
		
	End Function	
	
	Function BuildSelectListBox_SP(ByRef lst, iSelectName, SP_NAME, PS, isSelected, iStyle, iClass, iEvent, byref ParameterId)
	
		lst = "<SELECT size=""200"" multiple=""multiple"" NAME='" & iSelectName & "' ID='" & iSelectName & "' "
		
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		
		lst = lst & ">"
		
		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</OPTION>"
		End If
		
		Dim spRs, spRsDD
		Dim rsSet
		Dim color
		Dim elStatus
		
	set spRs = Server.CreateObject("ADODB.Command")
		spRs.ActiveConnection = Conn
		spRs.CommandText = SP_NAME
		spRs.CommandType = 4
		spRs.CommandTimeout = 0
		spRs.Parameters.Refresh
        spRs.Parameters(1) = ParameterId
 		spRs.Prepared = true
		
				
	set spRsDD = spRs.Execute 
	
		if NOT isNull(isSelected) then
			While (NOT spRsDD.EOF)
				lstid = spRsDD(0)
				optionSelected = ""
				if (CStr(lstid) = CStr(isSelected)) then
					optionSelected = " selected"
				end if
				lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & spRsDD(1) & "</option>"
				spRsDD.MoveNext()
			Wend
		else		
			While (NOT spRsDD.EOF)
				lst = lst & "<option value='" & spRsDD(0) & "'>" & spRsDD(1) & "</option>"
				spRsDD.MoveNext()
			Wend
		end if		
	
		lst = lst & "</SELECT>"

	set spRsDD = nothing
		
	End Function

Function BuildSelect_From1DArray(ByRef lst, iSelectName, iArray, PS, isSelected, iStyle, iClass, iEvent)
        lst = "<select name='" & iSelectName & "' id='" & iSelectName & "' "
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		lst = lst & ">"

		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</option>"
		End If

       For i = 0 to UBound(iArray)
            lstId = iArray(i)
            lstText = lstId
            optionSelected = ""
            If NOT isNull(isSelected) AND (CStr(lstId) = CStr(isSelected)) Then 
	            optionSelected = " selected"
            End If
            lst = lst & "<option value='" & lstId & "'" & optionSelected & ">" &lstText  & "</option>"
        Next

		lst = lst & "</select>"
End Function

Function Decode(sIn)
    dim x, y, abfrom, abto
    Decode="": ABFrom = ""

    For x = 0 To 25: ABFrom = ABFrom & Chr(65 + x): Next 
    For x = 0 To 25: ABFrom = ABFrom & Chr(97 + x): Next 
    For x = 0 To 9: ABFrom = ABFrom & CStr(x): Next 

    abto = Mid(abfrom, 14, Len(abfrom) - 13) & Left(abfrom, 13)
    For x=1 to Len(sin): y=InStr(abto, Mid(sin, x, 1))
        If y = 0 then
            Decode = Decode & Mid(sin, x, 1)
        Else
            Decode = Decode & Mid(abfrom, y, 1)
        End If
    Next
End Function

Function Encode(sIn)
    dim x, y, abfrom, abto
    Encode="": ABFrom = ""

    For x = 0 To 25: ABFrom = ABFrom & Chr(65 + x): Next 
    For x = 0 To 25: ABFrom = ABFrom & Chr(97 + x): Next 
    For x = 0 To 9: ABFrom = ABFrom & CStr(x): Next 

    abto = Mid(abfrom, 14, Len(abfrom) - 13) & Left(abfrom, 13)
    For x=1 to Len(sin): y = InStr(abfrom, Mid(sin, x, 1))
        If y = 0 Then
             Encode = Encode & Mid(sin, x, 1)
        Else
             Encode = Encode & Mid(abto, y, 1)
        End If
    Next
End Function 

%>
