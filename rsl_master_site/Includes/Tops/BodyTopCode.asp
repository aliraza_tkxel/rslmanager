<%
ZZ_Time_2 = StopTimer(2)
StartTimer(3)

Dim rsModules

UserID_193 = Session("UserID")

Call OpenDB()

SQLModules = "EXECUTE AC_MODULELIST @USERID = " & UserID_193

Set rsModules = Conn.Execute (SQLModules)

ModuleCount = 0
Set THEPATH_193 = rsModules("THEPATH")
Set IMAGE_193 = rsModules("IMAGE")
While (NOT rsModules.EOF)
	ModuleID = rsModules("MODULEID")
	If (ModuleCount = 0) Then
		ModuleData = "<td><a href=""" & THEPATH_193 & """ " &_
		"ONMOUSEOVER=""changeImages('MyText', '/myImages/Modules/" & rsModules("IMAGEMOUSEOVER") & "'); return true;"" " &_
		"ONMOUSEOUT=""changeImages('MyText', '/myImages/Modules/NoText.gif'); return true;""> " &_
		"<img name=""My" & ModuleID & """ alt=""" & rsModules("DESCRIPTION") & """ src=""/myImages/Modules/" & rsModules("IMAGEOPEN") & """ width=""41"" height=""19"" border=""0"" /></a>" &_
		"</td>"
		MenuModules = "hMenu1.addSub(""Menu1Sub" & ModuleID & """, ""Menu1"", """ & rsModules("DESCRIPTION") & """, """ & THEPATH_193 & """);" & VbCrLf
	Else
		ModuleData = ModuleData & "<td><a href=""" & THEPATH_193 & """ " &_
		"ONMOUSEOVER=""changeImages('My" & ModuleID & "', '/myImages/Modules/" & rsModules("IMAGEOPEN") & "', 'MyText', '/myImages/Modules/" & rsModules("IMAGEMOUSEOVER") & "'); return true;"" " &_
		"ONMOUSEOUT=""changeImages('My" & ModuleID & "', '/myImages/Modules/" & IMAGE_193 & "', 'MyText', '/myImages/Modules/NoText.gif'); return true;""> " &_
		"<img name=""My" & ModuleID & """ alt=""" & rsModules("DESCRIPTION") & """ src=""/myImages/Modules/" & IMAGE_193 & """ width=""41"" height=""19"" border=""0"" /></a>" &_
		"</td>"
		ModuleDescription = rsModules("DESCRIPTION")
		if ModuleDescription="HR" OR ModuleDescription="My Job" Then
		MenuModules = MenuModules & "hMenu1.addSub(""Menu1Sub" & ModuleID & """, ""Menu1"", """ & rsModules("DESCRIPTION") & """, """ & THEPATH_193+UserID_193 & """);" & VbCrLf
		Else
		MenuModules = MenuModules & "hMenu1.addSub(""Menu1Sub" & ModuleID & """, ""Menu1"", """ & rsModules("DESCRIPTION") & """, """ & THEPATH_193 & """);" & VbCrLf
		End If
	End If
	ModuleCount = ModuleCount + 1
	rsModules.moveNext
Wend

If (ModuleCount > 0) Then
	ModuleData = ModuleData & "<td><img src=""/myImages/Modules/EndBit.gif"" width=""7"" height=""19"" alt="""" /></td>"	
	ModuleData = ModuleData & "<td><img src=""/myImages/spacer.gif"" width=""" & (523 - (ModuleCount * 41) - 7) & """ height=""19"" alt="""" /></td>"	
Else
	ModuleData = ModuleData & "<td><img src=""/myImages/spacer.gif"" width=""" & (523 - (ModuleCount * 41)) & """ height=""19"" alt="""" /></td>"
End If

Call CloseRs(rsModules)

Dim rsSelectedModule
SQLSelectedModule = "SELECT IMAGETAG, IMAGETITLE FROM AC_MODULES WHERE MODULEID = " & ModuleIdentifier

Call OpenRs (rsSelectedModule, SQLSelectedModule)
If (NOT rsSelectedModule.EOF) Then
	BodyTag = rsSelectedModule("IMAGETAG")
	BodyTitle = rsSelectedModule("IMAGETITLE")
Else
	BodyTag = ""
	BodyTitle = ""
End If
Call CloseRs(rsSelectedModule)
Call CloseDB()

If (ModuleIdentifier <> 0) Then
	RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING

	MenuSQL = "SHAPE {EXEC AC_MENULIST @USERID = " & UserID_193 & ", @MODULEID = " & ModuleIdentifier & "} " &_
			  	  "APPEND ({EXEC AC_PAGELIST @USERID = " & UserID_193 & ", @MODULEID = " & ModuleIdentifier & "} AS PAGES " &_
					  "RELATE MENUID TO MENUID) "

	Set ShapeConn = Server.CreateObject("ADODB.Connection")
	ShapeConn.Open RSL_DATASHAPE_CONNECTION_STRING
	Set rst = ShapeConn.Execute(MenuSQL)

	MenuCounter = 2
	Do While Not rst.EOF
		MenuID = rst("MENUID")
		MenuLink = rst("THEPATH")
		If (isNULL(MenuLink) OR MenuLink = "") Then
			MenuLink = "#"
		End If

		RemainingMenus = RemainingMenus & "hMenu1Att[""width""] = " & rst("MENUWIDTH") & ",hMenu1Att[""height""] = 16,hMenu1Att[""align""] = ""left"";" & VbCrLf
		RemainingMenus = RemainingMenus & "hMenu1.addMain(""Menu" & MenuCounter & """, true, """ & rst("DESCRIPTION") & """, """ & MenuLink & """);" & VbCrLf
		RemainingMenus = RemainingMenus & "hMenu1SubAtt[""width""] = " & rst("SUBMENUWIDTH") & ",hMenu1SubAtt[""height""] = 16,hMenu1SubAtt[""align""] = ""left"";" & VbCrLf

		Set rstPages = rst("PAGES").Value
		Do While Not rstPages.EOF
			PageID = rstPages("PAGEID")
			PageDescription = rstPages("DESCRIPTION")
			'Start handling ASB Module
			If(PageDescription="ASB Management") Then
			RemainingMenus = RemainingMenus & "hMenu1.addSub(""Menu" & MenuCounter & "Sub" & PageID & """, ""Menu" & MenuCounter & """, """ & rstPages("DESCRIPTION") & """, """ & rstPages("THEPATH") +UserID_193 & """);" & VbCrLf
	        ElseIf (PageDescription="Cost Centre (ALL)") Then
			RemainingMenus = RemainingMenus & "hMenu1.addSub(""Menu" & MenuCounter & "Sub" & PageID & """, ""Menu" & MenuCounter & """, """ & rstPages("DESCRIPTION") & """, """ & rstPages("THEPATH") +UserID_193 & """);" & VbCrLf
			ElseIf (PageDescription="HB Upload") Then
			RemainingMenus = RemainingMenus & "hMenu1.addSub(""Menu" & MenuCounter & "Sub" & PageID & """, ""Menu" & MenuCounter & """, """ & rstPages("DESCRIPTION") & """, """ & rstPages("THEPATH") +UserID_193 & """);" & VbCrLf
			ElseIf (PageDescription="Journal Templates") Then
			RemainingMenus = RemainingMenus & "hMenu1.addSub(""Menu" & MenuCounter & "Sub" & PageID & """, ""Menu" & MenuCounter & """, """ & rstPages("DESCRIPTION") & """, """ & rstPages("THEPATH") +UserID_193 & """);" & VbCrLf
            Else
			RemainingMenus = RemainingMenus & "hMenu1.addSub(""Menu" & MenuCounter & "Sub" & PageID & """, ""Menu" & MenuCounter & """, """ & rstPages("DESCRIPTION") & """, """ & rstPages("THEPATH") & """);" & VbCrLf        
	        End If
	        'End handling ASB Module
			rstPages.moveNext
		Loop
		Menucounter = MenuCounter + 1
		rst.moveNext
	Loop

	rst.close
	Set rst = Nothing
End If

ZZ_Time_3 = Stoptimer(3) 
StartTimer(4)
%>