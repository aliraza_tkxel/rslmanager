<%
	Dim CANAMEND
	Call OpenDB()

	TEN_ID_FOR_REPAIRS = Request.Item("tenancyid")

'	DETERMINE WETHER OR NOT TO SHOW RENT JOURNAL AMENDS BUTTON
	CANAMEND = can_amend_rent_journal()

    Function can_amend_rent_journal()

		SQL = "SELECT * FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'CANUPDATERENTJOURNAL' AND DEFAULTVALUE = " & SESSION("USERID")
		Call OpenRs(rsSet, SQL)
		if Not rsSet.EOF Then C = 1 Else C = 0 End If
		Call CloseRs(rsSet)
		can_amend_rent_journal = C

	End Function


	if (TEN_ID_FOR_REPAIRS = "" OR TEN_ID_FOR_REPAIRS = "-1") then

		PendingRepairs = 0
		InProgressRepairs = 0
		CompletedRepairs = 0
		CancelledRepairs = 0
		GasAppointmentDate = ""
		GasServicingRenewalDate = ""

    else

		SQL = "SELECT COUNT(JOURNALID) AS PENDING FROM C_JOURNAL WHERE ITEMNATUREID = 2 AND CURRENTITEMSTATUSID IN (1,2,3,4,8,12) AND TENANCYID = " & TEN_ID_FOR_REPAIRS
		Call OpenRs(rsPending, SQL)
		    PendingRepairs = rsPending("PENDING")
		Call CloseRs(rsPending)

		SQL = "SELECT COUNT(JOURNALID) AS INPROGRESS FROM C_JOURNAL WHERE ITEMNATUREID = 2 AND CURRENTITEMSTATUSID IN (6,7,9) AND TENANCYID = " & TEN_ID_FOR_REPAIRS
		Call OpenRs(rsInProgress, SQL)
		    InProgressRepairs = rsInProgress("INPROGRESS")
		Call CloseRs(rsInProgress)

		SQL = "SELECT COUNT(JOURNALID) AS CANCELLED FROM C_JOURNAL WHERE ITEMNATUREID = 2 AND CURRENTITEMSTATUSID IN (5) AND TENANCYID = " & TEN_ID_FOR_REPAIRS
		Call OpenRs(rsCancelled, SQL)
		    CancelledRepairs = rsCancelled("CANCELLED")
		Call CloseRs(rsCancelled)

		SQL = "SELECT COUNT(JOURNALID) AS COMPLETED FROM C_JOURNAL WHERE ITEMNATUREID = 2 AND CURRENTITEMSTATUSID IN (10,11,22) AND TENANCYID = " & TEN_ID_FOR_REPAIRS
		Call OpenRs(rsCompleted, SQL)
		    CompletedRepairs = rsCompleted("COMPLETED")
		Call CloseRs(rsCompleted)

	' ------- Start of comments by Adnan 30/10/2013-------------------------------------------
    '	SQL= " SELECT APPOINTMENTDATE,RN.ISSUDATE,CT.PROPERTYID, AP.JOURNALID "&_
    '         " FROM C_TENANCY CT "&_
    '         "    LEFT JOIN ( "&_
    '         "               SELECT MIN(DATEADD(MM,12,ISSUEDATE)) AS ISSUDATE,PROPERTYID "&_
    '         "               FROM GS_PROPERTY_APPLIANCE "&_
    '         "               GROUP BY PROPERTYID "&_
    '         "               ) RN ON RN.PROPERTYID=CT.PROPERTYID "&_
    '         "     LEFT JOIN ( "&_
	'		 "                 SELECT GJ.APPOINTMENTDATE,CJ.PROPERTYID,CJ.JOURNALID	"&_
	'		 "                 FROM  C_REPAIRTOGASSERVICING GJ "&_
	'		 "             	   INNER JOIN C_REPAIR CR ON CR.REPAIRHISTORYID=GJ.REPAIRHISTORYID "&_
	'		 "          	   INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID "&_
	'		 "                 WHERE  GJ.REPAIRHISTORYID =(SELECT MAX( REPAIRHISTORYID) "&_
	'		 "                                             FROM C_REPAIR "&_
	'		 "                    						   WHERE JOURNALID=CJ.JOURNALID "&_
	'		 "                      						 ) "&_
 	'		 "                 ) AP ON AP.PROPERTYID=CT.PROPERTYID "&_
    '        " WHERE CT.TENANCYID=" & TEN_ID_FOR_REPAIRS
             

        ' - Edited by Adnan to use Appointment and IssueDate details in line with Appliance Servicing 30/10/2013 '
        'sql = "EXEC AS_APPOINTMENT_ISSUEDATE " & TEN_ID_FOR_REPAIRS

        set spUser = Server.CreateObject("ADODB.Command")
	    spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "AS_APPOINTMENT_ISSUEDATE"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@TenancyId", 3, 1,4,TEN_ID_FOR_REPAIRS)


	    set rsGas = spUser.Execute

		'Call OpenRs(rsGas, SQL)
'  End of modifications by Adnan 30/10/2013
'-------------------------------------------------------------------

		if Not rsGas.EOF Then
		      GasJournalID =  rsGas("JournalID")
    	      GasPropertyID =  rsGas("PropertyID")
		      GasAppointmentDate = rsGas("APPOINTMENTDATE")
		      GasAppointmentDateAndTime =  GasAppointmentDate

		      GasServicingRenewalDate = rsGas("ISSUDATE")
		      GasServicingRenewalDateAndTime = GasServicingRenewalDate
			
			if GasAppointmentDate <> "" then GasAppointmentDate = FormatDateTime(GasAppointmentDate,2) end if
		    if GasServicingRenewalDate <> "" then GasServicingRenewalDate=  FormatDateTime(GasServicingRenewalDate ,2) end if

	end if
		Call CloseRs(rsGas)
	end if

    gas_gif="ic_gas"

    if(GasServicingRenewalDate<>"") then
        if(DateDiff("d",FormatDateTime(Date()),FormatDateTime(GasServicingRenewalDate))<0) then
            gas_gif="ic_gas_expire"
        end if
    end if

	str_repairs =  	"<table style=""color:#133e71"" height=""87"" width=""100%"" border=""0"">" &_
					    "<tr>" &_
                            "<td><img src=""/Customer/Images/repairs.gif""></td>" &_
                            "<td align=""right""><b><a href=""#"" onclick=""javascript:parent.open_Reactive_Repair()""><font color=""blue"">View Repairs</font></a>&nbsp;&nbsp;</b></td>" &_
                        "</tr>" &_
					    "<tr>" &_
                            "<td>" & PendingRepairs & " pending</td>" &_
                            "<td>" & CancelledRepairs & " cancelled</td>" &_
                        "</tr>" &_
					    "<tr>" &_
                            "<td nowrap=""nowrap""> " & InProgressRepairs & " in progress</td>" &_
                            "<td>" & CompletedRepairs & " completed</td>" &_
                        "</tr>"

					If (GasJournalID <> "") Then
	                    str_repairs = str_repairs & "<tr>" &_
                                                        "<td style='border-top:solid #133E71 1px;height:1px;margin:0 0 0 0;padding:0 2 0 0;width:20px;vertical-align:bottom;cursor:hand;' title='" & GasAppointmentDateAndTime & "'  onClick=parent.goGasServicingDetail('" & GasPropertyID & "'," & GasJournalID & ")>&nbsp;"& GasAppointmentDate & "</td>" &_
                                                        "<td style='border-top:solid #133E71 1px;height:1px;margin:0 0 0 0;padding:0 0 0 0;vertical-align:bottom;'> Gas Appointment&nbsp; <img src='../../myImages/Repairs/"& gas_gif &".gif' valign=right valign=top></td>" &_
                                                        "</tr>" &_
    					                            "<tr>" &_
                                                        "<td style='padding-left:20px;width:20px;' title='" & GasServicingRenewalDateAndTime  & "' onHover='style.color = blue' onClick=parent.goGasServicingDetail('" & GasPropertyID & "'," & GasJournalID & ")>" & GasServicingRenewalDate  & "</td>" &_
                                                        "<td> Gas Servicing Renewal</td>" &_
                                                    "</tr>" 
					Else
					    str_repairs = str_repairs & "<tr>" &_
                                                        "<td style='border-top:solid #133E71 1px;height:1px;margin:0 0 0 0;padding:0 2 0 0;width:20px;vertical-align:bottom;' title='" & GasAppointmentDateAndTime & "'>&nbsp;"& GasAppointmentDate & "</td>" &_
                                                        "<td style='border-top:solid #133E71 1px;height:1px;margin:0 0 0 0;padding:0 0 0 0;vertical-align:bottom;'> Gas Appointment&nbsp; <img src='../../myImages/Repairs/"& gas_gif &".gif' valign=right valign=top></td>" &_
                                                    "</tr>" &_
					                                "<tr>" &_
                                                        "<td style=""padding-left:20px;width:20px;"" title='" & GasServicingRenewalDateAndTime  & "' onHover='style.color = blue'>" & GasServicingRenewalDate  & "</td>" &_
                                                        "<td> Gas Servicing Renewal</td>" &_
                                                    "</tr>"
					End If

	str_repairs =	str_repairs &	"</table>" &_
					"<table style=""color:#133e71"" height=""86"" width=""100%"" cellpadding=""1"" cellspacing=""2"" border=""0"">" &_
					                "<tr>" &_
                                    "<td style=""padding-bottom:0px;""><img src=""/Customer/Images/rents.gif""></td>" &_
                                    "<td align=""right"" style=""padding-bottom:0px;"">" &_
				                    "<select name=""sel_Statement"" id=""sel_Statement"" class=""textbox200"" style=""width:130px;"" onchange=""parent.OpenStatement(this.value)"">"  &_
				                    "	<option value='-1'>Choose Statement</option>"  &_
				                    "	<option value='Tenant'>Rent Statement</option>"  &_
				                    "	<option value='Court'>Court Statement</option>"  &_
				                    "	<option value='Recharge'>Recharge Statement</option>"  &_
				                    "</select>" &_
					                "</td>" &_
                                    "</tr>"
	IF CANAMEND = 1 Then
		str_repairs = str_repairs & "<tr><td><img style=""cursor:pointer"" src=""/Customer/Images/amendments.gif"" onclick=""javascript:parent.swap_div(13, 'bottom')""></td><td align=""right""></td></tr>"
	Else
		str_repairs = str_repairs & "<tr><td>&nbsp;</td><td align=""right""></td></tr>"
	End If

    if(TEN_ID_FOR_REPAIRS="") then TEN_ID_FOR_REPAIRS=-1 end if

	SQL="SELECT T.Tenancyid FROM C_TENANCY T  WHERE T.Tenancyid =" &  TEN_ID_FOR_REPAIRS & "  AND T.ENDDATE IS NULL"
	Call OpenRs(rsSet,SQL )
	If rsSet.EOF Then 
		tenancy_id = -1
		disa_bled = " style=""display:none"" "
	End If
	Call CloseRs(rsSet)

	str_repairs = str_repairs & "<tr>" &_
        "<td colspan=""6"" style=""border-top-style:dashed; border-top-width:1px"">&nbsp;</td>" &_
    "</tr>" &_
	"<tr>" &_
        "<td colspan=""6"">" &_
        "<table width=""100%"" cellpadding=""0"" cellspacing=""0""> " &_
        "  <tr>  " &_
        " 	<td>&nbsp;&nbsp;</td>" &_
        "    <TD style='margin:0 0 0 0;'><img name='cend' id=""cend"" src='/customer/Images/cforecast.gif' title='Forecast final month payment based on projected tenancy end date.' style='cursor:pointer' onclick='parent.forecast()'></td> " &_
        "    <TD style='margin:0 0 0 0;'><img name='cpost' id=""cpost"" src='/customer/Images/cpost.gif' width='27' height='22' title='Post cash/chq to this tenancy' style='cursor:pointer' onclick='parent.post_cash()'></td> " &_
        "    <TD style='margin:0 0 0 0;'><img name='cpostcard' id=""cpostcard"" src='/customer/Images/cards.jpg' width='27' height='22' title='Post card to this tenancy' style='cursor:pointer' onclick='parent.post_card()'></td> " &_
        "    <TD style='margin:0 0 0 0;'><img name='sbasket' id=""sbasket"" src='/myImages/Repairs/sbasket.gif' title='Create new fault(s)' width='27' height='22' style='cursor:pointer' onclick='parent.work_order()' " & disa_bled  & "></td> " &_
        "    <TD style='margin:0 0 0 0;'><img name='spad' id=""spad"" src='/customer/Images/spad.gif' width='27' height='22' style='cursor:pointer' title='Open up General/Legal scratch pad data' onclick='parent.open_pad()' ></td> " &_
        "  </tr> " &_
        "</table> "&_
				"</td></tr>" &_
					"</table>"
%>
