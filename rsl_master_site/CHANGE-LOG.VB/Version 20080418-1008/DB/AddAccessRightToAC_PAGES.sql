/*
Date: 15-Jul-2015
Description: Add access rights of SupplierList page to Management Menu Item of Business Module
Written By: Aqib Javed

Written */
Insert into AC_PAGES (Description, MenuId,Active, Link, Page,AccessLevel, DisplayInTree)
Values('Supplier Profiles',(Select MENUID from AC_MENUS Where DESCRIPTION='Management' AND ACTIVE=1),1,1,'SupplierList.asp',1,1)
