/*
Invoices for Approval & Goods for Approval
Also make the same changes to the menu items located via:

Finance > Purchasing > Invoice Received
Finance > Purchasing > Goods Received

to:
 
Finance > Purchasing > Invoices for Approval
Finance > Purchasing > Goods for Approval

*/
UPDATE    E_ALERTS
SET   AlertName = 'Invoices for Approval'          
WHERE     ( AlertName = 'Received Invoices')

UPDATE    E_ALERTS
SET   AlertName = 'Goods for Approval'          
WHERE     ( AlertName = 'GoodsForApproval')

UPDATE    AC_PAGES
SET   [DESCRIPTION] = 'Invoices for Approval'          
WHERE     ( [DESCRIPTION] = 'Invoice Received')

UPDATE    AC_PAGES
SET   [DESCRIPTION] = 'Goods for Approval'          
WHERE     ( [DESCRIPTION] = 'Goods Received')





