USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GET_DOCUMENT_TYPES]    Script Date: 08/25/2015 11:42:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author				Create date			Description						

--Simon Rogers			1st August 2014			Retrieve Document Types
			 

-- Modified By:       Modification Date  

-- Raja Aneeq			25/8/2015	

-- =============================================

ALTER PROCEDURE [dbo].[P_GET_DOCUMENT_TYPES]
	-- Add the parameters for the stored procedure here
    @ShowActiveOnly BIT = 1
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        IF @ShowActiveOnly = 1 
            SELECT  -1 AS DocumentTypeId ,
                    'Please select' AS Title ,
                    1 AS [Active] ,
                    0 AS [Order]
            UNION
            SELECT  DocumentTypeId ,
                    Title ,
                    Active ,
                    [Order]
            FROM    dbo.P_Documents_Type dt
            WHERE   [Active] = 1 AND DocumentTypeId >19 --changed
            ORDER BY [Order] ,
                    [Title]
	
        ELSE 
            SELECT  -1 AS DocumentTypeId ,
                    'Please select' AS Title ,
                    1 AS [Active] ,
                    0 AS [order]
            UNION
            SELECT  DocumentTypeId ,
                    Title ,
                    Active ,
                    [Order]
            FROM    dbo.P_Documents_Type dt
            WHERE DocumentTypeId >19 --changed
            ORDER BY [Order] ,
                    [Title]
	
	
    END


