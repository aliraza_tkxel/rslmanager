-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <14/10/2015>
-- Description:	<Calculate alert count of Invoices for approval on whiteboard>
-- =============================================
ALTER PROCEDURE POReceivedInvoicesCount
	
	@USERID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 DECLARE @POSTATUSID INT    
 DECLARE @FINALPOSTATUSNAME NVARCHAR(100)    
 SELECT @POSTATUSID = PS.POSTATUSID, @FINALPOSTATUSNAME =PS.POSTATUSNAME From    
 F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'invoice approved'    
 SELECT COUNT(distinct PO.ORDERID) AS InvoicesCount    
 FROM F_PURCHASEORDER PO    
 CROSS APPLY (SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM WHERE    
 ACTIVE = 1 AND ORDERID = PO.ORDERID) PI Left JOIN F_PURCHASEITEM P ON P.ORDERID = PO.ORDERID    
 INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PO.POSTATUS    
 LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID    
 LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID    
 CROSS APPLY (SELECT TOP 1 1 AS 'GoodsApproved' FROM F_PURCHASEORDER_LOG POL    
 WHERE POL.ORDERID = PO.ORDERID ) GoodsApproved    
 OUTER APPLY (SELECT TOP 1 image_url AS image_url    
 FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND image_url IS NOT NULL    
 ORDER BY POTIMESTAMP DESC) Invoice    
 WHERE(POSTATUS = 1 Or POSTATUS = 5 Or POSTATUS = 7 Or POSTATUS = 18) And    
 (P.APPROVED_BY = @USERID    OR P.USERID = @USERID  ) 

END
GO
