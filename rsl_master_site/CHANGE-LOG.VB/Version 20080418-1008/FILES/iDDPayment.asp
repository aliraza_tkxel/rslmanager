<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7


	Dim row_id, lstitemtype, action, btn_HTML, reedonly, tenancy_id, customer_id
	Dim dd_itemtype, dd_accountname, dd_sortcode, dd_paymentdate, dd_initialamount 
	Dim dd_regularpayment, dd_ddperiod,	dd_accountnumber
	Dim dd_periodbalance, dd_creationdate, dd_suspend
	Dim CollectionDateDaysRule,InitialDateChangeAllowed 

	action = Request("action")
	row_id = Request("rowid")
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	dd_ddperiod = 999
	dd_suspend = 0
	dd_action = 1
	
	' We set this to 1 so on create we allow insert of initial date
	InitialDateChangeAllowed  = 1 

	' there us a business rule that on set up you can not select a date for payment within 14 days or 5 days on amend
	' we first set it to 14 and reset it to five in the amend bit
	CollectionDateDaysRule = 14
	
	if action = "NEW" then 
		row_id = 0 
		btn_HTML = "<INPUT TYPE=BUTTON NAME='btn_SAVE' VALUE='Save' onclick='ConfirmChanges(""NEW"")' CLASS='RSLBUTTON'>"
	Else
		btn_HTML = "<INPUT TYPE=BUTTON NAME='btn_SAVE' VALUE='Amend' onclick='ConfirmChanges(""AMEND"")' CLASS='RSLBUTTON'>"
	end if
	
	OpenDB()

	SQL = 	"SELECT	D.DDSCHEDULEID,D.INITIALDATE, " &_
			"		ISNULL(D.ITEMTYPE,0) AS ITEMTYPE, " &_
			"		ISNULL(D.ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
			"		ISNULL(D.SORTCODE,'') AS SORTCODE, " &_
			"		ISNULL(D.ACCOUNTNUMBER,'') AS ACCOUNTNUMBER, D.PAYMENTDATE as realpaydate, " &_
			"		PAYMENTDATE = (CASE WHEN ISNULL(CONVERT(NVARCHAR, D.PAYMENTDATE, 103) ,GETDATE() + 1) >=  convert(datetime,CONVERT(NVARCHAR, GETDATE(), 103))  " &_
			"					THEN ISNULL(CONVERT(NVARCHAR, D.PAYMENTDATE, 103) ,'') " &_
			"				ELSE CONVERT(DATETIME,CAST(DatePart(dd,D.PAYMENTDATE) AS VARCHAR) + ' ' + dATENAME(mm,DATEADD(MM,1,getdate())) + ' ' + CAST(DatePart(yyyy,DATEADD(MM,1,getdate())) AS VARCHAR)) " &_
			"				END), " &_	
			"		ISNULL(DATEPART(DD,D.PAYMENTDATE) ,'') AS PAYMENTDAY, " &_			
			"		ISNULL(D.INITIALAMOUNT,0) AS INITIALAMOUNT, " &_
			"		ISNULL(D.REGULARPAYMENT,0) AS REGULARPAYMENT, " &_
			"		ISNULL(D.DDPERIOD,0) AS DDPERIOD, " &_
			"		ISNULL(D.PERIODBALANCE,0) AS PERIODBALANCE, " &_
			"		ISNULL(D.CREATIONDATE,'') AS CREATIONDATE, " &_
			"		ISNULL(D.SUSPEND,0) AS SUSPEND ," &_
			"		D.LASTUPDATED , ISNULL(H.JOURNALITEMID,0) AS ITEMID, ISNULL(H.LETTERPRINTED,0) as LETTERPRINTED " &_
			"FROM	F_DDSCHEDULE D " &_
			"	LEFT JOIN  F_DDHISTORY H ON H.DDSCHEDULEID = D.DDSCHEDULEID " &_
	 		"	AND H.DDHISTORYID = (SELECT MAX(DDHISTORYID) FROM F_DDHISTORY WHERE DDSCHEDULEID = D.DDSCHEDULEID) " &_
			"WHERE	D.DDSCHEDULEID = " & row_id
		

	'"		ISNULL(CONVERT(NVARCHAR, D.PAYMENTDATE, 103) ,'') AS PAYMENTDATE, " &_	

	
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then

		
		amend_flag = 1

		'-- change the business rule for dates
		CollectionDateDaysRule = 5

		'-- if we are amedning we send actionid to the server
		dd_action = 2

		dd_initialdate = rsLoader("INITIALDATE")
		dd_itemID = rsLoader("ITEMID")
		dd_LETTERPRINTED = rsLoader("LETTERPRINTED")
		dd_itemtype = rsLoader("ITEMTYPE")
		dd_accountname = rsLoader("ACCOUNTNAME")
		dd_sortcode = rsLoader("SORTCODE")
		dd_paymentdate = rsLoader("PAYMENTDATE")

		dd_paymentday = rsLoader("PAYMENTDAY")


		dd_initialamount = rsLoader("INITIALAMOUNT")
		dd_regularpayment = rsLoader("REGULARPAYMENT")
		dd_ddperiod = rsLoader("DDPERIOD")
		dd_accountnumber = rsLoader("ACCOUNTNUMBER")
		dd_periodbalance = rsLoader("PERIODBALANCE")
		  
		If dd_itemID <> 0 and dd_LETTERPRINTED = 0 then 
		lETTERBUTTON = " <INPUT TYPE=BUTTON NAME='btn_PRINT' VALUE='Save & Print' CLASS='RSLBUTTON' onClick='AttachLetter()'> "
		end if

		jAVASCRIPT_PAYMENTDATE = "  document.RSLFORM.txt_PAYMENTDATE.options[document.RSLFORM.txt_PAYMENTDATE.selectedIndex].text = '" & dd_paymentday & "th';"
		jAVASCRIPT_PAYMENTDATE = jAVASCRIPT_PAYMENTDATE & "document.RSLFORM.txt_REGDATE.value = '" & dd_paymentdate & "'" 
		InitialDateChangeAllowed = 0
		IF dd_initialdate <> "" THEN
			date1 = Now()
			date2 = dd_initialdate
			strDateDiff = DateDiff("d" , date2 , date1 )
			
			If strDateDiff < 0 Then
				InitialDateChangeAllowed = 1
			End If
		 
		END IF

		if dd_periodbalance = "999" then
			Javascript_paymenttype = "RSLFORM.sel_PAYMENTTYPE.value = 2;"
		else
			Javascript_paymenttype = "RSLFORM.sel_PAYMENTTYPE.value = 1;"
		end if

		dd_creationdate = rsLoader("CREATIONDATE")
		dd_suspend = rsLoader("SUSPEND")

		
		DD_LASTUPDATED = rsLoader("LASTUPDATED")
		
		if dd_initialdate <> "" then
			InitialMessage = "on (" & FormatDateTime(dd_initialdate ,2) & ")"
		end if
		
		// if first payment has been made then make initial payment and start date readonly
		if Date >= DateValue(dd_paymentdate) Then
			reedonly = "  READONLY" 
		Else
			reedonly = ""
		End If

reedonly = "  READONLY"

		sql = "SELECT TOP 1 T.PROPERTYID ,REASSESMENTDATE, OLD_DDTOTAL,  DDREGULARPAYMENT " &_
			"FROM P_TARGETRENT T " &_
			"	INNER JOIN C_TENANCY TN ON TN.PROPERTYID = T.PROPERTYID " &_
			"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = TN.TENANCYID " &_	
			"WHERE REASSESMENTDATE > GETDATE()  " &_
			"	AND T.TARGETRENTID IS NOT NULL " &_
			"	AND T.DDREGULARPAYMENT IS NOT NULL AND CT.CUSTOMERID = " & customer_id

		Call OpenRs(rsDDinfo, SQL)
		if (NOT rsDDinfo.EOF) then

			dd_INCREASEDATE = rsDDinfo("REASSESMENTDATE")
			dd_OLDTOTAL = rsDDinfo("OLD_DDTOTAL")
			dd_NEWTOTAL = rsDDinfo("DDREGULARPAYMENT")

		END IF
		CloseRs(rsDDinfo)
		
	End If
	CloseRs(rsLoader)
	

GETDATE_COLUMN = "       CASE WHEN  " &_
		"	(CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() + " & CollectionDateDaysRule  & ") THEN " &_
			"case WHEN (CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() +  " & CollectionDateDaysRule  & ") " &_
			"THEN " &_
			"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,2,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
			"ELSE " &_
			"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
			"END " &_
		"	ELSE " &_
		"	CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103) " &_
		"	END " 

Getdate_ORDERBY = " CASE WHEN " &_
		"	(CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() +  " & CollectionDateDaysRule  & ") THEN " &_
			"case WHEN (CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() +  " & CollectionDateDaysRule  & ") " &_
			"THEN " &_
			"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,2,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
			"ELSE " &_
			"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
			"END " &_
		"	ELSE " &_
		"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) " &_
		"	END ASC "

'rw Getdate_ORDERBY 
	Call BuildSelect(lstitemtype, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (1,5,6) ", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", dd_itemtype, NULL, "textbox100", NULL)

	Call BuildSelect(lstDates, "txt_PAYMENTDATE", "F_DD_PAYMENTDAY ", GETDATE_COLUMN & ", PAYMENTDAY + '' + dayalias", Getdate_ORDERBY , "Select", dd_paymentdate ,"", "textbox100", "style='width:50' onChange='document.RSLFORM.txt_REGDATE.value = document.RSLFORM.txt_PAYMENTDATE.options[document.RSLFORM.txt_PAYMENTDATE.selectedIndex].value'")
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">

	var FormFields = new Array();
	FormFields[0] = "txt_INITIALAMOUNT|Initial Amount|PCURRENCY|Y"
	FormFields[1] = "txt_PAYMENTDATE|Payment Date|DATE|Y"
	FormFields[2] = "txt_REGULARPAYMENT|Regular Payment|PCURRENCY|Y"
	FormFields[3] = "txt_DDPERIOD|Period|INTEGER|Y"
	FormFields[4] = "txt_ACCOUNTNAME|Account Name|TEXT|Y"
	FormFields[5] = "txt_ACCOUNTNUMBER|Account Number|INTEGER|Y"
	FormFields[6] = "txt_SORTCODE|Sort Code|INTEGER|Y"
	FormFields[7] = "sel_ITEMTYPE|Item Type|SELECT|Y"
	FormFields[8] = "sel_PAYMENTTYPE|Payment Type|SELECT|Y"
 
	function sel_change(caller){
	
		var caller;
		caller = RSLFORM.sel_TYPE.value;
		
		document.getElementById("DD").style.display = "none";
		document.getElementById("SO").style.display = "none";
		document.getElementById("PC").style.display = "none";
		
		document.getElementById("" + caller + "").style.display = "block";
	
	}
	
	function chk_clicked(int_what){
	
		if (int_what == 2){
			//RSLFORM.finite.checked = false;
			document.getElementById("DDPERIOD").style.display='none';
			RSLFORM.txt_DDPERIOD.value = 999;
			}
		else {
			
			//RSLFORM.infinite.checked = false;
			document.getElementById("DDPERIOD").style.display='block';
			RSLFORM.txt_DDPERIOD.value = 0;
			}
	}

	function SaveForm(str_action){
		
		// validate form
		if (!checkForm()) return false;
		if (RSLFORM.sel_PAYMENTTYPE.value == 1 && RSLFORM.txt_DDPERIOD.value < 1){		
			alert("If regular payments is chosen then you must pick at least 1 payment.")
			return false;
			}
		
		RSLFORM.target = "frm_ps";
		RSLFORM.action = "../ServerSide/ddpayments_svr.asp?action="+str_action;
		
		STATUS_DIV.style.visibility = 'visible';
		
		// amending do not overwrite creation date
		if (str_action == "AMEND")
			RSLFORM.hid_CREATIONDATE.value = "<%=dd_creationdate%>";
		// if new dd mandate then initialise preiodbalance
		RSLFORM.hid_PERIODBALANCE.value = RSLFORM.txt_DDPERIOD.value;
				
		RSLFORM.submit()
		
	}

	function sortcode(){
	
		var s1, s2, s3, st
		s1 = RSLFORM.txt_SORTCODE1.value;
		s2 = RSLFORM.txt_SORTCODE2.value;
		s3 = RSLFORM.txt_SORTCODE3.value;
	
		st = "" + s1.toString() + "" + s2.toString() + "" + s3.toString()
		RSLFORM.txt_SORTCODE.value = st;
	}
	
	function check_amount(theItem){
		alert(document.getElementsByName("theItem").value)
		if (!(theItem.value > 0)){
			ImageID = "img_" + theItem.substring(4, theItem.length)
			document.images[ImageID].src = document.images["FVW_Image"].src
			document.getElementById(ImageID).title = "Must be greater than zero"	
			document.getElementById(ImageID).style.cursor = "hand"
			}
	}
	
	function chk_sus_clicked(){

	if (!confirm("Are you sure you want to cancel these direct debit payments?"))
		return false;
			RSLFORM.hid_LASTTRANSACTIONTYPE.value = 3;
			RSLFORM.hid_SUSPEND.value = 1;
			SaveForm("AMEND")
	}
	
	function ConfirmChanges(strACTION){

<% if InitialDateChangeAllowed = 1 THEN %>
	RSLFORM.hid_INITIALDATE.value = RSLFORM.txt_PAYMENTDATE.value
<% end if %>		
		if (!checkForm()) return false;



		FirstPayment = RSLFORM.txt_INITIALAMOUNT.value
		FirstPaymentDate = RSLFORM.txt_PAYMENTDATE.value
		NextPayment = RSLFORM.txt_REGULARPAYMENT.value
		PaymentDay =  document.RSLFORM.sel_PAYMENTTYPE.options[document.RSLFORM.sel_PAYMENTTYPE.selectedIndex].value
window.showModalDialog("../popups/pDDSchedule_Confirm.asp?strACTION=" + strACTION + "&FirstPayment=" + FirstPayment  + "&FirstPaymentDate=" + FirstPaymentDate + "&NextPayment=" + NextPayment + "&PaymentDay=" + PaymentDay, self,"dialogHeight: 260px; dialogWidth: 400px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
 		//window.open("../popups/pDDSchedule_Confirm.asp?strACTION=" + strACTION + "&FirstPayment=" + FirstPayment  + "&FirstPaymentDate=" + FirstPaymentDate + "&NextPayment=" + NextPayment + "&PaymentDay=" + PaymentDay, "display","scrollbars=1,resizable=1,width=400,height=260") ;
		  }

	function AttachLetter(){
		var str_win
		str_win = "../PopUps/pDDScheduleLetterPrint.asp?scheduleid=<%=row_id%>" ;
		window.open(str_win,"display","width=407,height=180, left=200,top=200") ;
	}

</SCRIPT>
<SCRIPT FOR=window EVENT=onload LANGUAGE="JAVASCRIPT">
	
	// SPLIT SORTCODE AND ENTER INTO APPROPRIATE BOXES
	var st, s1, s2, s3
	st = new String(RSLFORM.txt_SORTCODE.value);
	s1 = st.substr(0,2);
	s2 = st.substr(2,2);
	s3 = st.substr(4,2);	
	RSLFORM.txt_SORTCODE1.value = s1;
	RSLFORM.txt_SORTCODE2.value = s2;
	RSLFORM.txt_SORTCODE3.value = s3;
	

	<%=Javascript_paymenttype%>
	
	<%=jAVASCRIPT_PAYMENTDATE %>

	// DISABLE CHECKBOXES IF DD SCHEDULE HAS STARTED
	if ("<%=reedonly%>" == "  READONLY"){
		RSLFORM.txt_PAYMENTDATE.READONLY = true;
		//RSLFORM.finite.disabled = true;
		//RSLFORM.infinite.disabled = true;
		RSLFORM.sel_PAYMENTTYPE.disabled = true;
		RSLFORM.txt_INITIALAMOUNT.READONLY = true;
		RSLFORM.txt_DDPERIOD.READONLY = true;
		}


 
		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">

<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD WIDTH=70% HEIGHT=100%  >
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71" CELLPADDING=0 CELLSPACING=0 BORDER=0 CLASS="TAB_TABLE">
				<TR STYLE='HEIGHT:5PX'><TD></TD></TR>
				<TR HEIGHT=100% ID=DD STYLE='DISPLAY:DISPLAY'>
				<TD COLSPAN=4>
					<!-- DIRECT DEBIT AREA----------------------------->
					<TABLE ALIGN=CENTER VALIGN=CENTER border=0 CELLPADDING=1 CELLSPACING=0>
						<TR>
							<TD>Item Type</TD>
							<TD COLSPAN=2><%=lstitemtype%>
								<image src="/js/FVS.gif" name="img_ITEMTYPE" width="10px" height="10px" border="0">
							</TD>
							<TD align=right><b>Direct Debit</b></TD>
						</TR>
						<TR>
							<TD>Account Name</TD>
							<TD COLSPAN=3>
								<INPUT TYPE=TEXT NAME='txt_ACCOUNTNAME' CLASS='TEXTBOX200' VALUE="<%=dd_accountname%>" maxlength=50 <%=reedonly%>>
								<image src="/js/FVS.gif" name="img_ACCOUNTNAME" width="15px" height="15px" border="0">
							</TD>
						</TR>
						<TR>
							<TD>Sort Code</TD>
							<TD><INPUT TYPE=TEXT NAME='txt_SORTCODE1' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX' <%=reedonly%>>&nbsp;-
								<INPUT TYPE=TEXT NAME='txt_SORTCODE2' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX' <%=reedonly%>>&nbsp;-
								<INPUT TYPE=TEXT NAME='txt_SORTCODE3' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX'  <%=reedonly%>>
								<INPUT TYPE=HIDDEN NAME='txt_SORTCODE' VALUE=<%=dd_sortcode%>>
								<image src="/js/FVS.gif" name="img_SORTCODE" width="15px" height="15px" border="0">
								</TD>
							<TD>Account No</TD>
							<TD><INPUT TYPE=TEXT NAME='txt_ACCOUNTNUMBER' MAXLENGTH=8 CLASS='TEXTBOX100' value=<%=dd_accountnumber%>  <%=reedonly%>>
								<image src="/js/FVS.gif" name="img_ACCOUNTNUMBER" width="15px" height="15px" border="0">
							</TD>
						</TR>
						<TR>
							<TD>Initial payment of </TD>
							<TD COLSPAN=3>
								<INPUT TYPE=TEXT NAME='txt_INITIALAMOUNT' CLASS='TEXTBOX100' STYLE='WIDTH:60PX' VALUE=<%=FormatNumber(dd_initialamount,2,-1,0,0)%> <%=reedonly%>>
								<image src="/js/FVS.gif" name="img_INITIALAMOUNT" width="10px" height="10px" border="0">
								
								<%=InitialMessage  %>
							</TD>
						</TR>
						<TR>
							<TD nowrap>Then regular payments of </TD>
							<TD COLSPAN=3>
								<INPUT TYPE=TEXT NAME='txt_REGULARPAYMENT' CLASS='TEXTBOX100' STYLE='WIDTH:60PX' VALUE=<%=FormatNumber(dd_regularpayment,2,-1,0,0)%>>
								<image src="/js/FVS.gif" name="img_REGULARPAYMENT" width="10px" height="10px" border="0"><%=UpdateMessage%>

							on 

								<%=lstDates%>								
								<image src="/js/FVS.gif" name="img_PAYMENTDATE" width="10px" height="10px" border="0">

 							of each month <INPUT TYPE=TEXT NAME='txt_REGDATE' CLASS='TEXTBOX100' STYLE='BORDER:0;WIDTH:70PX' VALUE="">

							</TD>
							
							
						</TR>
						<TR>
							<TD nowrap></TD>
							<TD COLSPAN=1>
								</TD>
							<td>Payment Type</td>
							<td VALIGN=TOP>


                					<select id="sel_PAYMENTTYPE" name="sel_PAYMENTTYPE" class="textbox200" style="width:70" onChange="chk_clicked(document.RSLFORM.sel_PAYMENTTYPE.options[document.RSLFORM.sel_PAYMENTTYPE.selectedIndex].value)" >
                   						<option value="" selected>Select</option> 
								<option value="1">Regular</option> 
		    						<option value="2">Infinite</option> 
                					 </select>
							<image src="/js/FVS.gif" name="img_PAYMENTTYPE" width="10px" height="10px" border="0">

							</td>
						</TR>

						<TR >
						
							<TD nowrap colspan=4>
<TABLE WIDTH="100%">
<TR><TD WIDTH=74%>
<DIV ID=STATUS_DIV STYLE='VISIBILITY:HIDDEN'>
							<FONT STYLE='FONT-SIZE:13PX'><B>Please wait saving...</B></FONT>
</DIV>
</TD>
<TD>
<div id=DDPERIOD name=DDPERIOD style="display:none">	
							x  
							<INPUT TYPE=TEXT NAME='txt_DDPERIOD' CLASS='TEXTBOX100' STYLE='WIDTH:30PX'  <%=reedonly%> value=<%=dd_ddperiod%> <%=reedonly%>>
							Payments <image src="/js/FVS.gif" name="img_DDPERIOD" width="10px" height="10px" border="0">
</div>
</TD>
</TR>
</TABLE>
							</td>
	
						</TR>
<% if dd_suspend  <> 1 then
 %>

<% IF dd_NEWTOTAL <> "" THEN %>
						<TR>
							<TD colspan=4>


							<table style="background-color:white;border:SOLID 1PX red">
							 <tr>
								<td>Note : The current rent payments (<%=FormatCurrency(dd_OLDTOTAL,2)%>) will<br>
							 	     increase to <%=FormatCurrency(dd_NEWTOTAL,2)%> on <%=dd_INCREASEDATE%>	
							  	</td>
							 </tr>
							</table>
							</td>
						</TR>

<% END IF %>

						<TR >
						<TD><% if amend_flag = 1 then %>
							<INPUT TYPE=BUTTON NAME='btn_CANCEL' VALUE='Cancel Direct Debit' CLASS='RSLBUTTON' onClick="chk_sus_clicked()">
						    <% end if %>	
						</td>	
															 
						<TD COLSPAN=3 ALIGN=RIGHT>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<INPUT TYPE=BUTTON NAME='btn_CANCEL' VALUE='Back' CLASS='RSLBUTTON' ONCLICK="location.href ='iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>'">
							<%=lETTERbUTTON%>								
							<%=btn_HTML%>
							
						</TR>
<% else %>

						<tr>
						<TD COLSPAN=4 ALIGN=RIGHT><%=lETTERbUTTON%></td></TR>
<%end if%>	
					</TABLE>
					<!-- ---------------------------------------------->
				</TD>
				</TR>
			</TABLE>
		</TD>
		<TD WIDTH=30% HEIGHT=100% valign=top>
			<%=str_repairs%>
		</TD>
	</TR>
	<INPUT TYPE=HIDDEN NAME='hid_TENANCYID' VALUE='<%=tenancy_id%>'>
	<INPUT TYPE=HIDDEN NAME='hid_USERID' VALUE='<%=Session("USERID")%>'>
	<INPUT TYPE=HIDDEN NAME='hid_CUSTOMERID' VALUE='<%=customer_id%>'>
	<INPUT TYPE=HIDDEN NAME='hid_DDSCHEDULEID' VALUE='<%=row_id%>'>
	<INPUT TYPE=HIDDEN NAME='hid_CREATIONDATE' VALUE='<%=DATE%>'>
	<INPUT TYPE=HIDDEN NAME='hid_PERIODBALANCE' '<%=dd_periodbalance%>'>
	<INPUT TYPE=HIDDEN NAME='hid_SUSPEND' VALUE=<%=dd_suspend%>>
	<INPUT TYPE=HIDDEN NAME='hid_LASTTRANSACTIONTYPE' VALUE=<%=dd_action%>>
	<INPUT TYPE=HIDDEN NAME='hid_INITIALDATE' VALUE="<%=dd_initialdate %>">
</FORM>
</TABLE>

<iframe name=frm_ps width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>	
	
	
	
	
	