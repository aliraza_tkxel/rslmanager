<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 10
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName,searchSQL
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	searchFROM 	= Replace(Request("txt_FROM"),"'","''")
	searchTO 	= Replace(Request("txt_TO"),"'","''")
	searchAction 	= Replace(Request("sel_ACTION"),"'","''")
	searchTenancy 	= Replace(Request("txt_TENANCY"),"'","''")


	searchSQL = ""
	if searchFROM 	<> "" Then
		searchSQL = searchSQL & " AND DD.ORIG_PAYMENTDATE >= '" & searchFROM & " 00:00' "
		MAXSQL = MAXSQL &  " AND DD.ORIG_PAYMENTDATE >= '" & searchFROM & " 00:00' "
	End If

	
	if searchTO <> "" Then
		searchSQL = searchSQL & " AND DD.ORIG_PAYMENTDATE <= '" & searchTO & " 23:59' "
		MAXSQL = MAXSQL &  " AND DD.ORIG_PAYMENTDATE <= '" & searchTO & " 23:59' "
	End If

	
	if searchAction <> "" Then
		searchSQL = searchSQL & " AND DD.TRANSACTIONTYPE = " & searchAction & " "
		MAXSQL = MAXSQL & " AND TRANSACTIONTYPE = '" & searchAction & "' "
	End If

	
	if searchTenancy <> "" Then
		searchSQL = searchSQL & " AND DD.TENANCYID = " & searchTenancy & " "
		MAXSQL = MAXSQL & " AND TENANCYID = " & searchTenancy & " "
	End If

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "DDMain.asp"
	MaxRowSpan = 10
	
	
	OpenDB()
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' NONE		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Dim strSQL, rsSet, intRecord 

			intRecord = 0
			str_data = ""
			strSQL	=	"SELECT  DD.DDTIMESTAMP AS CREATIONDATE, " &_
				"	E.FIRSTNAME + ' ' + E.LASTNAME AS RECORDEDBY, " &_
				"	JA.DESCRIPTION AS ACTIONTYPE, " &_
				"	DD.CUSTOMERID, " &_
				"	ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'') AS CUSTOMERNAME, " &_
				"	DD.TENANCYID, " &_
				"	ISNULL(DD.OLD_REGULARPAYMENT,0) AS DD_PREVIOUS, " &_
				"	ISNULL(DD.ORIG_REGULARPAYMENT,0) AS DD_NEWDD, " &_
				"	ISNULL(DD.ORIG_REGULARPAYMENT,0) - ISNULL(DD.OLD_REGULARPAYMENT,0) AS DD_DIFFERENCE, " &_
				"	DD.ORIG_PAYMENTDATE, " &_
				"	DD.LETTERPRINTED " &_
				"FROM 	F_DDHISTORY DD " &_
				"	LEFT JOIN C_JOURNALACTION JA ON JA.JOURNALACTIONID = DD.TRANSACTIONTYPE " &_
				"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = DD.CUSTOMERID " &_
				"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = DD.USERID " &_
				"WHERE DD.TRANSACTIONTYPE IS NOT NULL  " & searchSQL &_
				" ORDER BY  DD.DDhistoryid desc "


' if multiple records shouldnt be displayed use this
'" DD.DDHISTORYID = 	( " &_
'				"				SELECT MAX(DDHISTORYID)  " &_
'				"				FROM F_DDHISTORY  " &_
''				"				WHERE DDSCHEDULEID = DD.DDSCHEDULEID " & MAXSQL &_
'				"				)


			'rw strSQL & "<BR>"
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
									
				CREATIONDATE = rsSet("CREATIONDATE") 
				RECORDEDBY = rsSet("RECORDEDBY")
				ACTIONTYPE = rsSet("ACTIONTYPE")
				CUSTOMERID= rsSet("CUSTOMERID")
				CUSTOMERNAME  = rsSet("CUSTOMERNAME")
				TENANCYID = rsSet("TENANCYID")
				DD_PREVIOUS  = rsSet("DD_PREVIOUS")
				DD_NEWDD = rsSet("DD_NEWDD")
				DD_DIFFERENCE = rsSet("DD_DIFFERENCE")

				IF DD_DIFFERENCE < 0 THEN 
					DD_DIFFERENCE = "<font color='red'>" & FormatCurrency(DD_DIFFERENCE,2) & "</font>"
				else
					DD_DIFFERENCE = FormatCurrency(DD_DIFFERENCE,2)
				end if

				ORIG_PAYMENTDATE =  rsSet("ORIG_PAYMENTDATE")
				LETTERPRINTED=  rsSet("LETTERPRINTED")
				

				IF LETTERPRINTED=  1 THEN
					LetterPrinted  = "<img name=img_letterprinted src='\myImages\tick.gif'>"
				else
					LetterPrinted  = "<img name=img_letterprinted src='\myImages\x.gif'>"
				END IF 
				'MAINTTOTAL 		= rsSet("RECORDEDBY")
				'MANAGETOTAL 	= rsSet("ALL_MANAGETOTAL")
				'FURNTOTAL 		= rsSet("ALL_FURNTOTAL")
				'SERVICESTOTAL 	= rsSet("ALL_SERVICESTOTAL")
				'GROSSCOST 		= (MAINTTOTAL + MANAGETOTAL + FURNTOTAL + SERVICESTOTAL)
				
					STRrow  =  STRrow & "<tr>" &_
						"  <td align='left' >" & CREATIONDATE  & "</td>" &_
						"  <td align='left' >" & RECORDEDBY  & "</td>" &_
						"  <td align='left' >" & ACTIONTYPE  & "</td>" &_
						"  <td align='left' ><a href='/Customer/CRM.asp?CustomerID=" & CUSTOMERID & "' target='blank'>" & CUSTOMERNAME   & "</a></td>" &_
						"  <td align='left' >" & TENANCYID   & "</td>" &_
						"  <td align='left'>" & FormatCurrency(DD_PREVIOUS,2)   & "</td>" &_
						"  <td align='left' >" & FormatCurrency(DD_NEWDD,2) & "</td>" &_
						"  <td align='left' >" & DD_DIFFERENCE  & "</td>" &_
						"  <td align='left' >" & ORIG_PAYMENTDATE & "</td>" &_
						"  <td align='center' >" & LETTERPRINTED & "</td>" &_
						"</tr>"
	
					str_data = STRrow
						'bottom_MAINTTOTAL		= bottom_MAINTTOTAL + MAINTTOTAL
						'bottom_MANAGETOTAL		= bottom_MANAGETOTAL + MANAGETOTAL
						'bottom_FURNTOTAL		= bottom_FURNTOTAL + FURNTOTAL
						'bottom_SERVICESTOTAL	= bottom_SERVICESTOTAL + SERVICESTOTAL
						'bottom_GROSSCOST		= bottom_GROSSCOST + GROSSCOST
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next




				strSQL_totals	=	"SELECT sum(ISNULL(DD.OLD_REGULARPAYMENT,0)) AS DD_PREVIOUS_total, " &_
						"		sum(ISNULL(DD.ORIG_REGULARPAYMENT,0)) AS DD_NEWDD_total, " &_
						"		sum(ISNULL(DD.ORIG_REGULARPAYMENT,0) - ISNULL(DD.OLD_REGULARPAYMENT,0)) AS DD_DIFFERENCE_total " &_
						"FROM 	F_DDHISTORY DD " &_
						"	LEFT JOIN C_JOURNALACTION JA ON JA.JOURNALACTIONID = DD.TRANSACTIONTYPE " &_
						"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = DD.CUSTOMERID " &_
						"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = DD.USERID " &_
						"WHERE DD.TRANSACTIONTYPE IS NOT NULL  " & searchSQL
		
				
				Call OpenRs(rsSet_totals, strSQL_totals	)
			' rw strSQL_totals
					DD_PREVIOUS_total 	= rsSet_totals("DD_PREVIOUS_total")
					DD_NEWDD_total 		=  rsSet_totals("DD_NEWDD_total")
					DD_DIFFERENCE_total 	=  rsSet_totals("DD_DIFFERENCE_total")

					STRrow  =  STRrow & "<tr><td colspan=10 STYLE='BORDER-TOP:2PX SOLID #133E71'></td></tr><tr >" &_
						"  <td align='left' ></td>" &_
						"  <td align='left' ></td>" &_
						"  <td align='left' ></td>" &_
						"  <td align='left' ></td>" &_
						"  <td align='left' ></td>" &_
						"  <td align='left'></td>" &_
						"  <td align='left' ><b>Total:</b></td>" &_
						"  <td align='left' ><b>" & FormatCurrency(DD_DIFFERENCE_total,2)  & "</b></td>" &_
						"  <td align='left' ></td>" &_
						"  <td align='center' ></td>" &_
						"</tr>"
	
					str_data = STRrow
			 
				CloseRs(rsSet_totals)
				
				str_data = str_data & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No Records Found !</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL_Non_Logo.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT="0" LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0">

<form name = thisForm method=get >

  <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0 >
  </TABLE>
  <TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=2  STYLE="BORDER-COLLAPSE:COLLAPSE;" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=17% valign=top><strong>Creation Date</strong></TD>
      <TD CLASS='NO-BORDER' WIDTH=16% valign=top>&nbsp;<B>Recorded By </B></TD>
      <TD CLASS='NO-BORDER' WIDTH=16% valign=top>&nbsp;<strong>Action</strong>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=17% valign=top><strong>Customer</strong>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=11% valign=top><strong>Tenancy No </strong></TD>
      <TD WIDTH=7% align="center"  valign=top CLASS='NO-BORDER'><strong>Previous DD</strong></TD>
      <TD CLASS='NO-BORDER' WIDTH=7%  valign=top>&nbsp;<b>New DD</b></TD>
<TD CLASS='NO-BORDER' WIDTH=7% valign=top>&nbsp;<b>Diff</b></TD>
<TD CLASS='NO-BORDER' WIDTH=12% valign=top>&nbsp;<b>Collection</b></TD>
<TD CLASS='NO-BORDER' WIDTH=4% valign=top>&nbsp;<b>Letter</b></TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=12 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
</form>

</BODY>
</HTML>