<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, UpdateID, tenancy_id, row_id,sched_id 
	
	OpenDB()

	tenancy_id = Request.Form("hid_TENANCYID")
	customer_id = Request.Form("hid_CUSTOMERID")	
	action = Request("action")
	
	Dim ID
	Dim DataFields   (15)
	Dim DataTypes    (15)
	Dim ElementTypes (15)
	Dim FormValues   (15)
	Dim FormFields   (15)

	UpdateID	  = "hid_DDSCHEDULEID"
	FormFields(0) = "txt_ACCOUNTNAME|TEXT"
	FormFields(1) = "txt_SORTCODE|TEXT"
	FormFields(2) = "txt_ACCOUNTNUMBER|TEXT"
	FormFields(3) = "txt_INITIALAMOUNT|CURRENCY"
	FormFields(4) = "txt_PAYMENTDATE|DATE"
	FormFields(5) = "txt_REGULARPAYMENT|CURRENCY"
	FormFields(6) = "txt_DDPERIOD|INTEGER"
	FormFields(7) = "hid_TENANCYID|INTEGER"
	FormFields(8) = "sel_ITEMTYPE|INTEGER"
	FormFields(9) = "hid_CREATIONDATE|DATE"
	FormFields(10) = "hid_PERIODBALANCE|INTEGER"
	FormFields(11) = "hid_SUSPEND|INTEGER"
	FormFields(12) = "hid_CUSTOMERID|INTEGER"
	FormFields(13) = "hid_USERID|INTEGER"
	FormFields(14) = "hid_LASTTRANSACTIONTYPE|INTEGER"	
	FormFields(15) = "hid_INITIALDATE|DATE"	
	do_dd()
		
	CloseDB()

	// deals with Direct debit payments
	Function do_dd()
		
		Dim str_front_sql
		If action = "NEW" Then
			new_record()
		ElseIf action = "AMEND" Then
			amend_record()
		End If	
	
	End Function
	
	Function amend_record()

		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE F_DDSCHEDULE " & strSQL & " WHERE DDSCHEDULEID = " & ID
		
		Response.Write SQL
		Conn.Execute SQL, recaffected

		SQL = "EXEC C_DDSCHEDULE_JOURNALITEM_SP " & Request("hid_CUSTOMERID") & "," & Session("UserID") & ",2," & ID
		Conn.Execute(SQL)
		sched_id = id
		run_dd_job()
				
	End Function

	
	// creates all kinds of new records depending on SQL
	Function new_record()

		
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "set nocount on;INSERT INTO F_DDSCHEDULE " & strSQL & ";SELECT SCOPE_IDENTITY() AS SCHEDULEID;"
		Response.Write SQL
		set rsSet = Conn.Execute(SQL)
		sched_id = rsSet("SCHEDULEID")
		
		SQL = "EXEC C_DDSCHEDULE_JOURNALITEM_SP " & Request("hid_CUSTOMERID") & "," & Session("UserID") & ",1," & sched_id 
		Conn.Execute(SQL)
 
		run_dd_job()
		
	End Function

	// runs the job which controls the DD rent account entries
	Function run_dd_job()
		
		Set Rs = Server.CreateObject("ADODB.Command")
		Set Rs.activeconnection = Conn
		Rs.commandtype = 4 
		Rs.commandtext = "F_AUTO_DDSCHEDULE_ENTRY"
		Rs.execute
			
	End Function
	
%>
<html>
<head></head>
<script language=javascript>
	function ReturnData(){
		
		parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>DD Details Saved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</B></FONT>";
		parent.location.href = "../iFrames/iDDPayment.asp?rowid=<%=sched_id%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
		//parent.location.reload()
		}
</script>
<body onload="ReturnData()">
</body>
</html>