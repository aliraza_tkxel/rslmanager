<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Version.aspx.vb" Inherits="Version" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Build Information</title>
    <style type="text/css">
    body
    {
        font-family:arial;
        font-size:10px;
    }
    h1
    {
        color:white;
        font-size:14px;
        padding:15px;
    }
    .trHeader
    {
         background-color: lightgrey;
        height:20px;
    }
    .trHeader *
    {
        height:20px;
        font-weight:bold;
        padding:5px;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                <tr>
                    <td style="height: 10px; background-color: #cc0000">
                        <h1>
                            AHV_RSLManager Build Information</h1>
                    </td>
                </tr>
            </table>
            <table>
                <tr class="trHeader">
                    <td style="width: 148px;">
                        Release Version</td>
                    <td style="width: 100px;">
                        <b>
                            <% Response.WriteFile("RELEASE_VERSION.txt")%>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="VersionHistory\<% Response.WriteFile("RELEASE_VERSION.txt") %>.pdf" target="_blank">
                            View Software Release Form</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="ReleaseHistory.aspx">View Release History</a>
                    </td>
                </tr>
            </table>
            <br />
                        <table>
                <tr class="trHeader">
                    <td style="width: 148px;">
                        Build Info</td>
                    <td style="width: 100px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 148px">
                        Integration ID:</td>
                    <td style="width: 100px">
                        <asp:Label ID="lblBuildProject" runat="Server" /></td>
                </tr>
                <tr>
                    <td style="width: 148px">
                        Build Date:</td>
                    <td style="width: 100px">
                        <asp:Label ID="lblBuildDate" runat="Server" />
                        <asp:Label ID="lblBuildTime" runat="Server" /></td>
                </tr>
                <tr>
                    <td style="width: 148px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 148px">
                        Build Issue:</td>
                    <td style="width: 100px">
                        <asp:Label ID="lblBuildNumber" runat="Server" /></td>
                </tr>
                <tr>
                    <td style="width: 148px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 148px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
            </table>
            <br />
            <asp:HyperLink runat="server" ID="hlkBack" NavigateUrl="~/bhaintranet/CustomPages/MyWhiteboard.aspx"
                Text="<< Back">
            </asp:HyperLink>
        </div>
    </form>
</body>
</html>
