<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
CompanyID = Request("CompanyID")
If (CompanyID = "") Then
	CompanyID = -1
End If

APPROVED = ""

SQL = "SELECT * FROM S_FINANCIAL WHERE ORGID = " & CompanyID
Call OpenDB()
Call OpenRs(rsLoader, SQL)
ACTION = "NEW"
If (NOT rsLoader.EOF) Then
	L_CURRENTTURNOVER = rsLoader("CURRENTTURNOVER")
	L_LATESTACCOUNTDATE = rsLoader("LATESTACCOUNTDATE")
	L_REVIEWEDBY = rsLoader("REVIEWEDBY")
	L_DATEREVIEWED = rsLoader("DATEREVIEWED")
	L_TURNOVERLIMIT = rsLoader("TURNOVERLIMIT")
	L_CONTRACTLIMIT = rsLoader("CONTRACTLIMIT")
	L_APPROVEDBY = rsLoader("APPROVEDBY")
	If (L_APPROVEDBY <> "") Then
		approveddisabled = " disabled"
	End If
	L_DATEAPPROVED = rsLoader("DATEAPPROVED")
	APPROVED = rsLoader("APPROVALSTATUS")
	ACTION = "AMEND"
End If
Call CloseRs(rsLoader)

If (L_APPROVEDBY <> "") Then
	Call BuildSelect(lstAPPROVEDBY, "sel_APPROVEDBYDUMMY", "E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID WHERE GRADE >= 12 AND E.EMPLOYEEID ="  & SESSION("USERID"), "E. EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", L_APPROVEDBY, NULL, "textbox200", " onchange=""Approve()"" disabled=""disabled"" tabindex=""1"" ")
	txt_DATEAPP = "<input type=""text"" name=""txt_DATEAPPROVEDDUMMY"" class=""textbox200"" maxlength=""50"" value=""" & L_DATEAPPROVED & """ tabindex=""1"" DISABLED>"
	lst_ACTUALDATA = "<input type=""hidden"" name=""sel_APPROVEDBY"" id=""sel_APPROVEDBY"" value=""" & L_APPROVEDBY & """ />"
	lst_DATEDATA = "<input type=""hidden"" name=""txt_DATEAPPROVED"" id=""txt_DATEAPPROVED"" value=""" & L_DATEAPPROVED & """ />"
Else
	txt_DATEAPP = "<input type=""text"" name=""txt_DATEAPPROVED"" id=""txt_DATEAPPROVED"" class=""textbox200"" maxlength=""50"" value=""" & L_DATEAPPROVED & """ tabindex=""1"" readonly=""readonly"" />"
	Call BuildSelect(lstAPPROVEDBY, "sel_APPROVEDBY", "E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID WHERE GRADE >= 12 AND E.EMPLOYEEID ="  & SESSION("USERID"), "E. EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", L_APPROVEDBY, NULL, "textbox200", " onchange=""Approve()"" tabindex=""1"" ")
End If

Call BuildSelect(lstREVIEWEDBY, "sel_REVIEWEDBY", "E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID WHERE GRADE >= 12 ", "E. EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", L_REVIEWEDBY, NULL, "textbox200", " tabindex=""1"" ")

Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Suppliers --&gt; Tools --&gt; Financial Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_CURRENTTURNOVER|Current Turnover|CURRENCY|Y"
        FormFields[1] = "txt_LATESTACCOUNTDATE|Latest Account Date|DATE|N"
        FormFields[2] = "sel_REVIEWEDBY|Reviewed By|SELECT|N"
        FormFields[3] = "txt_DATEREVIEWED|Date of Review|DATE|N"
        FormFields[4] = "txt_TURNOVERLIMIT|Turnover Limit|CURRENCY|N"
        FormFields[5] = "txt_CONTRACTLIMIT|Contract Limit|CURRENCY|N"
        FormFields[6] = "sel_APPROVEDBY|Approved By|SELECT|N"
        FormFields[7] = "txt_DATEAPPROVED|Date of Approval|DATE|N"

        function SaveForm() {
            if (!checkForm()) return false
            document.RSLFORM.action = "ServerSide/Financial_svr.asp"
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
        }

        function Approve() {
            if (document.getElementById("sel_APPROVEDBY").options[document.getElementById("sel_APPROVEDBY").selectedIndex].value != "") {
                document.getElementById("PasswordLabel").style.display = "block"
                document.getElementById("txt_PASSWORDDATEAPPROVED").style.display = "block"
                FormFields[7] = "txt_DATEAPPROVED|Date of Approval|DATE|Y"
                document.getElementById("txt_DATEAPPROVED").value = "<%=FormatDateTime(Date,2)%>"
                document.getElementById("txt_DATEAPPROVED").readOnly = false
                FormFields[8] = "txt_PASSWORDDATEAPPROVED|Approval Password|TEXT|Y"
            }
            else {
                document.getElementById("PasswordLabel").style.display = "none"
                FormFields[7] = "txt_DATEAPPROVED|Date of Approval|DATE|N"
                FormFields.length = 7
                document.getElementById("txt_PASSWORDDATEAPPROVED").style.display = "none"
                document.getElementById("txt_DATEAPPROVED").readOnly = true
                document.getElementById("txt_DATEAPPROVED").value = ""
            }
        }
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="Company.asp?OrgID=<%=CompanyID%>">
                    <img name="tab_main_details" src="images/tab_org_info.gif" width="127" height="20"
                        border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Address.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" src="images/tab_address.gif" width="72" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Financial.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" src="images/tab_financial-over.gif" width="79" height="20"
                        border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_attributes" src="images/tab_contact-tab_address_ove.gif" width="70"
                        height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Scope.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_warranties" src="images/tab_scope.gif" width="61" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <img name="tab_utilities" src="images/TabEndClosed.gif" width="8" height="20" border="0"
                    alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companyname%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/myImages/spacer.gif" width="333" height="1" alt="" /></td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <table cellspacing="2" style="height: 90%; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">
        <tr>
            <td>
                Current Turnover:
            </td>
            <td>
                <input type="text" name="txt_CURRENTTURNOVER" id="txt_CURRENTTURNOVER" class="textbox200"
                    maxlength="50" value="<%=L_CURRENTTURNOVER%>" tabindex="1" />
            </td>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="/js/FVS.gif" name="img_CURRENTTURNOVER" id="img_CURRENTTURNOVER" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td nowrap="nowrap">
                            <font color="red">
                                <%=Request("Text")%></font>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="100%">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Latest Account Date:
            </td>
            <td>
                <input type="text" name="txt_LATESTACCOUNTDATE" id="txt_LATESTACCOUNTDATE" class="textbox200"
                    maxlength="50" value="<%=L_LATESTACCOUNTDATE%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LATESTACCOUNTDATE" id="img_LATESTACCOUNTDATE" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <tr>
                <td>
                    Reviewed By:
                </td>
                <td>
                    <%=lstREVIEWEDBY%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_REVIEWEDBY" id="img_REVIEWEDBY" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Date of Review:
                </td>
                <td>
                    <input type="text" name="txt_DATEREVIEWED" id="txt_DATEREVIEWED" class="textbox200"
                        maxlength="50" value="<%=L_DATEREVIEWED%>" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_DATEREVIEWED" id="img_DATEREVIEWED" width="15px"
                        height="15px" border="0" alt="" />
                </td>
                <tr>
                    <td>
                        Turnover Limit:
                    </td>
                    <td>
                        <input type="text" name="txt_TURNOVERLIMIT" id="txt_TURNOVERLIMIT" class="textbox200"
                            maxlength="50" value="<%=L_TURNOVERLIMIT%>" tabindex="1" />
                    </td>
                    <td>
                        <img src="/js/FVS.gif" name="img_TURNOVERLIMIT" id="img_TURNOVERLIMIT" width="15px"
                            height="15px" border="0" alt="" />
                    </td>
                    <tr>
                        <td>
                            Contract Limit:
                        </td>
                        <td>
                            <input type="text" name="txt_CONTRACTLIMIT" id="txt_CONTRACTLIMIT" class="textbox200"
                                maxlength="50" value="<%=L_CONTRACTLIMIT%>" tabindex="1" />
                        </td>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_CONTRACTLIMIT" id="img_CONTRACTLIMIT" width="15px"
                                            height="15px" border="0" alt="" />
                                    </td>
                                    <td>
                                        <div id="PasswordLabel" style="display: none">
                                            Please enter your password</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Approved By:
                        </td>
                        <td>
                            <%=lstAPPROVEDBY%><%=lst_ACTUALDATA%><%=lst_DATEDATA%>
                        </td>
                        <td nowrap="nowrap" width="350">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_APPROVEDBY" id="img_APPROVEDBY" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td>
                                        <input type="PASSWORD" name="txt_PASSWORDDATEAPPROVED" id="txt_PASSWORDDATEAPPROVED"
                                            class="textbox200" maxlength="50" style="display: none" value="" tabindex="1" />
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_PASSWORDDATEAPPROVED" id="img_PASSWORDDATEAPPROVED"
                                            width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date of Approval:
                        </td>
                        <td>
                            <%=txt_DATEAPP%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DATEAPPROVED" id="img_DATEAPPROVED" width="15" height="15"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            <input type="hidden" name="hid_APPROVED" id="hid_APPROVED" value="<%=APPROVED%>" />
                            <input type="hidden" name="hid_ACTION" id="hid_ACTION" value="<%=ACTION%>" />
                            <input type="hidden" name="hid_CompanyID" id="hid_CompanyID" value="<%=CompanyID%>" />
                            <input type="hidden" name="hid_ORGID" id="hid_ORGID" value="<%=CompanyID%>" />
                            <input type="button" name="BtnSave" id="BtnSave" title="SAVE" value="SAVE" class="RSLButton"
                                onclick="SaveForm()" style="cursor: pointer" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="100%">
                        </td>
                    </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
