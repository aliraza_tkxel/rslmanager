<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION,ContactTypeRadio,RadioCount

	RadioCount = 0
	ACTION = "NEW"
	CompanyID = Request("CompanyID")

If (CompanyID = "") Then CompanyID = -1 End If

Call OpenDB()

SQL=" DELETE FROM S_SCOPETOAPPLIANCE "&_
	" WHERE SCOPEID NOT IN (SELECT SCOPEID FROM S_SCOPE) "

Conn.Execute SQL

SQL = "SELECT NAME FROM S_ORGANISATION " &_
		"WHERE ORGID = " & CompanyID 

Call OpenRs(rsName, SQL)
If (NOT rsName.EOF) Then
	l_companyname = rsName("NAME")
End If
Call CloseRS(rsName)

SQL="	SELECT S.SCOPEID, S.CONTRACTNAME, APPROVALSTATUS AS APPROVED, A.DESCRIPTION AS AREAOFWORK, "&_
    "        S.DATEAPPROVED,E.LOCATION,P.SCHEMENAME, B.BLOCKNAME,ISNULL(ESTIMATEDVALUE,0) AS ESTIMATEDVALUE,SC.COSTTYPE "&_
	"	FROM S_SCOPE S "&_
	"	LEFT JOIN S_AREAOFWORK A ON S.AREAOFWORK = A.AREAOFWORKID "&_
	"	LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID "&_
	"	LEFT JOIN E_PATCH E ON E.PATCHID=SS.PATCHID "&_
	"	LEFT JOIN P_SCHEME P ON P.SCHEMEID=SS.SCHEMEID "&_
    "	LEFT JOIN P_BLOCK B ON B.BLOCKID= SS.BLOCKID "&_
	"	LEFT JOIN S_COSTTYPE SC ON SC.COSTTYPEID=S.COSTTYPEID "&_
	"	WHERE S.ORGID = " & CompanyID & " ORDER BY CONTRACTNAME"

Call OpenRs(rsLoader, SQL)
ContactString = ""
If (NOT rsLoader.EOF) Then
	While NOT rsLoader.EOF

		ContactString = ContactString & "<tr onclick=""LoadForm(" & rsLoader("SCOPEID") & ")"">"&_
					"<td width=""157"" nowrap=""nowrap"" style=""cursor:pointer"" >" & rsLoader("CONTRACTNAME") & "</td>" &_
					"<td width=""105"" nowrap=""nowrap"" style=""cursor:pointer"" >" & rsLoader("AREAOFWORK") & "</td>" &_
					"<td width=""66"" nowrap=""nowrap"" style=""cursor:pointer"" >" & rsLoader("LOCATION") & "</td>" &_
					"<td width=""142"" style=""cursor:pointer"" >" & rsLoader("SCHEMENAME") & "</td>"&_
                    "<td width=""130"" style=""cursor:pointer"" >" & rsLoader("BLOCKNAME") & "</td>"&_
					"<td width=""80"" nowrap=""nowrap"" style=""cursor:pointer"" >" & rsLoader("COSTTYPE") & "</td>"&_
					"<td width=""80"" nowrap=""nowrap"" style=""cursor:pointer"" >" & rsLoader("ESTIMATEDVALUE") & "</td>"&_
					"</tr>"

		rsLoader.MoveNext
	Wend
Else
	ContactString = ContactString & "<tr><td colspan=""6"" align=""center"">No Contracts exist for this company</td></tr>"
End If
Call CloseRs(rsLoader)

SQL="SELECT COSTTYPEID,COSTTYPE FROM S_COSTTYPE"
Call OpenRs(rsCosttype, SQL)
ContactTypeRadio=""
If (NOT rsCosttype.EOF) Then
	While NOT rsCosttype.EOF
		ContactTypeRadio=ContactTypeRadio & "<input id=""radio" & RadioCount & """ type=""radio"" name=""radio_COSTTYPEID"" value=""" & rsCosttype("COSTTYPEID") & """ onclick=""GSCost();""/><label for=""perprop"">" & rsCosttype("COSTTYPE")& "</label>"
		RadioCount=RadioCount+1
		rsCosttype.MoveNext
	Wend
End If

Call CloseRs(rsCosttype)


Dim FISCALYEAR_STARTDATE
SQL="SELECT YSTART FROM F_FISCALYEARS WHERE YSTART <= CONVERT(VARCHAR,GETDATE(),103)  AND YEND >= CONVERT(VARCHAR,GETDATE(),103)"
Call OpenRs(rsFISCALYEAR_STARTDATE, SQL)
If (NOT rsFISCALYEAR_STARTDATE.EOF) Then
	FISCALYEAR_STARTDATE = rsFISCALYEAR_STARTDATE(0)
End If
Call CloseRs(rsFISCALYEAR_STARTDATE)

Call BuildSelect(lstAREAOFWORK, "sel_AREAOFWORK", "S_AREAOFWORK", "AREAOFWORKID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='margin-left:0px;' tabindex=""1"" onchange=""GSDisplay();"" ")
Call BuildSelect_SP_DROPDOWN(lstPATCH,"sel_PATCH","GS_PATCH_AVAILABLE","Please Select",Null,Null,"textbox200","style='margin-left:0px;' tabindex=""1"" onchange=""GetSchemes()"" ")
Call BuildSelect_SP_DROPDOWN(lstSCHEME,"sel_SCHEME","GS_SCHEMES_AVAILABLE","All",Null,Null,"textbox200","style='margin-left:0px' tabindex=""1"" disabled=""disabled"" onchange=""GetBlockAndEstimateCost()"" ")
Call BuildSelect_SP_DROPDOWN(lstBLOCK,"sel_BLOCK","GS_BLOCK_AVAILABLE","All",Null,Null,"textbox200","style='margin-left:0px' tabindex=""1"" disabled=""disabled"" onchange=""GetEstimatedCost()"" ")
Call BuildSelect(lstAPPLIANCE, "sel_APPLIANCE", "GS_APPLIANCE_TYPE", "APPLIANCETYPEID, APPLIANCETYPE", "APPLIANCETYPE", "Please Select", NULL, NULL, "textbox200", " style=""margin-left:13px;"" tabindex=""1"" ")

Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Suppliers --&gt; Tools --&gt; Scope Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
        #bottom_left
        {
            width: 50%;
            float: left;
            margin-right: 3px;
            margin-top: 10px;
        }
        #bottom_right
        {
            background-color: #ffffff;
            width: 95%;
            padding-top: 5px;
        }
        .margintenleft
        {
            margin-left: 10px;
        }
        .rowspacing
        {
            margin: 6 0 6 0;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_CONTRACTNAME|Name of Contract|TEXT|Y"
        FormFields[1] = "sel_AREAOFWORK|Area of Work|SELECT|Y"
        FormFields[2] = "txt_ESTIMATEDVALUE|Estimated Value|CURRENCY|Y"
        FormFields[3] = "txt_RENEWALDATE|End Date|DATE|Y"
        FormFields[4] = "txt_DESCRIPTION|Description|TEXT|N"
        FormFields[5] = "sel_SCHEME|Scheme|SELECT|N"
        FormFields[6] = "sel_PATCH|Patch|SELECT|N"
        FormFields[7] = "txt_PROPERTY|Property|CURRENCY|N"
        FormFields[8] = "sel_APPLIANCE|Appliance type|SELECT|N"
        FormFields[9] = "txt_APPLIANCE_COST|Appliance|CURRENCY|N"
        FormFields[10] = "txt_STARTDATE|Start Date|DATE|Y"
        FormFields[11] = "txt_FINANCIALDATE|Financial DATE|DATE|Y"
        FormFields[12] = "sel_BLOCK|Block|SELECT|N"

        function long_date(str_date) {
            var arrMon = new Array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            var date_array = str_date.split("/");
            return new String(date_array[0] + " " + arrMon[parseInt(date_array[1], 10)] + " " + date_array[2])
        }

        function SetButtons(Code) {
            document.getElementById("AddButton").disabled = false;
            if (Code == "AMEND") {
                document.getElementById("AddButton").value = "AMEND"
                document.getElementById("AddButton").onclick = function () { AmendForm() }
            }
            else
                if (Code == "DISABLED") {
                    document.getElementById("AddButton").value = "AMEND"
                    document.getElementById("AddButton").disabled = true;
                }
                else {
                    document.getElementById("AddButton").value = "ADD"
                    document.getElementById("AddButton").onclick = function () { SaveForm() }
                }
        }
        // Code added by Tkxel.
        // 24 November 2010
        // functiion to check the mandatory controls for External Agency Area of Work
        function CheckExternalAgencyControls() {
            var index = document.getElementById("sel_AREAOFWORK").selectedIndex;
            var text = document.getElementById("sel_AREAOFWORK").options[index].text;
            if (text == "External Agency") {
                FormFields[2] = "txt_ESTIMATEDVALUE|Estimated Value|CURRENCY|N"
                // FormFields[3] = "txt_RENEWALDATE|End Date|DATE|N"
                FormFields[4] = "txt_DESCRIPTION|Description|TEXT|N"
                // FormFields[5] = "sel_SCHEME|Scheme|SELECT|N"
                FormFields[6] = "sel_PATCH|Patch|SELECT|N"
                //FormFields[7] = "txt_PROPERTY|Property|CURRENCY|N"
                //FormFields[8] = "sel_APPLIANCE|Appliance type|SELECT|N"
                //FormFields[9] = "txt_APPLIANCE_COST|Appliance|CURRENCY|N"
                FormFields[10] = "txt_STARTDATE|Start Date|DATE|N"
                FormFields[11] = "txt_FINANCIALDATE|Financial DATE|DATE|N"
            }
        }

        function AmendForm() {
            document.getElementById("hid_Action").value = "AMEND"

            var btnCOSTTYPE = valButton(RSLFORM.radio_COSTTYPEID);
            if (btnCOSTTYPE == 1) {
                FormFields[7] = "txt_PROPERTY|Property|CURRENCY|Y"
            }
            else {
                FormFields[7] = "txt_PROPERTY|Property|CURRENCY|N"
            }
            // Code added by Tkxel to add the External Agency Contract.
            //  Date: 24 November 2010
            CheckExternalAgencyControls();

            if (!checkForm()) return false;

            var txt_TODAY = new Date(long_date('<%=FormatDateTime(FISCALYEAR_STARTDATE,2)%>'))
            var str_today = '<%=FormatDateTime(FISCALYEAR_STARTDATE,2)%>'
            var txt_STARTDATE = new Date(long_date(document.getElementById("txt_STARTDATE").value))
            var txt_ENDDATE = new Date(long_date(document.getElementById("txt_RENEWALDATE").value))

            if (!(txt_STARTDATE == "")) {
                if (txt_STARTDATE < txt_TODAY) {
                    alert("You cannot enter a Start Date before (" + long_date(str_today) + ") ");
                    return false;
                }
            }

            if (!(txt_ENDDATE == "")) {
                if (txt_ENDDATE < txt_TODAY) {
                    alert("You cannot enter a End Date before (" + long_date(str_today) + ") ");
                    return false;
                }
            }

            if (txt_ENDDATE <= txt_STARTDATE) {
                alert("You cannot enter a End Date before (" + long_date(document.getElementById("txt_STARTDATE").value) + ") ");
                return false;
            }

            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.action = "ServerSide/Scope_svr.asp"
            document.RSLFORM.submit()
            //ResetForm()
            return;
        }

        function LoadForm(ID) {
            ResetForm_New();
            document.getElementById("hid_Action").value = "LOAD"
            document.getElementById("hid_SCOPEID").value = ID
            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.action = "ServerSide/Scope_svr.asp"
            document.RSLFORM.submit();
            return;
        }

        function valButton(btn) {
            var cnt = -1;
            for (var i = btn.length - 1; i > -1; i--) {
                if (btn[i].checked) { cnt = i; }
            }
            if (cnt > -1) return btn[cnt].value;
            else return null;
        }

        function SaveForm() {

            document.getElementById("hid_Action").value = "NEW"

            var btnCOSTTYPE = valButton(RSLFORM.radio_COSTTYPEID);

            if (btnCOSTTYPE == 1) {
                FormFields[7] = "txt_PROPERTY|Property|CURRENCY|Y"
            }
            else {
                FormFields[7] = "txt_PROPERTY|Property|CURRENCY|N"
            }

            // Code added by Tkxel to add the External Agency Contract.
            //  Date: 24 November 2010
            CheckExternalAgencyControls();

            if (!checkForm()) return false;

            var txt_TODAY = new Date(long_date('<%=FormatDateTime(FISCALYEAR_STARTDATE,2)%>'))
            var str_today = '<%=FormatDateTime(FISCALYEAR_STARTDATE,2)%>'
            var txt_STARTDATE = new Date(long_date(document.getElementById("txt_STARTDATE").value))
            var txt_ENDDATE = new Date(long_date(document.getElementById("txt_RENEWALDATE").value))
            var txt_FINANCIALDATE = new Date(long_date(document.getElementById("txt_FINANCIALDATE").value))


            if (!(txt_STARTDATE == "")) {
                if (txt_STARTDATE < txt_TODAY) {
                    alert("You cannot enter a Start Date before (" + long_date(str_today) + ") ");
                    return false;
                }
            }

            if (!(txt_ENDDATE == "")) {
                if (txt_ENDDATE < txt_TODAY) {
                    alert("You cannot enter a End Date before (" + long_date(str_today) + ") ");
                    return false;
                }
            }

            if (txt_ENDDATE <= txt_STARTDATE) {
                alert("You cannot enter a End Date before (" + long_date(document.getElementById("txt_STARTDATE").value) + ") ");
                return false;
            }

            //alert("The ability to create new contracts is curretnly not available.\n Once 'End Dates' for existing contracts missing this information are updated this ability will be re-enabled.")
            //return false

            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.action = "ServerSide/Scope_svr.asp"
            document.RSLFORM.submit()
            return;
        }

        function SaveAppliance() {
            if (document.getElementById("txt_APPLIANCE_COST").value == "") {
                alert("Please enter the cost of Appliance");
                return;
            }
            document.getElementById("hid_Action_Appliance").value = "NEW"
            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.action = "ServerSide/Save_Appliance.asp"
            document.RSLFORM.submit()
            return;
        }
        function Delete_Appliance(ApplianceId) {
            document.getElementById("hid_Action_Appliance").value = "DELETE";
            document.getElementById("hid_Appliancetypeid").value = ApplianceId;
            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.action = "ServerSide/Save_Appliance.asp"
            document.RSLFORM.submit()
            return;
        }
        function ResetForm() {
            ApplianceDeleteAll();
            RSLFORM.hid_SCOPEID.value = ""
            RSLFORM.txt_CONTRACTNAME.value = ""
            RSLFORM.sel_AREAOFWORK.value = ""
            RSLFORM.txt_ESTIMATEDVALUE.value = ""
            RSLFORM.txt_STARTDATE.value = ""
            RSLFORM.txt_FINANCIALDATE.value = ""
            RSLFORM.txt_RENEWALDATE.value = ""
            RSLFORM.txt_DESCRIPTION.value = ""
            RSLFORM.hid_Action.value = "NEW"
            RSLFORM.sel_PATCH.value = "";
            RSLFORM.sel_SCHEME.value = "";
            RSLFORM.sel_SCHEME.disabled = true;
            RSLFORM.sel_BLOCK.value = "";
            RSLFORM.sel_BLOCK.disabled = true;
            RSLFORM.txt_PROPERTY.value = "";
            RSLFORM.txt_APPLIANCE_COST.value = "";
            RSLFORM.sel_APPLIANCE.value = "";
            RSLFORM.radio_COSTTYPEID.value = "";
            document.getElementById("app_detail").innerHTML = "";
            document.getElementById("gscost").style.display = "none";
            document.getElementById("propcost").style.display = "none";
            document.getElementById("appcost").style.display = "none";
            document.getElementById("bottom_right").style.display = "none";
            SetButtons('NEW')
            return;
        }

        function ResetForm_New() {
            document.getElementById("hid_SCOPEID").value = ""
            document.getElementById("txt_CONTRACTNAME").value = ""
            document.getElementById("sel_AREAOFWORK").options[document.getElementById("sel_AREAOFWORK").selectedIndex].value = ""
            document.getElementById("txt_ESTIMATEDVALUE").value = ""
            document.getElementById("txt_STARTDATE").value = ""
            document.getElementById("txt_FINANCIALDATE").value = "";
            document.getElementById("txt_RENEWALDATE").value = ""
            document.getElementById("txt_DESCRIPTION").value = ""
            document.getElementById("hid_Action").value = "NEW"
            document.getElementById("sel_PATCH").options[document.getElementById("sel_PATCH").selectedIndex].value = ""
            document.getElementById("sel_SCHEME").options[document.getElementById("sel_SCHEME").selectedIndex].value = ""
            document.getElementById("sel_SCHEME").disabled = true;
            document.getElementById("sel_BLOCK").options[document.getElementById("sel_BLOCK").selectedIndex].value = ""
            document.getElementById("sel_BLOCK").disabled = true;
            document.getElementById("txt_PROPERTY").value = "";
            document.getElementById("txt_APPLIANCE_COST").value = "";
            document.getElementById("sel_APPLIANCE").options[document.getElementById("sel_APPLIANCE").selectedIndex].value = ""
            document.getElementsByName("radio_COSTTYPEID")[0].checked = false;
            document.getElementsByName("radio_COSTTYPEID")[1].checked = false;
            document.getElementById("app_detail").innerHTML = "";
            document.getElementById("gscost").style.display = "none";
            document.getElementById("propcost").style.display = "none";
            document.getElementById("appcost").style.display = "none";
            document.getElementById("bottom_right").style.display = "none";
            document.getElementById("txt_CONTACTNO").value = "";
            document.getElementById("txt_CONTACTEMAIL").value = "";

            document.getElementById("p_patch").style.display = "block";
            document.getElementById("p_scheme").style.display = "block";
            document.getElementById("estcost").style.display = "block";
            document.getElementById("p_startdate").style.display = "block";
            document.getElementById("p_enddate").style.display = "block";
            document.getElementById("p_financialdate").style.display = "block";
            FormFields[2] = "txt_ESTIMATEDVALUE|Estimated Value|CURRENCY|Y"
            FormFields[3] = "txt_RENEWALDATE|End Date|DATE|Y"
            FormFields[6] = "sel_PATCH|Patch|SELECT|N"
            FormFields[10] = "txt_STARTDATE|Start Date|DATE|Y"
            FormFields[11] = "txt_FINANCIALDATE|Financial DATE|DATE|Y"

            SetButtons('NEW')
            return;
        }

        function GSDisplay() {
            var areaofwork = document.getElementById("sel_AREAOFWORK").options[document.getElementById("sel_AREAOFWORK").selectedIndex].value;
            document.getElementsByName("radio_COSTTYPEID")[0].checked = false;
            document.getElementsByName("radio_COSTTYPEID")[1].checked = false;
            document.getElementById("txt_PROPERTY").value = "";
            document.getElementById("txt_APPLIANCE_COST").value = "";
            document.getElementById("sel_APPLIANCE").options[document.getElementById("sel_APPLIANCE").selectedIndex].value = "";
            document.getElementById("txt_ESTIMATEDVALUE").value = "";

            var index = document.getElementById("sel_AREAOFWORK").selectedIndex;
            var text = document.getElementById("sel_AREAOFWORK").options[index].text;

            if (text == "External Agency") {
                // Code added by Tkxel.
                // to block the controls that are not apllicable against the external agency area of work
                // 24 November 2010
                RemoveUnMandatoryFields()
                CheckExternalAgencyControls()
                // return;
            }
            if (areaofwork == 6 || areaofwork == 5) {
                document.getElementById("gscost").style.display = "block";
                document.getElementById("divFinancialDate").style.display = "block";
                document.getElementById("divEstimatedValue").style.display = "block";
            }
            else {
                document.getElementById("gscost").style.display = "none";
                document.getElementById("propcost").style.display = "none";
                document.getElementById("appcost").style.display = "none";
                document.getElementById("bottom_right").style.display = "none";
                document.getElementById("divFinancialDate").style.display = "block";
                document.getElementById("divEstimatedValue").style.display = "block";
            }

            if (areaofwork == 8 || areaofwork == 9) {
                document.getElementById("p_patch").style.display = "none";
                document.getElementById("p_scheme").style.display = "none";
                document.getElementById("estcost").style.display = "none";
                document.getElementById("p_startdate").style.display = "none";
                document.getElementById("p_enddate").style.display = "none";
                document.getElementById("p_financialdate").style.display = "none";
                document.getElementById("divFinancialDate").style.display = "block";
                document.getElementById("divEstimatedValue").style.display = "block";
                FormFields[2] = "txt_ESTIMATEDVALUE|Estimated Value|CURRENCY|N"
                FormFields[3] = "txt_RENEWALDATE|End Date|DATE|N"
                FormFields[6] = "sel_PATCH|Patch|SELECT|N"
                FormFields[10] = "txt_STARTDATE|Start Date|DATE|N"
                FormFields[11] = "txt_FINANCIALDATE|Financial DATE|DATE|N"

            }

            else {
                document.getElementById("p_patch").style.display = "block";
                document.getElementById("p_scheme").style.display = "block";
                document.getElementById("estcost").style.display = "block";
                document.getElementById("p_startdate").style.display = "block";
                document.getElementById("p_enddate").style.display = "block";
                document.getElementById("p_financialdate").style.display = "block";
                document.getElementById("divFinancialDate").style.display = "block";
                document.getElementById("divEstimatedValue").style.display = "block";
                FormFields[2] = "txt_ESTIMATEDVALUE|Estimated Value|CURRENCY|Y"
                FormFields[3] = "txt_RENEWALDATE|End Date|DATE|Y"
                FormFields[6] = "sel_PATCH|Patch|SELECT|N"
                FormFields[10] = "txt_STARTDATE|Start Date|DATE|Y"
                FormFields[11] = "txt_FINANCIALDATE|Financial DATE|DATE|Y"
            }
        }

        // Code added by Tkxel.
        // to block the controls that are not apllicable against the external agency area of work
        // 24 November 2010
        function RemoveUnMandatoryFields() {
            document.getElementById("gscost").style.display = "none";
            document.getElementById("divFinancialDate").style.display = "none";
            document.getElementById("divEstimatedValue").style.display = "none";
        }


        function GSCost() {
            var val = 0;
            for (i = 0; i < document.getElementsByName("radio_COSTTYPEID").length; i++) {
                if (document.getElementsByName("radio_COSTTYPEID")[i].checked == true)
                    val = document.getElementsByName("radio_COSTTYPEID")[i].value;
            }
            if (val == "1") {
                document.getElementById("propcost").style.display = "block";
                document.getElementById("appcost").style.display = "none";
                document.getElementById("bottom_right").style.display = "none";
                document.getElementById("txt_APPLIANCE_COST").value = "";
                document.getElementById("sel_APPLIANCE").options[document.getElementById("sel_APPLIANCE").selectedIndex].value = "";
                return
            }
            else if (val == "2") {
                document.getElementById("appcost").style.display = "block";
                document.getElementById("bottom_right").style.display = "block";
                document.getElementById("propcost").style.display = "none";
                document.getElementById("txt_PROPERTY").value = "";
                return
            }
        }

        function ApplianceDeleteAll() {
            for (i = 0; i < document.getElementsByName("radio_COSTTYPEID").length; i++) {
                document.getElementsByName("radio_COSTTYPEID")[i].checked = false;
            }
            document.getElementById("hid_Action").value = "APPLIANCEDELETE"
            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.action = "ServerSide/Scope_svr.asp"
            document.RSLFORM.submit()
            return;
        }
        function GetSchemes() {
            document.getElementById("sel_SCHEME").disabled = true;
            if (document.getElementById("sel_PATCH").options[document.getElementById("sel_PATCH").selectedIndex].value != "") {
                //document.getElementById("txt_PROPERTY").value = "";
                //document.getElementById("txt_APPLIANCE_COST").value = "";
                //document.getElementById("sel_APPLIANCE").options[document.getElementById("sel_APPLIANCE").selectedIndex].value = "";
                //RSLFORM.sel_AREAOFWORK.value = ""
                //document.getElementById("txt_ESTIMATEDVALUE").value = ""
                //document.getElementById("txt_STARTDATE").value = ""
                //document.getElementById("txt_FINANCIALDATE").value = ""
                //document.getElementById("txt_RENEWALDATE").value = ""
                //document.getElementById("txt_DESCRIPTION").value = ""
                //RSLFORM.radio_COSTTYPEID.value="";
                //document.getElementById("gscost").style.display="none";
                //document.getElementById("propcost").style.display="none";
                //document.getElementById("appcost").style.display="none";
                //document.getElementById("bottom_right").style.display="none";
                document.RSLFORM.target = "ServerFrameContact"
                document.RSLFORM.action = "Serverside/GetSchemes.asp"
                document.RSLFORM.submit();
                return;
            }
            else {
                document.getElementById("sel_SCHEME").options[document.getElementById("sel_SCHEME").selectedIndex].value = "";
                return;
            }
        }

        function GetBlockAndEstimateCost() {
            GetEstimatedCost()
            GetBlocks()
        }

        function GetBlocks() {
            document.getElementById("sel_BLOCK").disabled = true;
            if (document.getElementById("sel_PATCH").options[document.getElementById("sel_PATCH").selectedIndex].value != "") {
                document.getElementById("txt_PROPERTY").value = "";
                document.getElementById("txt_APPLIANCE_COST").value = "";
                document.getElementById("sel_APPLIANCE").options[document.getElementById("sel_APPLIANCE").selectedIndex].value = "";
                document.getElementById("txt_ESTIMATEDVALUE").value = ""
                document.getElementById("txt_STARTDATE").value = ""
                document.getElementById("txt_FINANCIALDATE").value = ""
                document.getElementById("txt_RENEWALDATE").value = ""
                document.getElementById("txt_DESCRIPTION").value = ""
                document.RSLFORM.target = "ServerFrameContact"
                document.RSLFORM.action = "Serverside/GetBlocks.asp"
                document.RSLFORM.submit();
                return;
            }
            else {
                document.getElementById("sel_BLOCK").options[document.getElementById("sel_BLOCK").selectedIndex].value = "";
                return;
            }
        }

        function GetEstimatedCost() {
            document.getElementById("txt_ESTIMATEDVALUE").value = ""
            if (document.getElementById("sel_AREAOFWORK").options[document.getElementById("sel_AREAOFWORK").selectedIndex].value == "5" || document.getElementById("sel_AREAOFWORK").options[document.getElementById("sel_AREAOFWORK").selectedIndex].value == "6") {
                document.RSLFORM.target = "ServerFrameContact"
                document.RSLFORM.action = "Serverside/GetEstimatedCost.asp"
                document.RSLFORM.submit()
            }
            else
                return false;
        }
	
    </script>
</head>
<body onload="initSwipeMenu(3);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="764" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="Company.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_main_details" src="images/tab_org_info.gif" width="127" height="20"
                        border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Address.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" src="images/tab_address.gif" width="72" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Financial.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_attributes" src="images/tab_contact.gif" width="70" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Scope.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_warranties" src="images/tab_scope-over.gif" width="61" height="20"
                        border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <img name="tab_utilities" src="images/TabEnd.gif" width="8" height="20" border="0"
                    alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companyname%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/myImages/spacer.gif" width="333" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <div style="border: 1px solid #133E71; border-top-width: 0px; width: 762; margin-top: -6px; height:440px;">
        <form name="RSLFORM" method="post" action="">
        <div id="div_top" class="margintenleft">
            <p class="rowspacing">
                <div style="float: left; width: 120px;">
                    <label for="sel_AREAOFWORK">
                        Area of Work:</label></div>
                <div style="float: left" id="dvAreaOfWork">
                    <%=lstAREAOFWORK%></div>
                &nbsp;<img alt="" src="/js/FVS.gif" name="img_AREAOFWORK" id="img_AREAOFWORK" width="15px"
                    height="15px" border="0" />
            </p>
            <p class="rowspacing">
                <div style="float: left; width: 120px;">
                    <label for="txt_CONTRACTNAME">
                        Name of Contract:</label></div>
                <div style="float: left">
                    <input type="text" class="textbox200" name="txt_CONTRACTNAME" id="txt_CONTRACTNAME"
                        maxlength="40" value="" tabindex="1" /></div>
                &nbsp;<img alt="" src="/js/FVS.gif" name="img_CONTRACTNAME" id="img_CONTRACTNAME"
                    width="15px" height="15px" border="0" />
            </p>
            <p id="p_patch" class="rowspacing">
                <div style="float: left; width: 120px;">
                    <label for="sel_PATCH">
                        Patch:</label></div>
                <div style="float: left" id="dvPatch">
                    <%=lstPATCH%></div>
                &nbsp; <img alt="" src="/js/FVS.gif" name="img_PATCH" id="img_PATCH" width="15px"
                        height="15px" style="border-width: 0px;" />
            </p>
            <p id="p_scheme" class="rowspacing">
                <div style="float: left; width: 120px;">
                    <label for="sel_SCHEME">
                        Scheme:</label></div>
                <div style="float: left" id="dvScheme">
                    <%=lstSCHEME%></div>
                &nbsp;<img alt="" src="/js/FVS.gif" name="img_SCHEME" id="img_SCHEME" width="15px"
                    height="15px" border="0" />
            </p>
            <p id="p_block" class="rowspacing">
                <div style="float: left; width: 120px;">
                    <label for="sel_BLOCK">
                        Block:</label></div>
                <div style="float: left" id="dvBlock">
                    <%=lstBLOCK%></div>
                &nbsp;<img alt="" src="/js/FVS.gif" name="img_BLOCK" id="img1" width="15px" height="15px"
                    border="0" />
            </p>
            <p id="gscost" style="display: none" class="rowspacing">
                <label for="radio_COSTTYPEID" style="float: left; width: 120px;">
                    Cost type:</label>
                <span style="border: 1px solid #133e71; padding: 0 5 2 2;">
                    <%=ContactTypeRadio %>
                </span>
                <img alt="" src="/js/FVS.gif" name="img_gas" id="img_gas" width="15px" height="15px"
                    style="border-width: 0px;" />
            </p>
        </div>
        <div id="div_bottom" class="margintenleft">
            <div id="bottom_left">
                <p id="propcost" style="display: none" class="rowspacing">
                    <div style="float: left; width: 120px;">
                        <label for="txt_PROPERTY" style="padding-left: 0px; ">
                            Property:</label></div>
                    <span style="padding-left: 0px;">�&nbsp;<input type="text" class="textbox200" name="txt_PROPERTY"
                        id="txt_PROPERTY" maxlength="20" value="" style="width: 188px;" tabindex="1"
                        onchange="GetEstimatedCost();" /></span>
                    <img alt="" src="/js/FVS.gif" name="img_PROPERTY" id="img_PROPERTY" width="15px"
                        height="15px" style="border-width: 0px;" />
                </p>
                <div id="appcost" style="display: none">
                    <p class="rowspacing">
                        <div style="float: left; width: 120px;">
                            <label for="sel_APPLIANCE">
                                Appliance type:</label>
                        </div>
                        <%=lstAPPLIANCE%>
                        <img alt="" src="/js/FVS.gif" name="img_APPLIANCE" id="img_APPLIANCE" width="15px"
                            height="15px" style="border-width: 0px;" />
                    </p>
                    <p class="rowspacing">
                        <div style="float: left; width: 120px;">
                            <label for="txt_APPLIANCE_COST" >
                                Appliance:</label>
                        </div>
                        <span style="padding-left: 0px;">�&nbsp;<input type="text" class="textbox200" name="txt_APPLIANCE_COST"
                            id="txt_APPLIANCE_COST" maxlength="20" value="" style="width: 188px;" tabindex="1"
                            onchange="GetEstimatedCost();" /></span>
                        <img alt="" src="/js/FVS.gif" name="img_APPLIANCE_COST" id="img_APPLIANCE_COST" width="15px"
                            height="15px" style="border-width: 0px;" />
                    </p>
                </div>
                <div id="divEstimatedValue">
                    <p id="estcost" class="rowspacing">
                        <div style="float: left; width: 120px;">
                            <label for="txt_ESTIMATEDVALUE" >
                                Estimated Value:</label>
                        </div>
                        <span style="padding-left: 0px;">�&nbsp;<input type="text" class="textbox200" name="txt_ESTIMATEDVALUE"
                            id="txt_ESTIMATEDVALUE" maxlength="20" value="" tabindex="1" style="width: 188px" /></span>
                        <img alt="" src="/js/FVS.gif" name="img_ESTIMATEDVALUE" id="img_ESTIMATEDVALUE" width="15px"
                            height="15px" style="border-width: 0px;" />
                    </p>
                </div>
                <p id="p_startdate" class="rowspacing">
                    <div style="float: left; width: 120px;">
                        <label for="txt_STARTDATE">
                            Start Date:</label>
                    </div>
                    <input type="text" class="textbox200" name="txt_STARTDATE" id="txt_STARTDATE" maxlength="20"
                        value="" tabindex="1" style="width: 197px;" />
					 <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_STARTDATE',180,341,'de','#FFFFFF','#133E71')"
                    tabindex="1">
                    <img alt="calender_image" width="18px" height="19px" src="images/cal.gif" /></a>	
						
                    <img alt="" src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px"
                        height="15px" border="0" />
                </p>
				 <p  class="rowspacing"><div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                    top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                </div></p>
                <p id="p_enddate" class="rowspacing">
                    <div style="float: left; width: 120px;">
                        <label for="txt_RENEWALDATE">
                            End Date:</label></div>
                    <input type="text" class="textbox200" name="txt_RENEWALDATE" id="txt_RENEWALDATE"
                        maxlength="20" value="" tabindex="1" style="width: 197px;" />
						 <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_RENEWALDATE',180,341,'de','#FFFFFF','#133E71')"
                    tabindex="1">
                    <img alt="calender_image" width="18px" height="19px" src="images/cal.gif" /></a>	
						
                    <img alt="" src="/js/FVS.gif" name="img_RENEWALDATE" id="img_RENEWALDATE" width="15px"
                        height="15px" border="0" />
                </p>
                <div id="divFinancialDate">
                    <p id="p_financialdate" class="rowspacing">
                        <div style="float: left; width: 120px;">
                            <label for="txt_FINANCIALDATE">
                                Financial Date:</label></div>
                        <input type="text" class="textbox200" name="txt_FINANCIALDATE" id="txt_FINANCIALDATE"
                            maxlength="20" value="" tabindex="1" style="width: 197px;" />
							 <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_FINANCIALDATE',180,351,'de','#FFFFFF','#133E71')"
                    tabindex="1">
                    <img alt="calender_image" width="18px" height="19px" src="images/cal.gif" /></a>
                        <img alt="" src="/js/FVS.gif" name="img_FINANCIALDATE" id="img_FINANCIALDATE" width="15px"
                            height="15px" border="0" />
                    </p>
                </div>
                <p class="rowspacing">
                    <div style="float: left; width: 120px;">
                        <label for="txt_CONTACTNO">
                            Contact Number:</label></div>
                    <input type="text" class="textbox200" name="txt_CONTACTNO" id="txt_CONTACTNO" maxlength="20"
                        value="" tabindex="1" style=" width: 197px;" />
                    <img alt="" src="/js/FVS.gif" name="img_CONTACTNO" id="img_CONTACTNO" width="15px"
                        height="15px" border="0" />
                </p>
                <p class="rowspacing">
                    <div style="float: left; width: 120px;">
                        <label for="txt_CONTACTEMAIL">
                            Contact Email:</label></div>
                    <input type="text" class="textbox200" name="txt_CONTACTEMAIL" id="txt_CONTACTEMAIL"
                        maxlength="20" value="" tabindex="1" style=" width: 197px;" />
                    <img alt="" src="/js/FVS.gif" name="img_CONTACTEMAIL" id="img_CONTACTEMAIL" width="15px"
                        height="15px" border="0" />
                </p>
                <p class="rowspacing">
                    <div style="float: left; width: 120px;">
                        <label for="txt_DESCRIPTION" style="vertical-align: top;">
                            Description:</label></div>
                    <textarea cols="1" rows="3" class="textbox200" name="txt_DESCRIPTION" id="txt_DESCRIPTION"
                        tabindex="1" ></textarea>
                    <img alt="" src="/js/FVS.gif" name="img_DESCRIPTION" id="img_DESCRIPTION" width="15px"
                        height="15px" border="0" />
                </p>
                <p class="rowspacing" style="padding-bottom: 5px;float: right;padding-right: 27px;">
                    <input type="button" class="RSLButton" value="RESET" onclick="ResetForm()" style="width: 70px;
                        padding-right: 0px; margin-right: 0px; cursor: pointer" />&nbsp;
                    <input type="button" class="RSLButton" value=" ADD " onclick="SaveForm()" name="AddButton"
                        id="AddButton" style="width: 70px; cursor: pointer" />
                </p>
                <input type="hidden" name="hid_CompanyID" id="hid_CompanyID" value="<%=CompanyID%>" />
                <input type="hidden" name="hid_ORGID" id="hid_ORGID" value="<%=CompanyID%>" />
                <input type="hidden" name="hid_SCOPEID" id="hid_SCOPEID" />
                <input type="hidden" name="hid_APPROVED" id="hid_APPROVED" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="" />
                <input type="hidden" name="hid_Action_Appliance" id="hid_Action_Appliance" value="" />
                <input type="hidden" name="hid_Appliancetypeid" id="hid_Appliancetypeid" value="" />
                <input type="hidden" name="hid_LASTACTIONDATE" id="hid_LASTACTIONDATE" value="<%=Date()%>" />
            </div>
            <div id="bottom_right" style="display: none;">
                <p style="float: left; margin-right: 10px;">
                    <input type="button" value="ADD" name="AddAppliance" class="RSLButton" onclick="SaveAppliance()"
                        style="width: 70px; cursor: pointer" />
                </p>
                <table style="border-collapse: collapse; border: 1px solid #133E71;" border="1">
                    <tr style="color: #FFFFFF; background-color: #133E71;">
                        <td style="width: 170px; font-weight: bold; padding-left: 5px;">
                            Appliance Type:
                        </td>
                        <td style="width: 50px; font-weight: bold; padding-left: 10px;">
                            Value
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; height: 100%;" colspan="2">
                            <div style='height: 150; overflow-x: hidden; overflow-y: scroll; overflow: -moz-scrollbars-vertical !important;'
                                class='TA' id="app_detail">
                                <table cellspacing="0" cellpadding="3" border="1" style='border-collapse: collapse;
                                    behavior: url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE' width="100%">
                                    <tr>
                                        <td>
                                            Please use add button to add the appliance
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </form>
    </div>
    <br />
    <table width="764" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img name="tab_main_details" src="images/tab_scope-over.gif" width="61" height="20"
                    border="0" alt="" />
            </td>
            <td rowspan="2">
                <img name="tab_utilities" src="images/TabEnd.gif" width="8" height="20" border="0"
                    alt="" />
            </td>
            <td align="right" width="690" height="19">
                <font color="red">
                    <%=l_companyname%>'s Contracts</font>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="1" style="height: 232px; border-collapse: collapse;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71"
        width="764">
        <tr bgcolor="#133E71" style="color: #FFFFFF">
            <td height="20" width="162" nowrap="nowrap">
                <b>Contract Name:</b>
            </td>
            <td width="110" nowrap="nowrap">
                <b>Area of Work:</b>
            </td>
            <td width="72" nowrap="nowrap">
                <b>Patch:</b>
            </td>
            <td width="132" nowrap="nowrap">
                <b>Scheme:</b>
            </td>
            <td width="120" nowrap="nowrap">
                <b>Block:</b>
            </td>
            <td width="82" nowrap="nowrap">
                <b>Cost Type:</b>
            </td>
            <td width="87" nowrap="nowrap">
                <b>Value:</b>
            </td>
        </tr>
        <tr>
            <td valign="top" height="100%" colspan="7">
                <div style="height: 210; width: 764; overflow: auto" class="TA" id="detail">
                    <table cellspacing="0" cellpadding="3" border="1" style='border-collapse: collapse;
                        behavior: url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE' width="100%">
                        <%=ContactString%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <iframe name="ServerFrameContact" id="ServerFrameContact" src="/dummy.asp" style="display: none">
    </iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
