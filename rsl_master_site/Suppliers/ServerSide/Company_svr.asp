<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim Text
TotalFields = 24
ReDim DataFields   (TotalFields)
ReDim DataTypes    (TotalFields)
ReDim ElementTypes (TotalFields)
ReDim FormValues   (TotalFields)
ReDim FormFields   (TotalFields)
UpdateID	  = "hid_CompanyID"
FormFields(0) = "txt_DESCRIPTION|TEXT"
FormFields(1) = "txt_PICOVERAMOUNT|NUMBER"
FormFields(2) = "txt_DATEESTABLISHED|DATE"
FormFields(3) = "txt_INSURANCECO|NUMBER"
FormFields(4) = "txt_COMPANYNUMBER|TEXT"
FormFields(5) = "txt_DUNSNUMBER|TEXT"
FormFields(6) = "txt_PROFESSIONALBODY|TEXT"
FormFields(7) = "sel_PAYMENTTYPE|NUMBER"
FormFields(8) = "txt_BANKNAME|TEXT"
FormFields(9) = "txt_VATREGNUMBER|TEXT"
FormFields(10) = "txt_SORTCODE|TEXT"
FormFields(11) = "sel_CISCATEGORY|TEXT"
FormFields(12) = "txt_ACCOUNTNUMBER|TEXT"
FormFields(13) = "txt_CISCERTIFICATENUMBER|TEXT"
FormFields(14) = "txt_PAYMENTTERMS|NUMBER"
FormFields(15) = "txt_CISEXPIRYDATE|DATE"
FormFields(16) = "sel_TRADE|NUMBER"
FormFields(17) = "sel_COMPANYTYPE|NUMBER"
FormFields(18) = "txt_ACCOUNTNAME|TEXT"
FormFields(19) = "chk_AUTOACCEPTREPAIR|NUMBER"
FormFields(20) = "txt_NAME|TEXT"
FormFields(21) = "sel_SHOWDOCSONWHITEBOARD|NUMBER"
FormFields(22) = "rdo_ORGACTIVE|NUMBER"
FormFields(23) = "txt_UniqueTaxRefNo|TEXT"
FormFields(24) = "sel_COMPANYID|NUMBER"

Function NewRecord()
	SQL = "SELECT NAME FROM S_ORGANISATION WHERE NAME = '" & Replace(REQUEST("txt_NAME"), "'", "''") & "'"
	Call OpenRs(rsCheck, SQL)
	If (NOT rsCheck.EOF) Then
		CloseRs(rsCheck)
		Text = "Please use another company name, as the one you have selected is already in use"
		FormFields(20) = "cde_NAME|'Temporary Name'"
	End If
	Call MakeInsert(strSQL)
	SQL = "SET NOCOUNT ON; INSERT INTO S_ORGANISATION " & strSQL & "; SELECT SCOPE_IDENTITY() AS NEWID; SET NOCOUNT OFF"
	Call OpenRs (rsCompany, SQL)
		ID = rsCompany("NewID")
	Call CloseRs (rsCompany)
	Call GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)
	SQL = "SELECT NAME FROM S_ORGANISATION WHERE NAME = '" & Replace(REQUEST("txt_NAME"), "'", "''") & "' AND ORGID <> " & ID
	Call OpenRs(rsCheck, SQL)
	If (NOT rsCheck.EOF) Then
		Call CloseRs(rsCheck)
		Text = "Please use another company name, as the one you have selected is already in use"
		Redim Preserve FormFields(20)
	End If
	Call MakeUpdate(strSQL)

	SQL = "UPDATE S_ORGANISATION " & strSQL & " WHERE ORGID = '" & ID & "'"
    Response.Write SQL
	Conn.Execute SQL, recaffected
	Call GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM S_ORGANISATION WHERE ORGID = '" & ID & "'"
	Conn.Execute SQL, recaffected
End Function

Function GO()
	Call CloseDB()
	Response.Redirect "../Company.asp?OrgID=" & ID & "&Text=" & Text
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
Call CloseDB()
%>
