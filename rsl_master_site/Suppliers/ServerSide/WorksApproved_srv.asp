<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")

	detotal = Request("detotal")
	if detotal = "" Then detotal = 0 end if
	detotal = detotal
	selected = Request("selected") // get selected checkboxes into an array
	response.write selected
	sel_split = Split(selected,",")
	
	office = Request("sel_OFFICE")
	// GENERATE OFFICE SQL
	if office = "" Then 
		office_sql = "" 
	else
		office_sql = " AND CP.OFFICE = " & office & " "
	end if
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 WIDTH='100%' CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
				"<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" &_
					"<TD WIDTH='100'><FONT STYLE='COLOR:WHITE'><B>Order No.</B></FONT></TD>" &_
					"<TD WIDTH='180'><FONT STYLE='COLOR:WHITE'><B>Work Detail</B></FONT></TD>" &_
					"<TD WIDTH='200'><FONT STYLE='COLOR:WHITE'><B> Address</B></FONT></TD>" &_
					"<TD WIDTH='40'><FONT STYLE='COLOR:WHITE'><B>Value</B></FONT></TD>" &_
					"<td WIDTH='20'><FONT STYLE='COLOR:WHITE'></FONT></td>" &_
					"<td WIDTH='0%'></td>" &_
				"</TR>" &_
				"<TR><TD HEIGHT='100%'></TD></TR>" 


		cnt = 0

		strSQL = 	"SELECT J.JOURNALID, R.TITLE ,  R.LASTACTIONDATE, PO.ORDERID, " &_
					"	ISNULL((SELECT REPLACE(PROP.HOUSENUMBER,'',NULL) + '' +  REPLACE(PROP.ADDRESS1,NULL,'') + ' ' +  REPLACE(PROP.ADDRESS2,NULL,'')+ ' ' +  REPLACE(PROP.TOWNCITY,NULL,'') AS PROPERTYID " &_
					"		FROM P__PROPERTY PROP " &_
					"		WHERE PROPERTYID = J.PROPERTYID),'Address Unavailable') AS PROPERTYADDRESS, P.GROSSCOST " &_
					"FROM C_JOURNAL J  " &_
					"	INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
					"	LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
					"	INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " &_
					"	INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID  " &_
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = P.ORDERID  " &_
					"WHERE 	J.ITEMID = 1  " &_
					"	AND J.CURRENTITEMSTATUSID = 10  " &_
					"	and R.CONTRACTORID = " & Session("OrgID") &_
					"	AND J.ITEMNATUREID IN (2,22,20,21)  " &_
					"	AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " 
		
		response.write strSQL			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 12
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if select box has been previously selected
			isselected = ""
			For each key in sel_split
				response.write "<BR> KEY : " & key & " id : " & Rs("JOURNALID")
				
				If Rs("JOURNALID") = clng(key) Then
					isselected = " checked "
				Exit For
				End If
			Next
		PONUMBER = PurchaseNumber(rs("ORDERID"))	
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			str_data = str_data & 	"<TR><TD>" & PONUMBER & "</TD>" &_
						"<TD>" & Rs("TITLE") & "</TD>" &_
						"<TD>" & Rs("PROPERTYADDRESS") & "</TD>" &_
						"<TD>" & Rs("GROSSCOST") & "</TD>" &_
						"<TD></TD><TD><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & Rs("JOURNALID") & "' value='" & Rs("JOURNALID") & "' onclick='do_sum("& Rs("JOURNALID") &")'></TD>" &_
						"</TR>"
			Rs.movenext()
			End If
		Next
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=6 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			'str_data = str_data & "<TR style='height:'8px'><TD></TD></TR>" &_
				'"<TR><TD COLSPAN=4 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></TD>" &_
				'"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD align=right>" &_
								"<INPUT TYPE=BUTTON VALUE='Invoice Sent' CLASS='rslbutton' ONCLICK='process()'>" &_
								"</TD></TR></TABLE>"
		End If
		
			str_data = str_data & "<tr bgcolor=black>" &_
			"	<td COLSPAN=6 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=black>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & prevpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & nextpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & numpages& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=5 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
<body onload="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>