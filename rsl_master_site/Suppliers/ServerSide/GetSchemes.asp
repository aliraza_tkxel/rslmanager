<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim patchid,SCHEMESQL
		patchid = Request.Form("sel_PATCH")
		scopeid = Request.Form("hid_SCOPEID")

		If (patchid > 0 and patchid <> 27 ) Then
			SCHEMESQL = " P_SCHEME " &_
		                    " INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID  "&_
							" WHERE P_SCHEME.SCHEMEID  NOT IN( "&_
							"								SELECT ISNULL(SS.SCHEMEID,-1) "&_
							"								FROM S_SCOPETOPATCHANDSCHEME SS "&_
							"								INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID "&_
							"								WHERE S.RENEWALDATE>=DATEADD(M,1,GETDATE()) "&_
							"								) "&_
							" AND PDR_DEVELOPMENT.PATCHID = " & PATCHID
		Else
			SCHEMESQL = " P_SCHEME " &_
		                    " INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID  "&_
							" WHERE P_SCHEME.SCHEMEID  NOT IN( "&_
							"								SELECT ISNULL(SS.SCHEMEID,-1) "&_
							"								FROM S_SCOPETOPATCHANDSCHEME SS "&_
							"								INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID "&_
							"								WHERE S.RENEWALDATE>=DATEADD(M,1,GETDATE()) "&_
							"								) "
		End If

	Call OpenDB()

	If(scopeid <> "") Then
		SQL=" SELECT COUNT(SCHEMEID) AS SCHEMEID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE PATCHID=" & PATCHID &_
			" AND SCOPEID<>" & scopeid
	Else
		SQL=" SELECT COUNT(SCHEMEID) AS SCHEMEID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE PATCHID=" & PATCHID 
	End If

	Call OpenRs(rsSet, SQL)

	If(Not rsSet.eof) Then
		If(rsSet("SCHEMEID")>0) Then
			text = "Please Select"
		Else
			text = "All"
		End If
	Else
		text = "All"
	End If

	Call CloseRs(rsSet)

	Dim lstDevelopments
	Call BuildSelect(lstDevelopments, "sel_SCHEME", SCHEMESQL, "SCHEMEID, SCHEMENAME", "SCHEMENAME", text, schemeId, NULL, "textbox200", " style=""margin-left:0px;""  tabindex=""1"" onchange=""GetBlockAndEstimateCost()"" ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="javascript" type="text/javascript">
        function ReturnData() {
            parent.document.getElementById("dvScheme").innerHTML = document.getElementById("dvScheme").innerHTML;
            parent.ApplianceDeleteAll();
            return;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvScheme">
        <%=lstDevelopments%></div>
</body>
</html>
