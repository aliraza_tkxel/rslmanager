<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim Text
TotalFields = 9
ReDim DataFields   (TotalFields)
ReDim DataTypes    (TotalFields)
ReDim ElementTypes (TotalFields)
ReDim FormValues   (TotalFields)
ReDim FormFields   (TotalFields)
UpdateID	  = "hid_COMPANYID"
FormFields(0) = "txt_CURRENTTURNOVER|NUMBER"
FormFields(1) = "txt_LATESTACCOUNTDATE|DATE"
FormFields(2) = "txt_DATEREVIEWED|DATE"
FormFields(3) = "txt_TURNOVERLIMIT|NUMBER"
FormFields(4) = "txt_CONTRACTLIMIT|NUMBER"
FormFields(5) = "sel_REVIEWEDBY|NUMBER"
FormFields(6) = "txt_DATEAPPROVED|DATE"
FormFields(7) = "sel_APPROVEDBY|NUMBER"
FormFields(8) = "hid_ORGID|NUMBER"
FormFields(9) = "cde_APPROVALSTATUS|1"

Function NewRecord ()
	If (Request.Form("sel_APPROVEDBY") <> "") Then
		TheEmployee = Request.Form("sel_APPROVEDBY")
		SQL = "SELECT LOGIN FROM AC_LOGINS WHERE EMPLOYEEID = " & TheEmployee & " AND PASSWORD = '" & Request.Form("txt_PASSWORDDATEAPPROVED") & "'"
		Call OpenRs(rsPassCheck, SQL)
		If (NOT rsPAssCheck.EOF) Then
			FormFields(9) = "cde_APPROVALSTATUS|1"
			Pass = True
			Text = "Supplier has been approved..."
		Else
			Redim Preserve FormFields(8)
			FormFields(6) = "cde_DATEAPPROVED|NULL"
			FormFields(7) = "cde_APPROVEDBY|NULL"
			Pass = False
			Text = "Incorrect Password Entered..."
		End If
		Call CloseRs(rsPassCheck)
	Else
		Redim Preserve FormFields(8)
	End If

	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO S_FINANCIAL " & strSQL & ";"
	Conn.Execute (SQL)
	Call GO()
End Function

Function AmendRecord()
	If (Request.Form("sel_APPROVEDBY") <> "" AND Request.Form("hid_APPROVED") = "") Then
		TheEmployee = Request.Form("sel_APPROVEDBY")
		SQL = "SELECT LOGIN FROM AC_LOGINS WHERE EMPLOYEEID = " & TheEmployee & " AND PASSWORD = '" & Request.Form("txt_PASSWORDDATEAPPROVED") & "'"
		Call OpenRs(rsPassCheck, SQL)
		If (NOT rsPAssCheck.EOF) Then
			Redim Preserve FormFields(8)
			FormFields(8) = "cde_APPROVALSTATUS|1"
			Pass = True
			Text = "Supplier has been approved..."
		Else
			FormFields(6) = "cde_DATEAPPROVED|NULL"
			FormFields(7) = "cde_APPROVEDBY|NULL"
			Redim Preserve FormFields(5)
			Pass = false
			Text = "Incorrect Password Entered..."
		End If
		Call CloseRs(rsPassCheck)
	Else
		Redim Preserve FormFields(5)
	End If
	ID = Request.Form(UpdateID)
	Call MakeUpdate(strSQL)	
	SQL = "UPDATE S_FINANCIAL " & strSQL & " WHERE ORGID = '" & ID & "'" 
	Conn.Execute SQL, recaffected
	Call GO()
End Function

Function GO()
	Call CloseDB()
	Response.Redirect "../Financial.asp?CompanyID=" & ID & "&Text=" & Text
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW"   NewRecord()
	Case "AMEND" AmendRecord()
	Case "LOAD"  LoadRecord()
End Select
Call CloseDB()
%>