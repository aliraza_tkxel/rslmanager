<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim schemeid,BLOCKSQL
		schemeid = Request.Form("sel_SCHEME")
        scopeid = Request.Form("hid_SCOPEID")

		If (schemeid <> "") Then
			BLOCKSQL =  " P_BLOCK " &_
                        " WHERE P_BLOCK.SCHEMEID = "& schemeid

		Else
			BLOCKSQL = " P_BLOCK "                   
		End If

	Call OpenDB()

	If(scopeid <> "") Then
		SQL=" SELECT COUNT(BLOCKID) AS BLOCKID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE SCHEMEID = " & schemeid & " AND SCOPEID <> " & scopeid
            
	Else
		SQL=" SELECT COUNT(BLOCKID) AS BLOCKID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE SCHEMEID = " & schemeid 
	End If

	Call OpenRs(rsSet, SQL)

	If(Not rsSet.eof) Then
		If(rsSet("BLOCKID") > 0) Then
			text = "Please Select"
		Else
			text = "All"
		End If
	Else
		text = "All"
	End If

	Call CloseRs(rsSet)

	Dim lstDevelopments
	Call BuildSelect(lstBlock, "sel_BLOCK", BLOCKSQL, "BLOCKID, BLOCKNAME", "BLOCKNAME", text, blockId, NULL, "textbox200", " style=""margin-left:0px;""  tabindex=""1"" onchange=""GetEstimatedCost()"" ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="javascript" type="text/javascript">
        function ReturnData() {
            parent.document.getElementById("dvBlock").innerHTML = document.getElementById("dvBlock").innerHTML;
            parent.ApplianceDeleteAll();
            return;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvBlock">
        <%=lstBlock%></div>
</body>
</html>
