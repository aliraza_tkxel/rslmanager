<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim scopeid,applianceid,costperappliance,appliancedata,Action,Applianceidtobedeleted,javascriptMessage_dupappliance

	Function GetPostedData()
		'This function will get all the posted data
		scopeid=Request.Item("hid_SCOPEID")
		applianceid=Request.Item("sel_APPLIANCE")
		costperappliance=Request.Item("txt_APPLIANCE_COST")
		Action=Request.Item("hid_Action_Appliance")
		Applianceidtobedeleted=Request.Item("hid_Appliancetypeid")
	End Function

	Function GetCurrentScopeId()
		SQL = "SELECT IDENT_CURRENT('S_SCOPE')+1 AS Identity_Seed"
		Call OpenRs(rsSet, SQL)
			If(Not rsSet.eof) Then
				GetCurrentScopeId=rsSet("Identity_Seed")
			End If
		Call CloseRs(rsSet)
	End Function

	Function SaveAppliance()

		If(scopeid="") Then
			scopeid=GetCurrentScopeId()
		End If

		SQL = "SELECT COUNT(1) AS CHECKCOUNT FROM S_SCOPETOAPPLIANCE  WHERE SCOPEID = " & scopeid & " AND APPLIANCETYPEID = " & applianceid 
		Call OpenRs(rsSet, SQL) 
		If rsSet("CHECKCOUNT") = 0 Then
			SQL = "  INSERT INTO S_SCOPETOAPPLIANCE (SCOPEID, APPLIANCETYPEID,COSTPERAPPLIANCE) " &_
				"  VALUES (" & scopeid & "," & applianceid & "," & costperappliance & ")"
			Conn.Execute(SQL)
		Else
			javascriptMessage_dupappliance = 1
		End If

	End Function

	Function DeleteAppliance()

		If (scopeid="") Then
			scopeid=GetCurrentScopeId()
		End If

		SQL = "  DELETE FROM S_SCOPETOAPPLIANCE  "&_
			"  WHERE SCOPEID=" & scopeid &_
			"  AND APPLIANCETYPEID= "& Applianceidtobedeleted
		Conn.Execute(SQL)

	End Function

	Function GetApplianceData()

		SQL=" SELECT SA.SCOPEID,SA.APPLIANCETYPEID,APPLIANCETYPE,SA.COSTPERAPPLIANCE "&_
			" FROM S_SCOPETOAPPLIANCE SA "&_
			"   INNER JOIN GS_APPLIANCE_TYPE GAT ON GAT.APPLIANCETYPEID=SA.APPLIANCETYPEID "&_
			" WHERE SA.SCOPEID=" & scopeid
		Call OpenRs(rsSet, SQL)
		If(not rsSet.eof) Then
			appliancedata="<table cellspacing=0 cellpadding=3 border=1 style='border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE' width='100%'> "
			While(not rsSet.eof)
				appliancedata=appliancedata & "<tr>"
				appliancedata=appliancedata & "<td style='width:170px;'>" & rsSet("APPLIANCETYPE") & "</td>"
				appliancedata=appliancedata & "<td>" & rsSet("COSTPERAPPLIANCE") & "</td>"
				appliancedata=appliancedata & "<td style=""cursor:pointer"" onclick=""Delete_Appliance('" & rsSet("APPLIANCETYPEID") & "');""><img alt=""Delete Appliance"" src=""../myImages/x.gif"" name=""img_DESCRIPTION"" id=""img_DESCRIPTION"" width=""13px"" height=""13px"" border=""0""/></td>"
				appliancedata=appliancedata & "</tr>"
				rsSet.movenext()
			Wend
			appliancedata = appliancedata & "</table>"
		Else
			appliancedata = " <table cellspacing=""0"" cellpadding=""3"" border=""1"" style=""border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc)"" slcolor='' hlcolor='STEELBLUE' width=""100%""> "&_
							"   <tr> "&_
							"      <td>Please use add button to add the appliance</td> "&_
							"   </tr> "&_
							" </table>"
		End If

		Call CloseRs(rsSet)

	End Function

	Call OpenDB()
		Call GetPostedData()
		If(Action="NEW") Then
			Call SaveAppliance()
		Else
			Call DeleteAppliance()
		End If
		Call GetApplianceData()
	Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Save Appointments</title>
</head>
<script language="javascript" type="text/javascript">
function ReturnData(){
<% If javascriptMessage_dupappliance = 1 Then %>
	alert("Appliance is already in the list.")
<% Else %>
	parent.document.getElementById("app_detail").innerHTML = document.getElementById("detail").innerHTML;
	parent.document.getElementById("hid_SCOPEID").value="<%=scopeid%>";
	parent.document.getElementById("txt_APPLIANCE_COST").value="";
	parent.document.getElementById("sel_APPLIANCE").options[parent.document.getElementById("sel_APPLIANCE").selectedIndex].value = 0;
	parent.GetEstimatedCost();
	return;
<% End If %>
}
</script>
<body onload="ReturnData()">
    <div style="text-align: center" id="detail">
        <%=appliancedata%></div>
</body>
</html>
