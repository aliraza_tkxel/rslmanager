<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim companyname
        companyname = Replace(Request.Form("txt_NAME"), "'", "''")
	If (companyname = "") Then
		namesql = ""
	Else
		namesql = " AND O.NAME LIKE '%" & companyname & "%' "
	End If

	trade = Replace(Request.Form("sel_TRADE"), "'", "''")
	If (trade = "") Then
		tradesql = ""
	Else
		tradesql = " AND O.TRADE = " & trade & " "
	End If

	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " T.DESCRIPTION AS TRADE, O.NAME, O.ORGID " &_
			"FROM S_ORGANISATION O " &_
			"LEFT JOIN S_COMPANYTRADE T ON O.TRADE = T.TRADEID " &_
			"WHERE 1 = 1 " & tradesql & namesql & " AND ORGACTIVE=1 ORDER BY NAME, T.DESCRIPTION"
    Call OpenDB()
	Call OpenRs(rsSearch, SQL)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }

* { margin: 0; padding: 0; }
body { font: 14px Georgia, serif; }

table { border-collapse: collapse; width: 420px; }
td, th { border: 1px solid #ccc; padding: 10px; }

.slim { width: 88px; }
.hover { background-color: #eee; }
    </style>
    <script language="javascript" type="text/javascript">

    function DisplayCompany(CompanyID) {
        event.cancelBubble = true
        parent.location.href = "../Company.asp?CompanyID=" + CompanyID
    }

    function LoadCompany(CompanyID) {
        parent.location.href = "../SRM.asp?CompanyID=" + CompanyID
    }

</script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>
<script type="text/javascript" src="js/example-three.js"></script>
</head>
<body>
	<div id="page-wrap">	
	   <table>	   
	       <colgroup></colgroup>
	       <colgroup class="slim"></colgroup>
	       <colgroup class="slim"></colgroup>	   
	    <thead>
	       <tr>
	           <th>Company Name</th>
	           <th>Trade</th>
	       </tr>
	    </thead>
        <%
        COUNT = 0
        If (not rsSearch.EOF) Then
            Response.Write "<tbody>"
            While NOT rsSearch.EOF
	            COMPANYID = rsSearch("ORGID")
	            Response.Write "<tr style=""cursor:pointer"" onclick=""LoadCompany(" & COMPANYID & ")"">"
                    Response.Write "<td valign=""top"">" & rsSearch("NAME") & "</td>"
                    Response.Write "<td valign=""top"">" & rsSearch("TRADE") & "</td>"
                    Response.Write "</tr>"
	            COUNT = COUNT + 1
	            rsSearch.moveNext
            Wend
            Response.Write "</tbody>"
            Response.Write "<tfooter>"
        Else
            Response.Write "<tr>"
            Response.Write "<td colspan=""2"" align=""center"">No Records Found</td>"
            Response.Write "</tr>"
        End If
        If (COUNT = MaxRecords) Then
            Response.Write "<tr>"
            Response.Write "<td colspan=""2"" align=""center"">Maximum number of records returned.<br/>To Filter the list apply more filters.</td>"
            Response.Write "</tr>"
        End If
        Response.Write "<tr>"
        Response.Write "<td colspan=""2"" align=""center"">TOTAL RECORDS RETURNED :" & COUNT & "</td>"
        Response.Write "</tr>"
        Response.Write "</tfooter>"
        Call CloseRs(rsSearch)
        Call CloseDB()
        %>
        </table>
    </div>

    <table cellspacing="0" cellpadding="0" style="height:100%; display:none">
        <tr>
            <td valign="top" height="20">
                <table width="100%">
                    <tr>
                        <td width="230px" nowrap="nowrap" style="color: #133e71">
                            <b>Company Name:</b>
                        </td>
                        <td width="140px" nowrap="nowrap" style="color: #133e71">
                            <b>Trade:</b>
                        </td>
                    </tr>
                    <tr style="line-height: 2">
                        <td colspan="2" height="2" style="line-height: 2">
                            <hr style="height:2px; border: 1px dotted #133e71" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="100%" valign="top">
                <div style="height: 100%; overflow: auto" class='TA'>
                    <table width="393"><!-- style='behavior: url(/Includes/Tables/tablehl.htc)' hlcolor="#133e71" //-->
                        <%
	                    'COUNT = 0 
	                    'If (not rsSearch.EOF) Then
		                '    while NOT rsSearch.EOF
			            '        COMPANYID = rsSearch("ORGID")
			            '        Response.Write "<tr style=""cursor:pointer"" onclick=""LoadCompany(" & COMPANYID & ")"">"
                        '            Response.Write "<td width=""240px"" valign=""top"">" & rsSearch("NAME") & "</td>"
                        '            Response.Write "<td width=""160px"" valign=""top"">" & rsSearch("TRADE") & "</td>"
                        '            Response.Write "</tr>"
			            '        COUNT = COUNT + 1
			            '        rsSearch.moveNext
		                '    Wend
	                    'Else
		                '    Response.Write "<tr>"
                        '    Response.Write "<td colspan=""4"" align=""center"">No Records Found</td>"
                        '    Response.Write "</tr>"
	                    'End If
	                    'If (COUNT = MaxRecords) Then
		                '    Response.Write "<tr>"
                        '    Response.Write "<td colspan=""4"" align=""center"">Maximum number of records returned.<br/>To Filter the list apply more filters.</td>"
                        '    Response.Write "</tr>"
	                    'End If
	                    'Call CloseRs(rsSearch)
	                    'Call CloseDB()
                        %>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td height="20">
                <table width="100%">
                    <tfoot>
                        <tr style="line-height: 2px">
                            <td height="2" style="line-height: 2px">
                                <hr style="height:2px; border: 1px dotted #133E71" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                TOTAL RECORDS RETURNED :
                                <%=COUNT%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
