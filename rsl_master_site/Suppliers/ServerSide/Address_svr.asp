<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim Text
TotalFields = 11
ReDim DataFields   (TotalFields)
ReDim DataTypes    (TotalFields)
ReDim ElementTypes (TotalFields)
ReDim FormValues   (TotalFields)
ReDim FormFields   (TotalFields)
UpdateID	  = "hid_CompanyID"
FormFields(0) = "txt_BUILDINGNAME|TEXT"
FormFields(1) = "txt_TELEPHONE1|TEXT"
FormFields(2) = "txt_ADDRESS1|TEXT"
FormFields(3) = "txt_TELEPHONE2|TEXT"
FormFields(4) = "txt_ADDRESS2|TEXT"
FormFields(5) = "txt_FAX|TEXT"
FormFields(6) = "txt_ADDRESS3|TEXT"
FormFields(7) = "txt_WEBSITE|TEXT"
FormFields(8) = "txt_TOWNCITY|TEXT"
FormFields(9) = "txt_COUNTY|TEXT"
FormFields(10) = "txt_POSTCODE|TEXT"
FormFields(11) = "sel_BROADLANDCONTACT|NUMBER"

Function NewRecord()
	Call MakeInsert(strSQL)
	SQL = "SET NOCOUNT ON; INSERT INTO S_ORGANISATION " & strSQL & "; SELECT SCOPE_IDENTITY() AS NEWID; SET NOCOUNT OFF"
	Call OpenRs (rsCompany, SQL)
		ID = rsCompany("NewID")
	Call CloseRs(rsCompany)
	Call GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)
	Call MakeUpdate(strSQL)
	SQL = "UPDATE S_ORGANISATION " & strSQL & " WHERE ORGID = '" & ID & "'"
	Conn.Execute SQL, recaffected
	Call GO()
End Function

Function GO()
	Call CloseDB()
	Response.Redirect "../Address.asp?CompanyID=" & ID & "&Text=" & Text
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "LOAD"	    LoadRecord()
End Select
Call CloseDB()
%>