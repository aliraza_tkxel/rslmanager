<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim companyname
		companyname = Replace(Request.Form("txt_NAME"), "'", "''")
	If (companyname = "") Then
		namesql = ""
	Else
		namesql = " AND O.NAME LIKE '%" & companyname & "%' "
	End If

	trade = Replace(Request.Form("sel_TRADE"), "'", "''")
	If (trade = "") Then
		tradesql = ""
	Else
		tradesql = " AND O.TRADE = " & trade & " "
	End If

    company = Replace(Request.Form("sel_COMPANY"), "'", "''")
	If (company = "") Then
		companysql = ""
	Else
		companysql = " AND isnull(O.companyid,1) = " & company & " "
	End If

	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " T.DESCRIPTION AS TRADE, O.NAME, O.ORGID " &_
			"FROM S_ORGANISATION O " &_
			"LEFT JOIN S_COMPANYTRADE T ON O.TRADE = T.TRADEID " &_
			"WHERE 1 = 1 " & tradesql & namesql & companysql & " AND ORGACTIVE=1 ORDER BY NAME, T.DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsSearch, SQL)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
        
        *
        {
            margin: 0;
            padding: 0;
        }
        table
        {
            border-collapse: collapse;
            width: 100%;
        }
        td, th
        {
            border: 1px solid #ccc;
            padding: 5px;
         }
        .slim
        {
            /*width: 100%;*/
        }
        .hover
        {
            background-color: #eee;
        }
        .floatingHeader
        {
            position: fixed;
            top: 0;
            left:0;
            visibility: hidden;
            width: 100%;
        }
        th
        {
            background-color: #eee;
            font-size:10pt;
            font-weight:bold;
            width: 100%;
        }
    </style>
    <script type="text/javascript" src="../../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="../../js/tbl_TdRollOver.js"></script>
    <script language="javascript" type="text/javascript">

        function DisplayCompany(OrgID) {
            event.cancelBubble = true
            parent.location.href = "../Company.asp?OrgID=" + OrgID
        }

        function LoadCompany(CompanyID) {
            parent.location.href = "../SRM.asp?CompanyID=" + CompanyID
        }

        function UpdateTableHeaders() {
            $(".persist-area").each(function () {

                var el = $(this),
                offset = el.offset(),
                scrollTop = $(window).scrollTop(),
                floatingHeader = $(".floatingHeader", this)

                if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
                    floatingHeader.css({
                        "visibility": "visible"
                    });
                } else {
                    floatingHeader.css({
                        "visibility": "hidden"
                    });
                };
            });
        }

        // DOM Ready
        $(function () {

            var clonedHeaderRow;

            $(".persist-area").each(function () {
                clonedHeaderRow = $(".persist-header", this);
                clonedHeaderRow
             .before(clonedHeaderRow.clone())
             .css("width", clonedHeaderRow.width())
             .addClass("floatingHeader");

            });

            $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");

        });

    </script>
</head>
<body>
    <table class="persist-area">
        <colgroup class="slim">
        </colgroup>
        <colgroup class="slim">
        </colgroup>
        <thead>
            <tr class="persist-header">
                <th>
                    Company Name
                </th>
                <th>
                    Trade
                </th>
            </tr>
        </thead>
        <%
        tbodySting = ""
        tfootSting = "<tfoot>"
        COUNT = 0
        If (not rsSearch.EOF) Then
            tbodySting = tbodySting & "<tbody>"
            While NOT rsSearch.EOF
                tbodySting = tbodySting & "<tr style=""cursor:pointer"" onclick=""LoadCompany(" & rsSearch("ORGID") & ")"">"
                tbodySting = tbodySting & "<td valign=""top"" class=""RollOver"" title=""" & rsSearch("NAME") &""">" & rsSearch("NAME") & "</td>"
                tbodySting = tbodySting & "<td valign=""top"" class=""RollOver"" title=""" & rsSearch("NAME") &""">" & rsSearch("TRADE") & "</td>"
                tbodySting = tbodySting & "<tr>"
	            COUNT = COUNT + 1
	            rsSearch.moveNext
            Wend
            tbodySting = tbodySting & "</tbody>"
        Else
            tbodySting = tbodySting & "<tr>"
            tbodySting = tbodySting & "<td colspan=""2"" align=""center"">No Records Found</td>"
            tbodySting = tbodySting & "</tr>"
            tbodySting = tbodySting & "</tbody>"
        End If
        If (COUNT = MaxRecords) Then
            tfootSting = tfootSting & "<tr>"
            tfootSting = tfootSting & "<td colspan=""2"" align=""center"">Maximum number of records returned.<br/>To Filter the list apply more filters.</td>"
            tfootSting = tfootSting & "</tr>"
        End If
        tfootSting = tfootSting & "<tr>"
        tfootSting = tfootSting & "<td colspan=""2"" align=""center"">TOTAL RECORDS RETURNED :" & COUNT & "</td>"
        tfootSting = tfootSting & "</tr>"
        tfootSting = tfootSting & "</tfoot>"

        rw tfootSting
        rw tbodySting

        Call CloseRs(rsSearch)
        Call CloseDB()
        %>
    </table>
</body>
</html>
