<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim patchid,schemeId,propertycost,costtypeid,estimatedcost,dbrs,dbcmd,scopeid

	Function GetFormData()

		If(Request.Form("sel_PATCH") <> "") Then
			patchid = Request.Form("sel_PATCH")
		Else
			patchid = null
		End If

		If(Request.Form("sel_SCHEME") <> "") Then
			schemeId = Request.Form("sel_SCHEME")
		Else
			schemeId = null
		End If

		If(Request.Form("hid_SCOPEID") <> "") Then
			scopeid = Request.Form("hid_SCOPEID")
		Else
			scopeid = null
		End If

		If(Request.Form("txt_PROPERTY") = "") Then
			propertycost = 0
		Else
			propertycost = Request.Form("txt_PROPERTY")
		End If

		If(Request.Form("radio_COSTTYPEID") = "") Then
			costtypeid = 1
		Else
			costtypeid = Request.Form("radio_COSTTYPEID")
		End If

	End Function

	Call GetFormData()

	set dbrs=server.createobject("ADODB.Recordset")
	set dbcmd= server.createobject("ADODB.Command")

	dbrs.CursorType = 3
	dbrs.CursorLocation = 3

	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "GS_ESTIMATED_CONTRACT_COST"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1)=costtypeid
	dbcmd.Parameters(2)=patchid
	dbcmd.Parameters(3)=schemeId
	dbcmd.Parameters(4)=propertycost
	dbcmd.Parameters(5)=scopeid

	set dbrs=dbcmd.execute

	If(not dbrs.eof) Then
		estimatedcost=dbrs("ESTIMATEDCOST")
	End If

	dbrs.Close()
	dbcmd.Cancel()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="javascript" type="text/javascript">
        function ReturnData() {
            parent.document.getElementById("txt_ESTIMATEDVALUE").value = "<%=estimatedcost %>";
            return;
        }
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
