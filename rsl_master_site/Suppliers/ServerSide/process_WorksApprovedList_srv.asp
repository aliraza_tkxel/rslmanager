<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%

	Dim insert_list  
	Dim sel_split
	
	insert_list = Request.Form("idlist")

	response.write "idlist : " & insert_list & "<BR><BR>"
		
	OpenDB()
	
	update_Repair()
	
	CloseDB()
	
	// This function Adds a new History item to c_repair and updates the c_journal
 	Function update_Repair()
		
		sel_split = Split(insert_list ,",") ' split selected string
		
		For each key in sel_split
			
			'Find extra values in relation to the journal id
			SQL = "SELECT  R.CONTRACTORID, J.TITLE, ITEMDETAILID " &_
						" FROM	C_JOURNAL J" &_
						" INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID" &_
						" LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID" &_
						" WHERE 	J.JOURNALID = " & clng(key) & " AND" &_
						" R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID)"
			'response.write "select SQL: " & SQL & "<BR><BR>"
			
			Call OpenRs(rsGetRepairData, SQL)
				
				'Extra Items assigned
				ItemContractor = rsGetRepairData("CONTRACTORID")
				theTitle = Replace(rsGetRepairData("TITLE"), "'", "''")
				ItemDetailID = rsGetRepairData("ITEMDETAILID")				
			
			CloseRs(rsGetRepairData)
						
			'Insert new repair history item into c_repair
			strSQL = "INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER,CONTRACTORID,TITLE,NOTES,ITEMDETAILID) " &_
					 "VALUES (" & clng(key) & ",22,15,'" & date() & "'," & Session("USERID") & ","  & ItemContractor & ",'" & theTitle & "','Supplier logged on and Confirmed'," & ItemDetailID & ")"
			Conn.Execute(strSQL)
			'response.write "Insert SQL: " & strSQL & "<BR><BR>"
					
			'Update the journal status for the item in question
			strSQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = 22 WHERE JOURNALID = " & clng(key)
			Conn.Execute(strSQL)
			'response.write "update SQL: " & strSQL & "<BR><BR>"

			NEW_STATUS = 5 'WORK COMPLETED
			Call ChangePIStatus(clng(key), NEW_STATUS)			
			
		Next
	
	End Function
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.location.href = "../WorksApprovedList.asp"
	
	}
</script>
<body onload="ReturnData()">
	


</body>
</html>