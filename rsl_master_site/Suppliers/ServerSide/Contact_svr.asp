<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim Text
Dim LoaderString
TotalFields = 5
ReDim DataFields   (TotalFields)
ReDim DataTypes    (TotalFields)
ReDim ElementTypes (TotalFields)
ReDim FormValues   (TotalFields)
ReDim FormFields   (TotalFields)
UpdateID	  = "hid_EmployeeID"
FormFields(0) = "txt_NAME|TEXT"
FormFields(1) = "txt_JOBTITLE|TEXT"
FormFields(2) = "txt_EMAIL|TEXT"
FormFields(3) = "txt_TELEPHONE|TEXT"
FormFields(4) = "txt_MOBILE|TEXT"
FormFields(5) = "hid_ORGID|NUMBER"

Dim FIRSTNAME
	FIRSTNAME = Replace(Request("txt_FIRSTNAME"),"'","''")

Dim LASTNAME
	LASTNAME = Replace(Request("txt_LASTNAME"),"'","''")

Dim EMAIL
	EMAIL = Replace(Request("txt_EMAIL"),"'","''")

Dim MOBILE
	MOBILE = Replace(Request("txt_MOBILE"),"'","''")

Dim TELEPHONE
	TELEPHONE = Replace(Request("txt_TELEPHONE"),"'","''")


Dim JOBTITLE
	JOBTITLE= Replace(Request("txt_JOBTITLE"),"'","''")


Function NewRecord()
	Call MakeInsert(strSQL)
	SQL = "SET NOCOUNT ON;INSERT INTO E__EMPLOYEE (FIRSTNAME, LASTNAME, ORGID) VALUES ('" & FIRSTNAME & "','" & LASTNAME & "'," & Request("hid_ORGID")& ");SELECT SCOPE_IDENTITY() AS NEWID;"
	Call OpenRs (rsEmployee, SQL)
		EmployeeID = rsEmployee("NewID")
	Call CloseRs (rsEmployee)
	'THE TEAM VALUE SHOULD BE EQUAL TO 1 -- THIS IS THE CONTRACTORS TEAM
	SQL = "INSERT INTO E_JOBDETAILS (EMPLOYEEID, JOBTITLE, TEAM, ACTIVE) VALUES (" & EmployeeID & ", '" & JOBTITLE & "', 1, '" & Request("rdo_ACTIVE")& "')"
	Conn.Execute(SQL)
	SQL = "INSERT INTO E_CONTACT (EMPLOYEEID, WORKEMAIL, WORKDD, MOBILE) VALUES (" & EmployeeID & ",'" & EMAIL & "','" & TELEPHONE & "','" & MOBILE & "')"
	Conn.Execute(SQL)
	Call GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)

	Call MakeUpdate(strSQL)

	SQL = "UPDATE E__EMPLOYEE SET FIRSTNAME = '" & FIRSTNAME & "', LASTNAME = '" & LASTNAME & "' WHERE EMPLOYEEID = " & ID & ""
	Conn.Execute SQL, recaffected

	SQL = "UPDATE E_JOBDETAILS SET JOBTITLE = '" & JOBTITLE & "', ACTIVE = '" & Request("rdo_ACTIVE")& "' WHERE EMPLOYEEID = " & ID & ""
	Conn.Execute SQL, recaffected

	SQL = "UPDATE E_CONTACT SET WORKEMAIL = '" & EMAIL & "', WORKDD = '" & TELEPHONE & "', MOBILE = '" & MOBILE & "' WHERE EMPLOYEEID = " & ID & ""
	Conn.Execute SQL, recaffected

	Call GO()
End Function

Function LoadRecord()
	ID = Request.Form(UpdateID)

	SQL = "SELECT E.EMPLOYEEID, J.JOBTITLE, E.FIRSTNAME, E.LASTNAME, C.WORKDD, C.MOBILE, C.WORKEMAIL, J.ACTIVE FROM E__EMPLOYEE E LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID WHERE E.EMPLOYEEID = " & ID
	Call OpenRs(rsLoader, SQL)
	LoaderString = ""
	If (NOT rsLoader.EOF) Then
		LoaderString = LoaderString & "parent.document.getElementById(""txt_FIRSTNAME"").value = """ & rsLoader("FIRSTNAME") & """;"
		LoaderString = LoaderString & "parent.document.getElementById(""txt_LASTNAME"").value = """ & rsLoader("LASTNAME") & """;"		
		LoaderString = LoaderString & "parent.document.getElementById(""txt_EMAIL"").value = """ & rsLoader("WORKEMAIL") & """;"
		LoaderString = LoaderString & "parent.document.getElementById(""txt_TELEPHONE"").value = """ & rsLoader("WORKDD") & """;"
		LoaderString = LoaderString & "parent.document.getElementById(""txt_MOBILE"").value = """ & rsLoader("MOBILE") & """;"
		LoaderString = LoaderString & "parent.document.getElementById(""txt_JOBTITLE"").value = """ & rsLoader("JOBTITLE") & """;"
		isActive = rsLoader("ACTIVE")
		If (isActive = 1) Then
			LoaderString = LoaderString & "parent.document.getElementsByName(""rdo_ACTIVE"")[0].checked = true;"
		Else
			LoaderString = LoaderString & "parent.document.getElementsByName(""rdo_ACTIVE"")[1].checked = true;"
		End If
	End If
	Call CloseRs(rsLoader)
End Function

Function GO()
	Call CloseDB()
	Response.Redirect "../Contact.asp?CompanyID=" & Request("hid_CompanyID")
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW"   NewRecord()
	Case "AMEND" AmendRecord()
	Case "LOAD"  LoadRecord()
End Select
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="javascript" type="text/javascript">

		function ReturnData(){
			<%=LoaderString%>
			parent.SetButtons('AMEND');
			parent.checkForm()
		}

    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
