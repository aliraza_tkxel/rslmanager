<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull

	End Function


Dim COSTTYPEID ' radio button -- per property... per appliance
Dim AREAOFWORK
Dim OrgID


Dim ID,msgalert
Dim Text
Dim LoaderString,ContactString,TheAction,ApplianceRecord,RebuildString
	TotalFields = 9

ReDim DataFields   (TotalFields)
ReDim DataTypes    (TotalFields)
ReDim ElementTypes (TotalFields)
ReDim FormValues   (TotalFields)
ReDim FormFields   (TotalFields)
Dim UpdateID

	UpdateID	  = "hid_SCOPEID"
	FormFields(0) = "txt_CONTRACTNAME|TEXT"
	FormFields(1) = "txt_ESTIMATEDVALUE|TEXT"
	FormFields(2) = "txt_RENEWALDATE|TEXT"
	FormFields(3) = "sel_AREAOFWORK|NUMBER"
	FormFields(4) = "txt_DESCRIPTION|TEXT"
	FormFields(5) = "hid_ORGID|NUMBER"
	FormFields(6) = "radio_COSTTYPEID|NUMBER"
	FormFields(7) = "txt_PROPERTY|NUMBER"
	FormFields(8) = "txt_STARTDATE|TEXT"
	FormFields(9) = "txt_FINANCIALDATE|TEXT"

	ID = nulltest(Request.Form(UpdateID))
	COSTTYPEID = nulltest(Request.Form("radio_COSTTYPEID"))
	AREAOFWORK = nulltest(Request.Form("sel_AREAOFWORK"))
	ORGID = nulltest(Request.Form("hid_ORGID"))

	If IsNull(ID) Then
		ID = -1
	End If

	If IsNull(ORGID) Then
		ORGID = -1
	End If

	If IsNull(COSTTYPEID) Then
		COSTTYPEID = -1 
	End If

Function NewRecord()

	Dim New_SCOPEID	
	Dim ApplianceTrueFalse

	If ((AREAOFWORK = 5) OR (AREAOFWORK = 6)) Then	' if Gas Servicing or Gas Servicing/Maintenance	
		If COSTTYPEID = -1 Then
			msgalert = "Please select a Cost Type"
			Exit Function
		Else
			If(COSTTYPEID=2) Then							' if per appliance
				ApplianceTrueFalse = CheckApplianceCount()	' check the applaince count
				 If(ApplianceTrueFalse = false) Then		' if it has no appliances
					'ALERT ERROR TO USER THAT NO APPLIANCE IS ADDED TO THE SCOPE
					msgalert = "Please select Appliance"
					Exit Function
				 End If
			Else
			ApplianceTrueFalse = false
			End If
		End If
	End If

	Call MakeInsert(strSQL)
	SQL = "SET NOCOUNT ON;INSERT INTO S_SCOPE " & strSQL & ";" &_
		"SELECT @@IDENTITY AS NewID;"
		
	Call OpenRs(rsNewRecord, SQL)

	If (NOT rsNewRecord.EOF) Then
		New_SCOPEID=rsNewRecord("NewID")
	End If
	Call CloseRs(rsNewRecord)

	If (Request.Form("sel_SCHEME")<>"" AND Request.Form("sel_BLOCK")<>"") Then

        InsertString = "(SCOPEID,PATCHID,SchemeId,BlockId) VALUES( " &_
						" " & New_SCOPEID &_
						"," & Request.Form("sel_PATCH") &_
                        "," & Request.Form("sel_SCHEME") &_
						"," & Request.Form("sel_BLOCK") & " );"
		SQL="INSERT INTO S_SCOPETOPATCHANDSCHEME " & InsertString
		Conn.Execute SQL


	ElseIF(Request.Form("sel_SCHEME")<>"") THEN

        If (Request.Form("sel_PATCH")>0) Then
			InsertString="(SCOPEID,PATCHID,SchemeId) VALUES(" &_
						" " & New_SCOPEID &_
                        "," & Request.Form("sel_PATCH") &_
                        "," & Request.Form("sel_SCHEME") & ");"
			SQL="INSERT INTO S_SCOPETOPATCHANDSCHEME " & InsertString
			Conn.Execute SQL
		Else
		InsertString="(SCOPEID,SchemeId) VALUES(" &_
						" " & New_SCOPEID &_                        
                        "," & Request.Form("sel_SCHEME") & ");"
			SQL="INSERT INTO S_SCOPETOPATCHANDSCHEME " & InsertString
			Conn.Execute SQL
		End If

	ELSEIF(Request.Form("sel_BLOCK")<>"") Then

        If (Request.Form("sel_PATCH")> 0 ) Then
			InsertString="(SCOPEID,PATCHID,BlockId) VALUES(" &_
						" " & New_SCOPEID &_
                        "," & Request.Form("sel_PATCH") &_
                        "," & Request.Form("sel_BLOCK") & ");"
			SQL="INSERT INTO S_SCOPETOPATCHANDSCHEME " & InsertString
			Conn.Execute SQL
			Else
			InsertString="(SCOPEID,BlockId) VALUES(" &_
						" " & New_SCOPEID &_                        
                        "," & Request.Form("sel_BLOCK") & ");"
			SQL="INSERT INTO S_SCOPETOPATCHANDSCHEME " & InsertString
			Conn.Execute SQL			
		End If

    ELSE 

        If (Request.Form("sel_PATCH")>0) Then
			InsertString="(SCOPEID,PATCHID) VALUES(" &_
						" " & New_SCOPEID &_
						"," & Request.Form("sel_PATCH") & ");"
			SQL="INSERT INTO S_SCOPETOPATCHANDSCHEME " & InsertString
			Conn.Execute SQL		
		End If
	End If

	' TO REFRESH AND REBUILD THE BOTTOM IFRAME
	Call Rebuild()

End Function

Function AmendRecord()

	Dim ApplianceTrueFalse

	If ((AREAOFWORK = 5) OR (AREAOFWORK = 6)) Then	' if Gas Servicing or Gas Servicing/Maintenance
		If COSTTYPEID = -1 Then
			msgalert = "Please select a Cost Type"
			Exit Function
		Else
			If(COSTTYPEID=2) Then						' if per appliance
				ApplianceTrueFalse = CheckApplianceCount()	' check the applaince count
				If(ApplianceTrueFalse=false) then			' if it has no appliances
					'ALERT ERROR TO USER THAT NO APPLIANCE IS ADDED TO THE SCOPE
					msgalert = "Please select Appliance"
					Exit Function
				End If
			Else
			ApplianceTrueFalse = false
			End If
		End If
	End If

	SQL = "UPDATE S_SCOPE " & strSQL & " WHERE SCOPEID = " & ID

		FormFields(0) = "txt_CONTRACTNAME|TEXT"
		FormFields(1) = "txt_ESTIMATEDVALUE|TEXT"
		FormFields(2) = "txt_RENEWALDATE|TEXT"
		FormFields(3) = "sel_AREAOFWORK|NUMBER"
		FormFields(4) = "txt_DESCRIPTION|TEXT"
		FormFields(5) = "radio_COSTTYPEID|NUMBER"
		FormFields(6) = "txt_PROPERTY|NUMBER"
		FormFields(7) = "txt_STARTDATE|TEXT"
		FormFields(8) = "hid_LASTACTIONDATE|TEXT"
		FormFields(9) = "txt_FINANCIALDATE|TEXT"

	Redim Preserve FormFields(9)

    UpdateString="SET PATCHID=" & Request.Form("sel_PATCH") & " "
    If(Request.Form("sel_SCHEME")="") Then
        UpdateString = UpdateString & ",SchemeId=null "
    else
        UpdateString = UpdateString & ",SchemeId=" & Request.Form("sel_SCHEME")
    End If

    If(Request.Form("sel_BLOCK")="") Then
        UpdateString = UpdateString & ",BlockId=null "
    else
        UpdateString = UpdateString & ",BlockId=" & Request.Form("sel_BLOCK")
    End If

	Call MakeUpdate(strSQL)

		SQL = "UPDATE S_SCOPE " & strSQL & " WHERE SCOPEID = " & ID
		SQL = SQL &  ";UPDATE S_SCOPETOPATCHANDSCHEME " & UpdateString & " WHERE SCOPEID = " & ID
	Conn.Execute SQL, recaffected

	Call Rebuild()

End Function

Function CheckWorkOrder()
	SQL="SELECT TOP 1 SCOPEID FROM C_REPAIR WHERE SCOPEID = " & ID
	Call OpenRs(rsWO, SQL)
	If (NOT rsWO.EOF) Then
		msgalert = "Cannot amend because work order has been created for this contract."
	End If
	Call CloseRs(rsWO)
End Function

Function CheckApplianceCount()
	SQL="SELECT COUNT(*) AS APPLIANCECOUNT FROM S_SCOPETOAPPLIANCE WHERE SCOPEID = " & ID
	Call OpenRs(rsAP, SQL)
	If (NOT rsAP.EOF) Then
		If (rsAP("APPLIANCECOUNT")<7) Then
			msgalert="Please select all the appliance before saving the contract."
			CheckApplianceCount = false
		Else
			CheckApplianceCount = true
		End If
	End If
	Call CloseRs(rsAP)
End Function

Function LoadRecord()
	ID = Request.Form(UpdateID)

	Call CheckWorkOrder()


	SQL = " SELECT CONTRACTNAME,AREAOFWORK,A.AREAOFWORKID,STARTDATE,RENEWALDATE,S.DESCRIPTION,ESTIMATEDVALUE, "&_
			" APPROVALSTATUS,ISNULL(SS.PATCHID,0) AS PATCHID,ISNULL(SS.SchemeId,-1) AS SCHEMEID,ISNULL(SS.BlockId,-1) AS BLOCKID,COSTTYPEID,PROPERTY,FINANCIALDATE "&_ 
			" FROM S_SCOPE S "&_
			" LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID "&_
			" LEFT JOIN S_AREAOFWORK A ON S.AREAOFWORK = A.AREAOFWORKID "&_
			" WHERE S.SCOPEID = " & ID
	Call OpenRs(rsLoader, SQL)
	LoaderString = ""
	If (NOT rsLoader.EOF) Then
		LoaderString = LoaderString & "parent.document.getElementById(""txt_CONTRACTNAME"").value = """ & rsLoader("CONTRACTNAME") & """;"	
		textSchemeAll=GetSchemeAlltext(ID,rsLoader("PATCHID"))
        textBlockAll=GetBlockAlltext(ID,rsLoader("PATCHID"),rsLoader("SCHEMEID"))

		Call BuildSelect(lstAREAOFWORK, "sel_AREAOFWORK", "S_AREAOFWORK", "AREAOFWORKID, DESCRIPTION", "DESCRIPTION", "Please Select", rsLoader("AREAOFWORKID"), NULL, "textbox200", " style='margin-left:0px;' tabindex='1' onchange='GSDisplay();' ")

		PATCHSQL = " E_PATCH E "


		Call BuildSelect(lstPATCH, "sel_PATCH", PATCHSQL, "E.PATCHID, E.LOCATION", "LOCATION", "ALL", "" & replace(rsLoader("PATCHID"),0,"")&"", NULL, "textbox200", " style='margin-left:0px;' tabindex='1' onchange'GetSchemes()' ")
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""dvPatch"").innerHTML=""" & lstPATCH  & """;"



		SCHEMESQL=  " P_SCHEME "&_
                    " INNER JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID  "&_
						" WHERE ( "&_
						"         SCHEMEID  NOT IN( "&_
						"           					 SELECT ISNULL(SS.SCHEMEID,-1) "&_
						"	                             FROM S_SCOPETOPATCHANDSCHEME SS  "&_
						"     			    			 INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID "&_
						"           		    		 WHERE DATEADD(M,11,S.RENEWALDATE)>=GETDATE() "&_
						"             			         ) "&_
						"           OR "&_
						"           SCHEMEID = " & rsLoader("SCHEMEID")  &_
						"       )" &_
                        " AND PDR_DEVELOPMENT.PATCHID = " &  rsLoader("PATCHID")


		Call BuildSelect(lstSCHEME, "sel_SCHEME", SCHEMESQL, "SCHEMEID, SCHEMENAME", "SCHEMENAME", textSchemeAll, "" & Replace(rsLoader("SCHEMEID"),-1,"") & "", NULL, "textbox200", "style='margin-left:0px;' tabindex='1' onchange='GetBlocks()' " )
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""dvScheme"").innerHTML=""" & lstSCHEME  & """;"
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""sel_SCHEME"").disabled = false;"


        BLOCKSQL= " P_BLOCK "&_
						" WHERE ( "&_
						"         BLOCKID NOT IN( "&_
						"           					 SELECT ISNULL(SS.BLOCKID,-1) "&_
						"	                             FROM S_SCOPETOPATCHANDSCHEME SS  "&_
						"     			    			 INNER JOIN S_SCOPE S ON S.SCOPEID=SS.SCOPEID "&_
						"           		    		 WHERE DATEADD(M,11,S.RENEWALDATE)>=GETDATE() "&_
						"             			         ) "&_
						"           OR "&_
						"           BLOCKID = " & rsLoader("BLOCKID")  &_
						"       )" &_
						" AND SCHEMEID=" & rsLoader("SCHEMEID")

		Call BuildSelect(lstBLOCK, "sel_BLOCK", BLOCKSQL, "BLOCKID, BLOCKNAME", "BLOCKNAME", textBlockAll, "" & Replace(rsLoader("BLOCKID"),-1,"") & "", NULL, "textbox200", "style='margin-left:0px;' tabindex='1' " )
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""dvBlock"").innerHTML=""" & lstBLOCK  & """;"
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""sel_BLOCK"").disabled = false;"


		'LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""sel_SCHEME"").options[parent.document.getElementById(""sel_SCHEME"").selectedIndex].value = """ & Replace(rsLoader("DEVELOPMENTID"),-1,"") & """;"

		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""dvAreaOfWork"").innerHTML=""" & lstAREAOFWORK  & """;"
		'LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""sel_AREAOFWORK"").options[parent.document.getElementById(""sel_AREAOFWORK"").selectedIndex].value = """ & rsLoader("AREAOFWORK") & """;"
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_STARTDATE"").value = """ & rsLoader("STARTDATE") & """;"
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_FINANCIALDATE"").value = """ & rsLoader("FINANCIALDATE") & """;"
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_RENEWALDATE"").value = """ & rsLoader("RENEWALDATE") & """;"
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_DESCRIPTION"").value = """ & rsLoader("DESCRIPTION") & """;"
        LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_PROPERTY"").value = """ & rsLoader("PROPERTY") & """;"

		If(rsLoader("COSTTYPEID")<>"") Then
			LoaderString = LoaderString & "parent.document.getElementById(""gscost"").style.display=""block""; "
			LoaderString = LoaderString & "for( i = 0; i < parent.document.getElementsByName(""radio_COSTTYPEID"").length; i++ ){ "
			LoaderString = LoaderString & "if(i==" & rsLoader("COSTTYPEID")-1 & ") parent.document.getElementsByName(""radio_COSTTYPEID"")[i].checked = true}"
			If(rsLoader("COSTTYPEID")=1) Then
				LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""propcost"").style.display=""block"";"
				LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_PROPERTY"").value = """ & rsLoader("PROPERTY") & """;"
			ElseIf(rsLoader("COSTTYPEID")=2) Then
				LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""appcost"").style.display=""block"";"
				LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""bottom_right"").style.display=""block"";"
				ApplianceRecord=GetApplianceData(ID)
			End If
		End If
		LoaderString = LoaderString & vbCrLf & "parent.document.getElementById(""txt_ESTIMATEDVALUE"").value = """ & rsLoader("ESTIMATEDVALUE") & """;"
	End If

	Call CloseRs(rsLoader)
	' TO REFRESH AND REBUILD THE BOTTOM IFRAME
	Call Rebuild()

End Function

Function Rebuild()

	SQL="  SELECT S.SCOPEID, S.CONTRACTNAME, APPROVALSTATUS AS APPROVED, A.DESCRIPTION AS AREAOFWORK, "&_
        "        S.DATEAPPROVED,E.LOCATION,P.SCHEMENAME,B.BLOCKNAME, ISNULL(ESTIMATEDVALUE,0) AS ESTIMATEDVALUE,SC.COSTTYPE "&_
		"   FROM S_SCOPE S "&_
		"   LEFT JOIN S_AREAOFWORK A ON S.AREAOFWORK = A.AREAOFWORKID "&_
		"   LEFT JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID "&_
		"   LEFT JOIN E_PATCH E ON E.PATCHID=SS.PATCHID "&_
		"   LEFT JOIN P_SCHEME P ON P.SCHEMEID=SS.SCHEMEID "&_
        "   LEFT JOIN P_BLOCK B ON B.BLOCKID=SS.BLOCKID "&_
		"   LEFT JOIN S_COSTTYPE SC ON SC.COSTTYPEID=S.COSTTYPEID "&_
		"   WHERE S.ORGID = " & ORGID & " ORDER BY CONTRACTNAME"

	Call OpenRs(rsLoader, SQL)
	ContactString = ""
	If (NOT rsLoader.EOF) Then
		WHILE NOT rsLoader.EOF
			ContactString = ContactString & "<tr onclick=""LoadForm(" & rsLoader("SCOPEID") & ")"">" &_
				"<td width=""157"" nowrap=""nowrap"" style=""cursor:pointer"">" & rsLoader("CONTRACTNAME") & "</td>" &_
				"<td width=""105"" nowrap=""nowrap"" style=""cursor:pointer"">" & rsLoader("AREAOFWORK") & "</td>" &_
				"<td width=""66"" nowrap=""nowrap"" style=""cursor:pointer"">" & rsLoader("LOCATION") & "</td>" &_
				"<td width=""142"" style=""cursor:pointer"">" & rsLoader("SCHEMENAME") & "</td>"&_
                "<td width=""130"" style=""cursor:pointer"">" & rsLoader("BLOCKNAME") & "</td>"&_
				"<td width=""80"" nowrap=""nowrap"" style=""cursor:pointer"">" & rsLoader("COSTTYPE") & "</td>"&_
				"<td width=""80"" nowrap=""nowrap"" style=""cursor:pointer"">" & rsLoader("ESTIMATEDVALUE") & "</td>"&_
			"</tr>"
		rsLoader.MoveNext
		wend
	Else
		ContactString = ContactString & "<tr><td colspan=""7"" align=""center"">No Contracts exist for this company</td></tr>"
	End If

Call CloseRs(rsLoader)

Call GetPatchandScheme()

End Function


Function GetPatchandScheme()

	Call BuildSelect_SP_DROPDOWN(lstPATCH,"sel_PATCH","GS_PATCH_AVAILABLE","All",Null,Null,"textbox200","style='margin-left:0px' tabindex='1'")
	Call BuildSelect_SP_DROPDOWN(lstSCHEME,"sel_SCHEME","GS_SCHEMES_AVAILABLE","All",Null,Null,"textbox200","style='margin-left:0px' tabindex='1' onchange='GetBlocks()' ")
    Call BuildSelect_SP_DROPDOWN(lstBLOCK,"sel_BLOCK","GS_BLOCK_AVAILABLE","All",Null,Null,"textbox200","style='margin-left:0px' tabindex='1' ")
	RebuildString = "parent.document.getElementById(""dvPatch"").innerHTML=""" & lstPATCH  & """;"
	RebuildString = RebuildString & vbCrLf & "parent.document.getElementById(""dvScheme"").innerHTML=""" & lstSCHEME  & """;"
    RebuildString = RebuildString & vbCrLf & "parent.document.getElementById(""dvBlock"").innerHTML=""" & lstBLOCK  & """;"

End Function


Function GetApplianceData(scopeid)
	Dim appliancedata
		SQL=" SELECT SA.SCOPEID,SA.APPLIANCETYPEID,APPLIANCETYPE,SA.COSTPERAPPLIANCE "&_
		" FROM S_SCOPETOAPPLIANCE SA "&_
		"   INNER JOIN GS_APPLIANCE_TYPE GAT ON GAT.APPLIANCETYPEID=SA.APPLIANCETYPEID "&_
		" WHERE SA.SCOPEID=" & scopeid &_
		" ORDER BY SCOPETOAPPLIANCEID ASC "

		Call OpenRs(rsSet, SQL)
		If(not rsSet.eof) Then
			appliancedata="<table cellspacing=0 cellpadding=3 border=1 style='border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE' width='100%'> "
			while(not rsSet.eof)
				appliancedata=appliancedata & "<tr>"
				appliancedata=appliancedata & "<td style='width:170px;'>" & rsSet("APPLIANCETYPE") & "</td>"
				appliancedata=appliancedata & "<td>" & rsSet("COSTPERAPPLIANCE") & "</td>"
				appliancedata=appliancedata & "<td style='cursor:pointer' onclick=""Delete_Appliance('" & rsSet("APPLIANCETYPEID") & "');""><img alt='' src='../myImages/x.gif' name='img_DESCRIPTION' width='13px' height='13px' border='0'/></td>"
				appliancedata=appliancedata & "</tr>"
			rsSet.movenext()
			wend
			appliancedata = appliancedata & "</table>"
		Else
			appliancedata = " <table cellspacing=0 cellpadding=3 border=1 style='border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE' width='100%'> "&_
							"   <tr> "&_
							"      <td>Please use add button to add the appliance</td> "&_
							"   </tr> "&_
							" </table> "
		End If

	Call CloseRs(rsSet)
	GetApplianceData = appliancedata
End Function

Function DeleteAppliance()
	ID = nulltest(Request(UpdateID))
	If(ID <> "") then
		DELETESQL = " DELETE FROM S_SCOPETOAPPLIANCE WHERE SCOPEID NOT IN (SELECT SCOPEID FROM S_SCOPE) "
		Conn.Execute DELETESQL
	End If
End Function

Function GetSchemeAlltext(scopeid,patchid)
	If(scopeid <> "") Then
		SQL=" SELECT COUNT(SCHEMEID) AS SCHEMEID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE PATCHID=" & patchid &_
		" AND SCOPEID<>" & scopeid
	Else
		SQL=" SELECT COUNT(SCHEMEID) AS SCHEMEID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE PATCHID=" & patchid 
	End If

	Call OpenRs(rsSet, SQL)

	If(not rsSet.eof) Then
		If(rsSet("SCHEMEID")>0) Then
			text = "Please Select"
		Else
			text = "All"
		End If
	Else
		text = "All"
	End If
	GetSchemeAlltext = text
End Function

Function GetBlockAlltext(scopeid,patchid,schemeid)
	If(scopeid <> "") Then
		SQL=" SELECT COUNT(BLOCKID) AS BLOCKID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE PATCHID=" & patchid &_
		" AND SCOPEID<>" & scopeid &_
        " AND SCHEMEID = " & schemeid
	Else
		SQL=" SELECT COUNT(BLOCKID) AS BLOCKID FROM S_SCOPETOPATCHANDSCHEME "&_
			" WHERE PATCHID=" & patchid 
	End If

	Call OpenRs(rsSet, SQL)

	If(not rsSet.eof) Then
		If(rsSet("BLOCKID")>0) Then
			text = "Please Select"
		Else
			text = "All"
		End If
	Else
		text = "All"
	End If
	GetBlockAlltext = text
End Function

TheAction = Request("hid_Action")

Call OpenDB()

	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "LOAD"	    LoadRecord()
		Case "APPLIANCEDELETE"    DeleteAppliance()
	End Select

Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Supplier Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="javascript" type="text/javascript">

        function ReturnData(){

        if("<%=TheAction%>"!="APPLIANCEDELETE")
         {
	        if("<%=TheAction%>"=="LOAD")
	        {
	            if("<%=msgalert%>"=="")
	            {
	                parent.SetButtons('AMEND');
	            }
	            else
	            {
	                 parent.alert("<%=msgalert%>")
	                 parent.SetButtons('DISABLED');
	            }
	           <%=LoaderString%>
            }
	        else if("<%=TheAction%>"=="AMEND"||"<%=TheAction%>"=="NEW")
	        {
	           if("<%=msgalert%>"=="")
	           {
	                parent.SetButtons('ADD');
	                <%=RebuildString%>
	           }
	           else
	           {
	                parent.alert("<%=msgalert%>")
	                return false;
	           }
	           parent.ResetForm_New();
	        }
	        else
	        {
	            parent.SetButtons('ADD');
	            <%=RebuildString%>
	        }
	        parent.document.getElementById("detail").innerHTML = document.getElementById("detail").innerHTML;
	        parent.document.getElementById("app_detail").innerHTML=document.getElementById("app_detail").innerHTML;
	        return;
        }
         else
           return;
        }

    </script>
</head>
<body onload="ReturnData()">
    <div id="detail">
        <table cellspacing="0" cellpadding="3" border="1" style="border-collapse: collapse;
            behavior: url(/Includes/Tables/tablehl.htc)" slcolor="" hlcolor="STEELBLUE" width="100%">
            <%=ContactString%>
        </table>
    </div>
    <div id="reload_patch">
    </div>
    <div style="text-align: center;" id="app_detail">
        <%=ApplianceRecord%></div>
</body>
</html>
