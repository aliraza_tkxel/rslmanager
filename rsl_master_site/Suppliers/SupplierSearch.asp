<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()
	
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:200px'")	

	Call BuildSelect(lstTrades, "sel_TRADE", "S_COMPANYTRADE", "TRADEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_trade, NULL, "textbox200", " tabindex=""2"" ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Suppliers --&gt; Find --> Supplier Search</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_NAME|Company Name|TEXT|N"
        FormFields[1] = "sel_TRADE|Trade|SELECT|N" 

        function SearchNow() {
            if (!checkForm()) return false
            document.RSLFORM.action = "ServerSide/SupplierSearch_svr.asp"
            document.RSLFORM.target = "SearchResults"
            document.RSLFORM.submit()
            document.RSLFORM.submit()
            return false
        } 

    </script>
</head>
<body onload="initSwipeMenu(4);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table cellspacing="0" cellpadding="0" style="height: 100%">
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" style="cursor: pointer">
                            <img name="tab_standardsearch" src="images/tab_find_supplier.gif" width="121" height="20"
                                border="0" onclick="StandardSearch()" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/spacer.gif" width="209" height="19" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="/images/spacer.gif" width="195" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
                        </td>
                    </tr>
                </table>
                <table width="233" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
                    border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
                    <tr>
                        <td nowrap="nowrap" width="105px">
                            Trade:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstTrades%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_TRADE" id="img_TRADE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                        <td nowrap="nowrap" width="105px">
                            Company:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstCompany%>
                        </td>
                        <td nowrap="nowrap">
                           
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Company Name:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_NAME" id="txt_NAME" class="textbox200" maxlength="50" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_NAME" id="img_NAME" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" colspan="2" align="right">
                            <input type="hidden" name="DONTDOIT" id="DONTDOIT" value="1" />
                            <input type="reset" name="BtnReset" id="BtnReset" title="RESET" value=" RESET " class="RSLButton"
                                style="cursor: pointer" />
                            <input type="button" name="BtnSearch" id="BtnSearch" onclick="SearchNow()" title="SEARCH"
                                value=" SEARCH " class="RSLButton" style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td valign="top" height="100%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2">
                            <img name="tab_main_details" src="Images/tab_search_results.gif" width="122" height="20"
                                border="0" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/spacer.gif" width="298" height="19" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="/images/spacer.gif" width="298" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
                        </td>
                    </tr>
                </table>
                <table width="420" border="0" cellpadding="2" cellspacing="0" style="height: 90%;
                    border-right: 1px solid #133E71; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
                    <tr>
                        <td nowrap="nowrap" height="100%" valign="top">
                            <iframe src="/secureframe.asp" name="SearchResults" id="SearchResults" height="400"
                                width="100%" frameborder="0"></iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="Property_Server" id="Property_Server" width="4"
        height="4" style="display: none"></iframe>
</body>
</html>
