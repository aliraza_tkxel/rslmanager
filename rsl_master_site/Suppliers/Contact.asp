<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
CompanyID = Request("CompanyID")
If (CompanyID = "") Then CompanyID = -1 End If

SQL = "SELECT NAME FROM S_ORGANISATION " &_
		"WHERE ORGID = " & CompanyID 
Call OpenDB()
Call OpenRs(rsName, SQL)
If (NOT rsName.EOF) Then
	l_companyname = rsName("NAME")
End If
Call CloseRS(rsName)

SQL = "SELECT E.EMPLOYEEID, J.JOBTITLE, E.FIRSTNAME, E.LASTNAME, C.WORKDD, C.MOBILE, C.WORKEMAIL, J.ACTIVE FROM E__EMPLOYEE E LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID WHERE ORGID = " & CompanyID
Call OpenRs(rsLoader, SQL)
ContactString = ""
If (NOT rsLoader.EOF) Then
	WHILE NOT rsLoader.EOF
		active = rsLoader("ACTIVE")
		bgcolor = ""
		If (active <> 1) Then
			bgcolor = " bgcolor=""#F599A8"" title=""Inactive Contact"" "
		End If
		ContactString = ContactString & "<tr " & bgcolor & " style=""cursor:pointer"" onclick=""LoadForm(" & rsLoader("EMPLOYEEID") & ")"">" &_
					"<td width=""140"" nowrap=""nowrap"">" & rsLoader("FIRSTNAME") & " " & rsLoader("LASTNAME") & "</TD>" &_
					"<td width=""140"" nowrap=""nowrap"">" & rsLoader("JOBTITLE") & "</td>" &_
					"<td width=""220"" nowrap=""nowrap"">" & rsLoader("WORKEMAIL") & "</td>" &_
					"<td width=""110"" nowrap=""nowrap"">" & rsLoader("WORKDD") & "</td>" &_
					"<td width=""100%"">" & rsLoader("MOBILE") & "</td>" &_
					"</tr>"
		rsLoader.MoveNext
	wend
Else
	ContactString = ContactString & "<tr style=""cursor:pointer""><td colspan=""5"" align=""center"">No Contacts exist for this company</td></tr>" 
End If

Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Suppliers --&gt; Tools --&gt; Address Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_JOBTITLE|Job Title|TEXT|N"
        FormFields[1] = "txt_EMAIL|Email|EMAIL|Y"
        FormFields[2] = "txt_TELEPHONE|Telephone|TELEPHONE|N"
        FormFields[3] = "txt_MOBILE|Mobile|TELEPHONE|N"
        FormFields[4] = "txt_FIRSTNAME|First Name|TEXT|Y"
        FormFields[5] = "txt_LASTNAME|Last Name|TEXT|Y"
        FormFields[6] = "rdo_ACTIVE|Active|RADIO|Y"

        function SetButtons(Code) {
            if (Code == "AMEND") {
                document.getElementById("BtnAdd").style.display = "none"
                document.getElementById("BtnAmend").style.display = "block"
            }
            else {
                document.getElementById("BtnAdd").style.display = "block"
                document.getElementById("BtnAmend").style.display = "none"
            }
        }
        function AmendForm() {
            document.getElementById("hid_Action").value = "AMEND"
            if (!checkForm()) return false;
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
            ResetForm()
        }

        function LoadForm(ID) {
            document.getElementById("hid_Action").value = "LOAD"
            document.getElementById("hid_EmployeeID").value = ID
            document.RSLFORM.target = "ServerFrameContact"
            document.RSLFORM.submit()
        }

        function SaveForm() {
            document.getElementById("hid_Action").value = "NEW"
            if (!checkForm()) return false;
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
        }

        function ResetForm() {
            document.RSLFORM.rdo_ACTIVE[0].checked = true
            document.getElementById("txt_LASTNAME").value = ""
            document.getElementById("txt_FIRSTNAME").value = ""
            document.getElementById("txt_EMAIL").value = ""
            document.getElementById("txt_TELEPHONE").value = ""
            document.getElementById("txt_MOBILE").value = ""
            document.getElementById("txt_JOBTITLE").value = ""
            document.getElementById("hid_EmployeeID").value = ""
            document.getElementById("hid_Action").value = "NEW"
            SetButtons('NEW')
        }	
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="ServerSide/Contact_svr.asp">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="Company.asp?OrgID=<%=CompanyID%>">
                    <img name="tab_main_details" src="images/tab_org_info.gif" width="127" height="20"
                        border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Address.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" src="images/tab_address.gif" width="72" height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Financial.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_attributes" src="images/tab_contact-over.gif" width="70" height="20"
                        border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Scope.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_warranties" src="images/tab_scope-tab_contact_over.gif" width="61"
                        height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <img name="tab_utilities" src="images/TabEndClosed.gif" width="8" height="20" border="0" alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companyname%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/myImages/spacer.gif" width="333" height="1" alt="" /></td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <table cellspacing="2" style='border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71' width="750">        
        <tr>
            <td nowrap="nowrap">
                First Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="20" value=""
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Last Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="20" value=""
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LASTNAME" id="img_LASTNAME" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Job Title:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_JOBTITLE" id="txt_JOBTITLE" maxlength="50" value=""
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_JOBTITLE" id="img_JOBTITLE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td width="90px">
                Email:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMAIL" id="txt_EMAIL" maxlength="100" value=""
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td width="90px">
                Telephone:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_TELEPHONE" id="txt_TELEPHONE" maxlength="20" value=""
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TELEPHONE" id="img_TELEPHONE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Mobile:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_MOBILE" id="txt_MOBILE" maxlength="20" value=""
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Active:
            </td>
            <td>
                <input type="radio" name="rdo_ACTIVE" value="1" tabindex="1" checked="checked" />
                YES
                <input type="radio" name="rdo_ACTIVE" value="0" tabindex="1" />
                NO
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACTIVE" id="img_ACTIVE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="" />
                <input type="hidden" name="hid_CompanyID" id="hid_CompanyID" value="<%=CompanyID%>" />
                <input type="hidden" name="hid_ORGID" id="hid_ORGID" value="<%=CompanyID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="" />
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <input type="button" name="BtnReset" id="BtnReset" class="RSLButton" value=" RESET " onclick="ResetForm()" style="cursor:pointer" />&nbsp;
                        </td>
                        <td>
                            <input type="button" name="BtnAmend" id="BtnAmend" class="RSLButton" value=" AMEND " onclick="AmendForm()" style="display: none; cursor:pointer" />
                        </td>
                        <td>
                            <input type="button" name="BtnAdd" id="BtnAdd" class="RSLButton" value=" ADD " onclick="SaveForm()" style="cursor:pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img name="tab_main_details" src="images/tab_contacts.gif" width="85" height="20"
                    border="0" alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companyname%>'S Contacts</font>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="1" style='border-collapse: collapse;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71'
        width="750" height="232">
        <tr bgcolor='#133E71' style='color: #FFFFFF'>
            <td height="20" width="140" nowrap="nowrap">
                <b>&nbsp;Name:</b>
            </td>
            <td width="140" nowrap="nowrap">
                <b>&nbsp;Job Title:</b>
            </td>
            <td width="220" nowrap="nowrap">
                <b>&nbsp;Email:</b>
            </td>
            <td width="110" nowrap="nowrap">
                <b>&nbsp;Telephone:</b>
            </td>
            <td width="140" nowrap="nowrap">
                <b>&nbsp;Mobile:</b>
            </td>
        </tr>
        <tr>
            <td valign="top" height="100%" colspan="5">
                <div style="height: 210; width: 750; overflow: auto" class="TA">
                    <table cellspacing="0" cellpadding="3" border="1" style="border-collapse: collapse;
                        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" width="100%">
                        <%=ContactString%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerFrameContact" id="ServerFrameContact" style="display: none"></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
