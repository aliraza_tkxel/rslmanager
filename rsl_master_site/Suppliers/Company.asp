<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION
	Dim rq_AutoAcceptRepairs_checked
	Dim rq_Active,rq_Inactive

	ACTION = "NEW"
	l_companystatus = "New Company"
	OrgID = Request("OrgID")
	if (OrgID = "") then OrgID = -1 end if

	'Defaulyt values required to build select boxes
	l_blockid = -1
	l_developmentid = -1
	l_status = -1
	rq_Active = "CHECKED"
	rq_Inactive = ""

	SQL = "SELECT * FROM S_ORGANISATION WHERE ORGID = '" & OrgID & "'"
	Call OpenDB()
	Call OpenRs(rsLoader, SQL)

	If (NOT rsLoader.EOF) Then

		' tbl E_JOBDETAILS
		l_companyname = rsLoader("NAME")
		l_companystatus = l_companyname
		l_companytype = rsLoader("COMPANYTYPE")
		l_dateestablished = rsLoader("DATEESTABLISHED")
		l_companynumber = rsLoader("COMPANYNUMBER")
		l_dunsnumber = rsLoader("DUNSNUMBER")
		l_vatregnumber = rsLoader("VATREGNUMBER")
		l_ciscategory = rsLoader("CISCATEGORY")
		l_ciscertificatenumber = rsLoader("CISCERTIFICATENUMBER")
		l_cisexpirydate = rsLoader("CISEXPIRYDATE")
		l_trade = rsLoader("TRADE")
		l_accountname = rsLoader("ACCOUNTNAME")
		l_picoveramount = rsLoader("PICOVERAMOUNT")
		l_insuranceco = rsLoader("INSURANCECO")
		l_certificatenumber = rsLoader("CERTIFICATENUMBER")
		l_professionalbody = rsLoader("PROFESSIONALBODY")
		l_bankname = rsLoader("BANKNAME")
		l_sortcode = rsLoader("SORTCODE")
		If l_sortcode <> "" Then
			alldata="" & l_sortcode & ""
			l_sortcode1 = left(alldata,2)
			l_sortcode2 = mid(alldata,3,2)
			l_sortcode3 = right(alldata,2)
		End If
		l_accountnumber = rsLoader("ACCOUNTNUMBER")
		l_paymentterms = rsLoader("PAYMENTTERMS")
		l_paymenttype = rsLoader("PAYMENTTYPE")
		l_description = rsLoader("DESCRIPTION")
		l_showdocsonwhiteboard = rsLoader("SHOWDOCSONWHITEBOARD")
        l_UniqueTaxRefNo = rsLoader("UniqueTaxRefNo")
        l_companyid = rsLoader("companyid")
		ACTION = "AMEND"

		' if the organisation skips the accept stage of the repairs process then we can tell here
		l_AutoAcceptRepair = rsLoader("AUTOACCEPTREPAIR")
		If l_AutoAcceptRepair = 1 Then
			rq_AutoAcceptRepairs_checked = "CHECKED"
		End If

		' if the organisation is active radio button will set it to active
		l_orgactive= rsLoader("ORGACTIVE")

		If l_orgactive = 1 Then
			rq_Active = "CHECKED"
			rq_Inactive = ""
		Else
			rq_Inactive = "CHECKED"
			rq_Active =""
		End If

	End If
	Call CloseRs(rsLoader)

	Call BuildSelect(lstTrades, "sel_TRADE", "S_COMPANYTRADE", "TRADEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_trade, NULL, "textbox200", " TABINDEX=2" )
	Call BuildSelect(lstCompanyTypes, "sel_COMPANYTYPE", "S_COMPANYTYPE", "TYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_companytype, NULL, "textbox200", " TABINDEX=1")
	Call BuildSelect(lstPaymentTypes, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (2,5,8,9)", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_paymenttype, NULL, "textbox200", " TABINDEX=2")
    Call BuildSelect(lstCISCATEGORY, "sel_CISCATEGORY", "G_CISCATEGORY", "CISCATEGORY, CISCATEGORY", "1 ASC", "Please Select", l_ciscategory, NULL, "textbox200", "onchange=""CISCategoryChange()"" TABINDEX=3")
    Call BuildSelect(lstCompany, "sel_COMPANYID", "G_Company", "CompanyId, Description", "CompanyId", null, l_companyid, NULL, "textbox200", "  ")

    Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Suppliers--&gt; Company Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
        .style1
        {
            width: 34px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_NAME|Company Name|TEXT|Y";
        FormFields[1] = "txt_PICOVERAMOUNT|PI Cover Amount|CURRENCY|N";
        FormFields[2] = "txt_DATEESTABLISHED|Date Established|DATE|Y";
        FormFields[3] = "txt_INSURANCECO|Insurance Cover|CURRENCY|N";
        FormFields[4] = "txt_COMPANYNUMBER|Company Number|TEXT|N";
        FormFields[5] = "txt_DUNSNUMBER|DUNS Number|TEXT|N";
        FormFields[6] = "txt_PROFESSIONALBODY|Professional Body|TEXT|N";
        FormFields[7] = "sel_PAYMENTTYPE|Payment Type|SELECT|Y";
        FormFields[8] = "txt_BANKNAME|Bank Name|TEXT|N";
        FormFields[9] = "txt_VATREGNUMBER|VAT Reg Number|TEXT|N";
        FormFields[10] = "txt_SORTCODE|Sort Code|INTEGER|N";
        FormFields[11] = "sel_CISCATEGORY|CIS Category|SELECT|Y";
        FormFields[12] = "txt_ACCOUNTNUMBER|Account Number|INTEGER|N";
        FormFields[13] = "txt_PAYMENTTERMS|Payment Terms|INTEGER|Y";
        FormFields[14] = "sel_TRADE|Trade|SELECT|Y";
        FormFields[15] = "sel_COMPANYTYPE|Company Type|SELECT|Y";
        FormFields[16] = "txt_ACCOUNTNAME|Account Name|TEXT|N";
        FormFields[17] = "chk_AUTOACCEPTREPAIR|Auto Accept Repairs|INT|N";
        FormFields[18] = "sel_SHOWDOCSONWHITEBOARD|Documents on the Whiteboard|INT|N";
        FormFields[19] = "rdo_ORGACTIVE|Organisation Status|RADIO|N"
        FormFields[20] = "txt_CISCERTIFICATENUMBER|CIS Certificate Number|TEXT|N"
        FormFields[21] = "txt_UniqueTaxRefNo|Unique Tex Reference Number|TEXT|N"

        function SaveForm() {
            var CisCategoryList = document.getElementById("sel_CISCATEGORY");
            var CisCategory = CisCategoryList.options[CisCategoryList.selectedIndex].value;

            if (CisCategory == "Gross" || CisCategory == "Higher Rate" || CisCategory == "Standard") {
                FormFields[20] = "txt_CISCERTIFICATENUMBER|CIS Certificate Number|TEXT|Y";
                FormFields[21] = "txt_UniqueTaxRefNo|Unique Tex Reference Number|TEXT|Y";
            }
            else {
                FormFields[20] = "txt_CISCERTIFICATENUMBER|CIS Certificate Number|TEXT|N";
                FormFields[21] = "txt_UniqueTaxRefNo|Unique Tex Reference Number|TEXT|N";
            }

            document.getElementById("txt_SORTCODE").value = document.getElementById("txt_SORTCODE1").value + document.getElementById("txt_SORTCODE2").value + document.getElementById("txt_SORTCODE3").value;
            if (!checkForm()) return false;
            if (document.getElementById("txt_SORTCODE").value.length != 6 && document.getElementById("txt_SORTCODE").value.length != "") {
                ManualError("img_SORTCODE", "The Sort Code must consist of 6 numbers made up of 3 sets of 2.", 0)
                return false;
            }
            if (document.getElementById("txt_ACCOUNTNUMBER").value.length != 8 && document.getElementById("txt_ACCOUNTNUMBER").value.length != 0) {
                ManualError("img_ACCOUNTNUMBER", "The Account Number must consist of 8 numbers.", 0)
                return false;
            }
            if (!checkForm()) return false;
            document.RSLFORM.action = "ServerSide/Company_svr.asp"
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
        }

        var isNN = (navigator.appName.indexOf("Netscape") != -1);
        function autoTab(input, len, e) {

            var keyCode = (isNN) ? e.which : e.keyCode;
            var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];

            if (input.value.length >= len && !containsElement(filter, keyCode)) {
                input.value = input.value.slice(0, len);
                input.form[(getIndex(input) + 1) % input.form.length].focus();
            }

            function containsElement(arr, ele) {
                var found = false, index = 0;
                while (!found && index < arr.length)
                    if (arr[index] == ele)
                        found = true;
                    else
                        index++;
                return found;
            }

            function getIndex(input) {
                var index = -1, i = 0, found = false;
                while (i < input.form.length && index == -1)
                    if (input.form[i] == input) index = i;
                    else i++;
                return index;
            }
            return true;
        }

        function limitText(textArea, length, errorTag) {
            if (textArea.value.length > length) {
                textArea.value = textArea.value.substr(0, length);
                ManualError(errorTag, "This field has been truncated, the maximum number of characters allowed is 1000.", 2)
            }
        }
        function CISCategoryChange() {
            var CisCategoryList = document.getElementById("sel_CISCATEGORY");
            var CisCategory = CisCategoryList.options[CisCategoryList.selectedIndex].value.toString();

            var strNone = "None"

            if (CisCategory.toLowerCase() == strNone.toLowerCase()) {
                document.getElementById("txt_CISCERTIFICATENUMBER").value = "";
                document.getElementById("txt_UniqueTaxRefNo").value = "";
            }
        }

    </script>
</head>
<body onload="initSwipeMenu(4);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <% If ACTION = "NEW" Then %>
        <tr>
            <td rowspan="2">
                <img name="tab_main_details" src="images/tab_org_info-over.gif" width="127" height="20"
                    border="0" alt="" />
            </td>
            <td rowspan="2">
                <img name="tab_main_details" src="images/TabEnd.gif" width="8" height="20" border="0"
                    alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companystatus%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/myImages/spacer.gif" width="615" height="1" alt="" /></td>
        </tr>
        <% Else %>
        <tr>
            <td rowspan="2">
                <img name="tab_main_details" src="images/tab_org_info-over.gif" width="127" height="20"
                    border="0" alt="" />
            </td>
            <td rowspan="2">
                <a href="Address.asp?CompanyID=<%=OrgID%>">
                    <img name="tab_financial" src="images/tab_address-tab_org_info_ov.gif" width="72"
                        height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Financial.asp?CompanyID=<%=OrgID%>">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?CompanyID=<%=OrgID%>">
                    <img name="tab_attributes" src="images/tab_contact.gif" width="70" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Scope.asp?CompanyID=<%=OrgID%>">
                    <img name="tab_warranties" src="images/tab_scope.gif" width="61" height="20" border="0"
                        alt="" /></a>
            </td>
            <td rowspan="2">
                <img name="tab_utilities" src="images/TabEndClosed.gif" width="8" height="20" border="0"
                    alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companystatus%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/myImages/spacer.gif" width="333" height="1" alt="" /></td>
        </tr>
        <% End If %>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="2" cellspacing="0" style="height:90%; border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr>
            <td nowrap="nowrap">
                Company Name:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_NAME" id="txt_NAME" class="textbox200" maxlength="90"
                    value="<%=l_companyname%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_NAME" id="img_NAME" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Trade:
            </td>
            <td nowrap="nowrap">
                <%=lstTrades%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TRADE" id="img_TRADE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Company Type:
            </td>
            <td nowrap="nowrap" class="style1">
                <%=lstCompanyTypes%>
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_COMPANYTYPE" id="img_COMPANYTYPE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                PI Cover Amount:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_PICOVERAMOUNT" id="txt_PICOVERAMOUNT" class="textbox200"
                    maxlength="20" value="<%=l_picoveramount%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_PICOVERAMOUNT" id="img_PICOVERAMOUNT" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Date Established:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_DATEESTABLISHED" id="txt_DATEESTABLISHED" class="textbox200"
                    maxlength="10" value="<%=l_dateestablished%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_DATEESTABLISHED" id="img_DATEESTABLISHED" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Insurance Co:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_INSURANCECO" id="txt_INSURANCECO" class="textbox200"
                    maxlength="50" value="<%=l_insuranceco%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_INSURANCECO" id="img_INSURANCECO" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Company Number:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_COMPANYNUMBER" id="txt_COMPANYNUMBER" class="textbox200"
                    maxlength="50" value="<%=l_companynumber%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_COMPANYNUMBER" id="img_COMPANYNUMBER" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Unique Tax Ref. No:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_UniqueTaxRefNo" id="txt_UniqueTaxRefNo" class="textbox200"
                    maxlength="50" value="<%=l_UniqueTaxRefNo%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_UniqueTaxRefNo" id="img_UniqueTaxRefNo" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                DUNS Number:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_DUNSNUMBER" id="txt_DUNSNUMBER" class="textbox200" maxlength="50"
                    value="<%=l_dunsnumber%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_DUNSNUMBER" id="img_DUNSNUMBER" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Account Name:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_ACCOUNTNAME" id="txt_ACCOUNTNAME" class="textbox200"
                    maxlength="50" value="<%=l_accountname%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACCOUNTNAME" id="img_ACCOUNTNAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Professional Body:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_PROFESSIONALBODY" id="txt_PROFESSIONALBODY" class="textbox200"
                    maxlength="50" value="<%=l_professionalbody%>" tabindex="2" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_PROFESSIONALBODY" id="img_PROFESSIONALBODY" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Bank Name:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_BANKNAME" id="txt_BANKNAME" class="textbox200" maxlength="50"
                    value="<%=l_bankname%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_BANKNAME" id="img_BANKNAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                VAT Reg Number:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_VATREGNUMBER" id="txt_VATREGNUMBER" class="textbox200"
                    maxlength="50" value="<%=l_vatregnumber%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_VATREGNUMBER" id="img_VATREGNUMBER" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Sort Code:
            </td>
            <td nowrap="nowrap">
                <input type="text" class="textbox" name="txt_SORTCODE1" id="txt_SORTCODE1" value="<%=l_sortcode1%>"
                    size="2" maxlength="2" tabindex="9" onkeyup="return autoTab(this, 2, event);" />
                -
                <input type="text" class="textbox" name="txt_SORTCODE2" id="txt_SORTCODE2" value="<%=l_sortcode2%>"
                    size="2" maxlength="2" tabindex="10" onkeyup="return autoTab(this, 2, event);" />
                -
                <input type="text" class="textbox" name="txt_SORTCODE3" id="txt_SORTCODE3" value="<%=l_sortcode3%>"
                    size="2" maxlength="2" tabindex="11" />
                <input type="hidden" name="txt_SORTCODE" id="txt_SORTCODE" value="<%=l_sortcode%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SORTCODE" id="img_SORTCODE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                CIS Category:
            </td>
            <td nowrap="nowrap" class="style1">
                <%=lstCISCATEGORY %>                
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_CISCATEGORY" id="img_CISCATEGORY" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Account Number:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_ACCOUNTNUMBER" id="txt_ACCOUNTNUMBER" class="textbox200"
                    maxlength="8" value="<%=l_accountnumber%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACCOUNTNUMBER" id="img_ACCOUNTNUMBER" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                CIS Verification No:
            </td>
            <td nowrap="nowrap" class="style1">
                <input type="text" name="txt_CISCERTIFICATENUMBER" id="txt_CISCERTIFICATENUMBER"
                    class="textbox200" maxlength="50" value="<%=l_ciscertificatenumber%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_CISCERTIFICATENUMBER" id="img_CISCERTIFICATENUMBER"
                    width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Payment Terms:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_PAYMENTTERMS" id="txt_PAYMENTTERMS" class="textbox200"
                    maxlength="50" value="<%=l_paymentterms%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_PAYMENTTERMS" id="img_PAYMENTTERMS" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr><td nowrap="nowrap">
                Company:
            </td>
            <td nowrap="nowrap" class="style1">
                <%=lstCompany%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_CERTIFICATENUMBER" id="img_CERTIFICATENUMBER" width="15px"
                    height="15px" border="0" alt="" />
            </td>            
            <td nowrap="nowrap">
                Payment Type:
            </td>
            <td nowrap="nowrap">
                <%=lstPaymentTypes%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_PAYMENTTYPE" id="img_PAYMENTTYPE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="7">
                <hr style="border: 1px dotted #133e71" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="border: 1px" colspan="2">
                <b>RSL Manager Admin Rules</b>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Auto Accept Repairs:
            </td>
            <td nowrap="nowrap" colspan="5">
                <input name="chk_AUTOACCEPTREPAIR" id="chk_AUTOACCEPTREPAIR" type="checkbox" value="1"
                    <%=rq_AutoAcceptRepairs_checked%> />
                <img src="/js/FVS.gif" name="img_AUTOACCEPTREPAIR" id="img_AUTOACCEPTREPAIR" width="15px"
                    height="15px" border="0" alt="" />
                (Ticking this box will tell the system to skip the acceptance stage of the repairs
                process.)
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Status of Supplier:
            </td>
            <td nowrap="nowrap" colspan="5">
                <input name="rdo_ORGACTIVE" id="chk1" value="1" type="radio" <%=rq_Active%> />
                <i>Active</i>
                <input name="rdo_ORGACTIVE" id="chk2" value="0" type="radio" <%=rq_Inactive%> /><i>Inactive</i>
                <img src="/js/FVS.gif" name="img_ORGACTIVE" id="img_ORGACTIVE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="7">
                <hr style="border: 1px dotted #133e71" />
            </td>
        </tr>
        <tr>
            <td>
                Show Documents
            </td>
            <td colspan="5">
                <select name="sel_SHOWDOCSONWHITEBOARD" id="sel_SHOWDOCSONWHITEBOARD" class="textbox200">
                    <option value="0" <% if l_showdocsonwhiteboard = 0 then rw " selected" %>>No</option>
                    <option value="1" <% if l_showdocsonwhiteboard = 1 then rw " selected" %>>Yes</option>
                </select>
                <img src="/js/FVS.gif" name="img_SHOWDOCSONWHITEBOARD" id="img_SHOWDOCSONWHITEBOARD"
                    width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" valign="top">
                Description:
            </td>
            <td nowrap="nowrap" colspan="4">
                <textarea name="txt_DESCRIPTION" id="txt_DESCRIPTION" class="textbox" rows="4" cols="88"
                    tabindex="3" onblur="limitText(this,1000,'img_DESCRIPTION')"><%=l_description%></textarea>
            </td>
            <td valign="top">
                <img src="/js/FVS.gif" name="img_DESCRIPTION" id="img_DESCRIPTION" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="5" align="right">
                <font color="red">
                    <%=Replace(Request("Text"), "'", "''")%></font>
                <input type="hidden" name="txt_ORIGINALNAME" id="txt_ORIGINALNAME" value="<%=l_companyname%>" />
                <input type="hidden" name="hid_CompanyID" id="hid_CompanyID" value="<%=OrgID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="button" name="BtnSave" id="BtnSave" onclick="SaveForm()" title="SAVE"
                    value=" SAVE " class="RSLButton" tabindex="3" style="cursor: pointer" />
            </td>
        </tr>
        <tr>
            <td height="100%">
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="Property_Server" id="Property_Server" width="4"
        height="4" style="display: none"></iframe>
</body>
</html>
