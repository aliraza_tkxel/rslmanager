<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
CompanyID = Request("CompanyID")
If (CompanyID = "") Then CompanyID = -1 End If

SQL = "SELECT * FROM S_ORGANISATION WHERE ORGID = " & CompanyID
Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	l_companyname = rsLoader("NAME")
	l_buildingname = rsLoader("BUILDINGNAME")
	l_address1 = rsLoader("ADDRESS1")
	l_address2 = rsLoader("ADDRESS2")
	l_address3 = rsLoader("ADDRESS3")
	l_towncity = rsLoader("TOWNCITY")
	l_county = rsLoader("COUNTY")
	l_postcode = rsLoader("POSTCODE")
	l_telephone1 = rsLoader("TELEPHONE1")
	l_telephone2 = rsLoader("TELEPHONE2")
	l_fax = rsLoader("FAX")
	l_website = rsLoader("WEBSITE")
	l_broadlandcontact = rsLoader("BROADLANDCONTACT")
	ACTION = "AMEND"
End If
Call CloseRs(rsLoader)

Call BuildSelect(lstBC, "sel_BROADLANDCONTACT", "E__EMPLOYEE", "EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME", "FULLNAME", "Please Select", l_broadlandcontact, NULL, "textbox200", " tabindex=""2"" " )
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Suppliers --&gt; Tools --&gt; Address Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_ADDRESS1|Address 1|TEXT|Y"
        FormFields[1] = "txt_ADDRESS2|Address 2|TEXT|N"
        FormFields[2] = "txt_ADDRESS3|Address 3|TEXT|N"
        FormFields[3] = "txt_TOWNCITY|TOWNCITY|TEXT|Y"
        FormFields[4] = "txt_COUNTY|County|TEXT|N"
        FormFields[5] = "txt_POSTCODE|Postcode|POSTCODE|Y"
        FormFields[6] = "txt_BUILDINGNAME|Building Name|TEXT|N"
        FormFields[7] = "txt_TELEPHONE1|Telephone 1|TELEPHONE|N"
        FormFields[8] = "txt_TELEPHONE2|Telephone 2|TELEPHONE|N"
        FormFields[9] = "txt_FAX|Fax|TELEPHONE|N"
        FormFields[10] = "txt_WEBSITE|Web Site|TEXT|N"
        FormFields[11] = "sel_BROADLANDCONTACT|Broadland Contact|SELECT|N"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.submit()
        }
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="ServerSide/Address_svr.asp">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="Company.asp?OrgID=<%=CompanyID%>">
                    <img name="tab_main_details" id="tab_main_details" src="images/tab_org_info.gif"
                        width="127" height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Address.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_address" id="tab_address" src="images/tab_address-over.gif" width="72"
                        height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Financial.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_financial" id="tab_financial" src="images/tab_financial-tab_main_deta.gif"
                        width="79" height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_attributes" id="tab_attributes" src="images/tab_contact.gif" width="70"
                        height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <a href="Scope.asp?CompanyID=<%=CompanyID%>">
                    <img name="tab_warranties" id="tab_warranties" src="images/tab_scope.gif" width="61"
                        height="20" border="0" alt="" /></a>
            </td>
            <td rowspan="2">
                <img name="tab_utilities" id="tab_utilities" src="images/TabEndClosed.gif" width="8"
                    height="20" border="0" alt="" />
            </td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_companyname%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/myImages/spacer.gif" width="333" height="1" alt="" /></td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/myImages/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table> 
    <table cellspacing="2" style="border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">
        <tr>
            <td nowrap="nowrap">
                Building Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_BUILDINGNAME" id="txt_BUILDINGNAME" maxlength="50" value="<%=l_buildingname%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_BUILDINGNAME" id="img_BUILDINGNAME" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap" width="85px">
                Telephone 1:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_TELEPHONE1" id="txt_TELEPHONE1" maxlength="20" value="<%=l_telephone1%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TELEPHONE1" id="img_TELEPHONE1" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Address 1:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS1" id="txt_ADDRESS1" maxlength="50" value="<%=l_address1%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap" width="85px">
                Telephone 2:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_TELEPHONE2" id="txt_TELEPHONE2" maxlength="20" value="<%=l_telephone2%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TELEPHONE2" id="img_TELEPHONE2" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td width="90px">
                Address 2:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS2" id="txt_ADDRESS2" maxlength="40" value="<%=l_address2%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Fax:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_FAX" id="txt_FAX" maxlength="20" value="<%=l_fax%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FAX" id="img_FAX" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td width="90px">
                Address 3:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS3" id="txt_ADDRESS3" maxlength="50" value="<%=l_address3%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS3" id="img_ADDRESS3" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Web Site:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WEBSITE" id="txt_WEBSITE" maxlength="200" value="<%=l_website%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WEBSITE" id="img_WEBSITE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Town / City:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_TOWNCITY" id="txt_TOWNCITY" maxlength="40" value="<%=l_towncity%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TOWNCITY" id="img_TOWNCITY" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                County:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY" maxlength="30" value="<%=l_county%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Broadland Contact:
            </td>
            <td>
                <%=lstBC%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_BROADLANDCONTACT" id="img_BROADLANDCONTACT" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="10" value="<%=l_postcode%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
            <td align="right">
                <input type="hidden" name="hid_CompanyID" id="hid_CompanyID" value="<%=CompanyID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="button" name="BtnSave" id="BtnSave" class="RSLButton" title="SAVE" value=" SAVE " onclick="SaveForm()" style="cursor:pointer" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
