<%@  language="VBSCRIPT" codepage="1252" %>
<%  ByPassSecurityAccess= True %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=200" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim CompanyID
	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev, disa_bled
	CompanyID = Request("CompanyID")

	Call OpenDB()

    SQL = "SELECT CISCATEGORY FROM S_ORGANISATION WHERE ORGID = '" & CompanyID & "'"
    Call OpenRs(rsLoader, SQL)    
    If (NOT rsLoader.EOF) Then
        l_ciscategory = rsLoader("CISCATEGORY")
    End If    

	Call BuildSelect(lst_work, "sel_STATUS", " C_STATUS WHERE ITEMSTATUSID IN (1,2,6,10,11) ", "ITEMSTATUSID, DESCRIPTION", "ITEMSTATUSID", "All", NULL, "WIDTH:125", "textbox100", " onchange=""javascript:WOGO(7)"" ")
	Call BuildSelect(lst_rpo, "sel_RPO", " F_POSTATUS WHERE POSTATUSID NOT IN (1,2,8,10,11,12,0) ", "POSTATUSID, POSTATUSNAME", "POSTATUSID", "All", NULL, "WIDTH:125", "textbox100", " onchange=""javascript:WOGO(8)"" ")
	Call BuildSelect(lst_po, "sel_PO", " F_POSTATUS WHERE POSTATUSID NOT IN (3,4,5,8,10,11,12,0) ", "POSTATUSID, POSTATUSNAME", "POSTATUSID", "All", NULL, "WIDTH:125", "textbox100", " onchange=""javascript:WOGO(9)"" ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge" />
    <title>RSL Manager Portfolio -- > Portfolio Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }        
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/Loading.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" language="JavaScript">
<!--
        var FormFields = new Array()
        var JOURNALID
        var iFrameArray = new Array("empty",
								"imaindetails",
								"iaddress",
								"ifinancial",
								"icontact",
								"iscope",
								"icustomerdetails",
								"iRepairjournal",
								"iaccount",
								"ipurchases",
								"iInvoiceDtl",
								"irepairdetail")

        var FilterArray = new Array()
        FilterArray[7] = "sel_STATUS"
        FilterArray[8] = "sel_RPO"
        FilterArray[9] = "sel_PO"
        FilterArray[10] = "sel_STATUS"
        FilterArray[11] = "sel_STATUS"

        var MAIN_OPEN_BOTTOM_FRAME = 7
        var LOADER_FRAME_APPEND
        var MASTER_OPEN_WORKORDER = ""
        var MASTER_OPEN_WORKORDER_PAGE = ""
        var MASTER_OPEN_WORKORDER_SORT = ""
        var MASTER_OPEN_WORKORDER_SORT2 = ""
        var MASTER_OPEN_PURCHASEORDER = ""
        var MASTER_OPEN_PURCHASEORDER_PAGE = ""
        var MASTER_OPEN_PURCHASEORDER_SORT = ""

        var LOADER_FRAME_APPEND
        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        // where is either 'top' or 'bottom' relating to the tab area of the page
        // FULL RELOAD FUNCTIONALITY NEEDS TO BE IMPLEMETED TO RELOAD ALL DYNAMIC DIVS -- PDP
        function swap_div(int_which_one, str_where) {

            var divid, imgid, upper
            //		if (	document.getElementById("txt_INVDT").value != "")
            //		{
            //			FormFields[0] = "txt_INVDT|Date|DATE|Y"
            //			if (!checkForm()) 
            //			{
            //				alert("Invalid Date selection")
            //				return false
            //			}
            //		}

            if (str_where == 'top') {
                upper = 6; lower = 1;
                //STARTLOADER('TOP')
                if (int_which_one == 6)
                    SRM_TOP_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?JournalID=" + JOURNALID;
                else
                    SRM_TOP_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>";
            }
            else {
                upper = 11; lower = 7;
                STARTLOADER('BOTTOM')
                MAIN_OPEN_BOTTOM_FRAME = int_which_one
                if (int_which_one == 11)
                    SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?JournalID=" + JOURNALID;
                else if (int_which_one == 9)
                    SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CPO=" + MASTER_OPEN_PURCHASEORDER + "&page=" + MASTER_OPEN_PURCHASEORDER_PAGE + "&cc_sort=" + MASTER_OPEN_PURCHASEORDER_SORT + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&PO=" + document.getElementById("txt_PO").value;
                else if (int_which_one == 8)
                    SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CWO=" + MASTER_OPEN_WORKORDER + "&page=" + MASTER_OPEN_WORKORDER_PAGE + "&cc_sort=" + MASTER_OPEN_WORKORDER_SORT2 + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&PO=" + document.getElementById("txt_WOPO").value;
                else if (int_which_one == 7)
                    SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CWO=" + MASTER_OPEN_WORKORDER + "&page=" + MASTER_OPEN_WORKORDER_PAGE + "&cc_sort=" + MASTER_OPEN_WORKORDER_SORT + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&WO=" + document.getElementById("txt_WO").value;
                else if (int_which_one == 10)
                    SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CWO=" + MASTER_OPEN_WORKORDER + "&page=" + MASTER_OPEN_WORKORDER_PAGE + "&cc_sort=" + MASTER_OPEN_WORKORDER_SORT + "&WO=" + document.getElementById("txt_INVNO").value;
                //				SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CWO=" + MASTER_OPEN_WORKORDER + "&page=" + MASTER_OPEN_WORKORDER_PAGE + "&cc_sort=" + MASTER_OPEN_WORKORDER_SORT + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&WO=" + document.getElementById("txt_INVO").value + "&InvDate='" + document.getElementById("txt_INVDT").value + "'" ;	

                //document.getElementById("txt_INVO").value=""
            }
            imgid = "img" + int_which_one.toString();

            // manipulate images
            for (j = lower; j <= upper; j++) {
                document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
                if (j >= 7)
                    document.getElementById("filterList" + j + "").style.display = "none"
            }

            // unless last image in row

            if (int_which_one != upper)
                document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
            document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"

            if (int_which_one >= 7)
                document.getElementById("filterList" + int_which_one + "").style.display = "block"


            //		document.getElementById("txt_INVO").value=""
            //		document.getElementById("txt_INVDT").value =""
        }

        // receives the url of the page to open plus the required width and the height of the popup
        function update_record(str_redir, wid, hig) {
            window.open(str_redir, "display", "width=" + wid + ",height=" + hig + ",left=100,top=200");
        }

        function WOGO(WH) {
            MASTER_OPEN_PURCHASEORDER_PAGE = 1
            MASTER_OPEN_WORKORDER_PAGE = 1
            swap_div(WH, 'BOTTOM')
        }

        function InvNoChange() {
            document.getElementById("txt_INVDT").value = ""
        }

        function InvDtChange() {
            document.getElementById("txt_INVO").value = ""
        }

        function showCisApprovedalert(f_CisCategory) {
            if (f_CisCategory == "Gross" || f_CisCategory == "Higher Rate" || f_CisCategory == "Standard") {
                $("#CISApprovedSupplier").dialog({
                    title: "CIS Approved Supplier",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width: 200,
                    position: {
                        my: "center",
                        at: "center",
                        of: $("body"),
                        within: $("body")
                    },
                    buttons: { Ok: function () {
                        $(this).dialog("close");
                    }
                    }
                });
            }
        }
// -->
    </script>
</head>
<body onload="showCisApprovedalert(<%="'"+l_ciscategory+"'" %>);initSwipeMenu(0);preloadImages();"
    onunload="macGo()" class="ta">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="img1" id="img1" title='Organisation Information' src="images/1-open.gif"
                    width="127" height="20" border="0" onclick="swap_div(1, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img2" id="img2" title='Address Details' src="images/2-previous.gif" width="72"
                    height="20" border="0" onclick="swap_div(2, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img3" id="img3" title='Financial Information' src="images/3-closed.gif"
                    width="79" height="20" border="0" onclick="swap_div(3, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img4" id="img4" title='Contact Information' src="images/4-closed.gif"
                    width="71" height="20" border="0" onclick="swap_div(4, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img5" id="img5" title='Scope Information' src="images/5-closed.gif" width="61"
                    height="20" border="0" onclick="swap_div(5, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img6" id="img6" title='' src="images/6-closed.gif" width="95" height="20"
                    border="0" style="cursor: pointer" alt="" />
            </td>
            <td>
                <img src="/myImages/spacer.gif" width="246" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="/images/spacer.gif" width="244" height="1" alt="" />
            </td>
        </tr>
    </table>
    <!-- End ImageReady Slices -->
    <div id="TOP_DIV" style="display: block; overflow: hidden">
        <iframe name="SRM_TOP_FRAME" <%=TABLE_DIMS%> src="iframes/iMainDetails.asp?CompanyID=<%=CompanyID%>"
            style="overflow: hidden" frameborder="0"></iframe>
    </div>
    <div id="TOP_DIV_LOADER" style='display: none; overflow: hidden; width: 750; height: 200'
        <%=TABLE_DIMS%>>
        <table width="750" style="height: 180px; border-right: SOLID 1PX #133E71" cellpadding="1"
            cellspacing="2" border="0" class="TAB_TABLE">
            <tr>
                <td rowspan="2" width="70%" height="100%">
                    <table width="100%" style="height: 100%; border: SOLID 1PX #133E71; border-collapse: COLLAPSE"
                        cellpadding="3" cellspacing="0" border="0" class="TAB_TABLE">
                        <tr>
                            <td width="17px">
                            </td>
                            <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">
                                <div id="LOADINGTEXT_TOP">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <table width="730" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <img name="img7" id="img7" src="images/7-open.gif" width="54" height="20" border="0"
                    onclick="swap_div(7, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img8" id="img8" src="images/8-previous.gif" width="85" height="20" border="0"
                    onclick="swap_div(8, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img9" id="img9" src="images/9-closed.gif" width="105" height="20" border="0"
                    onclick="swap_div(9, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img10" id="img10" src="images/10-closed.gif" width="69" height="20" border="0"
                    onclick="swap_div(10, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img11" id="img11" src="images/11-closed.gif" width="67" height="20" border="0"
                    style="cursor: pointer" alt="" />
            </td>
            <td align="right" height="19">
                <table width="100%" cellpadding="4" style="height: 100%" cellspacing="0">
                    <tr>
                        <td align="right" height="24">
                            <div id="filterList7">
                                <table width="330" cellspacing="0" cellpadding="1" style="border: 1px solid #133e71">
                                    <tr style="background-color: #f5f5dc" valign="middle">
                                        <td>
                                            <b>WO :</b>
                                            <input class="textbox" type="text" size="10" name="txt_WO" id="txt_WO" maxlength="6" />
                                        </td>
                                        <td>
                                            &nbsp;<b>STATUS</b> :
                                            <%=lst_work%>
                                        </td>
                                        <td>
                                            <input type="button" onclick="WOGO(7)" value=" GO " class="RSLButtonSmall" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="filterList8" style="display: none">
                                <table width="330" cellspacing="0" cellpadding="1" style='border: 1px solid #133e71'>
                                    <tr style='background-color: #f5f5dc' valign="middle">
                                        <td>
                                            <b>&nbsp;PO :</b>
                                            <input class="textbox" type="text" size="10" name="txt_WOPO" id="txt_WOPO" maxlength="6" />
                                        </td>
                                        <td>
                                            &nbsp;<b>STATUS</b> :
                                            <%=lst_rpo%>
                                        </td>
                                        <td>
                                            <input type="button" onclick="WOGO(8)" value=" GO " class="RSLButtonSmall" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="filterList9" style="display: none">
                                <table width="330" cellspacing="0" cellpadding="1" style="border: 1px solid #133e71">
                                    <tr style='background-color: #f5f5dc' valign="middle">
                                        <td>
                                            <b>&nbsp;PO :</b>
                                            <input class="textbox" type='text' size="10" name="txt_PO" id="txt_PO" maxlength="6" />
                                        </td>
                                        <td>
                                            &nbsp;<b>STATUS</b> :
                                            <%=lst_po%>
                                        </td>
                                        <td>
                                            <input type="button" name="BtnWOGO9" id="BtnWOGO9" onclick="WOGO(9)" value=" GO "
                                                class="RSLButtonSmall" style="cursor: pointer" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--<span id="filterList10" style="display:none"><table width=330 cellspacing=0 cellpadding=1 style='border:1px solid #133e71'><tr style='background-color:beige' valign=middle><td><b>&nbsp;INVOICE :</b> <input class="textbox" type='text' size=19 name="txt_INVO" maxlength=25 onClick=InvNoChange()></td><td><b>&nbsp;Date :</b> <input class="textbox" type='text' size=11 name="txt_INVDT" maxlength=10 onClick=InvDtChange()><img src="/js/FVS.gif" width="1" height="1" name="img_INVDT"></td><td><input type=button onclick="WOGO(10)" value=" GO " class="RSLButtonSmall"></tr></table></span>-->
                            <div id='filterList10' style="display: none">
                                <table width="330" cellspacing="0" cellpadding="1" style='border: 1px solid #133e71'>
                                    <tr style="background-color: #f5f5dc" valign="middle">
                                        <td>
                                            <b>Invoice Number :</b>
                                            <input class="textbox" type="text" size="28" name="txt_INVNO" id="txt_INVNO" />
                                        </td>
                                        <td>
                                            <input type="button" name="BtnWOGO10" id="BtnWOGO10" onclick="WOGO(10)" value=" GO "
                                                class="RSLButtonSmall" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="filterList11" style="display: none">
                                <table width="330" cellspacing="0" cellpadding="1" style="border: 1px solid #133e71">
                                    <tr style="background-color: #f5f5dc" valign="middle">
                                        <td>
                                            <b>Editing Journal Entry</b><select class="textbox" style="visibility: hidden"><option
                                                value=''>1</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#004174">
                <img src="/images/spacer.gif" width="370" height="1" alt="" />
            </td>
        </tr>
    </table>
    <div id="BOTTOM_DIV" style="display: block; overflow: hidden">
        <table width="750" style="height: 280px; border-right: solid 1px #133e71" cellpadding="1"
            cellspacing="0" border="0" class="TAB_TABLE">
            <tr>
                <td>
                    <iframe name="SRM_BOTTOM_FRAME" id="SRM_BOTTOM_FRAME" src="iFrames/iRepairJournal.asp?CompanyID=<%=CompanyID%>"
                        width="100%" height="100%" frameborder="0" style="border: none"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <div id="BOTTOM_DIV_LOADER" style="display: none; overflow: hidden; width: 750px;
        height: 210px">
        <table width="750" style="height: 210px; border-right: solid 1px #133e71" cellpadding="10"
            cellspacing="10" border="0" class="TAB_TABLE">
            <tr>
                <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">
                    <div id="LOADINGTEXT_BOTTOM">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" name="null" />
    </form>
    <!--#include virtual="includes/Bottoms/BodyBottom.asp" -->
    <div id="CISApprovedSupplier" title="CIS Approved Supplier" style="display: none;">
        <p>
            Category -
            <%=l_ciscategory%>
        </p>
    </div>
</body>
</html>
