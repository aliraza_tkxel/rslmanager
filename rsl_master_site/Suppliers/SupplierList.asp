<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match
	Dim TDFunc		   (3)
	Dim searchSQL,searchName


	searchName = Replace(Request("name"),"'","''")
	searchSQL = ""

	If searchName <> "" Then
		searchSQL = " Where (NAME LIKE '%" & searchName & "%' ) "
	Else
		searchName = ""
	End If

	ColData(0)  = "Name|ORGNAME|200"
	SortASC(0)  = "ORGNAME ASC"
	SortDESC(0) = "ORGNAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0)   = ""

	ColData(1)  = "Address|ADDRESS|200"
	SortASC(1)  = "ADDRESS ASC"
	SortDESC(1) = "ADDRESS DESC"
	TDSTUFF(1)  = ""
	TDFunc(1)   = ""

	ColData(2)  = "Postcode|POSTCODE|95"
	SortASC(2)  = "POSTCODE ASC"
	SortDESC(2) = "POSTCODE DESC"
	TDSTUFF(2)  = ""
	TDFunc(2)   = ""

	ColData(3)  = "Trade|COMPANYTRADE|150"
	SortASC(3)  = "COMPANYTRADE ASC"
	SortDESC(3) = "COMPANYTRADE DESC"
	TDSTUFF(3)  = ""
	TDFunc(3)   = ""

	PageName = "SupplierList.asp"
	EmptyText = "No Suppliers found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""onclick=""""load_me("" & rsSet(""ORGID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy

	Call SetSort()

	SQLCODE ="SELECT ORGID, NAME AS ORGNAME, CT.DESCRIPTION AS COMPANYTRADE, ADDRESS1 + ' ' + ADDRESS2 AS ADDRESS, POSTCODE FROM S_ORGANISATION O " &_
				"LEFT JOIN S_COMPANYTRADE CT ON CT.TRADEID = O.TRADE "  &_
				"" & searchSQL   & " " &_
				"ORDER BY " & orderBy
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1
		end if
	End If

	Call Create_Table()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Suppliers --> Supplier List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

        function load_me(OrgID) {
            location.href = "Company.asp?OrgID=" + OrgID
        }

        function quick_find() {
            location.href = "SupplierList.asp?name=" + document.getElementById("findemp").value;
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_employees" title="Supplier List" src="images/tab_suppliers.gif" width="84"
                    height="20" border="0" alt="" />
            </td>
            <td>
                <img src="/myImages/spacer.gif" width="370" height="19" alt="" />
            </td>
            <td nowrap="nowrap">
                <input type="button" name="btn_FIND" id="btn_FIND" title="Search for matches using Development Name."
                    class="RSLBUTTON" value="Quick Find" onclick="quick_find()" style="cursor:pointer" />
                &nbsp;<input type="text" size="18" name="findemp" id="findemp" class="textbox200" style="cursor:pointer" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="2">
                <img src="/myImages/spacer.gif" width="355" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0" style="display: none">
        <tr>
            <td rowspan="2">
                <img name="" src="images/tab_suppliers.gif" width="84" height="20" border="0" alt="" />
            </td>
            <td>
                <img src="/myImages/spacer.gif" width="657" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="/images/spacer.gif" width="610" height="1" alt="" />
            </td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
