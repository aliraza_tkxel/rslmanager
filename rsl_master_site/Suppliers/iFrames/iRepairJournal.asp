<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	'THIS PAGE BUILDS THE WORK ORDER JOURNAL FOR TYHE SUPPLIER CRM

	Dim cnt, customer_id, str_journal_table, status_sql, theURL
	Dim mypage, orderBy, str_table_bottom

	'HOLDS THE ID OF THE SELECTED SUPPLIER
	CompanyID = Request("CompanyID")

	'STORES THE CURRENT OPEN WORK ORDER VALUE, WHICHIS CONTINAULLY ROTATED WITH THE MASTER HOLDING CRM PAGE
	CurrentWorkOrder = Request("CWO")

	' BUILD FILTER SQL STRING
	If request("filterid") = "" Then
		status_sql = ""
	Else
		status_sql = " AND WS.ITEMSTATUSID = " & request("filterid")
	End If

	FilterWO = Replace(Request("WO"), "'", "''")
	If FilterWO <> "" AND isNumeric(FilterWO) Then
		status_sql = status_sql & " AND WO.WOID = " & CLng(FilterWO) & " "
	End If

	Call OpenDB()
	Call build_work_order()
	Call CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function

	Function build_work_order()

		'THIS PART BUILDS THE MAIN WORK ORDER ROWS
		'IT JOINS THE P_WORKORDERENTITY AND P_WORKORDERMODULE TABLES TO GET RESPECTIVE IMAGES
		'THE FOLLOWING ARE RULES WHICH DEFINE HOW THIS SHOULD WORK.
		'IF THE DEVELOPMENTID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS SCHEME WIDE 
		'IF THE BLOCKID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS BLOCK WIDE
		'IF THE PROPERTYID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS PROPERTY SPECIFIC

		cnt = 0
		PageName = "iRepairJournal.asp"
		theURL = theURLSET("cc_sort|page")

		orderBy = "WO.WOID DESC"
		If (Request("CC_Sort") <> "") Then orderBy = Request("CC_Sort")

		Const adCmdText = &H0001

		mypage = Request("page")
		If (NOT isNumeric(mypage)) Then
			mypage = 1
		Else
			mypage = CInt(mypage)
		End If

		If mypage = 0 Then mypage = 1 End If

		pagesize = 8

		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WOM.BIRTH_MODULE_IMAGE, WOM.BIRTH_MODULE_DESCRIPTION, WOE.BIRTH_ENTITY_IMAGE, WOE.BIRTH_ENTITY_DESCRIPTION, " &_
				"N.DISPLAYIMAGE, WO.ORDERID, WO.WOID, WO.TITLE,ISNULL(POS.POSTATUSNAME, 'N/A') AS WOSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, WO.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), WO.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
				"FULLADDRESS = CASE " &_
				"WHEN B.BLOCKID IS NOT NULL THEN 'Block: ' + B.BLOCKNAME + ' Development: ' + D.DEVELOPMENTNAME " &_
				"WHEN D.DEVELOPMENTID IS NOT NULL THEN ' Development: ' + D.DEVELOPMENTNAME " &_
				"ELSE P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE " &_
				"END " &_
				"FROM P_WORKORDER WO " &_
				"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
				"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID " &_
				"LEFT JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_
				"INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN C_STATUS WS ON WS.ITEMSTATUSID = WO.WOSTATUS " &_
				"INNER JOIN P_WORKORDERENTITY WOE ON WOE.BIRTH_ENTITY_ID = WO.BIRTH_ENTITY " &_
				"INNER JOIN P_WORKORDERMODULE WOM ON WOM.BIRTH_MODULE_ID = WO.BIRTH_MODULE " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = WO.BIRTH_NATURE " &_
				"INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PO.POSTATUS " &_
				"WHERE WO.WOSTATUS <> 12 AND PO.SUPPLIERID = " & CompanyID & "  " & status_sql & " " &_ 
				"ORDER BY " & Replace(orderBy, "'", "''")
				
				
				
				
				

 		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize

		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount

	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1

		Dim nextpage, prevpage
		nextpage = mypage + 1
		If nextpage > numpages Then
			nextpage = numpages
		End If
		prevpage = mypage - 1
		If prevpage <= 0 Then
			prevpage = 1
		End If
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF Then
			rsSet.AbsolutePage = mypage
		End If

		If (nextpage = 0) Then nextpage = 1 End If
		If (numpages = 0) Then numpages = 1 End If

		str_journal_table = ""
		For i=1 To pagesize
			If NOT rsSet.EOF Then

			cnt = cnt + 1
			WOID = rsSet("WOID")

			'THE IMAGE TAG IS BUILT HERE, WILL ALWAYS CONTAIN THREEE IMAGES
			ImageTD = "<td style=""background-color:white"" width=""20"" nowrap=""nowrap""><img src=""/myImages/Repairs/" & rsSet("BIRTH_MODULE_IMAGE") & ".gif"" title='" & rsSet("BIRTH_MODULE_DESCRIPTION") & "' width=""20"" height=""18"" border=""0"" alt="""" /></td>" &_
						"<td style=""background-color:white"" width=""20"" nowrap=""nowrap""><img src=""/myImages/Repairs/" & rsSet("BIRTH_ENTITY_IMAGE") & ".gif"" title='" & rsSet("BIRTH_ENTITY_DESCRIPTION") & "' width=""20"" height=""18"" border=""0"" alt="""" /></td>" &_
						"<td style=""background-color:white"" width=""34"" nowrap=""nowrap""><img src=""/myImages/Repairs/" & rsSet("DISPLAYIMAGE") & ".gif"" title=""" & rsSet("TITLE") & """ width=""29"" height=""18"" border=""0"" alt="""" /></td>"

			AddressTitle = ""
			THEADDRESS = rsSet("FULLADDRESS")
			If (Len(THEADDRESS) > 35) Then
				AddressTitle = " title=""" & THEADDRESS & """"
				THEADDRESS = Left(THEADDRESS, 35) & "..."
			End If

			'HERE WE HAVE CHECK IF THE ITEM HAS BEEN SELECTED IN WHICH CASE WE SHOW THE RESPECTIVE REPAIRS FOR THE ITEM
			If ( (CStr(CurrentWorkOrder) = CStr(WOID)) ) Then
				'ON THE STRING THAT APPEARS NEXT YOU WILL SEE THAT AN <A> TAG HAS BEEN ENTERED THIS IS SO THAT THE FRAME
				'CAN BE SCROLLED TO THIS WORK ORDER ON LOAD....
				str_journal_table = str_journal_table & 	"<tr onclick='ReloadWO(" & WOID & ")' style=""cursor:pointer""><a name=""WOID"">" &_
																"<td width=""20"" nowrap=""nowrap"" style=""background-color:white""><img src=""/js/tree/img/minus.gif"" width=""18"" height=""18"" border=""0"" alt="" /></td>" &_
																ImageTD &_
																"<td width=""120"" nowrap=""nowrap""><b>" & rsSet("CREATIONDATE") & "</b></td>" &_
																"<td width=""120"" nowrap=""nowrap""><b>" & WorkNumber(rsSet("WOID")) & "</b></td>" &_
																"<td width=""270"" nowrap=""nowrap"" " & AddressTitle & "><b>" & THEADDRESS & "</b></td>" &_			
																"<td><b>" & rsSet("WOSTATUSNAME")  & "</b></td>" &_
															"<tr>"
				'CHECK WHEHTER THE ITEM HAS BEEN CLICKED ON AGAIN IN WHICH CASE WE CLOSE THE ITEM
				If (Request("CloseWO") <> "1") Then
					BuildRepairList(WOID)	'SHOW THE APPROPRIATE REPAIR ROWS IF APPLICABLE
				Else
				'RESET THE CURRENTWORKORDER VARIABLE AS IT HAS BEEN CLOSED
					CurrentWorkOrder = ""
				End If
			Else
				'THIS PART BUILDS THE STANDARD WORK ORDER LINE WHICH IS CLOSED...
				str_journal_table = str_journal_table & 	"<tr onclick='ReloadWO(" & WOID & ")' style=""cursor:pointer"">" &_
																"<td width=20 nowrap=""nowrap"" style=""background-color:white""><img src=""/js/tree/img/plus.gif"" width=""18"" height=""18"" border=""0"" alt="" /></td>" &_
																ImageTD &_
																"<td width=120 nowrap=""nowrap""><b>" & rsSet("CREATIONDATE") & "</b></td>" &_
																"<td width=120 nowrap=""nowrap""><b>" & WorkNumber(rsSet("WOID")) & "</b></td>" &_
																"<td width=270 nowrap=""nowrap"" " & AddressTitle & "><b>" & THEADDRESS & "</b></td>" &_
																"<td><b>" & rsSet("WOSTATUSNAME")  & "</b></td>" &_
															"<tr>"
			End If

			rsSet.movenext()

			End If
		Next
		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tr><td height=""18"" colspan=""8"" align=""center"">No repair work order entries exist for selected criteria.</td></tr>"
		End If

		'ADD RECORD PAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<tfoot>" &_
		"<tr>" &_
		"<td height=""100%"">" &_
		"</td>" &_
		"</tr>" &_
		"<tr>" &_
		"<td colspan=""8"" align=""center"" height=""20"">" &_
		"<table cellspacing=""0"" cellpadding=""0"" width=""100%"" style=""border-top:1px solid;"">" &_
		"<thead>" &_
		"<tr style=""background-color:#f5f5dc"">"  &_
		"<td width=""100""></td>"  &_
		"<td align=""center"">"  &_
		"<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=""blue"">First</font></b></a> "  &_
		"<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>"  &_
		" Page <font color=red><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b><font color=""blue"">Last</font></b></a>"  &_
		"</td>" &_
		"<td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"">&nbsp;"  &_
		"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px"" />"  &_
		"</td>" &_
		"</tr>" &_
		"</thead>" &_
		"</table>" &_
		"</td>" &_
		"</tr>" &_
		"</tfoot>"

	End Function

	'THIS FUNCTION BUILDS THE ACTUAL REPAIR ITEMS FOR THE SELECTED WORK ORDER
	Function BuildRepairList(WOID)
		cnt2 = 0
		'HUMONGOUS SQL WHICH DOES SOMETHING.....
		'INCLUDING GETTING THE REDIRECT PAGE FOR EACH SELECTED ITEM....

		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF;" &_
					"SELECT 	J.JOURNALID, CURRENTITEMSTATUSID, " &_
					"			CASE WHEN J.ITEMNATUREID = 1 THEN 'iDefectsDetail.asp' " &_
					"			WHEN J.ITEMNATUREID IN (2,20,21,22,34,35,36,37,38,39,40) THEN 'iRepairDetail.asp'  " &_
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE AS FULLADDRESS, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			TITLE = CASE " &_
					"			WHEN J.PROPERTYID IS NULL AND LEN(J.TITLE) > 80 THEN LEFT(J.TITLE,80) + '...' " &_
					"			WHEN J.PROPERTYID IS NOT NULL AND LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
					"			ELSE ISNULL(J.TITLE,'No Title') " &_
					"			END, J.TITLE AS FULLTITLE, J.PROPERTYID, " &_
					"			ISNULL(POS.POSTATUSNAME, 'N/A') AS STATUS ," &_
					"			J.ITEMNATUREID, " &_
                    "			WO.OrderID AS OrderId, " &_
                    "			WTR.ORDERITEMID " &_
					"FROM	 	C_JOURNAL J " &_
					"			INNER JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID " &_
					"			INNER JOIN P_WORKORDER WO ON WTR.WOID = WO.WOID " &_
					"			LEFT JOIN P__PROPERTY P ON J.PROPERTYID = P.PROPERTYID " &_
					"			LEFT JOIN C_STATUS S ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"            INNER JOIN  F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID" &_
                    "            INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PO.POSTATUS " &_
					"WHERE	 	J.ITEMNATUREID IN (1,2,20,21,22,34,35,36,37,38,39,40) " &_
					"			AND	WO.WOID = " & WOID & " " &_
					"ORDER BY WTR.ORDERITEMID DESC"
        
		Call OpenRs (rsSet2, strSQL) 

		While Not rsSet2.EOF

			cnt2 = cnt2 + 1

			TRSTYLE = ""
			If (rsSet2("CURRENTITEMSTATUSID") = 5) Then
				TRSTYLE = ";color:red;text-decoration:line-through"
			End If

			'WE CHECK IF THE EACH REPAIR JOURNAL HAS AN ADDRESS IN WHICH CASE
			'WE DISPLAY THE ADDRESS FOR EACH INDIVIDUAL REPAIR
			RefNo = "0" & cnt2
            'Adding Purchase Item Reference #
             if(cnt2<=9) then RefNo = "0" & RefNo  
             
			If (rsSet2("PROPERTYID") <> "") Then

				'THIS PART GETS THE ADDRESS AND CHECKS WHETHER IT IS TOO LONG IN WHICH CASE IT IS REDUCED IN LENGTH AND A TITLE ADDED
				'TO THE RESPECTIVE TD
				AddressTitle = ""
				THEADDRESS = rsSet2("FULLADDRESS")
				If (Len(THEADDRESS) > 35) Then
					AddressTitle = " title=""" & THEADDRESS & """"
					THEADDRESS = Left(THEADDRESS, 35) & "..."
				End If

				str_journal_table = str_journal_table & "<tr onclick='show_repair_details(""" & rsSet2("OrderId")&"-"&rsSet2("ORDERITEMID")&""","&RefNo&",0"& ")' style='cursor:pointer" & TRSTYLE & "'>" &_
															"<td colspan=""4"" style='background-color:white'><img src=""/js/tree/img/joinlong.gif"" width=""84"" height=""18"" border=""0"" alt="""" /></td>" &_
															"<td colspan=""2"" " & AddressTitle & ">&nbsp;&nbsp;" & THEADDRESS & "</td>" &_
															"<td title=""" & rsSet2("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet2("TITLE") & "</td>" &_
															"<td>&nbsp;&nbsp;" & rsSet2("STATUS")  & "</td>" &_
														"<tr>"
			Else
				str_journal_table = str_journal_table & "<tr onclick='show_repair_details(""" & rsSet2("OrderId")&"-"&rsSet2("ORDERITEMID")&""","&RefNo&",0"& ")' style='cursor:pointer" & TRSTYLE & "'>" &_
															"<td colspan=""4"" style=""background-color:white""><img src=""/js/tree/img/joinlong.gif"" width=""84"" height=""18"" border=""0"" alt="""" /></td>" &_
															"<td colspan=""3"" title=""" & rsSet2("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet2("TITLE") & "</td>" &_
															"<td>&nbsp;&nbsp;" & rsSet2("STATUS")  & "</td>" &_
														"<tr>"
			End If
			rsSet2.movenext()

		Wend
		Call CloseRs(rsSet2)

		If cnt2 = 0 Then
			str_journal_table = str_journal_table & "<tr><td colspan=""9"" align=""center"">No repair item entries exist.</td></tr>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">
    
	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iRepairJournal.asp?CompanyID=<%=CompanyID%>&page=<%=mypage%>&CC_Sort=<%=orderBy%>&CWO=" + WOID + "&filterid=" + parent.document.getElementById("sel_STATUS").value + "&WO=" + parent.document.getElementById("txt_WO").value
	}

	function SORTPAGE(SORT){
		location.href = "iRepairJournal.asp?CompanyID=<%=CompanyID%>&page=<%=mypage%>&CC_Sort=" + SORT + "&CWO=<%=CurrentWorkOrder%>&filterid=" + parent.document.getElementById("sel_STATUS").value + "&WO=" + parent.document.getElementById("txt_WO").value		
	}

	//THIS FUNCTION OPENS UP THE RESPECTIVE REPAIRS JOURNAL
	function open_me(int_nature, int_journal_id, str_redir){
		parent.JOURNALID = int_journal_id
		parent.swap_div(11, "BOTTOM")
		parent.swap_div(6, "top")
	}

	//THIS FUNCTION UPDATES THE CURRENTPURCHASEORDER VARIABLE IN THE PARENT HOLDING CRM
	//IT THEN MAKES THE FRAME SCROLL TO THE TOP OF THE CURRENT SELECTED WORK ORDER...
	function DoSync(){
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		parent.MASTER_OPEN_WORKORDER_PAGE = "<%=mypage%>"
		parent.MASTER_OPEN_WORKORDER_SORT = "<%=orderBy%>"
		location.href = "#WOID"
		}

    function show_repair_details(orderId,refNo,child) {
   		if(refNo<=9) refNo = "00" + refNo;
        else refNo = "0" + refNo;
   		parent.swap_div(11, "BOTTOM")
        parent.SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/iNonRepairJournal.asp?OrderID=" + orderId+"&RefNo="+refNo+"&ischild="+child;
    }
		
	<%= WriteJavaJumpFunction() %>

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM');">
    <table width="746" cellpadding="0" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: #f5f5dc">
                <td style="border-bottom: 1px solid" width="20" height="20">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="20" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="20" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="34" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WO.CREATIONDATE ASC')" />
                        Date
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WO.CREATIONDATE DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WO.WOID ASC')" />
                        Code
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WO.WOID DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid" width="270" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('FULLADDRESS ASC')" />
                        Address
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('FULLADDRESS DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid;" width="142" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WOSTATUSNAME ASC')" />
                        Status
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WOSTATUSNAME DESC')" />
                    </b>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="8">
                    <div style='overflow: auto; height: 165'>
                        <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                            <%=str_journal_table%>
                        </table>
                    </div>
                </td>
            </tr>
            <%=str_table_bottom%>
        </tbody>
    </table>
</body>
</html>
