<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	CompanyID = Request("CompanyID")

	SQL = "SELECT S.SCOPEID, S.CONTRACTNAME, APPROVALSTATUS AS APPROVED, A.DESCRIPTION AS AREAOFWORK, S.DATEAPPROVED, ISNULL(ESTIMATEDVALUE,0) AS ESTIMATEDVALUE FROM S_SCOPE S LEFT JOIN S_AREAOFWORK A ON S.AREAOFWORK = A.AREAOFWORKID WHERE S.ORGID = " & CompanyID & " ORDER BY CONTRACTNAME"
	Call OpenDB()
	Call OpenRs(rsLoader, SQL)
	ContactString = ""
	If (NOT rsLoader.EOF) Then
		WHILE NOT rsLoader.EOF
			APPROVED = rsLoader("APPROVED")
			If (CSTR(APPROVED) = "1") Then
				APPROVED = "YES"
			Else
				APPROVED = "NO"
			End If
			ContactString = ContactString & "<tr>" &_
						"<td width=""240"" nowrap=""nowrap"">" & rsLoader("CONTRACTNAME") & "</td>" &_
						"<td width=""240"" nowrap=""nowrap"">" & rsLoader("AREAOFWORK") & "</td>" &_
						"<td width=""80"" nowrap=""nowrap"" align=""right"">" & FormatCurrency(rsLoader("ESTIMATEDVALUE")) & "</td>" &_
						"<td width=""60"" nowrap=""nowrap"">" & APPROVED & "</td>" &_
						"<td width=""100%"">" & rsLoader("DATEAPPROVED") & "</td>" &_
					"</tr>"
			rsLoader.MoveNext
		Wend
	Else
		ContactString = ContactString & "<tr><td colspan=""5"" align=""center"">No Contracts exist for this company</td></tr>" 
	End If

	Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <table <%=TABLE_DIMS%> cellspacing="0" cellpadding="0" border="0" style="height:192px; border-collapse: collapse;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71">
        <tr><td style="height:3px"></td></tr>
        <tr bgcolor="#133E71" style="color: #FFFFFF">
            <td height="20" width="240" nowrap="nowrap">
                <b>&nbsp;Contract Name:</b>
            </td>
            <td width="240" nowrap="nowrap">
                <b>&nbsp;Area of Work:</b>
            </td>
            <td width="80" nowrap="nowrap">
                <b>&nbsp;Value:</b>
            </td>
            <td width="60" nowrap="nowrap" title='Approval Status'>
                <b>&nbsp;App:</b>
            </td>
            <td width="128" nowrap="nowrap">
                <b>&nbsp;Date Approved:</b>
            </td>
        </tr>
        <tr>
            <td valign="top" height="100%" colspan="5">
                <div style="height: 170; width: 748; overflow: auto" class='TA'>
                    <table cellspacing="0" cellpadding="3" border="1" style='border-collapse: collapse;
                        behavior: url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor="STEELBLUE" width="100%">
                        <%=ContactString%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
