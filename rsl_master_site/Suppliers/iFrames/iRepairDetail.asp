<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, repair_history_id
	Dim Repair_Title, last_status, isDisabled, ButtonValue
	Dim jobstartdate, completiondate,PROPERTYID,ASBESTOS

	journal_id = Request("journalid")
	nature_id = Request("natureid")	

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		CONVERT(NVARCHAR(17), J.CREATIONDATE, 113) AS JOBSTART,J.PROPERTYID AS PROPERTYID, ISNULL(CONVERT(NVARCHAR(17), ISNULL(RC.COMPLETIONDATE, LC.LASTACTIONDATE), 113), 'Not Set') AS COMPLETIONDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, R.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), R.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(R.NOTES, 'N/A') AS NOTES, " &_
					"			R.TITLE, R.REPAIRHISTORYID, O.NAME, " &_
					"			A.ITEMACTIONID, A.DESCRIPTION AS ACTION, PARTACTION = CASE " &_
					"			WHEN LEN(A.DESCRIPTION) > 20 THEN LEFT(A.DESCRIPTION,20) + '...' " &_
					"			ELSE A.DESCRIPTION END , " &_
					"			S.DESCRIPTION AS STATUS, " &_
					"			ID.DESCRIPTION AS ITEMDETAIL " &_
					"FROM		C_JOURNAL J " &_
					"			INNER JOIN C_REPAIR R ON J.JOURNALID = R.JOURNALID " &_
					"			LEFT JOIN R_ITEMDETAIL ID ON ID.ITEMDETAILID = R.ITEMDETAILID  " &_
					"			LEFT JOIN C_STATUS S ON R.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_ACTION A ON R.ITEMACTIONID = A.ITEMACTIONID  " &_
					"			LEFT JOIN S_ORGANISATION O ON R.CONTRACTORID = O.ORGID " &_
					"			LEFT JOIN C_MAXCOMPLETEDREPAIRDATE RC ON RC.JOURNALID = J.JOURNALID " &_
					"			LEFT JOIN C_LASTCOMPLETEDACTION LC ON LC.JOURNALID = J.JOURNALID " &_
					"WHERE		R.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY R.REPAIRHISTORYID DESC"
					

		Call OpenRs (rsSet, strSQL)
		last_action = -1
		str_journal_table = ""
		completiondate = "Not Completed"
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"" valign=""top"">"
			Else
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				Repair_Title = rsSet("ITEMDETAIL")
				repair_history_id = rsSet("REPAIRHISTORYID")
				last_status = rsSet("STATUS")
				last_action = rsSet("ITEMACTIONID")
				jobstartdate = rsSet("JOBSTART")
				LastActionTaken = rsSet("ITEMACTIONID")
				If (LastActionTaken = 6 OR LastActionTaken = 9 OR LastActionTaken = 10 OR LastActionTaken = 15) Then
					completiondate = rsSet("COMPLETIONDATE")
				End If
			End If
				str_journal_table = str_journal_table & 	"<td>" & rsSet("CREATIONDATE") & "</td>" &_
															"<td title='" & rsSet("ACTION") & "'>" & rsSet("PARTACTION") & "</td>" &_
															"<td>" & rsSet("NOTES") & "</td>" &_
															"<td>" & rsSet("NAME")  & "</td>" &_
														"<tr>"
			PROPERTYID=rsSet("PROPERTYID")
			rsSet.movenext()
		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">no journal entries exist.</td></tr></tfoot>"
		End If

		ButtonValue = " Update "
		isDisabled = ""

		SQL = "SELECT * FROM C_ACTION WHERE ITEMACTIONID = " & last_action
		Call OpenRs(rsCheckOption,SQL)
		If (NOT rsCheckOption.EOF) Then
			If (rsCheckOption("CONTRACTORONLY") = "" OR ISNULL(rsCheckOption("CONTRACTORONLY"))) Then
				isDisabled = " disabled=""disabled"" "
				ButtonValue = " No Options "
			End If
		End If
		Call CloseRs(rsCheckOption)

		SQL = "SELECT RISKLEVELID AS RISKLEVEL,SUBSTRING(ASBRISKLEVELDESCRIPTION,1,CHARINDEX(' ',ASBRISKLEVELDESCRIPTION,1)) AS RISK  FROM P_ASBRISKLEVEL WHERE ASBRISKLEVELID IN (SELECT Max(PR.ASBRISKLEVELID) " &_
				" FROM P_PROPERTY_ASBESTOS_RISKLEVEL PR,P_PROPERTY_ASBESTOS_RISK PAR" &_
				" WHERE PR.PROPASBLEVELID = PAR.PROPASBLEVELID" &_
				" AND PR.PROPERTYID='" & PROPERTYID & "')"
		Call OpenRs(rsASB,SQL)
		If (NOT rsASB.EOF) Then
			ASBESTOS="<img src=""../Images/asbestos.gif"" alt="""&rsASB("RISK")&""" id=""ASBESTOS"" />"
		End If
		Call CloseRs(rsASB)
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

     

        function update_repair(int_repair_history_id) {
            var str_win
            str_win = "../PopUps/pRepair.asp?repairhistoryid=" + int_repair_history_id + "&natureid=<%=nature_id%>";
            window.open(str_win, "display", "width=407,height=385, left=200,top=200");
        }

    </script>
</head>
<body class="TA" onload="parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="4">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td width="100%">
                                <b>Repair Information:</b>&nbsp;<%=Repair_Title%>
                            </td>
                            <td nowrap="nowrap">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>&nbsp;
                            </td>
                            <td align="right" width="100" rowspan="2">
                                <input type="button" name="BTN_UPDATE" class="RSLBUTTON" title="<%=ButtonValue%>" value="<%=ButtonValue%>"
                                    style="background-color: #f5f5dc; cursor:pointer" onclick="update_repair(<%=repair_history_id%>)"
                                    <%=isDisabled%> />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>Job Start Date:</b>
                                <%=jobstartdate%>
                                &nbsp;&nbsp;<b>Job Completion Date:</b>
                                <%=completiondate%>&nbsp;&nbsp;<%=ASBESTOS%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 5px">
                <td colspan="4">
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="120px">
                    Date
                </td>
                <td style="border-bottom: 1px solid" width="160px">
                    Action
                </td>
                <td style="border-bottom: 1px solid" width="300px">
                    Notes
                </td>
                <td style="border-bottom: 1px solid" width="200px">
                    Contractor
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
