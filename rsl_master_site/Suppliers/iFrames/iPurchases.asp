<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/URLQueryReturn.asp" -->
<%
	'THIS PAGE BUILDS THE REPAIR PURCHASE ORDERS FOR TYHE SUPPLIER CRM

	Dim cnt, customer_id, str_journal_table, orderby, status_sql, theURL
	Dim mypage, str_table_bottom

	CompanyID = Request("CompanyID")
	CurrentPurchaseOrder = Request("CPO")

	' BIUILD FILTER SQL STRING
	If request("filterid") = "" Then
		status_sql = " AND POSTATUS not in (select POSTATUSID from F_POSTATUS where POSTATUSNAME like '%Cancelled%')"
	Else
		status_sql = " AND POS.POSTATUSID = " & request("filterid") & " AND POSTATUS not in (select POSTATUSID from F_POSTATUS where POSTATUSNAME like '%Cancelled%')" & " "
	End If

	FilterPO = Replace(Request("PO"), "'", "''")
	If FilterPO <> "" AND isNumeric(FilterPO) Then
		status_sql = status_sql & " AND PO.ORDERID = " & cLng(FilterPO) & " "
	End If

	orderby = Request("sort")
	If orderby = "" Then orderby = "PO.ORDERID" End If

	Call OpenDB()
	Call build_purchase_orders()
	Call CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function

	Function build_purchase_orders()

		cnt = 0
		PageName = "iPurchases.asp"
		theURL = theURLSET("cc_sort|page")

		orderBy = "PO.ORDERID DESC"
		If (Request("CC_Sort") <> "") Then orderBy = Request("CC_Sort")

		Const adCmdText = &H0001

		mypage = Request("page")
		If (NOT isNumeric(mypage)) Then
			mypage = 1
		Else
			mypage = CInt(mypage)
		End If

		If mypage = 0 Then mypage = 1 End If

		pagesize = 8

		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT PO.ORDERID, " &_
				"ISNULL(CONVERT(NVARCHAR, Po.PoDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), Po.PoDATE, 108) ,'') AS FORMATTEDDATE, " &_
				"			PODATE, Case When 	 cm.Scheme <> '-' and  cm.block <> '-'  Then cm.Scheme +'/'+cm.block+' ' + PONAME " &_
				"	When cm.Scheme <> '-' then cm.Scheme + ' '+PONAME	 " &_
				"	 Else PONAME END as PONAME, " &_
				" ISNULL(TOTALCOST,0) AS GROSSCOST, POSTATUSNAME,	" &_
				"			ITEMDESCPART = CASE " &_
				"			WHEN LEN(PO.PONOTES) > 40 THEN LEFT(PO.PONOTES,40) + '...' " &_
				"			ELSE ISNULL(PO.PONOTES,'No Title') " &_
				"			END, PO.PONOTES " &_
				"FROM F_PURCHASEORDER PO " &_
				"outer Apply (Select ISNULL(S.SCHEMENAME,'-') as Scheme,ISNULL(B.BLOCKNAME,'-') as block  from CM_ServiceItems SI " &_
				"		LEFT JOIN P_BLOCK B ON SI.BLOCKID=B.BLOCKID " &_
				"		LEFT JOIN   P_SCHEME S ON SI.SchemeId=S.SCHEMEID Or B.SchemeId=S.SCHEMEID " &_
				" where SI.PORef = PO.ORDERID ) cm " &_
				"INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PO.POSTATUS " &_
				"INNER JOIN (SELECT ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PIStatus NOT IN (0) GROUP BY ORDERID) PI1 ON PI1.ORDERID = PO.ORDERID " &_
                "INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI2 ON PI2.ORDERID = PI1.ORDERID  " &_
				"WHERE PO.SUPPLIERID = " & CompanyID & " AND PO.ACTIVE = 1 AND PO.POTYPE IN(1,2) " & status_sql & " " &_ 
				"ORDER BY " & Replace(orderBy, "'", "''")
				
        'RESPONSE.Write(strSQL)
        'RESPONSE.End()
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize

		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount

	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1

		Dim nextpage, prevpage
		nextpage = mypage + 1
		If nextpage > numpages Then
			nextpage = numpages
		End If
		prevpage = mypage - 1
		If prevpage <= 0 Then
			prevpage = 1
		End If
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF Then
			rsSet.AbsolutePage = mypage
		End If

		If (nextpage = 0) Then nextpage = 1 End If
		If (numpages = 0) Then numpages = 1 End If

		PrevAddressString = ""
		str_journal_table = ""
		For i=1 To pagesize
			If NOT rsSet.EOF Then

			cnt = cnt + 1
			ORDERID = rsSet("ORDERID")

			If (CStr(CurrentPurchaseOrder) = CStr(ORDERID)) Then
			str_journal_table = str_journal_table & 	"<tr style=""cursor:pointer"">" &_
															"<td width=""20"" nowrap=""nowrap"" style=""background-color:white;"" onclick=""ReloadPO(" & ORDERID & ")""><img src=""/js/tree/img/minus.gif"" width=""18"" height=""18"" border=""0"" alt="""" /></td>" &_
															"<td width=""120"" nowrap=""nowrap""><b>" & rsSet("FORMATTEDDATE") & "</b></td>" &_
															"<td width=""100"" nowrap=""nowrap""><a href=""#"" onclick=""return popUp('../Popups/PurchaseOrder.asp?OrderID=" & ORDERID & "', '_blank', 'height=470px,width=850px,status=no,scrollbars=no,resizable=yes,location=no,menubar=no,toolbar=no', event);""><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></td>" &_															
															"<td width=""270"" ><b>" & rsSet("PONAME") & "</b></td>" &_
															"<td width=""120""><b></b></td>" &_
															"<td align=""right""><b>" & FormatCurrency(rsSet("GROSSCOST")) & "</b>&nbsp;</td>" &_
														"<tr>"
				Build_Purchase_Item_List(ORDERID)
				CurrentPurchaseOrder = ORDERID
			Else
  
			str_journal_table = str_journal_table & 	"<tr  style=""cursor:pointer"">" &_
															"<td width=""20"" nowrap=""nowrap"" style=""background-color:white;"" onclick=""ReloadPO(" & ORDERID & ")""><img src='/js/tree/img/plus.gif' width=""18"" height=""18"" border=""0"" alt="""" /></td>" &_
															"<td width=""120"" nowrap=""nowrap""><b>" & rsSet("FORMATTEDDATE") & "</b></td>" &_
															"<td width=""100"" nowrap=""nowrap""><a href=""#"" onclick=""return popUp('../Popups/PurchaseOrder.asp?OrderID=" & ORDERID & "', '_blank', 'height=470px,width=850px,status=no,scrollbars=no,resizable=yes,location=no,menubar=no,toolbar=no', event);""><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></td>" &_															
															"<td width=""270"" nowrap=""nowrap""><b>" & rsSet("PONAME")& "</b></td>" &_
															"<td width=""120"" nowrap=""nowrap""></td>" &_
															"<td align=""right""><b>" & FormatCurrency(rsSet("GROSSCOST")) & "</b>&nbsp;</td>" &_
														"<tr>"
			End If

			rsSet.movenext()
            '	"<td width=""120"" nowrap=""nowrap"" onclick='show_non_repair_details(" & ORDERID&",0"&","&rsSet("GROSSCOST")&","""",0,0)'><b>" & rsSet("FORMATTEDDATE") & "</b></td>" &_
														
			End If
		Next
		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tr><td height=""20"" colspan=""6"" align=""center"">No purchase order entries exist for selected criteria.</td></tr>"
		End If

		'ADD RECORD PAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<tfoot>"  &_
		"<tr>"  &_
		"<td height=""100%""></td>"  &_
		"</tr>"  &_
		"<tr>"  &_
		"<td colspan=""8"" align=""center"" height=""20"">" &_
		"<table cellspacing=""0"" cellpadding=""0"" width=""100%"" style=""border-top:1px solid;"">"  &_
		"<thead>"  &_
		"<tr style=""background-color:#f5f5dc"">"  &_
		"<td width=""100""></td>"  &_
		"<td align=""center"">"  &_
		"<a href= '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=""blue"">First</font></b></a> "  &_
		"<a href= '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>"  &_
		" Page <font color=""red""><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b><font color=""blue"">Last</font></b></a>"  &_
		"</td>"  &_
		"<td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value='' size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"">&nbsp;"  &_
		"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px; cursor:pointer"" />"  &_
		"</td>"  &_
		"</tr>"  &_
		"</thead>"  &_
		"</table>"  &_
		"</td>"  &_
		"</tr>"  &_
		"</tfoot>"

	End Function

	Function Build_Purchase_Item_List(ORDERID)

		cnt2 = 0
		strSQL = "SELECT PI.ORDERID,PI.ORDERITEMID, pi.active, PI.ITEMDESC, CASE WHEN LEN(PI.ITEMNAME) > 50 THEN LEFT(PI.ITEMNAME,50) + '...' ELSE PI.ITEMNAME "&_
		"END AS THEITEMNAME,Case when cwd.CMContractorId > 0 Then  PI.itemname+' Cycle Date:' + CONVERT(NVARCHAR(10), cwd.CycleDate,103) ELSE " &_
       "PI.itemname  END as itemname, PI.PIDATE, PI.GROSSCOST, PS.POSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, PI.PIDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), PI.PIDATE, 108) ,'')AS formatteddate, PI.REFERENCENO,PI.PISTATUS " &_
				"FROM F_PURCHASEITEM PI " &_
				" outer Apply(Select cwd.CMContractorId,cwd.CycleDate from CM_ContractorWorkDetail cwd where PurchaseOrderItemId = PI.ORDERITEMID) cwd " &_
				"INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID AND ACTIVE = 1 " &_
				"WHERE PI.ORDERID = " & ORDERID & " " &_
				"ORDER BY pi.ORDERITEMID"
        'RESPONSE.Write(strSQL)
        'RESPONSE.End()
		Call OpenRs_RecCount(rsSet2, strSQL)
		totalPIs = rsSet2.RecordCount
		
      ' response.Write("totalPIs "&totalPIs&" invoiceUploadedPIs "&invoiceUploadedPIs)
		RefNo = 0
        While Not rsSet2.EOF

			cnt2 = cnt2 + 1
		    'Adding Purchase Item Reference #
            if(len(rsSet2("REFERENCENO"))>0) then
             RefNo = rsSet2("REFERENCENO")
            else
             RefNo = RefNo + 1
            end if 
            if(RefNo<=9) then 
              RefNo = "00" & RefNo  
             else 
              RefNo = "0" & RefNo
             end if
			' if(rsSet2("PISTATUS")=9 or rsSet2("PISTATUS")=11 or rsSet2("PISTATUS")=13) then 	
            '    str_journal_table = str_journal_table & "<tr style=""cursor:pointer"">"
            ' else

            'Checking if any purchase item is in earlier stage than Invoice received
            SQL =  " SELECT DISTINCT PI.ORDERITEMID FROM F_PURCHASEITEM PI WHERE PI.ORDERID = " & ORDERID & " AND PI.ORDERITEMID NOT IN (" & rsSet2("ORDERITEMID") & ") AND PI.ACTIVE = 1 AND PISTATUS IN (0,1,2,3,4,5,18) " 
            
            Call OpenRs_RecCount(rsCheckPI, SQL)
            invoiceUploadedPIs = rsCheckPI.RecordCount
            updateParent = 1
            if(invoiceUploadedPIs > 0) then
                updateParent = 0
            end if    

                if IsNull(rsSet2("ITEMNAME")) Then
                    tmpITEMNAME = ""
                else
                    tmpITEMNAME = replace(rsSet2("ITEMNAME"),"'","")
                end if

                str_journal_table = str_journal_table & 	"<tr style=""cursor:pointer"" onclick='show_non_repair_details(""" &rsSet2("ORDERID")&"-"&rsSet2("ORDERITEMID")&""","&RefNo&","&rsSet2("GROSSCOST")&","""&tmpITEMNAME&""","&updateParent&",1)'>" 
            ' end if
				str_journal_table = str_journal_table &"<td style=""background-color:white""><img src=""/js/tree/img/join.gif"" width=""18"" height=""18"" border=""0"" alt="""" /></td>" &_
                                    "<td>&nbsp;&nbsp;" & rsSet2("FORMATTEDDATE") & "</td>" &_
                                    "<td colspan=""2"" title=""" & rsSet2("ITEMNAME") & """>&nbsp;&nbsp;"&RefNo&"  "&rsSet2("ITEMNAME") & "</td>" &_
									"<td>&nbsp;&nbsp;" & rsSet2("POSTATUSNAME")  & "</td>" &_
									"<td align=""right""><font color=""blue"">" & fORMATcURRENCY(rsSet2("GROSSCOST")) & "</font>&nbsp;</td>" &_
									"</tr>"
			rsSet2.movenext()

		Wend
		Call CloseRs(rsSet2)

		If cnt2 = 0 Then
			str_journal_table = str_journal_table & "<tr><td height=20 colspan=""6"" align=center>No purchase item entries exist.</td></tr>"
		End If

	End Function

    function SaveFiles
        Dim Upload, fileName, fileSize, ks, i, fileKey, outFileName

        Set Upload = New FreeASPUpload
        Upload.SaveOne uploadsDirVar, 0, fileName, outFileName

	    ' If something fails inside the script, but the exception is handled
	    If Err.Number<>0 then Exit function

        SaveFiles = ""
        ks = Upload.UploadedFiles.keys
        if (UBound(ks) <> -1) then
            SaveFiles = "<B>Files uploaded:</B> "
            for each fileKey in Upload.UploadedFiles.keys
                SaveFiles = SaveFiles & Upload.UploadedFiles(fileKey).FileName & " (" & Upload.UploadedFiles(fileKey).Length & "B) "
            next
        else
            SaveFiles = "No file selected for upload or the file name specified in the upload form does not correspond to a valid file in the system."
        end if
	    SaveFiles = SaveFiles & "<br>Enter a number = " & Upload.Form("enter_a_number") & "<br>"
	    SaveFiles = SaveFiles & "Checkbox values = " & Upload.Form("checkbox_values") & "<br>"
	    SaveFiles = SaveFiles & "List values = " & Upload.Form("list_values") & "<br>"
	    SaveFiles = SaveFiles & "Text area = " & Upload.Form("t_area") & "<br>"
    end function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

    window.popUpObj = null;

    function popUp(url, winName, params, evt)
    {

    var e = (evt) ? evt : window.event;
    if (window.event) {
        e.cancelBubble = true;
    } else {
        e.stopPropagation();
    }

      // If the window has not been defined or if it has been closes open it.
      if(window.popUpObj == null || window.popUpObj.closed || window.popUpObj.name == undefined)
      {
        window.popUpObj = window.open(url+"&Random=" + new Date(), winName, params);
      }

      // Get window focus
      window.popUpObj.focus();

      // Return false.
      return false;
    }

	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadPO(POID){
   		parent.MASTER_OPEN_PURCHASEORDER = POID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentPurchaseOrder%>",10) == parseInt(POID,10))
			POID = "&CLOSEPO=1"
		location.href = "iPurchases.asp?CompanyID=<%=CompanyID%>&CPO=" + POID + "&page=<%=mypage%>&CC_Sort=<%=orderBy%>&filterid=" + parent.document.getElementById("sel_PO").value + "&PO=" + parent.document.getElementById("txt_PO").value
	}

	function SORTPAGE(SORT){
		location.href = "iPurchases.asp?CompanyID=<%=CompanyID%>&page=1&CC_Sort=" + SORT + "&CPO=<%=CurrentPurchaseOrder%>&filterid=" + parent.document.getElementById("sel_PO").value + "&PO=" + parent.document.getElementById("txt_PO").value
	}

	function DoSync(){
		parent.MASTER_OPEN_PURCHASEORDER = "<%=CurrentPurchaseOrder%>"
		parent.MASTER_OPEN_PURCHASEORDER_PAGE = "<%=mypage%>"
		parent.MASTER_OPEN_PURCHASEORDER_SORT = "<%=orderBy%>"
		location.href = "#POID"
		}

    function show_non_repair_details(orderId, refNo, cost,itemName,updateParent,child) {
     
   		parent.swap_div(11, "BOTTOM")
        if(refNo<=9) refNo = "00" + refNo;
        else refNo = "0" + refNo;
        parent.SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/iNonRepairJournalByPurchaseItemId.asp?OrderID=" + orderId+"&RefNo="+refNo+"&amount="+cost+"&name="+itemName+"&COMPANYID="+ '<%=REQUEST("COMPANYID")%>'+"&ischild="+child+"&updateparent="+updateParent;
        
    }

	<%= WriteJavaJumpFunction() %>

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM');">
    <table width="746" cellpadding="0" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: #f5f5dc">
                <td style="border-bottom: 1px solid" width="20" height="20">
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('PO.PODATE ASC')" />
                        Date :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('PO.PODATE DESC')" />
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="100" nowrap="nowrap">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('PO.ORDERID ASC')" />
                        Order No :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('PO.ORDERID DESC')" />
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="270" nowrap="nowrap">
                    <b><font color="blue">Item:</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('POSTATUSNAME ASC')" />
                        Status :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('POSTATUSNAME DESC')" />
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" align="right">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('GROSSCOST ASC')" />
                        Total Cost :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('GROSSCOST DESC')" />
                    </font></b>
                </td>
            </tr>
            </thead><tbody>
                <tr>
                    <td colspan="6">
                        <div style="overflow: auto; height: 165px">
                            <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                                border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                                <%=str_journal_table%>
                            </table>
                        </div>
                    </td>
                </tr>
                <%=str_table_bottom%>
            </tbody>
    </table>

    

</body>
</html>
