<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "width=""750""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	CompanyID = Request("CompanyID")

	strSQL = 	"SELECT E.FIRSTNAME + ' ' + E.LASTNAME AS REVIEWDBY, ISNULL(CURRENTTURNOVER, 0) AS CURRENTTURNOVER, " &_
				"ISNULL(TURNOVERLIMIT,0) AS TURNOVERLIMIT, " &_
				"ISNULL(CONTRACTLIMIT,0) AS CONTRACTLIMIT, DATEREVIEWED = CONVERT(VARCHAR, DATEREVIEWED,103), " &_
				"DATEAPPROVED = CONVERT(VARCHAR,DATEAPPROVED,103), " &_
				"LATESTACCOUNTDATE = CONVERT(VARCHAR,LATESTACCOUNTDATE,103), " &_
				"AE.FIRSTNAME + ' ' + AE.LASTNAME AS APPROVEDBY FROM S_FINANCIAL F " &_
				"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = F.REVIEWEDBY " &_
				"LEFT JOIN E__EMPLOYEE AE ON AE.EMPLOYEEID = F.APPROVEDBY " &_
				"WHERE F.ORGID = " & CompanyID

	Call OpenDB()
	Call OpenRs (rsSet,strSQL) 

	l_currentturnover = 0
	l_turnoverlimit = 0
	l_contractlimit = 0
	If not rsSet.EOF Then
		' get property details
		l_currentturnover = rsSet("CURRENTTURNOVER")
		l_turnoverlimit = rsSet("TURNOVERLIMIT")
		l_contractlimit = rsSet("CONTRACTLIMIT")
		l_datereviewed = rsSet("DATEREVIEWED")
		l_dateapproved = rsSet("DATEAPPROVED")
		l_approvedby = rsSet("APPROVEDBY")
		l_reviewedby = rsSet("REVIEWDBY")
		l_latestaccountdate = rsSet("LATESTACCOUNTDATE")
	Else
		l_latestaccountdate = "<b>No Financial Figures set.</b>"
	End If

	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script language="JavaScript" src="/js/preloader.js"></script>
    <script language="JavaScript" src="/js/general.js"></script>
    <script language="JavaScript" src="/js/menu.js"></script>
    <script language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <table <%=TABLE_DIMS%> style="height:192px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" height="100%">
                <table width="100%" style="height:100%; border: solid 1px #133e71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td width="150" nowrap class='LONGTITLE'>
                            Current Turnover:
                        </td>
                        <td width="100%">
                            <%=FormatCurrency(l_currentturnover)%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Latest Account Date:
                        </td>
                        <td>
                            <%=l_latestaccountdate%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Reviewed By:
                        </td>
                        <td>
                            <%=l_reviewedby%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Date of Review:
                        </td>
                        <td>
                            <%=l_datereviewed%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Turnover Limit:
                        </td>
                        <td>
                            <%=FormatCurrency(l_turnoverlimit)%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Contract Limit:
                        </td>
                        <td>
                            <%=FormatCurrency(l_contractlimit)%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Approved By:
                        </td>
                        <td>
                            <%=l_approvedby%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Date of Approval:
                        </td>
                        <td>
                            <%=l_dateapproved%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" height="100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
