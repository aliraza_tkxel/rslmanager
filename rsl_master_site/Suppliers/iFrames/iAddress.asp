<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Call OpenDB()
	CompanyID = Request("CompanyID")

	strSQL = 	"SELECT E.FIRSTNAME + ' '  + E.LASTNAME AS FULLNAME, O.* FROM S_ORGANISATION O " &_
				"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = O.BRoADLANDCONTACT " &_
				"WHERE o.ORGID = " & CompanyID

	Call OpenRs (rsSet,strSQL) 

	If not rsSet.EOF Then
		' get property details
		l_buildingname = rsSet("BUILDINGNAME")
		l_address1 = rsSet("ADDRESS1")
		l_address2 = rsSet("ADDRESS2")
		l_address3 = rsSet("ADDRESS3")
		l_towncity = rsSet("TOWNCITY")
		l_county = rsSet("COUNTY")
		l_postcode = rsSet("POSTCODE")
		l_telephone1 = rsSet("TELEPHONE1")
		l_telephone2 = rsSet("TELEPHONE2")
		l_fax = rsSet("FAX")
		l_website = rsSet("WEBSITE")
		l_bc = rsSet("FULLNAME")
	Else
		l_buildingname = "<b>Company Not found !!!!</b>"
	End If

	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <table <%=TABLE_DIMS%> style="height:192px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" height="100%">
                <table width="100%" style="height:100%; border: solid 1px #133e71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td width="120" class='LONGTITLE'>
                            Building Name:
                        </td>
                        <td width="200">
                            <%=l_buildingname%>
                        </td>
                        <td width="120" class='LONGTITLE'>
                            Telephone 1:
                        </td>
                        <td>
                            <%=l_telephone1%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Address 1:
                        </td>
                        <td>
                            <%=l_address1%>
                        </td>
                        <td class='LONGTITLE'>
                            Telephone 2:
                        </td>
                        <td>
                            <%=l_telephone2%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Address 2:
                        </td>
                        <td>
                            <%=l_address2%>
                        </td>
                        <td class='LONGTITLE'>
                            Fax:
                        </td>
                        <td>
                            <%=l_fax%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Address 3:
                        </td>
                        <td>
                            <%=l_address3%>
                        </td>
                        <td class='LONGTITLE'>
                            Web Site:
                        </td>
                        <td>
                            <%=l_website%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Town / City:
                        </td>
                        <td>
                            <%=l_towncity%>
                        </td>
                        <td class='LONGTITLE'>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            County:
                        </td>
                        <td>
                            <%=l_county%>
                        </td>
                        <td class='LONGTITLE'>
                            Broadland Contact:
                        </td>
                        <td>
                            <%=l_bc%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Postcode:
                        </td>
                        <td>
                            <%=l_postcode%>
                        </td>
                        <td class='LONGTITLE'>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="100%">
                        </td>
                        </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
