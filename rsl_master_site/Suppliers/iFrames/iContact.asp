<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "width=""750""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	CompanyID = Request("CompanyID")

	SQL = "SELECT E.EMPLOYEEID, J.JOBTITLE, E.FIRSTNAME, E.LASTNAME, C.WORKDD, C.MOBILE, C.WORKEMAIL, J.ACTIVE FROM E__EMPLOYEE E LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID WHERE ORGID = " & CompanyID
	Call OpenDB()
	Call OpenRs(rsLoader, SQL)
	ContactString = ""
	If (NOT rsLoader.EOF) Then
		WHILE NOT rsLoader.EOF
			active = rsLoader("ACTIVE")
			bgcolor = ""
			If (active <> 1) Then
				bgcolor = " bgcolor=""#F599A8"" title=""Inactive Contact"" "
			End If
			ContactString = ContactString & "<tr " & bgcolor & ">" &_
					"<td width=""140"" nowrap=""nowrap"">" & rsLoader("FIRSTNAME") & " " & rsLoader("LASTNAME") & "</td>" &_
					"<td width=""140"" nowrap=""nowrap"">" & rsLoader("JOBTITLE") & "</td>" &_
					"<td width=""220"" nowrap=""nowrap"">" & rsLoader("WORKEMAIL") & "</td>" &_
					"<td width=""110"" nowrap=""nowrap"">" & rsLoader("WORKDD") & "</td>" &_
					"<td width=""100%"">" & rsLoader("MOBILE") & "</td>" &_
				"</tr>"
			rsLoader.MoveNext
		wend
	Else
		ContactString = ContactString & "<tr><td colspan=""5"" align=""center"">No Contacts exist for this company</td></tr>" 
	End If

	Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <table <%=TABLE_DIMS%> cellspacing="0" cellpadding="0" border="0" style="height:192px; border-collapse: collapse;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71">
        <tr><td style="height:3px"></td></tr>
        <tr bgcolor="#133E71" style="color: #FFFFFF">
            <td height="20" width="140" nowrap="nowrap">
                <b>&nbsp;Name:</b>
            </td>
            <td width="140" nowrap="nowrap">
                <b>&nbsp;Job Title:</b>
            </td>
            <td width="220" nowrap="nowrap">
                <b>&nbsp;Email:</b>
            </td>
            <td width="110" nowrap="nowrap">
                <b>&nbsp;Telephone:</b>
            </td>
            <td width='138' nowrap="nowrap">
                <b>&nbsp;Mobile:</b>
            </td>
        </tr>
        <tr>
            <td valign="top" height="100%" colspan="5">
                <div style="height: 170; width: 748; overflow: auto" class="TA">
                    <table cellspacing="0" cellpadding="3" border="1" style="border-collapse: collapse;
                        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" width="100%">
                        <%=ContactString%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
