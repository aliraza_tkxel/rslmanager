<%@  language="VBScript" codepage="65001" %>
<% 
Response.Expires = -1
Server.ScriptTimeout = 600
%>
<% Response.Buffer = true %>
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include file="../../freeaspupload.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
    Call OpenDB()
	Dim cnt, current_status, str_po_detail, status_sql, theURL, poname, OrderID, rsSet,Upload, cnt2, rsSet2, user_id
	Dim mypage, orderBy, str_table_bottom, isDisabled, invoiceNo,taxDate,OrderItemID, isChild, updateParent
    Dim uploadsDirVar

    uploadsDirVar = Server.MapPath("/Invoice_Images/")
	
    'Don't know why we are checking the Form Method type when we are using REQUEST object    
	if Request.ServerVariables("REQUEST_METHOD") <> "POST" then
        
    	'//Getting RefNo and Amount of Purchased Item
        RefNo =  Request("RefNo")
      '//Getting Purchase Item name
        if(Request("name")<>"") then 
            itemName = " : "&Request("name")
        end if
        'Amount check
        if(Request("amount")>0 and Request("amount")<=9) then
            amount = Request("amount")&".00"
        else
           amount = Request("amount")
        End if
        'Getting the OderId & OrderItemId(if Child Item selected)
	    isChild = Request("ischild")
    	if(InStr(Request("OrderID"), "-") > 0) then
         orderArray = Split(Request("OrderID"), "-")  
         OrderID = orderArray(0)
         OrderItemID = orderArray(1)
        else
         OrderID = Request("OrderID") 
        end if 
         '//Checking if last child item then update parent as well
         Session("OrderID") = Request("OrderID")
        updateParent =  Request("updateparent")  
        
        '//Checking Uploading Request
    	Call build_po_detail()

    end if

    if Request.ServerVariables("REQUEST_METHOD") = "POST" then

        
        SaveFiles()
       'POST Purchase Item Parameters
       
        refNo =Upload.Form("refNo")
        amount =Upload.Form("amount")
        itemName =Upload.Form("itemName")
        isChild =Upload.Form("isChild")
        updateParent =Upload.Form("updateParent")
       'End Parametters
        Call build_po_detail()
	    Call SendEmail(OrderID)

    End if

    Call CloseDB()

	Function build_po_detail()
        'To check if invoice is already attached then disable invoice upload (update) button.
        'strSQL = " SELECT PONAME, ORDERID, POSTATUSNAME, USERID, ISNULL(POL.InvoiceAttached,0) InvoiceAttached FROM F_PURCHASEORDER PO LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID " &_
        '         " OUTER APPLY (SELECT TOP 1 1 As InvoiceAttached FROM F_PURCHASEORDER_LOG iPOL WHERE iPOL.ORDERID = PO.ORDERID AND image_url IS NOT NULL ORDER BY POTIMESTAMP DESC) POL " &_
        '         " WHERE ORDERID = " & OrderID
        
        strSQL = " SELECT PO.PONAME, PS.POSTATUSNAME, PO.USERID FROM F_PURCHASEITEM PI " &_
                 " INNER JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID " &_                 
                 " LEFT JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID " &_                 
                 " WHERE PI.ORDERITEMID = " & OrderItemID 
        
        Call OpenRs (rsSet, strSQL) 
        rsSet.MoveFirst()
        
		str_po_detail = ""
        current_status = rsSet("POSTATUSNAME")
        user_id = rsSet("USERID")

        'To check if invoice is already attached then disable update button.
        'isInvoiceAttached = CBool(rsSet("InvoiceAttached"))        
        'if Not isInvoiceAttached then

        if current_status = "Goods Received" or current_status = "Goods Approved" or current_status = "Invoice Received" or current_status = "Invoice Approved" or current_status = "Work Completed" or current_status = "Part Paid" then 
            isDisabled = ""
        else
            isDisabled = "disabled"
        end if
        poname = rsSet("PONAME")    
        
        BuildPOHistory(OrderID)
		Call CloseRs(rsSet)


	End Function

	Function BuildPOHistory(OrderID)
		cnt2 = 0
        if (isChild=1) then
             strSQL = "SELECT Distinct [TIMESTAMP], POSTATUSNAME,[ACTION], [image_url], O.NAME, PI.PIDATE from F_PURCHASEITEM_LOG PIL " &_
                 "LEFT JOIN F_PURCHASEITEM PI ON PIL.ORDERID = PI.ORDERID  " &_
                 "LEFT JOIN F_POSTATUS PS ON PIL.PISTATUS = PS.POSTATUSID " &_
                 "Inner JOIN F_PURCHASEORDER PO ON PIL.ORDERID = PO.ORDERID " &_
                 "LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_	
                 "WHERE PIL.ORDERITEMID = " & OrderItemID & " Order By PIL.TIMESTAMP, PI.PIDATE"
        else
		    strSQL = "SELECT [TIMESTAMP], POSTATUSNAME,[ACTION], [image_url], O.NAME, PO.PODate from F_PURCHASEORDER_LOG PL " &_
                 "LEFT JOIN F_PURCHASEORDER PO ON PL.ORDERID = PO.ORDERID " &_
                 "LEFT JOIN F_POSTATUS PS ON PL.POSTATUS = PS.POSTATUSID " &_
                 "LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_	
                 "WHERE PL.ORDERID = " & OrderID & " Order By PL.TIMESTAMP, PO.PODate"
        end if         
        'response.Write(strSQL)         
                 
		Call OpenRs (rsSet2, strSQL) 

		While Not rsSet2.EOF

			cnt2 = cnt2 + 1
            Dim attachment_image
            attachment_image = "<td></td>"
            if rsSet2("POSTATUSNAME") = "Invoice Received" Then
                'image_url = "window.location.host" & rsSet2("image_url")
                'response.Write ("image:"&rsSet2("image_url"))
                if(StrComp(rsSet2("image_url"),"/Invoice_Images/")<>0) then
                 attachment_image = "<td style='cursor:pointer;' class='hand-cursor'><img src='../../IMAGES/IconAttachment.gif'" &_
                                   " onclick=window.open('" & Replace(rsSet2("image_url")," ","%20") & "');></td>"
                else
                  attachment_image = "<td>No Attachment</td>"
                end if 
            End If
			str_po_detail = str_po_detail & "<tr>" &_
											    "<td width=""170px"" style=""background-color:white"">" & rsSet2("TIMESTAMP") & "</td>" &_
												"<td>" & rsSet2("POSTATUSNAME") & "</td>" &_
												"<td>" & rsSet2("ACTION")  & "</td>" &_
                                                "<td>" & rsSet2("NAME")  & "</td>" &_
                                                attachment_image &_
											"</tr>"
			rsSet2.movenext()

		Wend
		Call CloseRs(rsSet2)
        
		If cnt2 = 0 Then
		 message = "No history exist for this Purchase Order."
		 if (isChild=1) then
		   message = "No history exist for this Purchase Item."
		 end if
			str_po_detail = str_po_detail & "<tr><td colspan=""9"" align=""center"">"&message&"</td></tr>"
		End If

	End Function


    Function SaveFiles

        Dim FSO
        Set FSO = CreateObject("Scripting.FileSystemObject")
        If NOT (FSO.FolderExists(uploadsDirVar)) Then
            FSO.CreateFolder(uploadsDirVar)
        End If

        Dim fileName, fileSize, ks, i, fileKey, outFileName
        
        Set Upload = New FreeASPUpload
        Upload.SaveOne uploadsDirVar, 0, fileName, outFileName

	    ' If something fails inside the script, but the exception is handled
	    If Err.Number<>0 then Exit function
       'OrderID = Upload.Form("OrderID")
        '//Start Parameters
        'Getting the OderId & OrderItemId(if Child Item selected)
        isChild = Upload.Form("isChild")
	 	if(InStr(Upload.Form("OrderID"), "-") > 0) then
         orderArray = Split(Upload.Form("OrderID"), "-")  
         OrderID = orderArray(0)
         OrderItemID = orderArray(1)
        else
         OrderID =Upload.Form("OrderID")
        end if 
        updateParent =Upload.Form("updateParent")
        Session("OrderID")=Upload.Form("OrderID")
        '//End Parameters
        SaveFiles = ""
        ks = Upload.UploadedFiles.keys
        if (UBound(ks) <> -1) then
            SaveFiles = "<B>Files uploaded:</B> "
            for each fileKey in Upload.UploadedFiles.keys
                SaveFiles = SaveFiles & Upload.UploadedFiles(fileKey).FileName & " (" & Upload.UploadedFiles(fileKey).Length & "B) "
            next
        else
            SaveFiles = "No file selected for upload or the file name specified in the upload form does not correspond to a valid file in the system."
        end if
        SavePOHistory OrderID, Upload.Form("list_values"), Upload.Form("t_area"), "/Invoice_Images/" & outFileName ,Upload.Form("txtInvoiceNo"),Upload.Form("txtTaxDate"),isChild,updateParent, OrderItemID
             
    End Function

    function SavePOHistory(OrderID, POStatus, Notes, ImageURL, InvoiceNo, TaxDate,isChild,updateParent,OrderItemID)
        'ALTER TABLE [RSLBHALive1].[dbo].[F_PURCHASEORDER_LOG] ADD image_url VARCHAR(1000) null ;
        Dim SQLCODE
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
     '//Updating Main Tables (Purchase Order & Item)
        if(isChild=1) then 'Check if selected item is child
         SQLCODE = "UPDATE F_PURCHASEITEM SET PISTATUS = " & POStatus & "  WHERE ORDERITEMID = " & OrderItemID
		 SQL = "SELECT WorkDetailId from CM_ContractorWorkDetail where PurchaseOrderItemId = " & OrderItemID
         Call OpenRs(rs, SQL)
		 if not rs.EOF then
        WorkDetailId = rs("WorkDetailId")        
		end if
        CloseRs(rs)
        if(WorkDetailId > 0) then
		SQLUpdate ="Update CM_ContractorWorkDetail SET StatusId = (Select s.StatusId from CM_Status s where s.Title='Invoice Uploaded' ) where WorkDetailId=" & WorkDetailId
         Conn.Execute(SQLUpdate)
        end if
         'If the last child item is going to update then we have to update parent as well
         if(updateParent = 1) then
          SQLCODE = SQLCODE & "; UPDATE F_PURCHASEORDER SET POSTATUS = " & POStatus & "  WHERE ORDERID = " & OrderID
		  SQL = "Select CMContractorId,ServiceItemId from CM_ContractorWork where PurchaseOrderId =" & OrderID
            Call OpenRs(rs, SQL)
			if not rs.EOF then
              CMContractorId = rs("CMContractorId")  
              ServiceItemId = rs("ServiceItemId") 
			end if	
            CloseRs(rs)
            if(ServiceItemId > 0) then
             SQLUpdate ="Update CM_ServiceItems set StatusId =  (Select s.StatusId from CM_Status s where s.Title='Invoice Uploaded' ) WHERE ServiceItemId = " & ServiceItemId
                 Conn.Execute(SQLUpdate)
            end if 
		  
         end if
        'Otherwise it's parent item
        else
         SQL ="Select Distinct ORDERITEMID from F_PURCHASEITEM Where OrderID= " & OrderID & " and PIStatus<>7"
         Call OpenRs(rsPurchaseItems, SQL)
         '//Updating Purchase Item Logs
         Dim PIArray()
         aryCounter = 0
         While (NOT rsPurchaseItems.EOF)
           ReDim Preserve PIArray(aryCounter) 
           PIArray(aryCounter) = rsPurchaseItems("ORDERITEMID")
           aryCounter = aryCounter + 1
           rsPurchaseItems.movenext
         Wend
          Call CloseRs(rsPurchaseItems) 
         SQLCODE = "UPDATE F_PURCHASEORDER SET POSTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "')" &_
         "; UPDATE F_PURCHASEITEM SET PISTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "')" 
         SQL = "Select CMContractorId,ServiceItemId from CM_ContractorWork where PurchaseOrderId = " & OrderID
            Call OpenRs(rs, SQL)
			if not rs.EOF then
              CMContractorId = rs("CMContractorId")  
              ServiceItemId = rs("ServiceItemId")        
            end if
			CloseRs(rs)
            if(ServiceItemId > 0) then
             SQLUpdate ="Update CM_ServiceItems set StatusId =  (Select s.StatusId from CM_Status s where s.Title='Invoice Uploaded' ) WHERE ServiceItemId = " & ServiceItemId
                 Conn.Execute(SQLUpdate)
            SQLUpdate ="Update CM_ContractorWorkDetail SET StatusId = (Select s.StatusId from CM_Status s where s.Title='Invoice Uploaded' ) where CMContractorId=" & CMContractorId
             Conn.Execute(SQLUpdate)
			  end if
        end if
        Conn.Execute(SQLCODE)
           
       '//Updating Log tables 
      if(isChild = 1)  then
        '//Checking Image Duplication in Purchase Items
        SQL ="Select Distinct ORDERID from F_PURCHASEORDER Where ORDERID= " & orderID & " and POStatus=17"
       'response.Write (SQL)
        Call OpenRs(rsPurchaseItem, SQL)
        if(NOT rsPurchaseItem.EOF) then
         updateSQL = "Update F_PURCHASEORDER set POSTATUS='&ImageURL&' Where ORDERID= " & orderID & " and POStatus=17"
         Conn.Execute(updateSQL)
         'response.Write(updateSQL)
        'else 
        
        end if '//
         Call LOG_PI_ACTION_WITH_IMAGE(orderItemID, ImageURL, Notes," AND PISTATUS=" & POStatus)
        if(updateParent = 1) then
         Call LOG_PO_ACTION_WITH_IMAGE(OrderID, ImageURL, Notes, " AND POSTATUS=" & POStatus)
        end if '//
        Call CloseRs(rsPurchaseItem) 
       '//Updating Purchase Order
       else
        '//Checking Image Duplication in Purchase Items
        SQL ="Select Distinct ORDERID from F_PURCHASEORDER_LOG Where OrderID= " & orderID & " and POStatus=7"
       'response.Write (SQL)
        Call OpenRs(rsPurchaseOrder, SQL)
        
        if(NOT rsPurchaseOrder.EOF) then
         updateSQL = "Update F_PURCHASEOrder_LOG set image_url='"&ImageURL&"' Where OrderID= " & orderID & " and POStatus=7"
         Conn.Execute(updateSQL)
        ' response.Write(updateSQL)
        else 
         Call LOG_PO_ACTION_WITH_IMAGE(OrderID, ImageURL, Notes, " AND POSTATUS=" & POStatus)
         '//Getting Purchase Items
         SQL ="Select Distinct ORDERITEMID from F_PURCHASEITEM Where OrderID= " & OrderID & " and PIStatus<>7"
       '  response.Write (SQL)
         Call OpenRs(rsPurchaseItems, SQL)
         '//Updating Purchase Item Logs
         for each item in PIArray
          '  response.Write("i am here" & item)
          if(len(item)>0) then
           Call LOG_PI_ACTION_WITH_IMAGE(item, ImageURL, Notes," AND PISTATUS=" & POStatus)
          end if 
         Next
         
         Call CloseRs(rsPurchaseOrder) 
       
        End If'// 
         
        '  Call SaveInvoiceInfo (OrderID,InvoiceNo,TaxDate,OrderItemID)
     End If 'Main Log If
      'Storing Invoice & TAX
       'Call LOG_PO_ACTION_WITH_IMAGE(OrderID, ImageURL, Notes, " AND POSTATUS=" & POStatus)
       Call SaveInvoiceInfo (OrderID,InvoiceNo,TaxDate,OrderItemID)
    End Function

    '// Storing Invoice No & Tax Date
    Function SaveInvoiceInfo(OrderID, InvoiceNo,TaxDate,OrderItemID)
        Dim SQLCODE
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
        SQLCODE = "SET NOCOUNT ON;INSERT INTO F_INVOICE (INVOICENUMBER, ORDERID, TAXDATE, USERID,VAT,GROSSCOST,NETCOST,OrderItemId) " &_
					"VALUES ('" & Replace(InvoiceNo,"'","''") & "', " & OrderID & ", '" & TaxDate & "', " &_
					SESSION("USERID") & ",0,0,0,"&OrderItemID&"); SELECT SCOPE_IDENTITY() AS INVOICEID"
         'response.write(SQLCODE)
         Conn.Execute(SQLCODE)
      
    End Function	

    Function SendEmail(OrderId)
        Dim strRecipient, iMsg, iConf, Flds,emailbody, objBP, piFilter, objImg
        piFilter =""
        if(Len(OrderItemID)>0) then
          piFilter = " and OrderItemId="& OrderItemID 
        end if
        SQL = " SELECT DISTINCT C.WorkEmail AS WORKEMAIL " & _
    	      " ,E.FIRSTNAME + ' ' + E.LASTNAME AS RasiedBy " & _
	          " ,BH.FIRSTNAME + ' ' + BH.LASTNAME AS BudgetHolder " & _
              " , PI.ITEMNAME AS ItemName " & _
              " , PI.ITEMDESC AS ItemNotes " & _
              " , O.NAME AS SupplierName " &_
              " , T.GrossTotal AS GrossTotal " &_
              " , PI.GrossCost AS GrossCost " &_
              " , PO.PONotes AS PONotes " &_
              " FROM F_PURCHASEORDER PO " & _
              "  CROSS APPLY (SELECT SUM(iPI.GROSSCOST) GrossTotal FROM F_PURCHASEITEM iPI WHERE iPI.ORDERID = PO.ORDERID AND iPI.ACTIVE = 1) T " & _
              "   /*  Check from purchase order log if Goods are already approved. */ " &_
              "  CROSS APPLY (SELECT TOP 1 1 AS 'GoodsApproved' FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND (POL.POSTATUS = 18 OR POL.POSTATUS = 5 OR POL.POSTATUS = 7)) GoodsApproved " &_
              "  INNER JOIN F_PURCHASEITEM PI ON PO.ORDERID = PI.ORDERID " & _
              "  INNER JOIN E__EMPLOYEE BH ON ISNULL(PI.APPROVED_BY, PI.USERID) = BH.EMPLOYEEID " & _
              "  INNER JOIN E_CONTACT C ON BH.EMPLOYEEID = C.EMPLOYEEID " & _
              "  INNER JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " & _
              "  INNER JOIN E__EMPLOYEE E ON PI.USERID = E.EMPLOYEEID " & _
              " WHERE PO.ORDERID = " & OrderId & piFilter
	    Call OpenRs(rsEmail, SQL)

         emailbody = getHTMLEmailBody()
         'response.Write ("Before:"&emailbody)
        Set iMsg = CreateObject("CDO.Message")
        
        Set iConf = CreateObject("CDO.Configuration")
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        Set iMsg.Configuration = iConf

        iMsg.HTMLBody = emailbody
        
        ' Here's the good part, thanks to some little-known members.
        ' This is a BodyPart object, which represents a new part of the multipart MIME-formatted message.
        ' Note you can provide an image of ANY name as the source, and the second parameter essentially
        ' renames it to anything you want.  Great for giving sensible names to dynamically-generated images.        
        Set objBP = iMsg.AddRelatedBodyPart(Server.MapPath("/myImages/BHA_TAB.gif"), "broadlandImage",cdoRefTypeId)
        Set objImg = iMsg.AddRelatedBodyPart(Server.MapPath("/myImages/50years.gif"), "broadland50YearsImage",cdoRefTypeId)
        ' Now assign a MIME Content ID to the image body part.
        ' This is the key that was so hard to find, which makes it 
        ' work in mail readers like Yahoo webmail & others that don't
        ' recognise the default way Microsoft adds it's part id's,
        ' leading to "broken" images in those readers.  Note the
        ' < and > surrounding the arbitrary id string.  This is what
        ' lets you have SRC="cid:broadlandImage" in the IMG tag.
        objBP.Fields.Item("urn:schemas:mailheader:Content-ID") = "<broadlandImage>"
        objBP.Fields.Update
        objImg.Fields.Item("urn:schemas:mailheader:Content-ID") = "<broadland50YearsImage>"
        objImg.Fields.Update
        iMsg.From = "noreply@broadlandgroup.org"
        'iMsg.Subject = "PO Invoice has been uploaded"
        iMsg.Subject = "Invoice Received"
	   
        While NOT rsEmail.EOF And NOT rsEmail.BOF
		    ' send email
		    
		    strRecipient  = rsEmail("WORKEMAIL")
		    emailbody = getHTMLEmailBody()
            
            emailbody = Replace(emailbody, "{BudgetHolder}",rsEmail("BudgetHolder"))
            if(isChild = 1) then 
             emailbody = Replace(emailbody, "{PONumber}","PO "&OrderId&"/"& refNo)
             emailbody = Replace(emailbody, "{GrossCost}",FormatNumber(rsEmail("GROSSCOST"), 2))
             emailbody = Replace(emailbody, "{ItemName}",rsEmail("ItemName"))
             emailbody = Replace(emailbody, "{ItemNotes}",rsEmail("ItemNotes"))
            else
             emailbody = Replace(emailbody, "{PONumber}","PO "&OrderId)
             emailbody = Replace(emailbody, "{GrossCost}",FormatNumber(rsEmail("GrossTotal"), 2))  
             if (len(itemName)<=0) then
              itemName = rsEmail("ItemName")
             else
              itemName =  itemName & "/" & rsEmail("ItemName")
             end if    
             emailbody = Replace(emailbody, "{ItemName}",itemName)
             emailbody = Replace(emailbody, "{ItemNotes}",rsEmail("PONotes"))
            end if 
            emailbody = Replace(emailbody, "{SupplierName}",rsEmail("SupplierName"))
            emailbody = Replace(emailbody, "{RaisedBy}",rsEmail("RasiedBy"))
            
          ' Check next record to send email.
            rsEmail.movenext
           ' response.Write (emailbody)
	    
        Wend
	    'Sending Email
        'iMsg.To = "aqib.javed@tkxel.com"
         iMsg.To = strRecipient 
         iMsg.HTMLBody = emailbody
	   
	    On Error Resume Next
        iMsg.Send
	    Call CloseRs(rsEmail)                
        Set iMsg = Nothing
        SET iConf = Nothing
        SET Flds = Nothing
        SET objBP = Nothing
         SET objImg = Nothing
    End Function

    Function currentPageURL()
         dim s ,protocol, port

         if Request.ServerVariables("HTTPS") = "on" then 
           s = "s"
         else 
           s = ""
         end if  
 
         protocol = strleft(LCase(Request.ServerVariables("SERVER_PROTOCOL")), "/") & s 

         if Request.ServerVariables("SERVER_PORT") = "80" OR Request.ServerVariables("SERVER_PORT") = "443" then
           port = ""
         else
           port = ":" & Request.ServerVariables("SERVER_PORT")
         end if  

         currentPageURL = protocol & "://" & Request.ServerVariables("SERVER_NAME") & port
                      '& Request.ServerVariables("SCRIPT_NAME")
    End Function

    Function strLeft(str1,str2)
            strLeft = Left(str1,InStr(str1,str2)-1)
    End Function

    Function getHTMLEmailBody()

        getHTMLEmailBody = "<html><head><title>Queued Purchase Order</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "* {font-size: 12px ; font-family: Arial;}"  & _
                           "             </style> " & _
                           "             <span>Dear {BudgetHolder}</span> " & _
                           "             <p>The invoice for the following PO has been received.</p><p>As the budget holder please approve the invoice for payment using the Invoice Received alert.</p><br/><br/> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td>PO Number:</td><td>{PONumber}</td></tr> " & _
                           "                      <tr><td>Supplier Name:</td><td>{SupplierName}</td></tr> " & _
                           "                     <tr><td>Item Name:</td><td>{ItemName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">Notes:</td><td>{ItemNotes}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(&pound;):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>Invoice received</td></tr> " & _
                           "                     <tr><td>Raised by:</td><td>{RaisedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr>Kind regards <br/><b>The Finance Team</b></tr><tr><td><img src=""cid:broadland50YearsImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "               <td class=""bottomAllignedCell""> " & _
                           "                             <img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "

              '             "             <table><tbody><tr><td><img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /></td> " & _
              '             "                         <td class=""bottomAllignedCell""> " & _
              '             "                             Broadland Housing Group<br /> " & _
              '             "                             NCFC, The Jarrold Stand<br /> " & _
              '             "                             Carrow Road, Norwich, NR1 1HU<br /> " & _
              '             "                         </td> " & _
              '             "                     </tr></tbody></table></body></html> "
       ' getHTMLEmailBody = "<html><head><title>Invoice Received</title></head> " & _
        '                   "         <body> " & _
        '                   "             <style type=""text/css""> " & _
        '                   "                 .topAllignedCell{vertical-align: top;} " & _
        '                   "                 .bottomAllignedCell{vertical-align: bottom;} " & _
        '                   "             </style> " & _
        '                   "             <span>Dear {BudgetHolder}</span> " & _
        '                   "             <p>The invoice for {PONumber} has been received and the goods have been approved against the PO.</p><p>As the budget holder please approve the invoice for payment using the Invoice Received alert.</p><br/><br/> " & _
'
 '                          "             <table><tbody><tr>Kind regards <br/><b>The Finance Team</b></tr><tr><td><img src=""cid:broadland50YearsImage"" alt=""Broadland Housing Group"" /></td> " & _
  '                         "                         <td class=""bottomAllignedCell""> " & _
   '                        "                             <img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /> " & _
    '                   
    '                       "                         </td> " & _
    '                       "                     </tr></tbody></table></body></html> "
    End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <script type="text/javascript" src="/js/general.js"></script>
    <script src="../../js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function update_status(self) {

            console.log($(self).val());
            self.disabled = "disabled";

            var btn_txt = $(self).val();
            if (btn_txt == "Save Changes") {
                var po_status = $(".postatus_select option:selected").val();
                var notes = $(".update-postatus-text textarea").val();
                var order_id = $(".order-id").text();
                $('.po-status-form').submit();
            }

            $(self).val(btn_txt == "Update" ? "Save Changes" : "Update");
            $('.po-history-data').toggleClass('hidden');
            $('.update-postatus').toggleClass('hidden');
            $('.po-table-header').toggleClass('hidden');
        }
        //Disabling uploading button

        function changeState(btnName) {
            btnName.style.display = "none";
            document.getElementById("btnSubmit").style.display = "block";
            //btnName.disabled = true;
        }
        function checkFileUpload() {
            var totalFiles = document.getElementById("uploadImage").files.length;
            var submitbutton = document.getElementById("btnUploadInvoice");
            if (totalFiles > 0) {
                //var submitbutton = document.getElementById("btnUploadInvoice");
                document.getElementById("btnUploadInvoice").style.display = "block";
                document.getElementById("btnSubmit").style.display = "none";
            }
            else {
                document.getElementById("btnUploadInvoice").style.display = "none";
                document.getElementById("btnSubmit").style.display = "block";
            }

        }
    </script>
    <link href="../../css/Suppliers/iFrames/iNonRepairJournal.css" rel="stylesheet" type="text/css" />
</head>
<body class="TA" onload="parent.STOPLOADER('BOTTOM');">
    <table width="746" cellpadding="0" cellspacing="0" style="border-collapse: collapse;"
        slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: #f5f5dc">
                <td style="border-bottom: 1px solid" width="20" height="20">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="20" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="20" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="34" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WO.CREATIONDATE ASC')" />
                        Date
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WO.CREATIONDATE DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WO.WOID ASC')" />
                        Code
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WO.WOID DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid" width="270" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('FULLADDRESS ASC')" />
                        Address
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('FULLADDRESS DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid;" width="142" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WOSTATUSNAME ASC')" />
                        Status
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WOSTATUSNAME DESC')" />
                    </b>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="8">
                    <div style='overflow: auto;'>
                        <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                            <thead>
                                <tr>
                                    <td colspan="4">
                                        <table cellspacing="0" cellpadding="1" width="100%">
                                            <tr valign="top">
                                                <td width="100%">
                                                    <%' Request("RefNo")%>
                                                    <b>Order No:</b>&nbsp;<span class='order-id'><%=OrderID & " / " & RefNo%></span>
                                                </td>
                                                <td nowrap="nowrap">
                                                    Current Status:&nbsp;<font color="red"><%=current_status%></font>&nbsp;
                                                </td>
                                                <td align="right" width="100" rowspan="2">
                                                    <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" title="Update"
                                                        value="Update" style="background-color: #f5f5dc; cursor: pointer" onclick="update_status(this);"
                                                        <%=isDisabled%> />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <b>Item:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%=poname&itemName%>
                                                    &nbsp;&nbsp; <b>Amount:</b>&nbsp;
                                                    <%="&pound;"&amount%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 5px">
                                    <td colspan="4">
                                    </td>
                                </tr>
                                <tr valign="top" class='po-table-header'>
                                    <td style="border-bottom: 1px solid" nowrap="nowrap" width="170px">
                                        Date
                                    </td>
                                    <td style="border-bottom: 1px solid" width="160px">
                                        Action
                                    </td>
                                    <td style="border-bottom: 1px solid" width="300px">
                                        Notes
                                    </td>
                                    <td style="border-bottom: 1px solid" width="200px">
                                        Contractor
                                    </td>
                                    <td style="border-bottom: 1px solid" width="100px">
                                        Image
                                    </td>
                                </tr>
                                <tr style="height: 3px">
                                    <td colspan="5">
                                    </td>
                                </tr>
                            </thead>
                            <tbody class='po-history-data'>
                                <%=str_po_detail%>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class='update-postatus hidden'>
        <form name="frmSend" id="frmSend" method="post" enctype="multipart/form-data" accept-charset="utf-8"
        action="iNonRepairJournal.asp" class='po-status-form'>
        <div class='left-container'>
            <div class='update-postatus-select'>
                <span>Status:</span>
                <select class='postatus_select' name="list_values" id="list_values">
                    <option value="7">Invoice Received</option>
                </select>
            </div>
        </div>
        <div class='right-container'>
            <div class='text-boxes'>
                <span>Invoice No:</span>
                <input type="text" name="txtInvoiceNo" id="txtInvoiceNo" maxlength="15" value="<%=invoiceNo%>" />
            </div>
            <div class='text-boxes'>
                <span>Tax Date:</span>
                <input type="text" name="txtTaxDate" id="txtTaxDate" maxlength="10" value="<%=taxDate%>"
                    class='box-label' />
            </div>
            <div class='update-postatus-text'>
                <span>Notes:</span>
                <textarea rows="4" cols="20" name="t_area" id="t_area" maxlength="100" class='text-area'></textarea>
            </div>
            <div class='update-postatus-upload'>
                <input name="uploadImage" type="file" id="uploadImage" accept="image/*, application/pdf" onchange="checkFileUpload()"
                    size="25" class='upload_field' /><br />
            </div>
            <div class='submit-button'>
                <input type="submit" id="btnUploadInvoice" name="btnUploadInvoice" value="Upload and Save" style="display:none;" onclick ="changeState(this)" />
                 <input type="submit" id="btnSubmit" name="btnSubmit" value="Upload and Save" style="display:block;" disabled="disabled"/>
            </div>
        </div>
        <input type="hidden" name='isChild' id='isChild' value="<%=isChild%>">
        <input type="hidden" name='updateParent' id='updateParent' value="<%=updateParent%>">
        <input type="hidden" name='refNo' id='refNo' value="<%=refNo%>">
        <input type="hidden" name='OrderID' id='OrderID' value="<%=Session("OrderID")%>">
        <input type="hidden" name='amount' id='amount' value="<%=amount%>">
        <input type="hidden" name='itemName' id='itemName' value="<%=itemName%>">
        </form>
    </div>
</body>
</html>
