<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "width=""750""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	CompanyID = Request("CompanyID")

	strSQL = 	"SELECT PT.DESCRIPTION AS PAYMENTDESC, CTR.DESCRIPTION AS CTRADE, CT.DESCRIPTION AS CTYPE, ISNULL(O.PICOVERAMOUNT,0) AS FPICOVERAMOUNT, " &_
				"ISNULL(O.INSURANCECO,0) AS FINSURANCECO, O.* FROM S_ORGANISATION O " &_
				"LEFT JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = O.PAYMENTTYPE " &_
				"LEFT JOIN S_COMPANYTYPE CT ON O.COMPANYTYPE = CT.TYPEID " &_
				"LEFT JOIN S_COMPANYTRADE CTR ON O.TRADE = CTR.TRADEID " &_
				"WHERE ORGID = " & CompanyID
	Call OpenDB()
	Call OpenRs (rsSet,strSQL) 

	If not rsSet.EOF Then
		' get property details
		l_companyname = rsSet("NAME")
		l_companytype = rsSet("CTYPE")
		l_companytrade = rsSet("CTRADE")
		l_picoveramount = rsSet("FPICOVERAMOUNT")
		l_insuranceco = rsSet("FINSURANCECO")
		l_certificatenumber = rsSet("CERTIFICATENUMBER")
		l_professionalbody = rsSet("PROFESSIONALBODY")
		l_dateestablished = rsSet("DATEESTABLISHED")
		l_companynumber = rsSet("COMPANYNUMBER")
		l_dunsnumber = rsSet("DUNSNUMBER")
		l_vatregnumber = rsSet("VATREGNUMBER")
		l_ciscategory = rsSet("CISCATEGORY")
		l_ciscertificateno = rsSet("CISCERTIFICATENUMBER")
		l_cisexpirydate = rsSet("CISEXPIRYDATE")
		l_bankname = rsSet("BANKNAME")
		l_sortcode = rsSet("SORTCODE")
		l_accountnumber = rsSet("ACCOUNTNUMBER")
		l_paymentterms = rsSet("PAYMENTTERMS")
		l_paymenttype = rsSet("PAYMENTDESC")
        l_uniqueTaxRefNo = rsSet("UniqueTaxRefNo")
        l_accountname = rsSet("ACCOUNTNAME")
	Else
		l_companyname = "<b>Company Not found !!!!</b>"
	End If

	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="parent.STOPLOADER('TOP')">

    <table <%=TABLE_DIMS%> style="height: 192px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" height="100%">
                <table width="100%" style="height:100%; border: solid 1px #133e71; border-collapse: collapse"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td width="120" class='LONGTITLE'>
                            Company Name :
                        </td>
                        <td width="200">
                            <%=l_companyname%>
                        </td>
                        <td width="120" class='LONGTITLE'>
                            Trade :
                        </td>
                        <td>
                            <%=l_companytrade%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Company Type :
                        </td>
                        <td>
                            <%=l_companytype%>
                        </td>
                        <td class='LONGTITLE'>
                            PI Cover Amount :
                        </td>
                        <td>
                            <%=fORMATcURRENCY(l_picoveramount)%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Date Established:
                        </td>
                        <td>
                            <%=l_dateestablished%>
                        </td>
                        <td class='LONGTITLE'>
                            Insurance Co:
                        </td>
                        <td>
                            <%=fORMATcURRENCY(l_insuranceco)%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Company Number:
                        </td>
                        <td>
                            <%=l_companynumber%>
                        </td>
                        <td class='LONGTITLE'>
                            Professional Body:
                        </td>
                        <td>
                            <%=l_professionalbody%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            DUNS Number:
                        </td>
                        <td>
                            <%=l_dunsnumber%>
                        </td>
                        <td class='LONGTITLE'>
                            Account Name:
                        </td>
                        <td>
                           <%=l_accountname%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            VAT Reg Number:
                        </td>
                        <td>
                            <%=l_vatregnumber%>
                        </td>
                        <td class='LONGTITLE'>
                            Bank Name:
                        </td>
                        <td>
                            <%=l_bankname%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            CIS Category:
                        </td>
                        <td>
                            <%=l_ciscategory%>
                        </td>
                        <td class='LONGTITLE'>
                            Sort Code:
                        </td>
                        <td>
                            <%=l_sortcode%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            CIS Verification No:
                        </td>
                        <td>
                            <%=l_ciscertificateno%>
                        </td>
                        <td class='LONGTITLE'>
                            Account Number:
                        </td>
                        <td>
                            <%=l_accountnUMBER%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Unique Tax Ref No:
                        </td>
                        <td>
                            <%=l_uniqueTaxRefNo%>
                        </td>
                        <td class='LONGTITLE'>
                            Payment Terms:
                        </td>
                        <td>
                            <%=l_paymentterms%>
                            days via
                            <%=l_paymenttype%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
