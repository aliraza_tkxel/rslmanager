<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	'THIS PAGE BUILDS THE REPAIR PURCHASE ORDERS FOR TYHE SUPPLIER CRM

	Dim cnt, customer_id, str_journal_table, orderby, status_sql, theURL
	Dim mypage, str_table_bottom

	CompanyID = Request("CompanyID")
	CurrentWorkOrder = Request("CWO")

	' BUILD FILTER SQL STRING
	If request("filterid") = "" Then
		status_sql = ""
	Else
		status_sql = " AND POS.POSTATUSID = " & request("filterid")
	End If

	FilterPO = Replace(Request("PO"), "'", "''")
	If FilterPO <> "" AND isNumeric(FilterPO) Then
		status_sql = status_sql & " AND PO.ORDERID = " & CLng(FilterPO) & " " 
	End If

	orderby = Request("sort")
	if orderby = "" then orderby = "PO.ORDERID" end if

	Call OpenDB()
	Call build_purchase_orders()
	Call CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function

	Function build_purchase_orders()

		cnt = 0
		PageName = "iAccount.asp"
		theURL = theURLSET("cc_sort|page")

		orderBy = "WO.WOID DESC"
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")

		Const adCmdText = &H0001

		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CInt(mypage)
		end if

		if mypage = 0 then mypage = 1 end if

		pagesize = 8

		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WO.ORDERID, WO.WOID,  " &_
				"ISNULL(CONVERT(NVARCHAR, Po.PoDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), Po.PoDATE, 108) ,'')AS formatteddate, " &_				
				"			PODATE, PONAME, ISNULL(TOTALCOST,0) AS GROSSCOST, POSTATUSNAME,	" &_
				"			ITEMDESCPART = CASE " &_
				"			WHEN LEN(PO.PONAME) > 30 THEN LEFT(PO.PONAME,30) + '...' " &_
				"			ELSE ISNULL(PO.PONAME,'No Title') " &_
				"			END, PO.PONOTES " &_
				"FROM F_PURCHASEORDER PO " &_
				"INNER JOIN P_WORKORDER WO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PO.POSTATUS " &_
                "INNER JOIN (SELECT ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PIStatus NOT IN (0) GROUP BY ORDERID) PI1 ON PI1.ORDERID = PO.ORDERID " &_
                "INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI2 ON PI2.ORDERID = PI1.ORDERID " &_
				"WHERE PO.POSTATUS <> 0 AND PO.SUPPLIERID = " & CompanyID & " AND PO.ACTIVE = 1 " & status_sql & " " &_ 
				"ORDER BY " & Replace(orderBy, "'", "''") 

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize

		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount

	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages
		If mypage < 1 Then mypage = 1

		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF then
			rsSet.AbsolutePage = mypage
		end if

		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if

		PrevAddressString = ""
		str_journal_table = ""
		For i=1 To pagesize
			If NOT rsSet.EOF Then

			cnt = cnt + 1
			ORDERID = rsSet("ORDERID")
			WOID = rsSet("WOID")

			If (CStr(CurrentWorkOrder) = CStr(WOID)) Then
			str_journal_table = str_journal_table & 	"<tr style=""cursor:pointer""><A NAME=""WOID"">" &_
															"<td width=""20"" nowrap=""nowrap"" style='background-color:white' onclick=""ReloadWO(" & WOID & ")""><img src=""/js/tree/img/minus.gif"" width=""18"" height=""18"" border=""0"" /></td>" &_
															"<td width=""120"" nowrap=""nowrap"" ><b>" & rsSet("FORMATTEDDATE") & "</b></TD>" &_
															
                                                            "<td width=""100"" nowrap=""nowrap""><a href=""#"" onclick=""open_me(" & ORDERID & ")""><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></td>" &_
															"<td width=""270"" title=""" & rsSet("PONOTES") & """><b>&nbsp;&nbsp;" & rsSet("ITEMDESCPART")  & "</b></td>" &_
															"<td width=""120""></td>" &_
															"<td align=""right""><b>" & FormatCurrency(rsSet("GROSSCOST"))	  & "</b>&nbsp;</td>" &_
														"<tr>"
				Build_Purchase_Item_List(ORDERID)
				CurrentWorkOrder = WOID
			Else
			str_journal_table = str_journal_table & 	"<tr style=""cursor:pointer"">" &_
															"<td width=""20"" nowrap=""nowrap"" style=""background-color:white"" onclick=""ReloadWO(" & WOID& ")""><img src=""/js/tree/img/plus.gif"" width=""18"" height=""18"" border=""0"" /></td>" &_
															"<td width=""120"" nowrap=""nowrap""><b>" & rsSet("FORMATTEDDATE") & "</b></td>" &_
															"<td width=""100"" nowrap=""nowrap""><a href=""#"" onclick=""open_me(" & ORDERID & ")""><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></td>" &_
															"<td width=""270"" nowrap=""nowrap"" title=""" & rsSet("PONOTES") & """><b>&nbsp;&nbsp;" & rsSet("ITEMDESCPART")  & "</b></td>" &_
															"<td width=""120"" nowrap=""nowrap""></td>" &_
															"<td align=""right""><b>" & FormatCurrency(rsSet("GROSSCOST")) & "</b>&nbsp;</td>" &_
														"<tr>"
			End If
            '"<td width=""120"" nowrap=""nowrap""  onclick='show_repair_details(" & ORDERID&",0,"&rsSet("GROSSCOST")&",0,0)'><b>" & rsSet("FORMATTEDDATE") & "</b></TD>" &_
															
			rsSet.movenext()

			End If
		Next
		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tr><td height=""20"" colspan=""6"" align=""center"">No purchase order entries exist for selected criteria.</td></tr>"
		End If

		'ADD RECORD PAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<tfoot>"  &_
		"<tr>"  &_
		"<td height=""100%""></td>"  &_
		"</tr>"  &_
		"<tr>"  &_
		"<td colspan=""8"" align=""center"" height=""20"">" &_
		"<table cellspacing=""0"" cellpadding=""0"" width=""100%"" style=""border-top:1px solid;"">"  &_
		"<thead>"  &_
		"<tr style=""background-color:#f5f5dc"">"  &_
		"<td width=""100""></td>"  &_
		"<td align=""center"">"  &_
		"<a href= '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=""blue"">First</font></b></a> "  &_
		"<a href= '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>"  &_
		" Page <font color=""red""><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b><font color=""blue"">Last</font></b></a>"  &_
		"</td>"  &_
		"<td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"">&nbsp;"  &_
		"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px; cursor:pointer"" />"  &_
		"</td>"  &_
		"</tr>"  &_
		"</thead>"  &_
		"</table>"  &_
		"</td>"  &_
		"</tr>"  &_
		"</tfoot>" 

	End Function

	Function Build_Purchase_Item_List(ORDERID)
		cnt2 = 0
		strSQL = "SELECT PI.ORDERID, PI.ACTIVE, PI.ITEMDESC, CASE WHEN LEN(PI.ITEMNAME) > 50 THEN LEFT(PI.ITEMNAME,50) + '...' ELSE PI.ITEMNAME END AS THEITEMNAME, PI.ITEMNAME, PI.PIDATE, PI.GROSSCOST, PS.POSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, PI.PIDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), PI.PIDATE, 108) ,'') AS FORMATTEDDATE,PI.ORDERITEMID, PI.PISTATUS,PI.REFERENCENO " &_
				"FROM F_PURCHASEITEM PI " &_
				"INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID AND ACTIVE = 1 " &_
				"INNER JOIN P_WOTOREPAIR WTR ON WTR.ORDERITEMID = PI.ORDERITEMID " &_
				"WHERE PI.ORDERID = " & ORDERID & " " &_
				"ORDER BY PI.ORDERITEMID DESC"


		Call OpenRs_RecCount(rsSet2, strSQL)
		totalPIs = rsSet2.RecordCount
        'Checking if all Purchase Items Approved
	    SQL =  "SELECT DISTINCT PI.ORDERITEMID FROM F_PURCHASEITEM PI WHERE PI.ORDERID = " & ORDERID & " AND PI.ACTIVE = 1 AND PISTATUS IN (3,5,10,11,12,14,17,18,21) " 
        Call OpenRs_RecCount(rsCheckPI, SQL)
		invoiceUploadedPIs = rsCheckPI.RecordCount
		updateParent = 0
       
	    if(totalPIs - invoiceUploadedPIs = 1) then
	     updateParent = 1
	    end if  
        refNo = 0  
		While Not rsSet2.EOF

			cnt2 = cnt2 + 1
			'Adding Purchase Item Reference #
            if(len(rsSet2("REFERENCENO"))>0) then
             RefNo = rsSet2("REFERENCENO")
            else
             RefNo = RefNo + 1
            end if 
            if(RefNo<=9) then 
              RefNo = "00" & RefNo  
             else 
              RefNo = "0" & RefNo
             end if
           ' response.write("status"&rsSet2("PISTATUS"))  
			'if(rsSet2("PISTATUS")=9 or rsSet2("PISTATUS")=11 or rsSet2("PISTATUS")=13) then
             '  str_journal_table = str_journal_table & "<tr style=""cursor:pointer"" >" 
            'else   
               str_journal_table = str_journal_table & "<tr style=""cursor:pointer"" onclick='show_repair_details(""" & ORDERID&"-"&rsSet2("ORDERITEMID")&""","&RefNo&","&rsSet2("GROSSCOST")&","&updateParent&",1"&")'>" 
            'end if   
            str_journal_table = str_journal_table &"<td colspan=""3"" >&nbsp;&nbsp;" & rsSet2("FORMATTEDDATE") & "</td>" &_
															"<td colspan=""1"" title=""" & rsSet2("ITEMNAME") & """>&nbsp;&nbsp;" & rsSet2("THEITEMNAME") & "</td>" &_
															"<td>&nbsp;&nbsp;" & rsSet2("POSTATUSNAME")  & "</td>" &_
															"<td align=""right""><font color=""blue"">" & fORMATcURRENCY(rsSet2("GROSSCOST")) & "</font>&nbsp;</td>" &_
														"<tr>"
			rsSet2.movenext()

		Wend

		Call CloseRs(rsSet2)

		If cnt2 = 0 Then
			str_journal_table = str_journal_table & "<tr><td colspan=""6"" align=""center"">No purchase item entries exist.</td></tr>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function open_me(Order_id){
		event.cancelBubble = true
		

         if (window.showModelessDialog) {        // Internet Explorer
                window.showModelessDialog("../Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")	
            } 
            else {
                window.open("../Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
            }


		}

	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iAccount.asp?CompanyID=<%=CompanyID%>&CWO=" + WOID + "&page=<%=mypage%>&CC_Sort=<%=orderBy%>&filterid=" + parent.document.getElementById("sel_RPO").value + "&PO=" + parent.document.getElementById("txt_WOPO").value
	}

	function SORTPAGE(SORT){
		location.href = "iAccount.asp?CompanyID=<%=CompanyID%>&page=1&CC_Sort=" + SORT + "&CWO=<%=CurrentWorkOrder%>&filterid=" + parent.document.getElementById("sel_RPO").value + "&PO=" + parent.document.getElementById("txt_WOPO").value		
	}

	function DoSync(){
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		parent.MASTER_OPEN_WORKORDER_PAGE = "<%=mypage%>"
		parent.MASTER_OPEN_WORKORDER_SORT2 = "<%=orderBy%>"
		location.href = "#WOID"
	
	}

    function show_repair_details(orderId,refNo,cost,updateParent,child) {
   		parent.swap_div(11, "BOTTOM")
   		if(refNo<=9) refNo = "00" + refNo;
        else refNo = "0" + refNo;
        parent.SRM_BOTTOM_FRAME.location.href = "/suppliers/iframes/iNonRepairJournal.asp?OrderID=" + orderId+"&RefNo="+refNo+"&amount="+cost+"&name=&ischild="+child+"&updateparent="+updateParent;
    }

	<%= WriteJavaJumpFunction() %>

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM');">
    <table width="746" cellpadding="0" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: #f5f5dc">
                <td style="border-bottom: 1px solid" width="20" height="20">
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('PO.PODATE ASC')" />
                        Date :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('PO.PODATE DESC')" />
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="100" nowrap="nowrap">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('PO.ORDERID ASC')" />
                        Order No :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('PO.ORDERID DESC')" />
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="270" nowrap="nowrap">
                    <b><font color="blue">Item:</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('POSTATUSNAME ASC')" />
                        Status :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('POSTATUSNAME DESC')" />
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" align="right">
                    <b><font color="blue">
                        <img src="/myImages/sort_arrow_up.gif" style="cursor: pointer" border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('GROSSCOST ASC')" />
                        Total Cost :
                        <img src="/myImages/sort_arrow_down.gif" style="cursor: pointer" border="0" alt="Sort Descending"
                            onclick="SORTPAGE('GROSSCOST DESC')" />
                    </font></b>
                </td>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="6">
                        <div style="overflow: auto; height: 165px">
                            <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                                border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                                <%=str_journal_table%>
                            </table>
                        </div>
                    </td>
                </tr>
                <%=str_table_bottom%>
            </tbody>
    </table>
</body>
</html>
