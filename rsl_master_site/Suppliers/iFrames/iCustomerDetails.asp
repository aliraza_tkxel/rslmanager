<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST TABLE_DIMS = "width=""750""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	JournalID = Request("JournalID")

	strSQL = 	"SELECT " &_
				"C.FIRSTNAME, C.LASTNAME, " &_
				"ISNULL(P.HOUSENUMBER, 'N/A') AS HOUSENUMBER, ISNULL(P.ADDRESS1, 'N/A') AS ADDRESS1, " &_
				"ISNULL(P.ADDRESS2, 'N/A') AS ADDRESS2, ISNULL(P.ADDRESS3,'N/A') AS ADDRESS3, " &_
				"ISNULL(P.TOWNCITY,'N/A') AS TOWNCITY, ISNULL(P.POSTCODE,'N/A') AS POSTCODE, ISNULL(P.COUNTY,'N/A') AS COUNTY, " &_
				"J.TITLE, PI.PIDATE, PI.EXPPIDATE, " &_
				"isnull(PI.ORDERID,0) as orderid, isnull(PI.GROSSCOST,0) as quotedcost, " &_
				"PR.ESTTIME, PR.DESCRIPTION " &_
				"FROM C_JOURNAL J " &_
				"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
				"INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_
				"INNER JOIN R_ITEMDETAIL IT ON R.ITEMDETAILID = IT.ITEMDETAILID " &_
				"LEFT JOIN R_PRIORITY PR ON PR.PRIORITYID = IT.PRIORITY " &_
				"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"LEFT JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID " &_
				"LEFT JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WTR.ORDERITEMID " &_
				"WHERE R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR RR WHERE RR.JOURNALID = J.JOURNALID) " &_
				"AND J.JOURNALID = " & JournalID
	Call OpenDB()
	Call OpenRs (rsSet,strSQL)

	If not rsSet.EOF Then
		' get property details
		LL = rsSet("FIRSTNAME")
		LLL = rsSet("LASTNAME")
		fullname = LL & " " & LLL
		if (fullname = " ") then fullname = "N/A"
		l_housenumber = rsSet("housenumber")
		l_address1 = rsSet("ADDRESS1")
		l_address2 = rsSet("ADDRESS2")
		l_address3 = rsSet("ADDRESS3")
		l_towncity = rsSet("TOWNCITY")
		l_county = rsSet("COUNTY")
		l_postcode = rsSet("POSTCODE")
		orderid = rsSet("ORDERID")
		l_orderid = PurchaseNumber(rsSet("ORDERID"))
		l_orderdate = rsSet("pIdate")
		l_expdeldate = rsSet("expPIdate")
		l_priority = rsSet("description")
		l_esttime = rsSet("esttime")
		cost = FormatCurrency(rsSet("quotedcost"))
		l_title = rsSet("title")
	Else
		l_buildingname = "<b>Repair Information Not Found !!!!</b>"
	End If

	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        function open_me(Order_id) {
            window.showModalDialog("../Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
        }

        function ShowMap() {
            window.showModalDialog('http://www.streetmap.co.uk/streetmap.dll?postcode2map?<%=l_postcode%>', '', 'DialogHeight=650px; DialogWidth=700px')
        }

    </script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <table <%=TABLE_DIMS%> style="height: 192px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" height="100%">
                <table width="100%" style="height:100%; border: solid 1px #133e71; border-collapse: collapse"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td width="120" class='LONGTITLE'>
                            Repair Info:
                        </td>
                        <td colspan="3">
                            <%=l_title%>
                        </td>
                    </tr>
                    <tr>
                        <td width="120" class='LONGTITLE'>
                            Customer Name:
                        </td>
                        <td width="200">
                            <%=fullname%>
                        </td>
                        <td width="120" class='LONGTITLE'>
                            Order No:
                        </td>
                        <td longtitle='<%=pd_ethnicity%>'>
                            <%=l_orderid%>
                        </td>
                    </tr>
                    <tr>
                        <td width="120" class='LONGTITLE'>
                            Flat/House No:
                        </td>
                        <td width="200">
                            <%=l_housenumber%>
                        </td>
                        <td width="120" class='LONGTITLE'>
                            Order Date:
                        </td>
                        <td>
                            <%=l_orderdate%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Address 1:
                        </td>
                        <td>
                            <%=l_address1%>
                        </td>
                        <td class='LONGTITLE'>
                            Exp Del Date:
                        </td>
                        <td>
                            <%=l_expdeldate%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Address 2:
                        </td>
                        <td>
                            <%=l_address2%>
                        </td>
                        <td class='LONGTITLE'>
                            Est Time:
                        </td>
                        <td>
                            <%=l_esttime%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Address 3:
                        </td>
                        <td>
                            <%=l_address3%>
                        </td>
                        <td class='LONGTITLE'>
                            Priority:
                        </td>
                        <td>
                            <%=l_priority%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Town / City:
                        </td>
                        <td>
                            <%=l_towncity%>
                        </td>
                        <td class='LONGTITLE'>
                            Cost:
                        </td>
                        <td>
                            <%=cost%>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            County:
                        </td>
                        <td>
                            <%=l_county%>
                        </td>
                        <td class='LONGTITLE'>
                            Purchase Order:
                        </td>
                        <td>
                            <a href="javascript:open_me(<%=orderid%>)"><font color="blue"><b>Show FULL Purchase
                                Order</b></font></a>
                        </td>
                    </tr>
                    <tr>
                        <td class='LONGTITLE'>
                            Postcode:
                        </td>
                        <td>
                            <%=l_postcode%>
                        </td>
                        <td class='LONGTITLE'>
                            Map Link:
                        </td>
                        <td>
                            <a href="#" onclick="javascript:ShowMap()"><b><font color="blue">Show Map</font></b></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
