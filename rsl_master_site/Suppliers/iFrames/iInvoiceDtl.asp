<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Connections/db_connection.asp" -->
<!--#include virtual="Includes/Functions/URLQueryReturn.asp" -->
<%
	'THIS PAGE BUILDS THE REPAIR PURCHASE ORDERS FOR TYHE SUPPLIER CRM

	Dim cnt, customer_id, str_journal_table, orderby, status_sql, theURL
	Dim mypage, str_table_bottom,wo, invdate

	CompanyID = Request("CompanyID")
	CurrentPurchaseOrder = Request("CPO")

	' BIUILD FILTER SQL STRING
	If request("filterid") = "" Then
		status_sql = ""
	Else
		status_sql = " AND POS.POSTATUSID = " & request("filterid") & " "
	End If

	FilterPO = Replace(Request("PO"), "'", "''")
	If FilterPO <> "" AND isNumeric(FilterPO) Then
		status_sql = status_sql & " AND PO.ORDERID = " & Cint(FilterPO) & " " 
	End If

	'the filter are not required so have been removed...
	'wo=Request("WO")
	'invdate=Request("InvDate")
	If Request("WO") = "" Then
		wo = "0"
	Else
		wo = Request("WO")
	End If

	'If Request("InvDate") = "''" Then
		invdate="0"
	'End If

	orderby = Request("sort")
	If orderby = "" Then orderby = "PO.ORDERID" End If

	Call OpenDB()
	Call build_purchase_orders()
	Call CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function

	Function build_purchase_orders()

		cnt = 0
		PageName = "iInvoiceDtl.asp"
		theURL = theURLSET("cc_sort|page")

		orderBy = "PO.ORDERID DESC"
		If (Request("CC_Sort") <> "") Then orderBy = Request("CC_Sort")

		Const adCmdText = &H0001

		mypage = Request("page")
		If (NOT isNumeric(mypage)) Then
			mypage = 1
		Else
			mypage = CInt(mypage)
		End If

		If mypage = 0 Then mypage = 1 End If
		
		pagesize = 13

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.CursorType = 2
		rsSet.LockType = 1
		rsSet.CursorLocation = 3

		set spUser = Server.CreateObject("ADODB.Command")
			spUser.ActiveConnection = RSL_CONNECTION_STRING
			spUser.CommandText = "[F_GETSUPPLIER_INVOICE]"
			spUser.CommandType = 4
			spUser.CommandTimeout = 0
			spUser.Prepared = true
			spUser.Parameters.Append spUser.CreateParameter("@Supid", 3, 1,4,CompanyID)
			spUser.Parameters.Append spUser.CreateParameter("@wo", 200, 1,30,Replace(wo, "'", "''"))
			spUser.Parameters.Append spUser.CreateParameter("@poDate", 200, 1,12,Replace(invdate, "'", "''"))
		rsSet.Open spUser

		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize

		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount

	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1

		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF then
			rsSet.AbsolutePage = mypage
		end if

		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if

		PrevAddressString = ""
		str_journal_table = ""

		For i=1 to pagesize

			If NOT rsSet.EOF Then

			cnt = cnt + 1
			ORDERID = rsSet(1)

			str_journal_table = str_journal_table &	"<tr>" &_
				"<td width=""160"" nowrap=""nowrap"">" &  rsSet("Pdate") & "</td>" &_
				"<td width=""140"" align=""left"">" &  rsSet("INVOICENUMBER") & "</td>"
				If IsNull(rsSet("INVOICENUMBER")) Then
					str_journal_table = str_journal_table & "<td width=""110"">" & CreditNumber(ORDERID) & "</td>"
				Else
					str_journal_table = str_journal_table & "<td width=""110"">" & PurchaseNumber(ORDERID) & "</td>"
				End If
			str_journal_table = str_journal_table &	"<td width=""70"" align=""right"">" & rsSet("DEBIT") & "&nbsp;</td>" &_
				"<td width=""140"" align=""right"">" & rsSet("CREDIT") & "&nbsp;</td>" &_
				"<td align=""right"">" & formatcurrency(rsSet("BREAKDOWN")) & "&nbsp;</td>" &_
			"</tr>"

			rsSet.movenext()
			End If
		Next
		Call CloseRs(rsSet)

		IF cnt = 0 then
			str_journal_table = "<tr><td height=""20"" colspan=""6"" align=""center"">No invoice entries exist for selected criteria.</td></tr>"
		End If

		'ADD RECORD PAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<tfoot>"  &_
		"<tr>"  &_
		"<td height=""100%"">"  &_
		"</td>"  &_
		"</tr>"  &_
		"<tr>"  &_
		"<td colspan=""8"" align=""center"" height=""20"">" &_
		"<table cellspacing=""0"" cellpadding=""0"" width=""100%"" style=""border-top:1px solid;"">"  &_
		"<thead>"  &_
		"<tr style=""background-color:#f5f5dc;"">"  &_
		"<td width=""100""></td>"  &_
		"<td align=""center"">"  &_
		"<a href= '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=""blue"">First</font></b></a> "  &_
		"<a href= '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>"  &_
		" Page <font color=""red""><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
		" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b><font color=""blue"">Last</font></b></a>"  &_
		"</td>"  &_
		"<td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"">&nbsp;"  &_
		"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px; cursor:pointer"">"  &_
		"</td>"  &_
		"</tr>"  &_
		"</thead>"  &_
		"</table>"  &_
		"</td>"  &_
		"</tr>"  &_
		"</tfoot>" 

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function open_me(Order_id){
		event.cancelBubble = true
		window.showModelessDialog("../Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")	
		}

	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadPO(POID){
		parent.MASTER_OPEN_PURCHASEORDER = POID	
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentPurchaseOrder%>",10) == parseInt(POID,10))
			POID = "&CLOSEPO=1"
		location.href = "iPurchases.asp?CompanyID=<%=CompanyID%>&CPO=" + POID + "&page=<%=mypage%>&CC_Sort=<%=orderBy%>&filterid=" + parent.document.getElementById("sel_PO").value + "&PO=" + parent.document.getElementById("txt_PO").value
	}

	function SORTPAGE(SORT){
		location.href = "iPurchases.asp?CompanyID=<%=CompanyID%>&page=1&CC_Sort=" + SORT + "&CPO=<%=CurrentPurchaseOrder%>&filterid=" + parent.document.getElementById("sel_PO").value + "&PO=" + parent.document.getElementById("txt_PO").value
	}

	<%= WriteJavaJumpFunction() %>

    </script>
</head>
<body class="TA" onload="parent.STOPLOADER('BOTTOM');">
    <table width="746" cellpadding="0" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: #f5f5dc">
                <td height="20" style="border-bottom: 1px solid" width="65" nowrap="nowrap">
                    <b><font color="blue">
                        <!--<img src="/myImages/sort_arrow_up.gif" style='cursor:pointer' border="0" alt="Sort Ascending" onclick="SORTPAGE('PO.PODATE ASC')" />-->
                        Date :
                        <!--<img src="/myImages/sort_arrow_down.gif" style='cursor:pointer' border="0" alt="Sort Descending" onclick="SORTPAGE('PO.PODATE DESC')" />-->
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="110" nowrap="nowrap" align="right">
                    <b><font color="blue">
                        <!--<img src="/myImages/sort_arrow_up.gif" style='cursor:pointer' border="0" alt="Sort Ascending" onclick="SORTPAGE('PO.ORDERID ASC')" />-->
                        Invoice No :
                        <!--<img src="/myImages/sort_arrow_down.gif" style='cursor:pointer' border="0" alt="Sort Descending" onclick="SORTPAGE('PO.ORDERID DESC')" />-->
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="70" nowrap="nowrap" align="left">
                    <b><font color="blue">
                        <!--<img src="/myImages/sort_arrow_up.gif" style='cursor:pointer' border="0" alt="Sort Ascending" onclick="SORTPAGE('PO.ORDERID ASC')" />-->
                        Order No :
                        <!--<img src="/myImages/sort_arrow_down.gif" style='cursor:pointer' border="0" alt="Sort Descending" onclick="SORTPAGE('PO.ORDERID DESC')" />-->
                    </font></b>
                </td>
                <!--<td style="border-bottom:1px solid" width=270 nowrap><b><font color='blue'>Item:</font></b></td>-->
                <td style="border-bottom: 1px solid" width="80" nowrap="nowrap">
                    <b><font color="blue">
                        <!--<img src="/myImages/sort_arrow_up.gif" style='cursor:pointer' border="0" alt="Sort Ascending" onclick="SORTPAGE('POSTATUSNAME ASC')" />-->
                        Debit :
                        <!--<img src="/myImages/sort_arrow_down.gif" style='cursor:pointer' border="0" alt="Sort Descending" onclick="SORTPAGE('POSTATUSNAME DESC')" />-->
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" width="80" align="right" nowrap="nowrap">
                    <b><font color="blue">
                        <!--<img src="/myImages/sort_arrow_up.gif" style='cursor:pointer' border="0" alt="Sort Ascending" onclick="SORTPAGE('GROSSCOST ASC')" />-->
                        Credit :
                        <!--<img src="/myImages/sort_arrow_down.gif" style='cursor:pointer' border="0" alt="Sort Descending" onclick="SORTPAGE('GROSSCOST DESC')" />-->
                    </font></b>
                </td>
                <td style="border-bottom: 1px solid" align="right" nowrap="nowrap">
                    <b><font color="blue">
                        <!--<img src="/myImages/sort_arrow_up.gif" style='cursor:pointer' border="0" alt="Sort Ascending" onclick="SORTPAGE('GROSSCOST ASC')" />-->
                        Balance :
                        <!--<img src="/myImages/sort_arrow_down.gif" style='cursor:pointer' border="0" alt="Sort Descending" onclick="SORTPAGE('GROSSCOST DESC')" />-->
                    </font></b>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="6">
                    <div style='overflow: auto; height: 165'>
                        <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                            <%=str_journal_table%>
                        </table>
                    </div>
                </td>
            </tr>
            <%=str_table_bottom%>
        </tbody>
    </table>
</body>
</html>
