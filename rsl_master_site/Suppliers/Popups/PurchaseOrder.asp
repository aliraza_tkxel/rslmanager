<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim L_ORDERID,PropertyID, recCount,noOfItems
L_ORDERID = ""
noOfItems=0
PropertyID = Request("PropertyID")
if (PropertyID= "") then PropertyID= -1 end if

if(Request.QueryString("OrderID") <> "") then L_ORDERID = Request.QueryString("OrderID")

Call OpenDB()

SQL = "SELECT PS_S.SCHEMENAME,BL.BLOCKNAME, BLOCKNAME, PO.ORDERID, PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE, PO.PONOTES,  EXPPODATE, " &_
		"ISNULL(O.NAME, 'SUPPLIER NOT ASSIGNED') AS SUPPLIER, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY, O.TELEPHONE1, O.TELEPHONE2, O.FAX, "&_
		"C.FIRSTNAME + ' ' + C.LASTNAME AS CUSTOMERNAME, P.PROPERTYID, P.FLATNUMBER, P.HOUSENUMBER, P.ADDRESS1 AS PADDRESS1, P.ADDRESS2 AS PADDRESS2, P.ADDRESS3 AS PADDRESS3, P.TOWNCITY AS PTOWNCITY, P.POSTCODE AS PPOSTCODE, P.COUNTY AS PCOUNTY, ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, '') AS ORDEREDBY, " &_
		"D.DEVELOPMENTNAME, WO.BLOCKID, WO.SCHEMEID, BL.TOWNCITY as BTC, BL.COUNTY as BCount, BL.POSTCODE as BPost, COMP.DESCRIPTION AS COMPANY FROM F_PURCHASEORDER PO " &_
		"LEFT JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID " &_
        "LEFT JOIN G_COMPANY COMP ON COMP.COMPANYID = PO.COMPANYID " &_
		"LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_
		"LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " &_
		"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = WO.CUSTOMERID " &_
		"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
        "LEFT JOIN P_BLOCK BL ON BL.BLOCKID IN(PO.BLOCKID, WO.BLOCKID,P.BLOCKID) " &_
        "LEFT JOIN P_SCHEME SCH ON SCH.DEVELOPMENTID IN(PO.DEVELOPMENTID, WO.DEVELOPMENTID,P.DEVELOPMENTID) " &_
        "LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = SCH.DEVELOPMENTID " &_
        "LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_
		"LEFT JOIN CM_ContractorWork CW on CW.PURCHASEORDERID=PO.ORDERID " &_
		"LEFT JOIN CM_ServiceItems SI ON  SI.ServiceItemId = CW.ServiceItemId " &_
		"LEFT JOIN P_SCHEME PS_S ON PS_S.SchemeId = si.SCHEMEID " &_
		"WHERE PO.ACTIVE = 1 AND PO.ORDERID = " & L_ORDERID
SQLMAX = "SELECT MAX(PROCESSDATE) AS LASTPAYMENTDATE FROM F_POBACS PB " &_
			"INNER JOIN F_INVOICE INV ON INV.INVOICEID = PB.INVOICEID " &_
			"WHERE INV.ORDERID = " & L_ORDERID
LASTPAYMENT = ""
Call OpenRs(rsLAST, SQLMAX)
if (NOT rsLAST.EOF) then
	LASTPAYMENT = rsLAST("LASTPAYMENTDATE")
end if
Call CloseRs(rsLAST)

Call OpenRs(rsPP, SQL)
if (NOT rsPP.EOF) then
	ORDERID = rsPP("ORDERID")
	PONUMBER = PurchaseNumber(rsPP("ORDERID"))
	PONAME = rsPP("PONAME")
	PODATE = FormatDateTime(rsPP("PODATE"),1)
	if (rsPP("EXPPODATE") <> "" AND NOT isNULL(rsPP("EXPPODATE"))) then
		EXPPODATE = FormatDateTime(rsPP("EXPPODATE"),1)
	end if
	SUPPLIER = rsPP("NAME")
	SUPPLIERID = rsPP("SUPPLIERID")	
	POSTATUSNAME = rsPP("POSTATUSNAME")
	DEVELOPMENT = rsPP("DEVELOPMENTNAME")
	BLOCK = rsPP("BLOCKNAME")
    BTC = rsPP("BTC")
    BCount = rsPP("BCount")
    BPost = rsPP("BPost")
	PROPERTYID = rsPP("PROPERTYID")
    BLOCKID = rsPP("BLOCKID")
    SCHEMEID = rsPP("SCHEMEID")
	PONOTES = rsPP("PONOTES")
    ORDEREDBY = rsPP("ORDEREDBY")
    COMPANY = rsPP("COMPANY")
	SCHEMENAME = rspp("SCHEMENAME")
	'Response.write(SCHEMENAME)
	'Response.end
	'Response.flush
	AddressArray = Array("Supplier", "Address1", "Address2", "TownCity", "PostCode", "County")
	AddressString = ""
	For i=0 to ubound(AddressArray)
		if (rsPP(AddressArray(i)) <> "" AND NOT ISNULL(rsPP(AddressArray(i)))) THEN
			AddressString = AddressString & rsPP(AddressArray(i)) & "<br/>"
		end if
	next
	AddressString = AddressString & "<br/>"
	ContactArray = Array("Telephone1", "Telephone2", "Fax")
	ContactString = ""
	For i=0 to ubound(ContactArray)
		if (rsPP(ContactArray(i)) <> "" AND NOT ISNULL(rsPP(ContactArray(i)))) THEN
			ContactString = ContactString & "<i>" & ContactArray(i) & ":</i> " & rsPP(ContactArray(i)) & "<br/>"
		end if
	next
	if (ContactString <> "") then
		ContactString = ContactString & "<br/>"
	end if

    if (NOT ISNULL(PROPERTYID) and( StrComp(UCase(PONAME),"SCHEME/BLOCK REACTIVE REPAIR WORK ORDER")<>0 OR StrComp(UCase(PONAME),"SCHEME BLOCK PURCHASE ORDER")<>0 )) then
		'THIS SECTION WILL GET THE PROPERTY ADDRESS
		PAddressArray = Array("Housenumber","PAddress1", "PAddress2", "PAddress3", "PTownCity", "PPostCode", "PCounty")		
		PAddressString = ""

		For i=0 to ubound(PAddressArray)
			if (rsPP(PAddressArray(i)) <> "" AND NOT ISNULL(rsPP(PAddressArray(i)))) THEN
                If PAddressArray(i) = "Housenumber" THEN
                    PAddressString = PAddressString & rsPP(PAddressArray(i)) & ","
                Else
				    PAddressString = PAddressString & rsPP(PAddressArray(i)) & "<br/>"
                End If
			end if
		next
		PAddressString = PAddressString & "<br/>"
	end if

    If ( ( StrComp(UCase(PONAME),"SCHEME/BLOCK REACTIVE REPAIR WORK ORDER")=0 ) OR (StrComp(UCase(PONAME),"SCHEME BLOCK PURCHASE ORDER")=0 ) and Not IsNull(ORDERID) ) Then
        
        '------------------ GETTING SCHEME ID AND DEVELOPMENT ----------------
        if(ISNULL(SCHEMEID)) THEN
            SchemePOSQL = " Select PS.SCHEMENAME,PS.SCHEMEID as sid, PD.DEVELOPMENTNAME as pdev from PDR_CONTRACTOR_WORK CW "&_ 
                       " JOIN [PDR_JOURNAL]PJ ON  CW.JournalId = PJ.JOURNALID"&_
                       " JOIN PDR_MSAT PM ON PJ.MSATID = PM.MSATId" &_
                       " JOIN P_SCHEME PS ON PM.SchemeId = PS.SCHEMEID" &_
                       " left join PDR_Development PD on PS.DEVELOPMENTID = PD.DEVELOPMENTID " &_
                       " Where CW.PurchaseOrderId ="&ORDERID
             Call OpenRs(rsSchemeName, SchemePOSQL)
             If (NOT rsSchemeName.EOF) Then
                 SchemeName = rsSchemeName("SCHEMENAME")
                 SCHEMEID = rsSchemeName("sid")
                 DEVELOPMENT = rsSchemeName("pdev")
             End if
        ELSE

            SchemePOSQL = " Select PS.SCHEMENAME,PS.SCHEMEID as sid , PD.DEVELOPMENTNAME as pdev from P_SCHEME PS " &_
            " left join PDR_Development PD on PS.DEVELOPMENTID = PD.DEVELOPMENTID " &_
            "Where SCHEMEID = "&SCHEMEID
            Call OpenRs(rsSchemeName, SchemePOSQL)
             If (NOT rsSchemeName.EOF) Then
                 SchemeName = rsSchemeName("SCHEMENAME")
                 SCHEMEID = rsSchemeName("sid")
                 DEVELOPMENT = rsSchemeName("pdev")
             End if
        END IF

        '-------------------------- GETTING BLOCK ID AND ADDRESS                    
        if(( ISNULL(BLOCKID) OR BLOCKID < 1 ) AND (NOT ISNULL(SCHEMEID))) then
        SQL = "select top 1 BL.blockid as blId,BL.TOWNCITY as BTC, BL.COUNTY as BCount, BL.POSTCODE as BPost from P_BLOCK as BL where schemeid=" &SCHEMEID
        Call OpenRs(rsPP, SQL)
        if (NOT rsPP.EOF) then
            BLOCKID = rsPP("blId")
            BTC = rsPP("BTC")
            BCount = rsPP("BCount")
            BPost = rsPP("BPost")
        end if
        AddressArray = Array( "BTC", "BPost","BCount")
	    PAddressString = ""
	    CountSQL = "select count(BL.blockid)as Total from P_BLOCK as BL where schemeid=" &SCHEMEID 'get count of blocks associated with a scheme
        Call OpenRs(rsRows, CountSQL)
	    For i=0 to ubound(AddressArray)	   
		    if (rsRows("Total")>0)THEN 
			    PAddressString = PAddressString & rsPP(AddressArray(i)) & "<br>"
		    end if
	    next
        PAddressString = PAddressString & "<br>"
        ELSE 
            AddressArray = Array( "BTC", "BPost","BCount")
	        PAddressString = ""
	        For i=0 to ubound(AddressArray)
		        if (rsPP(AddressArray(i)) <> "" AND NOT ISNULL(rsPP(AddressArray(i)))) THEN
			        PAddressString = PAddressString & rsPP(AddressArray(i)) & "<br>"
		        end if
	        next
            PAddressString = PAddressString & "<br>"
        END IF

    End If
    
    '<!--Response.Write(PROPERTYID)
	'if a repair get the property address
    'if ( isNull(PROPERTYID) AND ((NOT isNull(BLOCKID)) OR (NOT isNull(SCHEMEID)))) THEN
        'Response.Write(BTC)
        'Response.End()
        'AddressArray = Array( "BTC", "BPost","BCount")
	    'PAddressString = ""
	    'For i=0 to ubound(AddressArray)
		    'if (rsPP(AddressArray(i)) <> "" AND NOT ISNULL(rsPP(AddressArray(i)))) THEN
			'    PAddressString = PAddressString & rsPP(AddressArray(i)) & "<br>"
		 '   end if
	    'next
       ' PAddressString = PAddressString & "<br>"
      '  Response.Write(PROPERTYID)
     '   Response.Write("x")
    'end if
	

else
	Response.Redirect ("Purchase Order NOt Found Page.asp")
end if
Call CloseRs(rsPP)

SQL = "SELECT CWD.CycleDate,PI.ORDERITEMID, PI.ITEMNAME, PI.ITEMDESC, PI.PIDATE, PI.NETCOST, PI.VAT, V.VATNAME, PI.GROSSCOST, ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, '') AS APPROVEDBY, " &_
		"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE, C.DESCRIPTION AS PURCHASEORDER, H.DESCRIPTION AS PURCHASEHEAD, EX.DESCRIPTION AS PURCHASEEXPEND FROM F_PURCHASEITEM PI " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
        "INNER JOIN F_HEAD H ON EX.HEADID = H.HEADID " &_
		"INNER JOIN F_COSTCENTRE C ON C.COSTCENTREID = H.COSTCENTREID " &_
		"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
        "LEFT JOIN CM_ContractorWorkDetail CWD on CWD.PurchaseOrderItemId = PI.ORDERITEMID "&_
		"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PI.APPROVED_BY " &_
		"WHERE PI.ACTIVE = 1 AND ORDERID = " & L_ORDERID & " ORDER BY ORDERITEMID DESC"

Call OpenRs(rsPI, SQL)
noOfItems = 0	
while (NOT rsPI.EOF)

	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
    ApprovedBy = rsPI("APPROVEDBY")

    PURCHASEORDER = rsPI("PURCHASEORDER")
    PURCHASEHEAD  = rsPI("PURCHASEHEAD")
    PURCHASEEXPEND = rsPI("PURCHASEEXPEND")

	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost

    PIString = PIString & "<TR ALIGN=RIGHT>" &_
            "<TD ALIGN=LEFT width=360>" & DataStorage & rsPI("ITEMNAME") & "</TD>" &_
            "<TD align=center title='" & rsPI("VATNAME") & "' nowrap width=40>" & rsPI("VATCODE") & "</TD>" &_
            "<TD nowrap width=151>" & ApprovedBy & "</TD>" &_
            "<TD nowrap width=80>" & FormatNumber(NetCost,2) & "</TD>" &_
            "<TD nowrap width=75>" & FormatNumber(VAT,2) & "</TD>" &_
            "<TD nowrap width=80>" & FormatNumber(GrossCost,2) & "</TD>" &_
            "<TD nowrap width=5></TD></TR>"

	ITEMDESC = rsPI("ITEMDESC")

	if (NOT isNull(ITEMDESC) AND ITEMDESC <> "") then
		    PIString = PIString & "<TR id=""PIDESCS" & noOfItems & """ STYLE='DISPLAY:NONE'><TD colspan=5>" & PURCHASEORDER & " > " & PURCHASEHEAD & " > " &  PURCHASEEXPEND & "<br /><I>" & rsPI("ITEMDESC") & "</I><br /><I>"&rsPI("CycleDate")&"</I></TD></TR>"
        else
    	    PIString = PIString & "<TR id=""PIDESCS" & noOfItems & """ STYLE='DISPLAY:NONE'><TD colspan=5>" & PURCHASEORDER & " > " & PURCHASEHEAD & " > " &  PURCHASEEXPEND & "</TD></TR>"
	    end if
        noOfItems = noOfItems + 1
	rsPI.moveNext
wend
CloseRs(rsPI)

'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
SQL = "SELECT *, THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE WHERE ORDERID = " & ORDERID
Call OpenRs(rsINV, SQL)
if (NOT rsINV.EOF) then
	iTGC = 0
	iTNC = 0
	iTVA = 0

  	InvoiceString = "<br/>" &_
    "<table width=""791"" cellpadding=""2"" cellspacing=""0"" style=""border:1px solid black"">" &_
        "<tr style='color:white;background-color:#133e71'>" &_
			"<td nowrap=""nowrap"" width=""350"" height=""20""><b>Invoice No:</b></td>" &_
            "<td nowrap=""nowrap"" width=""201""><b>Tax Date:</b></td>" &_
            "<td nowrap=""nowrap"" align=""right"" width=""80""><b>Net (�):</b></td>" &_
            "<td nowrap=""nowrap"" align=""right"" width=""75""><b>VAT (�):</b></td>" &_
            "<td nowrap=""nowrap"" align=""right"" width=""80""><b>Gross (�):</b></td>" &_
            "<td width=""5""></td>" &_
        "</tr>"
	while NOT rsINV.EOF
		iVAT = rsINV("VAT")
		iNetCost = rsINV("NETCOST")
		iGrossCost = rsINV("GROSSCOST")
		iTGC = iTGC + iGrossCost
		iTNC = iTNC + iNetCost
		iTVA = iTVA + iVAT
		InvoiceString = InvoiceString & "<tr>" &_
            "<td nowrap=""nowrap"">" & rsINV("InvoiceNumber") & "</td>" &_
            "<td nowrap=""nowrap"">" & rsINV("TAXDATE") & "</td>" &_
            "<td align=""right"" nowrap=""nowrap"">" & FormatNumber(iNetCost,2) & "</td>" &_
            "<td align=""right"" nowrap=""nowrap"">" & FormatNumber(iVAT,2) & "</td>" &_
            "<td align=""right"" nowrap=""nowrap"">" & FormatNumber(iGrossCost,2) & "</td>" &_
        "</tr>"
		rsINV.movenext
	wend
	InvoiceString = InvoiceString & "</table>"

  InvoiceString = InvoiceString & "<table style=""border:1px solid black;border-top:none"" cellspacing=""0"" cellpadding=""2"" width=""791"">"
	InvoiceString = InvoiceString & "<tr bgcolor=""steelblue"" align=""right""> "
	  InvoiceString = InvoiceString & "<td height=""20"" width=""551"" nowrap=""nowrap"" style=""border:none;color:white""><b>TOTAL : &nbsp;</b></td>"
	  InvoiceString = InvoiceString & "<td width=""80"" bgcolor=""white"" nowrap=""nowrap""><b>" & FormatNumber(iTNC,2) & "</b></td>"
	  InvoiceString = InvoiceString & "<td width=""75"" bgcolor=""white"" nowrap=""nowrap""><b>" & FormatNumber(iTVA,2) & "</b></td>"
	  InvoiceString = InvoiceString & "<td width=""80"" bgcolor=""white"" nowrap=""nowrap""><b>" & FormatNumber(iTGC,2) & "</b></td>"
	  InvoiceString = InvoiceString & "<td width=""5"" bgcolor=""white"" nowrap=""nowrap""></td>"
	InvoiceString = InvoiceString & "</tr>"
  InvoiceString = InvoiceString & "</table>"
end if
Call CloseRs(rsINV)

SQL="SELECT PAR.PROPASBRISKID AS PROPASBESTOSRISK ,PR.PROPASBLEVELID AS PROPASBESTOSLEVELID, " &_
	"PL.ASBRISKLEVELID AS RISKLEVELID, PL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL,PRI.ASBRISKID AS RISK, " &_
	"P.ASBESTOSID AS ELEMENTID, P.RISKDESCRIPTION AS ELEMENTDISCRIPTION FROM P_ASBRISK PRI,"&_
	"P_PROPERTY_ASBESTOS_RISKLEVEL PR left join  P_PROPERTY_ASBESTOS_RISK PAR ON " &_ 
	"PR.PROPASBLEVELID=PAR.PROPASBLEVELID INNER Join P_ASBRISKLEVEL PL " &_
	"ON PR.ASBRISKLEVELID=PL.ASBRISKLEVELID INNER Join P_ASBESTOS P ON " &_
	"PR.ASBESTOSID =P.ASBESTOSID WHERE PAR.ASBRISKID=PRI.ASBRISKID AND PR.PROPERTYID='" & PropertyID & "' and (PR.DateRemoved is null or  CONVERT(DATE, PR.DateRemoved) > '"& date() &"' )"

 Call OpenRs_RecCount(rsASB, SQL)

recCount=rsASB.recordCount

ASBESTOS=""
Dim flag,COLUMNCOUNTER,ROWCOUNTER
redim ASBESTOS_ARRAY(20,recCount + 1)
flag=0
COLUMNCOUNTER=0
ROWCOUNTER=1

    while NOT rsASB.EOF
        for i=0 to COLUMNCOUNTER
            if(ASBESTOS_ARRAY(i,0)= rsASB("RISKLEVEL")) then
                ASBESTOS_ARRAY(i,ROWCOUNTER)=rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b>"
                ROWCOUNTER=ROWCOUNTER+1
                flag=1
                exit for
            end if
        next

        if(flag=0) then
            ASBESTOS_ARRAY(COLUMNCOUNTER,0)=rsASB("RISKLEVEL")
            ASBESTOS_ARRAY(COLUMNCOUNTER,ROWCOUNTER)=rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b>"
            COLUMNCOUNTER=COLUMNCOUNTER+1
            ROWCOUNTER=ROWCOUNTER+1
         end if
	flag=0
        rsASB.movenext
    wend
    Call CloseRs(rsASB)

    if(COLUMNCOUNTER>0) then
        ASBESTOS="<table cellspacing=""0"" cellpadding=""2"" width=""791"" border=""0"">" &_
            "<tr>" &_
                "<td colspan=""2""><b>ASBESTOS RISK:</b></td>" &_
            "</tr>"
        for i=0 to COLUMNCOUNTER+1
            if(i<1 and ASBESTOS_ARRAY(i+1,0) = "") then
                ASBESTOS=ASBESTOS & "<tr><td style='border:1px solid #133e71; border-right-color:#FFFFFF;' valign='top' width='400px'>" & ASBESTOS_ARRAY(i,0) & "</td><td style='border:1px solid #133e71;' valign=top width=791px>"
             elseif(i=1) then
                ASBESTOS=ASBESTOS & "<tr><td style='border:1px solid #133e71; border-right-color:#FFFFFF;' valign='top' width='400px'>" & ASBESTOS_ARRAY(i,0) & "</td><td style='border:1px solid #133e71;' valign=top width=791px>"
             else
                 ASBESTOS=ASBESTOS & "<tr><td style='border:1px solid #133e71; border-right-color:#FFFFFF; border-bottom-color:#FFFFFF;' valign='top' width='400px'>" & ASBESTOS_ARRAY(i,0) & "</td><td style='border:1px solid #133e71;border-bottom-color:#FFFFFF;' valign='top' width='791px'>"
            end if
            for j=1 to ROWCOUNTER
                If(ASBESTOS_ARRAY(i,j) <> "") then
                    ASBESTOS=ASBESTOS & ASBESTOS_ARRAY(i,j) & "<br/>" 
                 end if
            next 

            ASBESTOS=ASBESTOS & "</td></tr>"
       next
       ASBESTOS=ASBESTOS & "</table>"
    end if
CloseDB()

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript">

        function PrintThePP() {
            document.getElementById("PrintButton").style.visibility = "hidden";
            document.getElementById("SHOWDESC").style.visibility = "hidden";
            ReturnBack = 0
            if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
                ToggleDescs()
                ReturnBack = 1
            }
            window.print();
            if (ReturnBack == 1) ToggleDescs()
            document.getElementById("PrintButton").style.visibility = "visible";
            document.getElementById("SHOWDESC").style.visibility = "visible";
        }

        function ToggleDescs(noOfItems) {
            if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
                NewText = "HIDE DESCRIPTIONS"
                NewStatus = "block"
            }
            else {
                NewText = "SHOW DESCRIPTIONS"
                NewStatus = "none"
            }
            for (a = 0; a < noOfItems; a++) {
                var pItemId = "PIDESCS" + a
                document.getElementById(pItemId).style.display = NewStatus
                iDesc = document.getElementById(pItemId)
                if (iDesc.length) {
                    for (i = 0; i < iDesc.length; i++)
                        iDesc[i].style.display = NewStatus
                }
            }
            document.getElementById("SHOWDESC").innerHTML = NewText
        }

    </script>
</head>
<body class='TA'>
<table cellspacing=0 cellpadding=0 width="791" ALIGN=CENTER>
  <tr><td width=10></td><td>

      <TABLE BORDER=0 STYLE='BORDER:1PX SOLID BLACK' CELLSPACING=0 CELLPADDING=2 width=791>
        <TR width="100%"> 
          <TD width="40%" nowrap valign=top height=100> <b>Supplier Details</b><br>
            <%=AddressString%> <%=ContactString%> </TD>
          <TD width="40%" nowrap valign=top height=100> 
            
          </TD>
          <TD ALIGN=RIGHT VALIGN=TOP width="20%"><IMG SRC="/myImages/Broadland_Purchase_Image.gif" width="134" height="106"></TD>
        </TR>
      </TABLE>
<BR>
                <div style="border: 1px solid #000000;float: left;margin-bottom: 10px;padding: 15px;width: 760px;">
<div style="float: left;width: 49%;margin-left: -14px;">
    <div>Order Name:&nbsp;&nbsp;&nbsp;<span><%=PONAME%></span></div>
    <div>Order No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=PONUMBER%></span></div>
    <div>Status:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=POSTATUSNAME%></span></div>
   
    <% IF BLOCK <> "" AND NOT ISNULL(BLOCK) THEN %> 
        <div>Block:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=BLOCK%></span></div>
    <% END IF %>  
    <% IF SCHEMENAME <> "" AND NOT ISNULL(SCHEMENAME) THEN %> 
        <div>Scheme:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=SCHEMENAME%></span></div>
    <% END IF %>    
    <% IF (NOT ISNULL(PROPERTYID) OR NOT ISNULL(BLOCKID)) THEN %>
            <div>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="vertical-align:top; display:inline-block;">
            <%=PAddressString%></span></div>
    <% END IF %>
</div>

<div style="float: right; width: 49%;">
    <div>Company:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=COMPANY%></span></div>
    <div>Ordered By:&nbsp;&nbsp;&nbsp;&nbsp;<span><%=ORDEREDBY%></span></div>
    <% if (ApprovedBy <> "") then %>
        <div>Approved By:&nbsp;&nbsp;<span><%=ApprovedBy%></span></div>
    <% else %>
        <div>Approved By:&nbsp;&nbsp;<span>N/A</span></div>
    <% end if %>
    <div>Order Date:&nbsp;&nbsp;&nbsp;&nbsp;<span><%=PODATE%></span></div>
    <div>Expected Date:<span><%=EXPPODATE%></span></div>
    <% if (LASTPAYMENT <> "") then %>
        <div>Last Payment Date : <%=FormatDateTime(LASTPAYMENT,2)%></div>
    <% end if %>
</div>

<div style="width: 100%;float: left;">
    <input type="button" id="PrintButton" name="PrintButton" style="float: right;" value=" PRINT " onclick="PrintThePP()"/>
</div>


</div>




<br/>
                <table style="border: 1px solid black" cellspacing="0" cellpadding="2" width="791">
                    <tr bgcolor="#133e71" align="right" style="color: white">
                        <td height="20" align="left" width="360" nowrap="nowrap">
                            <table cellspacing="0" cellpadding="0" width="356">
                                <tr style="color: white">
                                    <td>
                                        <b>Item Name:</b>
                                    </td>
                                    <td align="right" style="cursor: pointer; font-weight: bold" onclick="ToggleDescs(<%=noOfItems%>)">
                                        <div id="SHOWDESC">SHOW DESCRIPTIONS</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="40">
                            <b>Code:</b>
                        </td>
                        <td width="151">
                            <b>Approved By:</b>
                        </td>
                        <td width="80">
                            <b>Net (�):</b>
                        </td>
                        <td width="75">
                            <b>VAT (�):</b>
                        </td>
                        <td width="80">
                            <b>Gross (�):</b>
                        </td>
                        <td width="5">
                        </td>
                    </tr>
                    <%=PIString%>
                </table>
                <table style="border: 1px solid black; border-top: none" cellspacing="0" cellpadding="2"
                    width="791">
                    <tr bgcolor="steelblue" align="right">
                        <td height="20" width="400" nowrap="nowrap" style="border: none; color: white">
                            <b>TOTAL : &nbsp;</b>
                        </td>
                        <td width="151" bgcolor="white" nowrap>
                        </td>
                        <td width="80" bgcolor="white" nowrap="nowrap">
                            <b>
                                <%=FormatNumber(TotalNetCost,2)%></b>
                        </td>
                        <td width="75" bgcolor="white" nowrap="nowrap">
                            <b>
                                <%=FormatNumber(TotalVAT,2)%></b>
                        </td>
                        <td width="80" bgcolor="white" nowrap="nowrap">
                            <b>
                                <%=FormatNumber(TotalGrossCost,2)%></b>
                        </td>
                        <td width="5" bgcolor="white" nowrap="nowrap">
                        </td>
                    </tr>
                </table>
                <%=InvoiceString%>
                <table cellspacing="0" cellpadding="2" width="791">
                    <tr>
                        <td>
                            <b>PURCHASE NOTES :</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #133e71; height: 60px" valign="top" width="791px">
                            <%=PONOTES%>&nbsp;
                        </td>
                    </tr>
                </table>
                <%=ASBESTOS%>
                <div align="center" style="width: 791px">
                    <b>Registered Office</b> Broadland Housing Association Limited, NCFC Jarrold Stand,
                    Carrow Road, Norwich, NR1 1HU<br />
                    Tel: 01603 750200, Fax: 01603 750222</div>
            
</body>
</html>



