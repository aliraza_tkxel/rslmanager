<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim absence_history_id		' the historical id of the absence record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this absence used to render page, either sickness or otherwise QUERYSTING
	Dim is_sickness				' determines whether this is a sickness record - uses nature_id
	Dim a_status				' status of absence reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of absence reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim startdate
	Dim returndate
	Dim certno
	Dim notes
	Dim drname
	Dim reason
	Dim duration
	Dim employeeid
	Dim isEmployeeOnly
	Dim holType
    Dim duration_hrs
    Dim new_history_id
    Dim Result
    Dim First 
	Dim non_working

    First = 1
    Result = 0
	non_working = 0
	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")

	absence_history_id 	= Request("absencehistoryid")
	nature_id 			= Request("natureid")
	employeeid			= Request("employeeid")

	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT STARTDATE,ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(" & employeeid & ")"
	Call OpenRs(rsDays, SQL)
	    emp_year_sdate = rsDays("STARTDATE")
	    emp_year_edate = rsDays("ENDDATE")
	Call CloseRs(rsDays)

	If nature_id = 1 Then is_sickness = True Else is_sickness = False End If

	' only allow line manager to approve holidays and sick leave
	If isLineManager(employeeid, Session("userid")) Then
		isEmployeeOnly = " "
	Else
		isEmployeeOnly = " and EMPLOYEEONLY = 1 "
	End If

	' begin processing
	Call OpenDB()

	If path  = "" then path = 0 end if ' initial entry to page

	If path = 0 Then
		If is_sickness Then
			Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (1, 2, 20)", "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", "onchange='workDays();checkForm();validateDate()'")
		Else
			Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (3, 4, 5, 20) " & isEmployeeOnly, "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", "onchange='workDays();checkForm();validateDate()'")
		End If
		Call get_record()
	Elseif path = 1 Then
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT 	ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.RETURNDATE, 103) ,'') AS RETURNDATE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS," &_
					"			ISNULL(A.CERTNO, 'N/A') AS CERTNO," &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES," &_
					"			ISNULL(A.DRNAME, 'N/A') AS DRNAME," &_
					"			ISNULL(A.REASON, 'N/A') AS REASON, " &_
					"			ISNULL(A.HOLTYPE, 'N/A') AS HOLTYPE, " &_
					"			DURATION , DURATION_HRS " &_
					"FROM 		E_ABSENCE A " &_
					"			LEFT JOIN E_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID " &_
					"WHERE 		ABSENCEHISTORYID = " & absence_history_id

		Call OpenRs(rsSet, strSQL)

		a_status 	= rsSet("STATUS")
		startdate	= rsSet("STARTDATE")
		returndate 	= rsSet("RETURNDATE")
		certno		= rsSet("CERTNO")
		notes 		= rsSet("NOTES")
		drname 		= rsSet("DRNAME")
		reason 		= rsSet("REASON")
		duration 	= rsSet("DURATION")
		holType		= rsSet("HOLTYPE")
		duration_hrs 	= rsSet("DURATION_HRS")

		Call CloseRs(rsSet)
		
		SQL = "SELECT dbo.E_CAL_WORKDAYS_IN_ABSENCE_BY_PATTERN(" & employeeid & ",'" & startdate & "','" & returndate & "') AS NON_WORKING_DAYS"
		Call OpenRs(rsSet, SQL)
			non_working = rsSet("NON_WORKING_DAYS")
		Call CloseRs(rsSet)

	End Function

	Function new_record()

		strSQL = "SELECT J.JOURNALID,J.EMPLOYEEID FROM E_ABSENCE A INNER JOIN E_JOURNAL J ON J.JOURNALID=A.JOURNALID WHERE ABSENCEHISTORYID = " & absence_history_id
		Call OpenRs(rsSet, strSQL)

		journalid = rsSet("JOURNALID")
		employee_id = rsSet("EMPLOYEEID")

		Call CloseRS(rsSet)

		a_status 	= Request.Form("sel_ACTION")
		actionid 	= Request.Form("sel_ACTION")
		startdate 	= DateValue(Request.Form("txt_STARTDATE"))
		returndate 	= DateValue(Request.Form("txt_RETURNDATE"))
		duration 	= Request.form("txt_DURATION")
		certno 		= Replace(Request.form("txt_CERTNO"),"'", " ")
		notes 		= Replace(Request.form("txt_NOTES"),"'", " ")
		drname 		= Replace(Request.form("txt_DRNAME"),"'", " ")
		reason 		= Replace(Request.form("txt_REASON"),"'", " ")
		holtype 	= request.form("holtype")
		Leave_Hrs   = 0
        strSQL= "SELECT dbo.E_CHECK_EXISTINGLEAVE_UPDATE("& employee_id &",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_RETURNDATE"),1) & "'," & Request.Form("txt_DURATION") & ", '" & holtype  & "', " & journalid & ")  AS RESULT"
		'Response.Write(strSQL)
        Call OpenRs(rsExistingLeave, strSQL)
	        RESULT = rsExistingLeave("RESULT")
	    Call CloseRs(rsExistingLeave)
        if (actionid = 20 or actionid = 4 )Then
            RESULT = 0
        End if 
        'Response.Write(RESULT)
        ' If no record found, then insert in tables
		If RESULT = 0 Then
		    ' RUN THE PROC TO CONVERT THE LEAVE DAYS INTO LEAVE HOURS
		    'AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
		    AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_RETURNDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
	        Call OpenRs (rsLeave_Hrs, AL_HRS)

		        If not rsLeave_Hrs.eof Then
			        Leave_Hrs = rsLeave_Hrs("LEAVE_IN_HRS")
			    Else
		            Leave_Hrs = 0
		        End If

		    Call CloseRS(rsLeave_Hrs)

		    strSQL = 	"SET NOCOUNT ON;" &_
                        "INSERT INTO E_ABSENCE " &_
					    "(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION,DURATION_HRS, REASON, NOTES, CERTNO, DRNAME, HOLTYPE) " &_
					    "VALUES (" & journalid & ", " & a_status & ", " & actionid & ", '" & Now & "', " & Session("userid") & ", '" & Request.Form("txt_STARTDATE") & "', '" & Request.Form("txt_RETURNDATE") & "', " & Duration & "," & Leave_Hrs & ", '" & reason & "', '" & notes & "', '" & certno & "', '" & drname & "', '" & Request.Form("HOLTYPE") & "')" &_
                        " SELECT SCOPE_IDENTITY() AS HISTORYID;"

            Set rsSet = Conn.Execute(strSQL)
		        new_history_id = rsSet.fields("HISTORYID").value
		        rsSet.close()
		    Set rsSet = Nothing

		    Call update_journal(journalid, a_status)

            ' if approved or declined then send email
            If (actionid = "4" Or actionid = "5") Then
                call sendEmail(journalid,new_history_id,employee_id)
            End If
        End if
	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = "UPDATE E_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function

    Function sendEmail(journalid,new_history_id,employee_id)

    sql =   " SELECT ITEMACTIONID,LASTACTIONUSER,STARTDATE,RETURNDATE,DURATION FROM E_ABSENCE WHERE ABSENCEHISTORYID = " & new_history_id
    Call OpenRs(rsEmpRec, sql)
    If NOT rsEmpRec.EOF Then
        action = rsEmpRec("ITEMACTIONID")
        userId = rsEmpRec("LASTACTIONUSER")
        sdate = rsEmpRec("STARTDATE")
        edate =  rsEmpRec("RETURNDATE")
        duration =  rsEmpRec("DURATION")
    End If
    Call CloseRs(rsEmpRec)

    SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT C WHERE EMPLOYEEID = " & employee_id
	Call OpenRs(rsEmail, SQL)

	If NOT rsEmail.EOF And NOT rsEmail.BOF Then
		' send email
		Dim strRecipient, emailSubject, strSender
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

        SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT WHERE EMPLOYEEID = " & userId
        Call OpenRs(rsSenderEmail, SQL)

        If NOT rsSenderEmail.EOF And NOT rsSenderEmail.BOF Then 
            strSender=(rsSenderEmail.Fields.Item("WORKEMAIL").Value)
        End if
        Call CloseRs(rsSenderEmail)

        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        If action ="5" then
            emailbody = "Your " & duration &  " day(s) leave for the period " & sdate & " to " & edate & " has been approved."
            emailSubject = "Leave Approved"
        elseif action="4" then
            emailbody = "Your " & duration &  " day(s) leave for the period " & sdate & " to " & edate & " has been declined."
            emailSubject = "Leave Declined"
        end if

        On Error Resume NEXT        
        Set iMsg.Configuration = iConf
        iMsg.To = strRecipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send
        If Err.number <> 0 Then
            If strRecipient = "" Or IsNull(strRecipient) Then
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed due to manager email address not provided, so please inform the concerned person.');</script>") 
            Else  
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed, so please inform the concerned person.');</script>")
            End If  
            Err.Clear()
            ON ERROR GOTO 0
        End If

	End If
	Call CloseRs(rsEmail)

    End Function
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=11" />
    <title>ERM --> Update Absence</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
<script type="text/javascript" language="JavaScript">

	var FormFields = new Array();
	    FormFields[0] = "txt_REASON|REASON|TEXT|Y"
	    FormFields[1] = "txt_STARTDATE|STARTDATE|DATE|Y"
	    FormFields[2] = "txt_RETURNDATE|RETURNDATE|DATE|Y"
	    FormFields[3] = "txt_DURATION|DURATION|TEXT|Y"
	    FormFields[4] = "txt_RECORDEDBY|RECORDEDBY|TEXT|Y"
	    FormFields[5] = "sel_ACTION|ACTION|SELECT|Y"
	    FormFields[6] = "txt_NOTES|Notes|TEXT|N"

	function save_form(){
	    if (!checkForm() || !validateDate()) return false;
                var dSDate = new Date(JSlong_date(document.getElementById("txt_STARTDATE").value));
                var dEDate = new Date(JSlong_date(document.getElementById("txt_RETURNDATE").value));
                var dSDate_hid = new Date(JSlong_date(document.getElementById("hid_STARTDATE").value));
                var dEDate_hid = new Date(JSlong_date(document.getElementById("hid_ENDDATE").value));
                //var hrule = document.getElementById("hid_hrule").value;

                if (dSDate > dEDate) {
                    alert('Invalid start/end date. End date should be greater than start date.');
                    return false;
                }
		
		getType();
		document.getElementById("hid_go").value = 1;
		document.RSLFORM.action = "pLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>";
		document.RSLFORM.target = ""
		document.RSLFORM.submit();
	}

	function return_data(){	
        
        if ("<%=RESULT%>" >= 1) {
            alert("You cannot book leave as you have already booked the date(s)");
            opener.location.reload();
            window.close();
            return false;
        }
        
		if (<%=path%> == 1)	{
			opener.location.reload();
			window.close();
            return false;
			}
		else 
			displayDetails();

        if ("<%=RESULT%>" < 1 && document.getElementById("hid_First").value < 1 ) {
            opener.location.reload();
            window.close();
            return false;
        }
        document.getElementById("hid_First").value = 0
	}
	
	// global variables
	var Eleavedur, Etype
	
	// initialises fields from server to be used by client
	function initFields() {
    	Eleavedur = <%=duration%>;
		Etype = new String('<%=HolType%>');
	}

	// fills page details according to holiday information
	function displayDetails() {
		var arrSplit;
		initFields();
		if (Eleavedur > 1){
		    document.getElementById("endText").style.visibility = "visible";
		    document.getElementById("radMulti").checked = true;
			}
		arrSplit = Etype.split("-");
		if (arrSplit.length > 1){
			if (arrSplit[0] == 'F')
			    document.getElementById("radStartFull").checked = true;
			else if (arrSplit[0] == 'M')
				document.getElementById("radStartAM").checked = true;
			else document.getElementById("radStartPM").checked = true;
			if (arrSplit[1] == 'F')
				document.getElementById("radEndFull").checked = true;
			else if (arrSplit[1] == 'M')
				document.getElementById("radEndAM").checked = true;
			else document.getElementById("radEndPM").checked = true;
			}
		else {
			if (Etype == 'F')
				document.getElementById("radStartFull").checked = true;
			else if (Etype == 'M')
				document.getElementById("radStartAM").checked = true;
			else document.getElementById("radStartPM").checked = true;
			}	
	}
	
	function check_date(){
	    if (!checkForm()) return false;
		calLeaveDuration();
	}
	function restBtnState()
	{
		 RSLFORM["btn_submit"].disabled = true;
	}
	function validateDate()
	{
    RSLFORM["btn_submit"].disabled = false;
	    var isValid = true;
	    if (document.getElementById("radMulti").checked == true) {

	        var dSDate = new Date(JSlong_date(document.getElementById("txt_STARTDATE").value));
	        var dEDate = new Date(JSlong_date(document.getElementById("txt_RETURNDATE").value));

	        if (dSDate > dEDate)
	        {
	            alert('Invalid start/end date. End date should be greater than start date.');
	            isValid= false;
	        }
	    }

	    return isValid;
	}
	
	function calLeaveDuration()
	{
	    if (!checkForm()) return false;
	    var emp = "<%= employee_id%>";
	    
	    if (emp == '') {
	        emp = "<%= employeeid%>";
	    }
        if (document.getElementById("radSingle").checked == true) {
		    document.getElementById("txt_RETURNDATE").value = document.getElementById("txt_STARTDATE").value
		}
		var sdate = document.getElementById("txt_STARTDATE").value;
		var edate = document.getElementById("txt_RETURNDATE").value;
		document.RSLFORM.action="iFrames/iEmpWorkDays.asp?employeeid=" + emp + "&sdate=" + sdate + "&edate=" + edate
	    document.RSLFORM.target="serverFrame"
	    document.RSLFORM.submit();
	}

</script>
</head>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="pLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="67">
                            <img src="Images/tab_leave.gif" width="67" height="20" alt="Leave" /></td>
                        <td height="19" width="8061"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71" width="8061">
                            <img src="images/spacer.gif" height="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71'>
                <table cellspacing="1" cellpadding="2" width="90%" align="center" border="0">
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_REASON" id="txt_REASON" value="<%=reason%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                border="0" alt="REASON" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="21">
                            Action
                        </td>
                        <td colspan="3" height="21">
                            <%=lst_action%>
                            <img src="/js/FVS.gif" name="img_ACTION" id="img_ACTION" width="15px" height="15px"
                                border="0" alt="ACTION" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Requested
                        </td>
                        <td colspan="3">
                            <input type="radio" id="radSingle" name="radType" value="single" checked="checked"
                                onclick='{document.getElementById("endText").style.visibility="hidden";workDays()}' />
                            Single
                            <input type="radio" id="radMulti" name="radType" value="multiple" onclick='{document.getElementById("endText").style.visibility="visible";workDays()}' />
                            Multiple
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date
                        </td>
                        <td colspan="3">
                            <input type="text" value="<%=startdate%>" name="txt_STARTDATE" id="txt_STARTDATE"
                                class="textbox100" maxlength="10" onblur="check_date()" onkeyup="workDays()" />
                            <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                border="0" alt="" />
                            &nbsp;full<input type="radio" id="radStartFull" name="radStart" value="days" onclick="workDays()"
                                checked="checked" />
                            am<input type="radio" id="radStartAM" name="radStart" value="morning" onclick="workDays()" />
                            pm<input type="radio" id="radStartPM" name="radStart" value="afternoon" onclick="workDays()" />
                        </td>
                    </tr>
                    <tr id="endText" style="visibility: hidden">
                        <td>
                            End Date
                        </td>
                        <td colspan="3">
                            <input type="text" value="<%=returndate%>" name="txt_RETURNDATE" id="txt_RETURNDATE"
                                class="textbox100" maxlength="10" onblur="check_date()" onkeyup ="workDays()" />
                            <img src="/js/FVS.gif" name="img_RETURNDATE" id="img_RETURNDATE" width="15px" height="15px"
                                border="0" alt="RETURNDATE" />
                            &nbsp;full<input type="radio" id="radEndFull" name="radEnd" value="full" onclick="workDays()"
                                checked="checked" />
                            am<input type="radio" id="radEndAM" name="radEnd" value="morning" onclick="workDays()" />
                            pm<input type="radio" id="radEndPM" name="radEnd" value="afternoon" onclick="workDays()" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Duration
                        </td>
                        <td colspan="3">
                            <input name="txt_DURATION" id="txt_DURATION" type="text" class="TEXTBOX100" readonly="readonly"
                                value="<%=duration%>"  />
                            <img src="/js/FVS.gif" name="img_DURATION" id="img_DURATION" width="15px" height="15px"
                                border="0" alt="DURATION" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Recorded By
                        </td>
                        <td colspan="2">
                            <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                                readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="RECORDED BY" />
                        </td>
                    </tr>
                    <% If is_sickness Then %>
                    <tr>
                        <td nowrap="nowrap">
                            Cert No:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_CERTNO" id="txt_CERTNO" value="<%=certno%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CERTNO" id="img_CERTNO" width="15px" height="15px"
                                border="0" alt="CERTNO" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Drs Name:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DRNAME" id="txt_DRNAME" value="<%=drname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DRNAME" id="DRNAME" width="15px" height="15px" border="0"
                                alt="DR NAME" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <% End If %>
                    <tr>
                        <td>
                            Notes
                        </td>
                        <td colspan="2">
                            <textarea maxlength="1999" style="overflow: hidden" rows="7" cols="20" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES"><%=notes%></textarea>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="NOTES" />
                            <input type="button" name="btn_submit" id="btn_submit" onclick="workDays();save_form();restBtnState()" value='Save'
                                class="RSLButton" style="cursor: pointer" />
                            <input type="hidden" name="dates" id="dates" value="<% =BANK_HOLIDAY_STRING%>" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="holtype" id="holtype" value="<%=HolType%>" />
                            <input type="hidden" name="hid_non_work_days" id="hid_non_work_days" value="<%=non_working%>" />
                            <input type="hidden" name="hid_STARTDATE" id="hid_STARTDATE" value="<%=emp_year_sdate%>" />
                            <input type="hidden" name="hid_ENDDATE" id="hid_ENDDATE" value="<%=emp_year_edate%>" />
							<input type="hidden" name="hid_First" id="hid_First" value="<%=First%>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" width="200" height="200" style="display: none"></iframe>
</body>
</html>
