<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include file="../Connections/ContactManager.asp" -->
<%
Dim UserData__UserID
UserData__UserID = "0"
if (Session("svUserID") <> "") then UserData__UserID = Session("svUserID")
%>
<%
set UserData = Server.CreateObject("ADODB.Recordset")
UserData.ActiveConnection = MM_ContactManager_STRING
UserData.Source = "SELECT *  FROM dbo.UserData left join myrole on userdata.roleid = myrole.roleid  left join usertitles on usertitle = usertitles.id  WHERE WUserID = " + Replace(UserData__UserID, "'", "''") + ""
UserData.CursorType = 0
UserData.CursorLocation = 2
UserData.LockType = 3
UserData.Open()
UserData_numRows = 0
%>
<%
Dim HolidayInfo__TheUser
HolidayInfo__TheUser = "0"
if (Session("svUserID") <> "") then HolidayInfo__TheUser = Session("svUserID")
%>
<%
set HolidayInfo = Server.CreateObject("ADODB.Recordset")
HolidayInfo.ActiveConnection = MM_ContactManager_STRING
HolidayInfo.Source = "select   (select isNull(sum(LeaveDur),0) as daystaken from leave where Userid = " + Replace(HolidayInfo__TheUser, "'", "''") + " and status = 'A') as daystaken,  (select isNull(sum(LeaveDur),0) as daysbooked from leave where Userid = " + Replace(HolidayInfo__TheUser, "'", "''") + " and status = 'P') as daysbooked,  (select isNull(sum(balance),0) as daysremaining from holidayaccounts where Userid = " + Replace(HolidayInfo__TheUser, "'", "''") + ") as daysremaining,  (select top 1 isNull(leavestart, 'N/A') as Nextleave from leave where Userid = " + Replace(HolidayInfo__TheUser, "'", "''") + " and status = 'A' order by leavestart desc) as nextleave"
HolidayInfo.CursorType = 0
HolidayInfo.CursorLocation = 2
HolidayInfo.LockType = 3
HolidayInfo.Open()
HolidayInfo_numRows = 0
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL My Job</TITLE>
<link rel="stylesheet" href="/css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<!--#include file="menu/iagMYJOB.INC"-->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include file="menu/BodyTop.html" -->
      <table>
        <tr> 
          <td valign=top> <img src=../myImages/my%2Bdetails.gif width="252" height="20"> 
            <table bgcolor=#e1e1e1 width="252" class="rslManager" style="border:1px solid #133E71" cellspacing=0 cellpadding=1>
              <tr> 
                <td rowspan=50>&nbsp;</td>
                <td>Title:&nbsp;&nbsp;&nbsp;<i><%=(UserData.Fields.Item("Title").Value)%></i></td>
                <td>First Name:&nbsp;&nbsp;<%=(UserData.Fields.Item("UserFName").Value)%></td>
              </tr>
              <tr> 
                <td>Last Name:</td>
                <td><%=(UserData.Fields.Item("UserSName").Value)%></td>
              </tr>
              <tr> 
                <td>Date of Birth:</td>
                <td><%=(UserData.Fields.Item("DOB").Value)%></td>
              </tr>
              <tr> 
                <td>Gender:</td>
                <td><%=(UserData.Fields.Item("Gender").Value)%></td>
              </tr>
              <tr> 
                <td>Marital Status:</td>
                <td><%=(UserData.Fields.Item("MaritalStatus").Value)%></td>
              </tr>
              <tr> 
                <td colspan=2 height=5> 
                  <hr style='border:1px solid #133e71' size=1>
                </td>
              </tr>
              <tr> 
                <td>Direct Line:</td>
                <td><%=(UserData.Fields.Item("UserDirectTel").Value)%></td>
              </tr>
              <tr> 
                <td>Email (work):</td>
                <td><%=(UserData.Fields.Item("WorkEmail").Value)%></td>
              </tr>
              <tr> 
                <td>Job Title:</td>
                <td><%=(UserData.Fields.Item("UserJobTitle").Value)%></td>
              </tr>
              <tr> 
                <td>Job Role:</td>
                <td><%=(UserData.Fields.Item("RoleTitle").Value)%></td>
              </tr>
              <tr> 
                <td>Team:</td>
                <td><%=(UserData.Fields.Item("UserTeam").Value)%></td>
              </tr>
              <tr> 
                <td>Office:</td>
                <td><%=(UserData.Fields.Item("UserOffice").Value)%></td>
              </tr>
              <tr> 
                <td>Start Date:</td>
                <td><%=FormatDateTime(UserData.Fields.Item("startdate").Value,2)%></td>
              </tr>
              <tr> 
                <td>Leave Entitlement:</td>
                <td><%=(UserData.Fields.Item("LeaveDaye").Value)%></td>
              </tr>
              <tr> 
                <td colspan=2 height=5> 
                  <hr style='border:1px solid #133e71' size=1>
                </td>
              </tr>
              <tr> 
                <td>Employee Ref No:</td>
                <td>US00<%=(UserData.Fields.Item("WUserID").Value)%></td>
                <td></td>
              </tr>
              <tr> 
                <td>Ni No:</td>
                <td><%=(UserData.Fields.Item("NINumber").Value)%></td>
              </tr>
              <tr> 
                <td>Tax Code:</td>
                <td></td>
              </tr>
              <tr> 
                <td>Previous Employer:</td>
                <td><%=(UserData.Fields.Item("PreviousEmployer").Value)%></td>
              </tr>
              <tr> 
                <td>Qualifications:</td>
                <td><%=(UserData.Fields.Item("Qualifications").Value)%></td>
              </tr>
              <tr> 
                <td colspan=2></td>
              </tr>
              <tr> 
                <td>Skills:</td>
                <td><%=(UserData.Fields.Item("Skills").Value)%></td>
              </tr>
              <tr> 
                <td colspan=2></td>
              </tr>
              <tr> 
                <td colspan=2 height=5> 
                  <hr style='border:1px solid #133e71' size=1>
                </td>
              </tr>
              <tr> 
                <td valign=top>Address (home):</td>
                <td valign=top> 
                  <%
				addfields = array("UserAdd1", "UserAdd2", "UserAdd3", "UserAdd4", "UserAddPC")
				addstring = ""
				for i=0 to Ubound(addfields)
					addpart = UserData(addfields(i))
					if (NOT(addpart = "" OR isNull(addpart))) then
						if (addstring = "") then
							addstring = addpart
						else
							addstring = addstring & "<BR>" & addpart
						end if
					end if
				next
				Response.Write addstring
				%>
                </td>
              </tr>
              <tr> 
                <td>Tel:</td>
                <td><%=(UserData.Fields.Item("UserTel").Value)%></td>
              </tr>
              <tr> 
                <td>Mobile:</td>
                <td><%=(UserData.Fields.Item("UserMob").Value)%></td>
              </tr>
              <tr> 
                <td>Email:</td>
                <td><%=(UserData.Fields.Item("UserEmail").Value)%></td>
              </tr>
              <tr> 
                <td colspan=2><i><b>Emergency Contact:</b></i></td>
              </tr>
              <tr> 
                <td>Name:</td>
                <td><%=(UserData.Fields.Item("EmergencyName").Value)%></td>
              </tr>
              <tr> 
                <td>Tel:</td>
                <td><%=(UserData.Fields.Item("EmergencyTel").Value)%></td>
              </tr>
            </table>
          </td>
          <td valign=top width="239"> <img src=../myImages/team%2Bnews.gif width="239" height="20"> 
            <!-- #include file=TeamNews.asp --><img src=../myImages/spacer.gif width=239 height=9><img src=../myImages/trainingplan.gif width="239" height="21"><img src=../myImages/spacer.gif width=239 height=3><table width="239" style='border:1px solid #133e71' class="rslManager">
              <tr>
                <td colspan=2><font style='font-size:12px'><b>My Courses...</b></font> 
                  all the information you need for course history, availability 
                  and booking <a href=#><font color=blue>here></font></a></td>
              </tr>
              <tr height=2px style='line-height:2px'> 
                <td colspan=2 style='line-height:2px' height=2px>
                  <hr size=1 style='border:1px solid #e1e1e1'>
                </td>
              </tr>
              <tr>
                <td colspan=2><font style='font-size:12px'><b>Needs Analysis...</b></font> 
                  <a href=#><font color=blue>click here></font></a></td>
              </tr>
            </table><img src=../myImages/spacer.gif width=239 height=9><img src=../myImages/policies%2Bhandbooks.gif width="239" height="20"><img src=../myImages/spacer.gif width=239 height=3><table width="239" style='border:1px solid #133e71' class="rslManager">
              <tr>
                <td colspan=2><font style='font-size:12px'><b>Staff Handbook...</b></font> 
                  and other policy documents <a href="/iagDocumentManager/iagDocumentSearchCorrType.asp"><font color=blue>click 
                  here></font></a></td>
              </tr>
            </table><img src=../myImages/spacer.gif width=239 height=9><a href="UserPage.asp" border=0><img src=../myImages/my%2Bprofile.gif width="239" height="20" border=0></a><img src=../myImages/spacer.gif width=239 height=3><table width="239" style='border:1px solid #133e71' class="rslManager">
              <tr> 
                <td colspan=2><%=(UserData.Fields.Item("MyProfile").Value)%></td>
              </tr>
            </table>
          </td>
          <td valign=top width="239"> <img src=../myImages/annual%2Bleave.gif width="239" height="20"> 
            <%
		  
		  %>
            <table width="239" style='border:1px solid #133e71' class="rslManager">
              <tr> 
                <td colspan=2 height=5></td>
              </tr>
              <tr> 
                <td>Days taken:</td>
                <td><%=(HolidayInfo.Fields.Item("daystaken").Value)%></td>
              </tr>
              <tr> 
                <td>Days booked:</td>
                <td><%=(HolidayInfo.Fields.Item("daysbooked").Value)%></td>
              </tr>
              <tr> 
                <td>Days remaining:</td>
                <td><%=(HolidayInfo.Fields.Item("daysremaining").Value)%></td>
              </tr>
              <tr> 
                <td>Next Leave:</td>
                <td><%=(HolidayInfo.Fields.Item("nextleave").Value)%></td>
              </tr>
              <tr> 
                <td colspan=2> 
                  <hr size=1 style='border:1px solid #e1e1e1'>
                </td>
              </tr>
              <tr> 
                <td colspan=2><font style='font-size:12px'><b>Policies:</b></font> 
                  Get all the information you need about your holiday entitlement 
                  <a href=#><font color=blue>here></font></a></td>
              </tr>
              <tr> 
                <td colspan=2> 
                  <hr size=1 style='border:1px solid #e1e1e1'>
                </td>
              </tr>
              <tr> 
                <td colspan=2><font style='font-size:12px'><b>Request leave:</b></font> 
                  <a href="UserPage.asp?ThePage=1"><font color=blue>click here to book leave></font></a></td>
              </tr>
              <tr> 
                <td colspan=2> 
                  <hr size=1 style='border:1px solid #e1e1e1'>
                </td>
              </tr>
              <tr> 
                <td colspan=2><font style='font-size:12px'><b>Request expenses:</b></font> 
                  <a href="TMExpenses.asp"><font color=blue>click here></font></a></td>
              </tr>
            </table>
			<img src=../myImages/spacer.gif width=239 height=9><img src=../myImages/team%2Bcontacts.gif width="239" height="20"><img src=../myImages/spacer.gif width=239 height=3><iframe src="Contacts2.asp" frameborder=0 height=250px width=245 border=0 scroll=no> 
            </iframe
          ></td>
        </tr>
      </table>
      <!--#include file="../include/BodyBottom.html" -->
</BODY>
</HTML>
<%
UserData.Close()
%>
<%
HolidayInfo.Close()
%>
