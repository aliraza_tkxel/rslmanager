<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/myjob_right_box.asp" -->
<%

	CONST TABLE_ROW_SIZE = 7
	
	Dim cnt, str_skills, rsSet, str_holiday, str_policy, employee_id
	Dim disable_my_updates
	
	employee_id = Request("employeeid")
	
	// only allow th4e user hm/herself to update their personal details
	If Cint(employee_id) = Cint(Session("userid")) Then
		disable_my_updates = ""
	Else
		disable_my_updates = " disabled "
	End If

	OpenDB()
	strSQL = 	"SELECT		ISNULL(RESULT,'N/A') AS RESULT, " &_
					"			ISNULL(Q.SUBJECT, 'N/A') AS SUBJECT,  " &_
					"			ISNULL(T.QUALIFICATIONTYPEDESC, 'N/A') AS EXISTPLANNED, " &_
					"			ISNULL(CONVERT(NVARCHAR, Q.QUALIFICATIONDATE, 103), '') AS QUALIFICATIONDATE " &_
					"FROM		E_QUALIFICATIONSANDSKILLS Q LEFT JOIN E_QUALIFICATIONTYPE T ON T.QUALIFICATIONTYPEID = Q.EXISTPLANNED " &_
					"WHERE 		EMPLOYEEID = " & employee_id
	Call OpenRs (rsSet, strSQL) 
	
	//response.write strSQL
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">

<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0>
	<TR>
		<TD ROWSPAN=2 WIDTH=70% HEIGHT=100%>
			
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER: SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
				<TR valign=top class='title'> 
					      <TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71'><b>Type</b></TD>
                          <TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71'><b>Subject</b></TD>
                          <TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71'><b>Result</b></TD>
                          <TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71'><B>Date </B></TD>
	  			</TR>
			<% 
				cnt = 1
				  	While Not rsSet.EOF And cnt < 6
					cnt = cnt + 1 
				%>
				<TR>
					<TD> <% =rsSet("EXISTPLANNED") %> </TD>
					<TD> <% =rsSet("SUBJECT") %> </TD>
					<TD> <% =rsSet("RESULT")  %> </TD>
					<TD> <% =rsSet("QUALIFICATIONDATE") %> </TD>
				</TR>
				<% rsSet.movenext()
			Wend
			CloseRs(rsSet) 
			for j = 1 TO (TABLE_ROW_SIZE - cnt) %>
				<TR><TD COLSPAN=4>&nbsp;</TD></TR >
			<% Next 
				CloseDB()%>
			<TR>
				<TD ALIGN=RIGHT COLSPAN=4>
					<INPUT TYPE='BUTTON' NAME='BTN_UPDATEPD' <%=disable_my_updates%> VALUE='Update' CLASS='RSLBUTTONSMALL' ONCLICK="parent.update_record('/myjob/popups/pSkills.asp?employeeid=<%=employee_id%>',710,450)">
				</TD>
			</TR>
			</TABLE>
			
		</TD>
		<TD WIDTH=30% HEIGHT=50>
			<%=str_holiday%>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=30% HEIGHT=50% >
			<%=str_policy%>
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	