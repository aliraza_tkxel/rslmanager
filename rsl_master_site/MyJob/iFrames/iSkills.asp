<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/myjob_right_box.asp" -->
<%
	CONST TABLE_ROW_SIZE = 7

	Dim cnt, str_skills, rsSet, str_holiday, str_policy, employee_id
	Dim disable_my_updates

	employee_id = Request("employeeid")

	' only allow th4e user hm/herself to update their personal details
	If Cint(employee_id) = Cint(Session("userid")) Then
		disable_my_updates = ""
	Else
		disable_my_updates = " disabled "
	End If

	Call OpenDB()
	strSQL = 	"SELECT		ISNULL(RESULT,'N/A') AS RESULT, " &_
					"			ISNULL(Q.SUBJECT, 'N/A') AS SUBJECT,  " &_
					"			ISNULL(T.QUALIFICATIONTYPEDESC, 'N/A') AS EXISTPLANNED, " &_
					"			ISNULL(CONVERT(NVARCHAR, Q.QUALIFICATIONDATE, 103), '') AS QUALIFICATIONDATE,Q.QUALIFICATIONDATE AS QDATE " &_
					"FROM		E_QUALIFICATIONSANDSKILLS Q LEFT JOIN E_QUALIFICATIONTYPE T ON T.QUALIFICATIONTYPEID = Q.EXISTPLANNED " &_
					"WHERE 		EMPLOYEEID = " & employee_id & " " &_
					"ORDER BY QDATE DESC"
	Call OpenRs (rsSet, strSQL) 
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <table width="750" style="height: 180px; border-bottom: SOLID 1PX #133E71; border-left: SOLID 1PX #133E71;
        border-right: SOLID 1PX #133E71" cellpadding="1" cellspacing="2" border="0">
        <tr>
            <td rowspan="2" width="70%" height="100%" valign="top">
                <table width="100%" style="height: 100%; border: SOLID 1PX #133E71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr valign="top" class='title'>
                        <td>
                            <table height="100%" width="100%" cellpadding="3" cellspacing="0" border="0">
                                <tr valign="top" class='title'>
                                    <td width="80px">
                                        <b>Type</b>
                                    </td>
                                    <td width="100px">
                                        <b>Subject</b>
                                    </td>
                                    <td width="100px">
                                        <b>Result</b>
                                    </td>
                                    <td width="100px">
                                        <b>Date</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="15px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="border: SOLID 1PX #133E71;">
                            <div style="height: 115px; overflow: auto;">
                                <table width="100%" style="height: 100%; border-collapse: COLLAPSE" cellpadding="3"
                                    cellspacing="0" border="1">
                                    <% 
								cnt = 1
									While Not rsSet.EOF 'And cnt < 6
									cnt = cnt + 1 
                                    %>
                                    <tr>
                                        <td width="80px">
                                            <%=rsSet("EXISTPLANNED")%>
                                        </td>
                                        <td width="100px">
                                            <%=rsSet("SUBJECT")%>
                                        </td>
                                        <td width="100px">
                                            <%=rsSet("RESULT")%>
                                        </td>
                                        <td width="100px">
                                            <%=rsSet("QUALIFICATIONDATE")%>
                                        </td>
                                    </tr>
                                    <% rsSet.movenext()
							Wend
							Call CloseRs(rsSet) 
							for j = 1 TO (TABLE_ROW_SIZE - cnt) %>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <% Next 
							Call CloseDB()
                                    %>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <input type="button" name="BTN_UPDATEPD" <%=disable_my_updates%> value="Update" class="RSLBUTTONSMALL"
                                style="cursor: pointer" title="Update" onclick="parent.update_record('/myjob/popups/pSkills.asp?employeeid=<%=employee_id%>',710,450)" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="50">
                <%=str_holiday%>
            </td>
        </tr>
        <tr>
            <td width="30%" height="50%">
                <%=str_policy%>
            </td>
        </tr>
    </table>
</body>
</html>