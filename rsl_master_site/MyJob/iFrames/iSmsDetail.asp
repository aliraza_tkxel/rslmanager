<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, sms_title, last_status, ButtonDisabled, ButtonText , Risk
	Dim latest_general_history_id
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()

		cnt = 0
				
		
 strSQL = 	    " SELECT	ISNULL(G.MOBILE,'') MOBILE,	" &_		 
	            " ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_ 
	            " ISNULL(G.[MESSAGE], 'N/A') AS MSG,  " &_
	            " G.SMSHISTORYID, J.TITLE, E.FIRSTNAME + ' ' + E.LASTNAME AS LASTACTIONUSERNAME,  " &_
	            " G.ITEMSTATUSID " &_
	            " FROM		E_SMS G  " &_
                " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.LASTACTIONUSER " &_
				" LEFT JOIN E_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
                " WHERE		G.JOURNALID = " & journal_id & " " &_  
                " ORDER 		BY SMSHISTORYID desc "
 		
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
		
					
			cnt = cnt + 1
			general_history_id = rsSet("SMSHISTORYID")
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				sms_title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("SMSHISTORYID")
				latest_general_history_id = general_history_id
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If
		
			
				message = rsSet("MSG")
				if (message = "" OR isNULL(message)) then
					ResponseNotes = "[Empty Notes]"
				elseif (message = PreviousNotes) then
					ResponseMessage = "[Same As Above]"
				else
					ResponseMessage = message
				end if
				PreviousMessage = message
				str_journal_table = str_journal_table & 	"<TD width='150'>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD width='120'>" & rsSet("LASTACTIONUSERNAME") & "</TD>" &_
															"<TD width='120'>" & rsSet("MOBILE") & "</TD>" &_
                                                            "<TD width='338'>" & ResponseMessage & "</TD>" &_
															"<TD width='12'>&nbsp;</TD>" 
															
															
															
															
				
				str_journal_table = str_journal_table & "<TD> </TD>"
				str_journal_table = str_journal_table & 	"</TD><TR>"
				
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	
		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=4>
        <table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=sms_title%></TD>
	      <td align=right width=100>
	        </td></tr>
        </table>
	</TD></TR>
	<TR valign=top>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=20PX><font color="blue"><b>Date:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100px><font color="blue"><b>Created By:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120PX><font color="blue"><b>Mobile:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=200PX><font color="blue"><b>Message:</b></font></TD>
    
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	