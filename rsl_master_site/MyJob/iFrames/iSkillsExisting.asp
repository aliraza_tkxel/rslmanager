<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST TABLE_ROW_SIZE = 7

	Dim cnt, str_skills, rsSet, str_holiday, str_policy

	strSQL = 	"SELECT		ISNULL(RESULT,'N/A') AS RESULT, " &_
					"			ISNULL(SUBJECT, 'N/A') AS SUBJECT,  " &_
					"			ISNULL(PROFESSIONALQUALIFICATION, 'N/A') AS PROFESSIONALQUALIFICATION, " &_
					"			ISNULL(CONVERT(NVARCHAR, QUALIFICATIONDATE, 103), '') AS QUALIFICATIONDATE " &_
					"FROM		E_QUALIFICATIONSANDSKILLS  " &_
					"WHERE 		EMPLOYEEID = " & Session("userid") & " AND EXISTPLANNED = 0"
    Call OpenDB()
	Call OpenRs (rsSet, strSQL)

	str_holiday = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/title_holidays.gif""></td><td align=""right""><img src=""../Images/job+description.gif"" onclick=""parent.open_desc()"" style=""cursor:pointer""></td></tr><tr><td colspan=""2""> 00 days available</td></tr><tr><td colspan=""2""> 00 days boooked  <b>Book Now!</b></td></tr></table>"

	str_policy = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/title_policies.gif""></td></tr><tr><td> Policy Handbook</td></tr><tr><td> Procedure Handbook</td></tr></table>"
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <table width="750" style="height:180px; border-right: SOLID 1PX #133E71" cellpadding="1"
        cellspacing="2" border="0">
        <tr>
            <td rowspan="2" width="70%" height="100%">
                <table width="100%" style="height:100%; border: SOLID 1PX #133E71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr valign="top" class='title'>
                        <td style='border-bottom: SOLID 1PX #133E71'>
                            <b>Qualification</b>
                        </td>
                        <td style='border-bottom: SOLID 1PX #133E71'>
                            <b>Subject</b>
                        </td>
                        <td style='border-bottom: SOLID 1PX #133E71'>
                            <b>Result</b>
                        </td>
                        <td style='border-bottom: SOLID 1PX #133E71'>
                            <b>Date </b>
                        </td>
                    </tr>
                    <% 
				cnt = 1
				  	While Not rsSet.EOF And cnt < 6
					cnt = cnt + 1 
                    %>
                    <tr>
                        <td>
                            <% =rsSet("PROFESSIONALQUALIFICATION") %>
                        </td>
                        <td>
                            <% =rsSet("SUBJECT") %>
                        </td>
                        <td>
                            <% =rsSet("RESULT")  %>
                        </td>
                        <td>
                            <% =rsSet("QUALIFICATIONDATE") %>
                        </td>
                    </tr>
                    <% rsSet.movenext()
			Wend
			Call CloseRs(rsSet) 
			for j = 1 TO (TABLE_ROW_SIZE - cnt) %>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <% Next 
				Call CloseDB()%>
                    <tr>
                        <td align="right" colspan="4">
                            <input type="button" name="BTN_UPDATEPD" value="<<<" class="RSLBUTTONSMALL" onclick="{location.href = '/myjob/iFrames/iSkills.asp'}" />
                            <input type="button" name="BTN_UPDATEPD" value="Update" class="RSLBUTTONSMALL" onclick="parent.update_record('/myjob/popups/pSkills.asp',710,450)" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="50">
                <%=str_holiday%>
            </td>
        </tr>
        <tr>
            <td width="30%" height="50%">
                <%=str_policy%>
            </td>
        </tr>
    </table>
</body>
</html>
