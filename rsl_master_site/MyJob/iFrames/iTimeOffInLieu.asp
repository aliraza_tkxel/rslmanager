<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim employee_id, item_id, itemnature_id, status_id, str_title, path, reason, holtype, notes , emp_year_sdate ,emp_year_edate, holiday_rule , time_off_in_lieu_owed , part_full , email_exist
	Dim Result

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")
	employee_id = Request("employeeid")

    a = "<string val=""'http://'""><ad>"

	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT STARTDATE,ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(" & employee_id & ")"
	Call OpenRs(rsDays, SQL)
	    emp_year_sdate = rsDays("STARTDATE")
	    emp_year_edate = rsDays("ENDDATE")
	Call CloseRs(rsDays)

	If path = "" Then path = 0 End If
	If path Then
		Call OpenDB()
		Call new_record()
		Call CloseDB()
	Else
		' these fields are used in the form tag of this page
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		employee_id = Request("employeeid")
		holiday_rule = Request("hrule")
		build_holiday_messages()
        check_Email_Exist()
	End If

    Function check_Email_Exist()
     
     email_exist = 1

    SQL = "SELECT WORKEMAIL " &_
          "FROM  dbo.E_JOBDETAILS JD " &_
          "INNER JOIN dbo.E__EMPLOYEE E ON JD.LINEMANAGER = E.EMPLOYEEID " &_
          "INNER JOIN dbo.E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
          "WHERE JD.EMPLOYEEID = " & employee_id

    Call OpenRs(rsEmail, SQL)

    If NOT rsEmail.EOF And NOT rsEmail.BOF Then

        Dim strRecipient
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)
        if strRecipient = "" then            
            email_exist = 0
        End If
    End If
    Call CloseRs(rsEmail)

    End Function

	Function build_holiday_messages()
		
	page="My Job"
    	strSQL = "EXEC E_HOLIDAY_DETAIL " & session("employeeid") & " ,'" & page & "'"
     
		Call OpenRs (rsHols, strSQL) 
		IF NOT rsHols.EOF Then
	            time_off_in_lieu_owed =  rsHols(6) 
                part_full =  rsHols(7)
		end if
		CloseRS(rsHols)		
	End Function

	Function new_record()

		Dim strSQL, journal_id, status_id, action_id , title ,AL_HRS

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		employee_id = Request("employeeid")
        title = Replace(Request("title"),"'", "''")

		If itemnature_id = 1 Then 
			status_id = 1
			action_id = 1
		Else
			status_id = 3
			action_id = 3 
		End If

		holtype = request.form("holtype")
		notes = Replace(request.form("txt_NOTES"),"'", "''")

		' Function to check any existing leaves on the same date(s)
		strSQL= "SELECT dbo.E_CHECK_EXISTINGLEAVE("& employee_id &",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "'," & Request.Form("hid_duration_in_days") & ", '" & holtype & "')  AS RESULT"
		Call OpenRs(rsExistingLeave, strSQL)
	        RESULT = rsExistingLeave("RESULT")
	    Call CloseRs(rsExistingLeave)

        ' If no record found, then insert in tables
		If RESULT = 0 Then
		    strSQL = 	"SET NOCOUNT ON;" &_
					    "INSERT INTO E_JOURNAL (EMPLOYEEID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE) " &_
					    "VALUES (" & employee_id & ", " & item_id & ", " & itemnature_id & ", " & status_id & ", '" & Now & "', '" & title & "');" &_
					    "SELECT SCOPE_IDENTITY() AS JOURNALID;"
		    Set rsSet = Conn.Execute(strSQL)
		    journal_id = rsSet.fields("JOURNALID").value


     		strSQL = 	"SET NOCOUNT ON;" &_
                        "INSERT INTO E_ABSENCE " &_
					    "(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, DURATION_HRS, REASON, HOLTYPE, NOTES) " &_
					    "VALUES (" & journal_id & ", " & status_id & " , " & action_id & ", '" & Now & "', " & Session("userid") & ", '" & FormatDateTime(cDate(Request.Form("txt_STARTDATE") & " " & Request.Form("txt_From"))) & "', '" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "', " & Request.Form("hid_duration_in_days") & " ," &  Request.Form("txt_DURATION") & ", '" & Replace(title,"'", "''") & "', '" & holtype & "', '" & Replace(notes,"'", "''") & "')" &_
                        " SELECT SCOPE_IDENTITY() AS HISTORYID;"

            Set rsSet = Conn.Execute(strSQL)
		        new_history_id = rsSet.fields("HISTORYID").value
		        rsSet.close()
		    Set rsSet = Nothing

            Call sendEmail(journal_id,new_history_id,employee_id)
		End If
	End Function


    Function sendEmail(journalid,new_history_id,employee_id)

    SQL = "SELECT STARTDATE, RETURNDATE, DURATION_HRS " &_ 
          "FROM E_ABSENCE " &_
          "WHERE ABSENCEHISTORYID = " & new_history_id
    Call OpenRs(rsEmpRec, SQL)
    If NOT rsEmpRec.EOF Then
        sdate = rsEmpRec("STARTDATE")
        edate =  rsEmpRec("RETURNDATE")
        duration =  rsEmpRec("DURATION_HRS")
    End If
    Call CloseRs(rsEmpRec)

    SQL = "SELECT WORKEMAIL " &_
          "FROM dbo.E_JOBDETAILS JD " &_
          "INNER JOIN dbo.E__EMPLOYEE E ON JD.LINEMANAGER = E.EMPLOYEEID " &_
          "INNER JOIN dbo.E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
          "WHERE JD.EMPLOYEEID = " & employee_id

	Call OpenRs(rsEmail, SQL)

	If NOT rsEmail.EOF And NOT rsEmail.BOF Then
		' send email
		Dim strRecipient, emailSubject, strSender
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

        SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT WHERE EMPLOYEEID = " & employee_id
        Call OpenRs(rsSenderEmail, SQL)

        If NOT rsSenderEmail.EOF And NOT rsSenderEmail.BOF Then 
            strSender=(rsSenderEmail.Fields.Item("WORKEMAIL").Value)
        End if
        Call CloseRs(rsSenderEmail)

        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        emailbody = fullname & " has requested " & duration &  " hour(s) leave for the date " & sdate & ". Please log in to RSLmanager to acknowledge/approve/decline this request."
        emailSubject = "Leave Request"

        On Error Resume NEXT    
        Set iMsg.Configuration = iConf
        iMsg.To = strRecipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send
            
        If Err.number <> 0 Then
            If strRecipient = "" Or IsNull(strRecipient) Then
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed due to manager email address not provided, so please inform the concerned person.');</script>") 
            Else  
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed, so please inform the concerned person.');</script>")
            End If  
            Err.Clear()
            ON ERROR GOTO 0
        End If

	End If
	Call CloseRs(rsEmail)

    End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
<script type="text/javascript" language="javascript">

    var FormFields = new Array();
    FormFields[0] = "txt_STARTDATE|Startd Date|DATE|Y"
    FormFields[1] = "txt_From|From Time|TIME|Y"

    function save_form() {
        if (!checkForm()) return false;
        if(! validate_duration()) return false;
        if(!isPartFull()) return false;
        if(!validate_email()) return false;

        calLeaveDuration();
        var nature_id = "<%=itemnature_id%>";
        var dSDate = new Date(JSlong_date(document.getElementById("txt_STARTDATE").value));
        var dSDate_hid = new Date(JSlong_date(document.getElementById("hid_STARTDATE").value));
        var hrule = document.getElementById("hid_hrule").value;

        document.getElementById("hid_go").value = 1;
        document.RSLFORM.action = "iTimeOffInLieu.asp?employeeid=<%=employee_id%>&natureid=<%=itemnature_id%>&itemid=<%=item_id%>&title=<%=title%>";
        document.RSLFORM.target = "";
        document.RSLFORM.submit();
    }


    function calLeaveDuration() {
        if (!checkForm()) return false;
        var emp = "<%= employee_id%>";
        var natureid = "<%=itemnature_id%>"
        var currentDate = new Date
        var cMonth = currentDate.getMonth() + 1
        var edate = currentDate.getDate() + '/' + cMonth + '/' + currentDate.getFullYear();
        var sdate = document.getElementById("txt_STARTDATE").value;
        document.RSLFORM.action = "iEmpWorkDays.asp?employeeid=" + emp + "&sdate=" + sdate + "&edate=" + edate + "&natureid=" + natureid
        document.RSLFORM.target = "serverFrame"
        document.RSLFORM.submit();
    }


    function return_data() {
        document.getElementById("txt_STARTDATE").focus();
        //document.getElementById("btn_submit").disabled = true;
        if ("<%=path%>" == 1 && "<%=RESULT%>" == 0) {
            parent.document.getElementById("div9").style.display = "none";
            parent.frm_erm.location.reload();
            parent.frm_nature.location.href = "../blank.asp";
            parent.swap_item_div(8);
            parent.document.getElementById("div8").style.display = "block";
            parent.RSLFORM.reset();
            parent.document.getElementById("frm_pd").contentWindow.location.reload();
        }
        if ("<%=RESULT%>" >= 1) {
            alert("You cannot book leave as you have already booked the date(s)");
            parent.RSLFORM.reset(); 
            parent.document.getElementById("frm_nature").src = "blank.asp"; 
            return false;
        }
    }

    function check_date() {
        if (!checkForm()) return false;
        calLeaveDuration();
    }

    function isPartFull() { 

        var partFullTime = parseInt( <%=part_full%> ) ;
        if( partFullTime== 1 || partFullTime ==2)
           {
                return true;         
           }else{
               alert("Only Part/Full time employees are allowed to apply for Time Off in LIEU.");
               return false;           
           }           
        }

    function validate_duration() { 
    
        var timeOffInLieuOwed = parseFloat( <%=time_off_in_lieu_owed%> ) ;
        var duration = parseFloat(document.getElementById("txt_DURATION").value);

        if(duration > timeOffInLieuOwed)
        {
           alert("You have exceeded the number of hours you are currently eligible for 'Time Off In Lieu'.");
           return false;
        }

        if(duration <= 0 || duration >12)
        {
           alert("Invalid duration, maximum 12 hours are allowed.");
           return false;
        }

        return true;
        
    }

    function validate_email() {    
        var checkExist = parseInt(document.getElementById("hdnCheckEmail").value);
        if(checkExist == 0){
            alert("Unable to save because email address of recipient does not exist.") 
            return false;
        }else{
        
            return true;
        }
    }

</script>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="iTimeOffInLieu.asp?employeeid=<%=employee_id%>&natureid=<%=itemnature_id%>&itemid=<%=item_id%>&title=<%=title%>">
    <input type="text" value="" name="txt_RETURNDATE" id="txt_RETURNDATE" maxlength="10" style="display:none" />
    <input type="radio" id="radEndAM" name="radEndAM" value="morning" style="display:none" />
    <table cellpadding="3" cellspacing="0" border="0">
        <tr>
            <td>
                Start Date:
            </td>
            <td colspan="2">
                <input type="text" value="<%=Date%>" name="txt_STARTDATE" id="txt_STARTDATE" class="textbox100"
                    maxlength="10"  />
                     &nbsp
                <img src="../Images/calendar.png" name="img_Calendar" id="img_Calendar"
                    border="0" alt="Calendar" />                
            </td>
            <td>
               <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                    border="0" alt="START DATE" />
            </td>
        </tr>
        <tr>
            <td>
                From:
            </td>
            <td colspan="2">
                <input name="txt_From" id="txt_From" type="text" maxlength="5" class="textbox50"   
                    value="" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_From" id="img_From" width="15px" height="15px"
                    border="0" alt="Start Time" />
            </td>
        </tr>
        <tr>
            <td>
                Duration:
            </td>
            <td colspan="2">
                <input name="txt_DURATION" id="txt_DURATION" type="text" class="textbox50" 
                    value="" /> &nbsp Hours
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DURATION" id="img_DURATION" width="15px" height="15px"
                    border="0" alt="DURATION" />
            </td>
        </tr>
        <tr>
            <td>
                Recorded By: &nbsp
            </td>
            <td colspan="2">
                <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                    readonly="readonly" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                    border="0" alt="RECORDED BY" />
            </td>
        </tr>
        <tr>
            <td>
                Notes
            </td>
            <td colspan="2">
                <textarea style="overflow: hidden" rows="9" cols="20" class="textbox200" name="txt_NOTES"
                    id="txt_NOTES"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="NOTES" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value='Save'
                    class="RSLButton" style="cursor: pointer" />
                <input type="hidden" name="dates" id="dates" value="<%=BANK_HOLIDAY_STRING%>" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="hidden" name="hid_STARTDATE" id="hid_STARTDATE" value="<%=emp_year_sdate%>" />
                <input type="hidden" name="hid_ENDDATE" id="hid_ENDDATE" value="<%=emp_year_edate%>" />
                <input type="hidden" name="hid_non_work_days" id="hid_non_work_days" value="0" />
                <input type="hidden" name="hid_duration_in_days" id="hid_duration_in_days" value="0" />
                <input type="hidden" name="holtype" id="holtype" />
                <input type="hidden" name="check_email" id="hdnCheckEmail" value ="<%=email_exist%>" />
                <input type="hidden" name="hid_hrule" id="hid_hrule" value="<%=holiday_rule%>" />
                <input type="hidden" name="hid_pagename" id="hid_pagename" value="iTimeOffInLieu.asp" />
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" width="600" height="600" style="display: none"></iframe>
</body>
</html>
