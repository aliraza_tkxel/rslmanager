<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, appraisal_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
    Dim filename

	Call OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")
    employee_id = Request("employeeid")
	ButtonText = " Update "
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			G.APPRAISALHISTORYID, J.TITLE, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
					"			G.DOCFILE " &_
					"FROM		E_APPRAISAL G " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.LASTACTIONUSER " &_
					"			LEFT JOIN E_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY APPRAISALHISTORYID DESC "

		Call OpenRs (rsSet, strSQL) 

		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<tr style=""color:gray"">"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<tr valign=""top"">"
				appraisal_history_id = rsSet("APPRAISALHISTORYID")
                filename = rsSet("DOCFILE")
			End If
				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<td>" & rsSet("CREATIONDATE") & "</td>" &_
															"<td>" & ResponseNotes & "</td>" &_															
															"<td>" & rsSet("FULLNAME")  & "</td>"
				str_journal_table = str_journal_table &     "<td><img src=""../Images/attach_letter.gif"" style=""cursor:pointer"" title=""Open Attachment"" onclick=""View_Doc("& rsSet("APPRAISALHISTORYID") &")"" alt=""Open Attachment"" /></td>"
                str_journal_table = str_journal_table & 	"</td><tr>"

			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""5"" align=""center"">No journal entries exist for this Appraisal Item.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Tenancy Agreement Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript">

    function View_Doc(history_id) {
        var mimetype = getmimetype("<%=filename%>");
        var strUrlPrms = "ff=DOCUMENTFILE&fn=DOCFILE&tn=E_APPRAISAL&cs=connRSL&cr=AppraisalHistoryId=" + history_id + "&mm=" + mimetype;
        window.open("/DBFileUpload/uploader.aspx?" + strUrlPrms, "_blank", "TOP=200, LEFT=400, scrollbars=yes, height=590, width=460, status=NO, resizable= Yes");
    }

    function getmimetype(docfile) {
        var mimetype, filetype, splitnametype
        splitnametype = docfile.split(".")
        filetype = splitnametype[1]
        switch (filetype) {
            case 'doc':
                mimetype = 'application/msword'
                break;
            case 'pdf':
                mimetype = 'application/pdf'
                break;
            case 'docx':
                mimetype = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                break;
            default:
                mimetype = ''
        }
        return mimetype;
    }

    function open_letter(letter_id) {
        var tenancy_id = parent.parent.MASTER_TENANCY_NUM
        window.open("../Popups/training_letter_plain.asp?tenancyid=" + tenancy_id + "&letterid=" + letter_id, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
    }

</script>
<body class='TA'>
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse" border="0">
        <thead>
            <tr valign="top">
                <td style="border-bottom: 1PX SOLID" nowrap="nowrap" width="120PX">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1PX SOLID">
                    <font color="blue"><b>Notes:</b></font>
                </td>                
                <td style="border-bottom: 1PX SOLID" nowrap="nowrap" width="150PX">
                    <font color="blue"><b>Created By:</b></font>
                </td>
                <td style="border-bottom: 1PX SOLID"  width="20px">
                    &nbsp;
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="4">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
