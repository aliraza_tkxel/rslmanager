<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim employee_id, item_id, itemnature_id, status_id, str_title, path, reason, holtype, notes , emp_year_sdate ,emp_year_edate, holiday_rule
	Dim Result

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")
	employee_id = Request("employeeid")

    a = "<string val=""'http://'""><ad>"

	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT STARTDATE,ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(" & employee_id & ")"
	Call OpenRs(rsDays, SQL)
	    emp_year_sdate = rsDays("STARTDATE")
	    emp_year_edate = rsDays("ENDDATE")
	Call CloseRs(rsDays)

	If path = "" Then path = 0 End If
	If path Then
		Call OpenDB()
		Call new_record()
		Call CloseDB()
	Else
		' these fields are used in the form tag of this page
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		reason = Request("reason")
		employee_id = Request("employeeid")
		holiday_rule = Request("hrule")
	End If

	Function new_record()

		Dim strSQL, journal_id, status_id, action_id , reason ,AL_HRS

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		employee_id = Request("employeeid")
        reason = Request("reason")

		If itemnature_id = 1 Then 
			status_id = 1
			action_id = 1
		Else
			status_id = 3
			action_id = 3 
		End If

		holtype = request.form("holtype")
		notes = Replace(request.form("txt_NOTES"),"'", "''")

		' Function to check any existing leaves on the same date(s)
		strSQL= "SELECT dbo.E_CHECK_EXISTINGLEAVE("& employee_id &",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "'," & Request.Form("txt_DURATION") & ", '" & holtype & "')  AS RESULT"
		Call OpenRs(rsExistingLeave, strSQL)
	        RESULT = rsExistingLeave("RESULT")
	    Call CloseRs(rsExistingLeave)

        ' If no record found, then insert in tables
		If RESULT = 0 Then
		    strSQL = 	"SET NOCOUNT ON;" &_
					    "INSERT INTO E_JOURNAL (EMPLOYEEID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, REASONID) " &_
					    "VALUES (" & employee_id & ", " & item_id & ", " & itemnature_id & ", " & status_id & ", '" & Now & "', " & reason & ");" &_
					    "SELECT SCOPE_IDENTITY() AS JOURNALID;"
		    set rsSet = Conn.Execute(strSQL)
		    journal_id = rsSet.fields("JOURNALID").value

		    strSQL = 	"INSERT INTO E_ABSENCE " &_
					    "(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, DURATION, REASONID, HOLTYPE, NOTES) " &_
					    "VALUES (" & journal_id & ", " & status_id & " , " & action_id & ", '" & Now & "', " & Session("userid") & ", '" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "', " & Request.Form("txt_DURATION") & " , " & reason & ", '" & holtype & "', '" & Replace(notes,"'", "''") & "')"
		    set rsSet = Conn.Execute(strSQL)
		End If
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
<script type="text/javascript" language="javascript">

    var FormFields = new Array();
    FormFields[0] = "txt_STARTDATE|Startd Date|DATE|Y"


    function save_form() {
        if (!checkForm()) return false;
        if (document.getElementById("txt_DURATION").value == 0) {
            alert("Duration of leaves can't be zero.Please add your working hours or contact administrator or manager")
            return false;
        }
        calLeaveDuration();
        var nature_id = "<%=itemnature_id%>";
        var dSDate = new Date(JSlong_date(document.getElementById("txt_STARTDATE").value));
        var dSDate_hid = new Date(JSlong_date(document.getElementById("hid_STARTDATE").value));
        var hrule = document.getElementById("hid_hrule").value;
        getSicknessType();
        document.getElementById("hid_go").value = 1;
        document.RSLFORM.action = "iSick.asp?employeeid=<%=employee_id%>&natureid=<%=itemnature_id%>&itemid=<%=item_id%>&reason=<%=reason%>";
        document.RSLFORM.target = "";
        document.RSLFORM.submit();
    }


    function calLeaveDuration() {
        if (!checkForm()) return false;
        var emp = "<%= employee_id%>";
        var natureid = "<%=itemnature_id%>"
        var currentDate = new Date
        var cMonth = currentDate.getMonth() + 1
        var edate = currentDate.getDate() + '/' + cMonth + '/' + currentDate.getFullYear();
        var sdate = document.getElementById("txt_STARTDATE").value;
        document.RSLFORM.action = "iEmpWorkDays.asp?employeeid=" + emp + "&sdate=" + sdate + "&edate=" + edate + "&natureid=" + natureid
        document.RSLFORM.target = "serverFrame"
        document.RSLFORM.submit();
    }


    function return_data() {
        document.getElementById("txt_STARTDATE").focus();
        document.getElementById("btn_submit").disabled = true;
        if ("<%=path%>" == 1 && "<%=RESULT%>" == 0) {
            parent.document.getElementById("div9").style.display = "none";
            parent.frm_erm.location.reload();
            parent.frm_nature.location.href = "../blank.asp";
            parent.swap_item_div(8);
            parent.document.getElementById("div8").style.display = "block";
            parent.RSLFORM.reset();
        }
        if ("<%=RESULT%>" >= 1) {
            alert("You cannot book leave as you have already booked the date(s)");
            return false;
        }
    }


    function check_date() {
        if (!checkForm()) return false;
        calLeaveDuration();
    }
</script>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="iGeneral.asp?employeeid=<%=employee_id%>&natureid=<%=itemnature_id%>&itemid=<%=item_id%>&title=<%=title%>">
    <input type="text" value="" name="txt_RETURNDATE" id="txt_RETURNDATE" maxlength="10" style="display:none" />
    <input type="radio" id="radEndAM" name="radEndAM" value="morning" style="display:none" />
    <table cellpadding="1" cellspacing="0" border="0">
        <tr>
            <td>
                Start Date
            </td>
            <td colspan="3">
                <input type="text" value="<%=Date%>" name="txt_STARTDATE" id="txt_STARTDATE" class="textbox100"
                    maxlength="10" onblur='check_date()' />
                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                    border="0" alt="START DATE" />
                &nbsp;full<input type="radio" id="radStartFull" name="radStart" value="days" onclick="sicknessDays()"
                    checked="checked" />
                am<input type="radio" id="radStartAM" name="radStart" value="morning" onclick="sicknessDays()" />
                pm<input type="radio" id="radStartPM" name="radStart" value="afternoon" onclick="sicknessDays()" />
            </td>
        </tr>
        <tr>
            <td>
                Duration
            </td>
            <td colspan="2">
                <input name="txt_DURATION" id="txt_DURATION" type="text" class="TEXTBOX100" readonly="readonly"
                    value="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DURATION" id="img_DURATION" width="15px" height="15px"
                    border="0" alt="DURATION" />
            </td>
        </tr>
        <tr>
            <td>
                Recorded By
            </td>
            <td colspan="2">
                <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                    readonly="readonly" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                    border="0" alt="RECORDED BY" />
            </td>
        </tr>
        <tr>
            <td>
                Notes
            </td>
            <td colspan="2">
                <textarea style="overflow: hidden" rows="9" cols="20" class="textbox200" name="txt_NOTES"
                    id="txt_NOTES"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="NOTES" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value='Save'
                    class="RSLButton" style="cursor: pointer" />
                <input type="hidden" name="dates" id="dates" value="<%=BANK_HOLIDAY_STRING%>" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="hidden" name="hid_STARTDATE" id="hid_STARTDATE" value="<%=emp_year_sdate%>" />
                <input type="hidden" name="hid_ENDDATE" id="hid_ENDDATE" value="<%=emp_year_edate%>" />
                <input type="hidden" name="hid_non_work_days" id="hid_non_work_days" value="0" />
                <input type="hidden" name="holtype" id="holtype" />
                <input type="hidden" name="hid_hrule" id="hid_hrule" value="<%=holiday_rule%>" />
                <input type="hidden" name="hid_pagename" id="hid_pagename" value="iSick" />
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" width="600" height="600" style="display: none"></iframe>
</body>
</html>
