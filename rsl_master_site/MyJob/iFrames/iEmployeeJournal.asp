<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table
	Dim nature_id,filter_sql, theYear, YearFilter

	employee_id = Request("employeeid")
	' This will only be available if the filter has been used
	nature_id =  Request("natureid")
	theYear =  Request("sel_YEAR")

	Call OpenDB()
	Call build_filter()
	Call build_journal()
	Call CloseDB()

	Function build_filter()

		' Filter by the nature
		If nature_id <> "" then
			filter_sql = " AND J.ITEMNATUREID = " & nature_id & " "
		End If

		'Filter By the year
		If theYear <> "" then
			YearFilter = " AND YEAR(J.CREATIONDATE) = " & theYear
		Else
			YearFilter = " AND YEAR(J.CREATIONDATE) = YEAR(GETDATE()) "
		End If

	End Function

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT 	J.EMPLOYEEID, J.JOURNALID, " &_
					"			CASE J.ITEMID WHEN 1 THEN 'iAbsenceDetail.asp' " &_
					"			WHEN 3 THEN 'iDevelopmentDetail.asp'  " &_
					"			WHEN 4 THEN 'iDiciplinaryDetail.asp'  " &_
					"			WHEN 5 THEN 'iExpensesDetail.asp'  " &_
					"			WHEN 6 THEN 'iNotesDetail.asp'  " &_
					"			WHEN 7 THEN 'iAppraisalDetail.asp'  " &_
					"			WHEN 8 THEN 'iSmsDetail.asp'  " &_
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			ISNULL(I.DESCRIPTION, 'N/A') AS ITEM, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			CASE J.ITEMNATUREID WHEN 1 THEN LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(EA.REASON,'')) ELSE ISNULL(J.TITLE, 'N/A') END TITLE," &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			J.ITEMNATUREID " &_
					"FROM	 	E_JOURNAL J " &_
					"			LEFT JOIN E_ITEM I 	ON J.ITEMID = I.ITEMID " &_
					"			LEFT JOIN E_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN E_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"			LEFT JOIN E_ABSENCE EA ON EA.JOURNALID=J.JOURNALID AND EA.ABSENCEHISTORYID=(SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID=J.JOURNALID AND J.EMPLOYEEID=" & employee_id & " AND J.ITEMID=1) " &_
					"			LEFT JOIN E_ABSENCEREASON ER ON ER.SID = EA.REASONID " &_
					"WHERE	 	J.EMPLOYEEID = " & employee_id & filter_sql & YearFilter &_
					"ORDER		BY J.JOURNALID DESC"

		Call OpenRs (rsSet, strSQL) 

		str_journal_table = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			str_journal_table = str_journal_table & 	"<tr onclick=""open_me(" & rsSet("employeeid") & "," & rsSet("ITEMNATUREID") & "," & rsSet("JOURNALID") & ",'" & rsSet("REDIR") & "')"" style=""cursor:pointer"" title=""Update"">" &_
															"<td>" & rsSet("CREATIONDATE") & "</td>" &_
															"<td>" & rsSet("ITEM") & "</td>" &_
															"<td>" & rsSet("NATURE") & "</td>" &_
															"<td>" & rsSet("TITLE")  & "</td>" &_
															"<td>" & rsSet("STATUS")  & "</td>" &_
														"<tr>"
			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)

		IF cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">
        function open_me(employeeid, int_nature, int_journal_id, str_redir) {
            parent.swap_item_div(10);
            parent.frm_erm.location.href = str_redir + "?employeeid=" + employeeid + "&journalid=" + int_journal_id + "&natureid=" + int_nature;
        }
    </script>
</head>
<body class='TA'>
    <table width="100%" cellpadding="1" cellspacing="0" style="behavior: url(../../Includes/Tables/tablehl.htc);
        border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr>
                <td style="border-bottom: 1PX SOLID">
                    <b><font color='BLUE'>Date</font></b>
                </td>
                <td style="border-bottom: 1PX SOLID">
                    <b><font color='BLUE'>Item</font></b>
                </td>
                <td style="border-bottom: 1PX SOLID">
                    <b><font color='BLUE'>Nature</font></b>
                </td>
                <td style="border-bottom: 1PX SOLID">
                    <b><font color='BLUE'>Title</font></b>
                </td>
                <td style="border-bottom: 1PX SOLID">
                    <b><font color='BLUE'>Status</font></b>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="6">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
