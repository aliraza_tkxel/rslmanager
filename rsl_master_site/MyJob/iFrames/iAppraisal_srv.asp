<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim item_id, itemnature_id, title, customer_id, employee_id, action, nature_id, Appraisalhistory_id

	Dim Notes,enqID
	Call OpenDB()

	action = request.Form("hid_Action")
	if action = "insert" then
        Call getPostedData()
	    Call new_record()
	End If
	Call CloseDB()

	Function getPostedData()
		item_id = Request.Form("itemid")
		itemnature_id = Request.Form("natureid")
		title = Request.Form("title")
		employee_id = Request.Form("employeeid")
	End Function

    Function AmendedFileName(FilePath)
        Dim FilePart
        FilePart=Split(filePath, "\")
        AmendedFileName=FilePart(UBound(FilePart))
    End Function

	Function new_record()

		Dim strSQL, journal_id

		if (Request.Form("chk_CLOSE") = 1) then
			New_Status = 14
		else
			New_Status = 13
		end if

		if not employee_id <> "" then employee_id = "NULL" end if

		'JOURNAL ENTRY
        strSQL = "SET NOCOUNT ON; INSERT INTO E_JOURNAL (EMPLOYEEID,ITEMID, ITEMNATUREID, TITLE,CREATIONDATE) VALUES (" & employee_id & "," & item_id & "," & itemnature_id & ",'" & title & "','" & date & "'); SELECT SCOPE_IDENTITY() AS JOURNALID;"
		set rsSet = Conn.Execute(strSQL)
        journal_id = rsSet.fields("JOURNALID").value
		rsSet.close()
		Set rsSet = Nothing

		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO E_APPRAISAL " &_
					"(JOURNALID, LASTACTIONUSER, NOTES, DOCFILE, LASTACTIONDATE) " &_
					"VALUES (" & journal_id & ", " & Session("userid") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "','" & Replace(AmendedFileName(Request("rslfile")), "'", "''") & "', GETDATE())" &_
					"SELECT SCOPE_IDENTITY() AS APPRAISALHISTORYID;"

		set rsSet = Conn.Execute(strSQL)
		Appraisalhistory_id = rsSet.fields("APPRAISALHISTORYID").value
		rsSet.close()
		Set rsSet = Nothing

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Appraisal Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        function addDocument() {
            if ("<%=action%>" == "insert") {
                parent.Add_Document("<%=Appraisalhistory_id%>")
            }
            return
        }

        function red() {
            parent.redirect();
        }
    </script>
</head>
<body onload="addDocument();">
</body>
</html>
