<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<% 
Response.Expires = 0 
Response.Expiresabsolute = Now() - 1 
Response.AddHeader "pragma","no-cache" 
Response.AddHeader "cache-control","private" 
Response.CacheControl = "no-cache" 
%>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/myjob_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim c_address1
	Dim c_address2
	Dim c_address3
	Dim c_town
	Dim c_county
	Dim c_postcode
	Dim c_hometel
	Dim c_mobile
	Dim c_workdd
	Dim c_workmobile
	Dim c_workext
	Dim c_homeemail
	Dim c_workemail
	Dim c_emgname
	Dim c_emgnumber
	Dim employee_id
	Dim disable_my_updates,c_emginfo

	employee_id = Request("employeeid")
	' only allow th4e user hm/herself to update their personal details
	If Cint(employee_id) = Cint(Session("userid")) Then
		disable_my_updates = ""
	Else
		disable_my_updates = " disabled "
	End If

	Call OpenDB()
	Call get_main_details()
	Call CloseDB()

	' get employee details
	Function get_main_details()

		Dim strSQL
		strSQL = 	"SELECT		C.EMPLOYEEID, " &_
					"			ISNULL(C.ADDRESS1, '') AS ADDRESS1, " &_
					"			ISNULL(C.ADDRESS2, '') AS ADDRESS2, " &_
					"			ISNULL(C.ADDRESS3, '') AS ADDRESS3, " &_
					"			ISNULL(C.POSTALTOWN, '') AS POSTALTOWN, " &_
					"			ISNULL(C.COUNTY, '') AS COUNTY, " &_
					"			ISNULL(C.POSTCODE, '') AS POSTCODE, " &_
					"			ISNULL(C.HOMETEL, '') AS HOMETEL, " &_
					"			ISNULL(C.MOBILE, '') AS MOBILE, " &_
					"			ISNULL(C.WORKDD, '') AS WORKDD, " &_
					"			ISNULL(C.WORKEXT, '') AS WORKEXT, " &_
					"			ISNULL(C.HOMEEMAIL, '') AS HOMEEMAIL, " &_
					"			ISNULL(C.WORKMOBILE, '') AS WORKMOBILE, " &_
					"			ISNULL(C.WORKEMAIL, '') AS WORKEMAIL, " &_
					"			ISNULL(C.EMERGENCYCONTACTNAME, '') AS EMERGENCYCONTACTNAME, " &_
					"			ISNULL(C.EMERGENCYCONTACTTEL, '') AS EMERGENCYCONTACTTEL, " &_
					"			ISNULL(C.EMERGENCYINFO, '') AS EMERGENCYINFO " &_
					"FROM		E_CONTACT C " &_
					"WHERE		C.EMPLOYEEID = " & employee_id

		Call OpenRs (rsSet,strSQL)

		If Not rsSet.EOF Then
			' contact details section
			c_address1		= rsSet("ADDRESS1")
			c_address2		= rsSet("ADDRESS2")
			c_address3		= rsSet("ADDRESS3")
			c_town			= rsSet("POSTALTOWN")
			c_county		= rsSet("COUNTY")
			c_postcode		= rsSet("POSTCODE")
			c_hometel		= rsSet("HOMETEL")
			c_workmobile	= rsSet("WORKMOBILE")
			c_mobile		= rsSet("MOBILE")
			c_workdd		= rsSet("WORKDD")
			c_workext		= rsSet("WORKEXT")
			c_homeemail		= rsSet("HOMEEMAIL")
			c_workemail		= rsSet("WORKEMAIL")
			c_emgname		= rsSet("EMERGENCYCONTACTNAME")
			c_emgnumber		= rsSet("EMERGENCYCONTACTTEL")
			c_emginfo		= rsSet("EMERGENCYINFO")

			If c_emginfo <> "" Then
			    c_emgnumber = c_emgnumber & " (" & c_emginfo & ")"
			End If

		End If

		Call CloseRs(rsSet)

	End Function
%>
<html>
<head>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Tenancy Agreement Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body>
    <table width="750" style="height: 180px; border-right: SOLID 1PX #133E71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" width="70%" height="100%">
                <table width="100%" style="height: 100%; border: SOLID 1PX #133E71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td class='TITLE'>
                            Home Tel
                        </td>
                        <td>
                            <%=c_hometel%>
                        </td>
                        <td class='TITLE'>
                            Address1
                        </td>
                        <td width="160">
                            <%=c_address1%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Mobile (Emergency Staff Contact)
                        </td>
                        <td title='<%=c_mobile%>'>
                            <%=c_mobile%>
                        </td>
                        <td class='TITLE'>
                            Address2
                        </td>
                        <td>
                            <%=c_address2%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Home Email
                        </td>
                        <td>
                            <%=Left(c_homeemail,25)%>
                        </td>
                        <td class='TITLE'>
                            Address3
                        </td>
                        <td>
                            <%=c_address3%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Work DD
                        </td>
                        <td>
                            <%=c_workdd%>
                        </td>
                        <td class='TITLE'>
                            Postal Town
                        </td>
                        <td>
                            <%=c_town%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Work Mobile
                        </td>
                        <td title='<%=c_workmobile%>'>
                            <%=c_workmobile%>
                        </td>
                        <td class='TITLE'>
                            County
                        </td>
                        <td>
                            <%=c_county%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Work Email
                        </td>
                        <td title='<%=Left(c_workemail,25)%>'>
                            <%=Left(c_workemail,25)%>
                        </td>
                        <td class='TITLE'>
                            Postcode
                        </td>
                        <td>
                            <%=c_postcode%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Emergency
                        </td>
                        <td>
                            <%=c_emgname%>
                        </td>
                        <td class='TITLE'>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Emergency Tel
                        </td>
                        <td>
                            <%=c_emgnumber%>
                        </td>
                        <td class='TITLE'>
                            &nbsp;
                        </td>
                        <td align="right">
                            <input type="button" name="BTN_UPDATEPD" <%=disable_my_updates%> value="Update" class="RSLBUTTONSMALL"
                                style="cursor: pointer" title="Update" onclick="parent.update_record('/myjob/popups/pContact.asp?employeeid=<%=employee_id%>',720,270)" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="50%">
                <%=str_holiday%>
            </td>
        </tr>
        <tr>
            <td width="30%" height="50%">
                <%=str_policy%>
            </td>
        </tr>
    </table>
</body>
</html>
