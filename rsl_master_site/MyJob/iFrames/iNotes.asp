<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, employee_id, tenancy_id, path, lstTeams,DangerratingText,showJournal
	Call OpenDB()

	path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then
		Call get_querystring()
		Call loadData()
	Else
		Call new_record()
	End If

	Call CloseDB()

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		txt_notes = replace(Request.form("txt_notes"),"'", "''")
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		employee_id = Request("employeeid")
		tenancy_id = Request("tenancyid")

	End Function


	Function loadData()
		If employee_id <> "" Then
		    SQL = "SELECT * FROM E_NOTES WHERE employeeid = " & employee_id & " ORDER BY NOTEDATE DESC"
		    Set rsSet = Conn.Execute(SQL)
		    IF NOT rsSet.eof Then
		        DangerratingText = rsSet.fields("NOTESDESC").value
		    End If
		        rsSet.close()
		    Set rsSet = Nothing
		End If
	End Function


	Function new_record()

		Dim strSQL, journal_id
		Call get_querystring()
		txt_notes = replace(Request.form("txt_notes"),"'","''")
		' JOURNAL ENTRY
		SQL = "SET NOCOUNT ON; INSERT INTO e_journal (employeeid,ITEMID, ITEMNATUREID, TITLE,CREATIONDATE) VALUES (" & employee_id & "," & item_id & "," & itemnature_id & ",'" & title & "','" & date & "'); SELECT SCOPE_IDENTITY() AS JOURNALID;"

		set rsSet = Conn.Execute(SQL)
		journal_id = rsSet.fields("JOURNALID").value

		rsSet.close()

		strSQL = "INSERT INTO E_NOTES(NOTESDESC,NOTEDBY,employeeid, journalid, NOTEDATE) VALUES " &_
				 "('" & txt_notes & "'," & Session("UserID") & "," & employee_id & "," & journal_id &  ",GETDATE())"
		Conn.Execute(strSQL)
		showJournal = "true"

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
	
	var FormFields = new Array();
	    FormFields[0] = "txt_notes|Notes|TEXT|Y"
	
	function save_form(){
		if (!checkForm()) return false;
		document.RSLFORM.action = "INOTES.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&employeeid=<%=employee_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		document.RSLFORM.hid_go.value = 1;
		document.RSLFORM.submit();
	}
	
	function return_data(){	
		if (<%=path%> == 1)	{
			parent.document.getElementById("div9").style.display = "none";
			parent.frm_erm.location.reload();
			parent.frm_nature.location.href = "../blank.asp";
			parent.swap_item_div(8);
			parent.document.getElementById("div8").style.display = "block";
            parent.document.getElementById("RSLFORM").reset();
		}
	}

</script>
<body class='TA' onload="return_data()">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style='height: 7px'>
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea name="txt_notes" id="txt_notes" style="overflow: hidden; width: 300px"
                    onkeyup="" class='textbox200' rows="9" cols="20"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_notes" id="img_notes" width="15px" height="15px"
                    border="0" alt="Notes" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                &nbsp;
                <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="button" name="Submit" value="Save" onclick="save_form()" class="RSLButton" style="cursor:pointer" />
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" width="400" height="100" style="display: none">
    </iframe>
</body>
</html>
