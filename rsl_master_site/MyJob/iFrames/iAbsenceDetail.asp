<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, absence_history_id, only_allow_line_manager, last_startdate, reason_id
	Dim absence_status

	Call OpenDB()
        journal_id = Request("journalid")
        nature_id = Request("natureid")
        employee_id = Request("employeeid")
	Call build_journal()
	Call CloseDB()
 
	' only allow line manager to update holidays
	If Cint(employee_id) = Cint(Session("userid")) Or isLineManager(employee_id, Session("userid")) Then
		only_allow_line_manager = ""
	Else
		only_allow_line_manager = " disabled=""disabled"" "
	End If

	' if its me and nature is sick I cant update
	If (Cint(employee_id) = Cint(Session("userid")) ) AND nature_id = 1 Then only_allow_line_manager = " disabled=""disabled"" " End If

    ' if its me and nature is TOIL recorded I cant update
    If nature_id = 47 Then only_allow_line_manager = " disabled=""disabled"" " End If

	' if its me and holiday startdate has passed then I can't update
	If ((Cint(employee_id) = Cint(Session("userid")) ) AND (CDate(last_startdate)) <= date) Then only_allow_line_manager = " disabled=""disabled"" " End If

	' If the user is a memeber of HR then allow amends
	If Session("TeamCode") =  "HUM" Then
	    only_allow_line_manager = ""
	End If

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		ISNULL(CONVERT(NVARCHAR, A.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103) ,'') AS STARTDATE,  " &_
					"			ISNULL(CONVERT(NVARCHAR, A.RETURNDATE, 103) ,'') AS RETURNDATE,  " &_
					"			A.STARTDATE AS DTM_START, " &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES, " &_
					"			LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(A.REASON, '')) AS REASON, " &_
                    " CASE " &_
                    " WHEN N.description IN( 'BRS Time Off in Lieu', 'BRS TOIL Recorded', " &_
                    "                       'Internal Meeting' ) THEN  " &_
                    " CONVERT(VARCHAR, A.duration_hrs)  " &_
                    " + ' hour(s)'  " &_
                    " ELSE " &_
			        "    CONVERT(VARCHAR, CASE " &_
					"			            WHEN N.description = 'Sickness' THEN " &_
					"					            CASE " &_
					"						            WHEN A.returndate IS NULL  THEN " &_
					"							            dbo.Emp_sickness_duration(J.employeeid, A.startdate, Getdate(), A.journalid) " &_
					"						            WHEN A.startdate = A.returndate  AND A.duration > 1 AND dbo.Emp_sickness_duration(J.employeeid, A.startdate, A.returndate, A.journalid) !=0 THEN " &_
					"							            dbo.Emp_sickness_duration(J.employeeid, A.startdate, A.returndate, A.journalid) " &_
					"						            ELSE  " &_
					"							            A.duration  " &_
					"					            END  " &_
					"			            ELSE  " &_
					"				            A.duration  " &_
					"		             END ) +  " &_
                    "     ' day(s)' " &_
                    "  END " &_
                    " AS DURATION, " &_                    
					"			S.DESCRIPTION, " &_
					"			A.ABSENCEHISTORYID, " &_
					"			A.ITEMSTATUSID, ISNULL(ER.SID,-1) AS SID, " &_
                    "           ISNULL(NULLIF(RTRIM(LTRIM((ISNULL(E.[FirstName], '') + ISNULL(' ' + E.[LastName], '')))),''),'N/A') AS CreatedBy " &_
					"FROM		E_ABSENCE A " &_
                    "	        INNER JOIN  E_JOURNAL J ON A.JOURNALID = J.JOURNALID "&_
					"			INNER JOIN E_NATURE N ON J.ITEMNATUREID = N.ITEMNATUREID" &_
					"			LEFT JOIN E_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID  " &_
                    "			LEFT JOIN E_ABSENCEREASON ER ON ER.SID = A.REASONID " &_
                    "			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = A.LASTACTIONUSER " &_
					"WHERE		A.JOURNALID = " & journal_id &_
					"ORDER 		BY ABSENCEHISTORYID DESC "



		Call OpenRs (rsSet, strSQL)

		str_journal_table = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				str_journal_table = str_journal_table & "<tr>"
				absence_history_id = rsSet("ABSENCEHISTORYID")
                reason_id = rsSet("SID")
				last_startdate = rsSet("DTM_START")
				absence_status = rsSet("ITEMSTATUSID")
			End If
				str_journal_table = str_journal_table & 	"<td>" & rsSet("CREATIONDATE") & "</td>" &_
															"<td>" & rsSet("NOTES") & "</td>" &_
															"<td>" & rsSet("DESCRIPTION") & "</td>" &_
															"<td>" & rsSet("REASON")  & "</td>" &_
															"<td>" & rsSet("STARTDATE")  & "</td>" &_
															"<td>" & rsSet("RETURNDATE")  & "</td>" &_
															"<td>" & rsSet("DURATION")  & "</td>" &_
                                                            "<td>" & rsSet("CreatedBy")  & "</td>" &_
														"<tr>"
			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""8"" align=""center"">No journal entries exist.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

        function update_leave(int_absence_history_id) {
            var str_win
            str_win = "../pLeave.asp?employeeid=<%=employee_id%>&absencehistoryid=" + int_absence_history_id + "&natureid=<%=nature_id%>";
            window.open(str_win, "display", "width=407,height=320, left=200,top=200");
        }

        function update_TimeOffInLieuLeave(int_absence_history_id) {
            var str_win
            str_win = "../pTimeOffInLieuLeave.asp?employeeid=<%=employee_id%>&absencehistoryid=" + int_absence_history_id + "&natureid=<%=nature_id%>";
            window.open(str_win, "display", "width=407,height=320, left=200,top=200");
        }

        function update_sickness(int_absence_history_id, reason_id) {
            var str_win
            str_win = "../pSickLeave.asp?employeeid=<%=employee_id%>&absencehistoryid=" + int_absence_history_id + "&natureid=<%=nature_id%>" + "&reason_id=" + reason_id;
            window.open(str_win, "display", "width=407,height=350, left=200,top=200");
        }

        function update_InternalMeeting(int_absence_history_id) {
            var str_win
            str_win = "../pInternalMeeting.asp?employeeid=<%=employee_id%>&absencehistoryid=" + int_absence_history_id + "&natureid=<%=nature_id%>";
            window.open(str_win, "displayInternalMeeting", "width=407,height=320, left=200,top=200");
        }

    </script>
</head>
<body class="TA">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        border="0">
        <thead>
            <tr>
                <td colspan="7" align="right">
                    <% If nature_id = 1 Then %>
                    <input type="button" name="BTN_UPDATE" <%=only_allow_line_manager%> class="RSLBUTTON"
                        value="Update" onclick="update_sickness(<%=absence_history_id%>,<%=reason_id%>)"
                        style="cursor: pointer" title="Update" />
                    <% ElseIf nature_id = 48 Then%>
                    <input type="button" name="BTN_UPDATE" <%=only_allow_line_manager%> class="RSLBUTTON"
                        value="Update" onclick="update_InternalMeeting(<%=absence_history_id%>)"
                        style="cursor: pointer" title="Update" />
                    <% ElseIf nature_id = 49 Then%>
                    <input type="button" name="BTN_UPDATE" <%=only_allow_line_manager%> class="RSLBUTTON"
                        value="Update" onclick="update_TimeOffInLieuLeave(<%=absence_history_id%>,<%=reason_id%>)"
                        style="cursor: pointer" title="Update" />
                    <% Else %>
                    <input type="button" name="BTN_UPDATE" <%=only_allow_line_manager%> class="RSLBUTTON"
                        value="Update" onclick="update_leave(<%=absence_history_id%>)" style="cursor: pointer"
                        title="Update" />
                    <% End If %>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1PX SOLID">
                    Date
                </td>
                <td style="border-bottom: 1PX SOLID">
                    Notes
                </td>
                <td style="border-bottom: 1PX SOLID">
                    Status
                </td>
                <td style="border-bottom: 1PX SOLID">
                    Reason
                </td>
                <td style="border-bottom: 1PX SOLID">
                    Start Date
                </td>                
                <td style="border-bottom: 1PX SOLID">
                    End Date
                </td>                
                <td style="border-bottom: 1PX SOLID">
                    Duration
                </td>
                <td style="border-bottom: 1PX SOLID">
                    Created By
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="8">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
