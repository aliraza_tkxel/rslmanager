<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim EmpId, item_id, leave_sDate, leave_eDate, non_working_days,natureid

	EmpId = Request("employeeid")
    natureid = Request("natureid")
	leave_sDate = Request("sdate")
	leave_eDate = Request("edate")

    If leave_eDate = "" Then
        leave_eDate = date
    End If

    Call OpenDB()
	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT dbo.E_CAL_WORKDAYS_IN_ABSENCE_BY_PATTERN(" & EmpId & ",'" & leave_sDate & "','" & leave_eDate & "') AS NON_WORKING_DAYS"
	Call OpenRs(rsDays, SQL)
		non_working_days = rsDays("NON_WORKING_DAYS")
	Call CloseRs(rsDays)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function Return_NonWorkDays()
        {
            parent.document.getElementById("hid_non_work_days").value = "<%=non_working_days%>";
            <% if (natureid=1) then %>
            {
            parent.sicknessDays();
            }
            <% else %>
            {
            parent.workDays();
            }
            <% end if %>
        }

    </script>
</head>
<body onload="Return_NonWorkDays();">
</body>
</html>
