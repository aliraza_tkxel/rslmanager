<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim employee_id, item_id, itemnature_id, status_id, str_title, path, title, holtype

	path = request.form("hid_go")

	If path  = "" Then path = 0 End If
	If path Then
		Call OpenDB()
		Call new_record()
		Call CLoseDB()
	Else
		' these fields are used in the form tag of this page
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
	End If

	Function new_record()

		Dim strSQL, journal_id

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		holtype = request.form("holtype")

		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO E_JOURNAL (EMPLOYEEID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE) " &_
					"VALUES (" & Session("userid") & ", " & item_id & ", " & itemnature_id & ", 3, '" & Now & "', '" & title & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value

		strSQL = 	"INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, REASON, HOLTYPE) " &_
					"VALUES (" & journal_id & ", 3 , 3, '" & Now & "', " & Session("userid") & ", '" & Request.Form("txt_STARTDATE") & "', '" & Request.Form("txt_RETURNDATE") & "', " & Request.Form("txt_DURATION") & " , '" & title & "', '" & holtype & "')"
		set rsSet = Conn.Execute(strSQL)
		
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
<script type="text/javascript" language="javascript">

	var FormFields = new Array();
	    FormFields[0] = "txt_STARTDATE|Startd Date|DATE|Y"
	    FormFields[1] = "txt_RETURNDATE|Return Date|DATE|Y"

	function save_form() {
		if (!checkForm()) return false;
		document.RSLFORM.hid_go.value = 1;
		document.RSLFORM.submit();
	}


	function return_data() {
		if (<%=path%> == 1)	{
			parent.document.getElementById("div9").style.display = "none";
			parent.frm_erm.location.reload();
			parent.frm_nature.location.href = "../blank.asp";
			parent.swap_item_div(8);
			parent.document.getElementById("div8").style.display = "block";
			parent.RSLFORM.reset();
		}
	}

	function check_date(){
		if (!checkForm()) return false;
		workDays();
	}
</script>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="iLeave.asp?natureid=<%=itemnature_id%>&itemid=<%=item_id%>&title=<%=title%>">
    <table width="100%" cellpadding="1" cellspacing="0" border="0">
        <tr>
            <td>
                Requested
            </td>
            <td colspan="3">
                <input type="radio" id="radSingle" name="radType" value="single" checked="checked" onclick='{endText.style.visibility="hidden";workDays()}' />
                Single
                <input type="radio" id="radMulti" name="radType" value="multiple" onclick='{endText.style.visibility="visible";workDays()}' />
                Multiple
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Start Date
            </td>
            <td width="50PX">
                <input type="text" value="<%=Date%>" name="txt_STARTDATE" id="txt_STARTDATE" class="textbox100" maxlength="10"
                    onblur='check_date()' />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px" border="0" alt="STARTDATE" />
            </td>
            <td>
                &nbsp;full
                <input type="radio" id="radStartFull" name="radStart" value="days" onclick="workDays()"
                    checked="checked" />
                am
                <input type="radio" id="radStartAM" name="radStart" value="morning" onclick="workDays()" />
                pm
                <input type="radio" id="radStartPM" name="radStart" value="afternoon" onclick="workDays()" />
            </td>
        </tr>
        <tr id="endText" style="visibility: hidden">
            <td>
                End Date
            </td>
            <td>
                <input type="text" value="<%=Date%>" name="txt_RETURNDATE" id="txt_RETURNDATE" class="textbox100" maxlength="10" onblur='check_date()' />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RETURNDATE" id="img_RETURNDATE" width="15px" height="15px" border="0" alt="RETURN DATE" />
            </td>
            <td>
                &nbsp;full
                <input type="radio" id="radEndFull" name="radEnd" value="full" onclick="workDays()"
                    checked="checked" />
                am
                <input type="radio" id="radEndAM" name="radEnd" value="morning" onclick="workDays()" />
                pm
                <input type="radio" id="radEndPM" name="radEnd" value="afternoon" onclick="workDays()" />
            </td>
        </tr>
        <tr>
            <td>
                Duration
            </td>
            <td colspan="3">
                <input type="text" name="txt_DURATION" id="txt_DURATION" class="TEXTBOX100" readonly="readonly" value="1" />
                <input type="button" name='btn_submit' id="btn_submit" onclick='save_form()' value='Save' class="RSLButton" />
                <input type="hidden" name="dates" value="<% =BANK_HOLIDAY_STRING%>" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="hidden" name="holtype" id="holtype" />
                </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
</body>
</html>
