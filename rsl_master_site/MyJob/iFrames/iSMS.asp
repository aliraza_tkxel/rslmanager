<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpenDB()
    Dim item_id, itemnature_id, title, employee_id, tenancy_id, nature_id, nature_desc, mobile
	get_querystring()

    SQL = "SELECT WORKMOBILE FROM dbo.E_CONTACT WHERE EMPLOYEEID=" & employee_id
	Call OpenRs(rsMobile, SQL)
	
    IF rsMobile.EOF=False then    
        mobile = rsMobile("WORKMOBILE")
	End If
    Call CloseRs(rsMobile)
    ' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()
	
		txt_notes = replace(Request.form("txt_notes"),"'", "''")
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		employee_id = Request("employeeid")
		tenancy_id = Request("tenancyid")
		
	End Function
    CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > Send SMS</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "txt_MOBILE|Mobile|TEXT|Y"
	FormFields[1] = "txt_MESSAGE|Message|TEXT|Y"
	
	function save_form(){
		if (!checkForm()) return false;
		RSLFORM.action = "../Serverside/SMS_svr.asp?itemid=<%=item_id%>&nature_desc=<%=nature_desc%>&natureid=<%=itemnature_id%>&title=<%=title%>&employeeid=<%=employee_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		RSLFORM.target = "ServerIFrame"
		RSLFORM.submit()
	}


	var set_length = 4000;

	function countMe() {
	  
	  	var str = event.srcElement
	  	var str_len = str.value.length;

		  if(str_len >= set_length) {
			  str.value = str.value.substring(0, set_length-1);
	  			}
}		
    
    function return_data(){
	
	
			parent.div9.style.display = "none";
			parent.frm_erm.location.reload();
			parent.frm_nature.location.href = "../blank.asp";
			parent.swap_item_div(8);
			parent.div8.style.display = "block";
			parent.RSLFORM.reset();
		
	}	

</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="">
	<TABLE CELLPADDING=1 CELLSPACING=1 BORDER=0>
        <FORM NAME=RSLFORM METHOD=POST>		
		<tr>
			<td>Mobile : </td>
			<td><input type="text" name="txt_MOBILE" value="<%=mobile%>" class="textbox200" tabindex="1" /></td>
			<TD><image src="/js/FVS.gif" name="img_MOBILE" width="15px" height="15px" border="0"></TD>							
		</tr>
        <TR style='height:7px'><TD></TD></TR>
		<TR>
		    <TD VALIGN=TOP>Message :
		    </TD><TD><textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="countMe()" class="textbox200" rows="4" name="txt_MESSAGE"></textarea>
		    </TD>	
            <TD><image src="/js/FVS.gif" name="img_MESSAGE" width="15px" height="15px" border="0"> 
            </TD>
		</TR>
		
		<TR>		
        <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
            <td align="right" colspan="2">
            <input type="button" value="Save" class="RSLButton" title='View letter' onClick="save_form()" "name="button">
    	    </TD>
		</TR>
		
		<INPUT type=HIDDEN name=hid_TENANCYID value="<%=tenancy_id%>" />

	</FORM>
   	</TABLE>
<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:none'></IFRAME>
</BODY>
</HTML>	
	
	
	
	
	