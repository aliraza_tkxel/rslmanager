<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/myjob_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750"" HEIGHT=""180""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim pd_name
	Dim pd_dob
	Dim pd_gender
	Dim pd_marital
	Dim pd_ethnicity
	Dim pd_religion
	Dim pd_orientation
	Dim pd_bank
	Dim pd_sortcode
	Dim pd_accnumber
	Dim pd_accname
	Dim employee_id
	Dim disable_my_updates

		employee_id = Request("employeeid")
		' only allow th4e user hm/herself to update their personal details
		If Cint(employee_id) = Cint(Session("userid")) Then
			disable_my_updates = ""
		Else
			disable_my_updates = " disabled "
		End If
 
	Call OpenDB()
	Call get_main_details()
	Call CloseDB()

	' get employee details
	Function get_main_details()

		Dim strSQL
			strSQL = 	"SELECT		E.EMPLOYEEID, " &_
						"			LEFT(E.FIRSTNAME,1) + ' ' + E.LASTNAME AS FULLNAME," &_
						"			ISNULL(CONVERT(NVARCHAR, E.DOB, 103), '') AS DOB," &_
						"			ISNULL(E.GENDER, '') AS GENDER," &_
						"			ISNULL(M.DESCRIPTION, '') AS MARITALSTATUS," &_
						"			ISNULL(ETH.DESCRIPTION, '') AS ETHNICITY," &_
						"			ISNULL(R.DESCRIPTION, ISNULL(RELIGIONOTHER,'missing')) AS RELIGION," &_ 
						"			ISNULL(S.DESCRIPTION, ISNULL(SEXUALORIENTATIONOTHER,'missing')) AS SEXUALORIENTATION," &_
						"			ISNULL(E.BANK, '') AS BANK," &_
						"			ISNULL(E.SORTCODE, '') AS SORTCODE," &_
						"			ISNULL(E.ACCOUNTNUMBER, '') AS ACCOUNTNUMBER, " &_
						"			ISNULL(E.ACCOUNTNAME, '') AS ACCOUNTNAME " &_
						"FROM		E__EMPLOYEE E " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
						"			LEFT JOIN G_MARITALSTATUS M ON E.MARITALSTATUS = M.MARITALSTATUSID"  &_
						"			LEFT JOIN G_ETHNICITY ETH ON E.ETHNICITY = ETH.ETHID " &_
						"			LEFT JOIN G_RELIGION R ON E.RELIGION = R.RELIGIONID " &_
						"			LEFT JOIN G_SEXUALORIENTATION S ON E.SEXUALORIENTATION = S.SEXUALORIENTATIONID " &_
						"WHERE		E.EMPLOYEEID = " & employee_id

		Call OpenRs (rsSet,strSQL)
			' personal details section
			pd_name			= rsSet("FULLNAME")
			pd_dob			= rsSet("DOB")
			pd_gender		= rsSet("GENDER")
			pd_marital		= rsSet("MARITALSTATUS")
			pd_ethnicity	= rsSet("ETHNICITY")
			pd_religion		= rsSet("RELIGION")
			pd_orientation	= rsSet("SEXUALORIENTATION")
			pd_bank			= rsSet("BANK")
			pd_sortcode		= rsSet("SORTCODE")
			pd_accnumber	= rsSet("ACCOUNTNUMBER")
			pd_accname		= rsSet("ACCOUNTNAME")
		Call CloseRs(rsSet)

	End Function
%>
<html>
<head>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<body>
    <table width="750" style="height: 180px; border-right: SOLID 1PX #133E71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" width="70%" height="100%">
                <table width="100%" style="height: 100%; border: SOLID 1PX #133E71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td class='TITLE'>
                            Name
                        </td>
                        <td width="160">
                            <%=pd_name%>
                        </td>
                        <td class='TITLE'>
                            Bank
                        </td>
                        <td width="160">
                            ********
                        </td>
                        <td width="160" style='display: none'>
                            <%=pd_bank%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            DOB
                        </td>
                        <td>
                            <%=pd_dob%>
                        </td>
                        <td class='TITLE'>
                            Sort Code
                        </td>
                        <td width="160">
                            ********
                        </td>
                        <td style='display: none'>
                            <%=pd_sortcode%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Gender
                        </td>
                        <td>
                            <%=pd_gender%>
                        </td>
                        <td class='TITLE'>
                            Acc Number
                        </td>
                        <td width="160">
                            ********
                        </td>
                        <td style='display: none'>
                            <%=pd_accnumber%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Marital Status
                        </td>
                        <td width="160">
                            <%=pd_marital%>
                        </td>
                        <td class='TITLE'>
                            Acc Name
                        </td>
                        <td width="160">
                            ********
                        </td>
                        <td style='display: none'>
                            <%=pd_accname%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Ethnicity
                        </td>
                        <td>
                            <%=lEFT(pd_ethnicity, 25)%>
                        </td>
                        <td class='TITLE'>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Religion
                        </td>
                        <td>
                            <%=pd_religion%>
                        </td>
                        <td class='TITLE'>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Sexual Orientation
                        </td>
                        <td>
                            <%=pd_orientation%>
                        </td>
                        <td class='TITLE'>
                            &nbsp;
                        </td>
                        <td align="right">
                            <input type="button" name="BTN_UPDATEPD" id="BTN_UPDATEPD" <%=disable_my_updates%>
                                value="Update" class="RSLBUTTONSMALL" style="cursor: pointer" title="Update"
                                onclick="parent.update_record('/myjob/popups/ppersonaldetails.asp?employeeid=<%=employee_id%>',680,280)" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="50%">
                <%=str_holiday%>
            </td>
        </tr>
        <tr>
            <td width="30%" height="50%">
                <%=str_policy%>
            </td>
        </tr>
    </table>
</body>
</html>
