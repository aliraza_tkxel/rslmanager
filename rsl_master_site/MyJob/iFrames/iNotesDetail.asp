<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, absence_history_id, only_allow_line_manager, last_startdate
	Dim absence_status

	Call OpenDB()
	    journal_id = Request("journalid")
	    nature_id = Request("natureid")
	    employee_id = Request("employeeid")	
	Call build_journal()
	Call CloseDB()

	' only allow line manager to update holidays
	If Cint(employee_id) = Cint(Session("userid")) Or isLineManager(employee_id, Session("userid")) Then
		only_allow_line_manager = ""
	Else
		only_allow_line_manager = "disabled"
	End If

	' if its me and nature is sick I cant update
	If (Cint(employee_id) = Cint(Session("userid")) ) AND nature_id = 1 Then only_allow_line_manager = "disabled" End If

	' if its me and holiday startdate has passed then I can't update
	If ((Cint(employee_id) = Cint(Session("userid")) ) AND (CDate(last_startdate)) <= date) Then only_allow_line_manager = "disabled" End If

	' If the user is a memeber of HR then allow amends
	If Session("TeamCode") =  "HUM" Then
	    only_allow_line_manager = ""
	End If

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		ISNULL(CONVERT(NVARCHAR, N.NOTEDATE, 103) ,'') AS notedate, " &_
					"			ISNULL(N.NOTESDESC, 'N/A') AS NOTES, " &_
					"			ISNULL(ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,''),'') AS FULLNAME " &_
					"FROM		E_NOTES N " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = N.NOTEDBY  " &_
					"WHERE		N.JOURNALID = " & journal_id

		Call OpenRs (rsSet, strSQL) 

		str_journal_table = ""
		If Not rsSet.eof Then 
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			Else
				str_journal_table = str_journal_table & 	"<TR>"
			End If
				str_journal_table = str_journal_table & 	"<TD>" & rsSet("notedate") & "</TD>" &_
															"<TD>" & rsSet("notes") & "</TD>" &_
															"<TD>" & rsSet("FULLNAME") & "</TD>" &_
														"<TR>"
			rsSet.movenext()
		Wend
		End If
		Call CloseRs(rsSet)

		If cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript">

    function update_leave(int_absence_history_id) {
        var str_win
        str_win = "../pLeave.asp?employeeid=<%=employee_id%>&absencehistoryid=" + int_absence_history_id + "&natureid=<%=nature_id%>";
        window.open(str_win, "display", "width=407,height=320, left=200,top=200");
    }

    function update_sickness(int_absence_history_id) {
        var str_win
        str_win = "../pLeave.asp?employeeid=<%=employee_id%>&absencehistoryid=" + int_absence_history_id + "&natureid=<%=nature_id%>";
        window.open(str_win, "display", "width=407,height=350, left=200,top=200");
    }

</script>
<body class='TA'>
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse" border="0">
        <thead>
            <tr>
                <td colspan="3" align="right">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1PX SOLID" width="17%">
                    Date
                </td>
                <td style="border-bottom: 1PX SOLID" width="50%">
                    Notes
                </td>
                <td style="border-bottom: 1PX SOLID" width="33%">
                    Created By
                </td>
            </tr>
            <tr style='height: 7px'>
                <td colspan="3">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
