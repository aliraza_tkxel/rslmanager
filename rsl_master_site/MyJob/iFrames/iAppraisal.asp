<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim item_id, itemnature_id, title, customer_id, employee_id, path, lstAction, nature_id, action, uploadstatus, Notes

	Call OpenDB()

    ' check to see whether page is coming back from uploader.vb after upload of file
    uploadstatus= request("hdupload")
	path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then
		Call get_querystring()
    End If

	Call CloseDB()

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		txt_notes = replace(Request.form("txt_notes"),"'", "''")
        item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		employee_id = Request("employeeid")
        action = Request("submit")

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Tenancy Agreement</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">

    var FormFields = new Array();
    FormFields[0] = "txt_NOTES|Notes|TEXT|N"

    function redirect() {
        var employee_id = "<%=employee_id%>"
        parent.location.href = "iEmployeeJournal.asp?employeeid=" + employee_id
        parent.parent.document.getElementById("div8").style.display = "block";
        parent.parent.document.getElementById("div9").style.display = "none";
        parent.parent.document.getElementById("img8").src = "Images/item-1-open.gif"
        parent.parent.document.getElementById("img9").src = "Images/item-2-previous.gif"
        parent.parent.document.getElementById("img10").src = "Images/item-3-closed.gif"
        parent.parent.frm_erm.location.reload();
        parent.parent.RSLFORM.reset();
        parent.parent.frm_nature.location.href = "../blank.asp";
    }

    function pause(milliseconds) {
        var dt = new Date();
        while ((new Date()) - dt <= milliseconds) { /* Do nothing */ }
    }

    function save_form() {
        if (!checkForm()) return false;
        result = checkFileUpload(RSLFORM, 'pdf');
        if (!result) return false;
        document.RSLFORM.btnSave.disabled = "true";
        document.getElementById("trMsg").style.display = "block";
        document.RSLFORM.target = "bottom_frame";
        document.RSLFORM.action = "iAppraisal_srv.asp";
        document.RSLFORM.submit();
    }

    var set_length = 4000;

    function countMe() {
        var str = event.srcElement
        var str_len = str.value.length;
        if (str_len >= set_length) {
            str.value = str.value.substring(0, set_length - 1);
        }
    }

    function checkUploadStatus() {
        var uploadstatus = "<%=uploadstatus%>"
        // if page is coming from uploader.vb, then uploadstatus will have value of 1
        if (uploadstatus == '1') {
            redirect();
        }
    }

    function Add_Document(appraisalId) {
        document.RSLFORM.encoding = "multipart/form-data";
        document.RSLFORM.target = "bottom_frame";
        document.RSLFORM.keycriteria.value = "APPRAISALHISTORYID=" + appraisalId;
        document.RSLFORM.action = "/DBFileUpload/uploader.aspx";
        document.RSLFORM.submit();
    }

    function getFileExtension(filePath) { //v1.0
        fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length) : filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.length));
        return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
    }

    function checkFileUpload(form, extensions) { //v1.0
        document.MM_returnValue = true;
        if (extensions && extensions != '') {
            for (var i = 0; i < form.elements.length; i++) {
                field = form.elements[i];
                if (field.type.toUpperCase() != 'FILE') continue;
                if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
                    ManualError('img_DOCUMENTFILE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                    document.MM_returnValue = false; field.focus();
                    return false;
                    break;
                }
                if (field.value == '') {
                    ManualError('img_DOCUMENTFILE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                    document.MM_returnValue = false; field.focus();
                    return false;
                    break;
                }
            }
        }
        ManualError('img_DOCUMENTFILE', '', 3);
        return true;
    }

</script>
<body class='TA' onload="checkUploadStatus();">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style='height: 7px'>
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style='overflow: hidden; width: 300px' onkeyup="countMe()" class='textbox200'
                    rows="4" name="txt_NOTES" id="txt_NOTES" cols="20"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="NOTES" />
            </td>
        </tr>
        <tr>
            <td>
                Upload
            </td>
            <td>
                <input type="file" size="34" name="rslfile" class="RSLButton" style='background-color: white;
                    color: black' />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DOCUMENTFILE" id="img_DOCUMENTFILE" width="15px"
                    height="15px" border="0" alt="DOCUMENT FILE" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                &nbsp;
                <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
                <input type="button" value="Save" class="RSLButton" title='Save' onclick="save_form()"
                    name="btnSave" id="btnSave" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr id="trMsg" style="display: none">
            <td>
            </td>
            <td>
                Document Upload in Process. Please Wait ...
            </td>
        </tr>
        <tr id="trSuccess" style="display: none">
            <td>
            </td>
            <td>
                Document Uploaded Successfully.
            </td>
        </tr>
    </table>
    <!--Start of hidden fields for document upload tool-->
    <input type="hidden" name="itemid" value="<%=item_id%>" />
    <input type="hidden" name="title" value="<%=title%>" />
    <input type="hidden" name="employeeid" value="<%=employee_id%>" />
    <input type="hidden" name="natureid" value="<%=itemnature_id%>" />
    <input type="hidden" name="hid_Action" value="insert" />
    <input type="hidden" name="keycriteria" value="" />
    <input type="hidden" name="updatefields" value="" />
    <input type="hidden" name="filefield" value="DOCUMENTFILE" />
    <input type="hidden" name="filename" value="DOCFILE" />
    <input type="hidden" name="tablename" value="E_APPRAISAL" />
    <input type="hidden" name="connectionstring" value="connRSL" />
    <!--End of hidden fields for document upload tool-->
    </form>
    <iframe src="/secureframe.asp" name="bottom_frame" width="400" height="100" style='display: none'>
    </iframe>
</body>
</html>
