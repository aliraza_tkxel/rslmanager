<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim employee_id, item_id, itemnature_id, status_id, str_title, path, title, fullname

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")

	If path  = "" Then path = 0 End If
	If path Then
		' write new record
		Call OpenDB()
		Call new_record()
		Call CloseDB()
	Else
		' these fields are used in the form tag of this page
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
	End If

	Function new_record()

		Dim strSQL, journal_id

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")

		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO E_JOURNAL (EMPLOYEEID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE) " &_
					"VALUES (" & Session("userid") & ", " & item_id & ", " & itemnature_id & ", 1, '" & Date & "', '" & title & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value

		strSQL = 	"INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, REASON) " &_
					"VALUES (" & journal_id & ", 1 , 1, '" & Date & "', " & Session("userid") & ", '" & Request.Form("txt_STARTDATE") & "', '" & Request.Form("txt_RETURNDATE") & "', 0, '" & title & "')"
		set rsSet = Conn.Execute(strSQL)

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">
	
	var FormFields = new Array();
	    FormFields[0] = "txt_STARTDATE|Start Date|DATE|Y"
	    FormFields[1] = "txt_RETURNDATE|Return Date|DATE|Y"
	    FormFields[2] = "txt_RECORDEDBY|Recorded By|TEXT|Y"
	    FormFields[3] = "txt_NOTES|Notes|TEXT|N"
	
	function save_form(){	
		if (!checkForm()) return false;
		document.RSLFORM.hid_go.value = 1;
		document.RSLFORM.submit();	
	}


	function return_data(){	
		if (<%=path%> == 1)	{
			parent.document.getElementById("div9").style.display = "none";
			parent.frm_erm.location.reload();
			parent.frm_nature.location.href = "../blank.asp";
			parent.swap_item_div(8);
			parent.document.getElementById("div8").style.display = "block";
			parent.RSLFORM.reset();
		}
	}
    </script>
</head>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="0" border="0">
        <tr>
            <td>
                Start Date
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_STARTDATE" id="txt_STARTDATE" value="<%=Date%>"
                    title="START DATE" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                    border="0" alt="START DATE" />
            </td>
        </tr>
        <tr>
            <td>
                Return Date
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_RETURNDATE" id="txt_RETURNDATE" title="RETURN DATE" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RETURNDATE" id="img_RETURNDATE" width="15px" height="15px"
                    border="0" alt="RETURN DATE" />
                Anticipated
            </td>
        </tr>
        <tr>
            <td>
                Recorded By
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                    readonly="readonly" title="RECORDED BY" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                    border="0" alt="RECORDED BY" />
            </td>
        </tr>
        <tr>
            <td>
                Notes
            </td>
            <td>
                <textarea style='overflow: hidden' rows="19" cols="20" class="textbox200" name="txt_NOTES"
                    id="txt_NOTES"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="NOTES" />
                <input type="button" value="Save" class="RSLButton" onclick="save_form()" name="button"
                    title="Save" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
</body>
</html>
