<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
    Dim employee_id, item_id, itemnature_id, status_id, str_title, path, title, holtype, notes, Leave_Hrs, emp_year_sdate, emp_year_edate, holiday_rule
	Dim Result, return_date
    Dim new_history_id

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")
	employee_id = Request("employeeid")

	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT STARTDATE,ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(" & employee_id & ")"
	'response.Write (SQL)
	
	Call OpenRs(rsDays, SQL)
	    emp_year_sdate = rsDays("STARTDATE")
	    emp_year_edate = rsDays("ENDDATE")
	Call CloseRs(rsDays)

	If path = "" Then path = 0 End If
	If path Then
		Call OpenDB()
		Call new_record()
		Call CloseDB()
	Else
		' these fields are used in the form tag of this page
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		employee_id = Request("employeeid")
		holiday_rule = Request("hrule")
	End If

	Function new_record()

		Dim strSQL, journal_id, status_id, action_id , Leave_Hrs ,AL_HRS

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		employee_id = Request("employeeid")

		' set status id
		If itemnature_id = 1 Then
			status_id = 1
			action_id = 1
		Else
			status_id = 3
			action_id = 3
		End If

		title = Replace(Request("title"),"'", "''")
		holtype = request.form("holtype")
		notes = Replace(request.form("txt_NOTES"),"'", "''")

		' Function to check any existing leaves on the same date(s)
		strSQL= "SELECT dbo.E_CHECK_EXISTINGLEAVE("& employee_id &",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_RETURNDATE"),1) & "'," & Request.Form("txt_DURATION") & ", '" & holtype & "')  AS RESULT"
		
		Call OpenRs(rsExistingLeave, strSQL)
	        RESULT = rsExistingLeave("RESULT")
	    Call CloseRs(rsExistingLeave)

        ' If no record found, then insert in tables
		If RESULT = 0 Then

		    strSQL = 	"SET NOCOUNT ON;" &_
					    "INSERT INTO E_JOURNAL (EMPLOYEEID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE) " &_
					    "VALUES (" & employee_id & ", " & item_id & ", " & itemnature_id & ", " & status_id & ", '" & Now & "', '" & title & "');" &_
					    "SELECT SCOPE_IDENTITY() AS JOURNALID;"
		    Set rsSet = Conn.Execute(strSQL)
		    journal_id = rsSet.fields("JOURNALID").value

		    Leave_Hrs = 0

		    ' RUN THE PROC TO CONVERT THE LEAVE DAYS INTO LEAVE HOURS
		    AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_RETURNDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
	        Call OpenRs (rsLeave_Hrs, AL_HRS)
		        If not rsLeave_Hrs.eof Then
			        Leave_Hrs = rsLeave_Hrs("LEAVE_IN_HRS")
			    Else
		            Leave_Hrs = 0
		        End If
		    Call CloseRS(rsLeave_Hrs)
		    Response.Write(Leave_Hrs)
		    ' Single Day Check
            If(Request.Form("radType")="single") then
            ' For Half day 
             if(Request.Form("txt_DURATION")<1) then
                return_date= DateAdd("h",4,FormatDateTime(Request.Form("txt_STARTDATE"),1)&" 09:00:00AM" )
             else 
                return_date= FormatDateTime(Request.Form("txt_RETURNDATE"),1)
             end if   
              
            Else 'Multi Day Check
                return_date= FormatDateTime(Request.Form("txt_RETURNDATE"),1)     
            End If
		    strSQL = 	"SET NOCOUNT ON;" &_
                        "INSERT INTO E_ABSENCE " &_
					    "(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, DURATION_HRS, REASON, HOLTYPE, NOTES) " &_
					    "VALUES (" & journal_id & ", " & status_id & " , " & action_id & ", '" & Now & "', " & Session("userid") & ", '" & FormatDateTime(Request.Form("txt_STARTDATE"),1) &"','"& return_date & "', " & Request.Form("txt_DURATION") & " ," & Leave_Hrs & ", '" & Replace(title,"'", "''") & "', '" & holtype & "', '" & Replace(notes,"'", "''") & "')" &_
                        " SELECT SCOPE_IDENTITY() AS HISTORYID;"
           'response.Write(strSQL)
            Set rsSet = Conn.Execute(strSQL)
		    new_history_id = rsSet.fields("HISTORYID").value
		    rsSet.close()
		    Set rsSet = Nothing
            ' response.Write(strSQL)
            ' response.End
            Call sendEmail(journal_id,new_history_id,employee_id)
           
		End If

	End Function

    Function sendEmail(journalid,new_history_id,employee_id)

    SQL = "SELECT STARTDATE, RETURNDATE, DURATION " &_ 
          "FROM E_ABSENCE " &_
          "WHERE ABSENCEHISTORYID = " & new_history_id
    Call OpenRs(rsEmpRec, SQL)
    If NOT rsEmpRec.EOF Then
        sdate = rsEmpRec("STARTDATE")
        edate =  rsEmpRec("RETURNDATE")
        duration =  rsEmpRec("DURATION")
    End If
    Call CloseRs(rsEmpRec)

    SQL = "SELECT WORKEMAIL " &_
          "FROM dbo.E_JOBDETAILS JD " &_
          "INNER JOIN dbo.E__EMPLOYEE E ON JD.LINEMANAGER = E.EMPLOYEEID " &_
          "INNER JOIN dbo.E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
          "WHERE JD.EMPLOYEEID = " & employee_id

	Call OpenRs(rsEmail, SQL)

	If NOT rsEmail.EOF And NOT rsEmail.BOF Then
		' send email
		Dim strRecipient, emailSubject, strSender
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

        SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT WHERE EMPLOYEEID = " & employee_id
        Call OpenRs(rsSenderEmail, SQL)

        If NOT rsSenderEmail.EOF And NOT rsSenderEmail.BOF Then 
            strSender=(rsSenderEmail.Fields.Item("WORKEMAIL").Value)
        End if
        Call CloseRs(rsSenderEmail)

        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        emailbody = fullname & " has requested " & duration &  " day(s) leave for the period " & sdate & " to " & edate & ". Please log in to RSLmanager to acknowledge/approve/decline this request."
        emailSubject = "Leave Request"

        ON ERROR RESUME NEXT
        Set iMsg.Configuration = iConf
        iMsg.To = strRecipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send

        If Err.number <> 0 Then
            If strRecipient = "" Or IsNull(strRecipient) Then
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed due to manager email address not provided, so please inform the concerned person.');</script>") 
            Else  
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed, so please inform the concerned person.');</script>")
            End If  
            Err.Clear()
            ON ERROR GOTO 0
        End If

	End If
	Call CloseRs(rsEmail)

    End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctionsMyJob.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "txt_STARTDATE|Startd Date|DATE|Y"
        FormFields[1] = "txt_RETURNDATE|Return Date|DATE|Y"



        function save_form() {
            if (!checkForm()) return false;
            if (document.getElementById("txt_DURATION").value == 0) {
                alert("Duration of leaves can't be zero.Please add your working hours or contact administrator or manager")
                return false;
            }
            var nature_id = "<%=itemnature_id%>";
            if (document.getElementById("radSingle").checked == true) {
                document.getElementById("txt_RETURNDATE").value = document.getElementById("txt_STARTDATE").value
                var dSDate = new Date(JSlong_date(document.getElementById("txt_STARTDATE").value));
                var dSDate_hid = new Date(JSlong_date(document.getElementById("hid_STARTDATE").value));
                if (dSDate < dSDate_hid && nature_id == 2) {
                    alert('You cannot book leave in previous holiday periods.');
                    return false;
                }
            }
            if (document.getElementById("radMulti").checked == true) {
                var dSDate = new Date(JSlong_date(document.getElementById("txt_STARTDATE").value));
                var dEDate = new Date(JSlong_date(document.getElementById("txt_RETURNDATE").value));
                var dSDate_hid = new Date(JSlong_date(document.getElementById("hid_STARTDATE").value));
                var dEDate_hid = new Date(JSlong_date(document.getElementById("hid_ENDDATE").value));
                var hrule = document.getElementById("hid_hrule").value;

                if (dSDate > dEDate) {
                    alert('Invalid start/end date. End date should be greater than start date.');
                    return false;
                }

                // Meridian East Rule (based on 1st April)
                if (hrule == '6' || hrule == '7' || hrule == '8' || hrule == '9') {
                    var me_date = '1/4/' + dSDate.getFullYear();
                    var dME_Date = new Date(JSlong_date(me_date));
                    if ((dSDate < dME_Date) && (dEDate >= dME_Date) && (nature_id == 2)) {
                        alert('ME.You cannot book leave that overlaps two holiday periods.\n Please rebook your leave using two separate requests');
                        return false;
                    }
                }
                // BHA rule (based on Employee's Annivarsary)
                else {
                    if ((dSDate >= dSDate_hid && dSDate <= dEDate_hid) && dEDate > dEDate_hid && (nature_id == 2)) {
                        alert('You cannot book leave that overlaps two holiday periods.\n Please rebook your leave using two separate requests');
                        return false;
                    }
                }
            }
            getType();
            document.getElementById("hid_go").value = 1;
            document.RSLFORM.action = "iGeneral.asp?employeeid=<%=employee_id%>&natureid=<%=itemnature_id%>&itemid=<%=item_id%>&title=<%=title%>";
            document.RSLFORM.target = "";
            document.RSLFORM.submit();
        }


        function calLeaveDuration() {
            if (!checkForm()) return false;
            var emp = "<%= employee_id%>";
            if (document.RSLFORM.radSingle.checked == true) {
                document.getElementById("txt_RETURNDATE").value = document.getElementById("txt_STARTDATE").value
            }
            var sdate = document.getElementById("txt_STARTDATE").value;
            var edate = document.getElementById("txt_RETURNDATE").value;
            document.RSLFORM.action = "iEmpWorkDays.asp?employeeid=" + emp + "&sdate=" + sdate + "&edate=" + edate
            document.RSLFORM.target = "serverFrame"
            document.RSLFORM.submit();
        }


        function return_data() {
            if ("<%=path%>" == 1 && "<%=RESULT%>" == 0) {
                parent.document.getElementById("div9").style.display = "none";
                parent.frm_erm.location.reload();
                parent.frm_nature.location.href = "../blank.asp";
                parent.swap_item_div(8);
                parent.document.getElementById("div8").style.display = "block";
                parent.RSLFORM.reset();
            }
            if ("<%=RESULT%>" >= 1) {
                alert("You cannot book leave as you have already booked the date(s)");
                return false;
            }
        }


        function check_date() {
            if (!checkForm()) return false;
            calLeaveDuration();
        }
    </script>
</head>
<body onload="return_data()">
    <form name="RSLFORM" id="RSLFORM" method="post" action="iGeneral.asp?employeeid=<%=employee_id%>&natureid=<%=itemnature_id%>&itemid=<%=item_id%>&title=<%=title%>">
    <div id="SBS">
    </div>
    <table cellpadding="1" cellspacing="0" border="0">
        <tr>
            <td>
                Requested
            </td>
            <td colspan="3">
                <input type="radio" id="radSingle" name="radType" value="single" checked="checked"
                    onclick='{document.getElementById("endText").style.visibility="hidden";workDays()}' />
                Single
                <input type="radio" id="radMulti" name="radType" value="multiple" onclick='{document.getElementById("endText").style.visibility="visible";workDays()}' />
                Multiple
            </td>
        </tr>
        <tr>
            <td>
                Start Date
            </td>
            <td colspan="3">
                <input type="text" value="<%=Date%>" name="txt_STARTDATE" id="txt_STARTDATE" class="textbox100"
                    maxlength="10" onblur ="check_date()" />
                    <!--Displaying Calendar Image-->
                   
                    <a href="javascript:;" class="iagManagerSmallBlue"  onclick="YY_Calendar('txt_STARTDATE',180,35,'de','#FFFFFF','#133E71')" tabindex="1">
                        <img alt="calender_image"  src="../images/calendar.png" width="15px" height="14px" /></a>
                     <!--End Displaying Calendar Image-->
                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                    border="0" alt="STARTDATE" />
                    <!-- Calendar Div Start Here--> 
                    <div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                    top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                </div>
                <!-- Calendar Div End Here--> 
                &nbsp;full<input type="radio" id="radStartFull" name="radStart" value="days" onclick="workDays()"
                    checked="checked" />
                am<input type="radio" id="radStartAM" name="radStart" value="morning" onclick="workDays()" />
                pm<input type="radio" id="radStartPM" name="radStart" value="afternoon" onclick="workDays()" />
            </td>
        </tr>
        <tr id="endText" style="visibility: hidden">
            <td>
                End Date
            </td>
            <td colspan="3">
                <input type="text" value="<%=Date%>" name="txt_RETURNDATE" id="txt_RETURNDATE" class="textbox100"
                    maxlength="10" onblur="check_date()" />
                    <!--Displaying Calendar Image-->
                   
                    <a href="javascript:;" class="iagManagerSmallBlue"  onclick="YY_Calendar('txt_RETURNDATE',180,35,'de','#FFFFFF','#133E71')" tabindex="1">
                        <img alt="calender_image"  src="../images/calendar.png" width="15px" height="14px" /></a>
                         <!--End Displaying Calendar Image-->
           <img src="/js/FVS.gif" name="img_RETURNDATE" id="img_RETURNDATE" width="15px" height="15px"
                    border="0" alt="RETURN DATE" />
                       <!-- Calendar Div Start Here--> 
                    <div id='Calendar2' style='background-color: white; position: absolute; left: 1px;
                    top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                </div>
                <!-- Calendar Div End Here--> 
                
                &nbsp;full<input type="radio" id="radEndFull" name="radEnd" value="full" onclick="workDays()"
                    checked="checked" />
                am<input type="radio" id="radEndAM" name="radEnd" value="morning" onclick="workDays()" />
                pm<input type="radio" id="radEndPM" name="radEnd" value="afternoon" onclick="workDays()" />
            </td>
        </tr>
        <tr>
            <td>
                Duration
            </td>
            <td colspan="2">
                <input name="txt_DURATION" id="txt_DURATION" type="text" class="TEXTBOX100" readonly="readonly"
                    value="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DURATION" id="img_DURATION" width="15px" height="15px"
                    border="0" alt="DURATION" />
            </td>
        </tr>
        <tr>
            <td>
                Recorded By
            </td>
            <td colspan="2">
                <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                    readonly="readonly" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                    border="0" alt="RECORDED BY" />
            </td>
        </tr>
        <tr>
            <td>
                Notes
            </td>
            <td colspan="2">
                <textarea style="overflow: hidden" rows="9" cols="20" class="textbox200" name="txt_NOTES"
                    id="txt_NOTES"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="NOTES" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value="Save"
                    class="RSLButton" style="cursor: pointer" />
                <input type="hidden" name="dates" id="dates" value="<%=BANK_HOLIDAY_STRING%>" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="hidden" name="hid_STARTDATE" id="hid_STARTDATE" value="<%=emp_year_sdate%>" />
                <input type="hidden" name="hid_ENDDATE" id="hid_ENDDATE" value="<%=emp_year_edate%>" />
                <input type="hidden" name="hid_non_work_days" id="hid_non_work_days" value="0" />
                <input type="hidden" name="holtype" id="holtype" />
                <input type="hidden" name="hid_hrule" id="hid_hrule" value="<%=holiday_rule%>" />
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" id="serverFrame" width="6px" height="6px" style="display:none"></iframe>
    
</body>
</html>
