function initSwipeMenu(numberSelects) {
	//this code is used to set the num of selects to hide
	//do not modify the code as it used elsewhere...
	if (isNaN(numberSelects)) 
		var numSelectsToHide = 2;
	else
		var numSelectsToHide = numberSelects;	
	iSelectsRef = document.all.tags("SELECT");
	maxLoopMenu = 0;
	
	if (iSelectsRef.length){
		if (iSelectsRef.length < numSelectsToHide)
			maxLoopMenu = iSelectsRef.length;
		else
			maxLoopMenu = numSelectsToHide;
		}
		
	hMenu1Att = new Array();
	hMenu1Att["width"] = 95,
	hMenu1Att["height"] = 16,
	hMenu1Att["align"] = "left";
	hMenu1Att["font"] = "Verdana";
	hMenu1Att["font_size"] = 10;
	hMenu1Att["font_weight"] = "lighter";
	hMenu1Att["bg_off"] = "#133E71";
	hMenu1Att["bg_on"] = "#FFFFFF";
	hMenu1Att["fg_off"] = "#FFFFFF";
	hMenu1Att["fg_on"] = "#133E71";
	hMenu1Att["border"] = 1;
	hMenu1Att["border_color"] = "#FFFFFF";

	hMenu1SubAtt = new Array();
	hMenu1SubAtt["width"] = 95,
	hMenu1SubAtt["height"] = 15,
	hMenu1SubAtt["align"] = "left";
	hMenu1SubAtt["font"] = "Verdana";
	hMenu1SubAtt["font_size"] = 10;
	hMenu1SubAtt["font_weight"] = "lighter";
	hMenu1SubAtt["bg_off"] = "#133E71";
	hMenu1SubAtt["bg_on"] = "#FFFFFF";
	hMenu1SubAtt["fg_off"] = "#FFFFFF";
	hMenu1SubAtt["fg_on"] = "#133E71";
	hMenu1SubAtt["border"] = 1;
	hMenu1SubAtt["border_color"] = "#133E71";

hMenu1 = new wipeMenu("hMenu1", "h", hMenu1Att, hMenu1SubAtt);

hMenu1.addMain("Menu1", true, "Modules", "#");
hMenu1.addSub("Menu1Sub1", "Menu1", "Business", "../iagBusinessManager/iagBusinessManager.asp");
hMenu1.addSub("Menu1Sub2", "Menu1", "Contacts", "../iagContactManager/iagContactManager.asp");
hMenu1.addSub("Menu1Sub3", "Menu1", "Customer", "../iagCRM/iagCRM.asp");
hMenu1.addSub("Menu1Sub4", "Menu1", "Diary", "../iagDiary/Default.asp");
hMenu1.addSub("Menu1Sub5", "Menu1", "Documents", "../iagDocumentManager/iagDocumentManager.asp");
hMenu1.addSub("Menu1Sub6", "Menu1", "Email", "javascript: openInBlankWindow ( 'https://mail.nownetwork.org.uk', '', '' ) "); 
hMenu1.addSub("Menu1Sub7", "Menu1", "Finance", "../iagFinance/iagFinance.asp");
hMenu1.addSub("Menu1Sub8", "Menu1", "News", "../iagNews/iagNews.asp");
hMenu1.addSub("Menu1Sub9", "Menu1", "My Job", "../iagMyJob/iagMyJob.asp");

//hMenu1SubAtt["width"] = 110,hMenu1SubAtt["height"] = 15,	hMenu1SubAtt["align"] = "left";

	hMenu1.addMain("Menu2", true, "Profile", "#");
	hMenu1.addSub("Menu2Sub1", "Menu2", "My Role", "../iagMyJob/MyRole.asp");
	hMenu1.addSub("Menu2Sub2", "Menu2", "Skills", "#");
	hMenu1.addSub("Menu2Sub3", "Menu2", "Training", "#");
	hMenu1.addSub("Menu2Sub4", "Menu2", "Monitor", "#");
	hMenu1.addSub("Menu2Sub5", "Menu2", "Rating", "#");
	//hMenu1.addSub("Menu2Sub5", "Menu2", "", "#");
	//xMenu2End
	//xMenu3
	hMenu1.addMain("Menu3", true, "My Details", "#");
	hMenu1.addSub("Menu3Sub1", "Menu3", "Details", "/iagMyJob/TMUserDisplay.asp");
	hMenu1.addSub("Menu3Sub2", "Menu3", "Next of kin", "/iagMyJob/FurtherInformation.asp");
	hMenu1.addSub("Menu3Sub3", "Menu3", "Amend Details", "/iagMyJob/TMUserAmend.asp");
	hMenu1.addSub("Menu3Sub4", "Menu3", "Amend NoK", "/iagMyJob/FurtherInfo.asp");
	//hMenu1.addSub("Menu3Sub8", "Menu3", "", "#");
	//xMenu3End
	//xMenu4
	hMenu1.addMain("Menu4", true, "Holidays", "#");
	hMenu1.addSub("Menu4Sub1", "Menu4", "Absence", "#");
	if ('<MM:BeginLock translatorClass="MM_ASPSCRIPT" type="DynamicVariables" depFiles="" orig="%3C%25=Session(%22svUserType%22)%25%3E" ><MM_DYNAMIC_CONTENT SOURCE=Session BINDING="svUserType" DYNAMICDATA=1></MM_DYNAMIC_CONTENT><MM:EndLock>' == '3'){
		hMenu1.addSub("Menu4Sub2", "Menu4", "Approve leave", "/iagMyJob/ARLeave.asp");
		hMenu1.addSub("Menu4Sub3", "Menu4", "Display leave", "/iagMyJob/DisplayLeave.asp");
		hMenu1.addSub("Menu4Sub4", "Menu4", "Request Hols", "/iagMyJob/TMLeaveRequest.asp");
		hMenu1.addSub("Menu4Sub4", "Menu4", "Year End", "/iagMyJob/YearEnd.asp");
		}	
	else {
		hMenu1.addSub("Menu4Sub2", "Menu4", "Display leave", "/iagMyJob/DisplayLeave.asp");
		hMenu1.addSub("Menu4Sub3", "Menu4", "Request Hols", "/iagMyJob/TMLeaveRequest.asp");
		}	
	
	hMenu1.addMain("Menu5", true, "Expenses", "#");
	hMenu1.addSub("Menu5Sub1", "Menu5", "Expenses", "/iagMyJob/TMExpenses.asp");
	hMenu1.addSub("Menu5Sub2", "Menu5", "Time sheet", "#");
	
	hMenu1.addMain("Menu6", true, "Goals", "#");
	hMenu1.addSub("Menu6Sub1", "Menu6", "Actions", "#");
	hMenu1.addSub("Menu6Sub2", "Menu6", "Milestones", "#");

//hMenu1.addSub("Menu4Sub3", "Menu4", "", "#");
//xMenu4End

//xMenu7

//xMenu7End

hMenu1.buildMenu(150,99,0);

}