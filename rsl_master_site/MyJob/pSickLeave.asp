<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim absence_history_id		' the historical id of the absence record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this absence used to render page, either sickness or otherwise QUERYSTING
	Dim is_sickness				' determines whether this is a sickness record - uses nature_id
	Dim a_status				' status of absence reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of absence reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim startdate
	Dim returndate
	Dim certno
	Dim notes
	Dim drname
	Dim reason
	Dim duration
	Dim employeeid
	Dim isEmployeeOnly
	Dim holType
    Dim duration_hrs
    Dim lst_reason
    Dim reason_id
    Dim rdate

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")

	absence_history_id 	= Request("absencehistoryid")
	nature_id 			= Request("natureid")
	employeeid			= Request("employeeid")
    reason_id 			= Request("reason_id")	

	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT STARTDATE, ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(" & employeeid & ")"
    Call OpenDB()
	Call OpenRs(rsDays, SQL)
        If NOT (rsDays.EOF) Then
	        emp_year_sdate = rsDays("STARTDATE")
	        emp_year_edate = rsDays("ENDDATE")
        End If
	Call CloseRs(rsDays)

	If nature_id = 1 Then is_sickness = True Else is_sickness = False End If

	' only allow line manager to approve holidays and sick leave
	If isLineManager(employeeid, Session("userid")) Then
		isEmployeeOnly = " "
	Else
		isEmployeeOnly = " and EMPLOYEEONLY = 1 "
	End If

	' begin processing
	Call OpenDB()

	If path = "" Then path = 0 End If ' initial entry to page

	If path = 0 Then
		If is_sickness Then
			Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (1, 2, 20)", "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", "onchange='action_change()'")
            Call BuildSelect(lst_reason, "sel_REASON", "E_ABSENCEREASON", "SID, DESCRIPTION", "DESCRIPTION", "Please Select", reason_id, Null, "textbox200", "style='display:block'")
		Else
			Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (3, 4, 5, 20) " & isEmployeeOnly, "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", null)
		End If
		Call get_record()
	Elseif path = 1 Then
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT 	ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.RETURNDATE, 103) ,'') AS RETURNDATE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS," &_
					"			ISNULL(A.CERTNO, 'N/A') AS CERTNO," &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES," &_
					"			ISNULL(A.DRNAME, 'N/A') AS DRNAME," &_
					"			ISNULL(A.REASON, 'N/A') AS REASON, " &_
					"			ISNULL(A.HOLTYPE, 'N/A') AS HOLTYPE, " &_
					"		    CASE " &_
                    "           WHEN A.returndate IS NULL THEN  " &_
		            "	            dbo.Emp_sickness_duration(J.employeeid, A.startdate, Getdate(), A.journalid)  " &_
		            "           WHEN A.startdate = A.returndate  AND A.duration > 1 AND dbo.Emp_sickness_duration(J.employeeid, A.startdate, A.returndate, A.journalid) !=0 THEN  " &_
		            "	            dbo.Emp_sickness_duration(J.employeeid, A.startdate, A.returndate, A.journalid) " &_
                    "           ELSE  " &_
		            "	            A.duration  " &_
                    "           END                                              AS DURATION " &_
                    "           , DURATION_HRS " &_
					"FROM 		E_ABSENCE A " &_
                    "           INNER JOIN  E_JOURNAL J ON A.JOURNALID = J.JOURNALID "  &_
					"			LEFT JOIN E_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID " &_
					"WHERE 		ABSENCEHISTORYID = " & absence_history_id

		Call OpenRs(rsSet, strSQL)

		a_status 	= rsSet("STATUS")
		startdate	= rsSet("STARTDATE")
		returndate 	= rsSet("RETURNDATE")
		certno		= rsSet("CERTNO")
		notes 		= rsSet("NOTES")
		drname 		= rsSet("DRNAME")
		reason 		= rsSet("REASON")
		duration 	= rsSet("DURATION")
		holType		= rsSet("HOLTYPE")
		duration_hrs 	= rsSet("DURATION_HRS")

		Call CloseRs(rsSet)

	End Function

	Function new_record()

		strSQL = "SELECT J.JOURNALID,J.EMPLOYEEID FROM E_ABSENCE A INNER JOIN E_JOURNAL J ON J.JOURNALID=A.JOURNALID WHERE ABSENCEHISTORYID = " & absence_history_id
		Call OpenRs(rsSet, strSQL)
		    journalid = rsSet("JOURNALID")
		    employee_id = rsSet("EMPLOYEEID")
		Call CloseRS(rsSet)

		a_status 	= Request.Form("sel_ACTION")
		actionid 	= Request.Form("sel_ACTION")
		startdate 	= DateValue(Request.Form("txt_STARTDATE"))

        If isdate(Request.Form("txt_RETURNDATE")) Then
            returndate 	= DateValue(Request.Form("txt_RETURNDATE"))
            rdate = "'" & FormatDateTime(returndate,1) & "'"
        Else
            rdate="NULL"
        End If

		duration 	= Request.form("txt_DURATION")
		certno 		= Replace(Request.form("txt_CERTNO"),"'", " ")
		notes 		= Replace(Request.form("txt_NOTES"),"'", " ")
		drname 		= Replace(Request.form("txt_DRNAME"),"'", " ")
		reason 		=  Request.Form("sel_REASON")
		holtype 	= request.form("holtype")

		'Leave_Hrs = 0

		' RUN THE PROC TO CONVERT THE LEAVE DAYS INTO LEAVE HOURS
		'AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
		'AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_RETURNDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
	    'Call OpenRs (rsLeave_Hrs, AL_HRS)

		'    if not rsLeave_Hrs.eof then
		'	    Leave_Hrs = rsLeave_Hrs("LEAVE_IN_HRS")
		'	ELSE
		'       Leave_Hrs = 0
		'    end if
				
		'Call CloseRS(rsLeave_Hrs)

		strSQL = 	"INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, REASONID, NOTES, CERTNO, DRNAME, HOLTYPE) " &_
					"VALUES (" & journalid & ", " & a_status & ", " & actionid & ", '" & FormatDateTime(Date,1) & "', " & Session("userid") & ", '" & FormatDateTime(startdate,1) & "', " & rdate & ", " & Duration & ", " & reason & ", '" & notes & "', '" & certno & "', '" & drname & "', '" & Request.Form("HOLTYPE") & "')"
         set rsSet = Conn.Execute(strSQL)

		Call update_journal(journalid, a_status)

	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)
		strSQL = "UPDATE E_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)
	End Function
%>
<html>
<head>
    <title>ERM --> Update Absence</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
</head>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
<script type="text/javascript" language="JavaScript">

	var FormFields = new Array();
	    FormFields[0] = "sel_REASON|REASON|SELECT|Y"
	    FormFields[1] = "txt_STARTDATE|STARTDATE|DATE|Y"
	    FormFields[2] = "txt_RETURNDATE|RETURNDATE|DATE|Y"
	    FormFields[3] = "txt_DURATION|DURATION|TEXT|Y"
	    FormFields[4] = "txt_RECORDEDBY|RECORDEDBY|TEXT|Y"
	    FormFields[5] = "sel_ACTION|ACTION|SELECT|Y"
	    FormFields[6] = "txt_NOTES|Notes|TEXT|N"

	function save_form(){
		if (!checkForm()) return false;
        calLeaveDuration();
		getSicknessType();
        document.getElementById("hid_go").value = 1;
		document.RSLFORM.action="pSickLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>";
		document.RSLFORM.target=""
		document.RSLFORM.submit();
	}

    function action_change()
    {
        calLeaveDuration();
        // IF NOT RETURN TO WORK
        if ((RSLFORM.sel_ACTION.options[RSLFORM.sel_ACTION.selectedIndex].value) != '2')
        {
            FormFields[2] = "txt_RETURNDATE|RETURNDATE|DATE|N"
            document.getElementById("txt_RETURNDATE").disabled = true;
            document.getElementById("txt_RETURNDATE").style.background="#C0C0C0";
            document.getElementById("txt_RETURNDATE").value = "";
            document.getElementById("radEndAM").disabled = true;
            document.getElementById("radEndPM").disabled = true;
            document.getElementById("radEndFull").disabled = true;
        }
        else
        {
            FormFields[2] = "txt_RETURNDATE|RETURNDATE|DATE|Y"
            document.getElementById("txt_RETURNDATE").disabled = false;
            document.getElementById("txt_RETURNDATE").style.background="white";
            document.getElementById("radEndAM").disabled = false;
            document.getElementById("radEndPM").disabled = false;
            document.getElementById("radEndFull").disabled = false;
        }
    }

	function return_data(){
		if (<%=path%> == 1)	{
			opener.location.reload();
			window.close();
		}
		else {
            FormFields[2] = "txt_RETURNDATE|RETURNDATE|DATE|N"
            document.getElementById("txt_RETURNDATE").disabled = true;
            document.getElementById("txt_RETURNDATE").style.background="#C0C0C0";
            document.getElementById("radEndAM").disabled = true;
            document.getElementById("radEndPM").disabled = true;
            document.getElementById("radEndFull").disabled = true;
            displayDetails();
        }
	}

	// global variables
	var Eleavedur, Etype

	// initialises fields from server to be used by client
	function initFields(){
		Eleavedur = <%=duration%>;
       	Etype = new String('<%=HolType%>');
	}

	// fills page details according to holiday information
	function displayDetails(){

		var arrSplit;
		initFields();
  		if (Eleavedur > 0){
			document.getElementById("endText").style.visibility = "visible";
			}
		arrSplit = Etype.split("-");
		if (arrSplit.length > 1){
			if (arrSplit[0] == 'F')
                document.getElementById("radStartFull").checked = true
			else if (arrSplit[0] == 'M')
				document.getElementById("radStartAM").checked = true;
			else document.getElementById("radStartPM").checked = true;
			if (arrSplit[1] == 'F')
				document.getElementById("radEndFull").checked = true;
			else if (arrSplit[1] == 'M')
				document.getElementById("radEndAM").checked = true;
			else document.getElementById("radEndPM").checked = true;
			}
		else {
			if (Etype == 'F')
				document.getElementById("radStartFull").checked = true;
			else if (Etype == 'M')
				document.getElementById("radStartAM").checked = true;
			else document.getElementById("radStartPM").checked = true;
			}
	}

	function check_date(){
		if (!checkForm()) return false;
         calLeaveDuration();
	}
	
	function calLeaveDuration()
	{
	    if (!checkForm()) return false;
        var emp = "<%= employee_id%>";
	    if (emp == '')
	    {
	        emp = "<%= employeeid%>";
	    }
		var sdate = document.getElementById("txt_STARTDATE").value;
		var edate = document.getElementById("txt_RETURNDATE").value;
        var nature_id = <%=Request("natureid") %>
       	document.RSLFORM.action="iFrames/iEmpWorkDays.asp?employeeid=" + emp + "&sdate=" + sdate + "&edate=" + edate + "&natureid=" + nature_id
	    document.RSLFORM.target="serverFrame"
	    document.RSLFORM.submit();
	}
</script>
<body onload="sicknessDays();return_data();">
    <form name="RSLFORM" method="post" action="pSickLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="height: 100%">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="67">
                            <img src="Images/tab_leave.gif" width="67" height="20" alt="Leave" /></td>
                        <td height="19" width="8061"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71" width="8061">
                            <img src="images/spacer.gif" height="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71'>
                <table cellspacing="1" cellpadding="2" width="90%" align="center" border="0">
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Reason:
                        </td>
                        <td>
                            <%=lst_reason%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                border="0" alt="REASON" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="21">
                            Action
                        </td>
                        <td colspan="3" height="21">
                            <%=lst_action%>
                            <img src="/js/FVS.gif" name="img_ACTION" id="img_ACTION" width="15px" height="15px"
                                border="0" alt="ACTION" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date
                        </td>
                        <td colspan="3">
                            <input type="text" value="<%=startdate%>" name="txt_STARTDATE" id="txt_STARTDATE"
                                class="textbox100" maxlength="10" onblur="check_date()" />
                            <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                border="0" alt="START DATE" />
                            &nbsp;full<input type="radio" id="radStartFull" name="radStart" value="days" onclick="sicknessDays()"
                                checked="checked" />
                            am<input type="radio" id="radStartAM" name="radStart" value="morning" onclick="sicknessDays()" />
                            pm<input type="radio" id="radStartPM" name="radStart" value="afternoon" onclick="sicknessDays()" />
                        </td>
                    </tr>
                    <tr id="endText" style="visibility: hidden">
                        <td>
                            End Date
                        </td>
                        <td colspan="3">
                            <input type="text" value="<%=returndate%>" name="txt_RETURNDATE" id="txt_RETURNDATE"
                                class="textbox100" maxlength="10" onblur="check_date()" />
                            <img src="/js/FVS.gif" name="img_RETURNDATE" id="img_RETURNDATE" width="15px" height="15px"
                                border="0" alt="RETURN DATE" />
                            &nbsp;full<input type="radio" id="radEndFull" name="radEnd" value="full" onclick="sicknessDays()"
                                checked="checked" />
                            am<input type="radio" id="radEndAM" name="radEnd" value="morning" onclick="sicknessDays()" />
                            pm<input type="radio" id="radEndPM" name="radEnd" value="afternoon" onclick="sicknessDays()" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Duration
                        </td>
                        <td colspan="3">
                            <input name="txt_DURATION" id="txt_DURATION" type="text" class="TEXTBOX100" readonly="readonly"
                                value="<%=duration%>" />
                            <img src="/js/FVS.gif" name="img_DURATION" id="img_DURATION" width="15px" height="15px"
                                border="0" alt="DURATION" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Recorded By
                        </td>
                        <td colspan="2">
                            <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                                readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="RECORDED BY" />
                        </td>
                    </tr>
                    <% If is_sickness Then %>
                    <tr>
                        <td nowrap="nowrap">
                            Cert No:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_CERTNO" id="txt_CERTNO" value="<%=certno%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CERTNO" id="img_CERTNO" width="15px" height="15px"
                                border="0" alt="CERT NO" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Drs Name:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DRNAME" id="txt_DRNAME" value="<%=drname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DRNAME" id="img_DRNAME" width="15px" height="15px"
                                border="0" alt="DR NAME" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <% End If %>
                    <tr>
                        <td>
                            Notes
                        </td>
                        <td colspan="2">
                            <textarea rows="4" cols="20" style="overflow: hidden" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES"><%=notes%></textarea>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value="Save"
                                class="RSLButton" style="cursor: pointer" />
                            <input type="hidden" name="dates" id="dates" value="<% =BANK_HOLIDAY_STRING%>" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="holtype" id="holtype" value="<%=HolType%>" />
                            <input type="hidden" name="hid_non_work_days" id="hid_non_work_days" value="0" />
                            <input type="hidden" name="hid_STARTDATE" id="hid_STARTDATE" value="<%=emp_year_sdate%>" />
                            <input type="hidden" name="hid_ENDDATE" id="hid_ENDDATE" value="<%=emp_year_edate%>" />
                            <input type="hidden" name="hid_pagename" id="hid_pagename" value="pSickLeave" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" width="200" height="200" style="display: none"></iframe>
</body>
</html>