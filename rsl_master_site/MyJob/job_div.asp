<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
if (EmployeeID = "") then EmployeeID = -1 end if

OpenDB()
SQL = "SELECT * FROM E_PREVIOUSEMPLOYMENT WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsLoader, SQL)

SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsName, SQL)

If NOT rsName.EOF Then
	l_lastname = rsName("LASTNAME")
	l_firstname = rsName("FIRSTNAME")	
	FullName = l_firstname & " " & l_lastname

End If

strTable = "<TABLE WIDTH='100%' STYLE='BORDER: SOLID 1PX #133E71' CELLPADDING='1' CELLSPACING='2' border='0' CLASS='TAB_TABLE'>" & vbCrLf
strTable = strTable & "<thead>" & vbCrLf
	strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td width=190px><b>Employer</b></td>" & vbCrLf
		strTable = strTable & "<td width=80px><b>Start Date</b></td>" & vbCrLf
		strTable = strTable & "<td width=80px><b>End Date</b></td>" & vbCrLf
		strTable = strTable & "<td width=80px><b>Reason</b></td>" & vbCrLf
	strTable = strTable & "</tr>" & vbCrLf
	strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td colspan=4></td>" & vbCrLf
strTable = strTable & "</tr>" & vbCrLf
strTable = strTable & "</thead>" & vbCrLf
strTable = strTable & "<tbody>" & vbCrLf
if (not rsLoader.EOF) then
	while (NOT rsLoader.EOF) 
		special = rsLoader("Reason")
		titlebit = ""
		if (special <> "" AND not isNull(special)) then
			titlebit = " title='Reasons for Leaving:" & special & "'"
		end if
		strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td valign=top " & titlebit & ">" & rsLoader("EMPLOYER") & "</td>" & vbCrLf	
		strTable = strTable & "<td valign=top>" & rsLoader("STARTDATE") & "</td>" & vbCrLf	
		strTable = strTable & "<td valign=top>" & rsLoader("ENDDATE") & "</td>" & vbCrLf
		strTable = strTable & "<td valign=top>" & rsLoader("REASON") & "</td>" & vbCrLf
		strTable = strTable & "</tr>" & vbCrLf
		rsLoader.moveNext					
	wend
else
	strTable = strTable & "<tr><td colspan=3 align=center>No Previous Employers</td></tr>"
end if
strTable = strTable & "</tbody><tfoot><tr><td colspan=4></td></tr></tfoot></table>"


CloseRs(rsLoader)
CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000">
<!-- PREVIOUS EMPLOYMENT------------------------------------->
<TABLE <%= TABLE_DIMS %> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1
CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
	<TR>
		<TD ROWSPAN=2 WIDTH=70% HEIGHT=100%>
			<!--
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
				<TR valign=top>
					<TD>Start date</TD>
					<TD>End Date</TD>
					<TD>Company Name</TD>
					<TD>Reason for leaving</TD>
	  			</TR>
			</TABLE>
			//-->			
			<%=strTable%>
		</TD>
		<TD WIDTH=30% HEIGHT=50%>
			<%=str_holiday%>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=30% HEIGHT=50%>
			<%=str_policy%>
		</TD>
	</TR>
</TABLE>
</body>
</html>