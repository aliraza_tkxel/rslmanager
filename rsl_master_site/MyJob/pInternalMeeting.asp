<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim absence_history_id		' the historical id of the absence record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this absence used to render page, either sickness or otherwise QUERYSTING	
	Dim a_status				' status of absence reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of absence reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim startdate	
	Dim notes	
	Dim reason
	Dim duration
	Dim employeeid
	Dim isEmployeeOnly
	Dim holType
    Dim duration_hrs
    Dim new_history_id
    Dim time_off_in_lieu_owed , part_full ,page,starttime,startDateTime,endtime
    Dim lstFrom, lstTo

    timeIntervals = Array("06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00")

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")

	absence_history_id 	= Request("absencehistoryid")
	nature_id 			= Request("natureid")
	employeeid			= Request("employeeid")
    
	' only allow line manager to approve holidays and sick leave
	If isLineManager(employeeid, Session("userid")) Then
		isEmployeeOnly = " "
	Else
		isEmployeeOnly = " and EMPLOYEEONLY = 1 "
	End If

	' begin processing
	Call OpenDB()

	If path  = "" then path = 0 end if ' initial entry to page

	If path = 0 Then			
		Call get_record()
	Elseif path = 1 Then
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

        Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (3, 4, 5, 20) " & isEmployeeOnly, "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", null)

		Dim strSQL

		strSQL = 	"SELECT                                                                 " &_
	                "    ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103), '')    AS STARTDATE    " &_
	                "    ,ISNULL(S.DESCRIPTION, 'N/A')					    AS STATUS       " &_
	                "    ,ISNULL(A.NOTES, 'N/A')						    AS NOTES        " &_
	                "    ,ISNULL(A.REASON, 'N/A')						    AS REASON       " &_
	                "    ,DURATION_HRS									    AS DURATION_HRS " &_
	                "    ,LEFT(CONVERT(NVARCHAR, A.STARTDATE, 108),5)	    AS StartTime    " &_
	                "    ,LEFT(CONVERT(NVARCHAR, A.RETURNDATE, 108),5)		AS EndTime      " &_
					"FROM 		E_ABSENCE A                                                 " &_
					"			LEFT JOIN E_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID     " &_
					"WHERE 		ABSENCEHISTORYID = " & absence_history_id

		Call OpenRs(rsSet, strSQL)

	    startdate	= rsSet("STARTDATE")	
        a_status 	= rsSet("STATUS")
		notes 		= rsSet("NOTES")
        reason 		= rsSet("REASON")		
		duration_hrs 	= rsSet("DURATION_HRS")
        starttime	= rsSet("StartTime")
        endtime     = rsSet("EndTime")		

        
    
		Call CloseRs(rsSet)        

        Call BuildSelect_From1DArray(lstFrom, "sel_From", timeIntervals,NULL,starttime,"width:auto;","textbox100",NULL)
        Call BuildSelect_From1DArray(lstTo, "sel_To", timeIntervals,NULL,endtime,"width:auto;","textbox100",NULL)

	End Function

   Function build_holiday_messages()
		
	page="My Job"
    	strSQL = "EXEC E_HOLIDAY_DETAIL " & session("employeeid") & " ,'" & page & "'"
     
		Call OpenRs (rsHols, strSQL) 
		IF NOT rsHols.EOF Then
	            time_off_in_lieu_owed =  rsHols(6) 
                part_full =  rsHols(7)
		end if
		CloseRS(rsHols)		
	End Function

	Function new_record()

		strSQL = "SELECT J.JOURNALID,J.EMPLOYEEID FROM E_ABSENCE A INNER JOIN E_JOURNAL J ON J.JOURNALID=A.JOURNALID WHERE ABSENCEHISTORYID = " & absence_history_id
		Call OpenRs(rsSet, strSQL)

		journalid = rsSet("JOURNALID")
		employee_id = rsSet("EMPLOYEEID")

		Call CloseRS(rsSet)

		a_status 	= Request.Form("sel_ACTION")
		actionid 	= Request.Form("sel_ACTION")		
		duration 	= 0
		notes 		= Replace(Request.form("txt_NOTES"),"'", " ")		
		reason 		= Replace(Request.form("txt_REASON"),"'", " ")
		holtype 	= ""
		Leave_Hrs   = Request.Form("hid_DURATION")
        startdate = Request.Form("txt_STARTDATE")
        meetingFrom = Request.Form("sel_From")
        meetingTo = Request.Form("sel_To")
        
        meetingStartDateTime = FormatDateTime(CDATE(startdate & " " & meetingFrom))
        meetingEndDateTime = FormatDateTime(CDATE(startdate & " " & meetingTo))
                       
		strSQL = 	"SET NOCOUNT ON;" &_
                    "INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION_HRS, REASON, NOTES, HOLTYPE, DURATION) " &_
					"VALUES (" & journalid & ", " & a_status & ", " & actionid & ", '" & Now & "', " & Session("userid") & ", '" & meetingStartDateTime & "', '" & meetingEndDateTime & "', " & Leave_Hrs & ", '" & Replace(reason,"'", "''")  & "', '" & Replace(notes,"'", "''") & "', '" &  Request.Form("HOLTYPE") & "', 0)" &_
                    " SELECT SCOPE_IDENTITY() AS HISTORYID;"       
       
        Set rsSet = Conn.Execute(strSQL)
		    new_history_id = rsSet.fields("HISTORYID").value
		    rsSet.close()
		Set rsSet = Nothing

		Call update_journal(journalid, a_status)

        ' if approved or declined then send email
        If (actionid = "4" Or actionid = "5") Then            
            call sendEmail(journalid,new_history_id,employee_id)            
        End If        

	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = "UPDATE E_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function

    Function sendEmail(journalid,new_history_id,employee_id)

    sql =   " SELECT ITEMACTIONID,LASTACTIONUSER,STARTDATE,RETURNDATE,DURATION_HRS FROM E_ABSENCE WHERE ABSENCEHISTORYID = " & new_history_id
    Call OpenRs(rsEmpRec, sql)
    If NOT rsEmpRec.EOF Then
        action = rsEmpRec("ITEMACTIONID")
        userId = rsEmpRec("LASTACTIONUSER")
        sdate = rsEmpRec("STARTDATE")
        duration =  rsEmpRec("DURATION_HRS")
    End If
    Call CloseRs(rsEmpRec)

    SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT C WHERE EMPLOYEEID = " & employee_id
	Call OpenRs(rsEmail, SQL)

	If NOT rsEmail.EOF And NOT rsEmail.BOF Then
		' send email
		Dim strRecipient, emailSubject, strSender
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

        SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT WHERE EMPLOYEEID = " & userId
        Call OpenRs(rsSenderEmail, SQL)

        If NOT rsSenderEmail.EOF And NOT rsSenderEmail.BOF Then 
            strSender=(rsSenderEmail.Fields.Item("WORKEMAIL").Value)
        End if
        Call CloseRs(rsSenderEmail)

        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        If action ="5" then
            emailbody = "Your " & duration &  " hour(s) leave for the date " & sdate & " has been approved."
            emailSubject = "Leave Approved"
        elseif action="4" then
            emailbody = "Your " & duration &  " hour(s) leave for the date " & sdate & " has been declined."
            emailSubject = "Leave Declined"
        end if

        On Error Resume NEXT
        Set iMsg.Configuration = iConf
        iMsg.To = strRecipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send
        
        If Err.number <> 0 Then
            If strRecipient = "" Or IsNull(strRecipient) Then
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed due to manager email address not provided, so please inform the concerned person.');</script>") 
            Else  
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed, so please inform the concerned person.');</script>")
            End If  
            Err.Clear()
            ON ERROR GOTO 0
        End If

	End If
	Call CloseRs(rsEmail)

    End Function
%>
<html>
<head>
    <title>ERM --> Update Absence</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <style type="text/css">
        body {
            background-color: White;
            margin: 10px;
        }

        .ui-datepicker-trigger {
            vertical-align: bottom;
        }
    </style>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "txt_STARTDATE|Startd Date|DATE|Y"
        FormFields[1] = "sel_From|From Time|TIME|Y"
        FormFields[2] = "sel_ACTION|Action|INTEGER|Y"

        function save_form() {
            if (!checkForm()) return false;
            //if(!isPartFull()) return false;
            if (!validate_email()) return false;
            if (!validate_from_To()) return false;

            document.getElementById("hid_go").value = 1;
            document.RSLFORM.action = "pInternalMeeting.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>";
            document.RSLFORM.target = "";
            document.RSLFORM.submit();
        }

        function return_data() {
            document.getElementById("txt_STARTDATE").focus();
            if ("<%=path%>" == 1) {
                opener.location.reload();
                opener.parent.document.getElementById("frm_pd").contentWindow.location.reload();
                window.close();
            }
        }

        function isPartFull() {

            var partFullTime = parseInt("<%=part_full%>");
            if (partFullTime == 1 || partFullTime == 2) {
                return true;
            } else {
                alert("Only Part/Full time employees are allowed to apply for internal meeting.");
                return false;
            }
        }

        function validate_from_To() {
            var status = true;

            var dateparts = document.getElementById("txt_STARTDATE").value.split("/");
            var startDateTime = new Date(dateparts[2] + "/" + dateparts[1] + "/" + dateparts[0] + " " + document.getElementById("sel_From").value);
            var endDateTime = new Date(dateparts[2] + "/" + dateparts[1] + "/" + dateparts[0] + " " + document.getElementById("sel_To").value);

            if (startDateTime >= endDateTime) {
                status = false;
                alert("Start time must be less than end time.");
            }

            var duration = endDateTime - startDateTime;

            document.getElementById("hid_DURATION").value = duration / 3600000;

            return status;
        }

        function validate_email() {
            var checkExist = parseInt(document.getElementById("hdnCheckEmail").value);
            if (checkExist == 0) {
                alert("Unable to save because email address of recipient does not exist.");
                return false;
            } else {
                return true;
            }
        }

        $(function () {
            $('#txt_STARTDATE').datepicker({
                dateFormat: 'dd/mm/yy',
                buttonImage: 'Images/calendar.png',
                showOn: "button",
                buttonImageOnly: true,
                onSelect: function (datetext) {
                    $('#txt_STARTDATE').val(datetext);
                }
            });
        });

    </script>
</head>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="pTimeOffInLieuLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td valign="top" height="20">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td rowspan="2" width="67">
                                <img src="Images/tab_leave.gif" width="67" height="20" alt="Leave" />
                            </td>
                            <td height="19" width="8061"></td>
                        </tr>
                        <tr>
                            <td bgcolor="#133E71" width="8061">
                                <img src="images/spacer.gif" height="1" alt="" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71; border-bottom: 1px solid #133E71'>
                    <table cellspacing="1" cellpadding="2" width="90%" align="center" border="0">
                        <tr>
                            <td colspan="4">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Title:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_REASON" id="txt_REASON" value="<%=reason%>" />
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                    border="0" alt="REASON" />
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="21">Action
                            </td>
                            <td height="21">
                                <%=lst_action%>
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_ACTION" id="img_ACTION" width="15px" height="15px"
                                    border="0" alt="ACTION" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Start Date
                            </td>
                            <td>
                                <input type="text" value="<%=startdate%>" name="txt_STARTDATE" id="txt_STARTDATE"
                                    class="textbox100" maxlength="10" onblur="check_date()" />
                                &nbsp;
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>From:
                            </td>
                            <td colspan="2">
                                <%=lstFrom %>
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_From" id="img_From" width="15px" height="15px"
                                    border="0" alt="Start Time" />
                            </td>
                        </tr>
                        <tr>
                            <td>To:
                            </td>
                            <td colspan="2">
                                <%=lstTo %>
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_To" id="img_To" width="15px" height="15px"
                                    border="0" alt="End Time" />
                            </td>
                        </tr>
                        <tr>
                            <td>Recorded By: &nbsp
                            </td>
                            <td colspan="2">
                                <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                                    readonly="readonly" />
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                    border="0" alt="RECORDED BY" />
                            </td>
                        </tr>
                        <tr>
                            <td>Notes
                            </td>
                            <td colspan="2">
                                <textarea style="overflow: hidden" rows="9" cols="20" class="textbox200" name="txt_NOTES"
                                    id="txt_NOTES"><%=notes %></textarea>
                            </td>
                            <td>
                                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                    border="0" alt="NOTES" />
                                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value='Save'
                                    class="RSLButton" style="cursor: pointer" />
                                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                                <input type="hidden" name="check_email" id="hdnCheckEmail" value="<%=email_exist%>" />
                                <input type="hidden" name="hid_DURATION" id="hid_DURATION" value="0" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" width="200" height="200" style="display: none"></iframe>
</body>
</html>
