<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var FormFields = new Array();
function SaveForm(){
	FormFields[0] = "Status|Status|SELECT|Y"
	FormFields[1] = "Start_Date|Start Date|DATE|Y"
	FormFields[2] = "Return_Date|Return Date|DATE|Y"
	FormFields[3] = "Reason|Reason|TEXT|Y"
	FormFields[4] = "Initial_Diagnosis|Initial Diagnosis|TEXT|Y"
	FormFields[5] = "Cert_Number|Cert Number|TEXT|Y"
	FormFields[6] = "Dr_Name|Dr Name|TEXT|Y"	
	FormFields[7] = "Recorded_by|Recorded by|TEXT|Y"						
	FormFields[8] = "Notes|Notes|TEXT|Y"						
	if (!checkForm()) return false;
	RSLFORM.submit()
	}
</script> 
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table border="0" >
  <tr> 
    <td colspan="3" bgcolor="#133E71">&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Status:</td>
    <td width="210"> 
      <select  class="textbox200" name="Status">
      </select>
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Start Date:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Start_Date"> 
    </td>
    <td width="56" nowrap>&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Return Date:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Return_Date">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Reason:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Reason">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Initial Diagnosis:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Initial_Diagnosis">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Cert Number:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Cert_Number">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Dr.'s Name:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Dr_Name">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Recorded by:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Recorded_by">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" valign="top" nowrap>Notes:</td>
    <td width="210"> 
      <textarea style='border:1px solid black;OVERFLOW:HIDDEN' class="textbox200" name="Notes" ></textarea>
    </td>
    <td width="56"> 
      <input type="button" value="Save" class="RSLButton" onclick="SaveForm()">
    </td>
  </tr>
  <tr>
    <td width="122">&nbsp;</td>
    <td width="210">&nbsp;</td>
    <td width="56">&nbsp;</td>
  </tr>
</table>
</body>
</html>
