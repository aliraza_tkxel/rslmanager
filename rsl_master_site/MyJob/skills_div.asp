<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
If (EmployeeID = "") Then EmployeeID = -1 End If

Call OpenDB()

SQL = "SELECT * FROM E_QUALIFICATIONSANDSKILLS " &_
		"WHERE E_QUALIFICATIONSANDSKILLS.EMPLOYEEID = " & EmployeeID
Call OpenRs(rsLoader, SQL)

strTable = "<table width=""100%"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""tab_table"">" & vbCrLf
	strTable = strTable & "<thead>" & vbCrLf
	strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td width=""200px""><b>Subject</b></td>" & vbCrLf
		strTable = strTable & "<td width=""90px""><b>Result</b></td>" & vbCrLf
		strTable = strTable & "<td width=""80px""><b>Date</b></td>" & vbCrLf
		strTable = strTable & "<td width=""80px""><b>Reason</b></td>" & vbCrLf
	strTable = strTable & "</tr>" & vbCrLf
	strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td colspan=""4""></td>" & vbCrLf
	strTable = strTable & "</tr>" & vbCrLf
	strTable = strTable & "</thead>" & vbCrLf
	strTable = strTable & "<tbody>" & vbCrLf
If (NOT rsLoader.EOF) Then
	while (NOT rsLoader.EOF) 
		special = rsLoader("SpecialisedTraining")
		titlebit = ""
		If (special <> "" AND not isNull(special)) Then
			titlebit = " title='Specialised Training:" & special & "'"
		End If
		strTable = strTable & "<tr>" & vbCrLf
			strTable = strTable & "<td valign=""top """ & titlebit & ">" & rsLoader("Subject") & "</td>" & vbCrLf
			strTable = strTable & "<td valign=""top"">" & rsLoader("RESULT") & "</td>" & vbCrLf
			strTable = strTable & "<td valign=""top"">" & rsLoader("QUALIFICATIONDATE") & "</td>" & vbCrLf
			strTable = strTable & "<td valign=""top"">" & rsLoader("PROFESSIONALQUALIFICATION") & "</td>" & vbCrLf
		strTable = strTable & "</tr>"

		rsLoader.moveNext
	Wend
Else
	strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td colspan=""3"" align=""center"">No Qualifications\Skills</td>" & vbCrLf
	strTable = strTable & "</tr>" & vbCrLf
End If
	strTable = strTable & "</tbody>" & vbCrLf
	strTable = strTable & "<tfoot>" & vbCrLf
	strTable = strTable & "<tr>" & vbCrLf
		strTable = strTable & "<td></td>" & vbCrLf
	strTable = strTable & "</tr>" & vbCrLf
	strTable = strTable & "</tfoot>" & vbCrLf
strTable = strTable & "</table>"

Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <title>Skills Div</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body bgcolor="#FFFFFF" text="#000000">
    <!-- SKILLS / QUALIFICATIONS------------------------------------->
    <table width="750" style="height:190px; border-right: SOLID 1PX #133E71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" width="70%" height="100%">
                <table width="100%" style="height:100%; border: SOLID 1PX #133E71" cellpadding="1"
                    cellspacing="2" border="0" class="TAB_TABLE">
                    <tr valign="top">
                        <td>
                            <%= strTable %>
                        </td>
                    </tr>
                </table>
              </td>
            <td width="30%" height="50%">
                <table width="100%" style="height:100%; border: SOLID 1PX #133E71" cellpadding="1"
                    cellspacing="2" border="0" class="TAB_TABLE">
                    <tr>
                        <td>
                            Holidays
                        </td>
                    </tr>
                    <tr>
                        <td>
                            00 days available
                        </td>
                    </tr>
                    <tr>
                        <td>
                            00 days boooked <b>Book Now!</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="30%" height="50%">
                <table width="100%" style="height:100%; border: SOLID 1PX #133E71" cellpadding="1"
                    cellspacing="2" border="0" class="TAB_TABLE">
                    <tr>
                        <td>
                            Policies/Procedures
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Policy Handbook
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Procedure Handbook
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
