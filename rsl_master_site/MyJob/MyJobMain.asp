<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL My Job</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<!--#include file="menu/iagMYJOB.INC"-->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include file="menu/BodyTop.asp" -->  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="14">&nbsp;</td>
		<td>
		<table width="100%" border="0" cellspacing="5" cellpadding="0" class="rslmanager">
	 		<tr> 
        	<tr> 
				<td><b>My Job</b></td><td>&nbsp;</td>
			</tr>
		  		<td width="35%"><img src="../myImages/HoldingGraphics/myjobs.gif" width="295" height="283"></td>
          		<td width="65%" valign="top"> 
               <br><p><b>
              The My Job Module is an essential communication channel between 
              managers and their team members. The module helps individual 
              team members to effectively manage their contribution towards the 
              company's business aspirations.</b></p>
            <p>Working alongside all other modules My Job ensures:</p>
            <ul>
              <li>Team member responsibility for maintaining personal details</li>
              <li>Easy access to allocated business plan milestones and tasks</li>
              <li>Annual leave application and recording</li>
              <li>Accurate recording of business expenses against specific projects</li>
              <li>Resource planning and monitoring</li>
              <li>Effective staff development</li>
            </ul>

          		</td>
			</tr>
      	</table>
		</td>
	</tr>
	<tr>
		<td width="14">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

