<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

</script> 
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table border="0" >
  <tr> 
    <td colspan="3" bgcolor="#133E71">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Type:</td>
    <td width="210">
      <input type="text" class="textbox200" name="Type">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Nature:</td>
    <td width="210">
      <input type="text" class="textbox200" name="Nature">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr>
    <td width="122" nowrap>Title:</td>
    <td width="210">
      <input type="text" class="textbox200" name="Title">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Status:</td>
    <td width="210"> 
      <select  class="textbox200" name="Status">
      </select>
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Start Date:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Start_Date">
    </td>
    <td width="56" nowrap>&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Return Date:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Return_Date">
    </td>
    <td width="56">Anticipated</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Reason:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Reason">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Initial Diagnosis:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Initial_Diagnosis">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" nowrap>Recorded by:</td>
    <td width="210"> 
      <input type="text" class="textbox200" name="Recorded_by">
    </td>
    <td width="56">&nbsp;</td>
  </tr>
  <tr> 
    <td width="122" valign="top" nowrap>Notes:</td>
    <td width="210"> 
      <textarea style='border:1px solid black;OVERFLOW:HIDDEN' class="textbox200" name="Notes" ></textarea>
    </td>
    <td width="56">
      <input type="button" value="Close" class="RSLButton"  onclick="javascript:self.close()" "name="button">
     </td>
  </tr>
  <tr> 
    <td width="122">&nbsp;</td>
    <td width="210">&nbsp;</td>
    <td width="56">&nbsp;</td>
  </tr>
</table>
</body>
</html>
