<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	// id for sql
	Dim employee_id, str_isabsent
	// post variables
	Dim po_startdate, po_fptime, po_probation, po_reviewdate, po_jobtitle, po_refnumber, po_role, po_team, po_hours, po_grade, po_salary
	Dim po_taxoffice, po_taxcode, po_payroll, po_patch, po_linemanager, po_buddy, po_noticeperiod, po_noticedate, po_manager, po_licnumber,lst_year
	Dim po_ninumber, po_fnumber, po_holidays, po_appdate, po_director
	// benefit variables	
	//Pension//
	dim	b_memnumber,b_scheme,b_schemedesc,b_schemedesccut,b_salarypercent,b_contribution,b_avc,	b_contractedout ,b_professionalfees ,b_telephoneallowance ,b_firstaidallowance,_
		b_calloutallowance,b_carmake,b_model,b_listprice,b_conthirecharge,b_empcontribution,b_excesscontribution,b_carallowance ,b_carloanstartdate ,_
		b_enginesize, b_loaninformation,b_value,b_term,b_monthlyrepay,b_accomodationrent,b_counciltax ,b_heating,b_linerental,b_annualpremium,b_additionalmembers,_
		b_groupschemeref,b_membershipno
	
	// next of kin details
	Dim nok_name, nok_street, nok_town, nok_county, nok_postcode, nok_hometel, nok_mobile
	
	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev
	
	
	OpenDB()
	
	// if request variable then we come from my team else we come from my job
	employee_id = Request("employeeid") 
	if employee_id = "" Then
		employee_id = Session("userid")
	end if
	
	// only allow line manager or actual user to create items for this person
	if isLineManager(employee_id, Session("userid")) Or employee_id = Session("userid") THEN 
		only_allow_line_manager = ""
	Else
		only_allow_line_manager = "disabled"
	End If
	
	' Allow HR staff as well
	If session("Teamcode") = "HUM" or session("Teamcode") = "CORP" then
		only_allow_line_manager = ""
	End If
	
	'response.write "it is " & only_allow_line_manager

	session("employeeid") = employee_id // session variable required to pass emp id to include page
	%><!--#include virtual="includes/tables/myjob_right_box.asp" --><%
	get_main_details()
	build_diff_dis()
	BuildSelect_Year()
	str_isabsent = isAbsent(employee_id)
	
	Call BuildSelect(lst_item, "sel_ITEM", "E_ITEM", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange='item_change()'")
	Call BuildSelect(lst_itemfilter, "sel_ITEMFILTER", "E_ITEM", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox100", "style='display:block' onchange='nature_change()'")

	Function isAbsent(int_employeeid)
		
		Dim str_isabsent 
		str_isabsent = "No"
		SQL = "SELECT J.* FROM 	E__EMPLOYEE E 	INNER JOIN E_JOURNAL J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"AND J.ITEMNATUREID = 1 AND CURRENTITEMSTATUSID = 1 WHERE 	E.EMPLOYEEID = " & int_employeeid
		Call OpenRS(rsSet,SQL)
		if not rsSet.EOF Then
			str_isabsent = "Yes"
		End If

		isAbsent = str_isabsent	
		
	End Function

	
	// get employee details
	Function get_main_details()
		
		Call OpenRs (rsSet, "SELECT * FROM V_E_EMPLOYEEDETAILS WHERE EMPLOYEEID = " & employee_id) 
		// postdetails section
		po_startdate	= rsSet("STARTDATE")
		po_fptime		= rsSet("PARTFULLTIME")
		po_probation	= rsSet("PROBATIONPERIOD")
		po_reviewdate	= rsSet("REVIEWDATE")
		po_jobtitle		= rsSet("JOBTITLE")
		po_refnumber	= rsSet("REFERENCENUMBER")
		po_role			= rsSet("ROLE")
		po_team			= rsSet("TEAMNAME")
		po_hours		= rsSet("HOURS")
		po_grade		= rsSet("GRADE")
		po_salary		= rsSet("SALARY")
		po_taxoffice	= rsSet("TAXOFFICE")
		po_taxcode		= rsSet("TAXCODE")
		po_payroll		= rsSet("PAYROLLNUMBER")
		po_patch		= rsSet("PATCH")
		po_linemanager	= rsSet("LINEMANAGER")
		po_buddy		= rsSet("BUDDY")
		po_noticeperiod	= rsSet("NOTICEPERIOD")
		po_noticedate	= rsSet("DATEOFNOTICE")
		po_manager		= rsSet("MANAGER")
		po_licnumber	= rsSet("DRIVINGLICENSENUMBER")
		po_ninumber		= rsSet("NINUMBER")
		po_fnumber		= rsSet("FOREIGNNATIONALNUMBER")
		po_holidays		= rsSet("HOLIDAYENTITLEMENTDAYS") & " days, " & rsSet("HOLIDAYENTITLEMENTHOURS") & " hours"
		po_appdate		= rsSet("APPRAISALDATE")
		po_director		= rsSet("DIRECTOR")
		
		//Pension//
		b_memnumber 	= rsSet("MEMNUMBER") //new
		b_scheme 		= rsSet("SCHEME") //new
		b_salarypercent = FormatNumber(IsNullNumber(rsSet("SALARYPERCENT")),0,-1,0,0)

		b_contribution 	= FormatNumber(IsNullNumber(rsSet("CONTRIBUTION")),2,-1,0,0)
		b_avc 			= FormatNumber(IsNullNumber(rsSet("AVC")),2,-1,0,0)
		b_contractedout = rsSet("CONTRACTEDOUT")
		//General
		b_professionalfees 		= FormatNumber(IsNullNumber(rsSet("PROFESSIONALFEES")),2,-1,0,0)
		b_telephoneallowance 	= FormatNumber(IsNullNumber(rsSet("TELEPHONEALLOWANCE")),2,-1,0,0)	
		b_firstaidallowance 	= FormatNumber(IsNullNumber(rsSet("FIRSTAIDALLOWANCE")),2,-1,0,0)						
		b_calloutallowance 		= FormatNumber(IsNullNumber(rsSet("CALLOUTALLOWANCE")),2,-1,0,0)
		//Company Car
		b_carmake 				= rsSet("CARMAKE") //new
		b_model 				= rsSet("MODEL") //new
		b_listprice 			= FormatNumber(IsNullNumber(rsSet("LISTPRICE")),2,-1,0,0)						
		b_conthirecharge 		= FormatNumber(IsNullNumber(rsSet("CONTHIRECHARGE")),2,-1,0,0) //NEW
		b_empcontribution 		= FormatNumber(IsNullNumber(rsSet("EMPCONTRIBUTION")),2,-1,0,0) //new
		b_excesscontribution 	= FormatNumber(IsNullNumber(rsSet("EXCESSCONTRIBUTION")),2,-1,0,0)
		//Car Essential User
		b_carallowance 			= FormatNumber(IsNullNumber(rsSet("CARALLOWANCE")),2,-1,0,0)
		b_carloanstartdate 		= rsSet("CARLOANSTARTDATE")
		b_enginesize 			= FormatNumber(IsNullNumber(rsSet("ENGINESIZE")),0,-1,0,0) //new
		b_loaninformation 		= rsSet("LOANINFORMATION") //NEW
		b_value 				= FormatNumber(IsNullNumber(rsSet("VALUE")),2,-1,0,0)	
		b_term 					= FormatNumber(IsNullNumber(rsSet("TERM")),0,-1,0,0)
		b_monthlyrepay 			= FormatNumber(IsNullNumber(rsSet("MONTHLYREPAY")),2,-1,0,0)						
		//Accomodation
		b_accomodationrent 		= FormatNumber(IsNullNumber(rsSet("ACCOMODATIONRENT")),2,-1,0,0) //NEW
		b_counciltax 			= FormatNumber(IsNullNumber(rsSet("COUNCILTAX")),2,-1,0,0) //new
		b_heating 				= FormatNumber(IsNullNumber(rsSet("HEATING")),2,-1,0,0) //NEW
		b_linerental 			= FormatNumber(IsNullNumber(rsSet("LINERENTAL")),2,-1,0,0) //new
		//BUPA
		b_annualpremium 		= FormatNumber(IsNullNumber(rsSet("ANNUALPREMIUM")),2,-1,0,0) //new
		if not  rsSet("ADDITIONALMEMBERS") = "none" then
		b_additionalmembers 	= rsSet("ADDITIONALMEMBERS")
		End If
		b_groupschemeref=rsSet("GROUPSCHEMEREF") //new
		b_membershipno = rsSet("MEMBERSHIPNO") //new
		
	
		
		
		// next of kin details
		nok_name		= rsSet("NOK_FULLNAME")
		nok_street		= rsSet("NOK_STREET")
		nok_town		= rsSet("NOK_TOWN")
		nok_county		= rsSet("NOK_COUNTY")
		nok_postcode	= rsSet("NOK_POSTCODE")
		nok_hometel		= rsSet("NOK_HOMETEL")
		nok_mobile		= rsSet("NOK_MOBILE")
				
		CloseRs(rsSet)
		
	End Function	
	
	If (Not isNull(b_scheme) and Not b_scheme = "") Then
		strSQL = 	"SELECT 	ISNULL(SCHEMEDESC, '') AS SCHEMEDESC " &_
					"FROM	  	G_SCHEME G " &_
					"WHERE 		G.SCHEMEID = " & b_scheme 
		//Response.Write strSQL
		Call OpenRs (rsSet, strSQL) 
			b_schemedesc = rsSet("SCHEMEDESC")
			b_schemedesccut = b_schemedesc
			if Len(b_schemedesc) > 14 Then b_schemedesccut = Left(b_schemedesc,14) & "..."
		CloseRs(rsSet)
	End If	
		
	Function build_diff_dis()
	
		cnt = 1
		strSQL = 	"SELECT 	ISNULL(DIS.DESCRIPTION, 'dd') AS DIFFDIS " &_
				"FROM	 	E__EMPLOYEE E " &_
				"			LEFT JOIN E_DIFFDISID D ON E.EMPLOYEEID = D.EMPLOYEEID " &_
				"			LEFT JOIN E_DIFFDIS  DIS ON D.DIFFDIS = DIS.DIFFDISID " &_
				"WHERE 		E.EMPLOYEEID = " & employee_id & " " &_
				"ORDER	 	BY DESCRIPTION "
		
		Call OpenRs (rsSet, strSQL) 
		
		str_diff_dis = "<TABLE WIDTH='100%' HEIGHT='100%' STYLE='BORDER-COLLAPSE:COLLAPSE' BORDER=1 CELLSPACING=0>" &_
						"<TR><TD CLASS='TITLE'>&nbsp;Disability</TD></TR>"
		While Not rsSet.EOF
			
			cnt = cnt + 1
			str_diff_dis = str_diff_dis & 	"<TR>" &_
											"<TD>" & rsSet("DIFFDIS") & "</TD>" &_
											"</TR>"
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		// fill bottom with rows
		for i = 1 TO (TABLE_ROW_SIZE - cnt)
			str_diff_dis = str_diff_dis	 & 	"<TR><TD>&nbsp;</TD></TR>"
		Next	
		str_diff_dis = str_diff_dis	 & 	"</TABLE>"
	End Function	

	//Build Option Select for Years
	Function BuildSelect_Year()
	    dim current_year
	    current_year=Year(date())    
	    lst_year ="<select name=""sel_YEAR"" id=""sel_YEAR"" class=""textbox100"" >"
	        do while current_year>=2004 
	            lst_year=lst_year & "<option value=""" & current_year & """>Year "  & current_year & " </option>"
	            current_year=current_year-1
	        loop
	    lst_year =lst_year & "</select>"
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager My Job -- > Employment Relationship Manager</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	
	var FormFields = new Array();
	FormFields[0] = "txt_TITLE|Title|TEXT|Y"
	FormFields[1] = "sel_ITEM|Item|SELECT|Y"
	FormFields[2] = "sel_NATURE|Nature|SELECT|Y"

	
	// preload image variables -- global
	var open1, closed1, open2, closed2, previous2, open3, closed3, previous3, open4, closed4, previous4, open5
	var closed5, previous5, open6, closed6, previous6, open7, closed7, previous7

	preloadFlag = false;
	function preloadImages2() {
	
		if (document.images) {
			open1 = newImage("Images/1-open.gif");
			closed1 = newImage("Images/1-closed.gif");
			open2 = newImage("Images/2-open.gif");
			closed2 = newImage("Images/2-closed.gif");
			previous2 = newImage("Images/2-previous.gif");
			open3 = newImage("Images/3-open.gif");
			closed3 = newImage("images/3-closed.gif");
			previous3 = newImage("images/3-previous.gif");
			open4 = newImage("images/4-open.gif");
			closed4 = newImage("images/4-closed.gif");
			previous4 = newImage("images/4-previous.gif");
			open5 = newImage("images/5-open.gif");
			closed5 = newImage("images/5-closed.gif");
			previous5 = newImage("images/5-previous.gif");
			open6 = newImage("images/6-open.gif");
			closed6 = newImage("images/6-closed.gif");
			previous6 = newImage("images/6-previous.gif");
			open7 = newImage("images/7-open.gif");
			closed7 = newImage("images/7-closed.gif");
			previous7 = newImage("images/7-previous.gif");
			itemopen = newImage("images/item-1-open.gif");
			itemclosed = newImage("images/item-1-closed.gif");
			newopen = newImage("images/item-2-open.gif");
			newclosed = newImage("images/item-2-previous.gif");
			preloadFlag = true;
			}
	}

	// open close item divs and tabs
	function swap_item_div(int_which_one){
	
		if (int_which_one == 8){
			frm_erm.location.href = "iFrames/iEmployeeJournal.asp?employeeid=<%=employee_id%>";
			document.getElementById("div8").style.display = "block";
			document.getElementById("div9").style.display = "none";
			document.getElementById("img8").src = "Images/item-1-open.gif"
			document.getElementById("img9").src = "Images/item-2-previous.gif"
			document.getElementById("img10").src = "Images/item-3-closed.gif"
			}
		else if (int_which_one == 9){
			document.getElementById("div8").style.display = "none";
			document.getElementById("div9").style.display = "block";
			document.getElementById("img8").src = "Images/item-1-closed.gif"
			document.getElementById("img9").src = "Images/item-2-open.gif"
			document.getElementById("img10").src = "Images/item-3-previous.gif"
			}
		else {
			document.getElementById("img8").src = "Images/item-1-closed.gif"
			document.getElementById("img9").src = "Images/item-2-closed.gif"
			document.getElementById("img10").src = "Images/item-3-open.gif"
		}
	}
	
	// swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
	function swap_div(int_which_one){
		
		var divid, imgid, upper
		
		upper = 7;
		divid = "div" + int_which_one.toString();
		imgid = "img" + int_which_one.toString();
		
		// close all other divs and open src div
		for (i = 1 ; i <= upper ; i++)
			document.getElementById("div" + i + "").style.display = "none";
		document.getElementById(divid).style.display = "block";
		
		// manipulate images
		for (j = 1 ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
		}
		
		// unless last image in row
		// Amended by Mr C ..... If the person selects skill/quals then only show Health-previous not -closed. 
		// 		-Would have had to change alot of Divs and Images Names otherwise!!!
		if (int_which_one != upper)
		if	(int_which_one == 3)
			document.getElementById("img" + (int_which_one + 2) + "").src = "Images/" + (int_which_one + 2) + "-previous.gif"
		else
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
	}
	
	// sawps table that need more than one page to display details
	function swap_tables(str_which_one){
	
		
			
		if(str_which_one == "NEW")
		{	
			if (document.getElementById(str_which_one + "1").style.display == "block"){
				document.getElementById(str_which_one + "1").style.display = "none"
				document.getElementById("BEN2").style.display = "block"
				}
			else {
				document.getElementById(str_which_one + "1").style.display = "block"
				document.getElementById("BEN2").style.display = "none"
				}
		}
		else
		{
			if (document.getElementById(str_which_one + "1").style.display == "block"){
			document.getElementById(str_which_one + "1").style.display = "none"
			document.getElementById(str_which_one + "2").style.display = "block"
			}
			else {
			document.getElementById(str_which_one + "1").style.display = "block"
			document.getElementById(str_which_one + "2").style.display = "none"
			}
		}
			
	}
	
	function item_change(){
	
		RSLFORM.target = "frm_erm_srv";
		RSLFORM.action = "serverside/ItemChange_srv.asp?empoyeeid=<%=employee_id%>&itemid=" + RSLFORM.sel_ITEM.value;
		RSLFORM.submit();
	
	}
	
	function nature_change(){
	
		RSLFORM.target = "frm_erm_srv";
		RSLFORM.action = "serverside/NatureChange_srv.asp?empoyeeid=<%=employee_id%>&itemid=" + RSLFORM.sel_ITEMFILTER.value;
		RSLFORM.submit();
	
	}

		
	function go_nature(){
		
		if (!checkForm()) return false;
		var nature_id, item_id, title
		nature_id = RSLFORM.sel_NATURE.value;
		item_id = RSLFORM.sel_ITEM.value;
		title = RSLFORM.txt_TITLE.value;
		// for now only allow item 'absence' to be recorded by employees
		if (nature_id == "")
			frm_nature.location.href = "blank.asp";
		else if (item_id != 1){
			if (item_id == 6)
				frm_nature.location.href = "iFrames/iNotes.asp?employeeid=<%=employee_id%>&natureid="+nature_id+"&itemid="+item_id+"&title="+title;		
			else
				frm_nature.location.href = "unavailable.asp";
				}
		// you cannot create an absence record if one already exists
		else if ("<%=str_isAbsent%>" == "Yes" && nature_id == 1){
			alert("This person is already marked as absent!!")
			return false;
			}
		else{
			
			//alert("iFrames/iGeneral.asp?employeeid=<%=employee_id%>&natureid="+nature_id+"&itemid="+item_id+"&title="+title);
			frm_nature.location.href = "iFrames/iGeneral.asp?employeeid=<%=employee_id%>&natureid="+nature_id+"&itemid="+item_id+"&title="+title;		
			}
	}
	
	function blank(){
	
		frm_nature.location.href = "blank.asp";
	
	}
	// receives the url of the page to open plus the required width and the height of the popup
	function update_record(str_redir, wid, hig){
	
		window.open(str_redir, "display","width="+wid+",height="+hig+",left=100,top=200,scrollbars=yes") ;
	
	}
	
	function openAction(enqID){
		window.open("ActionPlan.asp?enquiryID=" + enqID, "IAGActionPlan", "height=595px,width=590px,scrollbars=yes,toolbar=no");
		}
	
	function open_desc(){
	
		window.open("/myjob/popups/pJob.asp", "display","width=590,height=595,left=100,top=50,scrollbars=yes,toolbar=no") ;
		
	}
	function filter_list(){
	
		RSLFORM.target = "frm_erm";
		RSLFORM.action = "iFrames/iEmployeeJournal.asp?employeeid=<%=employee_id%>&natureid=" + RSLFORM.sel_NATUREFILTER.value + "&sel_year=" + RSLFORM.sel_YEAR.value;;
		RSLFORM.submit();
	
	}
	
	function item_details(status){
	
		if (status == 1){
			document.getElementById("RSLitem").style.display = "block"
			document.getElementById("RSLnature").style.display = "block"
			document.getElementById("RSLyear").style.display = "block"
			}
		else
			{
			document.getElementById("RSLitem").style.display = "none"
			document.getElementById("RSLnature").style.display = "none"
			document.getElementById("RSLyear").style.display = "none"
			}
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF onLoad="initSwipeMenu(0);preloadImages();" onUnload="macGo()" class='ta' MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TR> 
  <TD HEIGHT=100% VALIGN=TOP STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71' width=772> 
<TR> 
  <TD> 
<TR> 
  <TD HEIGHT=100% VALIGN=TOP STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71' width=772> 
<TR> 
  <TD> 
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name = RSLFORM method=post>
      <!-- ImageReady Slices (My_Job_jan_perdetails_tabs.psd) -->
      <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
        <TR> 
          <TD ROWSPAN=2> <IMG NAME="img1" SRC="images/1-open.gif" WIDTH=115 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(1)"></TD>
          <TD ROWSPAN=2> <IMG NAME="img2" SRC="images/2-previous.gif" WIDTH=74 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(2)"></TD>
          <TD ROWSPAN=2> <IMG NAME="img3" SRC="images/3-closed.gif" WIDTH=94 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(3)"></TD>
          <TD ROWSPAN=2 style='display:none'> <IMG NAME="img4" SRC="images/4-closed.gif" WIDTH=86 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(4)"></TD>
          <TD ROWSPAN=2> <IMG NAME="img5" SRC="images/5-closed.gif" WIDTH=70 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(5)"></TD>
          <TD ROWSPAN=2> <IMG NAME="img6" SRC="images/6-closed.gif" WIDTH=52 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(6)"></TD>
          <TD ROWSPAN=2> <IMG NAME="img7" SRC="images/7-closed.gif" WIDTH=82 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_div(7)"></TD>
          <TD> <IMG SRC="images/spacer.gif" WIDTH=260 HEIGHT=19></TD>
        </TR>
        <TR> 
          <TD BGCOLOR=#133E71 height="2"> <IMG SRC="images/spacer.gif" WIDTH=260 HEIGHT=1></TD>
        </TR>
      </TABLE>
      <!-- End ImageReady Slices -->
      <DIV ID=div1 STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'> 
        <!-- PERSONAL DETAILS------------------------------------->
        <IFRAME NAME=frm_pd <%=TABLE_DIMS%> STYLE="OVERFLOW:hidden" SRC="iFrames/iPersonalDetails.asp?employeeid=<%=employee_id%>" frameborder=0></IFRAME> 
        <!-- END OF PERSONAL DETAILS------------------------------------->
      </DIV>
      <DIV ID=div2 STYLE='DISPLAY:NONE'> 
        <!-- CONTACTS------------------------------------------->
        <IFRAME NAME=frm_contact <%=TABLE_DIMS%> STYLE="OVERFLOW:hidden" SRC="iFrames/iContact.asp?employeeid=<%=employee_id%>" frameborder=0></IFRAME> 
        <!-- END OF CONTACTS------------------------------------>
      </DIV>
      <DIV ID=div3 STYLE='DISPLAY:NONE'> 
        <!-- END OF SKILLS / QUALIFICATIONS------------------------------------->
        <IFRAME NAME=frm_skills <%=TABLE_DIMS%> STYLE="OVERFLOW:hidden" SRC="iFrames/iSkills.asp?employeeid=<%=employee_id%>" frameborder=0></IFRAME> 
        <!-- SKILLS / QUALIFICATIONS-------------------------------------------->
      </DIV>
      <DIV ID=div4 STYLE='DISPLAY:NONE'> </DIV>
      <DIV ID=div5 STYLE='DISPLAY:NONE'> 
        <!-- HEALTH------------------------------------->
        <TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
          <TR> 
            <TD ROWSPAN=2 WIDTH=70% HEIGHT=100%> 
              <TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER: SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
                <TR>
                  <TD CLASS='TITLE'>Name</TD>
                  <TD WIDTH=160><%=nok_name%></TD>
                  <TD rowspan="7"><%=str_diff_dis%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Street</TD>
                  <TD><%=nok_street%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Town</TD>
                  <TD><%=nok_town%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>County</TD>
                  <TD><%=nok_county%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Postcode</TD>
                  <TD><%=nok_postcode%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Home Tel</TD>
                  <TD><%=nok_hometel%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Mobile</TD>
                  <TD><%=nok_mobile%></TD>
                </TR>
              </TABLE>
            </TD>
            <TD WIDTH=30% HEIGHT=50% > <%=str_holiday%> </TD>
          </TR>
          <TR> 
            <TD WIDTH=30% HEIGHT=50% > <%=str_policy%> </TD>
          </TR>
        </TABLE>
      </DIV>
      <DIV ID=div6 STYLE='DISPLAY:NONE'> 
        <TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
          <TR> 
            <TD ROWSPAN=2 WIDTH=70% HEIGHT=100%> 
              <TABLE HEIGHT=100% WIDTH=100% ID="POST1" STYLE="BORDER-COLLAPSE:COLLAPSE;DISPLAY:BLOCK" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
                <TR>
                  <TD CLASS='TITLE'>Start Date</TD>
                  <TD WIDTH=160><%=po_startdate%></TD>
                  <TD CLASS='TITLE'>Full/Part Time</TD>
                  <TD WIDTH=160><%=po_fptime%></TD>
                  <TD ALIGN=RIGHT VALIGN=TOP> <img src=images/down.gif border=0 style="cursor:hand"  title="Toggle Personal Details" onClick="swap_tables('POST')" width="17" height="16" align="absbottom"></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Probation</TD>
                  <TD><%=po_probation%></TD CLASS='TITLE'>
                  <TD CLASS='TITLE'>Hours</TD>
                  <TD COLSPAN=2><%=po_hours%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Review Date</TD>
                  <TD><%=po_reviewdate%></TD>
                  <TD CLASS='TITLE'>Grade</TD>
                  <TD COLSPAN=2><%=po_grade%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Job Title</TD>
                  <TD><%=po_jobtitle%></TD>
                  <TD CLASS='TITLE'>Salary</TD>
                  <TD COLSPAN=2><%=po_salary%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Ref Number</TD>
                  <TD><%=po_refnumber%></TD>
                  <TD CLASS='TITLE'>Tax Office</TD>
                  <TD COLSPAN=2><%=po_taxoffice%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Details of Role</TD>
                  <TD><%=po_role%></TD>
                  <TD CLASS='TITLE'>Tax Code</TD>
                  <TD COLSPAN=2><%=po_taxcode%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Team</TD>
                  <TD><%=po_team%></TD>
                  <TD CLASS='TITLE'>Payroll Number</TD>
                  <TD COLSPAN=2><%=po_payroll%></TD>
              </TABLE>
              <TABLE HEIGHT=100% WIDTH=100% ID="POST2" STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;DISPLAY:NONE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
                <TR>
                  <TD CLASS='TITLE'>Patch</TD>
                  <TD WIDTH=160><%=po_patch%></TD>
                  <TD CLASS='TITLE'>Lic Number</TD>
                  <TD WIDTH=160><%=po_licnumber%></TD>
                  <TD ALIGN=RIGHT VALIGN=TOP> <img src=images/up.gif border=0 style="cursor:hand"  title="Toggle Personal Details" onClick="swap_tables('POST')" width="17" height="16" align="absbottom"></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Line Manager</TD>
                  <TD><%=po_linemanager%></TD>
                  <TD CLASS='TITLE'>NI Number</TD>
                  <TD COLSPAN=2><%=po_ninumber%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Buddy</TD>
                  <TD><%=po_buddy%></TD>
                  <TD CLASS='TITLE'>FN N&deg;</TD>
                  <TD COLSPAN=2><%=po_fnumber%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Notice Period</TD>
                  <TD><%=po_noticeperiod%></TD>
                  <TD CLASS='TITLE'>Holidays</TD>
                  <TD COLSPAN=2><%=po_holidays%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Date Of notice</TD>
                  <TD><%=po_noticedate%></TD>
                  <TD CLASS='TITLE'>Appraisal Date</TD>
                  <TD COLSPAN=2><%=po_appdate%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Manager</TD>
                  <TD><%=po_manager%></TD>
                  <TD CLASS='TITLE'>Director</TD>
                  <TD COLSPAN=2><%=po_director%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD></TD>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD COLSPAN=2>&nbsp;</TD>
                </TR>
              </TABLE>
            </TD>
            <TD WIDTH=30% HEIGHT=50% > <%=str_holiday%> </TD>
          </TR>
          <TR> 
            <TD WIDTH=30% HEIGHT=50% > <%=str_policy%> </TD>
          </TR>
        </TABLE>
      </DIV>
      <DIV ID=div7 STYLE='DISPLAY:NONE'> 
        <TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
          <TR> 
            <TD ROWSPAN=2 WIDTH=70% HEIGHT=150> 
              <TABLE HEIGHT=100% WIDTH=100% ID="BEN1" STYLE="BORDER-COLLAPSE:COLLAPSE;DISPLAY:BLOCK" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
                <TR>
                  <TD CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX SOLID'><b>PENSION</b></TD>
                  <TD STYLE='BORDER-BOTTOM:1PX SOLID' WIDTH=160>
                </TR>
                <TD CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX SOLID'><b>COMPANY CAR</b></TD>
                <TD STYLE='BORDER-BOTTOM:1PX SOLID' WIDTH=160></TD>
                <TD ALIGN=RIGHT VALIGN=TOP STYLE='BORDER-BOTTOM:1PX SOLID;BORDER-LEFT:NONE'> 
                  <img src=images/down.gif border=0 style="cursor:hand"  title="Toggle Benefit Details" onClick="swap_tables('BEN')" width="17" height="16" align="absbottom"></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Mem Num:</TD>
                  <TD><%=b_memnumber%></TD>
                  <TD CLASS='TITLE'>Make:</TD>
                  <TD COLSPAN=2><%=b_carmake%></TD>
                </TR>
                <TR> 
                  <TD CLASS='TITLE'>Scheme Name</TD>
                  <TD  style='cursor:hand' title='<%=b_schemedesc%>'><%=b_schemedesccut%></TD>
                  <TD CLASS='TITLE'>Model:</TD>
                  <TD COLSPAN=2><%=b_model%></TD>
                </TR>
                <TR> 
                  <TD CLASS='TITLE'>Salary</TD>
                  <TD><%=b_salarypercent%></TD>
                  <TD CLASS='TITLE'>List Price</TD>
                  <TD COLSPAN=2><%=b_listprice%></TD>
                </TR>
                <TR> 
                  <TD CLASS='TITLE'>Contribution</TD>
                  <TD><%=b_contribution%></TD>
                  <TD CLASS='TITLE'>Hire Charge</TD>
                  <TD COLSPAN=2><%=b_conthirecharge%></TD>
                </TR>
                <TR> 
                  <TD CLASS='TITLE'>AVC</TD>
                  <TD><%=b_avc%></TD>
                  <TD CLASS='TITLE'>Contribution</TD>
                  <TD COLSPAN=2><%=b_empcontribution%></TD>
                </TR>
                <TR> 
                  <TD CLASS='TITLE'>Contract Out</TD>
                  <TD><%=b_contractedout%></TD>
                  <TD CLASS='TITLE'>Excess</TD>
                  <TD COLSPAN=2><%=b_excesscontribution%></TD>
                </TR>
              </TABLE>
              <TABLE HEIGHT=100% WIDTH=100% ID="BEN2" STYLE="BORDER-COLLAPSE:COLLAPSE;DISPLAY:NONE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
                <TR>
                  <TD CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX SOLID'><b>GENERAL</b></TD>
                  <TD WIDTH=160 STYLE='BORDER-BOTTOM:1PX SOLID'></TD>
                  <TD CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX SOLID'><b>CAR USER</b></TD>
                  <TD WIDTH=160 STYLE='BORDER-BOTTOM:1PX SOLID'></TD>
                  <TD ALIGN=RIGHT VALIGN=TOP STYLE='BORDER-BOTTOM:1PX SOLID'> 
                    <img src=images/Up.gif border=0 style="cursor:hand"  title="Toggle Benefit Details" onClick="swap_tables('BEN')" width="17" height="16" align="absbottom"><img src=images/down.gif border=0 style="cursor:hand"  title="Toggle Benefit Details" onClick="swap_tables('NEW')" width="17" height="16" align="absbottom"></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Prof Fees</TD>
                  <TD WIDTH=160><%=b_professionalfees%></TD>
                  <TD CLASS='TITLE'>Car Allowance</TD>
                  <TD COLSPAN=2><%=b_carallowance%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Tel Allowance</TD>
                  <TD><%=b_telephoneallowance%></TD>
                  <TD CLASS='TITLE'>Loan Dat</TD>
                  <TD COLSPAN=2><%=b_carloanstartdate%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>First Aider All</TD>
                  <TD><%=b_firstaidallowance%></TD>
                  <TD CLASS='TITLE'>Engine Size</TD>
                  <TD COLSPAN=2><%=b_enginesize%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Call Out All</TD>
                  <TD><%=b_calloutallowance%></TD>
                  <TD CLASS='TITLE'>Car Loan</TD>
                  <TD COLSPAN=2><%=b_loaninformation%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'></TD>
                  <TD></TD>
                  <TD CLASS='TITLE'>Loan Value</TD>
                  <TD COLSPAN=2><%=b_value%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'></TD>
                  <TD></TD>
                  <TD CLASS='TITLE'>Term</TD>
                  <TD COLSPAN=2><%=b_term%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'></TD>
                  <TD></TD>
                  <TD CLASS='TITLE'>Repayments</TD>
                  <TD COLSPAN=2><%=b_monthlyrepay%></TD>
                </TR>
              </TABLE>
              <TABLE HEIGHT=100% WIDTH=100% ID="NEW1" STYLE="BORDER-COLLAPSE:COLLAPSE;DISPLAY:NONE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
                <TR>
                  <TD CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX SOLID'><b>Accomodation</b></TD>
                  <TD WIDTH=160 STYLE='BORDER-BOTTOM:1PX SOLID'></TD>
                  <TD CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX SOLID'><b>BUPA</b></TD>
                  <TD WIDTH=160 STYLE='BORDER-BOTTOM:1PX SOLID'></TD>
                  <TD ALIGN=RIGHT VALIGN=TOP STYLE='BORDER-BOTTOM:1PX SOLID'> 
                    <img src=images/Up.gif border=0 style="cursor:hand"  title="Toggle Benefit Details" onClick="swap_tables('NEW')" width="17" height="16" align="absbottom"></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Rent</TD>
                  <TD WIDTH=160><%=b_accomodationrent%></TD>
                  <TD CLASS='TITLE'>Ann. Premium</TD>
                  <TD COLSPAN=2><%=b_annualpremium%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Council Tax</TD>
                  <TD><%=b_counciltax%></TD>
                  <TD CLASS='TITLE'>Add. Members</TD>
                  <TD COLSPAN=2><%=b_additionalmembers%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Heating</TD>
                  <TD><%=b_heating%></TD>
                  <TD CLASS='TITLE'>Group Scheme Ref:</TD>
                  <TD COLSPAN=2><%=b_groupschemeref%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>Line Rental</TD>
                  <TD><%=b_linerental%></TD>
                  <TD CLASS='TITLE'>Membership No:</TD>
                  <TD COLSPAN=2><%=b_membershipno%></TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD>&nbsp;</TD>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD COLSPAN=2>&nbsp;</TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD>&nbsp;</TD>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD COLSPAN=2>&nbsp;</TD>
                </TR>
                <TR>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD>&nbsp;</TD>
                  <TD CLASS='TITLE'>&nbsp;</TD>
                  <TD COLSPAN=2>&nbsp;</TD>
                </TR>
              </TABLE>
            </TD>
            <TD WIDTH=30% HEIGHT=50% > <%=str_holiday%> </TD>
          </TR>
          <TR> 
            <TD WIDTH=30% HEIGHT=50% > <%=str_policy%> </TD>
          </TR>
        </TABLE>
      </DIV>
      <BR>
      <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
        <TR> 
          <TD valign="bottom"> <IMG NAME="img8" SRC="images/item-1-open.gif" WIDTH=55 HEIGHT=20 style='cursor:hand' onClick="swap_item_div(8);item_details(1)"></TD>
          <TD valign="bottom"> <IMG NAME="img9" SRC="images/item-2-previous.gif" WIDTH=79 HEIGHT=20 BORDER=0 style='cursor:hand' onClick="swap_item_div(9);checkForm();item_details(0)"></TD>
          <TD valign="bottom"> <IMG NAME="img10" SRC="images/item-3-closed.gif" WIDTH=75 HEIGHT=20 BORDER=0></TD>
          <TD valign="bottom"> 
            <table width="541" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td valign="bottom"> 
                  <table width="541" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="9">&nbsp;</td>
                      <td width="207" align="left"><div name="RSLyear" id="RSLyear" style="display:block"> 
					    <!--<select name="sel_YEAR" id="sel_YEAR" class="textbox100" >
							   <option value="2007">Year 2007</option>	
							   <option value="2006">Year 2006</option>
							   <option value="2005">Year 2005</option>
							   <option value="2004">Year 2004</option>
                      </select>-->
		<%=lst_year %>
					  <input type="BUTTON" name="Submit" value="Filter Year" style="width:70" class="textbox100" onClick="filter_list()">
					 </div>
					  </td>
                      <td width="112" align="right"><div name="RSLnature" id="RSLitem" style="display:block"> <%=lst_itemfilter%></div></td>
                      <td width="213" align="right"> 
                        <div name="RSLnature" id="RSLnature" style="display:block"> 
                          <select name="sel_NATUREFILTER" id="sel_NATUREFILTER" class="textbox200" disabled >
                          </select>
                        </div>
                      </td>
                    </tr>
                    <tr> 
                      <td width="9"></td>
                      <td colspan="3" height="2"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td bgcolor="#000066" height="1"></td>
              </tr>
            </table>
          </TD>
        </TR>
      </TABLE>
      <DIV ID=div8 STYLE='DISPLAY:BLOCK'> 
        <TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
          <TR> 
            <TD><iframe src="iFrames/iEmployeeJournal.asp?employeeid=<%=employee_id%>" name=frm_erm width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
          </TR>
        </TABLE>
      </DIV>
      <DIV ID=div9 STYLE='DISPLAY:NONE'> 
        <TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
          <TR> 
            <TD WIDTH=30% HEIGHT=100% VALIGN=TOP> 
              <!-- NEW iTEM------------------------------------------->
              <TABLE WIDTH=100% BORDER=0>
                <TR> 
                  <TD>Title</TD>
                  <TD>
                    <INPUT TYPE=TXT NAME=txt_TITLE MAXLENGTH="200" SIZE=29 class="textbox200">
                  </TD>
                  <TD><image src="../js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>
                </TR>
                <TR> 
                  <TD>Item</TD>
                  <TD><%=lst_item%></TD>
                  <TD><image src="/js/FVS.gif" name="img_ITEM" width="15px" height="15px" border="0"></TD>
                </TR>
                <TR> 
                  <TD>Nature</TD>
                  <TD>
                    <SELECT NAME='sel_NATURE' class='textbox200'>
                      <option value=''>Select Item</OPTION>
                    </SELECT>
                  </TD>
                  <TD><image src="../js/FVS.gif" name="img_NATURE" width="15px" height="15px" border="0"></TD>
                </TR>
                <TR > 
                  <TD COLSPAN=2 ALIGN=RIGHT>
                    <INPUT TYPE=BUTTON <%=only_allow_line_manager%> NAME=BTN_DATES value='Next' CLASS="RSLBUTTON" ONCLICK=go_nature()>
                  <TD>
                  <TD> 
                </TR>
              </TABLE>
              <!------------------------------------------------------->
            </TD>
            <TD WIDTH=70% HEIGHT=100%> <iframe src="blank.asp" name='frm_nature' width=100% height="100%" frameborder=0 style="border:none"></iframe> 
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </form>
    <!--#include VIRTUAL="INCLUDES/Bottoms/BodyBottom.asp" -->
    <iframe  src="/secureframe.asp" name=frm_erm_srv width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

