<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim employeeId, mon, tue, wed, thu, fri,sat,sun,total
	employeeId = Request("employeeId")

    strSQL = "SELECT MON,TUE,WED,THU,FRI,SAT,SUN,TOTAL FROM E_WORKINGHOURS WHERE EMPLOYEEID=" & employeeId
    Call OpenDB()
    Call OpenRs(rsSet, strSQL)

	If 	rsSet.EOF = False Then
		mon = rsSet("MON")
		tue	= rsSet("TUE")
		wed = rsSet("WED")
		thu	= rsSet("THU")
		fri = rsSet("FRI")
		sat = rsSet("SAT")
		sun = rsSet("SUN")
		total = rsSet("TOTAL")
	End If
	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <title>ERM --> Employee Working Hours</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin-left:30px;
            margin-top:20px;
        }
        .fsize
        {
            font-family: Arial;
            font-size: 13px;
        }
    </style>
</head>
<body>
    <table height="100" width="200" cellpadding="2" cellspacing="0" border="2" bordercolor="gray">
        <tr>
            <th class="fsize" align="center">
                Monday
            </th>
            <th class="fsize" align="center">
                Tuesday
            </th>
            <th class="fsize" align="center">
                Wednesday
            </th>
            <th class="fsize" align="center">
                Thursday
            </th>
            <th class="fsize" align="center">
                Friday
            </th>
            <th class="fsize" align="center">
                Saturday
            </th>
            <th class="fsize" align="center">
                Sunday
            </th>
            <th class="fsize" align="center">
                Total
            </th>
        </tr>
        <tr>
            <td class="fsize" align="center">
                <%=mon %>
            </td>
            <td class="fsize" align="center">
                <%=tue %>
            </td>
            <td class="fsize" align="center">
                <%=wed %>
            </td>
            <td class="fsize" align="center">
                <%=thu %>
            </td>
            <td class="fsize" align="center">
                <%=fri %>
            </td>
            <td class="fsize" align="center">
                <%=sat %>
            </td>
            <td class="fsize" align="center">
                <%=sun %>
            </td>
            <td class="fsize" align="center">
                <%=total %>
            </td>
        </tr>
    </table>
    <table height="40" width="500">
        <tr>
            <td colspan="8" align="center">
                <input type="button" value="Close Window" class="RSLButton" onclick="window.close()" style="cursor:pointer" title="Close Window" />
            </td>
        </tr>
    </table>
</body>
</html>