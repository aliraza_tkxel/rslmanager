<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim a_status, startdate, returndate, certno, notes, drname, reason, lst_action, absence_history_id

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")
	absence_history_id = Request("absencehistoryid")

	If path  = "" Then path = 0 End If

    Call OpenDB()
	If path = 0 Then
		' IF INITIAL ENTRY BUILD SELECT BOX AND RETRIEVE RECORD
		Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (1, 2, 20)", "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", null)
		Call get_record()
	Elseif path = 1 Then
		' OTHERWISE WRITE NEW ABSENCE REOCRD AND UPDATE JOURNAL STATUS
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT 	ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.RETURNDATE, 103) ,'') AS RETURNDATE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS," &_
					"			ISNULL(A.CERTNO, 'N/A') AS CERTNO," &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES," &_
					"			ISNULL(A.DRNAME, 'N/A') AS DRNAME," &_
					"			ISNULL(A.REASON, 'N/A') AS REASON " &_
					"FROM 		E_ABSENCE A " &_
					"			LEFT JOIN E_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID " &_
					"WHERE 		ABSENCEHISTORYID = " & absence_history_id

		Call OpenRs(rsSet, strSQL)

		a_status 	= rsSet("STATUS")
		startdate	= rsSet("STARTDATE")
		returndate 	= rsSet("RETURNDATE")
		certno		= rsSet("CERTNO")
		notes 		= rsSet("NOTES")
		drname 		= rsSet("DRNAME")
		reason 		= rsSet("REASON")

		Call CloseRs(rsSet)

	End Function

	Function new_record()

		strSQL = "SELECT * FROM E_ABSENCE WHERE ABSENCEHISTORYID = " & absence_history_id
		Call OpenRs(rsSet, strSQL)
		    journalid = rsSet("JOURNALID")
		Call CloseRS(rsSet)

		a_status = Request.Form("sel_ACTION")
		actionid = Request.Form("sel_ACTION")
		startdate = DateValue(Request.Form("txt_STARTDATE"))
		returndate = DateValue(Request.Form("txt_RETURNDATE"))
		If actionid = 20 Then 'cancelled
			duration = 0
		Else
			duration = workDays(startdate, returndate)
		End If
		certno = Request.form("txt_CERTNO")
		notes = Request.form("txt_NOTES")
		drname = Request.form("txt_DRNAME")
		reason = Request.form("txt_REASON")

		strSQL = "INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, REASON, NOTES, CERTNO, DRNAME) " &_
					"VALUES (" & journalid & ", " & a_status & ", " & actionid & ", '" & Date & "', " & Session("userid") & ", '" & Request.Form("txt_STARTDATE") & "', '" & Request.Form("txt_RETURNDATE") & "', " & Duration & ", '" & reason & "', '" & notes & "', '" & certno & "', '" & drname & "')"
		set rsSet = Conn.Execute(strSQL)

		' UPDATE JPOURNAL WITH NEW STATUS
		Call update_journal(journalid, a_status)

	End Function

	' UPDATES THE JOURNAL WITH THE NEW STATUS DEPENDENT ON THE ACTION TAKEN
	Function update_journal(jid, j_status)

		strSQL = "UPDATE E_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function
%>
<html>
<head>
    <title>ERM --> Update Absence</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px;
        }
    </style>
</head>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript">

	var FormFields = new Array();
	    FormFields[0] = "txt_REASON|Reason|TEXT|Y"
	    FormFields[1] = "sel_ACTION|Action|SELECT|Y"
	    FormFields[2] = "txt_STARTDATE|Start Date|DATE|Y"
	    FormFields[3] = "txt_RETURNDATE|Return Date|DATE|N"
	    FormFields[4] = "txt_CERTNO|Certificate Number|TEXT|N"
	    FormFields[5] = "txt_DRNAME|Drs Name|TEXT|N"
	    FormFields[6] = "txt_LASTACTIONUSER|Recorded By|TEXT|N"
	    FormFields[7] = "txt_NOTES|Notes|TEXT|N"

	function save_form() {
		if (!checkForm()) return false;
		document.getElementById("hid_go").value = 1;
		document.RSLFORM.submit();
	}

		function return_data() {
		if (<%=path%> == 1)	{
			opener.location.reload();
			window.close();
		}
	}

</script>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="pSickness.asp?absencehistoryid=<%=absence_history_id%>">
    <table style="height:100%" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="67">
                            <img src="Images/tab_leave.gif" width="67" height="20" alt="Leave" />
                        </td>
                        <td height="19">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="images/spacer.gif" height="1" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71'>
                <table cellspacing="1" cellpadding="2" width="90%" align="center" border="0">
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_REASON" id="txt_REASON" value="<%=reason%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                border="0" alt="REASON" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Action:
                        </td>
                        <td>
                            <%=lst_action%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ACTION" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date
                        </td>
                        <td colspan="3">
                            <input type="text" value="<%=startdate%>" name="txt_STARTDATE" class="textbox100"
                                maxlength="10" onblur='check_date()' />
                            <img src="/js/FVS.gif" name="img_STARTDATE" width="15px" height="15px" border="0"
                                alt="" />
                            &nbsp;full<input type="radio" id="radStartFull" name="radStart" value="days" onclick="workDays()"
                                checked="checked" />
                            am<input type="radio" id="radStartAM" name="radStart" value="morning" onclick="workDays()" />
                            pm<input type="radio" id="radStartPM" name="radStart" value="afternoon" onclick="workDays()" />
                        </td>
                    </tr>
                    <tr id="endText" style="visibility: hidden">
                        <td>
                            Return Date
                        </td>
                        <td colspan="3">
                            <input type="text" value="<%=returndate%>" name="txt_RETURNDATE" id="txt_RETURNDATE"
                                class="textbox100" maxlength="10" onblur='check_date()' />
                            <img src="/js/FVS.gif" name="img_RETURNDATE" id="img_RETURNDATE" width="15px" height="15px"
                                border="0" alt="" />
                            &nbsp;full<input type="radio" id="radEndFull" name="radEnd" value="full" onclick="workDays()"
                                checked="checked" />
                            am<input type="radio" id="radEndAM" name="radEnd" value="morning" onclick="workDays()" />
                            pm<input type="radio" id="radEndPM" name="radEnd" value="afternoon" onclick="workDays()" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Cert No:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_CERTNO" id="txt_CERTNO" value="<%=certno%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CERTNO" id="img_CERTNO" width="15px" height="15px"
                                border="0" alt="CERT NO" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Drs Name:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DRNAME" id="txt_DRNAME" value="<%=drname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DRNAME" id="img_DRNAME" width="15px" height="15px"
                                border="0" alt="DR NAME" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Recorded by:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_LASTACTIONUSER" id="txt_LASTACTIONUSER"
                                value="<%=fullname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LASTACTIONUSER" id="img_LASTACTIONUSER" width="15px"
                                height="15px" border="0" alt="LAST ACTION USER" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap">
                            Notes:
                        </td>
                        <td>
                            <textarea rows="6" cols="20" style="border: 1px solid black; overflow: hidden" class="textbox200"
                                name="txt_NOTES" id="txt_NOTES"><%=notes%></textarea>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="NOTES" />
                        </td>
                        <td>
                            <input type="button" value="Save" class="RSLButton" onclick="save_form()" name="button" />
                            <input type="button" value="Close" class="RSLButton" onclick="javascript:self.close()"
                                name="button" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
</body>
</html>