<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim employeeid, path, frmChk, str_chk
	
	employeeid = Request("employeeid")
	path = Request.Form("hd_submit")
	
	OpenDB()
	
	If path = "yes" then 
		frmChk = request.form("checkbox")
		save_shift_detail()
	else
		path = "no"
		fill_chks()
	End If
	
	
	Function fill_chks()
		
		Dim rs, SQL
		str_chk = ""
		SQL = "SELECT * FROM E_SHIFTDETAIL WHERE EMPLOYEEID = " & employeeid
		Call OpenRs(rs, SQL)
		while not rs.EOF
			if str_chk = "" then
				str_chk = str_chk & rs("SHIFT")
			else			
				str_chk = str_chk & "," & rs("SHIFT") 
			end if
			rs.movenext()
		wend
		
		CloseRs(rs)	
		//response.write "<BR> CHECKED ARE :  " & str_chk
	
	End Function
	
	// parse checkbox string then loop through, entering a record for each
	Function save_shift_detail()
		
		Conn.Execute ("DELETE E_SHIFTDETAIL WHERE EMPLOYEEID = " & employeeid)
		arrChk = Split(frmChk, ",")
		
		For Each chk In arrChk
			
			Conn.Execute ("INSERT INTO E_SHIFTDETAIL VALUES (" & employeeid & ",'" & chk & "')")
	
		Next
		
		
	End Function
	CloseDB()
%>
<html>
<head>
<title>part_time days</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

function save_form(){

	RSLFORM.hd_submit.value = "yes"; 
	RSLFORM.submit();
	}
</script> 

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<FORM NAME=RSLFORM METHOD=POST ACTION="POP_UP_JOB_DETAILS.ASP?EMPLOYEEID=<%=EmployeeID%>">
<table border="0" width="325" height="232" >
  <tr> 
    <td colspan="5" bgcolor="#133E71">&nbsp;</td>
  </tr>
  
  <tr > 
    <td width="50" nowrap>&nbsp;</td>
    <td width="79" nowrap><b>Day</b></td>
    <td width="73"> 
      <div align="center"><b>Morning </b></div>
    </td>
    <td width="73"> 
      <div align="center"><b>Afternoon</b></div>
    </td>
    <td width="50">&nbsp;</td>
  </tr>
  <TR STYLE='HEIGHT:1PX'><TD COLSPAN=5 STYLE='BORDER-BOTTOM:1PX SOLID #133E71'></TD></TR>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Monday:</td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=1>
      </div>
    </td>
    <td nowrap> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=2>
      </div>
    </td>
    <td nowrap>&nbsp;</td>
  </tr>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Tuesday</td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=3>
      </div>
    </td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=4>
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Wednesday</td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=5>
      </div>
    </td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=6>
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Thursday</td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=7>
      </div>
    </td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=8>
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Friday</td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=9>
      </div>
    </td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=10>
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Saturday</td>
    <td> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=11>
      </div>
    </td>
    <td width="72"> 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=12>
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td nowrap>&nbsp;</td>
    <td nowrap>Sunday</td>
    <td > 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=13>
      </div>
    </td>
    <td > 
      <div align="center"> 
        <input type="checkbox" name="checkbox" value=14>
      </div>
    </td>
    <td >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" nowrap>&nbsp;</td>
    <td valign="top" nowrap>&nbsp;</td>
    <td>&nbsp; </td>
    <td>&nbsp; </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
   	<td colspan=5 align=center>
        <input type="button" value="Save" class="RSLButton" onClick="save_form()" name="btn_save">
		<input type="button" value="Close" class="RSLButton" onClick="window.close()" name="btn_close">
   </td>
  </tr>
</table>
<input type=hidden name=hd_submit value=0>
</FORM>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
	
	if ('<%=path%>' == 'yes')
		window.close();
	else {
		
		chkString = "<%=str_chk%>";
		//chkString  = new String("6,7");
		
		chkArray = chkString.split(",");
		for (chk in chkArray) {
			RSLFORM.checkbox[(chkArray[chk]-1)].checked = true;
			}
   		}
	
</script> 
