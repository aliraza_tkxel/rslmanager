<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%    
	CONST TABLE_DIMS = "width=""750"" height=""220"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	' id for sql
	Dim employee_id, str_isabsent
	' post variables
	Dim po_startdate, po_fptime, po_probation, po_reviewdate, po_jobtitle, po_refnumber, po_role, po_team, po_hours, po_grade, po_salary
	Dim po_taxoffice, po_taxcode, po_payroll, po_patch, po_linemanager, po_buddy, po_noticeperiod, po_noticedate, po_manager, po_licnumber,lst_year
	Dim po_ninumber, po_fnumber, po_holidays, po_appdate, po_director , po_hrule
	' benefit variables
	'Pension
	Dim	b_memnumber,b_scheme,b_schemedesc,b_schemedesccut,b_salarypercent,b_contribution,b_avc,	b_contractedout ,b_professionalfees ,b_telephoneallowance ,b_firstaidallowance,_
		b_calloutallowance,b_carmake,b_model,b_listprice,b_conthirecharge,b_empcontribution,b_excesscontribution,b_carallowance ,b_carloanstartdate ,_
		b_enginesize, b_loaninformation,b_value,b_term,b_monthlyrepay,b_accomodationrent,b_counciltax ,b_heating,b_linerental,b_annualpremium,b_additionalmembers,_
		b_groupschemeref,b_membershipno

	' next of kin details
	Dim nok_name, nok_street, nok_town, nok_county, nok_postcode, nok_hometel, nok_mobile

	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev

    Call OpenDB()

    ' if request variable then we come from my team else we come from my job
	employee_id = Request("employeeid")
	If employee_id = "" Then
		employee_id = Session("userid")
	End If

    '--=========== Update Time Stamp - Start

    Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE E_EMPLOYEE_AUDIT_GETLASTUPDATED	@EmployeeId = " & employee_id
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

    '--=========== Update Time Stamp - End
    
	' only allow line manager or actual user to create items for this person
	If isLineManager(employee_id, Session("userid")) Or employee_id = Session("userid") Then
		only_allow_line_manager = ""
	Else
		only_allow_line_manager = "disabled"
	End If

	' Allow HR staff as well
	If session("Teamcode") = "HUM" Or session("Teamcode") = "CORP" Then
		only_allow_line_manager = ""
	End If

	session("employeeid") = employee_id ' session variable required to pass emp id to include page
%>
<!--#include virtual="includes/tables/myjob_right_box.asp" -->
<%
	get_main_details()
	build_diff_dis()
	BuildSelect_Year()
	str_isabsent = isAbsent(employee_id)

	Call BuildSelect(lst_item, "sel_ITEM", "E_ITEM", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange='item_change()'")
	Call BuildSelect(lst_itemfilter, "sel_ITEMFILTER", "E_ITEM", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox100", "style='display:block' onchange='nature_change()'")
    Call BuildSelect(lst_reason, "sel_REASON", "E_ABSENCEREASON", "SID, DESCRIPTION", "SID", "Please Select", 0, NULL, "textbox200", "style='display:block'")

	Function isAbsent(int_employeeid)

		Dim str_isabsent 
		str_isabsent = "No"
		SQL = "SELECT J.* FROM 	E__EMPLOYEE E 	INNER JOIN E_JOURNAL J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"AND J.ITEMNATUREID = 1 AND CURRENTITEMSTATUSID = 1 WHERE 	E.EMPLOYEEID = " & int_employeeid
		Call OpenRS(rsSet,SQL)
		if not rsSet.EOF Then
			str_isabsent = "Yes"
		End If
       	isAbsent = str_isabsent

	End Function


	' get employee details
	Function get_main_details()

		Call OpenRs (rsSet, "SELECT * FROM V_E_EMPLOYEEDETAILS WHERE EMPLOYEEID = " & employee_id)
		Call OpenRs (rsSetWork, "SELECT ISNULL(TOTAL,0) AS TOTAL FROM E_WORKINGHOURS WHERE EMPLOYEEID = " & employee_id)
		' postdetails section
		po_startdate	= rsSet("STARTDATE")
		po_fptime		= rsSet("PARTFULLTIME")
		po_probation	= rsSet("PROBATIONPERIOD")
		po_reviewdate	= rsSet("REVIEWDATE")
		po_jobtitle		= rsSet("JOBTITLE")
		po_refnumber	= rsSet("REFERENCENUMBER")
		po_role			= rsSet("ROLE")
		po_team			= rsSet("TEAMNAME")
		If rsSetWork.EOF = False Then
		    po_hours		= rsSetWork("TOTAL")
		Else
		    po_hours		= rsSet("HOURS")
		End If
		po_grade		= rsSet("GRADE")
		po_salary		= rsSet("SALARY")
		po_taxoffice	= rsSet("TAXOFFICE")
		po_taxcode		= rsSet("TAXCODE")
		po_payroll		= rsSet("PAYROLLNUMBER")
		po_patch		= rsSet("PATCH")
		po_linemanager	= rsSet("LINEMANAGER")
		po_buddy		= rsSet("BUDDY")
		po_noticeperiod	= rsSet("NOTICEPERIOD")
		po_noticedate	= rsSet("DATEOFNOTICE")
		po_manager		= rsSet("MANAGER")
		po_licnumber	= rsSet("DRIVINGLICENSENUMBER")
		po_ninumber		= rsSet("NINUMBER")
		po_fnumber		= rsSet("FOREIGNNATIONALNUMBER")
		po_holidays		= rsSet("HOLIDAYENTITLEMENTDAYS") & " days, " & rsSet("HOLIDAYENTITLEMENTHOURS") & " hours"
		po_appdate		= rsSet("APPRAISALDATE")
		po_director		= rsSet("DIRECTOR")
		po_hrule        = rsSet("HRULE")
		'Pension
		b_memnumber 	= rsSet("MEMNUMBER")
		b_scheme 		= rsSet("SCHEME")
		b_salarypercent = FormatNumber(IsNullNumber(rsSet("SALARYPERCENT")),0,-1,0,0)
		b_contribution 	= FormatNumber(IsNullNumber(rsSet("CONTRIBUTION")),2,-1,0,0)
		b_avc 			= FormatNumber(IsNullNumber(rsSet("AVC")),2,-1,0,0)
		b_contractedout = rsSet("CONTRACTEDOUT")
		'General
		b_professionalfees 		= FormatNumber(IsNullNumber(rsSet("PROFESSIONALFEES")),2,-1,0,0)
		b_telephoneallowance 	= FormatNumber(IsNullNumber(rsSet("TELEPHONEALLOWANCE")),2,-1,0,0)
		b_firstaidallowance 	= FormatNumber(IsNullNumber(rsSet("FIRSTAIDALLOWANCE")),2,-1,0,0)
		b_calloutallowance 		= FormatNumber(IsNullNumber(rsSet("CALLOUTALLOWANCE")),2,-1,0,0)
		'Company Car
		b_carmake 				= rsSet("CARMAKE")
		b_model 				= rsSet("MODEL")
		b_listprice 			= FormatNumber(IsNullNumber(rsSet("LISTPRICE")),2,-1,0,0)
		b_conthirecharge 		= FormatNumber(IsNullNumber(rsSet("CONTHIRECHARGE")),2,-1,0,0)
		b_empcontribution 		= FormatNumber(IsNullNumber(rsSet("EMPCONTRIBUTION")),2,-1,0,0)
		b_excesscontribution 	= FormatNumber(IsNullNumber(rsSet("EXCESSCONTRIBUTION")),2,-1,0,0)
		'Car Essential User
		b_carallowance 			= FormatNumber(IsNullNumber(rsSet("CARALLOWANCE")),2,-1,0,0)
		b_carloanstartdate 		= rsSet("CARLOANSTARTDATE")
		b_enginesize 			= FormatNumber(IsNullNumber(rsSet("ENGINESIZE")),0,-1,0,0)
		b_loaninformation 		= rsSet("LOANINFORMATION")
		b_value 				= FormatNumber(IsNullNumber(rsSet("VALUE")),2,-1,0,0)
		b_term 					= FormatNumber(IsNullNumber(rsSet("TERM")),0,-1,0,0)
		b_monthlyrepay 			= FormatNumber(IsNullNumber(rsSet("MONTHLYREPAY")),2,-1,0,0)
		'Accomodation
		b_accomodationrent 		= FormatNumber(IsNullNumber(rsSet("ACCOMODATIONRENT")),2,-1,0,0)
		b_counciltax 			= FormatNumber(IsNullNumber(rsSet("COUNCILTAX")),2,-1,0,0)
		b_heating 				= FormatNumber(IsNullNumber(rsSet("HEATING")),2,-1,0,0)
		b_linerental 			= FormatNumber(IsNullNumber(rsSet("LINERENTAL")),2,-1,0,0)
		'BUPA
		b_annualpremium 		= FormatNumber(IsNullNumber(rsSet("ANNUALPREMIUM")),2,-1,0,0)
		If Not  rsSet("ADDITIONALMEMBERS") = "none" Then
		b_additionalmembers 	= rsSet("ADDITIONALMEMBERS")
		End If
		b_groupschemeref=rsSet("GROUPSCHEMEREF")
		b_membershipno = rsSet("MEMBERSHIPNO")
		' next of kin details
		nok_name		= rsSet("NOK_FULLNAME")
		nok_street		= rsSet("NOK_STREET")
		nok_town		= rsSet("NOK_TOWN")
		nok_county		= rsSet("NOK_COUNTY")
		nok_postcode	= rsSet("NOK_POSTCODE")
		nok_hometel		= rsSet("NOK_HOMETEL")
		nok_mobile		= rsSet("NOK_MOBILE")

		Call CloseRs(rsSet)
		Call CloseRs(rsSetWork)

	End Function

	If (Not isNull(b_scheme) And Not b_scheme = "") Then
		strSQL = 	"SELECT 	ISNULL(SCHEMEDESC, '') AS SCHEMEDESC " &_
					"FROM	  	G_SCHEME G " &_
					"WHERE 		G.SCHEMEID = " & b_scheme 
		Call OpenRs (rsSet, strSQL) 
			b_schemedesc = rsSet("SCHEMEDESC")
			b_schemedesccut = b_schemedesc
			If Len(b_schemedesc) > 14 Then b_schemedesccut = Left(b_schemedesc,14) & "..."
		Call CloseRs(rsSet)
	End If

	Function build_diff_dis()

		cnt = 1
		strSQL = 	"SELECT 	ISNULL(DIS.DESCRIPTION, 'dd') AS DIFFDIS " &_
				"FROM	 	E__EMPLOYEE E " &_
				"			LEFT JOIN E_DIFFDISID D ON E.EMPLOYEEID = D.EMPLOYEEID " &_
				"			LEFT JOIN E_DIFFDIS  DIS ON D.DIFFDIS = DIS.DIFFDISID " &_
				"WHERE 		E.EMPLOYEEID = " & employee_id & " " &_
				"ORDER	 	BY DESCRIPTION "
		Call OpenRs (rsSet, strSQL)
		str_diff_dis = "<table width='100%' height='100%' style='border-collapse:collapse' border='1' cellspacing='0'>" &_
						"<tr><td class='TITLE'>&nbsp;Disability</td></tr>"
		While Not rsSet.EOF
			cnt = cnt + 1
			str_diff_dis = str_diff_dis & 	"<tr>" &_
											"<td>" & rsSet("DIFFDIS") & "</td>" &_
											"</tr>"
			rsSet.movenext()
		Wend
		Call CloseRs(rsSet)

		' fill bottom with rows
		For i = 1 To (TABLE_ROW_SIZE - cnt)
			str_diff_dis = str_diff_dis	 & 	"<tr><td>&nbsp;</td></tr>"
		Next
		str_diff_dis = str_diff_dis	 & 	"</table>"
	End Function

	' Build Option Select for Years
	Function BuildSelect_Year()
	    Dim current_year
	    current_year=Year(date())
	    lst_year ="<select name=""sel_YEAR"" id=""sel_YEAR"" class=""textbox100"">"
	        do while current_year>=2004 
	            lst_year=lst_year & "<option value=""" & current_year & """>Year "  & current_year & " </option>"
	            current_year=current_year-1
	        loop
	    lst_year =lst_year & "</select>"
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
	<meta http-equiv="X-UA-Compatible" content="IE=11" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_TITLE|Title|TEXT|Y"
        FormFields[1] = "sel_ITEM|Item|SELECT|Y"
        FormFields[2] = "sel_NATURE|Nature|SELECT|Y"

        // preload image variables -- global
        var open1, closed1, open2, closed2, previous2, open3, closed3, previous3, open4, closed4, previous4, open5
        var closed5, previous5, open6, closed6, previous6, open7, closed7, previous7

        preloadFlag = false;

        function preloadImages2() {
            if (document.images) {
                open1 = newImage("Images/1-open.gif");
                closed1 = newImage("Images/1-closed.gif");
                open2 = newImage("Images/2-open.gif");
                closed2 = newImage("Images/2-closed.gif");
                previous2 = newImage("Images/2-previous.gif");
                open3 = newImage("Images/3-open.gif");
                closed3 = newImage("images/3-closed.gif");
                previous3 = newImage("images/3-previous.gif");
                open4 = newImage("images/4-open.gif");
                closed4 = newImage("images/4-closed.gif");
                previous4 = newImage("images/4-previous.gif");
                open5 = newImage("images/5-open.gif");
                closed5 = newImage("images/5-closed.gif");
                previous5 = newImage("images/5-previous.gif");
                open6 = newImage("images/6-open.gif");
                closed6 = newImage("images/6-closed.gif");
                previous6 = newImage("images/6-previous.gif");
                open7 = newImage("images/7-open.gif");
                closed7 = newImage("images/7-closed.gif");
                previous7 = newImage("images/7-previous.gif");
                itemopen = newImage("images/item-1-open.gif");
                itemclosed = newImage("images/item-1-closed.gif");
                newopen = newImage("images/item-2-open.gif");
                newclosed = newImage("images/item-2-previous.gif");
                preloadFlag = true;
            }
        }

        // open close item divs and tabs
        function swap_item_div(int_which_one) {
            if (int_which_one == 8) {
                frm_erm.location.href = "/MyJob/iFrames/iEmployeeJournal.asp?employeeid=<%=employee_id%>";
                document.getElementById("div8").style.display = "block";
                document.getElementById("div9").style.display = "none";
                document.getElementById("img8").src = "Images/item-1-open.gif"
                document.getElementById("img9").src = "Images/item-2-previous.gif"
                document.getElementById("img10").src = "Images/item-3-closed.gif"
            }
            else if (int_which_one == 9) {
                document.getElementById("div8").style.display = "none";
                document.getElementById("div9").style.display = "block";
                document.getElementById("img8").src = "Images/item-1-closed.gif"
                document.getElementById("img9").src = "Images/item-2-open.gif"
                document.getElementById("img10").src = "Images/item-3-previous.gif"
            }
            else {
                document.getElementById("img8").src = "Images/item-1-closed.gif"
                document.getElementById("img9").src = "Images/item-2-closed.gif"
                document.getElementById("img10").src = "Images/item-3-open.gif"
            }
        }

        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        function swap_div(int_which_one) {
            var upper = 7;
            var divid = "div" + int_which_one.toString();
            var imgid = "img" + int_which_one.toString();
            // close all other divs and open src div
            for (i = 1; i <= upper; i++)
                document.getElementById("div" + i + "").style.display = "none";
            document.getElementById(divid).style.display = "block";
            // manipulate images
            for (j = 1; j <= upper; j++) {
                document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
            }
            // unless last image in row
            // Amended by Mr C ..... If the person selects skill/quals then only show Health-previous not -closed. 
            // 		-Would have had to change alot of Divs and Images Names otherwise!!!
            if (int_which_one != upper)
                if (int_which_one == 3)
                    document.getElementById("img" + (int_which_one + 2) + "").src = "Images/" + (int_which_one + 2) + "-previous.gif"
                else
                    document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
                document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
            }

            // sawps table that need more than one page to display details
            function swap_tables(str_which_one) {
                if (str_which_one == "NEW") {
                    if (document.getElementById(str_which_one + "1").style.display == "block") {
                        document.getElementById(str_which_one + "1").style.display = "none"
                        document.getElementById("BEN2").style.display = "block"
                    }
                    else {
                        document.getElementById(str_which_one + "1").style.display = "block"
                        document.getElementById("BEN2").style.display = "none"
                    }
                }
                else {
                    if (document.getElementById(str_which_one + "1").style.display == "block") {
                        document.getElementById(str_which_one + "1").style.display = "none"
                        document.getElementById(str_which_one + "2").style.display = "block"
                    }
                    else {
                        document.getElementById(str_which_one + "1").style.display = "block"
                        document.getElementById(str_which_one + "2").style.display = "none"
                    }
                }
            }

            function item_change() {
                document.RSLFORM.target = "frm_erm_srv";
                document.RSLFORM.action = "serverside/ItemChange_srv.asp?empoyeeid=<%=employee_id%>&itemid=" + document.getElementById("sel_ITEM").value;
                document.RSLFORM.submit();
            }

            function nature_change() {
                document.RSLFORM.target = "frm_erm_srv";
                document.RSLFORM.action = "serverside/NatureChange_srv.asp?empoyeeid=<%=employee_id%>&itemid=" + document.getElementById("sel_ITEMFILTER").value;
                document.RSLFORM.submit();
            }

            function check_nature() {
                document.RSLFORM.target = "frm_erm_srv";
                document.RSLFORM.action = "serverside/NatureCheck_srv.asp?empoyeeid=<%=employee_id%>&natureid=" + document.getElementById("sel_NATURE").value;
                document.RSLFORM.submit();
            }

            function go_nature() {
                var nature_id = document.getElementById("sel_NATURE").value;
                var item_id = document.getElementById("sel_ITEM").value;
                var title = document.getElementById("txt_TITLE").value;
                var reason = document.getElementById("sel_REASON").value;
                if (nature_id == 1) {
                    FormFields[0] = "txt_TITLE|Title|TEXT|N"
                    FormFields[3] = "sel_REASON|Reason|SELECT|Y"
                }
                else {
                    FormFields[0] = "txt_TITLE|Title|TEXT|Y"
                    FormFields[3] = "sel_REASON|Reason|SELECT|N"
                }
                if (!checkForm()) return false;
                // for now only allow item 'absence' to be recorded by employees
                if (nature_id == "")
                    frm_nature.location.href = "blank.asp";
                else if (item_id != 1) {
                    if (item_id == 6)
                        frm_nature.location.href = "iFrames/iNotes.asp?employeeid=<%=employee_id%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                    else if (item_id == 7)
                        frm_nature.location.href = "iFrames/iAppraisal.asp?employeeid=<%=employee_id%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                    else if (item_id == 8)
                        frm_nature.location.href = "iFrames/iSMS.asp?employeeid=<%=employee_id%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                    else
                        frm_nature.location.href = "unavailable.asp";
                }
                // you cannot create an absence record if one already exists
                else if ("<%=str_isAbsent%>" == "Yes" && nature_id == 1) {
                    alert("This person is already marked as absent!!")
                    return false;
                }
                else if (nature_id == 47) {
                    frm_nature.location.href = "iFrames/iToilRecorded.asp?employeeid=<%=employee_id%>&hrule=<%=po_hrule%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                }
                else if (nature_id == 48) {
                    frm_nature.location.href = "iFrames/iInternalMeeting.asp?employeeid=<%=employee_id%>&hrule=<%=po_hrule%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                }
                else if (nature_id == 49) {
                    frm_nature.location.href = "iFrames/iTimeOffInLieu.asp?employeeid=<%=employee_id%>&hrule=<%=po_hrule%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                }
                // FOR SICKNESS RECORDS ONLY
                else if (nature_id == 1) {
                    frm_nature.location.href = "iFrames/iSick.asp?employeeid=<%=employee_id%>&hrule=<%=po_hrule%>&natureid=" + nature_id + "&itemid=" + item_id + "&reason=" + reason;
                }
                else {
                    frm_nature.location.href = "iFrames/iGeneral.asp?employeeid=<%=employee_id%>&hrule=<%=po_hrule%>&natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title;
                }
            }

            function blank() {
                frm_nature.location.href = "blank.asp";
            }

            // receives the url of the page to open plus the required width and the height of the popup
            function update_record(str_redir, wid, hig) {
                window.open(str_redir, "display", "width=" + wid + ",height=" + hig + ",left=100,top=200,scrollbars=yes");
            }

            function showEmpHours(empID) {
                window.open("pWorkingHours.asp?employeeid=" + empID, "WorkPattern", "height=200px,width=590px,scrollbars=yes,toolbar=no")
            }

            function openAction(enqID) {
                window.open("ActionPlan.asp?enquiryID=" + enqID, "IAGActionPlan", "height=595px,width=590px,scrollbars=yes,toolbar=no");
            }

            function open_desc() {
                window.open("/myjob/popups/pJob.asp", "display", "width=590,height=595,left=100,top=50,scrollbars=yes,toolbar=no");
            }

            function filter_list() {
                document.RSLFORM.target = "frm_erm";
                document.RSLFORM.action = "/MyJob/iFrames/iEmployeeJournal.asp?employeeid=<%=employee_id%>&natureid=" + document.getElementById("sel_NATUREFILTER").value + "&sel_year=" + document.getElementById("sel_YEAR").value; ;
                document.RSLFORM.submit();
            }

            function item_details(status) {
                if (status == 1) {
                    document.getElementById("RSLitem").style.display = "block"
                    document.getElementById("RSLnature").style.display = "block"
                    document.getElementById("RSLyear").style.display = "block"
                }
                else {
                    document.getElementById("RSLitem").style.display = "none"
                    document.getElementById("RSLnature").style.display = "none"
                    document.getElementById("RSLyear").style.display = "none"
                }
            }
	
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();" onunload="macGo()">
    <form name="RSLFORM" method="post" action="">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="img1" id="img1" src="images/1-open.gif" width="115" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(1)" alt="Personal Details" title="Personal Details" />
            </td>
            <td rowspan="2">
                <img name="img2" id="img2" src="images/2-previous.gif" width="74" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(2)" alt="Contacts" title="Contacts" />
            </td>
            <td rowspan="2">
                <img name="img3" id="img3" src="images/3-closed.gif" width="94" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(3)" alt="Skills/Qualifications" title="Skills/Qualifications" />
            </td>
            <td rowspan="2" style="display: none">
                <img name="img4" id="img4" src="images/4-closed.gif" width="86" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(4)" alt="Previous Employment" title="Previous Employment" />
            </td>
            <td rowspan="2">
                <img name="img5" id="img5" src="images/5-closed.gif" width="70" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(5)" alt="Health" title="Health" />
            </td>
            <td rowspan="2">
                <img name="img6" id="img6" src="images/6-closed.gif" width="52" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(6)" alt="Post" title="Post" />
            </td>
            <td rowspan="2">
                <img name="img7" id="img7" src="images/7-closed.gif" width="82" height="20" border="0"
                    style="cursor: pointer" onclick="swap_div(7)" alt="Benefits" title="Benefits" />
            </td>
            <td>
                <img src="images/spacer.gif" width="260" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133e71" height="1">
                <img src="images/spacer.gif" width="260" height="1" alt="" />
            </td>
        </tr>
    </table>
    <div id="div1" style="display: block; overflow: hidden">
        <!-- PERSONAL DETAILS------------------------------------->
        <iframe name="frm_pd" id="frm_pd"  <%=TABLE_DIMS%> style="overflow: hidden" src="iFrames/iPersonalDetails.asp?employeeid=<%=employee_id%>"
            frameborder="0"></iframe>
        <!-- END OF PERSONAL DETAILS------------------------------------->
    </div>
    <div id="div2" style='display: none'>
        <!-- CONTACTS------------------------------------------->
        <iframe name="frm_contact" <%=TABLE_DIMS%> style="overflow: hidden" src="iFrames/iContact.asp?employeeid=<%=employee_id%>"
            frameborder="0"></iframe>
        <!-- END OF CONTACTS------------------------------------>
    </div>
    <div id="div3" style="display: none">
        <!-- END OF SKILLS / QUALIFICATIONS------------------------------------->
        <iframe name="frm_skills" <%=TABLE_DIMS%> style="overflow: hidden" src="iFrames/iSkills.asp?employeeid=<%=employee_id%>"
            frameborder="0"></iframe>
        <!-- SKILLS / QUALIFICATIONS-------------------------------------------->
    </div>
    <div id="div4" style="display: none">
        &nbsp;
    </div>
    <div id="div5" style="display: none">
        <!-- HEALTH------------------------------------->
        <table <%=TABLE_DIMS%> style="border-right: solid 1px #133e71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr>
                <td rowspan="2" width="70%" height="100%">
                    <table height="100%" width="100%" style="border: solid 1px #133e71; border-collapse: collapse"
                        cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                        <tr>
                            <td class="TITLE">
                                Name
                            </td>
                            <td width="160">
                                <%=nok_name%>
                            </td>
                            <td rowspan="7">
                                <%=str_diff_dis%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Street
                            </td>
                            <td>
                                <%=nok_street%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Town
                            </td>
                            <td>
                                <%=nok_town%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                County
                            </td>
                            <td>
                                <%=nok_county%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Postcode
                            </td>
                            <td>
                                <%=nok_postcode%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Home Tel
                            </td>
                            <td>
                                <%=nok_hometel%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Mobile
                            </td>
                            <td>
                                <%=nok_mobile%>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="30%" height="50%">
                    <%=str_holiday%>
                </td>
            </tr>
            <tr>
                <td width="30%" height="50%">
                    <%=str_policy%>
                </td>
            </tr>
        </table>
    </div>
    <div id="div6" style='display: none'>
        <table <%=TABLE_DIMS%> style="border-right: SOLID 1PX #133E71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr>
                <td rowspan="2" width="70%" height="100%">
                    <table height="100%" width="100%" id="POST1" style="border: SOLID 1PX #133E71; border-collapse: COLLAPSE"
                        cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                        <tr>
                            <td class='TITLE'>
                                Start Date
                            </td>
                            <td width="160">
                                <%=po_startdate%>
                            </td>
                            <td class='TITLE'>
                                Full/Part Time
                            </td>
                            <td width="160">
                                <%=po_fptime%>
                            </td>
                            <td align="right" valign="top">
                                <img src="images/down.gif" border="0" style="cursor: pointer" title="Toggle Post Details"
                                    onclick="swap_tables('POST')" width="17" height="16" align="bottom" alt="Toggle Post Details" />
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Probation
                            </td>
                            <td>
                                <%=po_probation%>
                            </td>
                            <td class='TITLE'>
                                Hours
                            </td>
                            <td colspan="2">
                                <a href="javascript:showEmpHours(<%=employee_id %>)">
                                    <%=po_hours%></a>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Review Date
                            </td>
                            <td>
                                <%=po_reviewdate%>
                            </td>
                            <td class='TITLE'>
                                Grade
                            </td>
                            <td colspan="2">
                                <%=po_grade%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Job Title
                            </td>
                            <td>
                                <%=po_jobtitle%>
                            </td>
                            <td class='TITLE'>
                                Salary
                            </td>
                            <td colspan="2">
                                <%=po_salary%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Ref Number
                            </td>
                            <td>
                                <%=po_refnumber%>
                            </td>
                            <td class='TITLE'>
                                Tax Office
                            </td>
                            <td colspan="2">
                                <%=po_taxoffice%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Details of Role
                            </td>
                            <td>
                                <%=po_role%>
                            </td>
                            <td class='TITLE'>
                                Tax Code
                            </td>
                            <td colspan="2">
                                <%=po_taxcode%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Team
                            </td>
                            <td>
                                <%=po_team%>
                            </td>
                            <td class='TITLE'>
                                Payroll Number
                            </td>
                            <td colspan="2">
                                <%=po_payroll%>
                            </td>
                        </tr>
                    </table>
                    <table height="100%" width="100%" id="POST2" style="border: SOLID 1PX #133E71; border-collapse: COLLAPSE;
                        display: NONE" cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                        <tr>
                            <td class='TITLE'>
                                Patch
                            </td>
                            <td width="160">
                                <%=po_patch%>
                            </td>
                            <td class='TITLE'>
                                Lic Number
                            </td>
                            <td width="160">
                                <%=po_licnumber%>
                            </td>
                            <td align="right" valign="top">
                                <img src="images/up.gif" border="0" style="cursor: pointer" title="Toggle Personal Details"
                                    onclick="swap_tables('POST')" width="17" height="16" align="bottom" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Line Manager
                            </td>
                            <td>
                                <%=po_linemanager%>
                            </td>
                            <td class='TITLE'>
                                NI Number
                            </td>
                            <td colspan="2">
                                <%=po_ninumber%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Buddy
                            </td>
                            <td>
                                <%=po_buddy%>
                            </td>
                            <td class='TITLE'>
                                FN N&deg;
                            </td>
                            <td colspan="2">
                                <%=po_fnumber%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Notice Period
                            </td>
                            <td>
                                <%=po_noticeperiod%>
                            </td>
                            <td class='TITLE'>
                                Appraisal Date
                            </td>
                            <td colspan="2">
                                <%=po_appdate%>
                            </td>
                            <!--
                  <TD CLASS='TITLE'>Holidays</TD>
                  <TD COLSPAN=2><%=po_holidays%></TD>
                  -->
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Date Of notice
                            </td>
                            <td>
                                <%=po_noticedate%>
                            </td>
                            <td class='TITLE'>
                                Director
                            </td>
                            <td colspan="2">
                                <%=po_director%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Manager
                            </td>
                            <td>
                                <%=po_manager%>
                            </td>
                            <td class='TITLE'>
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td>
                            </td>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="30%" height="50%">
                    <%=str_holiday%>
                </td>
            </tr>
            <tr>
                <td width="30%" height="50%">
                    <%=str_policy%>
                </td>
            </tr>
        </table>
    </div>
    <div id="div7" style='display: none'>
        <table <%=TABLE_DIMS%> style="border-right: SOLID 1PX #133E71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr>
                <td rowspan="2" width="70%" height="150">
                    <table height="100%" width="100%" id="BEN1" style="border: SOLID 1PX #133E71; border-collapse: COLLAPSE;
                        display: block" cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                        <tr>
                            <td class='TITLE' style='border-bottom: 1PX SOLID'>
                                <b>PENSION</b>
                            </td>
                            <td style='border-bottom: 1px solid' width="160">
                            </td>
                            <td class='TITLE' style="border-bottom: 1px solid">
                                <b>COMPANY CAR</b>
                            </td>
                            <td style="border-bottom: 1px solid" width="160">
                            </td>
                            <td align="right" valign="top" style="border-bottom: 1px solid; border-left: none">
                                <img src="images/down.gif" border="0" style="cursor: pointer" title="Toggle Benefit Details"
                                    onclick="swap_tables('BEN')" width="17" height="16" align="bottom" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Mem Num:
                            </td>
                            <td>
                                <%=b_memnumber%>
                            </td>
                            <td class='TITLE'>
                                Make:
                            </td>
                            <td colspan="2">
                                <%=b_carmake%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Scheme Name
                            </td>
                            <td style="cursor: pointer" title='<%=b_schemedesc%>'>
                                <%=b_schemedesccut%>
                            </td>
                            <td class='TITLE'>
                                Model:
                            </td>
                            <td colspan="2">
                                <%=b_model%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Salary
                            </td>
                            <td>
                                <%=b_salarypercent%>
                            </td>
                            <td class='TITLE'>
                                List Price
                            </td>
                            <td colspan="2">
                                <%=b_listprice%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Contribution
                            </td>
                            <td>
                                <%=b_contribution%>
                            </td>
                            <td class='TITLE'>
                                Hire Charge
                            </td>
                            <td colspan="2">
                                <%=b_conthirecharge%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                AVC
                            </td>
                            <td>
                                <%=b_avc%>
                            </td>
                            <td class='TITLE'>
                                Contribution
                            </td>
                            <td colspan="2">
                                <%=b_empcontribution%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Contract Out
                            </td>
                            <td>
                                <%=b_contractedout%>
                            </td>
                            <td class='TITLE'>
                                Excess
                            </td>
                            <td colspan="2">
                                <%=b_excesscontribution%>
                            </td>
                        </tr>
                    </table>
                    <table height="100%" width="100%" id="BEN2" style="border: solid 1px #133e71; border-collapse: collapse;
                        display: none" cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                        <tr>
                            <td class='TITLE' style='border-bottom: 1px solid'>
                                <b>GENERAL</b>
                            </td>
                            <td width="160" style='border-bottom: 1px solid'>
                            </td>
                            <td class='TITLE' style='border-bottom: 1px solid'>
                                <b>CAR USER</b>
                            </td>
                            <td width="160" style='border-bottom: 1PX SOLID'>
                            </td>
                            <td align="right" valign="top" style='border-bottom: 1px solid'>
                                <img src="images/Up.gif" border="0" style="cursor: pointer" title="Toggle Benefit Details"
                                    onclick="swap_tables('BEN')" width="17" height="16" align="bottom" alt="" /><img
                                        src="images/down.gif" border="0" style="cursor: pointer" title="Toggle Benefit Details"
                                        onclick="swap_tables('NEW')" width="17" height="16" align="bottom" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Prof Fees
                            </td>
                            <td width="160">
                                <%=b_professionalfees%>
                            </td>
                            <td class='TITLE'>
                                Car Allowance
                            </td>
                            <td colspan="2">
                                <%=b_carallowance%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Tel Allowance
                            </td>
                            <td>
                                <%=b_telephoneallowance%>
                            </td>
                            <td class='TITLE'>
                                Loan Dat
                            </td>
                            <td colspan="2">
                                <%=b_carloanstartdate%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                First Aider All
                            </td>
                            <td>
                                <%=b_firstaidallowance%>
                            </td>
                            <td class='TITLE'>
                                Engine Size
                            </td>
                            <td colspan="2">
                                <%=b_enginesize%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Call Out All
                            </td>
                            <td>
                                <%=b_calloutallowance%>
                            </td>
                            <td class='TITLE'>
                                Car Loan
                            </td>
                            <td colspan="2">
                                <%=b_loaninformation%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                            </td>
                            <td>
                            </td>
                            <td class='TITLE'>
                                Loan Value
                            </td>
                            <td colspan="2">
                                <%=b_value%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                            </td>
                            <td>
                            </td>
                            <td class='TITLE'>
                                Term
                            </td>
                            <td colspan="2">
                                <%=b_term%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                            </td>
                            <td>
                            </td>
                            <td class='TITLE'>
                                Repayments
                            </td>
                            <td colspan="2">
                                <%=b_monthlyrepay%>
                            </td>
                        </tr>
                    </table>
                    <table height="100%" width="100%" id="NEW1" style="border: SOLID 1PX #133E71; border-collapse: COLLAPSE;
                        display: none" cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                        <tr>
                            <td class='TITLE' style='border-bottom: 1PX SOLID'>
                                <b>Accomodation</b>
                            </td>
                            <td width="160" style='border-bottom: 1PX SOLID'>
                            </td>
                            <td class='TITLE' style='border-bottom: 1PX SOLID'>
                                <b>BUPA</b>
                            </td>
                            <td width="160" style='border-bottom: 1PX SOLID'>
                            </td>
                            <td align="right" valign="top" style='border-bottom: 1PX SOLID'>
                                <img src="images/Up.gif" border="0" style="cursor: pointer" title="Toggle Benefit Details"
                                    onclick="swap_tables('NEW')" width="17" height="16" align="bottom" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Rent
                            </td>
                            <td width="160">
                                <%=b_accomodationrent%>
                            </td>
                            <td class='TITLE'>
                                Ann. Premium
                            </td>
                            <td colspan="2">
                                <%=b_annualpremium%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Council Tax
                            </td>
                            <td>
                                <%=b_counciltax%>
                            </td>
                            <td class='TITLE'>
                                Add. Members
                            </td>
                            <td colspan="2">
                                <%=b_additionalmembers%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Heating
                            </td>
                            <td>
                                <%=b_heating%>
                            </td>
                            <td class='TITLE'>
                                Group Scheme Ref:
                            </td>
                            <td colspan="2">
                                <%=b_groupschemeref%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                Line Rental
                            </td>
                            <td>
                                <%=b_linerental%>
                            </td>
                            <td class='TITLE'>
                                Membership No:
                            </td>
                            <td colspan="2">
                                <%=b_membershipno%>
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class='TITLE'>
                                &nbsp;
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="30%" height="50%">
                    <%=str_holiday%>
                </td>
            </tr>
            <tr>
                <td width="30%" height="50%">
                    <%=str_policy%>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="bottom">
                <img name="img8" id="img8" src="images/item-1-open.gif" width="55" height="20" style="cursor: pointer"
                    onclick="swap_item_div(8);item_details(1)" alt="" />
            </td>
            <td valign="bottom">
                <img name="img9" id="img9" src="images/item-2-previous.gif" width="79" height="20"
                    border="0" style="cursor: pointer" onclick="swap_item_div(9);checkForm();item_details(0)"
                    alt="" />
            </td>
            <td valign="bottom">
                <img name="img10" id="img10" src="images/item-3-closed.gif" width="75" height="20"
                    border="0" alt="" />
            </td>
            <td valign="bottom">
                <table width="541" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="bottom">
                            <table width="541" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="9">
                                        &nbsp;
                                    </td>
                                    <td width="207" align="left">
                                        <div id="RSLyear" style="display: block">
                                            <!--<select name="sel_YEAR" id="sel_YEAR" class="textbox100" >
							   <option value="2007">Year 2007</option>	
							   <option value="2006">Year 2006</option>
							   <option value="2005">Year 2005</option>
							   <option value="2004">Year 2004</option>
                      </select>-->
                                            <%=lst_year %>
                                            <input type="button" name="Submit" value="Filter Year" class="textbox100" onclick="filter_list()"
                                                style="cursor: pointer; width: 70px" title="Filter Year" />
                                        </div>
                                    </td>
                                    <td width="112" align="right">
                                        <div id="RSLitem" style="display: block">
                                            <%=lst_itemfilter%></div>
                                    </td>
                                    <td width="213" align="right">
                                        <div id="RSLnature" style="display: block">
                                            <select name="sel_NATUREFILTER" id="sel_NATUREFILTER" class="textbox200" disabled="disabled">
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="9">
                                    </td>
                                    <td colspan="3" height="2">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#000066" height="1">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="div8" style="display: block">
        <table <%=TABLE_DIMS%> style="border-right: SOLID 1PX #133E71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr>
                <td>
                    <iframe src="iFrames/iEmployeeJournal.asp?employeeid=<%=employee_id%>" name="frm_erm"
                        width="100%" height="100%" frameborder="0" style="border: none"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <div id="div9" style="display: none">
        <table <%=TABLE_DIMS%> style="border-right: SOLID 1PX #133E71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr>
                <td width="30%" height="100%" valign="top">
                    <!-- NEW iTEM------------------------------------------->
                    <div>
                        <table border="0">
                            <tr>
                                <td width="100">
                                    Item
                                </td>
                                <td>
                                    <%=lst_item%>
                                </td>
                                <td>
                                    <img src="/js/FVS.gif" name="img_ITEM" id="img_ITEM" width="15px" height="15px" border="0"
                                        alt="" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <table border="0">
                            <tr>
                                <td width="100">
                                    Nature
                                </td>
                                <td>
                                    <div id="divNATURE">
                                        <select name="sel_NATURE" id="sel_NATURE" class="textbox200">
                                            <option value=''>Select Item</option>
                                        </select></div>
                                </td>
                                <td>
                                    <img src="../js/FVS.gif" name="img_NATURE" id="img_NATURE" width="15px" height="15px"
                                        border="0" alt="" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <table border="0">
                            <tr id="RslTitle">
                                <td width="100">
                                    Title
                                </td>
                                <td>
                                    <input type="text" name="txt_TITLE" id="txt_TITLE" maxlength="200" size="29" class="textbox200" />
                                </td>
                                <td>
                                    <img src="../js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px"
                                        border="0" alt="" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <table border="0">
                            <tr id="RslReason" style="display: none">
                                <td width="100">
                                    Reason
                                </td>
                                <td>
                                    <%=lst_reason%>
                                </td>
                                <td>
                                    <img src="../js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                        border="0" alt="" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <input type="button" <%=only_allow_line_manager%> name="BTN_DATES" value='Next' title="Next"
                            class="RSLBUTTON" onclick="go_nature()" style="cursor: pointer; float: right;
                            margin-right: 22px" />
                    </div>
                    <!------------------------------------------------------->
                </td>
                <td width="70%" height="100%">
                    <iframe src="blank.asp" name="frm_nature" id="frm_nature" width="100%" height="100%"
                        frameborder="0" style="border: none"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn%>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
    </form>
    <!--#include VIRTUAL="INCLUDES/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_erm_srv" width="400px" height="400px" style='display: none'>
    </iframe>
</body>
</html>
