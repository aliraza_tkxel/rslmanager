<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION

	ACTION = "NEW"

	EmployeeID = Request("employeeid")
	If (EmployeeID = "") Then EmployeeID = -1 End If

    Call OpenDB()

	SQL = "SELECT * FROM E_QUALIFICATIONSANDSKILLS " &_
			"WHERE E_QUALIFICATIONSANDSKILLS.EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsLoader, SQL)

	SQL = "SELECT * FROM E__EMPLOYEE " &_
			"WHERE E__EMPLOYEE.EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsName, SQL)
	If (NOT rsName.EOF) Then
		l_lastname = rsName("LASTNAME")
		l_firstname = rsName("FIRSTNAME")
		FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"
	End If
	Call CloseRS(rsName)

	strTable = "<table cellspacing=""0"" cellpadding=""1"" style=""behavior:url(/Includes/Tables/tablehl.htc)"" slcolor="""" hlcolor=""STEELBLUE""><thead><tr><td width=""200px""><b>Subject</b></td><td width=""90px""><b>Result</b></td><td width=""80px""><b>Date</b></td></tr><tr><td colspan=""4""><hr style=""border:1px dotted #133E71""></td></td></thead><tbody>"
	If (Not rsLoader.EOF) Then
		while (NOT rsLoader.EOF)
			special = rsLoader("SpecialisedTraining")
			titlebit = ""
			If (special <> "" AND Not isNull(special)) Then
				titlebit = " title='Specialised Training:" & special & "'"
			End If
			strTable = strTable & "<tr style=""cursor:pointer"">"
            strTable = strTable & "<td onclick=""EditMe(" & rsLoader("QUALIFICATIONSID") & ")"" valign=top " & titlebit & ">" & rsLoader("Subject") & "</td>"
			strTable = strTable & "<td valign=""top"">" & rsLoader("RESULT") & "</td>"
			strTable = strTable & "<td valign=""top"">" & rsLoader("QUALIFICATIONDATE") & "</td>"
            strTable = strTable & "<td onclick=""DeleteMe(" & rsLoader("QUALIFICATIONSID") & ")"" title=""Delete"" style=""cursor:pointer""><font color=""red"">DEL</font></td>"
            strTable = strTable & "</tr>"
			rsLoader.moveNext
		wend
	Else
		strTable = strTable & "<tr><td colspan=""3"" align=""center"">No Existing Qualifications\Skills</td></tr>"
	End If
	strTable = strTable & "</tbody><tfoot><tr><td colspan=""4""><hr style=""border:1px dotted #133e71""></td></tr></tfoot></table>"

	Call BuildSelect(lstqualtype, "sel_EXISTPLANNED", "E_QUALIFICATIONTYPE", "QUALIFICATIONTYPEID, QUALIFICATIONTYPEDESC", "QUALIFICATIONTYPEDESC", "Please Select", NULL, NULL, "textbox200", " tabindex='1' " )
	Call BuildSelect(lstquallevel, "sel_QualLevelID", "E_QUALIFICATIONLEVEL", "QualLevelId, QualLevelDesc", "QualLevelSortOrder ASC", "Please Select", NULL, NULL, "textbox200", " tabindex='2' " )
        
    Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL MyJob --> Amend --> Existing Qualifications and Skills</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body {
            background-color: White;
            margin: 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_SUBJECT|Subject|TEXT|Y"
        FormFields[1] = "txt_RESULT|Result|TEXT|Y"
        FormFields[2] = "txt_QUALIFICATIONDATE|Date|DATE|Y"
        FormFields[3] = "txt_SPECIALISEDTRAINING|Specialised Training|TEXT|N"
        FormFields[4] = "txt_PROFESSIONALQUALIFICATION|Professional Qualification|TEXT|N"
        FormFields[5] = "txt_KEYCOMPETENCIES1|Key Competencies|TEXT|N"
        FormFields[6] = "txt_KEYCOMPETENCIES2|Key Competencies|TEXT|N"
        FormFields[7] = "txt_KEYCOMPETENCIES3|Key Competencies|TEXT|N"
        FormFields[8] = "txt_PERSONALBIO|Personal Biol|TEXT|N"
        FormFields[9] = "txt_CPD|CPD|TEXT|N"
        FormFields[10] = "txt_LANGUAGESKILLS|Language Skills|TEXT|N"
        FormFields[11] = "sel_EXISTPLANNED|Qualification Type|SELECT|Y"
        FormFields[12] = "sel_QualLevelID|Qualification Type|SELECT|Y"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.submit()
        }

        function ResetForm() {
            document.getElementById("hid_QUALID").value = ""
            document.getElementById("hid_Action").value = "NEW"
            document.getElementById("myButton").value = " ADD "
            document.RSLFORM.reset()
        }

        function DeleteMe(THE_ID) {
            document.getElementById("hid_QUALID").value = THE_ID
            document.getElementById("hid_Action").value = "DELETE"
            document.RSLFORM.submit()
        }

        function EditMe(THE_ID) {
            document.getElementById("hid_QUALID").value = THE_ID
            document.getElementById("hid_Action").value = "LOAD"
            document.RSLFORM.submit()
        }

    </script>
    <script language="JavaScript" type="text/JavaScript">

        //Function to limit text entered into textarea 
        function limitText(textArea, length) {
            if (textArea.value.length > length) {
                textArea.value = textArea.value.substr(0, length);
            }
        }

    </script>
</head>
<body onload="checkForm()">
    <form name="RSLFORM" method="post" action="../ServerSide/Qualifications_svr.asp"
        target="hiddenFrame">
        <table cellspacing="2" style='border: 1px solid #133E71; width: 90%; height: 90%'
            align="center">
            <tr>
                <td nowrap="nowrap" valign="top">Qualification Type :
                </td>
                <td>
                    <%=lstqualtype%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_EXISTPLANNED" id="img_EXISTPLANNED" width="15px"
                        height="15px" border="0" alt="EXIST PLANNED" />
                </td>
                <td width="100%" rowspan="17" valign="top">
                    <br />
                    <div id="ListRows">
                        <%=strTable%>
                    </div>
                    <div align="right">
                        <input type="button" value="End" title="End" class="RSLBUTTON" onclick="opener.frm_skills.location.reload(); window.close()"
                            tabindex="14" />
                    </div>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Qualification Level :
                </td>
                <td>
                    <%=lstquallevel%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_QualLevelID" id="img_QualLevelID" width="15px"
                        height="15px" border="0" alt=" PLANNED" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">Subject:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_SUBJECT" id="txt_SUBJECT" maxlength="50"
                        value="" tabindex="3" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SUBJECT" id="img_SUBJECT" width="15px" height="15px"
                        border="0" alt="SUBJECT" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">Result:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_RESULT" id="txt_RESULT" maxlength="10"
                        value="" tabindex="2" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_RESULT" id="img_RESULT" width="15px" height="15px"
                        border="0" alt="RESULT" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">Date:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_QUALIFICATIONDATE" id="txt_QUALIFICATIONDATE"
                        maxlength="10" value="" tabindex="3" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_QUALIFICATIONDATE" id="img_QUALIFICATIONDATE" width="15px"
                        height="15px" border="0" alt="QUALIFICATION DATE" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Specialised Training:
                </td>
                <td>
                    <textarea class="textbox200" name="txt_SPECIALISEDTRAINING" id="txt_SPECIALISEDTRAINING"
                        rows="4" cols="20" onkeypress="limitText(this,499);" tabindex="4"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_SPECIALISEDTRAINING" id="img_SPECIALISEDTRAINING"
                        width="15px" height="15px" border="0" alt="SPECIALISED TRAINING" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Professional Qualification :
                </td>
                <td>
                    <input type="text" name="txt_PROFESSIONALQUALIFICATION" id="txt_PROFESSIONALQUALIFICATION"
                        class="textbox200" maxlength="200" tabindex="5" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_PROFESSIONALQUALIFICATION" id="img_PROFESSIONALQUALIFICATION"
                        width="15px" height="15px" border="0" alt="PROFESSIONAL QUALIFICATION" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Key Competencies :
                </td>
                <td>
                    <input type="text" name="txt_KEYCOMPETENCIES1" id="txt_KEYCOMPETENCIES1" class="textbox200"
                        maxlength="200" tabindex="6" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES1" id="img_KEYCOMPETENCIES1" width="15px"
                        height="15px" border="0" alt="KEY COMPETENCIES LINE 1" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">&nbsp;
                </td>
                <td>
                    <input type="text" name="txt_KEYCOMPETENCIES2" id="txt_KEYCOMPETENCIES2" class="textbox200"
                        maxlength="200" tabindex="7" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES2" id="img_KEYCOMPETENCIES2" width="15px"
                        height="15px" border="0" alt="KEY COMPETENCIES LINE 2" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">&nbsp;
                </td>
                <td>
                    <input type="text" name="txt_KEYCOMPETENCIES3" id="txt_KEYCOMPETENCIES3" class="textbox200"
                        maxlength="200" tabindex="8" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES3" id="img_KEYCOMPETENCIES3" width="15px"
                        height="15px" border="0" alt="KEY COMPETENCIES LINE 3" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Personal Bio :
                </td>
                <td>
                    <textarea name="txt_PERSONALBIO" id="txt_PERSONALBIO" rows="4" cols="20" class="textbox200"
                        onkeypress="limitText(this,499);" tabindex="9"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_PERSONALBIO" id="img_PERSONALBIO" width="15px" height="15px"
                        border="0" alt="PERSONAL BIO" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">CPD :
                </td>
                <td>
                    <textarea name="txt_CPD" id="txt_CPD" rows="4" cols="20" class="textbox200" onkeypress="limitText(this,499);"
                        tabindex="10"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_CPD" id="img_CPD" width="15px" height="15px" border="0"
                        alt="CPD" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Language Skills :
                </td>
                <td>
                    <input type="text" name="txt_LANGUAGESKILLS" id="txt_LANGUAGESKILLS" class="textbox200"
                        maxlength="200" tabindex="11" title="LANGUAGE SKILLS" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_LANGUAGESKILLS" id="img_LANGUAGESKILLS" width="15px"
                        height="15px" border="0" alt="LANGUAGE SKILLS" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="hidden" name="hid_EMPLOYEEID" id="hid_EMPLOYEEID" value="<%=EmployeeID%>" />
                    <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                    <input type="hidden" name="hid_QUALID" id="hid_QUALID" value="" />
                    <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />
                    <input type="button" name="myButton" id="myButton" value="Add" class="RSLButton"
                        onclick="SaveForm()" tabindex="12" title="Add/Amend" style="cursor: pointer" />
                    <input type="button" class="RSLButton" value="Reset" onclick="ResetForm()" tabindex="13"
                        title="Reset" style="cursor: pointer" />
                </td>
            </tr>
            <tr style="height: 100%">
                <td style="height: 100%">&nbsp;
                </td>
            </tr>
        </table>
    </form>
    <iframe src="/secureframe.asp" name="hiddenFrame" id="hiddenFrame" style="display: none"></iframe>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
