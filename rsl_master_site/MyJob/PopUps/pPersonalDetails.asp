<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim lstTitles
	Dim lstEthnicities
	Dim lstMaritalStatus
	Dim lstReligion
	Dim lstSexualOrientation

	Dim l_sortcode
		l_sortcode = ""
	Dim FullName
		FullName = "<font color=""red"">New Employee</font>"

	Dim rsEmp, ACTION

	ACTION = "NEW"
	EmployeeID = Request("employeeid")
	If (EmployeeID = "") Then EmployeeID = -1 End If

	SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
	Call OpenDB()
	Call OpenRs(rsLoader, SQL)

	If (NOT rsLoader.EOF) Then

		l_title = rsLoader("TITLE")
		l_lastname = rsLoader("LASTNAME")
		l_firstname = rsLoader("FIRSTNAME")
		l_dob = rsLoader("DOB")
		l_gender = rsLoader("GENDER")
		l_maritalstatus = rsLoader("MARITALSTATUS")
		l_ethnicity = rsLoader("ETHNICITY")	
		l_ethnicityother = rsLoader("ETHNICITYOTHER")
		l_religion = rsLoader("RELIGION")
		l_religionother = rsLoader("RELIGIONOTHER")
		l_sexualorientation = rsLoader("SEXUALORIENTATION")
		l_sexualorientationother = rsLoader("SEXUALORIENTATIONOTHER")
		l_bank = rsLoader("BANK")
		l_sortcode = rsLoader("SORTCODE")
		l_accountnumber = rsLoader("ACCOUNTNUMBER")
		l_accountname = rsLoader("ACCOUNTNAME")
		ACTION = "AMEND"

		FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"

		If NOT isNull(l_sortcode) Then
			If Replace(l_sortcode, " ", "") <> "" Then
				alldata = trim("" & Replace(l_sortcode, " ", "") & "")
				l_sortcode1	=	left(alldata,2)
				l_sortcode2	=	mid(alldata,3,2)
				l_sortcode3	=	right(alldata,2)
			End If
		End If

	End If

	Call CloseRs(rsLoader)
	Call CloseDB()

	Call OpenDB()
		Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "---", l_title, "width:50px", "textbox", "onchange='title_change()' tabIndex='1'")
		Call BuildSelect(lstMaritalStatus, "sel_MARITALSTATUS", "G_MARITALSTATUS", "MARITALSTATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_maritalstatus, NULL, "textbox200", "tabIndex='6'")
		Call BuildSelect(lstEthnicities, "sel_ETHNICITY", "G_ETHNICITY", "ETHID, DESCRIPTION", "DESCRIPTION", "Please Select", l_ethnicity, NULL, "textbox200", "onChange='Disable_Other_Boxes()' tabIndex='7'")
		Call BuildSelect(lstReligion, "sel_RELIGION", "G_RELIGION", "RELIGIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_religion, NULL, "textbox200", "onChange='Disable_Other_Boxes()' tabIndex='8'")
		Call BuildSelect(lstSexualOrientation, "sel_SEXUALORIENTATION", "G_SEXUALORIENTATION", "SEXUALORIENTATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_sexualorientation, NULL, "textbox200", "onChange='Disable_Other_Boxes()' tabIndex='9'")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL MyJob --> Amend --> Personal Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript">

        var isNN = (navigator.appName.indexOf("Netscape") != -1);
        function autoTab(input, len, e) {

            var keyCode = (isNN) ? e.which : e.keyCode;
            var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];

            if (input.value.length >= len && !containsElement(filter, keyCode)) {
                input.value = input.value.slice(0, len);
                input.form[(getIndex(input) + 1) % input.form.length].focus();
            }

            function containsElement(arr, ele) {
                var found = false, index = 0;
                while (!found && index < arr.length)
                    if (arr[index] == ele)
                        found = true;
                    else
                        index++;
                return found;
            }

            function getIndex(input) {
                var index = -1, i = 0, found = false;
                while (i < input.form.length && index == -1)
                    if (input.form[i] == input) index = i;
                    else i++;
                return index;
            }
            return true;
        }

        var FormFields = new Array();
        FormFields[0] = "sel_TITLE|Title|SELECT|N"
        FormFields[1] = "txt_FIRSTNAME|First Name|TEXT|Y"
        FormFields[2] = "txt_LASTNAME|Last Name|TEXT|Y"
        FormFields[3] = "sel_ETHNICITY|Ethnicity|SELECT|Y"
        FormFields[4] = "sel_RELIGION|Religion|SELECT|Y"
        FormFields[5] = "sel_SEXUALORIENTATION|Sexual Orientation|SELECT|Y"
        FormFields[6] = "txt_DOB|Date of Birth|DATE|Y"
        FormFields[7] = "sel_GENDER|Gender|SELECT|N"
        FormFields[8] = "sel_MARITALSTATUS|Marital Status|SELECT|Y"
        FormFields[9] = "txt_ACCOUNTNUMBER|Account Number|INTEGER|N"

        function SaveForm() {

            document.getElementById("txt_SORTCODE").value = document.getElementById("txt_SORTCODE1").value.toString() + document.getElementById("txt_SORTCODE2").value.toString() + document.getElementById("txt_SORTCODE3").value.toString()

            var OptionSelectsArray = new Array("ETHNICITY", "RELIGION", "SEXUALORIENTATION")
            for (i = 0; i < OptionSelectsArray.length; i++) {
                str = "sel_" + OptionSelectsArray[i]
                sub_str = "txt_" + OptionSelectsArray[i] + "OTHER"
                none_option = document.getElementById("" + str + "").options[document.getElementById("" + str + "").selectedIndex].text

                if (none_option.indexOf('Other') > -1) {
                    FormFields[FormFields.length] = sub_str + "|" + OptionSelectsArray[i] + "|TEXT|Y"
                }
            }

            if (!checkForm()) return false;
            if (document.getElementById("txt_SORTCODE").value.length != 6 && document.getElementById("txt_SORTCODE").value.length != 0) {
                ManualError("img_SORTCODE", "The Sort Code must consist of 6 numbers made up of 3 sets of 2.", 0)
                return false;
            }
            if (document.getElementById("txt_ACCOUNTNUMBER").value.length != 8 && document.getElementById("txt_ACCOUNTNUMBER").value.length != 0) {
                ManualError("img_ACCOUNTNUMBER", "The Account Number must consist of 8 numbers.", 0)
                return false;
            }

            document.RSLFORM.submit();
        }

        function title_change() {

            if (document.RSLFORM.sel_TITLE.value == 1)
                document.RSLFORM.sel_GENDER.selectedIndex = 2;
            else if (document.RSLFORM.sel_TITLE.value == 5)
                document.RSLFORM.sel_GENDER.selectedIndex = 0;
            else
                document.RSLFORM.sel_GENDER.selectedIndex = 1;
        }


        function Disable_Other_Boxes() {

            var OptionSelectsArray = new Array("ETHNICITY", "RELIGION", "SEXUALORIENTATION")

            for (i = 0; i < OptionSelectsArray.length; i++) {
                str = "sel_" + OptionSelectsArray[i]
                none_option = document.getElementById("" + str + "").options[document.getElementById("" + str + "").selectedIndex].text

                sub_str = "txt_" + OptionSelectsArray[i] + "OTHER"

                if (none_option.indexOf('Other') > -1) {
                    document.getElementById("" + sub_str + "").disabled = false
                    document.getElementById("" + sub_str + "").style.backgroundColor = "white"
                }
                else {
                    document.getElementById("" + sub_str + "").disabled = true
                    document.getElementById("" + sub_str + "").value = ""
                    document.getElementById("" + sub_str + "").style.backgroundColor = "#AEB3BD"
                }
            }
        }

    </script>
</head>
<body onload="checkForm();Disable_Other_Boxes()">
    <form name="RSLFORM" method="post" action="../ServerSide/personaldetails_svr.asp">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img alt="Personal Details" title="Personal Details" name="E_PD" src="../Images/1-open.gif"
                    width="115" height="20" border="0" /></td>
            <td rowspan="2">
                <img alt="Personal Details" title="Personal Details" name="E_PD" src="../../Customer/Images/TabEnd.gif"
                    width="8" height="20" border="0" /></td>
            <td width="100%" height="19" align="right">
                <%=FullName%></td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img alt="" src="/images/spacer.gif" width="53" height="1" /></td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img alt="" src="/images/spacer.gif" width="53" height="6" /></td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-left: 1px solid #133E71;
        border-bottom: 1px solid #133E71; border-right: 1px solid #133E71">
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="2">
                    <tr>
                        <td nowrap="nowrap" width="110px">
                            Title:&nbsp;<%=lstTitles%>
                            <img alt="TITLE" src="/js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px"
                                border="0" />
                        </td>
                        <td nowrap="nowrap">
                            First Name:
                            <input type="text" class="textbox" name="txt_FIRSTNAME" id="txt_FIRSTNAME" style="width: 132px"
                                maxlength="30" value="<%=l_firstname%>" tabindex="2" />
                        </td>
                        <td>
                            <img alt="FIRSTNAME" src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px"
                                height="15px" border="0" />
                        </td>
                        <td nowrap="nowrap" width="85px">
                            Bank:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_BANK" maxlength="40" value="<%=l_bank%>"
                                tabindex="10" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_BANK" id="img_BANK" width="15px" height="15px" border="0"
                                alt="BANK" />
                        </td>
                        <td width="100%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="30"
                                value="<%=l_lastname%>" tabindex="3" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LASTNAME" id="img_LASTNAME" width="15px" height="15px"
                                border="0" alt="LASTNAME" />
                        </td>
                        <td>
                            Sort Code:
                        </td>
                        <td>
                            <input type="text" class="textbox" name="txt_SORTCODE1" id="txt_SORTCODE1" value="<%=l_sortcode1%>"
                                size="2" maxlength="2" readonly="readonly" tabindex="11" onkeyup="return autoTab(this, 2, event);" />
                            -
                            <input type="text" class="textbox" name="txt_SORTCODE2" id="txt_SORTCODE2" value="<%=l_sortcode2%>"
                                readonly="readonly" size="2" maxlength="2" tabindex="12" onkeyup="return autoTab(this, 2, event);" />
                            -
                            <input type="text" class="textbox" name="txt_SORTCODE3" id="txt_SORTCODE3" value="<%=l_sortcode3%>"
                                readonly="readonly" size="2" maxlength="2" tabindex="13" />
                            <input type="hidden" name="txt_SORTCODE" id="txt_SORTCODE" value="<%=l_sortcode%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_SORTCODE" id="img_SORTCODE" width="15px" height="15px"
                                border="0" alt="SORTCODE" />
                        </td>
                    </tr>
                    <tr>
                        <td height="33">
                            Date of Birth:
                        </td>
                        <td height="33">
                            <input type="text" class="textbox200" name="txt_DOB" id="txt_DOB" value="<%=l_dob%>"
                                tabindex="4" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DOB" id="img_DOB" width="15px" height="15px" border="0"
                                alt="DOB" />
                        </td>
                        <td height="33">
                            Acc Number:
                        </td>
                        <td height="33">
                            <input type="text" class="textbox200" name="txt_ACCOUNTNUMBER" id="txt_ACCOUNTNUMBER"
                                maxlength="10" value="<%=l_accountnumber%>" readonly="readonly" tabindex="14" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ACCOUNTNUMBER" id="img_ACCOUNTNUMBER" width="15px"
                                height="15px" border="0" alt="ACCOUNT NUMBER" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gender:
                        </td>
                        <td>
                            <select class="textbox200" name="sel_GENDER" id="sel_GENDER" tabindex="5">
                                <option value="">Please Select</option>
                                <%		
		            If ACTION = "AMEND" Then
			            if (l_gender = "Female") Then
				            FemaleSelected = " selected"
			            ElseIf (l_gender = "Male") Then
				            MaleSelected = " selected"
			            End If
		            End If
                                %>
                                <option value="Female" <%=FemaleSelected%>>Female</option>
                                <option value="Male" <%=MaleSelected%>>Male</option>
                            </select>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_GENDER" id="img_GENDER" width="15px" height="15px"
                                border="0" alt="GENDER" />
                        </td>
                        <td>
                            Acc Name:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_ACCOUNTNAME" id="txt_ACCOUNTNAME"
                                maxlength="40" value="<%=l_accountname%>" readonly="readonly" tabindex="15" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ACCOUNTNAME" id="img_ACCOUNTNAME" width="15px" height="15px"
                                border="0" alt="ACCOUNT NAME" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Marital Status:
                        </td>
                        <td>
                            <%=lstMaritalStatus%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_MARITALSTATUS" id="img_MARITALSTATUS" width="15px"
                                height="15px" border="0" alt="MARITAL STATUS" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ethnicity:
                        </td>
                        <td>
                            <%=lstEthnicities%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ETHNICITY" id="img_ETHNICITY" width="15px" height="15px"
                                border="0" alt="ETHNICITY" />
                        </td>
                        <td>
                            > Other :
                        </td>
                        <td>
                            <input name="txt_ETHNICITYOTHER" id="txt_ETHNICITYOTHER" type="text" class="textbox200"
                                tabindex="16" maxlength="50" value="<%=l_ethnicityother%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ETHNICITYOTHER" id="img_ETHNICITYOTHER" width="15px"
                                height="15px" border="0" alt="ETHNICITY (OTHER)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Religion:
                        </td>
                        <td>
                            <%=lstReligion%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RELIGION" id="img_RELIGION" width="15px" height="15px"
                                border="0" alt="RELIGION" />
                        </td>
                        <td>
                            > Other :
                        </td>
                        <td>
                            <input name="txt_RELIGIONOTHER" id="txt_RELIGIONOTHER" type="text" class="textbox200"
                                tabindex="17" maxlength="50" value="<%=l_religionother%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RELIGIONOTHER" id="img_RELIGIONOTHER" width="15px"
                                height="15px" border="0" alt="RELIGION (OTHER)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sexual Orientation:
                        </td>
                        <td>
                            <%=lstSexualOrientation%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_SEXUALORIENTATION" id="img_SEXUALORIENTATION" width="15px"
                                height="15px" border="0" alt="SEXUALORIENTATION" />
                        </td>
                        <td>
                            > Other :
                        </td>
                        <td>
                            <input name="txt_SEXUALORIENTATIONOTHER" id="txt_SEXUALORIENTATIONOTHER" type="text"
                                class="textbox200" tabindex="18" maxlength="50" value="<%=l_sexualorientationother%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_SEXUALORIENTATIONOTHER" id="img_SEXUALORIENTATIONOTHER"
                                width="15px" height="15px" border="0" alt="SEXUALORIENTATION (OTHER)" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                        </td>
                        <td align="right">
                            <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                            <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                            <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />                            
                            <input type="button" class="RSLButton" value=" SAVE " onclick="SaveForm()" tabindex="19" />
                            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
