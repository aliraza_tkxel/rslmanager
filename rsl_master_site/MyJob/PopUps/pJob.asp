<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim rsSet, ACTION

	EmployeeID = Session("userid")

	Call OpenDB()

	SQL = 		"SELECT		ISNULL(J.JOBTITLE,'') AS JOBTITLE, " &_
				"			ISNULL(J.DETAILSOFROLE ,'') AS DETAILSOFROLE, " &_
				"			ISNULL(J.SALARY, 0) AS SALARY, " &_
				"			ISNULL(J.GRADE,'') AS GRADE, " &_
				"			ISNULL(E.FIRSTNAME,'') + ' ' + " &_
				"			ISNULL(E.LASTNAME,'') AS FULLNAME, " &_
				"			ISNULL(M.FIRSTNAME,'') + ' ' + " &_
				"			ISNULL(M.LASTNAME,'') AS MANAGER, " &_
				"			ISNULL(T.TEAMNAME,'') AS TEAM, " &_
				"			ISNULL(Q.KEYCOMPETENCIES1,'') AS KEY1, " &_
				"			ISNULL(Q.KEYCOMPETENCIES2,'') AS KEY2, " &_
				"			ISNULL(Q.KEYCOMPETENCIES3,'') AS KEY3 " &_
				"FROM		E_JOBDETAILS J " &_
				"			LEFT JOIN E_QUALIFICATIONSANDSKILLS Q ON J.EMPLOYEEID = Q.EMPLOYEEID " &_
				"			LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID " &_
				"			LEFT JOIN E__EMPLOYEE M ON T.MANAGER = M.EMPLOYEEID " &_
				"			LEFT JOIN E__EMPLOYEE E ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				"WHERE		J.EMPLOYEEID = " & Session("userid")

	Call OpenRs(rsSet, SQL)

	If (NOT rsSet.EOF) Then
		jobtitle 	= rsSet("JOBTITLE")
		role		= rsSet("DETAILSOFROLE")
		salary		= rsSet("SALARY")
		grade		= rsSet("GRADE")
		manager		= rsSet("MANAGER")
		team		= rsSet("TEAM")
		key1		= rsSet("KEY1")
		key2		= rsSet("KEY2")
		key3		= rsSet("KEY3")
		fullname	= rsSet("FULLNAME")
	End If

	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL MyJob --> Job Description</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

        // hide buttons for printing purposes
        function window.onbeforeprint() {
            document.getElementById("btnPrint").style.visibility = "hidden";
            document.getElementById("btnClose").style.visibility = "hidden";
        }

        // show buttons after printing
        function window.onafterprint() {
            document.getElementById("btnPrint").style.visibility = "visible";
            document.getElementById("btnClose").style.visibility = "visible";
        }

    </script>
</head>
<body class="TA">
    <table cellspacing="0" cellpadding="2" style="height: 950px; border: 1px solid #000000;
        border-collapse: collapse" border="1" align="center" width="90%">
        <tr style="height: 10px">
            <td colspan="2" valign="middle">
                <br />
                &nbsp;<input type="button" class="RSLButton" value="Print" onclick="window.print()"
                    title="Print Action Plan" name="btnPrint" id="btnPrint" />
                <input type="button" class="RSLButton" value="Close" onclick="window.close()" title="Close Window"
                    name="btnClose" id="btnClose" />
                <font style="font-size: 15px"><b>Job Description for
                    <%=fullname%></b></font>
            </td>
        </tr>
        <tr style="height: 30px">
            <td>
                <u><b>Job title</b></u>
            </td>
            <td>
                <%=jobtitle%>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <u><b>Role</b></u>
            </td>
            <td valign="top">
                <%=role%>
            </td>
        </tr>
        <tr style="height: 30px">
            <td>
                <u><b>Team</b></u>
            </td>
            <td>
                <%=team%>
            </td>
        </tr>
        <tr style="height: 30px">
            <td>
                <u><b>Manager</b></u>
            </td>
            <td>
                <%=manager%>
            </td>
        </tr>
        <tr style="height: 30px">
            <td>
                <u><b>Salary</b></u>
            </td>
            <td>
                <%=FormatCurrency(salary)%>
            </td>
        </tr>
        <tr style="height: 30px">
            <td>
                <u><b>Grade</b></u>
            </td>
            <td>
                <%=grade%>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <u><b>Key Competency 1</b></u>
            </td>
            <td valign="top">
                <%=key1%>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <u><b>Key Competency 2</b></u>
            </td>
            <td valign="top">
                <%=key2%>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <u><b>Key Competency 3</b></u>
            </td>
            <td valign="top">
                <%=key3%>
            </td>
        </tr>
        <tr style="height: 10px">
            <td valign="top" colspan="2" align="center">
                <u><b>Date:
                    <%=Date%></b></u>
            </td>
        </tr>
    </table>
</body>
</html>
