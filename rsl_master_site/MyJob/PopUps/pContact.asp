<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION

	ACTION = "NEW"
	EmployeeID = Request("employeeid")

	SQL = "SELECT * FROM E_CONTACT WHERE EMPLOYEEID = " & EmployeeID
	Call OpenDB()
    Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		l_address1 = rsLoader("ADDRESS1")
		l_address2 = rsLoader("ADDRESS2")
		l_address3 = rsLoader("ADDRESS3")
		l_postaltown = rsLoader("POSTALTOWN")
		l_county = rsLoader("COUNTY")
		l_postcode = rsLoader("POSTCODE")
		l_hometel = rsLoader("HOMETEL")
		l_workmobile= rsLoader("WORKMOBILE")
		l_mobile = rsLoader("MOBILE")
		l_workdd = rsLoader("WORKDD")
		l_workext = rsLoader("WORKEXT")
		l_homeemail = rsLoader("HOMEEMAIL")
		l_workemail = rsLoader("WORKEMAIL")
		l_emergencycontactname = rsLoader("EMERGENCYCONTACTNAME")
		l_emergencycontacttel = rsLoader("EMERGENCYCONTACTTEL")
		ACTION = "AMEND"
	End If
	Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL MyJob --> Amend --> Contacts</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_ADDRESS1|Address 1|TEXT|Y"
        FormFields[1] = "txt_ADDRESS2|Address 2|TEXT|N"
        FormFields[2] = "txt_ADDRESS3|Address 3|TEXT|N"
        FormFields[3] = "txt_POSTALTOWN|Postal Town|TEXT|N"
        FormFields[4] = "txt_COUNTY|County|TEXT|N"
        FormFields[5] = "txt_POSTCODE|Postcode|POSTCODE|Y"
        FormFields[6] = "txt_HOMETEL|Home Telephone|TELEPHONE|N"
        FormFields[7] = "txt_WORKMOBILE|Work Mobile|TELEPHONE|N"
        FormFields[8] = "txt_MOBILE|Mobile|TELEPHONE|N"
        FormFields[9] = "txt_WORKDD|Work DD|TELEPHONE|N"
        FormFields[10] = "txt_WORKEXT|Work Ext|TELEPHONE|N"
        FormFields[11] = "txt_HOMEEMAIL|Home Email|EMAIL|N"
        FormFields[12] = "txt_WORKEMAIL|Work Email|EMAIL|N"
        FormFields[13] = "txt_EMERGENCYCONTACTNAME|Emergency Contact Name|TEXT|N"
        FormFields[14] = "txt_EMERGENCYCONTACTTEL|Emergency Contact Tel|TELEPHONE|N"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.submit()
        }

    </script>
</head>
<body onload="checkForm()">
    <form name="RSLFORM" method="post" action="../ServerSide/Contact_svr.asp">
    <table cellspacing="0" cellpadding="2" style='border: 1px solid #133E71; width: 90%;
        height: 90%' align="center">
        <tr>
            <td nowrap="nowrap">
                Address 1:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS1" id="txt_ADDRESS1" maxlength="50"
                    value="<%=l_address1%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px"
                    border="0" alt="ADDRESS LINE 1" />
            </td>
            <td nowrap="nowrap" width="85px">
                Work DD:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKDD" id="txt_WORKDD" maxlength="20"
                    value="<%=l_workdd%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKDD" id="img_WORKDD" width="15px" height="15px"
                    border="0" alt="Work Direct Dial" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="90px">
                Address 2:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS2" id="txt_ADDRESS2" maxlength="40"
                    value="<%=l_address2%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px"
                    border="0" alt="ADDRESS LINE 2" />
            </td>
            <td>
                Work Ext:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKEXT" id="txt_WORKEXT" maxlength="20"
                    value="<%=l_workext%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKEXT" id="img_WORKEXT" width="15px" height="15px"
                    border="0" alt="WORK EXTENSION" />
            </td>
        </tr>
        <tr>
            <td width="90px">
                Address 3:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS3" id="txt_ADDRESS3" maxlength="50"
                    value="<%=l_address3%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS3" id="img_ADDRESS3" width="15px" height="15px"
                    border="0" alt="ADDRESS LINE 3" />
            </td>
            <td>
                Home Email:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_HOMEEMAIL" id="txt_HOMEEMAIL" maxlength="120"
                    value="<%=l_homeemail%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HOMEEMAIL" id="img_HOMEEMAIL" width="15px" height="15px"
                    border="0" alt="HOME EMAIL" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Postal Town:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_POSTALTOWN" id="txt_POSTALTOWN" maxlength="40"
                    value="<%=l_postaltown%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSTALTOWN" id="img_POSTALTOWN" width="15px" height="15px"
                    border="0" alt="POSTAL TOWN" />
            </td>
            <td>
                Work Email:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKEMAIL" id="txt_WORKEMAIL" maxlength="120"
                    value="<%=l_workemail%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKEMAIL" id="img_WORKEMAIL" width="15px" height="15px"
                    border="0" alt="WORK EMAIL" />
            </td>
        </tr>
        <tr>
            <td>
                County:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY" maxlength="30"
                    value="<%=l_county%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px"
                    border="0" alt="COUNTY" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="10"
                    value="<%=l_postcode%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px"
                    border="0" alt="POSTCODE" />
            </td>
            <td nowrap="nowrap">
                Emergency Contact Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMERGENCYCONTACTNAME" id="txt_EMERGENCYCONTACTNAME"
                    maxlength="30" value="<%=l_emergencycontactname%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMERGENCYCONTACTNAME" id="img_EMERGENCYCONTACTNAME"
                    width="15px" height="15px" border="0" alt="EMERGENCY CONTACT NAME" />
            </td>
        </tr>
        <tr>
            <td>
                Home Tel:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_HOMETEL" id="txt_HOMETEL" maxlength="20"
                    value="<%=l_hometel%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HOMETEL" id="img_HOMETEL" width="15px" height="15px"
                    border="0" alt="HOME TELEPHONE NUMBER" />
            </td>
            <td nowrap="nowrap">
                Emergency Contact Tel:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMERGENCYCONTACTTEL" id="txt_EMERGENCYCONTACTTEL"
                    maxlength="40" value="<%=l_emergencycontacttel%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMERGENCYCONTACTTEL" id="img_EMERGENCYCONTACTTEL"
                    width="15px" height="15px" border="0" alt="EMERGENCY CONTACT TELEPHONE NUMBER" />
            </td>
        </tr>
        <tr>
            <td>
                Mobile:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_MOBILE" id="txt_MOBILE" maxlength="30"
                    value="<%=l_mobile%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px"
                    border="0" alt="MOBILE" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Work Mobile:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKMOBILE" id="txt_WORKMOBILE" maxlength="30"
                    value="<%=l_workmobile%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKMOBILE" id="img_WORKMOBILE" width="15px" height="15px"
                    border="0" alt="WORK MOBILE" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
            <td align="right">
                <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                <input type="button" name="BtnSave" id="BtnSave" class="RSLButton" value="Save" onclick="SaveForm()" />
                <input type="button" name="BtnClose" id="BtnClose" class="RSLButton" value="Close"
                    tabindex="15" onclick="window.close()" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />                
            </td>
        </tr>
        <tr style="height: 100%">
            <td style="height: 100%">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
