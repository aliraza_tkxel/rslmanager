<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim absence_history_id		' the historical id of the absence record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this absence used to render page, either sickness or otherwise QUERYSTING
	Dim is_sickness				' determines whether this is a sickness record - uses nature_id
	Dim a_status				' status of absence reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of absence reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim startdate
	Dim certno
	Dim notes
	Dim drname
	Dim reason
	Dim duration
	Dim employeeid
	Dim isEmployeeOnly
	Dim holType
    Dim duration_hrs
    Dim new_history_id
    Dim time_off_in_lieu_owed , part_full ,page,starttime,startDateTime

	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")

	absence_history_id 	= Request("absencehistoryid")
	nature_id 			= Request("natureid")
	employeeid			= Request("employeeid")

	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT STARTDATE,ENDDATE FROM dbo.EMPLOYEE_ANNUAL_START_END_DATE(" & employeeid & ")"
	Call OpenRs(rsDays, SQL)
	    emp_year_sdate = rsDays("STARTDATE")
	    emp_year_edate = rsDays("ENDDATE")
	Call CloseRs(rsDays)

	If nature_id = 1 Then is_sickness = True Else is_sickness = False End If

	' only allow line manager to approve holidays and sick leave
	If isLineManager(employeeid, Session("userid")) Then
		isEmployeeOnly = " "
	Else
		isEmployeeOnly = " and EMPLOYEEONLY = 1 "
	End If

	' begin processing
	Call OpenDB()

	If path  = "" then path = 0 end if ' initial entry to page

	If path = 0 Then
		If is_sickness Then
			Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (1, 2, 20)", "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", null)
		Else
			Call BuildSelect(lst_action, "sel_ACTION", "E_ACTION WHERE ITEMACTIONID IN (3, 4, 5, 20) " & isEmployeeOnly, "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, Null, "textbox200", null)
		End If
		Call get_record()
	Elseif path = 1 Then
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT 	ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.RETURNDATE, 103) ,'') AS RETURNDATE, " &_
                    "			ISNULL(CONVERT(VARCHAR(5),CONVERT(DATETIME, A.STARTDATE, 0), 108) ,'') AS STARTTIME, " &_                    
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS," &_
					"			ISNULL(A.CERTNO, 'N/A') AS CERTNO," &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES," &_
					"			ISNULL(A.DRNAME, 'N/A') AS DRNAME," &_
					"			ISNULL(A.REASON, 'N/A') AS REASON, " &_
					"			ISNULL(A.HOLTYPE, 'N/A') AS HOLTYPE, " &_
					"			DURATION , DURATION_HRS " &_
					"FROM 		E_ABSENCE A " &_
					"			LEFT JOIN E_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID " &_
					"WHERE 		ABSENCEHISTORYID = " & absence_history_id

		Call OpenRs(rsSet, strSQL)

		a_status 	= rsSet("STATUS")
		startdate	= rsSet("STARTDATE")
        starttime	= rsSet("STARTTIME")
		certno		= rsSet("CERTNO")
		notes 		= rsSet("NOTES")
		drname 		= rsSet("DRNAME")
		reason 		= rsSet("REASON")
		duration 	= rsSet("DURATION_HRS")
		holType		= rsSet("HOLTYPE")
		duration_hrs 	= rsSet("DURATION_HRS")

		Call CloseRs(rsSet)

	End Function

   Function build_holiday_messages()
		
	page="My Job"
    	strSQL = "EXEC E_HOLIDAY_DETAIL " & session("employeeid") & " ,'" & page & "'"
     
		Call OpenRs (rsHols, strSQL) 
		IF NOT rsHols.EOF Then
	            time_off_in_lieu_owed =  rsHols(6) 
                part_full =  rsHols(7)
		end if
		CloseRS(rsHols)		
	End Function

	Function new_record()

		strSQL = "SELECT J.JOURNALID,J.EMPLOYEEID FROM E_ABSENCE A INNER JOIN E_JOURNAL J ON J.JOURNALID=A.JOURNALID WHERE ABSENCEHISTORYID = " & absence_history_id
		Call OpenRs(rsSet, strSQL)

		journalid = rsSet("JOURNALID")
		employee_id = rsSet("EMPLOYEEID")

		Call CloseRS(rsSet)

		a_status 	= Request.Form("sel_ACTION")
		actionid 	= Request.Form("sel_ACTION")
		startdate 	= DateValue(Request.Form("txt_STARTDATE"))
        startDateTime = FormatDateTime(cDate(Request.Form("txt_STARTDATE") & " " & Request.Form("txt_From")))
		duration 	= Request.form("txt_DURATION")
		certno 		= Replace(Request.form("txt_CERTNO"),"'", " ")
		notes 		= Replace(Request.form("txt_NOTES"),"'", " ")
		drname 		= Replace(Request.form("txt_DRNAME"),"'", " ")
		reason 		= Replace(Request.form("txt_REASON"),"'", " ")
		holtype 	= request.form("holtype")
		Leave_Hrs   = 0

		' RUN THE PROC TO CONVERT THE LEAVE DAYS INTO LEAVE HOURS
		'AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
		AL_HRS = "E_BOOK_ANNUAL_LEAVE_HRS " & employee_id  & ",'" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & FormatDateTime(Request.Form("txt_STARTDATE"),1) & "','" & Request.Form("txt_DURATION") & "'"
	    Call OpenRs (rsLeave_Hrs, AL_HRS)

		    If not rsLeave_Hrs.eof Then
			    Leave_Hrs = rsLeave_Hrs("LEAVE_IN_HRS")
			Else
		        Leave_Hrs = 0
		    End If

		Call CloseRS(rsLeave_Hrs)

		strSQL = 	"SET NOCOUNT ON;" &_
                    "INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION_HRS, REASON, NOTES, CERTNO, DRNAME, HOLTYPE, DURATION) " &_
					"VALUES (" & journalid & ", " & a_status & ", " & actionid & ", '" & Now & "', " & Session("userid") & ", '" & startDateTime & "', '" & Request.Form("txt_STARTDATE") & "', " & duration & ", '" & reason & "', '" & notes & "', '" & certno & "', '" & drname & "', '" & Request.Form("HOLTYPE") & "', 0)" &_
                    " SELECT SCOPE_IDENTITY() AS HISTORYID;"
       
        Set rsSet = Conn.Execute(strSQL)
		    new_history_id = rsSet.fields("HISTORYID").value
		    rsSet.close()
		Set rsSet = Nothing

		Call update_journal(journalid, a_status)

        ' if approved or declined then send email
        If (actionid = "4" Or actionid = "5") Then
            call sendEmail(journalid,new_history_id,employee_id)
        End If

	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = "UPDATE E_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function

    Function sendEmail(journalid,new_history_id,employee_id)

    sql =   " SELECT ITEMACTIONID,LASTACTIONUSER,STARTDATE,RETURNDATE,DURATION_HRS FROM E_ABSENCE WHERE ABSENCEHISTORYID = " & new_history_id
    Call OpenRs(rsEmpRec, sql)
    If NOT rsEmpRec.EOF Then
        action = rsEmpRec("ITEMACTIONID")
        userId = rsEmpRec("LASTACTIONUSER")
        sdate = rsEmpRec("STARTDATE")
        duration =  rsEmpRec("DURATION_HRS")
    End If
    Call CloseRs(rsEmpRec)

    SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT C WHERE EMPLOYEEID = " & employee_id
	Call OpenRs(rsEmail, SQL)

	If NOT rsEmail.EOF And NOT rsEmail.BOF Then
		' send email
		Dim strRecipient, emailSubject, strSender
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

        SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT WHERE EMPLOYEEID = " & userId
        Call OpenRs(rsSenderEmail, SQL)

        If NOT rsSenderEmail.EOF And NOT rsSenderEmail.BOF Then 
            strSender=(rsSenderEmail.Fields.Item("WORKEMAIL").Value)
        End if
        Call CloseRs(rsSenderEmail)

        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        If action ="5" then
            emailbody = "Your " & duration &  " hour(s) leave for the date " & sdate & " has been approved."
            emailSubject = "Leave Approved"
        elseif action="4" then
            emailbody = "Your " & duration &  " hour(s) leave for the date " & sdate & " has been declined."
            emailSubject = "Leave Declined"
        end if

        On Error Resume NEXT        
        Set iMsg.Configuration = iConf
        iMsg.To = strRecipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send
        
        If Err.number <> 0 Then
            If strRecipient = "" Or IsNull(strRecipient) Then
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed due to manager email address not provided, so please inform the concerned person.');</script>") 
            Else  
                Response.Write("<script language=""javascript"">alert('Your leave request has been added successfully.\n However its email notification is failed, so please inform the concerned person.');</script>")
            End If  
            Err.Clear()
            ON ERROR GOTO 0
        End If

	End If
	Call CloseRs(rsEmail)

    End Function
%>
<html>
<head>
    <title>ERM --> Update Absence</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/holidayfunctions.js"></script>
    <script type="text/javascript" language="JavaScript">

	var FormFields = new Array();
	    FormFields[0] = "txt_REASON|REASON|TEXT|Y"
	    FormFields[1] = "txt_STARTDATE|STARTDATE|DATE|Y"
	    FormFields[2] = "txt_DURATION|DURATION|TEXT|Y"
	    FormFields[3] = "txt_RECORDEDBY|RECORDEDBY|TEXT|Y"
	    FormFields[4] = "sel_ACTION|ACTION|SELECT|Y"
	    FormFields[5] = "txt_NOTES|Notes|TEXT|N"
        FormFields[6] = "txt_From|From|TIME|Y"

	function save_form(){
		if (!checkForm()) return false;
        if (!validate_duration()) return false;

		document.getElementById("hid_go").value = 1;
		document.RSLFORM.action = "pTimeOffInLieuLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>";
		document.RSLFORM.target = ""
		document.RSLFORM.submit();
	}

	function return_data(){	
		if (<%=path%> == 1)	{
			opener.location.reload();
            opener.parent.document.getElementById("frm_pd").contentWindow.location.reload();
			window.close();
			}	
	}
	
	// global variables
	var Eleavedur, Etype
	
	// initialises fields from server to be used by client
	function initFields() {
    	Eleavedur = <%=duration%>;
		Etype = new String('<%=HolType%>');
	}

	function check_date(){
		if (!checkForm()) return false;
		calLeaveDuration();
	}
	
	function calLeaveDuration()
	{
	    if (!checkForm()) return false;
	    var emp = "<%= employee_id%>";
	    
	    if (emp == '') {
	        emp = "<%= employeeid%>";
	    }

		var sdate = document.getElementById("txt_STARTDATE").value;
		var edate = document.getElementById("txt_STARTDATE").value;
		document.RSLFORM.action="iFrames/iEmpWorkDays.asp?employeeid=" + emp + "&sdate=" + sdate + "&edate=" + edate
	    document.RSLFORM.target="serverFrame"
	    document.RSLFORM.submit();
	}

    function validate_duration() { 
    
        var timeOffInLieuOwed = parseInt( <%=time_off_in_lieu_owed%> ) ;
        var duration = parseInt(document.getElementById("txt_DURATION").value);

        if(duration > timeOffInLieuOwed)
        {
           alert("You have exceeded the number of hours you are currently eligible for 'Time Off In Lieu'.");
           return false;
        }

        if(duration <= 0 || duration >12)
        {
           alert("Invalid duration, maximum 12 hours are allowed.");
           return false;
        }

        return true;
        
    }

    </script>
</head>
<body onload="return_data()">
    <form name="RSLFORM" method="post" action="pTimeOffInLieuLeave.asp?employeeid=<%=employeeid%>&absencehistoryid=<%=absence_history_id%>">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="67">
                            <img src="Images/tab_leave.gif" width="67" height="20" alt="Leave" />
                        </td>
                        <td height="19" width="8061">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71" width="8061">
                            <img src="images/spacer.gif" height="1" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71'>
                <table cellspacing="1" cellpadding="2" width="90%" align="center" border="0">
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_REASON" id="txt_REASON" value="<%=reason%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                border="0" alt="REASON" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="21">
                            Action
                        </td>
                        <td height="21">
                            <%=lst_action%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ACTION" id="img_ACTION" width="15px" height="15px"
                                border="0" alt="ACTION" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date
                        </td>
                        <td>
                            <input type="text" value="<%=startdate%>" name="txt_STARTDATE" id="txt_STARTDATE"
                                class="textbox100" maxlength="10" onblur="check_date()" />
                            &nbsp;
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            From
                        </td>
                        <td >
                            <input name="txt_From" id="txt_From" type="text" maxlength="5" class="textbox50" value="<%=starttime%>"
                               />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_From" id="img_From" width="15px" height="15px" border="0"
                                alt="Start Time" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            Duration
                        </td>
                        <td>
                            <input name="txt_DURATION" id="txt_DURATION" type="text" class="TEXTBOX100" 
                                value="<%=duration%>" />
                            &nbsp hour(s)
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DURATION" id="img_DURATION" width="15px" height="15px"
                                border="0" alt="DURATION" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Recorded By
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=fullname%>"
                                readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="RECORDED BY" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Notes
                        </td>
                        <td>
                            <textarea style="overflow: hidden" rows="7" cols="20" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES"><%=notes%></textarea>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="NOTES" />
                        </td>
                        <td>
                            <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value='Save'
                                class="RSLButton" style="cursor: pointer" />
                            <input type="hidden" name="dates" id="dates" value="<% =BANK_HOLIDAY_STRING%>" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="holtype" id="holtype" value="<%=HolType%>" />
                            <input type="hidden" name="hid_non_work_days" id="hid_non_work_days" value="0" />
                            <input type="hidden" name="hid_STARTDATE" id="hid_STARTDATE" value="<%=emp_year_sdate%>" />
                            <input type="hidden" name="hid_ENDDATE" id="hid_ENDDATE" value="<%=emp_year_edate%>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    </form>
    <iframe name="serverFrame" width="200" height="200" style="display: none"></iframe>
</body>
</html>
