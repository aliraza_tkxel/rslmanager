<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

' PAGE DOES NOT APPEAR TO BE IN USE. FURTHER CHECKS REQUIRED.
' IF NO LONGER IN USE PLEASE DELETE FROM FILE SYSTEM. A COPY WILL BE HELD IN SVN

	Dim item_id, str_nature, lst

	item_id = Request("itemid")
	mysql = "SELECT ITEMNATUREID, DESCRIPTION FROM E_NATURE WHERE ITEMID = " & item_id
	Call OpenDB()
    Call build_select(str_nature, mysql)
	Call CloseDB()

	Function build_select(ByRef lst, ftn_sql)

		Call OpenRs(rsSet, ftn_sql)
		lst = "<select name='sel_NATURE' id='sel_NATURE' class='textbox200' onchange='change_nature()'>"
		lst = lst & "<option value=''>Please select</option>"
		int_lst_record = 0
		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet("ITEMNATUREID") & "'>" & rsSet("DESCRIPTION") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = "<select name='sel_NATURE' id='sel_NATURE' class='textbox200'><option value=''>No Records Exist</option></select>"
		End If

		'response.write lst
	End Function
%>
<html>
<head>
    <title>RSL MyJob --> Nature Change</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("divNATURE").innerHTML = "<%=str_nature%>";
        }
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
