<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim nature_id
	nature_id = Request("natureid")
%>
<html>
<head>
    <title>RSL MyJob --> Nature Check</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">
        function ReturnData(){
        // if Sickness
        <% If (nature_id = 1) Then %>
        {
            parent.document.getElementById("RslReason").style.display = "block"
            parent.document.getElementById("RslTitle").style.display = "none"
        }
        <% Else %>
        {
            parent.document.getElementById("RslReason").style.display = "none"
            parent.document.getElementById("RslTitle").style.display = "block"
        }
       <% End If %>
	
	    }
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>