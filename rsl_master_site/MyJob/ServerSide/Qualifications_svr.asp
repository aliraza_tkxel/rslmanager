<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	Dim strTable		' Table of Qualifications AS STRING
	Dim strfrmflds		' Rs fields returned with data AS STRING
	Dim DataFields   (14)
	Dim DataTypes    (14)
	Dim ElementTypes (14)
	Dim FormValues   (14)
	Dim FormFields   (14)
	UpdateID	  = "hid_EMPLOYEEID"
	UpdateID2     = "hid_QUALID"
	FormFields(0) = "txt_SUBJECT|TEXT"
	FormFields(1) = "txt_RESULT|TEXT"
	FormFields(2) = "txt_QUALIFICATIONDATE|DATE"
	FormFields(3) = "txt_SPECIALISEDTRAINING|TEXT"
	FormFields(4) = "hid_EMPLOYEEID|INT"
	FormFields(5) = "txt_PROFESSIONALQUALIFICATION|TEXT"
	FormFields(6) = "txt_KEYCOMPETENCIES1|TEXT"
	FormFields(7) = "txt_KEYCOMPETENCIES2|TEXT"
	FormFields(8) = "txt_KEYCOMPETENCIES3|TEXT"
	FormFields(9) = "txt_PERSONALBIO|TEXT"
	FormFields(10) = "txt_CPD|TEXT"
	FormFields(11) = "txt_LANGUAGESKILLS|TEXT"
	FormFields(12) = "sel_EXISTPLANNED|EXISTPLANNED|INTEGER|N"
    FormFields(13) = "hid_LASTACTIONUSER|INT"
    FormFields(14) = "sel_QualLevelID|QualLevelID|INTEGER|N"    
        
	Function NewRecord()
        
		Call MakeInsert(strSQL)
		SQL = "INSERT INTO E_QUALIFICATIONSANDSKILLS " & strSQL & ""
		Conn.Execute SQL, recaffected
        
		Call GO()
	End Function


	Function AmendRecord()
		ID = Request.Form("hid_QUALID")
		Call MakeUpdate(strSQL)
		SQL = "UPDATE E_QUALIFICATIONSANDSKILLS " & strSQL & ",LASTACTIONTIME = GETDATE() WHERE QUALIFICATIONSID = " & ID
		Conn.Execute SQL, recaffected
        
		Call GO()
	End Function


	Function DeleteRecord()
		ID = Request.Form("hid_QUALID")
        SQL = "UPDATE E_QUALIFICATIONSANDSKILLS SET LASTACTIONUSER = " & Session("UserId") & ", LASTACTIONTIME = GETDATE() WHERE QUALIFICATIONSID = " & ID & " "
		SQL = SQL & " DELETE FROM E_QUALIFICATIONSANDSKILLS WHERE QUALIFICATIONSID = " & ID
		Conn.Execute SQL, recaffected
                
		Call GO()
	End Function
    
	Function GO()
		Call CloseDB()
		Call LoadPreviousData()
	End Function


	Function LoadPreviousData()
	ID = Request.Form(UpdateID)
	Call OpenDB()
	SQL = "SELECT * FROM E_QUALIFICATIONSANDSKILLS WHERE EMPLOYEEID = " & ID
	Call OpenRs(rsLoader, SQL)
	strTable = "<table cellspacing=""0"" cellpadding=""1"" style=""behavior:url(/Includes/Tables/tablehl.htc)"" slcolor="" hlcolor=""STEELBLUE""><thead><tr><td width=""200px""><b>Subject</b></td><td width=""90px""><b>Result</b></td><td width=""80px""><b>Date</b></td></tr><tr><td colspan=""4""><hr style=""border:1px dotted #133E71""></td></td></thead><tbody>"
	If (not rsLoader.EOF) Then
		While (NOT rsLoader.EOF)
			special = rsLoader("SpecialisedTraining")
			titlebit = ""
			If (special <> "" AND NOT isNull(special)) Then
				titlebit = " title='Specialised Training: " & special & "'"
			End If
			strTable = strTable & "<tr style=""cursor:pointer"">"
            strTable = strTable & "<td onclick=""EditMe(" & rsLoader("QUALIFICATIONSID") & ")"" valign=top " & titlebit & ">" & rsLoader("Subject") & "</td>"
			strTable = strTable & "<td valign=""top"">" & rsLoader("RESULT") & "</td>"
			strTable = strTable & "<td valign=""top"">" & rsLoader("QUALIFICATIONDATE") & "</td>"
            strTable = strTable & "<td onclick=""DeleteMe(" & rsLoader("QUALIFICATIONSID") & ")"" title=""Delete"" style=""cursor:pointer""><font color=""red"">DEL</font></td>"
            strTable = strTable & "</tr>"
			rsLoader.moveNext
		Wend
	Else
		strTable = strTable & "<tr><td colspan=""3"" align=""center"">No Qualifications\Skills</td></tr>"
	End If
	strTable = strTable & "</tbody><tfoot><tr><td colspan=""4""><hr style=""border:1px dotted #133e71""></td></tr></tfoot></table>"
	Call CloseRs(rsLoader)
	End Function


	Function LoadRecord()
		ID = Request.Form("hid_QUALID")
		SQL = "SELECT QUALIFICATIONSID, EMPLOYEEID, SUBJECT, RESULT, QUALIFICATIONDATE, REPLACE(SPECIALISEDTRAINING, Char(13) + Char(10), '\r') AS SPECIALISEDTRAINING, PROFESSIONALQUALIFICATION,KEYCOMPETENCIES1,KEYCOMPETENCIES2,KEYCOMPETENCIES3, PERSONALBIO, REPLACE(CPD, Char(13) + Char(10), '\r') AS CPD, LANGUAGESKILLS, EXISTPLANNED, LASTACTIONUSER, QualLevelID FROM E_QUALIFICATIONSANDSKILLS WHERE QUALIFICATIONSID = " & ID
		Call OpenRs(rsLoaded, SQL)
		strfrmflds = ""
		For i = 0 To UBound(FormFields)
			FormSplit = Split(FormFields(i), "|")
			FormSecondSplit = Split(FormSplit(0), "_")
			DataFields(i) = FormSecondSplit(1)
			strfrmflds = strfrmflds & "parent.document.getElementById('" & FormSplit(0) & "').value = '" & rsLoaded(DataFields(i)) & "';" & vbNewLine
		Next
		strfrmflds = strfrmflds & "parent.document.getElementById('" & UpdateID2 & "').value = '" & ID & "';" & vbNewLine
		strfrmflds = strfrmflds & "parent.document.getElementById('hid_Action').value = 'AMEND';" & vbNewLine
		strfrmflds = strfrmflds & "parent.document.getElementById('myButton').value = 'AMEND';" & vbNewLine
        strfrmflds = strfrmflds & "parent.document.getElementById('hid_LASTACTIONUSER').value = '" & Session("UserId") & "';" & vbNewLine
		strfrmflds = strfrmflds
		Call GO()
	End Function

	TheAction = Request("hid_Action")

	Call OpenDB()
	Select Case TheAction
		Case "NEW"		Call NewRecord()
		Case "AMEND"	Call AmendRecord()
		Case "DELETE"   Call DeleteRecord()
		Case "LOAD"	    Call LoadRecord()
	End Select
	Call CloseDB()
%>
<html>
<head>
    <title>RSL MyJob --> Amend --> Existing Qualifications and Skills</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

	function ReturnData(){	
		parent.document.getElementById("ListRows").innerHTML = document.getElementById("ListRows").innerHTML;
		parent.ResetForm();
		<%= strfrmflds %>
	}

    </script>
</head>
<body onload="ReturnData()">
    <div id="ListRows">
        <%=strTable%></div>
</body>
</html>
