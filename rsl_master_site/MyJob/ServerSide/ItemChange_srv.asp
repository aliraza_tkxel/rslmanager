<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim item_id, str_nature, lst, employee_id, accesStr

	item_id = Request("itemid")
	employee_id = Request("empoyeeid")
	response.write item_id

	' only allow line manager to create sick items for this person
	If isLineManager(employee_id, Session("userid")) Then 
		accesStr = ""
	Else
		accesStr = " AND EMPLOYEEONLY = 1 "
	End If

	If Session("TeamCode") = "HUM" Then
		accesStr = ""
	End If
    	
	If item_id = "" Then item_id = 0 End If	
	mysql = "SELECT ITEMNATUREID, DESCRIPTION FROM E_NATURE WHERE ITEMID = " & item_id & " " & accesStr & " ORDER BY DESCRIPTION"
	Call OpenDB()
    Call build_select(str_nature, mysql)
	Call CloseDB()

	Function build_select(ByRef lst, ftn_sql)
        
		Call OpenRs(rsSet, ftn_sql)
		lst = "<select name='sel_NATURE' id='sel_NATURE' class='textbox200' onChange='check_nature()'>"
		lst = lst & "<option value=''>Please select</option>"
		int_lst_record = 0
		While (NOT rsSet.EOF)
        
                lst = lst & "<option value='" & rsSet("ITEMNATUREID") & "'>" & rsSet("DESCRIPTION") & "</option>"
		        rsSet.MoveNext()
		        int_lst_record = int_lst_record + 1 
	    			
		Wend
		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = "<select name='sel_NATURE' id='sel_NATURE' class='textbox200'><option value=''>Select Item</option></select>"
		End If

		'response.write lst
	End Function
%>
<html>
<head>
    <title>RSL MyJob --> Nature</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function ReturnData() {
            parent.document.getElementById("divNATURE").innerHTML = "<%=str_nature%>";
            parent.frm_nature.location.href = "../blank.asp";
        }

    </script>
</head>
<body onload="ReturnData()">
</body>
</html>