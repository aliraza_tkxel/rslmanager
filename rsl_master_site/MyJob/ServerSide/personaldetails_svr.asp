<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	Dim DataFields   (16)
	Dim DataTypes    (16)
	Dim ElementTypes (16)
	Dim FormValues   (16)
	Dim FormFields   (16)
		UpdateID	  = "hid_EmployeeID"
		FormFields(0) = "sel_TITLE|INT"
		FormFields(1) = "txt_FIRSTNAME|TEXT"
		FormFields(2) = "txt_LASTNAME|TEXT"
		FormFields(3) = "txt_BANK|TEXT"
		FormFields(4) = "txt_DOB|DATE"
		FormFields(5) = "txt_SORTCODE|TEXT"
		FormFields(6) = "sel_GENDER|TEXT"
		FormFields(7) = "txt_ACCOUNTNUMBER|TEXT"
		FormFields(8) = "txt_ACCOUNTNAME|TEXT"
		FormFields(9) = "sel_MARITALSTATUS|TEXT"
		FormFields(10) = "sel_ETHNICITY|INT"
		FormFields(11) = "sel_RELIGION|INT"
		FormFields(12) = "sel_SEXUALORIENTATION|INT"
		FormFields(13) = "txt_ETHNICITYOTHER|TEXT"
		FormFields(14) = "txt_RELIGIONOTHER|TEXT"
		FormFields(15) = "txt_SEXUALORIENTATIONOTHER|TEXT"
        FormFields(16) = "hid_LASTACTIONUSER|INT"        

	Call OpenDB()
	Call AmendRecord()
	Call CloseDB()

	Function AmendRecord()

		ID = Request.Form(UpdateID)
		Call MakeUpdate(strSQL)
		SQL = "UPDATE E__EMPLOYEE " & strSQL & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID        
		Conn.Execute SQL, recaffected

	End Function
%>
<html>
<head>
    <title>RSL MyJob --> Update Personal Details</title> 
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.opener.frm_pd.location.reload();
            parent.window.close();
        }
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
