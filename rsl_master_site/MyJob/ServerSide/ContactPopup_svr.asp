<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	Dim DataFields   (16)
	Dim DataTypes    (16)
	Dim ElementTypes (16)
	Dim FormValues   (16)
	Dim FormFields   (16)
	UpdateID	  = "hid_EmployeeID"
	FormFields(0) = "txt_ADDRESS1|TEXT"
	FormFields(1) = "txt_ADDRESS2|TEXT"
	FormFields(2) = "txt_ADDRESS3|TEXT"
	FormFields(3) = "txt_POSTALTOWN|TEXT"
	FormFields(4) = "txt_COUNTY|TEXT"
	FormFields(5) = "txt_POSTCODE|TEXT"
	FormFields(6) = "txt_HOMETEL|TEXT"
	FormFields(7) = "txt_MOBILE|TEXT"
	FormFields(8) = "txt_WORKMOBILE|TEXT"
	FormFields(9) = "txt_WORKDD|TEXT"
	FormFields(10) = "txt_WORKEXT|TEXT"
	FormFields(11) = "txt_HOMEEMAIL|TEXT"
	FormFields(12) = "txt_WORKEMAIL|TEXT"
	FormFields(13) = "txt_EMERGENCYCONTACTNAME|TEXT"
	FormFields(14) = "txt_EMERGENCYCONTACTTEL|TEXT"
	FormFields(15) = "hid_EMPLOYEEID|INT"
    FormFields(16) = "hid_LASTACTIONUSER|INT"    

	Function NewRecord ()
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO E_CONTACT " & strSQL & ""
		Conn.Execute SQL, recaffected        
	End Function

	Function AmendRecord()
		ID = Request.Form(UpdateID)
		Call MakeUpdate(strSQL)
		SQL = "UPDATE E_CONTACT " & strSQL & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
		Conn.Execute SQL, recaffected        
	End Function

	Function DeleteRecord(Id, theID)
		ID = Request.Form(UpdateID)
		SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
		Conn.Execute SQL, recaffected
	End Function

	TheAction = Request("hid_Action")

	Call OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	Call CloseDB()
%>
<html>
<head>
    <script type="text/javascript" language="JavaScript">

        function ReturnData() {
            parent.updateDetails();
        }

    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
