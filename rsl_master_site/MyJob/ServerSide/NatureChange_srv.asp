<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim nature_id, str_nature, lst, employee_id, accesStr

	nature_id = Request("natureid")
	employee_id = Request("empoyeeid")

	' only allow line manager to create sick items for this person
	If isLineManager(employee_id, Session("userid")) Then
		accesStr = ""
	Else
		accesStr = " and EMPLOYEEONLY = 1 "
	End If

	If Session("TeamCode") = "HUM" Then
		accesStr = ""
	End If

	If nature_id = "" Then nature_id = 0 End If
	mysql = "SELECT ITEMNATUREID, DESCRIPTION FROM E_NATURE WHERE ITEMID = " & nature_id & " " & accesStr & " ORDER BY DESCRIPTION"
	Call OpenDB()
    Call build_select(str_nature, mysql)
	Call CloseDB()

	Function build_select(ByRef lst, ftn_sql)

		Call OpenRs(rsSet, ftn_sql)
		lst = "<select name='sel_NATUREFILTER' id='sel_NATUREFILTER' class='textbox200' onChange='filter_list()'>"
		lst = lst & "<option value=''>Please select</option>"
		int_lst_record = 0
		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet("ITEMNATUREID") & "'>" & rsSet("DESCRIPTION") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		Call CloseRs(rsSet)
		lst = lst & "</select>"
		If int_lst_record = 0 then
			lst = "<select name='sel_NATUREFILTER' id='sel_NATUREFILTER' class='textbox200'><option value=''>Select Item</option></select>"
		End If

		'response.write lst
	End Function
%>
<html>
<head>
    <title>RSL MyJob --> Nature</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript">
    function ReturnData() {

        //parent.document.getElementById("reason").style.display = "block"
        //parent.RSLFORM.sel_NATUREFILTER.outerHTML = "<%=str_nature%>";
        //parent.frm_nature.location.href = "../blank.asp";

    }
</script>
<body onload="ReturnData()">
</body>
</html>