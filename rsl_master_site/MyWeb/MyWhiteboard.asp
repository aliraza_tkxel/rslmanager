<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim left_inc
	left_inc = Session("TeamCode")
	left_inc = ltrim(rtrim(left_inc))

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager --> My Whiteboard</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {
	color: #133E71;
	font-weight: bold;
}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<script language="JavaScript">
function cC(itemRef, theCol){
	if (itemRef.style.color == "") 
		itemRef.style.color = theCol;
	else
		itemRef.style.color = "";	
}

function load_MPIData(){
<% if Session("TeamCode") <> "SUP" then %>
	MPI_loadbutton.innerHTML = "Loading MPI Data ....."
	RSLFORM2.action = "include/MPIRight.asp"
	RSLFORM2.target = "MPIData"
	RSLFORM2.submit()
<%end if%>
}

</script>
<style TYPE="text/css">
	<!--
		a {text-decoration: none !important;}
		a:active {text-decoration: none !important;}
		a:hover {text-decoration: none !important;}
	-->
</STYLE>
<BODY BGCOLOR=#FFFFFF ONLOAD="preloadImages();initSwipeMenu(0)" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopWhiteboard.asp" -->
<div id=TheWholeBody>
<TABLE HEIGHT=100% CELLSPACING=0 CELLPADDING=0>
	<TR>
		<TD WIDTH=12PX HEIGHT=100% VALIGN=TOP nowrap>&nbsp;</TD>
	  	<TD WIDTH=173PX HEIGHT=100% VALIGN=TOP nowrap> 
        <!-- START OF LEFT SIDE BAR -->
	    <% If left_inc = "DAM" Then %>
			<!--#include virtual="myweb/include/etLeft.asp" -->
		<% ElseIf left_inc = "EXEC" Then %>
			<!--#include virtual="myweb/include/extLeft.asp" -->
		<% ElseIf left_inc = "FINA" Then %>
			<!--#include virtual="myweb/include/ftLeft.asp" -->
		<% ElseIf left_inc = "GNHM" Then %>
			<!--#include virtual="myweb/include/htLeft.asp" -->
		<% ElseIf left_inc = "SUP" Then %>	
			<!--#include virtual="myweb/include/crLeft.asp" -->
		<% Else %>
			<!--#include virtual="myweb/include/hrLeft.asp" -->
		<% End If %>
        <!-- END OF LEFT SIDE BAR -->
      	</TD>
      	<TD valign=top width=349px nowrap>
		
		<!-- START OF MAIN BODY NEWS -->						
		<!--#include file="myNews.asp" -->
		<!-- END OF MAIN BODY NEWS -->

		<!--#include file="include/findcoll.asp" -->

		</TD>
		<TD>
			<IMG SRC="/myImages/spacer.gif" WIDTH=1 HEIGHT=17>
		</TD>
		<TD valign=top width=100% style='border-left:1px solid #133e71' bgcolor="#f8f6f6">
			<form method="post" name="RSLFORM2">
			<table height=100%  CELLSPACING=0 CELLPADDING=0>
			<tr><td valign=top>
			<!-- START OF TASKS AND MESSAGES -->
			<!--#include file="TasksAndMessages.asp" -->
			<!-- END OF TASKS AND MESSAGES-->
			</td></tr>
			<tr><td valign=bottom>
			<% if Session("TeamCode") <> "SUP" then%>
			<!-- START OF TASKS AND MESSAGES -->
			<div id="MPI_loadbutton" style="display:block" >&nbsp;<img src="../myImages/View_MPIs.gif" style="cursor:hand" onClick="load_MPIData()"></div>
			<div class="style1" id="MPILoadingText" style="display:block" name ="MPILoadingText"></div>
			<iframe name="MPIData" src="/dummy.asp" height="260" width="239" scrolling="no" frameborder="0">Loading MPI Data .....</iframe>
			<!-- END OF TASKS AND MESSAGES-->
			<% end if %>
			</td></tr>
			</table>
			</form>
			</TD>
		</TR>
	</TABLE>
</div>
<!--#include virtual="Includes/Bottoms/BodyBottomWhiteboard.asp" -->
<!--iframe name=hiddenWhiteboardServer src="blankRefresher.asp" style="display:none"></iframe-->
</BODY>
</HTML>