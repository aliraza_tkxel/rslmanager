<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, repair_history_id
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		ISNULL(CONVERT(NVARCHAR, R.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			ISNULL(R.NOTES, 'N/A') AS NOTES, " &_
					"			R.TITLE, R.REPAIRHISTORYID, O.NAME, " &_
					"			S.DESCRIPTION AS STATUS " &_					
					"FROM		C_REPAIR R " &_
					"			LEFT JOIN C_STATUS S ON R.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN S_ORGANISATION O ON R.CONTRACTORID = O.ORGID " &_
					"WHERE		R.JOURNALID = " & journal_id &_
					"ORDER 		BY REPAIRHISTORYID DESC "
		
		//response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR valign=top STYLE='COLOR:GRAY'>"
			else
				str_journal_table = str_journal_table & 	"<TR valign=top>"
				repair_history_id = rsSet("REPAIRHISTORYID")
			End If
				str_journal_table = str_journal_table & 	"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & rsSet("TITLE") & "</TD>" &_
															"<TD>" & rsSet("NOTES") & "</TD>" &_
															"<TD>" & rsSet("NAME")  & "</TD>" &_
															"<TD>" & rsSet("STATUS")  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager My Job -- > Employment Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_repair(int_repair_history_id){
		var str_win
		str_win = "../PopUps/pRepair.asp?repairhistoryid="+int_repair_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=280, left=200,top=200") ;
	}

</SCRIPT>


<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="parent.swap_div(12, 'bottom')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=7 ALIGN=RIGHT>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE = Update onclick='update_repair(<%=repair_history_id%>)'>
	</TD></TR>
	<TR valign=top>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=80PX>Date</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150PX>Title</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=200PX>Notes</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Contractor</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120PX>Status</TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	