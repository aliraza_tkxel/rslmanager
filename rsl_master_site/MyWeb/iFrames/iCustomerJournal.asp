<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, customer_id, str_journal_table
	
	OpenDB()
	customer_id = Request("customerid")
	
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT 	DISTINCT J.JOURNALID, " &_
					"			CASE J.ITEMID WHEN 1 THEN 'iRepairDetail.asp' " &_
					"			WHEN 3 THEN 'iPropertyDetail.asp'  " &_
					"			WHEN 4 THEN 'iTenantDetail.asp'  " &_
					"			WHEN 5 THEN 'iGeneralDetail.asp'  " &_
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			ISNULL(I.DESCRIPTION, 'N/A') AS ITEM, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			ISNULL(J.TITLE, 'N/A') AS TITLE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			J.ITEMNATUREID, J.CUSTOMERID " &_
					"FROM	 	C_JOURNAL J " &_
					"			LEFT JOIN C_ITEM I 	ON J.ITEMID = I.ITEMID " &_
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID, C_REPAIR R  " &_
					"WHERE	 	J.JOURNALID = R.JOURNALID AND R.CONTRACTORID = " & SESSION("ORGID") & " " &_
					"ORDER		BY J.JOURNALID DESC"
		//response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			str_journal_table = str_journal_table & 	"<TR valign=top ONCLICK='open_me(" & rsSet("ITEMNATUREID") & "," & rsSet("JOURNALID") & ",""" & rsSet("REDIR") & """)' STYLE='CURSOR:HAND'>" &_
															"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & rsSet("ITEM") & "</TD>" &_
															"<TD>" & rsSet("NATURE") & "</TD>" &_
															"<TD>" & rsSet("TITLE")  & "</TD>" &_
															"<TD>" & rsSet("STATUS")  & "</TD>" &_
															"<TD style='background-color:#FFFFFF'><a href=""#"" onclick=""PopWindow(" & rsSet("CUSTOMERID") & ")""><img src=""/myImages/info.gif"" border=0></a></TD>" &_															
														"<TR>"
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function open_me(int_nature, int_journal_id, str_redir){
	parent.frm_erm.location.href = str_redir + "?journalid="+int_journal_id+"&natureid="+int_nature;
}

function PopWindow(CustomerID){
	event.cancelBubble = true
	window.open("/MyWeb/PopUps/Repair_Details.asp?CustomerID=" + CustomerID, "RepairDetails", "width=400,height=360")
	}	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=90><B><FONT COLOR='BLUE'>Date</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B><FONT COLOR='BLUE'>Item</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B><FONT COLOR='BLUE'>Nature</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID"><B><FONT COLOR='BLUE'>Title</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120><B><FONT COLOR='BLUE'>Status</FONT></B></TD>
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	