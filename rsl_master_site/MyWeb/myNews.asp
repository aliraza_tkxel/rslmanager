<%
Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

Dim upperbound ' used to specify the random images on the news, when no news exists.
upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
set rsMyNews = Server.CreateObject("ADODB.Recordset")
rsMyNews.ActiveConnection = RSL_CONNECTION_STRING
rsMyNews.Source = "SELECT TOP 1 NEWSID, CONTENT, TITLE, NEWSIMAGE FROM G_NEWS WHERE NEWSFOR LIKE '%," & Session("TeamCode") & ",%' ORDER BY DATECREATED DESC, NEWSID DESC"
rsMyNews.CursorType = 0
rsMyNews.CursorLocation = 2
rsMyNews.LockType = 3
rsMyNews.Open()
rsMyNews_numRows = 0
%>
<link rel="stylesheet" href="../css/RSL.css" type="text/css">

	<table valign=top cellspacing=0 cellpadding=0 width=324>
		<TR>
			<TD BGCOLOR=#FFFFFF colspan=2><IMG SRC="/myweb/images/broadland+news.gif" WIDTH=324 HEIGHT=18></TD>
		</TR>
		<TR>
			<TD colspan=2 valign=top>
				<table cellspacing=0 cellpadding=0 width=324 style='background-color:#133E71;color:white'>
					<tr>
						<td BGCOLOR=#FFFFFF><img src='/myImages/spacer.gif' width="324" height="8"></td>
					</tr>
					<tr>
						<td BGCOLOR=#133e71><img src='/myImages/spacer.gif' width="324" height="8"></td>
					</tr>
					<tr>
						<td valign=top class="iagManagerSmallBlk">
							<table cellspacing=0 cellpadding=0 width=324 style='background-color:#133E71;color:white'>
								<tr>
									<td height=125px>&nbsp;&nbsp;</td>
									<td>
										<div style='width:310px;height:124px;overflow:hidden'>
											<% if (NOT rsMyNews.EOF) Then 
													tempdata = rsMyNews("NewsImage") & ""
													if (rsMyNews("NewsImage") <> "") Then
														Response.Write "<image src='/News/NewsImages/" & rsMyNews("NewsImage") & "' width='100' height='100' hspace=0 valign=top align=right>"
													Else
														Randomize
														temp = Int((upperbound) * Rnd + 1)
														Response.Write "<image src='/myImages/news_image" & temp & ".gif' width='100px' height='100px' hspace=0 valign=top align=right alt='Default News Image'>"
													End If
												Else 
													Randomize
													temp = Int((upperbound) * Rnd + 1)
													%>
													<img src='/myImages/news_image<%=temp%>.gif' width="100" height="100px" valign=top align=right vspace=0 hspace=0 alt='Default News Image'>
													<% 
														End If 
													%>
													<img src='/myImages/spacer.gif' width="10" height="100" valign=top align=right vspace=0 hspace=0>			  
													<%
														if (NOT rsMyNews.EOF) Then
															Response.Write "<b><font style='font-size:14px'>" & rsMyNews("Title") & "</font><br><br>"
															NewsContent = "" & rsMyNews("Content") & ""
															NewsContent = Left(stripHTML(NewsContent),200) & " ...."
															Response.Write "<font style='font-size:10px' class='RSLWhite'>" & NewsContent & "</font>"
														Else
													%>
													  <font style='font-size:14px'>Main Headline</font><br>
													  <br>
													  <font style='font-size:10px'>
													  RSL Manager delivers efficiency and performance to financial control, 
													  quality, communications, partnership development, organisation workflow 
													  and people management. The enhancement of the Partnerships management 
													  of processes will ultimately increase your responsiveness to client 
													  and member needs. </font>
													<% End If %>
										</div>
									</td>
								</tr>
								<tr>
          							<td valign=center colspan=2>
									<%  if (NOT rsMyNews.EOF) Then %>
										<br><a href='/News/NewsItem.asp?NewsItem=<%=rsMyNews("NEWSID")%>'><i>
										<font color=white onmouseover="cC(this,'beige')" onmouseout="cC(this, 'white')" style='font-size:10px' >
										&nbsp;&nbsp;Click to find out more ></font></i></a>
									<%  Else %>
										<br><a href='/News/News.asp'><i>
										<font color=white onmouseover="cC(this,'beige')" onmouseout="cC(this, 'white')" style='font-size:10px' >
										&nbsp;&nbsp;View other news items ></font></i></a>									
									<%  End if %>
									</td>
								</tr>
								<tr>
									<td BGCOLOR=#133e71 colspan=2><img src='/myImages/spacer.gif' width="324" height="2"></td>
								</tr>	
								<tr>
									<td colspan=2>
									<a href='/News/NewsItem_Add.asp'>
									<font color=white onmouseover="cC(this,'beige')" onmouseout="cC(this, 'white')" style='font-size:10px'><i>
									&nbsp;&nbsp;Send in your news...click here ></i></font></a>
									</TD>
								</tr>
								<tr>
									<td BGCOLOR=#133e71 colspan=2><img src='/myImages/spacer.gif' width="324" height="10"></td>
								</tr>
							</table>
						</td>												
					</tr>
				</table>	
	</TD></TR>
	<TR>
		<TD class="iagManagerSmallBlk" colspan=2 height=17px>

		</td>
	</TR>	
	<%
		rsMyNews.close()
		Set rsMyNews = Nothing
	%>
	</table>