<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%


Dim dayToDayCompletedAverage,dayToDayReported,dayToDayCompleted
Dim voidValues
Dim newTenancies_generalneeds, terminations_generalneeds, newTenancies_sheltered, terminations_sheltered,newTenancies_supported, terminations_supported
Dim ArrearsCount,ArrearsValue,VoidRentValue

Dim fiscalyear 

fiscalyear = "8"
Redim  voidValues(3)
Redim dayToDayReportedAverage(3)
Redim dayToDayReported(3)
Redim dayToDayCompleted(3)
OpenDB()

	today = FormatDateTime(Date, 1)
		SQL = "SELECT YRange FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
		Call OpenRs(rsSet, SQL)
			If Not rsSet.EOF Then 
				fiscalyear = rsSet("YRange") 
			Else fiscalyear = 8 ' in case there is an error lets not break the page.
			End IF
		CloseRs(rsSet)

	Function loadVoids()
	
		Dim vrSubscr
		' FIRST OF ALL GET THE COUNT OF VOID RANGES TO BE USED AS SUBSCRIPT FOR ARRAYS
		SQL = "SELECT COUNT(*) AS NUM FROM MPI_VOIDRANGE" 
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then vrSubscr = rsSet("NUM") Else vrSubscr = 0 End IF
		CloseRs(rsSet)
	
		' BUILD AND RESET VOID ARRAY
		ReDim voidValues(vrSubscr)
		For x = 0 To vrSubscr
			voidValues(x) = 0
		Next
		
		SQL = " EXEC MPI_2_VOIDS '', '', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF 
			voidValues(rsSet("RANGEID")) = rsSet("NUM")
			rsSet.movenext()
		Wend
		CloseRs(rsSet)		
	
	End Function
	
	Function loadCustomers()
	
		SQL = " EXEC MPI_2_NEWTENANCIES '', '', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			newTenancies		= rsSet("NEWTENANCIES")
			terminations		= rsSet("TERMINATIONS")
		Else
			newTenancies		= 0
			terminations		= 0
		End If
		CloseRs(rsSet)
	End Function 
	
	'' this can be deleted if after the date 27 september 2006
	Function loadCustomers_sheltered()
	
		SQL = " EXEC MPI_2_NEWTENANCIES '', '11', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			newTenancies_sheltered		= rsSet("NEWTENANCIES")
			terminations_sheltered		= rsSet("TERMINATIONS")
		Else
			newTenancies_sheltered		= 0
			terminations_sheltered		= 0
		End If
		CloseRs(rsSet)
	End Function
	
	Function loadCustomers_supported()
	
		SQL = " EXEC MPI_2_NEWTENANCIES '', '2', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			newTenancies_supported	= rsSet("NEWTENANCIES")
			terminations_supported	= rsSet("TERMINATIONS")
		Else
			newTenancies_supported	= 0
			terminations_supported	= 0
		End If
		CloseRs(rsSet)
	End Function
	
	Function loadCustomers_generalneeds()
	
		SQL = " EXEC MPI_2_NEWTENANCIES '', '1', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			newTenancies_generalneeds	= rsSet("NEWTENANCIES")
			terminations_generalneeds	= rsSet("TERMINATIONS")
		Else
			newTenancies_generalneeds	= 0
			terminations_generalneeds	= 0
		End If
		CloseRs(rsSet)
	End Function


	Function loadDayToDay()
	
		' FIRST SET ALL VALUES IN ARRAY TO ZERO
		For x = 0 To 3
			dayToDayReported(x) = 0
			dayToDayReportedAverage(x) = 0
			dayToDayCompleted(x) = 0
		Next
		
		' RESET TOTAL VARIABLES
		dayToDayReportedTotal = 0
		dayToDayCompletedTotal = 0

		
		' GET DATA FOR REPORTED REPAIRS
		SQL = " EXEC MPI_1_DAYTODAY_REPAIRS_REPORTED '', '', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			dayToDayReported(rsSet("SORTORDER")) = rsSet("LOGGED")
			dayToDayReportedTotal = dayToDayReportedTotal + rsSet("LOGGED")
			rsSet.movenext()
		Wend
		Call CloseRs(rsSet)
		
		' CALCULATE AVERAGES IF TOTAL NOT ZERO
		If dayToDayReportedTotal <> 0 Then
			For x = 0 To 3
				dayToDayReportedAverage(x) = dayToDayReported(x)/dayToDayReportedTotal*100
			Next
		End If
	
	End Function

	Function Arrears()
	
		SQL = " EXEC MPI_4_ARREARS '', '', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			ArrearsCount = rsSet(0)
			ArrearsValue = rsSet(1)
		End If
		Call CloseRs(rsSet)
		
		SQL = " EXEC MPI_4_RENTINCOME '', '', '', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		Set rsSet = rsSet.NextRecordSet()
			VoidRentValue = rsSet("AVAILABLE")
		Call CloseRs(rsSet)
	End Function
	
			
	loadVoids()
	loadCustomers_generalneeds()
	loadCustomers_sheltered()
	loadCustomers_supported()
	loadDayToDay()
	Arrears()
	
	VoidProperties = voidValues(3)
	'AverageTurnaround = (newTenancies-terminations)	
	AverageTurnaround_GeneralNeeds = (newTenancies_GeneralNeeds-terminations_GeneralNeeds)	
	AverageTurnaround_supported = (newTenancies_supported-terminations_supported)	
	AverageTurnaround_sheltered = (newTenancies_sheltered-terminations_sheltered)	
	
	Emergency = dayToDayReportedAverage(1)
	Urgent = dayToDayReportedAverage(2)
	Routine = dayToDayReportedAverage(3)
	
	ArerarsCount  = ArrearsCount
	ArrearsValue = ArrearsValue
	VoidRentValue = VoidRentValue
	CloseDB()
		
		
%>
<HTML>
<HEAD>
<TITLE>RSL Manager --> My Whiteboard</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">

	function HideLoadingText(){
	
		parent.MPILoadingText.style.display = "none"
		parent.MPI_loadbutton.style.display = "none"
	}

</script>
<style type="text/css">
<!--
.style3 {
	color: #1F4C78;
	font-weight: bold;
}
.style4 {color: #1F4C78}
-->
</style>
</HEAD>
<BODY BGCOLOR=#FFFFFF  MARGINHEIGHT=0 LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" onLoad="HideLoadingText()">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
<style type="text/css">
<!--
.style1 {
	color: #003366;
	font-weight: bold;
}
.style2 {color: #003366}
-->
</style>
<TABLE width="239" CELLPADDING=0 CELLSPACING=0>
<tr><td width=2><font color="white">i</font></td>
<td>
<TABLE width="239" CELLPADDING=0 CELLSPACING=0>
  <TR>
    <TD VALIGN=TOP  colspan="3"><img src="../../myImages/MPI_title.gif" alt="MPI Management" width="234" height="18" /></TD>
  </TR>
    <TR>
    <TD colspan="2" height="4" VALIGN=TOP ></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP  colspan="3"><span class="style3">MPI : Tenancy and Letting </span></TD>
  </TR>
   <TR>
    <TD colspan="2" height="4" VALIGN=TOP ></TD>
  </TR>
  <TR>
    <TD width="152" VALIGN=TOP ><span style="color: #1F4C78"><font style='font-size:10px'>: Void Properties<br> 
    (3 - 6 month term) </font></span></TD>
    <TD width="85" VALIGN=TOP ><span style="color: #1F4C78;cursor:hand"><font style='font-size:10px' title="A count of Properties that are currently 'Available to Rent' but have been vacant between 3 to 6 months"><%=VoidProperties %></font></span></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><span class="style4">: Average Turnaround </span></TD>
    <TD VALIGN=TOP >&nbsp; </TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><div align="right" class="style4"><em>General Needs : </em></div></TD>
    <TD VALIGN=TOP ><span style="color: #1F4C78;cursor:hand"><em><font style='font-size:10px' title="The average number of days between a property becoming vacant and it being re-occupied.  These figures do not exclude the KPI Housing Corporation exceptions, such as Mutual Exchanges and Major Void Repairs."><%=AverageTurnaround_GeneralNeeds %> days </font></em></span></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><div align="right" class="style4"><em>supported : </em></div></TD>
    <TD VALIGN=TOP ><span style="color: #1F4C78;cursor:hand"><em><font style='font-size:10px;cursor:hand' title="This figure is made up of Asset Types: Supported ( <%=AverageTurnaround_supported%> days) and Sheltered ( <%=AverageTurnaround_sheltered%> days )"><%=AverageTurnaround_supported + AverageTurnaround_sheltered%> days </font></em></span></TD>
  </TR>
  <TR>
    <TD colspan="2" height="4" VALIGN=TOP ></TD>
  </TR>
  <TR>
    <TD colspan="2" VALIGN=TOP ><span class="style4"><strong>MPI : Arrears Management</strong>i</span></TD>
  </TR>
  <TR>
    <TD colspan="2" height="4" VALIGN=TOP ></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP  ><span class="style4" style="cursor:hand" title="This figure represents the Number of Customers">: Value of Arrears (<%=ArrearsCount%>)<em><strong>i</strong></em> </span></TD>
    <TD VALIGN=TOP  ><span class="style4" style="cursor:hand" title="This figure is the gross value of both Current & Previous Customers"><%=FormatCurrency(ArrearsValue,2)%></span></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><span class="style4">: Value of Void Rent </span></TD>
    <TD VALIGN=TOP ><span class="style4" style="cursor:hand" title="This figure is the Available to Let Potential Income as at the 1st of month"><%=FormatCurrency(VoidRentValue,2)%></span></TD>
  </TR>
   <TR>
    <TD colspan="2" height="4" VALIGN=TOP ></TD>
  </TR> 
  <TR>
    <TD colspan="2" VALIGN=TOP ><p class="style4"><strong>MPI : Asset management</strong> <BR> 
    Day to Day Repair Ratio</p>    </TD>
  </TR>
   <TR>
    <TD colspan="2" height="4" VALIGN=TOP ></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><span class="style4">: % of Emergency Reported </span></TD>
    <TD VALIGN=TOP ><span class="style4" style="cursor:hand" title="The percentage volume of Reactive Repairs for each of the Priority Bandings"><%=FormatNumber(Emergency,2)%>%</span></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><span class="style4">: % of Urgent Reported </span></TD>
    <TD VALIGN=TOP ><span class="style4" style="cursor:hand" title="The percentage volume of Reactive Repairs for each of the Priority Bandings"><%=FormatNumber(Urgent,2)%>%</span></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><span class="style4">: % of Routine Reported </span></TD>
    <TD VALIGN=TOP ><span class="style4" style="cursor:hand" title="The percentage volume of Reactive Repairs for each of the Priority Bandings"><%=FormatNumber(Routine,2)%>%</span></TD>
  </TR>
</TABLE>
</td></tr>
</table>
</BODY>
</HTML>