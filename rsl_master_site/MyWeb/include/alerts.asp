  <TR>
    <TD VALIGN=TOP  colspan="3"><img src="/MyWeb/images/alerts.gif" width="146" height="18"></TD>
  </TR>
  <%
  '	dim BudGetTransfer1
	sv_TeamCode = RTRIM(LTRIM(Session("TeamCode")))


	' retrieve holiday requests and number of absentees
		OpenDB()
		SQL = "SELECT 	COUNT(*) AS THECOUNT " &_
				"FROM 	E_JOURNAL J " &_
				"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID " &_
				"		INNER JOIN E_JOBDETAILS E ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				"WHERE	A.ABSENCEHISTORYID =  " &_
				"	   (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = A.JOURNALID) " &_
				"		AND J.ITEMID = 1 AND J.ITEMNATUREID <> 1 AND E.ACTIVE=1 " &_
				"		AND J.CURRENTITEMSTATUSID = 3 AND E.LINEMANAGER = " & Session("userid")
		
		SQL = 	"SELECT		COUNT(*) AS THECOUNT  " &_
			"FROM	 	E__EMPLOYEE E    " &_
			"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID    " &_
			"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM    " &_
			"			LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID   " &_
			"			LEFT JOIN E_JOURNAL JHOL ON J.EMPLOYEEID = JHOL.EMPLOYEEID  AND JHOL.CURRENTITEMSTATUSID = 3   " &_
			"			LEFT JOIN E_NATURE N ON JHOL.ITEMNATUREID = N.ITEMNATUREID AND APPROVALREQUIRED = 1   " &_
			"			LEFT JOIN E_ABSENCE AHOL ON JHOL.JOURNALID = AHOL.JOURNALID AND   " &_
			"			AHOL.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JHOL.JOURNALID)   " &_
			"WHERE	 	J.ACTIVE = 1  " &_
			"			AND AHOL.DURATION > 0 " &_
			"			AND J.LINEMANAGER = " & Session("userid")

		CALL OpenRs(rsCount, SQL)
		AbsenceRequestCount = 0
		if (NOT rsCount.EOF) then
			AbsenceRequestCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)
		
		' This recordset is to count the amount of completed items to be alerted
		If sv_TeamCode = "DAM" then
			SQL = " SELECT COUNT (*) AS CompletedItemCount" &_
				" FROM C_JOURNAL J  " &_
				"     INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
				"     LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
				"     INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
				"     INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID " &_
				" WHERE J.ITEMID = 1 " &_
				"    AND J.CURRENTITEMSTATUSID =  7  " &_
				"    AND J.ITEMNATUREID IN (2,22,20,21) " &_
				"    AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) "
			CALL OpenRs(rsCountItems, SQL)
			theCompletedItemCount = 0
			if (NOT rsCountItems.EOF) then
				theCompletedItemCount = rsCountItems("CompletedItemCount")
			End if
			CloseRs(rsCountItems)
		End If
		
		' End get completed item count
		
		' This recordset is to count the amount of invoices recieved - Only show on Finance whiteboard
		If sv_TeamCode = "FINA" then
			SQL = "SELECT count(*) as ReceivedInvioceCount " &_
				" FROM C_JOURNAL J " &_
				  "   INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID   " &_
				   "  LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID   " &_
				   "  INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " &_
				   "  INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID  " &_
					" INNER JOIN F_PURCHASEORDER PORDER ON PORDER.ORDERID = P.ORDERID  " &_
					" INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PORDER.POSTATUS  " &_
					" WHERE J.ITEMID = 1  " &_
					"	AND PORDER.POSTATUS = 7 " &_
					"	AND J.ITEMNATUREID IN (2,22,20,21)  " &_
					"	AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_
					"	AND R.CONTRACTORID = 1	"
			CALL OpenRs(rsRecievedInvioce, SQL)
			RecievedInvioce = 0
			if (NOT rsRecievedInvioce.EOF) then
				ReceivedInvioce = rsRecievedInvioce("ReceivedInvioceCount")
			End if
			CloseRs(rsRecievedInvioce)
		End If
		' End get completed item count

		SQL = "SELECT 	COUNT(*) AS THECOUNT " &_
				"FROM 	E_JOURNAL J " &_
				"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID " &_
				"		INNER JOIN E_JOBDETAILS E ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				"		INNER JOIN G_TEAMCODES G ON G.TEAMID = E.TEAM " &_
				"WHERE	E.ACTIVE = 1 AND A.ABSENCEHISTORYID =  " &_
				"	   (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = A.JOURNALID) " &_
				"		AND J.ITEMID = 1  AND J.CURRENTITEMSTATUSID = 1 AND E.LINEMANAGER = " & Session("userid") 

		CALL OpenRs(rsCount, SQL)
		SickCount = 0
		if (NOT rsCount.EOF) then
			SickCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)
	
	
'		QUEUED PURCHASE ORDERS
		' SEE IF ANY OTHER PEOPLE CAN SEE ALL QUEUED PURCHASES
		CANSEEPO = 0
		SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SEEALLQUEUEDPURCHASEORDERS' AND DEFAULTVALUE = " & SESSION("USERID") 
		Call openRs(rsSet, SQL)
		If Not rsSet.EOF Then 
			CANSEEPO = 1
		End If
		CloseRs(rsSet)

		'If sv_TeamCode = "CORP" Or Cint(CANSEEPO) = 1 Then
        If Cint(CANSEEPO) = 1 Then
			'SQL = "SELECT COUNT(*) AS QUEUEDREPAIRS " &_
			'		"FROM F_PURCHASEITEM PI  " &_
			'		"WHERE PI.PISTATUS = 0 AND PI.ACTIVE = 1 AND PI.PITYPE = 1 "
			
			SQL = "SELECT COUNT(*) AS QUEUEDREPAIRS " &_
				  "FROM F_PURCHASEORDER PO  " &_
				  "WHERE PO.POSTATUS = 0 AND PO.ACTIVE = 1 AND PO.POTYPE = 1 "	
				  	
		Else
			
			'SQL = "SELECT COUNT(*) AS QUEUEDREPAIRS " &_
			'		"FROM F_PURCHASEITEM PI  " &_
			'		"	INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID  " &_
			'		"	INNER JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") &_
			'		"WHERE PI.PISTATUS = 0 AND PI.ACTIVE = 1 AND EL.LIMIT >= PI.GROSSCOST  AND PI.PITYPE = 1 "
			
			SQL="    SELECT COUNT(*) AS QUEUEDREPAIRS "&_
                "    FROM F_PURCHASEORDER PO  "&_
	            "        INNER JOIN ( "&_
				"                  SELECT SUM(CASE WHEN EL.LIMIT >= PI.GROSSCOST  THEN 1 ELSE 0 END) AS EMPLOYEELIMIT,PI.ORDERID "&_
				"                  FROM F_PURCHASEITEM PI  "&_
				"                       INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID  "&_
				"       	            LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") &_
				"                  WHERE PI.PISTATUS = 0 AND PI.ACTIVE = 1 "&_
				"                   AND PI.PITYPE = 1 "&_
				"                   GROUP BY PI.ORDERID "&_ 
			    "       	    ) PI ON PI.ORDERID=PO.ORDERID "&_
                "    WHERE PO.POSTATUS = 0 AND PO.ACTIVE = 1 AND PO.POTYPE = 1 AND PI.EMPLOYEELIMIT>0 "

		End If
		
		CALL OpenRs(rsCount, SQL)
		POCount = 0
		if (NOT rsCount.EOF) then
			POCount = rsCount("QUEUEDREPAIRS")
		end if
		CloseRs(rsCount)

		' gets the general enquiry count
		SQL = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " &_
				"INNER JOIN C_GENERAL G ON J.JOURNALID = G.JOURNALID " &_
				"INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
				"INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " &_
				"WHERE J.ITEMID = 3 AND J.CURRENTITEMSTATUSID = 13 AND " &_
				"	G.GENERALHISTORYID = (SELECT MAX(GENERALHISTORYID) FROM C_GENERAL GG WHERE GG.JOURNALID = J.JOURNALID) " &_
				"	AND ASSIGNTO = " & SESSION("USERID")
		CALL OpenRs(rsCount, SQL)
		GeneralEnquiryCount = 0
		if (NOT rsCount.EOF) then
			GeneralEnquiryCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)

		' FIND ALL RENT ENQUIRIES
		OpenDB()
		SQL = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " &_
				"INNER JOIN C_GENERAL G ON J.JOURNALID = G.JOURNALID " &_
				"INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
				"INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " &_
				"WHERE J.ITEMID = 2 AND J.ITEMNATUREID NOT IN (5,6,7) AND J.CURRENTITEMSTATUSID = 13 AND " &_
				"	G.GENERALHISTORYID = (SELECT MAX(GENERALHISTORYID) FROM C_GENERAL GG WHERE GG.JOURNALID = J.JOURNALID) " &_
				"	AND ASSIGNTO = " & SESSION("USERID")				
		CALL OpenRs(rsCount, SQL)
		RentEnquiryCount = 0
		if (NOT rsCount.EOF) then
			RentEnquiryCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)

		' FIND ALL transfer/exchange ENQUIRIES
		OpenDB()
		SQL = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " &_
				"INNER JOIN C_TRANSFEREXCHANGE G ON J.JOURNALID = G.JOURNALID " &_
				"INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
				"INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " &_
				"WHERE J.ITEMID = 2 AND J.ITEMNATUREID IN (6,7) AND J.CURRENTITEMSTATUSID = 13 AND " &_
				"	G.TRANSFEREXCHANGEHISTORYID = (SELECT MAX(TRANSFEREXCHANGEHISTORYID) FROM C_TRANSFEREXCHANGE GG WHERE GG.JOURNALID = J.JOURNALID) " &_
				"	AND ASSIGNTO = " & SESSION("USERID")				
		CALL OpenRs(rsCount, SQL)
		TransferEnquiryCount = 0
		if (NOT rsCount.EOF) then
			TransferEnquiryCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)

		//FIND ALL adaptation ENQUIRIES
		OpenDB()
		SQL = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " &_
				"INNER JOIN C_ADAPTATION G ON J.JOURNALID = G.JOURNALID " &_
				"INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
				"INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " &_
				"WHERE J.ITEMID = 2 AND J.ITEMNATUREID IN (5) AND J.CURRENTITEMSTATUSID = 13 AND " &_
				"	G.ADAPTATIONHISTORYID = (SELECT MAX(ADAPTATIONHISTORYID) FROM C_ADAPTATION GG WHERE GG.JOURNALID = J.JOURNALID) " &_
				"	AND ASSIGNTO = " & SESSION("USERID")				
		
		CALL OpenRs(rsCount, SQL)
		AdaptationEnquiryCount = 0
		if (NOT rsCount.EOF) then
			AdaptationEnquiryCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)
				
		CloseDB()
		
	If sv_TeamCode = "EXEC" Then
		sql="select count(CCTRANID) as  THECOUNT  from F_BUDGETRTRANSFER where Approved=0"
		OpenDB()
		CALL OpenRs(rsCount, sql)
		BudGetTransfer=0
		if (NOT rsCount.EOF) then
			BudGetTransfer = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)
				
		CloseDB()
	end if	
		
	' build string to be used in 'IN 'statement in SQL containing IDs of directors teams
	Function get_director_sql()
		
		Dim str_team_ids
		str_team_ids = ""
		SQL = "SELECT TEAMID FROM E_TEAM WHERE DIRECTOR = " & Session("userid") & " OR MANAGER = " & Session("userid")
		str_team_ids = str_team_ids & " AND E.TEAM IN ("
		
		Call OpenRS(rsSet, SQL)
		while not rsSet.EOF 
			str_team_ids = str_team_ids & rsSet(0)
			rsSet.movenext()
			if not rsSet.EOF Then str_team_ids = str_team_ids & ", " end if
		Wend
		
		str_team_ids = str_team_ids & ")"
		get_director_sql = str_team_ids
		
	End Function

	' THIS FUNCTION RETURNS THE NUMBER OF SERVICE COMPLAINTS
	' QUERYTYPE = 1 (ALL) = 2 (CURRENT USER)
	Function get_num_service_complaints(QUERYTYPE)
		OpenDB()
		Dim num
		If Cint(QUERYTYPE) = 1 Then 
			SQL = " EXEC C_SERVICECOMPLAINTS_REPORT 2, '', '', '', '' "
		Else
			SQL = " EXEC C_SERVICECOMPLAINTS_REPORT 2, '', '', '', " & Session("USERID") & "  "
		End If
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then num = rsSet(0) Else num - 0 End If
		CloseRs(rsSet)
		CloseDb()
		get_num_service_complaints = num	
	End Function
	
%>   
<% if sv_TeamCode = "DAM" then %>
  <TR>
    
  <TD VALIGN=TOP ><font style='font-size:10px'>Repairs Awaiting Approval</font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/CompletedRepairsList.asp'><font color=red><%=theCompletedItemCount%></font></a></font></TD>
  </TR>
  <TR>
   <% End If %> 
  <% if sv_TeamCode = "FINA" then %>
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Received Invioces </font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/InvoiceRecievedList.asp'><font color=red><%=ReceivedInvioce%></font></a></font></TD>
  </TR>
  <% End If %> 
   <TR>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Leave Request </font></TD>
    <TD VALIGN=TOP ALIGN=left><font style='font-size:10px'><a href='/businessmanager/my_staff.asp'><font color=red><%=AbsenceRequestCount%></font></a></font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Absentees </font></TD>
    <TD VALIGN=TOP ALIGN=left><font style='font-size:10px'><a href='/businessmanager/my_staff.asp'><font color=red><%=SickCount%></font></a></font></TD>
  </TR>
    <TR>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Queued Purchases </font></TD>
    <TD VALIGN=TOP ALIGN=left><font style='font-size:10px'><a href='/Finance/PurchaseListQueued.asp'><font color=red><%=POCount%></font></a></font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>General Enquiries</font></TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/GeneralEnquiryList.asp'><font color=red><%=GeneralEnquiryCount%></font></a></font></TD>
  </TR>  
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Rent Enquiries </font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/TenantEnquiryList.asp'><font color=red><%=RentEnquiryCount%></font></a></font></TD>
  </TR>		
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Transfer /Exchange </font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/TransferExchangeEnquiryList.asp'><font color=red><%=TransferEnquiryCount%></font></a></font></TD>
  </TR>		
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Adaptation Enquiries </font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/AdaptationEnquiryList.asp'><font color=red><%=AdaptationEnquiryCount%></font></a></font></TD>
  </TR>
  <%If sv_TeamCode = "EXEC" Then%>
   <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Budget Virements </font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Finance/CostCenterTransfer_dtl.asp'><font color=red><%=BudGetTransfer%></font></a></font></TD>
  </TR>
  <%end if %> 	
  <%  
	if NOT sv_TeamCode = "SUP"  then %>
  <TR>
  <TD VALIGN=TOP ><font style='font-size:10px'>Service Complaints</font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><a href='/Customer/serviceComplaintsReport.asp?ASSIGNEDTO=<%=Session("LastName")%>'><font color=red><%=get_num_service_complaints(1)%>(<%=get_num_service_complaints(2)%>)</font></a></font></TD>
  </TR>
  <TR>
   <% End If %> 

  
