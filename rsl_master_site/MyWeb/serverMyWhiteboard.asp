<%@LANGUAGE="VBSCRIPT"%>
<!--#include virtual="autologout.asp" -->
<!--#include file="../Connections/db_connection.asp" --><%
	
dim Session
set Session = server.CreateObject("SessionMgr.Session2")

	set rsMyEvents = Server.CreateObject("ADODB.Recordset")
	rsMyEvents.ActiveConnection = RSL_CONNECTION_STRING
	rsMyEvents.Source = "SELECT event_id, eventdate = convert(nvarchar, event_date,103), event_start_time, event_title FROM dbo.event where event_date > GETDATE()-1"
	rsMyEvents.CursorType = 0
	rsMyEvents.CursorLocation = 2
	rsMyEvents.LockType = 3
	rsMyEvents.Open()
	rsMyEvents_numRows = 0

	set rsRepairs = Server.CreateObject("ADODB.Recordset")
	rsRepairs.ActiveConnection = RSL_CONNECTION_STRING
	rsRepairs.Source = "select count(*) as repcnt from rrid r, rrApp a where r.rr_Session = a.rrApp_SessionID and r.rr_Status = 1"
	rsRepairs.CursorType = 0
	rsRepairs.CursorLocation = 2
	rsRepairs.LockType = 3
	rsRepairs.Open()

	numMyHols = 0
	set rsMyHols = Server.CreateObject("ADODB.Recordset")
	rsMyHols.ActiveConnection = RSL_CONNECTION_STRING
	rsMyHols.Source = "SELECT * FROM HOLIDAYACCOUNTS WHERE USERID = " & Session("svUserID")
	rsMyHols.CursorType = 0
	rsMyHols.CursorLocation = 2
	rsMyHols.LockType = 3
	rsMyHols.Open()
	rsMyHols_numRows = 0
	if (NOT rsMyHols.EOF) Then
		numMyHols = rsMyHols("balance")
	End if
	rsMyHols.Close()
	Set rsMyHols = Nothing
%>
<html>
<head>
<title>My Whiteboard</title>
<META HTTP-EQUIV=Refresh CONTENT="60; URL=serverMyWhiteboard.asp">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript>
function returnFrontPage(){
	parent.TheWholeBody.innerHTML = HiddenDivData.innerHTML;
	}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnFrontPage()">
<div id=HiddenDivData>
<TABLE HEIGHT=100% CELLSPACING=0 CELLPADDING=0><TR>
	<TD WIDTH=12PX HEIGHT=100% BGCOLOR=#D7D3E6 VALIGN=TOP nowrap>&nbsp;</TD>
	<TD WIDTH=150PX HEIGHT=100% BGCOLOR=#D7D3E6 VALIGN=TOP nowrap>
<!-- START OF LEFT SIDE BAR -->						
	        <table class='RSLWhite' cellspacing=0 BGCOLOR=#133E71 width=100% style='word-break:break-all'>
              <tr> 
                <td BGCOLOR=#D7D3E6 colspan=2 HEIGHT=17 valign=middle class="RSLBlack"><b>&nbsp;<%=WeekDayName(WeekDay(Date)) & " " & Day(Date) & " " & MonthName(Month(Date),true) & " " & Year(Date)%></b></td>
              </tr>
              <tr class="RSLWhite"> 
                <td colspan=2><b><font style='font-size:12px'>&nbsp;My Diary</font></b></td>
              </tr>
              <% 
		LineCount = 0
		CurrentDate = 0
		Do While (NOT rsMyEvents.EOF)
			if (LineCount > 5) Then
				Response.Write "<TR><TD colspan=2 class=BTB>&nbsp;<a href='../iagDiary/default.asp'><b>More Diary Events...</B></a></TD></TR>"
				Exit Do
			End If
			iNewDate = CDate(rsMyEvents("eventdate"))
			if (iNewDate <> CurrentDate) Then
				tempDate = Day(iNewDate) & " " & MonthName(Month(iNewDate)) & " " & Year(iNewDate)
				Response.Write "<TR><TD colspan=2 class=BTB>&nbsp;<b>" & tempDate & "</b></TD></TR>"
				CurrentDate = iNewDate
				LineCount = LineCount + 1				
			End If
			Response.Write "<TR><TD valign=top width=40>&nbsp;<b>" & rsMyEvents("event_start_time") & "</b>&nbsp;</TD><TD valign=top><a href='../iagDiary/viewevent.asp?id=" & rsMyEvents("event_id") & "'>" & rsMyEvents("event_title") & "</a></TD></TR>"
			rsMyEvents.moveNext()
			LineCount = LineCount + 1			
		Loop
		rsMyEvents.Close()
		Set rsMyEvents = Nothing
		if (LineCount = 0) Then
			Response.Write "<TR><TD style='border-top:1px solid #D7D3E6'>&nbsp;</TD><TD style='border-top:1px solid #D7D3E6'>No appointments set for the foreseeable future.</TD></TR>"
			Response.Write "<TR><TD colspan=2 class=BTB>&nbsp;<a href='../iagDiary/default.asp'><b>Go to Diary...</b></a></TD></TR>"			
		End If
		%>
			<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>
            </table>
	        <table class='RSLBlack' cellspacing=0 BGCOLOR=#D6D6D6 width=100%>
				<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>
				<tr><td colspan=2>&nbsp;<img src=/myImages/My69.gif>&nbsp;<font style='font-size:12px'><b>Don't Forget</b></font></td></tr>
				<tr><td>&nbsp;</td><td>Keep your personal details up-to-date by <a href='../iagMyJob/UserPage.asp'><b><font color=black onmouseover="cC(this,'red')" onmouseout="cC(this, 'red')">clicking here</font></b></a></td></tr>				
				<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>
			</table>
			
			<table class='RSLBlack' cellspacing=0 BGCOLOR=#D7D3E6 width=100%>
				<form name=MyMembers action="../iagContactManager/FindMember.asp" method="post">
				<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>			
				<tr><td>&nbsp;</td>
                <td><img src="/myImages/My96_purple.gif" width=10 height=10>&nbsp;<font style='font-size:12px'><b>Find 
                  a Member</b></font></td></tr>
				<tr><td>&nbsp;</td><td>Please enter the name of the organisation<br>e.g. <b>uni</b> or <b>university</b></td></tr>							  
				<tr><td colspan=2>&nbsp;<input size=24 type=text value='Enter Organisation here' onfocus="javascript:if (this.value == 'Enter Organisation here') this.value='';" onblur="javascript:if (this.value == '') this.value='Enter Organisation here';" name=nameString class="RSLBlack" maxlength=10></td></tr>
				<tr><td colspan=2 align=right>&nbsp;<a href=# onclick="javascript:if(MyMembers.nameString.value != '' && MyMembers.nameString.value != 'Enter Organisation here') MyMembers.submit()"><b><font color=black onmouseover="cC(this,'red')" onmouseout="cC(this, 'red')">FIND</font></b></a>&nbsp;&nbsp;</td></tr>
				<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>							
				</form>
			</table>
			
			<table class='RSLWhite' cellspacing=0 width=100% BGCOLOR=#133E71>
				<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>			
				<tr><td>&nbsp;</td><td><a href='/iagMyJob/UserPage.asp?ThePage=1'><b><%=Session("svName")%></b>, you have <b><%=numMyHols%></b></a> days Annual Leave to take by January</td></tr>							
				<tr><td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>							
			</table>
<!-- END OF LEFT SIDE BAR -->						
</TD><TD valign=top>
<!-- START OF MAIN BODY NEWS -->						
<!--#include file="myNews.asp" -->
<!-- END OF MAIN BODY NEWS -->

<TABLE CELLSPACING=0 CELLPADDING=0 height=100%><TR><TD VALIGN=TOP>
	<!-- START OF CUSTOMER INFO -->
	<!--#include file="myCustomerData.asp" -->
	<!-- END OF CUSTOMER INFO -->
</TD><TD VALIGN=TOP>
	<IMG SRC="/myImages/spacer.gif" WIDTH=2 HEIGHT=17>
</TD><TD VALIGN=TOP>
	<TABLE CELLSPACING=0 border=0 CELLPADDING=0 WIDTH=185 CLASS=RSLBlack VALIGN=TOP ALIGN=left BGCOLOR=#D7D3E6 height="100%" style='word-break:break-all'>
		<TR>
			<TD BGCOLOR=#133E71 colspan=2><img src="/myImages/repairs.gif"></TD>
			<TD BGCOLOR=#133E71><IMG SRC="/myImages/spacer.gif" WIDTH=1 HEIGHT=17></TD>
		</TR>
		<tr>
		<td colspan=2><img src=/myImages/spacer.gif width=12 height=5></td></tr>
		<%
		While (NOT rsRepairs.EOF)
			If rsRepairs("repcnt") <> 1 Then
		%>
		<TR>
        	<TD align=left class="RSLBlack">&nbsp;<img src="/myImages/My102_white.gif" width=12 height=12 valign=top>
			<a href="/iagCRM/repairsearch.asp"><font color=black onmouseover="cC(this,'blue')" onmouseout="cC(this, 'blue')">There are 
			<%=rsRepairs("repcnt")%> repairs to<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;be dealt with. </font></a></TD>
       </TR>
	   <% Else %>
	   <TR>
        	<TD align=left class="RSLBlack">&nbsp;<img src="/myImages/My102_white.gif" width=12 height=12 valign=top>
			<a href="/iagCRM/repairsearch.asp"><font color=black onmouseover="cC(this,'blue')" onmouseout="cC(this, 'blue')">There is 
			<%=rsRepairs("repcnt")%> repair to<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;be dealt with. </font></a></TD>
       </TR>
	   <% End If %>
	   
		<TR><TD colspan=2>&nbsp;</TD></TR>		
	
			
		<% 	rsRepairs.movenext()
			Wend	%>
		<%
		rsRepairs.close()
		Set rsRepairs = Nothing
		%>			
		<TR><TD height=100% colspan=2>&nbsp;</TD></TR>
	</TABLE>
</TD></TR></TABLE>
</TD><TD>
<IMG SRC="/myImages/spacer.gif" WIDTH=1 HEIGHT=17>
</TD><TD valign=top width=100% BGCOLOR=#F6F4F4>
<!-- START OF TASKS AND MESSAGES -->
<%
Set tsTaskSumID = Server.CreateObject("ADODB.Recordset")
tsTaskSumID.ActiveConnection = RSL_CONNECTION_STRING
tsTaskSumID.Source = "SELECT * FROM dbo.tasks WHERE wuserid = " & Session("svUserID") & " and active = 0 order by storeddate desc"
tsTaskSumID.CursorType = 0
tsTaskSumID.CursorLocation = 2
tsTaskSumID.LockType = 1
tsTaskSumID.Open()
TaskCount = 0
%>
<TABLE CELLSPACING=0 CELLPADDING=0 valign=top class="RSLBlack" width=239>
<%
Do While (NOT tsTaskSumID.EOF)
	TaskCount = TaskCount + 1
	if (TaskCount = 10) Then
	%>
	<TR><TD class="RSLBlack" align=right valign=top colspan=2>&nbsp;&nbsp;<a href="/IagProfile/UserTaskSummary.asp"><font color=black onmouseover="cC(this,'blue')" onmouseout="cC(this, 'blue')">More Tasks...</font></a>&nbsp;&nbsp;</TD></TR>
	<TR style='line-height:5px'><TD>&nbsp;</TD></TR>	
	<%
	Exit Do
	End if

%>
	<TR><TD align=right width=30 valign=top><IMG SRC="/myImages/My48.gif" WIDTH=11 HEIGHT=11></TD><td WIDTH=209 style='word-breal:break-all' valign=top>&nbsp;&nbsp;<a href="/IagProfile/UserTaskSummary.asp?TaskSumID=<%= tsTaskSumID.Fields.Item("TaskID").Value %>"><font color=black onmouseover="cC(this,'blue')" onmouseout="cC(this, 'blue')"><%= tsTaskSumID.Fields.Item("tasksum").Value %></font></a></TD></TR>
	<TR style='line-height:5px'><TD>&nbsp;</TD></TR>		
<%
tsTaskSumID.moveNext()
Loop
tsTaskSumID.close()
Set tsTaskSumID = nothing
if (TaskCount = 0) Then
%>
	<TR><TD align=right width=30 valign=top NOWRAP><IMG SRC="/myImages/My48.gif" WIDTH=11 HEIGHT=11></TD><td WIDTH=209 style='word-breal:break-all' valign=top>&nbsp;&nbsp;<font color=black>No Tasks Waiting</font></TD></TR>
	<TR><TD>&nbsp;</TD></TR>		
<% End If %>
<TR><TD>&nbsp;</TD></TR>			
<TR><TD Colspan=2><IMG SRC="/myImages/My63_CLEAR.gif" WIDTH=48 HEIGHT=19 onClick="MM_openBrWindow('/IagProfile/PopMessage.asp','PopMessage','width=400,height=300')" style='cursor:hand'></TD></TR>
<TR><TD Colspan=2>
	<TABLE CELLSPACING=0 CELLPADDING=0>
		<TR>
			<TD><IMG SRC="/myImages/My65.gif" WIDTH=83 HEIGHT=17></TD>
			<TD BGCOLOR=#133E71><IMG SRC="/myImages/spacer.gif" WIDTH=142 HEIGHT=17></TD>
			<TD BGCOLOR=#8E869F><IMG SRC="/myImages/spacer.gif" WIDTH=14 HEIGHT=17></TD>
		</TR>
	</TABLE>
</TD></TR>
<TR><TD>&nbsp;</TD></TR>
<%
Set rsMessages = Server.CreateObject("ADODB.Recordset")
rsMessages.ActiveConnection = RSL_CONNECTION_STRING
rsMessages.Source = "SELECT * FROM dbo.MessagePadData WHERE messagefor = " & Session("svUserID") & " and active = 1 order by storeddate desc, messagetitle"
rsMessages.CursorType = 0
rsMessages.CursorLocation = 2
rsMessages.LockType = 1
rsMessages.Open()
MessageCount = 0
%>
<%
Do While (NOT rsMessages.EOF)
MessageCount = MessageCount + 1
	if (MessageCount = 10) Then
	%>
	<TR><TD class="RSLBlack" align=right valign=top colspan=2>&nbsp;&nbsp;<a href="/IagMyJob/AllMessages.asp"><font color=black onmouseover="cC(this,'red')" onmouseout="cC(this, 'red')">More Messages...</font></a>&nbsp;&nbsp;</TD></TR>
	<TR style='line-height:5px'><TD>&nbsp;</TD></TR>	
	<%
	Exit Do
	End if
%>
	<TR><TD align=right width=30 valign=top NOWRAP><img src="/myImages/My80.gif" width=17 height=12></TD><TD title='<%= rsMessages("Message") %>' style='word-breal:break-all' valign=top>&nbsp;&nbsp;<a href="#" onClick="MM_openBrWindow('/IagProfile/PopMessageDisplay.asp?MessagePadID=<%= rsMessages("MessagePadID") %>','YourMessage','width=400,height=300')"><font color=black onmouseover="cC(this,'red')" onmouseout="cC(this, 'red')"><%= rsMessages("MessageTitle") %></font></a></TD></TR>
	<TR style='line-height:5px'><TD>&nbsp;</TD></TR>		
<%
	rsMessages.moveNext()
Loop
rsMessages.close()
Set rsMessages = nothing
if (MessageCount = 0) Then
%>
	<TR>
                <TD align=right width=30 valign=top NOWRAP><IMG SRC="/myImages/My80.gif" WIDTH=17 HEIGHT=12></TD>
                <td WIDTH=209 style='word-breal:break-all' valign=top>&nbsp;&nbsp;<font color=black>No Messages Waiting</font></TD></TR>
	<TR><TD>&nbsp;</TD></TR>		
<% End If %>

</TABLE>
<!-- END OF TASKS AND MESSAGES-->
</TD></TR></TABLE>
</div>
</body>
</html>
