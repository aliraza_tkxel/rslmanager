<%

OpenDB()
			' Get the number of jobs that are in progress
				SQL = " SELECT COUNT (*) AS JobsInProgress" &_
					" FROM C_JOURNAL J  " &_
					"     INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
					"     LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
					"     INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
					"     INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID " &_
					" WHERE J.ITEMID = 1 " &_
					"    AND ((J.CURRENTITEMSTATUSID <  10 AND  J.CURRENTITEMSTATUSID <> 5) OR (J.CURRENTITEMSTATUSID = 12)) " &_
					"    AND J.ITEMNATUREID IN (2,22,20,21) " &_
					"    AND R.CONTRACTORID = 1 " &_
					"    AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) "
				CALL OpenRs(rsJobsInProgress, SQL)
				
				theJobsInProgress = 0
				if (NOT rsJobsInProgress.EOF) then
				
					theJobsInProgress = rsJobsInProgress("JobsInProgress")
				
				End if
				
				CloseRs(rsJobsInProgress)
		    ' End get completed item count
		
			' This recordset is to count the amount of completed items to be alerted
				SQL = " SELECT COUNT (*) AS JobsApproved" &_
					" FROM C_JOURNAL J  " &_
					"     INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
					"     LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
					"     INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
					"     INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID " &_
					" WHERE J.ITEMID = 1 " &_
					"    AND J.CURRENTITEMSTATUSID >  10  AND J.CURRENTITEMSTATUSID <> 12" &_
					" AND J.ITEMNATUREID IN (2,22,20,21) " &_
					"    AND R.CONTRACTORID = 1 " &_
					"    AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) "
				CALL OpenRs(rsJobsApproved, SQL)
				
				theJobsApproved = 0
				if (NOT rsJobsApproved.EOF) then
				
					theJobsApproved = rsJobsApproved("JobsApproved")
				
				End if
				
				CloseRs(rsJobsApproved)
			'''' End Jbs approved recordset
				
			' This sets the number of emergency jobs (At this time(21/06/2004) is set as Priority A)
				SQL	= " SELECT COUNT (*) AS EmergencyJobs " &_
						" FROM C_JOURNAL J " &_
							 " INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
							 " LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
							 " INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
							 " INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID "  &_ 
							 " INNER JOIN R_ITEMDETAIL I ON I.ITEMDETAILID = R.ITEMDETAILID " &_
							 " INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = I.PRIORITY " &_
						" WHERE J.ITEMID = 1 " &_
							  " AND PRI.PRIORITYID = 'A' " &_
							  " AND J.ITEMNATUREID IN (2,22,20,21) " &_
							  "    AND R.CONTRACTORID = 1 " &_
							  " AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " 
	
					CALL OpenRs(rsEmergencyJobs, SQL)
					
					EmergencyJobs = 0
					if (NOT rsEmergencyJobs.EOF) then
					
						EmergencyJobs = rsEmergencyJobs("EmergencyJobs")
					
					End if
					
					CloseRs(rsEmergencyJobs)
				''''' End Energency Jobs Recordset
				
				' This returns the number of jobs set today
					SQL	= " SELECT COUNT (*) AS TodaysJobs " &_
						" FROM C_JOURNAL J " &_
							 " INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
							 " LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
							 " INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
							 " INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID "  &_ 
							 " INNER JOIN R_ITEMDETAIL I ON I.ITEMDETAILID = R.ITEMDETAILID " &_
							 " INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = I.PRIORITY " &_
						" WHERE J.ITEMID = 1 " &_
							  " AND convert(datetime,convert(varchar,J.creationdate,103),103) >= convert(datetime,convert(varchar,getdate(),103),103) " &_
							  " AND J.ITEMNATUREID IN (2,22,20,21) " &_
							  "    AND R.CONTRACTORID = 1 " &_
							  " AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " 
	
					CALL OpenRs(rsTodaysJobs, SQL)
					
					TodaysJobs = 0
					if (NOT rsTodaysJobs.EOF) then
					
						TodaysJobs = rsTodaysJobs("TodaysJobs")
					
					End if
					
					CloseRs(rsTodaysJobs)
				''''' End Todays Jobs recordset
				
				
				' Get the start date of the financial year previously
				var1 = now()
				if Month(var1) < 5 and Day(var1) < 5 then
					YearIs = Year(var1) - 1
				Else
					YearIs = Year(var1)
				End If
				FinancialYearStart = cDate("5/April/" & YearIs)
				
				' Get the number of YTD jobs (Year start being the financial year
					SQL	= " SELECT COUNT (*) AS YTDJobs " &_
						" FROM C_JOURNAL J " &_
							 " INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
							 " LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
							 " INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
							 " INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID "  &_ 
							 " INNER JOIN R_ITEMDETAIL I ON I.ITEMDETAILID = R.ITEMDETAILID " &_
							 " INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = I.PRIORITY " &_
						" WHERE J.ITEMID = 1 " &_
							  " AND convert(datetime,convert(varchar,J.creationdate,103),103) >= '" & FinancialYearStart & "' " &_
							  " AND J.ITEMNATUREID IN (2,22,20,21) " &_
							  " AND R.CONTRACTORID = 1 " &_
							  " AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " 
	
					CALL OpenRs(rsYTDjobs, SQL)
					
					YTDJobs = 0
					if (NOT rsYTDjobs.EOF) then
					
						YTDJobs = rsYTDjobs("YTDJobs")
					
					End if
					
					CloseRs(rsYTDjobs)
				'''''''  End YTD JOBS recordset
				
				'Close DB off
				CloseDB()
		
		
%>
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
<TABLE width="146" CELLPADDING=3 CELLSPACING=0>
  <TR>
    <TD VALIGN=TOP  colspan="3"><img src="/myweb/images/performance.gif" width="146" height="18"></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Emergency Jobs</font></TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><font color=red><%=EmergencyJobs %></font></font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Jobs in Progress</font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><font color=red><%=theJobsInProgress %></font></font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Jobs Approved</font> </TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><font color=red><%=theJobsApproved %></font></font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP  colspan="3" ></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP  ><font style='font-size:10px'>New Jobs Today</font></TD>
    <TD VALIGN=TOP  ><font style='font-size:10px'><font color=red><%=TodaysJobs %></font></font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP ><font style='font-size:10px'>Value YTD</font></TD>
    <TD VALIGN=TOP ><font style='font-size:10px'><font color=red><%=YTDJobs %></font></font></TD>
  </TR>

</TABLE>

		<BR>
		<TABLE STYLE='BORDER:1PX SOLID #133E71' WIDTH=150PX>
		<%
		OpenDB()
		SQL = "SELECT * FROM S_ORGANISATION WHERE ORGID = " & SESSION("ORGID")
		Call OpenRS(rsContractor, SQL)
		IFields = Array("Address1", "Address2", "Address3", "TownCity", "Postcode", "County")
		Response.Write "<TR><TD><b>" & rsContractor("Name") & "</b></TD></TR>"
		For i=0 to Ubound(IFields)
			iValue = rsContractor(IFields(i))
			if (iValue <> "" AND NOT ISNULL(iValue)) then
				Response.Write "<TR><TD>" & iValue & "</TD></TR>"
			end if
		Next
		CloseRS(rsContractor)
		CloseDB()
		%>
		</TABLE>