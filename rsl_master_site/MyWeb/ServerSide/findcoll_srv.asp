<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim str_search, str_SQL, str_data, cnt
	
	str_search = Request.Form("txt_NAME")
	COUNT_SQL = "SELECT		COUNT(FIRSTNAME) AS THECOUNT "  &_
				"FROM 		E__EMPLOYEE E " &_
				"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"			LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID " &_
				"			LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
				"WHERE		E.FIRSTNAME + ' ' + E.LASTNAME LIKE '%" & str_search & "%'"&_
				"			AND T.TEAMID <> 1 AND J.ACTIVE = 1 "
	
	str_SQL = 	"SELECT		FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
				"			TEAMNAME = CASE " &_				
				"			WHEN O.NAME IS NULL THEN ISNULL(T.TEAMNAME, 'N/A') " &_								
				"			ELSE ISNULL(T.TEAMNAME, 'N/A') + ', <FONT COLOR=BLUE>' + O.NAME + '</FONT>' " &_								
				"			END, " &_																
				"			ISNULL(HOMETEL, 'N/A') AS HOMETEL," &_
				"			ISNULL(WORKMOBILE, 'N/A') AS WORKMOBILE, " &_
				"			ISNULL(WORKDD, 'N/A') AS WORKDD," &_
				"			ISNULL(WORKEXT, 'N/A') AS WORKEXT," &_
				"			ISNULL(HOMEEMAIL, 'N/A') AS HOMEEMAIL," &_
				"			ISNULL(WORKEMAIL, 'N/A') AS WORKEMAIL " &_
				"FROM 		E__EMPLOYEE E " &_
				"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"			LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID " &_
				"			LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
				"			LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID " &_				
				"WHERE		E.FIRSTNAME + ' ' + E.LASTNAME LIKE '%" & str_search & "%'" &_
				"			AND T.TEAMID <> 1 AND J.ACTIVE = 1 "&_
				"ORDER		BY	E.FIRSTNAME, E.LASTNAME "
	response.write str_SQL
	OpenDB()
	
	get_colleague()
	
	CloseDB()
				
	Function get_colleague()
		Call OpenRs(rsCount, COUNT_SQL)
		if (NOT rsCount.EOF) then
			theCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)
		
		Call OpenRs(rsSet, str_SQL)
		str_data = "<table cellspacing=0 cellpadding=0>"
		cnt = 0
		While not rsSet.EOF
			cnt = cnt + 1							
			str_data = str_data &_
				"<TR><TD colspan=3 align=right><A NAME='L" & cnt & "'></A>&nbsp; " & cnt & " of " & theCount & "</TD></TR>" &_
				"<TR><TD height=2px></TD></TR>" &_
				"<TR><TD>Name</TD><TD WIDTH='230PX' NOWRAP>:&nbsp; " & Left(rsSet("FULLNAME"),30) &_
					"</TD><TD ALIGN=RIGHT ROWSPAN=2 VALIGN=TOP><A HREF='#L" & cnt - 1 & "'>" &_
					"<img src='/MyWeb/images/UP.gif' BORDER=0></A></TD></TR>" &_
				"<TR><TD>Team</TD><TD>:&nbsp; " & rsSet("TEAMNAME") & "</TD><TD></TD></TR>" &_
				"<TR><TD>Work DD</TD><TD>:&nbsp; " & rsSet("WORKDD") & "</TD><TD></TD></TR>" &_
				"<TR><TD nowrap>Work Email&nbsp;</TD><TD>:&nbsp; <a href='mailto:" & rsSet("WORKEMAIL") & "'>"&rsSet("WORKEMAIL")&"</a></TD><TD></TD></TR>" &_
				"<TR><TD>Work Mobile</TD><TD>:&nbsp; " & rsSet("WORKMOBILE") & "</TD>" &_
					"<TD ALIGN=RIGHT rowspan=2 valign=bottom><A CLASS='RSLBLACK' HREF='#L" & cnt + 1 & "'>" &_
					"<img src='/MyWeb/images/DOWN.gif' BORDER=0></A></TD></TR>" &_
				"<TR><TD>Work Ext</TD><TD>:&nbsp; " & rsSet("WORKEXT") & " </TD></TR><TR><TD>&nbsp;</TD></TR>"
			rsSet.movenext()
			
		Wend
		str_data = str_data & 	"<TR><TD>&nbsp;</TD></TR></TABLE>"
		Call CloseRs(rsSet)	
	
	End function
	//<a href="mailto:iagsupport@reidmark.com">DSF</a>
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
<% if cnt = 0 then %>	
	parent.detail_div.innerHTML = Empty.innerHTML
<% else %>	
	parent.detail_div.innerHTML = NotEmpty.innerHTML
<% end if %>
	//parent.href="#L1"
	parent.detail_div.scrollTop = 0	
	}
</script>
<body onload="ReturnData()">
<div id="Empty">
	<TABLE cellspacing=0 cellpadding=0>
		<TR><TD>&nbsp;</TD></TR>
		<TR><TD height=2px></TD></TR>		
		<TR><TD>Name</TD><TD>:</TD></TR>
		<TR><TD>Team</TD><TD>:</TD></TR>
		<TR><TD>Work DD</TD><TD>:</TD></TR>
		<TR><TD>Work Email&nbsp;</TD><TD>:&nbsp;</TD></TR>
		<TR><TD>Work Mobile</TD><TD>:</TD></TR>
		<TR><TD>Work Ext</TD><TD>:</TD></TR>
		<TR><TD>&nbsp;</TD></TR>		
	</TABLE>
</div>
<div id="NotEmpty">
	<%=str_data%>
</div>
</body>
</html>