<%			
	OpenDB()
	''''''''
				' Count how many properties have no tenancy or the tenancy has ran out				
				SQL = " SELECT COUNT(*) AS FREEPROPERTIES FROM P__PROPERTY PROP " &_
				"  			INNER JOIN P_FINANCIAL F ON F.PROPERTYID = PROP.PROPERTYID "&_
				" WHERE PROP.PROPERTYID NOT IN (SELECT P.PROPERTYID  " &_ 
								" FROM P__PROPERTY P " &_
								" 	INNER JOIN C_TENANCY T ON T.PROPERTYID = P.PROPERTYID) " &_
				" OR PROP.PROPERTYID IN (SELECT PR.PROPERTYID    " &_
							 			" FROM P__PROPERTY PR " &_
										" 	INNER JOIN C_TENANCY TE ON TE.PROPERTYID = PR.PROPERTYID " &_
										"   WHERE TE.ENDDATE IS NOT NULL AND TE.ENDDATE <= GETDATE()) AND F.TARGETRENT NOT IN (1) " 
										
					'rw sql					
				CALL OpenRs(rsCountFreeProperties, SQL)
					
					theFreePropertiesCount = 0
					if (NOT rsCountFreeProperties.EOF) then
					
						theFreePropertiesCount = rsCountFreeProperties("FREEPROPERTIES")
					
					End if
					
				CloseRs(rsCountFreeProperties)
			
				' Count how many satisfaction letters are ready to be sent 
				
				SQL = " select count(SL.SURVEYID) AS LETTERS_READY " &_
						" from C_SATISFACTIONLETTER SL " &_
						"	 INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID " &_
						"	INNER JOIN C_REPAIR R ON R.JOURNALID = SL.JOURNALID  " &_
						"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID  " &_
						"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID  " &_
						"	INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID  " &_
						"	INNER JOIN P_WORKORDER WO ON WO.WOID = WOTO.WOID  " &_
						"	INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
						"	INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WOTO.ORDERITEMID  " &_
						" 	WHERE SL.SATISFACTION_LETTERSTATUS = 1  AND R.ITEMACTIONID = 5 j.currentitemstatusid NOT IN (5,10,11,12,22)" 
					
					'rw sql					
				CALL OpenRs(rsSatisfactionLettersWaiting, SQL)
					
					SatisfactionLettersWaiting = 0
					if (NOT rsSatisfactionLettersWaiting.EOF) then
					
						SatisfactionLettersWaiting = rsSatisfactionLettersWaiting("LETTERS_READY")
					
					End if
					
				CloseRs(rsSatisfactionLettersWaiting)
			

	CloseDB()
			%>
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
<TABLE width="146" CELLPADDING=3 CELLSPACING=0>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><img src="/myweb/images/performance.gif" width="146" height="18"></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap>Properties Available</TD>
    <TD VALIGN=TOP nowrap><font style='font-size:10px'><a href='/Customer/FreePropertyList.asp'><font color=red><%=theFreePropertiesCount%></font></a></font></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap><font style='font-size:10px'>Void Turnaround</font> 
    </TD>
    <TD VALIGN=TOP nowrap><font style='font-size:10px'>00</font></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP colspan="3"><img src="/myweb/spacer.gif" width="3" height="3"></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><img src="/MyWeb/images/Repair_Title.gif" width="146" height="18"></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Total No</font></TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>00</font></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Total Spend</font></TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>00</font></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Unallocated</font></TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>00</font></TD>
  </TR>
  <TR>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px'>Queued</font></TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px;'><a href='/Finance/QueuedRepairs.asp'><font color=red>
      <%'=Queued%>
      </font></a></font></TD>
  </TR>
  <%
'OpenDB()
'SQL = "SELECT ISNULL(EMPLOYEELIMIT,0) AS EMPLOYEELIMIT FROM E_JOBDETAILS WHERE EMPLOYEEID = " & Session("USERID")
'Call OpenRs(rsEmp, SQL)
'EmpLimit = -1
'if (NOT rsEmp.EOF) then EmpLimit = rsEmp("EMPLOYEELIMIT")
'CloseRs(rsEmp)

'SQL = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " &_
	'"INNER JOIN C_REPAIRPURCHASE RP ON RP.JOURNALID = J.JOURNALID " &_
	'"INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = RP.ORDERID " &_
	'"WHERE J.ITEMNATUREID = 2 AND J.CURRENTITEMSTATUSID = 12 AND PO.GROSSCOST <= " & EmpLimit
'Call OpenRs(rsQue, SQL)
'Queued = 0
'if (NOT rsQue.EOF) then Queued = rsQue("THECOUNT")
'CloseRs(rsQue)
'CloseDB()
%>
  <TR> 
    <TD VALIGN=TOP nowrap >Satisfaction Letters</TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px;'><a href='/Customer/SatisfactionLettersList.asp'><font color=red><%=SatisfactionLettersWaiting%>
      </font></a></font></TD>
  </TR>
  <TR STYLE='DISPLAY:NONE'> 
    <TD VALIGN=TOP colspan="3"><img src="/myweb/spacer.gif" width="3" height="3"></TD>
  </TR>
  <tr STYLE='DISPLAY:NONE'> 
    <td colspan="3"><img src="/myweb/images/find+a+contractor.gif" width="146" height="18"></td>
  </tr>
  <tr STYLE='DISPLAY:NONE'> 
    <td align="center" colspan="3">
      <input type="text" name="txt_FCONTRACTOR" class="textboxflat" style='width:105px;color:133e71'>
      <input type="button" name="btn_SUBMITCONTRACTOR" value=" GO " class="RSLButton">
    </td>
  </tr>
  <TR> 
    <TD VALIGN=TOP colspan="3"><img src="/myweb/spacer.gif" width="3" height="3"></TD>
  </TR>
  <!--#include virtual="myweb/include/alerts.asp" -->
  <TR> 
    <TD VALIGN=TOP colspan="3"><img src="/myweb/spacer.gif" width="3" height="3"></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><img src="/myweb/images/staff+handbook.gif" width="146" height="18"></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><font style='font-size:10px'>Holiday Entitlements</font> 
    </TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><font style='font-size:10px'>Correspondence</font></TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><font style='font-size:10px'>Answering the 
      Phone</font> </TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap colspan="3"><font style='font-size:10px'>The Complete 
      Handbook </font></TD>
  </TR>
</TABLE>

