<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim left_inc
	left_inc = Session("TeamCode")
%>
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=200" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager --> My Whiteboard</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<script language="JavaScript">
function cC(itemRef, theCol){
	if (itemRef.style.color == "") 
		itemRef.style.color = theCol;
	else
		itemRef.style.color = "";	
}

function refreshitems(){
	parent.frm_crm.location.href = "iFrames/iCustomerJournal.asp"
	parent.swap_div(11, 'bottom')	
}

function open_me(int_nature, int_journal_id, str_redir){
	parent.frm_erm.location.href = str_redir + "?journalid="+int_journal_id+"&natureid="+int_nature;
}

function swap_div(int_which_one, str_where){
	
	var divid, imgid, upper
	
	if (str_where == 'top'){
		upper = 7;
		lower = 1;
		}
	else {
		upper = 12;
		lower = 8;
		}
	
	divid = "div" + int_which_one.toString();
	imgid = "img" + int_which_one.toString();
	
	// close all other divs and open src div
	for (i = lower ; i <= upper ; i++)
		document.getElementById("div" + i + "").style.display = "none";
	document.getElementById(divid).style.display = "block";
	
	// manipulate images
	for (j = lower ; j <= upper ; j++){
		document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
	}
	
	// unless last image in row
	if (int_which_one != upper)
		document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
	document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
}
		

</script>
<style TYPE="text/css">
	<!--
		a {text-decoration: none !important;}
		a:active {text-decoration: none !important;}
		a:hover {text-decoration: none !important;}
	-->
</STYLE>
<BODY BGCOLOR=#FFFFFF ONLOAD="preloadImages();initSwipeMenu(0)" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopWhiteboard.asp" -->
<div id=TheWholeBody>
<TABLE HEIGHT=100% CELLSPACING=0 CELLPADDING=0>
	<TR>
		<TD WIDTH=12PX VALIGN=TOP nowrap>&nbsp;</TD>
	  	<TD WIDTH=173PX VALIGN=TOP nowrap> 
        <!-- START OF LEFT SIDE BAR -->
        <!--#include virtual="myweb/include/crleft.asp" -->
        <!-- END OF LEFT SIDE BAR -->
		<BR>
		<TABLE STYLE='BORDER:1PX SOLID #133E71' WIDTH=150PX>
		<%
		OpenDB()
		SQL = "SELECT * FROM S_ORGANISATION WHERE ORGID = " & SESSION("ORGID")
		Call OpenRS(rsContractor, SQL)
		IFields = Array("Address1", "Address2", "Address3", "TownCity", "Postcode", "County")
		Response.Write "<TR><TD><b>" & rsContractor("Name") & "</b></TD></TR>"
		For i=0 to Ubound(IFields)
			iValue = rsContractor(IFields(i))
			if (iValue <> "" AND NOT ISNULL(iValue)) then
				Response.Write "<TR><TD>" & iValue & "</TD></TR>"
			end if
		Next
		CloseRS(rsContractor)
		CloseDB()
		%>
		</TABLE>
      	</TD>
      	<TD valign=top width=349px nowrap>
		
		<!-- START OF MAIN BODY NEWS -->						
		<!--#include file="myNews.asp" -->
		<!-- END OF MAIN BODY NEWS -->
		</TD>
		<TD>
			<IMG SRC="/myImages/spacer.gif" WIDTH=1 HEIGHT=17>
		</TD>
		<TD valign=top width=100% style='border-left:1px solid #133e71;border-bottom:1px solid #133E71' bgcolor="#f8f6f6">
			<!-- START OF TASKS AND MESSAGES -->
			<!--#include file="TasksAndMessages.asp" -->
			<!-- END OF TASKS AND MESSAGES-->
			</TD>
		</TR>
<tr><td></td><td colspan=4 valign=top>
	<TABLE width=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR>
		<TD ROWSPAN=2>
				<IMG NAME="img11" SRC="images/11-open.gif" WIDTH=120 HEIGHT=20 BORDER=0 onclick="refreshitems();swap_div(11, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img12" SRC="images/12-previous.gif" WIDTH=79 HEIGHT=20 BORDER=0 onclick="swap_div(12, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD nowrap><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=19><IMG NAME="img8" width=1 height=1><IMG NAME="img9" width=1 height=1><IMG NAME="img10" width=1 height=1></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004376>
			<IMG SRC="images/spacer.gif" WIDTH=550 HEIGHT=1></TD>
	</TR>
</TABLE>
<DIV ID=div8 STYLE='DISPLAY:NONE'>
</DIV>
<DIV ID=div9 STYLE='DISPLAY:NONE'>
</DIV>
<DIV ID=div10 STYLE='DISPLAY:NONE'>
</DIV>
<DIV ID=div11 STYLE='DISPLAY:BLOCK'>
<TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
  <TR> 
	<TD><iframe name=frm_crm src="iFrames/iCustomerJournal.asp" width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
  </TR>
</TABLE>
</DIV>
<DIV ID=div12 STYLE='DISPLAY:NONE'>
	            <TABLE <%=TABLE_DIMS%> STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE" >
                  <TR> 
                    <TD><iframe src="blank.asp" name=frm_erm width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
                  </TR>
                </TABLE>
</DIV>				
</td></tr>		
	</TABLE>
</div>
<!--#include virtual="Includes/Bottoms/BodyBottomWhiteboardWhite.asp" -->
<!--iframe name=hiddenWhiteboardServer src="blankRefresher.asp" style="display:none"></iframe-->
</BODY>
</HTML>