<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim repair_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, lst_contractor, action, contractor
			
	path = request.form("hid_go")
	
	repair_history_id 	= Request("repairhistoryid")
	nature_id 			= Request("natureid")
	
	// begin processing
	OpenDB()
	
	If path  = "" then path = 0 end if ' initial entry to page
	
	If path = 0 Then
		get_record()
	Elseif path = 1 Then
		new_record()
	End If
	
	CLoseDB()
	
	Function get_record()
	
		Dim strSQL
		
		strSQL = 	"SELECT		ISNULL(CONVERT(NVARCHAR, R.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
					"			ISNULL(R.NOTES, 'N/A') AS NOTES, " &_
					"			R.TITLE, R.REPAIRHISTORYID, O.NAME, O.ORGID, ITEMACTIONID AS ACTION, " &_
					"			S.DESCRIPTION AS STATUS " &_					
					"FROM		C_REPAIR R " &_
					"			LEFT JOIN C_STATUS S ON R.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN S_ORGANISATION O ON R.CONTRACTORID = O.ORGID " &_
					"			LEFT JOIN E__EMPLOYEE E ON R.LASTACTIONUSER = E.EMPLOYEEID " &_					
					"WHERE 		REPAIRHISTORYID = " & repair_history_id
					
		'response.write strSQL
		Call OpenRs(rsSet, strSQL)
		
		i_status 	= rsSet("STATUS")
		title 		= rsSet("TITLE")
		notes		= rsSet("NOTES")
		action 		= rsSet("ACTION")
		contractor	= rsSet("ORGID")
		fullname	= rsSet("FULLNAME")
		
		CloseRs(rsSet)

		Call BuildSelect(lst_action, "sel_ACTION", "C_ACTION WHERE ITEMACTIONID NOT IN (1,9,2,8)", "ITEMACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", action, Null, "textbox200", null)
		'Call BuildSelect(lst_contractor, "sel_CONTRACTOR", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", contractor, NULL, "textbox200", null)		
	End Function

	Function new_record()
	
		strSQL = "SELECT JOURNALID, ITEMSTATUSID FROM C_REPAIR WHERE REPAIRHISTORYID = " & repair_history_id
	
		Call OpenRs(rsSet, strSQL)
		
		journalid = rsSet("JOURNALID")
		r_status = rsSet("ITEMSTATUSID")
		
		CloseRS(rsSet)
				
		actionid = Request.Form("sel_ACTION")
		notes = Request.form("txt_NOTES")
		title = Request.form("txt_TITLE")
		contractor = request.form("PreviousContractor")
		
		if (actionid = 9) then 'this is an update to the data, therefore keep the previous actions and status
			actionid = Request.Form("PreviousAction")
		else
			strSQL = "SELECT STATUS FROM C_ACTION WHERE ITEMACTIONID = " & actionid
			Call OpenRs(rsSet, strSQL)
				r_status = rsSet("STATUS")
			CloseRS(rsSet)		
		end if
		
		strSQL = 	"INSERT INTO C_REPAIR " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, CONTRACTORID, TITLE, NOTES) " &_
					"VALUES (" & journalid & ", " & r_status & ", " & actionid & ", '" & Date & "', " & Session("userid") & ", '" & contractor & "', '" & title & "', '" & notes & "')"
		//response.write strSQL		
		set rsSet = Conn.Execute(strSQL)
		
		Call update_journal(journalid, r_status)
		
	End Function
	
	// updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)
	
		strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid		
		set rsSet = Conn.Execute(strSQL)
	
	End Function
	
%>
<html>
<head>
<title>CRM --> Update Repair</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_TITLE|Title|TEXT|Y"
	FormFields[1] = "sel_ACTION|Action|SELECT|Y"
	FormFields[2] = "txt_NOTES|Notes|TEXT|N"

	function save_form(){
		if (!checkForm()) return false;
			RSLFORM.hid_go.value = 1;
			RSLFORM.submit();
	}

	function return_data(){
	
		if (<%=path%> == 1)	{
			opener.location.reload();
			window.close();
			}
	}
</script>	
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="return_data();window.focus()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_small_long.gif" width="10" height="22" alt="" border=0 /></td>
	  <td width=302 style='border-top:1px solid #133e71' bgcolor=#133e71 align=center class='RSLWhite'><b>Update 
        Repair </b></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71' bgcolor=#133e71><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
		<tr>
			<td nowrap>Title:</td>
			<td><input type="text" class="textbox200" name="txt_TITLE" value="<%=title%>"></td>
			<TD><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>
			<td>&nbsp;</td>
		</tr>
		<TR>
			<TD>Action</TD>
			<TD COLSPAN=3>
				<%=lst_action%>
				<image src="/js/FVS.gif" name="img_ACTION" width="15px" height="15px" border="0">
			</TD>
		</TR>
		<TR>
			<TD nowrap>Status</td>
			<TD colspan=2><input type="text" class="textbox200" name="txt_STATUS" value="<%=i_status%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"></TD>
		</TR>		

		<TR>
			<TD nowrap>Last Action By</td>
			<TD colspan=2><input type="text" class="textbox200" name="txt_RECORDEDBY" value="<%=fullname%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_RECORDEDBY" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD valign=top>Notes</TD>
			<TD clspan=2><textarea style='OVERFLOW:HIDDEN' class="textbox200" name="txt_NOTES" rows=3><%=notes%></textarea></TD>	
			<TD><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0">
				<INPUT TYPE=BUTTON NAME='btn_submit' onclick='save_form()' value = 'Save' class="RSLButton" >
				<input type="hidden" name="dates" value="<% =BANK_HOLIDAY_STRING%>">
				<input type="hidden" name="hid_go" value=0>
				<input type="hidden" name="holtype">
				<input type="hidden" name="PreviousAction" value="<%=action%>">
				<input type="hidden" name="PreviousContractor" value="<%=contractor%>">				
			</TD>
		</TR>           

</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

