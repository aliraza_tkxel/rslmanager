<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
CustomerID = Request("CustomerID")
if (CustomerID = "") then
	CustomerID = -1
end if

OpenDB()
SQL = "SELECT C.CUSTOMERID, T.TENANCYID, P.PROPERTYID, C.TENANCY, TIT.DESCRIPTION AS TITLE, FIRSTNAME, LASTNAME, C.NINUMBER, C.DOB, " &_
	"HOUSENUMBER = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.HOUSENUMBER ELSE NULL END, " &_
	"ADDRESS1 = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.ADDRESS1 ELSE A.ADDRESS1 END, " &_
	"ADDRESS2 = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.ADDRESS2 ELSE A.ADDRESS2 END, " &_
	"ADDRESS3 = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.ADDRESS3 ELSE A.ADDRESS3 END, " &_	
	"TOWNCITY = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.TOWNCITY ELSE A.TOWNCITY END, " &_
	"POSTCODE = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.POSTCODE ELSE A.POSTCODE END, " &_
	"COUNTY = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.COUNTY ELSE A.COUNTY END, " &_	
	"CTY.DESCRIPTION AS CUSTOMERTYPE " &_
	"FROM C__CUSTOMER C " &_
	"LEFT JOIN G_TITLE TIT ON C.TITLE = TIT.TITLEID " &_
	"LEFT JOIN C_CUSTOMERTYPE CTY ON CTY.CUSTOMERTYPEID = C.CUSTOMERTYPE " &_
	"LEFT JOIN C_CUSTOMERTENANCY CT ON C.CUSTOMERID = CT.CUSTOMERID " &_
	"LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID AND T.ENDDATE IS NULL AND T.STARTDATE <= GETDATE() " &_
	"LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
	"LEFT JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1 " &_
	"WHERE C.CustomerID = " & CustomerID
					
Call OpenRs(rsLoader, SQL)
if (not rsLoader.EOF) then
	CustomerID = CustomerNumber(rsLoader("CustomerID"))
	title = rsLoader("TITLE")
	firstname = rsLoader("FIRSTNAME")
	lastname = rsLoader("LASTNAME")
	fullname = firstname & " " & lastname
	if (title <> "" AND NOT ISNULL(title)) then
		fullname = title & " " & fullname
	end if
	fullname = Replace(fullname, "  ", " ")
			
	tenancyid = rsLoader("TENANCYID")
	propertyref = rsLoader("PROPERTYID")	
	customertype = rsLoader("CUSTOMERTYPE")	
	housenumber = rsLoader("HOUSENUMBER")
	address1 = rsLoader("ADDRESS1")
	address2 = rsLoader("ADDRESS2")
	address3 = rsLoader("ADDRESS3")
	towncity = rsLoader("TOWNCITY")
	county = rsLoader("COUNTY")
	postcode = rsLoader("POSTCODE")
	NINUMBER = rsLoader("NINUMBER")
	DOB = rsLoader("DOB")
end if										
CloseRs(rsLoader)
CloseDB()
%>
<HTML>
<HEAD>
<TITLE>Repair Info:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_small_long.gif" width="10" height="22" alt="" border=0 /></td>
	  <td width=302 style='border-top:1px solid #133e71' bgcolor=#133e71 align=center class='RSLWhite'><b>Repair 
        Information</b></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71' bgcolor=#133e71><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=220 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<br>&nbsp;&nbsp;&nbsp;Below you will find the details of the customer and <br>&nbsp;&nbsp;&nbsp;property that the repair was reported for.<br><br>
<table>
<tr>
            <td><b>Customer No:</b></td>
    <td><%=CustomerID%></td></tr>
<tr>
            <td><b>Full Name:</b></td>
    <td><%=fullname%></td></tr>
<tr>
            <td><b>Telephone:</b></td>
    <td><%=telephone%></td></tr>
<tr>
            <td><b>Tenancy No:</b></td>
    <td><%=TenancyID%></td></tr>
<tr>
            <td><b>Property Ref:</b></td>
    <td><%=PropertyRef%></td></tr>		
<tr>
    <td><b>House No:</b></td>
    <td><%=housenumber%></td></tr>
<tr>
    <td><b>Address 1:</b></td>
    <td><%=address1%></td></tr>
<tr>
    <td><b>Address 2:</b></td>
    <td><%=address2%></td></tr>
<tr>
    <td><b>Address 3:</b></td>
    <td><%=address3%></td></tr>
<tr>
    <td><b>Town / City:</b></td>
    <td><%=towncity%></td></tr>
<tr>
    <td><b>County:</b></td>
    <td><%=county%></td></tr>
<tr>
    <td><b>Postcode:</b></td>
    <td><%=postcode%></td></tr>

</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

