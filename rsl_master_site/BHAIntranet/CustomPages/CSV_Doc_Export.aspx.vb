
Partial Class BHAIntranet_CustomPages_CSV_Doc_Export
    Inherits System.Web.UI.Page

    Private tab As dalMandatoryRead.I_MANDATORY_READDataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Write(Request.QueryString("DocID"))
        'Response.Write("<br />")
        'Response.Write(Request.QueryString("type"))
        'If Request.QueryString("type") Is Nothing Then
        'tab = BLLCustomerConsultation.getSCExportList(Request.QueryString("pid"), Nothing)


        tab = bllMandatoryReads.getSCExportList(Request.QueryString("DocID"))
        'For Each row As dalStartConsultation.TC_GETCUSTOMERCOMMUNICATIONLISTBYTYPERow In tab
        'Dim taJournal As New dalStartConsultationTableAdapters.C_STANDARDLETTERSTableAdapter
        'taJournal.ContactExport(ASPSession("UserID"), row.CUSTOMERLISTID)
        'Next
        generateExport()


        'End If
        'If Request.QueryString("pid") Is Nothing Then
        'Dim script As String
        'script = "<script type=""text/javascript"" language=""javascript""> window.close(); </script>"
        'ClientScript.RegisterStartupScript(Me.GetType(), "wclose", script)
        'End If
    End Sub

    Private Sub generateExport()
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=scexport.csv")
        Response.Write(bllMandatoryReads.ToCSV(tab))
        Response.End()
    End Sub
End Class
