Imports System.IO

Partial Class BHAIntranet_CustomPages_JobAppForm
    Inherits basepage

    Private Const _sAppFormPath As String = "~/BHAIntranet/Pages/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Try

                Dim bllJobForms As New bllJobForms(sessionhelper.ViewApplicationID)
                Dim sPath As String = ""

                Select Case bllJobForms.FormName
                    Case "External Application"
                        sPath = Server.MapPath(_sAppFormPath + "Application_Form_External.doc")
                    Case "Internal Application"
                        sPath = Server.MapPath(_sAppFormPath + "Application_Form_Internal.doc")
                End Select

                If sPath <> "" Then
                    Response.ContentType = "application/msword"
                    Response.WriteFile(sPath)
                    Response.End()


                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "inline; filename=" & sPath)
                    Response.ContentType = "application/doc"
                    Response.WriteFile(sPath)
                    Response.End()
                    Response.Flush()
                End If

            Catch ex As Exception

            End Try
        End If
    End Sub
End Class
