<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    AutoEventWireup="false" CodeFile="NewsList.aspx.vb" Inherits="BHAIntranet_CustomPages_NewsList"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <div>
        <asp:GridView ID="gvwNewsList" runat="server" AllowPaging="true" AutoGenerateColumns="False"
            BorderWidth="0" CssClass="gvw" DataKeyNames="NewsID" PageSize="10" ShowFooter="false"
            ShowHeader="false" Width="99%">
            <Columns>
                <asp:TemplateField>
                    <ItemStyle VerticalAlign="Top" />
                    <ItemTemplate>
                        <table width="99%">
                            <tr>
                                <td>
                                    <div class="newslistimage">
                                        <asp:Image ID="imgNews" runat="server" CssClass="newslistimage" ImageAlign="left"
                                            ImageUrl='<%# GetImagePath(Eval("ImagePath")) %>' />
                                    </div>
                                    <div class="newsitem">
                                        <asp:Label ID="lblPageName" runat="server" CssClass="newstitle" Text='<%# Eval("Headline") %>'></asp:Label><br />
                                        <asp:Label ID="lblDateSubmitted" runat="server" CssClass="dateSubmitted" Text='<%# Eval("DateCreated","{0:dd-MMM-yyyy}") %>' > </asp:Label><br />
                                        <asp:Label ID="lblPageContent" CssClass="newsTaster" runat="server" Text='<%# FormatPageContent(Eval("NewsContent")) %>'></asp:Label>
                                    </div>
                                </td>
                                <div>
                                </div>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="linklbl" runat="server" Text='<%#getURL(Eval("NewsID")) %>'></asp:Label>
                                    <!--<asp:LinkButton ID="lnkButton" runat="server" CommandArgument='<%# Eval("NewsID") %>'
                                        CommandName="select">Read Article</asp:LinkButton>-->
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 1px; background-color: gray">
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div id="rssLink">
            <a href="RSSFeed.aspx">Read all the latest news in housing</a></div>
    </div>
</asp:Content>
