<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    CodeFile="MandReadReport.aspx.vb" Inherits="BHAIntranet_CustomPages_MandReadReport"
    Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <ajax:ScriptManager ID="scriptmanager" runat="server" enablepartialrendering="false">
    </ajax:ScriptManager>
    <asp:Panel ID="pnlSearch" runat="server">
        <ajax:UpdatePanel runat="server" ID="upVal">
            <ContentTemplate>
                <asp:Label ID="lblValidation" runat="server" ForeColor="Red" Text=""></asp:Label>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
        <!-- disable expiry from & to parameters in search but hide labels for now 
         so code doesn't break  -->
        <asp:TextBox Visible="false" CssClass="mandInput" runat="server" ID="txtDateFrom"></asp:TextBox>
        <asp:TextBox Visible="false" CssClass="mandInput" runat="server" ID="txtDateTo"></asp:TextBox>
        <asp:Label CssClass="mandLabel" AssociatedControlID="txtDateCreatedFrom" ID="lblDateCreateFrom"
            runat="server" Text="Creation Date From"></asp:Label>
        <asp:TextBox CssClass="mandInput" runat="server" ID="txtDateCreatedFrom"></asp:TextBox>
        <asp:ImageButton ID="imbCreateFrom" CssClass="calBtn" ImageUrl="~/BHAIntranet/Images/General/Forms/calendar.gif"
            runat="server" />
        <cc1:CalendarExtender PopupButtonID="imbCreateFrom" Format="dd/MM/yyyy" ID="CalendarExtender1"
            TargetControlID="txtDateCreatedFrom" runat="server">
        </cc1:CalendarExtender>
        <br />
        <asp:Label CssClass="mandLabel" AssociatedControlID="txtDateCreatedTo" ID="lblDateCreateTo"
            runat="server" Text="Creation Date To"></asp:Label>
        <asp:TextBox CssClass="mandInput" runat="server" ID="txtDateCreatedTo"></asp:TextBox>
        <asp:ImageButton ID="imbCreateTo" CssClass="calBtn" runat="server" ImageUrl="~/BHAIntranet/Images/General/Forms/calendar.gif" />
        <cc1:CalendarExtender PopupButtonID="imbCreateTo" Format="dd/MM/yyyy" ID="CalendarExtender2"
            TargetControlID="txtDateCreatedTo" runat="server">
        </cc1:CalendarExtender>
        <br />
        <asp:Label CssClass="mandLabel" ID="lblTeam" AssociatedControlID="drpTeam" runat="server"
            Text="Team"></asp:Label>
        <asp:DropDownList ID="drpTeam"  CssClass="mandInputDrp" runat="server" DataTextField="TEAMNAME" DataValueField="TEAMID">
        </asp:DropDownList><br />
        
        <asp:Label CssClass="mandLabel" AssociatedControlID="txtDocTitle" ID="lblDocTitle"
            runat="server" Text="Document Title"></asp:Label>
        <asp:TextBox CssClass="mandInputTitle" runat="server" ID="txtDocTitle"></asp:TextBox>
        
        <div id="btn">
            <asp:Button ID="btnSearch" runat="server" Text="Search" />
        </div>
    </asp:Panel>
    <ajax:UpdatePanel runat="server" ID="upList">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlMandReadsReport">
                <asp:GridView BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" AutoGenerateColumns="False"
                    DataKeyNames="DocumentID,DOCNAME" ID="gvwMand" runat="server" CssClass="gvwMand" EmptyDataText="There are no documents that match your criteria" OnRowCommand="gvwMand_RowCommand" OnRowDataBound="gvwMand_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="DOCNAME" HeaderText="Document Name">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Employees Who Should Read">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EMPTOREAD") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="75px" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEmpToRead" runat="server" CommandName="ViewEmps" Text='<%# Bind("EMPTOREAD") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="EMPREAD" HeaderText="Employees Who Have Actually Read">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="75px" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Date Submitted</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSuggestion" runat="server" Text='<%# FormatDate(Eval("DateCreated")) %>'></asp:Label></ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            
            <asp:Panel runat="server" Visible="false" ID="pnlMandReadsEmpList">
            <asp:label ID="lblDocName" runat="server" text="Document" CssClass="DocNamelabel"></asp:label>
                <asp:GridView ID="gvMandatoryreadreport" runat="server" AutoGenerateColumns="false"
                    AllowPaging="True" CellPadding="3">
                    <Columns>
                        
                        <asp:BoundField DataField="EMPLOYEENAME" HeaderText="Employee">
                            <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="TEAMNAME" HeaderText="Team">
                            <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="HASREAD" HeaderText="Document Read">
                            <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"></HeaderStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <div id="btn">
                <asp:Button ID="Button1" runat="server" Text="Back to results" OnClick="Button1_Click" />
                <asp:Button ID="BtnExport" runat="server" Text="Export" OnClick="BtnExport_Click" />
                </div>
            </asp:Panel>
            
        </ContentTemplate>
        <Triggers>
            <ajax:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </ajax:UpdatePanel>
</asp:Content>
