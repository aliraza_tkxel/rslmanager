
Imports dalMandatoryReadTableAdapters

Partial Class BHAIntranet_CustomPages_MandReads
    Inherits basepage

    Private Const MandReadSessionVar As String = "mandReadID_"

    Public Enum NavMode
        LoadPage = 0
        Search = 1
        NewStarter = 2
        EmpDetails = 3
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/MandReads.css")
        master.SetHeading = "Latest Documents"

        If ASPSession("USERID") = "" Then
            Response.Redirect("/BHAIntranet/login.aspx?status=signout")
        End If

        If Not Page.IsPostBack Then            
            BindLastest()
            uiController(NavMode.LoadPage)
        End If

    End Sub
    Public Sub uiController(ByVal controllerid As Integer)


        If controllerid = NavMode.LoadPage Then

        End If


    End Sub
    
    Public Sub BindLastest()
        Dim bllMand As New bllMandatoryReads
        gvwLastest.DataSource = bllMand.GetLatest(ASPSession("USERID"))
        gvwLastest.DataBind()
    End Sub

    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dddd, dd MMMM yyyy")
        End If

    End Function

    Protected Sub gvwLastest_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwLastest.RowCommand
        If e.CommandName = "docread" Then
            ' first check if they have read the document before storing
            ' that they have read it

            ' its ok they've read it
            Dim intRowID As Integer
            Dim hfDocId As HiddenField
            Dim hfDocUrl As HiddenField

            intRowID = Val(e.CommandArgument)
            hfDocId = gvwLastest.Rows(intRowID).FindControl("hfDocID")
            hfDocUrl = gvwLastest.Rows(intRowID).FindControl("hfDocUrl")

            If hfDocId IsNot Nothing Then
                If hfDocUrl IsNot Nothing Then

                    ' extract url and doc ID from hidden fields
                    Dim intDocID As Integer
                    intDocID = Val(hfDocId.Value)
                    Dim sDocUrl As String = hfDocUrl.Value
                    Dim sMandStr As String = MandReadSessionVar & sDocUrl

                    If Session.Item(sMandStr) IsNot Nothing Then
                        DisplayInfo("This document will now be removed from youre mandatory list.")

                        Dim bllMand As New bllMandatoryReads
                        bllMand.CreateRead(intDocID, ASPSession("USERID"))
                        BindLastest()
                    Else
                        ' display they must read the document first!
                        DisplayInfo("Please first read the document before marking it as read.")
                    End If
                End If
            End If
        End If

    End Sub

    Protected Sub gvwLastest_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwLastest.PageIndexChanging
        gvwLastest.PageIndex = e.NewPageIndex
        BindLastest()
    End Sub

    Public Function FormatDate(ByVal strDate As String) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Return strDate
        End If
    End Function

    Public Function DocType(ByVal strFileName As Object) As String
        If IsDBNull(strFileName) Then
            Return "N/A"
        Else
            Return filehelper.GetDocImage(strFileName)
        End If

    End Function

    Protected Sub ReadLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton
        lnk = CType(sender, LinkButton)

        If lnk IsNot Nothing Then
            ' set the read session var
            Dim sUrl As String = lnk.CommandArgument
            Dim sAddVar As String = MandReadSessionVar & sUrl
            Session.Add(sAddVar, 1)

            ' open the document
            Dim strPath As String = Server.MapPath(sUrl)
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(strPath)

            If file.Exists Then
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                Response.AddHeader("Content-Length", file.Length.ToString())
                Response.ContentType = "application/doc"
                Response.WriteFile(file.FullName)
                Response.End()
                Response.Flush()
            End If
        End If

    End Sub
  
    Private Sub DisplayInfo(ByVal sInfo As String)

        Dim strScript As String
        strScript = "alert('" + sInfo + "');"
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "ShowInfo", strScript, True)

    End Sub

    Protected Sub gvwMand_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        ' we need to push the row index into the command argument so 
        ' it can be retrieved for accessing the hidden values for doc id and url
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton
            lnk = e.Row.FindControl("lnkRead")
            If lnk IsNot Nothing Then
                lnk.CommandArgument = e.Row.RowIndex.ToString
            End If
        End If
    End Sub

    Protected Sub gvwLastest_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwLastest.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' we have a hidden field bound to Mandatory field which is 
            ' used to turn on/off the 'I have read' link
            Dim lnkButton As LinkButton = e.Row.FindControl("lnkRead")
            If lnkButton IsNot Nothing Then
                Dim hf As HiddenField = e.Row.FindControl("hfMand")
                Dim hfd As HiddenField = e.Row.FindControl("hfDocID")

                If hf IsNot Nothing And hfd IsNot Nothing Then
                    Dim iDocID As Integer = hfd.Value
                    lnkButton.Visible = False

                    If hf.Value = "True" Then
                        ' we've found a mandatory document, but first check whether
                        ' it has been read. If it hasn't then don't make the read link
                        ' hidden.
                        Dim daAlreadyRead As New dalMandatoryReadTableAdapters.I_MANDATORY_READTableAdapter
                        Dim dtAlreadyRead As dalMandatoryRead.I_MANDATORY_READDataTable
                        dtAlreadyRead = daAlreadyRead.GetAlreadyRead(ASPSession("USERID"), iDocID)

                        If dtAlreadyRead.Rows.Count = 0 Then
                            lnkButton.Visible = True
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub gvwLastest_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwLastest.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton
            lnk = e.Row.FindControl("lnkRead")
            If lnk IsNot Nothing Then
                lnk.CommandArgument = e.Row.RowIndex.ToString
            End If
        End If
    End Sub
End Class
