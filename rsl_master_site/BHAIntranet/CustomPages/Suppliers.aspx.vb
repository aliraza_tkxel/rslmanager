
Partial Class BHAIntranet_CustomPages_Suppliers
    Inherits basepage
    Public Enum NavMode
        LoadPage = 0
        Search = 1
        Details = 2
    End Enum

    Public Sub uiController(ByVal controllerid As Integer)
        pnlSupplierList.Visible = False
        pnlSupplierDetails.Visible = False

        If controllerid = NavMode.LoadPage Then

        End If

        If controllerid = NavMode.Search Then
            pnlSupplierList.Visible = True
        End If

        If controllerid = NavMode.Details Then
            pnlSupplierDetails.Visible = True
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Suppliers.css")
        master.SetHeading = "Supplier Directory"

        If Not Page.IsPostBack Then
            BindTrades()
            formhelper.ClearForm(pnlSearch)
            If Request.QueryString("Search") <> "" Then
                ViewState("Name") = Request.QueryString("Search")
                BindSupplierList()
                uiController(NavMode.Search)
            End If
        End If
        If Not Page.IsPostBack Then
            Dim rdoListSearch As RadioButtonList = CType(master.Master.FindControl("rdoListSearch"), RadioButtonList)
            rdoListSearch.SelectedValue = 4
            Dim txt As TextBox = CType(master.Master.FindControl("txtSearch"), TextBox)
            txt.Text = Request.QueryString("Search")
        End If

    End Sub


    Public Sub BindTrades()
        Dim bllTrades As New bllTrade
        drpTrades.DataTextField = "DESCRIPTION"
        drpTrades.DataValueField = "TRADEID"
        drpTrades.DataSource = bllTrades.GetTrade
        drpTrades.DataBind()

    End Sub

    Public Sub BindSupplierList()
        Dim bllSupplier As New bllSupplier
        Dim intOrgID As Nullable(Of Integer)
        Dim strName As String
        If ViewState("OrgID") <> "" Then
            intOrgID = CInt(ViewState("OrgID"))
        End If

        If ViewState("Name") <> "" Then
            strName = ViewState("Name")
        Else
            strName = Nothing
        End If

        gvwSupplierList.DataSource = bllSupplier.Search(intOrgID, strName)
        gvwSupplierList.DataBind()
    End Sub



    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If drpTrades.SelectedValue = "" Then
            lblValidation.Text = "You must select a trade <br /><br />"
        Else
            lblValidation.Text = ""
            ViewState("OrgID") = drpTrades.SelectedValue
            ViewState("Name") = ""
            BindSupplierList()
            uiController(NavMode.Search)
        End If
    End Sub

    Protected Sub gvwSupplierList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwSupplierList.PageIndexChanging
        gvwSupplierList.PageIndex = e.NewPageIndex
        BindSupplierList()
    End Sub

    Protected Sub gvwSupplierList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwSupplierList.RowCommand
        If e.CommandName = "SelectSupp" Then
            Dim intOrgID As Integer = e.CommandArgument
            BindSupplier(intOrgID)
            ViewState("OrgID") = intOrgID
            uiController(NavMode.Details)
        End If
    End Sub


    Public Sub BindSupplier(ByVal OrgID As Integer)
        Dim bllSupplier As New bllSupplier(OrgID)
        Dim bllEmp As New bllEmployees
        lblAddressText.Text = bllSupplier.FullAddress
        lblTelText.Text = bllSupplier.Tel
        lblFaxText.Text = bllSupplier.Fax
        lblNameText.Text = bllSupplier.Name
        If bllSupplier.Website <> "" Then
            lblWebsiteText.Text = "<a href=""http://" & bllSupplier.Website & """>" & bllSupplier.Website & "</a>"
        Else
            lblWebsiteText.Text = "N/A"
        End If
        gvwContacts.DataSource = bllEmp.GetByOrgID(OrgID)
        gvwContacts.DataBind()
    End Sub

    Protected Sub lnkReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReturn.Click
        uiController(NavMode.Search)
    End Sub

    Protected Sub gvwContacts_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwContacts.PageIndexChanging
        gvwContacts.PageIndex = e.NewPageIndex
        Dim bllEmp As New bllEmployees

        BindSupplier(ViewState("OrgID"))        
    End Sub
End Class
