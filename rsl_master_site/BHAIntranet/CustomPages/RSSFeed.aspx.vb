Imports System.Data
Imports System.IO
Imports System.Xml

Partial Class BHAIntranet_CustomPages_RSSFeed
    Inherits basepage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Not Request.QueryString("page") Is Nothing Then
            Try
                gvwRss.PageIndex = Integer.Parse(Request.QueryString("page")) - 1
                'Bind()
            Catch ex As Exception

            End Try
        End If
        If Not Page.IsPostBack Then
            BindRSS()
        End If
        Page.Title = "RSS Feed"
    End Sub

    Private Sub BindRSS()
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/News.css")

        Dim reader As XmlTextReader = New XmlTextReader("http://www.24dash.com/rss/feed.rss")
        Dim ds As DataSet = New DataSet()
        ds.ReadXml(reader)
        gvwRss.DataSource = ds.Tables(3)
        gvwRss.DataBind()
    End Sub

    'Protected Sub gvwRss_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwRss.PageIndexChanging

    '    gvwRss.PageIndex = e.NewPageIndex
    '    BindRSS()
    'End Sub

    Public Function FormatDate(ByVal strDate As String) As String
        Return Left(strDate, Len(strDate) - 9)
    End Function

    Protected Sub gvwRss_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwRss.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim i As Integer
            For i = 0 To e.Row.Cells(0).Controls(0).Controls(0).Controls.Count - 1
                If Not TypeOf (e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls(0)) Is Label Then
                    Dim pb As LinkButton = DirectCast(e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls(0), LinkButton)
                    Dim lab As New Label()
                    lab.Text = "<a href='?page=" + pb.CommandArgument + "'>" + pb.Text + "</a>"
                    e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls.RemoveAt(0)
                    e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls.Add(lab)
                End If
            Next
        End If
    End Sub
End Class
