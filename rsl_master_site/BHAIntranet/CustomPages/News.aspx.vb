
Partial Class BHAIntranet_Admin_News
    Inherits basepage
    Private blnIsHeadline As Boolean
    Public Enum NavMode
        LoadPage = 0
        CreateNews = 1
        EditNews = 2
        ShowHeadline = 3
        HideHeadline = 4
    End Enum

    Public Sub uiController(ByVal controllerid As Integer)

        pnlNewsStory.Visible = False
        pnlNewsList.Visible = False
        If controllerid = NavMode.LoadPage Then
            pnlNewsList.Visible = True
            gvwNewsList.DataBind()
            fckNews.ToolbarSet = "WebManager"
        End If
        If controllerid = NavMode.CreateNews Then
            pnlNewsEditMode.Visible = False
            btnDelete.Visible = False
            pnlNewsStory.Visible = True
            pnlHomeImage.Visible = False
            formhelper.ClearForm(pnlNewsStory)
        End If
        If controllerid = NavMode.EditNews Then
            pnlNewsEditMode.Visible = True
            btnDelete.Visible = True
            pnlNewsStory.Visible = True

            If chkIsHeadline.Checked Then
                pnlHomeImage.Visible = True
                pnlHeadline.Visible = True
            Else
                pnlHomeImage.Visible = False
                pnlHeadline.Visible = False
            End If
        End If

        If controllerid = NavMode.ShowHeadline Then
            pnlHeadline.Visible = True
            pnlNewsStory.Visible = True
        End If
        If controllerid = NavMode.HideHeadline Then
            pnlHeadline.Visible = False
            pnlNewsStory.Visible = True

        End If
    End Sub

    Protected Sub lnkAddNews_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNews.Click
        uiController(NavMode.CreateNews)
        hidNewsID.Value = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_templates_main = CType(Me.Master, BHAIntranet_templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/WebManager.css")
        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)
        End If
        RegisterJSFunction()
        If CStr(Request("__EVENTARGUMENT")) = CStr("ServersideCallback") Then
            uiController(NavMode.LoadPage)
        End If
    End Sub

    Public Sub UpdateNews()
        uiController(NavMode.LoadPage)
    End Sub

    Public Sub RegisterJSFunction()
        Dim sb As New StringBuilder
        sb.Append("<script language=""JavaScript"" type=""text/javascript"">" & Environment.NewLine)
        sb.Append("<!--" & Environment.NewLine)
        sb.Append("function refreshForm()" & Environment.NewLine)
        sb.Append("{" & Environment.NewLine)
        sb.Append(ClientScript.GetPostBackEventReference(Me, "ServersideCallback") & Environment.NewLine)
        sb.Append("}" & Environment.NewLine)
        sb.Append(" -->" & Environment.NewLine)
        sb.Append("</script>" & Environment.NewLine)


        Page.ClientScript.RegisterClientScriptBlock(GetType(Page), "sbbc", sb.ToString)



    End Sub


    Protected Sub gvwNewsList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwNewsList.RowCommand
        Dim gvw As GridView = CType(sender, GridView)
        Dim intNewsID As Integer = gvw.DataKeys(e.CommandArgument).Value()

        hidNewsID.Value = intNewsID
        BindNews(intNewsID)
        uiController(NavMode.EditNews)
    End Sub

    Public Sub BindNews(ByVal newsid)
        Dim bllNews As New bllNews(newsid)

        txtHeadline.Text = bllNews.Headline
        fckNews.Value = bllNews.Content
        txtDate.Text = bllNews.DateCreated
        chkActive.Checked = bllNews.Active
        chkIsTenantOnline.Checked = bllNews.TenantOnline
        chkIsHeadline.Checked = bllNews.HomePage

        If Not String.IsNullOrEmpty(bllNews.ImagePath) Then
            imgNews.ImageUrl = "~/BHAIntranetImagesNews/" & bllNews.ImagePath
        Else
            imgNews.ImageUrl = "~/BHAIntranet/Images/Defaults/noimage.gif"
        End If

        If Not String.IsNullOrEmpty(bllNews.HomeImagePath) Then
            imgHomeImage.ImageUrl = "~/BHAIntranetImagesNews/" & bllNews.HomeImagePath
        Else
            imgHomeImage.ImageUrl = "~/BHAIntranet/Images/Defaults/noimage.gif"
        End If

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        uiController(NavMode.LoadPage)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim bllNews As New bllNews()
            bllNews.DeleteNews(hidNewsID.Value)
            gvwNewsList.DataBind()
            uiController(NavMode.LoadPage)
        Catch ex As Exception
            lblValidation.Text = ex.Message
        End Try
    End Sub

    Protected Sub chkActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkActive.CheckedChanged


    End Sub

    Protected Sub chkIsHeadline_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsHeadline.CheckedChanged
        If chkIsHeadline.Checked Then
            uiController(NavMode.ShowHeadline)
        Else
            uiController(NavMode.HideHeadline)
        End If
    End Sub
End Class

