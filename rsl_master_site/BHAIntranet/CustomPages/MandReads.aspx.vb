
Partial Class BHAIntranet_CustomPages_MandReads
    Inherits basepage

    Private Const MandReadSessionVar As String = "mandReadID_"

    Public Enum NavMode
        LoadPage = 0
        Search = 1
        NewStarter = 2
        EmpDetails = 3
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/MandReads.css")
        master.SetHeading = "Mandatory Reads"

        If ASPSession("USERID") = "" Then
            Response.Redirect("/BHAIntranet/login.aspx?status=signout")
        End If

        If Not Page.IsPostBack Then
            BindMand()            
            uiController(NavMode.LoadPage)

        End If
    End Sub
    Public Sub uiController(ByVal controllerid As Integer)


        If controllerid = NavMode.LoadPage Then

        End If


    End Sub
    Public Sub BindMand()
        Dim bllMand As New bllMandatoryReads
        gvwMand.DataSource = bllMand.GetMandatoryRead(ASPSession("USERID"), 100)
        gvwMand.DataBind()
    End Sub

    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dddd, dd MMMM yyyy")
        End If

    End Function

    Protected Sub gvwMand_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwMand.PageIndexChanging
        gvwMand.PageIndex = e.NewPageIndex
        BindMand()
    End Sub

    Protected Sub gvwMand_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwMand.RowCommand
        If e.CommandName = "docread" Then
            ' first check if they have read the document before storing
            ' that they have read it

            ' its ok they've read it
            Dim intRowID As Integer
            Dim hfDocId As HiddenField
            Dim hfDocUrl As HiddenField

            intRowID = Val(e.CommandArgument)
            hfDocId = gvwMand.Rows(intRowID).FindControl("hfDocID")
            hfDocUrl = gvwMand.Rows(intRowID).FindControl("hfDocUrl")

            If hfDocId IsNot Nothing Then
                If hfDocUrl IsNot Nothing Then

                    ' extract url and doc ID from hidden fields
                    Dim intDocID As Integer
                    intDocID = Val(hfDocId.Value)
                    Dim sDocUrl As String = hfDocUrl.Value

                    'Dim sMandStr As String = MandReadSessionVar & sDocUrl
                    Dim sMandStr As String = MandReadSessionVar & intDocID

                    If Session.Item(sMandStr) IsNot Nothing Then
                        Dim bllMand As New bllMandatoryReads
                        bllMand.CreateRead(intDocID, ASPSession("USERID"))
                        BindMand()
                    Else

                        ' display they must read the document first!
                        DisplayClientError("Please first read the document before marking it as read.")
                    End If
                End If
            End If
        End If

    End Sub

    Public Function FormatDate(ByVal strDate As String) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Return strDate
        End If
    End Function

    Public Function DocType(ByVal strFileName As Object) As String
        If IsDBNull(strFileName) Then
            Return "N/A"
        Else
            Return filehelper.GetDocImage(strFileName)
        End If

    End Function

    Public Function GetFileSize(ByVal aDocumentId As Integer) As String
        Dim FileSize As String = "-"
        Dim FileSizeInKB As Long
        Dim taDocs As New dalDocumentManagerTableAdapters.DOC_DOCUMENTTableAdapter

        If (taDocs.GetData(aDocumentId).Rows.Count > 0) Then
            Dim docRow As dalDocumentManager.DOC_DOCUMENTRow = taDocs.GetData(aDocumentId).Rows.Item(0)
            FileSizeInKB = docRow.DOCUMENTFILE.Length / 1024
            FileSize = FileSizeInKB.ToString() + "kb"
        End If
        Return FileSize
    End Function

    Protected Sub ReadLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton
        lnk = CType(sender, LinkButton)

        If lnk IsNot Nothing Then
            ' set the read session var
            Dim sUrl As String = lnk.CommandArgument

            Dim sAddVar As String = MandReadSessionVar & sUrl
            Session.Add(sAddVar, 1)

            Dim taDocs As New dalDocumentManagerTableAdapters.DOC_DOCUMENTTableAdapter
            Dim docTab As dalDocumentManager.DOC_DOCUMENTDataTable = taDocs.GetData(Integer.Parse(lnk.CommandArgument))
            Dim docRow As dalDocumentManager.DOC_DOCUMENTRow
            If docTab.Rows.Count > 0 Then
                docRow = docTab(0)
            Else

            End If
            
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & docRow.DOCFILE)
            Response.AddHeader("Content-Length", docRow.DOCUMENTFILE.Length)
            Response.ContentType = "application/doc"
            Response.BinaryWrite(docRow.DOCUMENTFILE)
            Response.End()
            Response.Flush()

            ' open the document
            'Dim strPath As String = Server.MapPath(sUrl)
            'Dim file As System.IO.FileInfo = New System.IO.FileInfo(strPath)

            'If file.Exists Then
            '    Response.Clear()
            '    Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            '    Response.AddHeader("Content-Length", file.Length.ToString())
            '    Response.ContentType = "application/doc"
            '    Response.WriteFile(file.FullName)
            '    Response.End()
            '    Response.Flush()
            'End If
        End If

    End Sub
  
    Private Sub DisplayClientError(ByVal sEerrorDesc As String)

        Dim strScript As String
        strScript = "alert('" + sEerrorDesc + "');"
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "UserSecurity", strScript, True)

    End Sub

    Protected Sub gvwMand_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwMand.RowCreated
        ' we need to push the row index into the command argument so 
        ' it can be retrieved for accessing the hidden values for doc id and url
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton
            lnk = e.Row.FindControl("lnkRead")
            If lnk IsNot Nothing Then
                lnk.CommandArgument = e.Row.RowIndex.ToString
            End If
        End If
    End Sub
End Class
