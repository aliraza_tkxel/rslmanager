Imports System.Data
Imports System.Web.Mail

Partial Class BHAIntranet_CustomPages_SuggestionReport
    Inherits basepage
    Public Enum NavMode
        LoadPage = 0
        ViewSugg = 1
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Suggestions.css")

        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)
        End If
    End Sub
    Public Function FormatPageContent(ByVal strPageContent)
        Return formhelper.TruncateText(strPageContent, 100)
    End Function

    Public Sub uiController(ByVal controllerid As Integer)

        If controllerid = NavMode.LoadPage Then

            Dim bllTeam As New bllTeam
            drpTeam.DataTextField = "TeamName"
            drpTeam.DataValueField = "TeamID"
            drpTeam.DataSource = bllTeam.GetTeamsWithPermissions(0)
            drpTeam.DataBind()
            formhelper.ClearForm(pnlSearch)

        End If

    End Sub


    Public Sub SaveFilter()
        ViewState("Topic") = txtTopic.Text
        ViewState("DateFrom") = txtDateFrom.Text
        ViewState("DateTo") = txtDateTo.Text
        ViewState("Team") = drpTeam.SelectedValue
    End Sub
    Public Function GetResults() As DataTable
        Dim bllSugg As New bllSuggestions
        Return bllSugg.Search(ViewState("Topic"), _
                        ViewState("DateFrom"), _
                        ViewState("DateTo"), _
                        formhelper.ProcessSelect(ViewState("Team")))

    End Function




    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        SaveFilter()
        Try

            gvwSuggestionList.DataSource = GetResults()
            gvwSuggestionList.DataBind()
            lblValidation.Text = ""
            gvwSuggestionList.Visible = True
            pnlSuggestion.Visible = False
        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try



    End Sub

    Protected Sub gvwSuggestionList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwSuggestionList.RowCommand
        pnlSuggestion.Visible = True
        gvwSuggestionList.Visible = False
        Dim gvw As GridView = CType(sender, GridView)
        Dim intSuggID As Integer = gvw.DataKeys(e.CommandArgument).Value()
        Dim bllSugg As New bllSuggestions(intSuggID)
        lblTopicDetailText.Text = bllSugg.Topic
        lblSuggDetailText.Text = bllSugg.Suggestion

        Dim dt As DateTime = bllSugg.DateSubmitted
        lblSuggDetailDateText.Text = dt.ToString("dd/MM/yyyy HH:mm")

        lblEmpDetailText.Text = bllSugg.EmpName
        lblTeamDetailText.Text = bllSugg.TeamName
        txtAction.Text = bllSugg.Action
        hidSuggID.Value = intSuggID

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvwSuggestionList.Visible = True
        pnlSuggestion.Visible = False
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim bllSugg As New bllSuggestions
        bllSugg.Update(hidSuggID.Value, txtAction.Text)

        ' Send email response to originator
        SendSuggestionResponse(hidSuggID.Value)

        gvwSuggestionList.Visible = True
        pnlSuggestion.Visible = False
    End Sub


    Private Sub SendSuggestionResponse(ByVal iSuggestion As Integer)

        Dim bllSuggestion As New bllSuggestions(iSuggestion)

        If bllSuggestion IsNot Nothing Then
            ' get suggestion originator
            Dim bllEmp As New bllEmployees(bllSuggestion.UserID)

            If bllEmp IsNot Nothing Then
                Dim sTo As String
                Dim sFrom As String
                Dim sFromName As String
                Dim sSubject As String
                Dim sBody As New StringBuilder


                ' get line manager for this employee
                Dim iLineManID As Integer
                iLineManID = bllEmp.LineManagerId
                Dim bllLineManager As New bllEmployees(iLineManID)

                If bllLineManager IsNot Nothing Then

                    ' First check there are email addresses stored to be able
                    ' to send the email notification. If not, just stored the
                    ' notification within the database.
                    If bllEmp.Email <> "N/A" And bllLineManager.Email <> "N/A" Then

                        sFrom = bllLineManager.Email
                        sFromName = bllLineManager.FullName
                        sTo = bllEmp.Email

                        sSubject = "Suggestion Notification Response"

                        sBody.Append("You have received a new action for your suggestion:")
                        sBody.AppendLine()
                        sBody.AppendLine("From      : " + sFromName)
                        sBody.AppendLine("Topic     : " + bllSuggestion.Topic)
                        sBody.AppendLine("Suggestion: " + bllSuggestion.Suggestion)
                        sBody.AppendLine("Action    : " + bllSuggestion.Action)
                        sBody.AppendLine()

                        Try
                            Dim mmSuggestNotification As New MailMessage
                            mmSuggestNotification.From = sFrom
                            mmSuggestNotification.To = sTo
                            mmSuggestNotification.Cc = "paula.strachan@broadlandhousing.org"
                            mmSuggestNotification.Subject = sSubject
                            mmSuggestNotification.Body = sBody.ToString

                            SmtpMail.Send(mmSuggestNotification)
                            
                        Catch ex As Exception
                            ErrorHelper.LogError(ex, True)
                        End Try
                    End If
                End If

            End If
        End If
    End Sub

    Public Function ToCSV(ByVal dataTable As DataTable) As String

        Dim sb As StringBuilder = New StringBuilder
        Dim strTmp As String = ""
        If (dataTable.Columns.Count <> 0) Then
            Dim i As Integer
            i = 1
            For Each column As DataColumn In dataTable.Columns
                If i < dataTable.Columns.Count Then
                    sb.Append(column.ColumnName & ",")
                Else
                    sb.Append(column.ColumnName)
                    sb.Append(vbCrLf)
                End If
                i = i + 1
            Next



            For Each row As DataRow In dataTable.Rows

                i = 1
                For Each column As DataColumn In dataTable.Columns

                    strTmp = row(column).ToString.Replace(",", "-")
                    strTmp = strTmp.Replace(Chr(13), "-")
                    strTmp = strTmp.Replace(Chr(10), "-")

                    If i < dataTable.Columns.Count Then
                        sb.Append(strTmp & ",")
                    Else
                        sb.Append(strTmp)
                        sb.Append(vbCrLf)
                    End If

                    i = i + 1
                Next


            Next
        End If

        Return sb.ToString()
    End Function

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.AddHeader("content-disposition", "attachment; filename=""" & "test.csv" & """")
        SaveFilter()
        Response.Write(ToCSV(GetResults()))

        Response.End()
    End Sub
End Class
