<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false"
    CodeFile="Stationery.aspx.vb" Inherits="BHAIntranet_CustomPages_Stationery" Title="Stationery Ordering"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" runat="Server">

    <script type="text/javascript">
    function OpenPrintPage(OrderID)
    {
        window.open('Print/PrintStationeryReport.aspx?OrderID=' + OrderID, target='new', 'width=720,height=625,resizable=yes,top=50,left=50,toolbar=yes,scrollbars=yes');        
    }    
    function OpenExportWindow()
    {
        window.open('Export/ExportStationery.aspx', target='new', 'width=720,height=625,resizable=yes,top=50,left=50,toolbar=yes,scrollbars=yes');        
    }   
    </script>

    <ajax:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </ajax:ScriptManager>
    <div id="stationeryHeader">
        Stationery Ordering
    </div>
    <div id="stationeryContentBox">
        <ajax:UpdatePanel ID="upOrders" runat="server">
            <ContentTemplate>
                <asp:ObjectDataSource ID="odsFilterOrderStatus" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="dalStationeryTableAdapters.I_STATIONERY_ORDER_STATUSTableAdapter">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsFilterItemStatus" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="dalStationeryTableAdapters.I_STATIONERY_ORDERITEM_STATUSTableAdapter">
                </asp:ObjectDataSource>
                <div class="tabContainer">
                    <cc1:TabContainer ID="tcSearch" runat="server" ActiveTabIndex="0">
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                            <HeaderTemplate>
                                Search Orders
                            </HeaderTemplate>
                            <ContentTemplate>
                                <div id="filterArea">
                                    <asp:Label ID="lblFilterValidator" runat="server" ForeColor="Red" Text="Label"></asp:Label>
                                    <div id="leftCol">
                                        <cc1:CalendarExtender ID="ceExtenderFrom" runat="server" PopupButtonID="imbDateReqFrom"
                                            TargetControlID="txtFilterDateReqFrom" Enabled="True" Format="dd/MM/yy">
                                        </cc1:CalendarExtender>
                                        <cc1:CalendarExtender ID="ceExtenderTo" runat="server" PopupButtonID="imbDateReqTo"
                                            TargetControlID="txtFilterDateReqTo" Enabled="True" Format="dd/MM/yy">
                                        </cc1:CalendarExtender>
                                        <asp:Label ID="lFilterDateReqFrom" CssClass="lblPrompt" runat="server" AssociatedControlID="txtFilterDateReqFrom"
                                            Text="Date Requested (from):"></asp:Label>
                                        <asp:TextBox ID="txtFilterDateReqFrom" runat="server" CssClass="lblField" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imbDateReqFrom" runat="server" CssClass="calBtn" ImageUrl="~/BHAIntranet/Images/General/Forms/cal.gif" /><br />
                                        <asp:Label ID="lFilterDateReqTo" CssClass="lblPrompt" runat="server" AssociatedControlID="txtFilterDateReqTo"
                                            Text="Date Requested (to):"></asp:Label>
                                        <asp:TextBox ID="txtFilterDateReqTo" runat="server" CssClass="lblField" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imbDateReqTo" runat="server" CssClass="calBtn" ImageUrl="~/BHAIntranet/Images/General/Forms/cal.gif" /><br />
                                        <asp:Label ID="lblOrderStatus" CssClass="lblPrompt" runat="server" AssociatedControlID="ddlFilterOrderStatus"
                                            Text="Order Status:"></asp:Label>
                                        <asp:DropDownList CssClass="lblField" ID="ddlFilterOrderStatus" runat="server" AppendDataBoundItems="True"
                                            Width="110px" DataSourceID="odsFilterOrderStatus" DataTextField="OrderStatus"
                                            DataValueField="OrderStatusId" AutoPostBack="true">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList><br />
                                        <asp:Label ID="lblItemStatus" CssClass="lblPrompt" runat="server" AssociatedControlID="ddlFilterItemStatus"
                                            Text="Item Status:"></asp:Label>
                                        <asp:DropDownList CssClass="lblField" ID="ddlFilterItemStatus" runat="server" AppendDataBoundItems="True"
                                            Width="110px" DataSourceID="odsFilterItemStatus" DataTextField="OrderStatus"
                                            DataValueField="OrderStatusId" AutoPostBack="true">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList><br />
                                    </div>
                                    <div id="rightCol">
                                        <asp:Label ID="lblTeam" runat="server" CssClass="lblPrompt" Text="Team:" AssociatedControlID="ddlFilterTeam"></asp:Label>
                                        <asp:DropDownList CssClass="lblField" Width="150px" ID="ddlFilterTeam" runat="server"
                                            AppendDataBoundItems="True" DataSourceID="odsTeams" DataTextField="TEAMNAME"
                                            DataValueField="TEAMID" OnSelectedIndexChanged="ddlFilterTeam_SelectedIndexChanged"
                                            AutoPostBack="True">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList><br />
                                        <asp:Label ID="lblEmp" runat="server" CssClass="lblPrompt" AssociatedControlID="ddlFilterEmployee"
                                            Text="Employee:"></asp:Label>
                                        <asp:DropDownList CssClass="lblField" Width="150px" ID="ddlFilterEmployee" runat="server"
                                            AppendDataBoundItems="True" AutoPostBack="True">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList><br />
                                        <asp:Label ID="lblFilterLessThan" runat="server" CssClass="lblPrompt" Text="Value less than:"
                                            AssociatedControlID="txtFilterLessThan"></asp:Label>
                                        <asp:TextBox ID="txtFilterLessThan" runat="server" CssClass="lblField"></asp:TextBox><br />
                                        <asp:Label ID="lblFilterGreaterThan" runat="server" CssClass="lblPrompt" Text="Value greater than:"
                                            AssociatedControlID="txtFilterGreaterThan"></asp:Label>
                                        <asp:TextBox ID="txtFilterGreaterThan" runat="server" CssClass="lblField"></asp:TextBox>
                                    </div>
                                    <div id="btnCol">
                                        <div id="btnArea">
                                            <asp:Button ID="btnFilter" runat="server" CssClass="btn" Text="Filter" /> 
                                            <asp:Button ID="btnClearFilter" runat="server" CssClass="btn" Text="Clear Filter" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </div>
                <span class="waitSpinner">
                    <ajax:UpdateProgress ID="upgOrders" runat="server" AssociatedUpdatePanelID="upOrders"
                        DynamicLayout="false">
                        <ProgressTemplate>
                            <asp:Image ID="imgWaitSpinner1" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
                        </ProgressTemplate>
                    </ajax:UpdateProgress>
                </span>
                <div class="tabContainer">
                    <cc1:TabContainer AutoPostBack="true" ID="tcMainTabs" runat="server" ActiveTabIndex="3">
                        <cc1:TabPanel ID="tbOrders" runat="server" HeaderText="TabPanel1">
                            <HeaderTemplate>
                                My Orders
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:MultiView ID="mvOrders" runat="server">
                                    <asp:View ID="vwOrders" runat="server">
                                        <asp:LinkButton ID="lnkAdd" runat="server">Add New Stationery Order</asp:LinkButton>
                                        <br />
                                        <br />
                                        <div class="gridViewContainer">
                                            <asp:GridView CssClass="orderGridView" AllowPaging="True" ID="gvOrders" PageSize="10"
                                                runat="server" DataKeyNames="OrderId" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField ReadOnly="True" DataField="OrderId" InsertVisible="False" SortExpression="OrderId"
                                                        HeaderText="Order Ref"></asp:BoundField>
                                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MM/yy}" DataField="RequestedDate"
                                                        SortExpression="RequestedDate" HeaderText="Requested Date"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Title" SortExpression="Description">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkOrderTitle" runat="server" CommandName="Select" Text='<%# Eval("Description") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="RequestedForEmpFullName" HeaderText="Requested For">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("RequestedForEmpFullName") %>'
                                                                Font-Bold="True"></asp:Label>
                                                            (<asp:Label ID="Label8" runat="server" Text='<%# Bind("RequestedForTeamName") %>'></asp:Label>)
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TotalCost" HtmlEncode="False" DataFormatString="{0:c}"
                                                        HeaderText="Total Cost" />
                                                    <asp:BoundField HeaderText="Status" DataField="OrderStatus" />
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No Stationery Orders Exist.
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="#EEEEEE" />
                                            </asp:GridView>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwEditOrder" runat="server">
                                        <asp:Panel ID="pnlOrderEdit" runat="server">
                                            <asp:Label ID="lblOrderValidation" runat="server" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lOrderRef" runat="server" Text="Order Ref" CssClass="lbl" AssociatedControlID="txtOrderRef"></asp:Label>
                                            <asp:TextBox ID="txtOrderRef" runat="server" CssClass="lblField" Enabled="False"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lDesc" runat="server" Text="Title:" CssClass="lbl" AssociatedControlID="txtDesc"></asp:Label>
                                            <asp:TextBox ID="txtDesc" runat="server"  MaxLength="50" CssClass="lblLongField"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="Label2" runat="server" Text="Requested For Team" CssClass="lbl" AssociatedControlID="ddlForTeam"></asp:Label>
                                            <asp:DropDownList ID="ddlForTeam" runat="server" CssClass="lblLongField" DataValueField="TEAMID"
                                                AppendDataBoundItems="True" DataTextField="TEAMNAME" DataSourceID="odsTeams"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:Label ID="Label3" runat="server" Text="Requested For Employee" CssClass="lbl"
                                                AssociatedControlID="ddlForEmp"></asp:Label>
                                            <asp:DropDownList ID="ddlForEmp" runat="server" CssClass="lblLongField" />
                                            <br />
                                            <asp:Label ID="lblDevLoc" runat="server" Text="Delivery Location" CssClass="lbl"
                                                AssociatedControlID="txtDevLoc"></asp:Label>
                                            <asp:TextBox ID="txtDevLoc" runat="server" TextMode="MultiLine" Rows="4" MaxLength="300" CssClass="lblMultiLine"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lTotalCost" runat="server" AssociatedControlID="lblTotalCostField"
                                                CssClass="lbl" Font-Bold="True" Text="Total Value"></asp:Label>
                                            <asp:Label ID="lblTotalCostField" runat="server" CssClass="lblTotalField"></asp:Label>
                                            <br />
                                            <br />
                                            <asp:Button ID="btnAddNewOrder" runat="server" Text="Save & Add Order Items" CssClass="btn"
                                                Width="175px"></asp:Button>
                                            <asp:Button ID="btnSubmitOrder" runat="server" CssClass="btnLong" Text="Submit Order" />
                                            <asp:Button ID="btnOk" CssClass="btn" runat="server" Text="Ok" Visible="False" Width="56px" /><asp:Button
                                                ID="btnUpdate" runat="server" Text="Update" CssClass="btn"></asp:Button>
                                            <asp:Button ID="btnCancel" CssClass="btn" Text="Back" runat="server"></asp:Button><asp:Button
                                                ID="btnDelete" runat="server" CssClass="btn" Text="Delete" />
                                            <asp:Button ID="btnPrintPreview" CssClass="btnLong" runat="server" Text="Print Preview"
                                                OnClick="btnPrintPreview_Click" /></asp:Panel>
                                        <cc1:ConfirmButtonExtender ID="cbxDelete" runat="server" ConfirmText="Delete this order and all order items?"
                                            TargetControlID="btnDelete" Enabled="True">
                                        </cc1:ConfirmButtonExtender>
                                        <cc1:ConfirmButtonExtender ID="cbxSubmit" runat="server" ConfirmText="Submit this Order? No order items can be added/changed after submission."
                                            TargetControlID="btnSubmitOrder" Enabled="True">
                                        </cc1:ConfirmButtonExtender>
                                    </asp:View>
                                </asp:MultiView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tbTeamOrders" runat="server" HeaderText="TabPanel1">
                            <HeaderTemplate>
                                Team Orders
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:MultiView ID="mvTeamOrders" runat="server">
                                    <asp:View ID="vwTeamOrders" runat="server">
                                        <div class="gridViewContainer">
                                            <asp:GridView CssClass="orderGridView" AllowPaging="True" ID="gvTeamOrders" runat="server"
                                                DataKeyNames="OrderId" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField ReadOnly="True" DataField="OrderId" InsertVisible="False" SortExpression="OrderId"
                                                        HeaderText="Order Ref"></asp:BoundField>
                                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MM/yy}" DataField="RequestedDate"
                                                        SortExpression="RequestedDate" HeaderText="Requested Date"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Title" SortExpression="Description">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkOrderTitle" runat="server" CommandName="Select" Text='<%# Eval("Description") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="RequestedByEmpFullName" HeaderText="Requested By">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblreqbyemp" runat="server" Text='<%# Bind("RequestedByEmpFullName") %>'
                                                                Font-Bold="True"></asp:Label>
                                                            (<asp:Label ID="lblreqbyteam" runat="server" Text='<%# Bind("RequestedByTeamName") %>'></asp:Label>)
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="RequestedForEmpFullName" HeaderText="Requested For">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("RequestedForEmpFullName") %>'
                                                                Font-Bold="True"></asp:Label>
                                                            (<asp:Label ID="Label8" runat="server" Text='<%# Bind("RequestedForTeamName") %>'></asp:Label>)
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TotalCost" HtmlEncode="False" DataFormatString="{0:c}"
                                                        HeaderText="Total Cost" />
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOrderStatus" runat="server" Text='<%# Bind("OrderStatus") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No Stationery Orders Exist.
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="#EEEEEE" />
                                            </asp:GridView>
                                            <br />
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwEditTeamOrder" runat="server">
                                        <asp:Panel ID="pnlViewTeamOrder" runat="server" Width="450px">
                                            <asp:Label ID="lTeamOrderRef" runat="server" AssociatedControlID="txtTeamOrderRef"
                                                CssClass="lbl" Text="Order Ref"></asp:Label>
                                            <asp:TextBox ID="txtTeamOrderRef" runat="server" CssClass="lblField" Enabled="False"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lTeamOrderDesc" runat="server" AssociatedControlID="txtTeamOrderDesc"
                                                CssClass="lbl" Text="Title:"></asp:Label>
                                            <asp:TextBox ID="txtTeamOrderDesc" runat="server" CssClass="lblField"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lblTeamReqForTeam" runat="server" AssociatedControlID="ddlTeamOrderForTeam"
                                                CssClass="lbl" Text="Requested For Team"></asp:Label>
                                            <asp:DropDownList ID="ddlTeamOrderForTeam" runat="server" CssClass="lblField" DataValueField="TEAMID"
                                                AppendDataBoundItems="True" DataTextField="TEAMNAME" DataSourceID="odsTeams"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:Label ID="lblTeamReqForEmp" runat="server" AssociatedControlID="ddlTeamOrderForEmp"
                                                CssClass="lbl" Text="Requested For Employee"></asp:Label>
                                            <asp:DropDownList ID="ddlTeamOrderForEmp" runat="server" CssClass="lblField" />
                                            <br />
                                            <asp:Label ID="lblTeamDevLoc" runat="server" AssociatedControlID="txtTeamOrderDevLoc"
                                                CssClass="lbl" Text="Delivery Location"></asp:Label>
                                            <asp:TextBox ID="txtTeamOrderDevLoc" runat="server" TextMode="MultiLine" Rows="4"
                                                CssClass="lblMultiLine"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lTeamTotalCost" runat="server" AssociatedControlID="lblTeamTotalCost"
                                                CssClass="lbl" Font-Bold="True" Text="Total Value"></asp:Label>
                                            <asp:Label ID="lblTeamTotalCost" runat="server" CssClass="lblTotalField" Font-Bold="True"></asp:Label>
                                            <br />
                                            <br />
                                            <asp:Button ID="btnTeamOrderOk" CssClass="btn" runat="server" Text="Ok" OnClick="btnTeamOrderOk_Click" />
                                            <asp:Button ID="btnPrintTeamOrder" CssClass="btnLong" runat="server" Text="Print Preview"
                                                OnClick="btnTeamOrderOk_Click" />
                                        </asp:Panel>
                                    </asp:View>
                                </asp:MultiView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tbAllOrders" runat="server">
                            <HeaderTemplate>
                                All Orders</HeaderTemplate>
                            <ContentTemplate>
                                <asp:MultiView ID="mvAllOrders" runat="server">
                                    <asp:View ID="vwAllOrders" runat="server">
                                        <div class="gridViewContainer">
                                            <asp:GridView CssClass="orderGridView" AllowPaging="True" ID="gvAllOrders" runat="server"
                                                DataKeyNames="OrderId" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField ReadOnly="True" DataField="OrderId" InsertVisible="False" SortExpression="OrderId"
                                                        HeaderText="Order Ref"></asp:BoundField>
                                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MM/yy}" DataField="RequestedDate"
                                                        SortExpression="RequestedDate" HeaderText="Requested Date"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Title" SortExpression="Description">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkOrderTitle" runat="server" CommandName="Select" Text='<%# Eval("Description") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="RequestedByEmpFullName" HeaderText="Requested By">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblreqbyemp" runat="server" Text='<%# Bind("RequestedByEmpFullName") %>'
                                                                Font-Bold="True"></asp:Label>
                                                            (<asp:Label ID="lblreqbyteam" runat="server" Text='<%# Bind("RequestedByTeamName") %>'></asp:Label>)
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="RequestedForEmpFullName" HeaderText="Requested For">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("RequestedForEmpFullName") %>'
                                                                Font-Bold="True"></asp:Label>
                                                            (<asp:Label ID="Label8" runat="server" Text='<%# Bind("RequestedForTeamName") %>'></asp:Label>)
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TotalCost" HtmlEncode="False" DataFormatString="{0:c}"
                                                        HeaderText="Total Cost" />
                                                    <asp:BoundField HeaderText="Status" DataField="OrderStatus" />
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No Stationery Orders Exist.
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="#EEEEEE" />
                                            </asp:GridView>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwEditAllOrder" runat="server">
                                        <asp:Panel ID="pnlViewAllOrder" runat="server" Width="450px">
                                            <asp:Label ID="lAllOrderRef" runat="server" Text="Order Ref" CssClass="lbl" AssociatedControlID="txtAllOrderRef"></asp:Label>
                                            <asp:TextBox ID="txtAllOrderRef" runat="server" CssClass="lblField" Enabled="False"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lAllOrderDesc" runat="server" Text="Title:" CssClass="lbl" AssociatedControlID="txtAllOrderDesc"></asp:Label>
                                            <asp:TextBox ID="txtAllOrderDesc" runat="server" CssClass="lblField"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lblAllReqForTeam" runat="server" Text="Requested For Team" CssClass="lbl"
                                                AssociatedControlID="ddlAllOrderForTeam"></asp:Label>
                                            <asp:DropDownList ID="ddlAllOrderForTeam" runat="server" CssClass="lblField" AutoPostBack="True"
                                                DataSourceID="odsTeams" DataValueField="TEAMID" DataTextField="TEAMNAME" AppendDataBoundItems="True">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:Label ID="lblAllReqForEmp" runat="server" Text="Requested For Employee" CssClass="lbl"
                                                AssociatedControlID="ddlAllOrderForEmp"></asp:Label>
                                            <asp:DropDownList ID="ddlAllOrderForEmp" runat="server" CssClass="lblField">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:Label ID="lblAllDevLoc" runat="server" Text="Delivery Location" CssClass="lbl"
                                                AssociatedControlID="txtAllOrderDevLoc"></asp:Label>
                                            <asp:TextBox ID="txtAllOrderDevLoc" runat="server" TextMode="MultiLine" Rows="4"
                                                CssClass="lblMultiLine"></asp:TextBox>
                                            <br />
                                            <asp:Label ID="lAllTotalCost" runat="server" AssociatedControlID="lblAllTotalCost"
                                                CssClass="lbl" Font-Bold="True" Text="Total Value"></asp:Label>
                                            <asp:Label ID="lblAllTotalCost" runat="server" CssClass="lblTotalField" Font-Bold="True"></asp:Label>
                                            <br />
                                            <br />
                                            <asp:Button ID="btnAllOrderOk" runat="server" Text="Ok" CssClass="btn" Width="56px">
                                            </asp:Button>
                                            <asp:Button ID="btnPrintAllOrder" CssClass="btnLong" runat="server" Text="Print Preview"
                                                OnClick="btnTeamOrderOk_Click" /></asp:Panel>
                                    </asp:View>
                                </asp:MultiView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tbOrderReport" runat="server">
                            <HeaderTemplate>
                                Order Report</HeaderTemplate>
                            <ContentTemplate>
                                <div id="orderItemReportContainer">
                                    <div id="orderItemBtn">
                                        <asp:Button ID="btnExport" runat="server" CssClass="btn" Text="Export" OnClick="btnExport_Click" />&nbsp;</div>
                                    <div id="orderItemReport">
                                        <asp:GridView CssClass="orderGridView" AllowPaging="True" ID="gvOrderItemsReport"
                                            runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gvOrderItemsReport_PageIndexChanging"
                                            OnRowDataBound="gvOrderItemsReport_RowDataBound" ShowFooter="True">
                                            <Columns>
                                                <asp:BoundField DataField="RequestedDate" HeaderText="Requested" SortExpression="RequestedDate"
                                                    DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
                                                <asp:TemplateField HeaderText="Requested For" SortExpression="RequestedFor">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("RequestedFor") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("RequestedFor") %>' Font-Bold="True"></asp:Label>
                                                        (<asp:Label ID="Label4" runat="server" Text='<%# Bind("RequestedForTeam") %>'></asp:Label>)
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="OrderTitle" HeaderText="Order Title" SortExpression="OrderTitle" />
                                                <asp:BoundField DataField="ItemDesc" HeaderText="Item Description" SortExpression="ItemDesc" />
                                                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                                <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" DataFormatString="{0:c}"
                                                    HtmlEncode="False" />
                                                <asp:BoundField DataField="Total" HeaderText="Total" ReadOnly="True" SortExpression="Total"
                                                    DataFormatString="{0:c}" HtmlEncode="False" />
                                                <asp:BoundField DataField="OrderStatus" HeaderText="Status" SortExpression="OrderStatus" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No Stationery Order Items Exist.
                                            </EmptyDataTemplate>
                                            <SelectedRowStyle BackColor="#EEEEEE" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </div>
            </ContentTemplate>
        </ajax:UpdatePanel>
        <br />
        <ajax:UpdatePanel ID="upOrderItems" runat="server">
            <ContentTemplate>
                <span class="waitSpinner">
                    <ajax:UpdateProgress ID="upgOrderItems" runat="server" AssociatedUpdatePanelID="upOrderItems"
                        DynamicLayout="false">
                        <ProgressTemplate>
                            <asp:Image ID="imgWaitSpinner2" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
                        </ProgressTemplate>
                    </ajax:UpdateProgress>
                </span>
                <cc1:TabContainer ID="tcOrderItems" runat="server">
                    <cc1:TabPanel runat="server" ID="tbOrderItems">
                        <HeaderTemplate>
                            Order Items
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:MultiView ID="mvOrderItems" runat="server">
                                <asp:View ID="vwItemsGrid" runat="server">
                                    <asp:LinkButton ID="lnkAddItem" runat="server">Add New Items</asp:LinkButton>
                                    <asp:LinkButton ID="lnkOrderChecked" runat="server" OnClick="lnkOrderChecked_Click">Order Checked Items</asp:LinkButton><cc1:ConfirmButtonExtender
                                        ID="cbeOrderChecked" runat="server" ConfirmText="Confirm ordering of selected items?"
                                        TargetControlID="lnkOrderChecked" Enabled="True">
                                    </cc1:ConfirmButtonExtender>
                                    <br />
                                    <br />
                                    <asp:GridView CssClass="orderGridView" AllowPaging="True" ID="gvOrderItems" runat="server"
                                        AutoGenerateColumns="False" DataKeyNames="OrderItemId">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:BoundField DataField="OrderItemId" HeaderText="Order Item Ref" InsertVisible="False"
                                                ReadOnly="True" SortExpression="OrderItemId" />
                                            <asp:BoundField DataField="ItemDesc" HeaderText="Item Description" SortExpression="ItemDesc" />
                                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                            <asp:BoundField DataField="Cost" DataFormatString="{0:c}" HeaderText="Cost" SortExpression="Cost"
                                                HtmlEncode="False" />
                                            <asp:BoundField DataField="TOTALCOST" DataFormatString="{0:c}" HeaderText="Total Cost"
                                                HtmlEncode="False" />
                                            <asp:BoundField DataField="Priority" HeaderText="Priority" />
                                            <asp:TemplateField HeaderText="Status">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("OrderStatus") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrderStatus" runat="server" Text='<%# Bind("OrderStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order">
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkOrderAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkOrderAll_CheckedChanged"
                                                        Text="Order" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkOrder" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Order items exist for this order.
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="#EEEEEE" />
                                    </asp:GridView>
                                </asp:View>
                                <asp:View ID="vwEditItems" runat="server">
                                    <asp:Panel ID="pnlOrderItemEdit" runat="server">
                                        <br />
                                        <asp:Label ID="lblOrderItemValidation" runat="server" ForeColor="Red"></asp:Label>
                                        <br />
                                        <asp:Label ID="lPageNo" runat="server" AssociatedControlID="txtPageNo" CssClass="lbl"
                                            Text="Page No"></asp:Label>
                                        <asp:TextBox ID="txtPageNo" runat="server" CssClass="lblField" MaxLength="10"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lItemNo" runat="server" AssociatedControlID="txtItemNo" CssClass="lbl"
                                            Text="Catalogue No"></asp:Label>
                                        <asp:TextBox ID="txtItemNo" MaxLength="10" runat="server" CssClass="lblField"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lItemDesc" runat="server" AssociatedControlID="txtItemDesc" CssClass="lbl"
                                            Text="Cat Description"></asp:Label>
                                        <asp:TextBox ID="txtItemDesc" MaxLength="250" runat="server" CssClass="lblLongField"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lQuantity" runat="server" AssociatedControlID="txtQuantity" CssClass="lbl"
                                            Text="Quantity"></asp:Label>
                                        <asp:TextBox ID="txtQuantity" MaxLength="3" runat="server" CssClass="lblField"></asp:TextBox><br />
                                        <asp:Label ID="lSize" runat="server" AssociatedControlID="txtSize" CssClass="lbl"
                                            Text="Size"></asp:Label>
                                        <asp:TextBox ID="txtSize" MaxLength="50" runat="server" CssClass="lblField"></asp:TextBox><br />
                                        <asp:Label ID="lColour" runat="server" AssociatedControlID="txtSize" CssClass="lbl"
                                            Text="Colour"></asp:Label>
                                        <asp:TextBox ID="txtColour" MaxLength="50" runat="server" CssClass="lblField"></asp:TextBox><br />
                                        <asp:Label ID="lCost" runat="server" AssociatedControlID="txtCost" CssClass="lbl"
                                            Text="Cost"></asp:Label>
                                        <asp:TextBox ID="txtCost" runat="server" MaxLength="9" CssClass="lblField"></asp:TextBox>
                                        <asp:Label ID="lCostHelper" runat="server" CssClass="lblSmallRight" Text="(Per item)"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblPriority" runat="server" AssociatedControlID="ddlItemPriority"
                                            CssClass="lbl" Text="Priority"></asp:Label>
                                        <asp:DropDownList ID="ddlItemPriority" runat="server" CssClass="lblField" AppendDataBoundItems="True"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Label ID="lStatus" runat="server" CssClass="lbl" Text="Status:"></asp:Label><asp:DropDownList
                                            ID="ddlOrderItemStatus" runat="server" CssClass="lblField" AppendDataBoundItems="True"
                                            AutoPostBack="True">
                                        </asp:DropDownList><br />
                                        <br />
                                        <asp:Button ID="btnAddNewItem" runat="server" CssClass="btn" Text="Add Item" />
                                        <asp:Button ID="btnUpdateItem" runat="server" CssClass="btn" Text="Update" />
                                        <asp:Button ID="btnCanceltem" CssClass="btn" runat="server" Text="Cancel" />
                                        <asp:Button ID="btnOkItem" runat="server" CssClass="btn" Text="Ok" Visible="False"
                                            Width="61px" />
                                        <asp:Button ID="btnDeleteItem" runat="server" CssClass="btn" Text="Delete" /></asp:Panel>
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
                <asp:ObjectDataSource ID="odsTeams" runat="server" TypeName="bllTeam" SelectMethod="GetTeamsWithPermissions">
                    <SelectParameters>
                        <asp:Parameter Type="Int32" DefaultValue="0" Name="PageID"></asp:Parameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajax:UpdatePanel>
    </div>
</asp:Content>
