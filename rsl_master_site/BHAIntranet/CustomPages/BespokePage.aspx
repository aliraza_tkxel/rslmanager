<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CMS.master" AutoEventWireup="false" CodeFile="BespokePage.aspx.vb" Inherits="BHAIntranet_CustomPages_BespokePage" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ObjectDataSource ID="odsNewStarters" runat="server" SelectMethod="GetNewStarters"
        TypeName="bllEmployees"></asp:ObjectDataSource>
    <asp:GridView ID="gvwNewStarters" runat="server" AllowPaging="True" DataSourceID="odsNewStarters"
        PageSize="1">
    </asp:GridView>
</asp:Content>

