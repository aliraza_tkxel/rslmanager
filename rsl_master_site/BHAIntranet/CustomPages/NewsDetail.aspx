<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master" AutoEventWireup="false" CodeFile="NewsDetail.aspx.vb" Inherits="BHAIntranet_CustomPages_NewsDetail" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" Runat="Server">


  

    <div id="newsimage">
    <asp:Image ID="imgNews" runat="server" />
    </div>
    <div id="newsdetailstext">
    <div id="details_backtolist"><a href="NewsList.aspx">Back to News Page</a></div>
    <asp:Label ID="lblTitle" CssClass="title"  runat="server" Text=""></asp:Label><br />
    <asp:Label ID="lblDateSubmitted" runat="server" CssClass="dateSubmitted" Text="" > </asp:Label><br />
     <br />
    <asp:Label ID="lblNewsContent" runat="server" Text=""></asp:Label>
    </div>

<div id="rssLink"><a href="RSSFeed.aspx">Read all the latest news in housing</a></div> 

</asp:Content>

