Imports System.Data

Partial Class BHAIntranet_CustomPages_SiteSearch
    Inherits basepage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/SiteSearch.css")
        master.SetHeading = "Intranet Search"

        If Not Page.IsPostBack Then
            Dim rdoListSearch As RadioButtonList = CType(master.Master.FindControl("rdoListSearch"), RadioButtonList)
            rdoListSearch.SelectedValue = 1
            Dim txt As TextBox = CType(master.Master.FindControl("txtSearch"), TextBox)
            txt.Text = Request.QueryString("Search")

            ViewState("search") = Request.QueryString("Search")
            Bind()
        End If

    End Sub

    Public Sub Bind()
        Try
            If ViewState("search") <> "" Then

                lblValidation.Text = ""
                Dim wsSearch As New ReidSearch.ReidSearch

                Dim dsSearch As New DataSet
                Dim dtSearch As New DataTable
                Dim drSearch As DataRow

                Dim dtPage As New DataTable
                Dim drPage As DataRow

                Dim strSearchPageName As String = ""
                Dim strAllowedPageName As String = ""
                Dim blnHasMatch As Boolean
                Dim i As Integer = 0

                'This Line must be different for Development and Live site.
                'modify according to the server you want to deploy.
                dsSearch = wsSearch.SearchByScope(ViewState("search"), "BH_Scope")

                'code line for live will be this
                'dsSearch = wsSearch.SearchByScope(ViewState("search"), "BH_Scope")
                'code line for development will be this
                'dsSearch = wsSearch.SearchByScope(ViewState("search"), "BHA_Scope")

                dtSearch = dsSearch.Tables(0)
                Dim bllPage As New bllPage
                dtPage = bllPage.GetAllowedPages(ASPSession("USERID"))

                For Each drSearch In dtSearch.Rows
                    strSearchPageName = drSearch.Item("path").ToString
                    'Response.Write(drSearch.Item("path").ToString + "<br />")
                    blnHasMatch = False
                    For Each drPage In dtPage.Rows
                        strAllowedPageName = drPage.Item("filename").ToString

                        If strSearchPageName.ToLower.Contains(strAllowedPageName.ToLower) Then
                            blnHasMatch = True
                        End If
                        'Response.Write(strSearchPageName & "--" & strAllowedPageName & "-" & blnHasMatch & "<br>")
                    Next
                    If blnHasMatch = False Then
                        dtSearch.Rows(i).Delete()
                    End If

                    i = i + 1
                Next

                'Old code by CK disable because not filtering results correctly.
                '
                'For Each drSearch In dtSearch.Rows
                '    strSearchPageName = drSearch.Item("path").ToString
                '    Response.Write(drSearch.Item("path").ToString)
                '    If InStr(strSearchPageName, "/") > 0 Then
                '        strSearchPageName = Right(strSearchPageName, Len(strSearchPageName) - InStrRev(strSearchPageName, "/"))
                '    End If
                '    blnHasMatch = False
                '    For Each drPage In dtPage.Rows
                '        strAllowedPageName = drPage.Item("filename").ToString

                '        If strSearchPageName.ToLower = strAllowedPageName.ToLower Then
                '            blnHasMatch = True
                '        End If
                '        'Response.Write(strSearchPageName & "--" & strAllowedPageName & "-" & blnHasMatch & "<br>")
                '    Next
                '    If blnHasMatch = False Then
                '        dtSearch.Rows(i).Delete()
                '    End If

                '    i = i + 1
                'Next

                gvwSearch.DataSource = dtSearch
                gvwSearch.DataBind()
            Else
                lblValidation.Text = "You must enter search criteria in the search box to perform a search"
            End If
        Catch ex As Exception
            'Dim errtext As String = ex.Message
            'Dim index As Integer = errtext.IndexOf(" at ")
            'If index <> -1 Then
            '    errtext = errtext.Substring(0, index)
            'End If
            lblValidation.Text = "Query returned no results"
        End Try
    End Sub

    Public Function FormatSearch(ByVal strSearch As String) As String

        strSearch = strSearch.Replace("<c0>", "<strong>")
        strSearch = strSearch.Replace("</c0>", "</strong>")
        strSearch = strSearch.Replace("<c1>", "<strong>")
        strSearch = strSearch.Replace("</c1>", "</strong>")

        Return strSearch
    End Function

    Protected Sub gvwSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwSearch.PageIndexChanging
        gvwSearch.PageIndex = e.NewPageIndex
        Bind()
    End Sub
End Class
