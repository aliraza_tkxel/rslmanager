<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    Trace="false" AutoEventWireup="false" CodeFile="SiteSearch.aspx.vb" Inherits="BHAIntranet_CustomPages_SiteSearch"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <asp:Label ID="lblValidation" runat="server" ForeColor="Red" Text=""></asp:Label>
    <asp:GridView ShowHeader="false" ShowFooter="false" AutoGenerateColumns="false" Width="100%"
        ID="gvwSearch" runat="server" AllowPaging="True">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <strong>
                        <asp:Label ID="lblTitle" runat="server" Text='<%#  Eval("title") %>'></asp:Label></strong>
                    &nbsp;
                    <br />
                    <asp:Label ID="lblSummary" runat="server" Text='<%#  FormatSearch(Eval("HitHighlightedSummary")) %>'></asp:Label>
                    &nbsp;
                    <br />
                    <div class="rsslink">
                        <asp:HyperLink ID="hlLink" runat="server" Text="View full story" NavigateUrl='<%#  Eval("path") %>'></asp:HyperLink>
                    </div>
                    <br />
                    <br />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
