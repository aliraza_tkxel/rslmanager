<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false" CodeFile="KeyMessages.aspx.vb" Inherits="BHAIntranet_Admin_KeyMessages" title="Untitled Page" %>
<%@ Register TagPrefix="FCKeditorV2" Namespace="FredCK.FCKeditorV2" Assembly="FredCK.FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" Runat="Server">
    <asp:ObjectDataSource ID="odsMessages" runat="server" SelectMethod="GetKeyMessages"
        TypeName="bllKeyMessages"></asp:ObjectDataSource>
         <asp:Panel Visible="false" ID="pnlMessageList" runat="server">
    <asp:LinkButton ID="lnkNewMessage" runat="server">Add New Message</asp:LinkButton><br />
    <br />
    <asp:GridView ID="gvwMessages" runat="server" AutoGenerateColumns="False" DataKeyNames="KeyMessageID"
        DataSourceID="odsMessages">
        <Columns>
            <asp:BoundField DataField="KeyMessageText" HtmlEncode="false" HeaderText="Key Message" SortExpression="KeyMessageText">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="350px" />
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" />
        </Columns>
    </asp:GridView>
</asp:Panel>

    <asp:Panel Visible="false" ID="pnlMessage" runat="server">
        <asp:HiddenField ID="hidMessageID" runat="server" />
        <asp:Label ID="lblValidation" runat="server" ForeColor="Red"></asp:Label>
       <FCKeditorV2:FCKeditor BasePath="/BHAIntranet/fckeditor/" Height="600px"  id="fckMessage" runat="server" Width="600px"></FCKeditorV2:FCKeditor>
         <div class="buttons">
        <asp:Button ID="btnBack" runat="server" Text="Back" />
        <asp:Button ID="btnDelete" runat="server" Text="Delete" />
        <asp:Button ID="btnUpdate" runat="server" Text="Update" />
        </div>     
        

    </asp:Panel>


</asp:Content>

