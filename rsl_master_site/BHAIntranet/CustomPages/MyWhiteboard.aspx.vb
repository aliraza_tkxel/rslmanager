
Imports RSLBHALiveModel


Partial Class BHAIntranet_MyWhiteboard
    Inherits basepage

    Private ReadOnly Property EmployeeId As Integer
        Get
            Return CInt(ASPSession("USERID"))
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'mp1.Show()
        Dim master As BHAIntranet_templates_main = CType(Me.Master, BHAIntranet_templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Whiteboard.css")

        Me.iframeUpdateDetails.Attributes.Item("src") = String.Concat("/myjob/popups/pContactPopup.asp?employeeid=", Me.EmployeeId.ToString())

        ' Cater for contractors logging in
        If ASPSession("TeamCode") IsNot Nothing AndAlso ASPSession("TeamCode") = "SUP" Then
            Response.Redirect("/MyWeb/MyWhiteboard.asp", True)
        Else
            Response.Redirect("/RSLDashboard/Bridge.aspx?Uid=" & EmployeeId.ToString(), True)
            'If Me.ShowUpdateDetails(Me.EmployeeId) Then
            '    Me.mpExtender.Show()
            'End If
        End If

    End Sub

    Public Function ShowUpdateDetails(ByVal EmployeeId As Integer) As Boolean

        Using model As New RSLBHALiveModel.RSLBHALiveEntities

            If Me.GetLogCount(EmployeeId) = 0 Then
                Return True
            Else
                Return False
            End If

        End Using

    End Function

    Public Function GetLogCount(ByVal EmployeeId As Integer) As Integer

        Dim count As Integer = 0

        Using model As New RSLBHALiveModel.RSLBHALiveEntities

            count = (From item In model.E_UPDATE_DETAILS_LOG Where item.EmployeeId = EmployeeId).Count()

        End Using

        Return count

    End Function

    Public Sub LogUpdateDetails(ByVal EmployeeId As Integer)

        Using model As New RSLBHALiveModel.RSLBHALiveEntities

            Dim log As New RSLBHALiveModel.E_UPDATE_DETAILS_LOG

            log.EmployeeId = Me.EmployeeId
            log.DateCreated = DateTime.Now
            log.UpdatedBy = Me.EmployeeId
            model.E_UPDATE_DETAILS_LOG.AddObject(log)
            model.SaveChanges()

        End Using

    End Sub

    Protected Sub btnUpdateDetails_Click(sender As Object, e As System.EventArgs) Handles btnUpdateDetails.Click

        Me.LogUpdateDetails(Me.EmployeeId)
        Me.mpExtender.Hide()

    End Sub

End Class
