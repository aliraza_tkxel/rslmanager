
Partial Class BHAIntranet_CustomPages_NewStarters
    Inherits basepage

    Private Enum NavMode
        LoadPage = 0
        EmpDetails
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Employees.css")
        master.SetHeading = "New Starters"

        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)
            BindNewStarters()
        End If

    End Sub

    Public Sub uiController(ByVal controllerid As Integer)

        If controllerid = NavMode.LoadPage Then
            pnlEmpDetails.Visible = False
            pnlNewStarterList.Visible = True
        End If

        If controllerid = NavMode.EmpDetails Then
            pnlNewStarterList.Visible = False
            pnlEmpDetails.Visible = True
        End If
    End Sub

    Protected Function FormatStartDate(ByVal strDate As Date) As String
        Dim dt As DateTime = strDate
        Return dt.ToString("dddd, dd MMMM yyyy")
    End Function

    Protected Function GetImagePath(ByVal objImage As Object) As String
        If IsDBNull(objImage) Then
            Return "/BHAIntranet/Images/Defaults/emp.gif"
        Else
            Return "/EmployeeImage/" & Trim(objImage.ToString)
        End If

    End Function

    Protected Sub gvwEmpList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwEmpList.RowCommand

        If e.CommandName = "SelectEmp" Then
            Dim gvw As GridView = CType(sender, GridView)
            Dim intEmpID As Integer = e.CommandArgument

            uiController(NavMode.EmpDetails)
            BindEmp(intEmpID)
        End If

    End Sub

    Public Sub BindEmp(ByVal EmpID As Integer)
        Dim bllEmp As New bllEmployees(EmpID)

        If Trim(bllEmp.Email) <> "N/A" Then
            lblWorkEmailText.Text = "<a href=""mailto:" & bllEmp.Email & """>" & bllEmp.Email & "</a>"
        Else
            lblWorkEmailText.Text = "N/A"
        End If

        If bllEmp.ImagePath <> "" Then
            imgEmp.ImageUrl = "/BusinessManager/EmployeeImages/" & bllEmp.ImagePath
        Else
            imgEmp.ImageUrl = "/BHAIntranet/Images/Defaults/emp.gif"
        End If
        lblNameText.Text = bllEmp.FullName
        lblTitleText.Text = bllEmp.JobTitle
        lblTeamText.Text = bllEmp.TeamName
        Dim dt As DateTime = bllEmp.StartDate
        lblStartDateText.Text = dt.ToString("dddd, dd MMMM yyyy")

        lblWorkPhoneText.Text = bllEmp.WorkDD
        lblWorkMobileText.Text = bllEmp.WorkMobile

        lblOfficeLocationText.Text = bllEmp.OfficeLocation
        lblLineManagerText.Text = bllEmp.LineManager
        If Trim(bllEmp.RolePath) <> "" Then
            hlnkRoleProfile.Attributes.Add("href", "/BusinessManager/Rolefiles/" & bllEmp.RolePath)
            hlnkRoleProfile.Text = "View Role Profile"
            'lblRoleProfileText.Text = ""
        Else
            'hlnkRoleProfile.Visible = False
            hlnkRoleProfile.NavigateUrl = ""
            hlnkRoleProfile.Text = "There is no role profile available"

            'lblRoleProfileText.Text = "There is no role profile available"
        End If

        lblProfileText.Text = bllEmp.Profile
    End Sub

    Private Sub BindNewStarters()
        Dim bllEmp As New bllEmployees
        gvwEmpList.DataSource = bllEmp.GetNewStarters()
        gvwEmpList.DataBind()
    End Sub

    Protected Sub gvwEmpList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwEmpList.PageIndexChanging    
        gvwEmpList.PageIndex = e.NewPageIndex
        BindNewStarters()
    End Sub

    Protected Sub lnkReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs)    
        uiController(NavMode.LoadPage)
    End Sub
End Class
