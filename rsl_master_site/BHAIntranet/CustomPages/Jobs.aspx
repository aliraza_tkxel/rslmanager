<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false"
    CodeFile="Jobs.aspx.vb" Inherits="BHAIntranet_CustomPages_Jobs" Title="Jobs"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/BHAIntranet/UserControl/General/JobsListing.ascx" TagName="JobsListing"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" runat="Server">
    <ajax:ScriptManager ID="smScriptManager" runat="server" EnablePartialRendering="true"
        OnAsyncPostBackError="smScriptManager_AsyncPostBackError">
    </ajax:ScriptManager>
    <div id="jobsHeader">
        Jobs@broadland
    </div>
    <div id="jobsContentBox">
        <span class="waitSpinner">
            <ajax:UpdateProgress ID="upgFilter" DisplayAfter="50" runat="server" AssociatedUpdatePanelID="upTopPanel"
                DynamicLayout="false">
                <ProgressTemplate>
                    <asp:Image ID="imgWaitSpinner3" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
                </ProgressTemplate>
            </ajax:UpdateProgress>
        </span>
        <ajax:UpdatePanel ID="upTopPanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <ajax:TabContainer ID="tcTopTabs" runat="server" EnableViewState="true" OnActiveTabChanged="tcTopTabs_ActiveTabChanged"
                    ActiveTabIndex="1" AutoPostBack="true">
                    <ajax:TabPanel runat="server" HeaderText="TabPanel1" ID="tbFindJobs">
                        <HeaderTemplate>
                            Find Jobs
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="filterArea">
                                <div class="inputCol">
                                    <asp:Label ID="lSearchTitle" runat="server" Text="Search for Jobs:" CssClass="lbl"
                                        AssociatedControlID="txtSearchTitle"></asp:Label>
                                    <asp:TextBox ID="txtSearchTitle" runat="server" CssClass="txt"></asp:TextBox>
                                    <asp:Label ID="lSearchTitleSub" runat="server" Text="e.g. job title" CssClass="lblSubText"></asp:Label>
                                </div>
                                <div class="inputCol">
                                    <asp:Label ID="lSearchLocation" runat="server" Text="Location:" CssClass="lbl" AssociatedControlID="ddlSearchLoc"></asp:Label>
                                    <asp:DropDownList ID="ddlSearchLoc" runat="server" CssClass="txt" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlSearchLoc_SelectedIndexChanged" DataSourceID="sqlGetLocs"
                                        DataValueField="OFFICEID" DataTextField="DESCRIPTION" AppendDataBoundItems="True">
                                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="inputCol">
                                    <asp:Label ID="lPostedIn" runat="server" Text="Posted In:" CssClass="lbl" AssociatedControlID="ddlSearchPostedIn"></asp:Label>
                                    <asp:DropDownList ID="ddlSearchPostedIn" runat="server" CssClass="txt" AutoPostBack="True">
                                        <asp:ListItem Text="Not specified" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Last 7 days" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Last 2 days" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Today" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="The last 4 hours" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="The last 2 hours" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="btnCol">
                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Search"
                                        CssClass="btn"></asp:Button>
                                    <asp:Button ID="btnClear" OnClick="btnClear_Click" runat="server" Text="Clear" CssClass="btn">
                                    </asp:Button>
                                    <asp:SqlDataSource ID="sqlGetLocs" runat="server" SelectCommand="SELECT [DESCRIPTION], [OFFICEID] FROM [G_OFFICE]"
                                        ConnectionString="<%$ ConnectionStrings:RSL Manager Broadlands DevConnectionString %>">
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel runat="server" HeaderText="TabPanel1" ID="tbPostJobs">
                        <HeaderTemplate>
                            Post Jobs
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="pnlPostJobs" runat="server">
                                <div id="postJobArea">
                                    <asp:Label ID="lblPostJobValidation" runat="server" ForeColor="Red"></asp:Label>
                                    <asp:Label ID="lPostTeam" runat="server" Text="Team:" CssClass="lbl" AssociatedControlID="ddlPostTeam"></asp:Label>
                                    <asp:DropDownList ID="ddlPostTeam" runat="server" CssClass="lblField" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                    <br />
                                    <div id="promptAndSubText">
                                        <asp:Label ID="lPostDesc" runat="server" Text="Description of job:" CssClass="lbl"
                                            AssociatedControlID="txtPostJobDesc"></asp:Label>
                                        <asp:Label ID="lPostDescSubText" runat="server" Text="Use a maximum of 2500 characters"
                                            CssClass="lblSubText"></asp:Label>
                                    </div>
                                    <asp:TextBox ID="txtPostJobDesc" runat="server" CssClass="lblLargeField" Rows="6"
                                        TextMode="MultiLine" MaxLength="2500"></asp:TextBox>
                                    <br />
                                    <asp:Label ID="lPostJobType" runat="server" Text="Job Type:" CssClass="lbl" AssociatedControlID="ddlPostJobType"></asp:Label>
                                    <asp:DropDownList ID="ddlPostJobType" AutoPostBack="true" runat="server" CssClass="lblField" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlPostJobType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:Label ID="lPostJobTypeVal" runat="server" Text="(e.g. Permanent, Contract, Temp/Fixed Term, etc)"
                                        CssClass="lblFieldVal"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblOtherType" Visible="false" runat="server" AssociatedControlID="txtPostJobTitle"
                                        CssClass="lbl" Text="Other Job Type:"></asp:Label><asp:TextBox ID="txtOtherJobType"
                                            runat="server" CssClass="lblField" MaxLength="50" visible="False"></asp:TextBox><br />
                                    <asp:Label ID="lPostJobTitle" runat="server" Text="Job Title:" CssClass="lbl" AssociatedControlID="txtPostJobTitle"></asp:Label>
                                    <asp:TextBox ID="txtPostJobTitle" runat="server" CssClass="lblField" MaxLength="200"></asp:TextBox>
                                    <br />
                                    <asp:Label ID="lPostJobRef" runat="server" Text="Job Ref:" CssClass="lbl" AssociatedControlID="txtPostJobRef"></asp:Label>
                                    <asp:TextBox ID="txtPostJobRef" runat="server" CssClass="lblField" MaxLength="30"></asp:TextBox>
                                    <br />
                                    <asp:Label ID="lPostJobLoc" runat="server" Text="Job Location:" CssClass="lbl" AssociatedControlID="ddlPostJobLoc"></asp:Label>
                                    <asp:DropDownList ID="ddlPostJobLoc" runat="server" CssClass="lblField" DataSourceID="sqlGetOfficeLocs"
                                        DataValueField="OFFICEID" DataTextField="DESCRIPTION" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sqlGetOfficeLocs" runat="server" SelectCommand="SELECT * FROM [G_OFFICE]"
                                        ConnectionString="<%$ ConnectionStrings:RSL Manager Broadlands DevConnectionString %>">
                                    </asp:SqlDataSource>
                                    <br />
                                    <asp:Label ID="lPostJobDuration" runat="server" Text="Duration:" CssClass="lbl" AssociatedControlID="txtPostJobDuration"></asp:Label>
                                    <asp:TextBox ID="txtPostJobDuration" runat="server" CssClass="lblField" MaxLength="30"></asp:TextBox>
                                    <asp:Label ID="lPostJobDurationVal" runat="server" Text="(e.g. Permanent, 6 months, 1 year etc)"
                                        CssClass="lblFieldVal"></asp:Label>
                                    <br />
                                    <asp:Label ID="lPostJobStart" runat="server" Text="Start Date:" CssClass="lbl" AssociatedControlID="txtPostJobStart"></asp:Label>
                                    <asp:TextBox ID="txtPostJobStart" runat="server" CssClass="lblField" MaxLength="30"></asp:TextBox>
                                    <asp:Label ID="lPostJobStartVal" runat="server" Text="(e.g. ASAP, January 1st 2006, etc)"
                                        CssClass="lblFieldVal"></asp:Label>
                                    <br />
                                    <asp:Label ID="lClosingDate" runat="server" Text="Closing Date:" CssClass="lbl" AssociatedControlID="txtClosingDate"></asp:Label>
                                    <asp:TextBox ID="txtClosingDate" runat="server" CssClass="lblField" MaxLength="10"></asp:TextBox><ajax:CalendarExtender
                                        ID="calCD" runat="server" Format="dd/MM/yyyy" PopupButtonID="imbCal" PopupPosition="TopLeft"
                                        TargetControlID="txtClosingDate">
                                    </ajax:CalendarExtender>
                                    <asp:ImageButton ID="imbCal" runat="server" ImageUrl="~/myImages/cal.gif" />
                                    <asp:Label ID="lClosingDateHint" runat="server" Text="(e.g. 15/01/2009)" CssClass="lblFieldVal"></asp:Label>
                                    <br />
                                    <asp:Label ID="lPostSalary" runat="server" Text="Salary/Rate:" CssClass="lbl" AssociatedControlID="txtPostJobSalary"></asp:Label>
                                    <asp:TextBox ID="txtPostJobSalary" runat="server" CssClass="lblField" MaxLength="30"></asp:TextBox>
                                    <asp:Label ID="lPostSalaryVal" runat="server" Text="(e.g. �12-15k+ benefits)" CssClass="lblFieldVal"></asp:Label>
                                    <br />
                                    <asp:Label ID="lPostContactName" runat="server" Text="Contact Name:" CssClass="lbl"
                                        AssociatedControlID="txtPostJobContactName"></asp:Label>
                                    <asp:TextBox ID="txtPostJobContactName" runat="server" CssClass="lblField" MaxLength="30"></asp:TextBox>
                                    <br />
                                    <asp:Label ID="lJobAppForm" runat="server" Text="Application Type:" CssClass="lbl"
                                        AssociatedControlID="ddlJobAppForm"></asp:Label>
                                    <asp:DropDownList ID="ddlJobAppForm" runat="server" CssClass="lblField" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                    <div id="btnArea">
                                        <asp:Button ID="btnReset" OnClick="btnReset_Click" runat="server" Text="Reset" CssClass="btn">
                                        </asp:Button>
                                        <asp:Button ID="btnSave" runat="server" Text="Save Job" CssClass="btn"></asp:Button>
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel runat="server" HeaderText="TabPanel1" ID="tbShortList">
                        <HeaderTemplate>
                            Shortlist
                        </HeaderTemplate>
                        <ContentTemplate>
                            <uc:JobsListing ID="ucJobsShortList" runat="server" Mode="ShortList"></uc:JobsListing>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel runat="server" HeaderText="TabPanel1" ID="tbJobDetail">
                        <HeaderTemplate>
                            Job Detail
                        </HeaderTemplate>
                        <ContentTemplate>
                            <uc:JobsListing ID="ucJobsDetail" runat="server" Mode="JobDetail"></uc:JobsListing>
                            <br />
                            <br />
                            <a href="#JobDetailArea"></a>
                        </ContentTemplate>
                    </ajax:TabPanel>
                </ajax:TabContainer>
                <br />
            </ContentTemplate>
        </ajax:UpdatePanel>
        <span class="waitSpinner">
            <ajax:UpdateProgress ID="upgOrders" DisplayAfter="50" runat="server" AssociatedUpdatePanelID="upBottom"
                DynamicLayout="false">
                <ProgressTemplate>
                    <asp:Image ID="imgWaitSpinner1" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
                </ProgressTemplate>
            </ajax:UpdateProgress>
        </span>
        <ajax:UpdatePanel ID="upBottom" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <ajax:TabContainer ID="tcSearchResults" runat="server" AutoPostBack="true">
                    <ajax:TabPanel runat="server" HeaderText="Search Results" ID="tbSearchResults">      
                        <ContentTemplate>
                            <uc:JobsListing runat="server" ID="ucJobsListing" Mode="ShowAll"></uc:JobsListing>
                        </ContentTemplate>
                    </ajax:TabPanel>
                </ajax:TabContainer>
            </ContentTemplate>
        </ajax:UpdatePanel>
    </div>
</asp:Content>
