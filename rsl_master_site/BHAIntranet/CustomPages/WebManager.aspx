<%@ Page Language="VB" Trace="false" MasterPageFile="~/BHAIntranet/Templates/main.master"
    ValidateRequest="false" AutoEventWireup="false" CodeFile="WebManager.aspx.vb"
    Inherits="BHAIntranet_Admin_WebManager" Title="Untitled Page" %>

<%@ Register Src="~/BHAIntranet/UserControl/Admin/BHAPagePermissions.ascx" TagName="BHAPagePermissions"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="myControls" Namespace="CustomControls" %>
<%@ Register TagPrefix="FCKeditorV2" Namespace="FredCK.FCKeditorV2" Assembly="FredCK.FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" runat="Server">
    <ajax:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </ajax:ScriptManager>
    <div id="treeviewnav">
        <span class="waitSpinner">
            <ajax:UpdateProgress ID="upgTree" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="upTree">
                <ProgressTemplate>
                    <asp:Image ID="imgWait1" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
                </ProgressTemplate>
            </ajax:UpdateProgress>
        </span>
        <ajax:UpdatePanel ID="upTree" runat="server">
            <ContentTemplate>
                <asp:ObjectDataSource ID="odsWebsites" runat="server" SelectMethod="GetWebsites"
                    TypeName="bllWebsite"></asp:ObjectDataSource>
                <asp:Panel ID="pnlWebMenu" runat="server" Height="50px" Width="125px">
                    &nbsp;<asp:DropDownList ID="drpWebsites" runat="server" AutoPostBack="True" DataSourceID="odsWebsites"
                        DataTextField="WebSiteDesc" DataValueField="WebsiteID">
                    </asp:DropDownList>
                    <myControls:MenuTreeview ID="tvwWebMenu" runat="server" ExpandDepth="1" ImageSet="XPFileExplorer"
                        NodeIndent="25" LeafNodeStyle-ChildNodesPadding="10" ShowLines="True">
                        <LeafNodeStyle ChildNodesPadding="10px" />
                    </myControls:MenuTreeview>
                </asp:Panel>
                <br />
            </ContentTemplate>
        </ajax:UpdatePanel>
    </div>
    <div id="pageEditing">
        <span class="waitSpinner">
            <ajax:UpdateProgress ID="upPageEditing" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="upEditingArea">
                <ProgressTemplate>
                    <asp:Image ID="imgWait" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif">
                    </asp:Image>
                </ProgressTemplate>
            </ajax:UpdateProgress>
        </span>
        <ajax:UpdatePanel ID="upEditingArea" runat="server">
            <ContentTemplate>
                <asp:Label ID="lblValidation" runat="server" ForeColor="Red"></asp:Label>&nbsp;
                <asp:Panel ID="pnlMenu" runat="server" Visible="false" Width="384px">
                    <asp:Label CssClass="genLabel" ID="lblMenuTitle" runat="server" Text="Menu Title"
                        AssociatedControlID="txtMenu"></asp:Label>
                    <asp:TextBox CssClass="genInput" ID="txtMenu" runat="server"></asp:TextBox><br />
                    <asp:Label CssClass="genLabel" ID="lblActive" runat="server" AssociatedControlID="chkMenuActive"
                        Text="Active"></asp:Label>
                    <asp:CheckBox CssClass="genChk" ID="chkMenuActive" runat="server" /><br />
                    <br />
                    <div id="menubuttons">
                        <asp:Button ID="btnDeleteMenu" CssClass="genButton" runat="server" Text="Delete Menu" />
                        <asp:Button ID="btnUpdateMenu" CssClass="genButton" runat="server" Text="Update Menu" />
                    </div>
                </asp:Panel>
                <br />
                <asp:Panel ID="pnlPage" runat="server" Visible="false">
                    <asp:Label CssClass="genLabel" ID="lblIsBespoke" runat="server" AssociatedControlID="chkIsBespoke"
                        Text="Is Bespoke"></asp:Label>
                    <asp:CheckBox CssClass="genChk" ID="chkIsBespoke" runat="server" AutoPostBack="True" /><br />
                    <br />
                    <asp:Label CssClass="genLabel" ID="lblPageTitle" runat="server" Text="Page Title"
                        AssociatedControlID="txtPageTitle"></asp:Label>
                    <asp:TextBox CssClass="genInput" ID="txtPageTitle" runat="server"></asp:TextBox><br />
                    <asp:Label CssClass="genLabel" ID="lblPageName" runat="server" Text="Friendly Page Name"
                        AssociatedControlID="txtPageName"></asp:Label>
                    <asp:TextBox CssClass="genInput" ID="txtPageName" runat="server"></asp:TextBox><br />
                    <asp:Panel ID="pnlBespoke" runat="server" Visible="false">
                        <asp:Label CssClass="genLabel" ID="lblBespoke" runat="server" AssociatedControlID="drpBespoke"
                            Text="Is Bespoke"></asp:Label>
                        <asp:DropDownList ID="drpBespoke" runat="server">
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:Panel ID="pnlTemplate" runat="server">
                        <asp:Label CssClass="genLabel" ID="lblActualPageName" runat="server" Text="Page Title"
                            AssociatedControlID="txtActualPageName"></asp:Label>
                        <asp:TextBox CssClass="genInput" ID="txtActualPageName" Enabled="false" runat="server"></asp:TextBox><br />
                    </asp:Panel>
                    <asp:Label CssClass="genLabel" ID="lblPageActive" runat="server" AssociatedControlID="chkPageActive"
                        Text="Active"></asp:Label>
                    <asp:CheckBox CssClass="genChk" ID="chkPageActive" runat="server" /><br />
                    <br />
                    <asp:Label CssClass="genLabel" ID="lblDefault" runat="server" AssociatedControlID="chkDefault"
                        Text="Is Default" Visible="False"></asp:Label>
                    <asp:CheckBox CssClass="genChk" ID="chkDefault" runat="server" Visible="False" /><br />
                    <br />
                    <asp:Label CssClass="genLabel" ID="lblWebSiteList" runat="server" Text="Do you want this page to appear in multiple menus"
                        AssociatedControlID="chklstWebsite" Visible="False"></asp:Label>
                    <asp:CheckBoxList ID="chklstWebsite" runat="server" DataSourceID="odsWebsites" DataTextField="WebSiteDesc"
                        DataValueField="WebsiteID" AutoPostBack="True" Visible="False">
                    </asp:CheckBoxList>
                    <br />
                    <asp:Panel ID="pnlMultiMenu" runat="server">
                    </asp:Panel>
                    <br />
                    <ajax:UpdatePanel ID="upTeamsSelect" runat="server">
                        <ContentTemplate>
                            <br />
                            <uc1:BHAPagePermissions ID="ucBHAPermissions" runat="server" />
                        </ContentTemplate>
                    </ajax:UpdatePanel>
                    <div id="pagebuttons">
                        <asp:Button ID="btnDeletePage" CssClass="genButton" runat="server" Text="Delete Page" />
                        <asp:Button ID="btnUpdatePage" CssClass="genButton" runat="server" Text="Update Page" />
                    </div>
                    <FCKeditorV2:FCKeditor BasePath="/BHAIntranet/fckeditor/" Height="600px" ID="fckPage"
                        runat="server" Width="600px">
                    </FCKeditorV2:FCKeditor>
                </asp:Panel>
                <ajax:UpdatePanel ID="upRanking" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlRankMenus" Visible="false" runat="server">
                            <asp:Label ID="lblRankMenus" CssClass="genLabel" AssociatedControlID="lstRankMenus"
                                runat="server" Text="Rank Menu"></asp:Label>
                            <asp:ListBox ID="lstRankMenus" CssClass="webranklist" runat="server"></asp:ListBox>
                            <br />
                            <div class="rnkbuttons">
                                <asp:Button ID="btnMenuUp" runat="server" Text="<<" />
                                <asp:Button ID="btnMenuDown" runat="server" Text=">>" />
                                <asp:Button ID="btnRankMenu" runat="server" Text="Rank" />
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlRankPages" Visible="false" runat="server">
                            <asp:Label CssClass="genLabel" AssociatedControlID="lstRankPages" ID="lblRankPages"
                                runat="server" Text="Rank Pages"></asp:Label>
                            <asp:ListBox ID="lstRankPages" CssClass="webranklist" runat="server"></asp:ListBox>
                            <br />
                            <div class="rnkbuttons">
                                <asp:Button ID="btnPageUp" runat="server" Text="<<" />
                                <asp:Button ID="btnPageDown" runat="server" Text=">>" />
                                <asp:Button ID="btnRankPage" runat="server" Text="Rank" />
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="tvwWebMenu" EventName="SelectedNodeChanged"></ajax:AsyncPostBackTrigger>
            </Triggers>
        </ajax:UpdatePanel>
    </div>
</asp:Content>
