<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false"
    CodeFile="News.aspx.vb" Inherits="BHAIntranet_Admin_News" Title="Untitled Page" %>

<%@ Register TagPrefix="FCKeditorV2" Namespace="FredCK.FCKeditorV2" Assembly="FredCK.FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" runat="Server">
    <asp:Label ID="lblValidation" runat="server" ForeColor="red"></asp:Label>
    <asp:ObjectDataSource ID="odsNewsList" runat="server" SelectMethod="GetNewsStories"
        TypeName="bllNews"></asp:ObjectDataSource>
    <br />
    <div id="divError" style="color: Red">
    </div>
    <asp:Panel ID="pnlNewsList" runat="server">
        <asp:LinkButton ID="lnkAddNews" runat="server">Add a News Story</asp:LinkButton><br />
        <br />
        <asp:GridView Width="400px" ID="gvwNewsList" EmptyDataText="There are no news stories in the system"
            runat="server" AutoGenerateColumns="False" DataKeyNames="NewsID" DataSourceID="odsNewsList">
            <Columns>
                <asp:BoundField DataField="Headline" HeaderText="Headline" SortExpression="Headline">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    <ItemStyle Width="300px" />
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel Visible="false" ID="pnlNewsStory" runat="server">
        <asp:HiddenField ID="hidNewsID" runat="server" />
        <asp:Label ID="lblHeadline" AssociatedControlID="txtHeadline" CssClass="genLabel"
            runat="server" Text="News Headline"></asp:Label>
        <asp:TextBox ID="txtHeadline" runat="server" CssClass="genInput"></asp:TextBox><br />
        <asp:Panel ID="pnlNewsEditMode" runat="server">
            <asp:Label ID="lblDate" AssociatedControlID="txtDate" CssClass="genLabel" runat="server"
                Text="Date Created"></asp:Label>
            <asp:TextBox ID="txtDate" runat="server" Enabled="false" CssClass="genInput"></asp:TextBox><br />
            <asp:Label ID="lblImage" AssociatedControlID="imgNews" CssClass="genLabel" runat="server"
                Text="Image"></asp:Label>
            <asp:Image ID="imgNews" runat="server" /><br />
            <br />
        </asp:Panel>
        <asp:Label ID="lblFileImage" AssociatedControlID="filNewsImage" CssClass="genLabel"
            runat="server" Text="News Image"></asp:Label>
        <asp:FileUpload ID="filNewsImage" CssClass="genFileInput" runat="server" /><br />
        <asp:Label ID="lblActive" AssociatedControlID="chkActive" CssClass="genLabel" runat="server"
            Text="Active"></asp:Label>
        <asp:CheckBox ID="chkActive" CssClass="genChk" runat="server" /><br />
        <br />
        <asp:Label ID="lblIsHeadline" AssociatedControlID="chkIsHeadline" CssClass="genLabel"
            runat="server" Text="Is Headline"></asp:Label>
        <asp:CheckBox ID="chkIsHeadline" AutoPostBack="true" CssClass="genChk" runat="server" /><br />
        
        <br />
        
         <asp:Label ID="lblIsTenantOnline" AssociatedControlID="chkIsTenantOnline" CssClass="genLabel"
            runat="server" Text="Is Tenant Online"></asp:Label>
            <asp:CheckBox ID="chkIsTenantOnline" AutoPostBack="true" CssClass="genChk" runat="server" /><br />
        <br />
        <asp:Panel ID="pnlHeadline" Visible="false" runat="server">
            <asp:Label ID="lblNewsHeadlineImage" AssociatedControlID="filHomeImage" CssClass="genLabel"
                runat="server" Text="News Image"></asp:Label>
            <asp:FileUpload ID="filHomeImage" CssClass="genFileInput" runat="server" /><br />
            <asp:Panel ID="pnlHomeImage" runat="server">
                <asp:Label ID="lblHomeImage" AssociatedControlID="imgHomeImage" CssClass="genLabel"
                    runat="server" Text="Home Image"></asp:Label>
                <asp:Image ID="imgHomeImage" runat="server" /><br />
                <br />
            </asp:Panel>
        </asp:Panel>
        <div class="newsbuttons">
            <asp:Button ID="btnBack" runat="server" Text="Back" />
            <asp:Button ID="btnDelete" runat="server" Text="Delete" />
            <asp:Button ID="btnIframe" runat="server" OnClientClick="aspnetForm.target='ifrm_news';"
                UseSubmitBehavior="false" PostBackUrl="~/BHAIntranet/CustomPages/FileUpload.aspx"
                Text="Save Story" />
        </div>
        <FCKeditorV2:FCKeditor BasePath="/BHAIntranet/fckeditor/" Height="600px" ID="fckNews"
            runat="server" Width="600px">
        </FCKeditorV2:FCKeditor>
    </asp:Panel>
    <iframe width="600" id="fileupload" style="display: none" name="ifrm_news" height="600"
        src="iframe_holder.aspx"></iframe>
</asp:Content>
