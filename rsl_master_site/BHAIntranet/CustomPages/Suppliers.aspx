<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    AutoEventWireup="false" CodeFile="Suppliers.aspx.vb" Inherits="BHAIntranet_CustomPages_Suppliers"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server">
        <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red"></asp:Label>
        <asp:Label ID="lblTrade" runat="server" AssociatedControlID="drpTrades" Text="Trade"></asp:Label>
        <asp:DropDownList ID="drpTrades" runat="server">
        </asp:DropDownList>
        <asp:Button ID="btnSearch" CssClass="btn" runat="server" Text="Find" />
    </asp:Panel>
    <asp:Panel ID="pnlSupplierList" Visible="false" runat="server">
        <asp:GridView Visible="true" EmptyDataText="There are no suppliers that match your criteria"
            ShowFooter="false" Width="100%" ShowHeader="false" ID="gvwSupplierList" AutoGenerateColumns="False"
            runat="server" AllowPaging="True" PageSize="10" >
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div id="empcontent">
                            <div id="nameandtitle">
                                <asp:Label ID="lblNameText" runat="server" Text='<%# eval("NAME") %>' />
                            </div>
                            <div id="teamandstart">
                                <asp:Label ID="lblTeam" runat="server" Text="Address:" AssociatedControlID="lblTeamText" />
                                <asp:Label ID="lblTeamText" runat="server" Text='<%# eval("FULLADDRESS") %>' /><br />
                                <asp:Label ID="lblStartDate" runat="server" AssociatedControlID="lblStartDate" Text="Tel:" />
                                <asp:Label ID="lblStartDateText" runat="server" Text='<%# eval("TELEPHONE1") %>' /><br />
                            </div>
                            <div id="profile">
                                <asp:Label ID="lblProfile" runat="server" Text="" />
                            </div>
                            <div class="emplink">
                                <asp:LinkButton ID="lnkSupp" CommandName="SelectSupp" CommandArgument='<%# eval("ORGID") %>'
                                    runat="server">View Details</asp:LinkButton><br />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlSupplierDetails" Visible="false" runat="server">
        <div id="empcontent">
            <div id="nameandtitle">
                <asp:Label ID="lblName" runat="server" Text="Name: " />:
                <asp:Label ID="lblNameText" runat="server" />
            </div>
            <div id="teamandstart">
                <asp:Label ID="lblAddress" AssociatedControlID="lblAddressText" CssClass="lbl" runat="server"
                    Text="Address:" />
                <asp:Label ID="lblAddressText" CssClass="lbletxt" runat="server" /><br /><br />
                <asp:Label ID="lblTel" AssociatedControlID="lblTelText" CssClass="lbl" runat="server"
                    Text="Tel:" />
                <asp:Label ID="lblTelText" CssClass="lbltxt" runat="server" /><br /><br />
                <asp:Label ID="lblFax" AssociatedControlID="lblFaxText" CssClass="lbl" runat="server"
                    Text="Fax:" />
                <asp:Label ID="lblFaxText" CssClass="lbltxt" runat="server" /><br /><br />
                <asp:Label ID="lblWebsite" AssociatedControlID="lblWebsiteText" CssClass="lbl" runat="server"
                    Text="Website:" />
                <asp:Label ID="lblWebsiteText" CssClass="lbltxt" runat="server" /><br /><br />
                <br />
                <div class="emplink">
                    <asp:LinkButton ID="lnkReturn" runat="server">Return to List</asp:LinkButton>
                </div>
            </div>
            <br />
            <asp:GridView ID="gvwContacts" runat="server" AllowPaging="True" PageSize="10" AutoGenerateColumns="False"
                EmptyDataText="There are no contacts for this supplier">
                <Columns>
                    <asp:BoundField DataField="FullName" HeaderText="Name">
                        <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="JobTitle" HeaderText="Job Title">
                        <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField DataFormatString="<a href=mailto:{0}>{0}</a>" HtmlEncode="false" DataField="WORKEMAIL"
                        HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="WorkDD" HeaderText="Phone">
                        <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Mobile" HeaderText="Mobile">
                        <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>
</asp:Content>
