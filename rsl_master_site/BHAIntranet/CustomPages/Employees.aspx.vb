
Partial Class BHAIntranet_CustomPages_Employees
    Inherits basepage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Employees.css")
        master.SetHeading = "Employee Directory"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        If Not Page.IsPostBack Then

            If Request.QueryString("ViewEmpID") IsNot Nothing Then

                ' View specific employee from New starters pane in whiteboard
                Dim intEmpID As Integer = CType(Request.QueryString("ViewEmpID"), Integer)
                ViewState.Add("ViewEmpID", intEmpID)
                BindEmp(intEmpID)
                uiController(NavMode.LatestEmp)

            Else
                If Request.QueryString("ViewNew") IsNot Nothing Then
                    ' View all new starters
                    BindNewStarters()
                    uiController(NavMode.NewStarter)
                Else

                    Dim rdoListSearch As RadioButtonList = CType(master.Master.FindControl("rdoListSearch"), RadioButtonList)
                    rdoListSearch.SelectedValue = 3
                    Dim txt As TextBox = CType(master.Master.FindControl("txtSearch"), TextBox)
                    txt.Text = Request.QueryString("Search")

                    BindTeam()

                    ' View employees as a result of a search
                    If Request.QueryString("search") <> "" Then
                        ViewState("Name") = Request.QueryString("search")
                        ViewState("TeamID") = ""
                        BindEmpList()
                        uiController(NavMode.LoadPage)
                        uiController(NavMode.Search)
                    Else
                        ' Blank form ready for search
                        uiController(NavMode.LoadPage)
                    End If
                End If
            End If

        End If
    End Sub

    Public Enum NavMode
        LoadPage = 0
        Search = 1
        NewStarter = 2
        EmpDetails = 3
        LatestEmp = 4
    End Enum

    Public Sub uiController(ByVal controllerid As Integer)
        pnlNewStarterList.Visible = False
        pnlEmpByTeam.Visible = False
        pnlEmpDetails.Visible = False

        If controllerid = NavMode.LoadPage Then
            formhelper.ClearForm(pnlSearch)
        End If

        If controllerid = NavMode.Search Then

            pnlEmpByTeam.Visible = True
        End If

        If controllerid = NavMode.EmpDetails Then
            pnlEmpDetails.Visible = True
        End If

        If controllerid = NavMode.NewStarter Then
            pnlEmpByTeam.Visible = True
            pnlSearch.Visible = False
        End If


        If controllerid = NavMode.LatestEmp Then
            pnlSearch.Visible = False
            pnlEmpDetails.Visible = True
            lnkReturn.Text = "Return to Whiteboard"
        End If
    End Sub

    Public Sub BindTeam()


        Dim bllTeam As New bllTeam
        drpTeams.DataTextField = "TEAMNAME"
        drpTeams.DataValueField = "TEAMID"
        drpTeams.DataSource = bllTeam.GetTeamsWithPermissions(0)
        drpTeams.DataBind()

        ' remove Contractors and IS and CS from list        
        Dim removeListItem As ListItem
        removeListItem = drpTeams.Items.FindByText("Contractors")
        drpTeams.Items.Remove(removeListItem)

        removeListItem = drpTeams.Items.FindByText("IS and CS")
        drpTeams.Items.Remove(removeListItem)
    End Sub


    Public Function FormatStartDate(ByVal strDate As Date) As String
        Dim dt As DateTime = strDate
        Return dt.ToString("dddd, dd MMMM yyyy")
    End Function
    Public Function GetImagePath(ByVal objImage As Object) As String
        If IsDBNull(objImage) Then
            Return "/BHAIntranet/Images/Defaults/emp.gif"
        Else
            Return "/EmployeeImage/" & Trim(objImage.ToString)
        End If

    End Function


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If drpTeams.SelectedValue = "" Then
            lblValidation.Text = "You must select a team<br/><br/>"
        Else
            lblValidation.Text = ""
            uiController(NavMode.Search)
            ViewState("TeamID") = CInt(drpTeams.SelectedValue)
            ViewState("Name") = ""
            BindEmpList()

        End If
    End Sub

    Public Sub BindEmpList()

        Dim bllEmp As New bllEmployees

        If ViewState("Name") <> "" Then

            gvwEmpList.DataSource = bllEmp.Search(ViewState("Name"))
        Else
            gvwEmpList.DataSource = bllEmp.GetEmployeesByTeamID(ViewState("TeamID"))
        End If

        gvwEmpList.DataBind()

    End Sub

    Private Sub BindNewStarters()
        Dim bllEmp As New bllEmployees
        gvwEmpList.DataSource = bllEmp.GetNewStarters()
        gvwEmpList.DataBind()
    End Sub

    Protected Sub gvwEmpList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwEmpList.PageIndexChanging
        gvwEmpList.PageIndex = e.NewPageIndex
        BindEmpList()
    End Sub

    Protected Sub gvwEmpList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwEmpList.RowCommand

        If e.CommandName = "SelectEmp" Then
            Dim gvw As GridView = CType(sender, GridView)
            Dim intEmpID As Integer = e.CommandArgument
            BindEmp(intEmpID)
            uiController(NavMode.EmpDetails)
        End If

    End Sub

    Public Sub BindEmp(ByVal EmpID As Integer)
        Dim bllEmp As New bllEmployees(EmpID)

        If Trim(bllEmp.Email) <> "N/A" Then
            lblWorkEmailText.Text = "<a href=""mailto:" & bllEmp.Email & """>" & bllEmp.Email & "</a>"
        Else
            lblWorkEmailText.Text = "N/A"
        End If

        If bllEmp.ImagePath <> "" Then
            imgEmp.ImageUrl = "/EmployeeImage/" & bllEmp.ImagePath
        Else
            imgEmp.ImageUrl = "/BHAIntranet/Images/Defaults/emp.gif"
        End If

        lblNameText.Text = bllEmp.FullName
        lblTitleText.Text = bllEmp.JobTitle
        lblTeamText.Text = bllEmp.TeamName
        Dim dt As DateTime = bllEmp.StartDate
        lblStartDateText.Text = dt.ToString("dddd, dd MMMM yyyy")

        lblWorkPhoneText.Text = bllEmp.WorkDD
        lblWorkMobileText.Text = bllEmp.WorkMobile

        lblOfficeLocationText.Text = bllEmp.OfficeLocation
        lblLineManagerText.Text = bllEmp.LineManager
        If Trim(bllEmp.RolePath) <> "" Then
            hlnkRoleProfile.Attributes.Add("href", "/Rolefile/" & bllEmp.RolePath)
            hlnkRoleProfile.Text = "View Role Profile"
            'lblRoleProfileText.Text = ""
        Else
            'hlnkRoleProfile.Visible = False
            hlnkRoleProfile.NavigateUrl = ""
            hlnkRoleProfile.Text = "There is no role profile available"
            'lblRoleProfileText.Text = "There is no role profile available"
        End If

        lblProfileText.Text = bllEmp.Profile

    End Sub

    Protected Sub lnkReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReturn.Click
        If ViewState("ViewEmpID") Then
            ViewState.Remove("ViewEmpID")
            Response.Redirect("~/BHAIntranet/CustomPages/MyWhiteboard.aspx")
        Else
            uiController(NavMode.Search)
        End If
    End Sub
End Class
