Imports System.IO
Imports System.Web.Mail
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.data

Partial Class BHAIntranet_CustomPages_Stationery
    Inherits basepage

    Private Enum NavMode
        LoadPage = 0
        ViewOrders
        ViewOrdersFiltered
        ViewTeamOrders
        ViewTeamOrdersFiltered
        NewOrder
        ViewOrderAndItems
        ViewTeamOrderAndItems
        ViewOrderItems
        NewOrderItems
        EditOrderItems
        EditTeamOrderItems
        ViewAllOrders
        ViewAllOrderAndItems
        ViewAllOrdersFiltered
        OrderItemsReport
        OrderItemsReportFiltered
        LastMode
    End Enum

    Private Enum Tabs
        MyOrders = 0
        TeamOrders
        AllOrders
        OrderReport
    End Enum

    Private Function GetCurrentTab() As Tabs
        Select Case tcMainTabs.ActiveTab.ID
            Case "tbOrders"
                Return Tabs.MyOrders

            Case "tbAllOrders"
                Return Tabs.AllOrders

            Case "tbTeamOrders"
                Return Tabs.TeamOrders

            Case "tbOrderReport"
                Return Tabs.OrderReport
        End Select
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Request.QueryString("ID") IsNot Nothing Then
            ASPSession("USERID") = Request.QueryString("ID")
        End If

        Dim master As BHAIntranet_templates_main = CType(Me.Master, BHAIntranet_templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Stationery.css")

        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)
        End If
    End Sub

    Protected Sub gvOrders_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOrders.PageIndexChanging
        gvOrders.PageIndex = e.NewPageIndex
        BindOrderGrid()
    End Sub

    Protected Sub gvAllOrders_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAllOrders.PageIndexChanging
        gvAllOrders.PageIndex = e.NewPageIndex
        BindAllOrderGrid()
    End Sub

    Private Sub uiController(ByVal iMode As NavMode)

        lblOrderValidation.Text = ""
        lblOrderItemValidation.Text = ""
        lblFilterValidator.Text = ""

        If iMode = NavMode.LastMode Then
            iMode = ViewState("LastUIMode")
        End If

        Select Case iMode
            Case NavMode.LoadPage
                CheckTabsAccess()
                mvOrders.SetActiveView(vwOrders)
                tcMainTabs.ActiveTab = tbOrders
                tcSearch.Visible = True
                tcOrderItems.Visible = False
                EnableFilterItems(False)
                CheckEnableBatchOrder()
                CheckLineManagerStatus()
                BindOrderGrid()

            Case NavMode.ViewOrderAndItems
                ViewState("LastUIMode") = iMode
                mvOrders.SetActiveView(vwEditOrder)
                formhelper.ClearForm(pnlOrderEdit)
                tcSearch.Visible = False
                tcOrderItems.Visible = True
                CheckEnableBatchOrder()
                btnUpdate.Visible = True
                btnDelete.Visible = True
                btnAddNewOrder.Visible = False
                lOrderRef.Visible = True
                txtOrderRef.Visible = True
                btnPrintPreview.Visible = True
                BindOrderDetails()
                CheckEnableEdit()

                OrderItemsDisplay()


            Case NavMode.ViewTeamOrderAndItems
                ViewState("LastUIMode") = iMode
                mvTeamOrders.SetActiveView(vwEditTeamOrder)
                tcSearch.Visible = False
                tcOrderItems.Visible = True
                formhelper.ClearForm(pnlViewTeamOrder)
                formhelper.LockForm(pnlViewTeamOrder, True)
                btnUpdate.Visible = True
                btnDelete.Visible = True
                btnAddNewOrder.Visible = False
                lOrderRef.Visible = True
                txtOrderRef.Visible = True
                BindTeamOrderDetails()
                CheckEnableEdit()

                OrderItemsDisplay()
                CheckEnableBatchOrder()

            Case NavMode.ViewAllOrderAndItems
                ViewState("LastUIMode") = iMode
                mvAllOrders.SetActiveView(vwEditAllOrder)
                tcOrderItems.Visible = True
                tcSearch.Visible = False
                formhelper.ClearForm(pnlViewAllOrder)
                formhelper.LockForm(pnlViewAllOrder, True)
                btnUpdate.Visible = True
                btnDelete.Visible = True
                btnAddNewOrder.Visible = False
                lOrderRef.Visible = True
                txtOrderRef.Visible = True
                lTotalCost.Visible = True
                lblTotalCostField.Visible = True
                BindAllOrderDetails()
                CheckEnableEdit()

                OrderItemsDisplay()
                CheckEnableBatchOrder()

            Case NavMode.NewOrder
                tcMainTabs.ActiveTab = tbOrders
                mvOrders.SetActiveView(vwEditOrder)
                mvOrderItems.ActiveViewIndex = -1
                tcSearch.Visible = False
                formhelper.ClearForm(pnlOrderEdit)
                formhelper.LockForm(pnlOrderEdit, False)
                lOrderRef.Visible = False
                txtOrderRef.Visible = False
                lTotalCost.Visible = True
                lblTotalCostField.Visible = True
                lblTotalCostField.Text = "�0.00"
                btnUpdate.Visible = False
                btnDelete.Visible = False
                btnAddNewOrder.Visible = True
                btnSubmitOrder.Visible = False
                btnCancel.Visible = True
                btnOk.Visible = False
                btnPrintPreview.Visible = False
                BindItemPriorityList()

            Case NavMode.ViewOrders                
                EnableFilterItems(False)
                tcOrderItems.Visible = False
                tcSearch.Visible = True
                ViewState("TeamOrderSelection") = False
                mvOrders.SetActiveView(vwOrders)
                mvOrderItems.ActiveViewIndex = -1
                BindOrderGrid()

            Case NavMode.ViewOrdersFiltered
                If ValidateFilter() Then
                    mvOrders.SetActiveView(vwOrders)
                    mvOrderItems.ActiveViewIndex = -1
                    EnableFilterItems(False)
                    tcOrderItems.Visible = False
                    tcSearch.Visible = True
                    BindOrderGrid()
                End If

            Case NavMode.ViewTeamOrders                
                tcSearch.Visible = True
                tcOrderItems.Visible = False
                EnableFilterItems(False)
                mvTeamOrders.SetActiveView(vwTeamOrders)
                mvOrderItems.ActiveViewIndex = -1
                BindMyTeamsOrderGrid()

            Case NavMode.ViewTeamOrdersFiltered
                If ValidateFilter() Then
                    tcSearch.Visible = True
                    tcOrderItems.Visible = False
                    EnableFilterItems(False)
                    mvTeamOrders.SetActiveView(vwTeamOrders)
                    BindMyTeamsOrderGrid()
                End If

            Case NavMode.ViewOrderItems
                ' need to ensure we remember we are viewing items from a team members order
                If GetCurrentTab() = Tabs.TeamOrders Or GetCurrentTab() = Tabs.AllOrders Then
                    ViewState("TeamOrderSelection") = True
                    ' don't allow adding of order items if order in team
                    lnkAddItem.Visible = False
                End If

                formhelper.ClearForm(pnlOrderItemEdit)

                mvOrderItems.SetActiveView(vwItemsGrid)
                BindOrderItemsGrid(ViewState("OrderID"))


            Case NavMode.NewOrderItems
                tcOrderItems.Visible = True
                formhelper.ClearForm(pnlOrderItemEdit)
                formhelper.LockForm(pnlOrderItemEdit, False)
                btnAddNewItem.Visible = True
                btnUpdateItem.Visible = False
                btnDeleteItem.Visible = False
                mvOrderItems.SetActiveView(vwEditItems)
                ddlOrderItemStatus.Enabled = False
                BindOrderItemDetails(False)

            Case NavMode.EditOrderItems
                formhelper.ClearForm(pnlOrderItemEdit)
                mvOrderItems.SetActiveView(vwEditItems)
                btnAddNewItem.Visible = False
                btnUpdateItem.Visible = True
                btnDeleteItem.Visible = True
                BindOrderItemDetails(True)

            Case NavMode.EditTeamOrderItems
                formhelper.ClearForm(pnlOrderItemEdit)
                mvOrderItems.SetActiveView(vwEditItems)

                btnAddNewItem.Visible = False
                btnUpdateItem.Visible = True
                btnDeleteItem.Visible = True
                BindOrderItemDetails(True, True)
                CheckEnableBatchOrder()

            Case NavMode.ViewAllOrders                
                tcOrderItems.Visible = False
                tcSearch.Visible = True
                EnableFilterItems(False)
                mvAllOrders.SetActiveView(vwAllOrders)
                mvOrderItems.ActiveViewIndex = -1
                BindAllOrderGrid()

            Case NavMode.ViewAllOrdersFiltered
                If ValidateFilter() Then
                    tcOrderItems.Visible = False
                    EnableFilterItems(False)
                    mvAllOrders.SetActiveView(vwAllOrders)
                    mvOrderItems.ActiveViewIndex = -1
                    BindAllOrderGrid()
                End If


            Case NavMode.OrderItemsReport
                If ValidateFilter() Then                    
                    mvOrders.SetActiveView(vwOrders)
                    mvOrderItems.ActiveViewIndex = -1
                    tcOrderItems.Visible = False
                    tcSearch.Visible = True

                    EnableFilterItems(True)
                    BindOrderItemsReport()
                End If

            Case NavMode.OrderItemsReportFiltered
                If ValidateFilter() Then
                    mvOrders.SetActiveView(vwOrders)
                    mvOrderItems.ActiveViewIndex = -1
                    tcOrderItems.Visible = False
                    tcSearch.Visible = True

                    EnableFilterItems(True)
                    BindOrderItemsReport()
                End If

        End Select

    End Sub

    Private Sub CheckEnableEdit()
        ' this checks whether an order can be submitted or not
        ' it has to be an order that has already been selected AND 
        ' has order items associated

        Dim bAllowEdit As Boolean = False
        Dim bAllowSubmit As Boolean = False

        If ViewState("OrderID") IsNot Nothing Then
            Dim iOrderID As Integer = ViewState("OrderID")

            Dim bllStationery As New bllStationery
            Dim dr As dalStationery.I_STATIONERY_ORDERRow
            dr = bllStationery.SelectById(iOrderID)

            ' now check whether the order is at initial saved status
            If dr.StatusId = bllStationery.OrderStatus.Saved Then
                bAllowEdit = True
            End If

            ' count how many items are under this order
            Dim bllStationeryItem As New bllStationeryItem
            Dim dt As New dalStationery.I_STATIONERY_ORDER_ITEMDataTable
            dt = bllStationeryItem.GetItemsByOrderID(iOrderID)

            If dt.Rows.Count > 0 And bAllowEdit Then
                ' this order has order items
                bAllowSubmit = True
            End If
        End If

        btnSubmitOrder.Visible = bAllowSubmit
        btnUpdate.Visible = bAllowEdit
        btnCancel.Visible = bAllowEdit
        btnDelete.Visible = bAllowEdit
        btnOk.Visible = Not bAllowEdit

        formhelper.LockForm(pnlOrderEdit, Not bAllowEdit)
    End Sub

    Private Sub BindOrderGrid()

        If ASPSession("USERID") IsNot Nothing Then
            Dim bllStationeryOrders As bllStationery = New bllStationery
            Dim iUserID As Integer = Convert.ToInt32(ASPSession("USERID"))


            Dim iStatus As Nullable(Of Integer)
            Dim iTeamId As Nullable(Of Integer)
            Dim iEmpForId As Nullable(Of Integer)
            If ddlFilterOrderStatus.SelectedValue <> "" Then
                iStatus = Convert.ToInt32(ddlFilterOrderStatus.SelectedValue)
            End If

            If ddlFilterTeam.SelectedValue <> "" Then
                iTeamId = Convert.ToInt32(ddlFilterTeam.SelectedValue)
            End If

            If ddlFilterEmployee.SelectedValue <> "" Then
                iEmpForId = Convert.ToInt32(ddlFilterEmployee.SelectedValue)
            End If

            gvOrders.DataSource = bllStationeryOrders.GetFilteredResultsByEmp(iUserID, txtFilterDateReqFrom.Text, txtFilterDateReqTo.Text, iStatus, iTeamId, iEmpForId, txtFilterLessThan.Text, txtFilterGreaterThan.Text)
        

            gvOrders.DataBind()
            gvOrders.SelectedIndex = -1
        End If

    End Sub

    Private Sub BindMyTeamsOrderGrid()

        If ASPSession("USERID") IsNot Nothing Then
            Dim iUserID As Integer = Convert.ToInt32(ASPSession("USERID"))

            Dim bllStationeryOrders As bllStationery = New bllStationery

            Dim iStatus As Nullable(Of Integer)
            Dim iTeamID As Nullable(Of Integer)
            Dim iEmpForId As Nullable(Of Integer)
            If ddlFilterOrderStatus.SelectedValue <> "" Then
                iStatus = Convert.ToInt32(ddlFilterOrderStatus.SelectedValue)
            End If

            If ddlFilterTeam.SelectedValue <> "" Then
                iTeamID = Convert.ToInt32(ddlFilterTeam.SelectedValue)
            End If

            If ddlFilterEmployee.SelectedValue <> "" Then
                iEmpForId = Convert.ToInt32(ddlFilterEmployee.SelectedValue)
            End If


            gvTeamOrders.DataSource = bllStationeryOrders.GetFilteredResultsByLineManager(iUserID, txtFilterDateReqFrom.Text, txtFilterDateReqTo.Text, iStatus, iTeamID, iEmpForId, txtFilterLessThan.Text, txtFilterGreaterThan.Text)
            'gvTeamOrders.DataSource = bllStationeryOrders.GetFilteredResultsByLineManager(iUserID, txtFilterDateReq.Text, txtFilterLessThan.Text, txtFilterGreaterThan.Text, iStatus)

            gvTeamOrders.DataBind()
            gvTeamOrders.SelectedIndex = -1
        End If

    End Sub

    Private Sub BindAllOrderGrid()
        If ASPSession("USERID") IsNot Nothing Then
            Dim iUserID As Integer = Convert.ToInt32(ASPSession("USERID"))

            Dim bllStationeryOrders As bllStationery = New bllStationery


            Dim iStatus As Nullable(Of Integer)
            Dim iTeamID As Nullable(Of Integer)
            Dim iEmpForId As Nullable(Of Integer)
            If ddlFilterOrderStatus.SelectedValue <> "" Then
                iStatus = Convert.ToInt32(ddlFilterOrderStatus.SelectedValue)
            End If

            If ddlFilterTeam.SelectedValue <> "" Then
                iTeamID = Convert.ToInt32(ddlFilterTeam.SelectedValue)
            End If

            If ddlFilterEmployee.SelectedValue <> "" Then
                iEmpForId = Convert.ToInt32(ddlFilterEmployee.SelectedValue)
            End If

            gvAllOrders.DataSource = bllStationeryOrders.GetFilteredResultsAll(txtFilterDateReqFrom.Text, txtFilterDateReqTo.Text, iStatus, iTeamID, iEmpForId, txtFilterLessThan.Text, txtFilterGreaterThan.Text)
        
            gvAllOrders.DataBind()
            gvAllOrders.SelectedIndex = -1
        End If
    End Sub

    Private Sub BindOrderItemsGrid(ByVal iOrderID As Integer)
        ' if items belong to an order which is set saved we can add other items
        Dim bllStationeryOrder As New bllStationery(iOrderID)
        If bllStationeryOrder.StatusID = bllStationery.OrderStatus.Saved Then
            lnkAddItem.Visible = True
        Else
            lnkAddItem.Visible = False
        End If

        gvOrderItems.DataSource = New bllStationeryItem().GetItemsByOrderID(iOrderID)
        gvOrderItems.DataBind()
    End Sub

    Private Sub BindOrderDetails()
        Dim iSelected As Integer = ViewState("OrderID")
        Dim bllStationeryOrder As New bllStationery(iSelected)

        If bllStationeryOrder IsNot Nothing Then
            Try
                ' bind the teams and select values first
                ddlForTeam.Items.Clear()
                ddlForTeam.DataBind()

                Dim bllEmp As New bllEmployees(bllStationeryOrder.RequestedForEmpID)
                ddlForTeam.SelectedValue = bllEmp.TeamID


                BindForEmpList(ddlForTeam, ddlForEmp)
                ddlForEmp.SelectedValue = bllStationeryOrder.RequestedForEmpID

                ' fetch details and assign to form
                txtOrderRef.Text = bllStationeryOrder.OrderID
                txtDesc.Text = bllStationeryOrder.Description
                txtDevLoc.Text = bllStationeryOrder.DeliveryLocation
                lblTotalCostField.Text = String.Format("{0:c}", bllStationeryOrder.TotalCost)

            Catch ex As Exception
                'log error here
            End Try
        End If
    End Sub

    Private Sub BindTeamOrderDetails()
        Dim iSelected As Integer = ViewState("OrderID")
        Dim bllStationeryOrder As New bllStationery(iSelected)

        If bllStationeryOrder IsNot Nothing Then
            Try
                ' bind the teams and select values first
                ddlTeamOrderForTeam.Items.Clear()
                ddlTeamOrderForTeam.DataBind()


                Dim bllEmp As New bllEmployees(bllStationeryOrder.RequestedForEmpID)
                ddlTeamOrderForTeam.SelectedValue = bllEmp.TeamID
                BindForEmpList(ddlTeamOrderForTeam, ddlTeamOrderForEmp)
                ddlTeamOrderForEmp.SelectedValue = bllStationeryOrder.RequestedForEmpID

                ' fetch details and assign to form
                txtTeamOrderRef.Text = bllStationeryOrder.OrderID
                txtTeamOrderDesc.Text = bllStationeryOrder.Description
                txtTeamOrderDevLoc.Text = bllStationeryOrder.DeliveryLocation

                lblTeamTotalCost.Text = String.Format("{0:c}", bllStationeryOrder.TotalCost)
            Catch ex As Exception
                'log error here
            End Try
        End If
    End Sub

    Private Sub BindAllOrderDetails()
        Dim iSelected As Integer = ViewState("OrderID")
        Dim bllStationeryOrder As New bllStationery(iSelected)

        If bllStationeryOrder IsNot Nothing Then
            Try
                ' bind the teams and select values first
                ddlAllOrderForTeam.Items.Clear()
                ddlAllOrderForTeam.DataBind()

                Dim bllEmp As New bllEmployees(bllStationeryOrder.RequestedForEmpID)
                ddlAllOrderForTeam.SelectedValue = bllEmp.TeamID
                BindForEmpList(ddlAllOrderForTeam, ddlAllOrderForEmp)
                ddlAllOrderForEmp.SelectedValue = bllStationeryOrder.RequestedForEmpID

                ' fetch details and assign to form
                txtAllOrderRef.Text = bllStationeryOrder.OrderID
                txtAllOrderDesc.Text = bllStationeryOrder.Description
                txtAllOrderDevLoc.Text = bllStationeryOrder.DeliveryLocation

                lblAllTotalCost.Text = String.Format("{0:c}", bllStationeryOrder.TotalCost)

            Catch ex As Exception
                'log error here
            End Try
        End If
    End Sub

    Private Sub BindOrderItemDetails(ByVal bEditMode As Boolean, Optional ByVal bTeamEdit As Boolean = False)
        Dim taPriorities As New dalStationeryTableAdapters.I_STATIONERY_ORDER_PRIORITYTableAdapter

        ddlOrderItemStatus.Items.Clear()
        ddlItemPriority.Items.Clear()
        ddlItemPriority.DataSource = taPriorities.GetData()
        ddlItemPriority.DataValueField = "StationeryOrderPriorityId"
        ddlItemPriority.DataTextField = "Priority"
        ddlItemPriority.DataBind()

        Dim bllStationeryOrderItem As New bllStationeryItem(ViewState("OrderItemID"))

        If bEditMode Then
            With bllStationeryOrderItem
                txtColour.Text = .Colour
                txtCost.Text = String.Format("{0:c}", .Cost)
                txtItemDesc.Text = .Description
                txtItemNo.Text = .ItemNo.ToString
                txtPageNo.Text = .PageNo.ToString
                txtSize.Text = .Size
                txtQuantity.Text = .Quantity.ToString
                ddlItemPriority.SelectedValue = .PriorityId


                ' check if we can update/delete an item depending on its saved status
                Dim bAllowEdit As Boolean
                If .StatusId = 1 Then
                    formhelper.LockForm(pnlOrderItemEdit, False)
                    bAllowEdit = True
                Else
                    If bllStationery.HasAccessToAllOrders(ASPSession("USERID")) Then
                        ' allow access to edit items
                        formhelper.LockForm(pnlOrderItemEdit, False)
                    Else
                        formhelper.LockForm(pnlOrderItemEdit, True)
                    End If

                End If

                ' allow status to be changed if team manager
                ddlOrderItemStatus.Enabled = bTeamEdit

                ' only allow deleting of order items when whole order has not yet been requested
                Dim iOrderId As Integer = Convert.ToInt32(ViewState("OrderID"))
                Dim bllOrder As New bllStationery(iOrderId)
                If bllOrder.StatusID = bllStationery.OrderStatus.Saved Then
                    btnDeleteItem.Visible = True
                Else
                    btnDeleteItem.Visible = False
                End If

                If ViewState("TeamOrderSelection") = False Then
                    ' skip disabling of buttons if team priveledges                    
                    btnUpdateItem.Visible = bAllowEdit
                    btnCanceltem.Visible = bAllowEdit
                    btnOkItem.Visible = Not bAllowEdit
                    BindOrderItemStatus(.StatusId)

                Else
                    ' build the status listbox depending on the current value of the status - this is to 
                    ' prevent the user selecting an incorrect status which would cause the order item to effectively go backwards

                    Select Case bllStationeryOrderItem.StatusId
                        Case bllStationeryItem.OrderItemStatus.Requested
                            ddlOrderItemStatus.Items.Add(New ListItem("Requested", bllStationeryItem.OrderItemStatus.Requested))
                            ddlOrderItemStatus.Items.Add(New ListItem("Ordered", bllStationeryItem.OrderItemStatus.Ordered))
                            ddlOrderItemStatus.Items.Add(New ListItem("Rejected", bllStationeryItem.OrderItemStatus.Rejected))

                        Case bllStationeryItem.OrderItemStatus.Ordered
                            ddlOrderItemStatus.Items.Add(New ListItem("Ordered", bllStationeryItem.OrderItemStatus.Ordered))
                            ddlOrderItemStatus.Items.Add(New ListItem("Received", bllStationeryItem.OrderItemStatus.Received))

                        Case bllStationeryItem.OrderItemStatus.Rejected
                            ddlOrderItemStatus.Items.Add(New ListItem("Rejected", bllStationeryItem.OrderItemStatus.Rejected))

                        Case bllStationeryItem.OrderItemStatus.Received
                            ddlOrderItemStatus.Items.Add(New ListItem("Received", bllStationeryItem.OrderItemStatus.Received))
                    End Select
                End If


            End With
        Else
            txtQuantity.Text = "1"
            btnOkItem.Visible = False
            btnCanceltem.Visible = True
            BindOrderItemStatus(bllStationeryItem.OrderItemStatus.Saved)
        End If

    End Sub

    Private Sub BindOrderItemStatus(ByVal iStatus As Integer)
        Dim taStatus As New dalStationeryTableAdapters.I_STATIONERY_ORDERITEM_STATUSTableAdapter
        ddlOrderItemStatus.DataSource = taStatus.GetData()
        ddlOrderItemStatus.DataValueField = "OrderStatusId"
        ddlOrderItemStatus.DataTextField = "OrderStatus"
        ddlOrderItemStatus.DataBind()
        ddlOrderItemStatus.SelectedValue = iStatus
    End Sub

    Private Sub BindItemPriorityList(Optional ByVal iPriorityID As Integer = -1)
        Dim dalPriorities As New dalStationeryTableAdapters.I_STATIONERY_ORDER_PRIORITYTableAdapter

        If iPriorityID > 0 Then
            ddlItemPriority.DataSource = dalPriorities.GetPriorityById(iPriorityID)
        Else
            ddlItemPriority.DataSource = dalPriorities.GetData()
        End If
        ddlItemPriority.Items.Clear()

        ddlItemPriority.DataValueField = "StationeryOrderPriorityId"
        ddlItemPriority.DataTextField = "Priority"
        ddlItemPriority.DataBind()

    End Sub

    Private Sub BindForEmpList(ByVal ddlTeams As DropDownList, ByRef ddlEmps As DropDownList)
        Dim bllForEmp As New bllEmployees

        If ddlTeams.SelectedValue > 0 Then
            ddlEmps.DataSource = bllForEmp.GetEmployeesByTeamID(ddlTeams.SelectedValue)
            ddlEmps.DataValueField = "EMPLOYEEID"
            ddlEmps.DataTextField = "FULLNAME"
            ddlEmps.DataBind()
        End If
    End Sub

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        uiController(NavMode.NewOrder)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        uiController(NavMode.ViewOrders)
    End Sub

    Protected Sub ddlForTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlForTeam.SelectedIndexChanged
        Dim ddl As DropDownList = CType(sender, DropDownList)
        BindForEmpList(ddlForTeam, ddlForEmp)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim bllStationeryOrder As New bllStationery
        Dim iByEmpID As Integer = ASPSession("USERID")
        Dim bllEmp As New bllEmployees(iByEmpID)
        Dim iByTeamID As Integer = bllEmp.TeamID
        Dim errors As New ErrorCollection

        Try
            If String.IsNullOrEmpty(txtDevLoc.Text) Then
                errors.Add("Please enter a delivery location for this stationery order.")
            End If

            If String.IsNullOrEmpty(txtDesc.Text) Then
                errors.Add("Please enter a title for this stationery order.")
            End If

            If errors.Count > 0 Then
                Dim strError As String = valhelper.Display(errors)
                Throw New ArgumentException(strError, "")
            End If

            With bllStationeryOrder

                .RequestedByEmpID = iByEmpID
                .RequestedForEmpID = ddlForEmp.SelectedValue

                .RequestedDate = Now.Date
                .DeliveryLocation = txtDevLoc.Text
                .Description = txtDesc.Text
            End With
            bllStationeryOrder.Update(Convert.ToInt32(ViewState("OrderID")))
            uiController(NavMode.ViewOrders)

        Catch ex As Exception
            lblOrderValidation.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnAddNewOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewOrder.Click
        If ASPSession("USERID") IsNot Nothing Then
            Dim errors As New ErrorCollection

            Try

                If String.IsNullOrEmpty(txtDesc.Text) Then
                    errors.Add("Please enter a title for this stationery order.")
                End If

                If String.IsNullOrEmpty(ddlForEmp.SelectedValue) Then
                    errors.Add("Please select an employee for the stationery order.")
                End If

                If String.IsNullOrEmpty(txtDevLoc.Text) Then
                    errors.Add("Please enter a delivery location for this stationery order.")
                End If

                If errors.Count > 0 Then
                    Dim strError As String = valhelper.Display(errors)
                    Throw New ArgumentException(strError, "")
                End If

                Dim iByEmpID As Integer = CType(ASPSession("USERID"), Integer)
                Dim bllEmp As New bllEmployees(iByEmpID)
                Dim iByTeamID As Integer = bllEmp.TeamID

                Dim bllNewOrder As New bllStationery
                With bllNewOrder
                    .RequestedByEmpID = iByEmpID

                    .RequestedForEmpID = ddlForEmp.SelectedValue

                    .RequestedDate = Now
                    .DeliveryLocation = txtDevLoc.Text
                    .Description = txtDesc.Text
                    .StatusID = 1 ' Saved
                End With
                bllNewOrder.Create()
                BindOrderGrid()

                ' select last order added to list
                ViewState("OrderID") = gvOrders.DataKeys(0).Value

                uiController(NavMode.ViewOrderAndItems)

            Catch ex As Exception
                lblOrderValidation.Text = ex.Message
            End Try
        End If

    End Sub


    Protected Sub gvOrders_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOrders.RowCommand
        If e.CommandName = "Select" Then
            Dim iRowIdx As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("OrderID") = gvOrders.DataKeys(iRowIdx).Value
            gvOrders.SelectedIndex = iRowIdx
            uiController(NavMode.ViewOrderAndItems)
            'uiController(NavMode.ViewOrderItems)
        End If
    End Sub

    Protected Sub lnkAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddItem.Click
        uiController(NavMode.NewOrderItems)
    End Sub

    Protected Sub btnAddNewItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewItem.Click
        Dim errors As New ErrorCollection

        Try
            If valhelper.validateIsEmpty(txtPageNo.Text) Then
                errors.Add("Please enter a page no. value for this order item.")
            End If

            If valhelper.validateIsEmpty(txtItemNo.Text) Then
                errors.Add("Please enter a catalogue no. for this order item.")
            End If

            If String.IsNullOrEmpty(txtItemDesc.Text) Then
                errors.Add("Please enter a description for this order item.")
            End If

            If Not valhelper.validateValue(txtQuantity.Text) Then
                errors.Add("Please enter an item quantity for this order item.")
            End If

            If Not valhelper.validateCurrency(txtCost.Text) Then
                errors.Add("Please enter a valid item cost for this order item (between 1 and 999,999).")
            End If

            If errors.Count > 0 Then
                Dim strError As String = valhelper.Display(errors)
                Throw New ArgumentException(strError, "")
            End If

            Dim bllNewOrderItem As New bllStationeryItem
            With bllNewOrderItem

                .ParentOrderItemID = Convert.ToInt32(ViewState("OrderID"))
                .PageNo = txtPageNo.Text
                .ItemNo = txtItemNo.Text
                .Description = txtItemDesc.Text
                .Quantity = Val(txtQuantity.Text)
                .Colour = txtColour.Text
                .Size = txtSize.Text
                .Cost = Convert.ToDecimal(txtCost.Text.Replace("�", ""))
                .PriorityId = ddlItemPriority.SelectedValue
                .StatusId = ddlOrderItemStatus.SelectedValue
            End With

            bllNewOrderItem.Create()
            uiController(NavMode.ViewOrderAndItems)

        Catch ex As Exception
            lblOrderItemValidation.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnCanceltem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCanceltem.Click
        uiController(NavMode.LastMode)
    End Sub

    Protected Sub btnUpdateItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateItem.Click
        Dim errors As New ErrorCollection

        Try
            If valhelper.validateIsEmpty(txtPageNo.Text) Then
                errors.Add("Please enter a page no. for this stationery order.")
            End If

            If valhelper.validateIsEmpty(txtItemNo.Text) Then
                errors.Add("Please enter a catalogue no. for this order item.")
            End If

            If String.IsNullOrEmpty(txtItemDesc.Text) Then
                errors.Add("Please enter a description for this order item.")
            End If

            If Not valhelper.validateValue(txtQuantity.Text) Then
                errors.Add("Please enter an item quantity for this stationery order.")
            End If

            If Not valhelper.validateCurrency(txtCost.Text) Then
                errors.Add("Please enter a valid item cost for this stationery order (between 1 and 999,999)")
            End If

            If errors.Count > 0 Then
                Dim strError As String = valhelper.Display(errors)
                Throw New ArgumentException(strError, "")
            End If

            Dim bllNewOrderItem As New bllStationeryItem
            With bllNewOrderItem
                .OrderItemID = Convert.ToInt32(ViewState("OrderItemID"))
                .ParentOrderItemID = Convert.ToInt32(ViewState("OrderID"))
                .PageNo = txtPageNo.Text
                .ItemNo = txtItemNo.Text
                .Description = txtItemDesc.Text
                .Quantity = Val(txtQuantity.Text)
                .Colour = txtColour.Text
                .Size = txtSize.Text
                .Cost = Convert.ToDecimal(txtCost.Text.Replace("�", ""))
                .StatusId = ddlOrderItemStatus.SelectedValue
                .PriorityId = ddlItemPriority.SelectedValue
            End With

            bllNewOrderItem.Update()
            uiController(NavMode.LastMode)

        Catch ex As Exception
            lblOrderItemValidation.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvOrderItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOrderItems.PageIndexChanging
        gvOrderItems.PageIndex = e.NewPageIndex
        BindOrderItemsGrid(ViewState("OrderID"))
        CheckEnableBatchOrder()
    End Sub

    Protected Sub gvOrderItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOrderItems.RowDataBound
        Dim lbl As Label = e.Row.FindControl("lblOrderStatus")

        If lbl IsNot Nothing Then
            Dim chk As CheckBox = e.Row.FindControl("chkOrder")
            If chk IsNot Nothing Then
                If lbl.Text = "Requested" Then
                    ' status is set to requested, so show checkbox for selecting order status
                    chk.Visible = True
                Else
                    chk.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub gvOrderItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvOrderItems.SelectedIndexChanged
        Dim gv As GridView = CType(sender, GridView)
        ViewState("OrderItemID") = gv.SelectedDataKey.Value

        If ViewState("TeamOrderSelection") = True Then
            uiController(NavMode.EditTeamOrderItems)
        Else
            uiController(NavMode.EditOrderItems)
        End If

    End Sub

    Protected Sub tcMainTabs_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcMainTabs.ActiveTabChanged
        ClearFilterFields()
        Select Case tcMainTabs.ActiveTabIndex
            Case Tabs.MyOrders
                uiController(NavMode.ViewOrders)

            Case Tabs.TeamOrders
                uiController(NavMode.ViewTeamOrders)

            Case Tabs.AllOrders
                uiController(NavMode.ViewAllOrders)

            Case Tabs.OrderReport
                uiController(NavMode.OrderItemsReport)
        End Select
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim bllStationeryOrder As New bllStationery
        bllStationeryOrder.Delete(ViewState("OrderID"))
        uiController(NavMode.ViewOrders)
    End Sub

    Protected Sub btnSubmitOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitOrder.Click

        If ViewState("OrderID") IsNot Nothing Then
            Dim bllStationeryItem As New bllStationeryItem()
            Dim bllStationery As New bllStationery(ViewState("OrderID"))

            ' update status of both order items and order
            Try
                bllStationeryItem.SetItemsRequested(ViewState("OrderID"))
                bllStationery.SetOrderRequested(ViewState("OrderID"))

                uiController(NavMode.ViewOrders)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        uiController(NavMode.ViewOrders)
    End Sub

    Protected Sub btnOkItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkItem.Click
        uiController(NavMode.LastMode)
    End Sub

    Protected Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteItem.Click
        If ViewState("OrderItemID") IsNot Nothing Then
            Dim iOrderItemId As Integer = ViewState("OrderItemID")
            Dim bllStationeryOrder As New bllStationery

            bllStationeryOrder.DeleteItem(iOrderItemId)
            uiController(NavMode.LastMode)
        End If
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        CallUIFromTab()
    End Sub

    Protected Sub btnClearFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearFilter.Click
        ClearFilterFields()
        CallUIFromTab()
    End Sub

    Private Sub CallUIFromTab()
        If GetCurrentTab() = Tabs.MyOrders Then
            uiController(NavMode.ViewOrdersFiltered)
        ElseIf GetCurrentTab() = Tabs.AllOrders Then
            uiController(NavMode.ViewAllOrdersFiltered)
        ElseIf GetCurrentTab() = Tabs.TeamOrders Then
            uiController(NavMode.ViewTeamOrdersFiltered)
        Else
            uiController(NavMode.OrderItemsReportFiltered)
        End If
    End Sub

    Private Sub EnableFilterItems(ByVal bEnabled As Boolean)
        ddlFilterItemStatus.Enabled = bEnabled
        ddlFilterOrderStatus.Enabled = Not bEnabled
    End Sub

    Private Sub ClearFilterFields()
        txtFilterDateReqFrom.Text = ""
        txtFilterDateReqTo.Text = ""
        txtFilterGreaterThan.Text = ""
        txtFilterLessThan.Text = ""
        ddlFilterOrderStatus.SelectedIndex = 0
        ddlFilterItemStatus.SelectedIndex = 0
        ddlFilterTeam.SelectedIndex = 0

        ddlFilterEmployee.Items.Clear()
        ddlFilterEmployee.Items.Add(New ListItem(""))
    End Sub

    Protected Sub gvTeamOrders_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTeamOrders.RowCommand
        If e.CommandName = "Select" Then
            Dim iRowIdx As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("OrderID") = gvTeamOrders.DataKeys(iRowIdx).Value
            gvTeamOrders.SelectedIndex = iRowIdx
            uiController(NavMode.ViewTeamOrderAndItems)
        End If
    End Sub

    Protected Sub btnTeamOrderOk_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        uiController(NavMode.ViewTeamOrders)
    End Sub


    Private Sub CheckLineManagerStatus()
        If Not IsLineManager() Then
            ' display no emps underneath me!
            tcMainTabs.Tabs(Tabs.TeamOrders).Enabled = False        
        End If
    End Sub

    Private Function IsLineManager() As Boolean

        If ASPSession("USERID") IsNot Nothing Then
            ' first find out if this user appears as a Line manager in the database
            ' and has any employees underneath
            Dim iUserID As Integer = Convert.ToInt32(ASPSession("USERID"))
            Dim iNumEmpsUnderMe As Integer

            Dim bllEmp As New bllEmployees
            iNumEmpsUnderMe = bllEmp.GetNumEmployeesBelowMe(iUserID)
            If iNumEmpsUnderMe > 0 Then
                Return True
            End If
        End If

        Return False

    End Function

    Private Function ValidateFilter() As Boolean
        Dim errors As New ErrorCollection

        Try
            ' can ignore filter if empty
            If Not String.IsNullOrEmpty(txtFilterDateReqFrom.Text) And Not String.IsNullOrEmpty(txtFilterDateReqTo.Text) Then
                If DateTime.Compare(txtFilterDateReqFrom.Text, txtFilterDateReqTo.Text) > 0 Then
                    errors.Add("Please enter a 'from' date that is before the 'to' date.")
                End If

            End If

            If Not String.IsNullOrEmpty(txtFilterDateReqFrom.Text) Then
                If Not valhelper.validateDate(txtFilterDateReqFrom.Text) Then
                    errors.Add("Please enter valid 'from' date.")
                End If
            End If


            If Not String.IsNullOrEmpty(txtFilterDateReqTo.Text) Then
                If Not valhelper.validateDate(txtFilterDateReqTo.Text) Then
                    errors.Add("Please enter valid 'to' date.")
                End If
            End If

            If Not String.IsNullOrEmpty(txtFilterLessThan.Text) Then

                If Not valhelper.validateCurrency(txtFilterLessThan.Text) Then
                    errors.Add("Please enter a valid currency value for the 'less than' filter")
                End If
            End If

            If Not String.IsNullOrEmpty(txtFilterGreaterThan.Text) Then
                If Not valhelper.validateCurrency(txtFilterGreaterThan.Text) Then
                    errors.Add("Please enter a valid currency value for the 'greater than' filter")
                End If
            End If
            If errors.Count > 0 Then
                Throw New ArgumentException
            End If

            Return True
        Catch ex As Exception
            lblFilterValidator.Text = valhelper.Display(errors)
            Return False
        End Try
    End Function

    Protected Sub gvTeamOrders_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTeamOrders.PageIndexChanging
        gvTeamOrders.PageIndex = e.NewPageIndex
        BindMyTeamsOrderGrid()
    End Sub

    Protected Sub gvOrders_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOrders.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton = e.Row.FindControl("lnkOrderTitle")
            If lnk IsNot Nothing Then
                lnk.CommandArgument = e.Row.RowIndex.ToString
            End If
        End If
    End Sub

    Protected Sub gvAllOrders_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAllOrders.RowCommand
        If e.CommandName = "Select" Then
            Dim iRowIdx As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("OrderID") = gvAllOrders.DataKeys(iRowIdx).Value
            gvAllOrders.SelectedIndex = iRowIdx
            uiController(NavMode.ViewAllOrderAndItems)
        End If
    End Sub

    Protected Sub gvAllOrders_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAllOrders.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton = e.Row.FindControl("lnkOrderTitle")
            If lnk IsNot Nothing Then
                lnk.CommandArgument = e.Row.RowIndex.ToString
            End If
        End If
    End Sub

    Private Sub OrderItemsDisplay()
        ' need to ensure we remember we are viewing items from a team members order
        If GetCurrentTab() = Tabs.TeamOrders Or GetCurrentTab() = Tabs.AllOrders Then
            ViewState("TeamOrderSelection") = True

            ' don't allow adding of order items if order in team
            lnkAddItem.Visible = False
        End If

        formhelper.ClearForm(pnlOrderItemEdit)

        mvOrderItems.SetActiveView(vwItemsGrid)
        BindOrderItemsGrid(ViewState("OrderID"))
    End Sub


    Protected Sub btnPrintPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintAllOrder.Click, btnPrintTeamOrder.Click, btnPrintPreview.Click
        'JavaScript function embedded within ASPX page        
        Dim strJSFunc As String = String.Format("OpenPrintPage({0});", ViewState("OrderID"))
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "UserSecurity", strJSFunc, True)
    End Sub


    Protected Sub gvTeamOrders_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTeamOrders.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton = e.Row.FindControl("lnkOrderTitle")
            If lnk IsNot Nothing Then
                lnk.CommandArgument = e.Row.RowIndex.ToString
            End If
        End If
    End Sub

    Protected Sub btnAllOrderOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllOrderOk.Click
        uiController(NavMode.ViewAllOrders)
    End Sub

    Private Sub CheckTabsAccess()
        If ASPSession("USERID") <> "" Then
            If Not bllStationery.HasAccessToAllOrders(ASPSession("USERID")) Then
                tbAllOrders.Enabled = False
                tbOrderReport.Enabled = False
            Else
                tbAllOrders.Enabled = True
                tbOrderReport.Enabled = True
            End If
        Else
            Response.Redirect("/BHAIntranet/login.aspx?status=signout")
        End If
    End Sub

    Protected Sub lnkOrderChecked_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BatchOrderItems()
    End Sub

    Private Sub BatchOrderItems()
        ' here we iterate through the gridview rows, if one is checked 'order'
        ' pull the order item out of the adapter and then import it into the
        ' new adapter. Items that are at 'Requested' status can only have an order
        ' checkbox so don't need to worry about current orderitem status of other items.

        Try
            Dim iOrderItemId As Integer
            Dim dtOrderItems As New dalStationery.I_STATIONERY_ORDER_ITEMDataTable

            For Each gvr As GridViewRow In gvOrderItems.Rows
                Dim chk As CheckBox = gvr.FindControl("chkOrder")

                If chk.Checked Then
                    iOrderItemId = gvOrderItems.DataKeys(gvr.RowIndex).Value

                    Dim dt As New dalStationery.I_STATIONERY_ORDER_ITEMDataTable
                    Dim da As New dalStationeryTableAdapters.I_STATIONERY_ORDER_ITEMTableAdapter
                    dt = da.GetItemById(iOrderItemId)

                    If dt.Rows.Count > 0 Then
                        Dim rwOrderItem As dalStationery.I_STATIONERY_ORDER_ITEMRow
                        rwOrderItem = dt(0)

                        ' set to ordered                    
                        rwOrderItem.StatusId = bllStationeryItem.OrderItemStatus.Ordered

                        ' import row
                        dtOrderItems.ImportRow(rwOrderItem)
                    End If
                End If
            Next

            ' pass the modified rows
            Dim bllOrderItem As New bllStationeryItem
            Dim bllOrder As New bllStationery(ViewState("OrderID"))
            If bllOrderItem.UpdateOrderItemsWithTransaction(dtOrderItems) > 0 Then
                ' transaction ok, now set the status of the overall order to 'Ordered'
                bllOrder.SetOrderInProgress(ViewState("OrderID"))
            End If

            uiController(NavMode.LastMode)

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkOrderAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim thisChk As CheckBox = CType(sender, CheckBox)
        Dim chkOrder As CheckBox

        ' top checkbox of column
        For Each rw As GridViewRow In gvOrderItems.Rows
            chkOrder = rw.FindControl("chkOrder")
            If chkOrder IsNot Nothing Then
                If chkOrder.Visible = True Then
                    ' only copy the selection into the row if
                    ' the checkbox is visible (i.e. it has correct status)                    
                    chkOrder.Checked = thisChk.Checked
                Else
                    chkOrder.Checked = False
                End If
            End If
        Next
    End Sub

    Private Sub CheckEnableBatchOrder()

        Dim bEnable As Boolean
        bEnable = ViewState("TeamOrderSelection")

        If bEnable Then
            ' check if there are any order checkboxes
            ' if so then show the column and the Batch order link button
            Dim chk As CheckBox
            Dim bFound As Boolean
            For Each gvr As GridViewRow In gvOrderItems.Rows
                chk = gvr.FindControl("chkOrder")
                If chk IsNot Nothing Then
                    If chk.Visible = True Then
                        bFound = True
                        Exit For
                    End If
                End If
            Next

            bEnable = bFound
        End If

        lnkOrderChecked.Visible = bEnable
        ' hide/show the batch order column
        gvOrderItems.Columns(8).Visible = bEnable

    End Sub


    Protected Sub ddlFilterTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)

        ddlFilterEmployee.Items.Clear()
        ddlFilterEmployee.Items.Add(New ListItem(""))
        If Not String.IsNullOrEmpty(ddl.SelectedValue) Then
            Dim iTeamId As Integer = ddl.SelectedValue

            Dim bllEmp As New bllEmployees

            ddlFilterEmployee.DataValueField = "EMPLOYEEID"
            ddlFilterEmployee.DataTextField = "FULLNAME"

            ddlFilterEmployee.DataSource = bllEmp.GetEmployeesByTeamID(iTeamId)
            ddlFilterEmployee.DataBind()

            ' refresh report from filter
            CallUIFromTab()
        End If
    End Sub

    Private Sub BindOrderItemsReport()

        Dim dtDateReqTo As Nullable(Of DateTime)
        Dim dtDateReqFrom As Nullable(Of DateTime)
        Dim iOrderStatus As Nullable(Of Integer)
        Dim iTeamId As Nullable(Of Integer)
        Dim iEmpId As Nullable(Of Integer)
        Dim dcLessThan As Nullable(Of Decimal)
        Dim dcGreaterThan As Nullable(Of Decimal)

        If Not String.IsNullOrEmpty(ddlFilterItemStatus.SelectedValue) Then
            iOrderStatus = ddlFilterItemStatus.SelectedValue
        End If

        If Not String.IsNullOrEmpty(ddlFilterTeam.SelectedValue) Then
            iTeamId = ddlFilterTeam.SelectedValue
        End If

        If Not String.IsNullOrEmpty(ddlFilterEmployee.SelectedValue) Then
            iEmpId = ddlFilterEmployee.SelectedValue
        End If

        If Not String.IsNullOrEmpty(txtFilterDateReqFrom.Text) Then
            dtDateReqFrom = valhelper.convertToDate(txtFilterDateReqFrom.Text)
        End If

        If Not String.IsNullOrEmpty(txtFilterDateReqTo.Text) Then
            dtDateReqTo = valhelper.convertToDate(txtFilterDateReqTo.Text)
        End If

        If Not String.IsNullOrEmpty(txtFilterLessThan.Text) Then
            dcLessThan = Convert.ToDecimal(txtFilterLessThan.Text.Replace("�", ""))
        End If

        If Not String.IsNullOrEmpty(txtFilterGreaterThan.Text) Then
            dcGreaterThan = Convert.ToDecimal(txtFilterGreaterThan.Text.Replace("�", ""))
        End If

        Dim bllStationeryItem As New bllStationeryItem
        Dim dt As New DataTable
        dt = bllStationeryItem.GetFilteredReport(dtDateReqTo, dtDateReqFrom, iOrderStatus, iTeamId, iEmpId, dcLessThan, dcGreaterThan)

        Try
            gvOrderItemsReport.DataSource = dt
            gvOrderItemsReport.DataBind()

            ' cache the data for export if needed
            Cache.Insert("FilteredReport", dt)
        Catch ex As Exception

        End Try

    End Sub

    Private Function CalcTotals(ByVal bIncRejected As Boolean) As Decimal
        Dim dtDateReqTo As Nullable(Of DateTime)
        Dim dtDateReqFrom As Nullable(Of DateTime)
        Dim iOrderStatus As Nullable(Of Integer)
        Dim iTeamId As Nullable(Of Integer)
        Dim iEmpId As Nullable(Of Integer)
        Dim dcLessThan As Nullable(Of Decimal)
        Dim dcGreaterThan As Nullable(Of Decimal)

        If Not String.IsNullOrEmpty(ddlFilterItemStatus.SelectedValue) Then
            iOrderStatus = ddlFilterItemStatus.SelectedValue
        End If

        If Not String.IsNullOrEmpty(ddlFilterTeam.SelectedValue) Then
            iTeamId = ddlFilterTeam.SelectedValue
        End If

        If Not String.IsNullOrEmpty(ddlFilterEmployee.SelectedValue) Then
            iEmpId = ddlFilterEmployee.SelectedValue
        End If

        If Not String.IsNullOrEmpty(txtFilterDateReqFrom.Text) Then
            dtDateReqFrom = valhelper.convertToDate(txtFilterDateReqFrom.Text)
        End If

        If Not String.IsNullOrEmpty(txtFilterDateReqTo.Text) Then
            dtDateReqTo = valhelper.convertToDate(txtFilterDateReqTo.Text)
        End If

        If Not String.IsNullOrEmpty(txtFilterLessThan.Text) Then
            dcLessThan = Convert.ToDecimal(txtFilterLessThan.Text.Replace("�", ""))
        End If

        If Not String.IsNullOrEmpty(txtFilterGreaterThan.Text) Then
            dcGreaterThan = Convert.ToDecimal(txtFilterGreaterThan.Text.Replace("�", ""))
        End If


        Dim dcTotal As Decimal
        Dim sqlCmd As New SqlCommand
        Dim con As New SqlConnection

        con.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
        If bIncRejected Then
            ' if we are to include rejected items in our report then call
            ' the appropriate Sproc
            sqlCmd.CommandText = "I_STATIONERY_ORDERITEM_SUM_FILTERED"
        Else
            sqlCmd.CommandText = "I_STATIONERY_ORDERITEM_SUM_FILTERED_EXCREJECTED"
        End If
        sqlCmd.CommandType = Data.CommandType.StoredProcedure


        With sqlCmd.Parameters
            Dim sqlParam1 As New SqlParameter
            sqlParam1.DbType = Data.DbType.DateTime
            sqlParam1.Direction = Data.ParameterDirection.Input
            sqlParam1.IsNullable = True
            sqlParam1.ParameterName = "DATEREQUESTEDFROM"
            sqlParam1.Value = IIf(dtDateReqFrom.HasValue, dtDateReqFrom, DBNull.Value)
            .Add(sqlParam1)

            Dim sqlParam2 As New SqlParameter
            sqlParam2.DbType = Data.DbType.DateTime
            sqlParam2.Direction = Data.ParameterDirection.Input
            sqlParam2.IsNullable = True
            sqlParam2.ParameterName = "DATEREQUESTEDTO"
            sqlParam2.Value = IIf(dtDateReqFrom.HasValue, dtDateReqFrom, DBNull.Value)
            .Add(sqlParam2)

            Dim sqlParam3 As New SqlParameter
            sqlParam3.DbType = Data.DbType.Int32
            sqlParam3.Direction = Data.ParameterDirection.Input
            sqlParam3.IsNullable = True
            sqlParam3.ParameterName = "ITEMSTATUS"
            sqlParam3.Value = IIf(iOrderStatus.HasValue, iOrderStatus, DBNull.Value)
            .Add(sqlParam3)

            Dim sqlParam4 As New SqlParameter
            sqlParam4.DbType = Data.DbType.Int32
            sqlParam4.Direction = Data.ParameterDirection.Input
            sqlParam4.IsNullable = True
            sqlParam4.ParameterName = "TEAM"
            sqlParam4.Value = IIf(iTeamId.HasValue, iTeamId, DBNull.Value)
            .Add(sqlParam4)

            Dim sqlParam5 As New SqlParameter
            sqlParam5.DbType = Data.DbType.Int32
            sqlParam5.Direction = Data.ParameterDirection.Input
            sqlParam5.IsNullable = True
            sqlParam5.ParameterName = "EMPLOYEE"
            sqlParam5.Value = IIf(iEmpId.HasValue, iEmpId, DBNull.Value)
            .Add(sqlParam5)

            Dim sqlParam6 As New SqlParameter
            sqlParam6.DbType = Data.DbType.Decimal
            sqlParam6.Direction = Data.ParameterDirection.Input
            sqlParam6.IsNullable = True
            sqlParam6.ParameterName = "LESSTHAN"
            sqlParam6.Value = IIf(dcLessThan.HasValue, dcLessThan, DBNull.Value)
            .Add(sqlParam6)

            Dim sqlParam7 As New SqlParameter
            sqlParam7.DbType = Data.DbType.Decimal
            sqlParam7.Direction = Data.ParameterDirection.Input
            sqlParam7.IsNullable = True
            sqlParam7.ParameterName = "GREATERTHAN"
            sqlParam7.Value = IIf(dcGreaterThan.HasValue, dcGreaterThan, DBNull.Value)
            .Add(sqlParam7)
        End With

        sqlCmd.Connection = con
        sqlCmd.Connection.Open()
        Dim Ret As Object
        Ret = sqlCmd.ExecuteScalar()
        If Ret IsNot DBNull.Value Then
            dcTotal = Convert.ToDecimal(Ret)
        Else
            dcTotal = 0
        End If
        Return dcTotal
    End Function

    Protected Sub gvOrderItemsReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvOrderItemsReport.PageIndex = e.NewPageIndex
        BindOrderItemsReport()
    End Sub

    Protected Sub gvOrderItemsReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.Footer Then
            Dim dcTotalExcRejected As Decimal
            Dim dcTotalIncRejected As Decimal

            Dim bllStationeryItems As New bllStationeryItem

            Dim dtDateReqTo As Nullable(Of DateTime)
            Dim dtDateReqFrom As Nullable(Of DateTime)
            Dim iOrderStatus As Nullable(Of Integer)
            Dim iTeamId As Nullable(Of Integer)
            Dim iEmpId As Nullable(Of Integer)
            Dim dcLessThan As Nullable(Of Decimal)
            Dim dcGreaterThan As Nullable(Of Decimal)

            If Not String.IsNullOrEmpty(ddlFilterItemStatus.SelectedValue) Then
                iOrderStatus = ddlFilterItemStatus.SelectedValue
            End If

            If Not String.IsNullOrEmpty(ddlFilterTeam.SelectedValue) Then
                iTeamId = ddlFilterTeam.SelectedValue
            End If

            If Not String.IsNullOrEmpty(ddlFilterEmployee.SelectedValue) Then
                iEmpId = ddlFilterEmployee.SelectedValue
            End If

            If Not String.IsNullOrEmpty(txtFilterDateReqFrom.Text) Then
                dtDateReqFrom = valhelper.convertToDate(txtFilterDateReqFrom.Text)
            End If

            If Not String.IsNullOrEmpty(txtFilterDateReqTo.Text) Then
                dtDateReqTo = valhelper.convertToDate(txtFilterDateReqTo.Text)
            End If

            If Not String.IsNullOrEmpty(txtFilterLessThan.Text) Then
                dcLessThan = Convert.ToDecimal(txtFilterLessThan.Text.Replace("�", ""))
            End If

            If Not String.IsNullOrEmpty(txtFilterGreaterThan.Text) Then
                dcGreaterThan = Convert.ToDecimal(txtFilterGreaterThan.Text.Replace("�", ""))
            End If


            dcTotalExcRejected = bllStationeryItems.CalcTotals(False, dtDateReqTo, dtDateReqFrom, iOrderStatus, iTeamId, iEmpId, dcLessThan, dcGreaterThan)
            dcTotalIncRejected = bllStationeryItems.CalcTotals(True, dtDateReqTo, dtDateReqFrom, iOrderStatus, iTeamId, iEmpId, dcLessThan, dcGreaterThan)

            e.Row.Cells(5).Text = "Total (inc Rejected):" + "<br /><strong>" + "Total" + "</strong>"

            Dim strTotalExcRejected As String
            Dim strTotalIncRejected As String

            If Not String.IsNullOrEmpty(ddlFilterItemStatus.SelectedValue) And ddlFilterItemStatus.SelectedValue <> "4" Then
                strTotalIncRejected = "n/a"
                Cache.Remove("TotalIncRejected")
            Else
                strTotalIncRejected = dcTotalIncRejected.ToString("c")
                ' cache total for export
                Cache.Insert("TotalIncRejected", dcTotalIncRejected)
            End If


            ' cache total for export
            Cache.Insert("TotalExcRejected", dcTotalExcRejected)

            strTotalExcRejected = dcTotalExcRejected.ToString("c")
            e.Row.Cells(6).Text = strTotalIncRejected + "<br /><strong>" + strTotalExcRejected + "</strong>"

        End If
    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim strJSFunc As String = "OpenExportPage();"
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "UserSecurity", strJSFunc, True)

        Response.Redirect("~/BHAIntranet/CustomPages/Export/ExportStationery.aspx")
    End Sub

    Protected Sub ddlFilterListboxes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterEmployee.SelectedIndexChanged, ddlFilterItemStatus.SelectedIndexChanged, ddlFilterOrderStatus.SelectedIndexChanged
        ' refresh gridview whenever a dropdown is updated within the filter
        ' other fields such as textbox require a manual 'filter' button to be pressed
        CallUIFromTab()
    End Sub

    Protected Sub gvOrderItemsReport_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles gvOrderItemsReport.SelectedIndexChanged

    End Sub
End Class
