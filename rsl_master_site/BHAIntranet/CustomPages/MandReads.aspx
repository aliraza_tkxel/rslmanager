<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    AutoEventWireup="false" CodeFile="MandReads.aspx.vb" Inherits="BHAIntranet_CustomPages_MandReads"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <ajax:ScriptManager ID="ScriptManager1" runat="server">
    </ajax:ScriptManager>
    <ajax:UpdatePanel ID="upPanel" runat="server">
    </ajax:UpdatePanel>
    <strong>Mandatory Reads</strong><br />
    <br />
    <asp:GridView ShowFooter="false" CssClass="gvwMand" ShowHeader="false" ID="gvwMand"
        AutoGenerateColumns="False" runat="server" AllowPaging="True" PageSize="3">
        <EmptyDataTemplate>
        You have no mandatory reads.
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <div class="docimage">
                        <asp:Image ID="imgEmp" ImageUrl='<%# docType(eval("DOCFILE")) %>' runat="server" />
                    </div>
                    <div class="doccontent">
                        <div class="doctitle">
                            <asp:Label ID="lblNameText" runat="server" Text="Document" />:
                            <asp:Label ID="lblTitleText" runat="server" Text='<%# eval("DOCNAME") %>' />
                        </div>
                        <div class="docdetails">
                            <div class="docread">
                                <asp:HiddenField ID="hfDocId" Value='<%# eval("DOCUMENTID") %>' runat="server" />
                                <asp:HiddenField ID="hfDocUrl" Value='<%# eval("FULLPATH") %>' runat="server" />                                
                                <asp:LinkButton ID="lnkRead" CommandName="docread" runat="server">I have read and understood this document</asp:LinkButton>
                            </div>
                            <asp:Label ID="lblViewDoc" runat="server" Text="View Doc:" />                            
                            <asp:LinkButton ID="ReadLink" OnClick="ReadLink_Click" CommandArgument='<%# eval("DOCUMENTID") %>' runat="server">Click here</asp:LinkButton>                            
                            <br />
                            <asp:Label ID="lblDate" runat="server" Text="Date Submitted:" />
                            <asp:Label ID="lblDateText" runat="server" Text='<%# FormatDate(eval("DATECREATED")) %>' /><br />
                            <asp:Label ID="lblSize" runat="server" Text="Size:" />
                            <asp:Label ID="lblSizeText" runat="server" Text='<%# GetFileSize(eval("DOCUMENTID")) %>' /></div>
                            <br />
                            <br />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>           
        </Columns>
        
    </asp:GridView>   
</asp:Content>
