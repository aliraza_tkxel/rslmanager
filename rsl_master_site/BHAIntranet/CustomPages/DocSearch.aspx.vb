
Partial Class BHAIntranet_CustomPages_DocSearch
    Inherits basepage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/MandReads.css")
        master.SetHeading = "Document Search"

        If Request.QueryString("search") <> "" Then
            ViewState("DocName") = Request.QueryString("search")
            Bind()
        End If
        If Not Page.IsPostBack Then
            Dim txt As TextBox = CType(master.Master.FindControl("txtSearch"), TextBox)
            txt.Text = Request.QueryString("Search")
            Dim rdoListSearch As RadioButtonList = CType(master.Master.FindControl("rdoListSearch"), RadioButtonList)
            rdoListSearch.SelectedValue = 2
        End If

    End Sub


    Public Sub Bind()
        If ViewState("DocName") <> "" Then
            Dim bllMand As New bllMandatoryReads
            gvwDoc.DataSource = bllMand.SearchDocs(ASPSession("USERID"), ViewState("DocName"))
            gvwDoc.DataBind()
        Else
            lblValidation.Text = "You must enter search criteria in the search box to perform a search"
        End If
    End Sub
    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dddd, dd MMMM yyyy")
        End If

    End Function

    Public Function DocType(ByVal strFileName As Object) As String
        If IsDBNull(strFileName) Then
            Return "N/A"
        Else
            Return filehelper.GetDocImage(strFileName)
        End If

    End Function

    Protected Sub gvwDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwDoc.PageIndexChanging
        gvwDoc.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub SearchLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton
        lnk = CType(sender, LinkButton)

        If lnk IsNot Nothing Then

            Dim taDocs As New dalDocumentManagerTableAdapters.DOC_DOCUMENTTableAdapter
            Dim docTab As dalDocumentManager.DOC_DOCUMENTDataTable = taDocs.GetData(Integer.Parse(lnk.CommandArgument))
            Dim docRow As dalDocumentManager.DOC_DOCUMENTRow
            If docTab.Rows.Count > 0 Then
                docRow = docTab(0)
            Else
                docRow = Nothing
            End If

            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & docRow.DOCFILE)
            Response.AddHeader("Content-Length", docRow.DOCUMENTFILE.Length)
            Response.ContentType = "application/doc"
            Response.BinaryWrite(docRow.DOCUMENTFILE)
            Response.End()
            Response.Flush()

        End If

    End Sub


End Class
