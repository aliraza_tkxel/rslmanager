<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false"
    CodeFile="MyWhiteboard.aspx.vb" Inherits="BHAIntranet_MyWhiteboard" Title="Untitled Page" %>

<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/NewsHeadlines.ascx" TagName="NewsHeadlines"
    TagPrefix="uc1" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/Suggestions.ascx" TagName="Sugg"
    TagPrefix="ucsugg" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/LatestEmployee.ascx" TagName="Emp"
    TagPrefix="ucEmp" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/Blogs.ascx" TagName="Blogs"
    TagPrefix="ucBlogs" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/MandReads.ascx" TagName="Mand"
    TagPrefix="ucMand" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/LatestDocs.ascx" TagName="LatestDocs"
    TagPrefix="ucLatestDocs" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/SoftwareConnect.ascx" TagName="Soft"
    TagPrefix="ucSoft" %>
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/Alerts.ascx" TagName="Alerts"
    TagPrefix="ucAlerts" %>    
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/TravelSites.ascx" TagName="Travel"
    TagPrefix="ucTravel" %>        
<%@ Register Src="~/BHAIntranet/UserControl/Whiteboard/EnviroTips.ascx" TagName="EnviroTips"
 TagPrefix="ucEnviroTips"    %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" runat="Server">

    <ajax:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </ajax:ScriptManager>
 <script type="text/javascript">

     function updateDetails() {
         document.getElementById("btnUpdateDetails").click();
     }

</script>
    <style type="text/css">

        .lblValidation ul
        {
            list-style-type: none;
        }
        /*.loading-image
        {
            background-color: #fff;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0px;
            right: 0px;
            z-index: 1000000;
            text-align: center;
            opacity: 0.7;
            margin-right: -30px;
        }
        .modalBackground
        {
            background-color: #000000;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup
        {
            top: 260px !important;
            width: 80%;
        }
        .ClosePopupCls
        {
            animation: blinker 1s linear infinite;
            font-size: 16px !important;
            float: right;
            color: White;
            background: transparent !important;
            height: 18px !important;
            width: 70px !important;
            cursor: pointer;
        }
        @keyframes blinker 
        {
          50% {
            opacity: 0;
          }
        }*/

    </style>

    <div id="outerWhiteboardContainer">
        <div id="news_emp_container">
            <uc1:NewsHeadlines ID="ucNewsHeadlines" runat="server" />
            <div id="emp" style="">
                <div id="empheader">
                    New Starter:</div>
                <div id="empbody">
                    <ucEmp:Emp ID="ucEmp" runat="server" />
                </div>
            </div>
            <div id="EnviroTip" >
                <div id="EnviroTipsheader">Tips on Saving Energy</div>
                <div id="EnviroTipsbody">
                    <ucEnviroTips:EnviroTips ID="EnviroTips" runat="server" />
                </div>
              </div> 
        </div>
        <div id="mand_latest_container">
            <div id="manddoc">
                <div id="manddocheader">
                    Mandatory Reads:</div>
                <div id="manddocbody">
                    <ucMand:Mand ID="ucMand" runat="server" />
                </div>
            </div>
            <div id="latestdoc">
                <div id="latestdocheader">
                    Latest Documents:</div>
                <div id="latestdocbody">
                    <ucLatestDocs:LatestDocs ID="ucLatestDocs" runat="server" />
                </div>
            </div>
            <div id="blog" style="">
                <div id="blogheader">
                    BHG Blog :</div>
                <div id="blogbody">
                    <ucBlogs:Blogs ID="Emp1"  runat="server"  />
                </div>
            </div>
        </div>
        <div id="admin_soft_container">
            <div id="admin">
                <div id="adminheader">
                    Alerts:</div>
                   
                <div id="adminbody">
                    <ucAlerts:Alerts ID="ucAlerts" runat="server" />
                </div>
               
            </div>

              <div id="sugg">                
                <div id="suggbody">
                    <asp:Image ID="Image1" runat="server" 
                        ImageUrl="~/BHAIntranet/Images/challenge.png" /><br /><br />
                    <a target="_blank" href="http://broadlandgroup.org/challenge-change-coming-soon/">View Blog</a>
                </div>
            </div> 

            <!--
            <div id="soft">
                <div id="softheader">
                    Software Connect:</div>
                <div id="softbody">
                    <ucSoft:Soft ID="ucSofts" runat="server" />
                </div>
            </div>
            -->
              <div id="travel">
                <div id="travelheader">
                    Travel Websites:</div>
                <div id="travelbody">
                    <ucTravel:Travel ID="ucTravelSites" runat="server" />
                </div>
              </div>

        </div>
    </div>

    <asp:HiddenField ID="hfUpdateDetails1" runat="server" /> 
    <asp:HiddenField ID="hfUpdateDetails2" runat="server" /> 

    <cc1:ModalPopupExtender ID="mpExtender" runat="server" PopupControlID="pnlUpdateDetailsPrompt" TargetControlID="hfUpdateDetails1" CancelControlID="hfUpdateDetails2" BackgroundCssClass="Background">
    </cc1:ModalPopupExtender>
 
    <asp:Panel ID="pnlUpdateDetailsPrompt" runat="server" CssClass="Popup" align="center" style = "display:none">
 
    <iframe id="iframeUpdateDetails" src="" runat="server" class="updateDetails"></iframe>
    <br/>
      
    <asp:Button ID="btnUpdateDetails" runat="server" Text="Button" style="visibility:hidden" ClientIDMode="Static" />

    </asp:Panel>

    <!-- ModalPopupExtender -->
    <%--<asp:Button ID="btnShow" runat="server" Text="Edit" Style="display: none;" />
    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnShow"
        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <ajax:UpdatePanel>
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" style = "display:none">
                <video width="400" height="145" autoplay  src="../Promotions/MB_Logo_Animate_05-10.mp4" ></video>
                <br />
                <div style="margin: auto 0; width: 396px;">
                    <asp:Button ID="btnClose" runat="server" CssClass="btn ClosePopupCls" Text="Close [x]" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </<ajax:UpdatePanel>--%>

</asp:Content>
