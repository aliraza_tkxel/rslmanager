<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportStationery.aspx.vb"
    Inherits="BHAIntranet_CustomPages_Print_ExportStationery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="gvOrderItemsReport" runat="server" CssClass="orderGridView" ShowFooter="True"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="RequestedDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Requested"
                        HtmlEncode="False" SortExpression="RequestedDate"></asp:BoundField>
                    <asp:TemplateField HeaderText="Requested For" SortExpression="RequestedFor">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("RequestedFor") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("RequestedFor") %>'></asp:Label>
                            (<asp:Label ID="Label9" runat="server" Text='<%# Bind("RequestedForTeam") %>'></asp:Label>)
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OfficeLocation" HeaderText="Office Location" />
                    <asp:BoundField DataField="OrderTitle" HeaderText="Order Title" SortExpression="OrderTitle">
                    </asp:BoundField>
                    <asp:BoundField DataField="CatNo" HeaderText="Cat No" />
                    <asp:BoundField DataField="ItemDesc" HeaderText="Item Description" SortExpression="ItemDesc">
                    </asp:BoundField>
                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity">
                    </asp:BoundField>
                    <asp:BoundField DataField="Cost" DataFormatString="{0:c}" HeaderText="Cost"
                        SortExpression="Cost" HtmlEncode="False"></asp:BoundField>
                    <asp:BoundField DataField="Total" DataFormatString="{0:c}" HeaderText="Total"
                        ReadOnly="True" SortExpression="Total" HtmlEncode="False"></asp:BoundField>
                    <asp:BoundField DataField="OrderStatus" HeaderText="Status" SortExpression="OrderStatus">
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    No Stationery Orders Exist.
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
