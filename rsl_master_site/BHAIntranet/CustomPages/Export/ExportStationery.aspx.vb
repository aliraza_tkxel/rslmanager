Imports System.Data

Partial Class BHAIntranet_CustomPages_Print_ExportStationery
    Inherits System.Web.UI.Page

    Private Sub BindOrderItemsReport()


        Dim dt As New DataTable
        dt = Cache("FilteredReport")
        If dt IsNot Nothing Then
            gvOrderItemsReport.DataSource = dt
            gvOrderItemsReport.DataBind()
        End If
    End Sub

    Protected Sub gvOrderItemsReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOrderItemsReport.RowDataBound

        If e.Row.RowType = DataControlRowType.Footer Then
            Dim strTotalExcRejected As String
            Dim strTotalIncRejected As String

            ' retrieve cached results from previous page
            If Cache("TotalExcRejected") IsNot Nothing Then
                strTotalExcRejected = Convert.ToDecimal(Cache("TotalExcRejected")).ToString("c")
            Else
                strTotalExcRejected = "n/a"
            End If

            If Cache("TotalIncRejected") <> -1 Then
                strTotalIncRejected = Convert.ToDecimal(Cache("TotalIncRejected")).ToString("c")
            Else
                strTotalIncRejected = "n/a"
            End If

            e.Row.Cells(5).Text = "Total (inc Rejected):" + "<br /><strong>" + "Total" + "</strong>"
            e.Row.Cells(6).Text = strTotalIncRejected + "<br /><strong>" + strTotalExcRejected + "</strong>"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)


        Response.ContentType = "application/ms-excel"        
        Response.ContentEncoding = System.Text.Encoding.Default
        Response.AddHeader("Content-Disposition", "inline;filename=stationeryexport.xls")
        BindOrderItemsReport()
        gvOrderItemsReport.RenderControl(writer)

    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ' commented out to prevent 'Gridview needs to be in Form tags' error
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
End Class
