
Partial Class BHAIntranet_Admin_FileUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        RegisterJSResetForm()

        Dim pl As ContentPlaceHolder = CType(PreviousPage.Master.FindControl("mainmaster"), ContentPlaceHolder)

        Dim pnl As Panel = CType(pl.FindControl("pnlNewsStory"), Panel)

        Dim fil As FileUpload = pnl.FindControl("filNewsImage")
        Dim filHomeImage As FileUpload = pnl.FindControl("filHomeImage")
        Dim txtHeadline As TextBox = pnl.FindControl("txtHeadline")
        Dim chkActive As CheckBox = pnl.FindControl("chkActive")
        Dim chkIsTenantOnline As CheckBox = pnl.FindControl("chkIsTenantOnline")
        Dim chkIsHeadline As CheckBox = pnl.FindControl("chkIsHeadline")
        Dim fck As FredCK.FCKeditorV2.FCKeditor = pnl.FindControl("fckNews")
        Dim lblValidation As Label = pl.FindControl("lblValidation")
        Dim hidNewsID As HiddenField = pl.FindControl("hidNewsID")
        Try
            Dim bllNews As New bllNews
            bllNews.ImageUpload = fil
            bllNews.HomeImageUpload = filHomeImage
            bllNews.Content = fck.Value
            bllNews.Headline = txtHeadline.Text
            bllNews.Active = chkActive.Checked
            bllNews.HomePage = chkIsHeadline.Checked
            bllNews.TenantOnline = chkIsTenantOnline.Checked
            bllNews.UpdateNews(hidNewsID.Value)
            RegisterJSSuccess()
        Catch ex As Exception
            Response.Write(ex.Message)
            RegisterJSError(ex.Message)
        End Try
    End Sub

    Public Sub RegisterJSResetForm()
        Dim strJS As String
        strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
        strJS = strJS & "parent.document.forms[0].target='_self'" & Environment.NewLine
        strJS = strJS & "parent.document.forms[0].action=parent.location" & Environment.NewLine
        strJS = strJS & "</script>" & Environment.NewLine
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Reset", strJS)

    End Sub

    Public Sub RegisterJSError(ByVal strError As String)
        Dim strJS As String
        strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
        strJS = strJS & "showFormError('" & strError & "');" & Environment.NewLine
        strJS = strJS & "</script>" & Environment.NewLine
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Error", strJS)

    End Sub

    Public Sub RegisterJSSuccess()
        Dim strJS As String
        strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
        strJS = strJS & "showFormError('');" & Environment.NewLine & "parent.refreshForm();" & Environment.NewLine
        strJS = strJS & "</script>" & Environment.NewLine
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Success", strJS)

    End Sub

End Class
