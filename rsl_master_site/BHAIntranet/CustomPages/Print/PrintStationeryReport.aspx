<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/CustomPages/Print/PrintReport.master"
    AutoEventWireup="false" CodeFile="PrintStationeryReport.aspx.vb" Inherits="BHAIntranet_Print_PrintStationeryReport"
    Title="Print Stationery Order" %>
    
<%@ MasterType VirtualPath="~/BHAIntranet/CustomPages/Print/PrintReport.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPrintArea" runat="Server">
    <div id="printArea">
        <asp:FormView ID="fvOrder" runat="server" DataSourceID="odsOrder">                        
            <ItemTemplate>
                <h2>
                    Stationery Order Details</h2>
                <asp:Label ID="Label1" runat="server" CssClass="itemLabel" Text="Order Ref:"></asp:Label>
                <asp:Label ID="OrderIdLabel" runat="server" Text='<%# Bind("OrderId") %>' CssClass="itemField"></asp:Label><br />
                <asp:Label ID="Label2" runat="server" CssClass="itemLabel" Text="Requested Date:"></asp:Label>
                <asp:Label ID="RequestedDateLabel" runat="server" Text='<%# Bind("RequestedDate") %>'
                    CssClass="itemField"></asp:Label>
                <asp:Label ID="Label3" runat="server" CssClass="itemLabel" Text="Description:"></asp:Label>
                <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Bind("Description") %>'
                    CssClass="itemField"></asp:Label>
                <asp:Label ID="Label4" runat="server" CssClass="itemLabel" Text="Requested by:"></asp:Label>
                <asp:Label ID="RequestedByEmpFullNameLabel" runat="server" Text='<%# Bind("RequestedByEmpFullName")%>'
                    CssClass="itemField"></asp:Label>
                <asp:Label ID="Label5" runat="server" CssClass="itemLabel" Text="Requested for:"></asp:Label>
                <asp:Label ID="RequestedForEmpFullNameLabel" runat="server" Text='<%# Bind("RequestedForEmpFullName") %>'
                    CssClass="itemField"></asp:Label>
                <asp:Label ID="Label6" runat="server" CssClass="itemLabel" Text="Delivery Location:"></asp:Label>
                <asp:Label ID="lblAllOrderDevLoc" runat="server"  Text='<%# Replace(Eval("DeliveryLocation"),vbcrlf,"<br/>") %>'  CssClass="lblMultiLine"></asp:Label>
                <br />
                <asp:Label ID="Label7" runat="server" CssClass="itemLabel" Text="Order Status:"></asp:Label>
                <asp:Label ID="OrderStatusLabel" Font-Bold="true" runat="server" Text='<%# Bind("OrderStatus") %>'
                    CssClass="itemField"></asp:Label><br />
                <br />
                <strong>Total Cost:</strong>
                <asp:Label ID="TotalCostLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TotalCost", "{0:c}") %>'
                    CssClass="itemField" Font-Bold="False"></asp:Label>
            </ItemTemplate>
        </asp:FormView>
        <hr />
        <h3>
            Order Items</h3>
        <asp:ObjectDataSource ID="odsOrder" runat="server" SelectMethod="SelectById" TypeName="bllStationery">
            <SelectParameters>
                <asp:QueryStringParameter Name="OrderId" QueryStringField="OrderID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:GridView ID="gvOrderItems" runat="server" AutoGenerateColumns="False" CssClass="orderGridView"
            DataKeyNames="OrderItemId" DataSourceID="odsOrderItems">
            <Columns>
                <asp:BoundField DataField="OrderItemId" HeaderText="Order Item Ref" InsertVisible="False"
                    ReadOnly="True" SortExpression="OrderItemId" />
                <asp:BoundField DataField="ItemDesc" HeaderText="Item Description" SortExpression="ItemDesc" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                <asp:BoundField DataField="Cost" DataFormatString="{0:c}" HeaderText="Cost" HtmlEncode="False"
                    SortExpression="Cost" />
                <asp:BoundField DataField="TOTALCOST" DataFormatString="{0:c}" HeaderText="Total Cost"
                    HtmlEncode="False" />
                <asp:BoundField DataField="Priority" HeaderText="Priority" />
                <asp:BoundField DataField="OrderStatus" HeaderText="Status" />
            </Columns>
            <EmptyDataTemplate>
                No Order items exist for this order.
            </EmptyDataTemplate>
            <SelectedRowStyle BackColor="#EEEEEE" />
        </asp:GridView>
        <asp:ObjectDataSource ID="odsOrderItems" runat="server" SelectMethod="GetItemsByOrderID"
            TypeName="bllStationeryItem">
            <SelectParameters>
                <asp:QueryStringParameter Name="iOrderID" QueryStringField="OrderID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
    </div>
</asp:Content>
