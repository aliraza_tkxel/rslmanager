Imports System.IO
Imports System.Web.Mail

Partial Class BHAIntranet_Print_PrintStationeryReport
    Inherits Page

    Protected Function ConvertDecimal(ByVal objIn As Object) As String
        Dim dcIn As Decimal = CType(objIn, Decimal)

        If Not objIn Is DBNull.Value Then
            Return String.Format("{0:c}", DirectCast(objIn, Decimal)) 'can always use tryparse if not sure it is a valid decimal number
        Else
            Return String.Format("{0:c}", Decimal.Zero)
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.PrintReportTitle = "Print Stationery Report"
        Me.Header.Title = "Print Stationery Report"
    End Sub
End Class
