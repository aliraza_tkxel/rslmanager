Imports System.IO
Imports System.Web.Mail

Partial Class BHAIntranet_Print_PrintStationeryReport
    Inherits Page

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim master As BHAIntranet_CustomPages_Print_PrintReport = CType(Me.Master, BHAIntranet_CustomPages_Print_PrintReport)
            master.SetPageSpecific("/BHAIntranet/CSS/General/Jobs.css")

            Me.Master.PrintReportTitle = "Print Job Detail"
            Me.Header.Title = "Print Job Detail"
        End If
    End Sub


End Class
