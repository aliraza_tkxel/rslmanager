
Partial Class BHAIntranet_CustomPages_Print_PrintReport
    Inherits System.Web.UI.MasterPage

    Public WriteOnly Property PrintReportTitle() As String
        Set(ByVal strTitle As String)
            lblPrintHeader.Text = strTitle            
        End Set
    End Property

    Public Sub SetPageSpecific(ByVal strCSSPath As String)

        Dim CSSHtmlLink As New HtmlLink
        CSSHtmlLink.Href = "~" & strCSSPath
        CSSHtmlLink.Attributes.Add("rel", "Stylesheet")
        CSSHtmlLink.Attributes.Add("type", "text/css")
        CSSHtmlLink.Attributes.Add("media", "all")
        Page.Header.Controls.Add(CSSHtmlLink)

    End Sub
End Class

