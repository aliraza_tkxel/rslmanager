<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/CustomPages/Print/PrintReport.master"
    AutoEventWireup="false" CodeFile="PrintJobDetail.aspx.vb" Inherits="BHAIntranet_Print_PrintStationeryReport"
    Title="Print Stationery Order" %>

<%@ MasterType VirtualPath="~/BHAIntranet/CustomPages/Print/PrintReport.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPrintArea" runat="Server">
    <div id="printArea">
        <asp:FormView ID="fvOrder" runat="server" DataSourceID="odsJobDetail">
            <ItemTemplate>
                <asp:Image ID="imgLogo" CssClass="imgLogo" runat="server" ImageUrl="~/BHAIntranet/Images/General/bhlogo_sm.PNG" />
                <div id="logoJobTitle">
                    <h2>
                        Job Details</h2>
                </div>
                <asp:Label ID="lPostJobTitle" runat="server" Font-Bold="true" AssociatedControlID="lPostJobTitleField"
                    CssClass="itemLabel" Text="Job Title:"></asp:Label>
                <asp:Label ID="lPostJobTitleField" runat="server" Font-Bold="true" CssClass="itemField"
                    Text='<%# Eval("Title") %>'></asp:Label>
                <asp:Label ID="lPostTeam" runat="server" AssociatedControlID="lPostTeamField" CssClass="itemLabel"
                    Text="Team:"></asp:Label>
                <asp:Label ID="lPostTeamField" runat="server" CssClass="itemField" Text='<%# Eval("TeamName") %>'></asp:Label>
                <asp:Label ID="lPostJobType" runat="server" AssociatedControlID="lPostJobTypeField"
                    CssClass="itemLabel" Text="Job Type:"></asp:Label>
                <asp:Label ID="lPostJobTypeField" runat="server" CssClass="itemField" Text='<%# Eval("JobType") %>'></asp:Label>
                <asp:Label ID="lPostJobRef" runat="server" AssociatedControlID="lPostJobRefField"
                    CssClass="itemLabel" Text="Job Ref:"></asp:Label>
                <asp:Label ID="lPostJobRefField" runat="server" Text='<%# Eval("Ref") %>' CssClass="itemField"></asp:Label>
                <asp:Label ID="lPostJobLoc" runat="server" AssociatedControlID="lPostJobLocField"
                    CssClass="itemLabel" Text="Job Location:"></asp:Label>
                <asp:Label ID="lPostJobLocField" runat="server" Text='<%# Eval("Location") %>' CssClass="itemField"></asp:Label>
                <asp:Label ID="lPostJobDuration" runat="server" AssociatedControlID="lPostJobDurationField"
                    CssClass="itemLabel" Text="Duration:"></asp:Label>
                <asp:Label ID="lPostJobDurationField" runat="server" CssClass="itemField" Text='<%# Eval("Duration") %>'></asp:Label>
                <asp:Label ID="lPostJobStart" runat="server" AssociatedControlID="lPostJobStartField"
                    CssClass="itemLabel" Text="Start Date:"></asp:Label>
                <asp:Label ID="lPostJobStartField" CssClass="itemField" runat="server" Text='<%# Eval("StartDate") %>'></asp:Label>
                <asp:Label ID="lPostSalary" runat="server" AssociatedControlID="lPostSalaryField"
                    CssClass="itemLabel" Text="Salary/Rate:"></asp:Label>
                <asp:Label ID="lPostSalaryField" runat="server" Text='<%# Eval("Salary") %>' CssClass="itemField"></asp:Label>
                <asp:Label ID="lPostContactName" runat="server" AssociatedControlID="lPostContactNameField"
                    CssClass="itemLabel" Text="Contact Name:"></asp:Label>
                <asp:Label ID="lPostContactNameField" runat="server" CssClass="itemField" Text='<%# Eval("ContactName") %>'></asp:Label>
                <asp:Label ID="lClosingDate" runat="server" AssociatedControlID="lClosingDateField"
                    CssClass="itemLabel" Text="Closing Date:"></asp:Label>
                <asp:Label ID="lClosingDateField" CssClass="itemField" runat="server" Text='<%# Eval("ClosingDate","{0:d}") %>'></asp:Label>
                <br />
                <asp:Label ID="lPostDesc" runat="server" AssociatedControlID="lPostDescField" Font-Bold="true"
                    Text="Description:"></asp:Label><br />
                <asp:Label ID="lPostDescField" runat="server" CssClass="itemField" Text='<%# Replace(Eval("Description"),vbcrlf,"<br/>") %>'></asp:Label>
            </ItemTemplate>
        </asp:FormView>
        <hr />
        <asp:ObjectDataSource ID="odsJobDetail" runat="server" SelectMethod="GetJobById"
            TypeName="bllJobs">
            <SelectParameters>
                <asp:QueryStringParameter Name="iJobId" QueryStringField="JobID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
    </div>
</asp:Content>
