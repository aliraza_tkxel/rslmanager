Imports System.IO
Imports System.Collections.Generic

Partial Class BHAIntranet_CustomPages_JobsAdmin
    Inherits basepage


    Private Enum eUIMode
        LoadPage

    End Enum


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_templates_main = CType(Me.Master, BHAIntranet_templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Jobs.css")

        If Not Page.IsPostBack Then
            uiController(eUIMode.LoadPage)

        End If
    End Sub

    Private Sub uiController(ByVal iControllerId As eUIMode)
        Select Case iControllerId
            Case eUIMode.LoadPage
                GetTeamNodes()
                PopulateCheckBoxes()

        End Select
    End Sub

    Private Sub GetTeamNodes()

        ' populate teams
        Dim bllJobsAccess As New bllJobsAccess
        Dim dt As dalJobs.I_JOB_ACCESS_TEAMSDataTable
        dt = bllJobsAccess.GetTeams(0)

        For Each dr As dalJobs.I_JOB_ACCESS_TEAMSRow In dt
            Dim newNode As TreeNode = New TreeNode(dr.TEAMNAME, dr.TEAMID)
            newNode.SelectAction = TreeNodeSelectAction.Expand
            tvwRights.Nodes.Add(newNode)

            GetTeamEmployees(newNode)
        Next

    End Sub

    Private Sub GetTeamEmployees(ByVal node As TreeNode)
        ' populate employees
        Dim bllEmployees As New bllEmployees
        Dim dt As dalEmployee.E__EMPLOYEEDataTable        
        dt = bllEmployees.GetEmployeesByTeamID(node.Value)

        For Each dr As dalEmployee.E__EMPLOYEERow In dt
            Dim newNode As TreeNode = New TreeNode(dr.FULLNAME, dr.EMPLOYEEID)
            newNode.SelectAction = TreeNodeSelectAction.Expand
            newNode.SelectAction = TreeNodeSelectAction.Select
            node.ChildNodes.Add(newNode)
        Next

        ' remove parent node if no child nodes
        If Not dt.Rows.Count > 0 Then
            tvwRights.Nodes.Remove(node)
        End If
    End Sub

    Private Sub PopulateCheckBoxes()
        Dim bllJobsAccess As New bllJobsAccess
        Dim dt As dalJobs.I_JOB_ACCESSDataTable
        dt = bllJobsAccess.GetData()

        ' iterate through nodes and check whether exists in table
        For Each node As TreeNode In tvwRights.Nodes
            For Each childNode As TreeNode In node.ChildNodes
                If bllJobsAccess.HasAccessRight(childNode.Value) Then
                    childNode.Checked = True
                End If
            Next
        Next
    End Sub



    Private Sub SaveAccessRights()

        ' first clear database
        Dim bllJobsAccess As New bllJobsAccess
        bllJobsAccess.Clear()

        ' iterate throught tree and add checked rows
        Dim dt As New dalJobs.I_JOB_ACCESSDataTable
        dt = bllJobsAccess.GetData

        For Each node As TreeNode In tvwRights.Nodes

            For Each childNode As TreeNode In node.ChildNodes
                If childNode.Checked Then
                    Dim dr As dalJobs.I_JOB_ACCESSRow = dt.NewI_JOB_ACCESSRow

                    dr.EmployeeId = childNode.Value
                    dt.AddI_JOB_ACCESSRow(dr)
                End If
            Next
        Next


        ' execute multiple inserts to DB as batch
        Dim da As New dalJobsTableAdapters.I_JOB_ACCESSTableAdapter
        da.UpdateWithTransaction(dt)

        ' show success
        pnlTreeView.Visible = False
        pnlSaved.Visible = True

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveAccessRights()
    End Sub


    Protected Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        Dim bllJobsForms As New bllJobForms
        bllJobsForms.InsertBlankForm()

        gvForms.DataBind()
    End Sub


    Protected Sub gvForms_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvForms.RowDeleting
        odsJobForms.DeleteParameters(0).DefaultValue = gvForms.DataKeys(e.RowIndex).Value.ToString
    End Sub


    Protected Sub gvForms_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvForms.RowUpdating
        Dim fu As FileUpload

        fu = gvForms.Rows(e.RowIndex).Cells(4).FindControl("fuUploadForm")

        Dim lLen As Long
        lLen = fu.PostedFile.ContentLength
        Dim abBuffer(lLen) As Byte

        Try
            fu.PostedFile.InputStream.Read(abBuffer, 0, lLen)            
            e.NewValues.Item("FormData") = abBuffer

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearAll.Click
        For Each node As TreeNode In tvwRights.Nodes

            For Each childNode As TreeNode In node.ChildNodes
                If childNode.Checked Then
                    childNode.Checked = False
                End If
            Next
        Next
    End Sub
End Class

