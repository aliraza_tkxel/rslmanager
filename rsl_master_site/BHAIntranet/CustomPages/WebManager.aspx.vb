Imports CustomControls
Imports bllMenu
Imports bllWebsite
Imports dalWebsite

Partial Class BHAIntranet_Admin_WebManager
    Inherits basepage

    Public Enum NavMode
        LoadPage = 1
        CreateMenu = 2
        EditMenu = 3
        CreatePage = 4
        EditPage = 5
        RankPage = 6
        RankMenu = 7
        CreateBespoke = 8
        EditBespoke = 9
        ReloadWebMenu = 10
        LoadMultiMenus = 11
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim master As BHAIntranet_Templates_main = CType(Me.Master, BHAIntranet_Templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/WebManager.css")
        Page.Title = "rslManager CMS"
        AddMultiMenus()
        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)

        End If
    End Sub

    Public Sub AddMultiMenus()

        Dim bllMenu As New bllMenu
        bllMenu.DataFieldID = "menuid"
        bllMenu.DataTextField = "menutitle"
        bllMenu.DataFieldParentID = "parentid"
        bllMenu.SelectedPage = sessionhelper.CurrentID

        Dim bllWebsite As New bllWebsite
        Dim dtWebsite As New I_WEBSITEDataTable
        Dim drWebsite As I_WEBSITERow
        dtWebsite = bllWebsite.GetWebsites


        For Each drWebsite In dtWebsite

            Dim ctl As New Control
            ctl.Controls.Clear()
            ctl = Page.LoadControl("~/BHAIntranet/UserControl/Admin/MultiMenu.ascx")
            ctl.Visible = False
            ctl.ID = "ucMulti" & drWebsite.WebsiteID.ToString


            Dim tvw As MenuTreeview = CType(ctl.FindControl("tvwMultiMenu"), MenuTreeview)
            Dim lbl As Label = CType(ctl.FindControl("lblWebsiteName"), Label)
            lbl.Text = drWebsite.WebSiteDesc
            pnlMultiMenu.Controls.Add(ctl)




        Next
    End Sub


    Public Sub BindMultiMenus()
        Dim bllMenu As New bllMenu
        bllMenu.DataFieldID = "menuid"
        bllMenu.DataTextField = "menutitle"
        bllMenu.DataFieldParentID = "parentid"
        bllMenu.SelectedPage = sessionhelper.CurrentID

        Dim bllWebsite As New bllWebsite
        Dim dtWebsite As New I_WEBSITEDataTable
        Dim drWebsite As I_WEBSITERow
        dtWebsite = bllWebsite.GetWebsites


        For Each drWebsite In dtWebsite

            Dim ctl As New Control
            ctl = pnlMultiMenu.FindControl("ucMulti" & drWebsite.WebsiteID.ToString)
            Dim tvw As MenuTreeview = CType(ctl.FindControl("tvwMultiMenu"), MenuTreeview)
            bllMenu.PopulateMenu(tvw, True, drWebsite.WebsiteID)

        Next
    End Sub

    Public Sub RepopulateMultiMenus()
        Dim bllMenu As New bllMenu
        bllMenu.DataFieldID = "menuid"
        bllMenu.DataTextField = "menutitle"
        bllMenu.DataFieldParentID = "parentid"
        bllMenu.SelectedPage = sessionhelper.CurrentID
        Dim bllPage As New bllPage

        For i As Integer = 0 To chklstWebsite.Items.Count - 1

            Dim ctl As Control
            ctl = pnlMultiMenu.FindControl("ucMulti" & chklstWebsite.Items(i).Value)

            Dim tvw As MenuTreeview = CType(ctl.FindControl("tvwMultiMenu"), MenuTreeview)
            bllMenu.PopulateMenu(tvw, True, chklstWebsite.Items(i).Value)
            If bllPage.DoesPageHaveMultiMenus(sessionhelper.CurrentID, chklstWebsite.Items(i).Value) Then
                ctl.Visible = True
                chklstWebsite.Items(i).Selected = True
            End If
        Next

    End Sub



    Public Sub uiController(ByVal controllerid As Integer)

        pnlMenu.Visible = False
        pnlPage.Visible = False
        pnlRankMenus.Visible = False
        pnlRankPages.Visible = False
        lblValidation.Text = ""

        If controllerid = NavMode.LoadPage Then
            'Dim tvw As New CustomControls.MenuTreeview
            'pnlWebMenu.Controls.Remove(tvwWebMenu)
            'tvwWebMenu.Dispose()
            'tvw.ID = "tvwWebMenu"
            'pnlWebMenu.Controls.Add(tvw)
            fckPage.ToolbarSet = "WebManager"
            Dim bllMenu As New bllMenu
            bllMenu.DataFieldID = "menuid"
            bllMenu.DataTextField = "menutitle"
            bllMenu.DataFieldParentID = "parentid"
            bllMenu.PopulateMenu(tvwWebMenu, False, 1)
            drpWebsites.SelectedValue = "1"
            ucBHAPermissions.Visible = False



        End If

        If controllerid = NavMode.ReloadWebMenu Then
            fckPage.ToolbarSet = "WebManager"
            Dim bllMenu As New bllMenu
            Dim intWebsiteID As Integer
            intWebsiteID = CInt(drpWebsites.SelectedValue)
            bllMenu.DataFieldID = "menuid"
            bllMenu.DataTextField = "menutitle"
            bllMenu.DataFieldParentID = "parentid"
            bllMenu.PopulateMenu(tvwWebMenu, False, intWebsiteID)

        End If
        If controllerid = NavMode.CreateMenu Then
            sessionhelper.CurrentID = 0
            pnlMenu.Visible = True
            formhelper.ClearForm(pnlMenu)
            btnDeleteMenu.Visible = False
        End If

        If controllerid = NavMode.EditMenu Then
            pnlMenu.Visible = True
            btnDeleteMenu.Visible = True
            BindMenu()
        End If

        If controllerid = NavMode.CreatePage Then
            sessionhelper.CurrentID = 0
            chkIsBespoke.Enabled = True
            BindMultiMenus()
            HideMultiMenus()
            ShowTemplate()
            formhelper.ClearForm(pnlPage)
            ucBHAPermissions.SelectAllTeams(True)
        End If

        If controllerid = NavMode.EditPage Then
            pnlPage.Visible = True

            BindPage()
            chkIsBespoke.Enabled = False
            BindMultiMenus()
            HideMultiMenus()
            RepopulateMultiMenus()
            Dim bllPage As New bllPage(sessionhelper.CurrentID)
            chkIsBespoke.Checked = bllPage.IsBespoke
            ucBHAPermissions.PageID = sessionhelper.CurrentID
            ucBHAPermissions.Visible = False
            ucBHAPermissions.PopulatePermissions(sessionhelper.CurrentID)
            btnDeletePage.Visible = True
            If bllPage.IsBespoke Then
                ShowBespoke()
                BindBespokeList()
                'btnDeletePage.Visible = False
                drpBespoke.SelectedValue = bllPage.FileName
                formhelper.RemovePleaseSelect(drpBespoke)
            Else
                ShowTemplate()
            End If


        End If

        If controllerid = NavMode.CreateBespoke Then
            ShowBespoke()
            BindBespokeList()
            formhelper.ClearForm(pnlBespoke)
        End If

        If controllerid = NavMode.RankMenu Then
            Dim bllMenu As New bllMenu
            lstRankMenus.DataSource = bllMenu.GetMenusByParentID(sessionhelper.ParentID)
            lstRankMenus.DataValueField = "menuid"
            lstRankMenus.DataTextField = "menutitle"
            lstRankMenus.DataBind()
            pnlRankMenus.Visible = True
        End If

        If controllerid = NavMode.RankPage Then
            Dim bllPage As New bllPage
            pnlRankPages.Visible = False
            lstRankPages.DataValueField = "MULTIMENUID"
            lstRankPages.DataTextField = "TITLE"
            lstRankPages.DataSource = bllPage.GetMultiPageByMenuID(sessionhelper.ParentID)
            lstRankPages.DataBind()
            pnlRankPages.Visible = True
        End If
    End Sub

    Public Sub BindBespokeList()
        Dim bllPage As New bllPage
        drpBespoke.DataSource = bllPage.GetPageList
        drpBespoke.DataTextField = "Name"
        drpBespoke.DataValueField = "Name"
        drpBespoke.DataBind()
    End Sub

    Public Sub ShowBespoke()
        pnlPage.Visible = True
        ' btnDeletePage.Visible = False
        pnlTemplate.Visible = False
        pnlBespoke.Visible = True
        fckPage.Visible = False
    End Sub

    Public Sub ShowTemplate()
        pnlPage.Visible = True
        'btnDeletePage.Visible = False
        pnlTemplate.Visible = True
        pnlBespoke.Visible = False
        fckPage.Visible = True
    End Sub

    Public Sub BindMenu()
        Dim bllMenu As New bllMenu(sessionhelper.CurrentID)
        txtMenu.Text = bllMenu.MenuTitle
        chkMenuActive.Checked = bllMenu.Active

    End Sub

    Public Sub BindPage()
        Dim bllPage As New bllPage(sessionhelper.CurrentID)
        txtPageName.Text = bllPage.PageName
        txtPageTitle.Text = bllPage.PageTitle
        txtActualPageName.Text = bllPage.FileName
        chkPageActive.Checked = bllPage.Active
        fckPage.Value = bllPage.PageContent
        '        chkDefault.Checked = bllPage.IsDefault

    End Sub

    Protected Sub tvwWebMenu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvwWebMenu.SelectedNodeChanged
        Dim tvw As MenuTreeview = CType(sender, MenuTreeview)
        Dim node As MenuNode = CType(tvw.SelectedNode, MenuNode)


        Dim intDepth As Integer = tvw.SelectedNode.Depth
        Dim intParentID As Integer = 0

        Dim intCurrentid As Integer = CInt(tvw.SelectedNode.Value)
        Dim intActionid As Integer = 0
        Dim intNodeType As Integer = 0

        If Not IsNothing(tvw.SelectedNode.Parent) Then
            intParentID = tvw.SelectedNode.Parent.Value
        End If

        sessionhelper.ParentID = intParentID
        sessionhelper.CurrentID = intCurrentid
        sessionhelper.Depth = intDepth

        If node.NodeType = MenuType.Menu And node.NodeAction = NodeAction.Edit Then
            uiController(NavMode.EditMenu)
        End If

        If node.NodeType = MenuType.Menu And node.NodeAction = NodeAction.Create Then
            uiController(NavMode.CreateMenu)
        End If

        If node.NodeType = MenuType.Page And node.NodeAction = NodeAction.Edit Then

            uiController(NavMode.EditPage)
        End If

        If node.NodeType = MenuType.Page And node.NodeAction = NodeAction.Create Then
            uiController(NavMode.CreatePage)
        End If

        If node.NodeType = MenuType.RankFolder Then
            uiController(NavMode.RankMenu)
        End If

        If node.NodeType = MenuType.RankPage Then
            uiController(NavMode.RankPage)
        End If
    End Sub

    Protected Sub btnUpdateMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateMenu.Click
        Try
            Dim bllMenu As New bllMenu
            bllMenu.MenuTitle = txtMenu.Text
            bllMenu.Depth = sessionhelper.Depth
            bllMenu.Active = chkMenuActive.Checked
            bllMenu.ParentID = sessionhelper.ParentID

            bllMenu.UpdateMenu(sessionhelper.CurrentID)

            lblValidation.Text = ""
            uiController(NavMode.LoadPage)
        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub btnUpdatePage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePage.Click
        Try
            Dim bllPage As New bllPage

            bllPage.PageContent = fckPage.Value
            bllPage.PageName = txtPageName.Text
            bllPage.PageTitle = txtPageTitle.Text
            bllPage.MenuID = sessionhelper.ParentID
            bllPage.Active = chkPageActive.Checked
            bllPage.FileName = drpBespoke.SelectedValue
            bllPage.IsBespoke = chkIsBespoke.Checked
            '            bllPage.IsDefault = chkDefault.Checked
            bllPage.MultiMenu = GetMultiMenu()
            ucBHAPermissions.GetSelected()
            bllPage.SelectedTeams = ucBHAPermissions.GetSelectedTeams()
            bllPage.SelectedEmployees = ucBHAPermissions.GetSelectedEmployees()

            bllPage.UpdatePage(sessionhelper.CurrentID)

            lblValidation.Text = ""
            uiController(NavMode.LoadPage)


        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try

    End Sub

    Protected Sub btnDeletePage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeletePage.Click
        Try
            Dim bllPage As New bllPage

            bllPage.DeleteByPageID(sessionhelper.CurrentID)

            lblValidation.Text = ""
            uiController(NavMode.LoadPage)


        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub btnDeleteMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteMenu.Click
        Try
            Dim bllMenu As New bllMenu
            bllMenu.DeleteMenu(sessionhelper.CurrentID)

            lblValidation.Text = ""
            uiController(NavMode.LoadPage)

        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub chkIsBespoke_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsBespoke.CheckedChanged
        If chkIsBespoke.Checked Then
            uiController(NavMode.CreateBespoke)
        Else
            uiController(NavMode.CreatePage)
        End If
    End Sub

    Protected Sub drpWebsites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpWebsites.SelectedIndexChanged
        uiController(NavMode.ReloadWebMenu)
    End Sub

    Protected Sub chklstWebsite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chklstWebsite.SelectedIndexChanged

        Dim values As String = ""
        For i As Integer = 0 To chklstWebsite.Items.Count - 1

            Dim ctl As Control
            ctl = pnlMultiMenu.FindControl("ucMulti" & chklstWebsite.Items(i).Value)
            If chklstWebsite.Items(i).Selected Then
                ctl.Visible = True
            Else
                ctl.Visible = False
            End If
        Next


    End Sub

    Public Sub HideMultiMenus()
        Dim node As MenuNode

        For i As Integer = 0 To chklstWebsite.Items.Count - 1

            Dim ctl As Control
            ctl = pnlMultiMenu.FindControl("ucMulti" & chklstWebsite.Items(i).Value)
            Dim tvw As MenuTreeview = CType(ctl.FindControl("tvwMultiMenu"), MenuTreeview)
            For Each node In tvw.Nodes
                node.Checked = False
            Next
            chklstWebsite.Items(i).Selected = False
            ctl.Visible = False

        Next
    End Sub



    Public Function GetMultiMenu() As String
        Dim values As String = ""
        For i As Integer = 0 To chklstWebsite.Items.Count - 1

            Dim ctl As Control
            ctl = pnlMultiMenu.FindControl("ucMulti" & chklstWebsite.Items(i).Value)
            If chklstWebsite.Items(i).Selected Then
                Dim tvw As MenuTreeview = CType(ctl.FindControl("tvwMultiMenu"), MenuTreeview)
                values = values & GetSelectedNodes(tvw)

            Else

            End If
        Next
        values = values.TrimEnd(",")
        Return values

    End Function




    Public Function GetSelectedNodes(ByVal tvw As MenuTreeview)
        Dim strNodes As String = ""
        Dim node As New TreeNode
        For Each node In tvw.CheckedNodes
            strNodes = strNodes & node.Value & ","
        Next
        Return strNodes
    End Function

    Protected Sub btnMenuUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMenuUp.Click
        Dim SelectedIndex As Integer = lstRankMenus.SelectedIndex
        If SelectedIndex = -1 Then
            Return
            ' nothing selected 
        End If
        If SelectedIndex = lstRankMenus.Items.Count - 1 Then
            Return
            ' already at top of list 
        End If

        Dim Temp As ListItem
        Temp = lstRankMenus.SelectedItem
        lstRankMenus.Items.Remove(lstRankMenus.SelectedItem)
        lstRankMenus.Items.Insert(SelectedIndex + 1, Temp)
    End Sub

    Protected Sub btnMenuDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMenuDown.Click



        Dim SelectedIndex As Integer = lstRankMenus.SelectedIndex

        If SelectedIndex = -1 Then
            Return
            ' nothing selected 
        End If
        If SelectedIndex = 0 Then
            Return
            ' already at top of list 
        End If

        Dim Temp As ListItem
        Temp = lstRankMenus.SelectedItem

        lstRankMenus.Items.Remove(lstRankMenus.SelectedItem)
        lstRankMenus.Items.Insert(SelectedIndex - 1, Temp)


    End Sub



    Protected Sub btnRankMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRankMenu.Click
        Try
            Dim bllMenu As New bllMenu
            bllMenu.SetMenuByRank(lstRankMenus)
            uiController(NavMode.LoadPage)
        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub btnPageUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPageUp.Click
        Dim SelectedIndex As Integer = lstRankPages.SelectedIndex

        If SelectedIndex = -1 Then
            Return
            ' nothing selected 
        End If
        If SelectedIndex = lstRankPages.Items.Count - 1 Then
            Return
            ' already at top of list 
        End If

        Dim Temp As ListItem
        Temp = lstRankPages.SelectedItem
        lstRankPages.Items.Remove(lstRankPages.SelectedItem)
        lstRankPages.Items.Insert(SelectedIndex + 1, Temp)


    End Sub

    Protected Sub btnPageDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPageDown.Click

        Dim SelectedIndex As Integer = lstRankPages.SelectedIndex

        If SelectedIndex = -1 Then
            Return
            ' nothing selected 
        End If
        If SelectedIndex = 0 Then
            Return
            ' already at top of list 
        End If

        Dim Temp As ListItem
        Temp = lstRankPages.SelectedItem

        lstRankPages.Items.Remove(lstRankPages.SelectedItem)
        lstRankPages.Items.Insert(SelectedIndex - 1, Temp)

    End Sub

    Protected Sub btnRankPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRankPage.Click
        Try
            Dim bllPage As New bllPage
            bllPage.SetPageRank(lstRankPages)
            uiController(NavMode.LoadPage)
        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try
    End Sub

End Class
