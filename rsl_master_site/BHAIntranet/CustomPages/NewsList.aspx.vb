
Partial Class BHAIntranet_CustomPages_NewsList
    Inherits basepage

    Public Function FormatPageContent(ByVal strPageContent)
        Return formhelper.TruncateText(strPageContent, 350)
    End Function

    Public Function GetImagePath(ByVal strPageContent As Object) As String
        If IsDBNull(strPageContent) Then
            Return "/BHAIntranet/Images/Defaults/noimage.gif"
        Else
            Return "/BHAIntranetImagesNews/" & strPageContent.ToString
        End If
    End Function
    Public Function getURL(ByVal strItemID As Integer) As String
        Return "<a href=""/BHAintranet/CustomPages/NewsDetail.aspx?newsid=" + strItemID.ToString + """>Read Article</a>"
    End Function

    'Protected Sub gvwNewsList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwNewsList.PageIndexChanging
    '    gvwNewsList.PageIndex = e.NewPageIndex
    '    Bind()
    'End Sub

    'Protected Sub gvwNewsList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwNewsList.RowCommand
    '    If e.CommandName = "select" Then
    '        Dim gvw As GridView = CType(sender, GridView)
    '        Dim intNewsID As Integer = e.CommandArgument
    '        sessionhelper.NewsID = intNewsID
    '        Response.Redirect("/BHAintranet/CustomPages/NewsDetail.aspx?newsid=" & intNewsID.ToString)
    '    End If

    'End Sub

    Public Sub Bind()
        Dim bllNews As New bllNews
        gvwNewsList.DataSource = bllNews.GetNewsStories()
        gvwNewsList.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/News.css")
        master.SetSubMenuVisibility(False)

        If Not Request.QueryString("page") Is Nothing Then
            Try
                gvwNewsList.PageIndex = Integer.Parse(Request.QueryString("page")) - 1
                'Bind()
            Catch ex As Exception

            End Try
        End If
        If Not Page.IsPostBack Then
            Bind()
        End If
    End Sub

    Protected Sub gvwNewsList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNewsList.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim i As Integer
            For i = 0 To e.Row.Cells(0).Controls(0).Controls(0).Controls.Count - 1
                If Not TypeOf (e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls(0)) Is Label Then
                    Dim pb As LinkButton = DirectCast(e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls(0), LinkButton)
                    Dim lab As New Label()
                    lab.Text = "<a href='?page=" + pb.CommandArgument + "'>" + pb.Text + "</a>"
                    e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls.RemoveAt(0)
                    e.Row.Cells(0).Controls(0).Controls(0).Controls(i).Controls.Add(lab)
                End If
            Next
        End If
    End Sub
End Class
