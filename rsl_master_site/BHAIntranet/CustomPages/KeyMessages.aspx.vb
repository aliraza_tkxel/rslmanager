
Partial Class BHAIntranet_Admin_KeyMessages
    Inherits basepage
    Public Enum NavMode
        LoadPage = 0
        CreateMessage = 1
        EditMessage = 2
    End Enum
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim master As BHAIntranet_Templates_main = CType(Me.Master, BHAIntranet_Templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/WebManager.css")

        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)
        End If
    End Sub

    Public Sub uiController(ByVal controllerid As Integer)

        pnlMessageList.Visible = False
        pnlMessage.Visible = False
        lblValidation.Text = ""
        If controllerid = NavMode.LoadPage Then
            pnlMessageList.Visible = True
            pnlMessage.Visible = False
            gvwMessages.DataBind()
            fckMessage.ToolbarSet = "KeyMessage"
        End If
        If controllerid = NavMode.CreateMessage Then
            pnlMessageList.Visible = False
            pnlMessage.Visible = True
            btnDelete.Visible = False
            formhelper.ClearForm(pnlMessage)
        End If
        If controllerid = NavMode.EditMessage Then
            pnlMessageList.Visible = False
            pnlMessage.Visible = True
            btnDelete.Visible = True
        End If
    End Sub

    Protected Sub lnkNewMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNewMessage.Click
        uiController(NavMode.CreateMessage)
        hidMessageID.Value = 0
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        uiController(NavMode.LoadPage)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim bllKeyMessage As New bllKeyMessages()
            bllKeyMessage.KeyMessage = Trim(fckMessage.Value)
            bllKeyMessage.Update(hidMessageID.Value)
            uiController(NavMode.LoadPage)
        Catch ex As Exception
            lblValidation.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvwMessages_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwMessages.RowCommand
        Dim gvw As GridView = CType(sender, GridView)
        Dim intMessageID As Integer = gvw.DataKeys(e.CommandArgument).Value()

        uiController(NavMode.EditMessage)
        hidMessageID.Value = intMessageID
        BindMessage(intMessageID)
    End Sub

    Public Sub BindMessage(ByVal Messageid As Integer)
        Dim bllKeyMessage As New bllKeyMessages(Messageid)
        fckMessage.Value = bllKeyMessage.KeyMessage

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim bllKeyMessage As New bllKeyMessages()
        bllKeyMessage.Delete(hidMessageID.Value)
        uiController(NavMode.LoadPage)
    End Sub
End Class
