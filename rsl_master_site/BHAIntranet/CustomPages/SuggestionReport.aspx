<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    AutoEventWireup="false" CodeFile="SuggestionReport.aspx.vb" Inherits="BHAIntranet_CustomPages_SuggestionReport"
    Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <ajax:ScriptManager ID="scriptmanager" runat="server">
    </ajax:ScriptManager>
    <asp:Panel ID="pnlSearch" runat="server">
        <ajax:UpdatePanel runat="server" ID="upVal">
            <ContentTemplate>
                <asp:Label ID="lblValidation" runat="server" ForeColor="Red" Text=""></asp:Label>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
        <asp:Label CssClass="suggLabel" AssociatedControlID="txtDateFrom" ID="lblDateFrom"
            runat="server" Text="Date From"></asp:Label>
        <asp:TextBox CssClass="suggInput" runat="server" ID="txtDateFrom"></asp:TextBox>
        <asp:ImageButton ID="imbFrom" CssClass="calBtn" runat="server" ImageUrl="~/BHAIntranet/Images/General/Forms/calendar.gif" />
        <cc1:CalendarExtender Format="dd/MM/yyyy" ID="calExtxtDateFrom" PopupButtonID="imbFrom" TargetControlID="txtDateFrom"       
            runat="server" >
        </cc1:CalendarExtender>
        <asp:ObjectDataSource ID="odsTeams" runat="server" SelectMethod="GetTeamsWithPermissions"
            TypeName="bllTeam">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="PageID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:Label CssClass="suggLabel" AssociatedControlID="txtDateTo" ID="lblDateTo" runat="server"
            Text="Date To"></asp:Label>
        <asp:TextBox CssClass="suggInput" runat="server" ID="txtDateTo"></asp:TextBox>
        <asp:ImageButton ID="imbTo" CssClass="calBtn" ImageUrl="~/BHAIntranet/Images/General/Forms/calendar.gif" runat="server"  />
        <cc1:CalendarExtender PopupButtonID="imbTo" Format="dd/MM/yyyy" ID="calExtxtDateTo" TargetControlID="txtDateTo"
            runat="server" >
        </cc1:CalendarExtender>
        <br />
        <asp:Label CssClass="suggLabel" ID="lblTopic" AssociatedControlID="txtTopic" runat="server"
            Text="Topic"></asp:Label>
        <asp:TextBox runat="server" CssClass="suggInput" ID="txtTopic"></asp:TextBox><br />
        <asp:Label CssClass="suggLabel" ID="lblTeam" AssociatedControlID="drpTeam" runat="server"
            Text="Team"></asp:Label>
        <asp:DropDownList ID="drpTeam" runat="server" DataTextField="TEAMNAME" DataValueField="TEAMID">
        </asp:DropDownList><br />
        <div id="btn">
            <asp:Button ID="btnExport" runat="server" Text="Export" />
            <asp:Button ID="btnSearch" runat="server" Text="Search" />
        </div>
    </asp:Panel>
    <ajax:UpdatePanel runat="server" ID="upList">
        <ContentTemplate>
            <asp:GridView BorderStyle="Solid" BorderWidth="1" BorderColor="black" AutoGenerateColumns="False"
                DataKeyNames="SuggestionID" ID="gvwSuggestionList" runat="server" CssClass="gvwSugg"
                EmptyDataText="There are suggestions that match your criteria">
                <Columns>
                    <asp:BoundField DataField="Fullname" HeaderText="Name">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="100px" VerticalAlign="Top" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TeamName" HeaderText="Team">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="100px" VerticalAlign="Top" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Topic" HeaderText="Topic">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="200px" VerticalAlign="Top" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Suggestion</HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSuggestion" runat="server" Text='<%# FormatPageContent(Eval("Suggestion")) %>'></asp:Label></ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="200px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="True">
                        <ItemStyle VerticalAlign="Top" Width="75px" HorizontalAlign="Center" />
                    </asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <ajax:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <ajax:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
            <ajax:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
        </Triggers>
    </ajax:UpdatePanel>
    <ajax:UpdatePanel ID="upDetails" runat="server">
        <ContentTemplate>
            <asp:Panel Visible="false" ID="pnlSuggestion" runat="server">
                <asp:HiddenField ID="hidSuggID" runat="server" />
                <asp:Label CssClass="lbl" ID="lblAction" AssociatedControlID="txtAction"   runat="server"
                    Text="Action"></asp:Label>
                <asp:TextBox runat="server" CssClass="suggInputLarge"  MaxLength="1500" TextMode="MultiLine" ID="txtAction"></asp:TextBox><br />
                <asp:Label CssClass="lbl" AssociatedControlID="lblTopicDetailText" ID="lblTopicDetail"
                    runat="server" Text="Topic">
                </asp:Label>
                <asp:Label CssClass="txt" ID="lblTopicDetailText" runat="server" Text="Date Form">
                </asp:Label><br />
                <asp:Label CssClass="lbl" AssociatedControlID="lblSuggDetailDateText" ID="lblSuggDateText"
                    runat="server" Text="Date Submitted">
                </asp:Label>
                <asp:Label CssClass="txt" ID="lblSuggDetailDateText" runat="server" Text="Date Form">
                </asp:Label><br />
                <asp:Label CssClass="lbl" AssociatedControlID="lblTeamDetailText" ID="lblTeamDetail"
                    runat="server" Text="Team">
                </asp:Label>
                <asp:Label CssClass="txt" ID="lblTeamDetailText" runat="server" Text="Date Form">
                </asp:Label><br />
                <asp:Label CssClass="lbl" AssociatedControlID="lblEmpDetailText" ID="lblEmpDetail"
                    runat="server" Text="Employee">
                </asp:Label>
                <asp:Label CssClass="txt" ID="lblEmpDetailText" runat="server" Text="Date Form">
                </asp:Label><br />
                <strong>
                    <asp:Label AssociatedControlID="lblSuggDetailText" ID="lblSuggDetail" runat="server"
                        Text="Suggestion">
                    </asp:Label></strong>
                <br />
                <asp:Label ID="lblSuggDetailText" runat="server" Text="Date Form">
                </asp:Label>
                <div id="btnDetail">
                    <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" />
                    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <ajax:AsyncPostBackTrigger ControlID="gvwSuggestionList" EventName="RowCommand" />
        </Triggers>
    </ajax:UpdatePanel>
</asp:Content>
