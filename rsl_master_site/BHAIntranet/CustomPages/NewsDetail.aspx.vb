
Partial Class BHAIntranet_CustomPages_NewsDetail
    Inherits basepage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/News.css")
        master.SetSubMenuVisibility(False)

        Dim bllNews As New bllNews(Request.QueryString("newsid"))
        If Not String.IsNullOrEmpty(bllNews.ImagePath) Then
            imgNews.ImageUrl = "~/BHAIntranetImagesNews/" & bllNews.ImagePath
        Else
            imgNews.ImageUrl = "~/BHAIntranet/Images/Defaults/noimage.gif"
        End If


        lblNewsContent.Text = bllNews.Content
        lblTitle.Text = bllNews.Headline
        lblDateSubmitted.Text = String.Format("{0:dd-MMM-yyyy}", bllNews.DateCreated.Date)
        Page.Title = "News Story"
    End Sub
End Class
