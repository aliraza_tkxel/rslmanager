<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    AutoEventWireup="false" CodeFile="Employees.aspx.vb" Inherits="BHAIntranet_CustomPages_Employees"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server">
        <div id="empSearchArea">
            <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red"></asp:Label>
            <asp:Label ID="lblTeam" CssClass="genLabel" runat="server" Text="Team"></asp:Label><asp:DropDownList
                ID="drpTeams" runat="server">
            </asp:DropDownList>
            <asp:Button ID="btnSearch" CssClass="btn" runat="server" Text="Find" />
        </div>
    </asp:Panel>
    <asp:ObjectDataSource ID="odsNewStarters" runat="server" SelectMethod="GetNewStarters"
        TypeName="bllEmployees"></asp:ObjectDataSource>
    <asp:Panel ID="pnlNewStarterList" runat="server">
        <br />
        <asp:GridView ShowFooter="false" ShowHeader="false" ID="gvwNewStarters" AutoGenerateColumns="False"
            runat="server" AllowPaging="True" DataSourceID="odsNewStarters" PageSize="10">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div id="empimage">
                            <asp:Image ID="imgEmp" ImageUrl='<%# getImagePath(eval("IMAGEPATH")) %>' runat="server" />
                        </div>
                        <div id="empcontent">
                            <div id="nameandtitle">
                                <asp:Label ID="lblNameText" runat="server" Text='<%# eval("FULLNAME") %>' />:
                                <asp:Label ID="lblTitleText" runat="server" Text='<%# eval("JOBTITLE") %>' />
                            </div>
                            <div id="teamandstart">
                                <asp:Label ID="lblTeam" runat="server" Text="Department22:" />
                                <asp:Label ID="lblTeamText" runat="server" Text='<%# eval("TEAMNAME") %>' /><br />
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" />
                                <asp:Label ID="lblStartDateText" runat="server" Text='<%# FormatStartDate(eval("STARTDATE")) %>' />
                            </div>
                            <!--<div id="profile">
                                <asp:Label ID="lblProfile" runat="server" Text='<%# eval("PROFILE") %>' />
                            </div>-->
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlEmpByTeam" Visible="false" runat="server">
        <asp:GridView EmptyDataText="There are no employees, which match your criteria"
            ShowFooter="false" Width="100%" ShowHeader="false" ID="gvwEmpList" AutoGenerateColumns="False"
            runat="server" AllowPaging="True" PageSize="10">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div id="empimage">
                            <asp:Image ID="imgEmp" ImageUrl='<%# getImagePath(eval("IMAGEPATH")) %>' runat="server" />
                        </div>
                        <div id="empcontent">
                            <div id="nameandtitle">
                                <asp:Label ID="lblNameText" runat="server" Text='<%# eval("FULLNAME") %>' />:
                                <asp:Label ID="lblTitleText" runat="server" Text='<%# eval("JOBTITLE") %>' />
                            </div>
                            <div id="teamandstart">
                                <asp:Label ID="lblTeam" runat="server" Text="Department:" />
                                <asp:Label ID="lblTeamText" runat="server" Text='<%# eval("TEAMNAME") %>' /><br />
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" />
                                <asp:Label ID="lblStartDateText" runat="server" Text='<%# FormatStartDate(eval("STARTDATE")) %>' />
                            </div>
                            <div id="profile">
                                <asp:Label ID="lblProfile" runat="server" Text="" />
                            </div>
                            <div class="emplink">
                                <asp:LinkButton ID="lnkEmp" CommandName="SelectEmp" CommandArgument='<%# eval("EMPLOYEEID") %>'
                                    runat="server">View Details</asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlEmpDetails" Visible="false" runat="server">
        <div id="empimage">
            <asp:Image ID="imgEmp" ImageUrl='<%# getImagePath(eval("IMAGEPATH")) %>' runat="server" />
        </div>
        <div id="empcontent">
            <div id="nameandtitle">
                <asp:Label ID="lblNameText" runat="server" />:
                <asp:Label ID="lblTitleText" runat="server" />
            </div>
            <div id="teamandstart">
                <asp:Label ID="Label1" CssClass="lbl" runat="server" Text="Department:" />
                <asp:Label ID="lblTeamText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblStartDate" CssClass="lbl" runat="server" Text="Start Date:" />
                <asp:Label ID="lblStartDateText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblWorkEmail" CssClass="lbl" runat="server" Text="Work Email:" />
                <asp:Label ID="lblWorkEmailText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblWorkPhone" CssClass="lbl" runat="server" Text="Work Phone:" />
                <asp:Label ID="lblWorkPhoneText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblWorkMobile" CssClass="lbl" runat="server" Text="Work Mobile:" />
                <asp:Label ID="lblWorkMobileText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblOfficeLocation" CssClass="lbl" runat="server" Text="Office Location:" />
                <asp:Label ID="lblOfficeLocationText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblLineManager" CssClass="lbl" runat="server" Text="Line Manager:" />
                <asp:Label ID="lblLineManagerText" CssClass="lbltxt" runat="server" />
                <asp:Label ID="lblRoleProfile" CssClass="lbl" runat="server" Text="Role Profile:" />
                <asp:HyperLink ID="hlnkRoleProfile" Target="_blank" CssClass="lbltxt" runat="server"
                    Text="Click here to download"></asp:HyperLink>
            </div>
            <div id="profile">
                <asp:Label ID="lblProfile" CssClass="lbl" runat="server" Text="About Employee:" />
                <asp:Label ID="lblProfileText" CssClass="lbltxt" runat="server" Text='<%# eval("PROFILE") %>' />
            </div>
            <br />
            <div class="emplink">
                <asp:LinkButton ID="lnkReturn" OnClick="lnkReturn_Click" runat="server">Return to List</asp:LinkButton>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
