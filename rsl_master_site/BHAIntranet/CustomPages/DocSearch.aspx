<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/CustomPageWithBorder.master"
    AutoEventWireup="false" CodeFile="DocSearch.aspx.vb" Inherits="BHAIntranet_CustomPages_DocSearch"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ThreeTierContent" runat="Server">
    <asp:Label ID="lblValidation" runat="server" ForeColor="Red" Text=""></asp:Label>
    <asp:GridView ShowFooter="false" CssClass="gvwMand" ShowHeader="false" ID="gvwDoc"
        AutoGenerateColumns="False" runat="server" AllowPaging="True" PageSize="6">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <br />
                    <div class="docimage">
                        <asp:Image ID="imgEmp" ImageUrl='<%# docType(eval("DOCFILE")) %>' runat="server" />
                    </div>
                    <div class="doccontent">
                        <div class="doctitle">
                            <asp:Label ID="lblNameText" runat="server" Text="Document" />:
                            <asp:Label ID="lblTitleText" runat="server" Text='<%# eval("DOCNAME") %>' />
                        </div>
                        <div class="docdetails">
                            <asp:Label ID="lblTeam" runat="server" Text="View Doc:" />
                            
                            <asp:LinkButton ID="lnkDoc" OnClick="Searchlink_Click" CommandArgument='<%# eval("DOCUMENTID") %>' runat="server">Click here</asp:LinkButton>                            
                            <br />
                            <asp:Label ID="lblDate" runat="server" Text="Date Submitted:" />
                            <asp:Label ID="lblDateText" runat="server" Text='<%# FormatDate(eval("DATECREATED")) %>' />
                            <br />
                            <br />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
