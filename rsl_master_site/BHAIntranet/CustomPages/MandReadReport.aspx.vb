
Partial Class BHAIntranet_CustomPages_MandReadReport
    Inherits basepage
    Public Enum NavMode
        LoadPage = 0
    End Enum
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim master As BHAIntranet_Templates_CustomPageWithBorder = CType(Me.Master, BHAIntranet_Templates_CustomPageWithBorder)
        master.SetPageSpecific("/BHAIntranet/CSS/General/MandReads.css")
        master.SetHeading = "Mandatory Read Report"
        If Not Page.IsPostBack Then
            uiController(NavMode.LoadPage)
        End If
    End Sub


    Public Sub uiController(ByVal controllerid As Integer)

        If controllerid = NavMode.LoadPage Then

            Dim bllTeam As New bllTeam
            drpTeam.DataTextField = "TeamName"
            drpTeam.DataValueField = "TeamID"
            drpTeam.DataSource = bllTeam.GetTeamsWithPermissions(0)
            drpTeam.DataBind()
            formhelper.ClearForm(pnlSearch)

        End If

    End Sub

    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dddd, dd MMMM yyyy")
        End If

    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        ViewState("TeamID") = drpTeam.SelectedValue
        ViewState("ExpiresFrom") = txtDateFrom.Text
        ViewState("ExpiresTo") = txtDateTo.Text
        ViewState("CreatedFrom") = txtDateCreatedFrom.Text
        ViewState("CreatedTo") = txtDateCreatedTo.Text
        BindMand()
    End Sub

    Public Sub BindMand()
        Try


            Dim bllMand As New bllMandatoryReads
            Dim intTeamID As Nullable(Of Integer)
            If ViewState("TeamID") <> "" Then
                intTeamID = CInt(ViewState("TeamID"))
            End If
            gvwMand.DataSource = bllMand.GetReport(ViewState("ExpiresFrom"), _
                                                    ViewState("ExpiresTo"), _
                                                    ViewState("CreatedFrom"), _
                                                    ViewState("CreatedTo"), _
                                                    intTeamID, _
                                                    txtDocTitle.Text)

            gvwMand.DataBind()
            lblValidation.Text = ""
        Catch ex As Exception
            lblValidation.Text = ex.Message.ToString
        End Try

    End Sub

    Protected Sub gvwMand_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwMand.PageIndexChanging
        gvwMand.PageIndex = e.NewPageIndex
        BindMand()
    End Sub


    Protected Sub gvwMand_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "ViewEmps" Then
            Dim gv As GridView = CType(sender, GridView)
            'Dim iDocId As Integer = Convert.ToInt32(e.CommandArgument)
            Dim iRowIdx As Integer = Convert.ToInt32(e.CommandArgument)

            Dim iDocId As Integer = gv.DataKeys(iRowIdx).Values.Item("DocumentID")
            Dim sDocName As String = gv.DataKeys(iRowIdx).Values.Item("DOCNAME")

            ViewState("DocID") = iDocId
            ViewState("DocName") = sDocName

            BindMandatoryreadreport(iDocId)

            pnlMandReadsReport.Visible = False
            pnlMandReadsEmpList.Visible = True
            lblDocName.Text = sDocName
        End If
    End Sub

    Protected Sub gvwMand_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnk As LinkButton = e.Row.FindControl("lnkEmpToRead")

            'lnk.CommandArgument = gvwMand.DataKeys(e.Row.RowIndex).Value.ToString
            lnk.CommandArgument = e.Row.RowIndex.ToString
        End If
    End Sub

    Private Sub BindMandatoryreadreport(ByVal iDocId As Integer)
        Dim bllMandatoryReads As New bllMandatoryReads
        gvMandatoryreadreport.DataSource = bllMandatoryReads.GetEmployeesToRead(iDocId)
        gvMandatoryreadreport.DataBind()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlMandReadsReport.Visible = True
        pnlMandReadsEmpList.Visible = False
    End Sub

    Protected Sub gvMandatoryreadreport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMandatoryreadreport.PageIndexChanging
        gvMandatoryreadreport.PageIndex = e.NewPageIndex
        If ViewState("DocID") IsNot Nothing Then
            BindMandatoryreadreport(ViewState("DocID"))
        End If
    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("CSV_Doc_Export.aspx?DocID=" + ViewState("DocID").ToString)

    End Sub
End Class
