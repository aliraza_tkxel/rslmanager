<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false"
    CodeFile="JobAdmin.aspx.vb" Inherits="BHAIntranet_CustomPages_JobsAdmin" Title="Job Post Access"
    MaintainScrollPositionOnPostback="true" Trace="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/BHAIntranet/UserControl/General/JobsListing.ascx" TagName="JobsListing"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" runat="Server">
    <ajax:ScriptManager ID="smScriptManager" runat="server" EnablePartialRendering="true">
    </ajax:ScriptManager>
    <div id="jobsHeader">
        Jobs Administration&nbsp;</div>
    <div id="jobsContentBox">
        <ajax:UpdatePanel ID="upTopPanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <span class="waitSpinner">
                    <ajax:UpdateProgress ID="upgFilter" runat="server" AssociatedUpdatePanelID="upTopPanel"
                        DynamicLayout="false">
                        <ProgressTemplate>
                            <asp:Image ID="imgWaitSpinner3" runat="server" CssClass="waitSpinner" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
                        </ProgressTemplate>
                    </ajax:UpdateProgress>
                </span>
                <asp:Panel ID="pnlTreeView" runat="server" Visible="true">
                    <asp:TreeView ID="tvwRights" runat="server" CssClass="tvwRights" ExpandDepth="0"
                        ShowCheckBoxes="Leaf">
                        <SelectedNodeStyle BackColor="#FFFF80" />
                        <LeafNodeStyle ImageUrl="~/myImages/user.png" />
                    </asp:TreeView>
                    <br />
                    <asp:Button runat="server" ID="btnSave" Text="Apply" CssClass="btn" />
                    <asp:Button runat="server" ID="btnClearAll" Text="Clear" CssClass="btn" /><br />
                    <br />
                </asp:Panel>
                <asp:Panel ID="pnlSaved" runat="server" Visible="false">
                    <br />
                    <asp:Label ID="lblSaved" runat="server" Text="Settings have been saved."></asp:Label><br /><br />
                    <a href="Jobs.aspx">Click here</a> here to view the job page. <br />
                    <a href="MyWhiteboard.aspx">Click here</a> here to return to the whiteboard.
                </asp:Panel>
            </ContentTemplate>
        </ajax:UpdatePanel>
        <br />
        <asp:Panel ID="pnlUploadForms" runat="server" Visible="false">
            Application Form Uploader<br />
            <asp:GridView ID="gvForms" runat="server" AutoGenerateColumns="False" DataKeyNames="ApplicationId"
                DataSourceID="odsJobForms">
                <Columns>
                    <asp:BoundField DataField="ApplicationId" HeaderText="ApplicationId" ReadOnly="true"
                        SortExpression="ApplicationId" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:TemplateField HeaderText="Application Form">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><asp:FileUpload ID="fuUploadForm"
                                runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItemIndex %>'
                                CommandName="Delete" Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <br />
            <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="Insert" Width="174px">Add New Application Form</asp:LinkButton><br />
            <br />
            <asp:ObjectDataSource ID="odsJobForms" runat="server" DeleteMethod="DeleteForm" SelectMethod="GetData"
                TypeName="bllJobForms" UpdateMethod="UpdateForm">
                <DeleteParameters>
                    <asp:Parameter Name="ApplicationId" Type="Int32" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ApplicationId" Type="int32" />
                    <asp:Parameter Name="Name" Type="string" />
                    <asp:Parameter Name="FormData" Type="Object" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </asp:Panel>
        &nbsp;
    </div>
</asp:Content>
