Imports System.IO
Imports System.Collections.Generic

Partial Class BHAIntranet_CustomPages_Jobs
    Inherits basepage

    Private Enum NavMode
        LoadPage = 0
        FindJobs
        SearchJobsNow
        PostJobs
        ShortList
        JobDetail
    End Enum

    Private Enum Tabs
        FindJobs = 0
        PostJobs
        Shortlist
        JobDetail
    End Enum

    Private Function GetCurrentTab() As Tabs
        Select Case tcTopTabs.ActiveTab.ID
            Case "tbFindJobs"
                Return Tabs.FindJobs

            Case "tbPostJobs"
                Return Tabs.PostJobs

            Case "tbShortList"
                Return Tabs.Shortlist

            Case "tbJobDetail"
                Return Tabs.JobDetail

        End Select
    End Function

    Private ReadOnly Property HasJobAdminRights() As Boolean
        Get
            Dim bRet As Boolean = False
            If ASPSession("USERID") <> "" Then
                Dim bllJobsAccess As New bllJobsAccess
                bRet = bllJobsAccess.HasAccessRight(ASPSession("USERID"))
            Else
                Response.Redirect("/BHAIntranet/login.aspx?status=signout")
            End If
            Return bRet
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim master As BHAIntranet_templates_main = CType(Me.Master, BHAIntranet_templates_main)
        master.SetPageSpecific("/BHAIntranet/CSS/General/Jobs.css")


        If Not Page.IsPostBack Then                        
            uiController(NavMode.LoadPage)
        End If

    End Sub


    Private Sub uiController(ByVal iMode As NavMode)

        Select Case iMode
            Case NavMode.LoadPage
                tcSearchResults.Visible = True
                tcTopTabs.ActiveTabIndex = Tabs.FindJobs
                tbJobDetail.Enabled = False                
                tbPostJobs.Enabled = HasJobAdminRights
                ucJobsListing.JobAdminView = HasJobAdminRights
                ucJobsListing.BindJobsData()

            Case NavMode.FindJobs
                tcSearchResults.Visible = True
                tcTopTabs.ActiveTabIndex = Tabs.FindJobs
                tbJobDetail.Enabled = False
                ucJobsListing.BindJobsData()

            Case NavMode.SearchJobsNow
                tcSearchResults.Visible = True
                tbJobDetail.Enabled = False
                ucJobsListing.Mode = BHAIntranet_UserControl_General_JobsListing.UIModes.Search
                ucJobsListing.FilterLocId = ddlSearchLoc.SelectedValue
                ucJobsListing.FilterPostedIn = ddlSearchPostedIn.SelectedValue
                ucJobsListing.FilterTitle = txtSearchTitle.Text
                ucJobsListing.BindJobsData()

            Case NavMode.JobDetail
                tcSearchResults.Visible = False
                tcTopTabs.ActiveTabIndex = Tabs.JobDetail
                tbJobDetail.Enabled = True
                If ViewState("JobId") IsNot Nothing Then
                    ucJobsDetail.BindJobsData(ViewState("JobId"))
                End If

            Case NavMode.PostJobs
                tbJobDetail.Enabled = False                
                tcSearchResults.Visible = False
                PostJobView()

            Case NavMode.ShortList                
                tcTopTabs.ActiveTabIndex = Tabs.Shortlist
                tbJobDetail.Enabled = False
                tcSearchResults.Visible = False
                ucJobsShortList.BindJobsData()
        End Select
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        uiController(NavMode.SearchJobsNow)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'reset filters
        ddlSearchLoc.SelectedIndex = 0
        ddlSearchPostedIn.SelectedIndex = 0
        txtSearchTitle.Text = ""

        uiController(NavMode.FindJobs)
    End Sub

    Private Sub PostJobView()
        lblPostJobValidation.Text = ""

        ddlPostTeam.Items.Clear()
        ddlPostJobType.Items.Clear()
        ddlPostTeam.Items.Clear()
        ddlJobAppForm.Items.Clear()

        formhelper.ClearForm(pnlPostJobs)

        ' first bind teams
        Dim bllTeams As New bllTeam
        ddlPostTeam.DataSource = bllTeams.GetTeamsWithPermissions(0)
        ddlPostTeam.DataTextField = "TEAMNAME"
        ddlPostTeam.DataValueField = "TEAMID"
        ddlPostTeam.DataBind()

        ' now bind job types
        Dim bllJobs As New bllJobs
        ddlPostJobType.DataSource = bllJobs.GetJobTypes()
        ddlPostJobType.DataTextField = "JobType"
        ddlPostJobType.DataValueField = "JobTypeId"
        ddlPostJobType.DataBind()

        ' bind application form types
        Dim bllJobForms As New bllJobForms
        ddlJobAppForm.DataSource = bllJobForms.GetData()
        ddlJobAppForm.DataTextField = "Name"
        ddlJobAppForm.DataValueField = "ApplicationId"
        ddlJobAppForm.DataBind()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PostJobView()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click        

        If validatePostJob() Then
            lblPostJobValidation.Text = ""
            Dim bllJobs As New bllJobs
            With bllJobs
                .TeamId = ddlPostTeam.SelectedValue
                .Desc = txtPostJobDesc.Text
                .TypeId = ddlPostJobType.SelectedValue
                .OtherJobType = txtOtherJobType.Text
                .Title = txtPostJobTitle.Text
                .Ref = txtPostJobRef.Text
                .Location = ddlPostJobLoc.SelectedValue
                .Duration = txtPostJobDuration.Text
                .StartDate = txtPostJobStart.Text
                .ClosingDate = Convert.ToDateTime(txtClosingDate.Text)
                .Salary = txtPostJobSalary.Text
                .ContactName = txtPostJobContactName.Text
                .ApplicationId = ddlJobAppForm.SelectedValue
            End With

            Try
                bllJobs.PostJob()
                uiController(NavMode.FindJobs)
            Catch ex As Exception
                lblPostJobValidation.Text = ex.Message
            End Try


        End If

    End Sub

    Private Function validatePostJob() As Boolean
        Dim errors As New ErrorCollection

        Try


            If valhelper.validateIsEmpty(ddlPostTeam.SelectedValue) Then
                errors.Add("Please select a team.")
            End If

            If valhelper.validateIsEmpty(txtPostJobDesc.Text) Then
                errors.Add("Please enter a description.")
            End If

            If Not valhelper.validateLength(txtPostJobDesc.Text, 2500) Then
                errors.Add("Please enter only 2500 characters for the job description.")
            End If

            If valhelper.validateIsEmpty(ddlPostJobType.SelectedValue) Then
                errors.Add("Please select a job type.")
            End If

            If ddlPostJobType.SelectedItem.Text = "Other" Then
                If valhelper.validateIsEmpty(txtOtherJobType.Text) Then
                    errors.Add("Please specify other job type.")
                End If
            End If

            If valhelper.validateIsEmpty(txtPostJobTitle.Text) Then
                errors.Add("Please enter a job title.")
            End If

            If valhelper.validateIsEmpty(ddlPostJobLoc.SelectedValue) Then
                errors.Add("Please enter a job location.")
            End If

            If valhelper.validateIsEmpty(txtPostJobDuration.Text) Then
                errors.Add("Please enter a job duration.")
            End If

            If valhelper.validateIsEmpty(txtPostJobStart.Text) Then
                errors.Add("Please enter a job start date.")
            End If

            If Not valhelper.validateDate(txtClosingDate.Text) Then
                errors.Add("Please enter a valid closing date in format dd/mm/yy")
            Else
                If Convert.ToDateTime(txtClosingDate.Text) <= Now.Date Then
                    errors.Add("Please enter a closing date after today")
                End If
            End If

            If valhelper.validateIsEmpty(txtPostJobSalary.Text) Then
                errors.Add("Please enter a job salary/rate.")
            End If

            If valhelper.validateIsEmpty(txtPostJobContactName.Text) Then
                errors.Add("Please enter a contact name.")
            End If

            If valhelper.validateIsEmpty(ddlJobAppForm.SelectedValue) Then
                errors.Add("Please select an application type.")
            End If

        Catch ex As Exception
            errors.Add(ex.Message)
        End Try

        If errors.Count > 0 Then
            lblPostJobValidation.Text = valhelper.Display(errors)
            Return False
        End If

        Return True
    End Function

    Protected Sub tcTopTabs_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcTopTabs.ActiveTabChanged

        Select Case GetCurrentTab()
            Case Tabs.FindJobs
                uiController(NavMode.FindJobs)

            Case Tabs.PostJobs
                uiController(NavMode.PostJobs)

            Case Tabs.Shortlist
                uiController(NavMode.ShortList)

            Case Tabs.JobDetail
                uiController(NavMode.JobDetail)

        End Select


    End Sub

    Protected Sub ucJobsListing_Apply(ByVal sender As Object, ByVal e As BHAIntranet_UserControl_General_JobsListing.JobDetailEventArgs) _
        Handles ucJobsListing.Apply, ucJobsShortList.Apply
        ViewState("JobId") = e.JobId
        uiController(NavMode.JobDetail)
    End Sub

    Protected Sub ucJobsDetail_Apply(ByVal sender As Object, ByVal e As BHAIntranet_UserControl_General_JobsListing.JobDetailEventArgs) Handles ucJobsDetail.Apply
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "JumpAnchor", "window.location.href = '#JobDetailArea';", True)
    End Sub

    Protected Sub ucJobsDetail_Back(ByVal sender As Object, ByVal e As EventArgs) Handles ucJobsDetail.Back
        uiController(NavMode.FindJobs)
    End Sub

    Protected Sub ucJobsDetail_ChangedMode(ByVal sender As Object, ByVal e As BHAIntranet_UserControl_General_JobsListing.UiModeEventArgs) _
        Handles ucJobsDetail.ChangedMode, ucJobsListing.ChangedMode

        If e.uiMode = BHAIntranet_UserControl_General_JobsListing.UIModes.JobDetail Then
            ' here handle the control changing mode and change
            ' the header of the owning tab appropriately
            tbSearchResults.HeaderText = "Edit / Copy"
        ElseIf e.uiMode = BHAIntranet_UserControl_General_JobsListing.UIModes.Search Then
            tbSearchResults.HeaderText = "Search Results"
        End If

    End Sub


    Protected Sub ddlSearchLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        uiController(NavMode.SearchJobsNow)
    End Sub


    Protected Sub ddlSearchPostedIn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchPostedIn.SelectedIndexChanged
        uiController(NavMode.SearchJobsNow)
    End Sub

    Protected Sub smScriptManager_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles smScriptManager.AsyncPostBackError
        smScriptManager.AsyncPostBackErrorMessage = e.Exception.Message
    End Sub

    Protected Sub ddlPostJobType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList
        ddl = CType(sender, DropDownList)
        If ddl IsNot Nothing Then
            If ddl.SelectedItem.Text = "Other" Then
                txtOtherJobType.Visible = True
                lblOtherType.Visible = True
            Else
                txtOtherJobType.Text = ""
                txtOtherJobType.Visible = False
                lblOtherType.Visible = False
            End If
        End If
    End Sub
End Class
