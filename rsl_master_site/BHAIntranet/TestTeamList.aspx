<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false" CodeFile="TestTeamList.aspx.vb" Inherits="BHAIntranet_TestTeamList" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" Runat="Server">
    <asp:ObjectDataSource ID="odsTeams" runat="server" SelectMethod="GetTeamsWithPermissions"
        TypeName="bllTeam" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="TEAMID"
        DataSourceID="odsTeams">
       
        <Columns>
        <asp:TemplateField>
     
        <ItemTemplate>
            <asp:LinkButton ID="lnkSelect" runat="server" CausesValidation="False" CommandName="Select" Text="Select" /><br />
            <asp:Label ID="Label1" runat="server" Text='<%# Bind("TEAMNAME") %>'></asp:Label>
            <asp:Panel id="pnlTeam" cssclass="hide" runat="server" >
               
            
            <asp:CheckBox ID="chkTeam" AutoPostBack="true" runat="server" OnCheckedChanged="chkTeam_CheckedChanged" />
            <asp:Label ID="lblSelectAll"   runat="server" Text="Select All"></asp:Label>
            <br />
                    <asp:Label ID="lblTeamID" Visible="false"  runat="server" Text='<%# Bind("TeamID") %>'></asp:Label>
                    <asp:ObjectDataSource ID="odsEmps" runat="server" SelectMethod="GetEmployeesByTeamID" TypeName="bllEmployees">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="lblTeamID" Name="TeamID" PropertyName="Text" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    &nbsp;
                    <asp:CheckBoxList ID="chklistEmployees" runat="server" DataSourceID="odsEmps" DataTextField="FULLNAME"
                        DataValueField="EMPLOYEEID">
                    </asp:CheckBoxList>
             </asp:Panel>        
        </ItemTemplate>
        </asp:TemplateField>
         </Columns>
    </asp:GridView>
        
        
        
        
        
        
    <asp:GridView ID="gvwTeams" Visible="false" runat="server" AutoGenerateColumns="False" DataKeyNames="TEAMID"
        DataSourceID="odsTeams">
        <Columns>           
            <asp:TemplateField HeaderText="TEAMNAME" SortExpression="TEAMNAME">
                <EditItemTemplate>
                    <asp:Label ID="lblTeam" runat="server" Text='<%# Bind("TEAMNAME") %>'></asp:Label>
                    <asp:CheckBox ID="chkTeamPermission" runat="server" Checked='<%# Bind("haspermission") %>'
                        Enabled="false" />
                    
                    <asp:Label ID="lblTeamID" Visible="false" runat="server" Text='<%# Bind("TeamID") %>'></asp:Label>
                    <asp:ObjectDataSource ID="odsEmps" runat="server" SelectMethod="GetEmployeesByTeamID" TypeName="bllEmployees">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="lblTeamID" Name="TeamID" PropertyName="Text" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    &nbsp;
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSourceID="odsEmps" DataTextField="FULLNAME"
                        DataValueField="EMPLOYEEID">
                    </asp:CheckBoxList>
                    
                    
                    
                    
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("TEAMNAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-VerticalAlign="Top">
                <EditItemTemplate >
                    <asp:CheckBox  ID="chkTeamPermission" runat="server" Checked='<%# Bind("haspermission") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkTeamPermission" runat="server" Checked='<%# Bind("haspermission") %>'
                        Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField  >
            <asp:TemplateField ShowHeader="False" ItemStyle-VerticalAlign="Top">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkSelect" runat="server" CausesValidation="False" CommandName="Edit"
                        Text="Select"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    
    

</asp:Content>

