<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false" CodeFile="RSSFeed.aspx.vb" Inherits="BHAIntranet_Pages_RSSFeed" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" Runat="Server">
    <asp:GridView ID="gvwRss" AutoGenerateColumns="false" runat="server" AllowPaging="True">
    <Columns>
    <asp:TemplateField><ItemTemplate>
    <div>
    <strong><asp:Label ID="lblTitle" runat="server" Text='<%#  Eval("title") %>'></asp:Label></strong> &nbsp;
    (<asp:Label ID="lblDate" runat="server" Text='<%#  formatdate(Eval("pubdate")) %>'></asp:Label>)
    <br />
    <div>
    <asp:Label ID="lblDesc" runat="server" Text='<%#  Eval("description") %>'></asp:Label>
   </div>
   <div class="rsslink">
       <asp:HyperLink ID="hlLink" Target="_blank" runat="server" Text="View full story" NavigateUrl='<%#  Eval("link") %>'></asp:HyperLink>
   </div>
   <br /><br /><br />
    </div>
    </ItemTemplate>
    </asp:TemplateField>    
    </Columns>
    </asp:GridView>

</asp:Content>

