<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/Templates/main.master" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="BHAIntranet_Pages_Search" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainMaster" Runat="Server">

    <asp:Panel ID="pnlSeach" runat="server" DefaultButton="btnSearch" >
     
    <asp:Label ID="lblSearch" CssClass="genLabel" runat="server" Text="Search"></asp:Label>
    <asp:TextBox ID="txtSearch" CssClass="genInput" runat="server"></asp:TextBox>  
    <asp:Button ID="btnSearch" runat="server" Text="Search" cssClass="btnSearch" /> 
    <br />
     </asp:Panel> 
    <asp:GridView AutoGenerateColumns="false" Width="100%" ID="gvwSearch" runat="server" AllowPaging="True">
    
    <Columns>
    <asp:TemplateField>
    <ItemTemplate>
        <strong><asp:Label ID="lblTitle" runat="server" Text='<%#  Eval("title") %>'></asp:Label></strong> &nbsp;
        <br />
        <asp:Label ID="lblSummary" runat="server" Text='<%#  FormatSearch(Eval("HitHighlightedSummary")) %>'></asp:Label> &nbsp;
 <br />
  
     <div class="rsslink">
       <asp:HyperLink ID="hlLink"  runat="server" Text="View full story" NavigateUrl='<%#  Eval("path") %>'></asp:HyperLink>
   </div>  
   <br />
   <br />
    </ItemTemplate>
    </asp:TemplateField>
    </Columns>
    
    
    </asp:GridView>

</asp:Content>

