Imports System.Data
Imports System.IO
Imports System.Xml
Partial Class BHAIntranet_Pages_RSSFeed
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindRSS()
        End If
    End Sub

    Private Sub BindRSS()
        Dim reader As XmlTextReader = New XmlTextReader("http://www.24dash.com/rss/feed.rss")
        Dim ds As DataSet = New DataSet()
        ds.ReadXml(reader)
        gvwRss.DataSource = ds.Tables(3)
        gvwRss.DataBind()
    End Sub

    Protected Sub gvwRss_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwRss.PageIndexChanging

        gvwRss.PageIndex = e.NewPageIndex
        BindRSS()
    End Sub

    Public Function FormatDate(ByVal strDate As String) As String
        Return Left(strDate, Len(strDate) - 9)
    End Function
End Class
