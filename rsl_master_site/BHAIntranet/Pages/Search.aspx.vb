Imports System.Data
Partial Class BHAIntranet_Pages_Search
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not Page.IsPostBack Then

            Dim strSearch As String = Request.QueryString("search")


            If Request.QueryString("search") <> "" Then
                txtSearch.Text = strSearch
                ViewState("strSearch") = strSearch
                Bind(strSearch)
            End If

        End If
    End Sub


    Private Sub Bind(ByVal strSearch As String)
        'Dim queryService As New tmp_websearch.Service
        Dim queryService As New ReidSearch.ReidSearch
        Dim ds As DataSet = queryService.Search(strSearch)
        gvwSearch.DataSource = ds
        gvwSearch.DataBind()
    End Sub


    Public Function FormatSearch(ByVal strSearch As String) As String

        strSearch = strSearch.Replace("<c0>", "<strong>")
        strSearch = strSearch.Replace("</c0>", "</strong>")
        strSearch = strSearch.Replace("<c1>", "<strong>")
        strSearch = strSearch.Replace("</c1>", "</strong>")

        Return strSearch
    End Function


    Protected Sub gvwSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvwSearch.PageIndexChanging

        gvwSearch.PageIndex = e.NewPageIndex
        Bind(ViewState("strSearch"))
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Response.Write("test")
        Bind(txtSearch.Text)

    End Sub
End Class
