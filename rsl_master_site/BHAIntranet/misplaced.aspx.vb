﻿Imports System.Data.SqlClient
Imports System.Data
Imports bllLogin
Imports System.Drawing.Color
Imports System.Net.Mail

Partial Class BHAIntranet_misplaced
    Inherits basepage

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        resetMessage()
    End Sub

#End Region

#Region "Button Submit Click"

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        sendEmail()
    End Sub

#End Region

#Region "Send Email"

    Protected Sub sendEmail()

        If (Page.IsValid) Then
            Dim bllLogin As New bllIntranetLogin
            Dim status As Boolean = bllLogin.getCredentialsByWorkEmail(txtEmail.Text.Trim())

            Dim message As String

            If status Then
                Try
                    Dim mailMsg As New MailMessage
                    mailMsg.To.Add(txtEmail.Text.Trim())
                    mailMsg.Subject = "RSL Manager Login Information"
                    mailMsg.Body = "User Name: " + bllLogin.UserName + "<br>Password: " + bllLogin.Password '&#10; = Line Feed
                    mailMsg.From = New MailAddress("noreply@broadlandgroup.org")
                    mailMsg.IsBodyHtml = True

                    Dim smtp As SmtpClient = New SmtpClient
                    smtp.EnableSsl = True
                    smtp.Host = "smtp.office365.com"
                    smtp.Port = 25
                    smtp.UseDefaultCredentials = False
                    smtp.Credentials = New System.Net.NetworkCredential("outgoingmail@broadlandgroup.org", "S@xupad2")
                    smtp.Send(mailMsg)
                    setMessage("Your Login details have been emailed to you. Once you have received your login details try to login again.", False)
                Catch ex As Exception
                    setMessage(message + ex.message, True)
                End Try
            Else
                setMessage("Your email address does not match any of our records, please try again.", True)
            End If
        Else
            setMessage("Kindly provide a valid Email address.", True)
        End If
    End Sub

#End Region

#Region "set Message"

    Public Sub setMessage(ByVal message As String, ByVal isError As Boolean)

        If isError = True Then
            lblMessage.Text = message
            lblMessage.ForeColor = Red
            lblMessage.Font.Bold = True
        Else
            lblMessage.Text = message
            lblMessage.ForeColor = Green
            lblMessage.Font.Bold = True
        End If
        pnlMessage.Visible = True

    End Sub

#End Region

#Region "reset Message"

    Public Sub resetMessage()
        lblMessage.Text = String.Empty
        pnlMessage.Visible = False
    End Sub

#End Region


End Class
