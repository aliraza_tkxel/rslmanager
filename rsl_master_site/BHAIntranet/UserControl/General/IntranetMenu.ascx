<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IntranetMenu.ascx.vb"
    Inherits="BHAIntranet_UserControl_General_IntranetMenu" %>

<script type="text/javascript" language="javascript">
        function wb_rollover(strRollover)
        {
            document.getElementById("mainmenuheader").innerHTML = strRollover
        }
        
        function wb_rollout()
        {
        
         document.getElementById("mainmenuheader").innerHTML = "&nbsp;"
        }     
</script>

<%--<div id="mainmenu_buttons">
    <ul id="rslmodules">
        <asp:Repeater ID="rptRSLModuleButtons" runat="server">
            <ItemTemplate>
                <li>
                    <asp:HiddenField runat="server" ID="hfDesc" Value='<%# Eval("DESCRIPTION")%>' />
                    <asp:HyperLink runat="server" ID="hlkButton" NavigateUrl='<%# Eval("THEPATH")%>'
                        ImageUrl='<%# "~\BHAIntranet\Images\WBRollovers\" & Eval("IMAGETITLE") %>'>
                    </asp:HyperLink>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
<div id="mainmenuheader">
    Customers
</div>--%>
<ul id="nav">
    <li><a href="#">Broadland Business Systems Modules</a>
        <ul>
            <asp:Repeater ID="rptRSLMenu" runat="server">
                <ItemTemplate>
                    <li><a href="<%# Eval("THEPATH")%>">
                        <%#Eval("DESCRIPTION")%>
                    </a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </li>
</ul>
<asp:ObjectDataSource ID="odsRSLModules" runat="server" SelectMethod="GetRSLModules"
    TypeName="bllRSLModules">
    <SelectParameters>
        <asp:SessionParameter Name="sEmpID" SessionField="USERID" Type="String" />
        <asp:Parameter DefaultValue="" Name="bSortASC" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsParentMenu" runat="server" SelectMethod="GetTopLevelMenuByEmpID"
    TypeName="bllMenu" OldValuesParameterFormatString="original_{0}">
    <SelectParameters>
        <asp:SessionParameter Name="EmpID" SessionField="USERID" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:Repeater ID="rptParentRepeater" runat="server" DataSourceID="odsParentMenu">
    <HeaderTemplate>
        <ul id="intranetnav">
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="lblMenuID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "menuid")%>'></asp:Label>
        <li><a href="<%#DataBinder.Eval(Container.DataItem, "path")%>">
            <%#DataBinder.Eval(Container.DataItem, "menutitle")%>
        </a>
            <asp:ObjectDataSource ID="odsChildMenu" runat="server" SelectMethod="GetMenuByParentIDAndEmpID"
                TypeName="bllMenu">
                <SelectParameters>
                    <asp:ControlParameter ControlID="lblMenuID" Name="ParentID" PropertyName="Text" Type="Int32" />
                    <asp:SessionParameter Name="EmpID" SessionField="USERID" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Repeater ID="rptChildRepeater" runat="server" DataSourceID="odsChildMenu">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li><a href="<%#DataBinder.Eval(Container.DataItem, "path")%>">
                        <%#DataBinder.Eval(Container.DataItem, "menutitle")%>
                    </a></li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
&nbsp; 