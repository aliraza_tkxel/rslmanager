Imports System.Data
Partial Class BHAIntranet_UserControl_General_IntranetMenu
    Inherits baseusercontrol

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("USERID") = ASPSession("USERID")

        If Not Page.IsPostBack Then
            Dim bllRSLMods As New bllRSLModules
            rptRSLMenu.DataSource = bllRSLMods.GetRSLModules(Session("USERID"), 1)
            '  rptRSLModuleButtons.DataSource = bllRSLMods.GetRSLModules(Session("USERID"), 0)

            ' bind repeaters
            rptRSLMenu.DataBind()
            ' rptRSLModuleButtons.DataBind()
        End If
    End Sub


    Protected Sub rptParentRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptParentRepeater.ItemDataBound

        Dim dv As DataRowView = e.Item.DataItem
        If Not IsNothing(dv) Then
            Dim nestedRepeater As Repeater = CType(e.Item.FindControl("rptChildRepeater"), Repeater)
            If nestedRepeater.Items.Count = 0 Then
                nestedRepeater.Visible = False
            End If
        End If

    End Sub

    'Protected Sub rptRSLModuleButtons_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptRSLModuleButtons.ItemDataBound
    '    If e.Item.ItemType = ListItemType.AlternatingItem Or _
    '        e.Item.ItemType = ListItemType.Item Then
    '        Dim hlk As HyperLink = e.Item.FindControl("hlkButton")
    '        Dim hf As HiddenField = e.Item.FindControl("hfDesc")

    '        If hlk IsNot Nothing Then
    '            If hf IsNot Nothing Then
    '                hlk.Attributes.Add("onmouseover", "wb_rollover('" + hf.Value + "')")
    '                hlk.Attributes.Add("onmouseout", "wb_rollout()")
    '            End If
    '        End If
    '    End If
    'End Sub
End Class
