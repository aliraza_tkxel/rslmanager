Imports System.Data
Imports AjaxControlToolkit
Imports dalJobs

Imports System.Data.SqlClient
Partial Class BHAIntranet_UserControl_General_JobsListing
    Inherits baseusercontrol

    Public Class JobDetailEventArgs
        Inherits EventArgs
        Private _JobId As Integer

        Public Sub New(ByVal JobId As Integer)
            _JobId = JobId
        End Sub

        Public ReadOnly Property JobId() As Integer
            Get
                Return _JobId
            End Get
        End Property

    End Class

    Public Class UiModeEventArgs
        Inherits EventArgs
        Private _uiMode As UIModes

        Public Sub New(ByVal uiMode As UIModes)
            _uiMode = uiMode
        End Sub

        Public ReadOnly Property uiMode() As UIModes
            Get
                Return _uiMode
            End Get
        End Property
    End Class

    Const pageSize As Integer = 10

    Public Delegate Sub ApplyDelegate(ByVal sender As Object, ByVal e As JobDetailEventArgs)

    Public Event Apply As ApplyDelegate
    Public Event Back As EventHandler
    Public Event ChangedMode As EventHandler(Of UiModeEventArgs)

    Public Enum UIModes
        NotSet
        ShortList
        Search
        ShowAll
        JobDetail
    End Enum


#Region "Properties"


    Private _currentMode As UIModes
    Private _dataSource As I_JOBDataTable
    Private _currentPageIdx As Integer
    Private _numItems As Integer
    Private _filterTitle As String
    Private _filterLocId As Integer
    Private _filterPostedIn As Integer

    Public Property CurrentPage() As Integer
        Get
            Dim obj = ViewState("CurrentPage")
            If obj IsNot Nothing Then
                Return Convert.ToInt32(obj)
            Else
                Return 0
            End If
        End Get

        Set(ByVal value As Integer)
            ViewState("CurrentPage") = value
        End Set
    End Property

    Public Property NumPages() As Integer
        Get
            Dim obj = ViewState("NumPages")
            If obj IsNot Nothing Then
                Return Convert.ToInt32(obj)
            Else
                Return 0
            End If
        End Get

        Set(ByVal value As Integer)
            ViewState("NumPages") = value
        End Set
    End Property

    Public Property IsFirstPage() As Boolean
        Get
            Dim obj = ViewState("IsFirstPage")
            If obj IsNot Nothing Then
                Return Convert.ToBoolean(obj)
            Else
                Return 0
            End If
        End Get

        Set(ByVal value As Boolean)
            ViewState("IsFirstPage") = value
        End Set
    End Property

    Public Property IsLastPage() As Boolean
        Get
            Dim obj = ViewState("IsLastPage")
            If obj IsNot Nothing Then
                Return Convert.ToBoolean(obj)
            Else
                Return 0
            End If
        End Get

        Set(ByVal value As Boolean)
            ViewState("IsLastPage") = value
        End Set
    End Property

    Public Property NumItems() As Integer
        Get
            Dim obj = ViewState("NumItems")
            If obj IsNot Nothing Then
                Return Convert.ToInt32(obj)
            Else
                Return 0
            End If
        End Get

        Set(ByVal value As Integer)
            ViewState("NumItems") = value
        End Set
    End Property

    Public Property FilterTeam() As Integer
        Get
            Dim obj = ViewState("FilterTeam")
            If obj IsNot Nothing Then
                Return Convert.ToInt32(obj)
            Else
                Return 0
            End If
        End Get

        Set(ByVal value As Integer)
            ViewState("FilterTeam") = value
        End Set
    End Property

    Public Property ShowActive() As Boolean
        Get
            Dim obj = ViewState("ShowActive")
            If obj IsNot Nothing Then
                Return Convert.ToBoolean(obj)
            Else
                Return False
            End If
        End Get

        Set(ByVal value As Boolean)
            ViewState("ShowActive") = value
        End Set
    End Property

    Public Property Mode() As UIModes
        Get
            Return _currentMode
        End Get
        Set(ByVal value As UIModes)
            _currentMode = value
        End Set
    End Property

    Public Property FilterTitle() As String
        Get
            Dim obj = ViewState("FilterTitle")
            If obj IsNot Nothing And obj <> "" Then
                Return Convert.ToString(obj)
            Else
                Return Nothing
            End If
        End Get

        Set(ByVal value As String)
            ViewState("FilterTitle") = value
        End Set
    End Property

    Public Property JobAdminView() As Boolean
        Get
            Dim obj = ViewState("JobAdminView")
            If obj IsNot Nothing Then
                Return Convert.ToBoolean(obj)
            Else
                Return Nothing
            End If
        End Get

        Set(ByVal value As Boolean)
            ViewState("JobAdminView") = value
        End Set
    End Property


    Public Property FilterLocId() As Nullable(Of Integer)
        Get
            Dim obj = ViewState("FilterLocId")
            If obj IsNot Nothing And obj > 0 Then
                Return Convert.ToInt32(obj)
            Else
                Return Nothing
            End If
        End Get

        Set(ByVal value As Nullable(Of Integer))
            ViewState("FilterLocId") = value
        End Set
    End Property

    Public Property FilterPostedIn() As Nullable(Of Integer)
        Get
            Dim obj = ViewState("FilterPostedIn")
            If obj IsNot Nothing And obj > Nothing Then
                Return Convert.ToInt32(obj)
            Else
                Return Nothing
            End If
        End Get

        Set(ByVal value As Nullable(Of Integer))
            ViewState("FilterPostedIn") = value
        End Set
    End Property

#End Region

    Public Sub New()
        _currentMode = UIModes.NotSet
        ShowActive = 1
        CurrentPage = 0
    End Sub

    Public Sub BindJobsData(Optional ByVal iJobId As Integer = -1)

        Dim bllJobs As New bllJobs
        Dim pgdJobs As PagedDataSource = Nothing

        Select Case _currentMode
            Case UIModes.Search
                ' remove job editor if still visible
                mvJobsListing.SetActiveView(vwJobsListing)
                pgdJobs = bllJobs.GetSearchedJobsPageDataSource(FilterTeam, FilterTitle, FilterLocId, FilterPostedIn, CurrentPage, pageSize, ShowActive)

            Case UIModes.ShowAll
                ' remove job editor if still visible
                mvJobsListing.SetActiveView(vwJobsListing)
                pgdJobs = bllJobs.GetJobsAsPagedDataSource(FilterTeam, CurrentPage, pageSize, ShowActive)

            Case UIModes.ShortList
                Dim iEmpId As Integer = Convert.ToInt32(ASPSession("USERID"))
                pgdJobs = bllJobs.GetShortListAsPagedDataSource(iEmpId, CurrentPage, pageSize)

            Case UIModes.JobDetail
                If iJobId = -1 Then
                    Throw New ArgumentException("Supply Job ID when binding with job detail mode")
                Else
                    rptJobsListing.DataSource = bllJobs.GetJobById(iJobId)
                    rptJobsListing.DataBind()
                End If
        End Select

        ' store some data about pages into properties
        If pgdJobs IsNot Nothing Then            
            NumPages = pgdJobs.PageCount
            NumItems = pgdJobs.DataSourceCount
            IsFirstPage = pgdJobs.IsFirstPage
            IsLastPage = pgdJobs.IsLastPage

            rptJobsListing.DataSource = pgdJobs
            rptJobsListing.DataBind()
        End If
    End Sub

    Protected Function getJobType(ByVal atype As String, ByVal aothertype As String) As String
        If atype = "Other" Then
            Return aothertype
        Else
            Return atype
        End If
    End Function

    Protected Sub rptJobsListing_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptJobsListing.ItemCommand
        Select Case e.CommandName
            Case "Previous"
                CurrentPage -= 1
                BindJobsData()

            Case "Next"
                CurrentPage += 1
                BindJobsData()

            Case "AddShortList"
                Dim bllJobs As New bllJobs
                bllJobs.AddToShortList(Convert.ToInt32(ASPSession("USERID")), Convert.ToInt32(e.CommandArgument))
                RaiseEvent Back(Me, EventArgs.Empty)
                Mode = UIModes.ShowAll
                BindJobsData()

            Case "Apply"
                Dim ev As New JobDetailEventArgs(Convert.ToInt32(e.CommandArgument()))
                RaiseEvent Apply(Me, ev)

                ' if current mode is job detail then collapse the job detail so
                ' we can see the application instructions more clearly
                If _currentMode = UIModes.JobDetail Then
                    Dim cpx As CollapsiblePanelExtender
                    cpx = rptJobsListing.Items(0).FindControl("cpxLongDescription")
                    If cpx IsNot Nothing Then
                        cpx.Collapsed = True
                        cpx.ClientState = "true"
                    End If                    

                End If

            Case "RemoveShortList"
                Dim bllJobs As New bllJobs
                bllJobs.RemoveFromShortList(Convert.ToInt32(ASPSession("USERID")), Convert.ToInt32(e.CommandArgument))
                BindJobsData()

            Case "PrintDetail"
                Dim strPrintPath As String = "Print/PrintJobDetail.aspx?JobID=" + e.CommandArgument
                Dim strScript As String = PrintHelper.GetPrintScript(strPrintPath)
                ScriptManager.RegisterStartupScript(Me, Page.GetType, "UserSecurity", strScript, True)

            Case "TitleClick"
                Dim ev As New JobDetailEventArgs(Convert.ToInt32(e.CommandArgument()))
                RaiseEvent Apply(Me, ev)

            Case "Back"
                RaiseEvent Back(Me, EventArgs.Empty)

            Case "EditJob"
                mvJobsListing.SetActiveView(vwEditJob)
                BindEditJob(Convert.ToInt32(e.CommandArgument))                
                RaiseEvent ChangedMode(Me, New UiModeEventArgs(UIModes.JobDetail))

            Case "CopyJob"
                mvJobsListing.SetActiveView(vwEditJob)
                BindCopyJob(Convert.ToInt32(e.CommandArgument))
                RaiseEvent ChangedMode(Me, New UiModeEventArgs(UIModes.JobDetail))

        End Select

    End Sub

    Private Function validateEditJob() As Boolean
        Dim errors As New ErrorCollection

        Try
            Dim ddl As DropDownList = fvEditJob.FindControl("ddlPostTeam")
            If valhelper.validateIsEmpty(ddl.SelectedValue) Then
                errors.Add("Please select a team.")
            End If

            Dim txt As TextBox = fvEditJob.FindControl("txtPostJobDesc")
            If valhelper.validateIsEmpty(txt.Text) Then
                errors.Add("Please enter a description.")
            End If

            If Not valhelper.validateLength(txt.Text, 2500) Then
                errors.Add("Please enter only 2500 characters for the job description.")
            End If

            ddl = fvEditJob.FindControl("ddlJobDetailType")
            txt = fvEditJob.FindControl("txtOtherJobType")
            If valhelper.validateIsEmpty(ddl.SelectedValue) Then
                errors.Add("Please select a job type.")
            ElseIf ddl.SelectedItem.Text = "Other" Then
                If valhelper.validateIsEmpty(txt.Text) Then
                    errors.Add("Please specify other job type.")
                End If
            End If

            txt = fvEditJob.FindControl("txtPostJobTitle")
            If valhelper.validateIsEmpty(txt.Text) Then
                errors.Add("Please enter a job title.")
            End If

            ddl = fvEditJob.FindControl("ddlPostJobLoc")
            If valhelper.validateIsEmpty(ddl.SelectedValue) Then
                errors.Add("Please enter a job location.")
            End If

            'txt = fvEditJob.FindControl("txtJobDetailReference")
            'If valhelper.validateIsEmpty(txt.Text) Then
            '    errors.Add("Please enter a job reference.")
            'End If

            txt = fvEditJob.FindControl("txtPostJobDuration")
            If valhelper.validateIsEmpty(txt.Text) Then
                errors.Add("Please enter a job duration.")
            End If

            txt = fvEditJob.FindControl("txtPostJobStart")
            If valhelper.validateIsEmpty(txt.Text) Then
                errors.Add("Please enter a job start date.")
            End If

            txt = fvEditJob.FindControl("txtPostJobSalary")
            If valhelper.validateIsEmpty(txt.Text) Then
                errors.Add("Please enter a job salary/rate.")
            End If

            txt = fvEditJob.FindControl("txtPostJobContactName")
            If valhelper.validateIsEmpty(txt.Text) Then
                errors.Add("Please enter a contact name.")
            End If

            txt = fvEditJob.FindControl("txtClosingDate")
            If Not valhelper.validateDate(txt.Text) Then
                errors.Add("Please enter a valid closing date in format dd/mm/yy")
            Else
                If Convert.ToDateTime(txt.Text) <= Now.Date Then
                    errors.Add("Please enter a closing date after today")
                End If
            End If

            ddl = fvEditJob.FindControl("ddlJobForms")
            If valhelper.validateIsEmpty(ddl.SelectedValue) Then
                errors.Add("Please select a job application form")
            End If

        Catch ex As Exception
            errors.Add(ex.Message)
        End Try    

        If errors.Count > 0 Then
            lblPostJobValidation.Text = valhelper.Display(errors)
            Return False
        End If

        Return True
    End Function

    Private Sub BindEditJob(ByVal iJobId As Integer)
        lblPostJobValidation.Text = ""

        Dim bllJobs As New bllJobs()
        fvEditJob.ChangeMode(FormViewMode.Edit)
        fvEditJob.DataSource = bllJobs.GetJobById(iJobId)
        fvEditJob.DataBind()

        ' show/hide other job type
        EnableJobType()
    End Sub

    Private Sub BindCopyJob(ByVal iJobId As Integer)

        ' swap into insert mode and grab data
        lblPostJobValidation.Text = ""
        fvEditJob.ChangeMode(FormViewMode.Insert)

        ' ensure dropdowns are bound
        fvEditJob.DataBind()

        Dim bllJobs As New bllJobs(iJobId)

        Dim ddl As DropDownList
        Dim txt As TextBox

        With bllJobs
            ddl = fvEditJob.FindControl("ddlPostTeam")
            ddl.SelectedValue = .TeamId

            txt = fvEditJob.FindControl("txtPostJobDesc")
            txt.Text = .Desc

            ddl = fvEditJob.FindControl("ddlJobDetailType")
            ddl.SelectedValue = .TypeId

            txt = fvEditJob.FindControl("txtOtherJobType")
            txt.Text = .OtherJobType

            txt = fvEditJob.FindControl("txtPostJobTitle")
            txt.Text = .Title

            ddl = fvEditJob.FindControl("ddlPostJobLoc")
            ddl.SelectedValue = .Location

            txt = fvEditJob.FindControl("txtJobDetailReference")
            txt.Text = .Ref

            txt = fvEditJob.FindControl("txtPostJobDuration")
            txt.Text = .Duration

            txt = fvEditJob.FindControl("txtPostJobStart")
            txt.Text = .StartDate

            txt = fvEditJob.FindControl("txtPostJobSalary")
            txt.Text = .Salary

            txt = fvEditJob.FindControl("txtPostJobContactName")
            txt.Text = .ContactName

            ddl = fvEditJob.FindControl("ddlJobForms")
            ddl.SelectedValue = .ApplicationId

            ' add commandargument
            Dim lnk As LinkButton
            lnk = fvEditJob.FindControl("lnkPostJob")
            lnk.CommandArgument = iJobId.ToString

        End With

        ' show/hide other job type
        EnableJobType()

    End Sub


    Protected Sub rptJobsListing_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobsListing.ItemDataBound

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                ' if normal list then show 'add to shortlist' and hide 'remove...' buttons
                Dim btnRemove As ImageButton = e.Item.FindControl("btnRemoveShortList")
                Dim btnAdd As ImageButton = e.Item.FindControl("btnAddToShortList")                
                Dim btnApply As Button = e.Item.FindControl("btnApply")
                Dim pnlChk As Panel = e.Item.FindControl("pnlChkActive")
                Dim pnlEditJob As Panel = e.Item.FindControl("pnlEditJob")
                Dim hidExpired As HiddenField = e.Item.FindControl("hidExpired")
                Dim hidActive As HiddenField = e.Item.FindControl("hidActive")
                Dim lblTitle As Label = e.Item.FindControl("lblJobTitle")

                Select Case _currentMode
                    Case UIModes.ShortList
                        btnRemove.Visible = True
                        btnAdd.Visible = False
                        pnlChk.Visible = False
                        pnlEditJob.Visible = False

                    Case UIModes.ShowAll, UIModes.Search
                        ' remove buttons depending on status of job
                        btnRemove.Visible = False
                        If hidActive.Value = 0 Or hidExpired.Value = 1 Then                            
                            btnApply.Visible = False
                            btnAdd.Visible = False                            
                            lblTitle.Enabled = False
                        End If
                        

                        ' only line managers can check jobs active/de-active
                        If JobAdminView Then
                            pnlChk.Visible = True
                            pnlEditJob.Visible = True
                        Else
                            pnlChk.Visible = False
                            pnlEditJob.Visible = False
                        End If

                    Case UIModes.JobDetail
                        Dim btn As Button = e.Item.FindControl("btnBack")
                        Dim imb As ImageButton = e.Item.FindControl("imbExpandCollapse")

                        ' hide various items in the job detail view
                        btn.Visible = True
                        btnRemove.Visible = False
                        btnAdd.Visible = True
                        imb.Visible = False

                        ' show longer description
                        Dim pnlShort As Panel = e.Item.FindControl("pnlDescription")
                        Dim pnlLong As Panel = e.Item.FindControl("pnlLongDescription")
                        pnlShort.Visible = False
                        pnlLong.Visible = True
                        pnlChk.Visible = False

                        If Not JobAdminView Then
                            pnlEditJob.Visible = False
                        End If

                        ' hide apply button in job detail
                        btn = e.Item.FindControl("btnApply")
                        btn.Visible = False
                End Select

            Case ListItemType.Header

                Dim pnl As Panel = e.Item.FindControl("pnlFilterJob")
                If _currentMode = UIModes.ShortList Or _currentMode = UIModes.JobDetail Then
                    ' hide filter if in short list mode
                    pnl.Visible = False
                Else
                    pnl.Visible = True
                End If

                ' show empty template if no items (dont do test on Job Detail as this doesn't
                ' use a paged data source
                If _currentMode <> UIModes.JobDetail Then
                    pnl = e.Item.FindControl("pnlEmptyTemplate")
                    pnl.Visible = IIf(rptJobsListing.DataSource.DataSourceCount = 0, True, False)
                End If

                ' restore filter team selected value & also the location filte
                Dim ddl As DropDownList = e.Item.FindControl("ddlFilterTeam")
                ddl.SelectedValue = FilterTeam

                ' ... remove filter when normal employees - they see only active jobs
                ' and cannot filter active/non-active
                Dim pnlRblDisplayActive As Panel = e.Item.FindControl("pnlRblDisplayActive")

                If Not JobAdminView Then
                    pnlRblDisplayActive.Visible = False
                End If

                ' restore show/hide descriptions selector
                Dim rbl As RadioButtonList = e.Item.FindControl("rblDisplayActive")
                Dim rbiShow As ListItem = rbl.Items(0)
                Dim rbiHide As ListItem = rbl.Items(1)

                rbiShow.Selected = ShowActive
                rbiHide.Selected = Not ShowActive

                ddl.SelectedValue = FilterTeam

                Dim lnkPrev As LinkButton = e.Item.FindControl("lnkPrevious")
                Dim lnkNext As LinkButton = e.Item.FindControl("lnkNext")
                Dim iCurrPage As Integer = CurrentPage
                lnkNext.Visible = Not IsLastPage
                lnkPrev.Visible = Not IsFirstPage

                Dim iFirstPageItem As Integer
                Dim iLastPageItem As Integer

                If NumItems > 0 Then
                    iFirstPageItem = (iCurrPage * pageSize) + 1
                Else
                    iFirstPageItem = 0
                End If

                If NumItems > 0 Then
                    If IsLastPage And (NumItems Mod pageSize) > 0 Then
                        iLastPageItem = iFirstPageItem + (NumItems Mod pageSize) - 1
                    Else
                        iLastPageItem = iFirstPageItem + pageSize - 1
                    End If
                Else
                    iLastPageItem = 0
                End If

                Dim strPageResults = iFirstPageItem.ToString + " - " + iLastPageItem.ToString
                Dim lbl As Label = e.Item.FindControl("lRepeaterPagesRange")
                lbl.Text = strPageResults

                lbl = e.Item.FindControl("lRepeaterPagesTotal")
                lbl.Text = NumItems.ToString

                Dim strPageNums As String = "Page " + (iCurrPage + 1).ToString + " of " + (NumPages).ToString

                lbl = e.Item.FindControl("lCurrentPage")
                lbl.Text = strPageNums


        End Select
    End Sub

    Protected Sub ddlFilterTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = sender
        FilterTeam = ddl.SelectedValue

        ' reset page number
        CurrentPage = 0

        BindJobsData()
    End Sub

    Protected Sub rblDisplayActive_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim rbl As RadioButtonList = sender
        Dim rbi As ListItem
        rbi = rbl.Items.FindByText("Show Active")
        If rbi IsNot Nothing Then
            ShowActive = rbi.Selected
            CurrentPage = 0
        End If

        CurrentPage = 0
        BindJobsData()
    End Sub

    Protected Sub chkActive_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)

        ' job Id is stuffed into Checkbox text field since there is no command args
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim iJobId As Integer = Convert.ToInt32(chk.Text)

        CurrentPage = 0
        Dim bllJobs As New bllJobs
        bllJobs.SetActive(iJobId, chk.Checked)
        BindJobsData()
    End Sub

    Protected Function HideEmptyTemplate(ByVal iCount As Integer) As Boolean
        If iCount = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            mvJobsListing.SetActiveView(vwJobsListing)
        End If

    End Sub

    Protected Sub EnableJobType()
        Dim txt As TextBox = fvEditJob.FindControl("txtOtherJobType")
        Dim lbl As Label = fvEditJob.FindControl("lblOtherType")
        Dim ddl As DropDownList = fvEditJob.FindControl("ddlJobDetailType")
        If ddl.SelectedItem.Text = "Other" Then
            txt.Visible = True
            lbl.Visible = True
        Else
            txt.Text = ""
            txt.Visible = False
            lbl.Visible = False
        End If
    End Sub

    Protected Sub fvEditJob_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles fvEditJob.Init
        'fvEditJob.InsertItemTemplate = fvEditJob.EditItemTemplate
    End Sub

    Protected Sub fvEditJob_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewCommandEventArgs) Handles fvEditJob.ItemCommand
        Select Case e.CommandName
            Case "UpdateJob"
                If validateEditJob() Then
                    UpdateJob(Convert.ToInt32(e.CommandArgument))
                    mvJobsListing.SetActiveView(vwJobsListing)
                    BindJobsData()
                    RaiseEvent ChangedMode(Me, New UiModeEventArgs(UIModes.Search))
                End If

            Case "PostJob"
                If validateEditJob() Then
                    InsertJob(Convert.ToInt32(e.CommandArgument))
                    mvJobsListing.SetActiveView(vwJobsListing)
                    BindJobsData()
                    RaiseEvent ChangedMode(Me, New UiModeEventArgs(UIModes.Search))
                End If

            Case "Cancel"
                mvJobsListing.SetActiveView(vwJobsListing)
                RaiseEvent ChangedMode(Me, New UiModeEventArgs(UIModes.Search))

        End Select
    End Sub

    Protected Sub fvEditJob_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewModeEventArgs) Handles fvEditJob.ModeChanging
        fvEditJob.ChangeMode(e.NewMode)
    End Sub

    Private Sub UpdateJob(ByVal iJobId As Integer)
        Dim bllJobs As New bllJobs(iJobId)

        Dim ddl As DropDownList
        Dim txt As TextBox
        With bllJobs
            ddl = fvEditJob.FindControl("ddlPostTeam")
            .TeamId = ddl.SelectedValue

            txt = fvEditJob.FindControl("txtPostJobDesc")
            .Desc = txt.Text

            ddl = fvEditJob.FindControl("ddlJobDetailType")
            .TypeId = ddl.SelectedValue

            txt = fvEditJob.FindControl("txtOtherJobType")
            .OtherJobType = txt.Text

            txt = fvEditJob.FindControl("txtPostJobTitle")
            .Title = txt.Text

            txt = fvEditJob.FindControl("txtJobDetailReference")
            .Ref = txt.Text

            ddl = fvEditJob.FindControl("ddlPostJobLoc")
            .Location = ddl.SelectedValue

            txt = fvEditJob.FindControl("txtPostJobDuration")
            .Duration = txt.Text

            txt = fvEditJob.FindControl("txtPostJobStart")
            .StartDate = txt.Text

            txt = fvEditJob.FindControl("txtPostJobSalary")
            .Salary = txt.Text

            txt = fvEditJob.FindControl("txtPostJobContactName")
            .ContactName = txt.Text

            txt = fvEditJob.FindControl("txtClosingDate")
            .ClosingDate = Convert.ToDateTime(txt.Text)

            ddl = fvEditJob.FindControl("ddlJobForms")
            .ApplicationId = ddl.SelectedValue
        End With

        Try
            bllJobs.Update()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub InsertJob(ByVal iJobId As Integer)
        Dim bllJobs As New bllJobs()

        Dim ddl As DropDownList
        Dim txt As TextBox
        With bllJobs
            ddl = fvEditJob.FindControl("ddlPostTeam")
            .TeamId = ddl.SelectedValue

            txt = fvEditJob.FindControl("txtPostJobDesc")
            .Desc = txt.Text

            ddl = fvEditJob.FindControl("ddlJobDetailType")
            .TypeId = ddl.SelectedValue

            txt = fvEditJob.FindControl("txtOtherJobType")
            .OtherJobType = txt.Text

            txt = fvEditJob.FindControl("txtPostJobTitle")
            .Title = txt.Text

            ddl = fvEditJob.FindControl("ddlPostJobLoc")
            .Location = ddl.SelectedValue

            txt = fvEditJob.FindControl("txtJobDetailReference")
            .Ref = txt.Text

            txt = fvEditJob.FindControl("txtPostJobDuration")
            .Duration = txt.Text

            txt = fvEditJob.FindControl("txtPostJobStart")
            .StartDate = txt.Text

            txt = fvEditJob.FindControl("txtPostJobSalary")
            .Salary = txt.Text

            txt = fvEditJob.FindControl("txtPostJobContactName")
            .ContactName = txt.Text

            txt = fvEditJob.FindControl("txtClosingDate")
            .ClosingDate = Convert.ToDateTime(txt.Text)

            ddl = fvEditJob.FindControl("ddlJobForms")
            .ApplicationId = ddl.SelectedValue

        End With

        Try
            bllJobs.PostJob()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imbDownloadApplication_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' add session variable with id of application form and redirect to another page
        Dim imb As ImageButton = CType(sender, ImageButton)
        sessionhelper.ViewApplicationID = convert.ToInt32(imb.CommandArgument)

        ' build js script to open new window
        Dim sJScript As New StringBuilder
        sJScript.AppendLine("<script language=""javascript"" type=""text/javascript"">")
        sJScript.AppendLine("window.open('JobAppForm.aspx');")
        sJScript.AppendLine("</script>")
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "jobApp", sJScript.ToString, False)

        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "viewer", sJScript.ToString)
    End Sub

    Protected Function TestExp(ByVal iVal As Integer) As Boolean
        ' use this function to display/not display if Expiry/Active column is 0/1
        If iVal = 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Function TestExpOr(ByVal iVal1 As Integer, ByVal iVal2 As Integer) As Boolean        
        If iVal1 = 1 Or iVal2 = 1 Then
            Return True
        End If
        Return False
    End Function

    Protected Sub ddlJobDetailType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList
        ddl = CType(sender, DropDownList)
        If ddl IsNot Nothing Then
            Dim txt As TextBox = fvEditJob.FindControl("txtOtherJobType")
            Dim lbl As Label = fvEditJob.FindControl("lblOtherType")
            If ddl.SelectedItem.Text = "Other" Then
                txt.Visible = True
                lbl.Visible = True
            Else
                txt.Text = ""
                txt.Visible = False
                lbl.Visible = False
            End If
        End If

    End Sub
End Class
