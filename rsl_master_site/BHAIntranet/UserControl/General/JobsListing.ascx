<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobsListing.ascx.vb"
    Inherits="BHAIntranet_UserControl_General_JobsListing" EnableViewState="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<div id="jobDetailArea">
    <asp:Panel ID="pnlJobsListing" runat="server" Visible="true">
        <asp:MultiView ID="mvJobsListing" runat="server">
            <asp:View ID="vwJobsListing" runat="server">
                <asp:Repeater ID="rptJobsListing" runat="server">
                    <HeaderTemplate>
                        <asp:Panel ID="pnlFilterJob" runat="server" Visible="true">
                            <div id="repeaterFilter">
                                <asp:Panel ID="pnlRblDisplayActive" runat="server" Visible="true">
                                    <asp:RadioButtonList ID="rblDisplayActive" CssClass="repeaterFilterRadios" runat="server"
                                        RepeatDirection="Horizontal" Width="289px" EnableViewState="true" AutoPostBack="true"
                                        OnSelectedIndexChanged="rblDisplayActive_SelectedIndexChanged">
                                        <asp:ListItem Text="Show Active" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Show Non-Active"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </asp:Panel>
                                <asp:Label ID="lblFilterByTeam" CssClass="test" runat="server" Text="Team:"></asp:Label>
                                <asp:ObjectDataSource ID="odsTeams" runat="server" SelectMethod="GetTeamsWithPermissions"
                                    TypeName="bllTeam">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="0" Name="PageID" Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <asp:DropDownList ID="ddlFilterTeam" CssClass="ddlFilterTeam" runat="server" DataSourceID="odsTeams"
                                    DataTextField="TEAMNAME" DataValueField="TEAMID" EnableViewState="true" OnSelectedIndexChanged="ddlFilterTeam_SelectedIndexChanged"
                                    AutoPostBack="true" AppendDataBoundItems="true">
                                    <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lRepeaterPagesRange" runat="server" Font-Bold="true" Text="Label"></asp:Label>
                                <asp:Label ID="lRepeaterPagesRangeOf" runat="server" Text=" of "></asp:Label>
                                <asp:Label ID="lRepeaterPagesTotal" runat="server" Font-Bold="true" Text="Label"></asp:Label>
                                <asp:Label ID="lRepeaterPagesResults" runat="server" Font-Bold="true" Text="results"></asp:Label>
                                <div id="repeaterPagers">
                                    <asp:LinkButton ID="lnkPrevious" runat="server" CommandName="Previous">< Previous</asp:LinkButton>
                                    <asp:Label ID="lCurrentPage" runat="server" Text="Label"></asp:Label>
                                    <asp:LinkButton ID="lnkNext" runat="server" CommandName="Next">Next ></asp:LinkButton>
                                </div>
                            </div>
                            <br />
                            <hr />
                        </asp:Panel>
                        <asp:Panel ID="pnlEmptyTemplate" Visible="false" runat="server">
                            No Jobs to view
                        </asp:Panel>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div id="itemTemplateArea">
                            <div id="repeaterTitleArea">
                                <asp:Label ID="lblJobTitle" runat="server" CssClass="lblJobTitle"
                                    Text='<%# Eval("Title") %>'></asp:Label>
                                <div id="repeaterButtons">
                                    <ajax:ConfirmButtonExtender ID="cbxConfirmAddShortList" runat="server" ConfirmText="Add this job to your shortlist?"
                                        TargetControlID="btnAddToShortList">
                                    </ajax:ConfirmButtonExtender>
                                    <ajax:ConfirmButtonExtender ID="cbxConfirmRemoveShortList" runat="server" ConfirmText="Remove this job from your shortlist?"
                                        TargetControlID="btnRemoveShortList">
                                    </ajax:ConfirmButtonExtender>
                                    <asp:ImageButton ID="btnAddToShortList" CommandArgument='<%# Eval("JobId") %>' CommandName="AddShortList"
                                        ImageUrl="~/BHAIntranet/Images/General/im_addtomyshortlist.gif" runat="server"
                                        CssClass="btnAddtoShortList" />
                                    <asp:ImageButton ID="btnRemoveShortList" CommandArgument='<%# Eval("JobId") %>' CommandName="RemoveShortList"
                                        ImageUrl="~/BHAIntranet/Images/General/im_removefrommyshortlist.gif" runat="server"
                                        CssClass="btnRemoveFromShortList" />
                                    <asp:Button ID="btnPrintPreview" CommandName="PrintDetail" CommandArgument='<%# Eval("JobId") %>'
                                        CssClass="btnPrintApplyBtns" runat="server" Text="Print Version" Visible='<%# not TestExp(Eval("Expired")) %>' />
                                    <asp:Button ID="btnApply" CommandName="Apply" CommandArgument='<%# Eval("JobId") %>'
                                        CssClass="btnPrintApplyBtns" runat="server" Text="Apply for this position" Width="159px"
                                        Visible='<%# not TestExp(Eval("Expired")) %>' />
                                    <asp:Button ID="btnBack" Visible="false" CommandName="Back" CssClass="btnPrintApplyBtns"
                                        runat="server" Text="Back" Width="50px" />
                                    <asp:Panel ID="pnlChkActive" runat="server" Visible="true" CssClass="pnlChkActive">
                                        <asp:CheckBox ID="chkActive" Text='<%# Eval("JobId") %>' Visible='<%# not TestExp(Eval("Expired")) %>'
                                            CssClass="chkTextHide" Checked='<%# Eval("Active") %>' runat="server" OnCheckedChanged="chkActive_CheckChanged"
                                            AutoPostBack="true" />
                                        <asp:Label ID="lblChkActive" Text="Active" runat="server" Visible='<%# not TestExp(Eval("Expired")) %>'></asp:Label>
                                    </asp:Panel>
                                </div>
                            </div>
                            <asp:HiddenField ID="hidActive" runat="server" Value='<%# Eval("Active") %>' />
                            <asp:HiddenField ID="hidExpired" runat="server" Value='<%# Eval("Expired") %>' />
                            <asp:Label ID="lJobDetailSalary" runat="server" CssClass="lbl" Text="Salary:"></asp:Label>
                            <asp:Label ID="lblJobDetailSalary" Text='<%# Eval("Salary") %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lJobDetailLocation" CssClass="lbl" runat="server" Text="Location:"></asp:Label>
                            <asp:Label ID="lblJobDetailLocation" Text='<%# Eval("Location") %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lJobDetailType" CssClass="lbl" runat="server" Text="Job Type:"></asp:Label>
                            <asp:Label ID="lblJobDetailType" Text='<%#getJobType(Eval("JobType"),Eval("OtherType")) %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lJobDetailDatePosted" CssClass="lbl" runat="server" Text="Date Posted:">
                            </asp:Label>
                            <asp:Label ID="lblJobDetailDatePosted" Text='<%# Eval("DatePosted") %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lJobDetailStartDate" CssClass="lbl" runat="server" Text="Start Date:">
                            </asp:Label>
                            <asp:Label ID="lblJobDetailStartDate" Text='<%# Eval("StartDate") %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:Label runat="server" ID="lClosingDate" CssClass="lbl" Text="Closing Date:"></asp:Label>
                            <asp:Label runat="server" CssClass="lblField" ID="lblClosingDate" Text='<%# Eval("ClosingDate","{0:d}") %>'></asp:Label>
                            <asp:Label runat="server" CssClass="lblField" Visible='<%# TestExp(Eval("Expired")) %>'
                                ID="lblExpired" Text="(Expired)" ForeColor="red" Font-Italic="true"></asp:Label>
                            <br />
                            <asp:Label ID="lJobDetailDuration" CssClass="lbl" runat="server" Text="Duration:"></asp:Label>
                            <asp:Label ID="lblJobDetailDuration" Text='<%# Eval("Duration") %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lJobDetailReference" CssClass="lbl" runat="server" Text="Reference:">
                            </asp:Label>
                            <asp:Label ID="lblJobDetailReference" Text='<%# Eval("Ref") %>' CssClass="lblField"
                                runat="server"></asp:Label>
                            <br />
                            <asp:ImageButton ID="imbExpandCollapse" CssClass="btnExpandCollapse" runat="server" />
                            <asp:Panel ID="pnlDescription" runat="server">
                                <asp:Label ID="lblJobDetailDesc" CssClass="lblDescField" Text='<%# Replace(Eval("Description"),vbcrlf,"<br/>") %>'
                                    runat="server"></asp:Label><br />
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnlEditJob" runat="server">
                                <asp:LinkButton ID="lnkEditJob" CommandName="EditJob" Visible='<%# not TestExp(Eval("Expired")) %>'
                                    CommandArgument='<%# Eval("JobId") %>' runat="server" Text="Edit"></asp:LinkButton>
                                <asp:LinkButton ID="lnkCopyJob" CommandName="CopyJob" CommandArgument='<%# Eval("JobId") %>'
                                    runat="server" Text="Copy"></asp:LinkButton>
                            </asp:Panel>
                        </div>
                        <asp:Panel ID="pnlLongDescription" runat="server" Visible="false">
                            <asp:ImageButton ID="imbExpandCollapseLongDesc" CssClass="btnExpandCollapse" runat="server" />
                            <asp:Panel ID="pnlLongDescExpand" runat="server">
                                <asp:Label ID="Label1" CssClass="lblDescField" Text='<%# Replace(Eval("Description"),vbcrlf,"<br/>") %>'
                                    runat="server"></asp:Label><br />
                                <br />
                            </asp:Panel>
                            <hr />
                            <asp:Panel ID="pnlApplyArea" runat="server">
                                <div id="applyArea">
                                    <h1>
                                        Applying for this position:</h1>
                                    Follow these simple steps:
                                    <asp:ImageButton ID="imbDownloadApplication" CommandArgument='<%# Eval("ApplicationId") %>'
                                        CssClass="hlkDownloadApp" ImageUrl="~/BHAIntranet/Images/Defaults/doc_connect.gif"
                                        runat="server" OnClick="imbDownloadApplication_Click" />
                                    <ul>
                                        <li>- Download and 'Save' the Employment Application form by clicking on the icon to
                                            the left</li>
                                        <li>- Complete and save the Employment Application form</li>
                                        <li>- Email the completed form to <a href="mailto:recruitment@broadlandhousing.org">
                                            recruitment@broadlandhousing.org</a></li>
                                    </ul>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                        <ajax:CollapsiblePanelExtender ID="cpxDescription" TargetControlID="pnlDescription"
                            runat="server" Collapsed="true" ExpandControlID="imbExpandCollapse" CollapseControlID="imbExpandCollapse"
                            ExpandedImage="../../Images/General/Forms/collapse_desc.gif" CollapsedImage="../../Images/General/Forms/expand_desc.gif"
                            ImageControlID="imbExpandCollapse">
                        </ajax:CollapsiblePanelExtender>
                        <ajax:CollapsiblePanelExtender ID="cpxLongDescription" TargetControlID="pnlLongDescExpand"
                            runat="server" Collapsed="false" ExpandControlID="imbExpandCollapseLongDesc"
                            CollapseControlID="imbExpandCollapseLongDesc" ExpandedImage="../../Images/General/Forms/collapse_desc.gif"
                            CollapsedImage="../../Images/General/Forms/expand_desc.gif" ImageControlID="imbExpandCollapseLongDesc">
                        </ajax:CollapsiblePanelExtender>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:View>
            <asp:View ID="vwEditJob" runat="server">
                <asp:Label runat="server" ID="lblPostJobValidation" ForeColor="Red"></asp:Label>
                <br />
                <asp:ObjectDataSource ID="odsTeams" runat="server" SelectMethod="GetTeamsWithPermissions"
                    TypeName="bllTeam" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="0" Name="PageID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsJobTypes" runat="server" SelectMethod="GetJobTypes"
                    TypeName="bllJobs"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsJobLocations" runat="server" SelectMethod="GetJobTypes"
                    TypeName="bllJobs"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsJobForms" runat="server" SelectMethod="GetData" TypeName="bllJobForms">
                </asp:ObjectDataSource>
                <asp:FormView ID="fvEditJob" runat="server">
                    <EditItemTemplate>
                        <div id="postJobArea">
                            <asp:Label runat="server" ID="lPostTeam" AssociatedControlID="ddlPostTeam" CssClass="lbl"
                                Text="Team:" />
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlPostTeam" AppendDataBoundItems="True"
                                DataSourceID="odsTeams" DataTextField="TEAMNAME" DataValueField="TEAMID" SelectedValue='<%# Eval("TeamId") %>'>
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lDesc" CssClass="lbl" runat="server" Text="Description:">
                            </asp:Label>
                            <asp:TextBox runat="server" ID="txtPostJobDesc" MaxLength="2500" TextMode="MultiLine"
                                Rows="6" CssClass="lblLargeField" Text='<%# Eval("Description") %>'></asp:TextBox>
                            <br />
                            <asp:Label ID="lJobDetailType" CssClass="lbl" runat="server" Text="Job Type:"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlJobDetailType" AutoPostBack="true" AppendDataBoundItems="True"
                                DataSourceID="odsJobTypes" DataTextField="JobType" DataValueField="JobTypeId"
                                SelectedValue='<%# Eval("TypeId") %>' OnSelectedIndexChanged="ddlJobDetailType_SelectedIndexChanged">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblOtherType" runat="server" AssociatedControlID="txtPostJobTitle"
                                CssClass="lbl" Text="Other Type:"></asp:Label><asp:TextBox ID="txtOtherJobType" runat="server"
                                    CssClass="lblField" MaxLength="50" Text='<%# Eval("OtherType") %>'></asp:TextBox><br />
                            <asp:Label runat="server" ID="lPostJobTitle" AssociatedControlID="txtPostJobTitle"
                                CssClass="lbl" Text="Job Title:"></asp:Label>
                            <asp:TextBox runat="server" CssClass="lblField" ID="txtPostJobTitle" Text='<%# Eval("Title") %>'
                                MaxLength="200"></asp:TextBox>
                            <br />
                            <asp:Label ID="lJobDetailReference" CssClass="lbl" runat="server" Text="Reference:">
                            </asp:Label>
                            <asp:TextBox ID="txtJobDetailReference" Text='<%# Eval("Ref") %>' MaxLength="30" CssClass="lblField"
                                runat="server" />
                            <br />
                            <asp:Label runat="server" ID="lPostJobLoc" AssociatedControlID="ddlPostJobLoc" CssClass="lbl"
                                Text="Job Location:"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlPostJobLoc" AppendDataBoundItems="True"
                                DataSourceID="sqlGetOfficeLocs" SelectedValue='<%# Eval("LocationId") %>' DataTextField="DESCRIPTION"
                                DataValueField="OFFICEID">
                            </asp:DropDownList><asp:SqlDataSource ID="sqlGetOfficeLocs" runat="server" ConnectionString="<%$ ConnectionStrings:RSL Manager Broadlands DevConnectionString %>"
                                SelectCommand="SELECT * FROM [G_OFFICE]"></asp:SqlDataSource>
                            <br />
                            <asp:Label ID="lJobDetailDuration" CssClass="lbl" runat="server" Text="Duration:"></asp:Label>
                            <asp:TextBox ID="txtPostJobDuration" Text='<%# Eval("Duration") %>' CssClass="lblField"
                                runat="server" MaxLength="30" />
                            <br />
                            <asp:Label ID="lJobDetailStartDate" CssClass="lbl" runat="server" Text="Start Date:">
                            </asp:Label>
                            <asp:TextBox ID="txtPostJobStart" Text='<%# Eval("StartDate") %>' CssClass="lblField"
                                runat="server" MaxLength="30" /><br />
                            <ajax:CalendarExtender ID="calCD" runat="server" PopupButtonID="imbCal" TargetControlID="txtClosingDate" PopupPosition="TopLeft" Format="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:Label runat="server" ID="lClosingDate" AssociatedControlID="txtClosingDate"
                                CssClass="lbl" Text="Closing Date:"></asp:Label>
                            <asp:TextBox runat="server" CssClass="lblField" ID="txtClosingDate" Text='<%# Eval("ClosingDate","{0:d}") %>' MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="imbCal" runat="server" ImageUrl="~/myImages/cal.gif" />
                            <br />
                            <asp:Label ID="lJobDetailSalary" runat="server" CssClass="lbl" Text="Salary:"></asp:Label>
                            <asp:TextBox ID="txtPostJobSalary" Text='<%# Eval("Salary") %>' CssClass="lblField"
                                runat="server" MaxLength="30" />
                            <br />
                            <asp:Label runat="server" ID="lPostContactName" AssociatedControlID="txtPostJobContactName"
                                CssClass="lbl" Text="Contact Name:"></asp:Label>
                            <asp:TextBox runat="server" CssClass="lblField" MaxLength="30" ID="txtPostJobContactName" Text='<%# Eval("ContactName") %>'></asp:TextBox>
                            <br />
                            <asp:Label runat="server" ID="lJobAppForm" AssociatedControlID="ddlJobForms" CssClass="lbl"
                                Text="Application Type:"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlJobForms" AppendDataBoundItems="true"
                                DataSourceID="odsJobForms" DataTextField="Name" DataValueField="ApplicationId"
                                SelectedValue='<%# Eval("ApplicationId") %>'>
                            </asp:DropDownList>
                        </div>
                        <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="UpdateJob" CommandArgument='<%# Eval("JobId") %>'>Update</asp:LinkButton>
                        <asp:LinkButton ID="lnkCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <div id="postJobArea">
                            <asp:Label runat="server" ID="lPostTeam" AssociatedControlID="ddlPostTeam" CssClass="lbl"
                                Text="Team:" />
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlPostTeam" AppendDataBoundItems="True"
                                DataSourceID="odsTeams" DataTextField="TEAMNAME" DataValueField="TEAMID">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lDesc" CssClass="lbl" runat="server" Text="Description:">
                            </asp:Label>
                            <asp:TextBox runat="server" ID="txtPostJobDesc" MaxLength="2500" TextMode="MultiLine"
                                Rows="6" CssClass="lblLargeField" Text='<%# Bind("Description") %>'></asp:TextBox>
                            <br />
                            <asp:Label ID="lJobDetailType" CssClass="lbl" runat="server" Text="Job Type:"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlJobDetailType" AutoPostBack="true" AppendDataBoundItems="True"
                                DataSourceID="odsJobTypes" DataTextField="JobType" DataValueField="JobTypeId" OnSelectedIndexChanged="ddlJobDetailType_SelectedIndexChanged">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblOtherType" runat="server" AssociatedControlID="txtPostJobTitle"
                                CssClass="lbl" Text="Other Type:"></asp:Label><asp:TextBox ID="txtOtherJobType" runat="server"
                                    CssClass="lblField" MaxLength="50" Text='<%# Eval("OtherType") %>'></asp:TextBox><br />
                            <asp:Label runat="server" ID="lPostJobTitle" AssociatedControlID="txtPostJobTitle"
                                CssClass="lbl" Text="Job Title:"></asp:Label>
                            <asp:TextBox runat="server" CssClass="lblField" ID="txtPostJobTitle" Text='<%# Bind("Title") %>'
                                MaxLength="200"></asp:TextBox>
                            <br />
                            <asp:Label ID="lJobDetailReference" CssClass="lbl" runat="server" Text="Reference:">
                            </asp:Label>
                            <asp:TextBox ID="txtJobDetailReference" MaxLength="30" Text='<%# Bind("Ref") %>' CssClass="lblField"
                                runat="server" />
                            <br />
                            <asp:Label runat="server" ID="lPostJobLoc" AssociatedControlID="ddlPostJobLoc" CssClass="lbl"
                                Text="Job Location:"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlPostJobLoc" AppendDataBoundItems="True"
                                DataSourceID="sqlGetOfficeLocs" DataTextField="DESCRIPTION" DataValueField="OFFICEID">
                            </asp:DropDownList><asp:SqlDataSource ID="sqlGetOfficeLocs" runat="server" ConnectionString="<%$ ConnectionStrings:RSL Manager Broadlands DevConnectionString %>"
                                SelectCommand="SELECT * FROM [G_OFFICE]"></asp:SqlDataSource>
                            <br />
                            <asp:Label ID="lJobDetailDuration" CssClass="lbl" runat="server" Text="Duration:"></asp:Label>
                            <asp:TextBox ID="txtPostJobDuration" MaxLength="30" Text='<%# Bind("Duration") %>' CssClass="lblField"
                                runat="server" />
                            <br />
                            <asp:Label ID="lJobDetailStartDate" CssClass="lbl" runat="server" Text="Start Date:">
                            </asp:Label>
                            <asp:TextBox ID="txtPostJobStart" MaxLength="30" Text='<%# Bind("StartDate") %>' CssClass="lblField"
                                runat="server" />
                            <br />
                            <ajax:CalendarExtender ID="calCD" runat="server" PopupButtonID="imbCal" TargetControlID="txtClosingDate" PopupPosition="TopLeft" Format="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:Label runat="server" ID="lClosingDate" AssociatedControlID="txtClosingDate"
                                CssClass="lbl" Text="Closing Date:"></asp:Label>
                            <asp:TextBox runat="server" CssClass="lblField" MaxLength="10" ID="txtClosingDate"></asp:TextBox>
                            <asp:ImageButton ID="imbCal" runat="server" ImageUrl="~/myImages/cal.gif" /><br />
                            <asp:Label ID="lJobDetailSalary" runat="server" CssClass="lbl" Text="Salary:"></asp:Label>
                            <asp:TextBox ID="txtPostJobSalary" Text='<%# Bind("Salary") %>' MaxLength="30" CssClass="lblField"
                                runat="server" />
                            <br />
                            <asp:Label runat="server" ID="lPostContactName" AssociatedControlID="txtPostJobContactName"
                                CssClass="lbl" Text="Contact Name:"></asp:Label>
                            <asp:TextBox runat="server" CssClass="lblField" MaxLength="30" ID="txtPostJobContactName" Text='<%# Bind("ContactName") %>'></asp:TextBox>
                            <br />
                            <asp:Label runat="server" ID="lJobAppForm" AssociatedControlID="ddlJobForms" CssClass="lbl"
                                Text="Application Type:"></asp:Label>
                            <asp:DropDownList runat="server" CssClass="lblField" ID="ddlJobForms" AppendDataBoundItems="true"
                                DataSourceID="odsJobForms" DataTextField="Name" DataValueField="ApplicationId">
                            </asp:DropDownList>
                        </div>
                        <asp:LinkButton ID="lnkPostJob" runat="server" CommandName="PostJob" CommandArgument='<%# Eval("JobId") %>'>Post Job</asp:LinkButton>
                        <asp:LinkButton ID="lnkCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                    </InsertItemTemplate>
                </asp:FormView>
            </asp:View>
        </asp:MultiView>
    </asp:Panel>
</div>
