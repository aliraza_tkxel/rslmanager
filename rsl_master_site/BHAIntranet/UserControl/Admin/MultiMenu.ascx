<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MultiMenu.ascx.vb" Inherits="BHAIntranet_Admin_UserControls_MultiMenu" %>
<%@ Register TagPrefix="myControls" Namespace="CustomControls" %>
<div class="multimenu">
<asp:Label ID="lblWebsiteName" runat="server" Text="">Test</asp:Label>
<myControls:MenuTreeView ID="tvwMultiMenu" runat="server"  ImageSet="XPFileExplorer" NodeIndent="25"  LeafNodeStyle-ChildNodesPadding="10" ShowLines="True" /> 
</div>