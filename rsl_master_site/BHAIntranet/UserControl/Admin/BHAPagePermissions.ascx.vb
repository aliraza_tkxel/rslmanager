
Partial Class BHAIntranet_Admin_UserControls_BHAPagePermissions
    Inherits System.Web.UI.UserControl
    Private m_SelectedTeams As String
    Private m_SelectedEmployees As String
    Private m_ArrSelectedTeams() As String
    Private m_ArrSelectedEmps() As String
    Private m_PageID As Integer

    Public ReadOnly Property SelectedTeams() As Array
        Get
            Return m_ArrSelectedTeams
        End Get

    End Property

    Public ReadOnly Property SelectedEmployees() As Array
        Get
            Return m_ArrSelectedEmps
        End Get
    End Property

    Public Property PageID() As Integer
        Get
            Return m_PageID
        End Get
        Set(ByVal value As Integer)
            m_PageID = value
        End Set
    End Property

    Public Function GetSelectedEmployees() As Array
        Return m_ArrSelectedEmps
    End Function

    Public Function GetSelectedTeams() As Array
        Return m_ArrSelectedTeams
    End Function

    Public Sub SelectAllTeams(ByVal bSelectAll As Boolean)
        Dim chk As CheckBox

        For Each gvwRow As GridViewRow In gvwTeams.Rows
            chk = gvwRow.FindControl("chkTeam")
            If chk IsNot Nothing Then
                chk.Checked = bSelectAll
                CheckAllMembersOfTeam(chk, gvwRow, True)
            End If
        Next
    End Sub

    Private Sub CheckAllMembersOfTeam(ByVal chkTeamCheck As CheckBox, Optional ByVal gvrRow As GridViewRow = Nothing, Optional ByVal bExpandNodes As Boolean = False)
        Dim chkTeam As CheckBox = CType(chkTeamCheck, CheckBox)
        Dim blnChecked As Boolean

        If chkTeam.Checked = True Then
            blnChecked = True
        Else
            blnChecked = False
        End If


        Dim item As RepeaterItem
        Dim rpt As Repeater
        If gvrRow Is Nothing Then
            rpt = DirectCast(gvwTeams.Rows(gvwTeams.SelectedIndex).FindControl("rptEmp"), Repeater)
        Else
            rpt = DirectCast(gvrRow.FindControl("rptEmp"), Repeater)
        End If
        If bExpandNodes Then
            Dim pnlTeam As Panel = DirectCast(gvrRow.FindControl("pnlTeam"), Panel)
            'pnlTeam.CssClass = "show"
        End If        

        For Each item In rpt.Items
            Dim chkEmp As CustomControls.CheckBoxWithValue = item.FindControl("chkEmpPage")
            chkEmp.Checked = blnChecked
            'chkEmp.Enabled = blnChecked
        Next
    End Sub

    Protected Sub chkTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckAllMembersOfTeam(sender)
    End Sub

    Protected Sub gvwTeams_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwTeams.RowCommand

        Dim gv As GridView = DirectCast(sender, GridView)
        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
        Dim rowIndex As Integer = row.RowIndex
        Dim pnlTeamSelected As Panel = DirectCast(gv.Rows(rowIndex).FindControl("pnlTeam"), Panel)
        Dim strCSS As String = pnlTeamSelected.CssClass

        Dim dr As GridViewRow

        For Each dr In gv.Rows
            Dim pnlTeam As Panel = DirectCast(dr.FindControl("pnlTeam"), Panel)
            pnlTeam.CssClass = "hide"
        Next


        If e.CommandName = "Select" Then

            If strCSS = "show" Then
                pnlTeamSelected.CssClass = "hide"
            Else
                pnlTeamSelected.CssClass = "show"
            End If

        End If
    End Sub

    Public Sub GetSelected()
        Dim dr As GridViewRow
        Dim chk As CheckBox

        Dim lbl As Label

        For Each dr In gvwTeams.Rows

            chk = CType(dr.FindControl("chkTeam"), CheckBox)
            lbl = CType(dr.FindControl("lblTeamID"), Label)


            Dim item As RepeaterItem
            Dim rpt As Repeater = DirectCast(dr.FindControl("rptEmp"), Repeater)

            For Each item In rpt.Items

                Dim chkEmp As CustomControls.CheckBoxWithValue = item.FindControl("chkEmpPage")
                If chkEmp.Checked = True Then
                    m_SelectedEmployees = m_SelectedEmployees & chkEmp.Value & ","
                End If

            Next


            If chk.Checked Then
                m_SelectedTeams = m_SelectedTeams & lbl.Text & ","
            End If
        Next

        If Not String.IsNullOrEmpty(m_SelectedTeams) Then
            m_SelectedTeams = m_SelectedTeams.TrimEnd(",")
            m_ArrSelectedTeams = m_SelectedTeams.Split(",")
        Else

        End If
        If Not String.IsNullOrEmpty(m_SelectedEmployees) Then
            m_SelectedEmployees = m_SelectedEmployees.TrimEnd(",")
            m_ArrSelectedEmps = m_SelectedEmployees.Split(",")
        Else

        End If


    End Sub

    Public Sub PopulatePermissions(ByVal PageID As Integer)
        gvwTeams.DataBind()

        Dim dr As GridViewRow
        Dim chk As CheckBox

        For Each dr In gvwTeams.Rows

            chk = CType(dr.FindControl("chkTeam"), CheckBox)
            If chk.Checked = False Then


                Dim item As RepeaterItem
                Dim rpt As Repeater = DirectCast(dr.FindControl("rptEmp"), Repeater)

                For Each item In rpt.Items

                    Dim chkEmp As CustomControls.CheckBoxWithValue = item.FindControl("chkEmpPage")
                    'chkEmp.Enabled = False

                Next
            End If


        Next


    End Sub


    Protected Sub odsTeams_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsTeams.Selecting
        e.InputParameters("pageid") = m_PageID
    End Sub

    Public Function CurrentPageID() As Integer
        Return m_PageID
    End Function

    Public Function EmpChecked(ByVal intHasPermisson As Integer) As Boolean

        If intHasPermisson > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Protected Sub btnSelectAllTeams_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectAllTeams.Click
        SelectAllTeams(True)        
    End Sub

    Protected Sub btnDeSelectAllTeams_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeSelectAllTeams.Click
        SelectAllTeams(False)
    End Sub
End Class
