<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BHAPagePermissions.ascx.vb"
    Inherits="BHAIntranet_Admin_UserControls_BHAPagePermissions" %>
<%@ Register TagPrefix="myControls" Namespace="CustomControls" %>
<ajax:UpdateProgress ID="upgWait" runat="server" AssociatedUpdatePanelID="upTeamTree">
    <ProgressTemplate>
        Please wait...<br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/BHAIntranet/Images/General/loader.gif" />
    </ProgressTemplate>
</ajax:UpdateProgress>
<ajax:UpdatePanel ID="upTeamTree" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblTeams" runat="server" Text="Teams:" CssClass="" />
        <asp:Button ID="btnSelectAllTeams" runat="server" Text="Select All" CssClass="btn" /><asp:Button
            ID="btnDeSelectAllTeams" runat="server" Text="Deselect All" CssClass="btn" /><br />
        <br />
        <asp:ObjectDataSource ID="odsTeams" runat="server" SelectMethod="GetTeamsWithPermissions"
            TypeName="bllTeam" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
        <asp:GridView ShowHeader="false" ID="gvwTeams" runat="server" AutoGenerateColumns="False"
            DataKeyNames="TEAMID" DataSourceID="odsTeams">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="imgbtnSelect" ImageUrl="~/BHAIntranet/Images/General/Forms/treeview.gif"
                            CommandName="Select" runat="server" />
                        <asp:Label ID="lblTeamName" runat="server" Text='<%# Bind("TEAMNAME") %>'></asp:Label>
                        <asp:Label ID="lblTeamPermission" Visible="false" runat="server" Text='<%# Bind("HASPERMISSION") %>'></asp:Label>
                        <asp:Panel ID="pnlTeam" CssClass="hide" runat="server">
                            <asp:CheckBox ID="chkTeam" AutoPostBack="true" Checked='<%# EmpChecked(DataBinder.Eval(Container.DataItem, "HASPERMISSION"))%>'
                                runat="server" OnCheckedChanged="chkTeam_CheckedChanged" />
                            <asp:Label ID="lblSelectAll" runat="server" Text="Select All"></asp:Label>
                            <br />
                            <asp:Label ID="lblTeamID" Visible="false" runat="server" Text='<%# Bind("TeamID") %>'></asp:Label>
                            <asp:Label ID="lblPageID" Visible="false" runat="server" Text='<%# CurrentPageID() %>'></asp:Label>
                            <asp:ObjectDataSource ID="odsEmps" runat="server" SelectMethod="GetEmployeesByTeamIDAndPageID"
                                TypeName="bllEmployees" OldValuesParameterFormatString="original_{0}">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lblTeamID" Name="TeamID" PropertyName="Text" Type="Int32" />
                                    <asp:ControlParameter ControlID="lblPageID" Name="PageID" PropertyName="Text" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Repeater ID="rptEmp" runat="server" DataSourceID="odsEmps">
                                <ItemTemplate>
                                    <myControls:CheckBoxWithValue ID="chkEmpPage" Value='<%#DataBinder.Eval(Container.DataItem, "EMPLOYEEID")%>'
                                        runat="server" Checked='<%# EmpChecked(DataBinder.Eval(Container.DataItem, "HASPERMISSION"))%>' />
                                    <asp:Label ID="lblEmpName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FULLNAME")%>'></asp:Label><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
</ajax:UpdatePanel>
