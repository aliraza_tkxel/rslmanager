Imports System.Drawing.Color
Imports System.Web.Mail

Partial Class BHAIntranet_UserControl_Whiteboard_Suggestions
    'Inherits System.Web.UI.UserControl
    Inherits baseusercontrol

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click

        Try


            Dim bllSugg As New bllSuggestions
            bllSugg.UserID = ASPSession("UserID")
            bllSugg.Suggestion = txtSugg.Text
            bllSugg.Topic = txtTopic.Text

            ' add to database
            bllSugg.Create()

            ' send notification email
            SendSuggestionToLineManager()

            lblValidation.ForeColor = Green
            lblValidation.Text = "Your Suggestion has been sent"
            formhelper.ClearForm(pnlSugg)
        Catch ex As Exception
            lblValidation.ForeColor = Red
            lblValidation.Text = ex.Message
        End Try

    End Sub

    Private Sub SendSuggestionToLineManager()
        Dim bllEmp As New bllEmployees(ASPSession("USERID"))
        Dim sTo As String
        Dim sFrom As String
        Dim sFromName As String
        Dim sSubject As String
        Dim sBody As New StringBuilder

        If bllEmp IsNot Nothing Then

            If bllEmp.Email <> "N/A" Then

                sFrom = bllEmp.Email
                sFromName = bllEmp.FullName
                sTo = "adnan.mirza@broadlandhousing.org"

                sSubject = "Suggestion Notification"

                sBody.Append("You have received a Little Acorns suggestion:")
                sBody.AppendLine()
                sBody.AppendLine("From: " + sFromName)
                sBody.AppendLine("Topic: " + txtTopic.Text)
                sBody.AppendLine("Suggestion: " + txtSugg.Text)
                sBody.AppendLine()
                sBody.AppendLine("Please action this suggestion through the suggestions report.")
                sBody.AppendLine()

                Try
                    Dim mmSuggestNotification As New MailMessage
                    mmSuggestNotification.From = sFrom
                    mmSuggestNotification.To = sTo
                    mmSuggestNotification.Subject = sSubject
                    mmSuggestNotification.Body = sBody.ToString

                    SmtpMail.SmtpServer = "smtp.office365.com"
                    'mailMsg.From = New MailAddress("noreply@broadlandgroup.org")                   
                    SmtpMail.Send(mmSuggestNotification)

                Catch ex As Exception
                    ErrorHelper.LogError(ex, True)
                End Try
            End If
        End If        
    End Sub

End Class
