
Partial Class BHAIntranet_UserControl_Whiteboard_MandReads
    Inherits baseusercontrol

    Private Const MandReadSessionVar As String = "mandReadID_"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim bllMand As New bllMandatoryReads
            gvwMand.DataSource = bllMand.GetTopLatestByTeam(ASPSession("USERID"))
            gvwMand.DataBind()
            lnkMand.Text = "View all Latest Documents"
        End If

    End Sub

    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dd-MMM-yyyy")
        End If

    End Function

    Public Function DocType(ByVal strFileName As Object) As String
        If IsDBNull(strFileName) Then
            Return "N/A"
        Else
            Return filehelper.GetDocImage(strFileName)
        End If

    End Function

    Protected Sub lnkMand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMand.Click
        Response.Redirect("/BHAIntranet/CustomPages/LatestDocuments.aspx")
    End Sub

    Protected Sub ReadLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton
        lnk = CType(sender, LinkButton)

        If lnk IsNot Nothing Then
            ' set the read session var
            Dim sUrl As String = lnk.CommandArgument
            Dim sAddVar As String = MandReadSessionVar & sUrl
            Session.Add(sAddVar, 1)

            ' open the document
            Dim strPath As String = Server.MapPath(sUrl)
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(strPath)

            If file.Exists Then
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                Response.AddHeader("Content-Length", file.Length.ToString())
                Response.ContentType = "application/doc"
                Response.WriteFile(file.FullName)
                Response.End()
                Response.Flush()
            End If
        End If
    End Sub
End Class
