
Partial Class BHAIntranet_UserControl_Whiteboard_MandReads
    Inherits baseusercontrol

    Private Const MandReadSessionVar As String = "mandReadID_"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim bllMand As New bllMandatoryReads
            gvwMand.DataSource = bllMand.GetTopLatestByTeam(ASPSession("USERID"))
            gvwMand.DataBind()
            lnkMand.Text = "View all Latest Documents"
        End If

    End Sub

    Public Function FormatDate(ByVal strDate As Object) As String
        If IsDBNull(strDate) Then
            Return "N/A"
        Else
            Dim dt As DateTime = strDate
            Return dt.ToString("dd-MMM-yyyy")
        End If

    End Function

    Public Function DocType(ByVal strFileName As Object) As String
        If IsDBNull(strFileName) Then
            Return "N/A"
        Else
            Return filehelper.GetDocImage(strFileName)
        End If

    End Function

    Protected Sub lnkMand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMand.Click
        Response.Redirect("/BHAIntranet/CustomPages/LatestDocuments.aspx")
    End Sub
    Public Function GetFileSize(ByVal aDocumentId As Integer) As String
        Dim FileSize As String = "-"
        Dim FileSizeInKB As Long
        Dim taDocs As New dalDocumentManagerTableAdapters.DOC_DOCUMENTTableAdapter

        If (taDocs.GetData(aDocumentId).Rows.Count > 0) Then
            Dim docRow As dalDocumentManager.DOC_DOCUMENTRow = taDocs.GetData(aDocumentId).Rows.Item(0)
            If (docRow.DOCUMENTFILE.Length <> 0) Then
                FileSizeInKB = docRow.DOCUMENTFILE.Length / 1024
                FileSize = FileSizeInKB.ToString() + "kb"
            Else
                FileSize = "0 kb"
            End If
            
        End If
        Return FileSize
    End Function

    Protected Sub ReadLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton
        lnk = CType(sender, LinkButton)

        If lnk IsNot Nothing Then
            ' set the read session var
            Dim sUrl As String = lnk.CommandArgument
            Dim sAddVar As String = MandReadSessionVar & sUrl
            Session.Add(sAddVar, 1)
            Dim taDocs As New dalDocumentManagerTableAdapters.DOC_DOCUMENTTableAdapter
            Dim docTab As dalDocumentManager.DOC_DOCUMENTDataTable = taDocs.GetData(Integer.Parse(lnk.CommandArgument))
            Dim docRow As dalDocumentManager.DOC_DOCUMENTRow
            If docTab.Rows.Count > 0 Then
                docRow = docTab(0)
            Else

            End If

            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & docRow.DOCFILE)
            Response.AddHeader("Content-Length", docRow.DOCUMENTFILE.Length)
            Response.ContentType = "application/doc"
            Response.BinaryWrite(docRow.DOCUMENTFILE)
            Response.End()
            Response.Flush()
            '' open the document
            'Dim strPath As String = Server.MapPath(sUrl)
            'Dim file As System.IO.FileInfo = New System.IO.FileInfo(strPath)

            'If file.Exists Then
            '    Response.Clear()
            '    Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            '    Response.AddHeader("Content-Length", file.Length.ToString())
            '    Response.ContentType = "application/doc"
            '    Response.WriteFile(file.FullName)
            '    Response.End()
            '    Response.Flush()
            'End If
        End If
    End Sub
End Class
