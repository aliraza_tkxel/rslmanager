<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Alerts.ascx.vb" Inherits="BHAIntranet_UserControl_Whiteboard_SoftwareConnect" %>
 
<asp:GridView ID="gvAlerts" runat="server" AutoGenerateColumns="False" DataKeyNames="AlertID"
    ShowHeader="false" Width="100%" BorderStyle="none" BorderWidth="0px" >
    <Columns>
        <asp:TemplateField ItemStyle-Width="100%" HeaderText="AlertName" ShowHeader="False"
            SortExpression="AlertName">
            <ItemTemplate>
                <asp:Label ID="lblAlertNameText"  CssClass="alertName" runat="server" Text='<%# Bind("AlertName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField  ShowHeader="False">
            <ItemTemplate>
                <div class="alertCounts">
                    <asp:HyperLink ID="hlkAlertCount" Width="40px" runat="server" CssClass="alertCountText" NavigateUrl='<%# AlertUrl(Eval("AlertUrl")) %>'
                        Text='<%# AlertCountQuery(DataBinder.Eval(Container.DataItem, "AlertID")) %>'></asp:HyperLink>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        <div class="alertName">
            No alerts currently set-up.</div>
    </EmptyDataTemplate>
</asp:GridView>
<asp:Label ID="Label1" runat="server"></asp:Label>
     