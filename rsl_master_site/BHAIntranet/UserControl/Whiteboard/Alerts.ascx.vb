
Imports System.Data.SqlClient

Partial Class BHAIntranet_UserControl_Whiteboard_SoftwareConnect
    Inherits baseusercontrol

#Region "Properties"

    ''' <summary>
    ''' Enum to select count function
    ''' SEE MyWeb/include/alerts.asp for original code for alerts rules... **
    ''' keep this list uptodate with E_ALERTS table
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum eAlertIDs

        ReceivedInvoices = 1
        LeaveRequest = 2
        Absentees = 3
        QueuedPurchases = 4
        GeneralEnquiries = 5
        RentEnquiries = 6
        TransferExchange = 7
        AdaptationEnquiries = 8
        ServiceComplaints = 9
        RepairsAwaitingApproval = 10
        TenantsOnlineEnquiries = 11
        VulnerabilityReviews = 12
        TenantsOnlineResponses = 13
        PreInspection = 14
        PostInspection = 15
        RiskReviews = 16
        AsbComplaints = 17
        TenancySupportReferral = 18
        ConditionWorksApprovalRequired = 19
        ApprovedConditionWorks = 20
        GoodsReceivedAwaitingApproval = 21
        GoodsReceived = 22
        QueuedRepairs = 23
        AvailableProperties = 24
        VoidInspections = 25
        BritishGasNotification = 26
        FaultRejected = 27
        ASBOpenCases = 28
        DefectRequiringApproval = 29
        DeclarationOfInterest = 30
        TrainingApprovalMyStaff = 31
        TrainingApprovalHR = 32
	PendingReviewDeclarationOfInterest = 33
        PayPointReview = 34
        TrainingApprovalExec = 36
    End Enum

#End Region

#Region "Events Handling"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim bllMyAlerts As New bllAlerts
            gvAlerts.DataSource = bllMyAlerts.GetMyAlerts(ASPSession("USERID"))



            gvAlerts.DataBind()
           
        End If
    End Sub

#End Region

#Region "Functions"

    Protected Function AlertCountQuery(ByVal iAlert As Integer) As String

        AlertCountQuery = "?"
        Select Case iAlert
            Case eAlertIDs.AvailableProperties
                AlertCountQuery = AvailableProperties().ToString
            Case eAlertIDs.VoidInspections
                AlertCountQuery = VoidInspections().ToString
            Case eAlertIDs.BritishGasNotification
                AlertCountQuery = BritishGasNotification().ToString
            Case eAlertIDs.Absentees
                AlertCountQuery = ExecAbsentees().ToString

            Case eAlertIDs.AdaptationEnquiries
                AlertCountQuery = ExecAdaptationEnquiries().ToString()

            Case eAlertIDs.GeneralEnquiries
                AlertCountQuery = ExecGeneralEnquiries().ToString()

            Case eAlertIDs.LeaveRequest
                AlertCountQuery = ExecAbsenceLeaveRequest().ToString()

            Case eAlertIDs.QueuedPurchases
                AlertCountQuery = ExecQueuedPurchases().ToString

            Case eAlertIDs.ReceivedInvoices
                'AlertCountQuery = (ExecReceivedInvoices() + ExecPOReceivedInvoices()).ToString
                AlertCountQuery = ExecPOReceivedInvoices().ToString

            Case eAlertIDs.RentEnquiries
                AlertCountQuery = ExecRentEnquiries().ToString

            Case eAlertIDs.ServiceComplaints
                Dim iresult As Integer
                iresult = ExecServiceComplaints(False)
                AlertCountQuery = iresult.ToString

                iresult = ExecServiceComplaints(True)
                AlertCountQuery += " (" + iresult.ToString + ")"

            Case eAlertIDs.AsbComplaints
                Dim iresult As Integer
                iresult = Asb(False)
                AlertCountQuery = iresult.ToString

                iresult = Asb(True)
                AlertCountQuery += " (" + iresult.ToString + ")"

            Case eAlertIDs.TransferExchange
                AlertCountQuery = ExecTransferExchange().ToString

            Case eAlertIDs.RepairsAwaitingApproval
                AlertCountQuery = ExecRepairsAwaitingApproval().ToString

            Case eAlertIDs.TenantsOnlineEnquiries
                AlertCountQuery = TenantsOnlineEnquiries().ToString

            Case eAlertIDs.VulnerabilityReviews
                Dim iresult As Integer
                iresult = VulnerabilityReviews(False).ToString
                AlertCountQuery = iresult.ToString

                iresult = VulnerabilityReviews(True)
                AlertCountQuery += " (" + iresult.ToString + ")"

            Case eAlertIDs.TenantsOnlineResponses
                AlertCountQuery = TenantsOnlineResponses().ToString

            Case eAlertIDs.PreInspection
                Dim iresult As Integer
                iresult = PreInspectionRecords(False)
                AlertCountQuery = iresult.ToString

                iresult = PreInspectionRecords(True)
                AlertCountQuery += " (" + iresult.ToString + ")"

            Case eAlertIDs.PostInspection
                AlertCountQuery = PostInspectionRecords().ToString

            Case eAlertIDs.RiskReviews
                Dim iresult As Integer
                iresult = RiskReviews(False).ToString
                AlertCountQuery = iresult.ToString

                iresult = RiskReviews(True)
                AlertCountQuery += " (" + iresult.ToString + ")"

            Case eAlertIDs.TenantsOnlineResponses
                AlertCountQuery = TenantsOnlineResponses().ToString

            Case eAlertIDs.TenancySupportReferral
                AlertCountQuery = TenantsSupportReferralCount().ToString

            Case eAlertIDs.GoodsReceivedAwaitingApproval
                AlertCountQuery = ExecGoodsReceivedAwaitingApproval().ToString
            Case eAlertIDs.GoodsReceived
                AlertCountQuery = ExecGoodsReceived().ToString

            Case eAlertIDs.ConditionWorksApprovalRequired
                AlertCountQuery = ConditionApprovalRequired().ToString
            Case eAlertIDs.ApprovedConditionWorks
                AlertCountQuery = ConditionApproved().ToString
            Case eAlertIDs.QueuedRepairs
                AlertCountQuery = QueuedRepairsCount().ToString
            Case eAlertIDs.FaultRejected
                AlertCountQuery = ExecRejectedFaults().ToString
            Case eAlertIDs.ASBOpenCases
                Dim iresult As Integer
                iresult = ExecASBOpenCases(False)
                AlertCountQuery = iresult.ToString
                iresult = ExecASBOpenCases(True)
                AlertCountQuery += " (" + iresult.ToString + ")"
            Case eAlertIDs.DefectRequiringApproval
                Dim iresult As Integer
                iresult = ExecDefectApprovalListCount()
                AlertCountQuery = iresult.ToString
            Case eAlertIDs.DeclarationOfInterest
                AlertCountQuery = ExecDeclarationOfInterest().ToString()
            Case eAlertIDs.TrainingApprovalMyStaff
                AlertCountQuery = ExecTrainingApprovalMyStaff().ToString()
            Case eAlertIDs.TrainingApprovalHR
                AlertCountQuery = ExecTrainingApprovalHR().ToString()
	    Case eAlertIDs.PendingReviewDeclarationOfInterest
                AlertCountQuery = ExecPendingReviewDeclarationOfInterest().ToString()
	    Case eAlertIDs.PayPointReview
                AlertCountQuery = ExecPayPointReview().ToString()
            Case eAlertIDs.TrainingApprovalExec
                AlertCountQuery = ExecTrainingApprovalExec().ToString()

        End Select

    End Function

    Protected Function AlertUrl(ByVal sInputStr As String) As String

        If sInputStr.Contains("UserId") Then
            AlertUrl = sInputStr.Replace("{UserId}", ASPSession("UserId"))
        Else
            AlertUrl = sInputStr.Replace("{LastName}", ASPSession("LastName"))
        End If

    End Function

#Region "Count Functions"

    Private Function ExecDefectApprovalListCount()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("DF_GetDefectsToBeApprovedListCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure          

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try
        ExecDefectApprovalListCount = iResult
    End Function

    Private Function ExecGeneralEnquiries()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " & _
                            "INNER JOIN C_GENERAL G ON J.JOURNALID = G.JOURNALID " & _
                            "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " & _
                            "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " & _
                            "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " & _
                            "WHERE J.ITEMID = 3 AND J.CURRENTITEMSTATUSID = 13 AND " & _
                            "	G.GENERALHISTORYID = (SELECT MAX(GENERALHISTORYID) FROM C_GENERAL GG WHERE GG.JOURNALID = J.JOURNALID) " & _
                            "	AND ASSIGNTO = " & ASPSession("USERID")


            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecGeneralEnquiries = iResult

    End Function

    Private Function ExecReceivedInvoices()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT count(*) as ReceivedInvioceCount " & _
                            " FROM C_JOURNAL J " & _
                            "   INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID   " & _
                            "  LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID   " & _
                            "  INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " & _
                            "  INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID  " & _
                            " INNER JOIN F_PURCHASEORDER PORDER ON PORDER.ORDERID = P.ORDERID  " & _
                            " INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PORDER.POSTATUS  " & _
                            " WHERE J.ITEMID = 1  " & _
                            "	AND PORDER.POSTATUS = 7 " & _
                            "	AND J.ITEMNATUREID IN (2,22,20,21)  " & _
                            "	AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " & _
                            "	AND R.CONTRACTORID = 1	"

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecReceivedInvoices = iResult
    End Function

    Private Function ExecPOReceivedInvoices() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection
        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("F_POInvoiceReceivedCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@USERID", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)
            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecPOReceivedInvoices = iResult

    End Function

    Private Function ExecGoodsReceivedAwaitingApproval()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(*) AS GoodsReceivedAwaitingApproval " & _
                        "FROM F_PURCHASEORDER PO  " & _
                        "WHERE PO.POSTATUS = 1 AND PO.ACTIVE = 1 AND PO.POTYPE = 1 "

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecGoodsReceivedAwaitingApproval = iResult
    End Function

    Private Function ExecAbsentees()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_AbsentsCounts", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try
        ExecAbsentees = iResult

      

    End Function

    Private Function ExecAbsenceLeaveRequest()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_LeaveRequestCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecAbsenceLeaveRequest = iResult
    End Function

    Private Function ExecQueuedPurchases()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SEEALLQUEUEDPURCHASEORDERS' AND DEFAULTVALUE = " & ASPSession("USERID")

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)

            Dim CANSEE As Integer = 0
            If iResult Then
                CANSEE = 1
            End If

            sCommandText = " DECLARE @Date AS DATE = GETDATE() " &
                             " SELECT  COUNT(DISTINCT PO.ORDERID) As QueuedPurchasesCount " &
                             " FROM  F_PURCHASEORDER PO " &
                             " CROSS APPLY (SELECT SUM(GROSSCOST)AS TOTALCOST FROM F_PURCHASEITEM " &
                             " WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) SU " &
                             " INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &
                             " LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID " &
                             " INNER JOIN F_PURCHASEITEM PI on PI.ORDERID = PO.ORDERID "
            If (CANSEE = 0) Then
                sCommandText = sCommandText &
                    " JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID AND EL.EMPLOYEEID = " & ASPSession("USERID") &
                    " AND (  " &
                     "   ( " &
                     "	 El.LIMIT >= PI.GROSSCOST And " &
                     "	  (select isnull(Max(LIMIT), 0) from F_EMPLOYEELIMITS where  " &
                     "			EXPENDITUREID =  PI.EXPENDITUREID and LIMIT <  " &
                     "				(select LIMIT from F_EMPLOYEELIMITS where  " &
                     "					EXPENDITUREID =  PI.EXPENDITUREID and EMPLOYEEID = " & ASPSession("USERID") & ")) < Pi.GROSSCOST " &
                     "   )  " &
                     "   OR " &
                     "   ( El.LIMIT >= PI.GROSSCOST AND  Pi.GROSSCOST > " &
                     "	    (select Isnull(( " &
                     "		    select LIMIT  from " &
                     "		        F_EMPLOYEELIMITS where  " &
                     "			        EXPENDITUREID =   PI.EXPENDITUREID and LIMIT <=  " &
                     "				        (select LIMIT from F_EMPLOYEELIMITS where  " &
                     "					        EXPENDITUREID =   PI.EXPENDITUREID and EMPLOYEEID = " & ASPSession("USERID") & ") " &
                     "		        group by LIMIT  " &
                     "		        order by Limit desc  " &
                     "		        OFFSET (datediff(DD, PI.PIDATE, getDate())/3 +1) ROWS FETCH NEXT (1) ROWS ONLY), 0) as LIMIT) " &
                     "   ) " &
                     " ) "
            End If

            sCommandText = sCommandText &
                             " WHERE(PO.POTYPE = 1 Or PO.POTYPE = 2 Or PO.POTYPE = 7 Or PO.POTYPE = 8) " &
                             " AND ( PO.ACTIVE = 1 ) " &
                             " AND PO.ACTIVE = 1 AND PO.POSTATUS = 0 AND PI.ACTIVE = 1 And PI.PISTATUS = 0 "
            ' *****************************************************************************************************************
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecQueuedPurchases = iResult

    End Function

    Private Function ExecRentEnquiries()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " & _
                           "INNER JOIN C_GENERAL G ON J.JOURNALID = G.JOURNALID " & _
                           "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " & _
                           "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " & _
                           "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " & _
                           "WHERE J.ITEMID = 2 AND J.ITEMNATUREID NOT IN (5,6,7) AND J.CURRENTITEMSTATUSID = 13 AND " & _
                           "	G.GENERALHISTORYID = (SELECT MAX(GENERALHISTORYID) FROM C_GENERAL GG WHERE GG.JOURNALID = J.JOURNALID) " & _
                           "	AND ASSIGNTO = " & ASPSession("USERID")

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecRentEnquiries = iResult
    End Function

    Private Function ExecServiceComplaints(ByVal bUseUserId As Boolean) As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(5) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("C_SERVICECOMPLAINTS_REPORT", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@QUERYTYPE", Data.SqlDbType.Int)
            prms(0).Value = 2
            prms(1) = New SqlParameter("@ORDERBY", Data.SqlDbType.VarChar, 100)
            prms(1).Value = ""
            prms(2) = New SqlParameter("@TENANT", Data.SqlDbType.VarChar, 100)
            prms(2).Value = ""
            prms(3) = New SqlParameter("@ASSIGNEDTO", Data.SqlDbType.VarChar, 100)
            prms(3).Value = ""
            prms(4) = New SqlParameter("@USERID", Data.SqlDbType.Int)
            If bUseUserId Then
                prms(4).Value = ASPSession("USERID")
            Else
                prms(4).Value = System.DBNull.Value
            End If

            prms(5) = New SqlParameter("@return_value", Data.SqlDbType.Int)
            prms(5).Direction = Data.ParameterDirection.ReturnValue

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecServiceComplaints = iResult
    End Function

    Private Function ConditionApproved() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(5) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("PLANNED_GetConditionalToBeArrangedList", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@searchText", Data.SqlDbType.VarChar, 200)
            prms(0).Value = ""
            prms(1) = New SqlParameter("@pageSize", Data.SqlDbType.Int)
            prms(1).Value = 30
            prms(2) = New SqlParameter("@pageNumber", Data.SqlDbType.Int)
            prms(2).Value = 1
            prms(3) = New SqlParameter("@sortColumn", Data.SqlDbType.VarChar, 500)
            prms(3).Value = "Address"
            prms(4) = New SqlParameter("@sortOrder", Data.SqlDbType.VarChar, 5)
            prms(4).Value = "asc"
            prms(5) = New SqlParameter("@totalCount", Data.SqlDbType.Int)
            prms(5).Direction = Data.ParameterDirection.Output

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            cmdCommand.ExecuteReader()
            conConnection.Close()
            Integer.TryParse(cmdCommand.Parameters("@totalCount").Value.ToString(), iResult)
        Catch

        Finally
            conConnection.Close()
        End Try

        ConditionApproved = iResult
    End Function

    Private Function ConditionApprovalRequired() As Integer
        Dim iResult As Integer = 0
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(6) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("PLANNED_GetConditionRatingList", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@searchText", Data.SqlDbType.VarChar, 200)
            prms(0).Value = ""
            prms(1) = New SqlParameter("@isFullList", Data.SqlDbType.Bit)
            prms(1).Value = False
            prms(2) = New SqlParameter("@pageSize", Data.SqlDbType.Int)
            prms(2).Value = 0
            prms(3) = New SqlParameter("@pageNumber", Data.SqlDbType.Int)
            prms(3).Value = 1
            prms(4) = New SqlParameter("@sortColumn", Data.SqlDbType.VarChar, 500)
            prms(4).Value = "Address"
            prms(5) = New SqlParameter("@sortOrder", Data.SqlDbType.VarChar, 5)
            prms(5).Value = "asc"
            prms(6) = New SqlParameter("@totalCount", Data.SqlDbType.Int)
            prms(6).Direction = Data.ParameterDirection.Output

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            cmdCommand.ExecuteReader()
            conConnection.Close()
            Integer.TryParse(cmdCommand.Parameters("@totalCount").Value.ToString(), iResult)

        Catch

        Finally
            conConnection.Close()
        End Try

        ConditionApprovalRequired = iResult
    End Function

    Private Function Asb(ByVal bUseUserId As Boolean) As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(J.JOURNALID)" & _
            " FROM C_JOURNAL J " & _
            " INNER JOIN C_ASB G ON J.JOURNALID = G.JOURNALID " & _
            " LEFT JOIN dbo.C_ASB_CATEGORY_BY_GRADE cat ON cat.CATEGORYID=g.CATEGORY " & _
            " LEFT JOIN C_ASB_CATEGORY AC ON AC.CATEGORYID = G.CATEGORY " & _
            " INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " & _
            " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " & _
            " LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " & _
            " INNER JOIN ( SELECT MAX(TENANCYID) AS MAXTEN, CUSTOMERID FROM C_CUSTOMERTENANCY GROUP BY CUSTOMERID ) MT ON MT.CUSTOMERID = C.CUSTOMERID " & _
            " INNER JOIN ( SELECT MAX(ASBHISTORYID) AS MAXHIST, JOURNALID FROM C_ASB GROUP BY JOURNALID ) MH ON MH.JOURNALID = J.JOURNALID " & _
            " INNER JOIN C_TENANCY T ON T.TENANCYID = MT.MAXTEN " & _
            " INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " & _
            " LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID " & _
            " LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID " & _
            " INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " & _
            " INNER JOIN G_TITLE TIT2 ON TIT2.TITLEID = E.TITLE " & _
            " WHERE J.ITEMNATUREID IN (8,9,24,25) " & _
            " AND G.ASBHISTORYID=(SELECT MAX(ASBHISTORYID) FROM C_ASB WHERE JOURNALID=G.JOURNALID) AND J.CURRENTITEMSTATUSID = 13 "

            If bUseUserId Then
                Dim sAdditionalText As String
                sAdditionalText = " AND G.ASSIGNTO = " & ASPSession("USERID")
                sCommandText = sCommandText & sAdditionalText
            End If

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch ex As Exception

            ErrorHelper.LogError(ex, False)
        Finally
            conConnection.Close()
        End Try

        Asb = iResult
    End Function

    Private Function ExecAdaptationEnquiries()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " & _
                            "INNER JOIN C_ADAPTATION G ON J.JOURNALID = G.JOURNALID " & _
                            "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " & _
                            "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " & _
                            "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " & _
                            "WHERE J.ITEMID = 2 AND J.ITEMNATUREID IN (5) AND J.CURRENTITEMSTATUSID = 13 AND " & _
                            "	G.ADAPTATIONHISTORYID = (SELECT MAX(ADAPTATIONHISTORYID) FROM C_ADAPTATION GG WHERE GG.JOURNALID = J.JOURNALID) " & _
                            "	AND ASSIGNTO = " & ASPSession("USERID")

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecAdaptationEnquiries = iResult
    End Function

    Private Function ExecTransferExchange()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " & _
                            "INNER JOIN C_TRANSFEREXCHANGE G ON J.JOURNALID = G.JOURNALID " & _
                            "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " & _
                            "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " & _
                            "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " & _
                            "WHERE J.ITEMID = 2 AND J.ITEMNATUREID IN (6,7) AND J.CURRENTITEMSTATUSID = 13 AND " & _
                            "	G.TRANSFEREXCHANGEHISTORYID = (SELECT MAX(TRANSFEREXCHANGEHISTORYID) FROM C_TRANSFEREXCHANGE GG WHERE GG.JOURNALID = J.JOURNALID) " & _
                            "	AND ASSIGNTO = " & ASPSession("USERID")

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecTransferExchange = iResult
    End Function

    Private Function TenantsOnlineEnquiries()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = " SELECT COUNT(TO_ENQUIRY_LOG.EnquiryLogID) as NEW_ENQUIRY_COUNT " & _
                           " FROM TO_ENQUIRY_LOG " & _
                           " INNER JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID " & _
                           " INNER JOIN C_NATURE ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID " & _
                           " INNER JOIN C_ITEM ON C_NATURE.ITEMID = C_ITEM.ITEMID " & _
                           " WHERE(TO_ENQUIRY_LOG.ITEMSTATUSID = 26) "

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        TenantsOnlineEnquiries = iResult
    End Function

    Private Function TenantsOnlineResponses()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = "SELECT COUNT(TO_ENQUIRY_LOG.EnquiryLogID) As numOfRows " & _
                            " FROM TO_ENQUIRY_LOG" & _
                            " INNER JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID " & _
                            " INNER JOIN TO_CUSTOMER_RESPONSE CR 	ON TO_ENQUIRY_LOG.EnquiryLogId = CR.EnquiryLogId " & _
                            " INNER JOIN C_NATURE ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID " & _
                            " INNER JOIN C_ITEM ON C_NATURE.ITEMID = C_ITEM.ITEMID " & _
                            " WHERE(CR.Archived = 0 And TO_ENQUIRY_LOG.ItemStatusID = 29 And 1 = 1)"
            'sCommandText = "EXEC TO_ENQUIRY_LOG_RESPONSECOUNTSEARCHRESULTS"

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch


        Finally
            conConnection.Close()
        End Try

        TenantsOnlineResponses = iResult
    End Function

    Private Function VulnerabilityReviews(ByVal bUseUserId As Boolean) As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text
            Dim sCommandText As String
            sCommandText = " SELECT COUNT(CV.VULNERABILITYHISTORYID) " & _
                            " FROM C_JOURNAL J  " & _
                            " INNER JOIN C_VULNERABILITY CV ON CV.JOURNALID = J.JOURNALID  " & _
                            " INNER JOIN C_ADDRESS CUSTADD ON CUSTADD.CUSTOMERID = J.CUSTOMERID AND CUSTADD.ISDEFAULT=1  " & _
                            " INNER JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID=CUSTADD.CUSTOMERID  " & _
                            " INNER JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = CV.ASSIGNTO  " & _
                            " LEFT JOIN C_CUSTOMERTENANCY T  WITH (NOLOCK) ON T.CUSTOMERID= CUST.CUSTOMERID AND T.ENDDATE IS NULL  " & _
                            " LEFT JOIN C_TENANCY CT  WITH (NOLOCK) ON CT.TENANCYID= T.TENANCYID AND CT.ENDDATE IS NULL  " & _
                            " LEFT JOIN P__PROPERTY P  WITH (NOLOCK) ON P.PROPERTYID = CT.PROPERTYID  " & _
                            " LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   " & _
                            " LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 	 " & _
                            " WHERE(J.ITEMNATUREID = 61)  " & _
                            " AND VULNERABILITYHISTORYID=(SELECT MAX(VULNERABILITYHISTORYID) FROM C_VULNERABILITY WHERE JOURNALID=J.JOURNALID) " & _
                            " AND CV.ITEMSTATUSID=13 AND CV.ASSIGNTO = " & ASPSession("USERID")
            If bUseUserId Then
                Dim sAdditionalText As String
                sAdditionalText = " AND CV.REVIEWDATE < (SELECT DATEADD(WK,6,GETDATE())) AND CV.ASSIGNTO = " & ASPSession("USERID")
                sCommandText = sCommandText & sAdditionalText
            End If

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch ex As Exception

            ErrorHelper.LogError(ex, False)
        Finally
            conConnection.Close()
        End Try

        VulnerabilityReviews = iResult
    End Function

    Private Function RiskReviews(ByVal bUseUserId As Boolean) As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = " SELECT COUNT(CR.RISKHISTORYID) " & _
                            " FROM C_JOURNAL J  " & _
                            " INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID  " & _
                            " INNER JOIN C_ADDRESS CUSTADD ON CUSTADD.CUSTOMERID = J.CUSTOMERID AND CUSTADD.ISDEFAULT=1  " & _
                            " INNER JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID=CUSTADD.CUSTOMERID  " & _
                            " INNER JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = CR.ASSIGNTO  " & _
                            " LEFT JOIN C_CUSTOMERTENANCY T  WITH (NOLOCK) ON T.CUSTOMERID= CUST.CUSTOMERID AND T.ENDDATE IS NULL  " & _
                            " LEFT JOIN C_TENANCY CT  WITH (NOLOCK) ON CT.TENANCYID= T.TENANCYID AND CT.ENDDATE IS NULL " & _
                            " LEFT JOIN P__PROPERTY P  WITH (NOLOCK) ON P.PROPERTYID = CT.PROPERTYID  " & _
                            " LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   " & _
                            " LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 	 " & _
                            " WHERE(J.ITEMNATUREID = 63)  " & _
                            " AND RISKHISTORYID=(SELECT MAX(RISKHISTORYID) FROM C_RISK WHERE JOURNALID=J.JOURNALID) " & _
                            " AND CR.ITEMSTATUSID=13 AND CR.ASSIGNTO = " & ASPSession("USERID")
            If bUseUserId Then
                Dim sAdditionalText As String
                sAdditionalText = " AND CR.REVIEWDATE < (SELECT DATEADD(WK,6,GETDATE())) AND CR.ASSIGNTO = " & ASPSession("USERID")
                sCommandText = sCommandText & sAdditionalText
            End If

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch ex As Exception

            ErrorHelper.LogError(ex, False)
        Finally
            conConnection.Close()
        End Try

        RiskReviews = iResult
    End Function

    Private Function ExecRepairsAwaitingApproval()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim sCommandText As String
            sCommandText = " SELECT COUNT (*) AS CompletedItemCount" & _
                            " FROM C_JOURNAL J  " & _
                            "     INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " & _
                            "     LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " & _
                            "     INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " & _
                            "     INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID " & _
                            " WHERE J.ITEMID = 1 " & _
                            "    AND J.CURRENTITEMSTATUSID =  7  " & _
                            "    AND J.ITEMNATUREID IN (2,22,20,21) " & _
                            "    AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) "

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecRepairsAwaitingApproval = iResult
    End Function

    Private Function PreInspectionRecords(ByVal bUseUserId As Boolean) As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(0) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("FL_PREINSPECTION_REPORT_ALERT", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@USERID", Data.SqlDbType.Int)
            If bUseUserId Then
                prms(0).Value = ASPSession("USERID")
            Else
                prms(0).Value = System.DBNull.Value
            End If

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        PreInspectionRecords = iResult
    End Function

    Private Function PostInspectionRecords() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(0) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("FL_CO_POSTINSPECTION_ROWCOUNT", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure


            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        PostInspectionRecords = iResult
    End Function

    Private Function TenantsSupportReferralCount()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("TSR_WhiteBoardAlerts", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try
        TenantsSupportReferralCount = iResult
    End Function

    Private Function ExecGoodsReceived()
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try

            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("F_POGoodsReceivedCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@USERID", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)
            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)

            ' set-up initialisation
            'conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            'Dim cmdCommand As New SqlCommand()
            'cmdCommand.Connection = conConnection
            'cmdCommand.CommandType = Data.CommandType.Text

            'Dim sCommandText As String
            'sCommandText = " DECLARE @POSTATUSID INT  " & _
            '               " DECLARE @WOSTATUSID INT  " & _
            '               " Select @POSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'  " & _
            '               " Select @WOSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'   " & _
            '               " SELECT COUNT(DISTINCT PO.ORDERID) AS GoodsReceived " & _
            '               " FROM F_PURCHASEORDER PO  " & _
            '               " OUTER APPLY (SELECT 1 AS Approved FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND ( POL.POSTATUS = @POSTATUSID or POL.POSTATUS = @WOSTATUSID )) GA " & _
            '               " INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID " & _
            '               " WHERE PO.POSTATUS IN (1,3,  7, 17) AND PO.ACTIVE = 1 AND GA.Approved IS NULL " & _
            '               " AND " & Session("UserID") & " IN (PO.USERID, PIApprovedFilter.APPROVED_BY) "

            'sCommandText = " DECLARE @POSTATUSID INT  " & _
            '                " DECLARE @WOSTATUSID INT  " & _
            '                " Select @POSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'  " & _
            '                " Select @WOSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'   " & _
            '                " SELECT COUNT(DISTINCT PO.ORDERID) AS GoodsReceived " & _
            '                " FROM F_PURCHASEORDER PO  " & _
            '                " OUTER APPLY (SELECT 1 AS Approved FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND ( POL.POSTATUS = @POSTATUSID or POL.POSTATUS = @WOSTATUSID )) GA " & _
            '                " INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID " & _
            '                " WHERE PO.POSTATUS IN (1,3,  7, 17) AND PO.ACTIVE = 1 AND GA.Approved IS NULL " & _
            '                " AND " & Session("UserID") & " IN (PO.USERID, PIApprovedFilter.APPROVED_BY) "

            'cmdCommand.Connection.Open()
            'cmdCommand.CommandText = sCommandText
            'iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecGoodsReceived = iResult
    End Function

    Public Function QueuedRepairsCount() As String
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand()
            cmdCommand.Connection = conConnection
            cmdCommand.CommandType = Data.CommandType.Text

            Dim LoggedInUserId As String = ASPSession("USERID")

            Dim CANSEE As Integer = 0

            Dim CanSeeQuery As String = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SEEALLQUEUEDPURCHASEORDERS' AND DEFAULTVALUE = " & LoggedInUserId
            cmdCommand.CommandText = CanSeeQuery
            cmdCommand.Connection.Open()
            Dim reader As SqlDataReader = cmdCommand.ExecuteReader
            If (reader.HasRows AndAlso reader.Read) Then
                CANSEE = 1
            End If
            reader.Close()
            cmdCommand.Connection.Close()

            Dim SQL_show_all_pos As String = ""

            ' DETERMINE WETHER OR NOT TO SHOWL ALL PURCHASE ORDERS ONLY IF TEAM IS IT
            If Not (ASPSession("TeamCode") = "ITI" Or CInt(CANSEE) = 1) Then
                SQL_show_all_pos = " AND (SELECT COUNT(IT.ORDERITEMID) FROM F_PURCHASEITEM IT INNER JOIN F_EMPLOYEELIMITS LIM ON IT.EXPENDITUREID = LIM.EXPENDITUREID " & _
                    "WHERE ORDERID = PO.ORDERID AND LIM.EMPLOYEEID = " & LoggedInUserId & " AND LIM.LIMIT >= IT.GROSSCOST) > 0 "
            End If

            Dim sCommandText As String = "SELECT COUNT(WO.WOID) " & _
                                        "FROM F_PURCHASEORDER PO " & _
                                        "inner join P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " & _
                                        "left JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) SU ON SU.ORDERID = PO.ORDERID " & _
                                        "left JOIN (SELECT count(orderitemid) AS TOTALqueued, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 and pistatus = 0 GROUP BY ORDERID) tq ON tq.ORDERID = PO.ORDERID " & _
                                        "LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID " & _
                                        "INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " & _
                                        "WHERE PO.POTYPE = 2 AND PO.ACTIVE = 1 and totalqueued > 0 " & SQL_show_all_pos

            cmdCommand.Connection.Open()
            cmdCommand.CommandText = sCommandText
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        QueuedRepairsCount = iResult

    End Function

    Private Function AvailableProperties() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(6) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("V_GetAvailableProperties", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@searchText", Data.SqlDbType.VarChar, 200)
            prms(0).Value = ""
            prms(1) = New SqlParameter("@pageSize", Data.SqlDbType.Int)
            prms(1).Value = 30
            prms(2) = New SqlParameter("@pageNumber", Data.SqlDbType.Int)
            prms(2).Value = 1
            prms(3) = New SqlParameter("@sortColumn", Data.SqlDbType.VarChar, 500)
            prms(3).Value = "Address"
            prms(4) = New SqlParameter("@sortOrder", Data.SqlDbType.VarChar, 5)
            prms(4).Value = "asc"
            prms(5) = New SqlParameter("@getOnlyCount", Data.SqlDbType.Bit)
            prms(5).Value = True



            prms(6) = New SqlParameter("@totalCount", Data.SqlDbType.Int)
            prms(6).Direction = Data.ParameterDirection.Output

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            cmdCommand.ExecuteReader()
            conConnection.Close()
            Integer.TryParse(cmdCommand.Parameters("@totalCount").Value.ToString(), iResult)

        Catch

        Finally
            conConnection.Close()
        End Try

        AvailableProperties = iResult
    End Function

    Private Function VoidInspections() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(6) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("V_GetVoidInspectionsToBeArranged", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@searchText", Data.SqlDbType.VarChar, 200)
            prms(0).Value = ""
            prms(1) = New SqlParameter("@pageSize", Data.SqlDbType.Int)
            prms(1).Value = 30
            prms(2) = New SqlParameter("@pageNumber", Data.SqlDbType.Int)
            prms(2).Value = 1
            prms(3) = New SqlParameter("@sortColumn", Data.SqlDbType.VarChar, 500)
            prms(3).Value = "Address"
            prms(4) = New SqlParameter("@sortOrder", Data.SqlDbType.VarChar, 5)
            prms(4).Value = "asc"
            prms(5) = New SqlParameter("@getOnlyCount", Data.SqlDbType.Bit)
            prms(5).Value = True

            prms(6) = New SqlParameter("@totalCount", Data.SqlDbType.Int)
            prms(6).Direction = Data.ParameterDirection.Output

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            cmdCommand.ExecuteReader()
            conConnection.Close()
            Integer.TryParse(cmdCommand.Parameters("@totalCount").Value.ToString(), iResult)
        Catch

        Finally
            conConnection.Close()
        End Try

        VoidInspections = iResult
    End Function


    Private Function BritishGasNotification() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(7) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("V_GetBritishGasNotificationList", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@searchText", Data.SqlDbType.VarChar, 200)
            prms(0).Value = ""

            prms(1) = New SqlParameter("@notificationStatus", Data.SqlDbType.VarChar, 200)
            prms(1).Value = "InProgress"
            prms(2) = New SqlParameter("@pageSize", Data.SqlDbType.Int)
            prms(2).Value = 30
            prms(3) = New SqlParameter("@pageNumber", Data.SqlDbType.Int)
            prms(3).Value = 1
            prms(4) = New SqlParameter("@sortColumn", Data.SqlDbType.VarChar, 500)
            prms(4).Value = "Address"
            prms(5) = New SqlParameter("@sortOrder", Data.SqlDbType.VarChar, 5)
            prms(5).Value = "asc"
            prms(6) = New SqlParameter("@getOnlyCount", Data.SqlDbType.Bit)
            prms(6).Value = True

            prms(7) = New SqlParameter("@totalCount", Data.SqlDbType.Int)
            prms(7).Direction = Data.ParameterDirection.Output

            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            cmdCommand.ExecuteReader()
            conConnection.Close()
            Integer.TryParse(cmdCommand.Parameters("@totalCount").Value.ToString(), iResult)
        Catch

        Finally
            conConnection.Close()
        End Try

        BritishGasNotification = iResult
    End Function

    Private Function ExecRejectedFaults() As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("FL_CountRejectedFaults", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@userId", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try
        ExecRejectedFaults = iResult

    End Function

    Private Function ExecASBOpenCases(ByVal bUseUserId As Boolean) As Integer
        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms(1) As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("ASB_GetOpenCases", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            ' add parameters
            prms(0) = New SqlParameter("@USERID", Data.SqlDbType.Int)
            If bUseUserId Then
                prms(0).Value = ASPSession("USERID")
            Else
                prms(0).Value = System.DBNull.Value
            End If
            prms(1) = New SqlParameter("@return_value", Data.SqlDbType.Int)
            prms(1).Direction = Data.ParameterDirection.ReturnValue
            For Each prm As SqlParameter In prms
                cmdCommand.Parameters.Add(prm)
            Next

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecASBOpenCases = iResult

    End Function

    Private Function ExecDeclarationOfInterest()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_DeclarationOfInterestCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecDeclarationOfInterest = iResult
    End Function

    Private Function ExecTrainingApprovalMyStaff()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_TrainingApprovalMyStaff", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecTrainingApprovalMyStaff = iResult
    End Function

    Private Function ExecTrainingApprovalHR()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("TrainingApprovalHR", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@USERID", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecTrainingApprovalHR = iResult
    End Function
    
    Private Function ExecPendingReviewDeclarationOfInterest()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_PendingReviewDeclarationOfInterestCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecPendingReviewDeclarationOfInterest = iResult
    End Function

    Private Function ExecPayPointReview()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_PayPointReviewCount", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecPayPointReview = iResult
    End Function

    Private Function ExecTrainingApprovalExec()

        Dim iResult As Integer
        Dim conConnection As New SqlConnection

        Try
            ' set-up initialisation
            Dim prms As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
            Dim cmdCommand As New SqlCommand("E_TrainingApprovalDirector", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure
            ' add parameters
            prms = New SqlParameter("@LineMgr", Data.SqlDbType.Int)
            prms.Value = ASPSession("USERID")
            cmdCommand.Parameters.Add(prms)

            cmdCommand.Connection.Open()
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        Catch

        Finally
            conConnection.Close()
        End Try

        ExecPayPointReview = iResult
    End Function
#End Region

#End Region

End Class
