<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MandReads.ascx.vb" Inherits="BHAIntranet_UserControl_Whiteboard_MandReads" %>
<asp:GridView ShowFooter="false" CssClass="gvwMand" ShowHeader="false" ID="gvwMand"
    AutoGenerateColumns="False" runat="server" AllowPaging="True" PageSize="3">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <div class="docimage">
                    <asp:Image ID="imgEmp" ImageUrl='<%# docType(eval("DOCFILE")) %>' runat="server" />
                </div>
                <div class="doccontent">
                    <div class="doctitle">
                        <asp:LinkButton ID="ReadLink" OnClick="ReadLink_Click" CommandArgument='<%# eval("FULLPATH") %>'
                            runat="server" Text='<%# Eval("DOCNAME") %>' CssClass="readMandLink" ></asp:LinkButton></div>
                    <div class="docdetails">
                        <asp:Label ID="lblDate" runat="server" Text="Date Submitted:" />
                        <asp:Label ID="lblDateText" runat="server" Text='<%# FormatDate(eval("DATECREATED")) %>' /><br />
                        <asp:Label ID="lblSize" runat="server" Text="Size:" />
                        <asp:Label ID="lblSizeText" runat="server" Text='<%# formhelper.GetFileSize(eval("FULLPATH")) %>' /></div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="doclnk">
    <asp:LinkButton ID="lnkMand" runat="server">LinkButton</asp:LinkButton>
</div>
