<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Suggestions.ascx.vb" Inherits="BHAIntranet_UserControl_Whiteboard_Suggestions" %>
<asp:Panel ID="pnlSugg" runat="server">
    <asp:Image ID="imgAcorn" runat="server" ImageUrl="~/BHAIntranet/Images/General/im_little_acorns_03.gif" />
<asp:Label ID="lblValidation" ForeColor="Red" runat="server" Text=""></asp:Label><br />
    <asp:Label ID="lblTopic" cssClass="tpLabel" AssociatedControlID="txtTopic" runat="server" Text="Topic: "></asp:Label><asp:TextBox ID="txtTopic"  cssClass="tpInput" runat="server"></asp:TextBox><br />
<asp:Label ID="lblSuggestion" cssClass="suggTitle"  AssociatedControlID="txtSugg" runat="server" Text="Suggestion: "></asp:Label><br />
<asp:TextBox ID="txtSugg"  cssClass="tpTextArea" TextMode="MultiLine" runat="server"></asp:TextBox>
<div id="SuggButton">
<asp:Button ID="btnSend" runat="server" Text="Send" />
</div>
</asp:Panel>