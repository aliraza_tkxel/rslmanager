<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LatestEmployee.ascx.vb"
    Inherits="BHAIntranet_UserControl_Whiteboard_LatestEmployee" %>
<div id="empimage">
    <asp:Image ID="imgEmp" runat="server" />
</div>
<div id="empcontent">
    <div id="nameandtitle">
        <asp:Label ID="lblNameText" runat="server" Text="" />:
        <asp:Label ID="lblTitleText" runat="server" Text="" />
    </div>
    <div id="e">
        <asp:Label ID="lblTeam" runat="server" Text="Department:" />
        <asp:Label ID="lblTeamText" runat="server" Text="" /><br />
        <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" />
        <asp:Label ID="lblStartDateText" runat="server" Text="" />
    </div>
    <div id="profile">
        <asp:Label ID="lblProfile" runat="server" Text="" />&nbsp;<br />
        <asp:HyperLink ID="hlkViewDetails" runat="server">View Details...</asp:HyperLink><br />
        <div style="float: right;">            
            <asp:Button ID="btnNext" runat="server" CssClass="btn" PostBackUrl="~/BHAIntranet/CustomPages/NewStarters.aspx"
                Text="View All" />
        </div>
    </div>
</div>
