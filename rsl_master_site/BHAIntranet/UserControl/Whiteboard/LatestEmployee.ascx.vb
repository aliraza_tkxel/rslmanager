
Partial Class BHAIntranet_UserControl_Whiteboard_LatestEmployee
    Inherits baseusercontrol


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim bllEmp As New bllEmployees
        bllEmp.GetLatestEmployee()
        lblProfile.Text = formhelper.TruncateText(bllEmp.About, 150)
        lblNameText.Text = bllEmp.FullName
        Dim dt As DateTime = bllEmp.StartDate
        lblStartDateText.Text = dt.ToString("dddd, dd MMMM yyyy")

        lblTitleText.Text = bllEmp.JobTitle
        lblTeamText.Text = bllEmp.TeamName
        If bllEmp.ImagePath <> "" Then
            imgEmp.ImageUrl = "/EmployeeImage/" & bllEmp.ImagePath
        Else
            imgEmp.ImageUrl = "/BHAIntranet/Images/Defaults/emp.gif"
        End If

        Dim hlk As HyperLink = hlkViewDetails
        If hlk IsNot Nothing Then            
            hlk.NavigateUrl = "~/BHAIntranet/CustomPages/Employees.aspx?ViewEmpID=" & bllEmp.EmpID.ToString
        End If
    End Sub
End Class
