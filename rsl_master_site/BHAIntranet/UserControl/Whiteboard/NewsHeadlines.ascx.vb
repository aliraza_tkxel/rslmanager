
Partial Class BHAIntranet_UserControl_Whiteboard_NewsHeadlines
    Inherits System.Web.UI.UserControl

    Protected Function FormatPageContent(ByVal strPageContent)
        Return formhelper.TruncateText(strPageContent, 55)
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim bllNews As New bllNews
        Dim blnIsHomePage As Boolean = bllNews.GetHomePage

        If blnIsHomePage = True Then
            bllNews.GetHomePage()
            lblNewsHeadline.Text = bllNews.Headline
            hlnkGoToHomeStory.NavigateUrl = "/BHAintranet/CustomPages/NewsDetail.aspx?newsid=" & bllNews.NewsID.ToString
            lblnewsContent.Text = formhelper.TruncateText(bllNews.Content, 150)
            If bllNews.HomeImagePath <> "" Then
                imgNews.ImageUrl = "/BHAIntranetImagesNews/" & bllNews.HomeImagePath
            Else
                imgNews.ImageUrl = "/BHAIntranet/Images/Defaults/news.gif"
            End If

        Else

            lblNewsHeadline.Text = "There is no headline"
            lblnewsContent.Text = "There is no headline"
            imgNews.ImageUrl = "/BHAIntranet/Images/Defaults/news.gif"
        End If

    End Sub

    Protected Sub gvwHeadlines_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwHeadlines.RowCommand
        Dim gvw As GridView = CType(sender, GridView)        
        Dim iRowIdx As Integer = Val(e.CommandArgument)
        Dim intNewsID As Integer = gvw.DataKeys(iRowIdx).Value
        sessionhelper.NewsID = intNewsID
        Response.Redirect("/BHAintranet/CustomPages/NewsDetail.aspx?newsid=" & intNewsID.ToString)
    End Sub


End Class
