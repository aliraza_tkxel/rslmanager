<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SoftwareConnect.ascx.vb" Inherits="BHAIntranet_UserControl_Whiteboard_SoftwareConnect" %>

<div id="softtitle">
</div>

<ul id="software_connect">
<li><asp:Image ID="imgExcel" ImageUrl="~/BHAIntranet/Images/Defaults/xls_connect.gif" runat="server" />
    <asp:HyperLink ID="hlkExcel" Target="_blank" runat="server" NavigateUrl="~/BHAIntranet/Pages/Book1.xls">Microsoft Office Excel</asp:HyperLink></li>
<li><asp:Image ID="imgWord" ImageUrl="~/BHAIntranet/Images/Defaults/doc_connect.gif" runat="server" />
    <asp:HyperLink ID="hlkWord" Target="_blank" runat="server" NavigateUrl="~/BHAIntranet/Pages/Doc01.doc">Microsoft Office Word</asp:HyperLink></li>
<li><asp:Image ID="imgPP" ImageUrl="~/BHAIntranet/Images/Defaults/ppt_connect.gif" runat="server" />
    <asp:HyperLink ID="hlkPowerPoint" Target="_blank" runat="server" NavigateUrl="~/BHAIntranet/Pages/Presentation1.ppt">Microsoft Office PowerPoint</asp:HyperLink></li>
</ul>
