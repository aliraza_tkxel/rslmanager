<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TravelSites.ascx.vb" Inherits="BHAIntranet_UserControl_Whiteboard_TravelSites" %>

<div id="softtitle">
</div>

<ul id="software_connect">
<li><asp:HyperLink ID="hlkEastAnglia" Target="_blank" runat="server" NavigateUrl="http://www.travelineeastanglia.org.uk/ea/XSLT_TRIP_REQUEST2?language=en&amp;timeOffset=15">Traveline East Anglia</asp:HyperLink></li>
<li><asp:HyperLink ID="HyperLink1" Target="_blank" runat="server" NavigateUrl="http://www.firstgroup.com/ukbus/easterncounties/easterncounties/home/">First Buses</asp:HyperLink></li>
<li><asp:HyperLink ID="HyperLink2" Target="_blank" runat="server" NavigateUrl="http://www.norfolk.gov.uk/consumption/idcplg?IdcService=SS_GET_PAGE&amp;ssDocName=NCC006844">Park and Ride</asp:HyperLink></li>
<li><asp:HyperLink ID="HyperLink3" Target="_blank" runat="server" NavigateUrl="http://www.konectbus.co.uk/BusTimes.asp">Konect Bus</asp:HyperLink></li>
<li><asp:HyperLink ID="HyperLink4" Target="_blank" runat="server" NavigateUrl="http://www.anglianbus.co.uk/">Anglian Bus Company</asp:HyperLink></li>
<li><asp:HyperLink ID="HyperLink5" Target="_blank" runat="server" NavigateUrl="http://www.nationalexpresseastanglia.com/">National Express trains</asp:HyperLink></li>
</ul>
