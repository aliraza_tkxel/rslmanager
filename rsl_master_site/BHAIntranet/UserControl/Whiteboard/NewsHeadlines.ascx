<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewsHeadlines.ascx.vb"
    Inherits="BHAIntranet_UserControl_Whiteboard_NewsHeadlines" %>
<div id="news">
    <div id="newsheader">
        Broadland News:</div>
    <div id="newsbody">
        <asp:Label ID="lblNewsHeadline" runat="server" CssClass="newsHeadline"></asp:Label><br />
        <div id="headlinecontainer">
            <div>
                <asp:Label ID="lblnewsContent" runat="server" CssClass="newsContent"></asp:Label>
                <asp:Image ID="imgNews" runat="server" CssClass="NewsImageHeadline" />
            </div>
            <br />
        </div>
        <asp:HyperLink CssClass="lnkGoToHomeStory" ID="hlnkGoToHomeStory" runat="server">Go to Story</asp:HyperLink>
        <br />
        <br />
        <asp:ObjectDataSource ID="odsNewsHeadlines" runat="server" SelectMethod="GetHeadlines"
            TypeName="bllNews"></asp:ObjectDataSource>
        <asp:GridView ID="gvwHeadlines" CssClass="gvwHeadlines" BorderStyle="None" BorderWidth="0px"
            runat="server" AutoGenerateColumns="False" DataKeyNames="NewsID" DataSourceID="odsNewsHeadlines">
            <Columns>
                <asp:TemplateField HeaderText="Latest News" SortExpression="Headline">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Headline") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Headline") %>'></asp:Label><br />
                        <div style="font-size:10px; color:Gray;">
                            <asp:Label ID="lblTaster" CssClass="newsTaster" runat="server" Text='<%# FormatPageContent(Eval("NewsContent")) %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select"
                            Text='<%# Eval("DateCreated","{0:dd-MMM-yyyy}") %>' CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</div>
