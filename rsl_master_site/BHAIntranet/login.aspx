<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/login.master" AutoEventWireup="false"
    CodeFile="login.aspx.vb" Inherits="login" Title="rslManager" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $.watermark.options.useNative = false;
            $('<%= "#" +  txtUserName.ClientID %>').watermark("User Name");
            $('<%= "#" + txtPassword.ClientID %>').watermark("Password");
            $('<%= "#" + txtChangeUserName.ClientID %>').watermark("User Name");
            $('<%= "#" + txtChangePassword.ClientID %>').watermark("Current Password");
            $('<%= "#" + txtChangeNewPassword.ClientID %>').watermark("New Password");
            $('<%= "#" + txtConfirmPassword.ClientID %>').watermark("Confirm New Password");

            $('<%= "#" + txtUpdateUserName.ClientID %>').watermark("User Name");
            $('<%= "#" + txtUpdatePassword.ClientID %>').watermark("Current Password");
            $('<%= "#" + txtUpdateNewPassword.ClientID %>').watermark("New Password");
        });
    </script>
    <style type="text/css">

        .lblValidation ul
        {
            list-style-type: none;
        }
        /*.loading-image
        {
            background-color: #fff;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0px;
            right: 0px;
            z-index: 1000000;
            text-align: center;
            opacity: 0.7;
            margin-right: -30px;
        }
        .modalBackground
        {
            background-color: #000000;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup
        {
            top: 260px !important;
            width: 80%;
        }
        .ClosePopupCls
        {
            animation: blinker 1s linear infinite;
            font-size: 16px !important;
            float: right;
            color: White;
            background: transparent !important;
            height: 18px !important;
            width: 70px !important;
            cursor: pointer;
        }
        @keyframes blinker 
        {
          50% {
            opacity: 0;
          }
        }*/

    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajax:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </ajax:ScriptManager>
    <asp:Label ID="lblValidationLabel" runat="server" ForeColor="Red" Style="font-weight: 700"
        Visible="False"></asp:Label>
    <asp:Label ID="lblValidation" runat="server" ForeColor="Red" CssClass="lblValidation"></asp:Label>
    <asp:Image ID="imgLock" runat="server" ImageUrl="../BusinessManager/AuditReport/includes/lock.png"
        Visible="False" />
    <asp:Panel ID="pnlExpiry" Visible="false" runat="server">
        <asp:Label ID="lblLoginStatus" runat="server" Text=""></asp:Label><br />
        <br />
        &nbsp;
        <asp:LinkButton ID="lnkUpdate" runat="server"><span style=" font-weight:bold; ">Update My Login</span></asp:LinkButton>&nbsp;
        <asp:LinkButton ID="lnkLogin" Visible="false" runat="server"><span style=" font-weight:bold; ">Log Straight in </span></asp:LinkButton>
    </asp:Panel>
    <asp:Panel ID="pnlLogin" runat="server" Visible="true">
        <br />
        <div>
            <asp:TextBox ID="txtUserName" CssClass="txtfield username" runat="server" ToolTip="Username"
                AutoCompleteType="Disabled"></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="txtfield password" runat="server"
                ToolTip="Password" AutoCompleteType="Disabled"></asp:TextBox>
        </div>
        <div class="btn-area">
            <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAccountLocked" runat="server" Visible="false">
        <div class="lockdown">
            <strong>Access Lockdown:<br />
                Your account is now locked,<br />
                please contact the Systems<br />
                Team for assistance.<br />
            </strong>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlChangeLogin" runat="server" Visible="False">
        <div class="fpass">
            <p>
                <strong>Change your password</strong>
            </p>
            <br />
        </div>
        <div>
            <asp:RequiredFieldValidator ID="rfvChangeUserName" ErrorMessage="Please Enter a User Name.<br />"
                ControlToValidate="txtChangeUserName" runat="server" Display="Dynamic" ValidationGroup="changePassword" />
            <asp:TextBox CssClass="txtfield username" ID="txtChangeUserName" runat="server" AutoCompleteType="Disabled"
                ValidationGroup="changePassword"></asp:TextBox><br />
        </div>
        <div>
            <asp:RequiredFieldValidator ID="rfvChangePassword" ErrorMessage="Please enter Current Password.<br />"
                ControlToValidate="txtChangePassword" runat="server" Display="Dynamic" ValidationGroup="changePassword" />
            <asp:TextBox CssClass="txtfield password" TextMode="Password" ID="txtChangePassword"
                runat="server" Text="" ToolTip="Current password" ValidationGroup="changePassword"></asp:TextBox>
            <br />
        </div>
        <div>
            <asp:RequiredFieldValidator ID="rfvChangeNewPassword" ErrorMessage="Please Enter New Password.<br />"
                ControlToValidate="txtChangeNewPassword" runat="server" Display="Dynamic" ValidationGroup="changePassword" />
            <asp:TextBox CssClass="txtfield password" TextMode="Password" ID="txtChangeNewPassword"
                runat="server" Text="" ToolTip="New password" ValidationGroup="changePassword"></asp:TextBox><br />
        </div>
        <div>
            <asp:RequiredFieldValidator ID="rfvConfirmPassword" ErrorMessage="Please confirm new password.<br />"
                ControlToValidate="txtConfirmPassword" runat="server" Display="Dynamic" ValidationGroup="changePassword" />
            <asp:CompareValidator ID="cmpvConfirmPassword" runat="server" ErrorMessage="The new passwords do not match<br />"
                ControlToValidate="txtConfirmPassword" ControlToCompare="txtChangeNewPassword"
                Type="String" Display="Dynamic" ValidationGroup="changePassword"></asp:CompareValidator>
            <asp:TextBox CssClass="txtfield password" TextMode="Password" ID="txtConfirmPassword"
                runat="server" Text="" ToolTip="Confirm New password" ValidationGroup="changePassword"></asp:TextBox><br />
            <asp:RegularExpressionValidator ID="rvChangeNewPassword" runat="server" ControlToValidate="txtChangeNewPassword"
                ErrorMessage="Your new password must be at least 7 characters , contain at least one lower case letter, one upper case letter and one digit<br />"
                ValidationExpression="^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$" Display="Dynamic"
                ValidationGroup="changePassword"></asp:RegularExpressionValidator>
        </div>
        <div class="btn-area">
            <asp:Button ID="btnChangePassword" runat="server" Text="Update Password" CssClass="btn updatepass"
                ValidationGroup="changePassword" />
        </div>
    </asp:Panel>
    <asp:RegularExpressionValidator ID="reValidation" runat="server" ControlToValidate="txtUpdateNewPassword"
        ErrorMessage="Your new password must be at least 7 characters , contain at least one lower case letter, one upper case letter and one digit"
        ValidationExpression="^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$" Visible="false"></asp:RegularExpressionValidator>
    <br />
    <asp:Panel ID="pnlforgot" runat="server">
        <asp:LinkButton ID="lnkmisplaced" runat="server" PostBackUrl="misplaced.aspx"><span style=" font-weight:bold; ">Forgot your password</span></asp:LinkButton>
        &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
        <asp:LinkButton ID="lnkChangePassword" runat="server"><span style=" font-weight:bold; ">Change Password</span></asp:LinkButton>
    </asp:Panel>
    <asp:Panel ID="pnlUpdateLogin" runat="server" Visible="False">
        <div>
            <asp:RequiredFieldValidator ID="rfvxtUpdatePassword" ErrorMessage="Please Enter a User Name.<br />"
                ControlToValidate="txtUpdateUserName" runat="server" Display="Dynamic" ValidationGroup="updatePassword" />
            <asp:TextBox CssClass="txtfield username" ID="txtUpdateUserName" runat="server" ValidationGroup="updatePassword"></asp:TextBox><br />
            <br />
        </div>
        <div>
            <asp:RequiredFieldValidator ID="rfvUpdatePassword" ErrorMessage="Please enter Current Password.<br />"
                ControlToValidate="txtUpdatePassword" runat="server" Display="Dynamic" ValidationGroup="updatePassword" />
            <asp:TextBox CssClass="txtfield password" TextMode="Password" ID="txtUpdatePassword"
                runat="server" ValidationGroup="updatePassword"></asp:TextBox><br />
            <br />
        </div>
        <div>
            <asp:RequiredFieldValidator ID="rfvUpdateNewPassword" ErrorMessage="Please Enter New Password.<br />"
                ControlToValidate="txtUpdateNewPassword" runat="server" Display="Dynamic" ValidationGroup="updatePassword" />
            <asp:TextBox CssClass="txtfield password" TextMode="Password" ID="txtUpdateNewPassword"
                runat="server" ValidationGroup="updatePassword"></asp:TextBox><br />
            <asp:RegularExpressionValidator ID="rvUpdateNewPassword" runat="server" ControlToValidate="txtUpdateNewPassword"
                ErrorMessage="Your new password must be at least 7 characters, contain at least one lower case letter, one upper case letter and one digit.<br /><br />"
                ValidationExpression="^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$" Display="Dynamic"
                ValidationGroup="updatePassword"></asp:RegularExpressionValidator>
            <br />
        </div>
        <div class="btn-area">
            <asp:Button ID="btnUpdatePassword" runat="server" Text="Update Password" CssClass="btn updatepass"
                ValidationGroup="updatePassword" />
        </div>
    </asp:Panel>
    <%--<!-- ModalPopupExtender -->
    <asp:Button ID="btnShow" runat="server" Text="Edit" Style="display: none;" />
    <ajax:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnShow"
        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <ajax:UpdatePanel>
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" style = "display:none">
                <video width="400" height="145" autoplay  src="Promotions/MB_Logo_Animate_05-10.mp4" ></video>
                <br />
                <div style="margin: auto 0; width: 396px;">
                    <asp:Button ID="btnClose" runat="server" CssClass="btn ClosePopupCls" Text="Close [x]" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </<ajax:UpdatePanel>--%>
</asp:Content>