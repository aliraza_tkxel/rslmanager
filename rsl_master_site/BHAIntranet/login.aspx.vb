Imports System.Data.SqlClient
Imports System.Data
Imports bllLogin
Imports System.Drawing.Color
Partial Class login
    'Inherits System.Web.UI.Page
    Inherits basepage
    Public Enum NavMode
        LoadPage = 1
        AlmostExpired = 2
        Expired = 3
        UpdatePassword = 4
        ChangePassword = 5
    End Enum

    Public Sub uiController(ByVal controllerid As Integer)
        If controllerid = NavMode.LoadPage Then
            pnlLogin.Visible = True
            pnlExpiry.Visible = False
            pnlChangeLogin.Visible = False
            Master.Page.Title = "Rsl Manager"
        End If

        If controllerid = NavMode.AlmostExpired Then
            pnlExpiry.Visible = True
            pnlLogin.Visible = False
            pnlforgot.Visible = False
            pnlChangeLogin.Visible = False
            lnkLogin.Visible = True
        End If

        If controllerid = NavMode.Expired Then
            pnlExpiry.Visible = True
            lnkLogin.Visible = False
            pnlforgot.Visible = False
            pnlChangeLogin.Visible = False
            pnlLogin.Visible = False
        End If

        If controllerid = NavMode.UpdatePassword Then
            pnlExpiry.Visible = False
            pnlLogin.Visible = False
            pnlforgot.Visible = False
            pnlChangeLogin.Visible = False
            pnlUpdateLogin.Visible = True
        End If

        If controllerid = NavMode.ChangePassword Then
            pnlExpiry.Visible = False
            pnlLogin.Visible = False
            pnlforgot.Visible = False
            pnlUpdateLogin.Visible = False
            pnlChangeLogin.Visible = True
        End If

    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Try
            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(txtPassword.Text)
            Dim encodedPassword As String = Convert.ToBase64String(byt)
            Dim bllLogin As New bllIntranetLogin
            bllLogin.UserName = txtUserName.Text
            bllLogin.Password = encodedPassword
            lblLoginStatus.Text = String.Empty
            lblValidation.Text = String.Empty
            lblValidationLabel.Text = String.Empty

            If bllLogin.Login = True Then
                If bllLogin.LoginExpiryStatus = ExpiryType.Expired Then
                    lblLoginStatus.Text = "Your password has expired you have the following options: "
                    uiController(NavMode.Expired)
                    Session("bllLogin") = bllLogin
                ElseIf bllLogin.LoginExpiryStatus = ExpiryType.AlmostExpired Then
                    lblLoginStatus.Text = "Your password has almost expired you have the following options: "
                    uiController(NavMode.AlmostExpired)
                    Session("bllLogin") = bllLogin
                Else
                    If Not bllLogin.SetSession(ASPSession) Then
                        lblValidation.Text = "Your account is currently inactive."
                    End If
                End If

            Else
                imgLock.Visible = False
                lblValidationLabel.Visible = False
                lblValidation.ForeColor = Red
                If (bllLogin.LoginLocked = True) Then
                    'lblValidationLabel.Text = "Access Lockdown:"
                    'lblValidationLabel.Visible = True
                    'lblValidation.Text = "Your account is now locked, please contact the Systems Team for assistance."
                    'imgLock.Visible = True
                    pnlAccountLocked.Visible = True


                ElseIf (bllLogin.LoginThreshold = 4) Then
                    lblValidationLabel.Text = "Access Lockdown Warning:"
                    lblValidationLabel.Visible = True
                    lblValidation.Text = "You have one more login attempt before your access is locked down!"
                Else
                    lblValidation.Text = "Your username and password do not appear to be correct"
                End If

            End If
        Catch ex As Exception
            lblValidation.Text = ex.Message
        End Try

    End Sub

    Protected Sub lnkUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUpdate.Click
        uiController(NavMode.UpdatePassword)
        Dim bllLogin As New bllIntranetLogin
        bllLogin = CType(Session("bllLogin"), bllIntranetLogin)
        txtUpdateUserName.Text = bllLogin.UserName
        Session("bllLogin") = bllLogin
    End Sub

    Protected Sub btnUpdatePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePassword.Click
        If Page.IsValid Then
            lblValidation.Text = ""
            Try
                Dim bllLogin As New bllIntranetLogin
                bllLogin = CType(Session("bllLogin"), bllIntranetLogin)
                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(txtUpdatePassword.Text)
                Dim encodedUpdatePassword As String = Convert.ToBase64String(byt)

                Dim bytNew As Byte() = System.Text.Encoding.UTF8.GetBytes(txtUpdateNewPassword.Text)
                Dim encodedUpdateNewPassword As String = Convert.ToBase64String(bytNew)
                bllLogin.UserName = txtUpdateUserName.Text
                bllLogin.Password = encodedUpdatePassword
                bllLogin.NewPassword = encodedUpdateNewPassword

                Dim intUpdateStatus As Integer = bllLogin.UpdatePassword
                If intUpdateStatus = 0 Then
                    bllLogin.SetSession(ASPSession)
                ElseIf intUpdateStatus = 10 Then
                    lblValidation.ForeColor = Red
                    lblValidation.Text = "The username and password do not seem to exist"
                ElseIf intUpdateStatus = 11 Then
                    lblValidation.ForeColor = Red
                    lblValidation.Text = "Unable to update the password. The value provided for the new password does not meet the length, complexity, or history requirements of the RSLManager."
                End If

                'If intUpdateStatus = 0 Then
                '    lblValidation.ForeColor = Red
                '    lblValidation.Text = "The username and password do not seem to exist"
                'Else
                '    bllLogin.SetSession(ASPSession)
                'End If

            Catch ex As Exception
                lblValidation.ForeColor = Red
                lblValidation.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Sub lnkLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogin.Click
        Dim bllLogin As New bllIntranetLogin
        bllLogin = CType(Session("bllLogin"), bllIntranetLogin)
        bllLogin.SetSession(ASPSession)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'mp1.Show()
        'Dim SiteURL As String
        'SiteURL = Request.ServerVariables("SERVER_NAME")
        'Dim pageScript As String = "<script type='text/javascript' src='https://seal.verisign.com/getseal?host_name=" + SiteURL + "&amp;size=L&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en'></script>"
        ''Page.ClientScript.RegisterStartupScript(Page.GetType(), "scr", pageScript)

        'Me.lblSiteURL.Text = pageScript

        Dim status As String
        Dim bllLogin As New bllIntranetLogin

        If String.IsNullOrEmpty(Request.QueryString("status")) = False Then
            status = Request.QueryString("status")

            Select Case status
                Case "timedout"
                    ASPSession("USERID") = ""
                    Session("bllLogin") = ""
                    Server.Transfer("/BHAIntranet/Login.aspx", False)
                Case "signout"
                    ASPSession("USERID") = ""
                    Session("bllLogin") = ""
                    Server.Transfer("/BHAIntranet/Login.aspx", False)
                Case Else
                    Exit Select
            End Select
        End If

    End Sub

    Protected Sub lnkChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChangePassword.Click
        lblValidation.Text = ""
        lblValidationLabel.Text = ""
        uiController(NavMode.ChangePassword)
    End Sub

    Protected Sub btnChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangePassword.Click
        If Page.IsValid Then
            lblValidation.Text = ""
            Try
                Dim bllLogin As New bllIntranetLogin
                bllLogin.UserName = txtChangeUserName.Text
                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(txtChangePassword.Text)
                Dim encodedChangePassword As String = Convert.ToBase64String(byt)

                Dim bytNew As Byte() = System.Text.Encoding.UTF8.GetBytes(txtChangeNewPassword.Text)
                Dim encodedChangeNewPassword As String = Convert.ToBase64String(bytNew)



                bllLogin.Password = encodedChangePassword
                bllLogin.NewPassword = encodedChangeNewPassword

                Dim intUpdateStatus As Integer = bllLogin.UpdatePassword
                If intUpdateStatus = 0 Then
                    lblValidation.ForeColor = Green
                    lblValidation.Text = "The password has been changed successfully. Please use your new password to log in."
                    uiController(NavMode.LoadPage)
                ElseIf intUpdateStatus = 10 Then
                    lblValidation.ForeColor = Red
                    lblValidation.Text = "The username and password do not seem to exist"
                ElseIf intUpdateStatus = 11 Then
                    lblValidation.ForeColor = Red
                    lblValidation.Text = "Unable to update the password. The value provided for the new password does not meet the length, complexity, or history requirements of the RSLManager."
                End If

            Catch ex As Exception
                lblValidation.ForeColor = Red
                lblValidation.Text = ex.Message
            End Try
        End If
    End Sub

End Class
