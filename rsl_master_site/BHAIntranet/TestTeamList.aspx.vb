
Partial Class BHAIntranet_TestTeamList
    Inherits System.Web.UI.Page

    Protected Sub chkTeam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim chkList As CheckBoxList = DirectCast(GridView1.Rows(GridView1.SelectedIndex).FindControl("chklistEmployees"), CheckBoxList)
        If chk.Checked = True Then
            formhelper.ProcessCheckboxList(chkList, True)
        End If

    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim gv As GridView = DirectCast(sender, GridView)
        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
        Dim rowIndex As Integer = row.RowIndex
        Dim dr As GridViewRow
        Dim pnl As Panel

        For Each dr In gv.Rows
            pnl = DirectCast(dr.FindControl("pnlTeam"), Panel)
            pnl.CssClass = "hide"
        Next


        If e.CommandName = "Select" Then
            Dim pnlTeam As Panel = DirectCast(gv.Rows(rowIndex).FindControl("pnlTeam"), Panel)
            If pnlTeam.CssClass = "show" Then
                pnlTeam.CssClass = "hide"
            Else
                pnlTeam.CssClass = "show"
            End If

        End If
    End Sub
End Class
