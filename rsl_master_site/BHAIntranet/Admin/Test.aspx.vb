Imports System.Web.Mail
Imports System.IO

Partial Class BHAIntranet_Admin_Test
    Inherits System.Web.UI.Page

  
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Dim bllEmp As New bllEmployees(ASPSession("USERID"))
        Dim bllEmp As New bllEmployees(343)
        Dim sTo As String
        Dim sFrom As String
        Dim sFromName As String
        Dim sSubject As String
        Dim sBody As New StringBuilder

        If bllEmp IsNot Nothing Then

            If bllEmp.Email <> "N/A" Then

                sFrom = "jonswaino@hotmail.com" 'bllEmp.Email
                sFromName = bllEmp.FullName
                sTo = "jon.swain@reidmark.com"

                sSubject = "Suggestion Notification"

                sBody.Append("You have received a Little Acorns suggestion:")
                sBody.AppendLine()
                sBody.AppendLine("From: " + sFromName)
                sBody.AppendLine("Topic: " + "topic")
                sBody.AppendLine("Suggestion: " + "sugg")
                sBody.AppendLine()
                sBody.AppendLine("Please action this suggestion through the suggestions report.")
                sBody.AppendLine()

                Try
                    Dim mmSuggestNotification As New MailMessage
                    mmSuggestNotification.From = sFrom
                    mmSuggestNotification.To = sTo
                    mmSuggestNotification.Subject = sSubject
                    mmSuggestNotification.Body = sBody.ToString

                    SmtpMail.SmtpServer = "localhost"
                    SmtpMail.Send(mmSuggestNotification)

                Catch ex As Exception
                    ErrorHelper.LogError(ex, True)
                End Try
            End If
        End If
    End Sub
End Class
