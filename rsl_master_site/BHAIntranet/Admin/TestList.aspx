<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TestList.aspx.vb" Inherits="BHAIntranet_Admin_TestList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ObjectDataSource ID="odsParentMenu" runat="server" SelectMethod="GetTopLevelMenu" TypeName="bllMenu"></asp:ObjectDataSource>
 			<asp:Repeater id="rptParentRepeater" runat="server" DataSourceID="odsParentMenu">
 			    <HeaderTemplate>
 			    <ul id="intranetnav">
 			    </HeaderTemplate>
				<itemtemplate>
				        <asp:Label ID="lblMenuID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "menuid")%>'></asp:Label>				
						<li>
						<a href="<%#DataBinder.Eval(Container.DataItem, "path")%>"><%#DataBinder.Eval(Container.DataItem, "menutitle")%></a>					
					
		                        <asp:ObjectDataSource id="odsChildMenu" runat="server" SelectMethod="GetMenuByParentID" TypeName="bllMenu">
                                <SelectParameters>
                                <asp:ControlParameter ControlID="lblMenuID"  Name="ParentID" PropertyName="Text"
                                    Type="Int32" />
                                </SelectParameters>
                                </asp:ObjectDataSource>	
                           
                                    <asp:Repeater ID="rptChildRepeater" runat="server" DataSourceID="odsChildMenu">
                                        <HeaderTemplate>
 			                                <ul>
 			                                 </HeaderTemplate>
        				                    <itemtemplate>
						                        <li><a href="<%#DataBinder.Eval(Container.DataItem, "path")%>"><%#DataBinder.Eval(Container.DataItem, "menutitle")%></a></li>
				                            </itemtemplate>
				                            <FooterTemplate>
				                            </ul>
				                            </FooterTemplate>
                                     </asp:Repeater>
                            </li>         
				</itemtemplate>
				<FooterTemplate>
				   </ul>
				</FooterTemplate>
			</asp:Repeater>
			
			

    </div>
    </form>
</body>
</html>
