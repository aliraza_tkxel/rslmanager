Imports dalPage
Imports dalPageTableAdapters
Imports dalMenu
Imports dalMenuTableAdapters
Imports System.Data

Partial Class BHAIntranet_Admin_TestList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub


    Public Function HasChildren(ByVal childcount As Integer, ByVal StrHTML As String) As String

        If childcount > 0 Then
            Return StrHTML
        Else
            Return ""
        End If
    End Function

    Protected Sub rptParentRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptParentRepeater.ItemDataBound

        Dim dv As DataRowView = e.Item.DataItem
        If Not IsNothing(dv) Then
            Dim nestedRepeater As Repeater = CType(e.Item.FindControl("rptChildRepeater"), Repeater)
            If nestedRepeater.Items.Count = 0 Then
                nestedRepeater.Visible = False
            End If
        End If

    End Sub
End Class
