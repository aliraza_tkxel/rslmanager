
Partial Class BHAIntranet_Templates_CMS
    Inherits System.Web.UI.MasterPage



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Public Sub SetPageSpecific(ByVal strCSSPath As String)

        Dim CSSHtmlLink As New HtmlLink
        CSSHtmlLink.Href = "~" & strCSSPath
        CSSHtmlLink.Attributes.Add("rel", "Stylesheet")
        CSSHtmlLink.Attributes.Add("type", "text/css")
        CSSHtmlLink.Attributes.Add("media", "all")
        Page.Header.Controls.Add(CSSHtmlLink)


        Dim CSSBrowser As New HtmlLink

        If Request.Browser.Version = "8.0" Then
            CSSBrowser.Href = "~" & "/BHAIntranet/CSS/General/IE8.css"
        ElseIf Request.Browser.Version = "7.0" Then
            CSSBrowser.Href = "~" & "/BHAIntranet/CSS/General/IE7.css"
        Else
            CSSBrowser.Href = "~" & "/BHAIntranet/CSS/General/IE6.css"
        End If

        CSSBrowser.Attributes.Add("rel", "Stylesheet")
        CSSBrowser.Attributes.Add("type", "text/css")
        CSSBrowser.Attributes.Add("media", "all")
        Page.Header.Controls.Add(CSSBrowser)




    End Sub

End Class

