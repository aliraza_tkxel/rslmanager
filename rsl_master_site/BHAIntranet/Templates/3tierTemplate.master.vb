
Partial Class BHAIntranet_Templates_3tierTemplate
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim strPageName As String

        strPageName = Request.ServerVariables("SCRIPT_NAME")

        If InStr(strPageName, "/") > 0 Then
            strPageName = Right(strPageName, Len(strPageName) - InStrRev(strPageName, "/"))
        End If

        Dim bllPage As New bllPage(strPageName)
        Page.Title = bllPage.PageTitle
        lblHeader.Text = bllPage.PageTitle
        lblContent.Text = bllPage.PageContent

        Dim bltlstSubMenu As BulletedList = CType(Master.FindControl("bltlstSubMenu"), BulletedList)
        bltlstSubMenu.DataTextField = "Name"
        bltlstSubMenu.DataValueField = "Filename"
        bltlstSubMenu.DataSource = bllPage.GetSubMenu(bllPage.PageID)
        bltlstSubMenu.DataBind()
    End Sub
End Class

