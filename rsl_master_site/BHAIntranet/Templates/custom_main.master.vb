
Partial Class BHAIntranet_templates_custom_main
    Inherits System.Web.UI.MasterPage
    Private m_PageID As Integer = 0
    Private m_PageTitle As String
    Public ASPSession As MSDN.mySession
    Dim strPageName As String
    Public WriteOnly Property SearchType() As Integer
        Set(ByVal value As Integer)
            rdoListSearch.SelectedValue = value
            'response.Write("test")
        End Set
    End Property


    Public WriteOnly Property SearchCriteria() As String
        Set(ByVal value As String)
            'txtSearch.Text = value
            Trace.Write("test")
        End Set
    End Property

    Public ReadOnly Property PageID() As Integer
        Get
            Return m_PageID
        End Get
    End Property

    Public ReadOnly Property PageTitle() As String
        Get
            Return m_PageTitle
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        strPageName = Request.ServerVariables("SCRIPT_NAME")

        If InStr(strPageName, "/") > 0 Then
            strPageName = Right(strPageName, Len(strPageName) - InStrRev(strPageName, "/"))
        End If

        Dim CSSBrowser As New HtmlLink

        If Request.Browser.Version = "8.0" Then
            CSSBrowser.Href = "~" & "/BHAIntranet/CSS/General/IE8.css"
        ElseIf Request.Browser.Version = "7.0" Then
            CSSBrowser.Href = "~" & "/BHAIntranet/CSS/General/IE7.css"
        Else
            CSSBrowser.Href = "~" & "/BHAIntranet/CSS/General/IE6.css"
        End If

        CSSBrowser.Attributes.Add("rel", "Stylesheet")
        CSSBrowser.Attributes.Add("type", "text/css")
        CSSBrowser.Attributes.Add("media", "all")
        Page.Header.Controls.Add(CSSBrowser)

        Dim bllPage As New bllPage(strPageName)
        If bllPage.IsDB = True Then
            Page.Title = bllPage.PageTitle
            m_PageTitle = bllPage.PageTitle
            m_PageID = bllPage.PageID
        Else
            Page.Title = "Not Set in Web Manager"
            m_PageTitle = "Not Set in Web Manager"
            Trace.Write("TEST" & m_PageTitle)
            m_PageID = 0
        End If

    
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mainPage As basepage = CType(Me.Page, basepage)
        ASPSession = CType(mainPage.ASPSession, MSDN.mySession)

        If ASPSession("USERID") = "" Then
            'Response.Redirect("/BHAIntranet/Login.aspx?status=timedout")
            Response.Redirect("/BHAIntranet/login.aspx?status=signout")
        End If

        Dim bllPage As New bllPage(strPageName, ASPSession("USERID"))

        If strPageName <> "NoRights.aspx" Then
            If bllPage.HasAccess = False Then
                Response.Redirect("/BHAIntranet/NoRights.aspx")
            End If
        End If


        bltFooter.DataTextField = "Name"
        bltFooter.DataValueField = "Filename"
        bltFooter.DataSource = bllPage.GetFooter(ASPSession("USERID"))
        bltFooter.DataBind()

        Dim dt As DateTime = Now
        lblCurrentDate.Text = dt.ToString("dddd, dd MMMM yyyy HH:mm")
        lblLogInDetails.Text = ASPSession("FIRSTNAME") & " " & ASPSession("LASTNAME") & ": Last Logged In " & ASPSession("LASTLOGGEDIN")
    End Sub

    Public Sub SetPageSpecific(ByVal strCSSPath As String)

        Dim CSSHtmlLink As New HtmlLink
        CSSHtmlLink.Href = "~" & strCSSPath
        CSSHtmlLink.Attributes.Add("rel", "Stylesheet")
        CSSHtmlLink.Attributes.Add("type", "text/css")
        CSSHtmlLink.Attributes.Add("media", "all")
        Page.Header.Controls.Add(CSSHtmlLink)

    End Sub
    Public Sub SetSearchType(ByVal intSearchType As Integer)
        rdoListSearch.SelectedValue = intSearchType

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim intSearch As Integer = rdoListSearch.SelectedValue
        Dim strSearch As String = txtSearch.Text
        sessionhelper.SearchType = rdoListSearch.SelectedValue


        If intSearch = 1 Then
            Response.Redirect("/BHAIntranet/CustomPages/SiteSearch.aspx?search=" & txtSearch.Text)
            Response.Write("/BHAIntranet/CustomPages/DocSearch.aspx?search=" & txtSearch.Text)
        End If
        If intSearch = 2 Then
            Response.Redirect("/BHAIntranet/CustomPages/DocSearch.aspx?search=" & txtSearch.Text)
            Response.Write("/BHAIntranet/CustomPages/DocSearch.aspx?search=" & txtSearch.Text)
        End If
        If intSearch = 3 Then
            Response.Redirect("/BHAIntranet/CustomPages/Employees.aspx?search=" & txtSearch.Text)
            Response.Write("/BHAIntranet/CustomPages/Employees.aspx?search=" & txtSearch.Text)
        End If
        If intSearch = 4 Then
            Response.Redirect("/BHAIntranet/CustomPages/Suppliers.aspx?search=" & txtSearch.Text)
            Response.Write("/BHAIntranet/CustomPages/Employees.aspx?search=" & txtSearch.Text)
        End If


    End Sub

    Protected Sub lnkSignOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSignOut.Click
        Response.Redirect("/BHAIntranet/login.aspx?status=signout")
    End Sub

    Protected Sub lnkHome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHome.Click
        Response.Redirect("/BHAIntranet/CustomPages/MyWhiteboard.aspx")
    End Sub
End Class

