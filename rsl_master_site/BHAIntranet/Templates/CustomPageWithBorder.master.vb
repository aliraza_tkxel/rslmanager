
Partial Class BHAIntranet_Templates_CustomPageWithBorder
    Inherits System.Web.UI.MasterPage
    Private m_SearchType As Integer
    Private m_SearchCriteria As String
    Public ASPSession As MSDN.mySession
    Public WriteOnly Property SetHeading() As String
        Set(ByVal value As String)
            lblHeader.Text = Value
        End Set
    End Property
    Public WriteOnly Property SetSearchType() As Integer
        Set(ByVal value As Integer)
            m_SearchType = value
        End Set
    End Property
    Public WriteOnly Property SearchCriteria() As String
        Set(ByVal value As String)
            m_SearchCriteria = value
        End Set
    End Property

    Public Sub SetPageSpecific(ByVal strCSS As String)
        Dim mainmaster As BHAIntranet_templates_custom_main = CType(Me.Master, BHAIntranet_templates_custom_main)
        mainmaster.SetPageSpecific(strCSS)

    End Sub
    Public Sub SetSubMenuVisibility(ByVal blnVisibility As Boolean)
        Dim mainmaster As BHAIntranet_templates_custom_main = CType(Me.Master, BHAIntranet_templates_custom_main)
        Dim blt As BulletedList = CType(mainmaster.FindControl("bltlstSubMenu"), BulletedList)
        blt.Visible = blnVisibility
    End Sub


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim mainmaster As BHAIntranet_templates_custom_main = CType(Me.Master, BHAIntranet_templates_custom_main)
        mainmaster.SearchType = m_SearchType


        lblHeader.Text = mainmaster.PageTitle
        Trace.Write("Test" & mainmaster.PageTitle & "T:" & mainmaster.PageID)

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mainmaster As BHAIntranet_templates_custom_main = CType(Me.Master, BHAIntranet_templates_custom_main)
        mainmaster.SearchCriteria = m_SearchCriteria
        Trace.Write("BorderLoad" & m_SearchCriteria)


        Dim mainPage As basepage = CType(Me.Page, basepage)
        ASPSession = CType(mainPage.ASPSession, MSDN.mySession)

        If ASPSession("USERID") = "" Then
            Response.Redirect("/BHAIntranet/login.aspx?status=signout")
        End If

        Dim bllPage As New bllPage
        Dim bltlstSubMenu As BulletedList = CType(Master.FindControl("bltlstSubMenu"), BulletedList)
        bltlstSubMenu.DataTextField = "Name"
        bltlstSubMenu.DataValueField = "Filename"
        bltlstSubMenu.DataSource = bllPage.GetSubMenuByEmpID(mainmaster.PageID, ASPSession("USERID"))
        bltlstSubMenu.DataBind()
    End Sub
End Class

