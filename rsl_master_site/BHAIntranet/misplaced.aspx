﻿<%@ Page Language="VB" MasterPageFile="~/BHAIntranet/login.master" AutoEventWireup="false"
    CodeFile="misplaced.aspx.vb" Inherits="BHAIntranet_misplaced" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $.watermark.options.useNative = false;
            $('<%= "#" +  txtEmail.ClientID %>').watermark("Email");
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="fpass">
        <p>
            <strong>Forgotten your password?</strong>
        </p>
        <p>
            Please enter your designated email and your password will be sent to you.
        </p>
        <br />
    </div>
    <div>
        <asp:TextBox runat="server" class="txtfield email" ID="txtEmail" />
        <br />
        <asp:RequiredFieldValidator ErrorMessage="Kindly provide a valid Email address." ControlToValidate="txtEmail" runat="server"
            Style="vertical-align: top" Display="Dynamic" />
        <asp:RegularExpressionValidator ErrorMessage="Kindly provide a valid Email address."
            ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            runat="server" Display="Dynamic" />
    </div>
    <div class="btn-area">
        <asp:Button Text="Submit" runat="server" class="btn" ID="btnSubmit" />
    </div>
    <asp:Panel runat="server" ID="pnlMessage" Visible="false">
        <asp:Label ID="lblMessage" runat="server" />
    </asp:Panel>
    <div class="login-again">
        <strong><a href="../default.asp">Try to login again</a></strong>
    </div>
</asp:Content>
