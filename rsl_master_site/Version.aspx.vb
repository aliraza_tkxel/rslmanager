Imports System.Web.Configuration
Imports System.Globalization

Partial Class Version
    Inherits basepage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            GetWebConfigData("Build.Number", lblBuildNumber, "")
            GetWebConfigData("Build.Date", lblBuildDate, "Date")
            GetWebConfigData("Build.Time", lblBuildTime, "")
            GetWebConfigData("Build.Project", lblBuildProject, "")
        End If
    End Sub

    Private Sub GetWebConfigData(ByVal Section As String, ByVal ConfigLabel As Label, ByVal ConfigFormat As String)
        Dim config2 As String = ConfigurationManager.AppSettings(Section)
        If ConfigFormat = "Date" Then
            Dim mydt As String = DateTime.Parse(config2.ToString()).ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern)
            ConfigLabel.Text = mydt
        Else
            ConfigLabel.Text = config2.ToString()
        End If
    End Sub
End Class
