<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim isDirectorCode, director_sql
	
	// if the user is a director/ manager show only their team
	director_sql = ""		' default to empty
	isDirectorCode = Request("HFKS023")
	if isDirectorCode = "" Then isDirectorCode = 0 End If
	if isDirectorCode Then 
		director_sql =  get_director_sql() 
	end If  
		' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	getTeams()
	
	// build string to be used in 'IN 'statement in SQL containing IDs of directors teams
	Function get_director_sql()

		if not ((Session("TeamName") =  "HUMAN RESOURCES") or (Session("USERID") =  35)) then
		
			Dim str_team_ids
			str_team_ids = ""
			OpenDB()
			SQL = 	"SELECT T.TEAMID  " &_
					"FROM E__EMPLOYEE E " &_
					"	INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
					"	LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
					"WHERE	(LINEMANAGER = " & Session("userid") & " ) OR (DIRECTOR = " & Session("userid") & " OR MANAGER = " & Session("userid") & ") "
			
			Call OpenRS(rsSet, SQL)
			'rw " ---   1   --- <BR> "
			str_team_ids = str_team_ids & " AND TEAMID IN ("
			if not rsSet.EOF then
			
					while not rsSet.EOF 
					
						str_team_ids = str_team_ids & rsSet(0)
						rsSet.movenext()
						if not rsSet.EOF Then str_team_ids = str_team_ids & ", " end if
					Wend
			else
			
				str_team_ids = str_team_ids & "-1"
			End If
			str_team_ids = str_team_ids & ")"
			get_director_sql = str_team_ids
			'rw get_director_sql
			'rw get_director_sql
			CloseDB()
		End If
		
		
		
		
	End Function
		
	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 


		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT  T.TEAMID, T.TEAMNAME, " &_
					"(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM, " &_
					"(SELECT ISNULL(CAST(SUM(HOLIDAYENTITLEMENTDAYS)AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS LEAVE, " &_
					"(SELECT ISNULL(CAST(SUM(SALARY)AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS SALARY, " &_
					"(SELECT ISNULL(CAST(SUM(DURATION) AS NVARCHAR),'0') FROM 	E_JOURNAL E	LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID	LEFT JOIN E_ABSENCE A ON E.JOURNALID = A.JOURNALID AND A.STARTDATE >= '1 JAN 2006' AND A.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = A.JOURNALID) WHERE ITEMNATUREID = 1 AND DURATION IS NOT NULL AND TEAM = T.TEAMID) AS SICKNESSABSENCE "&_
					"FROM	E_TEAM T WHERE T.TEAMID <> 1 " & get_director_sql & " ORDER BY T.TEAMNAME "
		
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
			
				str_data = str_data & "<TR STYLE='CURSOR:HAND' ONCLICK='load_myteam(" & rsSet("TEAMID") & "," & isDirectorCode & ")'>" &_
						"<TD WIDTH=290PX>" & rsSet("TEAMNAME") & "</TD>" &_
						"<TD ALIGN=CENTER WIDTH=90PX>" & rsSet("TEAMNUM") &	"</TD>" &_
						"<TD ALIGN=CENTER WIDTH=90PX>" & rsSet("LEAVE") & "</TD>" & _
						"<TD ALIGN=CENTER WIDTH=90PX>" & rsSet("SICKNESSABSENCE") &"</TD>" 
					' MAKE SURE YOU DONT TRY TO DEVIDE BY ZERO
					If rsSet("TEAMNUM") > 0 then
						str_data = str_data & "<TD ALIGN=CENTER WIDTH=90PX>" & FormatNumber(IsNullNumber(  ( rsSet("SALARY") / 365) * rsSet("SICKNESSABSENCE")) /  rsSet("TEAMNUM")  ) & "</TD>"
					Else
						str_data = str_data & "<TD ALIGN=CENTER WIDTH=90PX>" & FormatNumber(IsNullNumber(  ( rsSet("SALARY") / 365) * rsSet("SICKNESSABSENCE")) ) & "</TD>"
					
					End If
						str_data = str_data & "<TD ALIGN=RIGHT WIDTH=90PX>" & FormatCurrency(rsSet("SALARY")) & "</TD>"
						
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
					
			Next
			
			get_orphans()
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A HREF = 'ESTABLISHMENT.asp?page=1'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'ESTABLISHMENT.asp?page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & (intRecordCount + 1) &_
			" <A HREF='ESTABLISHMENT.asp?page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='ESTABLISHMENT.asp?page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"

		End If
	
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=7 ALIGN=CENTER><B>No teams exist within the system !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=7 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function

	
	Function get_orphans()
	
		strSQL = 	"SELECT 	-1 AS TEAMID, 'No team' AS TEAMNAME,	COUNT(*) AS NOTEAMCOUNT " &_
					"FROM	 	E__EMPLOYEE E " &_
					"		 	LEFT JOIN E_JOBDETAILS T ON E.EMPLOYEEID = T.EMPLOYEEID " &_
					"WHERE		T.TEAM IS NULL"

		//RESPONSE.WRITE STRSQL
		set rsOrphan = Server.CreateObject("ADODB.Recordset")
		rsOrphan.ActiveConnection = RSL_CONNECTION_STRING 			
		rsOrphan.Source = strSQL
		rsOrphan.CursorType = 3
		rsOrphan.CursorLocation = 3
		rsOrphan.Open()
		
		while not rsOrphan.EOF
			
			count = count + 1
			str_data = str_data & "<TR STYLE='CURSOR:HAND' ONCLICK='load_myteam(" & rsOrphan("TEAMID") & ")'>" &_
					"<TD WIDTH=200PX STYLE='COLOR:BLUE'>" & rsOrphan("TEAMNAME") & "</TD>" &_
					"<TD ALIGN=CENTER WIDTH=90PX>" & rsOrphan("NOTEAMCOUNT") &	"</TD>" &_
					"<TD COLSPAN=6>&nbsp;</TD></TR>"

			rsOrphan.movenext()
		
		Wend
		
		rsOrphan.close()
		Set rsOrphan = Nothing
	
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --> HR Tools --> Establishment</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function load_myteam(team_id, isAdirector){
	
		if (isAdirector == 1)
			location.href = "man_team.asp?bypass=1&team_id=" + team_id;
		else
			location.href = "myteam.asp?team_id=" + team_id;
	
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->


<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2><IMG NAME="tab_establishment" TITLE='Team Overview' SRC="images/establishment.gif" WIDTH=115 HEIGHT=20 BORDER=0></TD>
		<TD><IMG SRC="images/spacer.gif" WIDTH=635 HEIGHT=19></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=635 HEIGHT=1></TD>
	</TR>
</TABLE>

<TABLE WIDTH=750 CELLPADDING=1 CLASS="TAB_TABLE" CELLSPACING=2 STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
	<THEAD>
	<TR> 
		<TD WIDTH=200PX CLASS='TABLE_HEAD'> <B>Team Name</B> </TD>
		<TD WIDTH=90PX ALIGN=CENTER CLASS='NO-BORDER'> <B>N� Emps</B> </TD>
		<TD WIDTH=90PX ALIGN=CENTER CLASS='NO-BORDER'> <B>Total Leave</B> </TD>
		<TD WIDTH=90PX ALIGN=CENTER CLASS='NO-BORDER'> <B>Sickness Absence</B> </TD>
		<TD WIDTH=90PX ALIGN=CENTER CLASS='NO-BORDER'> <B>Cost of Absence</B> </TD>
		<TD WIDTH=90PX ALIGN=CENTER CLASS='NO-BORDER'> <B>Total Salary</B> </TD>
	</TR>
	<TR STYLE='HEIGHT:3PX'>	<TD COLSPAN=7 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		 <%=str_data%>
</TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

