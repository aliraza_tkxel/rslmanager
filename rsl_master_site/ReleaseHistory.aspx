<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReleaseHistory.aspx.vb"
    Inherits="ReleaseHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Release History</title>     
    <style type="text/css">
    body
    {
        font-family:arial;
        font-size:10px;
    }
    h1
    {
        color:white;
        font-size:14px;
        padding:15px;
    }
    .trHeader
    {
         background-color: lightgrey;
        height:20px;
    }
    .trHeader *
    {
        height:20px;
        font-weight:bold;
        padding:5px;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" style="">
                <tr>
                    <td style="height: 10px; background-color: #cc0000">
                        <h1>
                            AHV_RSLManager Release History</h1>
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="gvReleaseHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />                
                <Columns>                    
                    <asp:TemplateField HeaderText="Release">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlkRelease" runat="server" NavigateUrl='<%# GetReleasePath(Eval("FullName")) %>'
                                Target="_blank" Text='<%# System.IO.Path.GetFileName(Eval("FullName")) %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                
            </asp:GridView>
            <br />
            <asp:HyperLink runat="server" ID="hlkBack" NavigateUrl="~/version.aspx" Text="<< Back">
            </asp:HyperLink>
        </div>
    </form>
</body>
</html>
