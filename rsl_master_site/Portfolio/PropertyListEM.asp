	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	OpenDB()
	SQL = "SELECT SCHEMENAME FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & REQUEST("DEVID")
	Call OpenRs(rsDEV,SQL)
	THE_DEV_NAME = rsDEV("SCHEMENAME")
	Call CloseRs(rsDEV)

	if (Request("blockid") = "") then
		THE_DEV_NAME = THE_DEV_NAME & "   &nbsp;&nbsp;</font><i>BLOCK :</i> <font color=blue> NON BLOCK ALIGNED"
	else
		SQL = "SELECT BLOCKNAME FROM P_BLOCK WHERE BLOCKID = " & REQUEST("BLOCKID")
		Call OpenRs(rsDEV,SQL)
		THE_DEV_NAME = THE_DEV_NAME & "   &nbsp;&nbsp;</font>BLOCK : <font color=blue>" & rsDEV("BLOCKNAME")
		Call CloseRs(rsDEV)
	end if
	CloseDB()
		
	DEVID = Request("DEVID")
	BLOCKID = Request("BLOCKID")

	searchName = Replace(Request("propid"),"'","''")
	searchSQL = ""
	if searchName <> "" Then
		searchSQL = " AND (P.PROPERTYID LIKE '%" & searchName & "%' ) "
	End If
	
	DIM CONST_PAGESIZE
	IF (Request("blockid") = "") THEN
		CONST_PAGESIZE = 20
	ELSE
		CONST_PAGESIZE = 9
	END IF	
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match
	Dim TDFunc         (6)	 'stores any functions that will be applied
	Dim SubQuery			 'used to get the name of the parent object
	Dim SubQueryTitle		 'the title of the parent object
	
	ColData (0) = "Ref No:|PROPERTYID|80"
	SortASC (0)	= "P.PROPERTYID ASC"
	SortDESC(0) = "P.PROPERTYID DESC"
	TDSTUFF (0) = ""
	TDFunc  (0) = ""

	ColData (1) = "Status|STATUS|110"
	SortASC (1)	= "STATUS ASC"
	SortDESC(1) = "STATUS DESC"	
	TDSTUFF (1) = " NOWRAP "
	TDFunc  (1) = ""		

	ColData (2) = "Property Type|PROPERTYTYPE|140"
	SortASC (2)	= "PROPERTYTYPE ASC"
	SortDESC(2) = "PROPERTYTYPE DESC"	
	TDSTUFF (2) = ""
	TDFunc  (2) = ""		

	ColData (3) = "Street|ADDRESS10|190"
	SortASC (3)	= "HOUSENUMBER, ADDRESS10"
	SortDESC(3) = "HOUSENUMBER DESC, ADDRESS10 DESC"	
	TDSTUFF (3) = " NOWRAP "
	TDFunc  (3) = ""		

	ColData (4) = "Town /City|TOWNCITY|120"
	SortASC (4)	= "TOWNCITY ASC"
	SortDESC(4) = "TOWNCITY DESC"	
	TDSTUFF (4) = ""
	TDFunc  (4) = ""		

	ColData (5) = "Beds|BEDROOMS|60"
	SortASC (5)	= "BEDROOMS ASC"
	SortDESC(5) = "BEDROOMS DESC"	
	TDSTUFF (5) = ""	
	TDFunc  (5) = ""
	
	ColData (6) = "TEXT|<IMG SRC=""/MYIMAGES/INFO.GIF"" BORDER=""0"" >|20"
	SortASC (6)	= ""
	SortDESC(6) = ""	
	TDSTUFF (6) = " "" STYLE=""""BACKGROUND-COLOR:#FFFFFF"""" ONCLICK=""""DisplayProperty('"" & rsSet(""PROPERTYID"") & ""')"""" """	
	TDFunc  (6) = ""

	PageName = "PropertyListEM.asp"
	EmptyText = "No properties found for the selected block!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""ONCLICK=""""load_me('"" & rsSet(""PROPERTYID"") & ""')"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()


	If Request.QueryString("BLOCKID") = "" Then
		BlockSQL = " AND P.BLOCKID IS NULL "
	Else
		if (IsNumeric(Request.QueryString("BLOCKID"))) then
			BlockSQL = " AND P.BLOCKID = " & Request.QueryString("BLOCKID") & " "
		else
			BlockSQL = " AND P.BLOCKID IS NULL "
		end if
	END IF
	
	If Request.QueryString("DEVID") = "" Then
		DevelopmentID = -1	
	Else
		if (IsNumeric(Request.QueryString("DEVID"))) then
			DevelopmentID = CInt(Request.QueryString("DEVID"))
		else
			DevelopmentID = -1			
		end if
	END IF
	
	SQLCODE = "SELECT P.PROPERTYID, D.DEVELOPMENTNAME, S.DESCRIPTION AS STATUS, PT.DESCRIPTION AS PROPERTYTYPE, " &_
			  "LTRIM(P.HOUSENUMBER + ' ' + ISNULL(P.FLATNUMBER,'') + ' ' + ISNULL(P.ADDRESS1, P.ADDRESS2)) AS ADDRESS10, " &_
			  "ISNULL(P.ADDRESS1, P.ADDRESS2) AS ADDRESS1, P.TOWNCITY, ISNULL(A.PARAMETERVALUE,0) AS BEDROOMS FROM P__PROPERTY P " &_
			  "LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			  "LEFT JOIN P_STATUS S ON P.STATUS = S.STATUSID " &_
			  "LEFT JOIN P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P.PROPERTYTYPE " &_
			  "Left outer Join PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P.PROPERTYID AND A.ITEMPARAMID = (" &_
									" SELECT ItemParamID from PA_ITEM_PARAMETER" &_
									" INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID" &_
									" Where ParameterName='Bedrooms')" &_

			  "WHERE P.DEVELOPMENTID = " & DevelopmentID & " " & BlockSQL & " " & searchSQL &_
			  "Order By " + Replace(orderBy, "'", "''") + ""
	 //response.write SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --> Estate Maintenance --> Block</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Loading.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(prop_id){
	location.href = "PRM.asp?PROPERTYID=" + prop_id
	}

function quick_find(){
	location.href = "<%=PageName & "?DEVID=" & Request("DevID") & "&DevSort=" & Request("DevSort") & "&DevPage=" & Request("DevPage") & "&name=" & Request("name")%>&CC_Sort=<%=Server.UrlEncode(SortASC(i))%>&propid="+thisForm.findemp.value + "&blockname=<%=Request("blockname")%>&BlockPage=<%=Request("BlockPage")%>&BlockSort=<%=Request("BlockSort")%>";
	}

	var JOURNALID
	var iFrameArray = new Array ()								
	iFrameArray[70] = "iRepairJournalBlo"
	iFrameArray[71] = "iAccountBlo"
	iFrameArray[72] = "iPurchasesBlo"	
	iFrameArray[73] = "iRepairDetailsBlo"	
								
	var MAIN_OPEN_BOTTOM_FRAME = 70
	var MASTER_OPEN_WORKORDER = ""
	var MASTER_OPEN_PURCHASEORDER = ""	
		
	var LOADER_FRAME_APPEND
	// swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
	// where is either 'top' or 'bottom' relating to the tab area of the page
	// FULL RELOAD FUNCTIONALITY NEEDS TO BE IMPLEMETED TO RELOAD ALL DYNAMIC DIVS -- PDP
	function swap_div(int_which_one, str_where){
		
		var divid, imgid, upper
		
		upper = 73;	lower = 70;
		//STARTLOADER('BOTTOM')
		MAIN_OPEN_BOTTOM_FRAME = int_which_one			
		BLO_BOTTOM_FRAME.location.href = "/portfolio/iframes/" + iFrameArray[int_which_one] + ".asp?blockid=<%=blockid%>&CWO=" + MASTER_OPEN_WORKORDER + "&CPO=" + MASTER_OPEN_PURCHASEORDER;

		imgid = "img" + int_which_one.toString();
		
		// manipulate images
		for (j = lower ; j <= upper ; j++)
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
		
		// unless last image in row
		if (int_which_one != upper)
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
	}

	function synchronize_tabs(int_which_one, str_where){
		var divid, imgid, upper
		upper = 73;	lower = 70;
		
		imgid = "img" + int_which_one.toString();
		for (j = lower ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
			}
		if (int_which_one != upper)
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
		}
	
	function work_order(){
		window.showModelessDialog("popups/WorkOrderBlo.asp?blockid=<%=blockid%>&Random=" + new Date(),"","dialogHeight: 540px; dialogWidth: 790px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		}			

	function purchase_order(){
		window.showModelessDialog("popups/PurchaseOrderBlo.asp?blockid=<%=blockid%>&Random=" + new Date(),"","dialogHeight: 555px; dialogWidth: 800px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		}			

	function DisplayProperty(PropID){
		event.cancelBubble = true
		window.open("/Portfolio/PopUps/Property_Details.asp?PropertyID=" + PropID, "PropertyDetails", "width=400,height=300")
		}

	function cyc_repair(){
		window.showModelessDialog("popups/CycRepairBlo.asp?blockid=<%=blockid%>&Random=" + new Date(),"","dialogHeight: 555px; dialogWidth: 800px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		}			
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
	  <TD ROWSPAN=2><a href="javascript:location.href='EstateMaintenance.asp?CC_Sort=<%=Request("DevSort")%>&page=<%=Request("DevPage")%>&name=<%=Request("name")%>'"><IMG NAME="tab_employees" TITLE='Scheme List' src="images/scheme_list-previous.gif" width=95 HEIGHT=20 BORDER=0></a></TD>
	  <TD ROWSPAN=2><a href="javascript:location.href='BlockList.asp?CC_Sort=<%=Request("BlockSort")%>&page=<%=Request("BlockPage")%>&blockname=<%=Request("blockname")%>&DevSort=<%=Request("DevSort")%>&DevPage=<%=Request("DevPage")%>&name=<%=Request("name")%>&DEVID=<%=Request("DEVID")%>'"><IMG NAME="tab_employees" TITLE='Scheme' SRC="images/scheme-previous.gif" WIDTH=67 HEIGHT=20 BORDER=0></a></TD>
	  <TD ROWSPAN=2><IMG NAME="tab_employees" TITLE='Property List' SRC="images/tab_block-over.gif" WIDTH=54 HEIGHT=20 BORDER=0></TD>
     <TD ROWSPAN=2><IMG NAME="tab_block" TITLE='Go back to Development List' SRC="images/TabEnd.gif" WIDTH=8 HEIGHT=20 BORDER=0></TD>
		<TD><IMG SRC="images/spacer.gif" WIDTH=230 HEIGHT=19></TD>
		<td nowrap><INPUT TYPE='BUTTON' name='btn_FIND' title='Search for matches using Block Name.' CLASS='RSLBUTTON' VALUE='Quick Find' ONCLICK='quick_find()'>
					&nbsp;<input type=text  size=18 name='findemp' class='textbox200' value="<%=searchName%>">
		</td>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71 colspan=2><IMG SRC="images/spacer.gif" WIDTH=230 HEIGHT=1></TD>
	</TR>
	<tr><td style='border-left:1px solid #133e71;border-right:1px solid #133e71;text-transform:uppercase' COLSPAN=6 height=20>&nbsp;<b><i>Scheme :</i> <font color=blue><%=THE_DEV_NAME%></font></b></td></tr>	
</TABLE>
<%=TheFullTable%>
<%IF (Request("blockid") <> "") THEN %>
<br>
	<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img70" SRC="images/70-open.gif" WIDTH=54 HEIGHT=20 BORDER=0  onclick="swap_div(70, 'bottom')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2 valign=bottom><IMG NAME="img71" SRC="images/71-previous.gif" WIDTH=117 HEIGHT=20 BORDER=0 onclick="swap_div(71, 'bottom')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2 valign=bottom><IMG NAME="img72" SRC="images/72-closed.gif" WIDTH=145 HEIGHT=20 BORDER=0 onclick="swap_div(72, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom><IMG NAME="img73" SRC="images/73-closed.gif" WIDTH=77 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
        
      <TD WIDTH=100 ALIGN=CENTER> <img src="/myImages/Repairs/sbasket.gif" title="Create new Work Order" width="27" height="22" style='cursor:hand' ONCLICK='work_order()'> 
        <img src="/myImages/Repairs/porder.gif" title="Create new Purchase Order" width="27" height="22" style='cursor:hand' ONCLICK='purchase_order()'>
		<img src="/myImages/Repairs/sCyc.gif" title="Create cyclical repair" width="27" height="22" style='cursor:hand' onClick='cyc_repair()'></TD>				
		<TD align=right height=19>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004174 colspan=2><IMG SRC="images/spacer.gif" WIDTH=355 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=BOTTOM_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=0 border=0 CLASS="TAB_TABLE" >
	  <TR> 
		<TD><iframe name=BLO_BOTTOM_FRAME src="iFrames/iRepairJournalBlo.asp?Blockid=<%=blockid%>" width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
	  </TR>
	</TABLE>
</DIV>
<DIV ID=BOTTOM_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:210' WIDTH=750 HEIGHT=210>
<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=10 CELLSPACING=10 border=0 CLASS="TAB_TABLE">
    <TR>
		<TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_BOTTOM"></DIV></b></TD>
	</TR>		
</TABLE>
</DIV>
<input name="null" type="hidden">
<% END IF %>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>