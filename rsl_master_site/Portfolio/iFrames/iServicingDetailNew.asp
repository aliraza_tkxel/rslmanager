<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, gas_history_id, Repair_Title, last_status, isDisabled, ButtonValue, appliancetypeid, propertyid

    journal_id = Request("journalid")
	nature_id = Request("natureid")

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

	    cnt = 0

	  strSQL=" SELECT ISNULL(CONVERT(NVARCHAR, GJH.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), GJH.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, "&_
	         "        CASE  "&_
			 "             WHEN GJH.LETTERACTIONID IS NOT NULL THEN CLA.DESCRIPTION "&_
			 "             WHEN GJH.APPOINTMENTID IS NOT NULL THEN CG.DESCRIPTION   "&_
			 "        ELSE CS.DESCRIPTION  "&_
		     "        END AS STAGE, "&_
		     "        CASE "&_
			 "             WHEN  GJH.LETTERID IS NOT NULL THEN CSL.LETTERDESC  "&_
             "             WHEN GJH.LETTERACTIONID=108 THEN '' "&_
             "             WHEN GJH.LETTERACTIONID=109 THEN '' "&_
			 "            ELSE 'Appointment TBC'   "&_
		     "        END AS DOCUMENT_DESC,ISNULL(GJH.NOTES,'N/A') AS NOTES,'BRS' AS CONTRACTOR, "&_
		     "        GJH.APPOINTMENTDATE AS APPOINTMENTDATE, "&_
		     "        CS.DESCRIPTION AS STATUS,CS.ITEMSTATUSID,GJH.GASHISTORYID,  "&_
		     "        ISNULL(GJH.LETTERID,0) AS LETTERID,GJH.LETTERCONTENT AS LETTERCONTENT,ISNULL(GJH.LETTERACTIONID,0) LETTERACTIONID, "&_
             "        GPA.PROPERTYID,ISNULL(GPA.APPLIANCETYPEID,0) AS APPLIANCETYPEID "&_
             " FROM P_GASJOURNALHISTORY GJH "&_
	         "      LEFT JOIN C_GASSERVICINGAPPOINTMENT CG ON CG.APPOINTMENTID=GJH.APPOINTMENTID  "&_
	         "      LEFT JOIN C_LETTERACTION CLA ON CLA.ACTIONID=GJH.LETTERACTIONID  "&_
	         "      LEFT JOIN C_STANDARDLETTERS CSL ON CSL.LETTERID=GJH.LETTERID "&_
	         "      LEFT JOIN C_STATUS CS ON CS.ITEMSTATUSID=GJH.STATUSID  "&_
             "      INNER JOIN P_GASJOURNAL GJ ON GJ.JOURNALID=GJH.JOURNALID  "&_
             "      LEFT JOIN (SELECT PROPERTYID,MAX(APPLIANCETYPEID) AS APPLIANCETYPEID FROM GS_PROPERTY_APPLIANCE GROUP BY PROPERTYID) GPA ON GPA.PROPERTYID=GJ.PROPERTYID "&_
	         " WHERE GJH.JOURNALID =" & journal_id & " "&_
             " ORDER BY GJH.GASHISTORYID DESC "

		Call OpenRs (rsSet, strSQL)
		last_action = -1
		str_journal_table = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"" valign=""top"">"
			Else
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				Repair_Title = "Annual Gas Servicing"
				gas_history_id = rsSet("GASHISTORYID")
				last_status = rsSet("STATUS")
				last_action = rsSet("ITEMSTATUSID")
                appliancetypeid=rsSet("APPLIANCETYPEID")
                propertyid=rsSet("PROPERTYID")
			End If
				str_journal_table = str_journal_table & "<td>" & rsSet("CREATIONDATE") & "</td>" &_
														"<td>" & rsSet("STAGE") & "</td>" &_
														"<td>" & rsSet("DOCUMENT_DESC") & "</td>" &_
														"<td>" & rsSet("APPOINTMENTDATE")  & "</td>" &_
														"<td>" & rsSet("NOTES")  & "</td>" &_
														"<td>" & rsSet("CONTRACTOR")  & "</td>"

            	If rsSet("LETTERID") <> 0 AND rsSet("LETTERCONTENT") <> "" Then
					str_journal_table = str_journal_table & "<td><img src=""../Images/attach_letter.gif"" name=""OpenLetter"" id=""OpenLetter"" style=""cursor:pointer"" title=""Open Letter"" onclick=""open_letter("& rsSet("GASHISTORYID") & "," & rsSet("LETTERID") & ")"" /></td>"
				ElseIf rsSet("LETTERACTIONID") <> 0 AND rsSet("LETTERACTIONID") = 109 Then
                    str_journal_table = str_journal_table & "<td><img src=""../Images/attach_letter.gif"" name=""OpenLGSR"" id=""OpenLGSR"" style=""cursor:pointer"" title=""Open LGSR"" onclick=""DisplayDoc('" & rsSet("PROPERTYID") & "'," & rsSet("APPLIANCETYPEID") & ")"" /></td>"
                ELse
					str_journal_table = str_journal_table & "<td> </td>"
				End If
				str_journal_table = str_journal_table &  "<tr>"
			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist.</td></tr></tfoot>"
		End If

		ButtonValue = " Update "
		isDisabled = ""
		SQL = "SELECT ITEMSTATUSID,DESCRIPTION FROM C_STATUS WHERE ITEMSTATUSID = " & last_action
		Call OpenRs(rsCheckOption,SQL)
		If (NOT rsCheckOption.EOF) Then
			If (rsCheckOption("ITEMSTATUSID") = "11") Then
				isDisabled = " disabled"
				ButtonValue = " No Options "
			End If
		End If
		Call CloseRs(rsCheckOption)

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">
        function update_repair(gashistoryid) {
            var str_win = "../PopUps/pGasServicingNew.asp?gashistoryid=" + gashistoryid + "&natureid=<%=nature_id%>&appliancetypeid=<%=appliancetypeid%>&propertyid=<%=propertyid%>";
            window.open(str_win, "display", "width=407,height=320, left=200,top=200");
        }

        function open_letter(gashistoryid, letter_id) {
            var tenancy_id = parent.parent.MASTER_TENANCY_NUM
            window.open("../Popups/pView_letter_previewnew.asp?gashistoryid=" + gashistoryid + "&letterid=" + letter_id, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
        }

        function DisplayDoc(propertyid, appliancetypeid) {
            window.open("../PopUps/pCP12Document.asp?propertyid=" + propertyid + "&appliancetypeid=" + appliancetypeid, "_blank", null);
        }
    </script>
</head>
<body class="TA" onload="parent.synchronize_tabs(14,'BOTTOM');parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        border="0">
        <thead>
            <tr>
                <td colspan="6">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td width="100%">
                                <b>Repair Information:</b>&nbsp;<%=Repair_Title%>
                            </td>
                            <td nowrap="nowrap">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>&nbsp;
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" value="<%=ButtonValue%>"
                                    style="background-color: #f5f5dc" onclick='update_repair(<%=gas_history_id%>)'
                                    <%=isDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid">
                    Date
                </td>
                <td style="border-bottom: 1px solid">
                    Stage
                </td>
                <td style="border-bottom: 1px solid">
                    Document
                </td>
                <td style="border-bottom: 1px solid">
                    Appointment Date
                </td>
                <td style="border-bottom: 1px solid">
                    Notes
                </td>
                <td style="border-bottom: 1px solid">
                    Contractor
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
