<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	property_id = Request("propertyid")
	HISTORYTYPE = Request("sel_HISTORYTYPE")
	ReDim aRentValues(0)
	ReDim aRentNames(0)
	Dim valueNum
	OpenDB()
	
	SQL = 	"SELECT " &_
					"FH.RENT, FH.SERVICES, FH.COUNCILTAX, FH.DATERENTSET, FH.RENTEFFECTIVE, "&_
					"FH.TARGETRENT, TL.DESCRIPTION + ' ' + E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME "&_
			"FROM P_FINANCIAL FH "&_
					"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = FH.PFUSERID "&_
					"	LEFT JOIN G_TITLE TL ON TL.TITLEID = E.TITLE "&_
			"WHERE PROPERTYID = '" & property_id & "' "	&_	
	"union SELECT " &_
					"FH.RENT, FH.SERVICES, FH.COUNCILTAX, FH.DATERENTSET, FH.RENTEFFECTIVE, "&_
					"FH.TARGETRENT, TL.DESCRIPTION + ' ' + E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME "&_
			"FROM P_FINANCIAL_HISTORY FH "&_
					"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = FH.PFUSERID "&_
					"	LEFT JOIN G_TITLE TL ON TL.TITLEID = E.TITLE "&_
			"WHERE PROPERTYID = '" & property_id & "' ORDER BY DATERENTSET"
	
		'response.write SQL
		Call OpenRs(rsLoader, SQL)
		
		if (NOT rsLoader.EOF) then
		valueNum = 0
			WHILE NOT rsLoader.EOF
			
				'' create arrays for the graph to form
				Redim Preserve aRentValues(valueNum)
				Redim Preserve aRentNames(valueNum)
				
				aRentValues(valueNum) = rsLoader("rent") 
				aRentNames(valueNum) = valueNum
				
				'' Create Legend Table information
				legenTable = legenTable & "<tr>" &_
					"<td align='left' width='30'>&nbsp;&nbsp;" & valueNum &"</td>"&_
					"<td align='left' width='5'>&nbsp;</td>"&_
					"<td align='left' width='122'>" & rsLoader("daterentset") & "</td></tr>"
			
				'' List of values
				pmstring = pmstring &_
					"<table>" &_
						"<tr>" &_
							 "<TD HEIGHT=20PX width=7 nowrap ><font color='#FFFFFF'>ss</font></TD>" &_
							 "<TD width=84 nowrap >" & rsLoader("daterentset") & "</TD>" &_
							 "<TD width=111 nowrap >" & rsLoader("renteffective") & "</TD>" &_
							  "<TD width=198 nowrap >" & rsLoader("fullname") & "</TD>" &_
							 "<TD width=70 >" & rsLoader("services") & "</TD>" &_
							 "<TD width=85 >" & rsLoader("counciltax") & "</TD>" &_
							 "<TD width=69>" & rsLoader("targetrent") & "</TD>" &_
							 "<TD width=72><b>" & rsLoader("rent") & "</b></TD>" &_
						"</tr>" &_
					"</table>"
				if valueNum  >  0 then
					avrg = avrg + (aRentValues(valueNum) - aRentValues(valueNum -1))
				End if					
				valueNum = valueNum + 1
				rsLoader.MoveNext

			wend
			
			'' Additional Information#
			TotalRentIncrease = "� " & (aRentValues(valueNum-1) - aRentValues(0))
			TotalPercentIncrease = cInt((( aRentValues(valueNum-1)/aRentValues(0))*100)-100) & " %" 
			'TotalAvrgIncrease = aRentValues(valueNum-1) aRentValues(0) 
			
		else
			pmstring = pmstring & "<Tr><TD colspan=5 align=center>No Tenancy History is available for this Property</TD></TR>" 
		end if
CloseRs(rsLoader)

%>
<html>
<head>
    <title>Rent History</title>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

		function changeHistoryType(){

			RSLFORM.target = ""
			RSLFORM.action = "iPropertyHistory.asp"
			RSLFORM.submit()		
		}

</script>
<body BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<%
sub displayverticalgraph(strtitle,strytitle,strxtitle,avalues,alabels)
 


'******************************* 
'           user customizeable values for different formats
'            of the graph
'             FREEWARE!!
'    Just tell me if you are going to use it - info@cool.co.za
'******************************** 


const GRAPH_HEIGHT     = 75             'set up the graph height
const GRAPH_WIDTH     = 200           'set up the graph width
const GRAPH_SPACING     = 0    
const GRAPH_BORDER     = 0             'if you would like to see the borders to align things differently    
const GRAPH_BARS     = 2             'loops through different colored bars e.g. 2 bars = gold,blue,gold,blue
const USELOWVALUE     = FALSE         'this uses the low value of the array as the value of origin (default = 0)
const SHOWLABELS     = TRUE          'set this to toggle whether or not the labels are shown
const L_LABEL_SEPARATOR = "|"            ' |Label
const R_LABEL_SEPARATOR = "|"            ' Label|
const LABELSIZE     = -4            
const GRAPHBORDERSIZE     = 1
const INTIMGBORDER     = 1               'border around the bars
Const ALT_TEXT        = 3            'Changes the format of the alternate text of the bar image
                        '1 = Labels ,2 = Values , 3 = Labels + Values , 4 = Percent

 

'************************************************************************
'array of different bars to loop through
'you can change the order of these 
'Count = 10 
'"dark_green","red","gold","blue","pink","light_blue","light_gold","orange","green","purple"
'cut and paste from here and insert into the agraph_bars array below Make sure the 
'number specified in the const GRAPH_BARS is the same as or less than that in the array 
' 7 graph_bars <= 7 elements in array
'************************************************************************


agraph_bars = array("dark_green","red","gold","blue","pink","light_blue","light_gold","orange","green","purple")


intmax = 0



'find the maximum value of the values array



for i = 0 to ubound(avalues)
if  cint(intmax) < cint(avalues(i)) then  intmax = cint(avalues(i)) 
next
if uselowvalue then 
intmin = avalues(0)
for i = 0 to ubound(avalues)
if  cint(intmin) > cint(avalues(i)) then  intmin = cint(avalues(i)) 
next
end if

 

'establish the graph multiplier


graphmultiplier = round(graph_height-100/intmax)

imgwidth = round(300/(ubound(avalues)+1))
if imgwidth > 16 then imgwidth = 16 


%>
<table width=750 height=180 style="BORDER-RIGHT: SOLID 1PX #133E71" cellpadding=1 cellspacing=2 border=0 class="TAB_TABLE">
  <form name=RSLFORM method=POST>
    <tr> 
      <td height=1 valign="top"> 
        <table cellspacing=0 cellpadding=3 border=1 style='border-top:none;border-collapse:collapse;border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' width=100% >
          <tr style='color:#133e71' height=1px> 
            <td nowrap bgcolor="beige"> 
              <table width="100%">
                <tr> 
                  <td height=1PX width=7 nowrap bgcolor="beige">&nbsp;</td>
                  <td width=126 nowrap bgcolor="beige" align="left"> <a href="iPropertyHistory.asp?propertyid=<%=property_id%>&sel_HISTORYTYPE=2">View 
                    Stats</a> </td>
                  <td nowrap bgcolor="beige" align="right"> Choose History Type: 
                    <input type="hidden" name="propertyid" value="<%=Request("propertyid")%>">
                    <select name='sel_HISTORYTYPE' class='textbox200' onChange="changeHistoryType()">
                      <option value='1'>Please Select</option>
                      <option value='1'>Tenancy History</option>
                      <option value='2'>Rent History</option>
                    </select>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td height=100% > </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td height=1 valign="top" align="center"> 
	  <div style="width:700px; height:116px; overflow: auto;"><table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td align="center" width="165" valign="top"> 
              <table width="200" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="1">&nbsp;</td>
                  <td align="center" colspan="2">&nbsp;</td>
                  <td width="3">&nbsp;</td>
                </tr>
                <tr> 
                  <td bgcolor="beige" rowspan="7" width="2">&nbsp;</td>
                  <td bgcolor="beige" align="center" colspan="2"><b>Additional 
                    Information</b></td>
                  <td bgcolor="beige" rowspan="7" width="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="128" align="center">&nbsp;</td>
                  <td align="center" width="68">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="128" align="right">Total Rent Increase</td>
                  <td align="center" width="68"> 
                    <% rw TotalRentIncrease%>
                  </td>
                </tr>
                <tr> 
                  <td width="128" align="right">Total % Increase</td>
                  <td align="center" width="68"> 
                    <% rw TotalPercentIncrease%>
                  </td>
                </tr>
                <tr> 
                  <td width="128" align="right" >Average Increase <br>
                    (per change)</td>
                  <td align="center" width="68" valign=top> &pound; 
                    <%
					if avrg <> "" then
					 rw avrg /(valueNum -1)
					 Else
					 rw "0"
					End If
					 %>
					 
                  </td>
                </tr>
                <tr> 
                  <td width="128" align="center" >&nbsp;</td>
                  <td align="center" width="68" valign=top>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="2" align="center" bgcolor="beige" height="2"></td>
                </tr>
              </table>
            </td>
            <td align="center" width="379"> 
              <table border=<%=graph_border%> cellpadding = 0 cellspacing = <%=graph_spacing%>>
                <tr> 
                <tr> 
                  <td height="100%"> 
                    <table border="<%=graph_border%>" height="100%">
                      <tr> 
                        <td height="50%" valign="top" align=right>� <%=intmax%></td>
                      </tr>
                      <tr> 
                        <td height="50%" valign="bottom" align=right> 
                          <%if uselowvalue then
                 response.write cstr(intmin)
           else
             response.write "0"
           end if
          %>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td valign="bottom" align="right"><img src="../../leftbord.gif" width="2" height="<%=graphmultiplier+8%>"> 
                  </td>
                  <%

 

             '*******************MAIN PART OF THE CHART************************************


         for i = 0 to ubound(avalues)
           strgraph = agraph_bars(count)
               if alt_text = 1 then 
           stralt = alabels(i)
          elseif alt_text = 2 then 
            stralt = avalues(i)
          elseif alt_text = 3 then 
            stralt = alabels(i) &" - "  &avalues(i)
          elseif alt_text = 4 then   
            stralt = round(avalues(i) /intmax  *100,2)  &"%"
         end if     
            
            if uselowvalue then  %>
                  <td valign="bottom" align="center"> <img src="../images/<%=strgraph%>.gif" height="<%=round((avalues(i)-intmin)/intmax*graphmultiplier,0)%>" 
width="<%=imgwidth%>" alt="<%=strAlt%>" border="<%=intimgborder%>"></td>
                  <%else%>
                  <td valign="bottom" align="center"> <img src="../images/<%=strgraph%>.gif" height="<%=round(avalues(i)/intmax*graphmultiplier,0)%>" 
width="<%=imgwidth %>" alt="<%=strAlt%>" border="<%=intimgborder%>"></td>
                  <%end if 
        
            if count = graph_bars-1 then 
              count = 0 
            else
              count = count + 1
            end if        
          next  
          

 

            'write out the border at the bottom of the bars also leave a blank cell for spacing on the right


             response.write "<td width='50'> </td></tr><tr><td width=8> </td>" _
&"<td> </td><td 	=" &(ubound(avalues)+1) &" valign='top'>" _
             &"<img src='botbord.gif' width='100%' height='2'</td></tr>"
         if showlabels then %>
                <tr> 
                  <td width=8 height=1> </td>
                  <td> </td>
                  <%for i = 0 to ubound(avalues)%>
                  <td valign="bottom" width=<%=imgwidth%> ><font size=
                   <%=labelsize &">" &l_label_separator &alabels(i) &r_label_separator %>></font></td>
                  <%next%>
                </tr>
                <%end if%>
                <tr> 
                  <td colspan=<%=ubound(avalues)+3%> height=50 align="center"><%=strxtitle%></td>
                </tr>
              </table>
            </td>
            <td width="200" valign="top"> 
              <table width="200" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="2">&nbsp;</td>
                  <td align="center" colspan="3">&nbsp;</td>
                  <td width="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td bgcolor="beige" rowspan="<%=valueNum + 3%>" width="2">&nbsp;</td>
                  <td bgcolor="beige" align="center" colspan="3"><b>Legend</b></td>
                  <td bgcolor="beige" rowspan="<%=valueNum + 3%>" width="2">&nbsp;</td>
                </tr>
 				<%=legenTable%> 
                <tr> 
                  <td bgcolor="beige" align="center" height="2" colspan="3"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
	  
	  </td>
    </tr>
  </form>
</table>
<%end sub %>
<%
displayverticalgraph "Rent Change History","Rent Value","Rent Changes",aRentValues,aRentNames 
%>
</body>
</html>
