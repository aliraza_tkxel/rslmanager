<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	//THIS PAGE BUILDS THE REPAIR PURCHASE ORDERS FOR TYHE SUPPLIER CRM
	
	//LAST MODIFIED BY ZANFAR ALI 15 JUNE 2004 AT 17:15 HOURS
	//WORK DONE: TOTAL REWRITE TO TAKE INTO ACCOUNT NEW WORK ORDER FUNCTIONALITY.
	
	Dim cnt, customer_id, str_journal_table, orderby
	
	DEVID = Request("DEVID")
	CurrentWorkOrder = Request("CWO")
		
	orderby = Request("sort")
	if orderby = "" then orderby = "PO.ORDERID" end if

	OpenDB()	
	build_purchase_orders()
	CloseDB()


	Function build_purchase_orders()
		
		cnt = 0
		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WO.ORDERID, WO.WOID,  " &_
				"ISNULL(CONVERT(NVARCHAR, Po.PoDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), Po.PoDATE, 108) ,'')AS formatteddate, " &_				
				"			PODATE, PONAME, ISNULL(TOTALCOST,0) AS GROSSCOST, POSTATUSNAME,	" &_
				"			ITEMDESCPART = CASE " &_
				"			WHEN LEN(PO.PONOTES) > 40 THEN LEFT(PO.PONOTES,40) + '...' " &_
				"			ELSE ISNULL(PO.PONOTES,'No Title') " &_
				"			END, PO.PONOTES " &_
				"FROM F_PURCHASEORDER PO " &_
				"INNER JOIN P_WORKORDER WO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PO.POSTATUS " &_															
				"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID " &_				
				"WHERE WO.DEVELOPMENTID = " & DEVID & " AND PO.POSTATUS <> 0 AND PO.ACTIVE = 1 " &_
				"ORDER BY " & orderby & " "					
		Call OpenRs (rsSet, strSQL) 

		PrevAddressString = ""				
		str_journal_table = ""
		While Not rsSet.EOF

		
			cnt = cnt + 1
			ORDERID = rsSet("ORDERID")
			WOID = rsSet("WOID")
			
			if (CStr(CurrentWorkOrder) = CStr(WOID)) then
			str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND'><A NAME=""WOID"">" &_
																"<TD style='background-color:white'><img src='/js/tree/img/minus.gif' width=18 height=18 border=0></TD>" &_
															"<TD><b>" & rsSet("formatteddate") & "</b></TD>" &_
															"<TD><a href=# onclick='open_me(" & ORDERID & ")'><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></TD>" &_															
															"<TD TITLE=""" & rsSet("PONOTES")& """><b>&nbsp;&nbsp;" & rsSet("PONAME")  & "</b></TD>" &_
															"<TD><b>" & rsSet("POSTATUSNAME")  & "</b></TD>" &_
															"<TD align=right><b>" & FormatCurrency(rsSet("GROSSCOST"))	  & "</b>&nbsp;</TD>" &_															
														"<TR>"
				Build_Purchase_Item_List(ORDERID)
				CurrentWorkOrder = WOID
			else
			str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND'>" &_
																"<TD style='background-color:white'><img src='/js/tree/img/plus.gif' width=18 height=18 border=0></TD>" &_
															"<TD><b>" & rsSet("formatteddate") & "</b></TD>" &_
															"<TD><a href=# onclick='open_me(" & ORDERID & ")'><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></TD>" &_															
															"<TD TITLE=""" & rsSet("PONOTES")& """><b>&nbsp;&nbsp;" & rsSet("PONAME")  & "</b></TD>" &_
															"<TD><b>" & rsSet("POSTATUSNAME")  & "</b></TD>" &_
															"<TD align=right><b>" & FormatCurrency(rsSet("GROSSCOST"))	  & "</b>&nbsp;</TD>" &_															
														"<TR>"
			
			end if			

			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No purchase order entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function

	Function Build_Purchase_Item_List(ORDERID)
		cnt = 0
		strSQL = "SELECT J.CURRENTITEMSTATUSID, PI.ORDERID, pi.active, PI.ITEMDESC, CASE WHEN LEN(PI.ITEMNAME) > 50 THEN LEFT(PI.ITEMNAME,50) + '...' ELSE PI.ITEMNAME END AS THEITEMNAME, PI.ITEMNAME, PI.PIDATE, PI.GROSSCOST, PS.POSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, PI.PIDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), PI.PIDATE, 108) ,'')AS formatteddate " &_
				"FROM F_PURCHASEITEM PI " &_
				"INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID " &_
				"INNER JOIN P_WOTOREPAIR WTR ON WTR.ORDERITEMID = PI.ORDERITEMID " &_				
				"INNER JOIN C_JOURNAL J ON WTR.JOURNALID = J.JOURNALID " &_								
				"WHERE PI.ORDERID = " & ORDERID & " AND (ACTIVE = 1 OR (J.CURRENTITEMSTATUSID = 5 AND ACTIVE = 0)) " &_
				"ORDER BY pi.ORDERITEMID DESC"
			
		
		//response.write strSQL
		Call OpenRs (rsSet2, strSQL) 
		
		While Not rsSet2.EOF
			
			cnt = cnt + 1

			TRSTYLE = ""
			if (rsSet2("CURRENTITEMSTATUSID") = 5) then
				TRSTYLE = "STYLE=';color:red;text-decoration:line-through'"
			end if		
			
			str_journal_table = str_journal_table & 	"<TR " & TRSTYLE & ">" &_
															"<TD style='background-color:white'><img src='/js/tree/img/join.gif' width=18 height=18 border=0></TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("formatteddate") & "</TD>" &_
															"<TD COLSPAN=2 TITLE=""" & rsSet2("ITEMNAME") & """>&nbsp;&nbsp;" & rsSet2("THEITEMNAME") & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("POSTATUSNAME")  & "</TD>" &_
															"<TD ALIGN=RIGHT><FONT COLOR=BLUE>" & fORMATcURRENCY(rsSet2("GROSSCOST")) & "</FONT>&nbsp;</TD>" &_
														"<TR>"
			rsSet2.movenext()
			
		Wend
		CloseRs(rsSet2)
		
		IF cnt = 0 then
			str_journal_table = str_journal_table & "<TR><TD COLSPAN=6 ALIGN=CENTER>No purchase item entries exist.</TD></TR>"
		End If	

	End Function	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function open_me(Order_id){
		event.cancelBubble = true
		window.showModelessDialog("../../Finance/Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")	
		}

	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iAccountDev.asp?DEVID=<%=DEVID%>&CWO=" + WOID
	}
	
	function DoSync(){
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		//if ("<%=Request("SyncTabs")%>" == "1")
		//	parent.synchronize_tabs(11, "BOTTOM")
		location.href = "#WOID"		
		}	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM');">

	<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0 STYLE="behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=TOP>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=20></TD>		
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120>
					<B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="location.href='iAccountDev.asp?sort=PO.podate asc&DEVID=<%=DEVID%>'">
						 Date : 
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="location.href='iAccountDev.asp?sort=PO.podate desc&DEVID=<%=DEVID%>'">
					</FONT></B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100>
					<B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="location.href='iAccountDev.asp?sort=PO.Orderid asc&DEVID=<%=DEVID%>'">
						Order No : 
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="location.href='iAccountDev.asp?sort=PO.Orderid desc&DEVID=<%=DEVID%>'">
					</FONT></B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=230><B><FONT COLOR='BLUE'>Item:</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120>
					<B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="location.href='iAccountDev.asp?sort=POSTATUSNAME asc&DEVID=<%=DEVID%>'">
						Status : 
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="location.href='iAccountDev.asp?sort=POSTATUSNAME desc&DEVID=<%=DEVID%>'">
					</FONT></B>
				</TD>				
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=70 align=right><B><FONT COLOR='BLUE'>Total Cost :</FONT></B></TD>				
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	