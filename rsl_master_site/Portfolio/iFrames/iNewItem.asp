<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim property_id
	    property_id = Request("propertyid")
    Call OpenDB()
	Call BuildSelect(lst_item, "sel_ITEM", "C_ITEM WHERE ITEMID = 1 ", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange='item_change()'")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

    var FormFields = new Array();
    FormFields[0] = "txt_TITLE|Title|TEXT|N"
    FormFields[1] = "sel_ITEM|Item|SELECT|Y"
    FormFields[2] = "sel_NATURE|Nature|SELECT|Y"

    function item_change() {
        document.RSLFORM.target = "frm_erm_srv";
        document.RSLFORM.action = "../serverside/ItemChange_srv.asp?itemid=" + document.RSLFORM.sel_ITEM.value;
        document.RSLFORM.submit();
    }

    // build array for item pages (only property at the mo)
    var iFrameArray = new Array("",
							"Defects", //1
							"Repairs", //2
							"blank",
							"blank",
							"blank", //5
							"blank",
							"blank",
							"blank",
							"blank",
							"blank", //10
							"blank",
							"blank",
							"blank",
							"blank",
							"blank", //15
							"blank",
							"blank",
							"blank",
							"blank",
							"Repairs", //20
							"Repairs",
							"Repairs",
							"blank",
							"blank"
							)

    function go_nature() {
        FormFields[0] = "txt_TITLE|Title|TEXT|N"
        if (document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 2 && document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 20 && document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 21 && document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 22)
            FormFields[0] = "txt_TITLE|Title|TEXT|Y"
        if (!checkForm()) return false;
        var nature_id, item_id, title
        nature_id = document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value;
        item_id = document.getElementById("sel_ITEM").options[document.getElementById("sel_ITEM").selectedIndex].value;
        title = document.getElementById("txt_TITLE").value;
        property_id = "<%=property_id%>"
        if (document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value == "")
            frm_nature.location.href = "blank.asp";
        //alert("i" + iFrameArray[nature_id] + ".asp?natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title + "&propertyid=" + property_id)
        frm_nature.location.href = "i" + iFrameArray[nature_id] + ".asp?natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title + "&propertyid=" + property_id;
    }

    function setTitle() {
        if (document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value == "")
            document.getElementById("txt_TITLE").value = ""
        else
            document.getElementById("txt_TITLE").value = document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].text;
    }
</script>
</head>
<body onload="parent.STOPLOADER('BOTTOM')" class='TA'>
    <table width="100%" cellpadding="1" cellspacing="0" style="height:205px; border-collapse: collapse"
        slcolor='' border="0" hlcolor="#4682b4">
        <tr>
            <td valign="top">
                <!-- NEW ITEM------------------------------------------->
                <form name="RSLFORM" method="post" action="">
                <table border="0">                    
                    <tr>
                        <td>
                            Item :
                        </td>
                        <td>
                            <%=lst_item%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ITEM" id="img_ITEM" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nature :
                        </td>
                        <td>
                            <div id="dvNature">
                                <select name="sel_NATURE" id="sel_NATURE" class='textbox200' onchange="setTitle()">
                                    <option value=''>Select Item</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NATURE" id="img_NATURE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Title :
                        </td>
                        <td>
                            <input type="text" name="txt_TITLE" id="txt_TITLE" maxlength="200" size="29" class="textbox200" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <input type="button" name="BTN_DATES" id="BTN_DATES" value='Next' class="RSLBUTTON" onclick="go_nature()" /></td>
                            <td>
                                </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            A title is not necessary when creating a Repair, as the repair details will become
                            the title itself.
                            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                        </td>
                    </tr>                    
                </table>
                </form>
            </td>
            <td width="550">
                <iframe src="/secureframe.asp" name="frm_nature" id="frm_nature" width="100%" height="100%" frameborder="0" style="border: none"></iframe>
            </td>
        </tr>
    </table>
    <iframe src="/secureframe.asp" name="frm_erm_srv" id="frm_erm_srv" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
