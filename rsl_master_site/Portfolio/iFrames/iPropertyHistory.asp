<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	property_id = Request("propertyid")
	HISTORYTYPE = Request("sel_HISTORYTYPE")
	Call OpenDB()

	If Not HISTORYTYPE <> "" Then HISTORYTYPE = "1" End If

	''''''''''
	' If the option is tenancy history or is intial opening of page
	''''''''''
	If HISTORYTYPE = "1" Then
		SQL = "select ct.startdate, CT.enddate , ct.customerid, ct.tenancyid, tl.description + ' ' + c.firstname + ' ' + c.lastname as fullname from c_customertenancy ct " &_
				" inner join c_tenancy t on  t.tenancyid = ct.tenancyid " &_
				" inner join c__customer c on c.customerid = ct.customerid " &_
				" left join g_title tl on tl.titleid = c.title " &_
				" where ct.tenancyid in (select tenancyid from c_tenancy where propertyid = '" & property_id & "')" &_
				" order by ct.startdate desc"
		Call OpenRs(rsLoader, SQL)
		If (NOT rsLoader.EOF) Then
		Dim enddate
			While NOT rsLoader.EOF
			enddate = rsLoader("enddate")
				If ( rsLoader("enddate") <> "") Then
					Pic = "<img src='/Customer/Images/FT.gif'  border=0 title='Previous Tenancy' alt='Previous Tenancy' />"
				Else
					Pic = "<img src='/Customer/Images/T.gif'  border=0 title='Tenancy On going' alt='Tenancy On going' />"
					enddate = "On going"
				End If
				pmstring = pmstring &_
							"<tr>" &_
								"<td width=12 nowrap='nowrap'>" & Pic & "</td>" &_
								"<td width=105 nowrap='nowrap'>" & rsLoader("startdate") & "</td>" &_
								"<td width=115 nowrap='nowrap'>" & enddate & "</td>" &_
								"<td width=256 nowrap='nowrap'>" & rsLoader("fullname") & " (<a href='/Customer/CRM.asp?CustomerID=" & rsLoader("customerid") & "' target='_parent'>"  & rsLoader("customerid") & "</a>)</td>" &_
								"<td width=212>" & tenancyreference(rsLoader("tenancyid")) & "</td>" &_
								"</tr>"
				rsLoader.MoveNext
			Wend
		Else
			pmstring = pmstring & "<tr><td colspan=""5"" align=""center"">No Tenancy History is available for this Property</td></tr>" 
		End If
	End If

	''''''''''
	' If the option is rent history
	''''''''''
	If HISTORYTYPE = "2" Then
	SQL = 		"SELECT " &_
							"ISNULL(FH.RENT, 0) AS RENT, ISNULL(FH.SERVICES, 0) AS SERVICES, ISNULL(FH.COUNCILTAX, 0) AS COUNCILTAX, FH.DATERENTSET, FH.RENTEFFECTIVE, ISNULL(FH.INELIGSERV, 0) AS INELIGSERV, ISNULL(FH.SUPPORTEDSERVICES, 0) AS SUPPORTEDSERVICES, ISNULL(FH.WATERRATES, 0) AS WATERRATES, "&_
							"FH.TARGETRENT, TL.DESCRIPTION + ' ' + E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, TC.description as therenttype "&_
					"FROM P_FINANCIAL FH "&_
							"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = FH.PFUSERID "&_
							"	LEFT JOIN G_TITLE TL ON TL.TITLEID = E.TITLE "&_
							"	LEFT JOIN C_TENANCYTYPE TC ON TC.TENANCYTYPEID = fh.renttype " &_
					"WHERE PROPERTYID = '" & property_id & "' "	&_
			"union SELECT " &_
							"ISNULL(FH.RENT, 0) AS RENT, ISNULL(FH.SERVICES, 0) AS SERVICES, ISNULL(FH.COUNCILTAX, 0) AS COUNCILTAX, FH.DATERENTSET, FH.RENTEFFECTIVE, ISNULL(FH.INELIGSERV, 0) AS INELIGSERV, ISNULL(FH.SUPPORTEDSERVICES, 0) AS SUPPORTEDSERVICES, ISNULL(FH.WATERRATES, 0) AS WATERRATES,"&_
							"FH.TARGETRENT, TL.DESCRIPTION + ' ' + E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME,  TC.description as therenttype "&_
					"FROM P_FINANCIAL_HISTORY FH "&_
							"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = FH.PFUSERID "&_
							"	LEFT JOIN G_TITLE TL ON TL.TITLEID = E.TITLE "&_
							"	LEFT JOIN C_TENANCYTYPE TC ON TC.TENANCYTYPEID = fh.renttype " &_
					"WHERE PROPERTYID = '" & property_id & "' ORDER BY DATERENTSET"
		Call OpenRs(rsLoader, SQL)
		If (NOT rsLoader.EOF) Then
			While NOT rsLoader.EOF
				pmstring = pmstring &_
						"<tr>" &_
							 "<td height='20px' width='2' nowrap='nowrap'></td>" &_
							 "<td width='75' nowrap='nowrap'>" & rsLoader("daterentset") & "</td>" &_
							 "<td width='80' nowrap='nowrap'>" & rsLoader("renteffective") & "</td>" &_
							 "<td width='90' nowrap='nowrap'>" & rsLoader("therenttype") & "</td>" &_
							 "<td width='70'>�" & FormatNumber(rsLoader("services"),2) & "</td>" &_
							 "<td width='77'>�" & FormatNumber(rsLoader("counciltax"),2) & "</td>" &_
							 "<td width='50'>�" & FormatNumber(rsLoader("WATERRATES"),2) & "</td>" &_
							 "<td width='50'>�" & FormatNumber(rsLoader("INELIGSERV"),2) & "</td>" &_
							 "<td width='65'>�" & FormatNumber(rsLoader("SUPPORTEDSERVICES"),2) & "</td>" &_
							 "<td width='58'><b>�" & FormatNumber(rsLoader("rent"),2) & "</b></td>" &_
							 "<td>�" & rsLoader("targetrent") & "</td>" &_
							 "<td>" & rsLoader("FULLNAME") & "</td>" &_
						"</tr>" &_
				rsLoader.MoveNext
			Wend
		Else
			pmstring = pmstring & "<tr><td colspan=""7"" align=""center"" valign=""top"">No Rent History is available for this Property</td></tr>"
		End If
	End If

	Call CloseRs(rsLoader)
%>
<html>
<head>
    <title>Customer Module > Portfolio > Property History</title>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        function changeHistoryType() {
            document.RSLFORM.target = ""
            document.RSLFORM.action = "iPropertyHistory.asp"
            document.RSLFORM.submit()
        }

        function Go() {
            document.location.href = "iPropertyHistoryGraph.asp?propertyid=<%=property_id%>"
        }

    </script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <table width="750" height="180" style="border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">        
        <tr>
            <td height="1" valign="top">
            <form name="RSLFORM" method="post" action="">
                <table cellspacing="0" cellpadding="3" border="0" style="border-top: 1px solid #133e71;
                    border-collapse: collapse; border-left: 1px solid #133e71; border-bottom: 1px solid #133e71;
                    border-right: 1px solid #133e71" width="100%">
                    <tr style="color: #133e71">
                        <td nowrap="nowrap" bgcolor="beige" style="border-bottom: 1px solid silver">
                            <%' this is the start of the top strip table with dropdown in %>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td height="1px" width="7" nowrap="nowrap">
                                        &nbsp;
                                    </td>
                                    <% If  HISTORYTYPE = "2" then %>
                                    <td width="85" nowrap="nowrap" align="left">
                                        <input type="button" name="Submit" id="Submit" value="View Chart" onclick="Go()" class="rslbutton" />
                                        <% End If 	%>
                                    </td>
                                    <td nowrap="nowrap" align="right" width="">
                                        Choose History Type:
                                        <input type="hidden" name="propertyid" id="propertyid" value="<%=Request("propertyid")%>" />
                                        <select name="sel_HISTORYTYPE" id="sel_HISTORYTYPE" class="textbox200" onchange="changeHistoryType()">
                                            <option value="1">Please Select</option>
                                            <option value="1">Tenancy History</option>
                                            <option value="2">Rent History</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="5" colspan="3">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="1px" colspan="3">
                                    </td>
                                </tr>
                            </table>
                            <%' this is the end of the top strip table with dropdown in %>
                            <% If  NOT HISTORYTYPE = "2" then %>
                            <table cellpadding="0" cellspacing="0" border="0" width="721" height="18">
                                <tr style="border-left: 1px solid #133e71; border-bottom: 1px solid #133e71">
                                    <td width="24" nowrap="nowrap" bgcolor="beige">
                                    </td>
                                    <td width="106" nowrap="nowrap" bgcolor="beige">
                                        <b>Start Date</b>
                                    </td>
                                    <td width="119" nowrap="nowrap" bgcolor="beige">
                                        <b>End Date</b>
                                    </td>
                                    <td width="262" nowrap="nowrap" bgcolor="beige">
                                        <b>Customer</b>
                                    </td>
                                    <td width="210" bgcolor="beige">
                                        <b>Tenancy Reference</b>
                                    </td>
                                </tr>
                            </table>
                            <% Else %>
                            <table cellpadding="0" cellspacing="0" border="0" width="722">
                                <tr>
                                    <td width="9" nowrap="nowrap" bgcolor="beige">
                                    </td>
                                    <td width="77" nowrap="nowrap" bgcolor="beige">
                                        <b>Date Set</b>
                                    </td>
                                    <td width="87" nowrap="nowrap" bgcolor="beige">
                                        <b>Effective</b>
                                    </td>
                                    <td width="95" nowrap="nowrap" bgcolor="beige">
                                        <b>Tenancy Type</b>
                                    </td>
                                    <td width="70" bgcolor="beige">
                                        <b>Services</b>
                                    </td>
                                    <td width="80" bgcolor="beige">
                                        <b>Council Tax</b>
                                    </td>
                                    <td width="50" bgcolor="beige">
                                        <b>Water</b>
                                    </td>
                                    <td width="60" bgcolor="beige">
                                        <strong>Inelig.<br />
                                            Serv</strong>
                                    </td>
                                    <td width="63" bgcolor="beige">
                                        <strong>Supt.
                                            <br />
                                            Serv </strong>
                                    </td>
                                    <td width="56" bgcolor="beige" height="18">
                                        <strong>Rent</strong>
                                    </td>
                                    <td width="65" bgcolor="beige">
                                        <b>Target</b>
                                    </td>
                                    <td width="65" bgcolor="beige">
                                        <b>By</b>
                                    </td>
                                </tr>
                            </table>
                            <% End If %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="width: 700px; height: 116px; overflow: auto;">
                                <table>
                                    <%=pmstring%></table>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
            </td>            
        </tr>        
    </table>
</body>
</html>
