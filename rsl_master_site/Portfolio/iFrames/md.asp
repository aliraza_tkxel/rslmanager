<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	Dim pd_name
	Dim pd_dob
	Dim pd_gender
	Dim pd_marital
	Dim pd_ethnicity
	Dim pd_customerno
	Dim pd_employment
	Dim pd_occupation
	Dim pd_mtf
	Dim pd_ni
	Dim pd_type
	Dim customer_id, pd_firstname, pd_lastname
	
	OpenDB()
	customer_id = Request("customerid")
	get_main_details()
	CloseDB()
	
	// get employee details
	Function get_main_details()
		
		Dim strSQL
		strSQL = 	"SELECT 	C.CUSTOMERID, FIRSTNAME, LASTNAME, OCCUPATION, " &_
					"			ISNULL(T.DESCRIPTION, '') AS CUSTOMERTYPE, " &_
					"			LEFT(C.FIRSTNAME,1) + ' ' + C.LASTNAME AS FULLNAME,  " &_
					"			ISNULL(CONVERT(NVARCHAR, C.DOB, 103), '') AS DOB,  " &_
					"			ISNULL(C.GENDER, '') AS GENDER,  " &_
					"			ISNULL(M.DESCRIPTION, '') AS MARITALSTATUS,  " &_
					"			ISNULL(ETH.DESCRIPTION, '') AS ETHNICITY, " &_
					"			ISNULL(E.DESCRIPTION, '') AS EMPLOYMENTSTATUS, " &_
					"			ISNULL(C.NINUMBER, '') AS NINUMBER, " &_
					"			ISNULL(C.MOVINGTIMEFRAME, '') AS MOVINGTIMEFRAME, " &_
					"			ISNULL(C.OCCUPATION, '') AS EMPLOYMENTSTATUS, " &_
					"			ISNULL(TEN.TENANCYID, '') AS TENANCYID, " &_
					"			ISNULL(TE.PROPERTYID, '') AS PROPERTYREF " &_
					"FROM 		C__CUSTOMER C  " &_
					"			LEFT JOIN G_MARITALSTATUS M ON C.MARITALSTATUS = M.MARITALSTATUSID  " &_
					"			LEFT JOIN G_ETHNICITY ETH ON C.ETHNICORIGIN = ETH.ETHID  " &_
					"			LEFT JOIN C_CUSTOMERTYPE T ON C.CUSTOMERTYPE = T.CUSTOMERTYPEID  " &_
					"			LEFT JOIN C_EMPLOYMENTSTATUS E ON C.EMPLOYMENTSTATUS = E.EMPLOYMENTSTATUSID " &_
					"			LEFT JOIN C_CUSTOMERTENANCY TEN ON C.CUSTOMERID = TEN.CUSTOMERID " &_
					"			LEFT JOIN C_TENANCY TE ON TEN.TENANCYID = TE.TENANCYid " &_
					"WHERE 		C.CUSTOMERID = " & customer_id
		//response.write strSQL
		Call OpenRs (rsSet,strSQL) 
		
		// personal details section
		pd_name			= rsSet("FULLNAME")
		pd_firstname	= rsSet("FIRSTNAME")
		pd_lastname		= rsSet("LASTNAME")				
		pd_dob			= rsSet("DOB")
		pd_gender		= rsSet("GENDER")
		pd_marital		= rsSet("MARITALSTATUS")
		pd_ethnicity	= rsSet("ETHNICITY")
		pd_customerno	= rsSet("CUSTOMERID")
		pd_employment	= rsSet("EMPLOYMENTSTATUS")
		pd_occupation	= rsSet("OCCUPATION")		
		pd_mtf			= rsSet("MOVINGTIMEFRAME")
		pd_ni			= rsSet("NINUMBER")
		pd_type			= rsSet("CUSTOMERTYPE")

		CloseRs(rsSet)
		
	End Function	

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD ROWSPAN=2 WIDTH=70% HEIGHT=100%>
			
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
				<TR><TD CLASS='TITLE'>Customer No</TD><TD WIDTH=160><%=CustomerNumber(pd_customerno)%></TD>
                  <TD CLASS='TITLE'>Ethnic Origin</TD>
                  <TD WIDTH=160 TITLE='<%=pd_ethnicity%>'><%=pd_ethnicity%></TD>
                </TR>
			   	<TR><TD CLASS='TITLE'>Type</TD><TD><%=pd_type%></TD>
                  <TD CLASS='TITLE'>Marital Status</TD>
                  <TD><%=pd_marital%></TD>
                </TR>
			   	<TR><TD CLASS='TITLE'>First Name</TD><TD><%=pd_firstname%></TD>
                  <TD CLASS='TITLE'>Emp. Status</TD>
                  <TD><%=pd_employment%></TD>
                </TR>
			   	<TR><TD CLASS='TITLE'>Last Name</TD><TD WIDTH=160><%=pd_lastname%></TD>
                  <TD CLASS='TITLE'>Occupation</TD>
                  <TD><%=pd_occupation%></TD>
                </TR>
			   	<TR>
                  <TD CLASS='TITLE'>DOB</TD>
                  <TD><%=pd_dob%></TD>
                  <TD CLASS='TITLE'>Danger Rating</TD>
                  <TD>&nbsp;</TD>
                </TR>
					<TR>
                  <TD CLASS='TITLE'>Gender</TD>
                  <TD><%=pd_gender%></TD>
                  <TD CLASS='TITLE'>MTF</TD>
                  <TD>&nbsp;</TD>
                </TR>
				<TR><TD CLASS='TITLE'>&nbsp;</TD><TD></TD><TD CLASS='TITLE'>&nbsp;</TD>
				<TD align=right>
				<INPUT TYPE='BUTTON' NAME='BTN_UPDATEPD' VALUE='Update' CLASS='RSLBUTTONSMALL' 
				ONCLICK="parent.update_record('/Customer/popups/ppersonaldetails.asp?CustomerID=<%=customer_id%>',380,320)">
				</TD></TR>
			</TABLE>
			
		</TD>
		<TD WIDTH=30% HEIGHT=50% >
			<%=str_repairs%>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=30% HEIGHT=50% >
			<%=str_rents%>
		</TD>
	</TR>
</FORM>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	