<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	property_id = Request("propertyid")
	
	OpenDB()
	
	SQL = "SELECT  p.PMID, ISNULL(B.BLOCKNAME,'Scheme Wide') AS CBLOCK, " &_
				"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_
				"ISNULL(P.CYCLE,'') AS CCYCLE, " &_
				"CREPAIR = CASE WHEN LEN(R.DESCRIPTION) > 35 THEN LEFT(R.DESCRIPTION,35) + '...' ELSE R.DESCRIPTION END, " &_
				"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST,	 " &_
				"ISNULL(P.NOTES ,'') AS CNOTES, " &_
				"ISNULL(S.NAME,'') AS CONTRACTOR, " &_
				"R.DESCRIPTION " &_
			"FROM 	P_PLANNEDMAINTENANCE P " &_
				"LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
				"INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID " &_
				"INNER JOIN P_PLANNEDINCLUDES PI ON PI.PMID = P.PMID " &_				
				"LEFT JOIN R_ITEMDETAIL R ON PI.REPAIRID = R.ITEMDETAILID " &_				
				"INNER JOIN P__PROPERTY PR ON PR.PROPERTYID = PI.PROPERTYID " &_
			"WHERE 	PR.PROPERTYID = '" & property_id & "' "
	//response.write SQL
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
	
		WHILE NOT rsLoader.EOF
			if (rsLoader("CBLOCK") = "Scheme Wide") then
				Pic = "<img src='/myImages/Repairs/ic_scheme.gif' width=20 height=18 border=0 title='Scheme Wide'>"
			else
				Pic = "<img src='/myImages/Repairs/ic_block.gif' width=20 height=18 border=0 title='" & rsLoader("CBLOCK") & "'>"
			end if			
			pmstring = pmstring &_
						"<TR TITLE='" & rsLoader("DESCRIPTION") & "' >" &_
							"<TD width=20 nowrap>" & Pic & "</TD>" &_
							"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
							"<TD width=240 nowrap>" & rsLoader("CREPAIR") & "</TD>" &_
							"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
							"<TD width=500>" & rsLoader("CONTRACTOR") & "</TD>" &_
							"</TR>"						
			rsLoader.MoveNext
		wend
		
	else
		pmstring = pmstring & "<Tr><TD colspan=5 align=center>No Cyclical Repairs exist for this Property</TD></TR>" 
	end if
	CloseRs(rsLoader)
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD>
		
		<TABLE cellspacing=0 cellpadding=3 border=1 style='border-top:none;border-collapse:collapse;border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=742 height="173">
		<TR bgcolor='beige' style='color:#133e71'> 
		  <TD HEIGHT=20PX width=20 nowrap><b>&nbsp;</b></TD>
		  <TD width=90 nowrap><b>&nbsp;Start Date</b></TD>
		  <TD width=240 nowrap><b>&nbsp;Repair</b></TD>
		  <TD width=100 nowrap><b>&nbsp;Last Cycle</b></TD>	  
		  <TD width=600><b>&nbsp;Contractor</b></TD>	  
		</TR>
			<%=pmstring%>
		<tr><td height=100% colspan=5></td></tr>
		</TABLE>

	</td></TR>
</FORM>
</TABLE>	
</BODY>
</HTML>	
	
	
	
	
	