<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, repair_history_id, Repair_Title, last_status, isDisabled, ButtonValue
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, R.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), R.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(R.NOTES, 'N/A') AS NOTES, " &_
					"			R.TITLE, R.REPAIRHISTORYID, O.NAME, " &_
					"			A.ITEMACTIONID, A.DESCRIPTION AS ACTION, PARTACTION = CASE " &_
					"			WHEN LEN(A.DESCRIPTION) > 20 THEN LEFT(A.DESCRIPTION,20) + '...' " &_
					"			ELSE A.DESCRIPTION END , " &_
					"			S.DESCRIPTION AS STATUS, " &_					
					"			isnull(ID.DESCRIPTION,'Annual Gas Servicing') AS ITEMDETAIL " &_										
					"FROM		C_REPAIR R " &_
					"			LEFT JOIN R_ITEMDETAIL ID ON ID.ITEMDETAILID = R.ITEMDETAILID  " &_
					"			LEFT JOIN C_STATUS S ON R.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_ACTION A ON R.ITEMACTIONID = A.ITEMACTIONID  " &_					
					"			LEFT JOIN S_ORGANISATION O ON R.CONTRACTORID = O.ORGID " &_
					"WHERE		R.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY REPAIRHISTORYID DESC "
		
		//response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		last_action = -1
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY' VALIGN=TOP>"
			else
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				Repair_Title = rsSet("ITEMDETAIL")
				repair_history_id = rsSet("REPAIRHISTORYID")
				last_status = rsSet("STATUS")
				last_action = rsSet("ITEMACTIONID")
			End If
				str_journal_table = str_journal_table & 	"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD title='" & rsSet("ACTION") & "'>" & rsSet("PARTACTION") & "</TD>" &_
															"<TD>" & rsSet("NOTES") & "</TD>" &_
															"<TD>" & rsSet("NAME")  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
		ButtonValue = " Update "
		isDisabled = ""
		SQL = "SELECT * FROM C_ACTION WHERE ITEMACTIONID = " & last_action
		Call OpenRs(rsCheckOption,SQL)
		if (NOT rsCheckOption.EOF) then
'			if (rsCheckOption("EMPLOYEEONLY") = "" OR ISNULL(rsCheckOption("EMPLOYEEONLY"))) then
			if ( (rsCheckOption("EMPLOYEEONLY") = "" OR ISNULL(rsCheckOption("EMPLOYEEONLY"))) AND (rsCheckOption("CONTRACTORONLY") = "" OR ISNULL(rsCheckOption("CONTRACTORONLY"))) ) then
				isDisabled = " disabled"
				ButtonValue = " No Options "
			end if
		end if
		CloseRs(rsCheckOption)
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager My Job -- > Employment Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_repair(int_repair_history_id){
		var str_win
		str_win = "../PopUps/pRepair.asp?repairhistoryid="+int_repair_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=390, left=200,top=200") ;
	}

</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="parent.synchronize_tabs(14,'BOTTOM');parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=4><table cellspacing=0 cellpadding=1 width=100%><tr valign=top><td width=100%><b>Repair Information:</b>&nbsp;<%=Repair_Title%></TD>
	<TD nowrap>Current Status:&nbsp;<font color=red><%=last_status%></font>&nbsp;</td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonValue%>" style="background-color:beige" onclick='update_repair(<%=repair_history_id%>)' <%=isDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=120PX>Date</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=160PX>Action</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=300PX>Notes</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=200>Contractor</TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	