<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	Dim f_body
	Dim f_authority
	Dim f_rent
	Dim f_services
	Dim f_counciltax
	Dim f_waterrates
	Dim f_ineligserv
	Dim f_supp
	Dim f_totalrent
	Dim f_renttype		
	Dim f_daterentset
	Dim f_renteffective
	Dim f_targetrent
	Dim f_yield
	Dim f_capitalvalue
	Dim f_insurancevalue
	
	OpenDB()
	property_id = Request("propertyid")
	get_main_details()
	CloseDB()
	
	// get employee details
	Function get_main_details()
		
		Dim strSQL
		strSQL = 	"SELECT 	ISNULL(NOMINATINGBODY,'') AS NOMINATINGBODY, " &_
					"			ISNULL(F.DESCRIPTION,'') AS FUNDINGAUTHORITY, " &_
					"			ISNULL(P.RENT ,0.00) AS RENT, " &_
					"			ISNULL(P.SERVICES ,0.00) AS SERVICES, " &_
					"			ISNULL(COUNCILTAX, 0.00) AS COUNCILTAX, " &_
					"			ISNULL(WATERRATES, 0.00) AS WATERRATES, " &_
					"			ISNULL(INELIGSERV, 0.00) AS INELIGSERV, " &_
					"			ISNULL(SUPPORTEDSERVICES, 0.00) AS SUPP, " &_
					"			ISNULL(TOTALRENT, 0.00) AS TOTALRENT, " &_
					"			ISNULL(TT.DESCRIPTION, '') AS RENTTYPE, " &_
					"			ISNULL(CONVERT(NVARCHAR, P.DATERENTSET, 103) ,'') AS DATERENTSET, " &_
					"			ISNULL(CONVERT(NVARCHAR, P.RENTEFFECTIVE, 103) ,'') AS RENTEFFECTIVE, " &_
					"			ISNULL(P.TARGETRENT,0.0) AS TARGETRENT, " &_
					"			ISNULL(P.YIELD,0.0) AS YIELD, " &_
					"			ISNULL(P.CAPITALVALUE,0.0) AS CAPITALVALUE, " &_
					"			ISNULL(P.TARGETRENT,0.0) AS INSURANCEVALUE " &_
					"FROM 		P_FINANCIAL P " &_
					"			LEFT JOIN P_FUNDINGAUTHORITY F ON P.FUNDINGAUTHORITY = F.FUNDINGAUTHORITYID " &_
					"			LEFT JOIN C_TENANCYTYPE TT ON P.RENTTYPE = TT.TENANCYTYPEID " &_
					"WHERE	 	PROPERTYID = '" & property_id & "'"
					
		//response.write strSQL
		Call OpenRs (rsSet,strSQL) 
		
		If not rsSet.EOF Then
			// get property details
			f_body				= rsSet("NOMINATINGBODY")
			f_authority			= rsSet("FUNDINGAUTHORITY")
			f_rent				= rsSet("RENT")				
			f_services			= rsSet("SERVICES")
			f_counciltax		= rsSet("COUNCILTAX")
			f_waterrates		= rsSet("WATERRATES")
			f_ineligserv		= rsSet("INELIGSERV")
			f_supp				= rsSet("SUPP")
			f_totalrent			= rsSet("TOTALRENT")
			f_renttype			= rsSet("RENTTYPE")		
			f_daterentset		= rsSet("DATERENTSET")
			f_renteffective		= rsSet("RENTEFFECTIVE")
			f_targetrent		= rsSet("TARGETRENT")
			f_yield				= rsSet("YIELD")
			f_capitalvalue		= rsSet("CAPITALVALUE")
			f_insurancevalue	= rsSet("INSURANCEVALUE")
		Else
			pd_body = "<B>Property Not found !!!!</b>"
		End If
		
		CloseRs(rsSet)
		
	End Function	

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY onload="parent.STOPLOADER('TOP')" BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD ROWSPAN=2 WIDTH=70% HEIGHT=100%>
			
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
				<TR>
					<TD CLASS='TITLE' nowrap>Fund Authority</TD>
					<TD WIDTH=220 nowrap><%=f_authority%></TD>
                  	<TD CLASS='TITLE' nowrap>Rent</TD>
                  	<TD WIDTH=300 TITLE='<%=pd_ethnicity%>'><%=FormatCurrency(f_rent)%></TD>
                </TR>
			   	<TR>
					<TD CLASS='TITLE'>Nominating Body</TD>
					<TD><%=f_body%></TD>
					<TD CLASS='TITLE'>Services</TD>
                  	<TD><%=FormatCurrency(f_services)%></TD>
                </TR>
			   	<TR>
					<TD CLASS='TITLE'>Rent Type</TD>
					<TD><%=f_renttype%></TD>
                  	<TD CLASS='TITLE'>Council Tax</TD>
                  	<TD><%=FormatCurrency(f_counciltax)%></TD>
                </TR>
			   	<TR>
            		<TD CLASS='TITLE'>Capital Value</TD>
            		<TD WIDTH=160><%=FormatCurrency(f_capitalvalue)%></TD>
                  	<TD CLASS='TITLE'>Water Rates</TD>
                  	<TD><%=FormatCurrency(f_waterrates)%></TD>
                </TR>
			   	<TR>
           			<!--Was asked to remove but i think some fool will want it back so it's just not displayed for the momentoe - Mr C --->       
            		<TD style='display:none' CLASS='TITLE'>Insurance Value</TD>
                  	<TD style='display:none'><%=FormatCurrency(f_insurancevalue)%></TD>
					<!-- p.s. paul you smell of cheese -->      
            		
					<TD CLASS='TITLE'>Inel. Services</TD>
					<TD><%=FormatCurrency(f_ineligserv)%></TD>
					 <TD CLASS='TITLE'>Yield</TD>
                  	<TD><%=FormatCurrency(f_yield)%></TD>
				</TR>
				<TR>
                 
                  <TD CLASS='TITLE'>Supp. Services</TD>
                  <TD><%=FormatCurrency(f_supp)%></TD>
				  <TD CLASS='TITLE'>Date Rent Set</TD>
                  <TD><%=f_daterentset%></TD>
                </TR>
				<TR>
				  <TD CLASS='TITLE'>Total rent</TD>
                  <TD><B><%=formatcurrency(f_totalrent)%></B></TD>
				  <TD CLASS='TITLE'>Rent Effective To</TD>
   				  <TD><%=f_renteffective%></TD>
                </TR>
				<TR>
            
            		<TD CLASS='TITLE'>Target Rent</TD>
					<TD>
						<TABLE width=100% cellpadding=0 cellspacing=0>
							<TR>
								<TD><%=formatcurrency(f_targetrent)%></TD>
								
							</TR>
						</TABLE>
					</TD>
					<TD align=right colspan=2><INPUT TYPE='BUTTON' style="visibility:hidden;" align=right NAME='BTN_UPDATEPD' VALUE='Update' CLASS='RSLBUTTONSMALL'></TD>
				</TR>
			</TABLE>
			
		</TD>
	</TR>
</FORM>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	